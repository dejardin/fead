EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L FEAD-rescue:GND-power1 #PWR0206
U 1 1 5C0FAE01
P 2075 3650
F 0 "#PWR0206" H 2075 3400 50  0001 C CNN
F 1 "GND" H 2075 3500 50  0000 C CNN
F 2 "" H 2075 3650 50  0000 C CNN
F 3 "" H 2075 3650 50  0000 C CNN
	1    2075 3650
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R202
U 1 1 5C0FAE0F
P 1550 750
F 0 "R202" V 1450 750 50  0000 C CNN
F 1 "DNP" V 1550 750 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 1550 750 50  0001 C CNN
F 3 "" H 1550 750 50  0000 C CNN
	1    1550 750 
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R201
U 1 1 5C0FAE16
P 1450 750
F 0 "R201" V 1350 750 50  0000 C CNN
F 1 "DNP" V 1450 750 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 1450 750 50  0001 C CNN
F 3 "" H 1450 750 50  0000 C CNN
	1    1450 750 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2075 3550 2075 3650
Text HLabel 9400 2700 2    50   Input ~ 0
Ch_out_P[1..5]
$Comp
L FEAD-rescue:GND-power1 #PWR?
U 1 1 5C0FAEE2
P 2075 6750
AR Path="/570DF051/5C0FAEE2" Ref="#PWR?"  Part="1" 
AR Path="/57059335/5C0FAEE2" Ref="#PWR?"  Part="1" 
AR Path="/57068F5F/5C0FAEE2" Ref="#PWR0204"  Part="1" 
F 0 "#PWR0204" H 2075 6500 50  0001 C CNN
F 1 "GND" H 2075 6600 50  0000 C CNN
F 2 "" H 2075 6750 50  0000 C CNN
F 3 "" H 2075 6750 50  0000 C CNN
	1    2075 6750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2075 6750 2075 6650
Connection ~ 2075 6650
Wire Wire Line
	2200 4150 1650 4150
Wire Wire Line
	1650 4150 1650 4100
Wire Wire Line
	2075 4050 2200 4050
Wire Wire Line
	3300 4150 3900 4150
Wire Wire Line
	3900 4150 3900 4100
Wire Wire Line
	3300 4050 3425 4050
Wire Wire Line
	2075 6650 2200 6650
Text HLabel 9400 2775 2    50   Input ~ 0
Ch_out_N[1..5]
$Comp
L MDJ_compo:ERNI-154765 J201
U 1 1 5C13DB87
P 2750 2150
F 0 "J201" H 2750 700 60  0000 C CNN
F 1 "ERNI-154765" V 2750 2150 60  0000 C CNN
F 2 "MDJ_mod:ERNI-154765" H 2750 575 60  0000 C CNN
F 3 "" H 2650 2150 60  0000 C CNN
	1    2750 2150
	-1   0    0    -1  
$EndComp
$Comp
L MDJ_compo:ERNI-154765 J202
U 1 1 5C1E0BD8
P 2750 5350
F 0 "J202" H 2750 3900 60  0000 C CNN
F 1 "ERNI-154765" V 2750 5350 60  0000 C CNN
F 2 "MDJ_mod:ERNI-154765" H 2750 3775 60  0000 C CNN
F 3 "" H 2650 5350 60  0000 C CNN
	1    2750 5350
	-1   0    0    -1  
$EndComp
Text HLabel 1325 950  0    50   Output ~ 0
I2C_clk_in
Text HLabel 3600 1250 2    50   Output ~ 0
ReSync_in-
Wire Wire Line
	2200 2050 1575 2050
Wire Wire Line
	2200 1950 1575 1950
Wire Wire Line
	2200 2850 1575 2850
Wire Wire Line
	2200 2750 1575 2750
Wire Wire Line
	2200 3350 1425 3350
Text Label 3925 2050 2    50   ~ 0
CalBusy_out_1
Text Label 3925 2850 2    50   ~ 0
CalBusy_out_2
Wire Wire Line
	2200 1350 1575 1350
Wire Wire Line
	3300 2050 3925 2050
Wire Wire Line
	2200 2150 1575 2150
Wire Wire Line
	3300 2850 3925 2850
Wire Wire Line
	2200 2950 1575 2950
Wire Wire Line
	3300 4650 3925 4650
Wire Wire Line
	2200 4650 1575 4650
Wire Wire Line
	2200 1650 1575 1650
Text Label 1575 1950 0    50   ~ 0
SEUA_out_1
Text Label 1575 1650 0    50   ~ 0
SEUD_out_1
Wire Wire Line
	2200 2450 1575 2450
Text Label 1575 2750 0    50   ~ 0
SEUA_out_2
Text Label 1575 2450 0    50   ~ 0
SEUD_out_2
Text Label 1575 2950 0    50   ~ 0
PllLock_out_3
Text Label 1575 1350 0    50   ~ 0
PllLock_out_1
Text Label 1575 2150 0    50   ~ 0
PllLock_out_2
Text HLabel 1325 1050 0    50   BiDi ~ 0
I2C_data_in
Text HLabel 3500 6350 2    50   Output ~ 0
Pwup_rstb_in
Wire Wire Line
	3925 1850 3300 1850
Wire Wire Line
	3925 1450 3300 1450
Wire Wire Line
	3925 2650 3300 2650
Wire Wire Line
	3925 2250 3300 2250
Wire Wire Line
	3300 6350 3500 6350
Wire Wire Line
	3300 3350 3900 3350
Text HLabel 3600 1150 2    50   Output ~ 0
ReSync_in+
Text Label 3925 1850 2    50   ~ 0
Ch_out_N1
Text Label 3925 1450 2    50   ~ 0
CLK_in_P1
Text Label 3925 2650 2    50   ~ 0
Ch_out_N2
Text Label 3925 2250 2    50   ~ 0
CLK_in_P2
Text Label 3925 3050 2    50   ~ 0
CLK_in_P3
Wire Wire Line
	3925 3050 3300 3050
Text Label 3925 1550 2    50   ~ 0
CLK_in_N1
Text Label 3925 1750 2    50   ~ 0
Ch_out_P1
Text Label 3925 2350 2    50   ~ 0
CLK_in_N2
Text Label 3925 2550 2    50   ~ 0
Ch_out_P2
Text Label 3925 3150 2    50   ~ 0
CLK_in_N3
Text Label 3925 4350 2    50   ~ 0
Ch_out_P3
Text Label 3925 4450 2    50   ~ 0
Ch_out_N3
Wire Wire Line
	3925 1550 3300 1550
Wire Wire Line
	3925 1750 3300 1750
Wire Wire Line
	3925 2350 3300 2350
Wire Wire Line
	3925 2550 3300 2550
Wire Wire Line
	3925 3150 3300 3150
Wire Wire Line
	3925 4350 3300 4350
Wire Wire Line
	3925 4450 3300 4450
Text Label 1575 2050 0    50   ~ 0
OVF_out_1
Text Label 1575 2850 0    50   ~ 0
OVF_out_2
Wire Wire Line
	3300 1250 3600 1250
Wire Wire Line
	2200 6550 2000 6550
Wire Wire Line
	3925 5250 3300 5250
Wire Wire Line
	3925 4850 3300 4850
Text Label 3925 5250 2    50   ~ 0
Ch_out_N4
Text Label 3925 4850 2    50   ~ 0
CLK_in_P4
Wire Wire Line
	3925 6050 3300 6050
Text Label 3925 6050 2    50   ~ 0
Ch_out_N5
Text Label 3925 5650 2    50   ~ 0
CLK_in_P5
Text Label 3925 4950 2    50   ~ 0
CLK_in_N4
Text Label 3925 5150 2    50   ~ 0
Ch_out_P4
Text Label 3925 5750 2    50   ~ 0
CLK_in_N5
Text Label 3925 5950 2    50   ~ 0
Ch_out_P5
Wire Wire Line
	3925 4950 3300 4950
Wire Wire Line
	3925 5150 3300 5150
Wire Wire Line
	3925 5950 3300 5950
Text HLabel 2000 6550 0    50   Output ~ 0
CalibMode_in
Text Label 1575 4650 0    50   ~ 0
OVF_out_3
Text Label 1575 5450 0    50   ~ 0
OVF_out_4
Text Label 1575 6250 0    50   ~ 0
OVF_out_5
Wire Wire Line
	2200 6450 2000 6450
Text HLabel 2000 6450 0    50   Output ~ 0
TP_trigger_in
Wire Wire Line
	3300 6550 3500 6550
Wire Wire Line
	2200 6350 2000 6350
Wire Wire Line
	2200 5450 1575 5450
Wire Wire Line
	2200 5350 1575 5350
Text Label 3925 5450 2    50   ~ 0
CalBusy_out_4
Wire Wire Line
	2200 6250 1575 6250
Wire Wire Line
	2200 6150 1575 6150
Text Label 3925 6250 2    50   ~ 0
CalBusy_out_5
Text Label 3925 4650 2    50   ~ 0
CalBusy_out_3
Wire Wire Line
	2200 4550 1575 4550
Wire Wire Line
	2200 4750 1575 4750
Wire Wire Line
	3300 5450 3925 5450
Wire Wire Line
	2200 5550 1575 5550
Wire Wire Line
	3300 6250 3925 6250
Text HLabel 2000 6350 0    50   Output ~ 0
TestMode_in
Text HLabel 3500 6550 2    50   Input ~ 0
Temp_CATIA_out
Text Label 1575 4550 0    50   ~ 0
SEUA_out_3
Text Label 1575 3250 0    50   ~ 0
SEUD_out_3
Wire Wire Line
	2200 3250 1575 3250
Text Label 1575 5350 0    50   ~ 0
SEUA_out_4
Text Label 1575 5050 0    50   ~ 0
SEUD_out_4
Wire Wire Line
	2200 5050 1575 5050
Text Label 1575 4750 0    50   ~ 0
PllLock_out_4
Text Label 1575 5550 0    50   ~ 0
PllLock_out_5
Wire Wire Line
	2200 5850 1575 5850
Text Label 1575 6150 0    50   ~ 0
SEUA_out_5
Text Label 1575 5850 0    50   ~ 0
SEUD_out_5
Wire Wire Line
	3300 6450 3500 6450
Text HLabel 3500 6450 2    50   Input ~ 0
Temp_APD_out
Connection ~ 2075 6750
Text HLabel 9400 2875 2    50   Output ~ 0
CLK_in_P[1..5]
Text HLabel 9400 2950 2    50   Output ~ 0
CLK_in_N[1..5]
Text HLabel 9400 3050 2    50   Input ~ 0
CalBusy_out_[1..5]
Text HLabel 9400 3150 2    50   Input ~ 0
OVF_out_[1..5]
Text HLabel 9400 3250 2    50   Input ~ 0
PllLock_out_[1..5]
Text HLabel 9400 3350 2    50   Input ~ 0
SEUA_out_[1..5]
Text HLabel 9400 3450 2    50   Input ~ 0
SEUD_out_[1..5]
$Comp
L FEAD-rescue:FE_VD-power1 #PWR0207
U 1 1 5D390B1F
P 1450 600
F 0 "#PWR0207" H 1450 450 50  0001 C CNN
F 1 "FE_VD" H 1300 675 50  0000 C CNN
F 2 "" H 1450 600 50  0000 C CNN
F 3 "" H 1450 600 50  0000 C CNN
	1    1450 600 
	-1   0    0    -1  
$EndComp
$Comp
L FEAD-rescue:FE_VD-power1 #PWR0201
U 1 1 5D390BA8
P 3900 3350
F 0 "#PWR0201" H 3900 3200 50  0001 C CNN
F 1 "FE_VD" H 3900 3500 50  0000 C CNN
F 2 "" H 3900 3350 50  0000 C CNN
F 3 "" H 3900 3350 50  0000 C CNN
	1    3900 3350
	-1   0    0    -1  
$EndComp
$Comp
L FEAD-rescue:FE_VD-power1 #PWR0203
U 1 1 5D390C15
P 3900 4100
F 0 "#PWR0203" H 3900 3950 50  0001 C CNN
F 1 "FE_VD" H 3915 4273 50  0000 C CNN
F 2 "" H 3900 4100 50  0000 C CNN
F 3 "" H 3900 4100 50  0000 C CNN
	1    3900 4100
	-1   0    0    -1  
$EndComp
$Comp
L FEAD-rescue:FE_VD-power1 #PWR0208
U 1 1 5D390C28
P 1650 4100
F 0 "#PWR0208" H 1650 3950 50  0001 C CNN
F 1 "FE_VD" H 1665 4273 50  0000 C CNN
F 2 "" H 1650 4100 50  0000 C CNN
F 3 "" H 1650 4100 50  0000 C CNN
	1    1650 4100
	-1   0    0    -1  
$EndComp
$Comp
L FEAD-rescue:FE_VD-power1 #PWR0209
U 1 1 5D390C3B
P 1425 3350
F 0 "#PWR0209" H 1425 3200 50  0001 C CNN
F 1 "FE_VD" H 1425 3500 50  0000 C CNN
F 2 "" H 1425 3350 50  0000 C CNN
F 3 "" H 1425 3350 50  0000 C CNN
	1    1425 3350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3425 3550 2075 3550
Connection ~ 2075 3550
Wire Wire Line
	2075 4050 2075 4250
Wire Wire Line
	3925 5750 3300 5750
Wire Wire Line
	3925 5650 3300 5650
Wire Wire Line
	2075 6750 3425 6750
Wire Wire Line
	3300 6650 3425 6650
Connection ~ 3425 6650
Wire Wire Line
	3425 6650 3425 6750
Wire Wire Line
	2200 3450 2075 3450
Connection ~ 2075 3450
Wire Wire Line
	2075 3450 2075 3550
Wire Wire Line
	3300 3450 3425 3450
Connection ~ 3425 3450
Wire Wire Line
	3425 3450 3425 3550
Wire Wire Line
	3300 1150 3425 1150
Wire Wire Line
	3300 850  3425 850 
Wire Wire Line
	3425 1150 3600 1150
Wire Wire Line
	3425 850  3425 950 
Wire Wire Line
	2200 850  2075 850 
Wire Wire Line
	2075 850  2075 1150
Wire Wire Line
	2200 1150 2075 1150
Connection ~ 2075 1150
Wire Wire Line
	2075 1150 2075 1250
Wire Wire Line
	2200 1250 2075 1250
Connection ~ 2075 1250
Wire Wire Line
	2075 1250 2075 1450
Wire Wire Line
	2200 1450 2075 1450
Connection ~ 2075 1450
Wire Wire Line
	2075 1450 2075 1550
Wire Wire Line
	2200 1550 2075 1550
Connection ~ 2075 1550
Wire Wire Line
	2075 1550 2075 1750
Wire Wire Line
	2200 1750 2075 1750
Connection ~ 2075 1750
Wire Wire Line
	2075 1750 2075 1850
Wire Wire Line
	2200 1850 2075 1850
Connection ~ 2075 1850
Wire Wire Line
	2075 1850 2075 2250
Wire Wire Line
	2200 2250 2075 2250
Connection ~ 2075 2250
Wire Wire Line
	2075 2250 2075 2350
Wire Wire Line
	2200 2350 2075 2350
Connection ~ 2075 2350
Wire Wire Line
	2075 2350 2075 2550
Wire Wire Line
	2200 2550 2075 2550
Connection ~ 2075 2550
Wire Wire Line
	2075 2550 2075 2650
Wire Wire Line
	2200 2650 2075 2650
Connection ~ 2075 2650
Wire Wire Line
	2075 2650 2075 3050
Wire Wire Line
	2200 3050 2075 3050
Connection ~ 2075 3050
Wire Wire Line
	2075 3050 2075 3150
Wire Wire Line
	2200 3150 2075 3150
Connection ~ 2075 3150
Wire Wire Line
	3300 2450 3425 2450
Connection ~ 3425 2450
Wire Wire Line
	3425 2450 3425 2750
Wire Wire Line
	3300 2750 3425 2750
Connection ~ 3425 2750
Wire Wire Line
	3425 2750 3425 2950
Wire Wire Line
	3300 2950 3425 2950
Connection ~ 3425 2950
Wire Wire Line
	3425 2950 3425 3250
Wire Wire Line
	3300 3250 3425 3250
Connection ~ 3425 3250
Wire Wire Line
	3425 3250 3425 3450
Wire Wire Line
	3300 2150 3425 2150
Connection ~ 3425 2150
Wire Wire Line
	3425 2150 3425 2450
Wire Wire Line
	3300 1950 3425 1950
Connection ~ 3425 1950
Wire Wire Line
	3425 1950 3425 2150
Wire Wire Line
	3300 1650 3425 1650
Connection ~ 3425 1650
Wire Wire Line
	3425 1650 3425 1950
Wire Wire Line
	3300 1350 3425 1350
Connection ~ 3425 1350
Wire Wire Line
	3425 1350 3425 1650
Wire Wire Line
	3300 1050 3425 1050
Connection ~ 3425 1050
Wire Wire Line
	3425 1050 3425 1350
Wire Wire Line
	3300 950  3425 950 
Connection ~ 3425 950 
Wire Wire Line
	3425 950  3425 1050
Wire Wire Line
	2200 4350 2075 4350
Connection ~ 2075 4350
Wire Wire Line
	2075 4350 2075 4450
Wire Wire Line
	2200 4450 2075 4450
Connection ~ 2075 4450
Wire Wire Line
	2075 4450 2075 4850
Wire Wire Line
	2200 4850 2075 4850
Connection ~ 2075 4850
Wire Wire Line
	2075 4850 2075 4950
Wire Wire Line
	2200 4950 2075 4950
Connection ~ 2075 4950
Wire Wire Line
	2075 4950 2075 5150
Wire Wire Line
	2200 5150 2075 5150
Connection ~ 2075 5150
Wire Wire Line
	2075 5150 2075 5250
Wire Wire Line
	2200 5250 2075 5250
Connection ~ 2075 5250
Wire Wire Line
	2075 5250 2075 5650
Wire Wire Line
	2200 5650 2075 5650
Connection ~ 2075 5650
Wire Wire Line
	2200 5750 2075 5750
Wire Wire Line
	2075 5650 2075 5750
Connection ~ 2075 5750
Wire Wire Line
	2075 5750 2075 5950
Wire Wire Line
	2200 5950 2075 5950
Connection ~ 2075 5950
Wire Wire Line
	2075 5950 2075 6050
Wire Wire Line
	2200 6050 2075 6050
Connection ~ 2075 6050
Wire Wire Line
	2075 6050 2075 6650
Wire Wire Line
	3300 6150 3425 6150
Connection ~ 3425 6150
Wire Wire Line
	3425 6150 3425 6650
Wire Wire Line
	3300 5850 3425 5850
Connection ~ 3425 5850
Wire Wire Line
	3425 5850 3425 6150
Wire Wire Line
	3300 5550 3425 5550
Connection ~ 3425 5550
Wire Wire Line
	3425 5550 3425 5850
Wire Wire Line
	3300 5350 3425 5350
Connection ~ 3425 5350
Wire Wire Line
	3425 5350 3425 5550
Wire Wire Line
	3300 5050 3425 5050
Connection ~ 3425 5050
Wire Wire Line
	3425 5050 3425 5350
Wire Wire Line
	3300 4750 3425 4750
Connection ~ 3425 4750
Wire Wire Line
	3425 4750 3425 5050
Wire Wire Line
	3300 4550 3425 4550
Connection ~ 3425 4550
Wire Wire Line
	3425 4550 3425 4750
Wire Wire Line
	3300 4250 3425 4250
Wire Wire Line
	3425 4050 3425 4250
Connection ~ 3425 4250
Wire Wire Line
	3425 4250 3425 4550
Wire Wire Line
	1450 600  1550 600 
Wire Wire Line
	1325 1050 1550 1050
Wire Wire Line
	1325 950  1450 950 
Connection ~ 1450 600 
Wire Wire Line
	1450 950  1450 900 
Connection ~ 1450 950 
Wire Wire Line
	1450 950  2200 950 
Wire Wire Line
	1550 900  1550 1050
Connection ~ 1550 1050
Wire Wire Line
	1550 1050 2200 1050
Wire Wire Line
	2075 3150 2075 3450
Wire Wire Line
	2200 4250 2075 4250
Connection ~ 2075 4250
Wire Wire Line
	2075 4250 2075 4350
$EndSCHEMATC
