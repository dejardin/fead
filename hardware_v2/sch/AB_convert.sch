EESchema Schematic File Version 2
LIBS:xc7a50tfgg484pkg
LIBS:erni-154742
LIBS:erni-154765
LIBS:power
LIBS:sfp
LIBS:pspice
LIBS:FEAD-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 2750 3350 0    60   Input ~ 0
CH3_IN_[13..0]
Text HLabel 2750 3600 0    60   Output ~ 0
CH3_OUT_[13..0]
$EndSCHEMATC
