EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L FEAD-rescue:PINS_14X2-pins_14x2 U601
U 1 1 572B7D3D
P 1900 4750
F 0 "U601" H 2050 4250 60  0000 C CNN
F 1 "PINS_14X2" V 1900 4700 60  0000 C CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_2x07_P2.00mm_Vertical_SMD" H 1900 4750 60  0001 C CNN
F 3 "" H 1900 4750 60  0000 C CNN
F 4 "Digikey" H 1900 4750 60  0001 C CNN "Supplier"
F 5 "WM18641-ND" H 1900 4750 60  0001 C CNN "Supplier P/N"
	1    1900 4750
	1    0    0    -1  
$EndComp
NoConn ~ 2350 5000
NoConn ~ 2350 5100
$Comp
L FEAD-rescue:GND-power1 #PWR0601
U 1 1 572C82E0
P 1375 5225
F 0 "#PWR0601" H 1375 4975 50  0001 C CNN
F 1 "GND" H 1375 5075 50  0000 C CNN
F 2 "" H 1375 5225 50  0000 C CNN
F 3 "" H 1375 5225 50  0000 C CNN
	1    1375 5225
	1    0    0    -1  
$EndComp
$Comp
L Device:R R603
U 1 1 572C8509
P 6150 3250
F 0 "R603" V 6230 3250 50  0000 C CNN
F 1 "4.7k" V 6150 3250 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" H 6150 3250 50  0001 C CNN
F 3 "" H 6150 3250 50  0000 C CNN
	1    6150 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R607
U 1 1 572C85D6
P 6650 3250
F 0 "R607" V 6730 3250 50  0000 C CNN
F 1 "2.4k" V 6650 3250 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" H 6650 3250 50  0001 C CNN
F 3 "" H 6650 3250 50  0000 C CNN
	1    6650 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R604
U 1 1 572C86D9
P 6400 3250
F 0 "R604" V 6480 3250 50  0000 C CNN
F 1 "330" V 6400 3250 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" H 6400 3250 50  0001 C CNN
F 3 "" H 6400 3250 50  0000 C CNN
	1    6400 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R601
U 1 1 572C8788
P 2700 4300
F 0 "R601" V 2780 4300 50  0000 C CNN
F 1 "4.7k" V 2700 4300 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" H 2700 4300 50  0001 C CNN
F 3 "" H 2700 4300 50  0000 C CNN
	1    2700 4300
	1    0    0    -1  
$EndComp
$Comp
L FEAD-rescue:GND-power1 #PWR0620
U 1 1 572CC065
P 7200 4550
F 0 "#PWR0620" H 7200 4300 50  0001 C CNN
F 1 "GND" H 7200 4400 50  0000 C CNN
F 2 "" H 7200 4550 50  0000 C CNN
F 3 "" H 7200 4550 50  0000 C CNN
	1    7200 4550
	1    0    0    -1  
$EndComp
Text Notes 8100 5050 1    120  ~ 0
24.1Mbit Bitstream\nUsing 32Mb flash
$Comp
L FEAD-rescue:GND-power1 #PWR0614
U 1 1 5791CA1A
P 5950 2200
F 0 "#PWR0614" H 5950 1950 50  0001 C CNN
F 1 "GND" H 5950 2050 50  0000 C CNN
F 2 "" H 5950 2200 50  0000 C CNN
F 3 "" H 5950 2200 50  0000 C CNN
	1    5950 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C601
U 1 1 5791CA20
P 5350 2000
F 0 "C601" V 5450 2150 50  0000 C CNN
F 1 "100nF" V 5450 1850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 5350 2000 50  0001 C CNN
F 3 "" H 5350 2000 50  0000 C CNN
	1    5350 2000
	1    0    0    -1  
$EndComp
$Comp
L FEAD-rescue:GND-power1 #PWR0611
U 1 1 5791CA27
P 5350 2200
F 0 "#PWR0611" H 5350 1950 50  0001 C CNN
F 1 "GND" H 5350 2050 50  0000 C CNN
F 2 "" H 5350 2200 50  0000 C CNN
F 3 "" H 5350 2200 50  0000 C CNN
	1    5350 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R605
U 1 1 5791CA38
P 6550 1900
F 0 "R605" V 6630 1900 50  0000 C CNN
F 1 "8.66k" V 6550 1900 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" H 6550 1900 50  0001 C CNN
F 3 "" H 6550 1900 50  0000 C CNN
	1    6550 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R606
U 1 1 5791CA3F
P 6550 2300
F 0 "R606" V 6630 2300 50  0000 C CNN
F 1 "8.06k" V 6550 2300 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" H 6550 2300 50  0001 C CNN
F 3 "" H 6550 2300 50  0000 C CNN
	1    6550 2300
	1    0    0    -1  
$EndComp
$Comp
L FEAD-rescue:GND-power1 #PWR0617
U 1 1 5791CA46
P 6550 2450
F 0 "#PWR0617" H 6550 2200 50  0001 C CNN
F 1 "GND" H 6550 2300 50  0000 C CNN
F 2 "" H 6550 2450 50  0000 C CNN
F 3 "" H 6550 2450 50  0000 C CNN
	1    6550 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C603
U 1 1 5791CA50
P 6800 1850
F 0 "C603" V 6900 2000 50  0000 C CNN
F 1 "0.47uF" V 6900 1700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6800 1850 50  0001 C CNN
F 3 "" H 6800 1850 50  0000 C CNN
	1    6800 1850
	1    0    0    -1  
$EndComp
$Comp
L FEAD-rescue:GND-power1 #PWR0619
U 1 1 5791CA57
P 6800 2050
F 0 "#PWR0619" H 6800 1800 50  0001 C CNN
F 1 "GND" H 6800 1900 50  0000 C CNN
F 2 "" H 6800 2050 50  0000 C CNN
F 3 "" H 6800 2050 50  0000 C CNN
	1    6800 2050
	1    0    0    -1  
$EndComp
$Comp
L MDJ_compo:ST715MR U602
U 1 1 5791CA60
P 5950 1800
F 0 "U602" H 6300 1300 60  0000 C CNN
F 1 "ST715MR" H 5975 1800 60  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5950 1800 60  0001 C CNN
F 3 "" H 5950 1800 60  0000 C CNN
F 4 "Digikey" H 5950 1800 60  0001 C CNN "Supplier"
F 5 "497-6832-1-ND" H 5950 1800 60  0001 C CNN "Supplier P/N"
	1    5950 1800
	1    0    0    -1  
$EndComp
Text Notes 7500 1900 0    120  ~ 0
V_FLASH is 2.5V\nwe can use 2.5V IOs
$Comp
L Device:C C602
U 1 1 57941BE0
P 5850 3150
F 0 "C602" V 5950 3300 50  0000 C CNN
F 1 "100uF" V 5950 3000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5850 3150 50  0001 C CNN
F 3 "" H 5850 3150 50  0000 C CNN
F 4 "Digi-key" V 5850 3150 60  0001 C CNN "Supplier"
F 5 "490-6542-1-ND	" V 5850 3150 60  0001 C CNN "Supplier Part"
	1    5850 3150
	1    0    0    -1  
$EndComp
$Comp
L FEAD-rescue:+3.6V-power1 #PWR0610
U 1 1 57B262E7
P 5350 1750
F 0 "#PWR0610" H 5350 1600 50  0001 C CNN
F 1 "+3.6V" H 5350 1890 50  0000 C CNN
F 2 "" H 5350 1750 50  0000 C CNN
F 3 "" H 5350 1750 50  0000 C CNN
	1    5350 1750
	1    0    0    -1  
$EndComp
$Comp
L FEAD-rescue:GND-power1 #PWR0606
U 1 1 5B5BF3FE
P 3400 4350
F 0 "#PWR0606" H 3400 4100 50  0001 C CNN
F 1 "GND" H 3400 4200 50  0000 C CNN
F 2 "" H 3400 4350 50  0000 C CNN
F 3 "" H 3400 4350 50  0000 C CNN
	1    3400 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 4600 2350 4600
Wire Wire Line
	3450 4650 2625 4650
Wire Wire Line
	2500 4700 2350 4700
Wire Wire Line
	3450 4700 2700 4700
Wire Wire Line
	1450 4500 1375 4500
Wire Wire Line
	1375 4500 1375 4600
Wire Wire Line
	1450 5000 1375 5000
Connection ~ 1375 5000
Wire Wire Line
	1450 4900 1375 4900
Connection ~ 1375 4900
Wire Wire Line
	1450 4800 1375 4800
Connection ~ 1375 4800
Wire Wire Line
	1450 4700 1375 4700
Connection ~ 1375 4700
Wire Wire Line
	1450 4600 1375 4600
Connection ~ 1375 4600
Wire Wire Line
	1450 5100 1375 5100
Connection ~ 1375 5100
Wire Wire Line
	3450 4450 2700 4450
Wire Wire Line
	2350 4500 2400 4500
Wire Wire Line
	2400 4500 2400 4450
Wire Wire Line
	2700 3900 2700 4150
Wire Wire Line
	6150 3000 6150 3100
Wire Wire Line
	6400 3100 6400 3000
Wire Wire Line
	6650 3000 6650 3100
Wire Wire Line
	7200 1650 7200 3450
Wire Wire Line
	7650 4400 7600 4400
Wire Wire Line
	7650 3450 7650 3900
Wire Wire Line
	7200 3450 7650 3450
Connection ~ 7200 3450
Wire Wire Line
	7600 3900 7650 3900
Connection ~ 7650 3900
Wire Wire Line
	5350 1750 5350 1800
Wire Wire Line
	5350 2200 5350 2150
Wire Wire Line
	5350 1800 5550 1800
Connection ~ 5350 1800
Wire Wire Line
	6550 1650 6550 1750
Wire Wire Line
	6550 2100 6350 2100
Wire Wire Line
	6350 2100 6350 1950
Wire Wire Line
	6800 2050 6800 2000
Wire Wire Line
	6800 1650 6800 1700
Connection ~ 6800 1650
Wire Wire Line
	6350 1650 6550 1650
Connection ~ 6550 1650
Wire Wire Line
	3400 3600 3400 3650
Connection ~ 3400 4250
$Comp
L FEAD-rescue:GND-power1 #PWR0613
U 1 1 5B5C1D15
P 5850 3300
F 0 "#PWR0613" H 5850 3050 50  0001 C CNN
F 1 "GND" H 5850 3150 50  0000 C CNN
F 2 "" H 5850 3300 50  0000 C CNN
F 3 "" H 5850 3300 50  0000 C CNN
	1    5850 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 4250 3400 4250
Wire Wire Line
	3450 4200 3400 4200
Wire Wire Line
	3450 4300 3300 4300
Wire Wire Line
	3300 4300 3300 4200
Connection ~ 3400 4200
Wire Wire Line
	3450 3600 3400 3600
Wire Wire Line
	3450 3650 3400 3650
Connection ~ 3400 3650
Wire Wire Line
	3450 3850 3400 3850
Connection ~ 3400 3850
Wire Wire Line
	3450 3900 3150 3900
Wire Wire Line
	6150 4500 6150 3400
Wire Wire Line
	6400 4550 6400 3400
Wire Wire Line
	5700 4150 5700 4200
Wire Wire Line
	5700 4200 6650 4200
Wire Wire Line
	5700 4050 5700 4000
Wire Wire Line
	5700 4000 6800 4000
Wire Wire Line
	6650 3400 6650 4200
Connection ~ 6650 4200
Wire Wire Line
	1375 5000 1375 5100
Wire Wire Line
	1375 4900 1375 5000
Wire Wire Line
	1375 4800 1375 4900
Wire Wire Line
	1375 4700 1375 4800
Wire Wire Line
	1375 4600 1375 4700
Wire Wire Line
	1375 5100 1375 5225
Wire Wire Line
	7200 3450 7200 3750
Wire Wire Line
	7650 3900 7650 4400
Wire Wire Line
	5350 1800 5350 1850
Wire Wire Line
	6800 1650 7200 1650
Wire Wire Line
	6550 1650 6800 1650
Wire Wire Line
	3400 4250 3400 4350
Wire Wire Line
	3400 4200 3400 4250
Wire Wire Line
	3400 3850 3400 4200
Wire Wire Line
	6650 4200 6800 4200
Wire Wire Line
	4500 3400 4550 3400
Wire Wire Line
	4550 3400 4550 3350
Wire Wire Line
	6550 2050 6550 2100
Wire Wire Line
	6550 2100 6550 2150
Connection ~ 6550 2100
$Comp
L MDJ_compo:XC7K70T-FBG484 U301
U 6 1 5C8A9A43
P 4400 4000
F 0 "U301" H 4400 2878 50  0000 C CNN
F 1 "XC7K70T-FBG484" H 4400 2787 50  0000 C CNN
F 2 "Package_BGA:BGA-484_23.0x23.0mm_Layout22x22_P1.0mm" H 4400 4000 50  0001 C CNN
F 3 "" H 4400 4000 50  0000 C CNN
	6    4400 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 4500 6150 4500
Wire Wire Line
	5350 4550 6400 4550
Wire Wire Line
	5350 4150 5700 4150
Wire Wire Line
	5350 4050 5700 4050
Wire Wire Line
	5350 4100 6800 4100
Wire Wire Line
	5350 4450 5700 4450
Wire Wire Line
	5700 4450 5700 4300
Wire Wire Line
	5700 4300 6800 4300
Connection ~ 4550 3400
$Comp
L FEAD-rescue:VccO-power1 #PWR?
U 1 1 5D4A0ABA
P 3150 3300
AR Path="/571914DE/5D4A0ABA" Ref="#PWR?"  Part="1" 
AR Path="/572B8C2E/5D4A0ABA" Ref="#PWR0604"  Part="1" 
F 0 "#PWR0604" H 3150 3150 50  0001 C CNN
F 1 "VccO" H 3150 3450 50  0000 C CNN
F 2 "" H 3150 3300 50  0000 C CNN
F 3 "" H 3150 3300 50  0000 C CNN
	1    3150 3300
	1    0    0    -1  
$EndComp
$Comp
L FEAD-rescue:VccO-power1 #PWR?
U 1 1 5D4A0B27
P 2400 4450
AR Path="/571914DE/5D4A0B27" Ref="#PWR?"  Part="1" 
AR Path="/572B8C2E/5D4A0B27" Ref="#PWR0602"  Part="1" 
F 0 "#PWR0602" H 2400 4300 50  0001 C CNN
F 1 "VccO" H 2400 4600 50  0000 C CNN
F 2 "" H 2400 4450 50  0000 C CNN
F 3 "" H 2400 4450 50  0000 C CNN
	1    2400 4450
	1    0    0    -1  
$EndComp
$Comp
L FEAD-rescue:VccO-power1 #PWR?
U 1 1 5D4A0BD9
P 2700 3900
AR Path="/571914DE/5D4A0BD9" Ref="#PWR?"  Part="1" 
AR Path="/572B8C2E/5D4A0BD9" Ref="#PWR0603"  Part="1" 
F 0 "#PWR0603" H 2700 3750 50  0001 C CNN
F 1 "VccO" H 2700 4050 50  0000 C CNN
F 2 "" H 2700 3900 50  0000 C CNN
F 3 "" H 2700 3900 50  0000 C CNN
	1    2700 3900
	1    0    0    -1  
$EndComp
$Comp
L FEAD-rescue:VccO-power1 #PWR?
U 1 1 5D4A0C00
P 3300 4200
AR Path="/571914DE/5D4A0C00" Ref="#PWR?"  Part="1" 
AR Path="/572B8C2E/5D4A0C00" Ref="#PWR0605"  Part="1" 
F 0 "#PWR0605" H 3300 4050 50  0001 C CNN
F 1 "VccO" H 3300 4350 50  0000 C CNN
F 2 "" H 3300 4200 50  0000 C CNN
F 3 "" H 3300 4200 50  0000 C CNN
	1    3300 4200
	1    0    0    -1  
$EndComp
$Comp
L FEAD-rescue:VccO-power1 #PWR?
U 1 1 5D4A0CEC
P 4550 3350
AR Path="/571914DE/5D4A0CEC" Ref="#PWR?"  Part="1" 
AR Path="/572B8C2E/5D4A0CEC" Ref="#PWR0609"  Part="1" 
F 0 "#PWR0609" H 4550 3200 50  0001 C CNN
F 1 "VccO" H 4550 3500 50  0000 C CNN
F 2 "" H 4550 3350 50  0000 C CNN
F 3 "" H 4550 3350 50  0000 C CNN
	1    4550 3350
	1    0    0    -1  
$EndComp
$Comp
L FEAD-rescue:VccO-power1 #PWR?
U 1 1 5D4A0DBF
P 5850 3000
AR Path="/571914DE/5D4A0DBF" Ref="#PWR?"  Part="1" 
AR Path="/572B8C2E/5D4A0DBF" Ref="#PWR0612"  Part="1" 
F 0 "#PWR0612" H 5850 2850 50  0001 C CNN
F 1 "VccO" H 5850 3150 50  0000 C CNN
F 2 "" H 5850 3000 50  0000 C CNN
F 3 "" H 5850 3000 50  0000 C CNN
	1    5850 3000
	1    0    0    -1  
$EndComp
$Comp
L FEAD-rescue:VccO-power1 #PWR?
U 1 1 5D4A0DE6
P 6150 3000
AR Path="/571914DE/5D4A0DE6" Ref="#PWR?"  Part="1" 
AR Path="/572B8C2E/5D4A0DE6" Ref="#PWR0615"  Part="1" 
F 0 "#PWR0615" H 6150 2850 50  0001 C CNN
F 1 "VccO" H 6150 3150 50  0000 C CNN
F 2 "" H 6150 3000 50  0000 C CNN
F 3 "" H 6150 3000 50  0000 C CNN
	1    6150 3000
	1    0    0    -1  
$EndComp
$Comp
L FEAD-rescue:VccO-power1 #PWR?
U 1 1 5D4A0E0D
P 6400 3000
AR Path="/571914DE/5D4A0E0D" Ref="#PWR?"  Part="1" 
AR Path="/572B8C2E/5D4A0E0D" Ref="#PWR0616"  Part="1" 
F 0 "#PWR0616" H 6400 2850 50  0001 C CNN
F 1 "VccO" H 6400 3150 50  0000 C CNN
F 2 "" H 6400 3000 50  0000 C CNN
F 3 "" H 6400 3000 50  0000 C CNN
	1    6400 3000
	1    0    0    -1  
$EndComp
$Comp
L FEAD-rescue:VccO-power1 #PWR?
U 1 1 5D4A0E34
P 6650 3000
AR Path="/571914DE/5D4A0E34" Ref="#PWR?"  Part="1" 
AR Path="/572B8C2E/5D4A0E34" Ref="#PWR0618"  Part="1" 
F 0 "#PWR0618" H 6650 2850 50  0001 C CNN
F 1 "VccO" H 6650 3150 50  0000 C CNN
F 2 "" H 6650 3000 50  0000 C CNN
F 3 "" H 6650 3000 50  0000 C CNN
	1    6650 3000
	1    0    0    -1  
$EndComp
$Comp
L MDJ_compo:IS25LP032 U603
U 1 1 5D4A8EA5
P 7200 4150
F 0 "U603" H 7200 4718 60  0000 C CNN
F 1 "IS25LP032" H 7200 4628 39  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 7200 4150 120 0001 C CNN
F 3 "" H 7200 4150 120 0000 C CNN
	1    7200 4150
	1    0    0    -1  
$EndComp
$Comp
L FEAD-rescue:VccAux-power1 #PWR0607
U 1 1 5CFD97D1
P 3675 3150
F 0 "#PWR0607" H 3675 3000 50  0001 C CNN
F 1 "VccAux" H 3692 3323 50  0000 C CNN
F 2 "" H 3675 3150 50  0000 C CNN
F 3 "" H 3675 3150 50  0000 C CNN
	1    3675 3150
	1    0    0    -1  
$EndComp
$Comp
L FEAD-rescue:GND-power1 #PWR0608
U 1 1 5CFDB855
P 4225 3300
F 0 "#PWR0608" H 4225 3050 50  0001 C CNN
F 1 "GND" H 4225 3150 50  0000 C CNN
F 2 "" H 4225 3300 50  0000 C CNN
F 3 "" H 4225 3300 50  0000 C CNN
	1    4225 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 3400 3950 3375
Text Label 7200 2450 1    50   ~ 0
VccROM
Text Label 2925 4700 0    39   ~ 0
TDI
Text Label 2925 4750 0    39   ~ 0
TCK
Text Label 2925 4650 0    39   ~ 0
TDO
Text Label 2925 4600 0    39   ~ 0
TMS
Wire Wire Line
	2500 4750 2500 4700
Wire Wire Line
	2500 4750 3450 4750
Wire Wire Line
	2625 4650 2625 4800
Wire Wire Line
	2350 4800 2625 4800
Wire Wire Line
	2700 4700 2700 4900
Wire Wire Line
	2350 4900 2700 4900
Wire Wire Line
	3950 3300 4225 3300
Wire Wire Line
	3675 3300 3900 3300
Wire Wire Line
	3900 3300 3900 3400
Wire Wire Line
	3400 3650 3400 3800
Wire Wire Line
	3450 3750 3325 3750
Wire Wire Line
	3325 3750 3325 3700
Wire Wire Line
	3325 3375 3950 3375
Connection ~ 3950 3375
Wire Wire Line
	3950 3375 3950 3300
Wire Wire Line
	3450 3700 3325 3700
Connection ~ 3325 3700
Wire Wire Line
	3325 3700 3325 3375
$Comp
L Device:C C604
U 1 1 5CBBE38A
P 3950 3150
F 0 "C604" V 4050 3300 50  0000 C CNN
F 1 "10uF" V 4050 3000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3950 3150 50  0001 C CNN
F 3 "" H 3950 3150 50  0000 C CNN
	1    3950 3150
	0    1    1    0   
$EndComp
Wire Wire Line
	3675 3150 3800 3150
Wire Wire Line
	3675 3150 3675 3300
Wire Wire Line
	4100 3150 4225 3150
Wire Wire Line
	4225 3150 4225 3300
Wire Wire Line
	4000 3400 4000 3375
Wire Wire Line
	3450 3800 3400 3800
Connection ~ 3400 3800
Wire Wire Line
	3400 3800 3400 3850
Wire Wire Line
	3150 3300 3150 3900
Connection ~ 3675 3150
Connection ~ 4225 3300
Wire Wire Line
	4000 3375 3950 3375
$EndSCHEMATC
