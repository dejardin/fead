#define EXTERN extern
#include "gdaq_VFE.h"
#include <TVirtualFFT.h>

void CATIA_calibration(void)
{
  uhal::HwInterface hw=devices.front();
  FILE *fcal;
  Double_t NSPS=160.e6;
  Int_t command, fead_ctrl;
  ValWord<uint32_t> address;
  ValVector< uint32_t > mem;
  double dv=1200./4096.; // 12 bits on 1.2V
  GtkWidget *widget;
  char widget_name[80], widget_text[80];

  char cdum;
  char output_file[256], hname[80];
  //Int_t debug_I2C_saved=daq.debug_I2C;
  Int_t debug_draw_saved=daq.debug_draw;
  Int_t trigger_type_saved=daq.trigger_type;
  Int_t nevent_saved=daq.nevent;
  Int_t nsample_saved=daq.nsample;
  Int_t n_TP_step_saved=daq.n_TP_step;
  Int_t TP_step_saved=daq.TP_step;
  Int_t TP_level_saved=daq.TP_level;
  Int_t TP_width_saved=daq.TP_width;
  Int_t TP_delay_saved=daq.TP_delay;
  Int_t sw_DAQ_delay_saved=daq.sw_DAQ_delay;
  Int_t hw_DAQ_delay_saved=daq.hw_DAQ_delay;
  Int_t LVRB_autoscan_saved=daq.LVRB_autoscan;
  daq.LVRB_autoscan=0;
  Int_t draw=0;
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_debug_draw");
  g_assert (widget);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ

  daq.nevent=1000;
  daq.nsample=100;
  daq.n_TP_step=1;
  daq.TP_step=128;
  daq.TP_level=0;
  daq.TP_width=150;
  Int_t TP_width_recovery=5;
  daq.TP_delay=50;
  daq.sw_DAQ_delay=0;
  daq.hw_DAQ_delay=50;
  UInt_t iret, I2C_data;

  Int_t ADC_reg_val[2][N_ADC_REG];
  Double_t ped[2], mean[6], rms[2];
  Double_t val_gain[2]={10.,1.};
  Double_t RTIA[3]={500.,400.,340.};
  Double_t Rshunt_V1P2=0.10;
  Double_t Rshunt_V2P5=0.10;

// Make calibration of CATIA on test board (single channel)
  Int_t ich=daq.channel_number[0];
  Double_t Vref_ref=1.0;
  if(asic.DTU_version[ich]>=20)
  {
    daq.TP_width/=4.; // With LiTE-DTU >2.0, width is a multiple of 40 MHz clock
  }
  if(asic.CATIA_version[ich]>=21)
  {
    Vref_ref=1.20;
    RTIA[0]=470;
    RTIA[1]=380;
    RTIA[2]=320;
  }
  if(asic.CATIA_version[ich]<14)
  {
    RTIA[0]=500;
    RTIA[1]=400;
  }

// CATIA settings if requested
  Int_t CATIA_number   = asic.CATIA_version[ich]*10000+9999;
  Int_t ref_number[10], n_ref_number=0;
  FILE *fnumber=NULL;
  fnumber=fopen(".last_CATIA_number","r");
  if(fnumber != NULL)
  {
    Int_t eof=0, loc_version;
    char snumber[80];
    while(eof!=EOF)
    {
      eof=fscanf(fnumber,"%s",snumber);
      if(eof!=EOF)
      {
        sscanf(snumber,"%2d",&loc_version);
        sscanf(snumber,"%d",&ref_number[n_ref_number]);
        printf("Number found : %d version : %d\n",ref_number[n_ref_number],loc_version);
        if(loc_version==asic.CATIA_version[ich])
        {
          ref_number[n_ref_number]++;
          CATIA_number=ref_number[n_ref_number];
        }
        n_ref_number++;
      }
    }
    fclose(fnumber);
  }
  printf("Will calibrate CATIA number %d\n",CATIA_number);

//DTU settings if requested
  Int_t use_ref_calib    = FALSE;
  daq.wait_for_ever      = TRUE; // in laser trigger mode, we let wait for ever, waiting for a signal

  Int_t average        = 128;
  Int_t XADC_reg, XADC_Temp, XADC_Vdac, XADC_Vref, XADC_V2P5, XADC_V1P2;
  XADC_Vdac=0x10; // Buffered DAC signal for CATIA-V1-rev3 connected to APD_temp_in
  XADC_Temp=0x11; // Catia temperature connected to CATIA_temp_in
  if(daq.is_VICEPP)XADC_Temp=0x18;
  XADC_Vref=0x12; // Vref_reg connected to APD_temp_out (flying wire) (V1.4)
  if(asic.CATIA_version[2]>=20)
    XADC_Vref=XADC_Temp; // Vref_reg connected to CATIA_temp_in (V2.0)

  Int_t n_Vref=15, n_Vdac=33, n_PED_dac=64;

  TF1 *f1, *fDAC[2];
  sprintf(output_file,"");

  Int_t wait=0;
  UInt_t VFE_control;

  unsigned int ireg,device_number,val;
  ValWord<uint32_t> free_mem, trig_reg, delays, reg, current, fault;

// Switch ON and Initialize current monitors
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_power_ON");
  g_assert (widget);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
// Wait for voltage stabilization
  usleep(1000000);

  printf("Redo pwup_reset and Start LVRB auto scan mode\n");
  hw.getNode("VFE_CTRL").write(1*PWUP_RESETB);
  hw.getNode("VFE_CTRL").write(0*PWUP_RESETB);
  hw.dispatch();

// DTU Resync init sequence:
// For LiTEDTU version >=2.0, put outputs in sync mode with 0xaaaaaaaa
  if(daq.firmware_version<0x21090000)
  {
    printf("Reset DTU\n");
    send_ReSync_command(LiTEDTU_DTU_reset);
    printf("Reset DTU I2C\n");
    send_ReSync_command(LiTEDTU_I2C_reset);
    printf("Reset DTUTestUnit\n");
    send_ReSync_command(LiTEDTU_ADCTestUnit_reset);
    printf("Reset ADCH\n");
    send_ReSync_command(LiTEDTU_ADCH_reset);
    printf("Reset ADCL\n");
    send_ReSync_command(LiTEDTU_ADCL_reset);
  }
  else
  {
    printf("Reset DTU/DTU_I2C/DTUTestUnit\n");
    send_ReSync_command((LiTEDTU_ADCTestUnit_reset<<8) | (LiTEDTU_DTU_reset<<4) | LiTEDTU_I2C_reset);
    printf("Reset ADCH/ADCL\n");
    send_ReSync_command((LiTEDTU_NORMAL_MODE<<8) | (LiTEDTU_ADCL_reset<<4) | LiTEDTU_ADCH_reset);
  }

// Program LiTE-DTU (ADC 1 & 2 ON and 4 output lines
  Int_t num=asic.DTU_number[ich];
  device_number=daq.I2C_LiTEDTU_type*1000+(num<<daq.I2C_shift_dev_number)+2;      // DTU address

  update_LiTEDTU_reg(ich,0);
  update_LiTEDTU_reg(ich,1);

// G10 and G1 baseline subtraction :
  update_LiTEDTU_reg(ich,5);
  update_LiTEDTU_reg(ich,6);

  if(asic.DTU_version[ich]<20)
  {
// Gain switching level :
    update_LiTEDTU_reg(ich,17);
    update_LiTEDTU_reg(ich,18);

// PLL setting :
    update_LiTEDTU_reg(ich,9);
    update_LiTEDTU_reg(ich,8);
// PLL open or closed ?
    update_LiTEDTU_reg(ich,15);
  }
  else
  {
    iret=I2C_RW(hw, device_number, 18, 0x88, 0, 1, 0);
    iret=I2C_RW(hw, device_number, 19, 0x88, 0, 1, 0);
    iret=I2C_RW(hw, device_number, 20, 0x00, 0, 1, 0);
    update_LiTEDTU_reg(ich,17);

// PLL setting, usefull if we force PLL:
    update_LiTEDTU_reg(ich,16);
    update_LiTEDTU_reg(ich,15);
    usleep(10000);

// Overwrite sync pattern word : 0xf0000000
//    iret=I2C_RW(hw, device_number,21, 0x0c, 0, 1, daq.debug_I2C);
//    iret=I2C_RW(hw, device_number,22, 0xcc, 0, 1, daq.debug_I2C);
//    iret=I2C_RW(hw, device_number,23, 0xcc, 0, 1, daq.debug_I2C);
//    iret=I2C_RW(hw, device_number,24, 0xcf, 0, 1, daq.debug_I2C);
// TP duration
    //update_LiTEDTU_reg(ich,25);

    I2C_data=0xfff-asic.DTU_BL_G10[ich]; // switch gain at 4095 - substrated baseline
    printf("Setting G10/G1 gain switching level for channel %d to 0x%x\n",ich,I2C_data);
    iret=I2C_RW(hw, device_number, 7, I2C_data&0xff, 0, 3, daq.debug_I2C);
    iret=I2C_RW(hw, device_number, 8, (I2C_data>>8)&0xf, 0, 3, daq.debug_I2C);
  }
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_width");
  g_assert (widget);
  sprintf(widget_text,"%d",daq.TP_width);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate((GtkWidget*)widget);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_delay");
  g_assert (widget);
  sprintf(widget_text,"%d",daq.TP_delay);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate((GtkWidget*)widget);

// Force test mode for synchronization :
  VFE_control= DELAY_AUTO_TUNE*daq.delay_auto_tune | LVRB_AUTOSCAN*daq.LVRB_autoscan |
           PED_MUX*1 | eLINK_ACTIVE*daq.eLink_active |
           DTU_TEST_MODE*1 | DTU_MEM_MODE*daq.MEM_mode | I2C_LPGBT_MODE*daq.I2C_lpGBT_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();
  usleep(1000);

  reg = hw.getNode("RESYNC_IDLE").read();
  hw.dispatch();
  UInt_t PLL_lock=(reg.value()>>DTU_PLL_LOCK)&0x1f;
  printf("PLL lock : %d",PLL_lock&1);
  printf("\n");
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    Int_t num=asic.DTU_number[ich];
    device_number=daq.I2C_LiTEDTU_type*1000+(num<<daq.I2C_shift_dev_number)+2;      // DTU sub-address
    if(asic.DTU_version[ich]>=20)
    {
      iret=I2C_RW(hw, device_number, 9, 0, 0, 2, daq.debug_I2C);
      printf("PLL status registers 0x09 : 0x%2.2x, ",iret&0xff);
      iret=I2C_RW(hw, device_number, 10, 0, 0, 2, daq.debug_I2C);
      printf("0x0a : 0x%2.2x\n",iret&0xff);
    }
  }

// Once the PLL is set, reset ISERDES instances and get ready for synchronization
  hw.getNode("DELAY_CTRL").write(DELAY_TAP_DIR*1 | DELAY_RESET*1);
  hw.dispatch();
  usleep(1000);

  hw.getNode("DTU_SYNC_PATTERN").write(0xeaaaaaaa); // Default synchro pattern during calibration with LiTEDTU
  reg = hw.getNode("DTU_SYNC_PATTERN").read();
  hw.dispatch();
  printf("Expected Sync patterns %d : 0x%8.8x\n",daq.DTU_test_mode,reg.value());
  hw.getNode("DELAY_CTRL").write(DELAY_TAP_DIR*1 | START_IDELAY_SYNC*0x1f);
  hw.dispatch();
  usleep(1000);
  hw.getNode("DELAY_CTRL").write(DELAY_TAP_DIR*1);
  hw.dispatch();
  reg = hw.getNode("DELAY_CTRL").read();
  hw.dispatch();
  printf("Get sync status : 0x%8.8x, VFE  sync %d, FE sync %d, ch1 0x%2.2x, ch2 0x%2.2x, ch3 0x%2.2x, ch4 0x%2.2x, ch5 0x%2.2x\n",
          reg.value(), (reg.value()>>31)&1, (reg.value()>>30)&1,
         (reg.value()>>0)&0x3f,(reg.value()>>6)&0x3f,(reg.value()>>12)&0x3f,(reg.value()>>18)&0x3f,(reg.value()>>24)&0x3f);

// Read voltages measured by controler
  for(int iLVR=0; iLVR<2; iLVR++)
  {
    char alim[80];
    if(iLVR==0)
    {
      device_number=0x67;
      sprintf(alim,"V2P5");
    }
    else
    {
      device_number=0x6C;
      sprintf(alim,"V1P2");
    }
    val=I2C_RW(hw, device_number, 0x1E, 0,1, 2, 0);
    double Vmeas=((val>>4)&0xfff);
    Vmeas=Vmeas/4096.*102.4; // Convert to V (102.4 V full scale)
    printf("%s measured power supply voltage %7.3f V\n",alim,Vmeas);
    val=I2C_RW(hw, device_number, 0x28, 0,1, 2, 0);
    Vmeas=((val>>4)&0xfff);
    Vmeas=Vmeas/4096.*2.048; // Convert to V (2.048 V full scale)
    if(iLVR==0)Vmeas*=2.;
    printf("%s measured sense voltage %7.3f V\n",alim,Vmeas);
  }

// Set CATIA outputs with calibration levels
  asic.CATIA_Vref_out[ich]=0;
  daq.CATIA_Vcal_out=TRUE;
  iret=update_CATIA_reg(ich,5);
  printf("Set CATIA outputs with calibration levels\n");
  usleep(100000); // Let some time for calibration voltages to stabilize

  //hw.getNode("DELAY_CTRL").write(DELAY_RESET*1);
  //hw.dispatch();
  send_ReSync_command(LiTEDTU_ADCH_reset);
  send_ReSync_command(LiTEDTU_ADCL_reset);
  usleep(1000);
  printf("Launch ADC calibration !\n");
  send_ReSync_command(LiTEDTU_ADCH_calib);
  send_ReSync_command(LiTEDTU_ADCL_calib);
  usleep(10000);

// Reset Test unit to get samples in right order :
  send_ReSync_command(LiTEDTU_ADCTestUnit_reset);

// Restore CAL mux in normal position after calibration
  daq.CATIA_Vcal_out=FALSE;
  iret=update_CATIA_reg(ich,5);
  VFE_control= DELAY_AUTO_TUNE*daq.delay_auto_tune | LVRB_AUTOSCAN*daq.LVRB_autoscan |
           PED_MUX*0 | eLINK_ACTIVE*daq.eLink_active |
           DTU_TEST_MODE*1 | I2C_LPGBT_MODE*daq.I2C_lpGBT_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  printf("Set CATIA outputs with physics levels : 0x%4.4x\n",iret&0xFFFF);
  usleep(100000); // Let some time for DC voltages to stabilize

// Read Temperature and others analog voltages with FPGA ADC
// Read XADC register 0x40 and set the requested average to 1 in XADC
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  ValWord<uint32_t> ave  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  unsigned loc_ave=ave.value()&0xffff;
  printf("Old config register 0x40 content : %x\n",loc_ave);

  loc_ave=0x8000;
  command=DRP_WRb*1 | (0x40<<16) | loc_ave;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  ave  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  loc_ave=ave.value()&0xffff;
  printf("New config register 0x40 content : %x\n",loc_ave);
  command=DRP_WRb*1 | (0x42<<16) | 0x0400;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();
// Switch off temp output on CATIA :
  asic.CATIA_temp_out[ich]=0;
  asic.CATIA_temp_X5[ich]=0;
  iret=update_CATIA_reg(ich,1);

  if(wait==1)
  {
    system("stty raw");
    cdum=getchar();
    system("stty -raw");
    if(cdum=='q')exit(-1);
  }

//===========================================================================================================
// 0 : Get Vdac vs DAC transfer function for current injection 
// Now, make the temp measurement on all catias, sequentially end for both current gain
  TGraph *tg_Vref=new TGraph();
  tg_Vref->SetLineColor(kRed);
  tg_Vref->SetMarkerColor(kRed);
  tg_Vref->SetMarkerStyle(20);
  tg_Vref->SetMarkerSize(1.);
  tg_Vref->SetName("Vref_vs_reg");
  tg_Vref->SetTitle("Vref_vs_reg");
  TGraph *tg_Vdac[2], *tg_Vdac_resi[2];
  tg_Vdac[0] =new TGraph();
  tg_Vdac[0]->SetLineColor(kRed);
  tg_Vdac[0]->SetMarkerColor(kRed);
  tg_Vdac[0]->SetMarkerStyle(20);
  tg_Vdac[0]->SetMarkerSize(1.);
  tg_Vdac[0]->SetName("Vdac1_vs_reg");
  tg_Vdac[0]->SetTitle("Vdac1_vs_reg");
  tg_Vdac[1] =new TGraph();
  tg_Vdac[1]->SetLineColor(kBlue);
  tg_Vdac[1]->SetMarkerColor(kBlue);
  tg_Vdac[1]->SetMarkerStyle(20);
  tg_Vdac[1]->SetMarkerSize(1.);
  tg_Vdac[1]->SetName("Vdac2_vs_reg");
  tg_Vdac[1]->SetTitle("Vdac2_vs_reg");
  tg_Vdac_resi[0] =new TGraph();
  tg_Vdac_resi[0]->SetLineColor(kRed);
  tg_Vdac_resi[0]->SetMarkerColor(kRed);
  tg_Vdac_resi[0]->SetMarkerStyle(20);
  tg_Vdac_resi[0]->SetMarkerSize(1.);
  tg_Vdac_resi[0]->SetName("Vdac1_resi_vs_reg");
  tg_Vdac_resi[0]->SetTitle("Vdac1_resi_vs_reg");
  tg_Vdac_resi[1] =new TGraph();
  tg_Vdac_resi[1]->SetLineColor(kBlue);
  tg_Vdac_resi[1]->SetMarkerColor(kBlue);
  tg_Vdac_resi[1]->SetMarkerStyle(20);
  tg_Vdac_resi[1]->SetMarkerSize(1.);
  tg_Vdac_resi[1]->SetName("Vdac2_resi_vs_reg");
  tg_Vdac_resi[1]->SetTitle("Vdac2_resi_vs_reg");
  Double_t DAC_val[33], Vdac[2][33], Vdac_offset[2], Vdac_slope[2], Vref[15], Temp[3];
  Int_t ref_best=0;
  Double_t dref_best=99999.;
  for(int XADC_meas=0; XADC_meas<5; XADC_meas++)
  {
    XADC_reg=0;
    if(XADC_meas==1)XADC_reg=XADC_Temp; // Temperature measurement
    if(XADC_meas==2)XADC_reg=XADC_Vref; // CATIA Vref measurement (pin 22)
    if(XADC_meas==3)XADC_reg=XADC_Vdac; // CATIA Vdac1 measurement (pin 25)
    if(XADC_meas==4)XADC_reg=XADC_Vdac; // CATIA Vdac2 measurement (pin 25)

    Int_t nmeas=1;
    if(XADC_meas==1)nmeas=2;
    if(XADC_meas==2)nmeas=n_Vref;
    if(XADC_meas==3)nmeas=n_Vdac;
    if(XADC_meas==4)nmeas=n_Vdac;
    for(int imeas=0; imeas<nmeas; imeas++)
    {
      int loc_meas=imeas;
      if(XADC_meas==1) // CATIA Temp measurements
      {
        asic.CATIA_temp_out[ich]=1;
        asic.CATIA_temp_X5[ich]=imeas;
        asic.CATIA_Vref_out[ich]=0;
        iret=update_CATIA_reg(ich,1);
        iret=update_CATIA_reg(ich,5);
        usleep(10000);
      }
      if(XADC_meas==2) // Scan Vref_reg values
      {
        asic.CATIA_temp_out[ich]=0;
        asic.CATIA_temp_X5[ich]=0;
        asic.CATIA_Vref_out[ich]=1;
        iret=update_CATIA_reg(ich,1);
        iret=update_CATIA_reg(ich,5);
        if(imeas>0)loc_meas=imeas+1;
// Do CATIA update through gtk menus
        //iret=update_CATIA_reg(ich,6);
        sprintf(widget_name,"%1.1d_CATIA_Vref_val",ich+1);
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
        g_assert (widget);
        gtk_spin_button_set_value((GtkSpinButton*)widget,(double)loc_meas);
        usleep(10);
      }
      if(XADC_meas==3) // Scan DAC1 values
      {
        asic.CATIA_temp_out[ich]=0;
        asic.CATIA_temp_X5[ich]=0;
        asic.CATIA_Vref_out[ich]=0;
        iret=update_CATIA_reg(ich,1);
        if(imeas==0)
        {
          sprintf(widget_name,"%1.1d_CATIA_Vref_val",ich+1);
          widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
          g_assert (widget);
          gtk_spin_button_set_value((GtkSpinButton*)widget,(double)ref_best);
          printf("Setting Vref to %.3f V : %d\n",Vref_ref,ref_best);
          //asic.CATIA_Vref_val[ich]=ref_best;
          //iret=update_CATIA_reg(ich,6);
        }
        loc_meas=imeas*128;
        if(loc_meas>4095)loc_meas=4095;
        //asic.CATIA_DAC1_status[ich]=TRUE;
        sprintf(widget_name,"%1.1d_CATIA_DAC1_status",ich+1);
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
        g_assert (widget);
        gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
        //asic.CATIA_DAC1_val[ich]=loc_meas;
        sprintf(widget_name,"%1.1d_CATIA_DAC1_val",ich+1);
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
        g_assert (widget);
        sprintf(widget_text,"%4d",loc_meas);
        gtk_entry_set_text((GtkEntry*)widget,widget_text);
        gtk_widget_activate((GtkWidget*)widget);

        //asic.CATIA_DAC2_status[ich]=FALSE;
        sprintf(widget_name,"%1.1d_CATIA_DAC2_status",ich+1);
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
        g_assert (widget);
        gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
        //asic.CATIA_DAC2_val[ich]=loc_meas;
        sprintf(widget_name,"%1.1d_CATIA_DAC2_val",ich+1);
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
        g_assert (widget);
        sprintf(widget_text,"%4d",loc_meas);
        gtk_entry_set_text((GtkEntry*)widget,widget_text);
        gtk_widget_activate((GtkWidget*)widget);

        //iret=update_CATIA_reg(ich,4);
        //iret=update_CATIA_reg(ich,5);
        if(imeas==0)usleep(100000);
        usleep(1000);
      }
      if(XADC_meas==4) // Scan Vdac values
      {
        loc_meas=imeas*128;
        if(loc_meas>4095)loc_meas=4095;
        sprintf(widget_name,"%1.1d_CATIA_DAC1_status",ich+1);
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
        g_assert (widget);
        gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
        sprintf(widget_name,"%1.1d_CATIA_DAC1_val",ich+1);
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
        g_assert (widget);
        sprintf(widget_text,"%4d",loc_meas);
        gtk_entry_set_text((GtkEntry*)widget,widget_text);
        gtk_widget_activate((GtkWidget*)widget);

        sprintf(widget_name,"%1.1d_CATIA_DAC2_status",ich+1);
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
        g_assert (widget);
        gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
        sprintf(widget_name,"%1.1d_CATIA_DAC2_val",ich+1);
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
        g_assert (widget);
        sprintf(widget_text,"%4d",loc_meas);
        gtk_entry_set_text((GtkEntry*)widget,widget_text);
        gtk_widget_activate((GtkWidget*)widget);

        if(imeas==0)usleep(100000);
        usleep(1000);
      }
      while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ

      double ave_val=0.;
      ValWord<uint32_t> temp;
      for(int iave=0; iave<average; iave++)
      {
        command=DRP_WRb*0 | (XADC_reg<<16);
        hw.getNode("DRP_XADC").write(command);
        temp  = hw.getNode("DRP_XADC").read();
        hw.dispatch();
        double loc_val=double((temp.value()&0xffff)>>4)/4096.;
        ave_val+=loc_val;
      }
      ave_val/=average;

      if(XADC_meas==0)
      {
        Temp[0]=ave_val*4096*0.123-273.;
        printf("FPGA temperature, meas %d : %.2f deg\n",imeas, Temp[0]);
      }
      else if(XADC_meas==1)
      {
        ave_val/=daq.Tsensor_divider;
        Temp[imeas+1]=ave_val;
        printf("CATIA 3, X5 %d, temperature : %.4f V\n", imeas,Temp[imeas+1]);
      }
      else if(XADC_meas==2)
      {
        ave_val/=daq.Tsensor_divider; // 4.02k-1k Resistor bridge on PCB
        printf("CATIA 0, Vref DAC %d, Vref : %.4f V\n", loc_meas,ave_val);
        Vref[imeas]=ave_val;
        tg_Vref->SetPoint(imeas,(Double_t)loc_meas,Vref[imeas]);
        if(fabs(Vref[imeas]-Vref_ref)<dref_best)
        {
          dref_best=fabs(Vref[imeas]-Vref_ref);
          ref_best=loc_meas;
          printf("%d %f %d\n",loc_meas,dref_best,ref_best);
        }
        if(CATIA_number<20000)ref_best=3;
        if(wait==1)
        {
          system("stty raw");
          cdum=getchar();
          system("stty -raw");
          if(cdum=='q')exit(-1);
        }
      }
      else if(XADC_meas==3)
      {
        //printf("CATIA 0, TP DAC1 %d, Vdac : %.4f V\n", loc_meas, ave_val);
        ave_val*=2.;
        tg_Vdac[0]->SetPoint(imeas,(Double_t)loc_meas,ave_val);
        DAC_val[imeas]=loc_meas;
        Vdac[0][imeas]=ave_val;
        //printf("DAC %d : %f\n",loc_meas,ave_val);
      }
      else if(XADC_meas==4)
      {
        //printf("CATIA 0, TP DAC2 %d, Vdac : %.4f V\n", loc_meas, ave_val);
        ave_val*=2.;
        tg_Vdac[1]->SetPoint(imeas,(Double_t)loc_meas,ave_val);
        Vdac[1][imeas]=ave_val;
      }
    }
  }
  daq.c1->cd();
  for(int iDAC=0; iDAC<2; iDAC++)
  {
    tg_Vdac[iDAC]->Fit("pol1","WQ","",0.,3500.);
    fDAC[iDAC]=tg_Vdac[iDAC]->GetFunction("pol1");
    Int_t n=tg_Vdac[iDAC]->GetN();
    for(Int_t i=0; i<n; i++)
    {
      Double_t x,y;
      tg_Vdac[iDAC]->GetPoint(i,x,y);
      if(fDAC[iDAC]!=NULL)
        tg_Vdac_resi[iDAC]->SetPoint(i,x,y-fDAC[iDAC]->Eval(x));
      else
        tg_Vdac_resi[iDAC]->SetPoint(i,x,0.);
    }
    Vdac_offset[iDAC]=0.;
    Vdac_slope[iDAC]=0.;
    if(fDAC[iDAC]!=NULL)
    {
      Vdac_offset[iDAC]=fDAC[iDAC]->GetParameter(0)*1000.;
      Vdac_slope[iDAC]=fDAC[iDAC]->GetParameter(1)*1000.;
    }
    printf("Vdac%d offset : %e mV, slope : %e mV/lsb\n",iDAC,Vdac_offset[iDAC],Vdac_slope[iDAC]); 
  }

// Read current values for control :
  printf("Consumption with DAC1 OFF and DAC2 ON :\n");
  val=I2C_RW(hw, 0x67, 0x14, 0,1, 2, daq.debug_I2C); // Read Dsense value
  printf("CATIA V2P5 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V2P5);
  val=I2C_RW(hw, 0x6C, 0x14, 0,1, 2, daq.debug_I2C); // Read Dsense value
  printf("CATIA V1P2 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V1P2);
  if(wait==1)
  {
    system("stty raw");
    cdum=getchar();
    system("stty -raw");
    if(cdum=='q')exit(-1);
  }

// Prepare CATIA for calibration :
// Switch OFF DACs and re-measure power consumption :
  sprintf(widget_name,"%1.1d_CATIA_DAC1_status",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
  sprintf(widget_name,"%1.1d_CATIA_DAC1_val",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_entry_set_text((GtkEntry*)widget,"600");
  gtk_widget_activate((GtkWidget*)widget);
  sprintf(widget_name,"%1.1d_CATIA_DAC2_status",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
  sprintf(widget_name,"%1.1d_CATIA_DAC2_val",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_entry_set_text((GtkEntry*)widget,"600");
  gtk_widget_activate((GtkWidget*)widget);

  sprintf(widget_name,"%1.1d_CATIA_Rconv",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  sprintf(widget_text,"272");
  gtk_button_set_label((GtkButton*)widget,widget_text);
  gtk_widget_activate((GtkWidget*)widget); // switch to 2471 (click on 272 button)
  //asic.CATIA_Rconv[ich]=0;
  //iret=update_CATIA_reg(ich,4);
  //iret=update_CATIA_reg(ich,5);
  //iret=update_CATIA_reg(ich,6);
  usleep(200000);

// Read current values for control :
  printf("Consumption with DAC1 OFF and DAC2 OFF, Vref ON :\n");
  val=I2C_RW(hw, 0x67, 0x14, 0,1, 2, daq.debug_I2C); // Read Dsense value
  printf("CATIA V2P5 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V2P5);
  val=I2C_RW(hw, 0x6C, 0x14, 0,1, 2, daq.debug_I2C); // Read Dsense value
  printf("CATIA V1P2 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V1P2);
  if(wait==1)
  {
    system("stty raw");
    cdum=getchar();
    system("stty -raw");
    if(cdum=='q')exit(-1);
  }

//===========================================================================================================
// 0.9 : Redo ADC calibration after Vref_reg setting :
// Stop LVRB auto scan, not to generate noise in CATIA during calibration

  calib_ADC();

  if(wait==1)
  {
    system("stty raw");
    cdum=getchar();
    system("stty -raw");
    if(cdum=='q')exit(-1);
  }

  if(use_ref_calib)
  {
// Read ADC calibration (full list of registers :
    sprintf(output_file,"data/CATIA_calib/ADC_ref_calib.dat");
    printf("Read reference ADC calibration from file %s\n",output_file);
    fcal=fopen(output_file,"r");
    for(int ireg=0; ireg<N_ADC_REG; ireg++)
    {
      //printf("Register %d : 0x%2.2x 0x%2.2x\n",ireg,ADC_reg_val[0][ireg]&0xFF, ADC_reg_val[1][ireg]&0xFF);
      fscanf(fcal,"%d %d",&ADC_reg_val[0][ireg], &ADC_reg_val[1][ireg]);
    }
    fclose(fcal);
    for(int iADC=0; iADC<2; iADC++)
    {
      Int_t num=asic.DTU_number[daq.channel_number[0]];
      device_number=daq.I2C_LiTEDTU_type*1000+(num<<daq.I2C_shift_dev_number)+iADC;      // ADC sub-addresses
      for(int ireg=0; ireg<N_ADC_REG; ireg++)
      {
        ADC_reg_val[iADC][ireg]=I2C_RW(hw, device_number, ireg, ADC_reg_val[iADC][ireg], 0, 1, 0);
      }
    }
  }

// Reset Test unit to get samples in right order :
  send_ReSync_command(LiTEDTU_ADCTestUnit_reset);

// Read current values for control :
  printf("After LiTE-DTU calibration :\n");
  val=I2C_RW(hw, 0x67, 0x14, 0,1, 2, daq.debug_I2C); // Read Dsense value
  printf("CATIA V2P5 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V2P5);
  val=I2C_RW(hw, 0x6C, 0x14, 0,1, 2, daq.debug_I2C); // Read Dsense value
  printf("CATIA V1P2 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V1P2);

//===========================================================================================================
// 1 : Pedestal value vs PED_DAC value.
// Read 1 event of 1000 samples
  printf("Start Pedestal study\n");
  TGraph *tg_ped_DAC[2];
  tg_ped_DAC[0]=new TGraph();
  tg_ped_DAC[0]->SetLineColor(kRed);
  tg_ped_DAC[0]->SetMarkerColor(kRed);
  tg_ped_DAC[0]->SetMarkerSize(kRed);
  tg_ped_DAC[0]->SetName("Ped_vs_DAC_G1");
  tg_ped_DAC[0]->SetTitle("Ped_vs_DAC_G1");
  tg_ped_DAC[1]=new TGraph();
  tg_ped_DAC[1]->SetLineColor(kBlue);
  tg_ped_DAC[1]->SetMarkerColor(kBlue);
  tg_ped_DAC[1]->SetMarkerSize(kBlue);
  tg_ped_DAC[1]->SetName("Ped_vs_DAC_G10");
  tg_ped_DAC[1]->SetTitle("Ped_vs_DAC_G10");
  Double_t ped_DAC_par[2][2];
  //daq.trigger_type = 0;
  //daq.nevent       = 1;
  //daq.nsample      = 250;
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_ped_event");
  g_assert (widget);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_nevent");
  g_assert (widget);
  gtk_entry_set_text((GtkEntry*)widget,"1");
  gtk_widget_activate((GtkWidget*)widget);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_nsample");
  g_assert (widget);
  gtk_entry_set_text((GtkEntry*)widget,"250");
  gtk_widget_activate((GtkWidget*)widget);

// Switch to triggered mode + internal trigger, reduce noise by disabling CALIB_PULSE  :
  fead_ctrl=  CALIB_PULSE_ENABLED   *0 |
              SELF_TRIGGER_MASK     *0 |
              SELF_TRIGGER_THRESHOLD*0 |
              SELF_TRIGGER          *0 |
              SELF_TRIGGER_LOOP     *0 |
              FIFO_MODE             *1 |
              RESET                 *0;
  hw.getNode("FEAD_CTRL").write(fead_ctrl);

  //Int_t num=asic.CATIA_number[daq.channel_number[0]];
  //device_number=daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+3;      // CATIA address
  //if(ich==4 && daq.I2C_shift_dev_number==4)   device_number = daq.I2C_CATIA_type*1000+((ich+1)<<daq.I2C_shift_dev_number)+0xb;
  //val=I2C_RW(hw, device_number, 1, 2,0, 3, daq.debug_I2C); // Switch off temp sensor
  //val=I2C_RW(hw, device_number, 4, 0x8000,1, 3, daq.debug_I2C); // Switch off TP stuff except Vref 
  //val=I2C_RW(hw, device_number, 5, 0x4000,1, 3, daq.debug_I2C);
  //val=I2C_RW(hw, device_number, 6, (ref_best<<4) | 0x0B, 0, 3, daq.debug_I2C);
  //usleep(100000);


  //asic.CATIA_gain[ich]=3;
  sprintf(widget_name,"%1.1d_CATIA_gain",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  if(asic.CATIA_version[ich]<14)
    gtk_button_set_label((GtkButton*)widget,"500");
  else if(asic.CATIA_version[ich]<21)
    gtk_button_set_label((GtkButton*)widget,"500");
  else
    gtk_button_set_label((GtkButton*)widget,"470");
  gtk_button_clicked((GtkButton*)widget);

  //asic.CATIA_LPF35[ich]=1;
  sprintf(widget_name,"%1.1d_CATIA_LPF35",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);

  Int_t ped_ref[2]={-1,-1};
  for(int idac=0; idac<n_PED_dac; idac++)
  {
    mean[0]=0.;
    mean[1]=0.;
    mean[2]=0.;
    mean[3]=0.;
    mean[4]=0.;
    mean[5]=0.;
    //printf("VCM DAC : %d\n",idac);
    //asic.CATIA_ped_G10[ich]=idac;
    sprintf(widget_name,"%1.1d_CATIA_ped_G10",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    gtk_spin_button_set_value((GtkSpinButton*)widget,(Double_t) idac);
    gtk_widget_activate(widget);

    //asic.CATIA_ped_G1[ich]=idac;
    sprintf(widget_name,"%1.1d_CATIA_ped_G1",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    gtk_spin_button_set_value((GtkSpinButton*)widget,(Double_t) idac);
    gtk_widget_activate(widget);

    while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ

    //iret=update_CATIA_reg(ich,3);
    usleep(100000);
    draw=(idac%4);
    for(int ievt=0; ievt<daq.nevent; ievt++)
    {
      get_event(1, draw);
      draw=0;
      for(Int_t is=0; is<daq.all_sample[0]; is++)
      {
        mean[0]+=daq.fevent[0][is]*dv;
        mean[1]+=daq.fevent[1][is]*dv;
        mean[2]+=daq.fevent[2][is]*dv;
        mean[3]+=daq.fevent[3][is]*dv;
      }
      for(Int_t is=0; is<daq.all_sample[4]; is++)
      {
        mean[4]+=daq.fevent[4][is]*dv;
        mean[5]+=daq.fevent[5][is]*dv;
      }
    }
    mean[0]/=(daq.all_sample[0]*daq.nevent);
    mean[1]/=(daq.all_sample[0]*daq.nevent);
    mean[2]/=(daq.all_sample[0]*daq.nevent);
    mean[3]/=(daq.all_sample[0]*daq.nevent);
    mean[4]/=(daq.all_sample[4]*daq.nevent);
    mean[5]/=(daq.all_sample[4]*daq.nevent);
    tg_ped_DAC[0]->SetPoint(idac,(Double_t)idac,mean[4]);
    tg_ped_DAC[1]->SetPoint(idac,(Double_t)idac,mean[5]);
    if(mean[4]/dv<40. && ped_ref[0]<0.)ped_ref[0]=idac;
    if(mean[5]/dv<40. && ped_ref[1]<0.)ped_ref[1]=idac;
    printf("%d %f (%f %f) %d, %f (%f %f) %d\n",idac,mean[4]/dv,mean[0]/dv,mean[1]/dv,ped_ref[0],mean[5]/dv,mean[2]/dv,mean[3]/dv,ped_ref[1]);
    if(wait==1)
    {
      system("stty raw");
      cdum=getchar();
      system("stty -raw");
      if(cdum=='q')exit(-1);
    }
  }
  ped_ref[0]&=0x3F;
  ped_ref[1]&=0x3F;
  daq.c1->cd();
  tg_ped_DAC[0]->Fit("pol1","WQ","",0.,ped_ref[0]);
  f1=tg_ped_DAC[0]->GetFunction("pol1");
  ped_DAC_par[0][0]=0.;
  ped_DAC_par[0][1]=0.;
  if(f1!=NULL)
  {
    ped_DAC_par[0][0]=f1->GetParameter(0);
    ped_DAC_par[0][1]=f1->GetParameter(1);
  }
  printf("G10  pedestal offset %.2f mV, slope %.2f mV/lsb\n",ped_DAC_par[0][0],ped_DAC_par[0][1]);
  tg_ped_DAC[1]->Fit("pol1","WQ","",0.,ped_ref[1]);
  f1=tg_ped_DAC[1]->GetFunction("pol1");
  ped_DAC_par[1][0]=0.;
  ped_DAC_par[1][1]=0.;
  if(f1!=NULL)
  {
    ped_DAC_par[1][0]=f1->GetParameter(0);
    ped_DAC_par[1][1]=f1->GetParameter(1);
  }
  printf("G1  pedestal offset %.2f mV, slope %.2f mV/lsb\n",ped_DAC_par[1][0],ped_DAC_par[1][1]);

//===========================================================================================================
// 2 : Noise for R=340, R=400 and R=500 and for LPF=50MHz and LPF=35 MHz
  Double_t G1_noise[3][2]={0.,0.,0.,0.,0.,0.};
  Double_t G10_noise[3][2]={0.,0.,0.,0.,0.,0.};
  //daq.trigger_type=0;
  //daq.nevent=1;
  //daq.nsample=26622;
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_ped_event");
  g_assert (widget);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_nevent");
  g_assert (widget);
  gtk_entry_set_text((GtkEntry*)widget,"1");
  gtk_widget_activate((GtkWidget*)widget);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_nsample");
  g_assert (widget);
  gtk_entry_set_text((GtkEntry*)widget,"26622");
  gtk_widget_activate((GtkWidget*)widget);
  Double_t *rex,*rey,*imx,*imy,*mod;
// In test mode, we have 2 samples/word on even lines and 2 sample/word in odd lines
// Thus 4 samples per event in merged lines 4 (G10) and 5 (G1)
  rex=(Double_t*)malloc(daq.nsample*4*sizeof(Double_t));
  imx=(Double_t*)malloc(daq.nsample*4*sizeof(Double_t));
  rey=(Double_t*)malloc(daq.nsample*4*sizeof(Double_t));
  imy=(Double_t*)malloc(daq.nsample*4*sizeof(Double_t));
  mod=(Double_t*)malloc(daq.nsample*4*sizeof(Double_t));
  Int_t loc_nsample=daq.nsample*4;
  TVirtualFFT *fft_f=TVirtualFFT::FFT(1,&loc_nsample,"C2CF K");
  Double_t dt=1./NSPS;
  Double_t tmax=loc_nsample*dt;
  Double_t fmax=1./dt;
  Double_t df=fmax/(loc_nsample);
  TProfile *pmod[2];
  pmod[0]=new TProfile("NDS_G10","NDS_G10",loc_nsample,0,fmax);
  pmod[1]=new TProfile("NDS_G1","NDS_G1",loc_nsample,0,fmax);
  TH1D *hmod[2][6];
  hmod[0][0]=new TH1D("NDS_G10_RTIA_500_LPF_50","NDS_G10_RTIA_500_LPF_50",loc_nsample/2,0.,fmax/2.);
  hmod[0][1]=new TH1D("NDS_G10_RTIA_500_LPF_35","NDS_G10_RTIA_500_LPF_35",loc_nsample/2,0.,fmax/2.);
  hmod[0][2]=new TH1D("NDS_G10_RTIA_400_LPF_50","NDS_G10_RTIA_400_LPF_50",loc_nsample/2,0.,fmax/2.);
  hmod[0][3]=new TH1D("NDS_G10_RTIA_400_LPF_35","NDS_G10_RTIA_400_LPF_35",loc_nsample/2,0.,fmax/2.);
  hmod[0][4]=new TH1D("NDS_G10_RTIA_340_LPF_50","NDS_G10_RTIA_340_LPF_50",loc_nsample/2,0.,fmax/2.);
  hmod[0][5]=new TH1D("NDS_G10_RTIA_340_LPF_35","NDS_G10_RTIA_340_LPF_35",loc_nsample/2,0.,fmax/2.);
  hmod[1][0]=new TH1D("NDS_G1_RTIA_500_LPF_50","NDS_G1_RTIA_500_LPF_50",loc_nsample/2,0.,fmax/2.);
  hmod[1][1]=new TH1D("NDS_G1_RTIA_500_LPF_35","NDS_G1_RTIA_500_LPF_35",loc_nsample/2,0.,fmax/2.);
  hmod[1][2]=new TH1D("NDS_G1_RTIA_400_LPF_50","NDS_G1_RTIA_400_LPF_50",loc_nsample/2,0.,fmax/2.);
  hmod[1][3]=new TH1D("NDS_G1_RTIA_400_LPF_35","NDS_G1_RTIA_400_LPF_35",loc_nsample/2,0.,fmax/2.);
  hmod[1][4]=new TH1D("NDS_G1_RTIA_340_LPF_50","NDS_G1_RTIA_340_LPF_50",loc_nsample/2,0.,fmax/2.);
  hmod[1][5]=new TH1D("NDS_G1_RTIA_340_LPF_35","NDS_G1_RTIA_340_LPF_35",loc_nsample/2,0.,fmax/2.);
  TGraph *tg_pulse[2];
  tg_pulse[0]=new TGraph();
  tg_pulse[0]->SetLineColor(kRed);
  tg_pulse[0]->SetMarkerColor(kRed);
  tg_pulse[0]->SetMarkerSize(0.1);
  tg_pulse[0]->SetTitle("G10_R340_LPF35_pedestal_frame");
  tg_pulse[0]->SetName("G10_R340_LPF35_pedestal_frame");
  tg_pulse[1]=new TGraph();
  tg_pulse[1]->SetLineColor(kRed);
  tg_pulse[1]->SetMarkerColor(kRed);
  tg_pulse[1]->SetMarkerSize(0.1);
  tg_pulse[1]->SetTitle("G1_R340_LPF35_pedestal_frame");
  tg_pulse[1]->SetName("G1_R340_LPF35_pedestal_frame");

  sprintf(widget_name,"%1.1d_CATIA_gain",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  if(asic.CATIA_version[ich]<14)
    gtk_button_set_label((GtkButton*)widget,"500");
  else if(asic.CATIA_version[ich]<21)
    gtk_button_set_label((GtkButton*)widget,"500");
  else
    gtk_button_set_label((GtkButton*)widget,"470");
  sprintf(widget_name,"%1.1d_CATIA_ped_G10",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_spin_button_set_value((GtkSpinButton*)widget,(Double_t) ped_ref[0]);
  gtk_widget_activate(widget);
  sprintf(widget_name,"%1.1d_CATIA_ped_G1",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_spin_button_set_value((GtkSpinButton*)widget,(Double_t) ped_ref[1]);
  gtk_widget_activate(widget);

  for(int iRTIA=2; iRTIA>=0; iRTIA--)
  {
    Int_t jRTIA=iRTIA;
    if(iRTIA==2)jRTIA=3;

    sprintf(widget_name,"%1.1d_CATIA_gain",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    gtk_button_clicked((GtkButton*)widget);

    for(int iLPF=0; iLPF<2; iLPF++)
    {
      pmod[0]->Reset();
      pmod[1]->Reset();

      //asic.CATIA_ped_G10[ich]=ped_ref[0];
      //asic.CATIA_ped_G1[ich] =ped_ref[1];
      //asic.CATIA_LPF35[ich] =iLPF;
      //asic.CATIA_gain[ich] =jRTIA;
      sprintf(widget_name,"%1.1d_CATIA_LPF35",ich+1);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      if(iLPF==0)
        gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      else
        gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);

      while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ

      //iret=update_CATIA_reg(ich,3);
      usleep(100000);
      draw=1;
      for(int ievt=0; ievt<daq.nevent; ievt++)
      {
        ped[0]=0.,rms[0]=0.;
        ped[1]=0.,rms[1]=0.;
        get_event(1,draw);
        draw=0;

        Double_t loc_rms=0., loc_ped=0.;
        Int_t ngood=0;
        for(Int_t is=0; is<daq.all_sample[4]; is++)
        {
          loc_ped+=daq.fevent[4][is]*dv;
          loc_rms+=daq.fevent[4][is]*dv*daq.fevent[4][is]*dv;
          Double_t t=dt*is;
          if(iRTIA==1 && iLPF==1 && ievt==0) tg_pulse[0]->SetPoint(is,t,daq.fevent[4][is]*dv);
          rex[is]=daq.fevent[4][is]*dv;
          imx[is]=0.;
        }
        loc_ped/=daq.all_sample[4];
        loc_rms/=daq.all_sample[4];
        loc_rms=sqrt(loc_rms-loc_ped*loc_ped);
        for(Int_t is=0; is<daq.all_sample[4]; is++)
        {
          if(fabs(daq.fevent[4][is]*dv-loc_ped)<loc_rms*5.)
          {
            ped[0]+=daq.fevent[4][is]*dv;
            rms[0]+=daq.fevent[4][is]*dv*daq.fevent[4][is]*dv;
            ngood++;
          }
        }
        ped[0]/=ngood;
        rms[0]/=ngood;
        rms[0]=rms[0]-ped[0]*ped[0];

        G10_noise[iRTIA][iLPF]+=rms[0];
        fft_f->SetPointsComplex(rex,imx);
        fft_f->Transform();
        fft_f->GetPointsComplex(rey,imy);
        for(Int_t is=0; is<daq.all_sample[4]; is++)
        {
          Double_t f=df*(is+0.5);
          rey[is]/=daq.all_sample[4];
          imy[is]/=daq.all_sample[4];
          mod[is]=rey[is]*rey[is]+imy[is]*imy[is];
          pmod[0]->Fill(f,mod[is]/df);
        }

        loc_ped=0.; loc_rms=0.; ngood=0;
        for(Int_t is=0; is<daq.all_sample[5]; is++)
        {
          loc_ped+=daq.fevent[5][is]*dv;
          loc_rms+=daq.fevent[5][is]*dv*daq.fevent[5][is]*dv;
          Double_t t=dt*is;
          if(iRTIA==1 && iLPF==1 && ievt==0)tg_pulse[1]->SetPoint(is,t,daq.fevent[5][is]*dv);
          rex[is]=daq.fevent[5][is]*dv;
          imx[is]=0.;
        }
        loc_ped/=daq.all_sample[5];
        loc_rms/=daq.all_sample[5];
        loc_rms=sqrt(loc_rms-loc_ped*loc_ped);
        for(Int_t is=0; is<daq.all_sample[5]; is++)
        {
          if(fabs(daq.fevent[5][is]*dv-loc_ped)<loc_rms*5.)
          {
            ped[1]+=daq.fevent[5][is]*dv;
            rms[1]+=daq.fevent[5][is]*dv*daq.fevent[5][is]*dv;
            ngood++;
          }
        }
        ped[1]/=ngood;
        rms[1]/=ngood;
        rms[1]=rms[1]-ped[1]*ped[1];
        G1_noise[iRTIA][iLPF]+=rms[1];
        fft_f->SetPointsComplex(rex,imx);
        fft_f->Transform();
        fft_f->GetPointsComplex(rey,imy);
        for(Int_t is=0; is<daq.all_sample[5]; is++)
        {
          Double_t f=df*(is+0.5);
          rey[is]/=daq.all_sample[5];
          imy[is]/=daq.all_sample[5];
          mod[is]=rey[is]*rey[is]+imy[is]*imy[is];
          pmod[1]->Fill(f,mod[is]/df);
        }
        if(ievt==0)printf("G10 noise : %.0f uV, %d good, G1 noise : %.0f uV\n",sqrt(rms[0])*1000.,ngood,sqrt(rms[1])*1000.);
        if(wait==1)
        {
          system("stty raw");
          cdum=getchar();
          system("stty -raw");
          if(cdum=='q')exit(-1);
        }
      }
      G1_noise[iRTIA][iLPF]=sqrt(G1_noise[iRTIA][iLPF]/daq.nevent)*1000.;
      G10_noise[iRTIA][iLPF]=sqrt(G10_noise[iRTIA][iLPF]/daq.nevent)*1000.;
      hmod[0][iRTIA*2+iLPF]->SetBinContent(1,0.);
      hmod[1][iRTIA*2+iLPF]->SetBinContent(1,0.);
      for(Int_t is=1; is<daq.all_sample[4]/2; is++)
      {
        Double_t mod=0.;
        mod=pmod[0]->GetBinContent(is+1);
        hmod[0][iRTIA*2+iLPF]->SetBinContent(is+1,sqrt(2.*mod));
        mod=pmod[1]->GetBinContent(is+1);
        hmod[1][iRTIA*2+iLPF]->SetBinContent(is+1,sqrt(2.*mod));
      }
    }
  }

  fnumber=fopen(".last_CATIA_number","w+");
  for(Int_t i=0; i<n_ref_number; i++)
  {
    fprintf(fnumber,"%d",ref_number[i]);
    if(i<n_ref_number-1)fprintf(fnumber,"\n");
  }
  fclose(fnumber);

  sprintf(output_file,"data/CATIA_calib/CATIA_%5.5d_calib.root",CATIA_number);
  TFile *fd=new TFile(output_file,"recreate");

//===========================================================================================================
// 3 : Send internal TP triggers for linearity measurements : G10 and G1
// calibration trigger setting :
  //daq.trigger_type=1;
  //daq.nevent=100;
  //daq.nevent=10;
  //daq.nsample=100;
  //daq.n_TP_step=60;
  //daq.TP_step=64;
  //daq.TP_level=0.;
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_event");
  g_assert (widget);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_nevent");
  g_assert (widget);
  gtk_entry_set_text((GtkEntry*)widget,"10");
  gtk_widget_activate((GtkWidget*)widget);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_nsample");
  g_assert (widget);
  gtk_entry_set_text((GtkEntry*)widget,"100");
  gtk_widget_activate((GtkWidget*)widget);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_level");
  g_assert (widget);
  gtk_entry_set_text((GtkEntry*)widget,"   0");
  gtk_widget_activate((GtkWidget*)widget);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_step");
  g_assert (widget);
  gtk_entry_set_text((GtkEntry*)widget,"  40");
  gtk_widget_activate((GtkWidget*)widget);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_n_TP_step");
  g_assert (widget);
  gtk_entry_set_text((GtkEntry*)widget,"  80");
  gtk_widget_activate((GtkWidget*)widget);

  Double_t V_Iinj_slope[2], INL_min[2]={9999.,9999.}, INL_max[2]={-9999.,-9999.};
  TProfile *tp_inj1[3][daq.n_TP_step];
  TH1D     *hp_inj1[3][daq.n_TP_step];
  for(int iinj=0; iinj<daq.n_TP_step; iinj++)
  {
    sprintf(hname,"tpulse_G10_DAC_%4.4d",daq.TP_level+iinj*daq.TP_step);
    tp_inj1[0][iinj]=new TProfile(hname,hname,daq.nsample*4,0.,dt*daq.nsample*4);
    sprintf(hname,"tpulse_G1_DAC_%4.4d",daq.TP_level+iinj*daq.TP_step);
    tp_inj1[1][iinj]=new TProfile(hname,hname,daq.nsample*4,0.,dt*daq.nsample*4);
    sprintf(hname,"hpulse_G10_DAC_%4.4d",daq.TP_level+iinj*daq.TP_step);
    hp_inj1[0][iinj]=new TH1D(hname,hname,daq.nsample*4,0.,dt*daq.nsample*4);
    //hp_inj1[0][iinj]=new TH1D(hname,hname,daq.nsample*2,0.,dt*daq.nsample*4);
    sprintf(hname,"hpulse_G1_DAC_%4.4d",daq.TP_level+iinj*daq.TP_step);
    hp_inj1[1][iinj]=new TH1D(hname,hname,daq.nsample*4,0.,dt*daq.nsample*4);
    //hp_inj1[1][iinj]=new TH1D(hname,hname,daq.nsample*2,0.,dt*daq.nsample*4);
  }
  sprintf(hname,"pulse_G10_DAC_Rcal");
  TProfile *tp_inj_cal=new TProfile(hname,hname,daq.nsample*4,0.,dt*daq.nsample*4);
  TGraph *tg_inj1[2], *tg_inj2[2], *tg_inj3[2];
  tg_inj1[0]=new TGraph();
  tg_inj1[0]->SetTitle("Inj_vs_iDAC_G10");
  tg_inj1[0]->SetName("Inj_vs_iDAC_G10");
  tg_inj1[0]->SetMarkerStyle(20);
  tg_inj1[0]->SetMarkerSize(1.0);
  tg_inj1[0]->SetMarkerColor(kRed);
  tg_inj1[0]->SetLineColor(kRed);
  tg_inj1[0]->SetLineWidth(2);
  tg_inj1[1]=new TGraph();
  tg_inj1[1]->SetTitle("Inj_vs_iDAC_G1");
  tg_inj1[1]->SetName("Inj_vs_iDAC_G1");
  tg_inj1[1]->SetMarkerStyle(20);
  tg_inj1[1]->SetMarkerSize(1.0);
  tg_inj1[1]->SetMarkerColor(kBlue);
  tg_inj1[1]->SetLineColor(kBlue);
  tg_inj1[1]->SetLineWidth(2);
  tg_inj2[0]=new TGraph();
  tg_inj2[0]->SetTitle("Inj_vs_vDAC_G10");
  tg_inj2[0]->SetName("Inj_vs_vDAC_G10");
  tg_inj2[0]->SetMarkerStyle(20);
  tg_inj2[0]->SetMarkerSize(1.0);
  tg_inj2[0]->SetMarkerColor(kRed);
  tg_inj2[0]->SetLineColor(kRed);
  tg_inj2[0]->SetLineWidth(2);
  tg_inj2[1]=new TGraph();
  tg_inj2[1]->SetTitle("Inj_vs_vDAC_G1");
  tg_inj2[1]->SetName("Inj_vs_vDAC_G1");
  tg_inj2[1]->SetMarkerStyle(20);
  tg_inj2[1]->SetMarkerSize(1.0);
  tg_inj2[1]->SetMarkerColor(kBlue);
  tg_inj2[1]->SetLineColor(kBlue);
  tg_inj2[1]->SetLineWidth(2);
  tg_inj3[0]=new TGraph();
  tg_inj3[0]->SetTitle("INL_vs_amplitude_G10");
  tg_inj3[0]->SetName("INL_vs_amplitude_G10");
  tg_inj3[0]->SetMarkerStyle(20);
  tg_inj3[0]->SetMarkerSize(1.0);
  tg_inj3[0]->SetMarkerColor(kRed);
  tg_inj3[0]->SetLineColor(kRed);
  tg_inj3[0]->SetLineWidth(2);
  tg_inj3[0]->SetMaximum(0.003);
  tg_inj3[0]->SetMinimum(-.003);
  tg_inj3[1]=new TGraph();
  tg_inj3[1]->SetTitle("INL_vs_amplitude_G1");
  tg_inj3[1]->SetName("INL_vs_amplitude_G1");
  tg_inj3[1]->SetMarkerStyle(20);
  tg_inj3[1]->SetMarkerSize(1.0);
  tg_inj3[1]->SetMarkerColor(kBlue);
  tg_inj3[1]->SetLineColor(kBlue);
  tg_inj3[1]->SetLineWidth(2);
  tg_inj3[1]->SetMaximum(0.003);
  tg_inj3[1]->SetMinimum(-.003);

  //command=(daq.TP_delay<<16) | (daq.TP_width&0xffff);
  //printf("Calibration trigger with %d clocks width and %d clocks delay : %x\n",daq.TP_width,daq.TP_delay,command);
  //hw.getNode("CALIB_CTRL").write(command);

  //asic.CATIA_LPF35[ich]=1;
  //asic.CATIA_gain[ich] =3;
  //iret=update_CATIA_reg(ich,3);
  sprintf(widget_name,"%1.1d_CATIA_gain",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  if(asic.CATIA_version[ich]<14)
    gtk_button_set_label((GtkButton*)widget,"500");
  else if(asic.CATIA_version[ich]<21)
    gtk_button_set_label((GtkButton*)widget,"500");
  else
    gtk_button_set_label((GtkButton*)widget,"470");
  gtk_button_clicked((GtkButton*)widget);
  sprintf(widget_name,"%1.1d_CATIA_LPF35",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  usleep(100000);

  while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ

// First round to compute the correction to apply to G10 TP due to an un-understood bug in CATIA-V2.x
  Double_t y_low=-9999., y_up=9999., ped_cal=0.;;
  daq.TP_level=450;
  //asic.CATIA_Rconv[ich]=0;
  //iret=update_CATIA_reg(ich,6);
  sprintf(widget_name,"%1.1d_CATIA_DAC1_status",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  sprintf(widget_name,"%1.1d_CATIA_DAC2_status",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
  sprintf(widget_name,"%1.1d_CATIA_DAC1_val",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  sprintf(widget_text,"%4d",daq.TP_level);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate((GtkWidget*)widget);
  sprintf(widget_name,"%1.1d_CATIA_Rconv",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  sprintf(widget_text,"272");
  gtk_button_set_label((GtkButton*)widget,widget_text);
  gtk_widget_activate((GtkWidget*)widget); // switch to 2471 (click on 272 button)

  if(wait==1)
  {
    system("stty raw");
    cdum=getchar();
    system("stty -raw");
    if(cdum=='q')exit(-1);
  }

  //asic.CATIA_DAC1_status[ich]=TRUE;
  sprintf(widget_name,"%1.1d_CATIA_DAC1_status",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);

  while(y_low<y_up-0.5)
  {
    daq.TP_level++;
    sprintf(widget_name,"%1.1d_CATIA_DAC1_val",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    sprintf(widget_text,"%4d",daq.TP_level);
    gtk_entry_set_text((GtkEntry*)widget,widget_text);
    gtk_widget_activate((GtkWidget*)widget);
    while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ
    usleep(100);

    tp_inj_cal->Reset();
    for(Int_t ievt=0; ievt<10; ievt++)
    {
      get_event(1,draw);
      draw=0;
      if(daq.error>0) printf("DAQ error on event %d : %d\n",ievt, daq.error);
      for(int is=0; is<daq.all_sample[4]; is++)
      {
        Double_t y=daq.fevent[4][is]*dv;
        if(y<1.e-6)y=1.e-6;
        tp_inj_cal->Fill(dt*(is+0.5),y);
      }
    }
    tp_inj_cal->Fit("pol0","wq","",1475.e-9,1500.e-9);
    f1=tp_inj_cal->GetFunction("pol0");
    y_low=0.;
    if(f1!=NULL) y_low=f1->GetParameter(0);
    tp_inj_cal->Fit("pol0","wq","",1600.e-9,1625.e-9);
    f1=tp_inj_cal->GetFunction("pol0");
    y_up=0.;
    if(f1!=NULL) y_up=f1->GetParameter(0);
    tp_inj_cal->Fit("pol0","wq","",0.,100.e-9);
    f1=tp_inj_cal->GetFunction("pol0");
    ped_cal=0.;
    if(f1!=NULL)ped_cal=f1->GetParameter(0);
  }
  printf("level %d : low %.2f, up %.2f\n",daq.TP_level,y_low,y_up);
  Int_t TP_base=daq.TP_level-10;
 
  sprintf(widget_name,"%1.1d_CATIA_Rconv",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  sprintf(widget_text,"272");
  gtk_button_set_label((GtkButton*)widget,widget_text);
  for(int igain=0; igain<2; igain++)
  {
    //daq.TP_level=TP_base;
    daq.TP_level=0;
    //asic.CATIA_Rconv[ich]=1-igain;
    //iret=update_CATIA_reg(ich,6);

    sprintf(widget_name,"%1.1d_CATIA_Rconv",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    gtk_widget_activate((GtkWidget*)widget); // switch to 2471 (click on 272 button) and vice-versa

    Double_t fit_min=-1, fit_max=-1;
    Int_t loc_step=0;
    for(int istep=0; istep<daq.n_TP_step; istep++)
    {
// Program DAC for this step
      //asic.CATIA_DAC1_status[ich]=TRUE;
      //asic.CATIA_DAC1_val[ich]=daq.TP_level;
      //iret=update_CATIA_reg(ich,4);
      sprintf(widget_name,"%1.1d_CATIA_DAC1_val",ich+1);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      sprintf(widget_text,"%4d",daq.TP_level);
      gtk_entry_set_text((GtkEntry*)widget,widget_text);
      gtk_widget_activate((GtkWidget*)widget);
      while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ

// Wait for Vdac to stabilize :
      if(istep==0)
        usleep(10000);
      else
        usleep(100);

      draw=(istep%4);
      for(Int_t ievt=0; ievt<daq.nevent; ievt++)
      {
        get_event(1,draw);
        draw=0;
        if(daq.error>0) printf("DAQ error on event %d : %d\n",ievt, daq.error);

        for(int is=0; is<daq.all_sample[4]; is++)
        {
          Double_t y=daq.fevent[4+igain][is]*dv;
          if(y<1.e-6)y=1.e-6;
          tp_inj1[igain][istep]->Fill(dt*(is+0.5),y);
        }
        //for(int is=0; is<daq.all_sample[1]; is++)
        //{
        //  Double_t y=daq.fevent[1+igain*2][is]*dv;
        //  if(y<1.e-6)y=1.e-6;
        //  tp_inj1[igain][istep]->Fill(2.*dt*(is+0.5),y);
        //}
      }
      for(int is=0; is<daq.all_sample[4]; is++)
      //for(int is=0; is<daq.all_sample[1]; is++)
      {
        Double_t y_raw, ey_raw, cal_cor;
        y_raw=tp_inj1[igain][istep]->GetBinContent(is+1);
        ey_raw=tp_inj1[igain][istep]->GetBinError(is+1);
        //cal_cor=tp_inj_cal->GetBinContent(is+1);
        //if(CATIA_number>=20000 && igain==0)
        //  hp_inj1[igain][istep]->SetBinContent(is+1,y_raw-cal_cor+ped_cal);
        //else
          hp_inj1[igain][istep]->SetBinContent(is+1,y_raw);
        hp_inj1[igain][istep]->SetBinError(is+1,ey_raw);
      }

      if(wait==1)
      {
        system("stty raw");
        cdum=getchar();
        system("stty -raw");
        if(cdum=='q')exit(-1);
      }
      hp_inj1[igain][istep]->Fit("pol0","wq","",300.e-9,700.e-9);
      f1=hp_inj1[igain][istep]->GetFunction("pol0");
      mean[igain]=0.;
      if(f1!=NULL) mean[igain]=f1->GetParameter(0);
      hp_inj1[igain][istep]->Fit("pol1","wq","",1000.e-9,1500.e-9);
      f1=hp_inj1[igain][istep]->GetFunction("pol1");
      Double_t val0=0.;
      if(f1!=NULL) val0=f1->GetParameter(0)+f1->GetParameter(1)*800.e-9-mean[igain];
      
      if(val0<-2.){daq.TP_level+=daq.TP_step; continue;}
      if(val0>1150. || (igain==1 && istep>75))
      {
        //if(igain==1)cdebug->Write();
        break;
      }

      tg_inj1[igain]->SetPoint(loc_step,daq.TP_level,val0);
      tg_inj2[igain]->SetPoint(loc_step,fDAC[0]->Eval(daq.TP_level)*1000.,val0);
      if(fit_min<0 && val0>0.)fit_min=daq.TP_level-daq.TP_step/2.;
      if(val0<1190.)fit_max=daq.TP_level+daq.TP_step/2.;
      loc_step++;
      daq.TP_level+=daq.TP_step;
    }
    tg_inj1[igain]->Fit("pol1","WQ","",fit_min,fit_max);
    tg_inj2[igain]->Fit("pol1","WQ","",fDAC[0]->Eval(fit_min)*1000.,fDAC[0]->Eval(fit_max)*1000.);
    f1=tg_inj2[igain]->GetFunction("pol1");
    V_Iinj_slope[igain]=0.;
    if(f1!=NULL) V_Iinj_slope[igain]=f1->GetParameter(1);
    printf("Gain %.0f : V_Iinj slope = %.3f\n",val_gain[igain],V_Iinj_slope[igain]);
    f1=tg_inj1[igain]->GetFunction("pol1");
// Compute residuals :
    Int_t n=tg_inj1[igain]->GetN();
    for(Int_t is=0; is<n; is++)
    {
      Double_t x,y;
      tg_inj1[igain]->GetPoint(is,x,y);
      if(f1!=NULL)
      {
        tg_inj3[igain]->SetPoint(is,y,(y-f1->Eval(x))/1200.);
        if(y>0. && y<1200. && (y-f1->Eval(x))/1200.>INL_max[igain])INL_max[igain]=(y-f1->Eval(x))/1200.;
        if(y>0. && y<1200. && (y-f1->Eval(x))/1200.<INL_min[igain])INL_min[igain]=(y-f1->Eval(x))/1200.;
      }
    }
    printf("Gain %.0f INL min %.2f max %.2f\n",val_gain[igain],INL_min[igain]*1000., INL_max[igain]*1000.);
  }

/*
//===========================================================================================================
// 3.1 : Send internal TP triggers for afterpulse recovery study in G10
// Send TP in G1 and look at G10 output
// calibration trigger setting :
  daq.trigger_type=1;
  //daq.nevent=100;
  daq.nevent=1;
  daq.nsample=26622;
  daq.n_TP_step=60;
  daq.TP_step=64;
  daq.TP_level=0.;

  for(int iinj=0; iinj<daq.n_TP_step; iinj++)
  {
    sprintf(hname,"recovery_G10_DAC_G1_%4.4d",daq.TP_level+iinj*daq.TP_step);
    tp_inj1[2][iinj]=new TProfile(hname,hname,daq.nsample*4,0.,dt*daq.nsample*4);
  }

  command=(daq.TP_delay<<16) | (daq.TP_width_recovery&0xffff);
  printf("Calibration trigger with %d clocks width and %d clocks delay : %x\n",daq.TP_width_recovery,daq.TP_delay,command);
  hw.getNode("CALIB_CTRL").write(command);
// Reset the reading base address :

  asic.CATIA_gain[ich]=3;
  asic.CATIA_ped_G10[ich]=0;
  asic.CATIA_ped_G1[ich] =0;
  asic.CATIA_LPF35[ich]  =1;
  iret=update_CATIA_reg(ich,3);
  usleep(10000);
  for(int igain=0; igain<1; igain++)
  {
    daq.TP_level=0;
    asic.CATIA_Rconv[ich]=1-igain;
    iret=update_CATIA_reg(ich,6);
    for(int istep=0; istep<daq.n_TP_step; istep++)
    {
// Program DAC for this step
      asic.CATIA_DAC1_status[ich]=TRUE;
      asic.CATIA_DAC1_val[ich]=daq.TP_level;
      iret=update_CATIA_reg(ich,4);
      if((iret&0xFFF)!=daq.TP_level) printf("Error on DAC setting : 0x%4.4x written, 0x%4.4x read back\n",daq.TP_level,iret&0xFFF);
      printf("DAC register value read : 0x%x, %d\n",iret,iret&0xFFFF);
// Wait for Vdac to stabilize :
      if(istep==0)
        usleep(10000);
      else
        usleep(1000);

      draw=1;
      Int_t nped_before=0, nped_after=0;
      Double_t level_before=0., level_after=0.;
      for(Int_t ievt=0; ievt<daq.nevent; ievt++)
      {
        get_event(1,draw);
        draw=0;
        if(daq.error>0) printf("DAQ error on event %d : %d\n",ievt, daq.error);

        for(int is=0; is<daq.all_sample[4]; is++)
        {
          Double_t t=dt*(is+0.5);
          Double_t y=daq.fevent[4][is]*dv;
          tp_inj1[2][istep]->Fill(dt*(is+0.5),y);
          if(t<400.e-9)
          {
            level_before+=y;
            nped_before++;
          }
          if(t>900.e-9 && t<1200.e-9)
          {
            level_after+=y;
            nped_after++;
          }
        }
      }
      level_before/=nped_before;
      level_after /=nped_after;
      printf("TP %d, before : %e, after %e\n",daq.TP_level,level_before,level_after);
      daq.TP_level+=daq.TP_step;
    }
  }
*/
//===========================================================================================================
// 3.2 : Send AWG pulses to saturate G10 and look at recovery
  if((daq.use_AWG&1)==1)
  {
    fead_ctrl=  CALIB_PULSE_ENABLED   *0 |
                SELF_TRIGGER_MASK     *3 |
                SELF_TRIGGER_THRESHOLD*100 |
                SELF_TRIGGER          *1 |
                SELF_TRIGGER_LOOP     *0 |
                FIFO_MODE             *1 | // Always DAQ on trigger
                SELF_TRIGGER_MODE     *0 | // trig on level
                RESET                 *0;
    hw.getNode("FEAD_CTRL").write(fead_ctrl);
    hw.dispatch();
    Int_t n_AWG_step=34;
    Double_t level_before[34], level_after[34];
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_laser_event");
    g_assert (widget);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_nevent");
    g_assert (widget);
    gtk_entry_set_text((GtkEntry*)widget,"100");
    gtk_widget_activate((GtkWidget*)widget);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_nsample");
    g_assert (widget);
    gtk_entry_set_text((GtkEntry*)widget,"50");
    gtk_widget_activate((GtkWidget*)widget);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_level");
    g_assert (widget);
    gtk_entry_set_text((GtkEntry*)widget,"   0");
    gtk_widget_activate((GtkWidget*)widget);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_step");
    g_assert (widget);
    gtk_entry_set_text((GtkEntry*)widget," 100");
    gtk_widget_activate((GtkWidget*)widget);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_n_TP_step");
    g_assert (widget);
    gtk_entry_set_text((GtkEntry*)widget,"  34");
    gtk_widget_activate((GtkWidget*)widget);

    TProfile *tp_awg[daq.n_TP_step];
    for(int istep=0; istep<n_AWG_step; istep++)
    {
      daq.TP_level+=daq.TP_step;
      sprintf(hname,"pulse_G10_AWG_G1_%4.4d",daq.TP_level);
      tp_awg[istep]=new TProfile(hname,hname,daq.nsample*4,0.,dt*daq.nsample*4);
    }

    //asic.CATIA_ped_G10[ich]=ped_ref[0];
    sprintf(widget_name,"%1.1d_CATIA_ped_G10",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    gtk_spin_button_set_value((GtkSpinButton*)widget,(Double_t)ped_ref[0]);
    gtk_widget_activate((GtkWidget*)widget);
    //asic.CATIA_ped_G1[ich] =ped_ref[1];
    sprintf(widget_name,"%1.1d_CATIA_ped_G1",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    gtk_spin_button_set_value((GtkSpinButton*)widget,(Double_t)ped_ref[1]);
    gtk_widget_activate((GtkWidget*)widget);
    //asic.CATIA_gain[ich]   =3;
    sprintf(widget_name,"%1.1d_CATIA_gain",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    if(asic.CATIA_version[ich]<14)
      gtk_button_set_label((GtkButton*)widget,"500");
    else if(asic.CATIA_version[ich]<21)
      gtk_button_set_label((GtkButton*)widget,"500");
    else
      gtk_button_set_label((GtkButton*)widget,"470");
    gtk_button_clicked((GtkButton*)widget);
    //asic.CATIA_LPF35[ich]  =1;
    sprintf(widget_name,"%1.1d_CATIA_LPF35",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
    //iret=update_CATIA_reg(ich,3);

    daq.TP_level=0;
    //asic.CATIA_DAC1_status[ich]=FALSE;
    sprintf(widget_name,"%1.1d_CATIA_DAC1_status",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
    //asic.CATIA_DAC1_val[ich]   =0;
    sprintf(widget_name,"%1.1d_CATIA_DAC1_val",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    gtk_entry_set_text((GtkEntry*)widget,"0");
    gtk_widget_activate((GtkWidget*)widget);
    //asic.CATIA_DAC2_status[ich]=FALSE;
    sprintf(widget_name,"%1.1d_CATIA_DAC2_status",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
    //asic.CATIA_DAC2_val[ich]   =0;
    sprintf(widget_name,"%1.1d_CATIA_DAC2_val",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    gtk_entry_set_text((GtkEntry*)widget,"0");
    gtk_widget_activate((GtkWidget*)widget);
    //daq.CATIA_Vcal_out         =FALSE;
    //asic.CATIA_Rconv[ich]      =1;
    sprintf(widget_name,"%1.1d_CATIA_Rconv",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    sprintf(widget_text,"272");
    gtk_button_set_label((GtkButton*)widget,widget_text);
    gtk_widget_activate((GtkWidget*)widget); // switch to 2471 (click on 272 button) and vice-versa
    //iret=update_CATIA_reg(ich,4);
    //iret=update_CATIA_reg(ich,5);
    //iret=update_CATIA_reg(ich,6);
    while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ

    for(int istep=0; istep<n_AWG_step; istep++)
    {
      daq.TP_level+=daq.TP_step;
      printf("Please switch ON the pulse generator, set width to 500 ns and level to %d mV\n",daq.TP_level);
      system("stty raw");
      cdum=getchar();
      system("stty -raw");
      if(cdum=='q')exit(-1);

      draw=1;
      Int_t nped_before=0, nped_after=0;
      level_before[istep]=0.;
      level_after[istep]=0.;
      for(Int_t ievt=0; ievt<daq.nevent; ievt++)
      {
        get_event(1,draw);
        draw=0;
        if(daq.error>0) printf("DAQ error on event %d : %d\n",ievt, daq.error);
        for(int is=0; is<daq.all_sample[4]; is++)
        {
          Double_t t=dt*(is+0.5);
          Double_t y=daq.fevent[4][is]*dv;
          tp_awg[istep]->Fill(dt*(is+0.5),y);
          if(t<300.e-9)
          {
            level_before[istep]+=y;
            nped_before++;
          }
          if(t>500.e-9 && t<600.e-9)
          {
            level_after[istep]+=y;
            nped_after++;
          }
        }
      }
      level_before[istep]/=nped_before;
      level_after[istep] /=nped_after;
      printf("TP %d, before : %d %e, after %d %e\n",daq.TP_level, nped_before, level_before[istep], nped_after, level_after[istep]);
    }
    for(int istep=0; istep<n_AWG_step; istep++)
    {
      printf("%e, ",level_before[istep]);
    }
    printf("\n");
    for(int istep=0; istep<n_AWG_step; istep++)
    {
      printf("%e, ",level_after[istep]);
    }
    printf("\n");
    for(int istep=0; istep<n_AWG_step; istep++)
    {
      tp_awg[istep]->Write();
    }
  }

//===========================================================================================================
// 4 : Send G10 and G1 calibration pulses with R=500 and R=400
// 4.1 : Using FPGA LVCMOS pulse :
// Enabe CALIB_PULSE  :
  TGraph *tg_calib[2];
  tg_calib[0]=new TGraph();
  tg_calib[0]->SetLineColor(kRed);
  tg_calib[0]->SetMarkerColor(kRed);
  tg_calib[0]->SetMarkerSize(0.1);
  tg_calib[1]=new TGraph();
  tg_calib[1]->SetLineColor(kRed);
  tg_calib[1]->SetMarkerColor(kRed);
  tg_calib[1]->SetMarkerSize(0.1);
  if(asic.CATIA_version[ich]<14)
  {
    tg_calib[0]->SetTitle("G10_R400_LPF35_calib_pulse");
    tg_calib[0]->SetName("G10_R400_LPF35_calib_pulse");
    tg_calib[1]->SetTitle("G1_R400_LPF35_calib_pulse");
    tg_calib[1]->SetName("G1_R400_LPF35_calib_pulse");
  }
  else if(asic.CATIA_version[ich]<21)
  {
    tg_calib[0]->SetTitle("G10_R340_LPF35_calib_pulse");
    tg_calib[0]->SetName("G10_R340_LPF35_calib_pulse");
    tg_calib[1]->SetTitle("G1_R340_LPF35_calib_pulse");
    tg_calib[1]->SetName("G1_R340_LPF35_calib_pulse");
  }
  else
  {
    tg_calib[0]->SetTitle("G10_R320_LPF35_calib_pulse");
    tg_calib[0]->SetName("G10_R320_LPF35_calib_pulse");
    tg_calib[1]->SetTitle("G1_R320_LPF35_calib_pulse");
    tg_calib[1]->SetName("G1_R320_LPF35_calib_pulse");
  }
  Double_t Rcalib[2]={10000.,10000.}; // Conversion resistor on PCB to compute CATIA gain
  Double_t Rconv[2];
  Double_t CMOS_amp=1.50;
  Double_t CATIA_gain[2][3], TIA_gain[3], gain_stage;
// Enable G10 and G1 calibration pulse from FPGA :
  fead_ctrl=  CALIB_PULSE_ENABLED   *1 |
              SELF_TRIGGER_MASK     *0 |
              SELF_TRIGGER_THRESHOLD*0 |
              SELF_TRIGGER          *0 |
              SELF_TRIGGER_LOOP     *1 |
              FIFO_MODE             *1 | // Always DAQ on trigger
              RESET                 *0;
  hw.getNode("FEAD_CTRL").write(fead_ctrl);
  hw.dispatch();
  //reg=hw.getNode("FEAD_CTRL").read();
  //hw.dispatch();
  //printf("Calib_pulse_enable : 0x%8.8x -> %d\n",reg.value(),(reg.value()>>27)&1);

  //daq.trigger_type=3;
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_fpga_event");
  g_assert (widget);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_nevent");
  g_assert (widget);
  gtk_entry_set_text((GtkEntry*)widget,"100");
  gtk_widget_activate((GtkWidget*)widget);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_nsample");
  g_assert (widget);
  gtk_entry_set_text((GtkEntry*)widget,"100");
  gtk_widget_activate((GtkWidget*)widget);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_delay");
  g_assert (widget);
  gtk_entry_set_text((GtkEntry*)widget,"   0");
  gtk_widget_activate((GtkWidget*)widget);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_width");
  g_assert (widget);
  gtk_entry_set_text((GtkEntry*)widget," 100");
  gtk_widget_activate((GtkWidget*)widget);
  //command=(0<<16) | (daq.TP_width&0xffff);
  //printf("Calibration trigger with %d clocks width and %d clocks delay : %x\n",daq.TP_width,daq.TP_delay,command);
  //hw.getNode("CALIB_CTRL").write(command);

  //asic.CATIA_Rconv[ich]=0;
  sprintf(widget_name,"%1.1d_CATIA_Rconv",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  sprintf(widget_text,"272");
  gtk_button_set_label((GtkButton*)widget,widget_text);
  gtk_widget_activate((GtkWidget*)widget); // switch to 2471 (click on 272 button) and vice-versa
  //asic.CATIA_DAC1_status[ich]=FALSE;
  sprintf(widget_name,"%1.1d_CATIA_DAC1_status",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
  //asic.CATIA_DAC1_val[ich]   =0;
  sprintf(widget_name,"%1.1d_CATIA_DAC1_val",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_entry_set_text((GtkEntry*)widget,"0");
  gtk_widget_activate((GtkWidget*)widget);
  //asic.CATIA_DAC2_status[ich]=FALSE;
  sprintf(widget_name,"%1.1d_CATIA_DAC2_status",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
  //asic.CATIA_DAC2_val[ich]   =0;
  sprintf(widget_name,"%1.1d_CATIA_DAC2_val",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_entry_set_text((GtkEntry*)widget,"0");
  gtk_widget_activate((GtkWidget*)widget);
  //daq.CATIA_Vcal_out         =FALSE;
  //iret=update_CATIA_reg(ich,4);
  //iret=update_CATIA_reg(ich,5);
  //iret=update_CATIA_reg(ich,6);

  TProfile *tp_calib[2][3];
  tmax=daq.nsample*4*dt;
  tp_calib[0][0]=new TProfile("G10_R500_calib_pulse_response","G10_R500_calib_pulse_response",daq.nsample*4,0.,tmax);
  tp_calib[0][1]=new TProfile("G10_R400_calib_pulse_response","G10_R400_calib_pulse_response",daq.nsample*4,0.,tmax);
  tp_calib[0][2]=new TProfile("G10_R340_calib_pulse_response","G10_R340_calib_pulse_response",daq.nsample*4,0.,tmax);
  tp_calib[1][0]=new TProfile("G1_R500_calib_pulse_response","G1_R500_calib_pulse_response",daq.nsample*4,0.,tmax);
  tp_calib[1][1]=new TProfile("G1_R400_calib_pulse_response","G1_R400_calib_pulse_response",daq.nsample*4,0.,tmax);
  tp_calib[1][2]=new TProfile("G1_R340_calib_pulse_response","G1_R340_calib_pulse_response",daq.nsample*4,0.,tmax);
  TF1 *fcalib=new TF1("fcalib","[0]+[1]*(1.+tanh((x-[2])/[3]))/2.",0.,2.e3);
  fcalib->SetParameter(0,0.);
  fcalib->SetParameter(1,2000.);
  fcalib->SetParameter(2,0.90e-6);
  fcalib->SetParameter(3,9.e-9);
  TH1D *hcalib_t0=new TH1D("t0_calib","t0_calib",1000,0.80e-6,1.00e-6);
  TH1D *hcalib_rt=new TH1D("RT_calib","RT_calib",1000,7.e-9,12.e-9);

  sprintf(widget_name,"%1.1d_CATIA_gain",ich+1);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  if(asic.CATIA_version[ich]<21)
    gtk_button_set_label((GtkButton*)widget,"500");
  else
    gtk_button_set_label((GtkButton*)widget,"470");

  for(int iRTIA=2; iRTIA>=0; iRTIA--)
  {
    Int_t jRTIA=iRTIA;
    if(iRTIA==2)jRTIA=3;
    //asic.CATIA_gain[ich]=jRTIA;
    sprintf(widget_name,"%1.1d_CATIA_gain",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    gtk_button_clicked((GtkButton*)widget);

    //asic.CATIA_LPF35[ich]=1;
    sprintf(widget_name,"%1.1d_CATIA_LPF35",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);

    while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ
    //iret=update_CATIA_reg(ich,3);
    usleep(10000);

    draw=1;
    for(int ievt=0; ievt<daq.nevent; ievt++)
    {
      get_event(1,draw);
      draw=0;
      for(int is=0; is<daq.all_sample[4]; is++)
      {
        Double_t t=dt*(is+0.5);
        tp_calib[0][iRTIA]->Fill(t,daq.fevent[4][is]*(dv/1000.));
        tp_calib[1][iRTIA]->Fill(t,daq.fevent[5][is]*(dv/1000.));
        if(iRTIA==2)tg_calib[0]->SetPoint(is,dt*is,daq.fevent[4][is]);

      }
      if(iRTIA==2)
      {
        tg_calib[0]->Fit(fcalib,"WQ","",0.8e-6,1.00e-6);
        hcalib_t0->Fill(fcalib->GetParameter(2));
        hcalib_rt->Fill(fcalib->GetParameter(3));
      }
    }
    tp_calib[0][iRTIA]->Fit("pol0","WQ","",0.,.8e-6);
    f1=tp_calib[0][iRTIA]->GetFunction("pol0");
    mean[0]=0.;
    if(f1!=NULL)mean[0]=f1->GetParameter(0);
    tp_calib[0][iRTIA]->Fit("pol1","WQ","",1.1e-6,1.5e-6);
    f1=tp_calib[0][iRTIA]->GetFunction("pol1");
    CATIA_gain[0][iRTIA]=-999.;
    if(f1!=NULL) CATIA_gain[0][iRTIA]=(f1->Eval(0.9e-6)-mean[0])/(CMOS_amp/Rcalib[0]);
    //CATIA_gain[0][iRTIA]=(f1->Eval(0.35e-6)-mean[0])/(CMOS_amp/Rcalib[0]);
    printf("Gain %.0f, R_TIA %.0f, Total gain : %e V/A\n",val_gain[0],RTIA[iRTIA],CATIA_gain[0][iRTIA]);

    tp_calib[1][iRTIA]->Fit("pol0","WQ","",0.,.8e-6);
    f1=tp_calib[1][iRTIA]->GetFunction("pol0");
    mean[1]=0.;
    if(f1!=NULL) mean[1]=f1->GetParameter(0);
    tp_calib[1][iRTIA]->Fit("pol1","WQ","",1.1e-6,1.5e-6);
    f1=tp_calib[1][iRTIA]->GetFunction("pol1");
    CATIA_gain[1][iRTIA]=-999.;
    if(f1!=NULL)CATIA_gain[1][iRTIA]=(f1->Eval(0.9e-6)-mean[1])/(CMOS_amp/Rcalib[1]);
    //CATIA_gain[1][iRTIA]=(f1->Eval(0.35e-6)-mean[1])/(CMOS_amp/Rcalib[1]);
    printf("Gain %.0f, R_TIA %.0f, Total gain : %e V/A\n",val_gain[1],RTIA[iRTIA],CATIA_gain[1][iRTIA]);

    if(wait==1)
    {
      system("stty raw");
      cdum=getchar();
      system("stty -raw");
      if(cdum=='q')exit(-1);
    }
  }
  // (G10-R500/G1-R500 + G10-R400/G1-R400 +G10-R340/G1-R340)/3
  gain_stage=(CATIA_gain[0][0]/CATIA_gain[1][0]+CATIA_gain[0][1]/CATIA_gain[1][1]+CATIA_gain[0][2]/CATIA_gain[1][2])/3.;
  TIA_gain[0]=CATIA_gain[1][0]; // G1-R500
  TIA_gain[1]=CATIA_gain[1][1]; // G1-R400
  TIA_gain[2]=CATIA_gain[1][2]; // G1-R340
  printf("G10 with R500 : %.2f, with R400 : %.2f, with R340 : %2f, mean : %.2f\n",
         CATIA_gain[0][0]/CATIA_gain[1][0],CATIA_gain[0][1]/CATIA_gain[1][1],CATIA_gain[0][2]/CATIA_gain[1][2],gain_stage);
  printf("R500 = %.0f Ohm\n",TIA_gain[0]);
  printf("R400 = %.0f Ohm\n",TIA_gain[1]);
  printf("R340 = %.0f Ohm\n",TIA_gain[2]);
  Rconv[0]=CATIA_gain[0][1]/V_Iinj_slope[0]; // Iinj conversion resistor for G1
  Rconv[1]=CATIA_gain[1][1]/V_Iinj_slope[1]; // Iinj conversion resistor for G10
  printf("Rconv Iinj G10 (2471 Ohm) = %.4f Ohm\n",Rconv[0]);
  printf("Rconv Iinj G1  (272 Ohm)  = %.4f Ohm\n",Rconv[1]);

// 4.2 : Using AWG pulse :
  TGraph *tg_calib_AWG[2];
  tg_calib_AWG[0]=new TGraph();
  tg_calib_AWG[0]->SetLineColor(kRed);
  tg_calib_AWG[0]->SetMarkerColor(kRed);
  tg_calib_AWG[0]->SetMarkerSize(0.1);
  tg_calib_AWG[0]->SetTitle("G10_R340_LPF35_AWG_calib_pulse");
  tg_calib_AWG[0]->SetName("G10_R340_LPF35_AWG_calib_pulse");
  tg_calib_AWG[1]=new TGraph();
  tg_calib_AWG[1]->SetLineColor(kRed);
  tg_calib_AWG[1]->SetMarkerColor(kRed);
  tg_calib_AWG[1]->SetMarkerSize(0.1);
  tg_calib_AWG[1]->SetTitle("G1_R340_LPF35_AWG_calib_pulse");
  tg_calib_AWG[1]->SetName("G1_R340_LPF35_AWG_calib_pulse");
  Double_t Rcalib_AWG[2]={10000.,10000.}; // Conversion resistor on PCB to compute CATIA gain
  Double_t Rconv_AWG[2];
  Double_t CATIA_gain_AWG[2][3], TIA_gain_AWG[3], gain_stage_AWG;
  TProfile *tp_calib_AWG[2][3];
  tp_calib_AWG[0][0]=new TProfile("G10_R500_AWG_calib_pulse_response","G10_R500_AWG_calib_pulse_response",daq.nsample*4,0.,tmax);
  tp_calib_AWG[0][1]=new TProfile("G10_R400_AWG_calib_pulse_response","G10_R400_AWG_calib_pulse_response",daq.nsample*4,0.,tmax);
  tp_calib_AWG[0][2]=new TProfile("G10_R340_AWG_calib_pulse_response","G10_R340_AWG_calib_pulse_response",daq.nsample*4,0.,tmax);
  tp_calib_AWG[1][0]=new TProfile("G1_R500_AWG_calib_pulse_response","G1_R500_AWG_calib_pulse_response",daq.nsample*4,0.,tmax);
  tp_calib_AWG[1][1]=new TProfile("G1_R400_AWG_calib_pulse_response","G1_R400_AWG_calib_pulse_response",daq.nsample*4,0.,tmax);
  tp_calib_AWG[1][2]=new TProfile("G1_R340_AWG_calib_pulse_response","G1_R340_AWG_calib_pulse_response",daq.nsample*4,0.,tmax);
  TH1D *hcalib_AWG_t0=new TH1D("t0_AWG_calib","t0_AWG_calib",1000,0.80e-6,1.00e-6);
  TH1D *hcalib_AWG_rt=new TH1D("RT_AWG_calib","RT_AWG_calib",1000,7.e-9,12.e-9);

  if((daq.use_AWG&2)==2)
  {
// Enable G10 and G1 calibration pulse from FPGA :
    fead_ctrl=  CALIB_PULSE_ENABLED   *0 |
                SELF_TRIGGER_MASK     *0xf |
                SELF_TRIGGER_THRESHOLD*1000 |
                SELF_TRIGGER          *1 |
                SELF_TRIGGER_LOOP     *0 |
                FIFO_MODE             *1 | // Always DAQ on trigger
                SELF_TRIGGER_MODE     *0 | // trig on absolite level
                RESET                 *0;
    hw.getNode("FEAD_CTRL").write(fead_ctrl);
    hw.dispatch();
    //reg=hw.getNode("FEAD_CTRL").read();
    //hw.dispatch();
    //printf("Calib_pulse_enable : 0x%8.8x -> %d\n",reg.value(),(reg.value()>>27)&1);

    //daq.trigger_type=2;
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_laser_event");
    g_assert (widget);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
    //daq.nevent=100;
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_nevent");
    g_assert (widget);
    gtk_entry_set_text((GtkEntry*)widget,"100");
    gtk_widget_activate((GtkWidget*)widget);
    //daq.nsample=100;
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_nsample");
    g_assert (widget);
    gtk_entry_set_text((GtkEntry*)widget,"100");
    gtk_widget_activate((GtkWidget*)widget);

    printf("Please switch ON the pulse generator with 1.2V pulse, 945 ns width\n");
    system("stty raw");
    cdum=getchar();
    system("stty -raw");
    if(cdum=='q')exit(-1);

    fcalib->SetParameter(0,0.);
    fcalib->SetParameter(1,2000.);
    fcalib->SetParameter(2,0.35e-6);
    fcalib->SetParameter(3,9.e-9);

    sprintf(widget_name,"%1.1d_CATIA_gain",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    if(asic.CATIA_version[ich]<21)
      gtk_button_set_label((GtkButton*)widget,"500");
    else
      gtk_button_set_label((GtkButton*)widget,"470");

    for(int iRTIA=2; iRTIA>=0; iRTIA--)
    {
      Int_t jRTIA=iRTIA;
      if(iRTIA==2)jRTIA=3;
      //asic.CATIA_gain[ich] =jRTIA;
      sprintf(widget_name,"%1.1d_CATIA_gain",ich+1);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_button_clicked((GtkButton*)widget);

      //asic.CATIA_LPF35[ich]=1;
      sprintf(widget_name,"%1.1d_CATIA_LPF35",ich+1);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);

      while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ
      //iret=update_CATIA_reg(ich,3);
      usleep(10000);

      draw=1;
      for(int ievt=0; ievt<daq.nevent; ievt++)
      {
        get_event(1,draw);
        draw=0;
        for(int is=0; is<daq.all_sample[4]; is++)
        {
          Double_t t=dt*(is+0.5);
          tp_calib_AWG[0][iRTIA]->Fill(t,daq.fevent[4][is]*(dv/1000.));
          tp_calib_AWG[1][iRTIA]->Fill(t,daq.fevent[5][is]*(dv/1000.));
          if(iRTIA==2)tg_calib_AWG[0]->SetPoint(is,dt*is,daq.fevent[4][is]);
        }
        if(iRTIA==2)
        {
          //tg_calib_AWG[0]->Fit(fcalib,"WQ","",0.8e-6,1.00e-6);
          tg_calib_AWG[0]->Fit(fcalib,"WQ","",0.3e-6,0.40e-6);
          hcalib_AWG_t0->Fill(fcalib->GetParameter(2));
          hcalib_AWG_rt->Fill(fcalib->GetParameter(3));
        }
      }
      tp_calib_AWG[0][iRTIA]->Fit("pol0","WQ","",0.,.8e-6);
      //tp_calib_AWG[0][iRTIA]->Fit("pol0","WQ","",0.,.2e-6);
      f1=tp_calib_AWG[0][iRTIA]->GetFunction("pol0");
      mean[0]=0.;
      if(f1!=NULL)mean[0]=f1->GetParameter(0);
      tp_calib_AWG[0][iRTIA]->Fit("pol1","WQ","",1.1e-6,1.8e-6);
      //tp_calib_AWG[0][iRTIA]->Fit("pol1","WQ","",0.6e-6,1.1e-6);
      f1=tp_calib_AWG[0][iRTIA]->GetFunction("pol1");
      CATIA_gain_AWG[0][iRTIA]=-999.;
      if(f1!=NULL)CATIA_gain_AWG[0][iRTIA]=(f1->Eval(0.9e-6)-mean[0])/(1.2/Rcalib_AWG[0]);
      //CATIA_gain_AWG[0][iRTIA]=(f1->Eval(0.35e-6)-mean[0])/(1.2/Rcalib_AWG[0]);
      printf("AWG : Gain %.0f, R_TIA %.0f, Total gain : %e V/A\n",val_gain[0],RTIA[iRTIA],CATIA_gain_AWG[0][iRTIA]);

      tp_calib_AWG[1][iRTIA]->Fit("pol0","WQ","",0.,.8e-6);
      //tp_calib_AWG[1][iRTIA]->Fit("pol0","WQ","",0.,.2e-6);
      f1=tp_calib_AWG[1][iRTIA]->GetFunction("pol0");
      mean[1]=0.;
      if(f1!=NULL)mean[1]=f1->GetParameter(0);
      tp_calib_AWG[1][iRTIA]->Fit("pol1","WQ","",1.1e-6,1.8e-6);
      //tp_calib_AWG[1][iRTIA]->Fit("pol1","WQ","",0.6e-6,1.1e-6);
      f1=tp_calib_AWG[1][iRTIA]->GetFunction("pol1");
      CATIA_gain_AWG[1][iRTIA]=-999.;
      if(f1!=NULL)CATIA_gain_AWG[1][iRTIA]=(f1->Eval(0.9e-6)-mean[1])/(1.2/Rcalib_AWG[1]);
      //CATIA_gain_AWG[1][iRTIA]=(f1->Eval(0.35e-6)-mean[1])/(1.2/Rcalib_AWG[1]);
      printf("AWG : Gain %.0f, R_TIA %.0f, Total gain : %e V/A\n",val_gain[1],RTIA[iRTIA],CATIA_gain_AWG[1][iRTIA]);

    }
    gain_stage_AWG=(CATIA_gain_AWG[0][0]/CATIA_gain_AWG[1][0]+CATIA_gain_AWG[0][1]/CATIA_gain_AWG[1][1]+CATIA_gain_AWG[0][2]/CATIA_gain_AWG[1][2])/3.;
    TIA_gain_AWG[0]=CATIA_gain_AWG[1][0]; // G1-R500
    TIA_gain_AWG[1]=CATIA_gain_AWG[1][1]; // G1-R400
    TIA_gain_AWG[2]=CATIA_gain_AWG[1][2]; // G1-R340
    printf("G10 with R500 : %.2f, with R400 : %.2f, with R340 : %2f, mean : %.2f\n",
           CATIA_gain_AWG[0][0]/CATIA_gain_AWG[1][0],CATIA_gain_AWG[0][1]/CATIA_gain_AWG[1][1],CATIA_gain_AWG[0][2]/CATIA_gain_AWG[1][2],gain_stage);
    printf("R500 = %.0f Ohm\n",TIA_gain_AWG[0]);
    printf("R400 = %.0f Ohm\n",TIA_gain_AWG[1]);
    printf("R340 = %.0f Ohm\n",TIA_gain_AWG[2]);
    Rconv_AWG[0]=CATIA_gain_AWG[0][1]/V_Iinj_slope[0]; // Iinj conversion resistor for G1
    Rconv_AWG[1]=CATIA_gain_AWG[1][1]/V_Iinj_slope[1]; // Iinj conversion resistor for G10
    printf("Rconv Iinj G10 (2471 Ohm) = %.0f Ohm\n",Rconv_AWG[0]);
    printf("Rconv Iinj G1  (272 Ohm)  = %.0f Ohm\n",Rconv_AWG[1]);
  }

  hcalib_t0->Fit("gaus","LQ","");
  f1=hcalib_t0->GetFunction("gaus");

  Double_t calib_pos=0.;
  Double_t calib_jitter=0.;
  if(f1!=NULL) calib_pos=f1->GetParameter(1);
  if(f1!=NULL) calib_jitter=f1->GetParameter(2);
  hcalib_rt->Fit("gaus","LQ","");
  f1=hcalib_rt->GetFunction("gaus");
  Double_t RT=0.;
  Double_t eRT=0.;
  if(f1!=NULL) RT=f1->GetParameter(1);
  if(f1!=NULL) eRT=f1->GetParameter(2);
  printf("Pulse jitter : %.1f ps, rise time %.2f ns\n",calib_jitter*1.e12,RT*1.e9);

// Switch on FE-adapter LEDs
  command = LED_ON*1+GENE_WTE*0+GENE_TRIGGER*0;
  hw.getNode("FW_VER").write(command);
  hw.dispatch();

// Switch OFF and Initialize current monitors
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_power_ON");
  g_assert (widget);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);

// Save ADC calibration (full list of registers :
  sprintf(output_file,"data/CATIA_calib/CATIA_%5.5d_ADC_calib.dat",CATIA_number);
  fcal=fopen(output_file,"w+");
  for(int iADC=0; iADC<2; iADC++)
  {
    Int_t num=asic.DTU_number[daq.channel_number[0]];
    device_number=daq.I2C_LiTEDTU_type*1000+(num<<daq.I2C_shift_dev_number)+iADC;      // ADC sub-addresses
    for(int ireg=0; ireg<N_ADC_REG; ireg++)
    {
      ADC_reg_val[iADC][ireg]=I2C_RW(hw, device_number, ireg, 0, 0, 2, 0);
    }
  }
  for(int ireg=0; ireg<N_ADC_REG; ireg++)
  {
    //printf("Register %d : 0x%2.2x 0x%2.2x\n",ireg,ADC_reg_val[0][ireg]&0xFF, ADC_reg_val[1][ireg]&0xFF);
    fprintf(fcal,"%d %d\n",ADC_reg_val[0][ireg]&0xff, ADC_reg_val[1][ireg]&0xff);
  }
  fclose(fcal);

  pmod[0]->~TProfile();
  pmod[1]->~TProfile();
// Now compute Rconv for current injection should be around 272 and 2472 Ohms
  tg_Vref->Write();
  tg_Vref->~TGraph();
  tg_Vdac[0]->Write();
  tg_Vdac[0]->~TGraph();
  tg_Vdac[1]->Write();
  tg_Vdac[1]->~TGraph();
  tg_Vdac_resi[0]->Write();
  tg_Vdac_resi[0]->~TGraph();
  tg_Vdac_resi[1]->Write();
  tg_Vdac_resi[1]->~TGraph();
  tg_ped_DAC[0]->Write();
  tg_ped_DAC[1]->Write();
  tg_pulse[0]->Write();
  tg_pulse[0]->~TGraph();
  tg_pulse[1]->Write();
  tg_pulse[1]->~TGraph();
  hmod[0][0]->Write();
  hmod[0][0]->~TH1D();
  hmod[0][1]->Write();
  hmod[0][1]->~TH1D();
  hmod[0][2]->Write();
  hmod[0][2]->~TH1D();
  hmod[0][3]->Write();
  hmod[0][3]->~TH1D();
  hmod[0][4]->Write();
  hmod[0][4]->~TH1D();
  hmod[0][5]->Write();
  hmod[0][5]->~TH1D();
  hmod[1][0]->Write();
  hmod[1][0]->~TH1D();
  hmod[1][1]->Write();
  hmod[1][1]->~TH1D();
  hmod[1][2]->Write();
  hmod[1][2]->~TH1D();
  hmod[1][3]->Write();
  hmod[1][3]->~TH1D();
  hmod[1][4]->Write();
  hmod[1][4]->~TH1D();
  hmod[1][5]->Write();
  hmod[1][5]->~TH1D();
  tp_inj_cal->Write();
  for(int iinj=0; iinj<daq.n_TP_step; iinj++)
  {
    tp_inj1[0][iinj]->Write();
    tp_inj1[0][iinj]->~TProfile();
    tp_inj1[1][iinj]->Write();
    tp_inj1[1][iinj]->~TProfile();
    hp_inj1[0][iinj]->Write();
    hp_inj1[0][iinj]->~TH1D();
    hp_inj1[1][iinj]->Write();
    hp_inj1[1][iinj]->~TH1D();
    //tp_inj1[2][iinj]->Write();
  }
  //for(int iinj=0; iinj<n_AWG_step; iinj++)
  //{
  //  tp_awg[iinj]->Write();
  //}
  tg_inj1[0]->Write();
  tg_inj1[0]->~TGraph();
  tg_inj1[1]->Write();
  tg_inj1[1]->~TGraph();
  tg_inj2[0]->Write();
  tg_inj2[0]->~TGraph();
  tg_inj2[1]->Write();
  tg_inj2[1]->~TGraph();
  tg_inj3[0]->Write();
  tg_inj3[0]->~TGraph();
  tg_inj3[1]->Write();
  tg_inj3[1]->~TGraph();
  tp_calib[0][0]->Write();
  tp_calib[0][0]->~TProfile();
  tp_calib[0][1]->Write();
  tp_calib[0][1]->~TProfile();
  tp_calib[0][2]->Write();
  tp_calib[0][2]->~TProfile();
  tp_calib[1][0]->Write();
  tp_calib[1][0]->~TProfile();
  tp_calib[1][1]->Write();
  tp_calib[1][1]->~TProfile();
  tp_calib[1][2]->Write();
  tp_calib[1][2]->~TProfile();
  tg_calib[0]->Write();
  tg_calib[0]->~TGraph();
  tg_calib[1]->Write();
  tg_calib[1]->~TGraph();
  hcalib_t0->Write();
  hcalib_t0->~TH1D();
  hcalib_rt->Write();
  hcalib_rt->~TH1D();
  fd->Close();

  sprintf(output_file,"data/CATIA_calib/CATIA_%5.5d_calib.txt",CATIA_number);
  FILE *fout=fopen(output_file,"w+");
  fprintf(fout,"# Temperature FPGA CATIA_X1 CATIA_X5\n");
  fprintf(fout,"%e %e %e\n",Temp[0],Temp[1],Temp[2]);
  fprintf(fout,"# Vref DAC values\n");
  fprintf(fout,"%d\n",n_Vref);
  for(Int_t i=0; i<n_Vref; i++) fprintf(fout,"%e ",Vref[i]);
  fprintf(fout,"\n");
  fprintf(fout,"# Iinj DAC values : in, DAC1, DAC2\n");
  fprintf(fout,"%d\n",n_Vdac);
  for(Int_t i=0; i<n_Vdac; i++) fprintf(fout,"%e ",DAC_val[i]);
  fprintf(fout,"\n");
  for(Int_t i=0; i<n_Vdac; i++) fprintf(fout,"%e ",Vdac[0][i]);
  fprintf(fout,"\n");
  for(Int_t i=0; i<n_Vdac; i++) fprintf(fout,"%e ",Vdac[1][i]);
  fprintf(fout,"\n");
  fprintf(fout,"# Iinj DAC : slope DAC1 DAC2, offset DAC1 DAC2\n");
  fprintf(fout,"%e %e %e %e\n",Vdac_slope[0],Vdac_slope[1],Vdac_offset[0],Vdac_offset[1]);
  fprintf(fout,"# PED DAC values : G1, G10\n");
  fprintf(fout,"%d\n",n_PED_dac);
  for(Int_t i=0; i<n_PED_dac; i++)
  {
    Double_t x,y;
    tg_ped_DAC[0]->GetPoint(i,x,y);
    fprintf(fout,"%e ",y);
  }
  tg_ped_DAC[0]->~TGraph();
  fprintf(fout,"\n");
  for(Int_t i=0; i<n_PED_dac; i++)
  {
    Double_t x,y;
    tg_ped_DAC[1]->GetPoint(i,x,y);
    fprintf(fout,"%e ",y);
  }
  tg_ped_DAC[1]->~TGraph();
  fprintf(fout,"\n");
  fprintf(fout,"# PED DAC parameters : G1_ped[0], G1_ped_slope, G10_ped[0], G10_ped_slope\n");
  fprintf(fout,"%e %e %e %e\n",ped_DAC_par[1][0],ped_DAC_par[1][1],ped_DAC_par[0][0],ped_DAC_par[0][1]);
  fprintf(fout,"# Noise : G1-R500-LPF50, G1-R500-LPF35 G1-R400-LPF50 G1-R400-LPF35 G1-R340-LPF50 G1-R340-LPF35 G10-R500-LPF50, G10-R500-LPF35 G10-R400-LPF50 G10-R400-LPF35 G10-R340-LPF50 G10-R340-LPF35\n");
  fprintf(fout,"%e %e %e %e %e %e %e %e %e %e %e %e\n",G1_noise[0][0], G1_noise[0][1], G1_noise[1][0], G1_noise[1][1], G1_noise[2][0], G1_noise[2][1],
                                                       G10_noise[0][0],G10_noise[0][1],G10_noise[1][0],G10_noise[1][1], G10_noise[2][0], G10_noise[2][1]);
  fprintf(fout,"# INL : G1-min G1-max G10-min G10-max\n");
  fprintf(fout,"%e %e %e %e\n",INL_min[1],INL_max[1],INL_min[0],INL_max[0]);
  fprintf(fout,"# CATIA gains : G1-R500 G1-R400 G1-R340 G10-R500 G10-R400 G10-R340 (LPF35)\n");
  fprintf(fout,"%e %e %e %e %e %e\n",CATIA_gain[1][0],CATIA_gain[1][1],CATIA_gain[1][2],CATIA_gain[0][0],CATIA_gain[0][1],CATIA_gain[0][2]);
  fprintf(fout,"# CATIA parameters : G10 R500 R400 R340 Rconv_G1 Rconv_G10\n");
  fprintf(fout,"%e %e %e %e %e %e\n",gain_stage,TIA_gain[0],TIA_gain[1],TIA_gain[2],Rconv[1],Rconv[0]);
  fprintf(fout,"# Timing with calib pulses : pos, jitter, rising_time, rising_time RMS\n");
  fprintf(fout,"%e %e %e %e\n",calib_pos,calib_jitter,RT,eRT);
  fclose(fout);

  //daq.debug_I2C=debug_I2C_saved;
  daq.LVRB_autoscan=LVRB_autoscan_saved;
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_debug_draw");
  g_assert (widget);
  if(debug_draw_saved==1)
    gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  else
    gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
  //daq.trigger_type=trigger_type_saved;
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_ped_event");
  g_assert (widget);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  //daq.sw_DAQ_delay=hw_DAQ_delay_saved;
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_hw_DAQ_delay");
  g_assert (widget);
  sprintf(widget_text,"%d",hw_DAQ_delay_saved);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate((GtkWidget*)widget);
  //daq.hw_DAQ_delay=sw_DAQ_delay_saved;
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_sw_DAQ_delay");
  g_assert (widget);
  sprintf(widget_text,"%d",sw_DAQ_delay_saved);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate((GtkWidget*)widget);
  //daq.nevent=nevent_saved;
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_nevent");
  g_assert (widget);
  sprintf(widget_text,"%d",nevent_saved);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate((GtkWidget*)widget);
  //daq.nsample=nsample_saved;
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_nsample");
  g_assert (widget);
  sprintf(widget_text,"%d",nsample_saved);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate((GtkWidget*)widget);
  //daq.TP_level=TP_level_saved;
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_level");
  g_assert (widget);
  sprintf(widget_text,"%d",TP_level_saved);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate((GtkWidget*)widget);
  //daq.TP_step=TP_step_saved;
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_step");
  g_assert (widget);
  sprintf(widget_text,"%d",TP_step_saved);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate((GtkWidget*)widget);
  //daq.n_TP_step=n_TP_step_saved;
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_n_TP_step");
  g_assert (widget);
  sprintf(widget_text,"%d",n_TP_step_saved);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate((GtkWidget*)widget);
  //daq.TP_delay=TP_delay_saved;
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_delay");
  g_assert (widget);
  sprintf(widget_text,"%d",TP_delay_saved);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate((GtkWidget*)widget);
  //daq.TP_width=TP_width_saved;
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_width");
  g_assert (widget);
  sprintf(widget_text,"%d",TP_width_saved);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate((GtkWidget*)widget);
}
