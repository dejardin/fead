#include <gtk/gtk.h>
#include "gdaq_VFE.h"

void end_of_run()
{
  gettimeofday(&tv,NULL);
  printf("%ld.%6.6ld : End of study\n",tv.tv_sec,tv.tv_usec);

  if(daq.fd!=NULL)
  {
    if(daq.tdata!=NULL)daq.tdata->Write();
    if(daq.c1!=NULL)daq.c1->Write();
    if(daq.c2!=NULL)daq.c2->Write();
    if(daq.c3!=NULL)daq.c3->Write();
    daq.fd->Close();
  }
  gtk_main_quit();
  exit(1);
}

void intHandler(int)
{
  SEU.test_running = FALSE;
  printf("Ctrl-C detected !\n");
  gettimeofday(&tv,NULL);
  printf("%ld.%6.6ld : Interupt detected\n",tv.tv_sec,tv.tv_usec);
  if(daq.fout_SEU != NULL)
  {
    fprintf(daq.fout_SEU,"%ld.%6.6ld : Interupt detected\n",tv.tv_sec,tv.tv_usec);
  }
  end_of_run();
}

void abortHandler(int)
{
  SEU.test_running = FALSE;
  printf("Abort !\n");
  gettimeofday(&tv,NULL);
  printf("%ld.%6.6ld : Run aborted\n",tv.tv_sec,tv.tv_usec);
  if(daq.fout_SEU != NULL)
  {
    fprintf(daq.fout_SEU,"%ld.%6.6ld : Run aborted\n",tv.tv_sec,tv.tv_usec);
  }
  end_of_run();
}

int main(int argc, char *argv [])
{
  signal(SIGINT, intHandler);
  signal(SIGABRT, abortHandler);

  gtk_data.main_window = NULL;
  GtkWidget *widget=NULL;
  daq.fout_SEU=NULL;

  daq.tdata=NULL;
  daq.c1=NULL;
  daq.c2=NULL;
  daq.c3=NULL;
  daq.fd=NULL;
  gchar *filename = NULL;
  GError *error = NULL;
  sprintf(daq.comment,"");

  Int_t loc_argc=1;
  char *loc_argv[10];
  for(int i=0; i<10; i++)loc_argv[i]=(char *)malloc(132*sizeof(char));
  sprintf(loc_argv[0],"test");
  TApplication *Root_App=new TApplication("test", &loc_argc, loc_argv);
  gStyle->SetPadGridX(kTRUE);
  gStyle->SetPadGridY(kTRUE);

  /* Initialisation de la librairie Gtk. */
  gtk_init(&argc, &argv);

  /* Ouverture du fichier Glade de la fenêtre principale */
  gtk_data.builder = gtk_builder_new();

  /* Création du chemin complet pour accéder au fichier test.glade. */
  /* g_build_filename(); construit le chemin complet en fonction du système */
  /* d'exploitation. ( / pour Linux et \ pour Windows) */
  //filename =  g_build_filename ("test.glade", NULL);
  filename =  g_build_filename ("xml/prod_test.glade", NULL);

  /* Chargement du fichier test.glade. */
  gtk_builder_add_from_file (gtk_data.builder, filename, &error);
  g_free (filename);
  if (error)
  {
    gint code = error->code;
    g_printerr("%s\n", error->message);
    g_error_free (error);
    return code;
  }

  /* Affectation des signaux de l'interface aux différents CallBacks. */
  gtk_builder_connect_signals (gtk_data.builder, NULL);

  /* Récupération du pointeur de la fenêtre principale */
  gtk_data.main_window = GTK_WIDGET(gtk_builder_get_object (gtk_data.builder, "Test_CATIA"));

  /* Affichage de la fenêtre principale. */
  gtk_widget_show_all(gtk_data.main_window);

  daq.daq_initialized=-1;
  daq.use_AWG=0;
// Read default values and
// Disable channels if default values are such :
  daq.selected_channel=0;
  for(Int_t ich=0; ich<5; ich++)
  {
    daq.delay_val[ich]=0;
    daq.bitslip_val[ich]=0;
    daq.byteslip_val[ich]=0;
    asic.DTU_clk_out[ich]=FALSE;
    asic.DTU_VCO_rail_mode[ich]=TRUE;
    asic.DTU_bias_ctrl_override[ich]=FALSE;
    asic.DTU_eLink[ich]=TRUE;
  }
  daq.calib_mux_enabled=0;
  daq.n_active_channel=0;
  daq.eLink_active=0;
  daq.MEM_mode=FALSE;
  daq.MEM_pos=-1;
  const char *content;
  char fname[132],fname2[110];
  read_conf_from_xml_file("xml/config_CATIA_test.xml");
  strncpy(fname2,daq.xml_filename,110);
  sprintf(fname,"gdaq_VFE.exe : %s",fname2);
  gtk_window_set_title((GtkWindow*)gtk_data.main_window,fname);
  printf("Start DAQ with %d active channels: ",daq.n_active_channel);

  daq.trigger_type=-1;
  daq.self_trigger=0;
  daq.self_trigger_mode=0;
  daq.self_trigger_threshold=100;
  daq.self_trigger_mask=0x1F;

  //widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_ReSync_data");
  //g_assert (widget);
  //gtk_widget_set_sensitive(widget,FALSE);

  gtk_main();

  return 0;
}
