#define EXTERN extern
#include "gdaq_VFE.h"
void dump_ADC_registers(void)
{
  Int_t ADC_reg_val[2][N_ADC_REG];
  uhal::HwInterface hw=devices.front();
  UInt_t device_number, ped_mux=0;
  if(daq.CATIA_Vcal_out)ped_mux=1;
  UInt_t VFE_control= DELAY_AUTO_TUNE*daq.delay_auto_tune | LVRB_AUTOSCAN*daq.LVRB_autoscan |
			   PED_MUX*ped_mux | eLINK_ACTIVE*daq.eLink_active |
			   DTU_TEST_MODE*daq.DTU_test_mode | DTU_MEM_MODE*daq.MEM_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();
  FILE *fcal=fopen("last_VFE_calib_reg.dat","w+");
  for(int ich=0; ich<5; ich++)
  {
	for(int iADC=0; iADC<2; iADC++)
	{
      Int_t num=asic.DTU_number[ich];
	  device_number=daq.I2C_LiTEDTU_type*1000+(num<<daq.I2C_shift_dev_number)+iADC;
	  for(int ireg=0; ireg<N_ADC_REG; ireg++)
	  {
		ADC_reg_val[iADC][ireg]=I2C_RW(hw, device_number, ireg, 0, 0, 2, daq.debug_I2C);
	  }
	}
	for(int ireg=0; ireg<N_ADC_REG; ireg++)
	{
	  printf("Register %d : 0x%2.2x 0x%2.2x\n",ireg,ADC_reg_val[0][ireg]&0xff, ADC_reg_val[1][ireg]&0xff);
	  fprintf(fcal,"%d %d\n",ADC_reg_val[0][ireg]&0xff, ADC_reg_val[1][ireg]&0xff);
	}
  }
  fclose(fcal);
  VFE_control= DELAY_AUTO_TUNE*daq.delay_auto_tune | LVRB_AUTOSCAN*daq.LVRB_autoscan |
			   PED_MUX*ped_mux | eLINK_ACTIVE*daq.eLink_active |
			   DTU_TEST_MODE*daq.DTU_test_mode | DTU_MEM_MODE*daq.MEM_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();
}
void load_ADC_registers(void)
{
  Int_t ADC_reg_val[2][N_ADC_REG];
  uhal::HwInterface hw=devices.front();
  UInt_t device_number, iret, ped_mux=0;
  if(daq.CATIA_Vcal_out)ped_mux=1;
  UInt_t VFE_control= DELAY_AUTO_TUNE*daq.delay_auto_tune | LVRB_AUTOSCAN*daq.LVRB_autoscan |
			   PED_MUX*ped_mux | eLINK_ACTIVE*daq.eLink_active |
			   DTU_TEST_MODE*daq.DTU_test_mode | DTU_MEM_MODE*daq.MEM_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();
  FILE *fcal=fopen("last_VFE_calib_reg.dat","r");
  for(int ich=0; ich<5; ich++)
  {
    for(int ireg=0; ireg<N_ADC_REG; ireg++)
    {
      fscanf(fcal,"%d %d\n",&ADC_reg_val[0][ireg], &ADC_reg_val[1][ireg]);
	  printf("Register %d : 0x%2.2x 0x%2.2x\n",ireg,ADC_reg_val[0][ireg]&0xff, ADC_reg_val[1][ireg]&0xff);
    }
    for(int iADC=0; iADC<2; iADC++)
    {
      Int_t num=asic.DTU_number[ich];
      int device_number=daq.I2C_LiTEDTU_type*1000+(num<<daq.I2C_shift_dev_number)+iADC;
      for(int ireg=0; ireg<N_ADC_REG; ireg++)
      {
        iret=I2C_RW(hw, device_number, ireg, ADC_reg_val[iADC][ireg], 0, 1, daq.debug_I2C);
      }
    }
  }

  fclose(fcal);
  VFE_control= DELAY_AUTO_TUNE*daq.delay_auto_tune | LVRB_AUTOSCAN*daq.LVRB_autoscan |
			   PED_MUX*ped_mux | eLINK_ACTIVE*daq.eLink_active |
			   DTU_TEST_MODE*daq.DTU_test_mode | DTU_MEM_MODE*daq.MEM_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();
}
