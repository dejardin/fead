#include <gtk/gtk.h>
#include "gdaq_VFE.h"

void end_of_run()
{
  gettimeofday(&tv,NULL);
  printf("%ld.%6.6ld : End of study\n",tv.tv_sec,tv.tv_usec);

  if(daq.fout_SEU!=NULL)
  {
    printf("%ld.%6.6ld : SEU summary per CATIA :\n",tv.tv_sec,tv.tv_usec);
    fprintf(daq.fout_SEU,"%ld.%6.6ld : End of study\n",tv.tv_sec,tv.tv_usec);
    fprintf(daq.fout_SEU,"%ld.%6.6ld : SEU summary per CATIA :\n",tv.tv_sec,tv.tv_usec);
    for(int i=0; i<=daq.n_active_channel; i++)
    {
      Int_t ich=daq.channel_number[i];
      printf("%ld.%6.6ld : Catia %d : SEU : %3d\n",tv.tv_sec,tv.tv_usec, ich+1,SEU.n_SEU[ich]);
      fprintf(daq.fout_SEU,"%ld.%6.6ld : Catia %d : SEU : %3d\n",tv.tv_sec,tv.tv_usec, ich+1,SEU.n_SEU[ich]);
    }
    printf("%ld.%6.6ld : Summary of errors for each register :\n",tv.tv_sec,tv.tv_usec);
    fprintf(daq.fout_SEU,"%ld.%6.6ld : Summary of errors for each register :\n",tv.tv_sec,tv.tv_usec);
    int tot_error=0;
    for(int i=0; i<=daq.n_active_channel; i++)
    {
      Int_t ich=daq.channel_number[i];
      printf("%ld.%6.6ld : Catia %d : ",tv.tv_sec,tv.tv_usec,ich+1);
      fprintf(daq.fout_SEU,"%ld.%6.6ld : Catia %d : ",tv.tv_sec,tv.tv_usec,ich+1);
      for(int j=0; j<=SEU.n_reg; j++)
      {
        Int_t ireg=SEU.reg_number[j];
        printf("%.6d ",SEU.error[ich][ireg]);
        fprintf(daq.fout_SEU,"%.6d ",SEU.error[ich][ireg]);
        tot_error+=SEU.error[ich][ireg];
      }   
      printf("\n");
      fprintf(daq.fout_SEU,"\n");
    }
    printf("Total number of errors : %d\n",tot_error);
    fprintf(daq.fout_SEU,"%ld.%6.6ld : Total number of errors : %d\n",tv.tv_sec,tv.tv_usec,tot_error);

    printf("%ld.%6.6ld : SEL summary per power supply : ",tv.tv_sec,tv.tv_usec);
    printf("V2P5 : %d, V1P2 :%d\n",SEU.n_SEL[0],SEU.n_SEL[1]);
    fprintf(daq.fout_SEU,"%ld.%6.6ld : SEL summary per power supply : ",tv.tv_sec,tv.tv_usec);
    fprintf(daq.fout_SEU,"V2P5 : %d, V1P2 : %d\n",SEU.n_SEL[0],SEU.n_SEL[1]);

    fflush(stdout);
    fclose(daq.fout_SEU);
  }

  if(daq.fd!=NULL)
  {
    if(daq.tdata!=NULL)daq.tdata->Write();
    if(daq.c1!=NULL)daq.c1->Write();
    if(daq.c2!=NULL)daq.c2->Write();
    if(daq.c3!=NULL)daq.c3->Write();
    daq.fd->Close();
  }
  gtk_main_quit();
  exit(1);
}

void intHandler(int)
{
  SEU.test_running = FALSE;
  printf("Ctrl-C detected !\n");
  gettimeofday(&tv,NULL);
  printf("%ld.%6.6ld : Interupt detected\n",tv.tv_sec,tv.tv_usec);
  if(daq.fout_SEU != NULL)
  {
    fprintf(daq.fout_SEU,"%ld.%6.6ld : Interupt detected\n",tv.tv_sec,tv.tv_usec);
  }
  end_of_run();
}

void abortHandler(int)
{
  SEU.test_running = FALSE;
  printf("Abort !\n");
  gettimeofday(&tv,NULL);
  printf("%ld.%6.6ld : Run aborted\n",tv.tv_sec,tv.tv_usec);
  if(daq.fout_SEU != NULL)
  {
    fprintf(daq.fout_SEU,"%ld.%6.6ld : Run aborted\n",tv.tv_sec,tv.tv_usec);
  }
  end_of_run();
}

int main(int argc, char *argv [])
{
  signal(SIGINT, intHandler);
  signal(SIGABRT, abortHandler);

  gtk_data.main_window = NULL;
  GtkWidget *widget=NULL;
  daq.fout_SEU=NULL;

  daq.tdata=NULL;
  daq.c1=NULL;
  daq.c2=NULL;
  daq.c3=NULL;
  daq.fd=NULL;
  //daq.fd=new TFile("data/gdaq/gdaq_data_tmp.root","recreate");
  gchar *filename = NULL;
  GError *error = NULL;
  sprintf(daq.comment,"");

  Int_t loc_argc=1;
  char *loc_argv[10];
  for(int i=0; i<10; i++)loc_argv[i]=(char *)malloc(132*sizeof(char));
  sprintf(loc_argv[0],"test");
  TApplication *Root_App=new TApplication("test", &loc_argc, loc_argv);
  gStyle->SetPadGridX(kTRUE);
  gStyle->SetPadGridY(kTRUE);

  /* Initialisation de la librairie Gtk. */
  gtk_disable_setlocale ();
  gtk_init(&argc, &argv);

  /* Ouverture du fichier Glade de la fenêtre principale */
  gtk_data.builder = gtk_builder_new();

  /* Création du chemin complet pour accéder au fichier test.glade. */
  /* g_build_filename(); construit le chemin complet en fonction du système */
  /* d'exploitation. ( / pour Linux et \ pour Windows) */
  //filename =  g_build_filename ("test.glade", NULL);
  filename =  g_build_filename ("xml/gdaq_VFE_menu.glade", NULL);

  /* Chargement du fichier test.glade. */
  gtk_builder_add_from_file (gtk_data.builder, filename, &error);
  g_free (filename);
  if (error)
  {
    gint code = error->code;
    g_printerr("%s\n", error->message);
    g_error_free (error);
    return code;
  }

  /* Affectation des signaux de l'interface aux différents CallBacks. */
  gtk_builder_connect_signals (gtk_data.builder, NULL);

  /* Récupération du pointeur de la fenêtre principale */
  gtk_data.main_window = GTK_WIDGET(gtk_builder_get_object (gtk_data.builder, "Test_CATIA"));

  /* Affichage de la fenêtre principale. */
  gtk_widget_show_all(gtk_data.main_window);

  daq.daq_initialized=-1;
  daq.use_AWG=0;
// Read default values and
// Disable channels if default values are such :
  daq.selected_channel=0;
  for(Int_t ich=0; ich<5; ich++)
  {
    daq.delay_val[ich]=0;
    daq.bitslip_val[ich]=0;
    daq.byteslip_val[ich]=0;
    asic.DTU_clk_out[ich]=FALSE;
    asic.DTU_VCO_rail_mode[ich]=TRUE;
    asic.DTU_bias_ctrl_override[ich]=FALSE;
    asic.DTU_eLink[ich]=TRUE;
  }
  daq.calib_mux_enabled=0;
  daq.n_active_channel=0;
  daq.eLink_active=0;
  daq.MEM_mode=FALSE;
  daq.MEM_pos=-1;
  const char *content;
  FILE *fd_xml=fopen(".last_xml_file","r");
  char fname[132],fname2[110];
  if(fd_xml != NULL)
  {
    fscanf(fd_xml,"%s",daq.xml_filename);
    read_conf_from_xml_file(daq.xml_filename);
    strncpy(fname2,daq.xml_filename,110);
    sprintf(fname,"gdaq_VFE.exe : %s",fname2);
    gtk_window_set_title((GtkWindow*)gtk_data.main_window,fname);
  }
  else
  {
    sprintf(daq.xml_filename,"xml/test.xml");
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_nsample");
    g_assert (widget);
    gtk_widget_activate(widget);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_hw_DAQ_delay");
    g_assert (widget);
    gtk_widget_activate(widget);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_sw_DAQ_delay");
    g_assert (widget);
    gtk_widget_activate(widget);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_delay");
    g_assert (widget);
    gtk_widget_activate(widget);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_width");
    g_assert (widget);
    gtk_widget_activate(widget);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_level");
    g_assert (widget);
    gtk_widget_activate(widget);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_step");
    g_assert (widget);
    gtk_widget_activate(widget);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_n_TP_step");
    g_assert (widget);
    gtk_widget_activate(widget);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_nevent");
    g_assert (widget);
    gtk_widget_activate(widget);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_DTU_override_Vc_bit");
    g_assert (widget);
    Int_t loc_Vc_bit=0;
    if(gtk_toggle_button_get_active((GtkToggleButton*)widget))loc_Vc_bit=1;
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_is_VICEPP");
    g_assert (widget);
    daq.is_VICEPP=gtk_toggle_button_get_active((GtkToggleButton*) widget);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_DTU_test_mode");
    g_assert (widget);
    daq.DTU_test_mode=0;
    if(gtk_toggle_button_get_active((GtkToggleButton*) widget)) daq.DTU_test_mode=1;
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_LVRB_autoscan");
    g_assert (widget);
    daq.LVRB_autoscan=0;
    if(gtk_toggle_button_get_active((GtkToggleButton*) widget)) daq.LVRB_autoscan=1;
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_Rshunt_V2P5");
    g_assert (widget);
    content=gtk_entry_get_text((GtkEntry*)widget);
    sscanf(content,"%lf",&daq.Rshunt_V2P5);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_Rshunt_V1P2");
    g_assert (widget);
    content=gtk_entry_get_text((GtkEntry*)widget);
    sscanf(content,"%lf",&daq.Rshunt_V1P2);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_CATIA_scan_Vref");
    g_assert (widget);
    content=gtk_button_get_label((GtkButton*)widget);
    if(strcmp(content,"Vref (free)")==0)
      daq.CATIA_scan_Vref=TRUE;
    else
      daq.CATIA_scan_Vref=FALSE;

    for(int i=1; i<=5; i++)
    {
      asic.DTU_override_Vc_bit[i-1]=loc_Vc_bit;
      printf("DTU%d Vc bit : %d\n",i,asic.DTU_override_Vc_bit[i-1]);
      printf("ch %d:",i);
      char widget_name[80];
      sprintf(widget_name,"%1.1d_channel_status",i);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      bool status_saved=gtk_switch_get_state((GtkSwitch*)widget);
  // Enable channel to retreive menu data :
      //enable_channel(i,TRUE);
      printf("%d ",status_saved);

      sprintf(widget_name,"%1.1d_CATIA_version",i);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      content=gtk_entry_get_text((GtkEntry*) widget);
      sscanf(content,"%d",&asic.CATIA_version[i-1]);
      printf("%d ",asic.CATIA_version[i-1]);
      sprintf(widget_name,"%1.1d_CATIA_Vref_val",i);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      asic.CATIA_Vref_val[i-1]=rint(gtk_spin_button_get_value((GtkSpinButton*) widget));
      printf("%d ",asic.CATIA_Vref_val[i-1]);
      sprintf(widget_name,"%1.1d_CATIA_ped_G10",i);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      asic.CATIA_ped_G10[i-1]=rint(gtk_spin_button_get_value((GtkSpinButton*) widget));
      printf("%d ",asic.CATIA_ped_G10[i-1]);
      sprintf(widget_name,"%1.1d_CATIA_ped_G1",i);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      asic.CATIA_ped_G1[i-1]=rint(gtk_spin_button_get_value((GtkSpinButton*) widget));
      printf("%d ",asic.CATIA_ped_G1[i-1]);
      sprintf(widget_name,"%1.1d_CATIA_gain",i);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      content=gtk_button_get_label((GtkButton*) widget);
      printf("%s ",content);
      if(asic.CATIA_version[i-1]<14)
      {
        if(strcmp(content,"500")==0)asic.CATIA_gain[i-1]=0;
        else                        asic.CATIA_gain[i-1]=1;
      }
      else if(asic.CATIA_version[i-1]<=20)
      {
        if(strcmp(content,"500")==0)     asic.CATIA_gain[i-1]=0;
        else if(strcmp(content,"400")==0)asic.CATIA_gain[i-1]=1;
        else                             asic.CATIA_gain[i-1]=2;
      }
      else
      {
        if(strcmp(content,"470")==0)     asic.CATIA_gain[i-1]=0;
        else if(strcmp(content,"380")==0)asic.CATIA_gain[i-1]=1;
        else                             asic.CATIA_gain[i-1]=2;
      }
      printf("(%d) ",asic.CATIA_gain[i-1]);
      sprintf(widget_name,"%1.1d_CATIA_Rconv",i);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      content=gtk_button_get_label((GtkButton*) widget);
      asic.CATIA_Rconv[i-1]=1;
      if(strcmp(content,"2471")==0)asic.CATIA_Rconv[i-1]=0;
      sprintf(widget_name,"%1.1d_CATIA_LPF35",i);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      if(gtk_toggle_button_get_active((GtkToggleButton*) widget))asic.CATIA_LPF35[i-1]=1;
      else                                                       asic.CATIA_LPF35[i-1]=0;
      sprintf(widget_name,"%1.1d_CATIA_DAC1_status",i);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      asic.CATIA_DAC1_status[i-1]=gtk_toggle_button_get_active((GtkToggleButton*) widget);
      printf("%d ",asic.CATIA_DAC1_status[i-1]);
      sprintf(widget_name,"%1.1d_CATIA_DAC1_val",i);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      content=gtk_entry_get_text((GtkEntry*) widget);
      sscanf(content,"%d",&asic.CATIA_DAC1_val[i-1]);
      printf("%d ",asic.CATIA_DAC1_val[i-1]);
      sprintf(widget_name,"%1.1d_CATIA_DAC2_status",i);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      asic.CATIA_DAC2_status[i-1]=gtk_toggle_button_get_active((GtkToggleButton*) widget);
      printf("%d ",asic.CATIA_DAC2_status[i-1]);
      sprintf(widget_name,"%1.1d_CATIA_DAC2_val",i);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      content=gtk_entry_get_text((GtkEntry*) widget);
      sscanf(content,"%d",&asic.CATIA_DAC1_val[i-1]);
      printf("%d ",asic.CATIA_DAC2_val[i-1]);
      sprintf(widget_name,"%1.1d_DTU_version",i);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      content=gtk_entry_get_text((GtkEntry*) widget);
      sscanf(content,"%d",&asic.DTU_version[i-1]);
      printf("%d ",asic.DTU_version[i-1]);
      sprintf(widget_name,"%1.1d_DTU_BL_G10",i);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      content=gtk_entry_get_text((GtkEntry*) widget);
      sscanf(content,"%d",&asic.DTU_BL_G10[i-1]);
      printf("%d ",asic.DTU_BL_G10[i-1]);
      sprintf(widget_name,"%1.1d_DTU_BL_G1",i);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      content=gtk_entry_get_text((GtkEntry*) widget);
      sscanf(content,"%d",&asic.DTU_BL_G1[i-1]);
      printf("%d ",asic.DTU_BL_G1[i-1]);
      sprintf(widget_name,"%1.1d_DTU_PLL",i);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      content=gtk_entry_get_text((GtkEntry*) widget);
      sscanf(content,"%x",&asic.DTU_PLL_conf[i-1]);
      printf("%d ",asic.DTU_PLL_conf[i-1]);
      asic.DTU_PLL_conf1[i-1]=asic.DTU_PLL_conf[i-1]&0x7;
      asic.DTU_PLL_conf2[i-1]=asic.DTU_PLL_conf[i-1]>>3;
      sprintf(widget_name,"%1.1d_DTU_force_G1",i);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      asic.DTU_force_G1[i-1]=gtk_toggle_button_get_active((GtkToggleButton*) widget);
      printf("%d ",asic.DTU_force_G1[i-1]);
      sprintf(widget_name,"%1.1d_DTU_force_PLL",i);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      asic.DTU_force_PLL[i-1]=gtk_toggle_button_get_active((GtkToggleButton*) widget);
      printf("%d ",asic.DTU_force_PLL[i-1]);
      printf("\n");

      sprintf(widget_name,"%1.1d_channel_status",i);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      enable_channel(i,status_saved);
      if(status_saved)
      {
        daq.channel_number[daq.n_active_channel++]=i-1;
        daq.eLink_active|=(1<<(i-1));
      }
      asic.DTU_force_G10[i-1]=FALSE;
      asic.DTU_invert_clock[i-1]=FALSE;
    }
    printf("Start DAQ with %d active channels: ",daq.n_active_channel);
    for(int i=0; i<daq.n_active_channel; i++)
    {
      int ich=daq.channel_number[i];
      printf("%d: ",ich+1);
      printf("%d %d %d %d %d %d %d %d %d %d %d %d %d\n",
        asic.CATIA_version[ich],asic.CATIA_Vref_val[ich],asic.CATIA_ped_G10[ich],asic.CATIA_ped_G1[ich],asic.CATIA_gain[ich],
        asic.CATIA_LPF35[ich],asic.CATIA_DAC1_status[ich],asic.CATIA_DAC1_val[ich],asic.CATIA_DAC2_status[ich],asic.CATIA_DAC2_val[ich],
        asic.DTU_version[ich],asic.DTU_BL_G10[ich],asic.DTU_BL_G1[ich]);
    }

    daq.debug_DAQ=0;
    daq.debug_I2C=0;
    daq.debug_GTK=0;
    daq.debug_draw=0;
    daq.zoom_draw=0;
    daq.c1=NULL;
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_debug_DAQ");
    g_assert (widget);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_debug_I2C");
    g_assert (widget);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
//    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_debug_GTK");
//    g_assert (widget);
//    gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_debug_draw");
    g_assert (widget);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_zoom_draw");
    g_assert (widget);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_gain_correction");
    g_assert (widget);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  // in laser trigger mode, we let wait for ever, waiting for a signal
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_wait_for_ever");
    g_assert (widget);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  }
  printf("Start DAQ with %d active channels: ",daq.n_active_channel);

  daq.trigger_type=-1;
  daq.self_trigger=0;
  daq.self_trigger_mode=0;
  daq.self_trigger_threshold=100;
  daq.self_trigger_mask=0x1F;

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_trigger_mode");
  g_assert (widget);
  printf("trigger mode : %d\n",gtk_toggle_button_get_active((GtkToggleButton*)widget));
  gtk_toggle_button_set_active((GtkToggleButton*)widget,!gtk_toggle_button_get_active((GtkToggleButton*)widget));
  gtk_toggle_button_set_active((GtkToggleButton*)widget,!gtk_toggle_button_get_active((GtkToggleButton*)widget));

// Disable actions before DAQ initialization :
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_LVRB_autoscan");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_CATIA_outputs");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_ADC_calibration");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_synchronize_links");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_shift_odd_samples");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_get_event");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_take_run");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_CATIA_calib");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_SEU_test");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_reset_line_delays");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_reset_DTUTestUnit");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_dump_ADC_registers");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_load_ADC_registers");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_optimize_pedestals");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_PLL_scan");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_power_ON");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_init_VFE");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_selected_channel");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_tap_slip");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_bit_slip");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_byte_slip");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_CATIA_temp");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_ReSync_data");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);

  gtk_main();

  return 0;
}
