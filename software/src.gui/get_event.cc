#define EXTERN extern
#include "gdaq_VFE.h"

void take_run(void)
{
  uhal::HwInterface hw=devices.front();
  ValWord<uint32_t> orbit_pos,orbit_pos1,orbit_pos2,orbit_pos3;
  Double_t dv=1200./4096.;
  Int_t debug_DAQ=daq.debug_DAQ;
  daq.debug_DAQ=0;
  Int_t TP_level=daq.TP_level;
  Int_t TP_step=daq.TP_step;
  Int_t n_TP_step=daq.n_TP_step;
  Int_t loc_TP_step=daq.n_TP_step;
  if(daq.trigger_type!=1)loc_TP_step=1;
  char hname[132], fname[132], widget_name[132], widget_text[132];
  GtkWidget *widget;
  time_t rawtime=time(NULL);
  struct tm *timeinfo=localtime(&rawtime);
  if(daq.trigger_type==0)
    sprintf(fname,"data/gdaq/ped_data_%d%2.2d%2.2d_%2.2d%2.2d%2.2d.root",
            timeinfo->tm_year-100,timeinfo->tm_mon+1,timeinfo->tm_mday,timeinfo->tm_hour,timeinfo->tm_min,timeinfo->tm_sec);
  else if(daq.trigger_type==1)
    sprintf(fname,"data/gdaq/TP_data_%d_%d_%d_%d%2.2d%2.2d_%2.2d%2.2d%2.2d.root",daq.n_TP_step,daq.TP_step,daq.TP_level,
            timeinfo->tm_year-100,timeinfo->tm_mon+1,timeinfo->tm_mday,timeinfo->tm_hour,timeinfo->tm_min,timeinfo->tm_sec);
  else if(daq.trigger_type==2)
    sprintf(fname,"data/gdaq/laser_data_%d%2.2d%2.2d_%2.2d%2.2d%2.2d.root",
            timeinfo->tm_year-100,timeinfo->tm_mon+1,timeinfo->tm_mday,timeinfo->tm_hour,timeinfo->tm_min,timeinfo->tm_sec);
  else if(daq.trigger_type==3)
    sprintf(fname,"data/gdaq/cal_data_%d%2.2d%2.2d_%2.2d%2.2d%2.2d.root",
            timeinfo->tm_year-100,timeinfo->tm_mon+1,timeinfo->tm_mday,timeinfo->tm_hour,timeinfo->tm_min,timeinfo->tm_sec);
  else if(daq.trigger_type==5)
    sprintf(fname,"data/gdaq/ReSync_data_0x%8.8x_%d%2.2d%2.2d_%2.2d%2.2d%2.2d.root",daq.ReSync_data,
            timeinfo->tm_year-100,timeinfo->tm_mon+1,timeinfo->tm_mday,timeinfo->tm_hour,timeinfo->tm_min,timeinfo->tm_sec);
  
  printf("Start run recording for %d events with %d samples and trigger type %d\n",daq.nevent, daq.nsample, daq.trigger_type);
  if(daq.trigger_type==1) printf("TP width %d, level %d, step %d, nstep %d\n",daq.TP_width, daq.TP_level, daq.TP_step, daq.n_TP_step);
  printf("Run file name : %s\n",fname);

  if(daq.fd != NULL)
  {
    daq.fd->Close();
    daq.fd->~TFile();
  }
  daq.fd=new TFile(fname,"recreate");

  Int_t nch=5;
  if(daq.DTU_test_mode==1 && daq.eLink_active==0x0f)nch=6;

  if(daq.tdata==NULL)
  {
    daq.tdata=new TTree("tdata","tdata");
    daq.tdata->Branch("timestamp",&daq.timestamp,"timestamp/l");
    for(int ich=0; ich<nch; ich++)
    {
      char bname[80], btype[80];
      sprintf(bname,"ch%d",ich);
      sprintf(btype,"ch%d[%d]/S",ich,daq.nsample*5);
      daq.tdata->Branch(bname,daq.event[ich],btype);
    }
  }

  for(Int_t ich=0; ich<nch; ich++)
  {
    if(daq.hmean[ich]!=NULL)daq.hmean[ich]->~TH1D();
    if(daq.hrms[ich]!=NULL)daq.hrms[ich]->~TH1D();
    if(daq.hdensity[ich]!=NULL)daq.hdensity[ich]->~TH1D();
    sprintf(hname,"mean_ch%d",ich);
    daq.hmean[ich]=new TH1D(hname,hname,100,150.,250.);
    sprintf(hname,"rms_ch%d",ich);
    daq.hrms[ich]=new TH1D(hname,hname,200,0.,2.);
    sprintf(hname,"code_density_ch%d",ich);
    daq.hdensity[ich]=new TH1D(hname,hname,4096,-0.5,4095.5);
  }

  if(daq.trigger_type==1)
  {
    for(Int_t i=0; i<daq.n_active_channel; i++)
    {
      Int_t ich=daq.channel_number[i];
      sprintf(widget_name,"%d_CATIA_DAC1_status",ich+1);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
      sprintf(widget_name,"%d_CATIA_DAC2_status",ich+1);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
    }
  }
  for(Int_t ich=0; ich<nch; ich++)
  {
    for(Int_t istep=0; istep<loc_TP_step; istep++)
    {
      if(daq.pshape[istep][ich]!=NULL)daq.pshape[istep][ich]->~TProfile();
      sprintf(hname,"ch_%d_step_%d_%d",ich,istep,daq.TP_level+istep*daq.TP_step);
      if(nch==5 || ich>3)
        daq.pshape[istep][ich]=new TProfile(hname,hname,daq.nsample*5,0.,6.25*daq.nsample*5);
      else
        daq.pshape[istep][ich]=new TProfile(hname,hname,daq.nsample*5,0.,12.5*daq.nsample*5);
    }
  }

  for(Int_t istep=0; istep<loc_TP_step; istep++)
  {
    if(daq.trigger_type==1)
    {
      //for(Int_t i=0; i<daq.n_active_channel; i++)
      //{
      //  Int_t ich=daq.channel_number[i];
      //  sprintf(widget_name,"%d_CATIA_DAC1_val",ich+1);
      //  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      //  g_assert (widget);
      //  sprintf(widget_text,"%d",daq.TP_level);
      //  gtk_entry_set_text((GtkEntry*)widget,widget_text);
      //}
      sprintf(widget_name,"0_TP_level");
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      sprintf(widget_text,"%d",daq.TP_level);
      gtk_entry_set_text((GtkEntry*)widget,widget_text);
      gtk_widget_activate((GtkWidget*)widget);
      sprintf(widget_name,"0_n_TP_step");
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      sprintf(widget_text,"%d",istep);
      gtk_entry_set_text((GtkEntry*)widget,widget_text);
      usleep(100000);
    }
    while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ
    daq.cur_evt=0;
    Int_t draw=1;
    while(daq.cur_evt<daq.nevent)
    {
      sprintf(widget_name,"0_nevent");
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      sprintf(widget_text,"%d",daq.cur_evt);
      gtk_entry_set_text((GtkEntry*)widget,widget_text);
      while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ
// Have we asked to abort the run ?
      if(!daq.daq_running)
      {
        printf("Abort button pressed 1 !\n");
        break;
      }

      get_event(0,draw);
      daq.tdata->Fill();
      for(Int_t ich=0; ich<nch; ich++)
      {
        Double_t mean=0., rms=0.; 
        for(Int_t is=0; is<daq.all_sample[ich]; is++)
        {
          mean+=daq.fevent[ich][is];
          rms+=daq.fevent[ich][is]*daq.fevent[ich][is];
        }
        mean/=daq.all_sample[ich];
        rms/=daq.all_sample[ich];
        rms=sqrt(rms-mean*mean);
        Double_t rms_limit=1.;
        if(asic.DTU_force_G1[ich]==1)rms_limit=0.5;
        if(daq.trigger_type==1 && rms*dv<rms_limit && ich==0)
        {
          orbit_pos=hw.getNode("DEBUG1_0").read();
          orbit_pos1=hw.getNode("DEBUG1_1").read();
          orbit_pos2=hw.getNode("DEBUG1_2").read();
          orbit_pos3=hw.getNode("DEBUG1_3").read();
          hw.dispatch();
          printf("Trigger sent at %d %d %d %d\n",orbit_pos.value(),orbit_pos1.value(),orbit_pos2.value(),orbit_pos3.value());
        }
        if(daq.trigger_type!=1 || rms*dv>rms_limit || istep==0)
        {
          //printf("ch %d, rms = %.3f\n",ich,rms);
          for(Int_t is=0; is<daq.all_sample[ich]; is++)
          {
            daq.hdensity[ich]->Fill((double)daq.event[ich][is]);
            //if(nch==5 || ich>3)
            //  daq.pshape[istep][ich]->Fill(6.25*is,(daq.fevent[ich][is]-daq.fevent[ich][0])*dv);
            //else
            //  daq.pshape[istep][ich]->Fill(12.5*is,(daq.fevent[ich][is]-daq.fevent[ich][0])*dv);
            if(nch==5 || ich>3)
              daq.pshape[istep][ich]->Fill(6.25*is,daq.fevent[ich][is]);
            else
              daq.pshape[istep][ich]->Fill(12.5*is,daq.fevent[ich][is]);
          }
          daq.hmean[ich]->Fill(mean*dv);
          daq.hrms[ich]->Fill(rms*dv);
          if(draw==1)printf("%d : %.1f %.3f mV, ",ich,mean*dv,rms*dv);
        }
      }
      if(draw==1)printf("\n");
      draw=0;
    }
    if(!daq.daq_running)
    {
      printf("Abort button pressed 2 !\n");
      break;
    }
    if(daq.trigger_type==1) daq.TP_level+=daq.TP_step;
  }

  if(daq.daq_running)
  {
    daq.tdata->Write();
    daq.tdata->~TTree();
    daq.tdata=NULL;
    for(Int_t ich=0; ich<nch; ich++)
    {
      for(Int_t istep=0; istep<loc_TP_step; istep++)
      {
        daq.pshape[istep][ich]->Write();
        daq.pshape[istep][ich]->~TProfile();
      }
      daq.hmean[ich]->Write();
      daq.hmean[ich]->~TH1D();
      daq.hrms[ich]->Write();
      daq.hrms[ich]->~TH1D();
      daq.hdensity[ich]->Write();
      daq.hdensity[ich]->~TH1D();
    }
    daq.fd->Close();
    daq.fd->~TFile();
    daq.fd=NULL;
  }
  else
  {
    daq.tdata->~TTree();
    daq.tdata=NULL;
    for(Int_t ich=0; ich<nch; ich++)
    {
      for(Int_t istep=0; istep<loc_TP_step; istep++)
      {
        daq.pshape[istep][ich]->~TProfile();
      }
      daq.hmean[ich]->~TH1D();
      daq.hrms[ich]->~TH1D();
      daq.hdensity[ich]->~TH1D();
    }
    daq.fd->Close();
    daq.fd->~TFile();
    daq.fd=NULL;
  }

  daq.debug_DAQ=debug_DAQ;
  daq.TP_level=TP_level;
  daq.n_TP_step=n_TP_step;
  sprintf(widget_name,"0_TP_level");
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  sprintf(widget_text,"%d",daq.TP_level);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate(widget);
  sprintf(widget_name,"0_n_TP_step");
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  sprintf(widget_text,"%d",daq.n_TP_step);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate(widget);
  sprintf(widget_name,"0_nevent");
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  sprintf(widget_text,"%d",daq.nevent);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate(widget);
  sprintf(widget_name,"0_take_run");
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  if(daq.daq_running)gtk_button_clicked((GtkButton*)widget);
  printf("End of run !\n");
}

void get_single_event(void)
{
  Double_t dv=1200./4096.;
  Int_t nch=5;
  if(daq.DTU_test_mode==1 && daq.eLink_active==0x0f)nch=6;

  daq.cur_evt=0;
  get_event(1,1);
  for(Int_t ich=0; ich<nch; ich++)
  {
    Double_t mean=0., rms=0.; 
    for(Int_t is=0; is<daq.all_sample[ich]; is++)
    {
      mean+=daq.fevent[ich][is];
      rms+=daq.fevent[ich][is]*daq.fevent[ich][is];
    }
    mean/=daq.all_sample[ich];
    rms/=daq.all_sample[ich];
    rms=sqrt(rms-mean*mean);
    printf("%d : %.1f %.3f mV, ",ich,mean*dv,rms*dv);
  }
  printf("\n");
  get_CRC_errors(0);
  get_n_orbit_samples();
}

void get_event(Int_t debug_mode, Int_t draw)
{
  static FILE *fd=NULL;
  static int trigger_type_old=-1;
  int n_word;
  int n_transfer_max=463;
  int n_transfer;
  int n_last;
  int BC0_pos[5]={0};
  daq.error      = 0;
  int command    = 0;
  Int_t loc_ch[6]={1,2,3,4,5,6};
  Double_t dt=6.25;
  uhal::HwInterface hw=devices.front();
  TF1 *f1=new TF1("f1","[0]+[1]*(1.+tanh((x-[2])/[3]))/2.",0.,1.e7);

/*
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    Int_t num=asic.DTU_number[ich];
    UInt_t device_number=daq.I2C_LiTEDTU_type*1000+(num<<daq.I2C_shift_dev_number)+2;      // DTU sub-address
    printf("Device number : %d 0x%x\n",device_number,device_number);

    UInt_t iret=I2C_RW(hw, device_number, 0, 0, 0, 2, daq.debug_I2C);
    printf("ch %d, DTU test mode : %d, Reg0 content : 0x%2.2x\n",ich, daq.DTU_test_mode, iret&0xff);
  }
*/

  //if(trigger_type_old!=daq.trigger_type)ievt=0;
  if(daq.DTU_test_mode==1 && daq.eLink_active==0x0f)
  {
    loc_ch[0]=1;
    loc_ch[1]=2;
    loc_ch[2]=4;
    loc_ch[3]=5;
    loc_ch[4]=3;
    loc_ch[5]=6;
  }
// In debug mode, we reinit DAQ buffer at each event :
  if(debug_mode!=0 || daq.cur_evt==0)
  {
	command = ((daq.nsample+1)<<16)+CAPTURE_STOP;
	hw.getNode("CAP_CTRL").write(command);
	hw.getNode("CAP_ADDRESS").write(0);
	command = ((daq.nsample+1)<<16)+CAPTURE_START;
	hw.getNode("CAP_CTRL").write(command);
	hw.dispatch();
	daq.old_read_address=-1;
  }

  ValWord<uint32_t> address,free_mem,orbit_pos,orbit_pos1,orbit_pos2,orbit_pos3;
  address = hw.getNode("CAP_ADDRESS").read();
  free_mem = hw.getNode("CAP_FREE").read();
  hw.dispatch();
  if(daq.debug_DAQ>0)printf("Starting with address : %8.8x, free memory %8.8x, trigger_type : %d\n",address.value(),free_mem.value(),daq.trigger_type);

  ValVector< uint32_t > block;

// For pedestal trigger, send a soft trigger to FEAD/VICEPP
// For TP, send a trigger pulse to CATIA through FEAD/VICEPP if DTU version <2.0
// For DTU version >= 2.0, send a RESYNC command to LiTE-DTU which will send trigger pulse to CATIA
  if(daq.trigger_type !=2 )
  {
    if(daq.trigger_type == 0)
      command = TP_MODE*0+LED_ON*1+GENE_WTE*0+GENE_TP*0+GENE_TRIGGER*1;
    else if(daq.trigger_type == 1)
      command = TP_MODE*0+LED_ON*1+GENE_WTE*0+GENE_TP*1+GENE_TRIGGER*0;
    else if(daq.trigger_type == 3)
      command = GENE_G1_PULSE*0 | GENE_G10_PULSE*1 | TP_MODE*0 | LED_ON*0 | GENE_WTE*0 | GENE_TP*0 | GENE_TRIGGER*0;
    else if(daq.trigger_type == 4)
      command = GENE_G1_PULSE*1 | GENE_G10_PULSE*0 | TP_MODE*0 | LED_ON*0 | GENE_WTE*0 | GENE_TP*0 | GENE_TRIGGER*0;
    else if(daq.trigger_type == 5)
      command = GENE_RESYNC*1+TP_MODE*0+LED_ON*1+GENE_WTE*0+GENE_TP*0+GENE_TRIGGER*0;
    //else if(daq.trigger_type == 1 && asic.DTU_version[2]<20)
    //  command = TP_MODE*0+LED_ON*1+GENE_WTE*0+GENE_TP*1+GENE_TRIGGER*0;
    //if(daq.trigger_type == 1 && asic.DTU_version[2]>=20)
    //{
    //  while(TRUE)
    //  {
    //    send_ReSync_command(LiTEDTU_CATIA_TP);
    //  }
    //  usleep(1000);
    //}

    if(daq.debug_DAQ>0)printf("Send trigger with command : 0x%8.8x ",command);
    //printf("Send trigger with command : 0x%8.8x ",command);
// Read base address and send trigger
    hw.getNode("FW_VER").write(command);
    orbit_pos=hw.getNode("DEBUG1_0").read();
    orbit_pos1=hw.getNode("DEBUG1_1").read();
    orbit_pos2=hw.getNode("DEBUG1_2").read();
    orbit_pos3=hw.getNode("DEBUG1_3").read();
    hw.dispatch();
    if(daq.debug_DAQ>0)printf("at orbit position %d %d %d %d\n",orbit_pos.value(),orbit_pos1.value(),orbit_pos2.value(),orbit_pos3.value());
    //printf("at orbit position %d %d %d %d\n",orbit_pos.value(),orbit_pos.value()/4,orbit_pos1.value(),orbit_pos1.value()/4);
// Read new address and wait for DAQ completion
    int nretry=0, new_write_address=-1, delta_address=-1;
    do
    {  
      free_mem = hw.getNode("CAP_FREE").read();
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      new_write_address=address.value()>>16;
      nretry++;
      delta_address=new_write_address-daq.old_read_address;
      if(delta_address<0)delta_address+=NSAMPLE_MAX;
      if(daq.debug_DAQ>0) printf("ongoing R/W addresses    : old %d, new %d delta %d\n", daq.old_read_address, new_write_address,delta_address);
    }
    while(delta_address < daq.nsample+1 && nretry<100);
    if(nretry==100)
    {
      printf("Stop waiting for sample capture after %d retries\n",nretry);
      printf("R/W addresses    : old %8.8x, new %8.8x add 0x%8.8x\n", daq.old_read_address, new_write_address, address.value());
      daq.error=1;
    }
  }
  else
  {
    int nretry=0;
    command = GENE_AWG*1+TP_MODE*0+LED_ON*1+GENE_WTE*0+GENE_TP*0+GENE_TRIGGER*0;
    hw.getNode("FW_VER").write(command);
// Wait for external trigger to fill memory :
    do
    {
      free_mem = hw.getNode("CAP_FREE").read();
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      if(daq.debug_DAQ>0)printf("address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
      usleep(100);
      //if(daq.wait_for_ever)
      //{
      //  usleep(1000);
      //  printf(".");
      //  fflush(stdout);
      //}
      nretry++;
    }
    while((free_mem.value()==NSAMPLE_MAX-1) && (nretry<100 || daq.wait_for_ever));
    //if(daq.wait_for_ever)printf("\n");
    if(nretry>=100 && !daq.wait_for_ever)
    {
      printf("Stop waiting for external trigger after %d retries\n",nretry);
      printf("R/W addresses    : add 0x%8.8x\n",address.value());
      daq.error=1;
    }
  }

  if(daq.debug_DAQ>0)printf("Event kept with address : %8.8x, free memory %d, trigger_type : %d\n",address.value(),free_mem.value(),daq.trigger_type);
// Keep reading address for next event
  daq.old_read_address=address.value()>>16;
  if(daq.old_read_address==NSAMPLE_MAX-1)daq.old_read_address=-1;

  daq.mem.clear();

// Read event samples from FPGA
  n_word=(daq.nsample+1)*6; // 6*32 bits words per sample to get the 5 channels data
  n_transfer=n_word/(MAX_PAYLOAD/4); // Max ethernet packet = 1536 bytes, max user payload = 1500 bytes
  n_last=n_word-n_transfer*(MAX_PAYLOAD/4);
  if(daq.debug_DAQ)printf("Will read %d words in %d transfer of %d + %d in last transfer\n",n_word,n_transfer,MAX_PAYLOAD/4,n_last);
  if(n_transfer > n_transfer_max)
  {
    printf("Event size too big ! Please reduce number of samples per frame.\n");
    printf("Max frame size : %d\n",NSAMPLE_MAX);
    printf("Error : ask for too many samples. Number of transfer %d greater than allowed max value %d\n",n_transfer, n_transfer_max);
    daq.error=1;
  }
  for(int itrans=0; itrans<n_transfer; itrans++)
  {
    block = hw.getNode ("CAP_DATA").readBlock(MAX_PAYLOAD/4);
    hw.dispatch();
    for(int is=0; is<MAX_PAYLOAD/4; is++) daq.mem.push_back(block[is]);
  }
  block = hw.getNode ("CAP_DATA").readBlock(n_last);
  address = hw.getNode("CAP_ADDRESS").read();
  free_mem = hw.getNode("CAP_FREE").read();
  hw.dispatch();
  if(daq.debug_DAQ>0)printf("After reading address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
  for(int is=0; is<n_last; is++)daq.mem.push_back(block[is]);
  daq.mem.valid(true);

// First sample should have bits 159 downto 64 at 1 and timestamp in bits 63 downto 0
  if((daq.mem[4]>>16)!=0xffff || daq.mem[5]!=0xffffffff)
  {
    printf("First samples not headers : %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",daq.mem[5], daq.mem[4], daq.mem[3], daq.mem[2], daq.mem[1], daq.mem[0]);
    daq.error=1;
  }
  daq.timestamp=daq.mem[2];
  
  daq.timestamp=(daq.timestamp<<32)+daq.mem[1];
  
  if(daq.debug_DAQ>0)
  {
    Int_t i0=0;
    printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+0],daq.mem[i0+1],daq.mem[i0+2],daq.mem[i0+3],daq.mem[i0+4],daq.mem[i0+5]);
    i0+=6;
    printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+0],daq.mem[i0+1],daq.mem[i0+2],daq.mem[i0+3],daq.mem[i0+4],daq.mem[i0+5]);
    i0+=6;
    printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+0],daq.mem[i0+1],daq.mem[i0+2],daq.mem[i0+3],daq.mem[i0+4],daq.mem[i0+5]);
    i0+=6;
    printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+0],daq.mem[i0+1],daq.mem[i0+2],daq.mem[i0+3],daq.mem[i0+4],daq.mem[i0+5]);
    i0+=6;
    printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+0],daq.mem[i0+1],daq.mem[i0+2],daq.mem[i0+3],daq.mem[i0+4],daq.mem[i0+5]);
    i0+=6;
    printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+0],daq.mem[i0+1],daq.mem[i0+2],daq.mem[i0+3],daq.mem[i0+4],daq.mem[i0+5]);
    printf("timestamp : %8.8x %8.8x %ld\n",daq.mem[2],daq.mem[1],daq.timestamp);
    printf("clk position : %d, orbit numner : %u\n",daq.mem[3]&0xFFFF,((daq.mem[4]&0xFFFF)<<16) + (daq.mem[3]>>16));
  }
  for(int ich=0; ich<6; ich++) daq.all_sample[ich]=0;
  for(int isample=0; isample<NSAMPLE_MAX*5; isample++)
  {
    daq.event[0][isample]=-1;
    daq.event[1][isample]=-1;
    daq.event[2][isample]=-1;
    daq.event[3][isample]=-1;
    daq.event[4][isample]=-1;
    daq.event[5][isample]=-1;
    if(daq.DTU_test_mode==1)
    {
      daq.gain[0][isample]=asic.DTU_force_G1[2];
      daq.gain[1][isample]=asic.DTU_force_G1[2];
      daq.gain[2][isample]=asic.DTU_force_G1[2];
      daq.gain[3][isample]=asic.DTU_force_G1[2];
      daq.gain[4][isample]=asic.DTU_force_G1[2];
      daq.gain[5][isample]=asic.DTU_force_G1[2];
    }
    else
    {
      daq.gain[0][isample]=asic.DTU_force_G1[0];
      daq.gain[1][isample]=asic.DTU_force_G1[1];
      daq.gain[2][isample]=asic.DTU_force_G1[2];
      daq.gain[3][isample]=asic.DTU_force_G1[3];
      daq.gain[4][isample]=asic.DTU_force_G1[4];
      daq.gain[5][isample]=0;
    }
  }
  Int_t nsample_ped[6]={0};
  Double_t loc_ped_G10[6]={0.};

  if(fd==NULL && daq.dump_data>0)
  {
    fd=fopen("data/dump.txt","w+");
    printf("Opening file for sample dumping\n");
  }
  //if(daq.dump_data>0 && daq.cur_evt>0) fclose(fd);

  for(int isample=0; isample<daq.nsample; isample++)
  {
    Int_t j=(isample+1)*6;
    unsigned int loc_mem[5];
    for(int ich=0; ich<5; ich++)
    {
      loc_mem[ich]=daq.mem[j+1+ich];
    }
    if(fd!=NULL && isample<daq.dump_data)
    {
      fprintf(fd,"%8.8x %8.8x %8.8x %8.8x %8.8x\n",loc_mem[0],loc_mem[1],loc_mem[2],loc_mem[3],loc_mem[4]);
    }

// Data in test mode :
    if(daq.DTU_test_mode==1)
    {
      dt=12.5;
      daq.event[0][daq.all_sample[0]]  =(loc_mem[0]>>0 )&0xfff;
      daq.event[0][daq.all_sample[0]+1]=(loc_mem[0]>>16)&0xfff;
      daq.event[1][daq.all_sample[1]]  =(loc_mem[1]>>0 )&0xfff;
      daq.event[1][daq.all_sample[1]+1]=(loc_mem[1]>>16)&0xfff;
      daq.event[2][daq.all_sample[2]]  =(loc_mem[2]>>0 )&0xfff;
      daq.event[2][daq.all_sample[2]+1]=(loc_mem[2]>>16)&0xfff;
      //daq.event[2][daq.all_sample[2]]  =(loc_mem[3]>>0 )&0xfff;
      //daq.event[2][daq.all_sample[2]+1]=(loc_mem[3]>>16)&0xfff;
      daq.event[3][daq.all_sample[3]]  =(loc_mem[3]>>0 )&0xfff;
      daq.event[3][daq.all_sample[3]+1]=(loc_mem[3]>>16)&0xfff;
      daq.event[4][daq.all_sample[4]]  =(loc_mem[4]>>0 )&0xfff;
      daq.event[4][daq.all_sample[4]+1]=(loc_mem[4]>>16)&0xfff;

      daq.fevent[0][daq.all_sample[0]+0]=(double)daq.event[0][daq.all_sample[0]];
      daq.fevent[0][daq.all_sample[0]+1]=(double)daq.event[0][daq.all_sample[0]+1];
      daq.fevent[1][daq.all_sample[1]+0]=(double)daq.event[1][daq.all_sample[1]];
      daq.fevent[1][daq.all_sample[1]+1]=(double)daq.event[1][daq.all_sample[1]+1];
      daq.fevent[2][daq.all_sample[2]+0]=(double)daq.event[2][daq.all_sample[2]];
      daq.fevent[2][daq.all_sample[2]+1]=(double)daq.event[2][daq.all_sample[2]+1];
      daq.fevent[3][daq.all_sample[3]+0]=(double)daq.event[3][daq.all_sample[3]];
      daq.fevent[3][daq.all_sample[3]+1]=(double)daq.event[3][daq.all_sample[3]+1];
      daq.fevent[4][daq.all_sample[4]+0]=(double)daq.event[4][daq.all_sample[4]];
      daq.fevent[4][daq.all_sample[4]+1]=(double)daq.event[4][daq.all_sample[4]+1];
      if (daq.eLink_active==0x0F)
      {
        if(daq.odd_sample_shift>=0 && daq.all_sample[1]>2*daq.odd_sample_shift)
        {
          daq.event[4][daq.all_sample[4]+0]  =  daq.event[1][daq.all_sample[1]-2*daq.odd_sample_shift];
          daq.event[4][daq.all_sample[4]+1]  =  daq.event[0][daq.all_sample[0]];
          daq.event[4][daq.all_sample[4]+2]  =  daq.event[1][daq.all_sample[1]+1-2*daq.odd_sample_shift];
          daq.event[4][daq.all_sample[4]+3]  =  daq.event[0][daq.all_sample[0]+1];
          daq.fevent[4][daq.all_sample[4]+0] = daq.fevent[1][daq.all_sample[1]-2*daq.odd_sample_shift];
          daq.fevent[4][daq.all_sample[4]+1] = daq.fevent[0][daq.all_sample[0]];
          daq.fevent[4][daq.all_sample[4]+2] = daq.fevent[1][daq.all_sample[1]+1-2*daq.odd_sample_shift];
          daq.fevent[4][daq.all_sample[4]+3] = daq.fevent[0][daq.all_sample[0]+1];
        }
        else if(daq.odd_sample_shift<0 && daq.all_sample[0]>-2*daq.odd_sample_shift)
        {
          daq.event[4][daq.all_sample[4]+0]  =  daq.event[1][daq.all_sample[1]];
          daq.event[4][daq.all_sample[4]+1]  =  daq.event[0][daq.all_sample[0]+2*daq.odd_sample_shift];
          daq.event[4][daq.all_sample[4]+2]  =  daq.event[1][daq.all_sample[1]+1];
          daq.event[4][daq.all_sample[4]+3]  =  daq.event[0][daq.all_sample[0]+1+2*daq.odd_sample_shift];
          daq.fevent[4][daq.all_sample[4]+0] = daq.fevent[1][daq.all_sample[1]];
          daq.fevent[4][daq.all_sample[4]+1] = daq.fevent[0][daq.all_sample[0]+2*daq.odd_sample_shift];
          daq.fevent[4][daq.all_sample[4]+2] = daq.fevent[1][daq.all_sample[1]+1];
          daq.fevent[4][daq.all_sample[4]+3] = daq.fevent[0][daq.all_sample[0]+1+2*daq.odd_sample_shift];
        }
        else
        {
          daq.event[4][daq.all_sample[4]+0]  =  daq.event[1][daq.all_sample[1]];
          daq.event[4][daq.all_sample[4]+1]  =  daq.event[0][daq.all_sample[0]];
          daq.event[4][daq.all_sample[4]+2]  =  daq.event[1][daq.all_sample[1]+1];
          daq.event[4][daq.all_sample[4]+3]  =  daq.event[0][daq.all_sample[0]+1];
          daq.fevent[4][daq.all_sample[4]+0] = daq.fevent[1][daq.all_sample[1]];
          daq.fevent[4][daq.all_sample[4]+1] = daq.fevent[0][daq.all_sample[0]];
          daq.fevent[4][daq.all_sample[4]+2] = daq.fevent[1][daq.all_sample[1]+1];
          daq.fevent[4][daq.all_sample[4]+3] = daq.fevent[0][daq.all_sample[0]+1];
        }
        daq.event[5][daq.all_sample[5]+0]  =  daq.event[3][daq.all_sample[3]];
        daq.event[5][daq.all_sample[5]+1]  =  daq.event[2][daq.all_sample[2]];
        daq.event[5][daq.all_sample[5]+2]  =  daq.event[3][daq.all_sample[3]+1];
        daq.event[5][daq.all_sample[5]+3]  =  daq.event[2][daq.all_sample[2]+1];
        daq.fevent[5][daq.all_sample[5]+0] = daq.fevent[3][daq.all_sample[3]];
        daq.fevent[5][daq.all_sample[5]+1] = daq.fevent[2][daq.all_sample[2]];
        daq.fevent[5][daq.all_sample[5]+2] = daq.fevent[3][daq.all_sample[3]+1];
        daq.fevent[5][daq.all_sample[5]+3] = daq.fevent[2][daq.all_sample[2]+1];
        daq.all_sample[4]+=4;
        daq.all_sample[5]+=4;
      }
      else
      {
        daq.all_sample[4]+=2;
      }
      for(int ich=0; ich<4; ich++)daq.all_sample[ich]+=2;
    }
    else
// Data in DTU mode :
    {
      dt=6.25;
      for(int ich=0; ich<5; ich++)
      {
        if(daq.ped_G10[ich]<0. && nsample_ped[ich]==NSAMPLE_PED)
        {
          daq.ped_G10[ich]=loc_ped_G10[ich]/NSAMPLE_PED;
          printf("G10 pedestal for channel %d : %f\n",ich,daq.ped_G10[ich]);
        }
        if(daq.debug_DAQ>2)printf("Channel %d\n",ich);
        UInt_t tmp_mem=loc_mem[ich];
        Int_t type=(tmp_mem>>30);
        if(daq.debug_DAQ>2)printf("Compressed data : 0x%8.8x, type %d\n",tmp_mem,type);
        if(type==3)
        {
          Int_t sub_type=(tmp_mem>>28)&0x3;
          if(sub_type==1 && daq.debug_DAQ>1) // Frame delimiter
          {
            printf("Ch %d, Frame delimiter %d : %d samples, CRC12= 0x%x\n",ich,tmp_mem&0xff, (tmp_mem>>20)&0xff, (tmp_mem>>8)&0xfff);
          }
          //else if (sub_type==2) // idle pattern
          continue;
        }
        Int_t sample_map=5;
        if(type==2)sample_map=(tmp_mem>>24)&0x3F;
        if(type==2 && sample_map>4)
        {
          if(daq.debug_DAQ>1)printf("Ch %d, Strange sample map : 0x%x, certainly loss of sync !\n",ich,sample_map);
          sample_map=4;
	      daq.error|=(1<<(ich+8));
        }
// pedestal data
        if(type==1 || type==2)
        {
          if(daq.debug_DAQ>2)printf("Baseline data : sample map 0x%x :",sample_map);
          for(Int_t i=0; i<sample_map; i++)
          {
            daq.event[ich][daq.all_sample[ich]]=tmp_mem&0x3F;
            daq.fevent[ich][daq.all_sample[ich]]=(double)daq.event[ich][daq.all_sample[ich]];
            if(daq.debug_DAQ>2)printf(" 0x%x",daq.event[ich][daq.all_sample[ich]]);
            if(daq.ped_G10[ich]<0. && nsample_ped[ich]<NSAMPLE_PED)
            {
              loc_ped_G10[ich]+=daq.fevent[ich][daq.all_sample[ich]];
              nsample_ped[ich]++;
            }
            daq.all_sample[ich]++;
            tmp_mem>>=6;
          }
          if(daq.debug_DAQ>2)printf("\n");
        }
        else if(!daq.MEM_mode) // 160 MHz mode
  // signal data
        {
          Int_t signal_type=(tmp_mem>>26)&0xF;
          if(daq.debug_DAQ>2)printf("Signal data : type 0x%x : ",signal_type);
          if(signal_type==0xA)
          {
            daq.event[ich][daq.all_sample[ich]]=tmp_mem&0xFFF;
            if(asic.DTU_force_G1[ich]==0) daq.gain[ich][daq.all_sample[ich]]=(tmp_mem>>12)&0x1;
            daq.fevent[ich][daq.all_sample[ich]]=(double)daq.event[ich][daq.all_sample[ich]];
// Take first G1 sample as pedestal :
            if(daq.ped_G1[ich]<0. && daq.gain[ich][daq.all_sample[ich]]==1)
            {
              daq.ped_G1[ich]=daq.fevent[ich][daq.all_sample[ich]];
              printf("G1 pedestal for channel %d : %f\n",ich,daq.ped_G1[ich]);
            }
// And correct for gain if needed :
            if(daq.gain[ich][daq.all_sample[ich]]==1 && daq.corgain && daq.ped_G1[ich]>0. && daq.ped_G10[ich]>0.)
              daq.fevent[ich][daq.all_sample[ich]]=(daq.fevent[ich][daq.all_sample[ich]]-daq.ped_G1[ich])*10.1+daq.ped_G10[ich];
            if(daq.debug_DAQ>2)printf(" 0x%x",daq.event[ich][daq.all_sample[ich]]);
            daq.all_sample[ich]++;

            tmp_mem>>=13;
            daq.event[ich][daq.all_sample[ich]]=tmp_mem&0xFFF;
            if(asic.DTU_force_G1[ich]==0) daq.gain[ich][daq.all_sample[ich]]=(tmp_mem>>12)&0x1;
            daq.fevent[ich][daq.all_sample[ich]]=(double)daq.event[ich][daq.all_sample[ich]];

// And correct for gain if needed :
            if(daq.gain[ich][daq.all_sample[ich]]==1 && daq.corgain && daq.ped_G1[ich]>0. && daq.ped_G10[ich]>0.)
              daq.fevent[ich][daq.all_sample[ich]]=(daq.fevent[ich][daq.all_sample[ich]]-daq.ped_G1[ich])*10.1+daq.ped_G10[ich];
            if(daq.debug_DAQ>2)printf(" 0x%x",daq.event[ich][daq.all_sample[ich]]);
            daq.all_sample[ich]++;
          }
          else if(signal_type==0xB)
          {
            if((tmp_mem&0xFFF00000) == 0x2d500000 || // Signal data format 1
               (tmp_mem&0xFFF00000) == 0x2fc00000 )  // BC0 tagged sample
            {
              daq.event[ich][daq.all_sample[ich]]=tmp_mem&0xFFF;
              if(asic.DTU_force_G1[ich]==0) daq.gain[ich][daq.all_sample[ich]]=(tmp_mem>>12)&0x1;
              //if(daq.gain[ich][daq.all_sample[ich]]==1 && daq.event[ich][daq.all_sample[ich]]>0xfa0)daq.event[ich][daq.all_sample[ich]]=0;
              daq.fevent[ich][daq.all_sample[ich]]=(double)daq.event[ich][daq.all_sample[ich]];
  // Take first G1 sample as pedestal :
              if(daq.ped_G1[ich]<0. && daq.gain[ich][daq.all_sample[ich]]==1)
              {
                daq.ped_G1[ich]=daq.fevent[ich][daq.all_sample[ich]];
                printf("G1 pedestal for channel %d : %f\n",ich,daq.ped_G1[ich]);
              }
  // And correct for gain if needed :
              if(daq.gain[ich][daq.all_sample[ich]]==1 && daq.corgain && daq.ped_G1[ich]>0. && daq.ped_G10[ich]>0.)
                daq.fevent[ich][daq.all_sample[ich]]=(daq.fevent[ich][daq.all_sample[ich]]-daq.ped_G1[ich])*10.1+daq.ped_G10[ich];
              if(daq.debug_DAQ>3)printf(" 0x%x",daq.event[ich][daq.all_sample[ich]]);
              if((tmp_mem&0xFFF00000) == 0x2fc00000)
              {
                if(daq.debug_DAQ>0)printf("BC0 marker found for channel %d, word number %d, diff from last BC0 %d\n",
                                          ich+1,daq.all_sample[ich], daq.all_sample[ich]-BC0_pos[ich]);
                BC0_pos[ich]=daq.all_sample[ich];
                daq.event[ich][daq.all_sample[ich]]=0;
                daq.fevent[ich][daq.all_sample[ich]]=0.;
              }
              daq.all_sample[ich]++;
            }
            else if((tmp_mem&0xFFF00000) == 0x2cf00000) // Flush sample
            {
              if(daq.debug_DAQ>0)printf("Flush marker found for channel %d, orbit position : %d\n",ich+1, daq.all_sample[ich]-BC0_pos[ich]); 
            }
            else
            {
              printf("Crap sample 0x%8.8x found for channel %d, orbit position : %d\n",tmp_mem, ich+1, daq.all_sample[ich]-BC0_pos[ich]); 
            }
          }
          if(daq.debug_DAQ>2)printf("\n");
        }
        else if(daq.MEM_mode) // 80 MHz mode
        {
          dt=12.5;
          Int_t sub_type=(tmp_mem>>28)&0x3;
          if(sub_type==1 || sub_type==3) // 2 samples in word or BC0 in even sample
          {
            daq.event[ich][daq.all_sample[ich]]=tmp_mem&0xFFF;
            if(asic.DTU_force_G1[ich]==0) daq.gain[ich][daq.all_sample[ich]]=(tmp_mem>>12)&0x1;
            daq.fevent[ich][daq.all_sample[ich]]=(double)daq.event[ich][daq.all_sample[ich]];
            if(daq.ped_G10[ich]<0. && nsample_ped[ich]<NSAMPLE_PED)
            {
              loc_ped_G10[ich]+=daq.fevent[ich][daq.all_sample[ich]];
              nsample_ped[ich]++;
            }
// And correct for gain if needed :
            if(daq.gain[ich][daq.all_sample[ich]]==1 && daq.corgain && daq.ped_G10[ich]>0.)
              daq.fevent[ich][daq.all_sample[ich]]=(daq.fevent[ich][daq.all_sample[ich]]-daq.ped_G10[ich])*12.5+daq.ped_G10[ich];
            daq.all_sample[ich]++;
          }
          if(sub_type==1 || sub_type==2) // 2 samples in word or BC0 in odd sample
          {
            daq.event[ich][daq.all_sample[ich]]=(tmp_mem>>13)&0xFFF;
            if(asic.DTU_force_G1[ich]==0) daq.gain[ich][daq.all_sample[ich]]=(tmp_mem>>25)&0x1;
            daq.fevent[ich][daq.all_sample[ich]]=(double)daq.event[ich][daq.all_sample[ich]];
            if(daq.ped_G10[ich]<0. && nsample_ped[ich]<NSAMPLE_PED)
            {
              loc_ped_G10[ich]+=daq.fevent[ich][daq.all_sample[ich]];
              nsample_ped[ich]++;
            }
// And correct for gain if needed :
            if(daq.gain[ich][daq.all_sample[ich]]==1 && daq.corgain && daq.ped_G10[ich]>0.)
              daq.fevent[ich][daq.all_sample[ich]]=(daq.fevent[ich][daq.all_sample[ich]]-daq.ped_G10[ich])*12.5+daq.ped_G10[ich];
            daq.all_sample[ich]++;
          }
        }
      }
    }

    //Search for 0x3 and 0x9 in incoming data
    if(daq.debug_DAQ>0 && isample==0)
    {
      printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",daq.mem[j+0],daq.mem[j+1],daq.mem[j+2],daq.mem[j+3],daq.mem[j+4],daq.mem[j+5],daq.mem[j+6]);
      UInt_t loc_mem=daq.mem[j+1];
      printf("data : %8.8x ",loc_mem);
      for(int i=31; i>=0; i--) printf("%1.1d",(loc_mem>>i)&1);
      printf(" 0x3 and 0x9 at ");
      for(int i=31; i>=0; i--) {if((loc_mem&0xf000f000) == 0x30009000) printf(" %d",i);loc_mem=loc_mem<<1 | ((loc_mem>>31)&1);} 
      printf("\n");

      loc_mem=daq.mem[j+2];
      printf("data : %8.8x ",loc_mem);
      for(int i=31; i>=0; i--) printf("%1.1d",(loc_mem>>i)&1);
      printf(" 0x3 and 0x9 at ");
      for(int i=31; i>=0; i--) {if((loc_mem&0xf000f000) == 0x30009000) printf(" %d",i);loc_mem=loc_mem<<1 | ((loc_mem>>31)&1);} 
      printf("\n");
      
      loc_mem=daq.mem[j+3];
      printf("data : %8.8x ",loc_mem);
      for(int i=31; i>=0; i--) printf("%1.1d",(loc_mem>>i)&1);
      printf(" 0x3 and 0x9 at ");
      for(int i=31; i>=0; i--) {if((loc_mem&0xf000f000) == 0x30009000) printf(" %d",i);loc_mem=loc_mem<<1 | ((loc_mem>>31)&1);} 
      printf("\n");
      
      loc_mem=daq.mem[j+4];
      printf("data : %8.8x ",loc_mem);
      for(int i=31; i>=0; i--) printf("%1.1d",(loc_mem>>i)&1);
      printf(" 0x3 and 0x9 at ");
      for(int i=31; i>=0; i--) {if((loc_mem&0xf000f000) == 0x30009000) printf(" %d",i);loc_mem=loc_mem<<1 | ((loc_mem>>31)&1);} 
      printf("\n");

      loc_mem=daq.mem[j+5];
      printf("data : %8.8x ",loc_mem);
      for(int i=31; i>=0; i--) printf("%1.1d",(loc_mem>>i)&1);
      printf(" 0x3 and 0x9 at ");
      for(int i=31; i>=0; i--) {if((loc_mem&0xf000f000) == 0x30009000) printf(" %d",i);loc_mem=loc_mem<<1 | ((loc_mem>>31)&1);} 
      printf("\n");
    }

  }
  if(daq.debug_DAQ>2)
  {
    for(int ich=0; ich<5; ich++)
    {
      for(Int_t is=0; is<daq.all_sample[ich]; is++)
      {
        printf("%4d ",daq.event[ich][is]);
      }
      printf("\n");
    }
  }
  if(fd!=NULL)
  {
    fprintf(fd,"\n");
  }
  if(daq.debug_DAQ>0)
  {
    for(Int_t ich=0; ich<5; ich++)printf("Channel %d : %d samples\n",ich,daq.all_sample[ich]);
  }

  if(daq.debug_draw==1 && daq.c1==NULL)
  {
    daq.c1=new TCanvas("c1","c1",700,660,1200.,420.);
    daq.c1->Divide(3,2);
  }

  if((daq.trigger_type !=trigger_type_old || draw==1) && daq.debug_draw==1 )
  {
    Int_t ch_max=5;
    if(daq.DTU_test_mode==1 && daq.eLink_active==0xF)ch_max=4;
    for(int ich=0; ich<ch_max; ich++)
    {
      daq.tg[ich]->Set(0);
      daq.tg_g1[ich]->Set(0);
      Double_t ymax=0., ymin=0.;
      Int_t nsample_G1=0, nsample_G10=0;
      Double_t ped=0.;
      Int_t nsample_ped=0, pulse_present=0, npulse=0, pulse_pos[100];
      Double_t pulse_amp[100];
      for(int isample=0; isample<daq.all_sample[ich]; isample++)
      {
        if(isample<10 && daq.fevent[ich][isample]>1.){ped+=daq.fevent[ich][isample]; nsample_ped++;}
        if(isample==10)ped/=nsample_ped;
        if(isample>10 && daq.debug_ana)
        {
          if(pulse_present==0 && daq.fevent[ich][isample]>ped+50.)
          {
            pulse_present=1;
            printf("New pulse seen on channel %d starting at pos %d, %d awy from BC0\n",ich,isample,isample-BC0_pos[ich]);
            pulse_pos[npulse]=isample;
            pulse_amp[npulse]=0.;
            npulse++;
          }
          else if(pulse_present==1 && daq.fevent[ich][isample]<ped+50.)
          {
            pulse_present=0;
            //printf("End of pulse seen at pos %d\n",isample);
          }
          if(pulse_present==1 && (daq.fevent[ich][isample]-ped)>pulse_amp[npulse-1])pulse_amp[npulse-1]=daq.fevent[ich][isample]-ped;
        }
        if(daq.fevent[ich][isample]>ymax)ymax=daq.fevent[ich][isample];
        if(daq.fevent[ich][isample]<ymin)ymin=daq.fevent[ich][isample];
        if(daq.gain[ich][isample]==0)
          daq.tg[ich]->SetPoint(nsample_G10++,dt*isample,daq.fevent[ich][isample]);
        else
          daq.tg_g1[ich]->SetPoint(nsample_G1++,dt*isample,daq.fevent[ich][isample]);
      }
      for(Int_t ipulse=0; ipulse<npulse && daq.debug_ana; ipulse++)
      {
        printf("Fit start of pulse %d on channel %d : %d %f\n",ipulse,ich,pulse_pos[ipulse],pulse_amp[ipulse]);
        f1->SetParameter(0,ped);
        f1->SetParameter(1,pulse_amp[ipulse]);
        f1->SetParameter(2,(pulse_pos[ipulse]+1.)*dt);
        f1->FixParameter(3,9.);
        printf("starting parameters : %f %f %f %f\n",f1->GetParameter(0),f1->GetParameter(1),f1->GetParameter(2),f1->GetParameter(3));
        daq.tg[ich]->Fit(f1,"WQ","",(pulse_pos[ipulse]-10.)*dt,(pulse_pos[ipulse]+5.)*dt);
        printf("Pulse fit at %f ns from BC0\n",f1->GetParameter(2)-BC0_pos[ipulse]*dt);
      }
      daq.tg[ich]->SetMaximum(100.*(int(ymax/100.)+1));
      daq.tg[ich]->SetMinimum(100.*(int(ymin/100.)));
      daq.tg_g1[ich]->SetMaximum(100.*(int(ymax/100.)+1));
      daq.tg_g1[ich]->SetMinimum(100.*(int(ymin/100.)));
      if(daq.zoom_draw==1)
      {
        daq.tg[ich]->SetMaximum(70.);
        daq.tg[ich]->SetMinimum(0.);
        daq.tg_g1[ich]->SetMaximum(70.);
        daq.tg_g1[ich]->SetMinimum(0.);
      }
    }
    if(daq.DTU_test_mode==1 && daq.eLink_active==0xF)
    {
      for(int ich=4; ich<6; ich++)
      {
        daq.tg[ich]->Set(0);
        for(int isample=0; isample<daq.all_sample[ich]; isample++) daq.tg[ich]->SetPoint(isample,6.25*isample,daq.fevent[ich][isample]);
      }
    }
    for(int ich=0; ich<ch_max; ich++)
    {
      daq.c1->cd(loc_ch[ich]);
      //daq.tg[ich]->SetMaximum(100.);
      //daq.tg[ich]->SetMinimum(0.);
      Int_t first_draw=1;
      if(daq.tg[ich]->GetN()>0)
      {
        daq.tg[ich]->Draw("alp");
        gPad->SetGridx();
        gPad->SetGridy();
        daq.tg[ich]->GetXaxis()->SetTitle("time [ns]");
        daq.tg[ich]->GetXaxis()->SetTitleSize(0.05);
        daq.tg[ich]->GetXaxis()->SetTitleFont(62);
        daq.tg[ich]->GetXaxis()->SetLabelSize(0.05);
        daq.tg[ich]->GetXaxis()->SetLabelFont(62);
        daq.tg[ich]->GetYaxis()->SetTitle("amplitude [lsb]");
        daq.tg[ich]->GetYaxis()->SetTitleSize(0.05);
        daq.tg[ich]->GetYaxis()->SetTitleOffset(0.9);
        daq.tg[ich]->GetYaxis()->SetTitleFont(62);
        daq.tg[ich]->GetYaxis()->SetLabelSize(0.05);
        daq.tg[ich]->GetYaxis()->SetLabelFont(62);
        if(daq.fd!=NULL)daq.tg[ich]->Write();
        first_draw=0;
      }
      if(daq.tg_g1[ich]->GetN()>0)
      {
        if(first_draw==0)
          daq.tg_g1[ich]->Draw("lp");
        else
          daq.tg_g1[ich]->Draw("alp");
        if(daq.fd!=NULL)daq.tg_g1[ich]->Write();
      }
      daq.c1->Update();
    }
    if(daq.DTU_test_mode==1 && daq.eLink_active==0xF)
    {
      //daq.tg[4]->SetMaximum(100.);
      //daq.tg[5]->SetMinimum(0.);
      daq.c1->cd(loc_ch[4]);
      daq.tg[4]->Draw("alp");
      daq.c1->cd(loc_ch[5]);
      daq.tg[5]->Draw("alp");
      if(daq.fd!=NULL)daq.tg[4]->Write();
      if(daq.fd!=NULL)daq.tg[5]->Write();
      daq.c1->Update();
    }
  }
  //if(daq.debug_DAQ>0)
  //{
  //  system("stty raw");
  //  char cdum=getchar();
  //  system("stty -raw");
  //  if(cdum=='q')exit(-1);
  //}

  trigger_type_old=daq.trigger_type;
  daq.cur_evt++;
  return;
}
