#define EXTERN extern
#include "gdaq_VFE.h"

void optimize_pedestal(void)
{
  UInt_t command=0;
  Int_t iret=0, nsample_ped=1000;
  Int_t nsample_saved=daq.nsample;
  Int_t debug_DAQ=daq.debug_DAQ;
  Int_t debug_I2C=daq.debug_I2C;
  Int_t ped_mux=0;
  if(daq.CATIA_Vcal_out)ped_mux=1;

  daq.debug_DAQ=0;
  daq.debug_I2C=0;
  Int_t trig_type_saved=daq.trigger_type;
  ValWord<uint32_t> address;
  UInt_t VFE_control= LVRB_AUTOSCAN*daq.LVRB_autoscan | PED_MUX*0 | eLINK_ACTIVE*daq.eLink_active |
                      DTU_TEST_MODE*daq.DTU_test_mode | DTU_MEM_MODE*daq.MEM_mode | I2C_LPGBT_MODE*daq.I2C_lpGBT_mode;

  uhal::HwInterface hw=devices.front();
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();

  UInt_t reg1[5]={0};
  Int_t ped_G10[5]={0}, ped_G1[5]={0};

  Int_t max_ped=32;
  if(asic.CATIA_version[daq.channel_number[0]]>=14)max_ped=64;
  Double_t *ped_val_G10[5];
  Double_t *ped_val_G1[5];
  Int_t sat_G1[5], sat_G10[5];
  Int_t device_number;

  for(int ich=0; ich<5; ich++)
  {
    ped_val_G1[ich]=(Double_t*)malloc(max_ped*sizeof(Double_t));
    ped_val_G10[ich]=(Double_t*)malloc(max_ped*sizeof(Double_t));
    for(Int_t i=0; i<max_ped; i++)
    {
      ped_val_G1[ich][i]=0.;
      ped_val_G10[ich][i]=0.;
    }
    sat_G1[ich]=-1;
    sat_G10[ich]=-1;
  }
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    Int_t num=asic.DTU_number[ich];
    device_number=daq.I2C_LiTEDTU_type*1000+(num<<daq.I2C_shift_dev_number)+2;
    reg1[ich]=I2C_RW(hw, device_number, 1, 0, 0, 2, 0);
    reg1[ich]&=0xFF;
  }

// Set trigger type to "pedestal"
  daq.trigger_type=0;

  for(int G10=0; G10<=1; G10++)
  {
    for(int i=0; i<daq.n_active_channel; i++)
    {
      Int_t ich=daq.channel_number[i];
      Int_t num=asic.DTU_number[ich];
      device_number=daq.I2C_LiTEDTU_type*1000+(num<<daq.I2C_shift_dev_number)+2;
      if(G10==0)
      {
        iret=I2C_RW(hw, device_number, 1, (reg1[ich]&0x3F)|0xc0, 0, 1, 0);
      }
      else
      {
        iret=I2C_RW(hw, device_number, 1, (reg1[ich]&0x3F)|0x40, 0, 1, 0);
      }
    }

    for(Int_t loc_ped=0; loc_ped<max_ped; loc_ped++)
    {
      for(int i=0; i<daq.n_active_channel; i++)
      {
        Int_t ich=daq.channel_number[i];
      Int_t num=asic.CATIA_number[ich];
        device_number=daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+3;
        if(ich==4 && daq.I2C_shift_dev_number==4)   device_number = daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+0xb;
        UInt_t reg3=loc_ped<<8 | loc_ped<<3 | (asic.CATIA_gain[ich]<<2) | asic.CATIA_LPF35[ich]<<1 | 1;
        if(asic.CATIA_version[ich]>=14)
        {
          reg3=(asic.CATIA_gain[ich]<<14) | loc_ped<<8 | loc_ped<<2 | (asic.CATIA_LPF35[ich]<<1) | 1;
        }
        iret=I2C_RW(hw, device_number, 3, reg3, 1, 1, 0);
      }
      command = ((nsample_ped+1)<<16)+CAPTURE_STOP;
      hw.getNode("CAP_CTRL").write(command);
      hw.getNode("CAP_ADDRESS").write(0);
      command = ((nsample_ped+1)<<16)+CAPTURE_START;
      hw.getNode("CAP_CTRL").write(command);
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      daq.old_read_address=address.value()>>16;

      // Read 1 pedestal event
      get_event(0,0);

      for(int i=0; i<daq.n_active_channel; i++)
      {
        Int_t ich=daq.channel_number[i];
        Double_t ped=0.;
        if(daq.DTU_test_mode==1)
        {
          if(G10==1)
          {
            for(int isample=0; isample<daq.all_sample[4]; isample++)
            {
              ped+=(double)(daq.event[4][isample]&0xfff);
            }
            ped/=daq.all_sample[4];
          }
          else
          {
            for(int isample=0; isample<daq.all_sample[5]; isample++)
            {
              ped+=(double)(daq.event[5][isample]&0xfff);
            }
            ped/=daq.all_sample[5];
          }
        }
        else
        {
          for(int isample=0; isample<daq.all_sample[ich]; isample++)
          {
            ped+=(double)(daq.event[ich][isample]&0xfff);
          }
          ped/=daq.all_sample[ich];
        }
        if(G10==0)
          ped_val_G1[ich][loc_ped]=ped;
        else
          ped_val_G10[ich][loc_ped]=ped;
        if(G10==1)printf(" %.3f/%.3f ",ped_val_G1[ich][loc_ped],ped_val_G10[ich][loc_ped]);
      }
      if(G10==1)printf("\n");
    }
  }

  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    for(Int_t loc_ped=1; loc_ped<max_ped; loc_ped++)
    {
      if(fabs(ped_val_G1[ich][loc_ped]-ped_val_G1[ich][loc_ped-1])<.2 && sat_G1[ich]<0)
      {
        sat_G1[ich]=loc_ped-1;
        printf("ch %d, G1 saturation at %d : %.3f\n",ich,loc_ped-1,ped_val_G1[ich][loc_ped-1]);
      }
      if(fabs(ped_val_G10[ich][loc_ped]-ped_val_G10[ich][loc_ped-1])<.2 && sat_G10[ich]<0)
      {
        sat_G10[ich]=loc_ped-1;
        printf("ch %d, G10 saturation at %d : %.3f\n",ich,loc_ped-1,ped_val_G10[ich][loc_ped-1]);
      }
    }
  }


  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    if(sat_G1[ich]<0)
    {
      sat_G1[ich]=max_ped-1;
      ped_G1[ich]=sat_G1[ich];
    }
    else
      ped_G1[ich]=sat_G1[ich]-1;
    if(sat_G10[ich]<0)
    {
      sat_G10[ich]=max_ped-1;
      ped_G10[ich]=sat_G10[ich];
    }
    else
      ped_G10[ich]=sat_G10[ich]-1;

    for(Int_t loc_ped=sat_G1[ich]; loc_ped>=0; loc_ped--)
    {
      if(ped_val_G1[ich][loc_ped]<30.)
      {
        ped_G1[ich]=loc_ped;
      }
    }
    if(ped_G1[ich]==max_ped)ped_G1[ich]--;
    for(Int_t loc_ped=sat_G10[ich]; loc_ped>=0; loc_ped--)
    {
      if(ped_val_G10[ich][loc_ped]<35.)
      {
        ped_G10[ich]=loc_ped;
      }
    }
  }
  printf("Final pedestal settings :\n");
  char widget_name[80];
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    Int_t num=asic.CATIA_number[ich];
    printf("ich %d : G10=0x%2x, G1=0x%2x\n",ich,ped_G10[ich],ped_G1[ich]);
    //UInt_t device_number=daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+3;
    //if(ich==4 && daq.I2C_shift_dev_number==4)   device_number = daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+0xb;
    //UInt_t reg3=(ped_G1[ich]&0x1f)<<8 | (ped_G10[ich]&0x1f)<<3 | asic.CATIA_gain[ich]<<2 | asic.CATIA_LPF35[ich]<<1 | 1;
    //if(asic.CATIA_version[ich]>=14)
    //{
    //  reg3=(asic.CATIA_gain[ich]<<14) | ped_G1[ich]<<8 | ped_G10[ich]<<2 | (asic.CATIA_LPF35[ich]<<1) | 1;
    //}
    //iret=I2C_RW(hw, device_number, 3, reg3, 1, 1, 0);

    sprintf(widget_name,"%1.1d_CATIA_ped_G10",ich+1);
    GtkWidget *widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    gtk_spin_button_set_value((GtkSpinButton*)widget,0.);
    gtk_spin_button_set_value((GtkSpinButton*)widget,(double)ped_G10[ich]);
    sprintf(widget_name,"%1.1d_CATIA_ped_G1",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    gtk_spin_button_set_value((GtkSpinButton*)widget,0.);
    gtk_spin_button_set_value((GtkSpinButton*)widget,(double)ped_G1[ich]);

// Restore DTU Reg1 content
    num=asic.DTU_number[ich];
    device_number=daq.I2C_LiTEDTU_type*1000+(num<<daq.I2C_shift_dev_number)+2;
    iret=I2C_RW(hw, device_number, 1, reg1[ich], 0, 1, 0);
// Reinitialize actual pedestal values :
    daq.ped_G10[ich]=-1.;
    daq.ped_G1[ich]=-1.;
  }
  usleep(500000);

// Get one event with final settings to display histograms if debug_draw selected
  Int_t draw=1;
  get_event(1,draw);

  daq.nsample=nsample_saved;
  daq.trigger_type=trig_type_saved;
  daq.debug_DAQ=debug_DAQ;
  daq.debug_I2C=debug_I2C;

// Restore CATIA output levels:
  VFE_control= LVRB_AUTOSCAN*daq.LVRB_autoscan | PED_MUX*ped_mux | eLINK_ACTIVE*daq.eLink_active |
                      DTU_TEST_MODE*daq.DTU_test_mode | DTU_MEM_MODE*daq.MEM_mode | I2C_LPGBT_MODE*daq.I2C_lpGBT_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();
// Reset CRC error conters after pedestal optimization :
  get_CRC_errors(1);
}
