#define EXTERN extern
#include "gdaq_VFE.h"
void send_ReSync_command(UInt_t command)
{
  uhal::HwInterface hw=devices.front();
// Reset Test unit to get samples in right order :
  hw.getNode("DTU_RESYNC").write(command);
  hw.dispatch();

}
