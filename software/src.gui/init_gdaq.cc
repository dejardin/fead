#define EXTERN extern
#include "gdaq_VFE.h"

void init_gDAQ()
{

  UInt_t command;
  ValWord<uint32_t> address;
  for(Int_t ich=0; ich<6; ich++) daq.event[ich]=(short int*)malloc(sizeof(short int)*5*NSAMPLE_MAX);
  for(Int_t ich=0; ich<6; ich++) daq.gain[ich]=(short int*)malloc(sizeof(short int)*5*NSAMPLE_MAX);
  for(Int_t ich=0; ich<6; ich++) daq.fevent[ich]=(double*)malloc(sizeof(double)*5*NSAMPLE_MAX);

// Define defaults for laser runs :
  daq.delay_auto_tune     = 1;

  Int_t resync_phase      = 0;
  daq.resync_idle_pattern = 0x00;
  daq.fifo_mode           = 1;
  daq.I2C_LiTEDTU_type    = I2C_LiTEDTU_TYPE;
  daq.I2C_CATIA_type      = I2C_CATIA_TYPE;
  daq.daq_running         = FALSE;

  printf("Start DAQ with FEAD : %d and VFE %d\n", daq.FEAD_number, daq.VFE_number);
  printf("Will address catias : %d with subaddress 3\n",(1<<daq.I2C_shift_dev_number));
  printf("            and DTU : %d with subaddresses 0,1,2\n",(1<<daq.I2C_shift_dev_number));

  printf("Parameters : \n");
  printf("Read ADCs for :\n");
  printf("  %d events \n",daq.nevent);
  printf("  %d samples \n",daq.nsample);
  printf("  trigger type  : %d (0=pedestal, 1=TP, 2=laser/AWG, 3=fpga pulse)\n",daq.trigger_type);
  printf("  trigger loop  : %d (0=triggered from FE, 1=self triggered DAQ from data)\n",daq.self_trigger_loop);
  printf("  self trigger  : %d (1=internal generated trigger if signal > threshold), mode : %d\n",daq.self_trigger, daq.self_trigger_mode);
  printf("  threshold     : %d (minimal signal amplitude to generate self trigger)\n",daq.self_trigger_threshold);
  printf("  mask          : 0x%x (channel mask to generate self triggers)\n",daq.self_trigger_mask);

  //Int_t loc_argc=1;
  //char *loc_argv[10];
  //for(int i=0; i<10; i++)loc_argv[i]=(char *)malloc(132*sizeof(char));
  //sprintf(loc_argv[0],"test");
  //TApplication *Root_App=new TApplication("test", &loc_argc, loc_argv);
  //gStyle->SetPadGridX(kTRUE);
  //gStyle->SetPadGridY(kTRUE);

  char hname[80];
  for(int ich=0; ich<6; ich++)
  {
    printf("ich = %d\n",ich);
    daq.tg[ich] = new TGraph();
    daq.tg[ich]->SetMarkerStyle(20);
    daq.tg[ich]->SetMarkerSize(0.5);
    sprintf(hname,"ch%d_G10_samples",ich);
    daq.tg[ich]->SetTitle(hname);
    daq.tg[ich]->SetName(hname);
    daq.tg[ich]->GetYaxis()->SetTitleFont(62);
    daq.tg[ich]->GetYaxis()->SetTitleSize(0.05);
    daq.tg[ich]->GetYaxis()->SetTitleOffset(1.);
    daq.tg[ich]->GetYaxis()->SetLabelSize(0.05);
    daq.tg[ich]->GetXaxis()->SetLabelFont(62);
    daq.tg[ich]->GetXaxis()->SetLabelSize(0.05);
    daq.tg[ich]->GetXaxis()->SetTitleSize(0.05);

    daq.tg_g1[ich] = new TGraph();
    daq.tg_g1[ich]->SetMarkerStyle(20);
    daq.tg_g1[ich]->SetMarkerSize(1.0);
    sprintf(hname,"ch%d_G1_samples",ich);
    daq.tg[ich]->SetTitle(hname);
    daq.tg[ich]->SetName(hname);
    daq.tg_g1[ich]->GetYaxis()->SetTitleFont(62);
    daq.tg_g1[ich]->GetYaxis()->SetTitleSize(0.05);
    daq.tg_g1[ich]->GetYaxis()->SetTitleOffset(1.);
    daq.tg_g1[ich]->GetYaxis()->SetLabelSize(0.05);
    daq.tg_g1[ich]->GetXaxis()->SetLabelFont(62);
    daq.tg_g1[ich]->GetXaxis()->SetLabelSize(0.05);
    daq.tg_g1[ich]->GetXaxis()->SetTitleSize(0.05);
    daq.ped_G10[ich]=-1.;
    daq.ped_G1[ich]=-1.;
  }

  if(daq.DTU_test_mode==1 && daq.eLink_active==0x0F)
  {
    daq.tg[0]->SetLineColor(kCyan);
    daq.tg[1]->SetLineColor(kCyan);
    daq.tg[2]->SetLineColor(kMagenta);
    daq.tg[3]->SetLineColor(kMagenta);
    daq.tg[4]->SetLineColor(kBlue);
    daq.tg[5]->SetLineColor(kRed);
    daq.tg[0]->SetMarkerColor(kCyan);
    daq.tg[1]->SetMarkerColor(kCyan);
    daq.tg[2]->SetMarkerColor(kMagenta);
    daq.tg[3]->SetMarkerColor(kMagenta);
    daq.tg[4]->SetMarkerColor(kBlue);
    daq.tg[5]->SetMarkerColor(kRed);
    daq.tg_g1[0]->SetLineColor(kCyan);
    daq.tg_g1[1]->SetLineColor(kCyan);
    daq.tg_g1[2]->SetLineColor(kMagenta);
    daq.tg_g1[3]->SetLineColor(kMagenta);
    daq.tg_g1[4]->SetLineColor(kBlue);
    daq.tg_g1[5]->SetLineColor(kRed);
    daq.tg_g1[0]->SetMarkerColor(kCyan);
    daq.tg_g1[1]->SetMarkerColor(kCyan);
    daq.tg_g1[2]->SetMarkerColor(kMagenta);
    daq.tg_g1[3]->SetMarkerColor(kMagenta);
    daq.tg_g1[4]->SetMarkerColor(kBlue);
    daq.tg_g1[5]->SetMarkerColor(kRed);
  }
  else
  {
    daq.tg[0]->SetLineColor(kCyan);
    daq.tg[1]->SetLineColor(kBlue);
    daq.tg[2]->SetLineColor(kMagenta);
    daq.tg[3]->SetLineColor(kRed);
    daq.tg[4]->SetLineColor(kGreen);
    daq.tg[0]->SetMarkerColor(kCyan);
    daq.tg[1]->SetMarkerColor(kBlue);
    daq.tg[2]->SetMarkerColor(kMagenta);
    daq.tg[3]->SetMarkerColor(kRed);
    daq.tg[4]->SetMarkerColor(kGreen);
    daq.tg_g1[0]->SetLineColor(kCyan);
    daq.tg_g1[1]->SetLineColor(kBlue);
    daq.tg_g1[2]->SetLineColor(kMagenta);
    daq.tg_g1[3]->SetLineColor(kRed);
    daq.tg_g1[4]->SetLineColor(kGreen);
    daq.tg_g1[0]->SetMarkerColor(kCyan);
    daq.tg_g1[1]->SetMarkerColor(kBlue);
    daq.tg_g1[2]->SetMarkerColor(kMagenta);
    daq.tg_g1[3]->SetMarkerColor(kRed);
    daq.tg_g1[4]->SetMarkerColor(kGreen);
  }
  if(daq.debug_draw!=NULL)daq.c1->Update();

// Check that FEAD/VICEPP board is reachable :
  Int_t iret, ntry=0;
  char sys_call[132];
  if(daq.FEAD_number<90)
    sprintf(sys_call,"ping -c 1 -W 1 10.0.0.%d",daq.FEAD_number+128);
  else
    sprintf(sys_call,"ping -c 1 -W 1 192.168.0.%d",(daq.FEAD_number-90)+128);
  do
  {
    iret=system(sys_call);
    if(iret!=0)
    {
      Int_t errno;
      GtkWidget *dialog = gtk_message_dialog_new (GTK_WINDOW(gtk_data.main_window),
                                                  GTK_DIALOG_DESTROY_WITH_PARENT,
                                                  GTK_MESSAGE_ERROR,
                                                  GTK_BUTTONS_CLOSE,
      "FEAD/VICEPP board not reachable,\nplease check the network connection.",
                                  NULL, g_strerror (errno));
      gtk_window_set_title(GTK_WINDOW(dialog), "Error");
      gtk_dialog_run (GTK_DIALOG (dialog));
      gtk_widget_destroy (dialog);
      //printf("FEAD/VICEPP board %d not reachable, please check the networ connection.\n",daq.FEAD_number);
      //system("stty raw");
      //char cdum=getchar();
      //system("stty -raw");
    }
  } while(iret!=0 && ntry++<2);

  ConnectionManager manager ( "file://xml/FEAD/connection_file.xml" );
  char fead_str[80];
  sprintf(fead_str,"fead.udp.%d",daq.FEAD_number);
  //uhal::HwInterface hw=manager.getDevice( fead_str );
  devices.push_back(manager.getDevice( fead_str ));
  uhal::HwInterface hw=devices.front();

  ValWord<uint32_t> free_mem;
  ValWord<uint32_t> trig_reg;
  ValWord<uint32_t> delays;
  ValWord<uint32_t> reg;
  ValWord<uint32_t> debug1_reg[32];
  ValWord<uint32_t> debug2_reg[32];


  UInt_t VFE_control= LVRB_AUTOSCAN*daq.LVRB_autoscan | 
                      PED_MUX*0 | eLINK_ACTIVE*daq.eLink_active |
                      DTU_TEST_MODE*daq.DTU_test_mode | DTU_MEM_MODE*daq.MEM_mode | I2C_LPGBT_MODE*daq.I2C_lpGBT_mode;
  printf("Setting MEM_mode to %d\n",daq.MEM_mode);

// Set the clock tree :
  if(daq.VICEPP_clk >=0)
  {
    reg = hw.getNode("VICEPP_CLK").read();
    hw.dispatch();
    printf("Clock configuration aleady present : 0x%x\n",reg.value());
    if((reg.value()&3) == (daq.VICEPP_clk&3))
    {
      printf("Present XPoint setting 0x%8.8x already conform to request 0x%8.8x\n",reg.value(),daq.VICEPP_clk);
      printf("Do nothing !\n");
    }
    else
    {
      printf("Present XPoint setting 0x%8.8x not conform to request 0x%8.8x\n",reg.value(),daq.VICEPP_clk);
      printf("Set clock configuration for VICE++ board : 0x%8.8x\n",daq.VICEPP_clk);
      hw.getNode("VICEPP_CLK").write(daq.VICEPP_clk);
      hw.dispatch();
      usleep(100000);
    }
  }

// Init stage :
  hw.getNode("FEAD_CTRL").write(RESET*1);
  hw.getNode("FEAD_CTRL").write(RESET*0);
  hw.dispatch();
  usleep(100000);

// Read FW version to check :
  reg = hw.getNode("FW_VER").read();
  hw.dispatch();
  char fw_version[80];
  daq.firmware_version=reg.value();
  sprintf(fw_version,"FW version:\n %8.8x",daq.firmware_version);
  GtkWidget* widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_fw_version");
  g_assert (widget);
  gtk_label_set_text((GtkLabel*)widget,fw_version);

// Switch to triggered mode + external trigger :
  command= 
             SELF_TRIGGER_MODE     *daq.self_trigger_mode                |
            (SELF_TRIGGER_MASK     *(daq.self_trigger_mask&0x1F))        |
            (SELF_TRIGGER_THRESHOLD*(daq.self_trigger_threshold&0xFFF))  |
             SELF_TRIGGER          *daq.self_trigger                     |
             SELF_TRIGGER_LOOP     *daq.self_trigger_loop                |
             FIFO_MODE             *(daq.fifo_mode&1)                    |
             RESET                 *0;
  hw.getNode("FEAD_CTRL").write(command);
// Stop DAQ and ask for NSAMPLE per frame (+timestamp) :
  command = ((daq.nsample+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
// Add laser latency before catching data ~ 40 us
  hw.getNode("TRIG_DELAY").write(SW_DAQ_DELAY*daq.sw_DAQ_delay | HW_DAQ_DELAY*daq.hw_DAQ_delay);
// Switch ON FE-adapter LEDs
  command = TP_MODE*0+LED_ON*1+GENE_WTE*0+GENE_TP*0+GENE_TRIGGER*0;
  hw.getNode("FW_VER").write(command);
  hw.dispatch();

// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
// Start DAQ :
  command = ((daq.nsample+1)<<16)+CAPTURE_START;
  hw.getNode("CAP_CTRL").write(command);
// Read back delay values :
  delays=hw.getNode("TRIG_DELAY").read();
// Read back the read/write base address
  address = hw.getNode("CAP_ADDRESS").read();
  free_mem = hw.getNode("CAP_FREE").read();
  trig_reg = hw.getNode("FEAD_CTRL").read();
  hw.dispatch();

  //firmware_version=0x21083100;
  printf("Firmware version      : %8.8x\n",daq.firmware_version);
  printf("Delays                : %8.8x\n",delays.value());
  printf("Initial R/W addresses : 0x%8.8x\n", address.value());
  printf("Free memory           : 0x%8.8x\n", free_mem.value());
  printf("Trigger mode          : 0x%8.8x\n", trig_reg.value());
  printf("Active links          : 0x%2.2x\n", daq.eLink_active);
  daq.old_read_address=address.value()&0xffff;
  if(daq.old_read_address==NSAMPLE_MAX-1)daq.old_read_address=-1;

// Put FEAD outputs with idle patterns :
  hw.getNode("OUTPUT_CTRL").write(0);
  hw.dispatch();
  
// TP trigger setting :
  command=(daq.TP_delay<<16) | (daq.TP_width&0xffff);
  printf("TP trigger with %d clocks width and %d clocks delay : %x\n",daq.TP_width,daq.TP_delay,command);
  hw.getNode("CALIB_CTRL").write(command);
  hw.dispatch();

// Put DTU resync in reset mode, stop uLVRB autoscan mod
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.getNode("RESYNC_IDLE").write(TE_ENABLE*daq.TE_enable | TE_COMMAND*daq.TE_command | TE_POS*daq.TE_pos | RESYNC_IDLE_PATTERN*daq.resync_idle_pattern);
  hw.getNode("CLK_SETTING").write(resync_phase*RESYNC_PHASE);
  hw.dispatch();
  usleep(200);
}
