#define EXTERN extern
#include <gtk/gtk.h>
#include <TSystem.h>
#include "gdaq_VFE.h"

void callback_about (GtkMenuItem *menuitem, gpointer user_data)
{
  GtkWidget *dialog = NULL;

  dialog =  gtk_about_dialog_new ();

  /* Pour l'exemple on va rendre la fenêtre "a propos" modale par rapport a la fenêtre principale. */
  gtk_window_set_transient_for (GTK_WINDOW(dialog), GTK_WINDOW(gtk_builder_get_object (gtk_data.builder, "MainWindow")));

  gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (dialog);
}

void delete_event(GtkMenuItem *menuitem, gpointer user_data)
{
  printf("Signal delete !\n");
  end_of_run();
}

void my_exit(GtkMenuItem *menuitem, gpointer user_data)
{
  printf("Signal destroy !\n");
  end_of_run();
}

void button_clicked(GtkWidget *button, gpointer user_data)
{
  GtkWidget *widget=NULL;
  char widget_name[80];
  char content[80];
  const char *name=gtk_buildable_get_name((GtkBuildable*)button);
  if(daq.debug_GTK==1)printf("Button %s clicked !\n",name);
  int channel_number;
  char button_type[80];
  sscanf(name,"%d*",&channel_number);
  sscanf(&name[2],"%s",button_type);
  if(strcmp(button_type,"CATIA_gain")==0)
  {
    const char *label=gtk_button_get_label((GtkButton*)button);
    if(daq.debug_GTK==1)printf("Gain button version %d, old : %s, new : ",asic.CATIA_version[channel_number-1],label);
    if(asic.CATIA_version[channel_number-1]<14)
    {
      if(     strcmp(label,"400")==0)
      {
        gtk_button_set_label((GtkButton*)button,"500");
        asic.CATIA_gain[channel_number-1]=0;
      }
      else
      {
        gtk_button_set_label((GtkButton*)button,"400");
        asic.CATIA_gain[channel_number-1]=1;
      }
    }
    else if(asic.CATIA_version[channel_number-1]<21)
    {
      if(     strcmp(label,"340")==0)
      {
        gtk_button_set_label((GtkButton*)button,"400");
        asic.CATIA_gain[channel_number-1]=1;
      }
      else if(strcmp(label,"400")==0)
      {
        gtk_button_set_label((GtkButton*)button,"500");
        asic.CATIA_gain[channel_number-1]=0;
      }
      else
      {
        gtk_button_set_label((GtkButton*)button,"340");
        asic.CATIA_gain[channel_number-1]=3;
      }
    }
    else
    {
      if(     strcmp(label,"320")==0)
      {
        gtk_button_set_label((GtkButton*)button,"380");
        asic.CATIA_gain[channel_number-1]=1;
      }
      else if(strcmp(label,"380")==0)
      {
        gtk_button_set_label((GtkButton*)button,"470");
        asic.CATIA_gain[channel_number-1]=0;
      }
      else
      {
        gtk_button_set_label((GtkButton*)button,"320");
        asic.CATIA_gain[channel_number-1]=3;
      }
    }

    label=gtk_button_get_label((GtkButton*)button);
    if(daq.debug_GTK==1)printf("%s\n",label);
    update_CATIA_reg(channel_number-1,3);
  }
  else if(strcmp(button_type,"CATIA_LPF35")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      gtk_button_set_label((GtkButton*)button,"ON");
      asic.CATIA_LPF35[channel_number-1]=1;
    }
    else
    {
      gtk_button_set_label((GtkButton*)button,"OFF");
      asic.CATIA_LPF35[channel_number-1]=0;
    }
    update_CATIA_reg(channel_number-1,3);
  }
  else if(strcmp(button_type,"CATIA_DAC1_status")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      gtk_button_set_label((GtkButton*)button,"ON");
      asic.CATIA_DAC1_status[channel_number-1]=TRUE;
    }
    else
    {
      gtk_button_set_label((GtkButton*)button,"OFF");
      asic.CATIA_DAC1_status[channel_number-1]=FALSE;
    }
    update_CATIA_reg(channel_number-1,4);
  }
  else if(strcmp(button_type,"CATIA_DAC2_status")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      gtk_button_set_label((GtkButton*)button,"ON");
      asic.CATIA_DAC2_status[channel_number-1]=TRUE;
    }
    else
    {
      gtk_button_set_label((GtkButton*)button,"OFF");
      asic.CATIA_DAC2_status[channel_number-1]=FALSE;
    }
    update_CATIA_reg(channel_number-1,4);
    update_CATIA_reg(channel_number-1,5);
  }
  else if(strcmp(button_type,"CATIA_Rconv")==0)
  {
    const char *label=gtk_button_get_label((GtkButton*)button);
    if(strcmp(label,"2471")==0)
    {
      gtk_button_set_label((GtkButton*)button,"272");
      asic.CATIA_Rconv[channel_number-1]=1;
    }
    else
    {
      gtk_button_set_label((GtkButton*)button,"2471");
      asic.CATIA_Rconv[channel_number-1]=0;
    }
    update_CATIA_reg(channel_number-1,6);
  }
  else if(strcmp(button_type,"DTU_test_mode")==0)
  {
    const char *label=gtk_button_get_label((GtkButton*)button);
    if(daq.debug_GTK==1)printf("DTU_test_mode button, old : %s, new : ",label);
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      gtk_button_set_label((GtkButton*)button,"DTU test mode\n        ON");
      daq.DTU_test_mode=1;
    }
    else
    {
      gtk_button_set_label((GtkButton*)button,"DTU test mode\n        OFF");
      daq.DTU_test_mode=0;
    }
    label=gtk_button_get_label((GtkButton*)button);
    if(daq.debug_GTK==1)printf("%s\n",label);

// Do we have a test board ?
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_CATIA_calib");
    g_assert (widget);
    if(daq.n_active_channel==1 && daq.channel_number[0]==2 && daq.DTU_test_mode==1)
    {
      daq.eLink_active=0xf;
// Activate the CATIA_calib button only for test board (1 channel, 4 outputs)
      gtk_widget_set_sensitive(widget,TRUE);
      printf("Switch to test board setting !\n");
    }
    else
    {
      gtk_widget_set_sensitive(widget,FALSE);
    }

    if(daq.daq_initialized>=0)
    {
      UInt_t ped_mux=0;
      if(daq.CATIA_Vcal_out)ped_mux=1;
      UInt_t VFE_control= LVRB_AUTOSCAN*daq.LVRB_autoscan | PED_MUX*ped_mux | eLINK_ACTIVE*daq.eLink_active |
                          DTU_TEST_MODE*daq.DTU_test_mode | DTU_MEM_MODE*daq.MEM_mode | I2C_LPGBT_MODE*daq.I2C_lpGBT_mode;
      uhal::HwInterface hw=devices.front();
      hw.getNode("VFE_CTRL").write(VFE_control);
      hw.dispatch();
    }
  }
  else if(strcmp(button_type,"DTU_force_PLL")==0)
  {
    const char *label=gtk_button_get_label((GtkButton*)button);
    if(daq.debug_GTK==1)printf("DTU_force_PLL button, old : %s, new : ",label);
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      gtk_button_set_label((GtkButton*)button,"ON");
      asic.DTU_force_PLL[channel_number-1]=TRUE;
    }
    else
    {
      gtk_button_set_label((GtkButton*)button,"OFF");
      asic.DTU_force_PLL[channel_number-1]=FALSE;
    }
    if(asic.DTU_version[channel_number-1]>=20)update_LiTEDTU_reg(channel_number-1,17);
    label=gtk_button_get_label((GtkButton*)button);
    if(daq.debug_GTK==1)printf("%s\n",label);
  }
  else if(strcmp(button_type,"CATIA_scan_Vref")==0)
  {
    daq.CATIA_scan_Vref=gtk_toggle_button_get_active((GtkToggleButton*)button);
    if(daq.CATIA_scan_Vref)
      gtk_button_set_label((GtkButton*)button,"Vref (free)");
    else
      gtk_button_set_label((GtkButton*)button,"Vref (fixed)");
    for(Int_t i=1; i<=5; i++)
    {
      sprintf(widget_name,"%1.1d_CATIA_Vref_val",i);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_widget_set_sensitive(widget,daq.CATIA_scan_Vref);
    }
  }
  else if(strcmp(button_type,"CATIA_SEU_correction")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      gtk_button_set_label((GtkButton*)button,"ON");
      asic.CATIA_SEU_corr[channel_number-1]=TRUE;
    }
    else
    {
      gtk_button_set_label((GtkButton*)button,"OFF");
      asic.CATIA_SEU_corr[channel_number-1]=FALSE;
    }
    update_CATIA_reg(channel_number-1,1);
  }
  else if(strcmp(button_type,"DTU_force_G10")==0)
  {
    const char *label=gtk_button_get_label((GtkButton*)button);
    if(daq.debug_GTK==1)printf("DTU_force_G10, old : %s, new : ",label);
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      gtk_button_set_label((GtkButton*)button,"ON");
      asic.DTU_force_G10[channel_number-1]=TRUE;
    }
    else
    {
      gtk_button_set_label((GtkButton*)button,"OFF");
      asic.DTU_force_G10[channel_number-1]=FALSE;
    }
    label=gtk_button_get_label((GtkButton*)button);
    if(daq.debug_GTK==1)printf("%s\n",label);
    update_LiTEDTU_reg(channel_number-1,1);
  }
  else if(strcmp(button_type,"DTU_force_G1")==0)
  {
    const char *label=gtk_button_get_label((GtkButton*)button);
    if(daq.debug_GTK==1)printf("DTU_force_G1, old : %s, new : ",label);
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      gtk_button_set_label((GtkButton*)button,"ON");
      asic.DTU_force_G1[channel_number-1]=TRUE;
    }
    else
    {
      gtk_button_set_label((GtkButton*)button,"OFF");
      asic.DTU_force_G1[channel_number-1]=FALSE;
    }
    label=gtk_button_get_label((GtkButton*)button);
    if(daq.debug_GTK==1)printf("%s\n",label);
    update_LiTEDTU_reg(channel_number-1,1);
  }
  else if(strcmp(button_type,"is_VICEPP")==0)
  {
    daq.is_VICEPP=gtk_toggle_button_get_active((GtkToggleButton*)button);
    if(daq.is_VICEPP)
      gtk_button_set_label((GtkButton*)button,"VICEPP #");
    else
      gtk_button_set_label((GtkButton*)button," FEAD # ");
  }
  else if(strcmp(button_type,"MEM_mode")==0)
  {
    //daq.MEM_mode=gtk_toggle_button_get_active((GtkToggleButton*)button);
    if(daq.MEM_pos==-1)      daq.MEM_pos=0; //MEM_2C
    else if(daq.MEM_pos==0)  daq.MEM_pos=1;
    else if(daq.MEM_pos==1)  daq.MEM_pos=2;
    else if(daq.MEM_pos==2)  daq.MEM_pos=9;
    else if(daq.MEM_pos==9)  daq.MEM_pos=10;
    else if(daq.MEM_pos==10) daq.MEM_pos=-1;
    else if(daq.MEM_pos==-1)  daq.MEM_pos=0;
    daq.MEM_mode=FALSE;
    sprintf(content,"MEM mode\n      OFF");
    if(daq.MEM_pos>=0)
    {
      daq.MEM_mode=TRUE;
      sprintf(content,"MEM mode\n        %d",daq.MEM_pos);
    }
    gtk_button_set_label((GtkButton*)button,content);
    if(daq.debug_GTK==1)
    {
      if(daq.MEM_mode)printf("Set MEM_mode to TRUE : %d\n",daq.MEM_pos);
      else printf("Set MEM_mode to FALSE : %d\n",daq.MEM_pos);
    }
    if(daq.MEM_mode)
    {
// Default MEM_mode : all channels running at 80 MHz
      if(daq.MEM_pos>0)
      {
        for(Int_t ich=0; ich<5; ich++)
        {
          asic.CATIA_number[ich]=-1;
          asic.DTU_number[ich]=-1;
          sprintf(widget_name,"%1.1d_channel_status",ich+1);
          widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
          g_assert (widget);
          gtk_switch_set_state((GtkSwitch*)widget,FALSE);
        }
      }
      if(daq.MEM_pos==1)
      {
        asic.CATIA_number[1]=1;
        daq.n_active_channel=1;  // CATIA address for TP setting
        daq.channel_number[0]=1; // FEAD channel for DAQ
        asic.DTU_number[1]=1; // DTU address
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_channel_status");
        g_assert (widget);
        gtk_switch_set_state((GtkSwitch*)widget,TRUE);
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_channel_status");
        g_assert (widget);
        gtk_switch_set_state((GtkSwitch*)widget,TRUE);
      }
      if(daq.MEM_pos==2)
      {
        asic.CATIA_number[0]=1;
        daq.n_active_channel=2;
        daq.channel_number[0]=0;
        daq.channel_number[1]=4;
        asic.DTU_number[0]=3;
        asic.DTU_number[4]=2;
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_channel_status");
        g_assert (widget);
        gtk_switch_set_state((GtkSwitch*)widget,TRUE);
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"5_channel_status");
        g_assert (widget);
        gtk_switch_set_state((GtkSwitch*)widget,TRUE);
      }
      if(daq.MEM_pos==9)
      {
        asic.CATIA_number[0]=1;
        daq.n_active_channel=2;
        daq.channel_number[0]=0;
        daq.channel_number[1]=4;
        asic.DTU_number[0]=5;
        asic.DTU_number[4]=4;
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_channel_status");
        g_assert (widget);
        gtk_switch_set_state((GtkSwitch*)widget,TRUE);
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"5_channel_status");
        g_assert (widget);
        gtk_switch_set_state((GtkSwitch*)widget,TRUE);
      }
      if(daq.MEM_pos==10)
      {
        asic.CATIA_number[3]=1;
        daq.n_active_channel=1;
        daq.channel_number[0]=3;
        asic.DTU_number[3]=6;
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_channel_status");
        g_assert (widget);
        gtk_switch_set_state((GtkSwitch*)widget,TRUE);
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_channel_status");
        g_assert (widget);
        gtk_switch_set_state((GtkSwitch*)widget,TRUE);
      }
      if(daq.daq_initialized>=0)
      {
        UInt_t ped_mux=0;
        if(daq.CATIA_Vcal_out)ped_mux=1;
        UInt_t VFE_control= LVRB_AUTOSCAN*daq.LVRB_autoscan | 
                            PED_MUX*ped_mux | eLINK_ACTIVE*daq.eLink_active |
                            DTU_TEST_MODE*daq.DTU_test_mode | DTU_MEM_MODE*daq.MEM_mode | I2C_LPGBT_MODE*daq.I2C_lpGBT_mode;
        uhal::HwInterface hw=devices.front();
        hw.getNode("VFE_CTRL").write(VFE_control);
        hw.dispatch();
  // Update Register 1 of LiTE-DTUs
        for(Int_t i=0; i<daq.n_active_channel; i++)
        {
          Int_t ich=asic.DTU_number[i]-1;
          update_LiTEDTU_reg(ich, 1);
        }
      }
    }
    else
    {
      for(Int_t ich=0; ich<5; ich++)
      {
        asic.CATIA_number[ich]=ich+1;
        asic.DTU_number[ich]=ich+1;
        sprintf(widget_name,"%1.1d_channel_status",ich+1);
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
        g_assert (widget);
        gtk_switch_set_state((GtkSwitch*)widget,FALSE);
        gtk_switch_set_state((GtkSwitch*)widget,TRUE);
      }
    }
  }
  else if(strcmp(button_type,"clock_setting")==0)
  {
    const char *label=gtk_button_get_label((GtkButton*)button);
    if(daq.debug_GTK==1)printf("Clock setting, old : %s, new : ",label);
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
      gtk_button_set_label((GtkButton*)button," Clock:\nfrom FE");
    else
      gtk_button_set_label((GtkButton*)button,"Clock:\n local");
    label=gtk_button_get_label((GtkButton*)button);
    if(daq.debug_GTK==1)printf("%s\n",label);
  }
  else if(strcmp(button_type,"resync_phase")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      gtk_button_set_label((GtkButton*)button,"ReSync\n  clk v");
      daq.resync_phase=1;
    }
    else
    {
      gtk_button_set_label((GtkButton*)button,"ReSync\n  clk ^");
      daq.resync_phase=0;
    }
    update_trigger();
  }
  else if(strcmp(button_type,"I2C_standard")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      daq.I2C_lpGBT_mode=1;
      gtk_button_set_label((GtkButton*)button,"   I2C\n  lpGBT ");
    }
    else
    {
      daq.I2C_lpGBT_mode=0;
      gtk_button_set_label((GtkButton*)button,"   I2C\nstandard");
    }
    printf("I2C set to standard: %d\n",daq.I2C_lpGBT_mode);
    if(daq.daq_initialized>=0)
    {
      UInt_t ped_mux=0;
      if(daq.CATIA_Vcal_out)ped_mux=1;
      UInt_t VFE_control= LVRB_AUTOSCAN*daq.LVRB_autoscan | 
                          PED_MUX*ped_mux | eLINK_ACTIVE*daq.eLink_active |
                          DTU_TEST_MODE*daq.DTU_test_mode | DTU_MEM_MODE*daq.MEM_mode | I2C_LPGBT_MODE*daq.I2C_lpGBT_mode;
      uhal::HwInterface hw=devices.front();
      hw.getNode("VFE_CTRL").write(VFE_control);
      hw.dispatch();
    }
  }
  else if(strcmp(button_type,"DTU_override_Vc_bit")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      for(Int_t i=0; i<daq.n_active_channel; i++)
      {
        Int_t ich=daq.channel_number[i];
        asic.DTU_override_Vc_bit[ich]=1;
      }
      gtk_button_set_label((GtkButton*)button,"PLL loop\n  open");
    }
    else
    {
      for(Int_t i=0; i<daq.n_active_channel; i++)
      {
        Int_t ich=daq.channel_number[i];
        asic.DTU_override_Vc_bit[ich]=0;
      }
      gtk_button_set_label((GtkButton*)button,"PLL loop\n closed");
    }
    for(Int_t i=0; i<daq.n_active_channel; i++)
    {
      Int_t ich=daq.channel_number[i];
      if(asic.DTU_version[ich]<20)
        update_LiTEDTU_reg(ich,15);
      else
        update_LiTEDTU_reg(ich,17);
    }
  }
  else if(strcmp(button_type,"SEU_test_mode")==0)
  {
    if(daq.SEU_test_mode==2)
    {
      gtk_button_set_label((GtkButton*)button,"SEU test mode :\n        reset");
      daq.SEU_test_mode=0;
    }
    else if(daq.SEU_test_mode==0)
    {
      gtk_button_set_label((GtkButton*)button,"SEU test mode :\n         keep");
      daq.SEU_test_mode=1;
    }
    else if(daq.SEU_test_mode==1)
    {
      gtk_button_set_label((GtkButton*)button,"SEU test mode\n       restore");
      daq.SEU_test_mode=2;
    }
  }
  else if(strcmp(button_type,"SEU_pattern")==0)
  {
    if(daq.SEU_pattern==6)
    {
      gtk_button_set_label((GtkButton*)button,"SEU pattern :\n      All 0");
      daq.SEU_pattern=0;
    }
    else if(daq.SEU_pattern==0)
    {
      gtk_button_set_label((GtkButton*)button,"SEU pattern :\n      All 1");
      daq.SEU_pattern=1;
    }
    else if(daq.SEU_pattern==1)
    {
      gtk_button_set_label((GtkButton*)button,"SEU pattern :\n    Comb 10");
      daq.SEU_pattern=2;
    }
    else if(daq.SEU_pattern==2)
    {
      gtk_button_set_label((GtkButton*)button,"SEU pattern :\n    Comb 01");
      daq.SEU_pattern=3;
    }
    else if(daq.SEU_pattern==3)
    {
      gtk_button_set_label((GtkButton*)button,"SEU pattern :\n    default");
      daq.SEU_pattern=4;
    }
    else if(daq.SEU_pattern==4)
    {
      gtk_button_set_label((GtkButton*)button,"SEU pattern :\n       xml");
      daq.SEU_pattern=5;
    }
    else if(daq.SEU_pattern==5)
    {
      gtk_button_set_label((GtkButton*)button,"SEU pattern :\n      keep");
      daq.SEU_pattern=6;
    }
  }
  else if(strcmp(button_type,"SEU_simul")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      gtk_button_set_label((GtkButton*)button,"SEU simulation :\n          ON");
      daq.SEU_simul=TRUE;
    }
    else
    {
      gtk_button_set_label((GtkButton*)button,"SEU simulation :\n          OFF");
      daq.SEU_simul=FALSE;
    }
  }
  else if(strcmp(button_type,"ped_event")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      printf("Set trigger for pedestal events\n");
      daq.trigger_type=0;
      daq.self_trigger=0;
      daq.calib_pulse_enabled=0;
      update_trigger();
// inactivate other trigger_type :
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_ReSync_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_laser_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_fpga_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
// activate soft_trigger mode
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_trigger_mode");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
    }
  }
  else if(strcmp(button_type,"ReSync_event")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      printf("Set trigger for ReSync events\n");
      daq.trigger_type=5;
      daq.self_trigger=0;
      daq.calib_pulse_enabled=0;
      update_trigger();
// inactivate other trigger_type :
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_ped_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_laser_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_fpga_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
// activate soft_trigger mode
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_trigger_mode");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
    }
  }
  else if(strcmp(button_type,"TP_event")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      printf("Set trigger for test-pulse events\n");
      daq.trigger_type=1;
      daq.self_trigger=0;
      daq.calib_pulse_enabled=0;
      update_trigger();
// inactivate other trigger_type :
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_ReSync_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_ped_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_laser_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_fpga_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
// activate soft_trigger mode
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_trigger_mode");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
    }
  }
  else if(strcmp(button_type,"laser_event")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      printf("Set trigger for external pulses\n");
      daq.trigger_type=2;
      daq.self_trigger=1;
      daq.self_trigger_loop=1;
      daq.calib_pulse_enabled=0;
      update_trigger();
// inactivate other trigger_type :
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_ReSync_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_ped_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_fpga_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
// activate self_trigger mode
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_trigger_mode");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
    }
  }
  else if(strcmp(button_type,"fpga_event")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      printf("Set trigger for pulses from FEAD board\nOnly with calibration board\n");
      daq.trigger_type=3;
      daq.self_trigger=0;
      daq.calib_pulse_enabled=1;
      update_trigger();
// inactivate other trigger_type :
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_ReSync_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_ped_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_laser_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);

// deactivate self_trigger mode
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_trigger_mode");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
    }
  }
  else if(strcmp(button_type,"debug_DAQ")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      if(daq.debug_DAQ==0)daq.debug_DAQ=1;
    }
    else
      daq.debug_DAQ=0;
  }
  else if(strcmp(button_type,"debug_I2C")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
      daq.debug_I2C=1;
    else
      daq.debug_I2C=0;
    if(daq.debug_GTK==1)printf("New debug_I2C : %d\n",daq.debug_I2C);
  }
  else if(strcmp(button_type,"debug_GTK")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
      daq.debug_GTK=1;
    else
      daq.debug_GTK=0;
  }
  else if(strcmp(button_type,"debug_draw")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      daq.debug_draw=1;
      printf("Set debug_draw to 1 : %d\n",daq.debug_draw);
      if(daq.c1==NULL)
      {
        daq.c1=new TCanvas("c1","c1",700,660,1200.,420.);
        daq.c1->Divide(3,2);
        daq.c1->Update();
      }
    }
    else
    {
      printf("destroy draw window !\n");
      daq.debug_draw=0;
      printf("Set debug_draw to 0 : %d\n",daq.debug_draw);
      if(daq.c1!=NULL)
      {
        //delete daq.c1;
        daq.c1->~TCanvas();
        gSystem->ProcessEvents();
        daq.c1=NULL;
      }
    }
  }
  else if(strcmp(button_type,"zoom_draw")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
      daq.zoom_draw=1;
    else
      daq.zoom_draw=0;
  }
  else if(strcmp(button_type,"selected_channel")==0)
  {
    do
    {
      daq.selected_channel++;
    }
    while((daq.eLink_active & (1<<(daq.selected_channel)))==0 && daq.selected_channel<5);
    if(daq.selected_channel==5)daq.selected_channel=0;
    sprintf(content,"Channel\n      %d",daq.selected_channel+1);
    gtk_button_set_label((GtkButton*)button,content);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_tap_slip");
    g_assert (widget);
    sprintf(content,"tap slip\n    (%d)",daq.delay_val[daq.selected_channel]);
    gtk_button_set_label((GtkButton*)widget,content);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_bit_slip");
    g_assert (widget);
    sprintf(content,"bit slip\n    (%d)",daq.bitslip_val[daq.selected_channel]);
    gtk_button_set_label((GtkButton*)widget,content);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_byte_slip");
    g_assert (widget);
    sprintf(content,"byte slip\n     (%d)",daq.byteslip_val[daq.selected_channel]);
    gtk_button_set_label((GtkButton*)widget,content);
  }
  else if(strcmp(button_type,"tap_slip")==0)
  {
    daq.delay_val[daq.selected_channel]=daq.delay_val[daq.selected_channel]+1;
    if(daq.delay_val[daq.selected_channel]>=32)daq.delay_val[daq.selected_channel]-=32;
    uhal::HwInterface hw=devices.front();
    hw.getNode("DELAY_CTRL").write(DELAY_TAP_DIR*1 | DELAY_RESET*0 | (1<<daq.selected_channel));
    hw.dispatch();
    sprintf(content,"tap slip\n    (%d)",daq.delay_val[daq.selected_channel]);
    gtk_button_set_label((GtkButton*)button,content);
  }
  else if(strcmp(button_type,"bit_slip")==0)
  {
    daq.bitslip_val[daq.selected_channel]=(daq.bitslip_val[daq.selected_channel]+1)%8;
    uhal::HwInterface hw=devices.front();
    hw.getNode("DELAY_CTRL").write((1<<daq.selected_channel)*BIT_SLIP | DELAY_TAP_DIR*1| DELAY_RESET*0);
    hw.dispatch();
    sprintf(content,"bit slip\n    (%d)",daq.bitslip_val[daq.selected_channel]);
    gtk_button_set_label((GtkButton*)button,content);
  }
  else if(strcmp(button_type,"byte_slip")==0)
  {
    daq.byteslip_val[daq.selected_channel]=(daq.byteslip_val[daq.selected_channel]+1)%4;
    uhal::HwInterface hw=devices.front();
    hw.getNode("DELAY_CTRL").write((1<<daq.selected_channel)*BYTE_SLIP | DELAY_TAP_DIR*1| DELAY_RESET*0);
    hw.dispatch();
    sprintf(content,"byte slip\n     (%d)",daq.byteslip_val[daq.selected_channel]);
    gtk_button_set_label((GtkButton*)button,content);
  }
  else if(strcmp(button_type,"gain_correction")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      gtk_button_set_label((GtkButton*)button,"  Draw with\ngain corr");
      daq.corgain=TRUE;
    }
    else
    {
      gtk_button_set_label((GtkButton*)button,"Draw without\ngain corr");
      daq.corgain=FALSE;
    }
  }
  else if(strcmp(button_type,"wait_for_ever")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      gtk_button_set_label((GtkButton*)button,"DAQ without\n    timeout");
      daq.wait_for_ever=TRUE;
    }
    else
    {
      gtk_button_set_label((GtkButton*)button,"DAQ with\n timeout");
      daq.wait_for_ever=FALSE;
    }
  }
  else if(strcmp(button_type,"CATIA_temp")==0)
  {
    if(daq.daq_initialized>=0) get_temp();
  }
  else if(strcmp(button_type,"CRC_errors")==0)
  {
    if(daq.daq_initialized>=0) get_CRC_errors(0);
  }
  else if(strcmp(button_type,"n_orbit_samples")==0)
  {
    if(daq.daq_initialized>=0) get_n_orbit_samples();
  }
  else if(strcmp(button_type,"PLL_scan")==0)
  {
    if(daq.daq_initialized>=0) PLL_scan();
  }
  else if(strcmp(button_type,"TE_enable")==0)
  {
    daq.TE_enable=gtk_toggle_button_get_active((GtkToggleButton*)button);
    if(daq.daq_initialized>=0)
    {
      uhal::HwInterface hw=devices.front();
      hw.getNode("RESYNC_IDLE").write(TE_ENABLE*daq.TE_enable | TE_COMMAND*daq.TE_command | TE_POS*daq.TE_pos | RESYNC_IDLE_PATTERN*daq.resync_idle_pattern);
      hw.dispatch();
      get_CRC_errors(1);
      get_n_orbit_samples();
    }
  }
  else if(strcmp(button_type,"power_ON")==0)
  {
    switch_power(gtk_toggle_button_get_active((GtkToggleButton*)button));
  }
  else if(strcmp(button_type,"LVRB_autoscan")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      daq.LVRB_autoscan=1;
      gtk_button_set_label((GtkButton*)button,"LVRB autoscan\n          ON");
    }
    else
    {
      daq.LVRB_autoscan=0;
      gtk_button_set_label((GtkButton*)button,"LVRB autoscan\n          OFF");
    }
    if(daq.daq_initialized>=0)
    {
      UInt_t ped_mux=0;
      if(daq.CATIA_Vcal_out)ped_mux=1;
      UInt_t VFE_control= LVRB_AUTOSCAN*daq.LVRB_autoscan | PED_MUX*ped_mux | eLINK_ACTIVE*daq.eLink_active |
                          DTU_TEST_MODE*daq.DTU_test_mode | DTU_MEM_MODE*daq.MEM_mode | I2C_LPGBT_MODE*daq.I2C_lpGBT_mode;
      uhal::HwInterface hw=devices.front();
      hw.getNode("VFE_CTRL").write(VFE_control);
      hw.dispatch();
    }
  }
  else if(strcmp(button_type,"trigger_mode")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      daq.self_trigger=1;
      gtk_button_set_label((GtkButton*)button,"Trigger mode:\n        self");
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_self_trigger_mode");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_self_trigger_threshold");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_self_trigger_mask_label");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_self_trigger_mask");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
    }
    else
    {
      daq.self_trigger=0;
      gtk_button_set_label((GtkButton*)button,"Trigger mode:\n        soft");
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_self_trigger_mode");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_self_trigger_threshold");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_self_trigger_mask_label");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_self_trigger_mask");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
    }
    update_trigger();
  }
  else if(strcmp(button_type,"self_trigger_mode")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      daq.self_trigger_mode=1;
      gtk_button_set_label((GtkButton*)button,"Trigger threshold:\n         delta");
    }
    else
    {
      daq.self_trigger_mode=0;
      gtk_button_set_label((GtkButton*)button,"Trigger threshold:\n       absolute");
    }
    update_trigger();
  }
  else if(strcmp(button_type,"reset_line_delays")==0)
  {
    printf("Reset FPGA delay lines\n");
    reset_DTU_sync();
  }
  else if(strcmp(button_type,"reset_DTUTestUnit")==0)
  {
    printf("Reset DTUTestUnit\n");
    send_ReSync_command(LiTEDTU_ADCTestUnit_reset);
  }
  else if(strcmp(button_type,"reset_DTU")==0)
  {
    printf("Reset DTU\n");
    send_ReSync_command(LiTEDTU_DTU_reset);
  }
  else if(strcmp(button_type,"reset_DTU_I2C")==0)
  {
    printf("Reset DTU I2C\n");
    send_ReSync_command(LiTEDTU_I2C_reset);
  }
  else if(strcmp(button_type,"CATIA_outputs")==0)
  {
    if(daq.CATIA_Vcal_out)
    {
      printf("Switch CATIA outputs to physics signals\n");
      daq.CATIA_Vcal_out=FALSE;
      gtk_button_set_label((GtkButton*)button,"CATIA outputs:\n      physics");
    }
    else
    {
      printf("Switch CATIA outputs to calibration levels\n");
      daq.CATIA_Vcal_out=TRUE;
      gtk_button_set_label((GtkButton*)button,"CATIA outputs:\n    calibration");
    }
    if(daq.daq_initialized>=0)
    {
      UInt_t ped_mux=0;
      if(daq.CATIA_Vcal_out)ped_mux=1;
      UInt_t VFE_control= LVRB_AUTOSCAN*daq.LVRB_autoscan | PED_MUX*ped_mux | eLINK_ACTIVE*daq.eLink_active |
                          DTU_TEST_MODE*daq.DTU_test_mode | DTU_MEM_MODE*daq.MEM_mode | I2C_LPGBT_MODE*daq.I2C_lpGBT_mode;
      uhal::HwInterface hw=devices.front();
      hw.getNode("VFE_CTRL").write(VFE_control);
      hw.dispatch();
    }
    for(Int_t i=0; i<daq.n_active_channel; i++) {Int_t ich=daq.channel_number[i]; update_CATIA_reg(ich,5);}
  }
  else if(strcmp(button_type,"ADC_calibration")==0)
  {
    printf("Launch ADC calibration\n");
    calib_ADC();
  }
  else if(strcmp(button_type,"synchronize_links")==0)
  {
    printf("Launch links synchronization\n");
    synchronize_links();
  }
  else if(strcmp(button_type,"shift_odd_samples")==0)
  {
    daq.odd_sample_shift++;
    if(daq.odd_sample_shift>2)daq.odd_sample_shift=-2;
    sprintf(content,"Shift odd samples\n             %d",daq.odd_sample_shift);
    gtk_button_set_label((GtkButton*)button,content);
  }
  else if(strcmp(button_type,"dump_ADC_registers")==0)
  {
    printf("Dump ADC registers to file\n");
    dump_ADC_registers();
  }
  else if(strcmp(button_type,"load_ADC_registers")==0)
  {
    printf("Restore ADC registers from file\n");
    load_ADC_registers();
  }
  else if(strcmp(button_type,"get_event")==0)
  {
    printf("Get event\n");
    get_single_event();
  }
  else if(strcmp(button_type,"take_run")==0)
  {
    if(!daq.daq_running)
    {
      printf("Take run\n");
      daq.daq_running=TRUE;
      sprintf(content,"Abort");
      gtk_button_set_label((GtkButton*)button,content);

// Disable "SEU_test" and others
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_SEU_test");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_get_event");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);

      take_run();
    }
    else
    {
      daq.daq_running=FALSE;
      sprintf(content,"Take run");
      gtk_button_set_label((GtkButton*)button,content);

// Re-enable "SEU_test"
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_SEU_test");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_get_event");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
    }
  }
  else if(strcmp(button_type,"SEU_test")==0)
  {
    if(!SEU.test_running)
    {
      printf("Start SEU test\n");
      SEU.test_running=TRUE;
      sprintf(content,"Abort");
      gtk_button_set_label((GtkButton*)button,content);

// Disable "take_run"
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_take_run");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_get_event");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);

      SEU_test();
    }
    else
    {
      SEU.test_running=FALSE;
      sprintf(content,"SEU test");
      gtk_button_set_label((GtkButton*)button,content);

// Re-enable "take_run"
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_take_run");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_get_event");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
    }
  }
  else if(strcmp(button_type,"optimize_pedestals")==0)
  {
    printf("Optimize pedestal\n");
    optimize_pedestal();
  }
  else if(strcmp(button_type,"CATIA_calib")==0)
  {
    printf("Launch CATIA_calibration\n");
    CATIA_calibration();
  }
  else if(strcmp(button_type,"init_VFE")==0)
  {
    if(daq.daq_initialized>=0)
    {
      init_VFE();
    }
  }
  else if(strcmp(button_type,"init_DAQ")==0)
  {
// Create contact with FEAD board :
    if(daq.daq_initialized<0)
    {
      init_gDAQ();
      gtk_button_set_label((GtkButton*)button,"DAQ initialized");
      daq.daq_initialized=0;
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_CATIA_outputs");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_ADC_calibration");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_synchronize_links");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_shift_odd_samples");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_SEU_test");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_reset_line_delays");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_reset_DTUTestUnit");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_dump_ADC_registers");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_load_ADC_registers");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_optimize_pedestals");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_PLL_scan");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_power_ON");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_LVRB_autoscan");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_init_VFE");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_selected_channel");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_tap_slip");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_bit_slip");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_byte_slip");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_CATIA_temp");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_ReSync_data");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
// Retreive ReSync data and load it in FEAD board;
      sscanf(gtk_entry_get_text((GtkEntry*)widget),"%x",&daq.ReSync_data);
      sprintf(content,"%x",daq.ReSync_data);
      gtk_entry_set_text((GtkEntry*)widget,content);
      uhal::HwInterface hw=devices.front();
      hw.getNode("DTU_RESYNC").write(daq.ReSync_data);
      hw.dispatch();
    }
    else
    {
      gtk_toggle_button_set_active((GtkToggleButton*)button,TRUE);
    }
  }
  if(daq.daq_initialized==0 && daq.trigger_type>=0)
  {
    daq.daq_initialized++;
// Release other actions :
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_get_event");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,TRUE);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_take_run");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,TRUE);
  }
}

void switch_toggled(GtkWidget *button, gpointer user_data)
{
  const char *name=gtk_buildable_get_name((GtkBuildable*)button);
  int channel_number;
  char button_type[80];
  char widget_name[80];
  GtkWidget *widget;
  sscanf(name,"%d*",&channel_number);
  sscanf(&name[2],"%s",button_type);

  if(daq.debug_GTK==1)printf("Switch %s toggled : channel %d, type %s, state %d\n",name,channel_number, button_type,gtk_switch_get_active((GtkSwitch*)button));
  gtk_switch_set_state((GtkSwitch*)button,gtk_switch_get_active((GtkSwitch*)button));

// (de)activate all widgets relative to this channel
  if(strcmp(button_type,"channel_status")==0)
  {
    enable_channel(channel_number,gtk_switch_get_active((GtkSwitch*)button));
  }

// Rescan every thing to see if we have a test board :
  daq.n_active_channel=0;
  daq.eLink_active=0;
  for(Int_t ich=0; ich<5; ich++)
  {
    sprintf(widget_name,"%1.1d_channel_status",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    if(gtk_switch_get_active((GtkSwitch*)widget) && asic.DTU_number[ich]>=0)
    {   
      daq.channel_number[daq.n_active_channel++]=ich;
      daq.eLink_active|=(1<<ich);
    }   
  }
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_CATIA_calib");
  g_assert (widget);
  if(daq.n_active_channel==1 && daq.channel_number[0]==2 && daq.DTU_test_mode==1)
  {
    daq.eLink_active=0xf;
// Activate the CATIA_calib button only for test board (1 channel, 4 outputs)
    gtk_widget_set_sensitive(widget,TRUE);
    printf("Switch to test board setting !\n");
  }
  else
  {
    gtk_widget_set_sensitive(widget,FALSE);
  }
}

void Entry_modified(GtkWidget *button, gpointer user_data)
{
  GtkWidget *widget;

  const char *name=gtk_buildable_get_name((GtkBuildable*)button);
  int channel_number, device_number, iret;
  char text[80], button_type[80], widget_name[80];
  sscanf(name,"%d*",&channel_number);
  sscanf(&name[2],"%s",button_type);

  if(daq.debug_GTK==1)printf("Entry %s modified : channel %d, type %s, new value %s\n",name,channel_number, button_type,gtk_entry_get_text((GtkEntry*)button));
  if(strcmp(button_type,"CATIA_version")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&asic.CATIA_version[channel_number-1]);
    if(asic.CATIA_version[channel_number-1]<=14)daq.calib_mux_enabled=1;
  }
  else if(strcmp(button_type,"DTU_version")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&asic.DTU_version[channel_number-1]);
  }
  else if(strcmp(button_type,"FEAD_number")==0)
  {
    Int_t loc_number;
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&loc_number);
    daq.FEAD_number=loc_number;
    daq.VFE_number=0;
    printf("New FEAD board %d\n",daq.FEAD_number);
  }
  else if(strcmp(button_type,"self_trigger_threshold")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.self_trigger_threshold);
    if(daq.self_trigger_threshold<0)daq.self_trigger_threshold=0;
    if(daq.self_trigger_threshold>4095)daq.self_trigger_threshold=4095;
    sprintf(text,"%d",daq.self_trigger_threshold);
    gtk_entry_set_text((GtkEntry*)button,text);
    update_trigger();
  }
  else if(strcmp(button_type,"self_trigger_mask")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.self_trigger_mask);
    sprintf(text,"%x",daq.self_trigger_mask&0x1F);
    gtk_entry_set_text((GtkEntry*)button,text);
    update_trigger();
  }
  else if(strcmp(button_type,"nsample")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.nsample);
    if(daq.nsample<=0)daq.nsample=1;
    if(daq.nsample>NSAMPLE_MAX-2)daq.nsample=NSAMPLE_MAX-2;
    sprintf(text,"%d",daq.nsample);
    gtk_entry_set_text((GtkEntry*)button,text);
  }
  else if(strcmp(button_type,"nevent")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.nevent);
    if(daq.nevent<-1)daq.nevent=-1;
    if(daq.nevent==0)daq.nevent=1;
    sprintf(text,"%d",daq.nevent);
    gtk_entry_set_text((GtkEntry*)button,text);
  }
  else if(strcmp(button_type,"sw_DAQ_delay")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.sw_DAQ_delay);
    if(daq.sw_DAQ_delay<0)daq.sw_DAQ_delay=0;
    sprintf(text,"%d",daq.sw_DAQ_delay);
    gtk_entry_set_text((GtkEntry*)button,text);
    if(daq.daq_initialized>=0)
    {
      uhal::HwInterface hw=devices.front();
      hw.getNode("TRIG_DELAY").write(SW_DAQ_DELAY*daq.sw_DAQ_delay | HW_DAQ_DELAY*daq.hw_DAQ_delay);
      hw.dispatch();
    }
  }
  else if(strcmp(button_type,"hw_DAQ_delay")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.hw_DAQ_delay);
    if(daq.hw_DAQ_delay<0)daq.hw_DAQ_delay=0;
    sprintf(text,"%d",daq.hw_DAQ_delay);
    gtk_entry_set_text((GtkEntry*)button,text);
    if(daq.daq_initialized>=0)
    {
      uhal::HwInterface hw=devices.front();
      hw.getNode("TRIG_DELAY").write(SW_DAQ_DELAY*daq.sw_DAQ_delay | HW_DAQ_DELAY*daq.hw_DAQ_delay);
      hw.dispatch();
    }
  }
  else if(strcmp(button_type,"TE_pos_value")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.TE_pos);
    if(daq.daq_initialized>=0)
    {
      uhal::HwInterface hw=devices.front();
      hw.getNode("RESYNC_IDLE").write(TE_ENABLE*daq.TE_enable | TE_COMMAND*daq.TE_command | TE_POS*daq.TE_pos | RESYNC_IDLE_PATTERN*daq.resync_idle_pattern);
      hw.dispatch();
      get_CRC_errors(1);
      get_n_orbit_samples();
    }
  }
  else if(strcmp(button_type,"TE_command_value")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.TE_command);
    if(daq.daq_initialized>=0)
    {
      uhal::HwInterface hw=devices.front();
      hw.getNode("RESYNC_IDLE").write(TE_ENABLE*daq.TE_enable | TE_COMMAND*daq.TE_command | TE_POS*daq.TE_pos | RESYNC_IDLE_PATTERN*daq.resync_idle_pattern);
      hw.dispatch();
      get_CRC_errors(1);
      get_n_orbit_samples();
    }
  }
  else if(strcmp(button_type,"TP_delay")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.TP_delay);
    if(daq.TP_delay<0)daq.TP_delay=0;
    sprintf(text,"%d",daq.TP_delay);
    gtk_entry_set_text((GtkEntry*)button,text);
    if(daq.daq_initialized>=0)
    {
      uhal::HwInterface hw=devices.front();
      hw.getNode("CALIB_CTRL").write((daq.TP_delay<<16) | (daq.TP_width&0xffff));
      hw.dispatch();
    }
  }
  else if(strcmp(button_type,"TP_width")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.TP_width);
    if(daq.TP_width<=1)daq.TP_width=1;
    if(daq.TP_width>255)daq.TP_width=255;
    sprintf(text,"%d",daq.TP_width);
    gtk_entry_set_text((GtkEntry*)button,text);
    if(daq.daq_initialized>=0)
    {
      uhal::HwInterface hw=devices.front();
// For LiTE-DTU version < 2.0
      hw.getNode("CALIB_CTRL").write((daq.TP_delay<<16) | (daq.TP_width&0xffff));
      hw.dispatch();
// For LiTE-DTU version >= 2.0
      for(Int_t i=0; i<5; i++)
      {
        Int_t num=asic.DTU_number[i];
        if(num<0)continue;
        device_number=daq.I2C_LiTEDTU_type*1000+(num<<daq.I2C_shift_dev_number)+2;      // DTU sub-address
        iret=I2C_RW(hw, device_number, 25, daq.TP_width, 0, 1, daq.debug_I2C);
      }
    }
  }
  else if(strcmp(button_type,"TP_level")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.TP_level);
    if(daq.TP_level<0)daq.TP_level=0;
    if(daq.TP_level>4095)daq.TP_level=4095;
    sprintf(text,"%d",daq.TP_level);
    gtk_entry_set_text((GtkEntry*)button,text);
    for(Int_t i=0; i<daq.n_active_channel; i++)
    {
      Int_t ich=daq.channel_number[i];
      sprintf(widget_name,"%d_CATIA_DAC1_val",ich+1);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_entry_set_text((GtkEntry*)widget,text);
      gtk_widget_activate((GtkWidget*)widget);
      sprintf(widget_name,"%d_CATIA_DAC2_val",ich+1);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_entry_set_text((GtkEntry*)widget,text);
      gtk_widget_activate((GtkWidget*)widget);
    }
  }
  else if(strcmp(button_type,"TP_step")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.TP_step);
    if(daq.TP_step==0)daq.TP_step=1;
    sprintf(text,"%d",daq.TP_step);
    gtk_entry_set_text((GtkEntry*)button,text);
  }
  else if(strcmp(button_type,"n_TP_step")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.n_TP_step);
    if(daq.n_TP_step<=1)daq.n_TP_step=1;
    sprintf(text,"%d",daq.n_TP_step);
    gtk_entry_set_text((GtkEntry*)button,text);
  }
  else if(strcmp(button_type,"CATIA_Vref_val")==0)
  {
    Int_t Vref_old=asic.CATIA_Vref_val[channel_number-1];
    asic.CATIA_Vref_val[channel_number-1]=rint(gtk_spin_button_get_value((GtkSpinButton*)button));
    if(asic.CATIA_Vref_val[channel_number-1]==1)
    {
      if(Vref_old==0)asic.CATIA_Vref_val[channel_number-1]=2;
      else           asic.CATIA_Vref_val[channel_number-1]=0;
    }
    gtk_spin_button_set_value((GtkSpinButton*)button,(double)asic.CATIA_Vref_val[channel_number-1]);
    update_CATIA_reg(channel_number-1,6);
  }
  else if(strcmp(button_type,"CATIA_ped_G10")==0)
  {
    asic.CATIA_ped_G10[channel_number-1]=rint(gtk_spin_button_get_value((GtkSpinButton*)button));
    if(asic.CATIA_ped_G10[channel_number-1]==32 && asic.CATIA_version[channel_number-1]<14)
      asic.CATIA_ped_G10[channel_number-1]=0;
    if(asic.CATIA_ped_G10[channel_number-1]==63 && asic.CATIA_version[channel_number-1]<14)
      asic.CATIA_ped_G10[channel_number-1]=31;
    gtk_spin_button_set_value((GtkSpinButton*)button,(double)asic.CATIA_ped_G10[channel_number-1]);
    update_CATIA_reg(channel_number-1,3);
// Reset CRC error counters after pedestal setting
    if(daq.daq_initialized>=0) get_CRC_errors(1);
  }
  else if(strcmp(button_type,"CATIA_ped_G1")==0)
  {
    asic.CATIA_ped_G1[channel_number-1]=rint(gtk_spin_button_get_value((GtkSpinButton*)button));
    if(asic.CATIA_ped_G1[channel_number-1]==32 && asic.CATIA_version[channel_number-1]<14)
      asic.CATIA_ped_G1[channel_number-1]=0;
    if(asic.CATIA_ped_G1[channel_number-1]==63  && asic.CATIA_version[channel_number-1]<14)
      asic.CATIA_ped_G1[channel_number-1]=31;
    gtk_spin_button_set_value((GtkSpinButton*)button,(double)asic.CATIA_ped_G1[channel_number-1]);
    update_CATIA_reg(channel_number-1,3);
  }
  else if(strcmp(button_type,"CATIA_DAC1_val")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&asic.CATIA_DAC1_val[channel_number-1]);
    update_CATIA_reg(channel_number-1,4);
  }
  else if(strcmp(button_type,"CATIA_DAC2_val")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&asic.CATIA_DAC2_val[channel_number-1]);
    update_CATIA_reg(channel_number-1,5);
  }
  else if(strcmp(button_type,"DTU_BL_G10")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&asic.DTU_BL_G10[channel_number-1]);
    if(asic.DTU_BL_G10[channel_number-1]<0)asic.DTU_BL_G10[channel_number-1]=0;
    if(asic.DTU_BL_G10[channel_number-1]>255)asic.DTU_BL_G10[channel_number-1]=255;
    if(daq.debug_GTK)printf("New G10 baseline for channel %d : %d\n",channel_number,asic.DTU_BL_G10[channel_number-1]);
    sprintf(text,"%4d",asic.DTU_BL_G10[channel_number-1]);
    gtk_entry_set_text((GtkEntry*)button,text);
    update_LiTEDTU_reg(channel_number-1,5);
    if(asic.DTU_version[channel_number-1]>=20) // Update saturation valer accordingly 
    {
      update_LiTEDTU_reg(channel_number-1,7);
      update_LiTEDTU_reg(channel_number-1,8);
    }
    else
    {
      update_LiTEDTU_reg(channel_number-1,17);
      update_LiTEDTU_reg(channel_number-1,18);
    }
// Reset CRC error counters after pedestal setting
    if(daq.daq_initialized>=0) get_CRC_errors(1);
  }
  else if(strcmp(button_type,"DTU_BL_G1")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&asic.DTU_BL_G1[channel_number-1]);
    if(asic.DTU_BL_G1[channel_number-1]<0)asic.DTU_BL_G1[channel_number-1]=0;
    if(asic.DTU_BL_G1[channel_number-1]>255)asic.DTU_BL_G1[channel_number-1]=255;
    sprintf(text,"%4d",asic.DTU_BL_G1[channel_number-1]);
    if(daq.debug_GTK)printf("New G1 baseline for channel %d : %d\n",channel_number,asic.DTU_BL_G1[channel_number-1]);
    gtk_entry_set_text((GtkEntry*)button,text);
    update_LiTEDTU_reg(channel_number-1,6);
  }
  else if(strcmp(button_type,"DTU_PLL")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&asic.DTU_PLL_conf[channel_number-1]);
    asic.DTU_PLL_conf[channel_number-1] &= 0x01ff;
    asic.DTU_PLL_conf2[channel_number-1]= asic.DTU_PLL_conf[channel_number-1]&0x7;
    asic.DTU_PLL_conf1[channel_number-1]= asic.DTU_PLL_conf[channel_number-1]>>3;
    if(daq.daq_initialized>=0)
    {
      uhal::HwInterface hw=devices.front();
      Int_t num=asic.DTU_number[channel_number-1];
      device_number=daq.I2C_LiTEDTU_type*1000+(num<<daq.I2C_shift_dev_number)+2;      // DTU sub-address
      if(asic.DTU_version[channel_number-1]<20)
      {
        iret=I2C_RW(hw, device_number, 9, (asic.DTU_PLL_conf[channel_number-1]&0xFF), 0, 1, daq.debug_I2C);
        iret=I2C_RW(hw, device_number, 8, 0, 0, 2, daq.debug_I2C);
        iret&=0xFF;
        iret=I2C_RW(hw, device_number, 8, (iret&0xFE)|(((asic.DTU_PLL_conf[channel_number-1]&0x100)>>8)&1), 0, 1, daq.debug_I2C);
      }
      else
      {
        iret=I2C_RW(hw, device_number, 16, (asic.DTU_PLL_conf[channel_number-1]&0xFF), 0, 1, daq.debug_I2C);
        iret=I2C_RW(hw, device_number, 15, 0, 0, 2, daq.debug_I2C);
        iret&=0xFF;
        iret=I2C_RW(hw, device_number, 15, (iret&0xFE)|(((asic.DTU_PLL_conf[channel_number-1]&0x100)>>8)&1), 0, 1, daq.debug_I2C);
      }
    }
  }
  else if(strcmp(button_type,"Rshunt_V2P5")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.Rshunt_V2P5);
    printf("%f\n",daq.Rshunt_V2P5);
    get_consumption();
  }
  else if(strcmp(button_type,"Rshunt_V1P2")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%lf",&daq.Rshunt_V1P2);
    printf("%f\n",daq.Rshunt_V1P2);
    get_consumption();
  }
  else if(strcmp(button_type,"ReSync_data")==0)
  {
// Retreive ReSync data and load it in FEAD board;
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.ReSync_data);
    sprintf(text,"%x",daq.ReSync_data);
    gtk_entry_set_text((GtkEntry*)button,text);
    uhal::HwInterface hw=devices.front();
    hw.getNode("DTU_RESYNC").write(daq.ReSync_data);
    hw.dispatch();
  }
  //else if(strcmp(button_type,"comment")==0)
  //{
  //  strcpy(daq.comment, gtk_entry_get_text((GtkEntry*)button));
  //}
}

UInt_t update_LiTEDTU_reg(Int_t ich, Int_t reg)
{
  UInt_t iret=0;
  if(daq.daq_initialized>=0 && ich>=0 && ich<5)
  {
    uhal::HwInterface hw=devices.front();
    UInt_t I2C_data=0;
    Int_t num=asic.DTU_number[ich];
    Int_t device_number=daq.I2C_LiTEDTU_type*1000+(num<<daq.I2C_shift_dev_number)+2;      // DTU sub-address
    if(reg==0)
    {
      // Enable outputs
      I2C_data=0x80;
      if(asic.DTU_clk_out[ich])I2C_data|=0x50;
      if(daq.DTU_test_mode==1)
        I2C_data|=0x0F;
      else
        I2C_data|=0x01;
      iret=I2C_RW(hw, device_number, 0, I2C_data, 0, 3, daq.debug_I2C);
      printf("1- ch %d, DTU test mode : %d, Reg0 content : W 0x%2.2x, R 0x%2.2x\n",ich, daq.DTU_test_mode, I2C_data, iret&0xff);
    }
    else if(reg==1)
    {
  // ADCs ON and Force/free gain, G1 window :
      I2C_data=0x03; // ADC H&L ON 
      if(daq.MEM_mode) I2C_data|=0x20; // MEM is using 80 MHz mode
      printf("Update register 1 of DTU %d with 0x%2.2x\n",ich+1,I2C_data);
      if(asic.DTU_force_G1[ich])
      {
        iret=I2C_RW(hw, device_number, reg, I2C_data|0xc0, 0, 3, daq.debug_I2C);
      }
      else if(asic.DTU_force_G10[ich])
      {
        iret=I2C_RW(hw, device_number, reg, I2C_data|0x40, 0, 3, daq.debug_I2C);
      }
      else if(asic.DTU_G1_window16[ich]==1)
      {
        iret=I2C_RW(hw, device_number, reg, I2C_data|0x80, 0, 3, daq.debug_I2C);
      }
      else
      {
        iret=I2C_RW(hw, device_number, reg, I2C_data, 0, 3, daq.debug_I2C);
      }
      printf("Reg 1 content 0x%2.2x\n",iret&0xff);
    }
    else if(reg==5) // G10 baseline
    {
      iret=I2C_RW(hw, device_number, reg, asic.DTU_BL_G10[ich], 0, 3, daq.debug_I2C);
    }
    else if(reg==6) // G10 baseline
    {
      iret=I2C_RW(hw, device_number, reg, asic.DTU_BL_G1[ich], 0, 3, daq.debug_I2C);
    }
    else if(reg==7 && asic.DTU_version[ich]>=20)
    {
      I2C_data=0xff0-asic.DTU_BL_G10[ich]; // switch gain at 4080 - substrated baseline
      iret=I2C_RW(hw, device_number, reg, I2C_data&0xff, 0, 3, daq.debug_I2C);
    }
    else if(reg==8 && asic.DTU_version[ich]>=20)
    {
      I2C_data=0xff0-asic.DTU_BL_G10[ich]; // switch gain at 4080 - substrated baseline
      iret=I2C_RW(hw, device_number, reg, (I2C_data>>8)&0xf, 0, 3, daq.debug_I2C);
    }
    else if(reg==17 && asic.DTU_version[ich]<20)
    {
      I2C_data=0xff0-asic.DTU_BL_G10[ich]; // switch gain at 4080 - substrated baseline
      iret=I2C_RW(hw, device_number, reg, I2C_data&0xff, 0, 3, daq.debug_I2C);
    }
    else if(reg==18 && asic.DTU_version[ich]<20)
    {
      I2C_data=0xff0-asic.DTU_BL_G10[ich]; // switch gain at 4080 - substrated baseline
      iret=I2C_RW(hw, device_number, reg, (I2C_data>>8)&0xf, 0, 3, daq.debug_I2C);
    }
    else if(reg==15 && asic.DTU_version[ich]<20) // CapBank_Override - V1.2
    {
      iret=I2C_RW(hw, device_number, reg, 0, 0, 2, daq.debug_I2C);
      iret=(iret&0xfd) | (asic.DTU_override_Vc_bit[ich]<<1);
      iret=I2C_RW(hw, device_number, reg, iret, 0, 3, daq.debug_I2C);
    }
    else if(reg==15 && asic.DTU_version[ich]>=20)
    {
      iret=I2C_RW(hw, device_number, reg, 0, 0, 2, daq.debug_I2C);
      iret&=0xFF;
      iret=I2C_RW(hw, device_number, reg, (iret&0xFE)|(((asic.DTU_PLL_conf[ich]&0x100)>>8)&1), 0, 1, 0);
    }
    else if(reg==16 && asic.DTU_version[ich]>=20)
    {
      iret=I2C_RW(hw, device_number, reg, asic.DTU_PLL_conf[ich]&0xFF, 0, 3, daq.debug_I2C);
    }
    else if(reg==8 && asic.DTU_version[ich]<20)
    {
      iret=I2C_RW(hw, device_number, reg, 0, 0, 2, daq.debug_I2C);
      iret&=0xFF;
      iret=I2C_RW(hw, device_number, reg, (iret&0xFE)|(((asic.DTU_PLL_conf[ich]&0x100)>>8)&1), 0, 1, 0);
    }
    else if(reg==9 && asic.DTU_version[ich]<20)
    {
      iret=I2C_RW(hw, device_number, reg, asic.DTU_PLL_conf[ich]&0xFF, 0, 3, daq.debug_I2C);
    }
    else if(reg==17 && asic.DTU_version[ich]>=20)
    {
      iret=I2C_RW(hw, device_number, reg, 0, 0, 2, daq.debug_I2C);
      iret=(iret&0xf4) | (asic.DTU_force_PLL[ich]<<3) | (1<<2) | (asic.DTU_VCO_rail_mode[ich]<<1) | (asic.DTU_override_Vc_bit[ich]<<0);
      iret=I2C_RW(hw, device_number, reg, iret, 0, 3, daq.debug_I2C);
    }
    else if(reg==25 && asic.DTU_version[ich]>=20)
    {
      iret=I2C_RW(hw, device_number, reg, daq.TP_width, 0, 3, daq.debug_I2C);
    }
  }
  return iret;
}

UInt_t update_CATIA_reg(Int_t ich, Int_t reg)
{
  UInt_t iret=0;
  if(daq.daq_initialized>=0 && ich>=0 && ich<5)
  {
    uhal::HwInterface hw=devices.front();
    UInt_t I2C_data=0, I2C_long=0;
    
    Int_t num=asic.CATIA_number[ich];
    if(num<=0)return -1;
    Int_t device_number=daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+3;
    if(ich==4 && daq.I2C_shift_dev_number==4)   device_number = daq.I2C_CATIA_type*1000+((ich+1)<<daq.I2C_shift_dev_number)+0xb;

    Int_t loc_DAC1_status=0, loc_DAC2_status=0;
    if(asic.CATIA_DAC1_status[ich])loc_DAC1_status=1;
    if(asic.CATIA_DAC2_status[ich])loc_DAC2_status=1;

    if(reg==1)
    {
      I2C_data=(asic.CATIA_temp_X5[ich]<<4) | (asic.CATIA_temp_out[ich]<<3) | (asic.CATIA_SEU_corr[ich]<<1);
      I2C_long=0;
    }
    else if(reg==3)
    {
// Set pedestal values, gain and LPF
      if(asic.CATIA_version[ich]>=14)
        I2C_data=asic.CATIA_gain[ich]<<14 | asic.CATIA_ped_G1[ich]<<8 | asic.CATIA_ped_G10[ich]<<2 | asic.CATIA_LPF35[ich]<<1 | 1;
      else
        I2C_data=asic.CATIA_ped_G1[ich]<<8 | asic.CATIA_ped_G10[ich]<<3 | asic.CATIA_gain[ich]<<2 | asic.CATIA_LPF35[ich]<<1 | 1;
      I2C_long=1;
    }
    else if(reg==4)
    {
      I2C_data=asic.CATIA_DAC_buf_off[ich]<<15 | loc_DAC2_status<<13 | loc_DAC1_status<<12 | asic.CATIA_DAC1_val[ich];
      I2C_long=1;
    }
    else if(reg==5)
    {
      I2C_data=(1-asic.CATIA_Vref_out[ich])<<14;;
      Int_t Vcal_out=0;
      if(daq.CATIA_Vcal_out)Vcal_out=3;
      I2C_data |= Vcal_out<<12 | asic.CATIA_DAC2_val[ich];
      I2C_long=1;
    }
    else if(reg==6)
    {
      I2C_data=asic.CATIA_Vref_val[ich]<<4 | asic.CATIA_Rconv[ich]<<2 | 0xb;
      I2C_long=0;
    }
    if(reg>0 && reg<=6)
    {
      iret=I2C_RW(hw, device_number, reg, I2C_data, I2C_long, 3, daq.debug_I2C);
      asic.CATIA_reg_loc[ich][reg]=I2C_data;
    }
  }
  return iret;
}

void enable_channel(int channel_number, bool enable)
{
  char widget_name[80];
  bool enable_CATIA=enable;
  bool enable_DTU=enable;
  if(enable && asic.CATIA_number[channel_number-1]<0)enable_CATIA=FALSE;
  if(enable && asic.DTU_number[channel_number-1]<0)enable_DTU=FALSE;

  sprintf(widget_name,"%1.1d_CATIA_version",channel_number);
  GtkWidget *widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_widget_set_sensitive(widget,enable_CATIA);
  sprintf(widget_name,"%1.1d_CATIA_Vref_val",channel_number);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  if(daq.CATIA_scan_Vref)
    gtk_widget_set_sensitive(widget,enable_CATIA);
  else
    gtk_widget_set_sensitive(widget,FALSE);
  sprintf(widget_name,"%1.1d_CATIA_ped_G10",channel_number);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_widget_set_sensitive(widget,enable_CATIA);
  sprintf(widget_name,"%1.1d_CATIA_ped_G1",channel_number);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_widget_set_sensitive(widget,enable_CATIA);
  sprintf(widget_name,"%1.1d_CATIA_gain",channel_number);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_widget_set_sensitive(widget,enable_CATIA);
  sprintf(widget_name,"%1.1d_CATIA_LPF35",channel_number);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_widget_set_sensitive(widget,enable_CATIA);
  sprintf(widget_name,"%1.1d_CATIA_DAC1_status",channel_number);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_widget_set_sensitive(widget,enable_CATIA);
  sprintf(widget_name,"%1.1d_CATIA_DAC1_val",channel_number);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_widget_set_sensitive(widget,enable_CATIA);
  sprintf(widget_name,"%1.1d_CATIA_DAC2_status",channel_number);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_widget_set_sensitive(widget,enable_CATIA);
  sprintf(widget_name,"%1.1d_CATIA_DAC2_val",channel_number);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_widget_set_sensitive(widget,enable_CATIA);
  sprintf(widget_name,"%1.1d_CATIA_Rconv",channel_number);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_widget_set_sensitive(widget,enable_CATIA);
  sprintf(widget_name,"%1.1d_CATIA_temp",channel_number);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_widget_set_sensitive(widget,enable_CATIA);
  sprintf(widget_name,"%1.1d_CATIA_SEU",channel_number);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_widget_set_sensitive(widget,enable_CATIA);
  sprintf(widget_name,"%1.1d_CATIA_SEU_correction",channel_number);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_widget_set_sensitive(widget,enable_CATIA);

  sprintf(widget_name,"%1.1d_DTU_version",channel_number);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_widget_set_sensitive(widget,enable_DTU);
  sprintf(widget_name,"%1.1d_DTU_BL_G10",channel_number);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_widget_set_sensitive(widget,enable_DTU);
  sprintf(widget_name,"%1.1d_DTU_BL_G1",channel_number);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_widget_set_sensitive(widget,enable_DTU);
  sprintf(widget_name,"%1.1d_DTU_PLL",channel_number);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_widget_set_sensitive(widget,enable_DTU);
  sprintf(widget_name,"%1.1d_DTU_force_G10",channel_number);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_widget_set_sensitive(widget,enable_DTU);
  sprintf(widget_name,"%1.1d_DTU_force_G1",channel_number);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_widget_set_sensitive(widget,enable_DTU);
  sprintf(widget_name,"%1.1d_DTU_force_PLL",channel_number);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  gtk_widget_set_sensitive(widget,enable_DTU);

}

void update_trigger(void)
{
  if(daq.daq_initialized>=0)
  {
    printf("Update trigger settings :\n");
    printf("calib pulse            : %d\n",daq.calib_pulse_enabled);
    printf("calib mux              : %d\n",daq.calib_mux_enabled);
    printf("self trigger           : %d\n",daq.self_trigger);
    printf("self trigger loop      : %d\n",daq.self_trigger_loop);
    printf("self trigger mode      : %d\n",daq.self_trigger_mode);
    printf("self trigger threshold : %d\n",daq.self_trigger_threshold);
    printf("self trigger mask      : %2.2x\n",daq.self_trigger_mask);
  // Switch to triggered mode + external trigger :
    UInt_t command=
               CALIB_PULSE_ENABLED   *daq.calib_pulse_enabled              |
               CALIB_MUX_ENABLED     *daq.calib_mux_enabled                |
               SELF_TRIGGER_MODE     *daq.self_trigger_mode                |
              (SELF_TRIGGER_MASK     *(daq.self_trigger_mask&0x1F))        |
              (SELF_TRIGGER_THRESHOLD*(daq.self_trigger_threshold&0xFFF))  |
               SELF_TRIGGER          *daq.self_trigger                     |
               SELF_TRIGGER_LOOP     *daq.self_trigger_loop                |
               FIFO_MODE             *(daq.fifo_mode&1)                    |
               RESET                 *0;
    uhal::HwInterface hw=devices.front();
    hw.getNode("FEAD_CTRL").write(command);
    hw.getNode("CLK_SETTING").write(RESYNC_PHASE*daq.resync_phase);
    //printf("resync phase : %d\n",daq.resync_phase);
    hw.dispatch();
  }
}

