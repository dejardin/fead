#define EXTERN extern
#include "gdaq_VFE.h"
void reset_DTU_sync(void)
{
  uhal::HwInterface hw=devices.front();
  UInt_t device_number, val, iret;
  UInt_t command=
	CALIB_MUX_ENABLED      *daq.calib_mux_enabled               |
	CALIB_PULSE_ENABLED    *daq.calib_pulse_enabled             |
	SELF_TRIGGER_MODE      *daq.self_trigger_mode               |
	(SELF_TRIGGER_MASK     *(daq.self_trigger_mask&0x1F))       |
	(SELF_TRIGGER_THRESHOLD*(daq.self_trigger_threshold&0xFFF)) |
	SELF_TRIGGER           *daq.self_trigger                    |
	SELF_TRIGGER_LOOP      *daq.self_trigger_loop               |
	FIFO_MODE              *(daq.fifo_mode&1)                   | // Always DAQ on trigger
	RESET                  *0;
  hw.getNode("FEAD_CTRL").write(command);
  hw.dispatch();
  command=DELAY_TAP_DIR*1 | DELAY_RESET*1;
  hw.getNode("DELAY_CTRL").write(command);
  hw.dispatch();
  printf("Get lock status of IDELAY input stages : ");
  ValWord<uint32_t> reg = hw.getNode("DELAY_CTRL").read();
  hw.dispatch();
  printf("0x%8.8x\n",reg.value());
  for(Int_t i=0; i<5; i++)
  {
    daq.delay_val[i]=0;
    daq.bitslip_val[i]=0;
    daq.byteslip_val[i]=0;
  }
  daq.selected_channel=-1;
  GtkWidget *widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_selected_channel");
  gtk_button_clicked((GtkButton*)widget);
}
