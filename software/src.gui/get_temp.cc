#define EXTERN extern
#include "gdaq_VFE.h"

void get_temp(void)
{
  uhal::HwInterface hw=devices.front();
  UInt_t device_number, command, iret;
  GtkWidget* widget;
  char widget_text[80], widget_name[80];

// Read XADC register 0x40 and set the requested average to 1 in XADC
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  ValWord<uint32_t> ave  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  unsigned loc_ave=ave.value()&0xffff;

  loc_ave=0x8000;
  command=DRP_WRb*1 | (0x40<<16) | loc_ave;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  ave  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  loc_ave=ave.value()&0xffff;
  printf("New config register 0x40 content : %x\n",loc_ave);
  command=DRP_WRb*1 | (0x42<<16) | 0x0400;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();

// Read FPGA temperature :
  Int_t XADC_Temp=0;
  Int_t average=32;
  Double_t loc_temp=0.;
  ValWord<uint32_t> temp;
  Double_t ave_val=0.;
  for(int iave=0; iave<average; iave++)
  {
    command=DRP_WRb*0 | (XADC_Temp<<16);
    hw.getNode("DRP_XADC").write(command);
    temp  = hw.getNode("DRP_XADC").read();
    hw.dispatch();
    double loc_val=double((temp.value()&0xffff)>>4)/4096.;
    ave_val+=loc_val;
  }
  loc_temp=(ave_val/average)*4096*0.123-273.;
  printf("FPGA temperature : %.2f deg\n", loc_temp);
  sprintf(widget_name,"0_FPGA_temp");
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  sprintf(widget_text,"FPGA %.1f C",loc_temp);
  gtk_label_set_text((GtkLabel*)widget,widget_text);

  for(int i=0; i<daq.n_active_channel; i++)
  {
    printf("active channel %d : CATIA %d\n",daq.channel_number[i],asic.CATIA_number[daq.channel_number[i]]);
  }
// Read CATIA temperature :
// First, Every body in High Z
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    Int_t num=asic.CATIA_number[ich];
    if(num<=0)continue;
    device_number=daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+3;
    if(ich==4 && daq.I2C_shift_dev_number==4)   device_number = daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+0xb;
    iret=I2C_RW(hw, device_number, 1, 0x02, 0, 3, daq.debug_I2C);
// Set CATIA outputs with requested levels with no Vref output
    if(asic.CATIA_version[ich]>=14)
    {
      if(daq.CATIA_Vcal_out)
        iret=I2C_RW(hw, device_number, 5, 0x7000, 1, 3, daq.debug_I2C);
      else
        iret=I2C_RW(hw, device_number, 5, 0x4000, 1, 3, daq.debug_I2C);
    }
  }
// Then present temperature voltage of each channel
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    Int_t num=asic.CATIA_number[ich];
    device_number=daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+3;
    if(ich==4 && daq.I2C_shift_dev_number==4)   device_number = daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+0xb;
    iret=I2C_RW(hw, device_number, 1, 0x0a, 0, 1, daq.debug_I2C);
    usleep(100000); // Let some time for calibration voltages to stabilize
// And read Temp line :
    XADC_Temp=0x11;
    if(daq.is_VICEPP)XADC_Temp=0x18;
    ave_val=0.;
    for(int iave=0; iave<average; iave++)
    {
      command=DRP_WRb*0 | (XADC_Temp<<16);
      hw.getNode("DRP_XADC").write(command);
      temp  = hw.getNode("DRP_XADC").read();
      hw.dispatch();
      double loc_val=double((temp.value()&0xffff)>>4)/4096.;
      ave_val+=loc_val;
    }
    ave_val/=average;
// Convert to degrees :
    loc_temp=(asic.CATIA_Temp_offset[ich]-ave_val/daq.Tsensor_divider)/0.0021;
    printf("CATIA %d temperature : %f %f\n",ich,ave_val,loc_temp);
    sprintf(widget_name,"%d_CATIA_temp",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    sprintf(widget_text,"%.1f",loc_temp);
    gtk_label_set_text((GtkLabel*)widget,widget_text);
// And put back in high Z :
    iret=I2C_RW(hw, device_number, 1, 0x02, 0, 1, daq.debug_I2C);
  }
}
