#define EXTERN extern
#include "gdaq_VFE.h"
void synchronize_links(void)
{
  uhal::HwInterface hw=devices.front();
  UInt_t device_number, iret, val, command;
  ValWord<uint32_t> reg;

// For link synchronizations, switch to test mode in VFE mode. Don't do it for MEM mode :
  Int_t DTU_test_mode=1;
  if(daq.MEM_mode)
  {
    DTU_test_mode=0;
    hw.getNode("DTU_SYNC_PATTERN").write(0x0ccccccf); // Default synchro pattern during calibration with LiTEDTU
    reg = hw.getNode("DTU_SYNC_PATTERN").read();
    hw.dispatch();
    printf("Expected Sync patterns : 0x%8.8x, test mode %d, MEM mode %d\n",reg.value(),daq.DTU_test_mode,daq.MEM_mode);
    // Put LiTE-DTUs in sync mode
    send_ReSync_command(LiTEDTU_SYNC_MODE);
    usleep(1000);
  }

  printf("Start links sync with pattern 0x%x\n",daq.eLink_active);
  UInt_t VFE_control= DELAY_AUTO_TUNE*1 | LVRB_AUTOSCAN*0 | eLINK_ACTIVE*daq.eLink_active | DTU_TEST_MODE*DTU_test_mode |
                      DTU_MEM_MODE*daq.MEM_mode | I2C_LPGBT_MODE*daq.I2C_lpGBT_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();

// Reset iDelay lines
  command=DELAY_TAP_DIR*1 | DELAY_RESET*1;
  hw.getNode("DELAY_CTRL").write(command);
  hw.dispatch();
  for(Int_t i=0; i<5; i++)
  {
    daq.delay_val[i]=0;
    daq.bitslip_val[i]=0;
    daq.byteslip_val[i]=0;
  }
  daq.selected_channel=-1;
  GtkWidget *widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_selected_channel");
  gtk_button_clicked((GtkButton*)widget);

// Launch links synchronization :
  command=DELAY_TAP_DIR*1 | START_IDELAY_SYNC*0x1fu;
  hw.getNode("DELAY_CTRL").write(command);
  hw.dispatch();
  usleep(1000);
  command=DELAY_TAP_DIR*1;
  hw.getNode("DELAY_CTRL").write(command);
  hw.dispatch();
  reg = hw.getNode("DELAY_CTRL").read();
  hw.dispatch();
  printf("Get sync status : 0x%8.8x, 0x%2.2x 0x%2.2x 0x%2.2x 0x%2.2x 0x%2.2x\n", reg.value(),
         (reg.value()>>0)&0x3f,(reg.value()>>6)&0x3f,(reg.value()>>12)&0x3f,(reg.value()>>18)&0x3f,(reg.value()>>24)&0x3f);
  reg = hw.getNode("RESYNC_IDLE").read();
  hw.dispatch();
  UInt_t PLL_lock=(reg.value()>>DTU_PLL_LOCK)&0x1f;
  printf("PLL lock values : 0x%x",PLL_lock);
  for(Int_t i=0; i<5; i++)printf(", %d",(PLL_lock>>i)&1);
  printf("\n");


// Restore CAL mux in normal position after calibration
  if(daq.MEM_mode)
  {
    // Put LiTE-DTUs in normal mode
    send_ReSync_command(LiTEDTU_NORMAL_MODE);
  }
  VFE_control= DELAY_AUTO_TUNE*daq.delay_auto_tune | LVRB_AUTOSCAN*daq.LVRB_autoscan |
               PED_MUX*daq.CATIA_Vcal_out | eLINK_ACTIVE*daq.eLink_active |
               DTU_TEST_MODE*daq.DTU_test_mode | DTU_MEM_MODE*daq.MEM_mode | I2C_LPGBT_MODE*daq.I2C_lpGBT_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();
}
