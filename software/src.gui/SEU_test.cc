#define EXTERN extern
#include "gdaq_VFE.h"
#include <TRandom.h>

void SEU_test(void)
{
  TRandom *tr=new TRandom();
  GtkWidget* widget;
  char widget_name[80], widget_text[80];
  SEU.test_running = TRUE;
  daq.fout_SEU=NULL;
  GtkTextBuffer *buffer;
  GtkTextIter start, end;

  Int_t XADC_FPGA_Temp=0;
  Int_t XADC_CATIA_Temp=0x11;
  if(daq.is_VICEPP)XADC_CATIA_Temp=0x18;

  ValWord<uint32_t> fault,current, voltage;
  uhal::HwInterface hw=devices.front();

  for(Int_t i=0; i<5; i++)
  {
    SEU.n_SEU[i]=0;
    sprintf(widget_name,"%d_CATIA_SEU",i+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    gtk_widget_set_sensitive(widget,TRUE);
    sprintf(widget_name,"%d_CATIA_I2C_error",i+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    gtk_widget_set_sensitive(widget,TRUE);
  }

// CATIA settings if requested
  SEU.n_reg               = 6;
// CATIA registers to address
  for(Int_t i=0; i<6; i++) SEU.reg_number[i]=i+1;
  SEU.reg_number[6]       = -1;
  Int_t delay             = 10;
  Int_t I2C_rw            = 2;                 // 2*read+write for the test loops
  Double_t I_margin       = (double)daq.SEU_current_limit;              // 10 mA margin on current to generate alert
  SEU.n_SEL[0]=0;
  SEU.n_SEL[1]=0;
  UInt_t device_number, I2C_long, I2C_mask;
  UInt_t command, iret;
  Int_t SEU_simul_counter[5]={0};

  Int_t I2P5_margin_lsb   = int(I_margin*4096/102.4*daq.Rshunt_V2P5);
  Int_t I1P2_margin_lsb   = int(I_margin*4096/102.4*daq.Rshunt_V1P2);
  
  FILE *frun=NULL;
  frun=fopen(".last_SEU_run_number","r");
  daq.numrun=1;
  if(frun==NULL)
  {
    printf("Last_run_number not found. Start with run 1\n");
  }
  else
  {
    Int_t last_run=0;
    Int_t eof=fscanf(frun,"%d",&last_run);
    if(eof==1)daq.numrun=last_run+1;
    fclose(frun);
    frun=fopen(".last_SEU_run_number","w");
    fprintf(frun,"%d",daq.numrun);
    fclose(frun);
  }
  char fname[80];
  sprintf(fname,"data/SEU_tests/run%6.6d.log",daq.numrun);
  daq.fout_SEU=fopen(fname,"w+");
  if(daq.fout_SEU==NULL)
  {
    printf("Cannot create log file %s, stop here\n",fname);
    return;
  }

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0-comment");
  g_assert (widget);
  buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(widget));
  g_assert (buffer);
  gtk_text_buffer_get_start_iter(buffer, &start);
  gtk_text_buffer_get_end_iter(buffer, &end);
  gchar *comment= gtk_text_buffer_get_text (buffer, &start, &end,FALSE);
  strcpy(daq.comment,comment);

  gettimeofday(&tv,NULL);
  fprintf(daq.fout_SEU,"%ld.%6.6ld : start of run %d\n",tv.tv_sec,tv.tv_usec,daq.numrun);
  printf("%ld.%6.6ld : start of run %d\n",tv.tv_sec,tv.tv_usec,daq.numrun);
  //fprintf(daq.fout_SEU,"%ld.%6.6ld : %s\n",tv.tv_sec,tv.tv_usec,comment);
  //printf("%ld.%6.6ld : %s\n",tv.tv_sec,tv.tv_usec,comment);

  fprintf(daq.fout_SEU,"%ld.%6.6ld : Comment :\n%s\n",tv.tv_sec,tv.tv_usec,daq.comment);
  printf("%ld.%6.6ld : Comment :\n%s\n",tv.tv_sec,tv.tv_usec,daq.comment);

  printf("%ld.%6.6ld : FEAD board number     : %d\n",tv.tv_sec,tv.tv_usec,daq.FEAD_number);
  fprintf(daq.fout_SEU,"%ld.%6.6ld : FEAD board      : %d\n",tv.tv_sec,tv.tv_usec,daq.FEAD_number);
  printf("%ld.%6.6ld : Firmware version      : %8.8x\n",tv.tv_sec,tv.tv_usec,daq.firmware_version);
  fprintf(daq.fout_SEU,"%ld.%6.6ld : Firmware version      : %8.8x\n",tv.tv_sec,tv.tv_usec,daq.firmware_version);

  printf("%ld.%6.6ld : Start SEU tests on channel %d\n",tv.tv_sec,tv.tv_usec,daq.selected_channel+1);
  fprintf(daq.fout_SEU,"%ld.%6.6ld : Start SEU tests on channel %d\n",tv.tv_sec,tv.tv_usec,daq.selected_channel+1);

  //hw.getNode("FEAD_CTRL").write(RESET*1);
  //hw.getNode("FEAD_CTRL").write(RESET*0);
  //hw.dispatch();

// Read register contents if we need them ("keep" mode):
  for(Int_t i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    Int_t num=asic.CATIA_number[ich];
    if(num<=0)continue;
    device_number=daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+3;
    if(ich==4 && daq.I2C_shift_dev_number==4)   device_number = daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+0xb;
    for(Int_t j=0; j<SEU.n_reg; j++)
    {
      Int_t ireg=SEU.reg_number[j];
      I2C_long=0;
      I2C_mask=0xFF;
      if(ireg==3 || ireg==4 || ireg==5) {I2C_long=1; I2C_mask=0xFFFF;}
      if(daq.SEU_pattern==6)      // "keep" mode
      {
        asic.CATIA_reg_loc[ich][ireg]=I2C_RW(hw, device_number, ireg, 0, I2C_long, 2, daq.debug_I2C)&I2C_mask;
      }
    }
  }

// Reset VFE board to reset CATIAs Register 2 content :
  printf("%ld.%6.6ld : Generate PowerUp reset 0x%8.8x\n",tv.tv_sec,tv.tv_usec,1*PWUP_RESETB);
  fprintf(daq.fout_SEU,"%ld.%6.6ld : Generate PowerUp reset 0x%8.8x\n",tv.tv_sec,tv.tv_usec,1*PWUP_RESETB);
  hw.getNode("VFE_CTRL").write(1*PWUP_RESETB);
  hw.getNode("VFE_CTRL").write(0*PWUP_RESETB);
  hw.dispatch();

// Setup default and reference values for CATIAs :
  for(Int_t i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    Int_t num=asic.CATIA_number[ich];
    if(num<=0)continue;
    device_number=daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+3;
    if(ich==4 && daq.I2C_shift_dev_number==4)   device_number = daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+0xb;

    asic.CATIA_temp_X5[ich]=0;
    asic.CATIA_temp_out[ich]=0;
    if(ich==daq.selected_channel)asic.CATIA_temp_out[ich]=1;
    for(Int_t j=0; j<SEU.n_reg; j++)
    {
      Int_t ireg=SEU.reg_number[j];
      I2C_long=0;
      I2C_mask=0xFF;
      if(ireg==3 || ireg==4 || ireg==5) {I2C_long=1; I2C_mask=0xFFFF;}
      if(daq.SEU_pattern==0)      // "All 0" mode
      {
        asic.CATIA_reg_loc[ich][ireg]=0;
      }
      else if(daq.SEU_pattern==1)      // "All 1" mode
      {
        asic.CATIA_reg_loc[ich][ireg]=0xFFFF&I2C_mask;
      }
      else if(daq.SEU_pattern==2)      // "Comb 10" mode
      {
        asic.CATIA_reg_loc[ich][ireg]=0x5555&I2C_mask;
      }
      else if(daq.SEU_pattern==3)      // "Comb 01" mode
      {
        asic.CATIA_reg_loc[ich][ireg]=0xAAAA&I2C_mask;
      }
      else if(daq.SEU_pattern==4)      // "default" mode
      {
        iret=I2C_RW(hw, device_number, ireg, 0, I2C_long, 2, daq.debug_I2C); // Register content after reset
        asic.CATIA_reg_loc[ich][ireg]=iret&I2C_mask;
      }
      else if(daq.SEU_pattern==5)      // "xml" mode
      {
        asic.CATIA_reg_loc[ich][ireg]=asic.CATIA_reg_def[ich][ireg];
      }
      if(ireg==1)asic.CATIA_reg_loc[ich][ireg]=
                (asic.CATIA_reg_loc[ich][ireg]&0xE5)|(0x08*asic.CATIA_temp_out[ich])
                                                    |(0x02*asic.CATIA_SEU_corr[ich]);
      iret=I2C_RW(hw, device_number, ireg, asic.CATIA_reg_loc[ich][ireg], I2C_long, 1, daq.debug_I2C);
    }
  }

  printf("%ld.%6.6ld : Will ",tv.tv_sec,tv.tv_usec);
  fprintf(daq.fout_SEU,"%ld.%6.6ld : Will ",tv.tv_sec,tv.tv_usec);
  if((I2C_rw&2)>0)
  {
    printf("read");
    fprintf(daq.fout_SEU,"read");
  }
  if((I2C_rw&1)>0)
  {
    printf("/write");
    fprintf(daq.fout_SEU,"/write");
  }
  printf(" %d catias : ",daq.n_active_channel);
  fprintf(daq.fout_SEU," %d catias : ",daq.n_active_channel);
  for(Int_t i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    printf("%d ",ich);
    fprintf(daq.fout_SEU,"%d ",ich+1);
    for(Int_t ireg=0; ireg<=6; ireg++)SEU.error[ich][ireg]=0;

    sprintf(widget_name,"%d_CATIA_I2C_error",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    gtk_label_set_text((GtkLabel*)widget,"0");
  }
  while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ

  printf("\n%ld.%6.6ld : Will access %d registers : ",tv.tv_sec,tv.tv_usec,SEU.n_reg);
  fprintf(daq.fout_SEU,"\n%ld.%6.6ld : Will access %d registers : ",tv.tv_sec,tv.tv_usec,SEU.n_reg);
  for(Int_t i=0; i<SEU.n_reg; i++)
  {
    printf("%d ",SEU.reg_number[i]);
    fprintf(daq.fout_SEU,"%d ",SEU.reg_number[i]);
  }
  printf("\n");
  fprintf(daq.fout_SEU,"\n");
  printf("\n%ld.%6.6ld : Default register contents : ",tv.tv_sec,tv.tv_usec);
  fprintf(daq.fout_SEU,"\n%ld.%6.6ld : Default register contents : ",tv.tv_sec,tv.tv_usec);
  for(Int_t i=0; i<SEU.n_reg; i++)
  {
    Int_t ich=daq.channel_number[daq.selected_channel];
    Int_t ireg=SEU.reg_number[i];
    if(ireg==3 || ireg==4 || ireg==5)
    {
      printf("%d : 0x%4.4x, ",ireg,asic.CATIA_reg_loc[ich][ireg]);
      fprintf(daq.fout_SEU,"%d : 0x%4.4x, ",ireg,asic.CATIA_reg_loc[ich][ireg]);
    }
    else
    {
      printf("%d : 0x%2.2x, ",ireg,asic.CATIA_reg_loc[ich][ireg]);
      fprintf(daq.fout_SEU,"%d : 0x%2.2x, ",ireg,asic.CATIA_reg_loc[ich][ireg]);
    }
  }
  printf("\n");
  fprintf(daq.fout_SEU,"\n");

  ValWord<uint32_t> meas, meas_V1P2,meas_V2P5, meas_CATIA_temp, meas_FPGA_temp;

// Initialize current monitors
  for(Int_t iLVR=0; iLVR<2; iLVR++)
  {
    printf("\n%ld.%6.6ld : Program LV sensor %d : ",tv.tv_sec,tv.tv_usec,iLVR);
    fprintf(daq.fout_SEU,"\n%ld.%6.6ld : Program LV sensor %d : ",tv.tv_sec,tv.tv_usec,iLVR);
    if(iLVR==0)
    {
      device_number=0x67;
      printf("V2P5\n");
      fprintf(daq.fout_SEU,"2.5V\n");
    }
    else
    {
      device_number=0x6C;
      printf("V1P2\n");
      fprintf(daq.fout_SEU,"1.2V\n");
    }

// Put LVRB in continuous ADC mode, monitor sense+, and power calculation with sense+
    Int_t ireg=0; // control register
    command=0x5;
    iret=I2C_RW(hw, device_number, ireg, command,0, 3, daq.debug_I2C);
    printf("%ld.%6.6ld : New content of control register : 0x%x\n",tv.tv_sec,tv.tv_usec,iret&0xffff);
    fprintf(daq.fout_SEU,"%ld.%6.6ld : New content of control register : 0x%x\n",tv.tv_sec,tv.tv_usec,iret&0xffff);
// Max power limit
    iret=I2C_RW(hw, device_number, 0xe,  0xff,0, 1, daq.debug_I2C);
    iret=I2C_RW(hw, device_number, 0xf,  0xff,0, 1, daq.debug_I2C);
    iret=I2C_RW(hw, device_number, 0x10, 0xff,0, 1, daq.debug_I2C);
// Min power limit
    iret=I2C_RW(hw, device_number, 0x11, 0x00,0, 1, daq.debug_I2C);
    iret=I2C_RW(hw, device_number, 0x12, 0x00,0, 1, daq.debug_I2C);
    iret=I2C_RW(hw, device_number, 0x13, 0x00,0, 1, daq.debug_I2C);
// Max ADin limit
    iret=I2C_RW(hw, device_number, 0x2E, 0xFFF0,1, 3, daq.debug_I2C);
// Min ADin limit
    iret=I2C_RW(hw, device_number, 0x30, 0x0000,1, 3, daq.debug_I2C);
// Max Vin limit
    iret=I2C_RW(hw, device_number, 0x24, 0xFFF0,1, 3, daq.debug_I2C);
// Min Vin limit
    iret=I2C_RW(hw, device_number, 0x26, 0x0000,1, 3, daq.debug_I2C);
// Max current limit
    iret=I2C_RW(hw, device_number, 0x1A, 0xFFF0,1, 3, daq.debug_I2C);
    printf("%ld.%6.6ld : New max threshold on current 0x%x\n",tv.tv_sec,tv.tv_usec,iret&0xffff);
    fprintf(daq.fout_SEU,"%ld.%6.6ld : New max threshold on current 0x%x\n",tv.tv_sec,tv.tv_usec,iret&0xffff);
// Min current limit
    iret=I2C_RW(hw, device_number, 0x1C, 0x0000,1, 3, daq.debug_I2C);
    printf("%ld.%6.6ld : New min threshold on current 0x%x\n",tv.tv_sec,tv.tv_usec,iret&0xffff);
    fprintf(daq.fout_SEU,"%ld.%6.6ld : New min threshold on current 0x%x\n",tv.tv_sec,tv.tv_usec,iret&0xffff);
// Read and clear Fault register :
    ireg=4; // Alert register
    iret=I2C_RW(hw, device_number, ireg, command,0, 2, daq.debug_I2C);
    printf("%ld.%6.6ld : Alert register content : 0x%x\n",tv.tv_sec,tv.tv_usec,iret&0xffff);
    fprintf(daq.fout_SEU,"%ld.%6.6ld : Alert register content : 0x%x\n",tv.tv_sec,tv.tv_usec,iret&0xffff);
    iret=I2C_RW(hw, device_number, ireg, command,0, 2, daq.debug_I2C);
    printf("%ld.%6.6ld : Alert register content : 0x%x\n",tv.tv_sec,tv.tv_usec,iret&0xffff);
    fprintf(daq.fout_SEU,"%ld.%6.6ld : Alert register content : 0x%x\n",tv.tv_sec,tv.tv_usec,iret&0xffff);
// Set Alert register
    ireg=1;
    command=0x20; // Alert on Dsense overflow
    iret=I2C_RW(hw, device_number, ireg, command,0, 3, daq.debug_I2C);
    printf("%ld.%6.6ld : New alert bit pattern : 0x%x\n",tv.tv_sec,tv.tv_usec, iret&0xffff);
    fprintf(daq.fout_SEU,"%ld.%6.6ld : New alert bit pattern : 0x%x\n",tv.tv_sec,tv.tv_usec, iret&0xffff);
  }
  printf("\n");
  fprintf(daq.fout_SEU,"\n");

  usleep(500000);

// Read voltages measured by controler
  for(Int_t iLVR=0; iLVR<2; iLVR++)
  {
    char alim[80];
    if(iLVR==0)
    {
      device_number=0x67;
      sprintf(alim,"V2P5");
    }
    else
    {
      device_number=0x6C;
      sprintf(alim,"V1P2");
    }
    iret=I2C_RW(hw, device_number, 0x1E, 0,1, 2, 0);
    Double_t Vmeas=((iret>>4)&0xfff);
    Vmeas=Vmeas/4096.*102.4; // Convert to V (102.4 V full scale)
    printf("%ld.%6.6ld : %s measured power supply voltage %7.3f V\n",tv.tv_sec,tv.tv_usec,alim,Vmeas);
    fprintf(daq.fout_SEU,"%ld.%6.6ld : %s measured power supply voltage %7.3f V\n",tv.tv_sec,tv.tv_usec,alim,Vmeas);
    iret=I2C_RW(hw, device_number, 0x28, 0,1, 2, 0);
    Vmeas=((iret>>4)&0xfff);
    Vmeas=Vmeas/4096.*2.048; // Convert to V (2.048 V full scale)
    if(iLVR==0)Vmeas*=2.;
    printf("%ld.%6.6ld : %s measured sense voltage %7.3f V\n",tv.tv_sec,tv.tv_usec,alim,Vmeas);
    fprintf(daq.fout_SEU,"%ld.%6.6ld : %s measured sense voltage %7.3f V\n",tv.tv_sec,tv.tv_usec,alim,Vmeas);
    if(iLVR==0) SEU.V2P5_sense=Vmeas;
    else        SEU.V1P2_sense=Vmeas;
  }

// Read voltages measured by FPGA
  for(Int_t iXADC=0; iXADC<2; iXADC++)
  {
    UInt_t XADC_reg=0x13;
    if(iXADC==1) XADC_reg=0x19;
    Double_t loc_val=0., mean=0.;
    for(Int_t iave=0; iave<16; iave++)
    {
      hw.getNode("DRP_XADC").write(DRP_WRb*0 | (XADC_reg<<16));
      hw.dispatch();
      meas  = hw.getNode("DRP_XADC").read();
      hw.dispatch();
      loc_val=((meas.value()&0xffff)>>4)/4096.;
      mean+=loc_val;
    }
    mean/=16.;
    if(iXADC==0)mean*=4.3; // (1k+3.3k divider)
    if(iXADC==1)mean*=2.;  // (1k+1k   divider)
    printf("%ld.%6.6ld : XADC average measurement : %.4f V\n",tv.tv_sec,tv.tv_usec,mean);
    fprintf(daq.fout_SEU,"%ld.%6.6ld : XADC average measurement : %.4f V\n",tv.tv_sec,tv.tv_usec,mean);
  }

  printf("\nDefault register contents : \n");
  for(Int_t i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    for(Int_t j=0; j<SEU.n_reg; j++)
    {
      Int_t ireg=SEU.reg_number[j];
      if(ireg==3 || ireg==4 || ireg==5)
      {
        printf("%d : 0x%4.4x, ",ireg,asic.CATIA_reg_loc[ich][ireg]);
      }
      else
      {
        printf("%d : 0x%2.2x, ",ireg,asic.CATIA_reg_loc[ich][ireg]);
      }
    }
    printf("\n");
  }

// Start monitoring SEUs
  for(daq.cur_evt=0; (daq.cur_evt<daq.nevent || daq.nevent<0) && SEU.test_running; daq.cur_evt++)
  {
    gettimeofday(&tv,NULL);

// Read status and currents and voltages
    fault     = hw.getNode("LVRB_FAULT").read();
    current   = hw.getNode("LVRB_CURRENT").read();
    voltage   = hw.getNode("LVRB_VOLTAGE").read();
    hw.dispatch();
    SEU.Imeas_V2P5=((current.value()>>4) &0xfff)/4096.*102.4/daq.Rshunt_V2P5;
    SEU.Imeas_V1P2=((current.value()>>20)&0xfff)/4096.*102.4/daq.Rshunt_V1P2;
    SEU.V2P5_sense=((voltage.value()>>4) &0xfff)/4096.*2.048*2.;
    SEU.V1P2_sense=((voltage.value()>>20)&0xfff)/4096.*2.048;
    SEU.V2P5_XADC=0.;
    SEU.V1P2_XADC=0.;
    SEU.CATIA_temp_val=0.;
    SEU.FPGA_temp_val=0.;
    for(Int_t iave=0; iave<32; iave++)
    {
      //hw.getNode("DRP_XADC").write(DRP_WRb*0 | (0x13<<16));
      //meas_V2P5 = hw.getNode("DRP_XADC").read();
      //hw.getNode("DRP_XADC").write(DRP_WRb*0 | (0x19<<16));
      //meas_V1P2 = hw.getNode("DRP_XADC").read();
      hw.getNode("DRP_XADC").write(DRP_WRb*0 | (XADC_CATIA_Temp<<16));
      meas_CATIA_temp = hw.getNode("DRP_XADC").read();
      hw.getNode("DRP_XADC").write(DRP_WRb*0 | (XADC_FPGA_Temp<<16));
      meas_FPGA_temp = hw.getNode("DRP_XADC").read();
      hw.dispatch();
      //SEU.V2P5_XADC+=((meas_V2P5.value()&0xffff)>>4)/4096.*4.3;
      //SEU.V1P2_XADC+=((meas_V1P2.value()&0xffff)>>4)/4096.*2.;
      SEU.CATIA_temp_val+=((meas_CATIA_temp.value()&0xffff)>>4)/4096.;
      SEU.FPGA_temp_val+=((meas_FPGA_temp.value()&0xffff)>>4)/4096.;
    }
    //SEU.V2P5_XADC/=32.;
    //SEU.V1P2_XADC/=32.;
    SEU.CATIA_temp_val/=32.;
    SEU.CATIA_temp_val=(asic.CATIA_Temp_offset[daq.channel_number[daq.selected_channel]]-SEU.CATIA_temp_val/daq.Tsensor_divider)/0.0021; // 1k+3.3k divider (0.772)
    SEU.FPGA_temp_val/=32.;
    SEU.FPGA_temp_val=SEU.FPGA_temp_val*4096.*0.123-273.;
    if((daq.cur_evt%10)==0)
    {
      sprintf(widget_name,"0_V2P5_consumption");
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      sprintf(widget_text,"%.1f mA",SEU.Imeas_V2P5);
      gtk_label_set_text((GtkLabel*)widget,widget_text);
      sprintf(widget_name,"0_V2P5_value");
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      sprintf(widget_text,"%.3f V",SEU.V2P5_sense);
      gtk_label_set_text((GtkLabel*)widget,widget_text);
      sprintf(widget_name,"0_V1P2_consumption");
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      sprintf(widget_text,"%.1f mA",SEU.Imeas_V1P2);
      gtk_label_set_text((GtkLabel*)widget,widget_text);
      sprintf(widget_name,"0_V1P2_value");
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      sprintf(widget_text,"%.3f V",SEU.V1P2_sense);
      gtk_label_set_text((GtkLabel*)widget,widget_text);
      sprintf(widget_name,"0_FPGA_temp");
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      sprintf(widget_text,"FPGA %3d C",(int)SEU.FPGA_temp_val);
      gtk_label_set_text((GtkLabel*)widget,widget_text);
    }

// Look for SEL :
// V2P5 alert
    //if((fault.value()&0x20) != 0 || (fault.value()&0x200000) != 0 || daq.cur_evt==0 || SEU.V2P5_XADC<2.2 || SEU.V1P2_XADC<1.0)
    if((fault.value()&0x20) != 0 || (fault.value()&0x200000) != 0 || daq.cur_evt==0 || SEU.V2P5_sense<2.2 || SEU.V1P2_sense<1.0)
    {
      printf("\n");
      fprintf(daq.fout_SEU,"\n");
      //if((fault.value()&0x20)     != 0 || SEU.V2P5_XADC<2.2)
      if((fault.value()&0x20)     != 0 || SEU.V2P5_sense<2.2)
      {
        SEU.n_SEL[0]++;
        //printf("%ld.%6.6ld : Alert seen on V2P5 : V=%7.3f, I=%7.3f mA\n",tv.tv_sec,tv.tv_usec,SEU.V2P5_XADC, SEU.Imeas_V2P5);
        //fprintf(daq.fout_SEU,"%ld.%6.6ld : Alert seen on V2P5 : V=%7.3f, I=%7.3f mA\n",tv.tv_sec,tv.tv_usec, SEU.V2P5_XADC, SEU.Imeas_V2P5);
        printf("%ld.%6.6ld : Alert seen on V2P5 : V=%7.3f, I=%7.3f mA\n",tv.tv_sec,tv.tv_usec,SEU.V2P5_sense, SEU.Imeas_V2P5);
        fprintf(daq.fout_SEU,"%ld.%6.6ld : Alert seen on V2P5 : V=%7.3f, I=%7.3f mA\n",tv.tv_sec,tv.tv_usec, SEU.V2P5_sense, SEU.Imeas_V2P5);
      }
      //if((fault.value()&0x200000) != 0 || SEU.V1P2_XADC<1.0)
      if((fault.value()&0x200000) != 0 || SEU.V1P2_sense<1.0)
      {
        SEU.n_SEL[1]++;
        //printf("%ld.%6.6ld : Alert seen on V1P2 : V=%7.3f, I=%7.3f mA\n",tv.tv_sec,tv.tv_usec, SEU.V1P2_XADC, SEU.Imeas_V1P2);
        //fprintf(daq.fout_SEU,"%ld.%6.6ld : Alert seen on V1P2 : V=%7.3f, I=%7.3f mA\n",tv.tv_sec,tv.tv_usec, SEU.V1P2_XADC, SEU.Imeas_V1P2);
        printf("%ld.%6.6ld : Alert seen on V1P2 : V=%7.3f, I=%7.3f mA\n",tv.tv_sec,tv.tv_usec, SEU.V1P2_sense, SEU.Imeas_V1P2);
        fprintf(daq.fout_SEU,"%ld.%6.6ld : Alert seen on V1P2 : V=%7.3f, I=%7.3f mA\n",tv.tv_sec,tv.tv_usec, SEU.V1P2_sense, SEU.Imeas_V1P2);
      }

// We had troubles, send a reset :
      if(daq.cur_evt>0)
      {
        hw.getNode("FEAD_CTRL").write(RESET*1);
        hw.getNode("FEAD_CTRL").write(RESET*0);
        hw.dispatch();
        usleep(100000);
        hw.getNode("VFE_CTRL").write(1*PWUP_RESETB);
        hw.getNode("VFE_CTRL").write(0*PWUP_RESETB);
        hw.dispatch();
        usleep(100000);

// Put back CATIAs ref values if we had to reset :
        for(Int_t i=0; i<daq.n_active_channel; i++)
        {
          Int_t ich=daq.channel_number[i];
          Int_t num=asic.CATIA_number[ich];
          if(num<=0)continue;
          device_number=daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+3;
          if(ich==4 && daq.I2C_shift_dev_number==4)   device_number = daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+0xb;

          for(Int_t j=0; j<SEU.n_reg; j++)
          {
            Int_t ireg=SEU.reg_number[j];
            I2C_long=0;
            I2C_mask=0xFF;
            if(ireg==3 || ireg==4 || ireg==5){I2C_long=1;I2C_mask=0xFFFF;}

            if(ireg==2)
            {
              asic.CATIA_reg_loc[ich][ireg]=0; // Expected SEU counter back to 0 after reset
              SEU_simul_counter[ich]=0;
            }
            if(daq.SEU_test_mode==0) // "reset" mode : overwrite reg1 for temp and SEU corr after reset
            {
              if(ireg==1)iret=I2C_RW(hw, device_number, ireg, asic.CATIA_reg_loc[ich][ireg], I2C_long, 1, daq.debug_I2C);
            }
            else if(daq.SEU_test_mode==1) // "keep" mode : update reference values with read ones except register 1 (temp and SEU cor)
            {
              if(ireg==1)
                iret=I2C_RW(hw, device_number, ireg, asic.CATIA_reg_loc[ich][ireg], I2C_long, 1, daq.debug_I2C);
              else
                asic.CATIA_reg_loc[ich][ireg]=I2C_RW(hw, device_number, ireg, asic.CATIA_reg_loc[ich][ireg], I2C_long, 2, daq.debug_I2C)&I2C_mask;
            }
            else if(daq.SEU_test_mode==2) // "restore" mode
            {
              iret=I2C_RW(hw, device_number, ireg, asic.CATIA_reg_loc[ich][ireg], I2C_long, 1, daq.debug_I2C);
            }
          }
        }
      }
// Remove alert bit on both power supplies (register 1)
      printf("%ld.%6.6ld : Clear alert bit pattern\n",tv.tv_sec,tv.tv_usec);
      fprintf(daq.fout_SEU,"%ld.%6.6ld : Clear alert bit pattern\n",tv.tv_sec,tv.tv_usec);
      command=0x00; // No more alert on Dsense overflow
      iret=I2C_RW(hw, 0x67, 0x01, command,0, 3, daq.debug_I2C);
      iret=I2C_RW(hw, 0x6C, 0x01, command,0, 3, daq.debug_I2C);
// Clear fault (register 4)
      //iret=I2C_RW(hw, 0x67, 0x04, command,0, 2, daq.debug_I2C);
      //printf("%ld.%6.6ld : V2P5 Fault register content : 0x%x cleared\n",tv.tv_sec,tv.tv_usec,iret&0xffff);
      //fprintf(daq.fout_SEU,"%ld.%6.6ld : V2P5 Fault register content : 0x%x cleared\n",tv.tv_sec,tv.tv_usec,iret&0xffff);
      //iret=I2C_RW(hw, 0x6C, 0x04, command,0, 2, daq.debug_I2C);
      //printf("%ld.%6.6ld : V1P2 Fault register content : 0x%x cleared\n",tv.tv_sec,tv.tv_usec,iret&0xffff);
      //fprintf(daq.fout_SEU,"%ld.%6.6ld : V1P2 Fault register content : 0x%x cleared\n",tv.tv_sec,tv.tv_usec,iret&0xffff);
// Wait for current stabilization
      printf("%ld.%6.6ld : Wait for current stabilization...\n",tv.tv_sec,tv.tv_usec);
      fprintf(daq.fout_SEU,"%ld.%6.6ld : Wait for current stabilization...\n",tv.tv_sec,tv.tv_usec);
      usleep(2000000);
// Measure current
      current = hw.getNode("LVRB_CURRENT").read();
      voltage = hw.getNode("LVRB_VOLTAGE").read();
      hw.dispatch();
      SEU.Imeas_V2P5=((current.value()>>4)&0xfff)/4096.*102.4/daq.Rshunt_V2P5;
      SEU.Imeas_V1P2=((current.value()>>20)&0xfff)/4096.*102.4/daq.Rshunt_V1P2;
      SEU.V2P5_sense=((voltage.value()>>4) &0xfff)/4096.*2.048*2.;
      SEU.V1P2_sense=((voltage.value()>>20)&0xfff)/4096.*2.048;
// Set new threshold
      printf("%ld.%6.6ld : Present V2P5 current : %7.3f mA (0x%x)\n",tv.tv_sec,tv.tv_usec,SEU.Imeas_V2P5,(current.value()>>4)&0xfff);
      fprintf(daq.fout_SEU,"%ld.%6.6ld : Present V2P5 current : %7.3f mA (0x%x)\n",tv.tv_sec,tv.tv_usec,SEU.Imeas_V2P5,(current.value()>>4)&0xfff);
      UInt_t thres=(((current.value()>>4)&0xfff)+I2P5_margin_lsb+1)<<4;
      printf("%ld.%6.6ld : Set alert threshold at %7.3f mA (0x%x)\n",tv.tv_sec,tv.tv_usec,SEU.Imeas_V2P5+I_margin,thres);
      fprintf(daq.fout_SEU,"%ld.%6.6ld : Set alert threshold at %7.3f mA (0x%x)\n",tv.tv_sec,tv.tv_usec,SEU.Imeas_V2P5+I_margin,thres);
      iret=I2C_RW(hw, 0x67, 0x1A, thres,1, 3, daq.debug_I2C);
      printf("%ld.%6.6ld : New V2P5 max threshold on current 0x%x\n",tv.tv_sec,tv.tv_usec,iret&0xffff);
      fprintf(daq.fout_SEU,"%ld.%6.6ld : New V2P5 max threshold on current 0x%x\n",tv.tv_sec,tv.tv_usec,iret&0xffff);

      printf("%ld.%6.6ld : Present V1P2 current : %7.3f mA (0x%x)\n",tv.tv_sec,tv.tv_usec,SEU.Imeas_V1P2,(current.value()>>20)&0xfff);
      fprintf(daq.fout_SEU,"%ld.%6.6ld : Present V1P2 current : %7.3f mA (0x%x)\n",tv.tv_sec,tv.tv_usec,SEU.Imeas_V1P2,(current.value()>>20)&0xfff);
      thres=(((current.value()>>20)&0xfff)+I1P2_margin_lsb+1)<<4;
      printf("%ld.%6.6ld : Set alert threshold at %7.3f mA (0x%x)\n",tv.tv_sec,tv.tv_usec,SEU.Imeas_V1P2+I_margin,((current.value()>>20)&0xfff)+I1P2_margin_lsb);
      fprintf(daq.fout_SEU,"%ld.%6.6ld : Set alert threshold at %7.3f mA (0x%x)\n",tv.tv_sec,tv.tv_usec,SEU.Imeas_V1P2+I_margin,((current.value()>>20)&0xfff)+I1P2_margin_lsb);
      iret=I2C_RW(hw, 0x6C, 0x1A, thres,1, 3, daq.debug_I2C);
      printf("%ld.%6.6ld : New V1P2 max threshold on current 0x%x\n",tv.tv_sec,tv.tv_usec,iret&0xffff);
      fprintf(daq.fout_SEU,"%ld.%6.6ld : New V1P2 max threshold on current 0x%x\n",tv.tv_sec,tv.tv_usec,iret&0xffff);
// Reset fault register :
      iret=I2C_RW(hw, 0x67, 0x04, command,0, 2, daq.debug_I2C);
      printf("%ld.%6.6ld : V2P5 Fault register content : 0x%x cleared\n",tv.tv_sec,tv.tv_usec,iret&0xffff);
      fprintf(daq.fout_SEU,"%ld.%6.6ld : V2P5 Fault register content : 0x%x cleared\n",tv.tv_sec,tv.tv_usec,iret&0xffff);
      iret=I2C_RW(hw, 0x6C, 0x04, command,0, 2, daq.debug_I2C);
      printf("%ld.%6.6ld : V1P2 Fault register content : 0x%x cleared\n",tv.tv_sec,tv.tv_usec,iret&0xffff);
      fprintf(daq.fout_SEU,"%ld.%6.6ld : V1P2 Fault register content : 0x%x cleared\n",tv.tv_sec,tv.tv_usec,iret&0xffff);
// Put back alert bit
      printf("%ld.%6.6ld : Put back alert bit pattern\n",tv.tv_sec,tv.tv_usec);
      fprintf(daq.fout_SEU,"%ld.%6.6ld : Put back alert bit pattern\n",tv.tv_sec,tv.tv_usec);
      command=0x20; // Alert on Dsense overflow
      iret=I2C_RW(hw, 0x6C, 0x01, command,0, 3, daq.debug_I2C);
      iret=I2C_RW(hw, 0x67, 0x01, command,0, 3, daq.debug_I2C);
      if(daq.cur_evt==0)
      {
        printf("%ld.%6.6ld : Start SEU/SER/SEL monitoring\n",tv.tv_sec,tv.tv_usec);
        fprintf(daq.fout_SEU,"%ld.%6.6ld : Start SEU/SER/SEL monitoring\n",tv.tv_sec,tv.tv_usec);
      }
      printf("\n");
      fprintf(daq.fout_SEU,"\n");
    }

    if((daq.cur_evt%100)==0)
    {
      printf("\r%ld.%6.6ld : %.6d : V2P5 fault 0x%2.2x V=%7.3f V I=%7.3f mA SEL=%2d, V1P2 fault 0x%2.2x, V=%7.3f V I=%7.3f mA SEL=%2d, SEU : %3d %3d %3d %3d %3d, T=%.2f %.2f C",
              tv.tv_sec,tv.tv_usec,
              daq.cur_evt,fault.value()&0xff,      SEU.V2P5_sense,SEU.Imeas_V2P5,SEU.n_SEL[0],
                         (fault.value()>>16)&0xff, SEU.V1P2_sense,SEU.Imeas_V1P2,SEU.n_SEL[1],
              SEU.n_SEU[0],SEU.n_SEU[1],SEU.n_SEU[2],SEU.n_SEU[3],SEU.n_SEU[4],
              SEU.CATIA_temp_val, SEU.FPGA_temp_val);
      fflush(stdout);
    }

    if((daq.cur_evt%1000)==0)
    {
      fprintf(daq.fout_SEU,"%ld.%6.6ld : %.6d : V2P5 fault 0x%2.2x V=%7.3f V I=%7.3f mA, V1P2 fault 0x%2.2x, V=%7.3f V I=%7.3f mA, SEU : %3d %3d %3d %3d %3d, T=%.2f %.2f C\n",
              tv.tv_sec,tv.tv_usec,
              daq.cur_evt,fault.value()&0xff,      SEU.V2P5_sense,SEU.Imeas_V2P5,
                   (fault.value()>>16)&0xff, SEU.V1P2_sense,SEU.Imeas_V1P2,
              SEU.n_SEU[0],SEU.n_SEU[1],SEU.n_SEU[2],SEU.n_SEU[3],SEU.n_SEU[4],
              SEU.CATIA_temp_val, SEU.FPGA_temp_val);
      fflush(daq.fout_SEU);
    }

    Int_t global_error=0;
    Int_t CATIA_reg_read[5][7]={0};

    if(daq.SEU_simul && tr->Rndm()>0.9995)
    {
      printf("\nSimulate SER !\n");
      fprintf(daq.fout_SEU,"%ld.%6.6ld : Simulate SER\n",tv.tv_sec,tv.tv_usec);
      hw.getNode("VFE_CTRL").write(1*PWUP_RESETB);
      hw.getNode("VFE_CTRL").write(0*PWUP_RESETB);
      hw.dispatch();
      for(Int_t i=0; i<daq.n_active_channel; i++)
      {
        Int_t ich=daq.channel_number[i];
        SEU_simul_counter[ich]=0;
      }
    }

// Read registers and compare with ref values :
    for(Int_t i=0; i<daq.n_active_channel; i++)
    {
      Int_t ich=daq.channel_number[i];
      Int_t num=asic.CATIA_number[ich];
      if(num<=0)continue;
      device_number=daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+3;
      if(ich==4 && daq.I2C_shift_dev_number==4)   device_number = daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+0xb;

      for(Int_t j=0; j<SEU.n_reg; j++)
      {
        Int_t ireg=SEU.reg_number[j];
        I2C_long=0;
        I2C_mask=0xFF;
        if(ireg==3 || ireg==4 || ireg==5){I2C_long=1;I2C_mask=0xFFFF;}

        CATIA_reg_read[ich][ireg]=(I2C_RW(hw, device_number, ireg, 0,I2C_long, 2, daq.debug_I2C))&I2C_mask;
// Simulate a SEU :
        double xrnd=tr->Rndm();
        if(daq.SEU_simul && xrnd>0.998 && ich==daq.selected_channel)
        {
          if(ireg==2)
          {
            SEU_simul_counter[ich]++;
            printf("\nSimulate recovered SEU !\n");
            fprintf(daq.fout_SEU,"%ld.%6.6ld : Simulate recovered SEU on channel %d\n",tv.tv_sec,tv.tv_usec,ich+1);
            CATIA_reg_read[ich][ireg]=CATIA_reg_read[ich][ireg]+1;
          }
          else if(xrnd>0.9999)
          {
            printf("\nSimulate non recovered SEU on channel %d, register %d!\n",ich+1, ireg);
            fprintf(daq.fout_SEU,"%ld.%6.6ld : Simulate non recovered SEU on channel %d, register %d\n",tv.tv_sec,tv.tv_usec,ich+1,ireg);
            CATIA_reg_read[ich][ireg]=CATIA_reg_read[ich][ireg]+1;
          }
        }
        if(daq.SEU_simul && ireg==2)CATIA_reg_read[ich][ireg]=SEU_simul_counter[ich];

        if(ireg==2 && CATIA_reg_read[ich][ireg]!=asic.CATIA_reg_loc[ich][ireg])
        {
          if(CATIA_reg_read[ich][ireg]<asic.CATIA_reg_loc[ich][ireg]) // In case of SER, restart at 0 for SEU counter
          {
            printf("\n%ld.%6.6ld : New SER detected on Catia %d : #SEU : %3d\n",tv.tv_sec,tv.tv_usec,ich+1,CATIA_reg_read[ich][ireg]);
            fprintf(daq.fout_SEU,"%ld.%6.6ld : New SER detected on Catia %d : #SEU : %3d\n",tv.tv_sec,tv.tv_usec,ich+1,CATIA_reg_read[ich][ireg]);
            SEU.n_SEU[ich]+=CATIA_reg_read[ich][ireg];
          }
          else
          {
            SEU.n_SEU[ich]+=CATIA_reg_read[ich][ireg]-asic.CATIA_reg_loc[ich][ireg];
            printf("\n%ld.%6.6ld : New SEU detected on Catia %d : #SEU : %3d\n",tv.tv_sec,tv.tv_usec,ich+1,CATIA_reg_read[ich][ireg]);
            fprintf(daq.fout_SEU,"%ld.%6.6ld : New SEU detected on Catia %d : #SEU : %3d\n",tv.tv_sec,tv.tv_usec,ich+1,CATIA_reg_read[ich][ireg]);
          }
          asic.CATIA_reg_loc[ich][ireg]=CATIA_reg_read[ich][ireg];
        }

        Int_t loc_error=0;
        if(ireg!=2 && CATIA_reg_read[ich][ireg]!= asic.CATIA_reg_loc[ich][ireg])
        {
          loc_error=1;
          global_error=1;
          SEU.error[ich][ireg]++;
        }
        if(daq.debug_I2C>1 || loc_error==1)
        {
          printf("\n%ld.%6.6ld : I2C : Read CATIA %d register %d.",tv.tv_sec,tv.tv_usec,ich+1, ireg);
          fprintf(daq.fout_SEU,"%ld.%6.6ld : I2C : Read CATIA %d register %d.",tv.tv_sec,tv.tv_usec,ich+1, ireg);
          printf("Value read : 0x%.8x, should be 0x%.8x, error=%d\n",CATIA_reg_read[ich][ireg], asic.CATIA_reg_loc[ich][ireg],loc_error);
          fprintf(daq.fout_SEU,"Value read : 0x%.8x, should be 0x%.8x, error=%d\n",CATIA_reg_read[ich][ireg], asic.CATIA_reg_loc[ich][ireg],loc_error);
        }

        if(loc_error==1 && ireg!=2)
        {
          Int_t tot_error=0;
          for(Int_t k=0; k<SEU.n_reg; k++)
          {
            Int_t kreg=SEU.reg_number[k];
            tot_error+=SEU.error[ich][kreg];
          }
          sprintf(widget_name,"%d_CATIA_I2C_error",ich+1);
          widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
          g_assert (widget);
          sprintf(widget_text,"%d",tot_error);
          gtk_label_set_text((GtkLabel*)widget,widget_text);
          while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ
        }
        usleep(delay);
      }
    }

    if(global_error==1)
    {
      if(daq.SEU_test_mode==0) // "reset" mode : reset the board after error
      {
        printf("%ld.%6.6ld : Reset VFE board\n",tv.tv_sec,tv.tv_usec);
        fprintf(daq.fout_SEU,"%ld.%6.6ld : Reset VFE board\n",tv.tv_sec,tv.tv_usec);
        hw.getNode("VFE_CTRL").write(1*PWUP_RESETB);
        hw.getNode("VFE_CTRL").write(0*PWUP_RESETB);
        hw.dispatch();
      }
      for(Int_t i=0; i<daq.n_active_channel; i++)
      {
        Int_t ich=daq.channel_number[i];
        Int_t num=asic.CATIA_number[ich];
        if(num<=0)continue;
        device_number=daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+3;
        if(ich==4 && daq.I2C_shift_dev_number==4)   device_number = daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+0xb;

        for(Int_t j=0; j<SEU.n_reg; j++)
        {
          Int_t ireg=SEU.reg_number[j];
          I2C_long=0;
          I2C_mask=0xFF;
          if(ireg==3 || ireg==4 || ireg==5){I2C_long=1;I2C_mask=0xFFFF;}

          if(daq.SEU_test_mode==0) // "reset" mode : overwrite reg1 for temp and SEU corr after reset
          {
            if(ireg==1)iret=I2C_RW(hw, device_number, ireg, asic.CATIA_reg_loc[ich][ireg], I2C_long, 1, daq.debug_I2C);
            if(ireg==2)
            {
              asic.CATIA_reg_loc[ich][ireg]=0; // Expected SEU counter back to 0
              SEU_simul_counter[ich]=0;
            }
          }
          else if(daq.SEU_test_mode==1) // "keep" mode : update reference values with read ones
          {
            asic.CATIA_reg_loc[ich][ireg]=CATIA_reg_read[ich][ireg];
          }
          else if(daq.SEU_test_mode==2) // "restore" mode
          {
            iret=I2C_RW(hw, device_number, ireg, asic.CATIA_reg_loc[ich][ireg], I2C_long, 1, daq.debug_I2C);
          }
        }
      }
    }

// Purge Gtk waiting action :
   
    if((daq.cur_evt%10)==0)
    {
      sprintf(widget_name,"0_nevent");
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      sprintf(widget_text,"%d",daq.cur_evt);
      gtk_entry_set_text((GtkEntry*)widget,widget_text);
      for(Int_t i=0; i<daq.n_active_channel; i++)
      {
        Int_t ich=daq.channel_number[i];
        sprintf(widget_name,"%d_CATIA_SEU",ich+1);
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
        g_assert (widget);
        sprintf(widget_text,"%d",SEU.n_SEU[ich]);
        gtk_label_set_text((GtkLabel*)widget,widget_text);
        if(ich==daq.selected_channel)
        {
          sprintf(widget_name,"%d_CATIA_temp",ich+1);
          widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
          g_assert (widget);
          sprintf(widget_text,"%.1f",SEU.CATIA_temp_val);
          gtk_label_set_text((GtkLabel*)widget,widget_text);
        }
      }
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_V2P5_value");
      g_assert (widget);
      sprintf(widget_text,"%.3f V",SEU.V2P5_sense);
      gtk_label_set_text((GtkLabel*)widget,widget_text);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_V1P2_value");
      g_assert (widget);
      sprintf(widget_text,"%.3f V",SEU.V1P2_sense);
      gtk_label_set_text((GtkLabel*)widget,widget_text);

      while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ
// Have we asked to abort the run ?
      if(!SEU.test_running)
      {   
        printf("\n");
        fprintf(daq.fout_SEU,"\n");
        printf("Abort button pressed !");
        fprintf(daq.fout_SEU,"Abort button pressed after %d events !",daq.cur_evt);
        fflush(stdout);
        break;
      }   
    }
  }

  printf("\n%ld.%6.6ld : End of SEU test after %d events\n",tv.tv_sec,tv.tv_usec,daq.cur_evt);
  fprintf(daq.fout_SEU,"\n%ld.%6.6ld : End of SEU test after %d events\n",tv.tv_sec,tv.tv_usec,daq.cur_evt);

  printf("%ld.%6.6ld : SEU summary per CATIA :\n",tv.tv_sec,tv.tv_usec);
  fprintf(daq.fout_SEU,"%ld.%6.6ld : End of study\n",tv.tv_sec,tv.tv_usec);
  fprintf(daq.fout_SEU,"%ld.%6.6ld : SEU summary per CATIA :\n",tv.tv_sec,tv.tv_usec);
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    printf("%ld.%6.6ld : Catia %d : SEU : %3d\n",tv.tv_sec,tv.tv_usec, ich+1,SEU.n_SEU[ich]);
    fprintf(daq.fout_SEU,"%ld.%6.6ld : Catia %d : SEU : %3d\n",tv.tv_sec,tv.tv_usec, ich+1,SEU.n_SEU[ich]);
  }
  printf("%ld.%6.6ld : Summary of errors for each register :\n",tv.tv_sec,tv.tv_usec);
  fprintf(daq.fout_SEU,"%ld.%6.6ld : Summary of errors for each register :\n",tv.tv_sec,tv.tv_usec);
  int tot_error=0;
  printf("%ld.%6.6ld : register :",tv.tv_sec,tv.tv_usec);
  fprintf(daq.fout_SEU,"%ld.%6.6ld : register :",tv.tv_sec,tv.tv_usec);
  for(int j=0; j<SEU.n_reg; j++)
  {
    Int_t ireg=SEU.reg_number[j];
    printf("%6d ",ireg);
    fprintf(daq.fout_SEU,"%6d ",ireg);
  }
  printf("\n");
  fprintf(daq.fout_SEU,"\n");
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    printf("%ld.%6.6ld : Catia %d : ",tv.tv_sec,tv.tv_usec,ich+1);
    fprintf(daq.fout_SEU,"%ld.%6.6ld : Catia %d : ",tv.tv_sec,tv.tv_usec,ich+1);
    for(int j=0; j<SEU.n_reg; j++)
    {
      Int_t ireg=SEU.reg_number[j];
      printf("%6d ",SEU.error[ich][ireg]);
      fprintf(daq.fout_SEU,"%6d ",SEU.error[ich][ireg]);
      tot_error+=SEU.error[ich][ireg];
    }
    printf("\n");
    fprintf(daq.fout_SEU,"\n");
  }
  printf("Total number of errors : %d\n",tot_error);
  fprintf(daq.fout_SEU,"%ld.%6.6ld : Total number of errors : %d\n",tv.tv_sec,tv.tv_usec,tot_error);

  printf("%ld.%6.6ld : SEL summary per power supply : ",tv.tv_sec,tv.tv_usec);
  printf("V2P5 : %d, V1P2 :%d\n",SEU.n_SEL[0],SEU.n_SEL[1]);
  fprintf(daq.fout_SEU,"%ld.%6.6ld : SEL summary per power supply : ",tv.tv_sec,tv.tv_usec);
  fprintf(daq.fout_SEU,"V2P5 : %d, V1P2 : %d\n",SEU.n_SEL[0],SEU.n_SEL[1]);

  fflush(stdout);
  fclose(daq.fout_SEU);
  daq.fout_SEU=NULL;

// Restore Gtk menus :
  sprintf(widget_name,"0_nevent");
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  sprintf(widget_text,"%d",daq.nevent);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate(widget);
  sprintf(widget_name,"0_SEU_test");
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  if(SEU.test_running)gtk_button_clicked((GtkButton*)widget);
  //tr->~TRandom();
}
