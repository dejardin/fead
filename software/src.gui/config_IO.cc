#define EXTERN extern
#include <gtk/gtk.h>
#include "gdaq_VFE.h"
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include "limits.h"

void read_config(GtkMenuItem *menuitem, gpointer user_data)
{
  GtkWidget *dialog = NULL;
  GtkFileFilter *filter;
  dialog = gtk_file_chooser_dialog_new ("Open File",
                      NULL,
                      GTK_FILE_CHOOSER_ACTION_OPEN,
                      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
                      NULL);

  filter = gtk_file_filter_new();
  gtk_file_filter_add_pattern(filter, "*.xml");
  gtk_file_filter_set_name(filter, "xml files");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);

// Get working directory :
  char cwd[PATH_MAX];
  if (getcwd(cwd, sizeof(cwd)) != NULL) printf("Current working dir: %s\n", cwd);
  strcat(cwd,"/xml/");
  printf("Defaults xml dir: %s\n", cwd);
  gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog),cwd);

  gtk_widget_show_all(dialog);
  if (gtk_dialog_run(GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
  {
    char *filename;

    filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER (dialog));
    strcpy(daq.xml_filename,filename);
    read_conf_from_xml_file (filename);
    FILE *fd_xml=fopen(".last_xml_file","w+");
    fprintf(fd_xml,"%s",daq.xml_filename);
    fclose(fd_xml);
    g_free (filename);
  }

  gtk_widget_destroy (dialog);
  return;
}
void save_config(GtkMenuItem *menuitem, gpointer user_data)
{
  printf("Save current config in %s\n",daq.xml_filename);
  write_conf_to_xml_file(daq.xml_filename);
  return;
}

void save_config_as(GtkMenuItem *menuitem, gpointer user_data)
{
  GtkWidget *dialog = NULL;
  GtkFileFilter *filter;
  dialog = gtk_file_chooser_dialog_new ("Save File",
                      NULL,
                      GTK_FILE_CHOOSER_ACTION_SAVE,
                      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
                      NULL);

  filter = gtk_file_filter_new();
  gtk_file_filter_add_pattern(filter, "*.xml");
  gtk_file_filter_set_name(filter, "xml files");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);
  printf("Current file : %s\n",daq.xml_filename);
  gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), daq.xml_filename);

// Get working directory :
  char cwd[PATH_MAX];
  if (getcwd(cwd, sizeof(cwd)) != NULL) printf("Current working dir: %s\n", cwd);
  strcat(cwd,"/xml/");
  printf("Defaults xml dir: %s\n", cwd);
  gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog),cwd);

  gtk_widget_show_all(dialog);
  if (gtk_dialog_run(GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
  {
    char *filename;
    filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER (dialog));
    printf("Select file : %s\n",filename);
    strcpy(daq.xml_filename,filename);
    FILE *fd_xml=fopen(".last_xml_file","w+");
    fprintf(fd_xml,"%s",daq.xml_filename);
    fclose(fd_xml);
    g_free (filename);
    write_conf_to_xml_file(daq.xml_filename);
  }

  gtk_widget_destroy (dialog);
  return;
}

void read_conf_from_xml_file(char *filename)
{
  xmlChar *object_name, *prop_name, *prop_value, *child_status, *child_id;
  xmlDocPtr doc;
  xmlNodePtr cur, child, child2;
  Int_t loc_ich;

  char fname[132], widget_name[80], entry_text[80];
  GtkWidget *widget;
  daq.FEAD_number=0;
  daq.VFE_number=0;
  daq.VICEPP_clk=0;
  daq.VICEPP_clk_tree=0;
  daq.resync_phase=0;
  daq.SEU_test_mode=1;
  daq.SEU_pattern=5; // values from xml by default
  daq.SEU_simul=0;
  daq.SEU_current_limit=10;
  daq.dump_data=0;

// disable channels by defaults :
  for(Int_t ich=0; ich<5; ich++)
  {
    asic.CATIA_number[ich]=-1;
    asic.DTU_number[ich]=-1;
    enable_channel(ich+1,FALSE);
    sprintf(widget_name,"%1.1d_channel_status",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    gtk_switch_set_state((GtkSwitch*)widget,FALSE);
    asic.CATIA_reg_def[ich][0]=0x00;
    asic.CATIA_reg_def[ich][1]=0x02;
    asic.CATIA_reg_def[ich][2]=0x00;
    asic.CATIA_reg_def[ich][3]=0x1087;
    asic.CATIA_reg_def[ich][4]=0xFFFF;
    asic.CATIA_reg_def[ich][5]=0xFFFF;
    asic.CATIA_reg_def[ich][6]=0x0F;
  }

  daq.n_active_channel=0;
  daq.eLink_active=0;
  if(filename==NULL)
    sprintf(fname,"xml/VFE_config_%2.2d.xml",daq.VFE_number);
  else
    strcpy(fname,filename);
  printf("Read conf from xml file %s\n",fname);

  doc = xmlParseFile(fname);
  if (doc == NULL )
  {
    fprintf(stderr,"Document not parsed successfully. \n");
    return;
  }

  cur = xmlDocGetRootElement(doc);
  if (cur == NULL)
  {
    fprintf(stderr,"empty document\n");
    xmlFreeDoc(doc);
    return;
  }

  printf("Start to parse document : %s\n",fname);
  if (xmlStrcmp(cur->name, (const xmlChar *) "vfe"))
  {
    fprintf(stderr,"document of the wrong type, root node != vfe");
    xmlFreeDoc(doc);
    return;
  }
  else
  {
    printf("Starting point : %s\n",cur->name);
  }

  cur = cur->xmlChildrenNode;
  while (cur != NULL)
  {
    if ((!xmlStrcmp(cur->name, (const xmlChar *)"object")))
    {
      printf("Start to parse object : %s\n",xmlGetProp(cur,(const xmlChar *)"name"));

// DAQ settings :
      object_name=xmlGetProp(cur,(const xmlChar *)"name");
      if ((!xmlStrcmp(object_name, (const xmlChar *)"DAQ")))
      {
        child = cur->xmlChildrenNode;
        while (child != NULL)
        {
          if ((!xmlStrcmp(child->name, (const xmlChar *)"property")))
          {
            prop_value = xmlNodeListGetString(doc, child->xmlChildrenNode, 1);
            prop_name=xmlGetProp(child,(const xmlChar *)"name");
            printf("property %s :  %s\n",prop_name, prop_value);
            if(!xmlStrcmp(prop_name, (const xmlChar *)"VICEPP"))
            {
              sscanf((const char*)prop_value,"%d",&daq.FEAD_number);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_FEAD_number");
              g_assert (widget);
              sprintf(entry_text,"%d",daq.FEAD_number);
              gtk_entry_set_text((GtkEntry*)widget,entry_text);
              prop_value=xmlGetProp(child,(const xmlChar *)"status");
              daq.is_VICEPP=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
              printf("VICEPP status : %s %d\n",prop_value, daq.is_VICEPP);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_is_VICEPP");
              g_assert (widget);
              gtk_toggle_button_set_active((GtkToggleButton*)widget,daq.is_VICEPP);
            }
            else if (!xmlStrcmp(prop_name, (const xmlChar *)"clock"))
            {
              sscanf((const char*)prop_value,"%d",&daq.VICEPP_clk);
              if((daq.VICEPP_clk&3)==3)daq.VICEPP_clk=-1;
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"custom_clock"))
            {
              sscanf((const char*)prop_value,"%x",&daq.VICEPP_clk_tree);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"resync_phase"))
            {
              sscanf((const char*)prop_value,"%d",&daq.resync_phase);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_resync_phase");
              g_assert (widget);
              if(daq.resync_phase==0)
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
              else
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"test_mode"))
            {
              sscanf((const char*)prop_value,"%d",&daq.DTU_test_mode);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_DTU_test_mode");
              g_assert (widget);
              if(daq.DTU_test_mode==0)
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
              else
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"I2C_shift_dev_number"))
            {
              sscanf((const char*)prop_value,"%d",&daq.I2C_shift_dev_number);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"debug_ana"))
            {
              sscanf((const char*)prop_value,"%d",&daq.debug_ana);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"debug_DAQ"))
            {
              sscanf((const char*)prop_value,"%d",&daq.debug_DAQ);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_debug_DAQ");
              g_assert (widget);
              if(daq.debug_DAQ==0)
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
              else
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"debug_I2C"))
            {
              sscanf((const char*)prop_value,"%d",&daq.debug_I2C);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_debug_I2C");
              g_assert (widget);
              if(daq.debug_I2C==0)
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
              else
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
            }
//            else if(!xmlStrcmp(prop_name, (const xmlChar *)"debug_GTK"))
//            {
//              sscanf((const char*)prop_value,"%d",&daq.debug_GTK);
//              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_debug_GTK");
//              g_assert (widget);
//              if(daq.debug_GTK==0)
//                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
//              else
//                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
//            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"debug_draw"))
            {
              sscanf((const char*)prop_value,"%d",&daq.debug_draw);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_debug_draw");
              g_assert (widget);
              if(daq.debug_draw==0)
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
              }
              else
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"zoom_draw"))
            {
              sscanf((const char*)prop_value,"%d",&daq.zoom_draw);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_zoom_draw");
              g_assert (widget);
              if(daq.zoom_draw==0)
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
              }
              else
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
              }
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"corgain"))
            {
              daq.corgain=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_gain_correction");
              g_assert (widget);
              gtk_toggle_button_set_active((GtkToggleButton*)widget,daq.corgain);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"wait_for_ever"))
            {
              daq.wait_for_ever=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_wait_for_ever");
              g_assert (widget);
              gtk_toggle_button_set_active((GtkToggleButton*)widget,daq.wait_for_ever);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"use_MEM_mode"))
            {
              sscanf((const char*)prop_value,"%d",&daq.MEM_pos);
              daq.MEM_mode=FALSE;
              sprintf(entry_text,"MEM mode\n      OFF");
              if(daq.MEM_pos>=0)
              {
                daq.MEM_mode=TRUE;
                sprintf(entry_text,"MEM mode\n        %d",daq.MEM_pos);
              }
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_MEM_mode");
              g_assert (widget);
              gtk_button_set_label((GtkButton*)widget,entry_text);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"I2C_lpGBT"))
            {
              daq.I2C_lpGBT_mode=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_I2C_standard");
              g_assert (widget);
              gtk_toggle_button_set_active((GtkToggleButton*)widget,daq.I2C_lpGBT_mode);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"HW_delay"))
            {
              sscanf((const char*)prop_value,"%d",&daq.hw_DAQ_delay);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_hw_DAQ_delay");
              g_assert (widget);
              sprintf(entry_text,"%3d",daq.hw_DAQ_delay);
              gtk_entry_set_text((GtkEntry*)widget,entry_text);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"SW_delay"))
            {
              sscanf((const char*)prop_value,"%d",&daq.sw_DAQ_delay);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_sw_DAQ_delay");
              g_assert (widget);
              sprintf(entry_text,"%3d",daq.sw_DAQ_delay);
              gtk_entry_set_text((GtkEntry*)widget,entry_text);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"TP_delay"))
            {
              sscanf((const char*)prop_value,"%d",&daq.TP_delay);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_delay");
              g_assert (widget);
              sprintf(entry_text,"%3d",daq.TP_delay);
              gtk_entry_set_text((GtkEntry*)widget,entry_text);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"TP_width"))
            {
              sscanf((const char*)prop_value,"%d",&daq.TP_width);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_width");
              g_assert (widget);
              sprintf(entry_text,"%3d",daq.TP_width);
              gtk_entry_set_text((GtkEntry*)widget,entry_text);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"TP_level"))
            {
              sscanf((const char*)prop_value,"%d",&daq.TP_level);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_level");
              g_assert (widget);
              sprintf(entry_text,"%3d",daq.TP_level);
              gtk_entry_set_text((GtkEntry*)widget,entry_text);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"TP_step"))
            {
              sscanf((const char*)prop_value,"%d",&daq.TP_step);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TP_step");
              g_assert (widget);
              sprintf(entry_text,"%3d",daq.TP_step);
              gtk_entry_set_text((GtkEntry*)widget,entry_text);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"n_TP_step"))
            {
              sscanf((const char*)prop_value,"%d",&daq.n_TP_step);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_n_TP_step");
              g_assert (widget);
              sprintf(entry_text,"%3d",daq.n_TP_step);
              gtk_entry_set_text((GtkEntry*)widget,entry_text);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"TE"))
            {
              daq.I2C_lpGBT_mode=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TE_enable");
              g_assert (widget);
              gtk_toggle_button_set_active((GtkToggleButton*)widget,daq.I2C_lpGBT_mode);
              prop_value=xmlGetProp(child,(const xmlChar *)"pos");
              sscanf((const char*)prop_value,"%d",&daq.TE_pos);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TE_pos_value");
              g_assert (widget);
              sprintf(entry_text,"%5d",daq.TE_pos);
              gtk_entry_set_text((GtkEntry*)widget,entry_text);
              prop_value=xmlGetProp(child,(const xmlChar *)"command");
              sscanf((const char*)prop_value,"%x",&daq.TE_command);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_TE_command_value");
              g_assert (widget);
              sprintf(entry_text,"%x",daq.TE_command);
              gtk_entry_set_text((GtkEntry*)widget,entry_text);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"nevent"))
            {
              sscanf((const char*)prop_value,"%d",&daq.nevent);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_nevent");
              g_assert (widget);
              sprintf(entry_text,"%3d",daq.nevent);
              gtk_entry_set_text((GtkEntry*)widget,entry_text);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"nsample"))
            {
              sscanf((const char*)prop_value,"%d",&daq.nsample);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_nsample");
              g_assert (widget);
              sprintf(entry_text,"%3d",daq.nsample);
              gtk_entry_set_text((GtkEntry*)widget,entry_text);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"CATIA_reg1_def"))
            {
              sscanf((const char*)prop_value,"0x%x",&asic.CATIA_reg_def[0][1]);
              for(Int_t i=1; i<5; asic.CATIA_reg_def[i++][1]=asic.CATIA_reg_def[0][1]);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"CATIA_reg2_def"))
            {
              sscanf((const char*)prop_value,"0x%x",&asic.CATIA_reg_def[0][2]);
              for(Int_t i=1; i<5; asic.CATIA_reg_def[i++][2]=asic.CATIA_reg_def[0][2]);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"CATIA_reg3_def"))
            {
              sscanf((const char*)prop_value,"0x%x",&asic.CATIA_reg_def[0][3]);
              for(Int_t i=1; i<5; asic.CATIA_reg_def[i++][3]=asic.CATIA_reg_def[0][3]);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"CATIA_reg4_def"))
            {
              sscanf((const char*)prop_value,"0x%x",&asic.CATIA_reg_def[0][4]);
              for(Int_t i=1; i<5; asic.CATIA_reg_def[i++][4]=asic.CATIA_reg_def[0][4]);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"CATIA_reg5_def"))
            {
              sscanf((const char*)prop_value,"0x%x",&asic.CATIA_reg_def[0][5]);
              for(Int_t i=1; i<5; asic.CATIA_reg_def[i++][5]=asic.CATIA_reg_def[0][5]);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"CATIA_reg6_def"))
            {
              sscanf((const char*)prop_value,"0x%x",&asic.CATIA_reg_def[0][6]);
              for(Int_t i=1; i<5; asic.CATIA_reg_def[i++][6]=asic.CATIA_reg_def[0][6]);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"SEU_test_mode"))
            {
              if(!xmlStrcasecmp(prop_value, (const xmlChar *)"reset"))daq.SEU_test_mode=0;
              if(!xmlStrcasecmp(prop_value, (const xmlChar *)"keep"))daq.SEU_test_mode=1;
              if(!xmlStrcasecmp(prop_value, (const xmlChar *)"restore"))daq.SEU_test_mode=2;
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_SEU_test_mode");
              g_assert (widget);
              daq.SEU_test_mode=(daq.SEU_test_mode-1)%3;
              gtk_button_clicked((GtkButton*)widget);

            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"SEU_pattern"))
            {
              if(!xmlStrcasecmp(prop_value, (const xmlChar *)"All_0"))daq.SEU_pattern=0;
              if(!xmlStrcasecmp(prop_value, (const xmlChar *)"All_1"))daq.SEU_pattern=1;
              if(!xmlStrcasecmp(prop_value, (const xmlChar *)"Comb_10"))daq.SEU_pattern=2;
              if(!xmlStrcasecmp(prop_value, (const xmlChar *)"Comb_01"))daq.SEU_pattern=3;
              if(!xmlStrcasecmp(prop_value, (const xmlChar *)"reset"))daq.SEU_pattern=4;
              if(!xmlStrcasecmp(prop_value, (const xmlChar *)"xml"))daq.SEU_pattern=5;
              if(!xmlStrcasecmp(prop_value, (const xmlChar *)"keep"))daq.SEU_pattern=6;
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0-SEU_pattern");
              g_assert (widget);
              printf("Setting SEU pattern to %d\n",daq.SEU_pattern);
              daq.SEU_pattern=(daq.SEU_pattern-1)%7;
              gtk_button_clicked((GtkButton*)widget);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"SEU_simul"))
            {
              daq.SEU_simul=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_SEU_simul");
              g_assert (widget);
              gtk_toggle_button_set_active((GtkToggleButton*)widget,daq.SEU_simul);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"SEU_current_limit"))
            {
              sscanf((const char*)prop_value,"%d",&daq.SEU_current_limit);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"dump_samples"))
            {
              sscanf((const char*)prop_value,"%d",&daq.dump_data);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"LVRB_autoscan"))
            {
              daq.LVRB_autoscan=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_LVRB_autoscan");
              g_assert (widget);
              gtk_toggle_button_set_active((GtkToggleButton*)widget,daq.LVRB_autoscan);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"CATIA_scan_Vref"))
            {
              daq.CATIA_scan_Vref=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_CATIA_scan_Vref");
              g_assert (widget);
              if(daq.CATIA_scan_Vref)
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
              }
              else
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
              }

            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"Rshunt_V1P2"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.Rshunt_V1P2);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_Rshunt_V1P2");
              g_assert (widget);
              sprintf(entry_text,"%.2f",daq.Rshunt_V1P2);
              gtk_entry_set_text((GtkEntry*)widget,entry_text);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"Rshunt_V2P5"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.Rshunt_V2P5);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_Rshunt_V2P5");
              g_assert (widget);
              sprintf(entry_text,"%.2f",daq.Rshunt_V2P5);
              gtk_entry_set_text((GtkEntry*)widget,entry_text);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"Tsensor_divider"))
            {
              sscanf((const char*)prop_value,"%lf",&daq.Tsensor_divider);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"use_AWG_for_calib"))
            {
              sscanf((const char*)prop_value,"%d",&daq.use_AWG);
            }
            else if(!xmlStrcmp(prop_name, (const xmlChar *)"PLL_override_Vc_bit"))
            {
              asic.DTU_override_Vc_bit[0]=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
              for(Int_t i=1; i<5; asic.DTU_override_Vc_bit[i++]=asic.DTU_override_Vc_bit[0]);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_DTU_override_Vc_bit");
              g_assert (widget);
              gtk_toggle_button_set_active((GtkToggleButton*)widget,asic.DTU_override_Vc_bit[0]);
            }
            xmlFree(prop_name);
            xmlFree(prop_value);

          }
          child = child->next;
        }
        if(daq.VICEPP_clk==0)printf("Will use clock from FEAD board, %d %x, ",daq.VICEPP_clk, daq.VICEPP_clk_tree);
        else                 printf("Will use clock from FE board, %d %x, ",daq.VICEPP_clk, daq.VICEPP_clk_tree);
        printf("with debug flags : DAQ %d, I2C %d, GTK %d, draw %d\n",daq.debug_DAQ,daq.debug_I2C,daq.debug_GTK,daq.debug_draw);
        printf("and MEM_mode set to %d %d\n",daq.MEM_mode, daq.MEM_pos);
      }

// Channel settings :
      else if (!xmlStrcmp(object_name, (const xmlChar *)"channel"))
      {
        prop_value=xmlGetProp(cur,(const xmlChar *)"id");
        sscanf((const char*)prop_value,"%d",&loc_ich);
        xmlFree(prop_value);
        if(loc_ich<1 || loc_ich>5) continue;
        prop_value=xmlGetProp(cur,(const xmlChar *)"status");
        if(!xmlStrcasecmp(prop_value,(const xmlChar *)"ON"))
        {
          enable_channel(loc_ich,TRUE);
          asic.CATIA_number[loc_ich-1]=loc_ich;
          asic.DTU_number[loc_ich-1]=loc_ich;
          sprintf(widget_name,"%1.1d_channel_status",loc_ich);
          daq.channel_number[daq.n_active_channel]=loc_ich-1;
          asic.DTU_eLink[loc_ich]=TRUE;
          daq.eLink_active|=(1<<(loc_ich-1));
// n_active_channel updated in widget action :
          GtkWidget *widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
          g_assert (widget);
          gtk_switch_set_state((GtkSwitch*)widget,TRUE);
          printf("n_active_channel : %d\n",daq.n_active_channel);
        }
        printf("Channel id %d, status %s\n",loc_ich,prop_value);
        xmlFree(prop_value);

        child = xmlFirstElementChild(cur);
        object_name=xmlGetProp(child,(const xmlChar *)"name");
        printf("First Child name : %s %s\n",child->name,object_name);
        do
        {
          if ((!xmlStrcmp(child->name, (const xmlChar *)"object")))
          {
            object_name=xmlGetProp(child,(const xmlChar *)"name");
            child_status=xmlGetProp(child,(const xmlChar *)"status");
            child_id=xmlGetProp(child,(const xmlChar *)"id");
            printf("Child name : %s %s %s, status %s\n",child->name,object_name,child_status,child_id);

            child2 = child->xmlChildrenNode;
            while (child2 != NULL)
            {
              if ((!xmlStrcmp(child2->name, (const xmlChar *)"property")))
              {
                prop_value=xmlNodeListGetString(doc, child2->xmlChildrenNode, 1);
                prop_name=xmlGetProp(child2,(const xmlChar *)"name");
                printf("property %s :  %s\n",prop_name, prop_value);
                if(!xmlStrcmp(object_name,(const xmlChar *)"CATIA"))
                {
                  sscanf((const char*)child_id,"%d",&asic.CATIA_number[loc_ich-1]);
                  if(!xmlStrcmp(prop_name,(const xmlChar *)"version"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.CATIA_version[loc_ich-1]);
                    if(asic.CATIA_version[loc_ich-1]<=14)daq.calib_mux_enabled=1;
                    sprintf(widget_name,"%1.1d_CATIA_version",loc_ich);
                    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
                    g_assert (widget);
                    sprintf(entry_text,"%2.2d",asic.CATIA_version[loc_ich-1]);
                    gtk_entry_set_text((GtkEntry*)widget,entry_text);
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"ped_G10"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.CATIA_ped_G10[loc_ich-1]);
                    if(asic.CATIA_version[loc_ich-1]<14 && asic.CATIA_ped_G10[loc_ich-1]>31)asic.CATIA_ped_G10[loc_ich-1]=31;
                    sprintf(widget_name,"%1.1d_CATIA_ped_G10",loc_ich);
                    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
                    g_assert (widget);
                    gtk_spin_button_set_value((GtkSpinButton*)widget,(double)asic.CATIA_ped_G10[loc_ich-1]);
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"ped_G1"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.CATIA_ped_G1[loc_ich-1]);
                    if(asic.CATIA_version[loc_ich-1]<14 && asic.CATIA_ped_G1[loc_ich-1]>31)asic.CATIA_ped_G1[loc_ich-1]=31;
                    sprintf(widget_name,"%1.1d_CATIA_ped_G1",loc_ich);
                    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
                    g_assert (widget);
                    gtk_spin_button_set_value((GtkSpinButton*)widget,(double)asic.CATIA_ped_G1[loc_ich-1]);
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"Vref"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.CATIA_Vref_val[loc_ich-1]);
                    sprintf(widget_name,"%1.1d_CATIA_Vref_val",loc_ich);
                    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
                    g_assert (widget);
                    gtk_spin_button_set_value((GtkSpinButton*)widget,(double)asic.CATIA_Vref_val[loc_ich-1]);
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"gain"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.CATIA_gain[loc_ich-1]);
                    if(asic.CATIA_version[loc_ich-1]<14 && asic.CATIA_gain[loc_ich-1]>1)asic.CATIA_gain[loc_ich-1]=1;
                    if(asic.CATIA_gain[loc_ich-1]==2)asic.CATIA_gain[loc_ich-1]++;
                    sprintf(widget_name,"%1.1d_CATIA_gain",loc_ich);
                    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
                    g_assert (widget);
                    if(asic.CATIA_version[loc_ich-1]<14)
                    {
                      if(asic.CATIA_gain[loc_ich-1]==0)
                        sprintf(entry_text,"500");
                      else
                        sprintf(entry_text,"400");
                    }
                    else if(asic.CATIA_version[loc_ich-1]<=20)
                    {
                      if(asic.CATIA_gain[loc_ich-1]==0)
                        sprintf(entry_text,"500");
                      else if(asic.CATIA_gain[loc_ich-1]==1)
                        sprintf(entry_text,"400");
                      else
                        sprintf(entry_text,"340");
                    }
                    else
                    {
                      if(asic.CATIA_gain[loc_ich-1]==0)
                        sprintf(entry_text,"470");
                      else if(asic.CATIA_gain[loc_ich-1]==1)
                        sprintf(entry_text,"380");
                      else
                        sprintf(entry_text,"320");
                    }
                    gtk_button_set_label((GtkButton*)widget,entry_text);
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"LPF"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.CATIA_LPF35[loc_ich-1]);
                    sprintf(widget_name,"%1.1d_CATIA_LPF35",loc_ich);
                    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
                    g_assert (widget);
                    if(asic.CATIA_LPF35[loc_ich-1]==0)
                      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
                    else
                      gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"DAC1"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.CATIA_DAC1_val[loc_ich-1]);
                    sprintf(widget_name,"%1.1d_CATIA_DAC1_val",loc_ich);
                    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
                    g_assert (widget);
                    sprintf(entry_text,"%4d",asic.CATIA_DAC1_val[loc_ich-1]);
                    gtk_entry_set_text((GtkEntry*)widget,entry_text);
                    gtk_widget_activate(widget);

                    prop_value=xmlGetProp(child2,(const xmlChar *)"status");
                    asic.CATIA_DAC1_status[loc_ich-1]=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
                    sprintf(widget_name,"%1.1d_CATIA_DAC1_status",loc_ich);
                    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
                    g_assert (widget);
                    if(asic.CATIA_DAC1_status[loc_ich-1])
                      gtk_button_set_label((GtkButton*)widget,"ON");
                    else
                      gtk_button_set_label((GtkButton*)widget,"OFF");
                    gtk_toggle_button_set_active((GtkToggleButton*)widget,asic.CATIA_DAC1_status[loc_ich-1]);
                    //gtk_widget_activate(widget);
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"DAC2"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.CATIA_DAC2_val[loc_ich-1]);
                    sprintf(widget_name,"%1.1d_CATIA_DAC2_val",loc_ich);
                    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
                    g_assert (widget);
                    sprintf(entry_text,"%4d",asic.CATIA_DAC2_val[loc_ich-1]);
                    gtk_entry_set_text((GtkEntry*)widget,entry_text);
                    gtk_widget_activate(widget);

                    prop_value=xmlGetProp(child2,(const xmlChar *)"status");
                    asic.CATIA_DAC2_status[loc_ich-1]=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
                    sprintf(widget_name,"%1.1d_CATIA_DAC2_status",loc_ich);
                    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
                    g_assert (widget);
                    if(asic.CATIA_DAC2_status[loc_ich-1])
                      gtk_button_set_label((GtkButton*)widget,"ON");
                    else
                      gtk_button_set_label((GtkButton*)widget,"OFF");
                    gtk_toggle_button_set_active((GtkToggleButton*)widget,asic.CATIA_DAC2_status[loc_ich-1]);
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"SEU_corr"))
                  {
                    asic.CATIA_SEU_corr[loc_ich-1]=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
                    sprintf(widget_name,"%1.1d_CATIA_SEU_correction",loc_ich);
                    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
                    g_assert (widget);
                    if(asic.CATIA_SEU_corr[loc_ich-1])
                      gtk_button_set_label((GtkButton*)widget,"ON");
                    else
                      gtk_button_set_label((GtkButton*)widget,"OFF");
                    gtk_toggle_button_set_active((GtkToggleButton*)widget,asic.CATIA_SEU_corr[loc_ich-1]);
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"Temp_offset"))
                  {
                    sscanf((const char*)prop_value,"%lf",&asic.CATIA_Temp_offset[loc_ich-1]);
                  }
                }
                else if(!xmlStrcmp(object_name,(const xmlChar *)"LiTEDTU"))
                {
                  sscanf((const char*)child_id,"%d",&asic.DTU_number[loc_ich-1]);
                  if(!xmlStrcmp(prop_name,(const xmlChar *)"version"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.DTU_version[loc_ich-1]);
                    sprintf(widget_name,"%1.1d_DTU_version",loc_ich);
                    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
                    g_assert (widget);
                    sprintf(entry_text,"%2.2d",asic.DTU_version[loc_ich-1]);
                    gtk_entry_set_text((GtkEntry*)widget,entry_text);
                  }
                  if(!xmlStrcmp(prop_name,(const xmlChar *)"eLink"))
                  {
                    asic.DTU_force_G10[loc_ich-1]=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"BL_G10"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.DTU_BL_G10[loc_ich-1]);
                    sprintf(widget_name,"%1.1d_DTU_BL_G10",loc_ich);
                    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
                    g_assert (widget);
                    sprintf(entry_text,"%4d",asic.DTU_BL_G10[loc_ich-1]);
                    gtk_entry_set_text((GtkEntry*)widget,entry_text);
                    gtk_widget_activate(widget);
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"BL_G1"))
                  {
                    sscanf((const char*)prop_value,"%d",&asic.DTU_BL_G1[loc_ich-1]);
                    sprintf(widget_name,"%1.1d_DTU_BL_G1",loc_ich);
                    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
                    g_assert (widget);
                    sprintf(entry_text,"%4d",asic.DTU_BL_G1[loc_ich-1]);
                    gtk_entry_set_text((GtkEntry*)widget,entry_text);
                    gtk_widget_activate(widget);
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"force_G10"))
                  {
                    asic.DTU_force_G10[loc_ich-1]=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
                    sprintf(widget_name,"%1.1d_DTU_force_G10",loc_ich);
                    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
                    g_assert (widget);
                    if(asic.DTU_force_G10[loc_ich-1])
                      gtk_button_set_label((GtkButton*)widget,"ON");
                    else
                      gtk_button_set_label((GtkButton*)widget,"OFF");
                    gtk_toggle_button_set_active((GtkToggleButton*)widget,asic.DTU_force_G10[loc_ich-1]);
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"force_G1"))
                  {
                    asic.DTU_force_G1[loc_ich-1]=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
                    sprintf(widget_name,"%1.1d_DTU_force_G1",loc_ich);
                    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
                    g_assert (widget);
                    if(asic.DTU_force_G1[loc_ich-1])
                      gtk_button_set_label((GtkButton*)widget,"ON");
                    else
                      gtk_button_set_label((GtkButton*)widget,"OFF");
                    gtk_toggle_button_set_active((GtkToggleButton*)widget,asic.DTU_force_G1[loc_ich-1]);
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"PLL"))
                  {
                    sscanf((const char*)prop_value,"%x",&asic.DTU_PLL_conf[loc_ich-1]);
                    asic.DTU_PLL_conf[loc_ich-1] &= 0x01ff;
                    asic.DTU_PLL_conf1[loc_ich-1]= asic.DTU_PLL_conf[loc_ich-1]&0x7;
                    asic.DTU_PLL_conf2[loc_ich-1]= asic.DTU_PLL_conf[loc_ich-1]>>3;
                    sprintf(widget_name,"%1.1d_DTU_PLL",loc_ich);
                    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
                    g_assert (widget);
                    sprintf(entry_text,"%2.2x",asic.DTU_PLL_conf[loc_ich-1]);
                    gtk_entry_set_text((GtkEntry*)widget,entry_text);
                    gtk_widget_activate(widget);
                  }
                  else if(!xmlStrcmp(prop_name,(const xmlChar *)"force_PLL"))
                  {
                    asic.DTU_force_PLL[loc_ich-1]=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
                    sprintf(widget_name,"%1.1d_DTU_force_PLL",loc_ich);
                    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
                    g_assert (widget);
                    if(asic.DTU_force_PLL[loc_ich-1]!=gtk_toggle_button_get_active((GtkToggleButton*)widget))
                      gtk_toggle_button_set_active((GtkToggleButton*)widget,asic.DTU_force_PLL[loc_ich-1]);
                  }
                  else if(!xmlStrcmp(prop_name, (const xmlChar *)"PLL_clock_out"))
                  {
                    asic.DTU_clk_out[loc_ich-1]=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
                  }
                  else if(!xmlStrcmp(prop_name, (const xmlChar *)"VCO_rail_mode"))
                  {
                    asic.DTU_VCO_rail_mode[loc_ich-1]=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
                  }
                  else if(!xmlStrcmp(prop_name, (const xmlChar *)"bias_ctrl_override"))
                  {
                    asic.DTU_bias_ctrl_override[loc_ich-1]=!xmlStrcasecmp(prop_value,(const xmlChar *)"ON");
                  }
                  else if(!xmlStrcmp(prop_name, (const xmlChar *)"driver_current")) 
                  {
                    sscanf((const char*)prop_value,"%d",&asic.DTU_driver_current[loc_ich-1]);
                    if(asic.DTU_driver_current[loc_ich-1]>7)asic.DTU_driver_current[loc_ich-1]=7;
                  }
                  else if(!xmlStrcmp(prop_name, (const xmlChar *)"PE_width")) // Drivers Pre-Emphasis width (*120 ps)
                  {
                    sscanf((const char*)prop_value,"%d",&asic.DTU_PE_width[loc_ich-1]);
                    if(asic.DTU_PE_width[loc_ich-1]>7)asic.DTU_PE_width[loc_ich-1]=7;
                  }
                  else if(!xmlStrcmp(prop_name, (const xmlChar *)"PE_strength")) // Drivers Pre-Emphasis strength (unknown unit ?)
                  {
                    sscanf((const char*)prop_value,"%d",&asic.DTU_PE_strength[loc_ich-1]);
                    if(asic.DTU_PE_strength[loc_ich-1]>7)asic.DTU_PE_strength[loc_ich-1]=7;
                  }
                }

                xmlFree(prop_value);
                xmlFree(prop_name);
              }
              child2 = child2->next;
            }
          }
          child=child->next;
        }while(child!=NULL);

        printf("Load config for channel %d : CATIA version : %.1f, LiTE-DTU version %.1f, bs10 0x%3.3x, bs1 0x%3.3x, pll 0x%3.3x=0x%2.2x 0x%1.1x, Vref %d, CATIA gain %d, CATIA LPF %d, force DTU PLL %d\n",
               loc_ich,asic.CATIA_version[loc_ich-1]/10.,asic.DTU_version[loc_ich-1]/10.,
               asic.DTU_BL_G10[loc_ich-1],   asic.DTU_BL_G1[loc_ich-1],
               asic.DTU_PLL_conf[loc_ich-1],  asic.DTU_PLL_conf1[loc_ich-1],asic.DTU_PLL_conf2[loc_ich-1],
               asic.CATIA_Vref_val[loc_ich-1],asic.CATIA_gain[loc_ich-1],   asic.CATIA_LPF35[loc_ich-1], asic.DTU_force_PLL[loc_ich-1]);
      }
    }

    cur = cur->next;
  }
  xmlFreeDoc(doc);

  for(Int_t i=0; i<5; i++)
  {
    sprintf(widget_name,"%d_CATIA_SEU",i+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    gtk_widget_set_sensitive(widget,FALSE);
    sprintf(widget_name,"%d_CATIA_I2C_error",i+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    gtk_widget_set_sensitive(widget,FALSE);
  }
// Update gtk menu with new parameters :
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_CATIA_calib");
  g_assert (widget);
  if(daq.n_active_channel==1 && daq.channel_number[0]==2 && daq.DTU_test_mode==1)
  {
    daq.eLink_active=0xF;
// Activate the CATIA_calib button only for test board (1 channel, 4 outputs)
    gtk_widget_set_sensitive(widget,TRUE);
    printf("Switch to test board setting !\n");
  }
  else
  {
    gtk_widget_set_sensitive(widget,FALSE);
  }
  printf("Will use I2C_shift_dev_number %d\n",daq.I2C_shift_dev_number);
  update_trigger();
}

void write_conf_to_xml_file(char *filename)
{
  char status[80];

  FILE*fd_out=fopen(filename,"w+");
  fprintf(fd_out,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
  fprintf(fd_out,"<vfe>\n");
  fprintf(fd_out,"  <object name=\"DAQ\">\n");
  sprintf(status,"OFF");
  if(daq.is_VICEPP) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"VICEPP\" status=\"%s\">%d</property>\n",status,daq.FEAD_number);
  fprintf(fd_out,"    <property name=\"clock\">%d</property>\n",daq.VICEPP_clk);
  fprintf(fd_out,"    <property name=\"custom_clock\">0x%x</property>\n",daq.VICEPP_clk_tree);
  fprintf(fd_out,"    <property name=\"resync_phase\">%d</property>\n",daq.resync_phase);
  fprintf(fd_out,"    <property name=\"test_mode\">%d</property>\n",daq.DTU_test_mode);
  fprintf(fd_out,"    <property name=\"I2C_shift_dev_number\">%d</property>\n",daq.I2C_shift_dev_number);
  fprintf(fd_out,"    <property name=\"debug_DAQ\">%d</property>\n",daq.debug_DAQ);
  fprintf(fd_out,"    <property name=\"debug_I2C\">%d</property>\n",daq.debug_I2C);
  fprintf(fd_out,"    <property name=\"debug_GTK\">%d</property>\n",daq.debug_GTK);
  fprintf(fd_out,"    <property name=\"debug_draw\">%d</property>\n",daq.debug_draw);
  fprintf(fd_out,"    <property name=\"debug_ana\">%d</property>\n",daq.debug_ana);
  fprintf(fd_out,"    <property name=\"zoom_draw\">%d</property>\n",daq.zoom_draw);
  sprintf(status,"OFF");
  if(daq.corgain) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"corgain\">%s</property>\n",status);
  sprintf(status,"OFF");
  if(daq.wait_for_ever) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"wait_for_ever\">%s</property>\n",status);
  sprintf(status,"OFF");
  if(daq.I2C_lpGBT_mode) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"I2C_lpGBT\">%s</property>\n",status);
  fprintf(fd_out,"    <property name=\"HW_delay\">%d</property>\n",daq.hw_DAQ_delay);
  fprintf(fd_out,"    <property name=\"SW_delay\">%d</property>\n",daq.sw_DAQ_delay);
  fprintf(fd_out,"    <property name=\"TP_delay\">%d</property>\n",daq.TP_delay);
  fprintf(fd_out,"    <property name=\"TP_width\">%d</property>\n",daq.TP_width);
  fprintf(fd_out,"    <property name=\"TP_level\">%d</property>\n",daq.TP_level);
  fprintf(fd_out,"    <property name=\"TP_step\">%d</property>\n",daq.TP_step);
  fprintf(fd_out,"    <property name=\"n_TP_step\">%d</property>\n",daq.n_TP_step);
  sprintf(status,"OFF");
  if(daq.TE_enable) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"TE\" pos=\"%d\" command=\"%x\">%s</property>\n",daq.TE_pos, daq.TE_command,status);
  fprintf(fd_out,"    <property name=\"nevent\">%d</property>\n",daq.nevent);
  fprintf(fd_out,"    <property name=\"nsample\">%d</property>\n",daq.nsample);
  sprintf(status,"OFF");
  if(daq.LVRB_autoscan) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"LVRB_autoscan\">%s</property>\n",status);
  sprintf(status,"OFF");
  if(daq.CATIA_scan_Vref) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"CATIA_scan_Vref\">%s</property>\n",status);
  fprintf(fd_out,"    <property name=\"Rshunt_V1P2\">%.2f</property>\n",daq.Rshunt_V1P2);
  fprintf(fd_out,"    <property name=\"Rshunt_V2P5\">%.2f</property>\n",daq.Rshunt_V2P5);
  fprintf(fd_out,"    <property name=\"Tsensor_divider\">%.3f</property>\n",daq.Tsensor_divider);
  sprintf(status,"OFF");
  if(asic.DTU_override_Vc_bit[0]==1) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"PLL_override_Vc_bit\">%s</property>\n",status);
  fprintf(fd_out,"    <property name=\"use_AWG_for_calib\">%d</property>\n",daq.use_AWG);
  fprintf(fd_out,"    <property name=\"use_MEM_mode\">%d</property>\n",daq.MEM_pos);
  fprintf(fd_out,"    <property name=\"CATIA_reg1_def\">0x%2.2x</property>\n",asic.CATIA_reg_def[0][1]);
  fprintf(fd_out,"    <property name=\"CATIA_reg2_def\">0x%2.2x</property>\n",asic.CATIA_reg_def[0][2]);
  fprintf(fd_out,"    <property name=\"CATIA_reg3_def\">0x%4.4x</property>\n",asic.CATIA_reg_def[0][3]);
  fprintf(fd_out,"    <property name=\"CATIA_reg4_def\">0x%4.4x</property>\n",asic.CATIA_reg_def[0][4]);
  fprintf(fd_out,"    <property name=\"CATIA_reg5_def\">0x%4.4x</property>\n",asic.CATIA_reg_def[0][5]);
  fprintf(fd_out,"    <property name=\"CATIA_reg6_def\">0x%2.2x</property>\n",asic.CATIA_reg_def[0][6]);
  fprintf(fd_out,"<!-- SEU_test_mode : What to do on error/reset : \"keep\" values, \"reset\" CATIA or \"restore\" user default values -->\n");
  if(daq.SEU_test_mode==0) fprintf(fd_out,"    <property name=\"SEU_test_mode\">reset</property>\n");
  if(daq.SEU_test_mode==1) fprintf(fd_out,"    <property name=\"SEU_test_mode\">keep</property>\n");
  if(daq.SEU_test_mode==2) fprintf(fd_out,"    <property name=\"SEU_test_mode\">restore</property>\n");
  if(daq.SEU_pattern==0) fprintf(fd_out,"    <property name=\"SEU_pattern\">All_0</property>\n");
  if(daq.SEU_pattern==1) fprintf(fd_out,"    <property name=\"SEU_pattern\">All_1</property>\n");
  if(daq.SEU_pattern==2) fprintf(fd_out,"    <property name=\"SEU_pattern\">Comb_10</property>\n");
  if(daq.SEU_pattern==3) fprintf(fd_out,"    <property name=\"SEU_pattern\">Comb_01</property>\n");
  if(daq.SEU_pattern==4) fprintf(fd_out,"    <property name=\"SEU_pattern\">reset</property>\n");
  if(daq.SEU_pattern==5) fprintf(fd_out,"    <property name=\"SEU_pattern\">xml</property>\n");
  if(daq.SEU_pattern==6) fprintf(fd_out,"    <property name=\"SEU_pattern\">keep</property>\n");
  sprintf(status,"OFF");
  if(daq.SEU_simul) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"SEU_simul\">%s</property>\n",status);
  fprintf(fd_out,"    <property name=\"SEU_current_limit\">%d</property>\n",daq.SEU_current_limit);
  fprintf(fd_out,"    <property name=\"dump_samples\">%d</property>\n",daq.dump_data);
  fprintf(fd_out,"  </object>\n");
  for(Int_t ich=0; ich<5; ich++)
  {
    sprintf(status,"OFF");
    if((daq.eLink_active & (1<<ich))!=0) sprintf(status,"ON");
    if(daq.n_active_channel==1 && ich!=daq.channel_number[0])sprintf(status,"OFF");
    fprintf(fd_out,"  <object name=\"channel\" id=\"%d\" status=\"%s\">\n",ich+1,status);
    fprintf(fd_out,"    <object name=\"CATIA\" id=\"%d\">\n",asic.CATIA_number[ich]);
    fprintf(fd_out,"      <property name=\"version\">%d</property>\n",asic.CATIA_version[ich]);
    fprintf(fd_out,"      <property name=\"ped_G10\">%d</property>\n",asic.CATIA_ped_G10[ich]);
    fprintf(fd_out,"      <property name=\"ped_G1\">%d</property>\n",asic.CATIA_ped_G1[ich]);
    fprintf(fd_out,"      <property name=\"Vref\">%d</property>\n",asic.CATIA_Vref_val[ich]);
    fprintf(fd_out,"      <property name=\"gain\">%d</property>\n",asic.CATIA_gain[ich]);
    fprintf(fd_out,"      <property name=\"LPF\">%d</property>\n",asic.CATIA_LPF35[ich]);
    sprintf(status,"OFF");
    if(asic.CATIA_DAC1_status[ich]) sprintf(status,"ON");
    fprintf(fd_out,"      <property name=\"DAC1\" status=\"%s\">%d</property>\n",status,asic.CATIA_DAC1_val[ich]);
    sprintf(status,"OFF");
    if(asic.CATIA_DAC2_status[ich]) sprintf(status,"ON");
    fprintf(fd_out,"      <property name=\"DAC2\" status=\"%s\">%d</property>\n",status,asic.CATIA_DAC2_val[ich]);
    sprintf(status,"OFF");
    if(asic.CATIA_SEU_corr[ich]) sprintf(status,"ON");
    fprintf(fd_out,"      <property name=\"SEU_corr\">%s</property>\n",status);
    fprintf(fd_out,"      <property name=\"Temp_offset\">%.3f</property>\n",asic.CATIA_Temp_offset[ich]);
    fprintf(fd_out,"    </object>\n");
    fprintf(fd_out,"    <object name=\"LiTEDTU\" id=\"%d\">\n",asic.DTU_number[ich]);
    fprintf(fd_out,"      <property name=\"version\">%d</property>\n",asic.DTU_version[ich]);
    sprintf(status,"OFF");
    if(asic.DTU_eLink[ich]) sprintf(status,"ON");
    fprintf(fd_out,"      <property name=\"eLink\">%s</property>\n",status);
    fprintf(fd_out,"      <property name=\"BL_G10\">%d</property>\n",asic.DTU_BL_G10[ich]);
    fprintf(fd_out,"      <property name=\"BL_G1\">%d</property>\n",asic.DTU_BL_G1[ich]);
    sprintf(status,"OFF");
    if(asic.DTU_force_G10[ich]) sprintf(status,"ON");
    fprintf(fd_out,"      <property name=\"force_G10\">%s</property>\n",status);
    sprintf(status,"OFF");
    if(asic.DTU_force_G1[ich]) sprintf(status,"ON");
    fprintf(fd_out,"      <property name=\"force_G1\">%s</property>\n",status);
    fprintf(fd_out,"      <property name=\"PLL\">0x%x</property>\n",asic.DTU_PLL_conf[ich]);
    sprintf(status,"OFF");
    if(asic.DTU_force_PLL[ich]) sprintf(status,"ON");
    fprintf(fd_out,"      <property name=\"force_PLL\">%s</property>\n",status);
    sprintf(status,"OFF");
    if(asic.DTU_clk_out[ich]) sprintf(status,"ON");
    fprintf(fd_out,"      <property name=\"PLL_clock_out\">%s</property>\n",status);
    sprintf(status,"OFF");
    if(asic.DTU_VCO_rail_mode[ich]) sprintf(status,"ON");
    fprintf(fd_out,"      <property name=\"VCO_rail_mode\">%s</property>\n",status);
    sprintf(status,"OFF");
    if(asic.DTU_bias_ctrl_override[ich]) sprintf(status,"ON");
    fprintf(fd_out,"      <property name=\"bias_ctrl_override\">%s</property>\n",status);
    fprintf(fd_out,"      <property name=\"driver_current\">%d</property>\n",asic.DTU_driver_current[ich]);
    fprintf(fd_out,"      <property name=\"PE_width\">%d</property>\n",asic.DTU_PE_width[ich]);
    fprintf(fd_out,"      <property name=\"PE_strength\">%d</property>\n",asic.DTU_PE_strength[ich]);
    fprintf(fd_out,"    </object>\n");
    fprintf(fd_out,"  </object>\n");
  }
  fprintf(fd_out,"</vfe>\n");
  fclose(fd_out);
}
