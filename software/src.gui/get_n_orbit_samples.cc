#define EXTERN extern
#include "gdaq_VFE.h"

void get_n_orbit_samples(void)
{
  uhal::HwInterface hw=devices.front();
  GtkWidget* widget;
  char widget_text[80], widget_name[80],reg_name[32];

// Read FEAD register for # orbit samples errors
  if(daq.debug_DAQ>0)printf("Getting number of samples per orbit\n");
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    sprintf(reg_name,"DEBUG1_%d",ich);
    ValWord<uint32_t> nsample=hw.getNode(reg_name).read();
    hw.dispatch();
    if(daq.debug_DAQ>1)printf("ch %d : %d samples\n",ich+1,nsample.value());
    asic.DTU_n_orbit_samples[ich]=nsample.value();
    sprintf(widget_name,"%d_n_orbit_samples",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    sprintf(widget_text,"%d",asic.DTU_n_orbit_samples[ich]);
    gtk_label_set_text((GtkLabel*)widget,widget_text);
  }
  while (gtk_events_pending()) gtk_main_iteration(); // Update menus before leaving
}
