#define EXTERN extern
#include "gdaq_VFE.h"

void init_VFE()
{
  // Now in xml file
  //daq.Vref_divider=0.8111; // VFE-V3 : 1k+4.02k
  //daq.Vref_divider=0.777; // VFE-MD : 1k+3.3k
  UInt_t device_number, val;
  UInt_t I2C_data, command;
  FILE *fcal;
  UInt_t iret=0;
  UInt_t ped_mux=0;
  if(daq.CATIA_Vcal_out)ped_mux=1;
  ValWord<uint32_t> reg;
  Double_t Vref_ref[5];
  Int_t ADC_reg_val[2][76];
  for(Int_t i=0; i<5; i++)
  {
    asic.DTU_invert_clock[i]=0;
    asic.DTU_dither[i]=FALSE;
    asic.DTU_nsample_calib_x4[i]=FALSE;
    asic.DTU_global_test[i]=FALSE;
    asic.DTU_G1_window16[i]=TRUE;
    asic.CATIA_DAC_buf_off[i]=0;
    asic.CATIA_temp_out[i]=0;
    asic.CATIA_temp_X5[i]=0;
    asic.CATIA_Vref_out[i]=0;
    Vref_ref[i]= 1.000;                             // Target Vref value for calibration level setting
    if(daq.MEM_mode)Vref_ref[i]=1.1;
  }
  GtkWidget *widget;
  char widget_name[80], widget_text[80];

// Switch ON and Initialize current monitors
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_power_ON");
  g_assert (widget);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  usleep(500000);

  uhal::HwInterface hw=devices.front();

// Update FEAD control register :
  update_trigger();

// Reset VFE board
  printf("Generate PowerUp reset 0x%8.8x\n",1*PWUP_RESETB);
  hw.getNode("VFE_CTRL").write(1*PWUP_RESETB);
  hw.getNode("VFE_CTRL").write(0*PWUP_RESETB);
  hw.dispatch();
  usleep(10000);

  printf("Prepare LiTEDTU for safe running (generate ReSync start sequence)\n");

// DTU Resync init sequence:
// For LiTEDTU version >=2.0, put outputs in sync mode with 0xaaaaaaaa
  if(daq.firmware_version<0x21090000)
  {
    printf("Reset DTU\n");
    send_ReSync_command(LiTEDTU_DTU_reset);
    printf("Reset DTU I2C\n");
    send_ReSync_command(LiTEDTU_I2C_reset);
    printf("Reset DTUTestUnit\n");
    send_ReSync_command(LiTEDTU_ADCTestUnit_reset);
    printf("Reset ADCH\n");
    send_ReSync_command(LiTEDTU_ADCH_reset);
    printf("Reset ADCL\n");
    send_ReSync_command(LiTEDTU_ADCL_reset);
  }
  else
  {
    printf("Reset PLL/DTU/DTU_I2C/DTUTestUnit\n");
    send_ReSync_command((LiTEDTU_PLL_reset<<12) | (LiTEDTU_ADCTestUnit_reset<<8) | (LiTEDTU_DTU_reset<<4) | LiTEDTU_I2C_reset);
    //send_ReSync_command(LiTEDTU_I2C_reset);
    //printf("Reset ADCH/ADCL and present SYNC patterns \n");
    printf("Reset ADCH/ADCL \n");
    send_ReSync_command((LiTEDTU_NORMAL_MODE<<8) | (LiTEDTU_ADCL_reset<<4) | LiTEDTU_ADCH_reset);
  }

  printf("I2C_shift_dev_number : %d\n",daq.I2C_shift_dev_number);
// Load default DTU registers
// Surprisingly, when we load LiTE-DTU registers, we have a calibration process. So put ped-mux in position :
  UInt_t VFE_control= DELAY_AUTO_TUNE*daq.delay_auto_tune | LVRB_AUTOSCAN*daq.LVRB_autoscan |
               PED_MUX*1 | eLINK_ACTIVE*daq.eLink_active |
               DTU_TEST_MODE*daq.DTU_test_mode | DTU_MEM_MODE*daq.MEM_mode | I2C_LPGBT_MODE*daq.I2C_lpGBT_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();
  usleep(100000); // Let some time for calibration voltages to stabilize
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    Int_t num=asic.DTU_number[ich];
    device_number=daq.I2C_LiTEDTU_type*1000+(num<<daq.I2C_shift_dev_number)+2;      // DTU sub-address
    printf("Device number : %d 0x%x, %d\n",device_number,device_number,daq.I2C_shift_dev_number);

    update_LiTEDTU_reg(ich,0);
    update_LiTEDTU_reg(ich,1);

// G10 and G1 baseline subtraction :
    update_LiTEDTU_reg(ich,5);
    update_LiTEDTU_reg(ich,6);
    printf("BL G10 %d, G1 %d\n",asic.DTU_BL_G10[ich],asic.DTU_BL_G1[ich]);

    if(asic.DTU_version[ich]<20)
    {
      I2C_data=0xff0-asic.DTU_BL_G10[ich]; // switch gain at 4080 - substrated baseline
      printf("Setting G10/G1 gain switching level for channel %d to 0x%x\n",ich,I2C_data);
      update_LiTEDTU_reg(ich,17);
      update_LiTEDTU_reg(ich,18);
      //iret=I2C_RW(hw, device_number, 17, I2C_data&0xff, 0, 3, daq.debug_I2C);
      //iret=I2C_RW(hw, device_number, 18, (I2C_data>>8)&0xf, 0, 3, daq.debug_I2C);
// PLL setting :
      update_LiTEDTU_reg(ich,9);
      update_LiTEDTU_reg(ich,8);
      //iret=I2C_RW(hw, device_number, 9, (asic.DTU_PLL_conf[ich]&0xFF), 0, 1, 0);
      //iret=I2C_RW(hw, device_number, 8, 0, 0, 2, 0);
      //iret&=0xFF;
      //iret=I2C_RW(hw, device_number, 8, (iret&0xFE)|(((asic.DTU_PLL_conf[ich]&0x100)>>8)&1), 0, 3, 0);
// PLL open or closed ?
      update_LiTEDTU_reg(ich,15);
      //iret=I2C_RW(hw, device_number, 15, 0, 0, 2, daq.debug_I2C);
      //iret=(iret&0xfd) | (asic.DTU_override_Vc_bit[ich]<<1);
      //iret=I2C_RW(hw, device_number, 15, iret, 0, 3, daq.debug_I2C);
    }
    else
    {
// 00 9f [Main control registers]
// 01 03 [ADC control registers]
// 02 04 [CLPS drivers control]
// 03 20 [CLPS drivers pre-emphasis control]
// 04 00 [CLPS receivers control]
// 05 00 [Baseline subtraction H]
// 06 00 [Baseline subtraction L]
// 07 ff [ADC saturation value registers]
// 08 0f [Baseline subtraction H]
// 09 00 [Status register 0 - READ ONLY]
// 0a 3c [Status register 1 - READ ONLY]
// 0b 88 [Bias control register 0]
// 0c 44 [Bias control register 1]
// 0d 55 [Bias control register 2]
// 0e 55 [Bias control register 3]
// 0f 00 [Bias control register 4]
// 10 3c [Bias control register 5]
// 11 32 [PLL control register 0]
// 12 88 [PLL control register 1]
// 13 88 [PLL control register 2]
// 14 00 [PLL control register 3]
// 15 0c [Synchronization pattern 31-24]
// 16 cc [Synchronization pattern 23-16]
// 17 cc [Synchronization pattern 15-8]
// 18 cf [Synchronization pattern 7-0]
// 19 00 [Test pulse duration]
      printf("Set LiTE-DTU driver %d strength to %d\n",ich+1,asic.DTU_driver_current[ich]);
      iret=I2C_RW(hw, device_number,  2, asic.DTU_driver_current[ich], 0, 1, 0);
      if(asic.DTU_PE_strength[ich]>=0)
      {
        printf("Activate LiTE-DTU %d pre-emphasis :%d %d\n",ich+1,asic.DTU_PE_width[ich],asic.DTU_PE_strength[ich]);
        iret=I2C_RW(hw, device_number,  3, 0x80 | (asic.DTU_PE_width[ich]<<3) | asic.DTU_PE_strength[ich], 0, 1, 0);
      }
      else
      {
        printf("De-activate LiTE-DTU %d pre-emphasis : %d %d\n",ich+1,asic.DTU_PE_width[ich],asic.DTU_PE_strength[ich]);
        iret=I2C_RW(hw, device_number,  3, 0x00, 0, 1, 0);
      }
      iret=I2C_RW(hw, device_number, 18, 0x88, 0, 1, 0);
      iret=I2C_RW(hw, device_number, 19, 0x88, 0, 1, 0);
      iret=I2C_RW(hw, device_number, 20, 0x00, 0, 1, 0);
// PLL reg 17 :
      iret=I2C_RW(hw, device_number, 17, 0, 0, 2, daq.debug_I2C);
      printf("Default PLL %d content of register 0x11 : 0x%2.2x\n",ich,iret&0xFF);
      printf("Prepare PLL %d for self-tuning\n", ich);
      update_LiTEDTU_reg(ich,17);

// PLL setting, usefull if we force PLL:
      update_LiTEDTU_reg(ich,16);
      update_LiTEDTU_reg(ich,15);
      usleep(10000);

// Overwrite sync pattern word : 0x0ccccccf
      iret=I2C_RW(hw, device_number,21, 0x0c, 0, 1, daq.debug_I2C);
      iret=I2C_RW(hw, device_number,22, 0xcc, 0, 1, daq.debug_I2C);
      iret=I2C_RW(hw, device_number,23, 0xcc, 0, 1, daq.debug_I2C);
      iret=I2C_RW(hw, device_number,24, 0xcf, 0, 1, daq.debug_I2C);
// TP duration
      update_LiTEDTU_reg(ich,25);
      //iret=I2C_RW(hw, device_number,25, daq.TP_width, 0, 1, daq.debug_I2C);

      printf("Setting G10/G1 gain switching level for channel %d to 0x%x\n",ich,I2C_data);
      update_LiTEDTU_reg(ich,7);
      update_LiTEDTU_reg(ich,8);
      //I2C_data=0xff0-asic.DTU_BL_G10[ich]; // switch gain at 4080 - substrated baseline
      //iret=I2C_RW(hw, device_number, 7, I2C_data&0xff, 0, 3, daq.debug_I2C);
      //iret=I2C_RW(hw, device_number, 8, (I2C_data>>8)&0xf, 0, 3, daq.debug_I2C);
    }
  }
// Force test mode for synchronization except for MEM_mode :
  //Int_t DTU_test_mode=1;
  //if(daq.MEM_mode) DTU_test_mode=0;
  VFE_control= DELAY_AUTO_TUNE*daq.delay_auto_tune | LVRB_AUTOSCAN*daq.LVRB_autoscan |
           PED_MUX*ped_mux | eLINK_ACTIVE*daq.eLink_active |
           DTU_TEST_MODE*daq.DTU_test_mode | DTU_MEM_MODE*daq.MEM_mode | I2C_LPGBT_MODE*daq.I2C_lpGBT_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();
  usleep(1000);

  //printf("Send PLL_init signal\n");
  //send_ReSync_command(LiTEDTU_DTU_reset);
  usleep(100000);
  reg = hw.getNode("RESYNC_IDLE").read();
  hw.dispatch();
  UInt_t PLL_lock=(reg.value()>>DTU_PLL_LOCK)&0x1f;
  printf("PLL lock values : 0x%x",PLL_lock);
  for(Int_t i=0; i<5; i++)printf(", %d",(PLL_lock>>i)&1);
  printf("\n");
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    Int_t num=asic.DTU_number[ich];
    device_number=daq.I2C_LiTEDTU_type*1000+(num<<daq.I2C_shift_dev_number)+2;      // DTU sub-address
    if(asic.DTU_version[ich]>=20)
    {
      iret=I2C_RW(hw, device_number, 9, 0, 0, 2, daq.debug_I2C);
      printf("PLL status registers 0x09 : 0x%2.2x, ",iret&0xff);
      iret=I2C_RW(hw, device_number, 10, 0, 0, 2, daq.debug_I2C);
      printf("0x0a : 0x%2.2x\n",iret&0xff);
    }
  }

// Once the PLL is set, reset ISERDES instances and get ready for synchronization
  hw.getNode("DELAY_CTRL").write(DELAY_TAP_DIR*1 | DELAY_RESET*1);
  hw.dispatch();
  usleep(1000);

// Launch link synchronization with LiTE-DTU-v2.0 using sync patterns :
  if(asic.DTU_version[2]>=20 && daq.DTU_test_mode==0)
  {
    hw.getNode("DTU_SYNC_PATTERN").write(0x0ccccccf); // Default synchro pattern with LiTEDTU-V2.0
    reg = hw.getNode("DTU_SYNC_PATTERN").read();
    hw.dispatch();
    send_ReSync_command(LiTEDTU_SYNC_MODE);
    usleep(1000);
    printf("Expected Sync patterns : 0x%8.8x\n",reg.value());
    hw.getNode("DELAY_CTRL").write(DELAY_TAP_DIR*1 | START_IDELAY_SYNC*0x1f);
    hw.dispatch();
    usleep(1000);
    hw.getNode("DELAY_CTRL").write(DELAY_TAP_DIR*1);
    hw.dispatch();
    send_ReSync_command(LiTEDTU_NORMAL_MODE);
    usleep(1000);
  }
  else if(daq.DTU_test_mode==1) // In test mode, we synchronize on headers
  {
    hw.getNode("DTU_SYNC_PATTERN").write(0xeaaaaaaa); // Default synchro pattern during calibration with LiTEDTU
    reg = hw.getNode("DTU_SYNC_PATTERN").read();
    hw.dispatch();
    hw.getNode("DELAY_CTRL").write(DELAY_TAP_DIR*1 | START_IDELAY_SYNC*0x1f);
    hw.dispatch();
    usleep(1000);
    hw.getNode("DELAY_CTRL").write(DELAY_TAP_DIR*1);
    hw.dispatch();
    send_ReSync_command(LiTEDTU_NORMAL_MODE);
  }
  else // For LiTE-DTU < V2.0 synchronize during calibration
  {
    hw.getNode("DTU_SYNC_PATTERN").write(0xeaaaaaaa); // Default synchro pattern during calibration with LiTEDTU
    reg = hw.getNode("DTU_SYNC_PATTERN").read();
    hw.dispatch();
    printf("Expected Sync patterns %d : 0x%8.8x\n",daq.DTU_test_mode,reg.value());
  }
  reg = hw.getNode("DELAY_CTRL").read();
  hw.dispatch();
  printf("Get sync status : 0x%8.8x, VFE  sync %d, FE sync %d, ch1 0x%2.2x, ch2 0x%2.2x, ch3 0x%2.2x, ch4 0x%2.2x, ch5 0x%2.2x\n",
          reg.value(), (reg.value()>>31)&1, (reg.value()>>30)&1,
         (reg.value()>>0)&0x3f,(reg.value()>>6)&0x3f,(reg.value()>>12)&0x3f,(reg.value()>>18)&0x3f,(reg.value()>>24)&0x3f);


// Set CATIA output mux for calibation :
// and read Vref value with XADC.
// First : disconnect temp measurement
// Next : Set Output mux
// Then : Set Vref_mux for measurement

// Read XADC register 0x40 and set the requested average to 1 in XADC
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  ValWord<uint32_t> ave  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  unsigned loc_ave=ave.value()&0xffff;
  printf("Old config register 0x40 content : %x\n",loc_ave);

  loc_ave=0x8000;
  command=DRP_WRb*1 | (0x40<<16) | loc_ave;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  ave  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  loc_ave=ave.value()&0xffff;
  printf("New config register 0x40 content : %x\n",loc_ave);
  command=DRP_WRb*1 | (0x42<<16) | 0x0400;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();

// Read  FPGA and CATIA temperatures
  get_temp();

// If we want to scan Vref values, switch OFF everything first :
  if(daq.CATIA_scan_Vref)
  {
    printf("Vref mux enabled : %d\n",daq.calib_mux_enabled);
    for(int i=0; i<daq.n_active_channel; i++) 
    {
      Int_t ich=daq.channel_number[i];
      Int_t num=asic.CATIA_number[ich];
      if(num<0)continue;
      device_number=daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+3;
      if(ich==4 && daq.I2C_shift_dev_number==4)   device_number = daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+0xb;
      iret=I2C_RW(hw, device_number, 1, 0x02, 0, 3, daq.debug_I2C);
// Set CATIA outputs with calibration levels with no Vref output
      if(asic.CATIA_version[ich]>=14)
      {
        iret=I2C_RW(hw, device_number, 5, 0x7000, 1, 3, daq.debug_I2C);
      }
    }
  }

  for(int i=0; i<daq.n_active_channel; i++) 
  {
    Int_t ich=daq.channel_number[i];
    Int_t num=asic.CATIA_number[ich];
    if(num<0)continue;
    device_number=daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+3;
    if(ich==4 && daq.I2C_shift_dev_number==4)   device_number = daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+0xb;

    iret=I2C_RW(hw, device_number, 1, 0x02, 0, 3, daq.debug_I2C);

    printf("ch %d : ped G10 %d, ped G1 %d, gain %d, lpf35 %d\n",ich+1,asic.CATIA_ped_G10[ich],asic.CATIA_ped_G1[ich],asic.CATIA_gain[ich],asic.CATIA_LPF35[ich]);
    if(asic.CATIA_version[ich]>=14)
    {
// Set pedestal values, gain and LPF
      I2C_data=asic.CATIA_gain[ich]<<14 | asic.CATIA_ped_G1[ich]<<8 | asic.CATIA_ped_G10[ich]<<2 | asic.CATIA_LPF35[ich]<<1 | 1;
      iret=I2C_RW(hw, device_number, 3, I2C_data, 0, 3, daq.debug_I2C);
// Set CATIA outputs with calibration levels with no Vref output
      iret=I2C_RW(hw, device_number, 5, 0x7000, 1, 3, daq.debug_I2C);
      printf("Set CATIA outputs with calibration levels : 0x%4.4x\n",iret&0xFFFF);

      if(daq.CATIA_scan_Vref)
      {
        printf("Scan CATIA setting to reach Vref_reg = %.3f\n",Vref_ref[ich]);
// present Vref to XADC
        if(asic.CATIA_version[ich]==14)
        {
// External multiplexer for version 1.4
          printf("Channel %d : external mux for Vref measurement\n",ich);
          hw.getNode("VREF_MUX_CTRL").write(1<<ich);
          hw.dispatch();
        }
        else
        {
// Internal multiplexer for version >= 2.0 (selected by I2C)
          hw.getNode("VREF_MUX_CTRL").write(0); // Remove all signals from other channels if any
          hw.dispatch();
          printf("Channel %d : internal mux for Vref measurement\n",ich);
          printf("Present Vref on Temp line\n");
          val=I2C_RW(hw, device_number, 1, 0x02, 0, 3, daq.debug_I2C); // switch OFF temp output 
          val=I2C_RW(hw, device_number, 5, 0x3000, 1, 3, daq.debug_I2C); // switch ON Vref output
        }
        usleep(100000); // Let some time for calibration voltages to stabilize
// And read Vref value on temp line :
        Int_t XADC_Temp=0x11;
        if(daq.is_VICEPP)XADC_Temp=0x18;
        ValWord<uint32_t> temp;
        Int_t average=128,n_Vref=15;
        printf("CATIA %d : default Vref value from XADC = ",num);
        Double_t Vref_best=0., dVref=99999.;
        asic.CATIA_Vref_val[ich]=0;
        for(int imeas=0; imeas<n_Vref; imeas++)
        {
          double ave_val=0.;
          Int_t loc_meas=imeas;
          if(imeas>0)loc_meas=imeas+1;
          command= (loc_meas<<4) | 0xF;
          val=I2C_RW(hw, device_number, 6, command,0, 3, 0);
          usleep(10000);

          for(int iave=0; iave<average; iave++)
          {
            command=DRP_WRb*0 | (XADC_Temp<<16);
            hw.getNode("DRP_XADC").write(command);
            temp  = hw.getNode("DRP_XADC").read();
            hw.dispatch();
            double loc_val=double((temp.value()&0xffff)>>4)/4096.;
            //printf("%f\n",loc_val);
            ave_val+=loc_val;
          }
          ave_val/=average;
          ave_val/=daq.Tsensor_divider;
          if(fabs(ave_val-Vref_ref[ich])<dVref)
          {
            dVref=fabs(ave_val-Vref_ref[ich]);
            asic.CATIA_Vref_val[ich]=loc_meas;
            Vref_best=ave_val;
          }
          printf(" %.2f",ave_val*1000.);
          //system("stty raw");
          //char cdum=getchar();
          //system("stty -raw");
        }
        //if(ich>0)DAC_Vref_best[ich]=4;
        printf(" mV, best=%.2f, DAC=%d\n",Vref_best*1000.,asic.CATIA_Vref_val[ich]);

// Put back CATIA output in high Z :
        if(asic.CATIA_version[ich]>=14)
        {
          iret=I2C_RW(hw, device_number, 5, 0x7000, 1, 3, daq.debug_I2C);
        }
      }
      I2C_data= (asic.CATIA_Vref_val[ich]<<4) | 0xF;
      val=I2C_RW(hw, device_number, 6, I2C_data,0, 3, 0);
      sprintf(widget_name,"%1.1d_CATIA_Vref_val",ich+1);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      gtk_spin_button_set_value((GtkSpinButton*)widget,(double)asic.CATIA_Vref_val[ich]);
    }
    else
    {
      I2C_data= asic.CATIA_ped_G1[ich]<<8 | asic.CATIA_ped_G10[ich]<<3 | asic.CATIA_gain[ich]<<2 | asic.CATIA_LPF35[ich]<<1 | 1;
      iret=I2C_RW(hw, device_number, 3, I2C_data, 0, 3, daq.debug_I2C);
    }
  }

  Int_t Vref_mux_ctrl_done=0;
  for(Int_t i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    Int_t num=asic.CATIA_number[ich];
    if(num<0)continue;
    if(asic.CATIA_version[num]==14 && Vref_mux_ctrl_done==0)
    {
// Put back in High Z
      hw.getNode("VREF_MUX_CTRL").write(0);
      hw.dispatch();
      Vref_mux_ctrl_done=1;
    }
    if(asic.CATIA_version[num]>=14)
    {
      device_number=daq.I2C_CATIA_type*1000+(num<<daq.I2C_shift_dev_number)+3;
      val=I2C_RW(hw, device_number, 1, 0x02, 0, 3, daq.debug_I2C); // switch OFF temp output
      val=I2C_RW(hw, device_number, 5, 0x7000, 1, 3, daq.debug_I2C); // switch OFF Vref output (>=V2.0) and let calibration voltages on CATIA outputs
    }
  }
  usleep(10000);

// Calibrate ADC with optimized Vref:
  calib_ADC();
  //while(TRUE)
  //{
  //  calib_ADC();
  //  usleep(5000000);
  //}

// Reset Test unit to get samples in right order :
  send_ReSync_command(LiTEDTU_ADCTestUnit_reset);

  if(daq.use_ref_ADC_calib)
  {
    printf("Loading ADC calibration coefficient from ref file\n");
    fcal=fopen("ref_VFE_calib_reg.dat","r");
    for(int ich=0; ich<5; ich++)
    {
      for(int ireg=0; ireg<75; ireg++)
      {
        fscanf(fcal,"%d %d\n",&ADC_reg_val[0][ireg], &ADC_reg_val[1][ireg]);
      }
      for(int iADC=0; iADC<2; iADC++)
      {
        Int_t num=asic.DTU_number[ich];
        int device_number=daq.I2C_LiTEDTU_type*1000+(num<<daq.I2C_shift_dev_number)+iADC;
        for(int ireg=0; ireg<75; ireg++)
        {
          iret=I2C_RW(hw, device_number, ireg, ADC_reg_val[iADC][ireg], 0, 1, daq.debug_I2C);
        }
      }
    }
    fclose(fcal);
  }

// Restore CAL mux in normal position after calibration
  VFE_control= DELAY_AUTO_TUNE*daq.delay_auto_tune | LVRB_AUTOSCAN*daq.LVRB_autoscan |
               PED_MUX*ped_mux | eLINK_ACTIVE*daq.eLink_active |
               DTU_TEST_MODE*daq.DTU_test_mode | DTU_MEM_MODE*daq.MEM_mode | I2C_LPGBT_MODE*daq.I2C_lpGBT_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();
// And wait for voltage stabilization
  usleep(100000);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_CATIA_calib");
  g_assert (widget);
  if(daq.n_active_channel==1 && daq.channel_number[0]==2 && daq.DTU_test_mode==1)
  {
    daq.eLink_active=0xF;
// Activate the CATIA_calib button only for test board (1 channel, 4 outputs)
    gtk_widget_set_sensitive(widget,TRUE);
    printf("Switch to test board setting !\n");
  }
  else
  {
    gtk_widget_set_sensitive(widget,FALSE);
  }
  printf("Will use I2C_shift_dev_number %d\n",daq.I2C_shift_dev_number);
  for(Int_t i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    update_CATIA_reg(ich,1);
    update_CATIA_reg(ich,3);
    update_CATIA_reg(ich,4);
    update_CATIA_reg(ich,5);
    update_CATIA_reg(ich,6);
  }
  update_trigger();
// Re-instantiate the ReSyns_data content
  send_ReSync_command(daq.ReSync_data);
  get_CRC_errors(1);
}
