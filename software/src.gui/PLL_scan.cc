#define EXTERN extern
#include "gdaq_VFE.h"

void PLL_scan(void)
{
  uhal::HwInterface hw=devices.front();

  ValWord<uint32_t> address, reg;
  UInt_t command;
  ValWord<uint32_t> free_mem;
  GtkWidget *widget;
  char widget_name[80], widget_content[80];

// We switch in test_mode to simplify the link synchronization, whatever the DTU version
  UInt_t VFE_control= DELAY_AUTO_TUNE*1 | LVRB_AUTOSCAN*0 | eLINK_ACTIVE*daq.eLink_active | DTU_TEST_MODE*1;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();

  UInt_t sync_pattern=0;

  Int_t pll_conf;
  Int_t pll_conf1_min=0;
  Int_t pll_conf1_max=0x3f;
  Int_t pll_conf2_min=0;
  Int_t pll_conf2_max=0x7;

// First put PLLs in force mode for V2.0 :
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    sprintf(widget_name,"%d_DTU_force_PLL",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  }
  //widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_DTU_toggle_Vc_bit");
  //g_assert (widget);
  //gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);

  hw.getNode("DTU_SYNC_PATTERN").write(sync_pattern); // Default synchro pattern during calibration with LiTEDTU
  reg = hw.getNode("DTU_SYNC_PATTERN").read();
  hw.dispatch();
  printf("Expected Sync patterns : 0x%8.8x, test mode %d\n",reg.value(),daq.DTU_test_mode);

// Put LiTE-DTUs in sync mode
  send_ReSync_command(LiTEDTU_SYNC_MODE);
  usleep(1000);

  Int_t first_good_conf[5]={-1,-1,-1,-1,-1}, last_good_conf[5]={-1,-1,-1,-1,-1};
  Int_t conf[512], nconf;
  printf("PLL sync status for conf :\n");
  nconf=0;
  for(Int_t pll_conf1=pll_conf1_min; pll_conf1<=pll_conf1_max; pll_conf1=(pll_conf1<<1)+1)
  {
    for(Int_t pll_conf2=pll_conf2_min; pll_conf2<=pll_conf2_max; pll_conf2++)
    {
      conf[nconf]=(pll_conf1<<3)|pll_conf2;
      nconf++;
    }
  }
  nconf=0;
  for(Int_t pll_conf1=pll_conf1_min; pll_conf1<=pll_conf1_max; pll_conf1=(pll_conf1<<1)+1)
  {
    for(Int_t pll_conf2=pll_conf2_min; pll_conf2<=pll_conf2_max; pll_conf2++)
    {
      pll_conf=(pll_conf1<<3)|pll_conf2;
      for(int i=0; i<daq.n_active_channel; i++)
      {
        Int_t ich=daq.channel_number[i];
        sprintf(widget_name,"%d_DTU_PLL",ich+1);
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
        sprintf(widget_content,"%x",pll_conf);
        gtk_entry_set_text((GtkEntry*)widget,widget_content);
        gtk_widget_activate(widget);
      }
      while (gtk_events_pending()) gtk_main_iteration(); // Update menus

// Let some time for PLL to stabilize ?
      usleep(100000);

// Once the PLL is set, reset ISERDES instances and get ready for synchronization
      command=DELAY_TAP_DIR*1 | DELAY_RESET*1;
      hw.getNode("DELAY_CTRL").write(command);
      hw.dispatch();
      usleep(1000);

// Launch link synchronization in test mode :
      command=DELAY_TAP_DIR*1 | START_IDELAY_SYNC*0x1fu;
      hw.getNode("DELAY_CTRL").write(command);
      hw.dispatch();
      usleep(1000);
      command=DELAY_TAP_DIR*1;
      hw.getNode("DELAY_CTRL").write(command);
      hw.dispatch();

// And look at sync status :
      reg = hw.getNode("DELAY_CTRL").read();
      hw.dispatch();
      printf("pll_conf 0x%3.3x, 0x%2.2x, 0x%1.1x : ",pll_conf,pll_conf1,pll_conf2);
      printf("sync status : 0x%8.8x, VFE  sync %d, FE sync %d, ch1 0x%2.2x, ch2 0x%2.2x, ch3 0x%2.2x, ch4 0x%2.2x, ch5 0x%2.2x\n",
              reg.value(), (reg.value()>>31)&1, (reg.value()>>30)&1,
             (reg.value()>>0)&0x3f,(reg.value()>>6)&0x3f,(reg.value()>>12)&0x3f,(reg.value()>>18)&0x3f,(reg.value()>>24)&0x3f);
      for(Int_t ich=0; ich<5; ich++)
      {
        if((daq.eLink_active&(1<<ich))==0)continue;
        if(((reg.value()>>(ich*6))&0x3f)!=0x1f)
        {
          printf(" X");
          if(first_good_conf[ich]==-1)first_good_conf[ich]=nconf;
          last_good_conf[ich]=nconf;
        }
        else
        {
          printf(" -");
        }
      }
      printf("\n");
      nconf++;
    }
  }
 
// Update DTU registers through GUI :
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i];
    Int_t pll_conf=0x1f;
    if(first_good_conf[ich]>=0)
    {
      Int_t iconf=(first_good_conf[ich]+last_good_conf[ich])/2;
      pll_conf=conf[iconf];
    }
    sprintf(widget_name,"%d_DTU_PLL",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    sprintf(widget_content,"%x",pll_conf);
    gtk_entry_set_text((GtkEntry*)widget,widget_content);
    gtk_widget_activate(widget);
    sprintf(widget_name,"%d_DTU_force_PLL",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  }
  //send_ReSync_command(LiTEDTU_NORMAL_MODE);

// Re-init VFE board after scan :
  bool scan_Vref=daq.CATIA_scan_Vref;
  daq.CATIA_scan_Vref=FALSE;
  init_VFE();
  daq.CATIA_scan_Vref=scan_Vref;
}
