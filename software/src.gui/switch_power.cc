#define EXTERN extern
#include <gtk/gtk.h>
#include "gdaq_VFE.h"

void switch_power(bool status)
{
  UInt_t device_number, iret;
  GtkWidget* widget;
  char content[80];

  uhal::HwInterface hw=devices.front();
  if(status)
  {
    printf("Switch ON VFE power supply\n");
    for(Int_t iLVR=0; iLVR<2; iLVR++)
    {
      if(iLVR==0){device_number=0x67; if(daq.debug_DAQ)printf("V2P5 :\n");}
      else       {device_number=0x6C; if(daq.debug_DAQ)printf("V1P2 :\n");}
    // Put LVRB in continuous ADC mode, monitor sense+, and power calculation with sense+
      iret=I2C_RW(hw, device_number, 0x00, 0x05,0, 3, daq.debug_I2C);
      if(daq.debug_DAQ)printf("New content of control register : 0x%x\n",iret&0xffff);
// Max power limit
      iret=I2C_RW(hw, device_number, 0x0e, 0xff,0, 1, daq.debug_I2C);
      iret=I2C_RW(hw, device_number, 0x0f, 0xff,0, 1, daq.debug_I2C);
      iret=I2C_RW(hw, device_number, 0x10, 0xff,0, 1, daq.debug_I2C);
// Min power limit
      iret=I2C_RW(hw, device_number, 0x11, 0x00,0, 1, daq.debug_I2C);
      iret=I2C_RW(hw, device_number, 0x12, 0x00,0, 1, daq.debug_I2C);
      iret=I2C_RW(hw, device_number, 0x13, 0x00,0, 1, daq.debug_I2C);
// Max ADin limit
      iret=I2C_RW(hw, device_number, 0x2E, 0xFFF0,1, 3, daq.debug_I2C);
// Min ADin limit
      iret=I2C_RW(hw, device_number, 0x30, 0x0000,1, 3, daq.debug_I2C);
// Max Vin limit
      iret=I2C_RW(hw, device_number, 0x24, 0xFFF0,1, 3, daq.debug_I2C);
// Min Vin limit
      iret=I2C_RW(hw, device_number, 0x26, 0x0000,1, 3, daq.debug_I2C);
// Max current limit
      iret=I2C_RW(hw, device_number, 0x1A, 0xFFF0,1, 3, daq.debug_I2C);
      if(daq.debug_DAQ)printf("New max threshold on current 0x%x\n",iret&0xffff);
// Min current limit
      iret=I2C_RW(hw, device_number, 0x1C, 0x0000,1, 3, daq.debug_I2C);
      if(daq.debug_DAQ)printf("New min threshold on current 0x%x\n",iret&0xffff);
// Read and clear Fault register :
      // Alert register
      iret=I2C_RW(hw, device_number, 0x4, 0x00,0, 2, daq.debug_I2C);
      if(daq.debug_DAQ)printf("Alert register content : 0x%x\n",iret&0xffff);
      iret=I2C_RW(hw, device_number, 0x4, 0x00,0, 2, daq.debug_I2C);
      if(daq.debug_DAQ)printf("Alert register content : 0x%x\n",iret&0xffff);
// Set Alert register
      // Alert on Dsense overflow
      iret=I2C_RW(hw, device_number, 0x01, 0x20,0, 3, daq.debug_I2C);
      if(daq.debug_DAQ)printf("New alert bit pattern : 0x%x\n", iret&0xffff);
    }
    if(daq.debug_DAQ)printf("\n");
    usleep(500000);
    printf("Redo pwup_reset and Start LVRB auto scan mode\n");
    hw.getNode("VFE_CTRL").write(PWUP_RESETB*1);
    hw.getNode("VFE_CTRL").write(0);
    hw.getNode("VFE_CTRL").write(LVRB_AUTOSCAN*1);
    hw.dispatch();

    get_consumption();
    /*
    iret=I2C_RW(hw, 0x67, 0x14, 0,1, 2, daq.debug_I2C); // Read Dsense value
    Double_t I2P5=((iret>>4)&0xfff)/4096.*102.4/daq.Rshunt_V2P5;
    printf("CATIA V2P5 current : %8.8x, %.2f mA\n",iret,I2P5);
    iret=I2C_RW(hw, 0x6C, 0x14, 0,1, 2, daq.debug_I2C); // Read Dsense value
    Double_t I1P2=((iret>>4)&0xfff)/4096.*102.4/daq.Rshunt_V1P2;
    printf("CATIA V1P2 current : %8.8x, %.2f mA\n",iret,I1P2);
    iret=I2C_RW(hw, 0x67, 0x28, 0,1, 2, daq.debug_I2C);
    Double_t V2P5=((iret>>4)&0xfff)/4096.*2.048*2.;
    printf("VFE V2P5 measured : %.3f\n",V2P5);
    iret=I2C_RW(hw, 0x6C, 0x28, 0,1, 2, daq.debug_I2C);
    Double_t V1P2=((iret>>4)&0xfff)/4096.*2.048;
    printf("VFE V1P2 measured : %.3f\n",V1P2);

    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_V2P5_consumption");
    g_assert (widget);
    sprintf(content,"%5.1f mA",I2P5);
    gtk_label_set_text((GtkLabel*)widget,content);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_V1P2_consumption");
    g_assert (widget);
    sprintf(content,"%5.1f mA",I1P2);
    gtk_label_set_text((GtkLabel*)widget,content);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_V2P5_value");
    g_assert (widget);
    sprintf(content,"%.3f V",V2P5);
    gtk_label_set_text((GtkLabel*)widget,content);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_V1P2_value");
    g_assert (widget);
    sprintf(content,"%.3f V",V1P2);
    gtk_label_set_text((GtkLabel*)widget,content);
    */
    for(Int_t i=0; i<5; i++)
    {
      daq.delay_val[i]=0;
      daq.bitslip_val[i]=0;
      daq.byteslip_val[i]=0;
    }
    daq.selected_channel=-1;
    GtkWidget *widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_selected_channel");
    gtk_button_clicked((GtkButton*)widget);

    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_power_ON");
    g_assert (widget);
    gtk_button_set_label((GtkButton*)widget,"Power ON");
  }
  else
  {
    printf("Switch OFF VFE power supply\n");
// First ask to generate alert with overcurrent
    iret=I2C_RW(hw,0x67,0x01,0x20,0,1,daq.debug_I2C);
    iret=I2C_RW(hw,0x6C,0x01,0x20,0,1,daq.debug_I2C);
// Now, generate a current overflow :
    iret=I2C_RW(hw,0x67,0x03,0x20,0,1,daq.debug_I2C);
    iret=I2C_RW(hw,0x6C,0x03,0x20,0,1,daq.debug_I2C);
    usleep(1000000);
// Read current values for control :
    iret=I2C_RW(hw, 0x67, 0x14, 0,1, 2, daq.debug_I2C); // Read Dsense value
    Double_t I2P5=((iret>>4)&0xfff)/4096.*102.4/daq.Rshunt_V2P5;
    printf("VFE V2P5 current : %8.8x, %.2f mA\n",iret,I2P5);
    iret=I2C_RW(hw, 0x6C, 0x14, 0,1, 2, daq.debug_I2C); // Read Dsense value
    Double_t I1P2=((iret>>4)&0xfff)/4096.*102.4/daq.Rshunt_V1P2;
    printf("VFE V1P2 current : %8.8x, %.2f mA\n",iret,I1P2);
    iret=I2C_RW(hw, 0x67, 0x28, 0,1, 2, 0);
    Double_t V2P5=((iret>>4)&0xfff)/4096*2.048*2.;
    printf("VFE V2P5 measured : %.3f\n",V2P5);
    iret=I2C_RW(hw, 0x6C, 0x28, 0,1, 2, 0);
    Double_t V1P2=((iret>>4)&0xfff)/4096*2.048;
    printf("VFE V1P2 measured : %.3f\n",V1P2);

    char content[80];
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_V2P5_consumption");
    g_assert (widget);
    sprintf(content,"%5.1f mA",I2P5);
    gtk_label_set_text((GtkLabel*)widget,content);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_V1P2_consumption");
    g_assert (widget);
    sprintf(content,"%5.1f mA",I1P2);
    gtk_label_set_text((GtkLabel*)widget,content);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_V2P5_value");
    g_assert (widget);
    sprintf(content,"%.3f V",V2P5);
    gtk_label_set_text((GtkLabel*)widget,content);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_V1P2_value");
    g_assert (widget);
    sprintf(content,"%.3f V",V1P2);
    gtk_label_set_text((GtkLabel*)widget,content);

    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_power_ON");
    g_assert (widget);
    gtk_button_set_label((GtkButton*)widget,"Power OFF");
  }
}

void get_consumption(void)
{
  GtkWidget* widget;
  char content[80];

  if(daq.daq_initialized<0) return;

  uhal::HwInterface hw=devices.front();
  Int_t iret=I2C_RW(hw, 0x67, 0x14, 0,1, 2, daq.debug_I2C); // Read Dsense value
  Double_t I2P5=((iret>>4)&0xfff)/4096.*102.4/daq.Rshunt_V2P5;
  printf("CATIA V2P5 current : %8.8x, %.2f mA\n",iret,I2P5);
  iret=I2C_RW(hw, 0x6C, 0x14, 0,1, 2, daq.debug_I2C); // Read Dsense value
  Double_t I1P2=((iret>>4)&0xfff)/4096.*102.4/daq.Rshunt_V1P2;
  printf("CATIA V1P2 current : %8.8x, %.2f mA\n",iret,I1P2);
  iret=I2C_RW(hw, 0x67, 0x28, 0,1, 2, daq.debug_I2C);
  Double_t V2P5=((iret>>4)&0xfff)/4096.*2.048*2.;
  printf("VFE V2P5 measured : %.3f\n",V2P5);
  iret=I2C_RW(hw, 0x6C, 0x28, 0,1, 2, daq.debug_I2C);
  Double_t V1P2=((iret>>4)&0xfff)/4096.*2.048;
  printf("VFE V1P2 measured : %.3f\n",V1P2);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_V2P5_consumption");
  g_assert (widget);
  sprintf(content,"%5.1f mA",I2P5);
  gtk_label_set_text((GtkLabel*)widget,content);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_V1P2_consumption");
  g_assert (widget);
  sprintf(content,"%5.1f mA",I1P2);
  gtk_label_set_text((GtkLabel*)widget,content);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_V2P5_value");
  g_assert (widget);
  sprintf(content,"%.3f V",V2P5);
  gtk_label_set_text((GtkLabel*)widget,content);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_V1P2_value");
  g_assert (widget);
  sprintf(content,"%.3f V",V1P2);
  gtk_label_set_text((GtkLabel*)widget,content);
}

