#FEAD/VICEPP config : 0 Xpoint_config/eLink_active
# Cross point config
# 0          = clock from FEAD/VICE++
# 1          = clock from FE
# 0xuuuuuuu2 = custon clock setting
# 3          = keep default clock settings made by switches
#      DTU test mode/debug_DAQ/debug_I2C/debug_GTK/debug_draw/corgain/wait_for_ever
0 0 1f 1 1 0 1 1 1 0
# Channel number/CATIA_version/LiTE-DTU_version/ped G10/ped G1/baseline G10/baseline G1/PLLconf/DAC_Vref/CATIA gain/CATIA LPF/force_PLL
# R340-LPF35:
# Ped ~50 :
3 20 20 36 36 0  0 3c 2 3 1 0
