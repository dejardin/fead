void get_shape()
{
  TFile *tf1=new TFile("/data/cms/ecal/fe/vfe_ADC160/test/laser_analysis.20170208.01.root");
  TFile *tf2=new TFile("/data/cms/ecal/fe/vfe_ADC160/test/laser_analysis.20170208.06.root");
  TProfile *p1,*p2;
  TH1D *hshape1[5], *hshape2[5], *hped[5];
  TCanvas *c[5];
  for(int ich=0; ich<5; ich++)
  {
    char hname[80];
    sprintf(hname,"shape_ch%d",ich);
    tf1->cd();
    gDirectory->GetObject(hname,p1);
    tf2->cd();
    gDirectory->GetObject(hname,p2);
    double q1=p1->GetMaximum();
    double q2=p2->GetMaximum();
    sprintf(hname,"clean_shape_high_ch%d",ich);
    hshape1[ich]=new TH1D(hname,hname,3000,0.,600.);
    sprintf(hname,"clean_shape_low_ch%d",ich);
    hshape2[ich]=new TH1D(hname,hname,3000,0.,600.);
    sprintf(hname,"sync_noise_ch%d",ich);
    hped[ich]=new TH1D(hname,hname,3000,0.,600.);
    for(int i=0; i<3000; i++)
    {
      double y1=p1->GetBinContent(i+1);
      double y2=p2->GetBinContent(i+1);
      double signal1=(y1-y2)*q1/(q1-q2);
      double signal2=(y1-y2)*q2/(q1-q2);
      double noise=(y2*q1-y1*q2)/(q1-q2);
      hshape1[ich]->SetBinContent(i+1,signal1);
      hshape2[ich]->SetBinContent(i+1,signal2);
      hped[ich]->SetBinContent(i+1,noise);
    }
    p1->SetLineColor(kRed);
    p1->SetMarkerColor(kRed);
    p2->SetLineColor(kBlue);
    p2->SetMarkerColor(kBlue);
    hshape1[ich]->SetLineColor(kMagenta);
    hshape1[ich]->SetMarkerColor(kMagenta);
    hshape2[ich]->SetLineColor(kCyan);
    hshape2[ich]->SetMarkerColor(kCyan);
    hped[ich]->SetLineColor(kGreen);
    hped[ich]->SetMarkerColor(kGreen);
    c[ich]=new TCanvas();
    c[ich]->cd();
    p1->Draw();
    p2->Draw("same");
    hshape1[ich]->Draw("same");
    hshape2[ich]->Draw("same");
    hped[ich]->Draw("same");
    c[ich]->Update();
  }
  TFile *tshape=new TFile("/data/cms/ecal/fe/vfe_ADC160/test/clean_shapes_20170208.tmp.root","recreate");
  for(int ich=0; ich<5; ich++)
  {
    c[ich]->Write();
    hshape1[ich]->Write();
    hshape2[ich]->Write();
    hped[ich]->Write();
  }
  tshape->Close();
}

