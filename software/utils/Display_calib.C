#include <stdio.h>
#include <stdlib.h>
#include "TPad.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TProfile.h"
void Display_calib(Int_t rev=2)
{
  Int_t color=kBlue;
  if(rev==1)color=kGreen;
  if(rev==2)color=kRed;
  char *line=NULL;
  size_t len;
  char fname[132];
  Double_t Temp[3];
  Int_t nVref;
  Double_t Vref[15];
  Double_t Vref_target=1000.;
  Int_t nDAC;
  Double_t DAC_val[33], Vdac[2][33];
  Double_t Vdac_slope[2], Vdac_offset[2];
  Double_t Vbuf_slope[2], Vbuf_offset[2];
  Int_t nPed;
  Double_t Ped_val[2][32];
  Double_t Ped_offset[2], Ped_slope[2];
  Double_t Noise_G1[2][2], Noise_G10[2][2];
  Double_t INL_min[2],INL_max[2];
  Double_t CATIA_gain[2][2];
  Double_t G10oG1, R500, R400, Rconv_G1, Rconv_G10;
  TH1D *hVref_init=new TH1D("Vref_reg_init","Vref_reg_init",80,800.,1200.);
  TH1D *hVref_all=new TH1D("Vref_reg_all","Vref_reg_all",80,800.,1200.);
  TH1D *hVref_best=new TH1D("Vref_reg_best","Vref_reg_best",80,800.,1200.);
  TH1D *hPed_offset_G1=new TH1D("Ped_offset_G1","Ped_offset_G1",100,-620.,-520.);
  hPed_offset_G1->SetLineColor(color);
  hPed_offset_G1->SetLineWidth(2);
  TH1D *hPed_offset_G10=new TH1D("Ped_offset_G10","Ped_offset_G10",100,-620.,-520.);
  hPed_offset_G10->SetLineColor(color);
  hPed_offset_G10->SetLineWidth(2);
  TH1D *hPed_slope_G1=new TH1D("Ped_slope_G1","Ped_slope_G1",100,-3.,-2.);
  hPed_slope_G1->SetLineColor(color);
  hPed_slope_G1->SetLineWidth(2);
  TH1D *hPed_slope_G10=new TH1D("Ped_slope_G10","Ped_slope_G10",100,-3.,-2.);
  hPed_slope_G10->SetLineColor(color);
  hPed_slope_G10->SetLineWidth(2);
  TH1D *hDAC1_slope=new TH1D("DAC1_slope","DAC1_slope",100,0.20,0.30);
  hDAC1_slope->SetLineColor(color);
  hDAC1_slope->SetLineWidth(2);
  TH1D *hDAC2_slope=new TH1D("DAC2_slope","DAC2_slope",100,0.20,0.30);
  hDAC2_slope->SetLineColor(color);
  hDAC2_slope->SetLineWidth(2);
  TH1D *hDAC1_offset=new TH1D("DAC1_offset","DAC1_offset",100,-20.,20.0);
  hDAC1_slope->SetLineColor(color);
  hDAC1_slope->SetLineWidth(2);
  TH1D *hDAC2_offset=new TH1D("DAC2_offset","DAC2_offset",100,-20.,20.0);
  hDAC2_slope->SetLineColor(color);
  hDAC2_slope->SetLineWidth(2);
  TH1D *hBUF1_slope=new TH1D("BUF1_slope","BUF1_slope",100,0.20,0.30);
  hBUF1_slope->SetLineColor(color);
  hBUF1_slope->SetLineWidth(2);
  TH1D *hBUF2_slope=new TH1D("BUF2_slope","BUF2_slope",100,0.20,0.30);
  hBUF2_slope->SetLineColor(color);
  hBUF2_slope->SetLineWidth(2);
  TH1D *hBUF1_offset=new TH1D("BUF1_offset","BUF1_offset",100,600.,700.0);
  hBUF1_offset->SetLineColor(color);
  hBUF1_offset->SetLineWidth(2);
  TH1D *hBUF2_offset=new TH1D("BUF2_offset","BUF2_offset",100,600.,700.0);
  hBUF2_offset->SetLineColor(color);
  hBUF2_offset->SetLineWidth(2);
  TH1D *hBUF_gain=new TH1D("BUF_gain","BUF_gain",100,0.95,1.05);
  hBUF_gain->SetLineColor(color);
  hBUF_gain->SetLineWidth(2);
  TH1D *hG10=new TH1D("G10","G10",100,9.,12.);
  hG10->SetLineColor(color);
  hG10->SetLineWidth(2);
  TH1D *hR400=new TH1D("R400","R400",100,400.,550.);
  hR400->SetLineColor(color);
  hR400->SetLineWidth(2);
  TH1D *hR500=new TH1D("R500","R500",100,500.,700.);
  hR500->SetLineColor(color);
  hR500->SetLineWidth(2);
  TH1D *hRconv_G1=new TH1D("Rconv_G1","Rconv_G1",100,260.,300.);
  hRconv_G1->SetLineColor(color);
  hRconv_G1->SetLineWidth(2);
  TH1D *hRconv_G10=new TH1D("Rconv_G10","Rconv_G10",100,2200.,2600.);
  hRconv_G10->SetLineColor(color);
  hRconv_G10->SetLineWidth(2);
  TH1D *hGain_G1_R500=new TH1D("Gain_G1_R500","Gain_G1_R500",100,500.,700.);
  hGain_G1_R500->SetLineColor(color);
  hGain_G1_R500->SetLineWidth(2);
  TH1D *hGain_G1_R400=new TH1D("Gain_G1_R400","Gain_G1_R400",100,400.,600.);
  hGain_G1_R400->SetLineColor(color);
  hGain_G1_R400->SetLineWidth(2);
  TH1D *hGain_G10_R500=new TH1D("Gain_G10_R500","Gain_G10_R500",100,5000.,7000.);
  hGain_G10_R500->SetLineColor(color);
  hGain_G10_R500->SetLineWidth(2);
  TH1D *hGain_G10_R400=new TH1D("Gain_G10_R400","Gain_G10_R400",100,4000.,6000.);
  hGain_G10_R400->SetLineColor(color);
  hGain_G10_R400->SetLineWidth(2);
  TH1D *hINL_G1=new TH1D("INL_G1","INL_G1",100,0.,2.);
  hINL_G1->SetLineColor(color);
  hINL_G1->SetLineWidth(2);
  TH1D *hINL_G10=new TH1D("INL_G10","INL_G10",100,0.,2.);
  hINL_G10->SetLineColor(color);
  hINL_G10->SetLineWidth(2);
  TH1D *hoNoise_G1_R400_LPF35=new TH1D("Output_noise_G1_R400_LPF35","Output_noise_G1_R400_LPF35",100,0.,400.);
  hoNoise_G1_R400_LPF35->SetLineColor(color);
  hoNoise_G1_R400_LPF35->SetLineWidth(2);
  TH1D *hoNoise_G10_R400_LPF35=new TH1D("Output_noise_G10_R400_LPF35","Output_noise_G10_R400_LPF35",100,0.,2000.);
  hoNoise_G10_R400_LPF35->SetLineColor(color);
  hoNoise_G10_R400_LPF35->SetLineWidth(2);
  TH1D *hiNoise_G1_R400_LPF35=new TH1D("Input_noise_G1_R400_LPF35","Input_noise_G1_R400_LPF35",100,300.,600.);
  hiNoise_G1_R400_LPF35->SetLineColor(color);
  hiNoise_G1_R400_LPF35->SetLineWidth(2);
  TH1D *hiNoise_G10_R400_LPF35=new TH1D("Input_noise_G10_R400_LPF35","Input_noise_G10_R400_LPF35",100,100.,300.);
  hiNoise_G10_R400_LPF35->SetLineColor(color);
  hiNoise_G10_R400_LPF35->SetLineWidth(2);

  FILE *fd=NULL;
  char *spart;
  const char delim[2]=" ";
  Int_t eof,num=0,miss=0;
  for(int inum=1; inum<=999; inum++)
  //while(1)
  {
    sprintf(fname,"data/CATIA_calib/CATIA_%4.4d_calib.txt",rev*1000+inum);
    printf("Look for %s file\n",fname);
    fd=fopen(fname,"r");
    if(fd==NULL && miss==0)
    {
      //printf("CATIA number %d missing. Skip it\n",num++);
      //miss=1;
      continue;
    }
    //if(fd==NULL && miss>0)
    //{
    //  num--;
    //  break;
    //}
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    sscanf(line,"%lf %lf %lf",&Temp[0],&Temp[1], &Temp[3]);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    sscanf(line,"%d",&nVref);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    spart=strtok(line,delim);
    Double_t Vref_best=0.;
    for(Int_t i=0; i<nVref && spart!=NULL; i++)
    {
      sscanf(spart,"%lf",&Vref[i]);
      Vref[i]*=1000.;
      hVref_all->Fill(Vref[i]);
      if(i==0) hVref_init->Fill(Vref[i]);
      if(fabs(Vref[i]-Vref_target)<fabs(Vref_best-Vref_target))
      {
        Vref_best=Vref[i];
      }
      spart=strtok(NULL,delim);
    }
    hVref_best->Fill(Vref_best);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    sscanf(line,"%d",&nDAC);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    for(Int_t i=0; i<nDAC; i++)sscanf(line,"%lf",&DAC_val[i]);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    for(Int_t i=0; i<nDAC; i++)sscanf(line,"%lf",&Vdac[0][i]);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    for(Int_t i=0; i<nDAC; i++)sscanf(line,"%lf",&Vdac[1][i]);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    sscanf(line,"%lf %lf %lf %lf",&Vdac_slope[0], &Vdac_slope[1], &Vdac_offset[0], &Vdac_offset[1]);
    hDAC1_slope->Fill(Vdac_slope[0]);
    hDAC2_slope->Fill(Vdac_slope[1]);
    hDAC1_offset->Fill(Vdac_offset[0]);
    hDAC2_offset->Fill(Vdac_offset[1]);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    sscanf(line,"%d",&nPed);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    for(Int_t i=0; i<nPed; i++)sscanf(line,"%lf",&Ped_val[0][i]);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    for(Int_t i=0; i<nPed; i++)sscanf(line,"%lf",&Ped_val[1][i]);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    sscanf(line,"%lf %lf %lf %lf",&Ped_offset[0], &Ped_slope[0], &Ped_offset[1], &Ped_slope[1]);
    hPed_offset_G1->Fill(Ped_offset[0]);
    hPed_offset_G10->Fill(Ped_offset[1]);
    hPed_slope_G1->Fill(Ped_slope[0]);
    hPed_slope_G10->Fill(Ped_slope[1]);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    sscanf(line,"%lf %lf %lf %lf %lf %lf %lf %lf",
           &Noise_G1[0][0], &Noise_G1[0][1], &Noise_G1[1][0], &Noise_G1[1][1],
           &Noise_G10[0][0], &Noise_G10[0][1], &Noise_G10[1][0], &Noise_G10[1][1]);
    hoNoise_G1_R400_LPF35->Fill(Noise_G1[1][1]); // uV RMS
    hoNoise_G10_R400_LPF35->Fill(Noise_G10[1][1]); // uV RMS
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    sscanf(line,"%lf %lf %lf %lf",&INL_min[0], &INL_max[0], &INL_min[1], &INL_max[1]);
    hINL_G1->Fill((INL_max[0]-INL_min[0])/2.*1000.);
    hINL_G10->Fill((INL_max[1]-INL_min[1])/2.*1000.);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    sscanf(line,"%lf %lf %lf %lf",&CATIA_gain[0][0], &CATIA_gain[0][1], &CATIA_gain[1][0], &CATIA_gain[1][1]);
    hGain_G1_R500->Fill(CATIA_gain[0][0]);
    hGain_G1_R400->Fill(CATIA_gain[0][1]);
    hGain_G10_R500->Fill(CATIA_gain[1][0]);
    hGain_G10_R400->Fill(CATIA_gain[1][1]);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    sscanf(line,"%lf %lf %lf %lf %lf",&G10oG1, &R500, &R400, &Rconv_G1, &Rconv_G10);
    hG10->Fill(G10oG1);
    hR500->Fill(R500);
    hR400->Fill(R400);
    hRconv_G1->Fill(Rconv_G1);
    hRconv_G10->Fill(Rconv_G10);
    hiNoise_G1_R400_LPF35->Fill(Noise_G1[1][1]/CATIA_gain[0][1]*1000.); // nA RMS
    hiNoise_G10_R400_LPF35->Fill(Noise_G10[1][1]/CATIA_gain[1][1]*1000.); // nA RMS
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    sscanf(line,"%lf %lf %lf %lf",&Vbuf_slope[0], &Vbuf_slope[1], &Vbuf_offset[0], &Vbuf_offset[1]);
    hBUF1_slope->Fill(Vbuf_slope[0]);
    hBUF2_slope->Fill(Vbuf_slope[1]);
    hBUF1_offset->Fill(Vbuf_offset[0]);
    hBUF2_offset->Fill(Vbuf_offset[1]);
    hBUF_gain->Fill(Vbuf_slope[0]/Vdac_slope[0]);

    fclose(fd);
    fd=NULL;
    num++;
  }
  printf("End of scan : %d chips seen\n",num);
  sprintf(fname,"data/CATIA_calib/CATIA_calib_rev%d.root",rev);
  TFile *tf=new TFile(fname,"recreate");
  hVref_init->Write();
  hVref_all->Write();
  hVref_best->Write();
  hG10->Write();
  hR400->Write();
  hR500->Write();
  hRconv_G1->Write();
  hRconv_G10->Write();
  hGain_G1_R500->Write();
  hGain_G1_R400->Write();
  hGain_G10_R500->Write();
  hGain_G10_R400->Write();
  hINL_G1->Write();
  hINL_G10->Write();
  hoNoise_G1_R400_LPF35->Write();
  hoNoise_G10_R400_LPF35->Write();
  hiNoise_G1_R400_LPF35->Write();
  hiNoise_G10_R400_LPF35->Write();
  hDAC1_slope->Write();
  hDAC2_slope->Write();
  hDAC1_offset->Write();
  hDAC2_offset->Write();
  hBUF1_slope->Write();
  hBUF2_slope->Write();
  hBUF1_offset->Write();
  hBUF2_offset->Write();
  hBUF_gain->Write();
  hPed_offset_G1->Write();
  hPed_offset_G10->Write();
  hPed_slope_G1->Write();
  hPed_slope_G10->Write();
  tf->Close();

}
