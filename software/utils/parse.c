#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>

static void parseDoc(char *docname)
{
  xmlChar *key;
  xmlDocPtr doc;
  xmlNodePtr cur, child, child2;

  doc = xmlParseFile(docname);
  
  if (doc == NULL )
  {
    fprintf(stderr,"Document not parsed successfully. \n");
    return;
  }
  
  cur = xmlDocGetRootElement(doc);
  
  if (cur == NULL)
  {
    fprintf(stderr,"empty document\n");
    xmlFreeDoc(doc);
    return;
  }
  
  printf("Start to parse document : %s\n",docname);
  if (xmlStrcmp(cur->name, (const xmlChar *) "vfe"))
  {
    fprintf(stderr,"document of the wrong type, root node != vfe");
    xmlFreeDoc(doc);
    return;
  }
  else
  {
    printf("Starting point : %s\n",cur->name);
  }
  
  cur = cur->xmlChildrenNode;
  while (cur != NULL)
  {
    if ((!xmlStrcmp(cur->name, (const xmlChar *)"object")))
    {
      printf("Start to parse object : %s\n",xmlGetProp(cur,"name"));

// DAQ settings :
      if ((!xmlStrcmp(xmlGetProp(cur,"name"), (const xmlChar *)"DAQ")))
      {
        child = cur->xmlChildrenNode;
        while (child != NULL)
        {
          if ((!xmlStrcmp(child->name, (const xmlChar *)"property")))
          {
            key = xmlNodeListGetString(doc, child->xmlChildrenNode, 1);
            printf("property %s :  %s\n",xmlGetProp(child,"name"), key);
            xmlFree(key);
          }
          child = child->next;
        }
      }

// Channel settings :
      if ((!xmlStrcmp(xmlGetProp(cur,"name"), (const xmlChar *)"channel")))
      {
        printf("Channel id %s, status %s\n",xmlGetProp(cur,"id"),xmlGetProp(cur,"status"));
        child = xmlFirstElementChild(cur);
        if ((!xmlStrcmp(child->name, (const xmlChar *)"child")))
        {
          child = xmlFirstElementChild(child);
          do
          {
            if ((!xmlStrcmp(child->name, (const xmlChar *)"object")))
            {
              printf("Chid name : %s %s\n",child->name,xmlGetProp(child,"name"));

              child2 = child->xmlChildrenNode;
              while (child2 != NULL)
              {
                if ((!xmlStrcmp(child2->name, (const xmlChar *)"property")))
                {
                  key = xmlNodeListGetString(doc, child2->xmlChildrenNode, 1);
                  printf("property %s :  %s\n",xmlGetProp(child2,"name"), key);
                  xmlFree(key);
                }
                child2 = child2->next;
              }
            }
            child=child->next;
          }while(child!=NULL);
        }
      }
    }

    cur = cur->next;
  }
  
  xmlFreeDoc(doc);
  return;
}

int main(int argc, char **argv)
{
  char *docname;
    
  if (argc <= 1)
  {
    printf("Usage: %s docname\n", argv[0]);
    return(0);
  }

  docname = argv[1];
  parseDoc (docname);

  return (1);
}
