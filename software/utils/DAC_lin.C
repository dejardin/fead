void DAC_lin()
{
  FILE *fd=fopen("data/DAC_lin.txt","r");
  Int_t eof=0;
  Double_t num[50], dac[50], vocm[50], tia_g10[50], tia_g1[50], err[50];
  Int_t n=0;
  while(eof!=EOF)
  {
    Double_t loc_dac, loc_vocm, loc_tia_g10, loc_tia_g1;
    eof=fscanf(fd,"%lf %lf %lf %lf",&loc_dac, &loc_vocm, &loc_tia_g10, &loc_tia_g1);
    if(eof==EOF)continue;
    num[n]=n;
    dac[n]=loc_dac;
    vocm[n]=loc_vocm;
    tia_g10[n]=loc_tia_g10;
    tia_g1[n]=loc_tia_g1;
    err[n]=0.5;
    n++;
  }
  printf("Read %d records\n",n);
  TGraphErrors *tg_dac=new TGraphErrors(n,num,dac,err,err);
  tg_dac->SetMarkerStyle(20);
  tg_dac->SetMarkerSize(1.);
  tg_dac->SetMarkerColor(kRed);
  tg_dac->SetLineColor(kRed);
  TGraphErrors *tg_vocm=new TGraphErrors(n,dac,vocm,err,err);
  tg_vocm->SetMarkerStyle(20);
  tg_vocm->SetMarkerSize(1.);
  tg_vocm->SetMarkerColor(kRed);
  tg_vocm->SetLineColor(kRed);
  TGraphErrors *tg_tia_g10=new TGraphErrors(n,dac,tia_g10,err,err);
  tg_tia_g10->SetMarkerStyle(20);
  tg_tia_g10->SetMarkerSize(1.);
  tg_tia_g10->SetMarkerColor(kRed);
  tg_tia_g10->SetLineColor(kRed);
  TGraphErrors *tg_tia_g1=new TGraphErrors(n,dac,tia_g1,err,err);
  tg_tia_g1->SetMarkerStyle(20);
  tg_tia_g1->SetMarkerSize(1.);
  tg_tia_g1->SetMarkerColor(kRed);
  tg_tia_g1->SetLineColor(kRed);
  TF1 *p1=new TF1("p1","pol1",0.,50.);
  TCanvas *c_dac    =new TCanvas("DAC","DAC");
  tg_dac->Fit(p1,"","",0.,31.);
  for(Int_t i=0; i<n; i++)
  {
    Double_t x,y;
    tg_dac->GetPoint(i,x,y);
    y-=p1->Eval(x);
    tg_dac->SetPoint(i,x,y);
  }
  tg_dac->Draw("alp");
  gPad->SetGridx();
  gPad->SetGridy();

  TCanvas *c_vocm   =new TCanvas("VOCM","VOCM");
  p1->SetParameter(0,0.);
  p1->SetParameter(1,0.);
  tg_vocm->SetMaximum(2.);
  tg_vocm->SetMinimum(-2.);
  tg_vocm->Fit(p1,"N","");
  tg_vocm->SetTitle("V_{OCM} vs DAC value");
  tg_vocm->SetName("V_{OCM} vs DAC value");
  for(Int_t i=0; i<n; i++)
  {
    Double_t x,y;
    tg_vocm->GetPoint(i,x,y);
    y-=p1->Eval(x);
    tg_vocm->SetPoint(i,x,y*1000.);
  }
  tg_vocm->Draw("alp");
  gPad->SetGridx();
  gPad->SetGridy();
  tg_vocm->GetXaxis()->SetTitle("DAC value");
  tg_vocm->GetXaxis()->SetTitleFont(62);
  tg_vocm->GetXaxis()->SetTitleSize(0.05);
  tg_vocm->GetXaxis()->SetLabelFont(62);
  tg_vocm->GetXaxis()->SetLabelSize(0.05);
  tg_vocm->GetYaxis()->SetTitle("residual [mV]");
  tg_vocm->GetYaxis()->SetTitleFont(62);
  tg_vocm->GetYaxis()->SetTitleSize(0.05);
  tg_vocm->GetYaxis()->SetTitleOffset(1.0);
  tg_vocm->GetYaxis()->SetLabelFont(62);
  tg_vocm->GetYaxis()->SetLabelSize(0.05);
  tg_vocm->Draw("alp");

  TCanvas *c_tia_g10=new TCanvas("TIA_G10","TIA_G10");
  p1->SetParameter(0,0.);
  p1->SetParameter(1,0.);
  tg_tia_g10->Fit(p1,"N","");
  tg_tia_g10->SetMaximum(2.);
  tg_tia_g10->SetMinimum(-2.);
  tg_tia_g10->SetTitle("V_{TIA_dummy} vs DAC value, R_{conv}= 2471 #Omega");
  tg_tia_g10->SetName("V_{TIA_dummy} vs DAC value, R_{conv}= 2471 #Omega");
  for(Int_t i=0; i<n; i++)
  {
    Double_t x,y;
    tg_tia_g10->GetPoint(i,x,y);
    y-=p1->Eval(x);
    tg_tia_g10->SetPoint(i,x,y*1000.);
  }
  tg_tia_g10->Draw("alp");
  gPad->SetGridx();
  gPad->SetGridy();
  tg_tia_g10->GetXaxis()->SetTitle("DAC value");
  tg_tia_g10->GetXaxis()->SetTitleFont(62);
  tg_tia_g10->GetXaxis()->SetTitleSize(0.05);
  tg_tia_g10->GetXaxis()->SetLabelFont(62);
  tg_tia_g10->GetXaxis()->SetLabelSize(0.05);
  tg_tia_g10->GetYaxis()->SetTitle("residual [mV]");
  tg_tia_g10->GetYaxis()->SetTitleFont(62);
  tg_tia_g10->GetYaxis()->SetTitleSize(0.05);
  tg_tia_g10->GetYaxis()->SetTitleOffset(1.0);
  tg_tia_g10->GetYaxis()->SetLabelFont(62);
  tg_tia_g10->GetYaxis()->SetLabelSize(0.05);
  tg_tia_g10->Draw("alp");

  TCanvas *c_tia_g  =new TCanvas("TIA_G1","TIA_G1");
  p1->SetParameter(0,0.);
  p1->SetParameter(1,0.);
  tg_tia_g1->Fit(p1,"N","",0.,3400.);
  tg_tia_g1->SetMaximum(5.);
  tg_tia_g1->SetMinimum(-2.);
  tg_tia_g1->SetTitle("V_{TIA_dummy} vs DAC value, R_{conv}= 272 #Omega");
  tg_tia_g1->SetName("V_{TIA_dummy} vs DAC value, R_{conv}= 272 #Omega");
  for(Int_t i=0; i<n; i++)
  {
    Double_t x,y;
    tg_tia_g1->GetPoint(i,x,y);
    y-=p1->Eval(x);
    tg_tia_g1->SetPoint(i,x,y*1000.);
  }
  tg_tia_g1->Draw("alp");
  gPad->SetGridx();
  gPad->SetGridy();
  tg_tia_g1->GetXaxis()->SetTitle("DAC value");
  tg_tia_g1->GetXaxis()->SetTitleFont(62);
  tg_tia_g1->GetXaxis()->SetTitleSize(0.05);
  tg_tia_g1->GetXaxis()->SetLabelFont(62);
  tg_tia_g1->GetXaxis()->SetLabelSize(0.05);
  tg_tia_g1->GetYaxis()->SetTitle("residual [mV]");
  tg_tia_g1->GetYaxis()->SetTitleFont(62);
  tg_tia_g1->GetYaxis()->SetTitleSize(0.05);
  tg_tia_g1->GetYaxis()->SetTitleOffset(1.0);
  tg_tia_g1->GetYaxis()->SetLabelFont(62);
  tg_tia_g1->GetYaxis()->SetLabelSize(0.05);
  tg_tia_g1->Draw("alp");
}
