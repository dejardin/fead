void Display_result(char *fname)
{
  TH1D *h1=new TH1D("h1","h1",16384,0.,1750.);
  TGraph *tg1=new TGraph();
  double t=0.;
  double dt=6.25;
  double dv=1.75/16384.*1000.;
  double val1, val2;
  int n=0;
  FILE *fd=fopen(fname,"r");
  int eof=0;
  while (eof!=EOF)
  {
    int val, valb, valh; 
    eof=fscanf(fd,"%x:",&val); 
    if(eof==EOF)continue;
    for(int i=0; i<8; i++)
    {
      eof=fscanf(fd,"%x",&val); 
      valb=val&0xffff;
      valh=(val&0xffff0000)>>16;
//    if(valb>8192)valb=valb-8192;
//    if(valh>8192)valh=valh-8192;
      printf("%d\n%d\n",valb,valh);
      val1=dv*valb;
      val2=dv*valh;
      if(n>=2)
      {
        h1->Fill(val1);
        h1->Fill(val2);
      }
      if(n>=2)tg1->SetPoint(n-2,t,val1);
      n++;
      t+=dt;
      if(n>=2)tg1->SetPoint(n-2,t,val2);
      n++;
      t+=dt;
    }
  }
  TCanvas *c1=new TCanvas();
  c1->cd();
  h1->Draw();
  TCanvas *c2=new TCanvas();
  c2->cd();
  tg1->SetMarkerStyle(21);
  tg1->SetMarkerSize(0.5);
  tg1->SetMarkerColor(kRed);
  tg1->SetLineColor(kRed);
  tg1->Draw("alp");
}
