// Launch DAQ with all initialization but no links synchro :
// bin/debug_LiTEDTU.exe -trigger_type 0 -nevt 40000 -nsample 1000 -soft_trigger 1 -do_VFE_reset 1 -do_pwup_reset 1 -do_IO_reset 1 -init_DTU 1 -synchronize_ADC 0 -reset_ADC 3 -calib_ADC 3 -CATIA_Reg3_def 3087 -debug 1 -retry_I2C 4000 -DAC_command 38dd54 -DAC_command 31562c -ped_mux 1 -do_ped_scan 0 -PLL_conf d8
// Scan DAC values without recalibration :
// bin/debug_LiTEDTU.exe -trigger_type 0 -nevt 40000 -nsample 1000 -soft_trigger 1 -do_VFE_reset 0 -do_pwup_reset 0 -do_IO_reset 0 -init_DTU 0 -synchronize_ADC 0 -reset_ADC 3 -calib_ADC 3 -CATIA_Reg3_def 3087 -debug 0 -retry_I2C 4000 -DAC_command 38dd54 -DAC_command 31562c -ped_mux 1 -do_ped_scan 1
//
// bin/debug_LiTEDTU.exe -trigger_type 0 -nevt 100 -nsample 26622 -soft_trigger 1 -ADC_test_mode 1 -do_VFE_reset 0 -do_pwup_reset 0 -do_IO_reset 0 -init_DTU 0 -synchronize_ADC 0 -reset_ADC 0 -calib_ADC 0 -CATIA_Reg3_def 7 -debug 0
// bin/debug_LiTEDTU.exe -trigger_type 2 -nevt 1000 -nsample 50 -soft_trigger 0 -self_trigger 1 -self_trigger_threshold 2000 -self_trigger_mask 1 -ADC_test_mode 1 -do_VFE_reset 0 -do_pwup_reset 0 -do_IO_reset 0 -init_DTU 0  -synchronize_ADC 0 -reset_ADC 0 -calib_ADC 0 -CATIA_Reg3_def 7 -debug 0 -hw_DAQ_delay 50
#include "uhal/uhal.hpp"
#include "I2C_RW.h"
#include "LiTEDTU_def.h"

#include <vector>
#include <iostream>
#include <cstdlib>
#include <typeinfo>

#include <TApplication.h>
#include <TProfile.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <TTree.h>
#include <TF1.h>
#include <TFile.h>

#define GENE_TRIGGER           (1<<0)
#define GENE_TP                (1<<1)
#define GENE_100HZ             (1<<2)
#define LED_ON                 (1<<3)
#define TP_MODE                (1<<4)

#define RESET                  (1<<0)
#define TRIGGER_MODE           (1<<1)
#define SOFT_TRIGGER           (1<<2)
#define SELF_TRIGGER           (1<<3)
#define CLOCK_PHASE            (1<<4)
#define CLOCK_RESET            (1<<7)
#define SELF_TRIGGER_THRESHOLD (1<<8)
#define SELF_TRIGGER_MASK      (1<<22)
#define BOARD_SN               (1<<28)

#define SEQ_CLOCK_PHASE        (1<<0)
#define IO_CLOCK_PHASE         (1<<4)
#define REG_CLOCK_PHASE        (1<<8)
#define MEM_CLOCK_PHASE        (1<<12)
#define RESYNC_PHASE           (1<<16)

#define I2C_TOGGLE_SDA         (1<<31)

#define PWUP_RESETB            (1<<31)
#define BULKY_I2C_DTU          (1<<28)
#define N_RETRY_I2C_DTU        (1<<14)
#define PED_MUX                (1<<13)
#define INVERT_RESYNC          (1<<3)
#define LVRB_AUTOSCAN          (1<<2)

#define CAPTURE_START 1
#define CAPTURE_STOP  2
//#define SW_DAQ_DELAY 0x1800 // delay for laser with internal trigger
#define SW_DAQ_DELAY (1<<16)  // delay for laser with external trigger
#define HW_DAQ_DELAY (1<<0)   // Laser with external trigger
#define NSAMPLE_MAX 26624
#define MAX_PAYLOAD 1380
#define MAX_VFE     10
#define MAX_STEP    10000

#define DRP_WRb         (1<<31)
#define DELAY_INCREASE  (1<<5)     // Increase ADC signal delay by one tip
#define DELAY_RESET     (1<<6)     // Reset ADC signal delays
#define BIT_SLIP        (1<<16)    // Slip serial bits by 1 unit (to align bits in ISERDES) (to multiply with 5 bits channel map)
#define BYTE_SLIP       (1<<21)    // Slip serial bytes by 1 unit (to align bytes in 40 MHz clock) (to multiply with 5 bits channel map)
#define DELAY_AUTO_TUNE (1<<27)    // Launch autamatic IDELAY tuning to get the eye center of each stream

#define ADC_CALIB_MODE      (1<<0)
#define ADC_TEST_MODE       (1<<1)
#define ADC_INVERT_DATA     (1<<8)
#define RESYNC_IDLE_PATTERN (1<<0)
#define eLINK_ACTIVE        (1<<8)
#define STATIC_RESET        (1<<31)

#define I2C_LVR_TYPE     0
#define I2C_CATIA_TYPE   1
#define I2C_LiTEDTU_TYPE 1

using namespace uhal;
Int_t delay_val[5];    // Number of delay taps to get a stable R/O with ADCs (160 MHz)
Int_t bitslip_val[5];  // Number of bit to slip with iserdes lines to get ADC values well aligned
Int_t byteslip_val[5]; // Number of byte to slip to aligned ADC data on 40 MHz clock
Int_t old_read_address, shift_odd_samples, ADC_invert_data;  
Short_t *event[6];
Double_t *fevent[6];
ValVector< uint32_t > block,mem;

Long_t get_event(uhal::HwInterface hw, int trigger_type, int nsample, int debug, int draw)
{
  static int ievt=0;
  static TGraph *tg_debug[6]={NULL};
  static TCanvas *cdebug=NULL;
  static int trigger_type_old=-1;
  int n_word;
  int n_transfer_max=463;
  int n_transfer;
  int n_last;
  int error      = 0;
  int command    = 0;
  Int_t loc_ch[6]={1,3,2,4,5,6};

  if(trigger_type_old!=trigger_type)ievt=0;
  if(cdebug==NULL)
  {
    for(Int_t ich=0; ich<6; ich++)
    {
      tg_debug[ich]=new TGraph();
      tg_debug[ich]->SetMarkerStyle(20);
      tg_debug[ich]->SetMarkerSize(0.5);
    }
    tg_debug[0]->SetLineColor(kCyan);
    tg_debug[1]->SetLineColor(kCyan);
    tg_debug[2]->SetLineColor(kMagenta);
    tg_debug[3]->SetLineColor(kMagenta);
    tg_debug[4]->SetLineColor(kBlue);
    tg_debug[5]->SetLineColor(kRed);
    tg_debug[0]->SetMarkerColor(kCyan);
    tg_debug[1]->SetMarkerColor(kCyan);
    tg_debug[2]->SetMarkerColor(kMagenta);
    tg_debug[3]->SetMarkerColor(kMagenta);
    tg_debug[4]->SetMarkerColor(kBlue);
    tg_debug[5]->SetMarkerColor(kRed);
    cdebug=new TCanvas("debug","debug",800,1000);
    cdebug->Divide(2,3);
  }

  ValWord<uint32_t> address,free_mem;
  address = hw.getNode("CAP_ADDRESS").read();
  hw.dispatch();
  if(debug>0)printf("Starting with address : %8.8x, trigger_type : %d\n",address.value(),trigger_type);
  //old_read_address=address.value()>>16;
  //if(old_read_address==NSAMPLE_MAX-1)old_read_address=-1;

  ValVector< uint32_t > block;

  if(trigger_type !=2)
  {
    if(trigger_type == 0)
      command = TP_MODE*0+LED_ON*1+GENE_100HZ*0+GENE_TP*0+GENE_TRIGGER*1;
    else if(trigger_type == 1)
      command = TP_MODE*0+LED_ON*1+GENE_100HZ*0+GENE_TP*1+GENE_TRIGGER*1;

    if(debug>0)printf("Send trigger with command : 0x%8.8x\n",command);
// Read base address and send trigger
    hw.getNode("FW_VER").write(command);
    hw.dispatch();
// Read new address and wait for DAQ completion
    int nretry=0, new_write_address=-1, delta_address=-1;
    do
    {  
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      new_write_address=address.value()>>16;
      nretry++;
      delta_address=new_write_address-old_read_address;
      if(debug>0)printf("ongoing R/W addresses    : old %d, new %d delta %d\n", old_read_address, new_write_address,delta_address);
      if(delta_address<0)delta_address+=NSAMPLE_MAX;
    }
    while(delta_address < nsample+1 && nretry<100);
    if(nretry==100)
    {
      printf("Stop waiting for sample capture after %d retries\n",nretry);
      printf("R/W addresses    : old %8.8x, new %8.8x add 0x%8.8x\n", old_read_address, new_write_address, address.value());
      error=1;
    }
  }
  else
  {
    int nretry=0;
// Wait for external trigger to fill memory :
    do
    {
      free_mem = hw.getNode("CAP_FREE").read();
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      if(debug>0)printf("address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
      usleep(100);
      nretry++;
    }
    while((free_mem.value()==NSAMPLE_MAX-1) && nretry<100);
    if(nretry==100)
    {
      printf("Stop waiting for external trigger after %d retries\n",nretry);
      printf("R/W addresses    : add 0x%8.8x\n",address.value());
      error=1;
    }
  }

// Keep reading address for next event
  old_read_address=address.value()&0xffff;
  if(old_read_address==NSAMPLE_MAX-1)old_read_address=-1;

  mem.clear();

// Read event samples from FPGA
  n_word=(nsample+1)*6; // 6*32 bits words per sample to get the 5 channels data
  n_transfer=n_word/(MAX_PAYLOAD/4); // Max ethernet packet = 1536 bytes, max user payload = 1500 bytes
  n_last=n_word-n_transfer*(MAX_PAYLOAD/4);
  if(n_transfer > n_transfer_max)
  {
    printf("Event size too big ! Please reduce number of samples per frame.\n");
    printf("Max frame size : %d\n",NSAMPLE_MAX);
    printf("Error : ask for too many samples. Number of transfer %d greater than allowed max value %d\n",n_transfer, n_transfer_max);
    error=1;
  }
  for(int itrans=0; itrans<n_transfer; itrans++)
  {
    block = hw.getNode ("CAP_DATA").readBlock(MAX_PAYLOAD/4);
    hw.dispatch();
    for(int is=0; is<MAX_PAYLOAD/4; is++)mem.push_back(block[is]);
  }
  block = hw.getNode ("CAP_DATA").readBlock(n_last);
  address = hw.getNode("CAP_ADDRESS").read();
  free_mem = hw.getNode("CAP_FREE").read();
  hw.dispatch();
  if(debug>0)printf("After reading address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
  for(int is=0; is<n_last; is++)mem.push_back(block[is]);
  mem.valid(true);

// First sample should have bits 159 downto 64 at 1 and timestamp in bits 63 downto 0
  if(mem[3]!=0xffffffff && mem[4]!=0xffffffff && mem[5]!=0xffffffff)
  {
    printf("First samples not headers : %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",mem[5], mem[4], mem[3], mem[2], mem[1], mem[0]);
    error=1;
  }
  Long_t timestamp=mem[2];
  
  timestamp=(timestamp<<32)+mem[1];
  
  if(debug>0)
  {
    printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",mem[0],mem[1],mem[2],mem[3],mem[4],mem[5]);
    printf("timestamp : %8.8x %8.8x %ld\n",mem[2],mem[1],timestamp);
  }
  int tot_sample=0, all_sample=0;
  for(int isample=0; isample<nsample; isample++)
  {
    Int_t j=(isample+1)*6;
    unsigned int loc_mem[4];
    for(int ich=0; ich<4; ich++)
    {
      loc_mem[ich]=mem[j+1+ich];
      if((ich==1 || ich==3) && isample>=shift_odd_samples && isample<nsample+shift_odd_samples) loc_mem[ich]=mem[j-shift_odd_samples*6+1+ich];
    }
    event[0][tot_sample]  =(loc_mem[0]>>0 )&0xfff;
    event[0][tot_sample+1]=(loc_mem[0]>>16)&0xfff;
    event[1][tot_sample]  =(loc_mem[1]>>0 )&0xfff;
    event[1][tot_sample+1]=(loc_mem[1]>>16)&0xfff;
    event[2][tot_sample]  =(loc_mem[2]>>0 )&0xfff;
    event[2][tot_sample+1]=(loc_mem[2]>>16)&0xfff;
    event[3][tot_sample]  =(loc_mem[3]>>0 )&0xfff;
    event[3][tot_sample+1]=(loc_mem[3]>>16)&0xfff;
    if(ADC_invert_data==1)
    {
      event[0][tot_sample]  =4095-event[0][tot_sample];
      event[0][tot_sample+1]=4095-event[0][tot_sample+1];
      event[1][tot_sample]  =4095-event[1][tot_sample];
      event[1][tot_sample+1]=4095-event[1][tot_sample+1];
      event[2][tot_sample]  =4095-event[2][tot_sample];
      event[2][tot_sample+1]=4095-event[2][tot_sample+1];
      event[3][tot_sample]  =4095-event[3][tot_sample];
      event[3][tot_sample+1]=4095-event[3][tot_sample+1];
    }
    event[4][all_sample+0]=event[1][tot_sample];
    event[4][all_sample+1]=event[0][tot_sample];
    event[4][all_sample+2]=event[1][tot_sample+1];
    event[4][all_sample+3]=event[0][tot_sample+1];
    event[5][all_sample+0]=event[3][tot_sample];
    event[5][all_sample+1]=event[2][tot_sample];
    event[5][all_sample+2]=event[3][tot_sample+1];
    event[5][all_sample+3]=event[2][tot_sample+1];
    fevent[0][tot_sample+0]=(double)event[0][tot_sample];
    fevent[0][tot_sample+1]=(double)event[0][tot_sample+1];
    fevent[1][tot_sample+0]=(double)event[1][tot_sample];
    fevent[1][tot_sample+1]=(double)event[1][tot_sample+1];
    fevent[2][tot_sample+0]=(double)event[2][tot_sample];
    fevent[2][tot_sample+1]=(double)event[2][tot_sample+1];
    fevent[3][tot_sample+0]=(double)event[3][tot_sample];
    fevent[3][tot_sample+1]=(double)event[3][tot_sample+1];
    fevent[4][all_sample+0]=(double)event[1][tot_sample];
    fevent[4][all_sample+1]=(double)event[0][tot_sample];
    fevent[4][all_sample+2]=(double)event[1][tot_sample+1];
    fevent[4][all_sample+3]=(double)event[0][tot_sample+1];
    fevent[5][all_sample+0]=(double)event[3][tot_sample];
    fevent[5][all_sample+1]=(double)event[2][tot_sample];
    fevent[5][all_sample+2]=(double)event[3][tot_sample+1];
    fevent[5][all_sample+3]=(double)event[2][tot_sample+1];
    tot_sample+=2;
    all_sample+=4;
    //Search for 0x3 and 0x9 in incoming data
    if(debug>0 && isample==0)
    {
      printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",mem[j+0],mem[j+1],mem[j+2],mem[j+3],mem[j+4],mem[j+5],mem[j+6]);
      printf("data : %8.8x ",mem[j+1]);
      for(int i=31; i>=0; i--) printf("%1.1d",(mem[j+1]>>i)&1);
      printf(" 0x3 at ");
      for(int i=0; i<32; i++) if(((mem[j+1]>>i)&0xf) == 3) printf(" %d",i);
      printf(" 0x9 at ");
      for(int i=0; i<32; i++) if(((mem[j+1]>>i)&0xf) == 9) printf(" %d",i);
      printf("\n");
      printf("data : %8.8x ",mem[j+2]);
      for(int i=31; i>=0; i--) printf("%1.1d",(mem[j+2]>>i)&1);
      printf(" 0x6 at ");
      for(int i=0; i<32; i++) if(((mem[j+2]>>i)&0xf) == 6) printf(" %d",i);
      printf(" 0xc at ");
      for(int i=0; i<32; i++) if(((mem[j+2]>>i)&0xf) == 12) printf(" %d",i);
      printf("\n");
      printf("data : %8.8x ",mem[j+3]);
      for(int i=31; i>=0; i--) printf("%1.1d",(mem[j+3]>>i)&1);
      printf(" 0xc at ");
      for(int i=0; i<32; i++) if(((mem[j+3]>>i)&0xf) == 12) printf(" %d",i);
      printf(" 0x6 at ");
      for(int i=0; i<32; i++) if(((mem[j+3]>>i)&0xf) == 6) printf(" %d",i);
      printf("\n");
      printf("data : %8.8x ",mem[j+4]);
      for(int i=31; i>=0; i--) printf("%1.1d",(mem[j+4]>>i)&1);
      printf(" 0x9 at ");
      for(int i=0; i<32; i++) if(((mem[j+4]>>i)&0xf) == 9) printf(" %d",i);
      printf(" 0x3 at ");
      for(int i=0; i<32; i++) if(((mem[j+4]>>i)&0xf) == 3) printf(" %d",i);
      printf("\n");
      ValWord<uint32_t> reg = hw.getNode("DEBUG1").read();
      hw.dispatch();
      printf("Delay debug : 0x%x\n",reg.value());
    }

  }

  if(trigger_type !=trigger_type_old || draw==1)
  {
    for(int ich=0; ich<4; ich++)
    {
      //tg_debug[ich]->Set(0);
      for(int isample=0; isample<nsample*2; isample++) tg_debug[ich]->SetPoint(isample,12.5*isample,fevent[ich][isample]);
    }
    for(int ich=4; ich<6; ich++)
    {
      //tg_debug[ich]->Set(0);
      for(int isample=0; isample<nsample*4; isample++) tg_debug[ich]->SetPoint(isample,6.25*isample,fevent[ich][isample]);
    }
    for(int ich=0; ich<6; ich++)
    {
      cdebug->cd(loc_ch[ich]);
      tg_debug[ich]->Draw("alp");
      cdebug->Update();
    }
  }
  //if(debug>0)
  //{
  //  system("stty raw");
  //  char cdum=getchar();
  //  system("stty -raw");
  //  if(cdum=='q')exit(-1);
  //}

  trigger_type_old=trigger_type;
  ievt++;
  if(error==0)
    return timestamp;
  else
    return -1;
}


Int_t synchronize_links(uhal::HwInterface hw, Int_t Channel_num, Int_t debug, Int_t eLink_active)
{
  ValWord<uint32_t> address;
  UInt_t command;
  ValWord<uint32_t> free_mem;
  Int_t old_address;

// First, reinit delay values :
  command=DELAY_RESET*1 ;
  for(Int_t iadc=0; iadc<5; iadc++)
  {
    delay_val[iadc]=0;
  }

  address = hw.getNode("CAP_ADDRESS").read();
  hw.dispatch();
  old_read_address=address.value()>>16;

  Int_t nsample_sync=1000;

// first look for headers in each sample
// if not, change idelay and restart 
// idelay between 0 and 10 should be enough : 1280 Mhz -> 781.25 sp period, idelay tap=5ns/64=78.125 ps)
// Once we have teh header position, do bitslip to get them in postition 4, 12, 20 or 28
// Then do bytelip to get the headers in [b31..b28] and [b15..b12]
// 26623 = 0x67ff
// Reset idelay :
  command = ((nsample_sync+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
  command = ((nsample_sync+1)<<16)+CAPTURE_START;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();
  Int_t max_delay=10;
  Int_t loop_event=1;
  Int_t add_one_tap[5]={-1,-1,-1,-1,-1};
  Int_t good_delay[5]={0,0,0,0,0};
  Int_t good_bit[5]  ={0,0,0,0,0};
  Int_t good_byte[5] ={0,0,0,0,0};

  while(true)
  {
// take one pedestal event with 1000 samples
    Long_t timestamp=get_event(hw,0,nsample_sync,0,0);

    unsigned int h1[5]={0x3,0x6,0xc,0x9,0x0};
    unsigned int h2[5]={0x9,0xc,0x6,0x3,0x0};
    Int_t good_pos[5][32];
    Int_t best_pos[5];
    Int_t good_pos_max[5];
    for(int iadc=0; iadc<5; iadc++)
    {
      for(int ib=0; ib<32; ib++)good_pos[iadc][ib]=0;
      good_pos_max[iadc]=0;
      best_pos[iadc]=0;
    }
// First, search for headers position and look if data transfer is reliable
// Otherwise tune iDelay taps to get no error
    printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",mem[0],mem[1],mem[2],mem[3],mem[4],mem[5]);
    printf("               %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",mem[6],mem[7],mem[8],mem[9],mem[10],mem[11]);
    printf("               %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",mem[12],mem[13],mem[14],mem[15],mem[16],mem[17]);
    for(int isample=0; isample<nsample_sync; isample++)
    {
      Int_t j=(isample+1)*6;
      for(int iadc=0; iadc<4; iadc++)
      {
  // ch0 headers : 0x3 and 0x9
  // ch1 headers : 0x6 and 0xc
  // ch2 headers : 0xc and 0x6
  // ch3 headers : 0x9 and 0x3
        unsigned int loc_mem=0;
        loc_mem=mem[j+1+iadc];
        //loc_mem=0;
        //for(int iB=0; iB<4; iB++)
        //{
        //  loc_mem=(loc_mem<<8)+((mem[j+1+iadc]>>(8*iB))&0xFF);
        //}
        for(int ib=0; ib<31; ib++)
        {
          int jb=ib+16;
          if(jb>31)jb-=32;
          if(((loc_mem>>ib)&0xf)==h1[iadc] && ((loc_mem>>jb)&0xf)==h2[iadc])
          {
            if(isample==0 || isample==1)printf("%d %d %d\n",ib,(loc_mem>>ib)&h1[iadc], (loc_mem>>jb)&h2[iadc]);
            good_pos[iadc][ib]++;
          }
        }
      }
    }

    printf("good positions :\n");
    for(int iadc=0; iadc<4; iadc++)
    {
      for(int ib=0; ib<32; ib++)
      {
        if(good_pos[iadc][ib]>0)printf("adc %d, b%d : %d,",iadc,ib,good_pos[iadc][ib]);
        if(good_pos[iadc][ib]>good_pos_max[iadc])
        {
          good_pos_max[iadc]=good_pos[iadc][ib];
          best_pos[iadc]=ib;
        }
      }
      printf("\n");
      if(good_delay[iadc]==1)continue;
      if(good_pos_max[iadc]!=nsample_sync || add_one_tap[iadc]>0)
      {
// Change idelay by one tap on this adc and restart
        command=DELAY_RESET*0 | DELAY_INCREASE*1 | (1<<iadc);
        hw.getNode("DELAY_CTRL").write(command);
        hw.dispatch();
        delay_val[iadc]++;
        add_one_tap[iadc]--;
      }
      else if(good_pos_max[iadc]==nsample_sync && add_one_tap[iadc]<0)
      {
// Once we have a good tap, add 2 more to be safe :
        add_one_tap[iadc]=2;
      }
      else if(good_pos_max[iadc]==nsample_sync && add_one_tap[iadc]==0)
      {
        good_delay[iadc]=1;
      }
    }
    if(delay_val[0]>max_delay || delay_val[1]>max_delay || delay_val[2]>max_delay || delay_val[3]>max_delay)
    {
// PLL certainly not locked : reload I2C
      printf("Reload I2C register map of LiTE-DTU\n");
      UInt_t VFE_control= BULKY_I2C_DTU*1 | N_RETRY_I2C_DTU*1000 |
                   INVERT_RESYNC*0 | LVRB_AUTOSCAN*0 | eLINK_ACTIVE*eLink_active | 
                   PED_MUX*1 | ADC_CALIB_MODE*0 | ADC_TEST_MODE*1;
      hw.getNode("VFE_CTRL").write(VFE_control);
      Int_t iret=0;
      do
      {
        iret=I2C_RW(hw, I2C_LiTEDTU_TYPE*1000+(Channel_num<<2)+2, 0, 0, 0, 1, debug);
        printf("Bulky write return code : %8.8x -> %d ACK\n",iret,(iret>>23)&0xff);
      }
      while(((iret>>23)&0xff) <15);
      good_delay[0]=0;
      good_delay[1]=0;
      good_delay[2]=0;
      good_delay[3]=0;
      delay_val[0]=0;
      delay_val[1]=0;
      delay_val[2]=0;
      delay_val[3]=0;
    }
    if(good_delay[0]==0 || good_delay[1]==0 || good_delay[2]==0 || good_delay[3]==0)continue;

    printf("Header positions : %d %d %d %d\n",best_pos[0],best_pos[1], best_pos[2], best_pos[3]);
    printf("Final tap values : %d %d %d %d\n",delay_val[0],delay_val[1], delay_val[2], delay_val[3]);

// The idelay is correctly set.
// Now put the header at the right place (h1 should be in pos 28, pos 4 in a byte, 4th byte):
    for(int iadc=0; iadc<4; iadc++)
    {
      int nbit_slip=best_pos[iadc]%8-4;
      if(nbit_slip<0)nbit_slip+=8;
      printf("ADC %d, header in pos %d, do bitslip by %d unit\n",iadc,best_pos[iadc],nbit_slip);
      for(int islip=0; islip<nbit_slip; islip++)
      {
        bitslip_val[iadc]=(bitslip_val[iadc]+1)%4;
        command=(1<<iadc)*BIT_SLIP | DELAY_RESET*0 | DELAY_INCREASE*0 | 0;
        hw.getNode("DELAY_CTRL").write(command);
        hw.dispatch();
      }
      if(nbit_slip==0)good_bit[iadc]=1;
    }
    if(good_bit[0]==0 || good_bit[1]==0 || good_bit[2]==0 || good_bit[3]==0)continue;

    printf("Final bit values : %d %d %d %d\n",bitslip_val[0],bitslip_val[1], bitslip_val[2], bitslip_val[3]);

    for(int iadc=0; iadc<4; iadc++)
    {
      //int nbyte_slip=4-(best_pos[iadc]+4)/8;
      int nbyte_slip=((best_pos[iadc]+4)/8)%4;
      printf("ADC %d, header in pos %d, do byteslip by %d unit\n",iadc,best_pos[iadc],nbyte_slip);
      for(int islip=0; islip<nbyte_slip; islip++)
      {
        //byteslip_val[iadc]=(byteslip_val[iadc]+1)%4;
        command=(1<<iadc)*BYTE_SLIP | DELAY_RESET*0 | DELAY_INCREASE*0 | 0;
        hw.getNode("DELAY_CTRL").write(command);
        hw.dispatch();
      }
      if(nbyte_slip==0)good_byte[iadc]=1;
    }
    if(good_byte[0]==0 || good_byte[1]==0 || good_byte[2]==0 || good_byte[3]==0)continue;

    printf("Final byte values : %d %d %d %d\n",byteslip_val[0],byteslip_val[1], byteslip_val[2], byteslip_val[3]);
    break;
  }
  return 1;
}


Int_t main ( Int_t argc,char* argv[] )
{
  TF1 *f1;
  Int_t ngood_event=0;
  UInt_t soft_reset, full_reset, command;
  ValWord<uint32_t> address;
  for(Int_t ich=0; ich<6; ich++) event[ich]=(short int*)malloc(sizeof(short int)*4*NSAMPLE_MAX);
  for(Int_t ich=0; ich<6; ich++) fevent[ich]=(double*)malloc(sizeof(double)*4*NSAMPLE_MAX);
  double dv=1200./4096.; // 12 bits on 1.2V
  double display_min=-1.;
  double display_max=-1.;
  Int_t debug=0;
// Define defaults for laser runs :
  Int_t nevent=1000;
  Int_t nsample=100;
  Int_t nsample_save;
  Int_t trigger_type=0;         // pedestal by default
  Int_t soft_trigger=0;         // external trigger by default 
  Int_t self_trigger=0;         // No self trigger 
  Int_t self_trigger_threshold=0;
  Int_t self_trigger_mask=0x1F; // trig on all channels amplitude
  Int_t ADC_calib_mode = 0;     // Put (1) or not (0) the outputs of CATIA in ADC_calib_mode (outputs near VCM)
  Int_t ADC_test_mode = 1;      // Put (1) or not (0) the LiTE-DTU in test mode (direct access to 2 ADC outputs)
  ADC_invert_data = 0;          // Invert (1) or not (0) LiTE-DTU data (usefull in case of AC coupling)
  Int_t seq_clock_phase    = 0; // sequence clock phase by steps of 45 degrees
  Int_t IO_clock_phase     = 0; // Capture clock phase by steps of 45 degrees
  Int_t reg_clock_phase    = 0; // Shift register clock phase by steps of 45 degrees
  Int_t mem_clock_phase    = 0; // memory clock phase by steps of 45 degrees
  Int_t clock_phase        = 0;
  Int_t n_TP_step=1;
  Int_t TP_gain=0;
  Int_t TP_step=128;
  Int_t TP_level=0;
  Int_t TP_width=32;
  Int_t TP_delay=50;
  Int_t TP_mode=0;
  Int_t vfe=3;
  char cdum, output_file[256];
  Int_t channel_sel=0;
  Int_t ADC_reg_val[2][76];
  Int_t sw_DAQ_delay=0;
  Int_t hw_DAQ_delay=50;
  Int_t iret;
  Int_t reset_all=0;
  Int_t use_ref_ADC_calib=0;
  Int_t swap_ADC_calib_voltage=0;
  shift_odd_samples=0;

// CATIA settings if requested
  Int_t n_catia=1;
  Int_t Channel_num    = 3;
  Int_t CATIA_data     =-1; // Data to write in CATIA register
  Int_t CATIA_reg      =-1; // Register to write in with I2C protocol
  Int_t I2C_dir        = 0; // read(2)+write(1) with I2C protocol, 0=don't use I2C
  Int_t CATIA_Reg1_def = 0x02;   // Default content of Register 1 : SEU auto correction, no Temp output
  Int_t CATIA_Reg3_def = 0x1087; // Default content of Register 3 : 1.2V, LPF35, 400 Ohms, ped mid scale
  Int_t CATIA_Reg4_def = 0x1000; // Default content of Register 4 : DAC1 0, DAC1 ON, DAC2 OFF, no copy
  Int_t CATIA_Reg5_def = 0x0000; // Default content of Register 5 : DAC2 0
  Int_t CATIA_Reg6_def = 0x0b;   // Default content of Register 6 : Vreg ON, Rconv=2471, TIA_dummy ON, TP ON

  UInt_t DAC_command[10];
  Int_t nDAC_command     = 0;
  Int_t ped_mux          = 0;
  UInt_t DAC_VCM         = 0x99c0;
  UInt_t DAC_VP          = 0xD9c0;
  UInt_t DAC_VN          = 0x59c0;
  Int_t do_ped_scan      = 0;
  Int_t do_calib_loop    = 0;
  Int_t nsample_calib_x4 = 0;
  Int_t dither           = 0;
  Int_t global_test      = 0;
  Int_t min_ack          = 15;

//DTU settings if requested
  Int_t init_DTU       = 1;
  Int_t synchronize_ADC= 1;
  Int_t I2C_long       = 0;
  Int_t reset_ADC      = 0;
  Int_t calib_ADC      = 0;
  int bulky_I2C      = 0;
  int retry_I2C      = 1;
  int LVRB_autoscan  = 0;
  int invert_Resync  = 0;
  int I2C_toggle_SDA = 0;
  int resync_phase   = 0;
  int eLink_active   = 0x0f;
  unsigned int Resync_idle_pattern = 0x66;
  unsigned int Resync_Hamming_data = 0;
  unsigned int static_DTU_reset    = 0;
  unsigned int DTU_bulk1=0x0007031F;
  unsigned int DTU_bulk2=0x88000000;
  //unsigned int DTU_bulk3=0x00553040;
  unsigned int DTU_bulk3=0x005530C0;
  unsigned int DTU_bulk4=0x65040000;
  unsigned int DTU_bulk5=0x0000002C;
  unsigned int pll_conf=0xd8;
  unsigned int I2C_DTU_reg[18]={0x01, 0x00, 0x04, 0x20, 0x00, 0x00, 0x00,
                                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x04, 0x00,
                                0x00};

  //unsigned int pll_conf=0x28;
  int do_VFE_reset  = 0;
  int do_IO_reset   = 0;
  int do_pwup_reset = 0;
  int inv_clk       = 0;

  double Rshunt_V1P2      = 0.255;
  double Rshunt_V2P5      = 0.05;
  double Imeas_V2P5, Imeas_V1P2;
  Double_t DAC_step=1000./pow(2,16); // 16 bits DAC between 0 and 1V
  unsigned int val;
  FILE *fcal;

  TProfile *pshape[MAX_STEP][6];
  sprintf(output_file,"");

  for(int k=1; k<argc; k++) 
  {    
    if(strcmp( argv[k], "-debug") == 0)
    {    
      sscanf( argv[++k], "%d", &debug );
      continue;
    }    
    else if(strcmp( argv[k], "-vfe") == 0)
    {    
      sscanf( argv[++k], "%d", &vfe );
      continue;
    }
    else if(strcmp( argv[k], "-Channel_num") == 0)
    {    
      sscanf( argv[++k], "%d", &Channel_num );
      continue;
    }
    else if(strcmp( argv[k], "-nevt") == 0)
    {    
      sscanf( argv[++k], "%d", &nevent );
      continue;
    }
    else if(strcmp(argv[k],"-nsample") == 0)
    {    
      sscanf( argv[++k], "%d", &nsample );
      continue;
    }    
    else if(strcmp(argv[k],"-soft_trigger") == 0)
    {    
// soft_trigger
// 0 : Use external trigger (GPIO)
// 1 : Generate trigger from software (1 written in FW register 
      sscanf( argv[++k], "%d", &soft_trigger );
      continue;
    }    
    else if(strcmp(argv[k],"-trigger_type") == 0)
    {    
// trigger_type
// 0 : pedestal
// 1 : TP
// 2 : laser
      sscanf( argv[++k], "%d", &trigger_type );
      continue;
    }
    else if(strcmp(argv[k],"-self_trigger") == 0)
    {    
// self_trigger
// 0 : Don't generate trigger from data themselves
// 1 : Generate trigger if any data > self_trigger_threshold
      sscanf( argv[++k], "%d", &self_trigger );
      continue;
    }
    else if(strcmp(argv[k],"-self_trigger_threshold") == 0)
    {    
// self_trigger_threshold in ADC counts
      sscanf( argv[++k], "%d", &self_trigger_threshold );
      continue;
    }
    else if(strcmp(argv[k],"-self_trigger_mask") == 0)
    {    
// channel mask to generate self trigger lsb=ch0 ... msb=ch4
      sscanf( argv[++k], "%x", &self_trigger_mask );
      continue;
    }
    else if(strcmp(argv[k],"-hw_DAQ_delay") == 0)
    {
      sscanf( argv[++k], "%d", &hw_DAQ_delay );
      continue;
    }    
    else if(strcmp(argv[k],"-sw_DAQ_delay") == 0)
    {
      sscanf( argv[++k], "%d", &sw_DAQ_delay );
      continue;
    }    
    else if(strcmp(argv[k],"-TP_mode") == 0)
    {    
// TP mode : 0=external trigger does not generate TP
//           1=external trigger generate TP trigger
      sscanf( argv[++k], "%d", &TP_mode );
      continue;
    }    
    else if(strcmp(argv[k],"-TP_level") == 0)
    {    
// DAC_value 0 ... 4095
      sscanf( argv[++k], "%d", &TP_level );
      continue;
    }    
    else if(strcmp(argv[k],"-TP_width") == 0)
    {    
// TP trigger width 0 ... 65532
      sscanf( argv[++k], "%d", &TP_width );
      continue;
    }    
    else if(strcmp(argv[k],"-TP_delay") == 0)
    {    
// DAQ delay for TP triggers : 0..65532
      sscanf( argv[++k], "%d", &TP_delay );
      continue;
    }    
    else if(strcmp(argv[k],"-n_TP_step") == 0)
    {    
// Number of TP step for linearity study
      sscanf( argv[++k], "%d", &n_TP_step );
      continue;
    }    
    else if(strcmp(argv[k],"-TP_step") == 0)
    {    
// DAC step for linearity study
      sscanf( argv[++k], "%d", &TP_step );
      continue;
    }    
    else if(strcmp(argv[k],"-TP_gain") == 0)
    {    
// Rconv for current injection (0=2471 Ohms=G1, 1=272 Ohms=G10)
      sscanf( argv[++k], "%d", &TP_gain );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_data") == 0)
    {    
      sscanf( argv[++k], "%x", &CATIA_data );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_reg") == 0)
    {    
      sscanf( argv[++k], "%d", &CATIA_reg );
      continue;
    }    
    else if(strcmp(argv[k],"-I2C_dir") == 0)
    {    
      sscanf( argv[++k], "%d", &I2C_dir );
      continue;
    }    
    else if(strcmp(argv[k],"-ADC_calib_mode") == 0)
    {    
      sscanf( argv[++k], "%d", &ADC_calib_mode );
      continue;
    }    
    else if(strcmp(argv[k],"-ADC_test_mode") == 0)
    {    
      sscanf( argv[++k], "%d", &ADC_test_mode );
      continue;
    }    
    else if(strcmp(argv[k],"-ADC_invert_data") == 0)
    {    
      sscanf( argv[++k], "%d", &ADC_invert_data );
      continue;
    }    
    else if(strcmp(argv[k],"-synchronize_ADC") == 0)
    {    
      sscanf( argv[++k], "%d", &synchronize_ADC );
      continue;
    }    
    else if(strcmp(argv[k],"-clock_phase") == 0)
    {    
      sscanf( argv[++k], "%d", &clock_phase );
      seq_clock_phase=(clock_phase>>0)&0x7;
      IO_clock_phase =(clock_phase>>4)&0x7;
      reg_clock_phase=(clock_phase>>8)&0x7;
      mem_clock_phase=(clock_phase>>12)&0x7;
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_Reg1_def") == 0)
    {    
      sscanf( argv[++k], "%x", &CATIA_Reg1_def );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_Reg3_def") == 0)
    {    
      sscanf( argv[++k], "%x", &CATIA_Reg3_def );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_Reg4_def") == 0)
    {    
      sscanf( argv[++k], "%x", &CATIA_Reg4_def );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_Reg5_def") == 0)
    {    
      sscanf( argv[++k], "%x", &CATIA_Reg5_def );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_Reg6_def") == 0)
    {    
      sscanf( argv[++k], "%x", &CATIA_Reg6_def );
      continue;
    }    
    else if(strcmp(argv[k],"-reset_ADC") == 0)
    {    
      sscanf( argv[++k], "%d", &reset_ADC );
      reset_ADC&=3;
      continue;
    }    
    else if(strcmp(argv[k],"-use_ref_ADC_calib") == 0)
    {    
      sscanf( argv[++k], "%d", &use_ref_ADC_calib );
      continue;
    }    
    else if(strcmp(argv[k],"-swap_ADC_calib_voltage") == 0)
    {    
      sscanf( argv[++k], "%d", &swap_ADC_calib_voltage );
      continue;
    }    
    else if(strcmp(argv[k],"-shift_odd_samples") == 0)
    {    
      sscanf( argv[++k], "%d", &shift_odd_samples );
      continue;
    }    
    else if(strcmp(argv[k],"-calib_ADC") == 0)
    {    
      sscanf( argv[++k], "%d", &calib_ADC );
      calib_ADC&=3;
      continue;
    }    
    else if(strcmp(argv[k],"-bulky_I2C") == 0)
    {
      sscanf( argv[++k], "%d", &bulky_I2C );
      if(bulky_I2C>0)bulky_I2C=1;
      continue;
    }
    else if(strcmp(argv[k],"-retry_I2C") == 0)
    {
      sscanf( argv[++k], "%d", &retry_I2C );
      if(retry_I2C>=(1<<14)) retry_I2C=(1<<14)-1;
      continue;
    }
    else if(strcmp(argv[k],"-static_DTU_reset") == 0)
    {
      sscanf( argv[++k], "%d", &static_DTU_reset );
      if(static_DTU_reset!=0) static_DTU_reset=1;
      continue;
    }
    else if(strcmp(argv[k],"-LVRB_autoscan") == 0)
    {
      sscanf( argv[++k], "%d", &LVRB_autoscan );
      continue;
    }
    else if(strcmp(argv[k],"-invert_Resync") == 0)
    {
      sscanf( argv[++k], "%d", &invert_Resync );
      continue;
    }
    else if(strcmp(argv[k],"-Resync_phase") == 0)
    {
      sscanf( argv[++k], "%d", &resync_phase );
      resync_phase&=3;
      continue;
    }
    else if(strcmp(argv[k],"-eLink_active") == 0)
    {
      sscanf( argv[++k], "%x", &eLink_active );
      eLink_active&=0x1f;
      continue;
    }
    else if(strcmp(argv[k],"-PLL_conf") == 0)
    {
      sscanf( argv[++k], "%x", &pll_conf );
      pll_conf&=0xff;
      continue;
    }
    else if(strcmp(argv[k],"-I2C_toggle_SDA") == 0)
    {
      sscanf( argv[++k], "%x", &I2C_toggle_SDA );
      continue;
    }
    else if(strcmp(argv[k],"-init_DTU") == 0)
    {
      sscanf( argv[++k], "%x", &init_DTU );
      continue;
    }
    else if(strcmp(argv[k],"-inv_clk") == 0)
    {
      sscanf( argv[++k], "%x", &inv_clk );
      continue;
    }
    else if(strcmp(argv[k],"-DAC_command") == 0)
    {
      sscanf( argv[++k], "%x", &DAC_command[nDAC_command++] );
      continue;
    }
    else if(strcmp(argv[k],"-do_ped_scan") == 0)
    {
      sscanf( argv[++k], "%x", &do_ped_scan );
      continue;
    }
    else if(strcmp(argv[k],"-do_calib_loop") == 0)
    {
      sscanf( argv[++k], "%x", &do_calib_loop );
      continue;
    }
    else if(strcmp(argv[k],"-dither") == 0)
    {
      sscanf( argv[++k], "%x", &dither );
      continue;
    }
    else if(strcmp(argv[k],"-global_test") == 0)
    {
      sscanf( argv[++k], "%x", &global_test );
      continue;
    }
    else if(strcmp(argv[k],"-min_ack") == 0)
    {
      sscanf( argv[++k], "%x", &min_ack );
      continue;
    }
    else if(strcmp(argv[k],"-nsample_calib_x4") == 0)
    {
      sscanf( argv[++k], "%x", &nsample_calib_x4 );
      continue;
    }
    else if(strcmp(argv[k],"-ped_mux") == 0)
    {
      sscanf( argv[++k], "%x", &ped_mux );
      continue;
    }
    else if(strcmp(argv[k],"-do_VFE_reset") == 0)
    {
      sscanf( argv[++k], "%d", &do_VFE_reset );
      continue;
    }
    else if(strcmp(argv[k],"-do_IO_reset") == 0)
    {
      sscanf( argv[++k], "%d", &do_IO_reset );
      continue;
    }
    else if(strcmp(argv[k],"-do_pwup_reset") == 0)
    {
      sscanf( argv[++k], "%d", &do_pwup_reset );
      continue;
    }
    else if(strcmp(argv[k],"-do_reset_all") == 0)
    {
      sscanf( argv[++k], "%d", &reset_all );
      continue;
    }
    else if(strcmp(argv[k],"-Resync_idle_pattern") == 0)
    {
      sscanf( argv[++k], "%x", &Resync_idle_pattern );
      Resync_idle_pattern&=0xff;
      continue;
    }
    else if(strcmp(argv[k],"-Resync_Hamming_data") == 0)
    {
      sscanf( argv[++k], "%x", &Resync_Hamming_data );
      Resync_Hamming_data&=0xffffff;
      continue;
    }
    else if(strcmp(argv[k],"-display_min") == 0)
    {
      sscanf( argv[++k], "%lf", &display_min );
      continue;
    }
    else if(strcmp(argv[k],"-display_max") == 0)
    {
      sscanf( argv[++k], "%lf", &display_max );
      continue;
    }
    else if(strcmp(argv[k],"-h") == 0)
    {
      printf("Start DAQ with : \n");
      printf("-debug debug_level        : Set debug level for this session [0]\n");
      printf("-nevt n                   : Number of events to record  [1000]\n");
      printf("-nsample n                : Number of sample per event (max=28670) [150]\n");
      printf("-trigger type n           : 0=pedestal, 1=calibration, 2=laser [0]\n");
      printf("-soft_trigger n           : 0=externally triggered DAQ, 1=softwared triggered DAQ [0]\n");
      printf("-self_trigger n           : 1=internal generated trigger if signal > threshold [0]\n");
      printf("-self_trigger_threshold n : minimal signal amplitude to generate self trigger [0]\n");
      printf("-self_trigger_mask n      : channel mask to generate self triggers, ch0=lsb    [0x1F]\n");
      printf("-TP_mode 0/1              : 1=external trigger generates calibration trigger in VFE [0]\n");
      printf("-TP_width n               : width of the calibration trigger sent to VFE [128]\n");
      printf("-TP_delay n               : delay between calibration trigger and DAQ start [0]\n");
      printf("-TP_level n               : DAC level to start linearity study [32768]\n");
      printf("-n_TP_step n              : number of calibration steps for linearity study [1]\n");
      printf("-TP_step n                : DAC step for linearity study [128]\n");
      printf("-TP_gain n                : Conversion gain for current pulse injection (0=2471 Ohms, 1=272 Ohms) [0]\n");
      printf("-ADC_calib_mode n         : Put (1) or not (0) CATIA in ADC_calib_mode (outputs near VCM) [0]\n");
      printf("-ADC_test_mode n          : Put (1) or not (0) iLiTE-DTU in test_mode (direct access to both ADCs outputs) [0]\n");
      printf("-reset_ADC n              : Reset ADCH (1), ADCL (2) or both (3) in boot procedure [0]\n");
      printf("-use_ref_ADC_calib n      : Load (1) ADC registers with result of previous calibration [0]\n");
      printf("-swap_ADC_calib_voltage n : Use (1) 0.35/0.85 V reference voltages for ADC calibration instead of 0.85/0.35 V [0]\n");
      printf("-calib_ADC n              : Launch auto calibration of ADCH (1), ADCL (2) or both (3) in boot procedure [0]\n");
      printf("-CATIA_data n             : data to write in CATIA register [-1]\n");
      printf("-CATIA_reg n              : CATIA register to read from/write in with I2C (1 to 6) [-1]\n");
      printf("                          : 1 : Slow Control reg [R/W]\n");
      printf("                          : 2 : SEU error counter reg [R]\n");
      printf("                          : 3 : TIA reg [R/W]\n");
      printf("                          : 4 : Inj DAC1 reg [R/W]\n");
      printf("                          : 5 : Inj DAC2 reg [R/W]\n");
      printf("                          : 6 : Inj Ctl reg [R/W]\n");
      printf("-CATIA_Reg1_def x         : default content of CATIA register 2 [0x02]\n");
      printf("-CATIA_Reg3_def x         : default content of CATIA register 3 [0x1087]\n");
      printf("-CATIA_Reg4_def x         : default content of CATIA register 4 [0x1000]\n");
      printf("-CATIA_Reg5_def x         : default content of CATIA register 5 [0x0000]\n");
      printf("-CATIA_Reg6_def x         : default content of CATIA register 6 [0x0b]\n");
      printf("-DAC_command x            : b23..20=action, b19..16=DAC number, b15..0=DAC value\n");
      printf("                            DAC number : b19=1 -> DAC_VP, b16=1 -> DAC_VN, b18=b17=0\n"); 
      printf("                            DAC value : lsb=15.259uV  -> 0.35V=0x59c0, 0.60V=0x99c0, 0.85V=0xd9c0\n");
      printf("                            DAC action : 0b0011 -> Write register and update output\n");
      printf("                            Multiple orders allowed, e.g. : -DAC_command 38d9c0 -DAC_command 3159c0\n");
      printf("-do_ped_scan n            : perform pedestal scan with DAC (40000 steps, 1 event/step) [0]\n");
      printf("-ped_mux n                : force mux to be (1) or not (0) in DAC position [0]\n");
      printf("-TP_step n                : external DAC step for INL/DNL/missing codes study [-1]\n");
      printf("                            Increment/decrement DAC value at each event. Force multiplexer in CALIB position\n");
      printf("-I2C_dir n                : Read (2) + write (1) with I2C protocol. 0=don't use\n");
      printf("-I2C_long                 : Perform long (1) or short (0) I2C transfer [0]\n");
      printf("-bulky_I2C                : Perform LiTE-DTU I2C setup using bulk transfer (1) or not (0) [0]\n");
      printf("-retry_I2C                : Retry (1) or not (0) I2C transfer until success [0]\n");
      printf("-static_DTU_reset         : Try I2C under (1) or not (0) DTU reset state [0]\n");
      printf("-init_DTU                 : Send (1) DTU init sequence or not (0) [1]\n");
      printf("-PLL_conf                 : Set PLL configuration register with value [0x3c]\n");
      printf("-do_pwup_reset            : Simulate power up reset at startup [0]\n");
      printf("-do_VFE_reset             : Reset VFE board at startup [0]\n");
      printf("-do_IO_reset              : Reset IO timing at startup [0]\n");
      printf("-do_reset_all             : Simulate Pwup reset, do IO_reset, VFE_reset, etc [0]\n");
      printf("-LVRB_autoscan            : Run LVRB autoscan mode (1) or not (0) [0]\n");
      printf("-I2C_toggle_SDA           : Toggle SDA line on DTU I2C bus during idle time [0]\n");
      printf("-invert_Resync            : Invert (1) or not (0) Resync signal polarities [0]\n");
      printf("-Resync_phase             : Set the Phase of teh resync signal wrt 160 MHz clock by 90 deg step (0..3) [0]\n");
      printf("-eLink_active             : Bit pattern of active eLinks (0x1f for full VFE board, 0x01 for test board and 0x0f for test board in test mode) [0x0f]");
      printf("-Resync_Hamming_data      : Send a custom ReSync code (3 bytes) [-1]\n");
      printf("-synchronize_ADC          : Perform synchronization step of the ADC links after reset\n");
      printf("-delay                    : Number of us to wait between cycles [100]\n");
      printf("-inv_clk                  : Inverse ADC clock to cure even/odd samples swap [0]\n");
      printf("-debug                    : Print debugging infos [0]\n");

      exit(-1);
    }
    else
    {
      printf("---------------------------------------------------\n");
      printf("Option %s not defined !, stop here.\n",argv[k]);
      printf("---------------------------------------------------\n");
      exit(-99);
    }
  }
  printf("Start DAQ with cards : %d\n", vfe);
  printf("Will address catias : %d with subaddress 3\n",(Channel_num<<2));
  printf("            and DTU : %d with subaddresses 0,1,2\n",(Channel_num<<2));

// Force mux to send DAC signals to ADC if requested
  nsample_save=nsample;

  DTU_bulk1=(DTU_bulk1&0xffffefff) | (inv_clk<<4);
  //if(trigger_type==0 || trigger_type==1)
  //{
  //  soft_trigger=1;
  //  self_trigger=0;
  //}
  //else if(trigger_type==2)
  //{
  //  soft_trigger=0;
  //  self_trigger=1;
  //  self_trigger_threshold=14000;
  //}
  printf("Parameters : \n");
  printf("Read ADCs for :\n");
  printf("  %d events \n",nevent);
  printf("  %d samples \n",nsample);
  printf("  trigger type  : %d (0=pedestal, 1=TP, 2=laser)\n",trigger_type);
  printf("  soft trigger  : %d (0=externally triggered DAQ, 1=softwared triggered DAQ)\n",soft_trigger);
  printf("  self trigger  : %d (1=internal generated trigger if signal > threshold)\n",self_trigger);
  printf("  threshold     : %d (minimal signal amplitude to generate self trigger)\n",self_trigger_threshold);
  printf("  mask          : 0x%x (channel mask to generate self triggers)\n",self_trigger_mask);

  if(trigger_type==1)
  {
    printf("Generate TP triggers :\n");
    printf("  %d events \n",nevent);
    printf("  TP_width   : %d (width of the calibration trigger sent to VFE)\n",TP_width);
    printf("  TP_delay   : %d (delay between calibration trigger and DAQ start)\n",TP_delay);
    printf("  n_TP_steps : %d (number of calibration steps for linearity study)\n",n_TP_step);
    printf("  TP_step    : %d (DAC step for linearity study)\n",TP_step);
  }
  if(n_TP_step==0)n_TP_step=1;

  for(int ich=0; ich<5; ich++)
  {
    delay_val[ich]=0; 
    bitslip_val[ich]=0;  
    byteslip_val[ich]=0; 
  }

  Int_t loc_argc=1;
  char *loc_argv[10];
  for(int i=0; i<10; i++)loc_argv[i]=(char *)malloc(132*sizeof(char));
  sprintf(loc_argv[0],"test");
  TApplication *Root_App=new TApplication("test", &loc_argc, loc_argv);
  TCanvas *c1=new TCanvas("c1","c1",1000,0,800.,1000.);
  c1->Divide(2,3);
  TGraph *tg[6];
  TGraph *tg_dac_VP, *tg_dac_VN, *tg_rms[6], *tg_resi[6], *tg_diff[2];
  TGraphErrors *tg_mean[6];
  TH1D *hmean[6], *hrms[6], *hdensity[6];
  double rms[6];
  char hname[80];
  Int_t dac_val=TP_level;
  for(int ich=0; ich<6; ich++)
  {
    tg[ich] = new TGraph();
    tg[ich]->SetMarkerStyle(20);
    tg[ich]->SetMarkerSize(0.5);
    sprintf(hname,"mean_ch%d",ich);
    hmean[ich]=new TH1D(hname,hname,100,150.,250.);
    sprintf(hname,"rms_ch%d",ich);
    hrms[ich]=new TH1D(hname,hname,200,0.,2.);
    sprintf(hname,"code_density_ch%d",ich);
    hdensity[ich]=new TH1D(hname,hname,4096,-0.5,4095.5);
    if(do_ped_scan==1)
    {
      sprintf(hname,"rms_vs_evt_ch%d",ich);
      tg_rms[ich] = new TGraph();
      tg_rms[ich]->SetMarkerStyle(20);
      tg_rms[ich]->SetMarkerSize(0.5);
      tg_rms[ich]->SetLineColor(kBlue);
      tg_rms[ich]->SetName(hname);
      tg_rms[ich]->SetName(hname);
      tg_rms[ich]->SetTitle(hname);
      sprintf(hname,"mean_vs_evt_ch%d",ich);
      tg_mean[ich] = new TGraphErrors();
      tg_mean[ich]->SetMarkerStyle(20);
      tg_mean[ich]->SetMarkerSize(0.5);
      tg_mean[ich]->SetMarkerColor(kBlue);
      tg_mean[ich]->SetLineColor(kBlue);
      tg_mean[ich]->SetName(hname);
      tg_mean[ich]->SetTitle(hname);
      sprintf(hname,"resi_vs_evt_ch%d",ich);
      tg_resi[ich] = new TGraph();
      tg_resi[ich]->SetMarkerStyle(20);
      tg_resi[ich]->SetMarkerSize(0.5);
      tg_resi[ich]->SetMarkerColor(kBlue);
      tg_resi[ich]->SetLineColor(kBlue);
      tg_resi[ich]->SetName(hname);
      tg_resi[ich]->SetTitle(hname);
    }
    dac_val=TP_level;
    for(int istep=0; istep<n_TP_step; istep++)
    {
      sprintf(hname,"ch_%d_step_%d_%d",ich,istep,dac_val);
      if(ich<4)
        pshape[istep][ich]=new TProfile(hname,hname,nsample*2,0.,12.5*nsample*2);
      else
        pshape[istep][ich]=new TProfile(hname,hname,nsample*4,0.,6.25*nsample*4);
      dac_val+=TP_step;
    }
  }
  dac_val=TP_level;
  if(do_ped_scan==1)
  {
    sprintf(hname,"dac_val_VP_vs_evt");
    tg_dac_VP = new TGraph();
    tg_dac_VP->SetMarkerStyle(20);
    tg_dac_VP->SetMarkerSize(0.5);
    tg_dac_VP->SetName(hname);
    tg_dac_VP->SetTitle(hname);
    sprintf(hname,"dac_val_VN_vs_evt");
    tg_dac_VN = new TGraph();
    tg_dac_VN->SetMarkerStyle(20);
    tg_dac_VN->SetMarkerSize(0.5);
    tg_dac_VN->SetName(hname);
    tg_dac_VN->SetTitle(hname);
    tg_diff[0]=new TGraph();
    tg_diff[0]->SetTitle("Odd_Even_diff_ADCH");
    tg_diff[0]->SetName("Odd_Even_diff_ADCH");
    tg_diff[0]->SetMarkerStyle(20);
    tg_diff[0]->SetMarkerSize(0.5);
    tg_diff[0]->SetMarkerColor(kRed);
    tg_diff[0]->SetLineColor(kRed);
    tg_diff[1]=new TGraph();
    tg_diff[1]->SetTitle("Odd_Even_diff_ADCL");
    tg_diff[1]->SetName("Odd_Even_diff_ADCL");
    tg_diff[1]->SetMarkerStyle(20);
    tg_diff[1]->SetMarkerSize(0.5);
    tg_diff[1]->SetMarkerColor(kRed);
    tg_diff[1]->SetLineColor(kRed);
  }
  tg[0]->SetLineColor(kCyan);
  tg[1]->SetLineColor(kCyan);
  tg[2]->SetLineColor(kMagenta);
  tg[3]->SetLineColor(kMagenta);
  tg[4]->SetLineColor(kBlue);
  tg[5]->SetLineColor(kRed);
  tg[0]->SetMarkerColor(kCyan);
  tg[1]->SetMarkerColor(kCyan);
  tg[2]->SetMarkerColor(kMagenta);
  tg[3]->SetMarkerColor(kMagenta);
  tg[4]->SetMarkerColor(kBlue);
  tg[5]->SetMarkerColor(kRed);
  c1->Update();


  ConnectionManager manager ( "file://xml/FEAD/connection_file.xml" );
  char fead_str[80];
  sprintf(fead_str,"fead.udp.%d",vfe);
  uhal::HwInterface hw=manager.getDevice( fead_str );

  ValWord<uint32_t> free_mem;
  ValWord<uint32_t> trig_reg;
  ValWord<uint32_t> delays;
  ValWord<uint32_t> reg;

// Reset board
  if(reset_all==1)
  {
    do_pwup_reset=1;
    do_VFE_reset=1;
    do_IO_reset=1;
    init_DTU=1;
  }
  if(synchronize_ADC==1 || do_pwup_reset==1)
  {
// Hard reset :
    printf("Generate PowerUp reset\n");
    hw.getNode("VFE_CTRL").write(1*PWUP_RESETB);
    hw.getNode("VFE_CTRL").write(0*PWUP_RESETB);
    hw.dispatch();
  }
  if(synchronize_ADC==1 || do_VFE_reset==1)
  {
    printf("Generate Warm reset\n");
// Soft reset :
    hw.getNode("FEAD_CTRL").write(RESET*1);
    hw.getNode("FEAD_CTRL").write(RESET*0);
    hw.dispatch();
  }

  if(synchronize_ADC==1 || do_IO_reset==1)
  {
    hw.getNode("DELAY_CTRL").write(1*DELAY_RESET);
    printf("Get lock status of IDELAY input stages\n");
    reg = hw.getNode("DELAY_CTRL").read();
    hw.dispatch();
    printf("Value read : 0x%x\n",reg.value());
  }

  unsigned int VFE_control= BULKY_I2C_DTU*bulky_I2C | N_RETRY_I2C_DTU*retry_I2C |
                            INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                            PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                            ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode;
// First : test I2C
// Read all registers of DTU and compare with expected values after pwup_reset
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.getNode("RESYNC_IDLE").write(I2C_toggle_SDA*I2C_TOGGLE_SDA  | RESYNC_IDLE_PATTERN*Resync_idle_pattern | ADC_INVERT_DATA*ADC_invert_data );
  hw.getNode("CLK_SETTING").write(resync_phase*RESYNC_PHASE);
  hw.dispatch();
  usleep(200);

  if(init_DTU==1)
  {
    printf("Prepare LiTEDTU for safe running (generate ReSync start sequence)\n");
// DTU Resync init sequence:
    //hw.getNode("DTU_RESYNC").write(LiTEDTU_stop);
    hw.getNode("DTU_RESYNC").write(LiTEDTU_start);
    //hw.getNode("DTU_RESYNC").write(LiTEDTU_DTU_reset);
    //hw.getNode("DTU_RESYNC").write(LiTEDTU_I2C_reset);
    //hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCTestUnit_reset);
    if((reset_ADC&1) == 1) hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCH_reset);
    if((reset_ADC&2) == 2) hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCL_reset);
    hw.getNode("DTU_BULK1").write(DTU_bulk1);
    hw.getNode("DTU_BULK2").write(DTU_bulk2);
    DTU_bulk3=(DTU_bulk3&0xffff00ff)|(pll_conf<<8);
    hw.getNode("DTU_BULK3").write(DTU_bulk3);
    hw.getNode("DTU_BULK4").write(DTU_bulk4);
    hw.getNode("DTU_BULK5").write(DTU_bulk5);
    hw.dispatch();
  }
  for(Int_t i=0; i<nDAC_command; i++)
  {
    hw.getNode("DAC_CTRL").write(DAC_command[i]);
    if(((DAC_command[i]&0x0f0000)>>16)==8)DAC_VP=DAC_command[i]&0xffff;
    if(((DAC_command[i]&0x0f0000)>>16)==1)DAC_VN=DAC_command[i]&0xffff;
    hw.dispatch();
  }
  usleep(200);

// Put FEAD outputs with idle patterns :
  hw.getNode("OUTPUT_CTRL").write(0);
  hw.dispatch();
  
// TP trigger setting :
  command=(TP_delay<<16) | (TP_width&0xffff);
  printf("TP trigger with %d clocks width and %d clocks delay : %x\n",TP_width,TP_delay,command);
  hw.getNode("CALIB_CTRL").write(command);
  hw.dispatch();

// Init stage :
// Read FW version to check :
  reg = hw.getNode("FW_VER").read();
// Switch to triggered mode + external trigger :
  command= 
            (SELF_TRIGGER_MASK     *(self_trigger_mask&0x1F))        |
            (SELF_TRIGGER_THRESHOLD*(self_trigger_threshold&0x3FFF)) |
            //(CLOCK_PHASE           *(clock_phase&0x7))               |
             SELF_TRIGGER          *self_trigger                     |
             SOFT_TRIGGER          *soft_trigger                     |
             TRIGGER_MODE          *1                                | // Always DAQ on trigger
             RESET                 *0;
  hw.getNode("FEAD_CTRL").write(command);
// Stop DAQ and ask for NSAMPLE per frame (+timestamp) :
  command = ((nsample+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
// Add laser latency before catching data ~ 40 us
  hw.getNode("TRIG_DELAY").write(SW_DAQ_DELAY*sw_DAQ_delay | HW_DAQ_DELAY*hw_DAQ_delay);
// Switch off FE-adapter LEDs
  command = TP_MODE*0+LED_ON*0+GENE_100HZ*0+GENE_TP*0+GENE_TRIGGER*0;
  hw.getNode("FW_VER").write(command);
  hw.dispatch();
// Set the clock phases
  command = MEM_CLOCK_PHASE*mem_clock_phase+REG_CLOCK_PHASE*reg_clock_phase+IO_CLOCK_PHASE*IO_clock_phase+SEQ_CLOCK_PHASE*seq_clock_phase;
  hw.getNode("CLK_SETTING").write(command);
  hw.dispatch();

// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
// Start DAQ :
  command = ((nsample+1)<<16)+CAPTURE_START;
  hw.getNode("CAP_CTRL").write(command);
// Read back delay values :
  delays=hw.getNode("TRIG_DELAY").read();
// Read back the read/write base address
  address = hw.getNode("CAP_ADDRESS").read();
  free_mem = hw.getNode("CAP_FREE").read();
  trig_reg = hw.getNode("FEAD_CTRL").read();
  hw.dispatch();

  printf("Firmware version      : %8.8x\n",reg.value());
  printf("Delays                : %8.8x\n",delays.value());
  printf("Initial R/W addresses : 0x%8.8x\n", address.value());
  printf("Free memory           : 0x%8.8x\n", free_mem.value());
  printf("Trigger mode          : 0x%8.8x\n", trig_reg.value());
  old_read_address=address&0xffff;
  if(old_read_address==NSAMPLE_MAX-1)old_read_address=-1;

  
  if(calib_ADC>0) 
  {
// Ask for 4 times more calibration samples for ADCs
    VFE_control= BULKY_I2C_DTU*0 | N_RETRY_I2C_DTU*retry_I2C |
                 INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                 PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                 ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode;
    hw.getNode("VFE_CTRL").write(VFE_control);
    hw.dispatch();
    unsigned int device_number, val;
    if(dither==1)
    {
      val=I2C_RW(hw, I2C_LiTEDTU_TYPE*1000+(Channel_num<<2)+0, 3, 0x01, 0, 1, debug);
      val=I2C_RW(hw, I2C_LiTEDTU_TYPE*1000+(Channel_num<<2)+1, 3, 0x01, 0, 1, debug);
    }
    if(nsample_calib_x4==1)
    {
      val=I2C_RW(hw, I2C_LiTEDTU_TYPE*1000+(Channel_num<<2)+0, 1, 0xfe, 0, 1, debug);
      val=I2C_RW(hw, I2C_LiTEDTU_TYPE*1000+(Channel_num<<2)+1, 1, 0xfe, 0, 1, debug);
    }
    if(global_test==1)
    {
      val=I2C_RW(hw, I2C_LiTEDTU_TYPE*1000+(Channel_num<<2)+0, 0, 0x01, 0, 1, debug);
      val=I2C_RW(hw, I2C_LiTEDTU_TYPE*1000+(Channel_num<<2)+1, 0, 0x01, 0, 1, debug);
    }
    VFE_control= BULKY_I2C_DTU*bulky_I2C | N_RETRY_I2C_DTU*retry_I2C |
                 INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                 PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                 ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode;
  
// Set DAC values for ADC calibration :
    if(swap_ADC_calib_voltage==1)
    {
      hw.getNode("DAC_CTRL").write(0x31d9c0);
      hw.dispatch();
      usleep(1000);
      hw.getNode("DAC_CTRL").write(0x3859c0);
    }
    else
    {
      hw.getNode("DAC_CTRL").write(0x38d9c0);
      hw.dispatch();
      usleep(1000);
      hw.getNode("DAC_CTRL").write(0x3159c0);
    }
    hw.getNode("VFE_CTRL").write(VFE_control | PED_MUX*1);
    hw.dispatch();
    usleep(10000);
  }

// Bulky write of DTU registers
// During init, we launch the ADC autocalibration during register 2 writing
  if(init_DTU==1)
  {
    unsigned int device_number, val;
    device_number=I2C_LiTEDTU_TYPE*1000+(Channel_num<<2)+2;      // DTU sub-address
    printf("Device number : %d 0x%x\n",device_number,device_number);
    do
    {
      val=I2C_RW(hw, device_number, 0, 0, 0, 1, debug);
      printf("Bulky write return code : %8.8x -> %d ACK\n",val,(val>>23)&0xff);
    }
    while(((val>>23)&0xff) <min_ack);
  }
  usleep(500);

  if(calib_ADC>0)
  {
    hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCH_reset);
    hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCL_reset);
    hw.dispatch();
    usleep(200);
    for(int i=0; i<5; i++)
    {
      hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCH_calib);
      hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCL_calib);
      hw.dispatch();
      usleep(10000);
    }
  }

  if(synchronize_ADC==1) iret=synchronize_links(hw, Channel_num, debug, eLink_active);

  if(use_ref_ADC_calib==1)
  {
    printf("Loading ADC calibration coefficient from ref file\n");
    VFE_control= BULKY_I2C_DTU*0 | N_RETRY_I2C_DTU*retry_I2C |
                 INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                 PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                 ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode;
    hw.getNode("VFE_CTRL").write(VFE_control);
    hw.dispatch();
    fcal=fopen("ref_LiTE-DTU_calib_reg.dat","r");
    for(int ireg=0; ireg<75; ireg++)
    {
      fscanf(fcal,"%d %d\n",&ADC_reg_val[0][ireg], &ADC_reg_val[1][ireg]);
    }
    fclose(fcal);
    for(int iADC=0; iADC<2; iADC++)
    {
      int device_number=I2C_LiTEDTU_TYPE*1000+(Channel_num<<2)+iADC;
      for(int ireg=0; ireg<75; ireg++)
      {
        iret=I2C_RW(hw, device_number, ireg, ADC_reg_val[iADC][ireg], 0, 1, debug);
      }
    }
  }

// Restore DAC mux in normal position after calibration
  VFE_control= BULKY_I2C_DTU*bulky_I2C | N_RETRY_I2C_DTU*retry_I2C |
               INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
               PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
               ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();

  Long_t timestamp=0;
  TTree *tdata=new TTree("data","data");
  tdata->Branch("timestamp",&timestamp,"timestamp/l");
  for(int ich=0; ich<6; ich++)
  {
    char bname[80], btype[80];
    sprintf(bname,"ch%d",ich);
    if(ich<4)
      sprintf(btype,"ch%d[%d]/S",ich,nsample_save*2);
    else
      sprintf(btype,"ch%d[%d]/S",ich,nsample_save*4);
    tdata->Branch(bname,event[ich],btype);
  }

// Program CATIA according to wishes :
// Setup default values for CATIA:
  unsigned int device_number = I2C_CATIA_TYPE*1000+(Channel_num<<2+3);

// SEU auto correction, no Temp output
  val=I2C_RW(hw, device_number, 1, CATIA_Reg1_def, 0, 3, debug);
  printf("Put Reg1 content to 0x%x : 0x%x\n",CATIA_Reg1_def,val);

// Gain 400 Ohm, Output stage for 1.2V, LPF35, 0 pedestal
  val=I2C_RW(hw, device_number, 3, CATIA_Reg3_def, 1, 3, debug);
  printf("Put Reg3 content to 0x%x : 0x%x\n",CATIA_Reg3_def,val);

// DAC1 0, DAC2 0, DAC1 ON, DAC2 OFF, DAC1 copied on DAC2
  val=I2C_RW(hw, device_number, 4, CATIA_Reg4_def, 1, 3, debug);
  printf("Put Reg4 content to 0x%x : 0x%x\n",CATIA_Reg4_def,val);

// DAC2 mid scale but OFF, so should not matter
  //val=I2C_RW(hw, device_number, 5, 0xffff, 1, 3, debug);
// DAC2 at 0 but OFF, so should not matter
  val=I2C_RW(hw, device_number, 5, CATIA_Reg5_def, 1, 3, debug);
  printf("Put Reg5 content to 0x%x : 0x%x\n",CATIA_Reg5_def,val);

// TIA dummy ON, Rconv 2471 (G10 scale), Vref ON (0xb) OFF (0x3), Injection in CATIA
  if(trigger_type!=1)
    CATIA_Reg6_def=0x0; // switch off every thing for pedestal events
  else
    CATIA_Reg6_def=0xb | (TP_gain<<2);

  val=I2C_RW(hw, device_number, 6, CATIA_Reg6_def, 0, 3, debug);
  printf("Put Reg6 content to 0x%x : 0x%x\n",CATIA_Reg6_def,val);

// Custom I2C settings
  if(CATIA_reg>=0 && I2C_dir>0)
  {
    printf("Access CATIA with I2C :\n");
    I2C_long=0;
    if(CATIA_reg==3 || CATIA_reg==4 || CATIA_reg==5)I2C_long=1;

    printf("Device number : %d 0x%x\n",device_number,device_number);
    val=I2C_RW(hw, device_number, CATIA_reg, CATIA_data,I2C_long, I2C_dir, debug);
    printf("CATIA %d, reg %d : value read 0x%x, data = 0x%x\n",Channel_num, CATIA_reg, CATIA_data, CATIA_data&0xffff);
  }

  if(n_TP_step<0)exit(-1);


  Int_t n_word=(nsample+1)*6; // 6*32 bits words per sample to get the 5 channels data
  Int_t n_transfer=n_word/(MAX_PAYLOAD/4); // Max ethernet packet = 1536 bytes, max user payload = 1500 bytes
  Int_t n_last=n_word-n_transfer*(MAX_PAYLOAD/4);
  printf("Reading events by blocks of %dx32b-words, %d bits\n",n_word, n_word*32);
  printf("Using %d transfers of %d words + 1 transfer of %d words\n",n_transfer, MAX_PAYLOAD/4,n_last);
  if(n_transfer > 463)
  {
    printf("Event size too big ! Please reduce number of samples per frame.\n");
    printf("Max frame size : %d\n",NSAMPLE_MAX);
  }

// Send triggers and wait between each trigger :
  Int_t draw=0;
  for(int istep=0; istep<n_TP_step; istep++)
  {
// Program TP-DAC for this step
    unsigned int device_number=I2C_CATIA_TYPE*1000+(Channel_num<<2)+3;
    unsigned int val;
    if(TP_level>=0)
    {
      val=I2C_RW(hw, device_number, 4, 0x1000 | (TP_level&0xfff),1, 3, debug);
      printf("Put %d in DAC register : 0x%x\n",TP_level,val);
// Wait for Vdac to stabilize :
    }
    else
    {
// switch off all the injection system
      val=I2C_RW(hw, device_number, 4, 0x0,1, 3, debug);
    }

// For ped scan, start from bottom line
    if(do_ped_scan==1)
    {
      DAC_VP=DAC_VCM-20000;
      hw.getNode("DAC_CTRL").write(0x38<<16|DAC_VP);
      hw.dispatch();
      usleep(1000);
      DAC_VN=DAC_VCM+20000;
      hw.getNode("DAC_CTRL").write(0x31<<16|DAC_VN);
      hw.dispatch();
      usleep(10000);
    }
    usleep(100000);

    Int_t ievt=0;
    printf("start sending triggers :\n"); 
    while(ievt<nevent)
    {
      if(do_ped_scan==1)
      {
        tg_dac_VP->SetPoint(ievt,ievt,DAC_VP);
        tg_dac_VN->SetPoint(ievt,ievt,DAC_VN);
      }
      if((ievt%100)==0)printf("%d\n",ievt); 
      if(debug>0 || ievt==0)
      {
// In debug mode, we reinit DAQ buffer at each event :
        command = ((nsample+1)<<16)+CAPTURE_STOP;
        hw.getNode("CAP_CTRL").write(command);
        hw.getNode("CAP_ADDRESS").write(0);
        command = ((nsample+1)<<16)+CAPTURE_START;
        hw.getNode("CAP_CTRL").write(command);
        hw.dispatch();
      }
      timestamp=get_event(hw,trigger_type,nsample,debug,draw);

      double ped[6]={0.}, ave[6]={0.}, rms[6]={0.};
      double max=0.;

      for(int ich=0; ich<4; ich++)
      {
        for(int isample=0; isample<nsample*2; isample++)
        {
          tg[ich]->SetPoint(isample,12.5*isample,fevent[ich][isample]);
          ave[ich]+=dv*fevent[ich][isample];
          if(isample<30)ped[ich]+=dv*fevent[ich][isample];
          rms[ich]+=dv*fevent[ich][isample]*dv*fevent[ich][isample];
          pshape[istep][ich]->Fill(12.5*isample+1.,dv*fevent[ich][isample]);
          hdensity[ich]->Fill(event[ich][isample]);
          if(ich==1 && dv*event[ich][isample]>max)max=dv*event[ich][isample];
        }
      }
      for(int ich=4; ich<6; ich++)
      {
        for(int isample=0; isample<nsample*4; isample++)
        {
          tg[ich]->SetPoint(isample,6.25*isample,fevent[ich][isample]);
          ave[ich]+=dv*fevent[ich][isample];
          if(isample<30)ped[ich]+=dv*fevent[ich][isample];
          rms[ich]+=dv*fevent[ich][isample]*dv*fevent[ich][isample];
          pshape[istep][ich]->Fill(6.25*isample+1.,dv*fevent[ich][isample]);
          hdensity[ich]->Fill(event[ich][isample]);
        }
      }
      for(int ich=0; ich<6; ich++)
      {
        int loc_sample=nsample*2;
        if(ich>=4)loc_sample=nsample*4;
        ave[ich]/=loc_sample;
        ped[ich]/=30.;
        rms[ich]/=loc_sample;
        rms[ich]=sqrt(rms[ich]-ave[ich]*ave[ich]);
        if(debug>0)printf("ich %d : ped=%f, ave=%f, rms=%f\n",ich,ped[ich],ave[ich],rms[ich]);
        hmean[ich]->Fill(ave[ich]);
        hrms[ich]->Fill(rms[ich]);
        if(do_ped_scan==1)
        {
          tg_mean[ich]->SetPoint(ievt,DAC_step*(double(DAC_VP)-double(DAC_VN)),ave[ich]);
          tg_rms[ich]->SetPoint(ievt,DAC_step*(double(DAC_VP)-double(DAC_VN)),rms[ich]);
        }
      }
      if(trigger_type==0 || max>0.)
      {
        tdata->Fill();
        if((ngood_event%200)==0)printf("%d events recorded\n",ngood_event);
        ngood_event++;
      }
      //if(debug>0 && max>0.)
      if(debug>0 || (ievt%100)==1)
      {
        
        if(display_min>=0)
        {
          tg[0]->SetMinimum(display_min);
          tg[1]->SetMinimum(display_min);
          tg[2]->SetMinimum(display_min);
          tg[3]->SetMinimum(display_min);
          tg[4]->SetMinimum(display_min);
          tg[5]->SetMinimum(display_min);
        }
        if(display_max>=0)
        {
          tg[0]->SetMaximum(display_max);
          tg[1]->SetMaximum(display_max);
          tg[2]->SetMaximum(display_max);
          tg[3]->SetMaximum(display_max);
          tg[4]->SetMaximum(display_max);
          tg[5]->SetMaximum(display_max);
        }
        c1->cd(1);
        tg[0]->Draw("alp");
        c1->cd(3);
        tg[1]->Draw("alp");
        c1->cd(2);
        tg[2]->Draw("alp");
        c1->cd(4);
        tg[3]->Draw("alp");
        c1->cd(5);
        tg[4]->Draw("alp");
        c1->cd(6);
        tg[5]->Draw("alp");
        c1->Update();
        if(debug==1)
        {
          UInt_t utmp;
          Int_t redo_calib=0;
          Int_t inc_PLL=0;
          Int_t bit_slip=-1;
          Int_t byte_slip=-1;
          Int_t delay_auto_tune=-1;
          Int_t increase_delay=-1;
          Int_t delay_reset=0;
          Int_t dump_ADC=0;
          Int_t set_clock=0, increase_seq_clock=0, increase_IO_clock=0, increase_reg_clock=0, increase_mem_clock=0;
          printf("Press any key to continue :\n");
          printf("z : reset line delays\n");
          printf("1<n<5 : tune ADC n line delays\n");
          printf("+ : increase delay of ADC n lines %3d%3d%3d%3d%3d\n",delay_val[0],delay_val[1], delay_val[2], delay_val[3], delay_val[4]);
          printf("- : decrease delay of ADC n lines\n");
          printf("S : increase sequence clock phase by 45 deg (%d)\n",seq_clock_phase);
          printf("I : increase IO clock phase by 45 deg (%d)\n",IO_clock_phase);
          printf("R : increase register clock phase by 45 deg (%d)\n",reg_clock_phase);
          printf("M : increase memory clock phase by 45 deg (%d)\n",mem_clock_phase);
          printf("b : slip incoming bits by 1 unit (%3d%3d%3d%3d%3d)\n",bitslip_val[0],bitslip_val[1],bitslip_val[2],bitslip_val[3],bitslip_val[4]);
          printf("B : slip incoming bytes by 1 unit (%3d%3d%3d%3d%3d)\n",byteslip_val[0],byteslip_val[1],byteslip_val[2],byteslip_val[3],byteslip_val[4]);
          printf("p : increase PLL config register content (%d)\n",pll_conf);
          printf("P : decrease PLL config register content (%d)\n",pll_conf);
          printf("T : Launch automatic IDELAY tuning\n");
          printf("m : Dump ADC register maps\n");
          printf("r : Load reference ADCs register map\n");
          printf("i : Reload I2C registers in bulky mode\n");
          printf("D : Increase DAC value\n");
          printf("d : Decrease DAC value\n");
          printf("t : Toggle DAC values 0x%x 0x%x\n",DAC_VP, DAC_VN);
          printf("c : Redo ADC calibration\n");
          printf("u : Toggle DAC mux\n");
          printf("g : Go ! remove debug\n");

          system("stty raw");
          if(do_calib_loop==1)
          {
            if((ievt%3)==0) cdum='c';
            if((ievt%3)==1) cdum='t';
            if((ievt%3)==2) cdum='t';
          }
          else
            cdum=getchar();
          system("stty -raw");
          while (cdum=='1' || cdum=='2' || cdum=='3' || cdum=='4' || cdum=='5')
          {
            if(cdum=='1')channel_sel=0;
            if(cdum=='2')channel_sel=1;
            if(cdum=='3')channel_sel=2;
            if(cdum=='4')channel_sel=3;
            if(cdum=='5')channel_sel=4;
            printf("Channel selected : %d ",channel_sel+1);
            system("stty raw");
            cdum=getchar();
            system("stty -raw");
          }
          if(cdum=='q' || cdum=='Q')
          {
            //hw.getNode("VFE_CTRL").write(STATIC_RESET*0 | ADC_TEST_MODE*ADC_test_mode);
            //hw.dispatch();
            printf("\n");
            ievt=nevent;
            break;
            //exit(-1);
          }
          switch(cdum)
          {
            case 'b':
              bitslip_val[channel_sel]=(bitslip_val[channel_sel]+1)%8;
              command=(1<<channel_sel)*BIT_SLIP | DELAY_RESET*0 | DELAY_INCREASE*0 | 0;
              hw.getNode("DELAY_CTRL").write(command);
              hw.dispatch();
              printf("Get lock status of IDELAY input stages\n");
              reg = hw.getNode("DELAY_CTRL").read();
              hw.dispatch();
              printf("Value read : 0x%x\n",reg.value());
              break;

            case 'B':
              byteslip_val[channel_sel]=(byteslip_val[channel_sel]+1)%4;
              command=(1<<channel_sel)*BYTE_SLIP | DELAY_RESET*0 | DELAY_INCREASE*0 | 0;
              hw.getNode("DELAY_CTRL").write(command);
              hw.dispatch();
              printf("Get lock status of IDELAY input stages\n");
              reg = hw.getNode("DELAY_CTRL").read();
              hw.dispatch();
              printf("Value read : 0x%x\n",reg.value());
              break;

            case 'T':
              iret=synchronize_links(hw, Channel_num, debug,eLink_active);
              break;

            case '+':
            case '=':
              delay_val[channel_sel]=(delay_val[channel_sel]+1)%32;
              if(delay_val[channel_sel]<0)delay_val[channel_sel]+=32;
              command=DELAY_RESET*0 | DELAY_INCREASE*1 | (1<<channel_sel);
              hw.getNode("DELAY_CTRL").write(command);
              break;

            case '-':
              delay_val[channel_sel]=(delay_val[channel_sel]-1)%32;
              if(delay_val[channel_sel]<0)delay_val[channel_sel]+=32;
              command=DELAY_RESET*0 | DELAY_INCREASE*0 | (1<<channel_sel);
              hw.getNode("DELAY_CTRL").write(command);
              hw.dispatch();
              break;

            case 'z':
              command=DELAY_RESET*delay_reset;
              hw.getNode("DELAY_CTRL").write(command);
              hw.dispatch();
              printf("Get lock status of IDELAY input stages\n");
              reg = hw.getNode("DELAY_CTRL").read();
              hw.dispatch();
              printf("Value read : 0x%x\n",reg.value());
              break;

            case 'S':
              if(increase_seq_clock==1)seq_clock_phase=(seq_clock_phase+1)%8;
              command = MEM_CLOCK_PHASE*mem_clock_phase+REG_CLOCK_PHASE*reg_clock_phase+IO_CLOCK_PHASE*IO_clock_phase+SEQ_CLOCK_PHASE*seq_clock_phase;
              hw.getNode("CLK_SETTING").write(command);
              hw.dispatch();
              break;

            case 'I':
              if(increase_IO_clock==1) IO_clock_phase=(IO_clock_phase+1)%8;
              command = MEM_CLOCK_PHASE*mem_clock_phase+REG_CLOCK_PHASE*reg_clock_phase+IO_CLOCK_PHASE*IO_clock_phase+SEQ_CLOCK_PHASE*seq_clock_phase;
              hw.getNode("CLK_SETTING").write(command);
              hw.dispatch();
              break;

            case 'R':
              if(increase_reg_clock==1)reg_clock_phase=(reg_clock_phase+1)%8;
              command = MEM_CLOCK_PHASE*mem_clock_phase+REG_CLOCK_PHASE*reg_clock_phase+IO_CLOCK_PHASE*IO_clock_phase+SEQ_CLOCK_PHASE*seq_clock_phase;
              hw.getNode("CLK_SETTING").write(command);
              hw.dispatch();
              break;

            case 'M':
              if(increase_mem_clock==1)mem_clock_phase=(mem_clock_phase+1)%8;
              command = MEM_CLOCK_PHASE*mem_clock_phase+REG_CLOCK_PHASE*reg_clock_phase+IO_CLOCK_PHASE*IO_clock_phase+SEQ_CLOCK_PHASE*seq_clock_phase;
              hw.getNode("CLK_SETTING").write(command);
              hw.dispatch();
              break;

            case 'i':
              printf("Reload I2C register map of LiTE-DTU\n");
              VFE_control= BULKY_I2C_DTU*1 | N_RETRY_I2C_DTU*retry_I2C |
                           INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                           PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                           ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode;
              hw.getNode("VFE_CTRL").write(VFE_control);
              do
              {
                iret=I2C_RW(hw, I2C_LiTEDTU_TYPE*1000+(Channel_num<<2)+2, 0, 0, 0, 1, debug);
                printf("Bulky write return code : %8.8x -> %d ACK\n",iret,(iret>>23)&0xff);
              }
              while(((iret>>23)&0xff) < min_ack);
              break;

            case 'm':
              VFE_control= BULKY_I2C_DTU*0 | N_RETRY_I2C_DTU*retry_I2C |
                           INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                           PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                           ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode;
              hw.getNode("VFE_CTRL").write(VFE_control);
              hw.dispatch();
              for(int iADC=0; iADC<2; iADC++)
              {
                int device_number=I2C_LiTEDTU_TYPE*1000+(Channel_num<<2)+iADC;
                for(int ireg=0; ireg<75; ireg++)
                {
                  ADC_reg_val[iADC][ireg]=I2C_RW(hw, device_number, ireg, 0, 0, 2, debug);
                }
              }
              fcal=fopen("last_LiTE-DTU_calib_reg.dat","w+");
              for(int ireg=0; ireg<75; ireg++)
              {
                printf("Register %d : 0x%8.8x 0x%8.8x\n",ireg,ADC_reg_val[0][ireg], ADC_reg_val[1][ireg]);
                fprintf(fcal,"%d %d\n",ADC_reg_val[0][ireg]&0xff, ADC_reg_val[1][ireg]&0xff);
              }
              fclose(fcal);
              VFE_control= BULKY_I2C_DTU*bulky_I2C | N_RETRY_I2C_DTU*retry_I2C |
                           INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                           PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                           ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode;
              hw.getNode("VFE_CTRL").write(VFE_control);
              hw.dispatch();
              break;

            case 'r':
              VFE_control= BULKY_I2C_DTU*0 | N_RETRY_I2C_DTU*retry_I2C |
                           INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                           PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                           ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode;
              hw.getNode("VFE_CTRL").write(VFE_control);
              hw.dispatch();
              fcal=fopen("ref_LiTE-DTU_calib_reg.dat","r");
              for(int ireg=0; ireg<75; ireg++)
              {
                fscanf(fcal,"%d %d\n",&ADC_reg_val[0][ireg], &ADC_reg_val[1][ireg]);
              }
              fclose(fcal);
              for(int iADC=0; iADC<2; iADC++)
              {
                int device_number=I2C_LiTEDTU_TYPE*1000+(Channel_num<<2)+iADC;
                for(int ireg=0; ireg<75; ireg++)
                {
                  iret=I2C_RW(hw, device_number, ireg, ADC_reg_val[iADC][ireg], 0, 1, debug);
                }
              }
              VFE_control= BULKY_I2C_DTU*bulky_I2C | N_RETRY_I2C_DTU*retry_I2C |
                           INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                           PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                           ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode;
              hw.getNode("VFE_CTRL").write(VFE_control);
              hw.dispatch();
              break;

            case 'p':
              inc_PLL=-1;
            case 'P':
              if(inc_PLL==0)inc_PLL=1;
              if(pll_conf==0 && inc_PLL==-1) pll_conf=255;
              else pll_conf+=inc_PLL;
              if(pll_conf==256)pll_conf=0;
              VFE_control= BULKY_I2C_DTU*1 | N_RETRY_I2C_DTU*retry_I2C |
                           INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                           PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                           ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode;
              hw.getNode("VFE_CTRL").write(VFE_control);
              DTU_bulk3=(DTU_bulk3&0xffff00ff)|(pll_conf<<8);
              hw.getNode("DTU_BULK3").write(DTU_bulk3);
              hw.dispatch();
              do
              {
                //iret=I2C_RW(hw, 2000+(Channel_num<<2), 9, pll_conf, 0, 1, 2);
                //iret=I2C_RW(hw, 2000+(Channel_num<<2)+2, 0, 0, 0, 1, debug);
                iret=I2C_RW(hw, I2C_LiTEDTU_TYPE*1000+(Channel_num<<2)+2, 0, 0, 0, 1, debug);
              }
              while(((iret>>23)&0xff) <0);
              printf("Bulky write return code : %8.8x -> %d ACK\n",iret,(iret>>23)&0xff);
              VFE_control= BULKY_I2C_DTU*bulky_I2C | N_RETRY_I2C_DTU*retry_I2C |
                           INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                           PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                           ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode;
              hw.getNode("VFE_CTRL").write(VFE_control);
              hw.dispatch();
              break;
            case 't':
              utmp=DAC_VP;
              DAC_VP=DAC_VN;
              DAC_VN=utmp;
              hw.getNode("DAC_CTRL").write(0x38<<16|DAC_VP);
              hw.dispatch();
              usleep(1000);
              hw.getNode("DAC_CTRL").write(0x31<<16|DAC_VN);
              hw.dispatch();
              usleep(10000);
              break;
            case 'd':
              DAC_VP--;
              DAC_VN++;
              hw.getNode("DAC_CTRL").write(0x38<<16|DAC_VP);
              hw.dispatch();
              usleep(1000);
              hw.getNode("DAC_CTRL").write(0x31<<16|DAC_VN);
              hw.dispatch();
              usleep(10000);
              break;
            case 'D':
              DAC_VP++;
              DAC_VN--;
              hw.getNode("DAC_CTRL").write(0x38<<16|DAC_VP);
              hw.dispatch();
              usleep(1000);
              hw.getNode("DAC_CTRL").write(0x31<<16|DAC_VN);
              hw.dispatch();
              usleep(10000);
              break;
            case 'c':
              if(swap_ADC_calib_voltage==1)
              {
                hw.getNode("DAC_CTRL").write(0x31d9c0);
                hw.dispatch();
                usleep(1000);
                hw.getNode("DAC_CTRL").write(0x3859c0);
              }
              else
              {
                hw.getNode("DAC_CTRL").write(0x38d9c0);
                hw.dispatch();
                usleep(1000);
                hw.getNode("DAC_CTRL").write(0x3159c0);
              }
              VFE_control= BULKY_I2C_DTU*0 | N_RETRY_I2C_DTU*retry_I2C |
                           INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                           PED_MUX*1 | eLINK_ACTIVE*eLink_active |
                           ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode;
              hw.getNode("VFE_CTRL").write(VFE_control);
              hw.dispatch();
              usleep(100000); // Wait for DAC stabilization
              hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCH_reset);
              hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCL_reset);
              hw.dispatch();
              if(global_test==1)
              {
                val=I2C_RW(hw, I2C_LiTEDTU_TYPE*1000+(Channel_num<<2)+0, 0, 0x01, 0, 1, debug);
                val=I2C_RW(hw, I2C_LiTEDTU_TYPE*1000+(Channel_num<<2)+1, 0, 0x01, 0, 1, debug);
              }
              if(nsample_calib_x4==1)
              {
                val=I2C_RW(hw, I2C_LiTEDTU_TYPE*1000+(Channel_num<<2)+0, 1, 0xfe, 0, 1, debug);
                val=I2C_RW(hw, I2C_LiTEDTU_TYPE*1000+(Channel_num<<2)+1, 1, 0xfe, 0, 1, debug);
              }
              if(dither==1)
              {
                val=I2C_RW(hw, I2C_LiTEDTU_TYPE*1000+(Channel_num<<2)+0, 3, 0x01, 0, 1, debug);
                val=I2C_RW(hw, I2C_LiTEDTU_TYPE*1000+(Channel_num<<2)+1, 3, 0x01, 0, 1, debug);
              }
              usleep(200);
              hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCH_calib);
              hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCL_calib);
              hw.dispatch();
              usleep(10000);
              VFE_control= BULKY_I2C_DTU*bulky_I2C | N_RETRY_I2C_DTU*retry_I2C |
                           INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                           PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                           ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode;
              hw.getNode("VFE_CTRL").write(VFE_control);
              hw.dispatch();
              DAC_VP--;
              DAC_VN++;
              break;
            case 'u':
              ped_mux=1-ped_mux;
              VFE_control= BULKY_I2C_DTU*bulky_I2C | N_RETRY_I2C_DTU*retry_I2C |
                           INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                           PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                           ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode;
              hw.getNode("VFE_CTRL").write(VFE_control);
              //hw.getNode("VFE_CTRL").write((VFE_control & PED_MUX*0) | PED_MUX*ped_mux);
              hw.dispatch();
              break;
            case 'g':
              debug=0;
              break;
          }
        }
        if(debug==2)usleep(500000);
      }
      if(debug>0)
      {
        command = ((nsample+1)<<16)+CAPTURE_STOP;
        hw.getNode("CAP_CTRL").write(command);
        hw.dispatch();
        old_read_address=-1;
      }

      if(do_ped_scan==1)
      {
  // For ped scan, move by one step
        DAC_VP++;
        hw.getNode("DAC_CTRL").write(0x38<<16|DAC_VP);
        hw.dispatch();
        usleep(1000);
        DAC_VN--;
        hw.getNode("DAC_CTRL").write(0x31<<16|DAC_VN);
        hw.dispatch();
        usleep(10000);
      }
      ievt++;
    }
    TP_level+=TP_step;
  }

// Stop DAQ :
  command = ((nsample+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
  // Switch on FE-adapter LEDs
  command = LED_ON*1+GENE_100HZ*0+GENE_TRIGGER*0;
  hw.getNode("FW_VER").write(command);
  hw.dispatch();

  TCanvas *c2=new TCanvas("mean","mean",800.,1000.);
  c2->Divide(2,3);
  c2->Update();
  TCanvas *c3=new TCanvas("rms","rms",800.,1000.);
  c3->Divide(2,3);
  c3->Update();
  printf("RMS : ");
  for(int ich=0; ich<6; ich++)
  {
    c2->cd(ich+1);
    hmean[ich]->Draw();
    c2->Update();
    c3->cd(ich+1);
    hrms[ich]->Draw();
    rms[ich]=hrms[ich]->GetMean();
    printf("%e, ",rms[ich]);
    c3->Update();
  }
  printf("\n");
  if(trigger_type==0)
    sprintf(output_file,"data/LiTEDTU/ped_data.root");
  else if(trigger_type==1)
    sprintf(output_file,"data/LiTEDTU/TP_data.root");
  else if(trigger_type==2)
    sprintf(output_file,"data/LiTEDTU/laser_data.root");
  else
    sprintf(output_file,"data/LiTEDTU/fead_data.root");

  TFile *fd=new TFile(output_file,"recreate");
  tdata->Write();
  if(do_ped_scan==1)
  {
    for(Int_t ich=0; ich<6; ich++)
    {
      tg_rms[ich]->Write();
      for(Int_t is=0; is<tg_rms[ich]->GetN(); is++)
      {
        Double_t x,y,ex,ey;
        tg_rms[ich]->GetPoint(is,x,y);
        ex=0.001;
        ey=0.3;
        if(y>1. || y<0.05)ey=1.e6;
        tg_mean[ich]->SetPointError(is,ex,ey);
      }
      tg_mean[ich]->Fit("pol1","q","",-550.,550.);
      f1=tg_mean[ich]->GetFunction("pol1");
      for(int is=0; is<tg_mean[ich]->GetN(); is++)
      {
        Double_t x,y;
        tg_mean[ich]->GetPoint(is,x,y);
        tg_mean[ich]->SetPointError(is,0.001,0.001);
        tg_resi[ich]->SetPoint(is,x,y-f1->Eval(x));
      }
      tg_mean[ich]->Write();
      tg_resi[ich]->SetMaximum(1.);
      tg_resi[ich]->SetMinimum(-1.);
      tg_resi[ich]->Write();
    }
    tg_dac_VP->Write();
    tg_dac_VN->Write();
    for(int is=0; is<tg_mean[0]->GetN(); is++)
    {
      Double_t x,y0,y1;
      tg_mean[0]->GetPoint(is,x,y0);
      tg_mean[1]->GetPoint(is,x,y1);
      tg_diff[0]->SetPoint(is,x,y1-y0);
      tg_mean[2]->GetPoint(is,x,y0);
      tg_mean[3]->GetPoint(is,x,y1);
      tg_diff[1]->SetPoint(is,x,y1-y0);
    }
    tg_diff[0]->Write();
    tg_diff[1]->Write();
  }
  c1->Write();
  c2->Write();
  c3->Write();
  for(int istep=0; istep<n_TP_step; istep++)
  {
    for(int ich=0; ich<6; ich++)
    {
      pshape[istep][ich]->Write();
    }
  }
  for(int ich=0; ich<6; ich++) hdensity[ich]->Write();
  fd->Close();
  printf("Finished with %d events recorded\n",ngood_event);
}




