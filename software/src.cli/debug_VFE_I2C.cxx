#include "uhal/uhal.hpp"
#include <signal.h>

#include "I2C_RW.h"
#include "LiTEDTU_def.h"

#include <vector>
#include <iostream>
#include <cstdlib>
#include <typeinfo>

#include <TApplication.h>
#include <TProfile.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <TTree.h>
#include <TF1.h>
#include <TFile.h>

#define I2C_SHIFT_DEV_NUMBER   4

#define GENE_TRIGGER           (1<<0)
#define GENE_TP                (1<<1)
#define GENE_100HZ             (1<<2)
#define LED_ON                 (1<<3)
#define TP_MODE                (1<<4)

#define RESET                  (1<<0)
#define TRIGGER_MODE           (1<<1)
#define SELF_TRIGGER_LOOP      (1<<2)
#define SELF_TRIGGER           (1<<3)
#define CLOCK_PHASE            (1<<4)
#define CLOCK_RESET            (1<<7)
#define SELF_TRIGGER_THRESHOLD (1<<8)
#define SELF_TRIGGER_MASK      (1<<22)
#define BOARD_SN               (1<<28)
#define SELF_TRIGGER_MODE      (1<<31)

#define SEQ_CLOCK_PHASE        (1<<0)
#define IO_CLOCK_PHASE         (1<<4)
#define REG_CLOCK_PHASE        (1<<8)
#define MEM_CLOCK_PHASE        (1<<12)
#define RESYNC_PHASE           (1<<16)

#define I2C_TOGGLE_SDA         (1<<31)

#define PWUP_RESETB            (1<<31)
#define BULKY_I2C_DTU          (1<<28)
#define I2C_LPGBT_MODE         (1<<27)
#define N_RETRY_I2C_DTU        (1<<14)
#define PED_MUX                (1<<13)
#define BUGGY_I2C_DTU          (1<<4)
#define INVERT_RESYNC          (1<<3)
#define LVRB_AUTOSCAN          (1<<2)

#define CAPTURE_START 1
#define CAPTURE_STOP  2
//#define SW_DAQ_DELAY 0x1800 // delay for laser with internal trigger
#define SW_DAQ_DELAY (1<<16)  // delay for laser with external trigger
#define HW_DAQ_DELAY (1<<0)   // Laser with external trigger
#define NSAMPLE_MAX 26624
#define MAX_PAYLOAD 1380
#define MAX_VFE     10
#define MAX_STEP    10000

#define DRP_WRb         (1<<31)
#define DELAY_TAP_DIR   (1<<5)     // Increase (1) or decrease (0) tap delay in ISERDES
#define DELAY_RESET     (1<<6)     // Reset ISERDES delay
#define IO_RESET        (1<<7)     // Reset ISERDES IO
#define BIT_SLIP        (1<<16)    // Slip serial bits by 1 unit (to align bits in ISERDES) (to multiply with 5 bits channel map)
#define BYTE_SLIP       (1<<21)    // Slip serial bytes by 1 unit (to align bytes in 40 MHz clock) (to multiply with 5 bits channel map)
#define DELAY_AUTO_TUNE (1<<29)    // Launch autamatic IDELAY tuning to get the eye center of each stream

#define ADC_CALIB_MODE      (1<<0)
#define ADC_TEST_MODE       (1<<1)
#define ADC_MEM_MODE        (1<<5)
#define ADC_INVERT_DATA     (1<<8)
#define RESYNC_IDLE_PATTERN (1<<0)
#define eLINK_ACTIVE        (1<<8)
#define STATIC_RESET        (1<<31)

#define I2C_LVR_TYPE     0
#define I2C_CATIA_TYPE   1
#define I2C_LiTEDTU_TYPE 2

#define NSAMPLE_PED      30

using namespace uhal;
Int_t I2C_LiTEDTU_type, I2C_CATIA_type, I2C_shift_dev_number;
Int_t I2C_DTU_nreg, buggy_DTU;  
Int_t firmware_version;

struct timeval tv;
void end_of_run()
{
  gettimeofday(&tv,NULL);
  printf("%ld.%6.6ld : End of study\n",tv.tv_sec,tv.tv_usec);
  exit(1);
}

void intHandler(int)
{
  printf("Ctrl-C detected !\n");
  gettimeofday(&tv,NULL);
  printf("%ld.%6.6ld : Interupt detected\n",tv.tv_sec,tv.tv_usec);
  end_of_run();
}
void abortHandler(int)
{
  printf("Abort !\n");
  gettimeofday(&tv,NULL);
  printf("%ld.%6.6ld : Run aborted\n",tv.tv_sec,tv.tv_usec);
  end_of_run();
} 

Int_t main ( Int_t argc,char* argv[] )
{
  signal(SIGINT, intHandler);
  signal(SIGABRT, abortHandler);

  TF1 *f1;
  Int_t ngood_event=0;
  UInt_t soft_reset, full_reset, command;
  ValWord<uint32_t> address;
  Int_t debug=0;
  Int_t debug_draw=0;
  Int_t fead=4;
  Int_t VICEPP_clk=-1;
  char cdum, output_file[256];
  Int_t channel_sel=0;
  Int_t reg_val=0;
  Int_t ADC_reg_val[2][76];
  Int_t iret;
  Int_t reset_all=0;
  Int_t debug_I2C=0;
  UInt_t device_number=0x12;
  UInt_t reg_min=9;
  UInt_t reg_max=10;
  UInt_t n_usleep=0;
  UInt_t data_ref=0;
  UInt_t I2C_transaction=3;
  UInt_t I2C_lpGBT_mode=0;

//DTU settings if requested
  I2C_CATIA_type         = I2C_CATIA_TYPE;
  I2C_LiTEDTU_type       = I2C_LiTEDTU_TYPE;
  I2C_shift_dev_number   = I2C_SHIFT_DEV_NUMBER;

  unsigned int val;
  FILE *fcal;

  TProfile *pshape[MAX_STEP][5];
  sprintf(output_file,"");

  for(int k=1; k<argc; k++) 
  {    
    if(strcmp( argv[k], "-fead") == 0)
    {    
      sscanf( argv[++k], "%d", &fead );
      continue;
    }
    if(strcmp( argv[k], "-device_number") == 0)
    {    
      sscanf( argv[++k], "%x", &device_number );
      continue;
    }
    if(strcmp( argv[k], "-reg_min") == 0)
    {    
      sscanf( argv[++k], "%d", &reg_min );
      continue;
    }
    if(strcmp( argv[k], "-reg_max") == 0)
    {    
      sscanf( argv[++k], "%d", &reg_max );
      continue;
    }
    if(strcmp( argv[k], "-sleep") == 0)
    {    
      sscanf( argv[++k], "%d", &n_usleep );
      continue;
    }
    if(strcmp( argv[k], "-data") == 0)
    {    
      sscanf( argv[++k], "%x", &data_ref );
      continue;
    }
    if(strcmp( argv[k], "-I2C_transaction") == 0)
    {    
      sscanf( argv[++k], "%d", &I2C_transaction );
      I2C_transaction&=3;
      continue;
    }
    if(strcmp( argv[k], "-I2C_lpGBT_mode") == 0)
    {    
      sscanf( argv[++k], "%d", &I2C_lpGBT_mode );
      continue;
    }
    if(strcmp( argv[k], "-debug_I2C") == 0)
    {    
      sscanf( argv[++k], "%d", &debug_I2C );
      debug_I2C&=1;
      continue;
    }
    else if(strcmp(argv[k],"-h") == 0)
    {
      printf("Start DAQ with : \n");
      printf("-fead n              : Fead card number to address [4]\n");
      printf("-device_number x     : Chip address [0x12]\n");
      printf("-reg_min n           : Minimum register number [9]\n");
      printf("-reg_max n           : Maximum register number [10]\n");
      printf("-sleep n             : Number of us to wait in loop [0]\n");
      printf("-data x              : Data to write in registers [0x00]\n");
      printf("-I2C_transaction bbb : Write (xx1), Read (x1x) or buggy lpGBT Read (1xx) I2C transaction [3]\n");
      printf("-debug_I2C n         : Print (1) or not (0) I2C debug info [0]\n");
      exit(-1);
    }
    else
    {
      printf("---------------------------------------------------\n");
      printf("Option %s not defined !, stop here.\n",argv[k]);
      printf("---------------------------------------------------\n");
      exit(-99);
    }
  }
      
  printf("Start DAQ with cards : %d\n", fead);
  printf("Will address device : %x\n",device_number);
  printf("I2C transaction requested : %d\n",I2C_transaction);


  Int_t loc_argc=1;
  char *loc_argv[10];
  for(int i=0; i<10; i++)loc_argv[i]=(char *)malloc(132*sizeof(char));
  sprintf(loc_argv[0],"test");
  TApplication *Root_App=new TApplication("test", &loc_argc, loc_argv);

  ConnectionManager manager ( "file://xml/FEAD/connection_file.xml" );
  char fead_str[80];
  sprintf(fead_str,"fead.udp.%d",fead);
  uhal::HwInterface hw=manager.getDevice( fead_str );

  ValWord<uint32_t> free_mem;
  ValWord<uint32_t> trig_reg;
  ValWord<uint32_t> delays;
  ValWord<uint32_t> reg;
  ValWord<uint32_t> debug1_reg[32];
  ValWord<uint32_t> debug2_reg[32];

// Set the clock tree :
  if(VICEPP_clk >=0)
  {
    reg = hw.getNode("VICEPP_CLK").read();
    hw.dispatch();
    printf("Clock configuration aleady present : 0x%x\n",reg.value());
    if((reg.value()&3) == (VICEPP_clk&3))
    {
      printf("Present XPoint setting 0x%8.8x already conform to request 0x%8.8x\n",reg.value(),VICEPP_clk);
      printf("Do nothing !\n");
    }
    else
    {
      printf("Present XPoint setting 0x%8.8x not conform to request 0x%8.8x\n",reg.value(),VICEPP_clk);
      printf("Set clock configuration for VICE++ board : 0x%8.8x\n",VICEPP_clk);
      hw.getNode("VICEPP_CLK").write(VICEPP_clk);
      hw.dispatch();
      usleep(100000);
    }
  }

// Reset VFE board
  printf("Generate PowerUp reset\n");
  hw.getNode("VFE_CTRL").write(1*PWUP_RESETB);
  hw.getNode("VFE_CTRL").write(0*PWUP_RESETB);
  hw.dispatch();

// Select I2C in standard mode or lpGBT mode :
  hw.getNode("VFE_CTRL").write(I2C_LPGBT_MODE*I2C_lpGBT_mode);
  hw.dispatch();

// Init stage :
// Read FW version to check :
  reg = hw.getNode("FW_VER").read();
  hw.dispatch();
  firmware_version=reg.value();
  hw.dispatch();

  //firmware_version=0x21083100;
  printf("Firmware version      : %8.8x\n",firmware_version);

  printf("Prepare LiTEDTU for safe running (generate ReSync start sequence)\n");
// DTU Resync init sequence:
  if(firmware_version<0x21090000)
  {
    hw.getNode("DTU_RESYNC").write(LiTEDTU_DTU_reset);
    hw.dispatch();
    hw.getNode("DTU_RESYNC").write(LiTEDTU_I2C_reset);
    hw.dispatch();
    hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCTestUnit_reset);
    hw.dispatch();
    hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCH_reset);
    hw.dispatch();
    hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCL_reset);
    hw.dispatch();
  }
  else
  {
    hw.getNode("DTU_RESYNC").write((LiTEDTU_ADCTestUnit_reset<<16) | (LiTEDTU_I2C_reset<<8) | LiTEDTU_DTU_reset);
    hw.getNode("DTU_RESYNC").write((LiTEDTU_ADCL_reset<<8) | LiTEDTU_ADCH_reset);
    hw.dispatch();
  }

// Load default DTU registers
  Int_t use_DTU=1;
  if(device_number&3 == 3)use_DTU=0; // CATIA access
  Int_t data=data_ref;
  while(true)
  {
    UInt_t dev=I2C_LiTEDTU_type*1000+device_number;
    if(use_DTU==0) dev=I2C_CATIA_type*1000+device_number;
    //printf("Device number : %d 0x%x\n",device_number,device_number);
    for(Int_t ireg=reg_min; ireg<=reg_max; ireg++)
    {
      Int_t I2C_long=0;
      if(use_DTU==0 && (ireg==3 || ireg==4 || ireg==5))I2C_long=1;
      iret=I2C_RW(hw, dev, ireg, data, I2C_long, I2C_transaction, debug_I2C);
    }
    data++;
    usleep(n_usleep);
  }
}


