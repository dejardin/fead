#include "uhal/uhal.hpp"

#include <vector>
#include <iostream>
#include <cstdlib>
#include <typeinfo>

#include "I2C_RW.h"
#include <TApplication.h>
#include <TProfile.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TProfile.h>
#include <TVirtualFFT.h>
#include <TF1.h>
#include <TH1D.h>
#include <TTree.h>
#include <TFile.h>

#define GENE_TRIGGER           (1<<0)
#define TP_TRIGGER             (1<<1)
#define GENE_100HZ             (1<<2)
#define LED_ON                 (1<<3)
#define G1_CALIB_TRIGGER       (1<<4)
#define G10_CALIB_TRIGGER      (1<<5)

#define PWUP_RESET             (1<<31)
#define RESET                  (1<<0)
#define TRIGGER_MODE           (1<<1)
#define SOFT_TRIGGER           (1<<2)
#define SELF_TRIGGER           (1<<3)
#define CLOCK_PHASE            (1<<4)
#define CLOCK_RESET            (1<<7)
#define SELF_TRIGGER_THRESHOLD (1<<8)
#define SELF_TRIGGER_MASK      (1<<22)
#define SEL_CALIB_PULSE        (1<<23)
#define ADC_CALIB_MODE         (1<<27)
#define BOARD_SN               (1<<28)
#define SEQ_CLOCK_PHASE        (1<<0)
#define IO_CLOCK_PHASE         (1<<4)
#define REG_CLOCK_PHASE        (1<<8)
#define MEM_CLOCK_PHASE        (1<<12)
#define LVRB_AUTOSCAN          (1<<2)

#define CAPTURE_START 1
#define CAPTURE_STOP  2
#define SW_DAQ_DELAY 0x0 // delay for laser with external trigger
#define HW_DAQ_DELAY 100 // Laser with external trigger
#define NSAMPLE_MAX 65536
#define MAX_PAYLOAD 1380
#define MAX_VFE     10
#define MAX_STEP    10000

#define I2C_RWb         (1<<31)
#define I2C_LONG        (1<<30)
#define SPI_CATIA       (1<<26)
#define I2C_CATIA       (1<<25)
#define ADC_WRITE       (1<<24)
#define DAC_FULL_RESET  (0xF<<16)
#define DAC_SOFT_RESET  (0x7<<16)
#define DAC_VAL_REG     (0x3<<16)
#define DAC_CTRL_REG    (0x4<<16)

#define ADC_OMODE_REG   (0x14<<16) // ADC register to define Output mode of the ADC
#define ADC_ISPAN_REG   (0x18<<16) // ADC register to define input span of the ADC from 1.383V to 2.087V
#define ADC_CLK_DIVIDE  (0x0B<<16) // ADC register to define clock division of the ADC
#define ADC_OTEST_REG   (0x0D<<16) // ADC register to define test mode of the ADC

#define DELAY_INCREASE  (1<<5)     // Increase ADC signal delay by one tip
#define DELAY_RESET     (1<<6)     // Reset ADC signal delays

#define DRP_WRb         (1<<31)    // Acces to FGPA internal ADCs

using namespace uhal;

int get_event(uhal::HwInterface hw, int trigger_type, int nsample, ValVector< uint32_t >mem, short int *event[2], double *fevent[2], int debug, int draw)
{
  static int old_address=-1, ievt=0;
  static TGraph *tg[2]={NULL, NULL};
  static TCanvas *cdebug=NULL;
  static int trigger_type_old=-1;
  int n_word;
  int n_transfer_max=189;
  int n_transfer;
  int n_last;
  int max_address= 65536;
  int error      = 0;
  int command    = 0;

  if(trigger_type_old!=trigger_type)ievt=0;
  if(cdebug==NULL)
  {
    tg[0]=new TGraph();
    tg[1]=new TGraph();
    cdebug=new TCanvas();
    cdebug->Divide(1,2);
  }

  ValWord<uint32_t> address,free_mem;
  address = hw.getNode("CAP_ADDRESS").read();
  hw.dispatch();
  if(debug>0)printf("Starting with address : %8.8x, trigger_type : %d\n",address.value(),trigger_type);
  old_address=address.value()>>16;
  if(old_address==max_address-1)old_address=-1;

  ValVector< uint32_t > block;
  if(trigger_type !=2)
  {
    if(trigger_type == 0)
      command = G10_CALIB_TRIGGER*0 | G1_CALIB_TRIGGER*0 | LED_ON*0 | GENE_100HZ*0 | TP_TRIGGER*0 | GENE_TRIGGER*1;
    else if(trigger_type == 1)
      command = G10_CALIB_TRIGGER*0 | G1_CALIB_TRIGGER*0 | LED_ON*0 | GENE_100HZ*0 | TP_TRIGGER*1 | GENE_TRIGGER*0;
    else if(trigger_type == 3)
      command = G10_CALIB_TRIGGER*0 | G1_CALIB_TRIGGER*1 | LED_ON*0 | GENE_100HZ*0 | TP_TRIGGER*0 | GENE_TRIGGER*1;
    else if(trigger_type == 4)
      command = G10_CALIB_TRIGGER*1 | G1_CALIB_TRIGGER*0 | LED_ON*0 | GENE_100HZ*0 | TP_TRIGGER*0 | GENE_TRIGGER*1;

    if(debug>0)printf("Send trigger with command : 0x%8.8x\n",command);
// Read base address and send trigger
    free_mem = hw.getNode("CAP_FREE").read();
    hw.getNode("FW_VER").write(command);
    hw.dispatch();
    if(debug>0)printf("Free memory : 0x%8.8x\n",free_mem.value());
// Read new address and wait for DAQ completion
    address = hw.getNode("CAP_ADDRESS").read();
    hw.dispatch();
    if(debug>0)printf("Start address : 0x%8.8x\n",address.value());
    int new_address=address.value()>>16;
    int loc_address=new_address;
    int nretry=0;
    while(((loc_address-old_address)%max_address) != nsample+2 && nretry<100)
    {
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      new_address=address.value()>>16;
      loc_address=new_address;
      if(new_address<old_address)loc_address+=max_address;
      nretry++;
      if(debug>0)printf("ongoing R/W addresses    : old %d, new %d add 0x%8.8x\n", old_address, new_address,address.value());
    }
    if(nretry==100)
    {
      printf("Stop waiting for sample capture after %d retries\n",nretry);
      printf("R/W addresses    : old %8.8x, new %8.8x add 0x%8.8x\n", old_address, new_address, address.value());
      error=1;
    }
    old_address=new_address;
    if(old_address==max_address-1)old_address=-1;
    if(debug>0)printf("R/W addresses    : old %8.8x, new %8.8x add 0x%8.8x\n", old_address, new_address, address.value());
  }
  else
  {
    int nretry=0;
// Wait for external trigger to fill memory :
    while((free_mem.value()==max_address-1) && nretry<100)
    {
      free_mem = hw.getNode("CAP_FREE").read();
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      //printf("address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
      if(debug>0)printf("address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
      usleep(100);
      nretry++;
    }
    if(nretry==100)
    {
      printf("Stop waiting for external trigger after %d retries\n",nretry);
      printf("R/W addresses    : add 0x%8.8x\n",address.value());
      error=1;
    }
  }
  mem.clear();

// Read event samples from FPGA
  n_word=nsample+2;     // 1*32 bits words per sample to get the 2 channels data (G10 and G1)
  n_transfer=n_word/(MAX_PAYLOAD/4); // Max ethernet packet = 1536 bytes, max user payload = 1380 bytes
  n_last=n_word-n_transfer*(MAX_PAYLOAD/4);
  if(n_transfer>n_transfer_max)
  {
    printf("Error : ask for too many samples. Number of transfer %d greater than allowed max value %d\n",n_transfer, n_transfer_max);
    error=1;
  }
  for(int itrans=0; itrans<n_transfer; itrans++)
  {
    block = hw.getNode ("CAP_DATA").readBlock(MAX_PAYLOAD/4);
    hw.dispatch();
    for(int is=0; is<MAX_PAYLOAD/4; is++)mem.push_back(block[is]);
  }
  block = hw.getNode ("CAP_DATA").readBlock(n_last);
  address = hw.getNode("CAP_ADDRESS").read();
  free_mem = hw.getNode("CAP_FREE").read();
  hw.dispatch();
  if(debug>0)printf("After reading address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
  for(int is=0; is<n_last; is++)mem.push_back(block[is]);
  mem.valid(true);

  if(debug>0)
  {
    printf("%8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",
        mem[0],mem[1],mem[2],mem[3],mem[4],mem[5],mem[6],mem[7],mem[8],mem[9]);
    printf("%8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",
        mem[10],mem[11],mem[12],mem[13],mem[14],mem[15],mem[16],mem[17],mem[18],mem[19]);
    printf("%8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",
        mem[20],mem[21],mem[22],mem[23],mem[24],mem[25],mem[26],mem[27],mem[28],mem[29]);
    printf("%.4d %.4d %.4d %.4d %.4d %.4d %.4d %.4d\n",
        mem[2]&0x3fff,mem[3]&0x3fff,mem[4]&0x3fff,mem[5]&0x3fff,mem[6]&0x3fff,mem[7]&0x3fff,mem[8]&0x3fff,mem[9]&0x3fff);
    printf("%.4d %.4d %.4d %.4d %.4d %.4d %.4d %.4d\n",
        (mem[2]>>14)&0x3ff,(mem[3]>>14)&0x3fff,(mem[4]>>14)&0x3fff,(mem[5]>>14)&0x3fff,
        (mem[6]>>14)&0x3ff,(mem[7]>>14)&0x3fff,(mem[8]>>14)&0x3fff,(mem[9]>>14)&0x3fff);
  }
// First sample should have bit 28 downto 0 at 1
  if(mem[0] != 0x0FFFFFFF)
  {
    printf("Sample 0 not a header : %8.8x\n",mem[0]);
    error=1;
  }

  int timestamp= mem[1];
  if(debug>0) printf("timestamp : %8.8x\n",mem[1]);

  for(int isample=2; isample<nsample+2; isample++)
  {
    event[0][isample-2]= mem[isample]       &0x3FFF;
    event[1][isample-2]= (mem[isample]>>14) &0x3FFF;
    fevent[0][isample-2]=double(event[0][isample-2]);
    fevent[1][isample-2]=double(event[1][isample-2]);
  }

  if(debug>0 || trigger_type !=trigger_type_old || draw==1)
  {
    tg[0]->Clear();
    tg[1]->Clear();
    tg[0]->Set(0);
    tg[1]->Set(0);
    for(int isample=0; isample<nsample; isample++)
    {
      tg[0]->SetPoint(isample,6.25*isample,fevent[0][isample]);
      tg[1]->SetPoint(isample,6.25*isample,fevent[1][isample]);
    }
    cdebug->cd(1);
    tg[0]->SetLineColor(kRed);
    tg[0]->Draw("alp");
    cdebug->cd(2);
    tg[1]->SetLineColor(kBlue);
    tg[1]->Draw("alp");
    cdebug->Update();
  }
  if(debug>0)
  {
    system("stty raw");
    char cdum=getchar();
    system("stty -raw");
    if(cdum=='q')exit(-1);
  }

  trigger_type_old=trigger_type;
  ievt++;
  if(error==0)
    return timestamp;
  else
    return -1;
}

Int_t main ( Int_t argc,char* argv[] )
{
  Double_t NSPS=160.e6;
  Int_t timestamp;
  Int_t soft_reset, full_reset, command, fead_ctrl;
  ValWord<uint32_t> address;
  ValWord<uint32_t> data[3*NSAMPLE_MAX];
  ValVector< uint32_t > mem;
  short int *event[2];
  event[0]=(short int*)malloc(NSAMPLE_MAX*sizeof(short int));
  event[1]=(short int*)malloc(NSAMPLE_MAX*sizeof(short int));
  double *fevent[2];
  fevent[0]=(double*)malloc(NSAMPLE_MAX*sizeof(double));
  fevent[1]=(double*)malloc(NSAMPLE_MAX*sizeof(double));
  double dv=1750./16384.; // 14 bits on 1.75V
  Int_t debug=-1, draw=0;
  double ped[2], mean[2], rms[2];
  Double_t gain[2]={1.,10.};
  Double_t RTIA[2]={500.,400.};
  Double_t Rshunt_V1P2=0.51;
  Double_t Rshunt_V2P5=0.10;
  char cdum, output_file[256];
// Define defaults for laser runs :
  Int_t nevent                = 0;
  Int_t nsample               =-1;
  Int_t trigger_type          = 0; // pedestal by default
  Int_t self_trigger          = 0; // No self trigger 
  Int_t self_trigger_threshold= 0;
  Int_t self_trigger_mask     = 0; // don't trig on any channels amplitude
  Int_t ADC_calib_mode        = 0; // Put (1) or not (0) the outputs of CATIA in ADC_calib_mode (outputs near VCM)
  Int_t seq_clock_phase       = 0; // sequence clock phase by steps of 45 degrees
  Int_t IO_clock_phase        = 0; // Capture clock phase by steps of 45 degrees for CATIA test board
  Int_t reg_clock_phase       = 0; // Shift register clock phase by steps of 45 degrees
  Int_t mem_clock_phase       = 1; // memory clock phase by steps of 45 degrees
  Int_t clock_phase           = 0;
  Int_t sel_calib_pulse       = 1; // Default calib pulse enable. Otherwise High_Z
  Int_t n_calib               = 32;
  Int_t calib_gain            = 0;
  Int_t calib_step            = 128;
  Int_t calib_level           = 0;
  Int_t calib_width           = 200;
  Int_t calib_delay           = 50;
  Int_t tune_delay            = 1;
  Int_t delay_val[2]          = {0,0}; // Number of delay taps to get a stable R/O with ADCs (160 MHz)
  Int_t CATIA_number          = 9999;
  Int_t test_I2C              = 0;

// ADC settings if any :
  UInt_t output_test = 0;  // Put ADC outputs in test mode (0=normal conversion values, 0xF=ramp)
  UInt_t input_span  = 0;
  UInt_t signed_data = 0;
  UInt_t negate_data = 0;

// CATIA settings if requested
  Int_t n_vfe          = 0;
  Int_t vfe            = 0;
  Int_t n_catia        = 1;
  Int_t CATIA_num      = 1; // CATIA number to address
  Int_t average        = 128;
  Int_t XADC_reg, XADC_Temp, XADC_Vdac, XADC_Vbuf, XADC_Vref;
  XADC_Temp=0x11;
  XADC_Vref=0x10;
  XADC_Vdac=0x1c;
  XADC_Vbuf=0x13; // Buffered DAC signal for CATIA-V1-rev3

  Int_t n_Vref=15, n_Vdac=33, n_PED_dac=32;

  TF1 *f1, *fDAC[2], *fBuf[2];
  TProfile *pshape[MAX_STEP];
  sprintf(output_file,"");

  for(int k=1; k<argc; k++) 
  {    
    if(strcmp( argv[k], "-debug") == 0)
    {    
      sscanf( argv[++k], "%d", &debug );
      continue;
    }    
    else if(strcmp(argv[k],"-num") == 0)
    {
      sscanf( argv[++k], "%d", &CATIA_number );
      continue;
    }
    else if(strcmp(argv[k],"-test_I2C") == 0)
    {
      sscanf( argv[++k], "%d", &test_I2C );
      continue;
    }
    else if(strcmp(argv[k],"-h") == 0)
    {
      printf("Start DAQ with : \n");
      printf("-debug dbg_level            : Set debug level for this session [0]\n");
      printf("-num CATIA_number           : CATIA reference to identify the output file (rev*1000+i) [9999]\n");
      exit(-1);
    }
    else
    {
      printf("---------------------------------------------------\n");
      printf("Option %s not defined !, stop here.\n",argv[k]);
      printf("---------------------------------------------------\n");
      exit(-99);
    }
  }

  int loc_argc=1;
  char *loc_argv[10];
  for(int i=0; i<10; i++)loc_argv[i]=(char *)malloc(132*sizeof(char));
  sprintf(loc_argv[0],"test");
  TApplication *Root_App=new TApplication("test", &loc_argc, loc_argv);

  TGraph *tg[2];
  TH1D *hmean[2], *hrms[2];
  char hname[80];
  printf("start\n");
  for(int ich=0; ich<2; ich++)
  {
    printf("ich = %d\n",ich);
    tg[ich] = new TGraph();
    tg[ich]->SetMarkerStyle(20);
    tg[ich]->SetMarkerSize(0.5);
    sprintf(hname,"mean_ch%d",ich);
    hmean[ich]=new TH1D(hname,hname,100,150.,250.);
    sprintf(hname,"rms_ch%d",ich);
    hrms[ich]=new TH1D(hname,hname,200,0.,2.);
    printf("ich = %d\n",ich);
    fflush(stdout);
  }
  tg[0]->SetLineColor(kRed);
  tg[1]->SetLineColor(kBlue);
  tg[0]->SetMarkerColor(kRed);
  tg[1]->SetMarkerColor(kBlue);
  int dac_val=0;
  if(calib_level>0)dac_val=calib_level;

  ConnectionManager manager ( "file://xml/FEAD/connection_file.xml" );
  uhal::HwInterface hw=manager.getDevice( "fead.udp.0" );

  unsigned int ireg,device_number,val;
  ValWord<uint32_t> free_mem, trig_reg, delays, reg, current, fault;

// Read FW version to check :
  reg = hw.getNode("FW_VER").read();
  hw.getNode("FEAD_CTRL").write( RESET*1);
  hw.getNode("FEAD_CTRL").write( RESET*0);
  hw.dispatch();
  printf("Reset board with firmware version      : %8.8x\n",reg.value());

// Initialize current monitors
  for(int iLVR=0; iLVR<2; iLVR++)
  {
    printf("\nProgram LV sensor %d : ",iLVR);
    unsigned int device_number;
    if(iLVR==0)
    {
      device_number=0x67;
      printf("V2P5\n");
    }
    else
    {
      device_number=0x6C;
      printf("V1P2\n");
    }
// Put LVRB in continuous ADC mode, monitor sense+, and power calculation with sense+
    int ireg=0; // control register
    command=0x5;
    val=I2C_RW(hw, device_number, ireg, command,0, 3, debug);
    printf("New content of control register : 0x%x\n",val&0xffff);
// Max power limit
    val=I2C_RW(hw, device_number, 0xe,  0xff,0, 1, debug);
    val=I2C_RW(hw, device_number, 0xf,  0xff,0, 1, debug);
    val=I2C_RW(hw, device_number, 0x10, 0xff,0, 1, debug);
// Min power limit
    val=I2C_RW(hw, device_number, 0x11, 0x00,0, 1, debug);
    val=I2C_RW(hw, device_number, 0x12, 0x00,0, 1, debug);
    val=I2C_RW(hw, device_number, 0x13, 0x00,0, 1, debug);
// Max ADin limit
    val=I2C_RW(hw, device_number, 0x2E, 0xFFF0,1, 3, debug);
// Min ADin limit
    val=I2C_RW(hw, device_number, 0x30, 0x0000,1, 3, debug);
// Max Vin limit
    val=I2C_RW(hw, device_number, 0x24, 0xFFF0,1, 3, debug);
// Min Vin limit
    val=I2C_RW(hw, device_number, 0x26, 0x0000,1, 3, debug);
// Max current limit
    val=I2C_RW(hw, device_number, 0x1A, 0xFFF0,1, 3, debug);
    printf("New max threshold on current 0x%x\n",val&0xffff);
// Min current limit
    val=I2C_RW(hw, device_number, 0x1C, 0x0000,1, 3, debug);
    printf("New min threshold on current 0x%x\n",val&0xffff);
// Read and clear Fault register :
    ireg=4; // Alert register
    val=I2C_RW(hw, device_number, ireg, command,0, 2, debug);
    printf("Alert register content : 0x%x\n",val&0xffff);
    val=I2C_RW(hw, device_number, ireg, command,0, 2, debug);
    printf("Alert register content : 0x%x\n",val&0xffff);
// Set Alert register
    ireg=1;
    command=0x20; // Alert on Dsense overflow
    val=I2C_RW(hw, device_number, ireg, command,0, 3, debug);
    printf("New alert bit pattern : 0x%x\n", val&0xffff);
  }
  printf("\n");
  printf("Redo pwup_reset and Start LVRB auto scan mode\n");
  hw.getNode("VFE_CTRL").write(PWUP_RESET*1);
  hw.getNode("VFE_CTRL").write(0);
  hw.getNode("VFE_CTRL").write(LVRB_AUTOSCAN*1);
  hw.dispatch();

// Wait for voltage stabilization
  usleep(2000000);

// Read voltages measured by controler
  for(int iLVR=0; iLVR<2; iLVR++)
  {
    char alim[80];
    if(iLVR==0)
    {
      device_number=0x67;
      sprintf(alim,"V2P5");
    }
    else
    {
      device_number=0x6C;
      sprintf(alim,"V1P2");
    }
    val=I2C_RW(hw, device_number, 0x1E, 0,1, 2, 0);
    double Vmeas=((val>>4)&0xfff);
    Vmeas=Vmeas/4096.*102.4; // Convert to V (102.4 V full scale)
    printf("%s measured power supply voltage %7.3f V\n",alim,Vmeas);
    val=I2C_RW(hw, device_number, 0x28, 0,1, 2, 0);
    Vmeas=((val>>4)&0xfff);
    Vmeas=Vmeas/4096.*2.048; // Convert to V (2.048 V full scale)
    if(iLVR==0)Vmeas*=2.;
    printf("%s measured sense voltage %7.3f V\n",alim,Vmeas);
  }

// Setup ADCs
  printf("Program_ADCs...\n");
// Put ADC in two's complement mode (if no pedestal bias) and invert de conversion result
  negate_data=(negate_data&1)<<2;
  signed_data&=3;
  command=ADC_WRITE | ADC_OMODE_REG | negate_data | signed_data;
  printf("Put ADC coding : 0x%x\n",command);
  hw.getNode("DTU_CTRL").write(command);
  hw.dispatch();

// Set ADC input range (default=1.75V)
  input_span&=0x1F;
  command=ADC_WRITE | ADC_ISPAN_REG | input_span;
  printf("Set ADC input span range : 0x%x\n",command);
  hw.getNode("DTU_CTRL").write(command);
  hw.dispatch();

// Set ADC output test mode
  output_test&=0x0F;
  command=ADC_WRITE | ADC_OTEST_REG | output_test;
  printf("Set ADC output test mode : 0x%x\n",command);
  hw.getNode("DTU_CTRL").write(command);
  hw.dispatch();

// Set delay tap value for each ADC
  for(int i=0; i<5; i++)
  {
    command=DELAY_RESET*1;
    hw.getNode("DELAY_CTRL").write(command);
    hw.dispatch();
    usleep(1000);
  }
  reg = hw.getNode("FEAD_CTRL").read();
  hw.dispatch();
  printf("FEAD_CTRL register content : 0x%8.8x\n",reg.value());
  printf("Get lock status of IDELAY input stages\n");
  reg = hw.getNode("DELAY_CTRL").read();
  hw.dispatch();
  printf("Delay lock value : 0x%x\n",reg.value());

  for(int iADC=0; iADC<2; iADC++)
  {
    command=DELAY_RESET*0 | DELAY_INCREASE*1 | (0x7F<<(iADC*7));
    for(Int_t itap=0; itap<delay_val[iADC]; itap++)
    {
      hw.getNode("DELAY_CTRL").write(command);
      hw.dispatch();
    }
    printf("Get lock status of IDELAY input stages\n");
    reg = hw.getNode("DELAY_CTRL").read();
    hw.dispatch();
    printf("Value read : 0x%x\n",reg.value());
  }
// Set the clock phases
  command = MEM_CLOCK_PHASE*mem_clock_phase+REG_CLOCK_PHASE*reg_clock_phase+IO_CLOCK_PHASE*IO_clock_phase+SEQ_CLOCK_PHASE*seq_clock_phase;
  printf("Set clock phases : 0x%x\n",command);
  hw.getNode("CLK_SETTING").write(command);
// Add laser latency before catching data ~ 40 us
  hw.getNode("TRIG_DELAY").write((SW_DAQ_DELAY<<16)+HW_DAQ_DELAY);
  hw.dispatch();

// Read Temperature and others analog voltages with FPGA ADC
// Read XADC register 0x40 and set the requested average to 1 in XADC
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  ValWord<uint32_t> ave  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  unsigned loc_ave=ave.value()&0xffff;
  printf("Old config register 0x40 content : %x\n",loc_ave);

  loc_ave=0x8000;
  command=DRP_WRb*1 | (0x40<<16) | loc_ave;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  ave  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  loc_ave=ave.value()&0xffff;
  printf("New config register 0x40 content : %x\n",loc_ave);
  command=DRP_WRb*1 | (0x42<<16) | 0x0400;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();
// Switch off temp output on CATIA :
  device_number=1000 + (CATIA_num<<3);
  val=I2C_RW(hw, device_number, 1, 0x2,0, 3, debug);

  Int_t reg_def[7]={0x0,0x2,0x0,0x1087,0xFFFF,0xFFFF,0xF};
  Int_t reg_error[7]={0};
  if(test_I2C>0)
  {
    for(Int_t iloop=0; iloop<test_I2C; iloop++)
    {
      printf("Test_I2C, loop %d : ",iloop);
      for(Int_t loc_reg=1; loc_reg<=6; loc_reg++)
      {
        Int_t I2C_long=0;
        if(loc_reg==3 || loc_reg==4 || loc_reg==5) I2C_long=1;
        device_number=1000 + (CATIA_num<<3);
        if(loc_reg==4 || loc_reg==5)
        {
          for(Int_t loc_dac=0; loc_dac<4096; loc_dac++)
          {
            val=I2C_RW(hw, device_number, loc_reg, (0xF000 | loc_dac),I2C_long, 3, debug);
            val&=0xFFFF;
            if(val!=(0xF000 | loc_dac))reg_error[loc_reg]++;
            //printf("register %d, value put 0x%4.4x, read 0x%4.4x\n",loc_reg, loc_dac,val);
          }
        }
        else
        {
          val=I2C_RW(hw, device_number, loc_reg, 0,I2C_long, 2, debug);
          val&=0xFFFF;
          if(val!=reg_def[loc_reg])reg_error[loc_reg]++;
        }
        //printf("register %d, value read 0x%4.4x\n",loc_reg, val);
      }
      for(Int_t loc_reg=1; loc_reg<=6; loc_reg++) printf("%d ",reg_error[loc_reg]);
      printf("errors\n");
    }
    for(Int_t loc_reg=1; loc_reg<=6; loc_reg++)
    {
      if(loc_reg==4 || loc_reg==5)
        printf("reg %d : %d error/%d tries\n",loc_reg,reg_error[loc_reg],test_I2C*4096);
      else
        printf("reg %d : %d error/%d tries\n",loc_reg,reg_error[loc_reg],test_I2C);
    }
  }
//===========================================================================================================
// 0 : Get Vdac vs DAC transfer function for current injection 
// Now, make the temp measurement on all catias, sequentially end for both current gain
  TGraph *tg_Vref=new TGraph();
  tg_Vref->SetLineColor(kRed);
  tg_Vref->SetMarkerColor(kRed);
  tg_Vref->SetMarkerStyle(20);
  tg_Vref->SetMarkerSize(1.);
  tg_Vref->SetName("Vref_vs_reg");
  tg_Vref->SetTitle("Vref_vs_reg");
  TGraph *tg_Vdac[2], *tg_Vdac_resi[2];
  tg_Vdac[0] =new TGraph();
  tg_Vdac[0]->SetLineColor(kRed);
  tg_Vdac[0]->SetMarkerColor(kRed);
  tg_Vdac[0]->SetMarkerStyle(20);
  tg_Vdac[0]->SetMarkerSize(1.);
  tg_Vdac[0]->SetName("Vdac1_vs_reg");
  tg_Vdac[0]->SetTitle("Vdac1_vs_reg");
  tg_Vdac[1] =new TGraph();
  tg_Vdac[1]->SetLineColor(kBlue);
  tg_Vdac[1]->SetMarkerColor(kBlue);
  tg_Vdac[1]->SetMarkerStyle(20);
  tg_Vdac[1]->SetMarkerSize(1.);
  tg_Vdac[1]->SetName("Vdac2_vs_reg");
  tg_Vdac[1]->SetTitle("Vdac2_vs_reg");
  tg_Vdac_resi[0] =new TGraph();
  tg_Vdac_resi[0]->SetLineColor(kRed);
  tg_Vdac_resi[0]->SetMarkerColor(kRed);
  tg_Vdac_resi[0]->SetMarkerStyle(20);
  tg_Vdac_resi[0]->SetMarkerSize(1.);
  tg_Vdac_resi[0]->SetName("Vdac1_resi_vs_reg");
  tg_Vdac_resi[0]->SetTitle("Vdac1_resi_vs_reg");
  tg_Vdac_resi[1] =new TGraph();
  tg_Vdac_resi[1]->SetLineColor(kBlue);
  tg_Vdac_resi[1]->SetMarkerColor(kBlue);
  tg_Vdac_resi[1]->SetMarkerStyle(20);
  tg_Vdac_resi[1]->SetMarkerSize(1.);
  tg_Vdac_resi[1]->SetName("Vdac2_resi_vs_reg");
  tg_Vdac_resi[1]->SetTitle("Vdac2_resi_vs_reg");
  TGraph *tg_Vbuf[2], *tg_Vbuf_resi[2];
  tg_Vbuf[0] =new TGraph();
  tg_Vbuf[0]->SetLineColor(kRed);
  tg_Vbuf[0]->SetMarkerColor(kRed);
  tg_Vbuf[0]->SetMarkerStyle(20);
  tg_Vbuf[0]->SetMarkerSize(1.);
  tg_Vbuf[0]->SetName("Vbuf1_vs_reg");
  tg_Vbuf[0]->SetTitle("Vbuf1_vs_reg");
  tg_Vbuf[1] =new TGraph();
  tg_Vbuf[1]->SetLineColor(kBlue);
  tg_Vbuf[1]->SetMarkerColor(kBlue);
  tg_Vbuf[1]->SetMarkerStyle(20);
  tg_Vbuf[1]->SetMarkerSize(1.);
  tg_Vbuf[1]->SetName("Vbuf2_vs_reg");
  tg_Vbuf[1]->SetTitle("Vbuf2_vs_reg");
  tg_Vbuf_resi[0] =new TGraph();
  tg_Vbuf_resi[0]->SetLineColor(kRed);
  tg_Vbuf_resi[0]->SetMarkerColor(kRed);
  tg_Vbuf_resi[0]->SetMarkerStyle(20);
  tg_Vbuf_resi[0]->SetMarkerSize(1.);
  tg_Vbuf_resi[0]->SetName("Vbuf1_resi_vs_reg");
  tg_Vbuf_resi[0]->SetTitle("Vbuf1_resi_vs_reg");
  tg_Vbuf_resi[1] =new TGraph();
  tg_Vbuf_resi[1]->SetLineColor(kBlue);
  tg_Vbuf_resi[1]->SetMarkerColor(kBlue);
  tg_Vbuf_resi[1]->SetMarkerStyle(20);
  tg_Vbuf_resi[1]->SetMarkerSize(1.);
  tg_Vbuf_resi[1]->SetName("Vbuf2_resi_vs_reg");
  tg_Vbuf_resi[1]->SetTitle("Vbuf2_resi_vs_reg");
  Double_t DAC_val[33], Vdac[2][33], Vbuf[2][33], Vdac_offset[2], Vdac_slope[2], Vbuf_offset[2], Vbuf_slope[2], Vref[15], Temp[3];

// Stop LVRB auto scan, not to generate noise in CATIA during calibration
  hw.getNode("VFE_CTRL").write(0);
  hw.dispatch();

//===========================================================================================================
// 3 : Send internal TP triggers for linearity measurements : G10 and G1
// calibration trigger setting :
  trigger_type=1;
  nevent=10;
  nsample=300;
  n_calib=128;
  calib_step=32;
  Double_t dt=6.25;
  Double_t V_Iinj_slope[2], INL_min[2]={9999.,9999.}, INL_max[2]={-9999.,-9999.};
  TProfile *tp_inj1[2][n_calib];
  for(int iinj=0; iinj<n_calib; iinj++)
  {
    sprintf(hname,"pulse_G1_DAC_%4.4d",calib_level+iinj*calib_step);
    tp_inj1[0][iinj]=new TProfile(hname,hname,nsample,0.,dt*nsample);
    sprintf(hname,"pulse_G10_DAC_%4.4d",calib_level+iinj*calib_step);
    tp_inj1[1][iinj]=new TProfile(hname,hname,nsample,0.,dt*nsample);
  }
  TGraph *tg_inj1[2], *tg_inj2[2], *tg_inj3[2];
  tg_inj1[0]=new TGraph();
  tg_inj1[0]->SetTitle("Inj_vs_iDAC_G1");
  tg_inj1[0]->SetName("Inj_vs_iDAC_G1");
  tg_inj1[0]->SetMarkerStyle(20);
  tg_inj1[0]->SetMarkerSize(1.0);
  tg_inj1[0]->SetMarkerColor(kRed);
  tg_inj1[0]->SetLineColor(kRed);
  tg_inj1[0]->SetLineWidth(2);
  tg_inj1[1]=new TGraph();
  tg_inj1[1]->SetTitle("Inj_vs_iDAC_G10");
  tg_inj1[1]->SetName("Inj_vs_iDAC_G10");
  tg_inj1[1]->SetMarkerStyle(20);
  tg_inj1[1]->SetMarkerSize(1.0);
  tg_inj1[1]->SetMarkerColor(kBlue);
  tg_inj1[1]->SetLineColor(kBlue);
  tg_inj1[1]->SetLineWidth(2);
  tg_inj2[0]=new TGraph();
  tg_inj2[0]->SetTitle("Inj_vs_vDAC_G1");
  tg_inj2[0]->SetName("Inj_vs_vDAC_G1");
  tg_inj2[0]->SetMarkerStyle(20);
  tg_inj2[0]->SetMarkerSize(1.0);
  tg_inj2[0]->SetMarkerColor(kRed);
  tg_inj2[0]->SetLineColor(kRed);
  tg_inj2[0]->SetLineWidth(2);
  tg_inj2[1]=new TGraph();
  tg_inj2[1]->SetTitle("Inj_vs_vDAC_G10");
  tg_inj2[1]->SetName("Inj_vs_vDAC_G10");
  tg_inj2[1]->SetMarkerStyle(20);
  tg_inj2[1]->SetMarkerSize(1.0);
  tg_inj2[1]->SetMarkerColor(kBlue);
  tg_inj2[1]->SetLineColor(kBlue);
  tg_inj2[1]->SetLineWidth(2);
  tg_inj3[0]=new TGraph();
  tg_inj3[0]->SetTitle("INL_vs_amplitude_G1");
  tg_inj3[0]->SetName("INL_vs_amplitude_G1");
  tg_inj3[0]->SetMarkerStyle(20);
  tg_inj3[0]->SetMarkerSize(1.0);
  tg_inj3[0]->SetMarkerColor(kRed);
  tg_inj3[0]->SetLineColor(kRed);
  tg_inj3[0]->SetLineWidth(2);
  tg_inj3[0]->SetMaximum(0.003);
  tg_inj3[0]->SetMinimum(-.003);
  tg_inj3[1]=new TGraph();
  tg_inj3[1]->SetTitle("INL_vs_amplitude_G10");
  tg_inj3[1]->SetName("INL_vs_amplitude_G10");
  tg_inj3[1]->SetMarkerStyle(20);
  tg_inj3[1]->SetMarkerSize(1.0);
  tg_inj3[1]->SetMarkerColor(kBlue);
  tg_inj3[1]->SetLineColor(kBlue);
  tg_inj3[1]->SetLineWidth(2);
  tg_inj3[1]->SetMaximum(0.003);
  tg_inj3[1]->SetMinimum(-.003);

  command=(calib_delay<<16) | (calib_width&0xffff);
  printf("Calibration trigger with %d clocks width and %d clocks delay : %x\n",calib_width,calib_delay,command);
  hw.getNode("CALIB_CTRL").write(command);
// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
// Start DAQ :
  command = ((nsample+2)<<16)+CAPTURE_START;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();

  val=I2C_RW(hw, device_number, 1, 2,0, 3, debug); // Switch off temp sensor
  val=I2C_RW(hw, device_number, 3, (0x10<<8) | (0x10<<3) | (1<<2) | (1<<1) | 0,1, 3, debug);
  //printf("Reg3 register value read : 0x%x, %d\n",val,val&0xFFF);
  val=I2C_RW(hw, device_number, 5, 0,1, 3, debug); // Switch off every thing 
  //printf("Reg5 register value read : 0x%x, %d\n",val,val&0xFFF);
  usleep(100000);
  for(int igain=0; igain<2; igain++)
  {
    calib_level=0;
    val=I2C_RW(hw, device_number, 6, 0x0b | ((1-igain)<<2),0, 3, debug); // Rconv 2471 or 272, TIA_dummy ON, TP ON, Vreg O
    //printf("Reg6 register value read : 0x%x, %d\n",val,val&0xFFF);
    Double_t fit_min=-1, fit_max=-1;
    for(int istep=0; istep<n_calib; istep++)
    {
// Program DAC for this step
      val=I2C_RW(hw, device_number, 4, 0x1000|calib_level,1, 3, debug); // TP with DAC1
      if((val&0xFFF)!=calib_level) printf("Error on DAC setting : 0x%4.4x written, 0x%4.4x read back\n",calib_level,val&0xFFF);
      //printf("DAC register value read : 0x%x, %d\n",val,val&0xFFF);
// Wait for Vdac to stabilize :
      usleep(10000);

      int ievt=0;
      draw=1;
      for(Int_t ievt=0; ievt<nevent; ievt++)
      {
        timestamp=get_event(hw,trigger_type,nsample,mem,event,fevent,debug,draw);
        if(debug==0)
        {
          system("stty raw");
          char cdum=getchar();
          system("stty -raw");
          if(cdum=='c')draw=0;
        }
        else 
          draw=0;
        if(timestamp<0) printf("DAQ error on event %d : %d\n",ievt, timestamp);

        for(int is=0; is<nsample; is++)
        {
          tp_inj1[igain][istep]->Fill(dt*(is+0.5),fevent[igain][is]*dv);
        }

      }
      tp_inj1[igain][istep]->Fit("pol0","wq","",0.e-9,450.e-9);
      f1=tp_inj1[igain][istep]->GetFunction("pol0");
      mean[igain]=f1->GetParameter(0);
      tp_inj1[igain][istep]->Fit("pol1","wq","",800.e-9,1600.e-9);
      f1=tp_inj1[igain][istep]->GetFunction("pol1");
      Double_t val0=f1->GetParameter(0)+f1->GetParameter(1)*500.e-9-mean[igain];
      tg_inj1[igain]->SetPoint(istep,calib_level,val0);
      tg_inj2[igain]->SetPoint(istep,fDAC[0]->Eval(calib_level)*1000.,val0);
      if(fit_min<0 && val0>0.)fit_min=calib_level-calib_step/2.;
      if(val0<1200.)fit_max=calib_level+calib_step/2.;
      calib_level+=calib_step;
    }
  }
// Stop DAQ
  command = ((nsample+2)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();

//===========================================================================================================
// 4 : Send G10 and G1 calibration pulses with R=500 and R=400
// Enabe CALIB_PULSE  :
  Double_t Rcalib[2]={1000.,10000.}; // Conversion resistor on PCB to compute CATIA gain
  Double_t Rconv[2];
  Double_t CATIA_gain[2][2], TIA_gain[2], gain_stage;
  fead_ctrl=  ADC_CALIB_MODE        *0 |
              SEL_CALIB_PULSE       *1 |
              SELF_TRIGGER_MASK     *0 |
              SELF_TRIGGER_THRESHOLD*0 |
              SELF_TRIGGER          *0 |
              SOFT_TRIGGER          *1 |
              TRIGGER_MODE          *1 | // Always DAQ on trigger
              RESET                 *0;
  hw.getNode("FEAD_CTRL").write(fead_ctrl);
  nsample=500;
  nevent=100;
  command=(calib_delay<<16) | (calib_width&0xffff);
  printf("Calibration trigger with %d clocks width and %d clocks delay : %x\n",calib_width,calib_delay,command);
  hw.getNode("CALIB_CTRL").write(command);
// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
// Start DAQ :
  command = ((nsample+2)<<16)+CAPTURE_START;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();

// Stop DAQ :
  command = ((nsample+2)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
// Switch on FE-adapter LEDs
  command = LED_ON*1+GENE_100HZ*0+GENE_TRIGGER*0;
  hw.getNode("FW_VER").write(command);
  hw.dispatch();
// Restart LVRB auto scan
  printf("Restart LVRB auto scan mode\n");
  hw.getNode("VFE_CTRL").write(LVRB_AUTOSCAN*1);
  hw.dispatch();
// Switch off CATIA LV to change chip :
// First ask to generate alert with overcurrent
  val=I2C_RW(hw,0x67,0x01,0x20,0,1,debug);
  val=I2C_RW(hw,0x6C,0x01,0x20,0,1,debug);
// Now, generate a current overflow :
  val=I2C_RW(hw,0x67,0x03,0x20,0,1,debug);
  val=I2C_RW(hw,0x6C,0x03,0x20,0,1,debug);
  usleep(1000000);
// Read current values for control :
  val=I2C_RW(hw, 0x67, 0x14, 0,1, 2, debug); // Read Dsense value
  printf("CATIA V1P2 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V1P2);
  val=I2C_RW(hw, 0x6C, 0x14, 0,1, 2, debug); // Read Dsense value
  printf("CATIA V2P5 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V2P5);

}

