#include "uhal/uhal.hpp"

#include <vector>
#include <iostream>
#include <cstdlib>
#include <typeinfo>

#include "I2C_RW.h"
#include <TApplication.h>
#include <TProfile.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TProfile.h>
#include <TVirtualFFT.h>
#include <TF1.h>
#include <TH1D.h>
#include <TTree.h>
#include <TFile.h>

#define VERSION                1
#define GENE_TRIGGER           (1<<0)
#define TP_TRIGGER             (1<<1)
#define GENE_100HZ             (1<<2)
#define LED_ON                 (1<<3)
#define G1_CALIB_TRIGGER       (1<<4)
#define G10_CALIB_TRIGGER      (1<<5)

#define PWUP_RESET             (1<<31)
#define RESET                  (1<<0)
#define TRIGGER_MODE           (1<<1)
#define SOFT_TRIGGER           (1<<2)
#define SELF_TRIGGER           (1<<3)
#define CLOCK_PHASE            (1<<4)
#define CLOCK_RESET            (1<<7)
#define SELF_TRIGGER_THRESHOLD (1<<8)
#define SELF_TRIGGER_MASK      (1<<22)
#define SEL_CALIB_PULSE        (1<<23)
#define ADC_CALIB_MODE         (1<<27)
#define BOARD_SN               (1<<28)
#define SEQ_CLOCK_PHASE        (1<<0)
#define IO_CLOCK_PHASE         (1<<4)
#define REG_CLOCK_PHASE        (1<<8)
#define MEM_CLOCK_PHASE        (1<<12)
#define LVRB_AUTOSCAN          (1<<2)

#define CAPTURE_START 1
#define CAPTURE_STOP  2
#define SW_DAQ_DELAY 0x0 // delay for laser with external trigger
#define HW_DAQ_DELAY 100 // Laser with external trigger
#define NSAMPLE_MAX 65536
#define MAX_PAYLOAD 1380
#define MAX_VFE     10
#define MAX_STEP    10000

#define I2C_RWb         (1<<31)
#define I2C_LONG        (1<<30)
#define SPI_CATIA       (1<<26)
#define I2C_CATIA       (1<<25)
#define ADC_WRITE       (1<<24)
#define DAC_FULL_RESET  (0xF<<16)
#define DAC_SOFT_RESET  (0x7<<16)
#define DAC_VAL_REG     (0x3<<16)
#define DAC_CTRL_REG    (0x4<<16)

#define ADC_OMODE_REG   (0x14<<16) // ADC register to define Output mode of the ADC
#define ADC_ISPAN_REG   (0x18<<16) // ADC register to define input span of the ADC from 1.383V to 2.087V
#define ADC_CLK_DIVIDE  (0x0B<<16) // ADC register to define clock division of the ADC
#define ADC_OTEST_REG   (0x0D<<16) // ADC register to define test mode of the ADC

#define DELAY_INCREASE  (1<<5)     // Increase ADC signal delay by one tip
#define DELAY_RESET     (1<<6)     // Reset ADC signal delays

#define DRP_WRb         (1<<31)    // Acces to FGPA internal ADCs

using namespace uhal;

int get_event(uhal::HwInterface hw, int trigger_type, int nsample, ValVector< uint32_t >mem, short int *event[2], double *fevent[2], int debug, int draw)
{
  static int old_address=-1, ievt=0;
  static TGraph *tg[2]={NULL, NULL};
  static TCanvas *cdebug=NULL;
  static int trigger_type_old=-1;
  int n_word;
  int n_transfer_max=189;
  int n_transfer;
  int n_last;
  int max_address= 65536;
  int error      = 0;
  int command    = 0;

  if(trigger_type_old!=trigger_type)ievt=0;
  if(cdebug==NULL)
  {
    tg[0]=new TGraph();
    tg[1]=new TGraph();
    cdebug=new TCanvas();
    cdebug->Divide(1,2);
  }

  ValWord<uint32_t> address,free_mem;
  address = hw.getNode("CAP_ADDRESS").read();
  hw.dispatch();
  if(debug>0)printf("Starting with address : %8.8x, trigger_type : %d\n",address.value(),trigger_type);
  old_address=address.value()>>16;
  if(old_address==max_address-1)old_address=-1;

  ValVector< uint32_t > block;
  if(trigger_type !=2)
  {
    if(trigger_type == 0)
      command = G10_CALIB_TRIGGER*0 | G1_CALIB_TRIGGER*0 | LED_ON*0 | GENE_100HZ*0 | TP_TRIGGER*0 | GENE_TRIGGER*1;
    else if(trigger_type == 1)
      command = G10_CALIB_TRIGGER*0 | G1_CALIB_TRIGGER*0 | LED_ON*0 | GENE_100HZ*0 | TP_TRIGGER*1 | GENE_TRIGGER*0;
    else if(trigger_type == 3)
      command = G10_CALIB_TRIGGER*0 | G1_CALIB_TRIGGER*1 | LED_ON*0 | GENE_100HZ*0 | TP_TRIGGER*0 | GENE_TRIGGER*1;
    else if(trigger_type == 4)
      command = G10_CALIB_TRIGGER*1 | G1_CALIB_TRIGGER*0 | LED_ON*0 | GENE_100HZ*0 | TP_TRIGGER*0 | GENE_TRIGGER*1;

    if(debug>0)printf("Send trigger with command : 0x%8.8x\n",command);
// Read base address and send trigger
    free_mem = hw.getNode("CAP_FREE").read();
    hw.getNode("FW_VER").write(command);
    hw.dispatch();
    if(debug>0)printf("Free memory : 0x%8.8x\n",free_mem.value());
// Read new address and wait for DAQ completion
    address = hw.getNode("CAP_ADDRESS").read();
    hw.dispatch();
    if(debug>0)printf("Start address : 0x%8.8x\n",address.value());
    int new_address=address.value()>>16;
    int loc_address=new_address;
    int nretry=0;
    while(((loc_address-old_address)%max_address) != nsample+2 && nretry<100)
    {
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      new_address=address.value()>>16;
      loc_address=new_address;
      if(new_address<old_address)loc_address+=max_address;
      nretry++;
      if(debug>0)printf("ongoing R/W addresses    : old %d, new %d add 0x%8.8x\n", old_address, new_address,address.value());
    }
    if(nretry==100)
    {
      printf("Stop waiting for sample capture after %d retries\n",nretry);
      printf("R/W addresses    : old %8.8x, new %8.8x add 0x%8.8x\n", old_address, new_address, address.value());
      error=1;
    }
    old_address=new_address;
    if(old_address==max_address-1)old_address=-1;
    if(debug>0)printf("R/W addresses    : old %8.8x, new %8.8x add 0x%8.8x\n", old_address, new_address, address.value());
  }
  else
  {
    int nretry=0;
// Wait for external trigger to fill memory :
    while((free_mem.value()==max_address-1) && nretry<100)
    {
      free_mem = hw.getNode("CAP_FREE").read();
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      //printf("address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
      if(debug>0)printf("address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
      usleep(100);
      nretry++;
    }
    if(nretry==100)
    {
      printf("Stop waiting for external trigger after %d retries\n",nretry);
      printf("R/W addresses    : add 0x%8.8x\n",address.value());
      error=1;
    }
  }
  mem.clear();

// Read event samples from FPGA
  n_word=nsample+2;     // 1*32 bits words per sample to get the 2 channels data (G10 and G1)
  n_transfer=n_word/(MAX_PAYLOAD/4); // Max ethernet packet = 1536 bytes, max user payload = 1380 bytes
  n_last=n_word-n_transfer*(MAX_PAYLOAD/4);
  if(n_transfer>n_transfer_max)
  {
    printf("Error : ask for too many samples. Number of transfer %d greater than allowed max value %d\n",n_transfer, n_transfer_max);
    error=1;
  }
  for(int itrans=0; itrans<n_transfer; itrans++)
  {
    block = hw.getNode ("CAP_DATA").readBlock(MAX_PAYLOAD/4);
    hw.dispatch();
    for(int is=0; is<MAX_PAYLOAD/4; is++)mem.push_back(block[is]);
  }
  block = hw.getNode ("CAP_DATA").readBlock(n_last);
  address = hw.getNode("CAP_ADDRESS").read();
  free_mem = hw.getNode("CAP_FREE").read();
  hw.dispatch();
  if(debug>0)printf("After reading address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
  for(int is=0; is<n_last; is++)mem.push_back(block[is]);
  mem.valid(true);

  if(debug>0)
  {
    printf("%8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",
        mem[0],mem[1],mem[2],mem[3],mem[4],mem[5],mem[6],mem[7],mem[8],mem[9]);
    printf("%8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",
        mem[10],mem[11],mem[12],mem[13],mem[14],mem[15],mem[16],mem[17],mem[18],mem[19]);
    printf("%8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",
        mem[20],mem[21],mem[22],mem[23],mem[24],mem[25],mem[26],mem[27],mem[28],mem[29]);
    printf("%.4d %.4d %.4d %.4d %.4d %.4d %.4d %.4d\n",
        mem[2]&0x3fff,mem[3]&0x3fff,mem[4]&0x3fff,mem[5]&0x3fff,mem[6]&0x3fff,mem[7]&0x3fff,mem[8]&0x3fff,mem[9]&0x3fff);
    printf("%.4d %.4d %.4d %.4d %.4d %.4d %.4d %.4d\n",
        (mem[2]>>14)&0x3ff,(mem[3]>>14)&0x3fff,(mem[4]>>14)&0x3fff,(mem[5]>>14)&0x3fff,
        (mem[6]>>14)&0x3ff,(mem[7]>>14)&0x3fff,(mem[8]>>14)&0x3fff,(mem[9]>>14)&0x3fff);
  }
// First sample should have bit 28 downto 0 at 1
  if(mem[0] != 0x0FFFFFFF)
  {
    printf("Sample 0 not a header : %8.8x\n",mem[0]);
    error=1;
  }

  int timestamp= mem[1];
  if(debug>0) printf("timestamp : %8.8x\n",mem[1]);

  for(int isample=2; isample<nsample+2; isample++)
  {
    event[0][isample-2]= mem[isample]       &0x3FFF;
    event[1][isample-2]= (mem[isample]>>14) &0x3FFF;
    fevent[0][isample-2]=double(event[0][isample-2]);
    fevent[1][isample-2]=double(event[1][isample-2]);
  }

  if(debug>0 || trigger_type !=trigger_type_old || draw==1)
  {
    tg[0]->Clear();
    tg[1]->Clear();
    tg[0]->Set(0);
    tg[1]->Set(0);
    for(int isample=0; isample<nsample; isample++)
    {
      tg[0]->SetPoint(isample,6.25*isample,fevent[0][isample]);
      tg[1]->SetPoint(isample,6.25*isample,fevent[1][isample]);
    }
    cdebug->cd(1);
    tg[0]->SetLineColor(kRed);
    tg[0]->Draw("alp");
    cdebug->cd(2);
    tg[1]->SetLineColor(kBlue);
    tg[1]->Draw("alp");
    cdebug->Update();
  }
  if(debug>0)
  {
    system("stty raw");
    char cdum=getchar();
    system("stty -raw");
    if(cdum=='q')exit(-1);
  }

  trigger_type_old=trigger_type;
  ievt++;
  if(error==0)
    return timestamp;
  else
    return -1;
}

Int_t main ( Int_t argc,char* argv[] )
{
  Double_t NSPS=160.e6;
  Int_t timestamp;
  Int_t soft_reset, full_reset, command, fead_ctrl;
  ValWord<uint32_t> address;
  ValWord<uint32_t> data[3*NSAMPLE_MAX];
  ValVector< uint32_t > mem;
  short int *event[2];
  event[0]=(short int*)malloc(NSAMPLE_MAX*sizeof(short int));
  event[1]=(short int*)malloc(NSAMPLE_MAX*sizeof(short int));
  double *fevent[2];
  fevent[0]=(double*)malloc(NSAMPLE_MAX*sizeof(double));
  fevent[1]=(double*)malloc(NSAMPLE_MAX*sizeof(double));
  double dv=1750./16384.; // 14 bits on 1.75V
  Int_t debug=-1, draw=0;
  double ped[2], mean[2], rms[2];
  Double_t gain[2]={1.,10.};
  Double_t RTIA[2]={500.,400.};
  Double_t Rshunt_V1P2=0.51;
  Double_t Rshunt_V2P5=0.10;
  char cdum, output_file[256];
// Define defaults for laser runs :
  Int_t nevent                = 0;
  Int_t nsample               =-1;
  Int_t trigger_type          = 0; // pedestal by default
  Int_t self_trigger          = 0; // No self trigger 
  Int_t self_trigger_threshold= 0;
  Int_t self_trigger_mask     = 0; // don't trig on any channels amplitude
  Int_t ADC_calib_mode        = 0; // Put (1) or not (0) the outputs of CATIA in ADC_calib_mode (outputs near VCM)
  Int_t seq_clock_phase       = 0; // sequence clock phase by steps of 45 degrees
  Int_t IO_clock_phase        = 0; // Capture clock phase by steps of 45 degrees for CATIA test board
  Int_t reg_clock_phase       = 0; // Shift register clock phase by steps of 45 degrees
  Int_t mem_clock_phase       = 0; // memory clock phase by steps of 45 degrees
  Int_t sel_calib_pulse       = 1; // Default calib pulse enable. Otherwise High_Z
  Int_t n_calib               = 32;
  Int_t calib_gain            = 0;
  Int_t calib_step            = 128;
  Int_t calib_level           = 0;
  Int_t calib_width           = 200;
  Int_t calib_delay           = 50;
  Int_t tune_delay            = 1;
  Int_t delay_val[2]          = {0,0}; // Number of delay taps to get a stable R/O with ADCs (160 MHz)
  Int_t CATIA_number          = 9999;
  Int_t test_I2C              = 0;
  Int_t DAC_buf_off           = 0;

// ADC settings if any :
  UInt_t output_test = 0;  // Put ADC outputs in test mode (0=normal conversion values, 0xF=ramp)
  UInt_t input_span  = 0;
  UInt_t signed_data = 0;
  UInt_t negate_data = 0;

// CATIA settings if requested
  Int_t n_vfe          = 0;
  Int_t vfe            = 0;
  Int_t n_catia        = 1;
  Int_t CATIA_num      = 1; // CATIA number to address
  Int_t average        = 128;
  Int_t XADC_reg, XADC_Temp, XADC_Vdac, XADC_Vbuf, XADC_Vref;
  XADC_Temp=0x11;
  XADC_Vref=0x10;
  XADC_Vdac=0x1c;
  XADC_Vbuf=0x13; // Buffered DAC signal for CATIA-V1-rev3

#if VERSION == 1
  Int_t n_Vref=15, n_Vdac=33, n_PED_dac=32;
#else
  Int_t n_Vref=15, n_Vdac=33, n_PED_dac=64; // For V1.4
#endif

  TF1 *f1, *fDAC[2], *fBuf[2];
  TProfile *pshape[MAX_STEP];
  sprintf(output_file,"");

  for(int k=1; k<argc; k++) 
  {    
    if(strcmp( argv[k], "-debug") == 0)
    {    
      sscanf( argv[++k], "%d", &debug );
      continue;
    }    
    else if(strcmp(argv[k],"-num") == 0)
    {
      sscanf( argv[++k], "%d", &CATIA_number );
      continue;
    }
    else if(strcmp(argv[k],"-test_I2C") == 0)
    {
      sscanf( argv[++k], "%d", &test_I2C );
      continue;
    }
    else if(strcmp(argv[k],"-IO_clock") == 0)
    {
      sscanf( argv[++k], "%d", &IO_clock_phase );
      continue;
    }
    else if(strcmp(argv[k],"-h") == 0)
    {
      printf("Start DAQ with : \n");
      printf("-debug dbg_level            : Set debug level for this session [0]\n");
      printf("-num CATIA_number           : CATIA reference to identify the output file (rev*1000+i) [9999]\n");
      exit(-1);
    }
    else
    {
      printf("---------------------------------------------------\n");
      printf("Option %s not defined !, stop here.\n",argv[k]);
      printf("---------------------------------------------------\n");
      exit(-99);
    }
  }

  int loc_argc=1;
  char *loc_argv[10];
  for(int i=0; i<10; i++)loc_argv[i]=(char *)malloc(132*sizeof(char));
  sprintf(loc_argv[0],"test");
  TApplication *Root_App=new TApplication("test", &loc_argc, loc_argv);

  TGraph *tg[2];
  TH1D *hmean[2], *hrms[2];
  char hname[80];
  printf("start\n");
  for(int ich=0; ich<2; ich++)
  {
    printf("ich = %d\n",ich);
    tg[ich] = new TGraph();
    tg[ich]->SetMarkerStyle(20);
    tg[ich]->SetMarkerSize(0.5);
    sprintf(hname,"mean_ch%d",ich);
    hmean[ich]=new TH1D(hname,hname,100,150.,250.);
    sprintf(hname,"rms_ch%d",ich);
    hrms[ich]=new TH1D(hname,hname,200,0.,2.);
    printf("ich = %d\n",ich);
    fflush(stdout);
  }
  tg[0]->SetLineColor(kRed);
  tg[1]->SetLineColor(kBlue);
  tg[0]->SetMarkerColor(kRed);
  tg[1]->SetMarkerColor(kBlue);
  int dac_val=0;
  if(calib_level>0)dac_val=calib_level;

  ConnectionManager manager ( "file://xml/FEAD/connection_file.xml" );
  uhal::HwInterface hw=manager.getDevice( "fead.udp.0" );

  unsigned int ireg,device_number,val;
  ValWord<uint32_t> free_mem, trig_reg, delays, reg, current, fault;

// Read FW version to check :
  reg = hw.getNode("FW_VER").read();
  hw.getNode("FEAD_CTRL").write( RESET*1);
  hw.getNode("FEAD_CTRL").write( RESET*0);
  hw.dispatch();
  printf("Reset board with firmware version      : %8.8x\n",reg.value());

// Initialize current monitors
  for(int iLVR=0; iLVR<2; iLVR++)
  {
    printf("\nProgram LV sensor %d : ",iLVR);
    unsigned int device_number;
    if(iLVR==0)
    {
      device_number=0x67;
      printf("V2P5\n");
    }
    else
    {
      device_number=0x6C;
      printf("V1P2\n");
    }
// Put LVRB in continuous ADC mode, monitor sense+, and power calculation with sense+
    int ireg=0; // control register
    command=0x5;
    val=I2C_RW(hw, device_number, ireg, command,0, 3, debug);
    printf("New content of control register : 0x%x\n",val&0xffff);
// Max power limit
    val=I2C_RW(hw, device_number, 0xe,  0xff,0, 1, debug);
    val=I2C_RW(hw, device_number, 0xf,  0xff,0, 1, debug);
    val=I2C_RW(hw, device_number, 0x10, 0xff,0, 1, debug);
// Min power limit
    val=I2C_RW(hw, device_number, 0x11, 0x00,0, 1, debug);
    val=I2C_RW(hw, device_number, 0x12, 0x00,0, 1, debug);
    val=I2C_RW(hw, device_number, 0x13, 0x00,0, 1, debug);
// Max ADin limit
    val=I2C_RW(hw, device_number, 0x2E, 0xFFF0,1, 3, debug);
// Min ADin limit
    val=I2C_RW(hw, device_number, 0x30, 0x0000,1, 3, debug);
// Max Vin limit
    val=I2C_RW(hw, device_number, 0x24, 0xFFF0,1, 3, debug);
// Min Vin limit
    val=I2C_RW(hw, device_number, 0x26, 0x0000,1, 3, debug);
// Max current limit
    val=I2C_RW(hw, device_number, 0x1A, 0xFFF0,1, 3, debug);
    printf("New max threshold on current 0x%x\n",val&0xffff);
// Min current limit
    val=I2C_RW(hw, device_number, 0x1C, 0x0000,1, 3, debug);
    printf("New min threshold on current 0x%x\n",val&0xffff);
// Read and clear Fault register :
    ireg=4; // Alert register
    val=I2C_RW(hw, device_number, ireg, command,0, 2, debug);
    printf("Alert register content : 0x%x\n",val&0xffff);
    val=I2C_RW(hw, device_number, ireg, command,0, 2, debug);
    printf("Alert register content : 0x%x\n",val&0xffff);
// Set Alert register
    ireg=1;
    command=0x20; // Alert on Dsense overflow
    val=I2C_RW(hw, device_number, ireg, command,0, 3, debug);
    printf("New alert bit pattern : 0x%x\n", val&0xffff);
  }
  printf("\n");
  printf("Redo pwup_reset and Start LVRB auto scan mode\n");
  hw.getNode("VFE_CTRL").write(PWUP_RESET*1);
  hw.getNode("VFE_CTRL").write(0);
  hw.getNode("VFE_CTRL").write(LVRB_AUTOSCAN*1);
  hw.dispatch();

// Wait for voltage stabilization
  usleep(2000000);

// Read voltages measured by controler
  for(int iLVR=0; iLVR<2; iLVR++)
  {
    char alim[80];
    if(iLVR==0)
    {
      device_number=0x67;
      sprintf(alim,"V2P5");
    }
    else
    {
      device_number=0x6C;
      sprintf(alim,"V1P2");
    }
    val=I2C_RW(hw, device_number, 0x1E, 0,1, 2, 0);
    double Vmeas=((val>>4)&0xfff);
    Vmeas=Vmeas/4096.*102.4; // Convert to V (102.4 V full scale)
    printf("%s measured power supply voltage %7.3f V\n",alim,Vmeas);
    val=I2C_RW(hw, device_number, 0x28, 0,1, 2, 0);
    Vmeas=((val>>4)&0xfff);
    Vmeas=Vmeas/4096.*2.048; // Convert to V (2.048 V full scale)
    if(iLVR==0)Vmeas*=2.;
    printf("%s measured sense voltage %7.3f V\n",alim,Vmeas);
  }

// Setup ADCs
  printf("Program_ADCs...\n");
// Put ADC in two's complement mode (if no pedestal bias) and invert de conversion result
  negate_data=(negate_data&1)<<2;
  signed_data&=3;
  command=ADC_WRITE | ADC_OMODE_REG | negate_data | signed_data;
  printf("Put ADC coding : 0x%x\n",command);
  hw.getNode("DTU_CTRL").write(command);
  hw.dispatch();

// Set ADC input range (default=1.75V)
  input_span&=0x1F;
  command=ADC_WRITE | ADC_ISPAN_REG | input_span;
  printf("Set ADC input span range : 0x%x\n",command);
  hw.getNode("DTU_CTRL").write(command);
  hw.dispatch();

// Set ADC output test mode
  output_test&=0x0F;
  command=ADC_WRITE | ADC_OTEST_REG | output_test;
  printf("Set ADC output test mode : 0x%x\n",command);
  hw.getNode("DTU_CTRL").write(command);
  hw.dispatch();

// Set delay tap value for each ADC
    command=DELAY_RESET*1;
    hw.getNode("DELAY_CTRL").write(command);
    hw.dispatch();
    usleep(1000);

// Set the clock phases
  command = MEM_CLOCK_PHASE*mem_clock_phase+REG_CLOCK_PHASE*reg_clock_phase+IO_CLOCK_PHASE*IO_clock_phase+SEQ_CLOCK_PHASE*seq_clock_phase;
  printf("Set clock phases : 0x%x\n",command);
  hw.getNode("CLK_SETTING").write(command);
  reg = hw.getNode("FEAD_CTRL").read();
  hw.dispatch();
  printf("FEAD_CTRL register content : 0x%8.8x\n",reg.value());

  printf("Get lock status of IDELAY input stages\n");
  reg = hw.getNode("DELAY_CTRL").read();
  hw.dispatch();
  printf("Delay lock value : 0x%x\n",reg.value());

  for(int iADC=0; iADC<2; iADC++)
  {
    command=DELAY_RESET*0 | DELAY_INCREASE*1 | (0x7F<<(iADC*7));
    for(Int_t itap=0; itap<delay_val[iADC]; itap++)
    {
      hw.getNode("DELAY_CTRL").write(command);
      hw.dispatch();
    }
    printf("Get lock status of IDELAY input stages\n");
    reg = hw.getNode("DELAY_CTRL").read();
    hw.dispatch();
    printf("Value read : 0x%x\n",reg.value());
  }
// Add laser latency before catching data ~ 40 us
  hw.getNode("TRIG_DELAY").write((SW_DAQ_DELAY<<16)+HW_DAQ_DELAY);
  hw.dispatch();

// Read Temperature and others analog voltages with FPGA ADC
// Read XADC register 0x40 and set the requested average to 1 in XADC
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  ValWord<uint32_t> ave  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  unsigned loc_ave=ave.value()&0xffff;
  printf("Old config register 0x40 content : %x\n",loc_ave);

  loc_ave=0x8000;
  command=DRP_WRb*1 | (0x40<<16) | loc_ave;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  ave  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  loc_ave=ave.value()&0xffff;
  printf("New config register 0x40 content : %x\n",loc_ave);
  command=DRP_WRb*1 | (0x42<<16) | 0x0400;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();
// Switch off temp output on CATIA :
  device_number=1000 + (CATIA_num<<3);
  val=I2C_RW(hw, device_number, 1, 0x2,0, 3, debug);

  Int_t reg_def[7]={0x0,0x2,0x0,0x1087,0xFFFF,0xFFFF,0xF};
  Int_t reg_error[7]={0};
  if(test_I2C>0)
  {
    for(Int_t iloop=0; iloop<test_I2C; iloop++)
    {
      printf("Test_I2C, loop %d : ",iloop);
      for(Int_t loc_reg=1; loc_reg<=6; loc_reg++)
      {
        Int_t I2C_long=0;
        if(loc_reg==3 || loc_reg==4 || loc_reg==5) I2C_long=1;
        device_number=1000 + (CATIA_num<<3);
        if(loc_reg==4 || loc_reg==5)
        {
          for(Int_t loc_dac=0; loc_dac<4096; loc_dac++)
          {
            val=I2C_RW(hw, device_number, loc_reg, (0xF000 | loc_dac),I2C_long, 3, debug);
            val&=0xFFFF;
            if(val!=(0xF000 | loc_dac))reg_error[loc_reg]++;
            //printf("register %d, value put 0x%4.4x, read 0x%4.4x\n",loc_reg, loc_dac,val);
          }
        }
        else
        {
          val=I2C_RW(hw, device_number, loc_reg, 0,I2C_long, 2, debug);
          val&=0xFFFF;
          if(val!=reg_def[loc_reg])reg_error[loc_reg]++;
        }
        //printf("register %d, value read 0x%4.4x\n",loc_reg, val);
      }
      for(Int_t loc_reg=1; loc_reg<=6; loc_reg++) printf("%d ",reg_error[loc_reg]);
      printf("errors\n");
    }
    for(Int_t loc_reg=1; loc_reg<=6; loc_reg++)
    {
      if(loc_reg==4 || loc_reg==5)
        printf("reg %d : %d error/%d tries\n",loc_reg,reg_error[loc_reg],test_I2C*4096);
      else
        printf("reg %d : %d error/%d tries\n",loc_reg,reg_error[loc_reg],test_I2C);
    }
  }
//===========================================================================================================
// 0 : Get Vdac vs DAC transfer function for current injection 
// Now, make the temp measurement on all catias, sequentially end for both current gain
  TGraph *tg_Vref=new TGraph();
  tg_Vref->SetLineColor(kRed);
  tg_Vref->SetMarkerColor(kRed);
  tg_Vref->SetMarkerStyle(20);
  tg_Vref->SetMarkerSize(1.);
  tg_Vref->SetName("Vref_vs_reg");
  tg_Vref->SetTitle("Vref_vs_reg");
  TGraph *tg_Vdac[2], *tg_Vdac_resi[2];
  tg_Vdac[0] =new TGraph();
  tg_Vdac[0]->SetLineColor(kRed);
  tg_Vdac[0]->SetMarkerColor(kRed);
  tg_Vdac[0]->SetMarkerStyle(20);
  tg_Vdac[0]->SetMarkerSize(1.);
  tg_Vdac[0]->SetName("Vdac1_vs_reg");
  tg_Vdac[0]->SetTitle("Vdac1_vs_reg");
  tg_Vdac[1] =new TGraph();
  tg_Vdac[1]->SetLineColor(kBlue);
  tg_Vdac[1]->SetMarkerColor(kBlue);
  tg_Vdac[1]->SetMarkerStyle(20);
  tg_Vdac[1]->SetMarkerSize(1.);
  tg_Vdac[1]->SetName("Vdac2_vs_reg");
  tg_Vdac[1]->SetTitle("Vdac2_vs_reg");
  tg_Vdac_resi[0] =new TGraph();
  tg_Vdac_resi[0]->SetLineColor(kRed);
  tg_Vdac_resi[0]->SetMarkerColor(kRed);
  tg_Vdac_resi[0]->SetMarkerStyle(20);
  tg_Vdac_resi[0]->SetMarkerSize(1.);
  tg_Vdac_resi[0]->SetName("Vdac1_resi_vs_reg");
  tg_Vdac_resi[0]->SetTitle("Vdac1_resi_vs_reg");
  tg_Vdac_resi[1] =new TGraph();
  tg_Vdac_resi[1]->SetLineColor(kBlue);
  tg_Vdac_resi[1]->SetMarkerColor(kBlue);
  tg_Vdac_resi[1]->SetMarkerStyle(20);
  tg_Vdac_resi[1]->SetMarkerSize(1.);
  tg_Vdac_resi[1]->SetName("Vdac2_resi_vs_reg");
  tg_Vdac_resi[1]->SetTitle("Vdac2_resi_vs_reg");
  TGraph *tg_Vbuf[2], *tg_Vbuf_resi[2];
  tg_Vbuf[0] =new TGraph();
  tg_Vbuf[0]->SetLineColor(kRed);
  tg_Vbuf[0]->SetMarkerColor(kRed);
  tg_Vbuf[0]->SetMarkerStyle(20);
  tg_Vbuf[0]->SetMarkerSize(1.);
  tg_Vbuf[0]->SetName("Vbuf1_vs_reg");
  tg_Vbuf[0]->SetTitle("Vbuf1_vs_reg");
  tg_Vbuf[1] =new TGraph();
  tg_Vbuf[1]->SetLineColor(kBlue);
  tg_Vbuf[1]->SetMarkerColor(kBlue);
  tg_Vbuf[1]->SetMarkerStyle(20);
  tg_Vbuf[1]->SetMarkerSize(1.);
  tg_Vbuf[1]->SetName("Vbuf2_vs_reg");
  tg_Vbuf[1]->SetTitle("Vbuf2_vs_reg");
  tg_Vbuf_resi[0] =new TGraph();
  tg_Vbuf_resi[0]->SetLineColor(kRed);
  tg_Vbuf_resi[0]->SetMarkerColor(kRed);
  tg_Vbuf_resi[0]->SetMarkerStyle(20);
  tg_Vbuf_resi[0]->SetMarkerSize(1.);
  tg_Vbuf_resi[0]->SetName("Vbuf1_resi_vs_reg");
  tg_Vbuf_resi[0]->SetTitle("Vbuf1_resi_vs_reg");
  tg_Vbuf_resi[1] =new TGraph();
  tg_Vbuf_resi[1]->SetLineColor(kBlue);
  tg_Vbuf_resi[1]->SetMarkerColor(kBlue);
  tg_Vbuf_resi[1]->SetMarkerStyle(20);
  tg_Vbuf_resi[1]->SetMarkerSize(1.);
  tg_Vbuf_resi[1]->SetName("Vbuf2_resi_vs_reg");
  tg_Vbuf_resi[1]->SetTitle("Vbuf2_resi_vs_reg");
  Double_t DAC_val[33], Vdac[2][33], Vbuf[2][33], Vdac_offset[2], Vdac_slope[2], Vbuf_offset[2], Vbuf_slope[2], Vref[15], Temp[3];
  for(int XADC_meas=0; XADC_meas<5; XADC_meas++)
  {
    XADC_reg=0;
    if(XADC_meas==1)XADC_reg=XADC_Temp; // Temperature measurement
    if(XADC_meas==2)XADC_reg=XADC_Vref; // CATIA Vref measurement (pin 22)
    if(XADC_meas==3)XADC_reg=XADC_Vdac; // CATIA Vdac1 measurement (pin 25)
    if(XADC_meas==4)XADC_reg=XADC_Vdac; // CATIA Vdac2 measurement (pin 25)

    Int_t nmeas=1;
    if(XADC_meas==1)nmeas=2;
    if(XADC_meas==2)nmeas=n_Vref;
    if(XADC_meas==3)nmeas=n_Vdac;
    if(XADC_meas==4)nmeas=n_Vdac;
    for(int imeas=0; imeas<nmeas; imeas++)
    {
      int loc_meas=imeas;
      if(XADC_meas==1) // CATIA Temp measurements
      {
        command= (imeas<<4) | 0xa;
        val=I2C_RW(hw, device_number, 1, command,0, 3, debug);
        usleep(10000);
      }
      if(XADC_meas==2) // Scan Vref_reg values
      {
        val=I2C_RW(hw, device_number, 1, 2,0, 3, debug); // switch off the temp sensor
        if(imeas>0)loc_meas=imeas+1;
        command= (loc_meas<<4) | 0xF;
        val=I2C_RW(hw, device_number, 6, command,0, 3, debug);
        usleep(1000);
      }
      if(XADC_meas==3) // Scan Vdac values
      {
        val=I2C_RW(hw, device_number, 6, 0xF,0, 3, debug); // Enable Vref_ref, 0 in Vref_dac
        loc_meas=imeas*128;
        if(loc_meas>4095)loc_meas=4095;
        command=(DAC_buf_off<<15) | 0x1000 | loc_meas; // Scan DAC1 values 
        val=I2C_RW(hw, device_number, 4, command,1, 3, debug);
        if(imeas==0)usleep(100000);
        usleep(1000);
      }
      if(XADC_meas==4) // Scan Vdac values
      {
        val=I2C_RW(hw, device_number, 6, 0xF,0, 3, debug); // Enable Vref_ref, 0 in Vref_dac
        val=I2C_RW(hw, device_number, 4, (DAC_buf_off<<15) | 0x2000,1, 3, debug); // Output on DAC2 and enable Vdac buffer on Rev3
        ireg=5; // DAC2 control register
        loc_meas=imeas*128;
        if(loc_meas>4095)loc_meas=4095;
        val=I2C_RW(hw, device_number, 5, loc_meas,1, 3, debug);
        if(imeas==0)usleep(100000);
        usleep(1000);
      }

      double ave_val=0., ave_buf=0.;
      ValWord<uint32_t> temp, buf;
      for(int iave=0; iave<average; iave++)
      {
        command=DRP_WRb*0 | (XADC_reg<<16);
        hw.getNode("DRP_XADC").write(command);
        temp  = hw.getNode("DRP_XADC").read();
        hw.dispatch();
        double loc_val=double((temp.value()&0xffff)>>4)/4096.;
        ave_val+=loc_val;
      }
      ave_val/=average;
      if(XADC_meas==3 || XADC_meas==4)
      {
        for(int iave=0; iave<average; iave++)
        {
          command=DRP_WRb*0 | (XADC_Vbuf<<16);
          hw.getNode("DRP_XADC").write(command);
          buf  = hw.getNode("DRP_XADC").read();
          hw.dispatch();
          double loc_val=double((buf.value()&0xffff)>>4)/4096.*2.; // Hardwired divided by 2 to fit in XADC range
          //printf("%d %d %e\n",XADC_meas,imeas,loc_val);
          ave_buf+=loc_val;
        }
        ave_buf/=average;
      }

      if(XADC_meas==0)
      {
        Temp[0]=ave_val*4096*0.123-273.;
        printf("FPGA temperature, meas %d : %.2f deg\n",imeas, Temp[0]);
      }
      else if(XADC_meas==1)
      {
        Temp[imeas+1]=ave_val;
        printf("CATIA 0, X5 %d, temperature : %.4f V\n", imeas,Temp[imeas+1]);
      }
      else if(XADC_meas==2)
      {
        //printf("CATIA 0, Vref DAC %d, Vref : %.4f V\n", loc_meas,ave_val);
        Vref[imeas]=ave_val;
        tg_Vref->SetPoint(imeas,(Double_t)loc_meas,Vref[imeas]);
      }
      else if(XADC_meas==3)
      {
        //printf("CATIA 0, TP DAC1 %d, Vdac : %.4f V\n", loc_meas, ave_val);
        tg_Vdac[0]->SetPoint(imeas,(Double_t)loc_meas,ave_val);
        tg_Vbuf[0]->SetPoint(imeas,(Double_t)loc_meas,ave_buf);
        DAC_val[imeas]=loc_meas;
        Vdac[0][imeas]=ave_val;
        Vbuf[0][imeas]=ave_buf;
      }
      else if(XADC_meas==4)
      {
        //printf("CATIA 0, TP DAC2 %d, Vdac : %.4f V\n", loc_meas, ave_val);
        tg_Vdac[1]->SetPoint(imeas,(Double_t)loc_meas,ave_val);
        tg_Vbuf[1]->SetPoint(imeas,(Double_t)loc_meas,ave_buf);
        Vdac[1][imeas]=ave_val;
        Vbuf[1][imeas]=ave_buf;
      }
    }
  }
  for(int iDAC=0; iDAC<2; iDAC++)
  {
    tg_Vdac[iDAC]->Fit("pol1","WQ","",0.,3500.);
    fDAC[iDAC]=tg_Vdac[iDAC]->GetFunction("pol1");
    Int_t n=tg_Vdac[iDAC]->GetN();
    for(Int_t i=0; i<n; i++)
    {
      Double_t x,y;
      tg_Vdac[iDAC]->GetPoint(i,x,y);
      tg_Vdac_resi[iDAC]->SetPoint(i,x,y-fDAC[iDAC]->Eval(x));
    }
    Vdac_offset[iDAC]=fDAC[iDAC]->GetParameter(0)*1000.;
    Vdac_slope[iDAC]=fDAC[iDAC]->GetParameter(1)*1000.;
    printf("Vdac%d offset : %e mV, slope : %e mV/lsb\n",iDAC,Vdac_offset[iDAC],Vdac_slope[iDAC]); 

    tg_Vbuf[iDAC]->Fit("pol1","WQ","",0.,3500.);
    fBuf[iDAC]=tg_Vbuf[iDAC]->GetFunction("pol1");
    n=tg_Vbuf[iDAC]->GetN();
    for(Int_t i=0; i<n; i++)
    {
      Double_t x,y;
      tg_Vbuf[iDAC]->GetPoint(i,x,y);
      tg_Vbuf_resi[iDAC]->SetPoint(i,x,y-fBuf[iDAC]->Eval(x));
    }
    Vbuf_offset[iDAC]=fBuf[iDAC]->GetParameter(0)*1000.;
    Vbuf_slope[iDAC]=fBuf[iDAC]->GetParameter(1)*1000.;
    printf("Vbuf%d Offset %e mV, slope : %e mV/lsb\n",iDAC,Vbuf_offset[iDAC],Vbuf_slope[iDAC]); 
  }

// Read CATIA power consumption :
  fault   = hw.getNode("LVRB_FAULT").read();
  current = hw.getNode("LVRB_CURRENT").read();
  hw.dispatch();
  Double_t Imeas_V2P5=((current.value()>>4)&0xfff)/4096.*102.4/Rshunt_V2P5;
  Double_t Imeas_V1P2=((current.value()>>20)&0xfff)/4096.*102.4/Rshunt_V1P2;
  printf("LVRB faults : %8.8x\n",fault.value()); 
  printf("CATIA consumption %8.8x : 1.2V : %.2f mA, 2.5V : %.2f mA\n",current.value(),Imeas_V1P2, Imeas_V2P5); 

// Stop LVRB auto scan, not to generate noise in CATIA during calibration
  hw.getNode("VFE_CTRL").write(0);
  hw.dispatch();

//===========================================================================================================
// 1 : Pedestal value vs PED_DAC value.
// Read 1 event of 1000 samples
  printf("Start Pedestal study\n");
  TGraph *tg_ped_DAC[2];
  tg_ped_DAC[0]=new TGraph();
  tg_ped_DAC[0]->SetLineColor(kRed);
  tg_ped_DAC[0]->SetMarkerColor(kRed);
  tg_ped_DAC[0]->SetMarkerSize(kRed);
  tg_ped_DAC[0]->SetName("Ped_vs_DAC_G1");
  tg_ped_DAC[0]->SetTitle("Ped_vs_DAC_G1");
  tg_ped_DAC[1]=new TGraph();
  tg_ped_DAC[1]->SetLineColor(kBlue);
  tg_ped_DAC[1]->SetMarkerColor(kBlue);
  tg_ped_DAC[1]->SetMarkerSize(kBlue);
  tg_ped_DAC[1]->SetName("Ped_vs_DAC_G10");
  tg_ped_DAC[1]->SetTitle("Ped_vs_DAC_G10");
  Double_t ped_DAC_par[2][2];
  trigger_type = 0;
  nevent       = 1;
  nsample      = 1000;
// Switch to triggered mode + internal trigger, reduce noise by disabling CALIB_PULSE  :
  fead_ctrl=  ADC_CALIB_MODE        *0 |
              SEL_CALIB_PULSE       *0 |
              SELF_TRIGGER_MASK     *0 |
              SELF_TRIGGER_THRESHOLD*0 |
              SELF_TRIGGER          *0 |
              SOFT_TRIGGER          *1 |
              TRIGGER_MODE          *1 | // Always DAQ on trigger
              RESET                 *0;
  hw.getNode("FEAD_CTRL").write(fead_ctrl);
// Stop DAQ and ask for NSAMPLE per frame (+timestamp) :
  command = ((nsample+2)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
// Switch off FE-adapter LEDs
  hw.getNode("FW_VER").write(0);
  hw.dispatch();

// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
// Start DAQ :
  command = ((nsample+2)<<16)+CAPTURE_START;
  hw.getNode("CAP_CTRL").write(command);
  reg=hw.getNode("CAP_CTRL").read();
  hw.dispatch();
  //printf("CAP_CTRL content : 0x%8.8x\n",reg.value());

  val=I2C_RW(hw, device_number, 1, 2,0, 3, debug); // Switch off temp sensor
  val=I2C_RW(hw, device_number, 4, 0,1, 3, debug); // Switch off every thing 
  val=I2C_RW(hw, device_number, 5, 0,1, 3, debug);
  val=I2C_RW(hw, device_number, 6, 0,0, 3, debug);
// First event to compute correction for 20 MHz oscillations :
#if VERSION == 1
  val=I2C_RW(hw, device_number, 3, 0x1086,1, 3, debug);
#else
  val=I2C_RW(hw, device_number, 3, 0xe082,1, 3, debug);
#endif
  usleep(100000);
  mean[0]=0.;
  mean[1]=0.;

  timestamp=get_event(hw,trigger_type,nsample,mem,event,fevent,debug,0);
  Int_t nseq=nsample/8;
  for(Int_t iseq=0; iseq<nseq; iseq++)
  {
    for(Int_t is=0; is<8; is++)
    {
      mean[0]+=fevent[0][iseq*8+is];
      mean[1]+=fevent[1][iseq*8+is];
    }
  }
  mean[0]/=nseq*8.;
  mean[1]/=nseq*8.;
//
  for(int idac=0; idac<n_PED_dac; idac++)
  {
    //printf("VCM DAC : %d\n",idac);
#if VERSION == 1
    val=I2C_RW(hw, device_number, 3, (idac<<8)|(idac<<3)|6,1, 3, debug);
#else
    val=I2C_RW(hw, device_number, 3, 0xc000 | (idac<<8)|(idac<<2)|2,1, 3, debug); // For V1.4
#endif
    usleep(100000);
    Double_t mean[2]={0.,0.};
    draw=1;
    for(int ievt=0; ievt<nevent; ievt++)
    {
      timestamp=get_event(hw,trigger_type,nsample,mem,event,fevent,debug,draw);
      draw=0;
      for(Int_t is=0; is<nsample; is++)
      {
        mean[0]+=fevent[0][is]*dv;
        mean[1]+=fevent[1][is]*dv;
      }
    }
    mean[0]/=(nsample*nevent);
    mean[1]/=(nsample*nevent);
    tg_ped_DAC[0]->SetPoint(idac,(Double_t)idac,mean[0]-900.);
    tg_ped_DAC[1]->SetPoint(idac,(Double_t)idac,mean[1]-900.);
    printf("ped DAC %d, ped G1 %f, ped G10 %f\n",idac,mean[0],mean[1]);
  }
  tg_ped_DAC[0]->Fit("pol1","WQ","",0.,n_PED_dac);
  f1=tg_ped_DAC[0]->GetFunction("pol1");
  ped_DAC_par[0][0]=f1->GetParameter(0);
  ped_DAC_par[0][1]=f1->GetParameter(1);
  printf("G1  pedestal offset %.2f mV, slope %.2f mV/lsb\n",ped_DAC_par[0][0],ped_DAC_par[0][1]);
  tg_ped_DAC[1]->Fit("pol1","WQ","",0.,n_PED_dac);
  f1=tg_ped_DAC[1]->GetFunction("pol1");
  ped_DAC_par[1][0]=f1->GetParameter(0);
  ped_DAC_par[1][1]=f1->GetParameter(1);
  printf("G10 pedestal offset %.2f mV, slope %.2f mV/lsb\n",ped_DAC_par[1][0],ped_DAC_par[1][1]);
// Stop DAQ
  command = ((nsample+2)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();

//===========================================================================================================
// 2 : Noise for R=400 and R=500 and for LPF=50MHz and LPF=35 MHz
  Double_t G1_noise[2][2]={0.,0.,0.,0.};
  Double_t G10_noise[2][2]={0.,0.,0.,0.};
  trigger_type=0;
  nevent=50;
  nsample=65532;
  Double_t *rex,*rey,*imx,*imy,*mod;
  rex=(Double_t*)malloc(nsample*sizeof(Double_t));
  imx=(Double_t*)malloc(nsample*sizeof(Double_t));
  rey=(Double_t*)malloc(nsample*sizeof(Double_t));
  imy=(Double_t*)malloc(nsample*sizeof(Double_t));
  mod=(Double_t*)malloc(nsample*sizeof(Double_t));
  TVirtualFFT *fft_f=TVirtualFFT::FFT(1,&nsample,"C2CF K");
  Double_t dt=1./NSPS;
  Double_t tmax=nsample*dt;
  Double_t fmax=1./dt;
  Double_t df=fmax/nsample;
  TProfile *pmod[2];
  pmod[0]=new TProfile("NDS_G1","NDS_G1",nsample,0,fmax);
  pmod[1]=new TProfile("NDS_G10","NDS_G10",nsample,0,fmax);
  TH1D *hmod[2][4];
  hmod[0][0]=new TH1D("NDS_G1_RTIA_500_LPF_50","NDS_G1_RTIA_500_LPF_50",nsample/2,0.,fmax/2.);
  hmod[0][1]=new TH1D("NDS_G1_RTIA_500_LPF_35","NDS_G1_RTIA_500_LPF_35",nsample/2,0.,fmax/2.);
  hmod[0][2]=new TH1D("NDS_G1_RTIA_400_LPF_50","NDS_G1_RTIA_400_LPF_50",nsample/2,0.,fmax/2.);
  hmod[0][3]=new TH1D("NDS_G1_RTIA_400_LPF_35","NDS_G1_RTIA_400_LPF_35",nsample/2,0.,fmax/2.);
  hmod[1][0]=new TH1D("NDS_G10_RTIA_500_LPF_50","NDS_G10_RTIA_500_LPF_50",nsample/2,0.,fmax/2.);
  hmod[1][1]=new TH1D("NDS_G10_RTIA_500_LPF_35","NDS_G10_RTIA_500_LPF_35",nsample/2,0.,fmax/2.);
  hmod[1][2]=new TH1D("NDS_G10_RTIA_400_LPF_50","NDS_G10_RTIA_400_LPF_50",nsample/2,0.,fmax/2.);
  hmod[1][3]=new TH1D("NDS_G10_RTIA_400_LPF_35","NDS_G10_RTIA_400_LPF_35",nsample/2,0.,fmax/2.);
  TGraph *tg_pulse[2];
  tg_pulse[0]=new TGraph();
  tg_pulse[0]->SetLineColor(kRed);
  tg_pulse[0]->SetMarkerColor(kRed);
  tg_pulse[0]->SetMarkerSize(0.1);
  tg_pulse[0]->SetTitle("G1_R400_LPF35_pedestal_frame");
  tg_pulse[0]->SetName("G1_R400_LPF35_pedestal_frame");
  tg_pulse[1]=new TGraph();
  tg_pulse[1]->SetLineColor(kRed);
  tg_pulse[1]->SetMarkerColor(kRed);
  tg_pulse[1]->SetMarkerSize(0.1);
  tg_pulse[1]->SetTitle("G10_R400_LPF35_pedestal_frame");
  tg_pulse[1]->SetName("G10_R400_LPF35_pedestal_frame");
  TGraph *tg_calib[2];
  tg_calib[0]=new TGraph();
  tg_calib[0]->SetLineColor(kRed);
  tg_calib[0]->SetMarkerColor(kRed);
  tg_calib[0]->SetMarkerSize(0.1);
  tg_calib[0]->SetTitle("G1_R400_LPF35_calib_pulse");
  tg_calib[0]->SetName("G1_R400_LPF35_calib_pulse");
  tg_calib[1]=new TGraph();
  tg_calib[1]->SetLineColor(kRed);
  tg_calib[1]->SetMarkerColor(kRed);
  tg_calib[1]->SetMarkerSize(0.1);
  tg_calib[1]->SetTitle("G10_R400_LPF35_calib_pulse");
  tg_calib[1]->SetName("G10_R400_LPF35_calib_pulse");

// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
// Start DAQ :
  command = ((nsample+2)<<16)+CAPTURE_START;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();
  val=I2C_RW(hw, device_number, 1, 2,0, 3, debug); // Switch off temp sensor
  val=I2C_RW(hw, device_number, 4, 0,1, 3, debug); // Switch off every thing 
  val=I2C_RW(hw, device_number, 5, 0,1, 3, debug);
  val=I2C_RW(hw, device_number, 6, 0,0, 3, debug);
  for(int iRTIA=0; iRTIA<2; iRTIA++)
  {
    for(int iLPF=0; iLPF<2; iLPF++)
    {
      pmod[0]->Reset();
      pmod[1]->Reset();
#if VERSION == 1
      val=I2C_RW(hw, device_number, 3, (0x10<<8) | (0x10<<3) | (iRTIA<<2) | (iLPF<<1) | 0,1, 3, debug);
#else
      val=I2C_RW(hw, device_number, 3, (0x20<<8) | (0x20<<2) | (iRTIA<<14) | (iLPF<<1) | 0,1, 3, debug);
#endif
      usleep(100000);
      draw=1;
      for(int ievt=0; ievt<nevent; ievt++)
      {
        ped[0]=0.,rms[0]=0.;
        ped[1]=0.,rms[1]=0.;
        timestamp=get_event(hw,trigger_type,nsample,mem,event,fevent,debug,draw);
        draw=0;
        mean[0]=0.;
        mean[1]=0.;
        Int_t nseq=nsample/8;
        for(Int_t iseq=0; iseq<nseq; iseq++)
        {
          for(Int_t is=0; is<8; is++)
          {
            mean[0]+=fevent[0][iseq*8+is];
            mean[1]+=fevent[1][iseq*8+is];
          }
        }
        mean[0]/=nseq*8.;
        mean[1]/=nseq*8.;

        for(Int_t is=0; is<nsample; is++)
        {
          ped[0]+=fevent[0][is]*dv;
          rms[0]+=fevent[0][is]*dv*fevent[0][is]*dv;
          Double_t t=dt*is;
          if(iRTIA==1 && iLPF==1 && ievt==0) tg_pulse[0]->SetPoint(is,t,fevent[0][is]*dv);
          rex[is]=fevent[0][is]*dv;
          imx[is]=0.;
        }
        ped[0]/=nsample;
        rms[0]/=nsample;
        rms[0]=rms[0]-ped[0]*ped[0];
        G1_noise[iRTIA][iLPF]+=rms[0];
        fft_f->SetPointsComplex(rex,imx);
        fft_f->Transform();
        fft_f->GetPointsComplex(rey,imy);
        for(Int_t is=0; is<nsample; is++)
        {
          Double_t f=df*(is+0.5);
          rey[is]/=nsample;
          imy[is]/=nsample;
          mod[is]=rey[is]*rey[is]+imy[is]*imy[is];
          pmod[0]->Fill(f,mod[is]/df);
        }
        for(Int_t is=0; is<nsample; is++)
        {
          ped[1]+=fevent[1][is]*dv;
          rms[1]+=fevent[1][is]*dv*fevent[1][is]*dv;
          Double_t t=dt*is;
          if(iRTIA==1 && iLPF==1 && ievt==0)tg_pulse[1]->SetPoint(is,t,fevent[1][is]*dv);
          rex[is]=fevent[1][is]*dv;
          imx[is]=0.;
        }
        ped[1]/=nsample;
        rms[1]/=nsample;
        rms[1]=rms[1]-ped[1]*ped[1];
        G10_noise[iRTIA][iLPF]+=rms[1];
        fft_f->SetPointsComplex(rex,imx);
        fft_f->Transform();
        fft_f->GetPointsComplex(rey,imy);
        for(Int_t is=0; is<nsample; is++)
        {
          Double_t f=df*(is+0.5);
          rey[is]/=nsample;
          imy[is]/=nsample;
          mod[is]=rey[is]*rey[is]+imy[is]*imy[is];
          pmod[1]->Fill(f,mod[is]/df);
        }
        if(ievt==0)printf("G1 noise : %.0f uV, G10 noise : %.0f uV\n",sqrt(rms[0])*1000.,sqrt(rms[1])*1000.);
      }
      G1_noise[iRTIA][iLPF]=sqrt(G1_noise[iRTIA][iLPF]/nevent)*1000.;
      G10_noise[iRTIA][iLPF]=sqrt(G10_noise[iRTIA][iLPF]/nevent)*1000.;
      hmod[0][iRTIA*2+iLPF]->SetBinContent(1,0.);
      hmod[1][iRTIA*2+iLPF]->SetBinContent(1,0.);
      for(Int_t is=1; is<nsample/2; is++)
      {
        Double_t mod=0.;
        mod=pmod[0]->GetBinContent(is+1);
        hmod[0][iRTIA*2+iLPF]->SetBinContent(is+1,sqrt(2.*mod));
        mod=pmod[1]->GetBinContent(is+1);
        hmod[1][iRTIA*2+iLPF]->SetBinContent(is+1,sqrt(2.*mod));
      }
    }
  }
// Stop DAQ
  command = ((nsample+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();

//===========================================================================================================
// 3 : Send internal TP triggers for linearity measurements : G10 and G1
// calibration trigger setting :
  trigger_type=1;
  nevent=100;
  nsample=300;
  n_calib=32;
  calib_step=128;
  Double_t V_Iinj_slope[2], INL_min[2]={9999.,9999.}, INL_max[2]={-9999.,-9999.};
  TProfile *tp_inj1[2][n_calib];
  for(int iinj=0; iinj<n_calib; iinj++)
  {
    sprintf(hname,"pulse_G1_DAC_%4.4d",calib_level+iinj*calib_step);
    tp_inj1[0][iinj]=new TProfile(hname,hname,nsample,0.,dt*nsample);
    sprintf(hname,"pulse_G10_DAC_%4.4d",calib_level+iinj*calib_step);
    tp_inj1[1][iinj]=new TProfile(hname,hname,nsample,0.,dt*nsample);
  }
  TGraph *tg_inj1[2], *tg_inj2[2], *tg_inj3[2];
  tg_inj1[0]=new TGraph();
  tg_inj1[0]->SetTitle("Inj_vs_iDAC_G1");
  tg_inj1[0]->SetName("Inj_vs_iDAC_G1");
  tg_inj1[0]->SetMarkerStyle(20);
  tg_inj1[0]->SetMarkerSize(1.0);
  tg_inj1[0]->SetMarkerColor(kRed);
  tg_inj1[0]->SetLineColor(kRed);
  tg_inj1[0]->SetLineWidth(2);
  tg_inj1[1]=new TGraph();
  tg_inj1[1]->SetTitle("Inj_vs_iDAC_G10");
  tg_inj1[1]->SetName("Inj_vs_iDAC_G10");
  tg_inj1[1]->SetMarkerStyle(20);
  tg_inj1[1]->SetMarkerSize(1.0);
  tg_inj1[1]->SetMarkerColor(kBlue);
  tg_inj1[1]->SetLineColor(kBlue);
  tg_inj1[1]->SetLineWidth(2);
  tg_inj2[0]=new TGraph();
  tg_inj2[0]->SetTitle("Inj_vs_vDAC_G1");
  tg_inj2[0]->SetName("Inj_vs_vDAC_G1");
  tg_inj2[0]->SetMarkerStyle(20);
  tg_inj2[0]->SetMarkerSize(1.0);
  tg_inj2[0]->SetMarkerColor(kRed);
  tg_inj2[0]->SetLineColor(kRed);
  tg_inj2[0]->SetLineWidth(2);
  tg_inj2[1]=new TGraph();
  tg_inj2[1]->SetTitle("Inj_vs_vDAC_G10");
  tg_inj2[1]->SetName("Inj_vs_vDAC_G10");
  tg_inj2[1]->SetMarkerStyle(20);
  tg_inj2[1]->SetMarkerSize(1.0);
  tg_inj2[1]->SetMarkerColor(kBlue);
  tg_inj2[1]->SetLineColor(kBlue);
  tg_inj2[1]->SetLineWidth(2);
  tg_inj3[0]=new TGraph();
  tg_inj3[0]->SetTitle("INL_vs_amplitude_G1");
  tg_inj3[0]->SetName("INL_vs_amplitude_G1");
  tg_inj3[0]->SetMarkerStyle(20);
  tg_inj3[0]->SetMarkerSize(1.0);
  tg_inj3[0]->SetMarkerColor(kRed);
  tg_inj3[0]->SetLineColor(kRed);
  tg_inj3[0]->SetLineWidth(2);
  tg_inj3[0]->SetMaximum(0.003);
  tg_inj3[0]->SetMinimum(-.003);
  tg_inj3[1]=new TGraph();
  tg_inj3[1]->SetTitle("INL_vs_amplitude_G10");
  tg_inj3[1]->SetName("INL_vs_amplitude_G10");
  tg_inj3[1]->SetMarkerStyle(20);
  tg_inj3[1]->SetMarkerSize(1.0);
  tg_inj3[1]->SetMarkerColor(kBlue);
  tg_inj3[1]->SetLineColor(kBlue);
  tg_inj3[1]->SetLineWidth(2);
  tg_inj3[1]->SetMaximum(0.003);
  tg_inj3[1]->SetMinimum(-.003);

  command=(calib_delay<<16) | (calib_width&0xffff);
  printf("Calibration trigger with %d clocks width and %d clocks delay : %x\n",calib_width,calib_delay,command);
  hw.getNode("CALIB_CTRL").write(command);
// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
// Start DAQ :
  command = ((nsample+2)<<16)+CAPTURE_START;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();

  val=I2C_RW(hw, device_number, 1, 2,0, 3, debug); // Switch off temp sensor
#if VERSION == 1
  val=I2C_RW(hw, device_number, 3, (0x10<<8) | (0x10<<3) | (1<<2) | (1<<1) | 0,1, 3, debug);
#else
  val=I2C_RW(hw, device_number, 3, (0x30<<8) | (0x30<<2) | (3<<14) | (1<<1) | 0,1, 3, debug);
#endif
  //printf("Reg3 register value read : 0x%x, %d\n",val,val&0xFFF);
  val=I2C_RW(hw, device_number, 5, 0,1, 3, debug); // Switch off every thing 
  //printf("Reg5 register value read : 0x%x, %d\n",val,val&0xFFF);
  usleep(10000);
  for(int igain=0; igain<2; igain++)
  {
    calib_level=0;
    val=I2C_RW(hw, device_number, 6, 0x0b | ((1-igain)<<2),0, 3, debug); // Rconv 2471 or 272, TIA_dummy ON, TP ON, Vreg O
    //printf("Reg6 register value read : 0x%x, %d\n",val,val&0xFFF);
    Double_t fit_min=-1, fit_max=-1;
    for(int istep=0; istep<n_calib; istep++)
    {
// Program DAC for this step
      val=I2C_RW(hw, device_number, 4, 0x1000|calib_level,1, 3, debug); // TP with DAC1
      if((val&0xFFF)!=calib_level) printf("Error on DAC setting : 0x%4.4x written, 0x%4.4x read back\n",calib_level,val&0xFFF);
      //printf("DAC register value read : 0x%x, %d\n",val,val&0xFFF);
// Wait for Vdac to stabilize :
      usleep(100000);

      int ievt=0;
      draw=1;
      for(Int_t ievt=0; ievt<nevent; ievt++)
      {
        timestamp=get_event(hw,trigger_type,nsample,mem,event,fevent,debug,draw);
        if(debug==0)
        {
          system("stty raw");
          char cdum=getchar();
          system("stty -raw");
          if(cdum=='c')draw=0;
        }
        else 
          draw=0;
        if(timestamp<0) printf("DAQ error on event %d : %d\n",ievt, timestamp);

        for(int is=0; is<nsample; is++)
        {
          tp_inj1[igain][istep]->Fill(dt*(is+0.5),fevent[igain][is]*dv);
        }

      }
      tp_inj1[igain][istep]->Fit("pol0","wq","",0.e-9,450.e-9);
      f1=tp_inj1[igain][istep]->GetFunction("pol0");
      mean[igain]=f1->GetParameter(0);
      tp_inj1[igain][istep]->Fit("pol1","wq","",800.e-9,1600.e-9);
      f1=tp_inj1[igain][istep]->GetFunction("pol1");
      //Double_t val0=f1->GetParameter(0)+f1->GetParameter(1)*500.e-9-mean[igain];
      Double_t val0=f1->Eval(500.e-9)-mean[igain];
      tg_inj1[igain]->SetPoint(istep,calib_level,val0);
      tg_inj2[igain]->SetPoint(istep,fDAC[0]->Eval(calib_level)*1000.,val0);
      if(fit_min<0 && val0>0.)fit_min=calib_level-calib_step/2.;
      if(val0<1200.)fit_max=calib_level+calib_step/2.;
      calib_level+=calib_step;
    }
    tg_inj1[igain]->Fit("pol1","WQ","",fit_min,fit_max);
    tg_inj2[igain]->Fit("pol1","WQ","",fDAC[0]->Eval(fit_min)*1000.,fDAC[0]->Eval(fit_max)*1000.);
    f1=tg_inj2[igain]->GetFunction("pol1");
    V_Iinj_slope[igain]=f1->GetParameter(1);
    printf("Gain %d : V_Iinj slope = %.3f\n",igain,V_Iinj_slope[igain]);
    f1=tg_inj1[igain]->GetFunction("pol1");
// Compute residuals :
    Int_t n=tg_inj1[igain]->GetN();
    for(Int_t is=0; is<n; is++)
    {
      Double_t x,y;
      tg_inj1[igain]->GetPoint(is,x,y);
      tg_inj3[igain]->SetPoint(is,y,(y-f1->Eval(x))/1200.);
      if(y>0. && y<1200. && (y-f1->Eval(x))/1200.>INL_max[igain])INL_max[igain]=(y-f1->Eval(x))/1200.;
      if(y>0. && y<1200. && (y-f1->Eval(x))/1200.<INL_min[igain])INL_min[igain]=(y-f1->Eval(x))/1200.;
    }
    printf("Gain %d INL min %.2f max %.2f\n",igain,INL_min[igain]*1000., INL_max[igain]*1000.);
  }
// Stop DAQ
  command = ((nsample+2)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();

//===========================================================================================================
// 4 : Send G10 and G1 calibration pulses with R=500 and R=400
// Enabe CALIB_PULSE  :
  Double_t Rcalib[2]={1000.,10000.}; // Conversion resistor on PCB to compute CATIA gain
  Double_t Rconv[2];
  Double_t CATIA_gain[2][2], TIA_gain[2], gain_stage;
  fead_ctrl=  ADC_CALIB_MODE        *0 |
              SEL_CALIB_PULSE       *1 |
              SELF_TRIGGER_MASK     *0 |
              SELF_TRIGGER_THRESHOLD*0 |
              SELF_TRIGGER          *0 |
              SOFT_TRIGGER          *1 |
              TRIGGER_MODE          *1 | // Always DAQ on trigger
              RESET                 *0;
  hw.getNode("FEAD_CTRL").write(fead_ctrl);
  nsample=500;
  nevent=100;
  command=(calib_delay<<16) | (calib_width&0xffff);
  printf("Calibration trigger with %d clocks width and %d clocks delay : %x\n",calib_width,calib_delay,command);
  hw.getNode("CALIB_CTRL").write(command);
// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
// Start DAQ :
  command = ((nsample+2)<<16)+CAPTURE_START;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();

  val=I2C_RW(hw, device_number, 6, 0x0,0, 3, debug); // TP stuff off
  val=I2C_RW(hw, device_number, 4, 0x0,1, 3, debug); // DAC1 and DAC2 off
  TProfile *tp_calib[2][2];
  tp_calib[0][0]=new TProfile("G1_R500_calib_pulse_response","G1_R500_calib_pulse_response",nsample,0.,nsample*dt);
  tp_calib[0][1]=new TProfile("G1_R400_calib_pulse_response","G1_R400_calib_pulse_response",nsample,0.,nsample*dt);
  tp_calib[1][0]=new TProfile("G10_R500_calib_pulse_response","G10_R500_calib_pulse_response",nsample,0.,nsample*dt);
  tp_calib[1][1]=new TProfile("G10_R400_calib_pulse_response","G10_R400_calib_pulse_response",nsample,0.,nsample*dt);
  TF1 *fcalib=new TF1("fcalib","[0]+[1]*(1.+tanh((x-[2])/[3]))/2.",0.,2.e3);
  fcalib->SetParameter(0,2800.);
  fcalib->SetParameter(1,5600.);
  fcalib->SetParameter(2,1.18e-6);
  fcalib->SetParameter(3,9.e-9);
  TH1D *hcalib_t0=new TH1D("t0_calib","t0_calib",1000,1.17e-6,1.19e-6);
  TH1D *hcalib_rt=new TH1D("RT_calib","RT_calib",1000,7.e-9,12.e-9);

  for(int iG=0; iG<2; iG++)
  {
    trigger_type=3+iG;
    for(int iRTIA=0; iRTIA<2; iRTIA++)
    {
#if VERSION == 1
      val=I2C_RW(hw, device_number, 3, (0x10<<8) | (0x10<<3) | (iRTIA<<2) | (1<<1) | 0,1, 3, debug);
#else
      val=I2C_RW(hw, device_number, 3, (0x30<<8) | (0x30<<2) | (iRTIA<<14) | (1<<1) | 0,1, 3, debug);
#endif
      usleep(100000);
      draw=1;
      for(int ievt=0; ievt<nevent; ievt++)
      {
        timestamp=get_event(hw,trigger_type,nsample,mem,event,fevent,debug,draw);
        mean[0]=0.;
        mean[1]=0.;
        Int_t nseq=22;
        for(Int_t iseq=0; iseq<nseq; iseq++)
        {
          for(Int_t is=0; is<8; is++)
          {
            mean[0]+=fevent[0][iseq*8+is];
            mean[1]+=fevent[1][iseq*8+is];
          }
        }
        mean[0]/=nseq*8.;
        mean[1]/=nseq*8.;

        draw=0;
        for(int is=0; is<nsample; is++)
        {
          Double_t t=dt*(is+0.5);
          tp_calib[iG][iRTIA]->Fill(t,fevent[iG][is]*(dv/1000.));
          if(iRTIA==1)tg_calib[iG]->SetPoint(is,dt*is,fevent[iG][is]);

        }
        if(iRTIA==1 && iG==1)
        {
          tg_calib[iG]->Fit(fcalib,"WQ","",1.1e-6,1.30e-6);
          hcalib_t0->Fill(fcalib->GetParameter(2));
          hcalib_rt->Fill(fcalib->GetParameter(3));
        }
      }
      tp_calib[iG][iRTIA]->Fit("pol0","WQ","",0.,1.e-6);
      f1=tp_calib[iG][iRTIA]->GetFunction("pol0");
      mean[iG]=f1->GetParameter(0);
      tp_calib[iG][iRTIA]->Fit("pol1","WQ","",1.3e-6,2.1e-6);
      f1=tp_calib[iG][iRTIA]->GetFunction("pol1");
      CATIA_gain[iG][iRTIA]=(f1->Eval(1.2e-6)-mean[iG])/(1.2/Rcalib[iG]);
      printf("Gain %.0f, R_TIA %.0f, Total gain : %e V/A\n",gain[iG],RTIA[iRTIA],CATIA_gain[iG][iRTIA]);
    }
  }
  gain_stage=(CATIA_gain[1][0]/CATIA_gain[0][0]+CATIA_gain[1][1]/CATIA_gain[0][1])/2.; // (G10-R500/G1-R500 + G10-R400/G1-R400)/2
  TIA_gain[0]=CATIA_gain[0][0]; // G1-R500
  TIA_gain[1]=CATIA_gain[0][1]; // G1-R400
  printf("G10 with R500 : %.2f, with R400 : %.2f, mean : %.2f\n",CATIA_gain[1][0]/CATIA_gain[0][0],CATIA_gain[1][1]/CATIA_gain[0][1],gain_stage);
  printf("R500 = %.0f Ohm\n",TIA_gain[0]);
  printf("R400 = %.0f Ohm\n",TIA_gain[1]);
  Rconv[0]=CATIA_gain[0][1]/V_Iinj_slope[0]; // Iinj conversion resistor for G1
  Rconv[1]=CATIA_gain[1][1]/V_Iinj_slope[1]; // Iinj conversion resistor for G10
  printf("Rconv Iinj G1  (272 Ohm)  = %.0f Ohm\n",Rconv[0]);
  printf("Rconv Iinj G10 (2471 Ohm) = %.0f Ohm\n",Rconv[1]);
  hcalib_t0->Fit("gaus","LQ","");
  f1=hcalib_t0->GetFunction("gaus");
  Double_t calib_pos=f1->GetParameter(1);
  Double_t calib_jitter=f1->GetParameter(2);
  hcalib_rt->Fit("gaus","LQ","");
  f1=hcalib_rt->GetFunction("gaus");
  Double_t RT=f1->GetParameter(1);
  Double_t eRT=f1->GetParameter(2);
  printf("Pulse jitter : %.1f ps, rise time %.2f ns\n",calib_jitter*1.e12,RT*1.e9);

// Stop DAQ :
  command = ((nsample+2)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
// Switch on FE-adapter LEDs
  command = LED_ON*1+GENE_100HZ*0+GENE_TRIGGER*0;
  hw.getNode("FW_VER").write(command);
  hw.dispatch();
// Restart LVRB auto scan
  printf("Restart LVRB auto scan mode\n");
  hw.getNode("VFE_CTRL").write(LVRB_AUTOSCAN*1);
  hw.dispatch();
// Switch off CATIA LV to change chip :
// First ask to generate alert with overcurrent
  val=I2C_RW(hw,0x67,0x01,0x20,0,1,debug);
  val=I2C_RW(hw,0x6C,0x01,0x20,0,1,debug);
// Now, generate a current overflow :
  val=I2C_RW(hw,0x67,0x03,0x20,0,1,debug);
  val=I2C_RW(hw,0x6C,0x03,0x20,0,1,debug);
  usleep(1000000);
// Read current values for control :
  val=I2C_RW(hw, 0x67, 0x14, 0,1, 2, debug); // Read Dsense value
  printf("CATIA V1P2 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V1P2);
  val=I2C_RW(hw, 0x6C, 0x14, 0,1, 2, debug); // Read Dsense value
  printf("CATIA V2P5 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V2P5);

// Now compute Rconv for current injection should be around 272 and 2472 Ohms
  sprintf(output_file,"data/CATIA_calib/CATIA_%4.4d_calib.root",CATIA_number);
  TFile *fd=new TFile(output_file,"recreate");
  tg_Vref->Write();
  tg_Vdac[0]->Write();
  tg_Vdac[1]->Write();
  tg_Vdac_resi[0]->Write();
  tg_Vdac_resi[1]->Write();
  tg_Vbuf[0]->Write();
  tg_Vbuf[1]->Write();
  tg_Vbuf_resi[0]->Write();
  tg_Vbuf_resi[1]->Write();
  tg_ped_DAC[0]->Write();
  tg_ped_DAC[1]->Write();
  tg_pulse[0]->Write();
  tg_pulse[1]->Write();
  hmod[0][0]->Write();
  hmod[0][1]->Write();
  hmod[0][2]->Write();
  hmod[0][3]->Write();
  hmod[1][0]->Write();
  hmod[1][1]->Write();
  hmod[1][2]->Write();
  hmod[1][3]->Write();
  for(int iinj=0; iinj<n_calib; iinj++)
  {
    tp_inj1[0][iinj]->Write();
    tp_inj1[1][iinj]->Write();
  }
  tg_inj1[0]->Write();
  tg_inj1[1]->Write();
  tg_inj2[0]->Write();
  tg_inj2[1]->Write();
  tg_inj3[0]->Write();
  tg_inj3[1]->Write();
  tp_calib[0][0]->Write();
  tp_calib[0][1]->Write();
  tp_calib[1][0]->Write();
  tp_calib[1][1]->Write();
  tg_calib[0]->Write();
  tg_calib[1]->Write();
  hcalib_t0->Write();
  hcalib_rt->Write();
  fd->Close();

  sprintf(output_file,"data/CATIA_calib/CATIA_%4.4d_calib.txt",CATIA_number);
  FILE *fout=fopen(output_file,"w+");
  fprintf(fout,"# Temperature FPGA CATIA_X1 CATIA_X5\n");
  fprintf(fout,"%e %e %e\n",Temp[0],Temp[1],Temp[2]);
  fprintf(fout,"# Vref DAC values\n");
  fprintf(fout,"%d\n",n_Vref);
  for(Int_t i=0; i<n_Vref; i++) fprintf(fout,"%e ",Vref[i]*1.33); // 10k+3.3k resistor bridge
  fprintf(fout,"\n");
  fprintf(fout,"# Iinj DAC values : in, DAC1, DAC2\n");
  fprintf(fout,"%d\n",n_Vdac);
  for(Int_t i=0; i<n_Vdac; i++) fprintf(fout,"%e ",DAC_val[i]);
  fprintf(fout,"\n");
  for(Int_t i=0; i<n_Vdac; i++) fprintf(fout,"%e ",Vdac[0][i]);
  fprintf(fout,"\n");
  for(Int_t i=0; i<n_Vdac; i++) fprintf(fout,"%e ",Vdac[1][i]);
  fprintf(fout,"\n");
  fprintf(fout,"# Iinj DAC : slope DAC1 DAC2, offset DAC1 DAC2\n");
  fprintf(fout,"%e %e %e %e\n",Vdac_slope[0],Vdac_slope[1],Vdac_offset[0],Vdac_offset[1]);
  fprintf(fout,"# PED DAC values : G1, G10\n");
  fprintf(fout,"%d\n",n_PED_dac);
  for(Int_t i=0; i<n_PED_dac; i++)
  {
    Double_t x,y;
    tg_ped_DAC[0]->GetPoint(i,x,y);
    fprintf(fout,"%e ",y);
  }
  fprintf(fout,"\n");
  for(Int_t i=0; i<n_PED_dac; i++)
  {
    Double_t x,y;
    tg_ped_DAC[1]->GetPoint(i,x,y);
    fprintf(fout,"%e ",y);
  }
  fprintf(fout,"\n");
  fprintf(fout,"# PED DAC parameters : G1_ped[0], G1_ped_slope, G10_ped[0], G10_ped_slope\n");
  fprintf(fout,"%e %e %e %e\n",ped_DAC_par[0][0],ped_DAC_par[0][1],ped_DAC_par[1][0],ped_DAC_par[1][1]);
  fprintf(fout,"# Noise : G1-R500-LPF50, G1-R500-LPF35 G1-R400-LPF50 G1-R400-LPF35 G10-R500-LPF50, G10-R500-LPF35 G10-R400-LPF50 G10-R400-LPF35\n");
  fprintf(fout,"%e %e %e %e %e %e %e %e\n",G1_noise[0][0], G1_noise[0][1], G1_noise[1][0], G1_noise[1][1],
                                           G10_noise[0][0],G10_noise[0][1],G10_noise[1][0],G10_noise[1][1]);
  fprintf(fout,"# INL : G1-min G1-max G10-min G10-max\n");
  fprintf(fout,"%e %e %e %e\n",INL_min[0],INL_max[0],INL_min[1],INL_max[1]);
  fprintf(fout,"# CATIA gains : G1-R500 G1-R400 G10-R500 G10-R400 (LPF35)\n");
  fprintf(fout,"%e %e %e %e\n",CATIA_gain[0][0],CATIA_gain[0][1],CATIA_gain[1][0],CATIA_gain[1][1]);
  fprintf(fout,"# CATIA parameters : G10 R500 R400 Rconv_G1 Rconv_G10\n");
  fprintf(fout,"%e %e %e %e %e\n",gain_stage,TIA_gain[0],TIA_gain[1],Rconv[0],Rconv[1]);
  fprintf(fout,"# Timing with calib pulses : pos, jitter, rising_time, rising_time RMS\n");
  fprintf(fout,"%e %e %e %e\n",calib_pos,calib_jitter,RT,eRT);
  fprintf(fout,"# Vbuf DAC values :DAC1, DAC2\n");
  for(Int_t i=0; i<n_Vdac; i++) fprintf(fout,"%e ",Vbuf[0][i]);
  fprintf(fout,"\n");
  for(Int_t i=0; i<n_Vdac; i++) fprintf(fout,"%e ",Vbuf[1][i]);
  fprintf(fout,"\n");
  fprintf(fout,"# Vbuf DAC : slopes DAC1 DAC2, offset DAC1 DAC2\n");
  fprintf(fout,"%e %e %e %e\n",Vbuf_slope[0],Vbuf_slope[1],Vbuf_offset[0],Vbuf_offset[1]);
  fclose(fout);

}

