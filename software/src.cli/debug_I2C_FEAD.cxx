#include "uhal/uhal.hpp"

#include <vector>
#include <iostream>
#include <cstdlib>
#include <typeinfo>
#include "I2C_RW.h"

#define GENE_TRIGGER    (1<<0)
#define GENE_CALIB      (1<<1)
#define GENE_100HZ      (1<<2)
#define LED_ON          (1<<3)
#define CALIB_MODE      (1<<4)

#define RESET                  (1<<0)
#define TRIGGER_MODE           (1<<1)
#define SOFT_TRIGGER           (1<<2)
#define SELF_TRIGGER           (1<<3)
#define CLOCK_PHASE            (1<<4)
#define CLOCK_RESET            (1<<7)
#define SELF_TRIGGER_THRESHOLD (1<<8)
#define SELF_TRIGGER_MASK      (1<<22)
#define ADC_CALIB_MODE         (1<<27)
#define BOARD_SN               (1<<28)
#define SEQ_CLOCK_PHASE        (1<<0)
#define IO_CLOCK_PHASE         (1<<4)
#define REG_CLOCK_PHASE        (1<<8)
#define MEM_CLOCK_PHASE        (1<<12)

#define CAPTURE_START 1
#define CAPTURE_STOP  2
//#define SW_DAQ_DELAY 0x1800 // delay for laser with internal trigger
#define SW_DAQ_DELAY 0x0 // delay for laser with external trigger
#define HW_DAQ_DELAY 100 // Laser with external trigger
#define NSAMPLE_MAX 28672
#define MAX_PAYLOAD 1380
#define MAX_VFE     10
#define MAX_STEP    10000

#define DRP_WRb         (1<<31)
#define I2C_RWb         (1<<31)
#define I2C_LONG        (1<<30)
#define I2C_DEVICE      (1<<23)
#define I2C_REG         (1<<16)
#define ADC_WRITE       (1<<24)

#define DELAY_INCREASE  (1<<5)     // Increase ADC signal delay by one tip
#define DELAY_RESET     (1<<6)     // Reset ADC signal delays

using namespace uhal;

int main ( int argc,char* argv[] )
{
  unsigned int device_number, command, val;

  int debug=0;
  int n_vfe=0;
  int vfe[MAX_VFE];

// CATIA settings if reuested
  int n_catia=0;
  int CATIA_num[5]   ={-1,-1,-1,-1,-1}; // CATIA number to address
  int CATIA_nums     =-1; // CATIA numbers present on I2C bus (xyzuv) -1=no CATIA)
  int n_reg=0;
  int CATIA_reg[6]   ={-1,-1,-1,-1,-1,-1}; // CATIA number to address
  int CATIA_regs     =-1; // CATIA register to loop on (xyzuvw) -1=no reg)
  int Def_num        = 5; // CATIA number to write in
  int Def_data       = 2; // Data to write in CATIA register
  int Def_reg        = 1; // Register to write in with I2C protocol
  int n_loop         = 1; // Number of loop to check I2C protocol
  int stop_on_error  = 0;
  int pause_on_error = 0;
  int reset_on_error = 0;
  int delay          = 100;
  int I2C_rw         = 2; // 2*read+write for the test loops

  unsigned int reg_def[6][7] = {{0x00, 0x02, 0x00, 0x1087, 0xffff, 0xffff, 0x0f},
                                {0x00, 0x02, 0x00, 0x1087, 0xffff, 0xffff, 0x0f},
                                {0x00, 0x02, 0x00, 0x1087, 0xffff, 0xffff, 0x0f},
                                {0x00, 0x02, 0x00, 0x1087, 0xffff, 0xffff, 0x0f},
                                {0x00, 0x02, 0x00, 0x1087, 0xffff, 0xffff, 0x0f},
                                {0x00, 0x02, 0x00, 0x1087, 0xffff, 0xffff, 0x0f}};
  int error[6][7];

  for(int k=1; k<argc; k++) 
  {    
    if(strcmp(argv[k],"-vfe") == 0)
    {
      sscanf( argv[++k], "%d", &vfe[n_vfe++] );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_nums") == 0)
    {    
      sscanf( argv[++k], "%d", &CATIA_nums );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_regs") == 0)
    {    
      sscanf( argv[++k], "%d", &CATIA_regs );
      continue;
    }    
    else if(strcmp(argv[k],"-Def_num") == 0)
    {    
      sscanf( argv[++k], "%d", &Def_num );
      continue;
    }    
    else if(strcmp(argv[k],"-Def_data") == 0)
    {    
      sscanf( argv[++k], "%x", &Def_data );
      continue;
    }    
    else if(strcmp(argv[k],"-Def_reg") == 0)
    {    
      sscanf( argv[++k], "%d", &Def_reg );
      continue;
    }    
    else if(strcmp(argv[k],"-n_loop") == 0)
    {    
      sscanf( argv[++k], "%d", &n_loop );
      continue;
    }    
    else if(strcmp(argv[k],"-stop_on_error") == 0)
    {    
      sscanf( argv[++k], "%d", &stop_on_error );
      continue;
    }    
    else if(strcmp(argv[k],"-pause_on_error") == 0)
    {    
      sscanf( argv[++k], "%d", &pause_on_error );
      continue;
    }    
    else if(strcmp(argv[k],"-reset_on_error") == 0)
    {    
      sscanf( argv[++k], "%d", &reset_on_error );
      continue;
    }    
    else if(strcmp(argv[k],"-debug") == 0)
    {    
      sscanf( argv[++k], "%d", &debug );
      continue;
    }    
    else if(strcmp(argv[k],"-delay") == 0)
    {    
      sscanf( argv[++k], "%d", &delay );
      continue;
    }    
    else if(strcmp(argv[k],"-I2C_rw") == 0)
    {    
      sscanf( argv[++k], "%d", &I2C_rw );
      continue;
    }    
    else if(strcmp(argv[k],"-h") == 0)
    {
      printf("Start DAQ with : \n");
      printf("-vfe                      : FE_adapter board to access [5]\n");
      printf("-CATIA_nums               : CATIA present on I2C bus which will be read (xyzuv) [-1=no catia]\n");
      printf("-CATIA_regs               : CATIA registers which will be read (xyzuvw) (1..6) [2]\n");
      printf("-Def_num                  : Put Default data in CATIA number [5]\n");
      printf("-Def_reg                  : Put Default data in CATIA register (1 to 5) [2]\n");
      printf("                          : 1 : Slow Control reg [R/W]\n");
      printf("                          : 2 : SEU error counter reg [R]\n");
      printf("                          : 3 : TIA reg [R/W]\n");
      printf("                          : 4 : Inj DAC1 reg [R/W]\n");
      printf("                          : 5 : Inj DAC2 reg [R/W]\n");
      printf("                          : 6 : Inj Ctl reg [R/W]\n");
      printf("-Def_data                 : Default data to write [2]\n");
      printf("-I2C_rw                   : read (2), write (1) or write_read (3) data on I2C bus [2]\n");
      printf("-n_loop                   : number of R/W cycle [1]\n");
      printf("-stop_on_error            : Stop the R/W cycles when an error is detected [0]\n");
      printf("-delay                    : Number of us to wait between cycles [100]\n");
      printf("-debug                    : Print debugging infos [0]\n");

      exit(-1);
    }
    else
    {
      printf("---------------------------------------------------\n");
      printf("Option %s not defined !, stop here.\n",argv[k]);
      printf("---------------------------------------------------\n");
      exit(-99);
    }
  }
  if(n_vfe==0)
  {
    n_vfe=1;
    vfe[0]=5;
  }

  ConnectionManager manager ( "file://xml/FEAD/connection_file.xml" );
  std::vector<uhal::HwInterface> devices;
  int i_vfe;
  for(i_vfe=0; i_vfe<n_vfe; i_vfe++)
  {
    char fead_str[80];
    sprintf(fead_str,"fead.udp.%d",vfe[i_vfe]);
    devices.push_back(manager.getDevice( fead_str ));
  }

  printf("Start DAQ with %d cards : ",n_vfe);
  for(int i=0; i<n_vfe; i++)printf("%d, ",vfe[i]);
  printf("\n");
  if(Def_num>0)printf("Will write register %d of catia %d with data %x\n",Def_reg, Def_num, Def_data);

  n_catia=0;
  while(CATIA_nums>0)
  {
    CATIA_num[n_catia]=CATIA_nums%10;
    CATIA_nums/=10;
    for(int ireg=1; ireg<=6; ireg++)error[CATIA_num[n_catia]][ireg]=0;
    n_catia++;
  }
  n_reg=0;
  while(CATIA_regs>0)
  {
    CATIA_reg[n_reg]=CATIA_regs%10;
    CATIA_regs/=10;
    n_reg++;
  }

  printf("Will ");
  if((I2C_rw&2)>0)printf("read");
  if((I2C_rw&1)>0)printf("/write");
  printf(" %d catias : ",n_catia);
  for(int icatia=0; icatia<n_catia; icatia++) printf("%d ",CATIA_num[icatia]);
  printf("\n");
  printf("Will access %d registers : ",n_reg);
  for(int ireg=0; ireg<n_reg; ireg++) printf("%d ",CATIA_reg[ireg]);
  printf("\n");

// Init stage :
  i_vfe=0;
  ValWord<uint32_t> busy,reg;
  for(auto & hw : devices)
  {
  // Read FW version to check :
    reg = hw.getNode("FW_VER").read();
    hw.dispatch();
    printf("Firmware version      : %8.8x\n",reg.value());
    i_vfe++;
  }

  printf("Reset VFE board\n");
  for(auto & hw : devices)
  {
    hw.getNode("FEAD_CTRL").write(RESET*1);
    hw.getNode("FEAD_CTRL").write(RESET*0);
    hw.getNode("VFE_CTRL").write(0);
    hw.dispatch();
  }
  usleep(100);

// Program CATIA according to wishes :
// Setup default values for CATIA:
  unsigned int I2C_long=0;
  for(auto & hw : devices)
  {
    I2C_long=0;
    if(Def_reg==3 || Def_reg==4 || Def_reg==5)I2C_long=1;
    if(Def_num>0)
    {
      device_number=(Def_num<<3)&0x7F;
      device_number+=1000; // For CATIA access
// Put LVRB in continuous ADC mode, monitor sense+, and power calculation with sense+
      int ireg=Def_reg&0x7F; // control register
      command=Def_data&0xffff;
      //while(1){
      val=I2C_RW(hw, device_number, ireg, command,I2C_long, 3, debug);
      //usleep(100000);}

      if(Def_reg>0 && Def_reg<=6) reg_def[Def_num][Def_reg]=Def_data & 0xFFFF;
      printf("Value read : 0x%.8x, should be 0x%.8x\n",val, reg_def[Def_num][Def_reg]);
    }

    for(int iloop=0; iloop<n_loop; iloop++)
    {
      if((iloop%100)==0)printf("%d\n",iloop);
      for(int icatia=0; icatia<n_catia; icatia++)
      {
        for(int ireg=0; ireg<n_reg; ireg++)
        {
          I2C_long=0;
          int loc_reg=CATIA_reg[ireg];
          if(loc_reg==3 || loc_reg==4 || loc_reg==5)I2C_long=1;

          device_number=(CATIA_num[icatia]<<3)&0x7F;
          device_number+=1000; // For CATIA access
          val=I2C_RW(hw, device_number, loc_reg, reg_def[CATIA_num[icatia]][loc_reg],I2C_long, I2C_rw, debug);
 
          int loc_error=0;
          if((val&0xffff)!= reg_def[CATIA_num[icatia]][loc_reg])
          {
            loc_error=1;
            error[CATIA_num[icatia]][loc_reg]++;
          }
          if(debug>1 || loc_error==1)
          {
            printf("I2C : Read CATIA %d register %d.",CATIA_num[icatia], loc_reg);
            printf("Value read : 0x%.8x, should be 0x%.8x, error=%d\n",val, reg_def[CATIA_num[icatia]][loc_reg],loc_error);
          }
          if(loc_error==1)
          {
            ValWord<uint32_t> ack_lsb = hw.getNode("I2C_ACK_LSB").read();
            ValWord<uint32_t> ack_msb = hw.getNode("I2C_ACK_MSB").read();
            hw.dispatch();
            printf("%5d : ACK spy = 0x%.8x %.8x : 0b",iloop,ack_msb.value(), ack_lsb.value());
            unsigned int loc_ack;
            loc_ack=ack_msb.value();
            for(int i=0; i<32; i++)
            {
              if((loc_ack&0x80000000)>0) 
                printf("1");
              else
                printf("0");
              if((i%4)==3) printf(" ");
              loc_ack<<=1;
            }
            loc_ack=ack_lsb.value();
            for(int i=0; i<32; i++)
            {
              if((loc_ack&0x80000000)>0) 
                printf("1");
              else
                printf("0");
              if((i%4)==3) printf(" ");
              loc_ack<<=1;
            }
            printf("\n");
          }
          if(loc_error==1 && loc_reg==2)reg_def[CATIA_num[icatia]][loc_reg]=val&0xffff;
          if(loc_error==1 && reset_on_error==1)
          {
            if(pause_on_error==1)
            {
              system("stty raw");
              char cdum=getchar();
              system("stty -raw");
              if (cdum=='q' || cdum=='Q') exit(-1);
            }

            printf("Reset VFE board\n");
            for(auto & hw_loc : devices)
            {
              hw_loc.getNode("FEAD_CTRL").write(RESET*1);
              hw_loc.getNode("FEAD_CTRL").write(RESET*0);
              hw_loc.dispatch();
            }
            usleep(100);
            I2C_long=0;
            if(Def_reg==3 || Def_reg==4 || Def_reg==5)I2C_long=1;
            if(Def_num>0)
            {
              device_number=(Def_num<<3)&0x7F;
              device_number+=1000; // For CATIA access
              val=I2C_RW(hw, device_number, Def_reg, Def_data,I2C_long, 1, debug);
            }
          }
          if(loc_error==1 && stop_on_error==1)
          {
            printf("Value read : 0x%.8x, should be 0x%.8x, error=%d\n",reg.value(), reg_def[CATIA_num[icatia]][loc_reg],loc_error);
            exit(-1);
          }
          usleep(delay);
        }
      }
    }
  }
  for(auto & hw : devices)
  {
    for(int icatia=1; icatia<=5; icatia++)
    {
      int loc_reg=2;
      device_number=1000+((icatia<<3)&0x7F);
      val=I2C_RW(hw, device_number, loc_reg, 0, 0, 2, debug);
      printf("Catia %d : Reg 2 : ",icatia);
      printf("%d ",val&0xffff);
      printf("\n");
    }
  }
  printf("End of I2C test :\n");
  int tot_error=0;
  for(int icatia=0; icatia<n_catia; icatia++)
  {
    printf("Catia %d : ",CATIA_num[icatia]);
    for(int ireg=1; ireg<=6; ireg++)
    {
      printf("%.6d ",error[CATIA_num[icatia]][ireg]);
      tot_error+=error[CATIA_num[icatia]][ireg];
    }
    printf("\n");
  }
  printf("Total number of errors : %d\n",tot_error);
}
