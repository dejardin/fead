#include "uhal/uhal.hpp"

#include <vector>
#include <iostream>
#include <cstdlib>
#include <typeinfo>

#include "I2C_RW.h"
#include <TApplication.h>
#include <TProfile.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TProfile.h>
#include <TVirtualFFT.h>
#include <TF1.h>
#include <TH1D.h>
#include <TTree.h>
#include <TFile.h>

#define N_CATIA                5

#define GENE_TRIGGER           (1<<0)
#define TP_TRIGGER             (1<<1)
#define GENE_100HZ             (1<<2)
#define LED_ON                 (1<<3)
#define G1_CALIB_TRIGGER       (1<<4)
#define G10_CALIB_TRIGGER      (1<<5)
#define INVERT_FE_TRIGGER      (1<<6)

#define PWUP_RESET             (1<<31)
#define RESET                  (1<<0)
#define TRIGGER_MODE           (1<<1)
#define SOFT_TRIGGER           (1<<2)
#define SELF_TRIGGER           (1<<3)
#define CLOCK_PHASE            (1<<4)
#define CLOCK_RESET            (1<<7)
#define SELF_TRIGGER_THRESHOLD (1<<8)
#define SELF_TRIGGER_MASK      (1<<22)
#define ADC_CALIB_MODE         (1<<27)
#define SEL_CALIB_PULSE        (1<<28)
#define BOARD_SN               (1<<28)
#define SEQ_CLOCK_PHASE        (1<<0)
#define IO_CLOCK_PHASE         (1<<4)
#define REG_CLOCK_PHASE        (1<<8)
#define MEM_CLOCK_PHASE        (1<<12)
#define LVRB_AUTOSCAN          (1<<2)

#define CAPTURE_START 1
#define CAPTURE_STOP  2
#define SW_DAQ_DELAY 0x0 // delay for laser with external trigger
#define HW_DAQ_DELAY 100 // Laser with external trigger
#define NSAMPLE_MAX 28670
#define MAX_PAYLOAD 1380
#define MAX_VFE     10
#define MAX_STEP    10000

#define I2C_RWb         (1<<31)
#define I2C_LONG        (1<<30)
#define SPI_CATIA       (1<<26)
#define I2C_CATIA       (1<<25)
#define ADC_WRITE       (1<<24)
#define DAC_FULL_RESET  (0xF<<16)
#define DAC_SOFT_RESET  (0x7<<16)
#define DAC_VAL_REG     (0x3<<16)
#define DAC_CTRL_REG    (0x4<<16)

#define ADC_OMODE_REG   (0x14<<16) // ADC register to define Output mode of the ADC
#define ADC_ISPAN_REG   (0x18<<16) // ADC register to define input span of the ADC from 1.383V to 2.087V
#define ADC_CLK_DIVIDE  (0x0B<<16) // ADC register to define clock division of the ADC
#define ADC_OTEST_REG   (0x0D<<16) // ADC register to define test mode of the ADC

#define DELAY_INCREASE  (1<<5)     // Increase ADC signal delay by one tip
#define DELAY_RESET     (1<<6)     // Reset ADC signal delays

#define DRP_WRb         (1<<31)    // Acces to FGPA internal ADCs

using namespace uhal;

int get_event(uhal::HwInterface hw, int trigger_type, int nsample, ValVector< uint32_t >mem, short int *event[N_CATIA], double *fevent[N_CATIA], int debug, int draw)
{
  static int old_address=-1, ievt=0;
  static TGraph *tg[N_CATIA]={NULL, NULL, NULL, NULL, NULL};
  static TCanvas *cdebug=NULL;
  static int trigger_type_old=-1;
  int n_word;
  int n_transfer_max=249; // 3*32 bits words per sample to get the 5 channels data
  int n_transfer;
  int n_last;
  int max_address= 28672;
  int error      = 0;
  int command    = 0;

  if(trigger_type_old!=trigger_type)ievt=0;
  if(cdebug==NULL)
  {
    for(Int_t ich=0; ich<N_CATIA; ich++)
    {
      tg[ich]=new TGraph();
    }
    cdebug=new TCanvas("cdebug","cdebug",60,0,1920-60,400);
    cdebug->Divide(N_CATIA,1);
  }

  ValWord<uint32_t> address,free_mem;
  address = hw.getNode("CAP_ADDRESS").read();
  hw.dispatch();
  if(debug>0)printf("Starting with address : %8.8x, trigger_type : %d\n",address.value(),trigger_type);
  old_address=address.value()>>16;
  if(old_address==max_address-1)old_address=-1;

  ValVector< uint32_t > block;
  if(trigger_type !=2)
  {
    if(trigger_type == 0)
      command = G10_CALIB_TRIGGER*0 | G1_CALIB_TRIGGER*0 | LED_ON*0 | GENE_100HZ*0 | TP_TRIGGER*0 | GENE_TRIGGER*1;
    else if(trigger_type == 1)
      command = G10_CALIB_TRIGGER*0 | G1_CALIB_TRIGGER*0 | LED_ON*0 | GENE_100HZ*0 | TP_TRIGGER*1 | GENE_TRIGGER*0;
    else if(trigger_type == 3)
      command = G10_CALIB_TRIGGER*0 | G1_CALIB_TRIGGER*1 | LED_ON*0 | GENE_100HZ*0 | TP_TRIGGER*0 | GENE_TRIGGER*1;
    else if(trigger_type == 4)
      command = G10_CALIB_TRIGGER*1 | G1_CALIB_TRIGGER*0 | LED_ON*0 | GENE_100HZ*0 | TP_TRIGGER*0 | GENE_TRIGGER*1;

    if(debug>0)printf("Send trigger with command : 0x%8.8x\n",command);
// Read base address and send trigger
    free_mem = hw.getNode("CAP_FREE").read();
    hw.getNode("FW_VER").write(command);
    hw.dispatch();
    if(debug>0)printf("Free memory : 0x%8.8x\n",free_mem.value());
// Read new address and wait for DAQ completion
    address = hw.getNode("CAP_ADDRESS").read();
    hw.dispatch();
    if(debug>0)printf("Start address : 0x%8.8x\n",address.value());
    int new_address=address.value()>>16;
    int loc_address=new_address;
    int nretry=0;
    while(((loc_address-old_address)%max_address) != nsample+1 && nretry<100)
    {
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      new_address=address.value()>>16;
      loc_address=new_address;
      if(new_address<old_address)loc_address+=max_address;
      nretry++;
      if(debug>0)printf("ongoing R/W addresses    : old %d, new %d add 0x%8.8x\n", old_address, new_address,address.value());
    }
    if(nretry==100)
    {
      printf("Stop waiting for sample capture after %d retries\n",nretry);
      printf("R/W addresses    : old %8.8x, new %8.8x add 0x%8.8x\n", old_address, new_address, address.value());
      error=1;
    }
    old_address=new_address;
    if(old_address==max_address-1)old_address=-1;
    if(debug>0)printf("R/W addresses    : old %8.8x, new %8.8x add 0x%8.8x\n", old_address, new_address, address.value());
  }
  else
  {
    int nretry=0;
// Wait for external trigger to fill memory :
    while((free_mem.value()==max_address-1) && nretry<100)
    {
      free_mem = hw.getNode("CAP_FREE").read();
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      //printf("address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
      if(debug>0)printf("address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
      usleep(100);
      nretry++;
    }
    if(nretry==100)
    {
      printf("Stop waiting for external trigger after %d retries\n",nretry);
      printf("R/W addresses    : add 0x%8.8x\n",address.value());
      error=1;
    }
  }
  mem.clear();

// Read event samples from FPGA
  n_word=(nsample+1)*3;     // 3*32 bits words per sample to get the 5 channels data n G10
  n_transfer=n_word/(MAX_PAYLOAD/4); // Max ethernet packet = 1536 bytes, max user payload = 1380 bytes
  n_last=n_word-n_transfer*(MAX_PAYLOAD/4);
  if(n_transfer>n_transfer_max)
  {
    printf("Error : ask for too many samples. Number of transfer %d greater than allowed max value %d\n",n_transfer, n_transfer_max);
    error=1;
  }
  for(int itrans=0; itrans<n_transfer; itrans++)
  {
    block = hw.getNode ("CAP_DATA").readBlock(MAX_PAYLOAD/4);
    hw.dispatch();
    for(int is=0; is<MAX_PAYLOAD/4; is++)mem.push_back(block[is]);
  }
  block = hw.getNode ("CAP_DATA").readBlock(n_last);
  address = hw.getNode("CAP_ADDRESS").read();
  free_mem = hw.getNode("CAP_FREE").read();
  hw.dispatch();
  if(debug>0)printf("After reading address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
  for(int is=0; is<n_last; is++)mem.push_back(block[is]);
  mem.valid(true);

  if(debug>0)
  {
    printf("%8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",
        mem[0],mem[1],mem[2],mem[3],mem[4],mem[5],mem[6],mem[7],mem[8],mem[9]);
    printf("%8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",
        mem[10],mem[11],mem[12],mem[13],mem[14],mem[15],mem[16],mem[17],mem[18],mem[19]);
    printf("%8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",
        mem[20],mem[21],mem[22],mem[23],mem[24],mem[25],mem[26],mem[27],mem[28],mem[29]);
    printf("%.4d %.4d %.4d %.4d %.4d %.4d %.4d %.4d\n",
        mem[2]&0x3fff,mem[3]&0x3fff,mem[4]&0x3fff,mem[5]&0x3fff,mem[6]&0x3fff,mem[7]&0x3fff,mem[8]&0x3fff,mem[9]&0x3fff);
    printf("%.4d %.4d %.4d %.4d %.4d %.4d %.4d %.4d\n",
        (mem[2]>>14)&0x3ff,(mem[3]>>14)&0x3fff,(mem[4]>>14)&0x3fff,(mem[5]>>14)&0x3fff,
        (mem[6]>>14)&0x3ff,(mem[7]>>14)&0x3fff,(mem[8]>>14)&0x3fff,(mem[9]>>14)&0x3fff);
  }

  unsigned long int t1=0, t2=0, t3=0, t4=0, t5=0;
// First sample should have bit 70 at 1
  if((mem[0] >> 31) != 1)
  {
    printf("Sample 0 not a header : %8.8x\n",mem[0]);
    error=1;
  }
  t1= mem[0]     &0xFFFF;
  t2= mem[1]     &0xFFFF;
  t3=(mem[1]>>16)&0xFFFF;
  t4= mem[2]     &0xFFFF;
  t5=(mem[2]>>16)&0x00FF;
  unsigned long int timestamp=(t5<<56)+(t4<<42)+(t3<<28)+(t2<<14)+t1;

  if(debug>0)
  { 
    printf("timestamp : %8.8x %8.8x %8.8x\n",mem[2],mem[1],mem[0]);
    printf("timestamp : %ld %4.4lx %4.4lx %4.4lx %4.4lx %4.4lx\n",timestamp,t5,t4,t3,t2,t1);
  }

  Double_t dv=1750./16384.; // 14 bits on 1.75V
  for(int isample=0; isample<nsample; isample++)
  {
    int j;
    j=(isample+1)*3;
    event[0][isample]= mem[j]       &0x3FFF;
    event[1][isample]= mem[j+1]     &0x3FFF;
    event[2][isample]=(mem[j+1]>>16)&0x3FFF;
    event[3][isample]= mem[j+2]     &0x3FFF;
    event[4][isample]=(mem[j+2]>>16)&0x3FFF;
    fevent[0][isample]=double(event[0][isample]);
    fevent[1][isample]=double(event[1][isample]);
    fevent[2][isample]=double(event[2][isample]);
    fevent[3][isample]=double(event[3][isample]);
    fevent[4][isample]=double(event[4][isample]);
  }

  if(debug>0 || trigger_type !=trigger_type_old || draw==1)
  {
    for(int ich=0; ich<N_CATIA; ich++)
    {
      tg[ich]->Clear();
      tg[ich]->Set(0);
      for(int isample=0; isample<nsample; isample++)
      {
        tg[ich]->SetPoint(isample,6.25*isample,fevent[ich][isample]);
      }
      cdebug->cd(N_CATIA-ich);
      tg[ich]->SetLineColor(kRed);
      tg[ich]->Draw("alp");
    }
    cdebug->Update();
  }
  if(debug>0)
  {
    system("stty raw");
    char cdum=getchar();
    system("stty -raw");
    if(cdum=='q')exit(-1);
  }

  trigger_type_old=trigger_type;
  ievt++;
  if(error==0)
    return timestamp;
  else
    return -1;
}

Int_t main ( Int_t argc,char* argv[] )
{
  Double_t NSPS=160.e6;
  Int_t timestamp;
  Int_t command, vice_ctrl;
  ValWord<uint32_t> address;
  ValWord<uint32_t> data[3*NSAMPLE_MAX];
  ValVector< uint32_t > mem;
  short int *event[N_CATIA];
  double *fevent[N_CATIA];
  for(Int_t ich=0; ich<N_CATIA; ich++)
  {
    event[ich]=(short int*)malloc(NSAMPLE_MAX*sizeof(short int));
    fevent[ich]=(double*)malloc(NSAMPLE_MAX*sizeof(double));
  }
  double dv=1750./16384.; // 14 bits on 1.75V
  Int_t debug=-1, draw=0;
  double ped[N_CATIA], mean[N_CATIA], rms[N_CATIA];
  Double_t RTIA[2]={500.,400.};
  Double_t Rshunt_V1P2=0.255;
  Double_t Rshunt_V2P5=0.05;
  char output_file[256];
// Define defaults for laser runs :
  Int_t nevent                = 0;
  Int_t nsample               =-1;
  Int_t trigger_type          = 0; // pedestal by default
  Int_t self_trigger          = 0; // No self trigger 
  Int_t self_trigger_threshold= 0;
  Int_t self_trigger_mask     = 0; // don't trig on any channels amplitude
  Int_t ADC_calib_mode        = 0; // Put (1) or not (0) the outputs of CATIA in ADC_calib_mode (outputs near VCM)
  Int_t seq_clock_phase       = 0; // sequence clock phase by steps of 45 degrees
  //Int_t IO_clock_phase        = 4; // Capture clock phase by steps of 45 degrees for CATIA test board
  Int_t IO_clock_phase        = 5; // Capture clock phase by steps of 45 degrees for CATIA test board
  Int_t reg_clock_phase       = 0; // Shift register clock phase by steps of 45 degrees
  Int_t mem_clock_phase       = 0; // memory clock phase by steps of 45 degrees
  Int_t clock_phase           = 0;
  Int_t sel_calib_pulse       = 1; // Default calib pulse enable. Otherwise High_Z
  Int_t n_calib               = 32;
  Int_t calib_gain            = 0;
  Int_t calib_step            = 128;
  Int_t calib_level           = 0;
  Int_t calib_width           = 200;
  Int_t calib_delay           = 50;
  Int_t tune_delay            = 1;
  //Int_t delay_val[N_CATIA]    = {15,15,13,12,10}; // Number of delay taps to get a stable R/O with ADCs (160 MHz)
  Int_t delay_val[N_CATIA]    = {20,22,15,15,14}; // Number of delay taps to get a stable R/O with ADCs (160 MHz)
  Int_t CATIA_number          = 9999;

// ADC settings if any :
  UInt_t output_test = 0;  // Put ADC outputs in test mode (0=normal conversion values, 0xF=ramp)
  UInt_t input_span  = 0;
  UInt_t signed_data = 0;
  UInt_t negate_data = 0;

// CATIA settings if requested
  Int_t CATIA_num[N_CATIA]={1,2,3,4,5}; // CATIA numbers to address
  Int_t average        = 256;
  Int_t XADC_reg, XADC_Temp=0x14, XADC_Vdac[5]={0x17, 0x1e, 0x1d, 0x1c, 0x1f};

  Int_t n_Vdac=65, n_PED_dac=32;

  TF1 *f1, *fDAC[N_CATIA][2];
  sprintf(output_file,"");

  for(int k=1; k<argc; k++) 
  {    
    if(strcmp( argv[k], "-debug") == 0)
    {    
      sscanf( argv[++k], "%d", &debug );
      continue;
    }    
    else if(strcmp(argv[k],"-num") == 0)
    {
      sscanf( argv[++k], "%d", &CATIA_number );
      continue;
    }
    else if(strcmp(argv[k],"-h") == 0)
    {
      printf("Start DAQ with : \n");
      printf("-debug dbg_level            : Set debug level for this session [0]\n");
      printf("-num CATIA_number           : CATIA reference to identify the output file (rev*1000+i) [9999]\n");
      exit(-1);
    }
    else
    {
      printf("---------------------------------------------------\n");
      printf("Option %s not defined !, stop here.\n",argv[k]);
      printf("---------------------------------------------------\n");
      exit(-99);
    }
  }

  int loc_argc=1;
  char *loc_argv[10];
  for(int i=0; i<10; i++)loc_argv[i]=(char *)malloc(132*sizeof(char));
  sprintf(loc_argv[0],"test");
  TApplication *Root_App=new TApplication("test", &loc_argc, loc_argv);

  struct timeval tv; 
  gettimeofday(&tv,NULL);
  printf(" Start VFE calibration at %ld.%6.6ld s\n",tv.tv_sec,tv.tv_usec);

  char hname[80];

  ConnectionManager manager ( "file://xml/VICE/connection_file.xml" );
  uhal::HwInterface hw=manager.getDevice( "vice.udp.3" );

  unsigned int device_number,val;
  ValWord<uint32_t> free_mem, trig_reg, delays, reg, current, fault;

// Read FW version to check :
  reg = hw.getNode("FW_VER").read();
  hw.getNode("VICE_CTRL").write( RESET*1);
  hw.getNode("VICE_CTRL").write( RESET*0);
  hw.dispatch();
  printf("Reset board with firmware version      : %8.8x\n",reg.value());

// Initialize current monitors
  for(int iLVR=0; iLVR<2; iLVR++)
  {
    printf("\nProgram LV sensor %d : ",iLVR);
    if(iLVR==0)
    {
      device_number=0x67;
      printf("V2P5\n");
    }
    else
    {
      device_number=0x6C;
      printf("V1P2\n");
    }
// Put LVRB in continuous ADC mode, monitor sense+, and power calculation with sense+
    int ireg=0; // control register
    command=0x5;
    val=I2C_RW(hw, device_number, ireg, command,0, 3, debug);
    printf("New content of control register : 0x%x\n",val&0xffff);
// Max power limit
    val=I2C_RW(hw, device_number, 0xe,  0xff,0, 1, debug);
    val=I2C_RW(hw, device_number, 0xf,  0xff,0, 1, debug);
    val=I2C_RW(hw, device_number, 0x10, 0xff,0, 1, debug);
// Min power limit
    val=I2C_RW(hw, device_number, 0x11, 0x00,0, 1, debug);
    val=I2C_RW(hw, device_number, 0x12, 0x00,0, 1, debug);
    val=I2C_RW(hw, device_number, 0x13, 0x00,0, 1, debug);
// Max ADin limit
    val=I2C_RW(hw, device_number, 0x2E, 0xFFF0,1, 3, debug);
// Min ADin limit
    val=I2C_RW(hw, device_number, 0x30, 0x0000,1, 3, debug);
// Max Vin limit
    val=I2C_RW(hw, device_number, 0x24, 0xFFF0,1, 3, debug);
// Min Vin limit
    val=I2C_RW(hw, device_number, 0x26, 0x0000,1, 3, debug);
// Max current limit
    val=I2C_RW(hw, device_number, 0x1A, 0xFFF0,1, 3, debug);
    printf("New max threshold on current 0x%x\n",val&0xffff);
// Min current limit
    val=I2C_RW(hw, device_number, 0x1C, 0x0000,1, 3, debug);
    printf("New min threshold on current 0x%x\n",val&0xffff);
// Read and clear Fault register :
    ireg=4; // Alert register
    val=I2C_RW(hw, device_number, ireg, command,0, 2, debug);
    printf("Alert register content : 0x%x\n",val&0xffff);
    val=I2C_RW(hw, device_number, ireg, command,0, 2, debug);
    printf("Alert register content : 0x%x\n",val&0xffff);
// Set Alert register
    ireg=1;
    command=0x20; // Alert on Dsense overflow
    val=I2C_RW(hw, device_number, ireg, command,0, 3, debug);
    printf("New alert bit pattern : 0x%x\n", val&0xffff);
  }
  printf("\n");
  printf("Redo pwup_reset and Start LVRB auto scan mode\n");
  hw.getNode("VFE_CTRL").write(PWUP_RESET*1);
  hw.getNode("VFE_CTRL").write(0);
  hw.getNode("VFE_CTRL").write(LVRB_AUTOSCAN*1);
  hw.dispatch();

// Wait for voltage stabilization
  usleep(2000000);

// Read voltages measured by controler
  for(int iLVR=0; iLVR<2; iLVR++)
  {
    char alim[80];
    if(iLVR==0)
    {
      device_number=0x67;
      sprintf(alim,"V2P5");
    }
    else
    {
      device_number=0x6C;
      sprintf(alim,"V1P2");
    }
    val=I2C_RW(hw, device_number, 0x1E, 0,1, 2, 0);
    double Vmeas=((val>>4)&0xfff);
    Vmeas=Vmeas/4096.*102.4; // Convert to V (102.4 V full scale)
    printf("%s measured power supply voltage %7.3f V\n",alim,Vmeas);
    val=I2C_RW(hw, device_number, 0x28, 0,1, 2, 0);
    Vmeas=((val>>4)&0xfff);
    Vmeas=Vmeas/4096.*2.048; // Convert to V (2.048 V full scale)
    if(iLVR==0)Vmeas*=2.;
    printf("%s measured sense voltage %7.3f V\n",alim,Vmeas);
  }

// Setup ADCs
  printf("Program_ADCs...\n");
// Put ADC in two's complement mode (if no pedestal bias) and invert de conversion result
  negate_data=(negate_data&1)<<2;
  signed_data&=3;
  command=ADC_WRITE | ADC_OMODE_REG | negate_data | signed_data;
  printf("Put ADC coding : 0x%x\n",command);
  hw.getNode("DTU_CTRL").write(command);
  hw.dispatch();

// Set ADC input range (default=1.75V)
  input_span&=0x1F;
  command=ADC_WRITE | ADC_ISPAN_REG | input_span;
  printf("Set ADC input span range : 0x%x\n",command);
  hw.getNode("DTU_CTRL").write(command);
  hw.dispatch();

// Set ADC output test mode
  output_test&=0x0F;
  command=ADC_WRITE | ADC_OTEST_REG | output_test;
  printf("Set ADC output test mode : 0x%x\n",command);
  hw.getNode("DTU_CTRL").write(command);
  hw.dispatch();

// Set delay tap value for each ADC
  for(int i=0; i<N_CATIA; i++)
  {
    command=DELAY_RESET*1;
    hw.getNode("DELAY_CTRL").write(command);
    hw.dispatch();
    usleep(1000);
  }
  reg = hw.getNode("VICE_CTRL").read();
  hw.dispatch();
  printf("VICE_CTRL register content : 0x%8.8x\n",reg.value());
  printf("Get lock status of IDELAY input stages\n");
  reg = hw.getNode("DELAY_CTRL").read();
  hw.dispatch();
  printf("Delay lock value : 0x%x\n",reg.value());

  for(int iADC=0; iADC<N_CATIA; iADC++)
  {
    command=DELAY_RESET*0 | DELAY_INCREASE*1 | (1<<iADC);
    for(Int_t itap=0; itap<delay_val[iADC]; itap++)
    {
      hw.getNode("DELAY_CTRL").write(command);
      hw.dispatch();
    }
    printf("Get lock status of IDELAY input stages\n");
    reg = hw.getNode("DELAY_CTRL").read();
    hw.dispatch();
    printf("Value read : 0x%x\n",reg.value());
  }
// Set the clock phases
  command = MEM_CLOCK_PHASE*mem_clock_phase+REG_CLOCK_PHASE*reg_clock_phase+IO_CLOCK_PHASE*IO_clock_phase+SEQ_CLOCK_PHASE*seq_clock_phase;
  printf("Set clock phases : 0x%x\n",command);
  hw.getNode("CLK_SETTING").write(command);
// Add laser latency before catching data ~ 40 us
  hw.getNode("TRIG_DELAY").write((SW_DAQ_DELAY<<16)+HW_DAQ_DELAY);
  hw.dispatch();

// Read Temperature and others analog voltages with FPGA ADC
// Read XADC register 0x40 and set the requested average to 1 in XADC
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  ValWord<uint32_t> ave  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  unsigned loc_ave=ave.value()&0xffff;
  printf("Old config register 0x40 content : %x\n",loc_ave);

  loc_ave=0x8000;
  command=DRP_WRb*1 | (0x40<<16) | loc_ave;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  ave  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  loc_ave=ave.value()&0xffff;
  printf("New config register 0x40 content : %x\n",loc_ave);
  command=DRP_WRb*1 | (0x42<<16) | 0x0400;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();
// Switch off temp output on CATIA :
  for(int ich=0; ich<N_CATIA; ich++)
  {
    device_number=1000 + (CATIA_num[ich]<<3);
    val=I2C_RW(hw, device_number, 1, 0x2,0, 3, debug);
  }

//===========================================================================================================
// 0 : Get Vdac vs DAC transfer function for current injection 
// Now, make the temp measurement on all catias, sequentially end for both current gain
  TGraph *tg_Vdac[N_CATIA][2], *tg_Vdac_resi[N_CATIA][2];
  for(int ich=0; ich<N_CATIA; ich++)
  {
    sprintf(hname,"Vdac1_vs_reg_catia_%d",ich);
    tg_Vdac[ich][0] =new TGraph();
    tg_Vdac[ich][0]->SetLineColor(kBlue);
    tg_Vdac[ich][0]->SetLineWidth(2.);
    tg_Vdac[ich][0]->SetMarkerColor(kBlue);
    tg_Vdac[ich][0]->SetMarkerStyle(20);
    tg_Vdac[ich][0]->SetMarkerSize(1.);
    tg_Vdac[ich][0]->SetName(hname);
    tg_Vdac[ich][0]->SetTitle(hname);
    sprintf(hname,"Vdac2_vs_reg_catia_%d",ich);
    tg_Vdac[ich][1] =new TGraph();
    tg_Vdac[ich][1]->SetLineColor(kBlue);
    tg_Vdac[ich][1]->SetLineWidth(2.);
    tg_Vdac[ich][1]->SetMarkerColor(kBlue);
    tg_Vdac[ich][1]->SetMarkerStyle(20);
    tg_Vdac[ich][1]->SetMarkerSize(1.);
    tg_Vdac[ich][1]->SetName(hname);
    tg_Vdac[ich][1]->SetTitle(hname);
    sprintf(hname,"Vdac1_resi_vs_reg_catia_%d",ich);
    tg_Vdac_resi[ich][0] =new TGraph();
    tg_Vdac_resi[ich][0]->SetLineColor(kBlue);
    tg_Vdac_resi[ich][0]->SetLineWidth(2.);
    tg_Vdac_resi[ich][0]->SetMarkerColor(kBlue);
    tg_Vdac_resi[ich][0]->SetMarkerStyle(20);
    tg_Vdac_resi[ich][0]->SetMarkerSize(1.);
    tg_Vdac_resi[ich][0]->SetName(hname);
    tg_Vdac_resi[ich][0]->SetTitle(hname);
    sprintf(hname,"Vdac2_resi_vs_reg_catia_%d",ich);
    tg_Vdac_resi[ich][1] =new TGraph();
    tg_Vdac_resi[ich][1]->SetLineColor(kBlue);
    tg_Vdac_resi[ich][1]->SetLineWidth(2.);
    tg_Vdac_resi[ich][1]->SetMarkerColor(kBlue);
    tg_Vdac_resi[ich][1]->SetMarkerStyle(20);
    tg_Vdac_resi[ich][1]->SetMarkerSize(1.);
    tg_Vdac_resi[ich][1]->SetName(hname);
    tg_Vdac_resi[ich][1]->SetTitle(hname);
  }

  Double_t DAC_val[n_Vdac], Vdac1[N_CATIA][n_Vdac], Vdac2[N_CATIA][n_Vdac], Vdac_slope[N_CATIA][2], Temp[1+N_CATIA*2];
  for(Int_t ich=0; ich<N_CATIA; ich++)
  {
    device_number=1000 + (CATIA_num[ich]<<3);
    for(int XADC_meas=0; XADC_meas<4; XADC_meas++)
    {
      if(XADC_meas==0 && ich>0) continue;      // Read FPGA temperature only once
      XADC_reg=0;                              // FPGA temperature
      if(XADC_meas==1)XADC_reg=XADC_Temp;      // CATIA Temperature measurement
      if(XADC_meas==2)XADC_reg=XADC_Vdac[ich]; // CATIA Vdac1 measurement (pin 25)
      if(XADC_meas==3)XADC_reg=XADC_Vdac[ich]; // CATIA Vdac2 measurement (pin 25)

      Int_t nmeas=1;
      if(XADC_meas==1)nmeas=2;
      if(XADC_meas==2)nmeas=n_Vdac;
      if(XADC_meas==3)nmeas=n_Vdac;
      for(int imeas=0; imeas<nmeas; imeas++)
      {
	int loc_meas=imeas;
	if(XADC_meas==1) // CATIA Temp measurements
	{
          for(Int_t i=0; i<N_CATIA; i++) val=I2C_RW(hw, 1000+(CATIA_num[i]<<3), 1, 2,0, 3, debug); // Disable Temp sensors on all CATIAs 
	  command= (imeas<<4) | 0xa;                            // Enable Temp output on this CATIA
	  val=I2C_RW(hw, device_number, 1, command,0, 3, debug);
	  usleep(10000);
	}
	if(XADC_meas==2) // Scan Vdac1_reg values
	{
	  val=I2C_RW(hw, device_number, 6, 0xF,0, 3, debug);    // Enable Vref_ref, 0 in Vref_dac
	  loc_meas=imeas*64;
	  if(loc_meas>4095)loc_meas=4095;
	  command=0x1000 | loc_meas;               // Scan DAC1 values 
	  val=I2C_RW(hw, device_number, 4, command,1, 3, debug);
	  usleep(1000);
	}
	if(XADC_meas==3) // Scan Vdac2_reg values
	{
	  val=I2C_RW(hw, device_number, 6, 0xF,0, 3, debug);    // Enable Vref_ref, 0 in Vref_dac
	  val=I2C_RW(hw, device_number, 4, 0x2000,1, 3, debug); // Output on DAC2
	  loc_meas=imeas*64;
	  if(loc_meas>4095)loc_meas=4095;
	  val=I2C_RW(hw, device_number, 5, loc_meas,1, 3, debug);
	  usleep(1000);
	}

	double ave_val=0.;
	ValWord<uint32_t> temp;
	for(int iave=0; iave<average; iave++)
	{
	  command=DRP_WRb*0 | (XADC_reg<<16);
	  hw.getNode("DRP_XADC").write(command);
	  temp  = hw.getNode("DRP_XADC").read();
	  hw.dispatch();
	  double loc_val=double((temp.value()&0xffff)>>4)/4096.;
	  ave_val+=loc_val;
	}
	ave_val/=average;
	if(XADC_meas==0)
	{
	  Temp[0]=ave_val*4096*0.123-273.;
	  printf("FPGA temperature, meas %d : %.2f deg\n",imeas, Temp[0]);
	}
	else if(XADC_meas==1)
	{
	  Temp[ich*2+imeas+1]=ave_val;
	  printf("CATIA %d, X5 %d, temperature : %.4f V\n", CATIA_num[ich], imeas,Temp[ich*2+imeas+1]);
	}
	else if(XADC_meas==2)
	{
	  //printf("CATIA %d, TP DAC1 %d, Vdac : %.4f V\n", CATIA_num[ich],loc_meas, ave_val);
	  tg_Vdac[ich][0]->SetPoint(imeas,(Double_t)loc_meas,ave_val);
	  DAC_val[imeas]=loc_meas;
	  Vdac1[ich][imeas]=ave_val;
	}
	else if(XADC_meas==3)
	{
	  //printf("CATIA %d, TP DAC2 %d, Vdac : %.4f V\n", CATIA_num[ich],loc_meas, ave_val);
	  tg_Vdac[ich][1]->SetPoint(imeas,(Double_t)loc_meas,ave_val);
	  Vdac2[ich][imeas]=ave_val;
	}
      }
    }
    for(int iDAC=0; iDAC<2; iDAC++)
    {
      tg_Vdac[ich][iDAC]->Fit("pol1","WQ","",100.,3500.);
      fDAC[ich][iDAC]=tg_Vdac[ich][iDAC]->GetFunction("pol1");
      Int_t n=tg_Vdac[ich][iDAC]->GetN();
      for(Int_t i=0; i<n; i++)
      {
	Double_t x,y;
	tg_Vdac[ich][iDAC]->GetPoint(i,x,y);
	tg_Vdac_resi[ich][iDAC]->SetPoint(i,x,y-fDAC[ich][iDAC]->Eval(x));
      }
      Vdac_slope[ich][iDAC]=fDAC[ich][iDAC]->GetParameter(1)*1000.;
      printf("CATIA %d, Vdac%d slope : %e mV/lsb\n",CATIA_num[ich],iDAC,Vdac_slope[ich][iDAC]); 
    }
  }

// Read CATIA power consumption :
  fault   = hw.getNode("LVRB_FAULT").read();
  current = hw.getNode("LVRB_CURRENT").read();
  hw.dispatch();
  Double_t Imeas_V2P5=((current.value()>>4)&0xfff)/4096.*102.4/Rshunt_V2P5;
  Double_t Imeas_V1P2=((current.value()>>20)&0xfff)/4096.*102.4/Rshunt_V1P2;
  printf("LVRB faults : %8.8x\n",fault.value()); 
  printf("CATIA consumption %8.8x : 1.2V : %.2f mA, 2.5V : %.2f mA\n",current.value(),Imeas_V1P2, Imeas_V2P5); 

// Stop LVRB auto scan, not to generate noise in CATIA during calibration
  hw.getNode("VFE_CTRL").write(0);
  hw.dispatch();

//===========================================================================================================
// 1 : Pedestal value vs PED_DAC value.
// Read 1 event of 1000 samples
  printf("Start Pedestal study\n");
  TGraph *tg_ped_DAC[N_CATIA];
  for(Int_t ich=0; ich<N_CATIA; ich++)
  {
    tg_ped_DAC[ich]=new TGraph();
    tg_ped_DAC[ich]->SetLineColor(kRed);
    tg_ped_DAC[ich]->SetMarkerColor(kRed);
    tg_ped_DAC[ich]->SetMarkerSize(kRed);
    sprintf(hname,"Ped_vs_DAC_CATIA%d",CATIA_num[ich]);
    tg_ped_DAC[ich]->SetName(hname);
    tg_ped_DAC[ich]->SetTitle(hname);
  }

  Double_t ped_DAC_par[N_CATIA][2];
  trigger_type = 0;
  nevent       = 1;
  nsample      = 1000;
// Switch to triggered mode + internal trigger, reduce noise by disabling CALIB_PULSE  :
  vice_ctrl=  ADC_CALIB_MODE        *0 |
              SEL_CALIB_PULSE       *0 |
              SELF_TRIGGER_MASK     *0 |
              SELF_TRIGGER_THRESHOLD*0 |
              SELF_TRIGGER          *0 |
              SOFT_TRIGGER          *1 |
              TRIGGER_MODE          *1 | // Always DAQ on trigger
              RESET                 *0;
  hw.getNode("VICE_CTRL").write(vice_ctrl);
// Stop DAQ and ask for NSAMPLE per frame (+timestamp) :
  command = ((nsample+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
// Switch off FE-adapter LEDs
  hw.getNode("FW_VER").write(0);
  hw.dispatch();

// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
// Start DAQ :
  command = ((nsample+1)<<16)+CAPTURE_START;
  hw.getNode("CAP_CTRL").write(command);
  reg=hw.getNode("CAP_CTRL").read();
  hw.dispatch();
  //printf("CAP_CTRL content : 0x%8.8x\n",reg.value());

  for(int ich=0; ich<N_CATIA; ich++)
  {
    device_number=1000 + (CATIA_num[ich]<<3);
    val=I2C_RW(hw, device_number, 1, 2,0, 3, debug); // Switch off temp sensor
    val=I2C_RW(hw, device_number, 4, 0,1, 3, debug); // Switch off every thing 
    val=I2C_RW(hw, device_number, 5, 0,1, 3, debug);
    val=I2C_RW(hw, device_number, 6, 0,0, 3, debug);
    val=I2C_RW(hw, device_number, 3, 0x1086,1, 3, debug); // Ped mid scale on G1 and G10, R400, LPF35
  }
  usleep(100000);

  for(int idac=0; idac<n_PED_dac; idac++)
  {
    for(int ich=0; ich<N_CATIA; ich++)
    {
      device_number=1000 + (CATIA_num[ich]<<3);
      //printf("VCM DAC : %d\n",idac);
      val=I2C_RW(hw, device_number, 3, (idac<<8)|(idac<<3)|6,1, 3, debug);
    }
    usleep(100000);
    Double_t mean[N_CATIA];
    for(Int_t ich=0; ich<N_CATIA; mean[ich++]=0.);
    draw=1;
    for(int ievt=0; ievt<nevent; ievt++)
    {
      timestamp=get_event(hw,trigger_type,nsample,mem,event,fevent,debug,draw);
      draw=0;
      for(Int_t ich=0; ich<N_CATIA; ich++)
      {
        for(Int_t is=0; is<nsample; is++)
        {
          mean[ich]+=fevent[ich][is]*dv;
        }
      }
    }
    for(Int_t ich=0; ich<N_CATIA; ich++)
    {
      mean[ich]/=(nsample*nevent);
      tg_ped_DAC[ich]->SetPoint(idac,(Double_t)idac,mean[ich]-900.);
    }
  }
  for(Int_t ich=0; ich<N_CATIA; ich++)
  {
    tg_ped_DAC[ich]->Fit("pol1","WQ","",0.,n_PED_dac);
    f1=tg_ped_DAC[ich]->GetFunction("pol1");
    ped_DAC_par[ich][0]=f1->GetParameter(0);
    ped_DAC_par[ich][1]=f1->GetParameter(1);
    printf("CATIA%d-G10  pedestal offset %.2f mV, slope %.2f mV/lsb\n",CATIA_num[ich],ped_DAC_par[ich][0],ped_DAC_par[ich][1]);
  }
// Stop DAQ
  command = ((nsample+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();

//===========================================================================================================
// 2 : Noise for R=400 and R=500 and for LPF=50MHz and LPF=35 MHz
  Double_t G10_noise[N_CATIA][4];
  trigger_type=0;
  nevent=100;
  nsample=28670;
  Double_t *rex,*rey,*imx,*imy,*mod;
  rex=(Double_t*)malloc(NSAMPLE_MAX*sizeof(Double_t));
  imx=(Double_t*)malloc(NSAMPLE_MAX*sizeof(Double_t));
  rey=(Double_t*)malloc(NSAMPLE_MAX*sizeof(Double_t));
  imy=(Double_t*)malloc(NSAMPLE_MAX*sizeof(Double_t));
  mod=(Double_t*)malloc(NSAMPLE_MAX*sizeof(Double_t));
  TVirtualFFT *fft_f=TVirtualFFT::FFT(1,&nsample,"C2CF K");
  Double_t dt=1./NSPS;
  Double_t tmax=nsample*dt;
  Double_t fmax=1./dt;
  Double_t df=fmax/nsample;
  TProfile *pmod[N_CATIA];
  TH1D *hmod[N_CATIA][4];
  TGraph *tg_pulse[N_CATIA];
  TGraph *tg_calib[N_CATIA];
  for(Int_t ich=0; ich<N_CATIA; ich++)
  {
    sprintf(hname,"CATIA%d_NDS_G10",CATIA_num[ich]);
    pmod[ich]=new TProfile(hname,hname,nsample,0,fmax);
    sprintf(hname,"CATIA%d_NDS_G10_RTIA_500_LPF_50",CATIA_num[ich]);
    hmod[ich][0]=new TH1D(hname,hname,nsample/2,0.,fmax/2.);
    sprintf(hname,"CATIA%d_NDS_G10_RTIA_500_LPF_35",CATIA_num[ich]);
    hmod[ich][1]=new TH1D(hname,hname,nsample/2,0.,fmax/2.);
    sprintf(hname,"CATIA%d_NDS_G10_RTIA_400_LPF_50",CATIA_num[ich]);
    hmod[ich][2]=new TH1D(hname,hname,nsample/2,0.,fmax/2.);
    sprintf(hname,"CATIA%d_NDS_G10_RTIA_400_LPF_35",CATIA_num[ich]);
    hmod[ich][3]=new TH1D(hname,hname,nsample/2,0.,fmax/2.);
    tg_pulse[ich]=new TGraph();
    tg_pulse[ich]->SetLineColor(kRed);
    tg_pulse[ich]->SetMarkerColor(kRed);
    tg_pulse[ich]->SetMarkerStyle(20);
    tg_pulse[ich]->SetMarkerSize(0.1);
    sprintf(hname,"CATIA%d_G10_R400_LPF35_pedestal_frame",CATIA_num[ich]);
    tg_pulse[ich]->SetTitle(hname);
    tg_pulse[ich]->SetName(hname);
    tg_calib[ich]=new TGraph();
    tg_calib[ich]->SetLineColor(kRed);
    tg_calib[ich]->SetMarkerColor(kRed);
    tg_calib[ich]->SetMarkerStyle(20);
    tg_calib[ich]->SetMarkerSize(0.5);
    sprintf(hname,"CATIA%d_G10_R400_LPF35_calib_pulse",CATIA_num[ich]);
    tg_calib[ich]->SetTitle(hname);
    tg_calib[ich]->SetName(hname);
  }

// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
// Start DAQ :
  command = ((nsample+1)<<16)+CAPTURE_START;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();
  for(Int_t ich=0; ich<N_CATIA; ich++)
  {
    device_number=1000 + (CATIA_num[ich]<<3);
    val=I2C_RW(hw, device_number, 1, 2,0, 3, debug); // Switch off temp sensor
    val=I2C_RW(hw, device_number, 4, 0,1, 3, debug); // Switch off every thing 
    val=I2C_RW(hw, device_number, 5, 0,1, 3, debug);
    val=I2C_RW(hw, device_number, 6, 0,0, 3, debug);
  }
  for(int iRTIA=0; iRTIA<2; iRTIA++)
  {
    for(int iLPF=0; iLPF<2; iLPF++)
    {
      if(iRTIA==0 && iLPF==0) printf("RMS noise for R500, LPF50 :\n");
      if(iRTIA==0 && iLPF==1) printf("RMS noise for R500, LPF35 :\n");
      if(iRTIA==1 && iLPF==0) printf("RMS noise for R400, LPF50 :\n");
      if(iRTIA==1 && iLPF==1) printf("RMS noise for R400, LPF35 :\n");
      for(Int_t ich=0; ich<N_CATIA; ich++)
      {
        G10_noise[ich][iRTIA*2+iLPF]=0.;
        pmod[ich]->Reset();
        device_number=1000 + (CATIA_num[ich]<<3);
        val=I2C_RW(hw, device_number, 3, (0x10<<8) | (0x10<<3) | (iRTIA<<2) | (iLPF<<1) | 0,1, 3, debug);
      }
      usleep(100000);
      draw=1;
      for(int ievt=0; ievt<nevent; ievt++)
      {
        timestamp=get_event(hw,trigger_type,nsample,mem,event,fevent,debug,draw);
        draw=0;

        for(Int_t ich=0; ich<N_CATIA; ich++)
        {
          ped[ich]=0.;
          rms[ich]=0.;
          for(Int_t is=0; is<nsample; is++)
          {
            ped[ich]+=fevent[ich][is]*dv;
            rms[ich]+=fevent[ich][is]*dv*fevent[ich][is]*dv;
            Double_t t=dt*is;
            if(iRTIA==1 && iLPF==1 && ievt==0) tg_pulse[ich]->SetPoint(is,t,fevent[ich][is]*dv);
            rex[is]=fevent[ich][is]*dv;
            imx[is]=0.;
          }
          ped[ich]/=nsample;
          rms[ich]/=nsample;
          rms[ich]=rms[ich]-ped[ich]*ped[ich];
          G10_noise[ich][iRTIA*2+iLPF]+=rms[ich];

          fft_f->SetPointsComplex(rex,imx);
          fft_f->Transform();
          fft_f->GetPointsComplex(rey,imy);
          for(Int_t is=0; is<nsample; is++)
          {
            Double_t f=df*(is+0.5);
            rey[is]/=nsample;
            imy[is]/=nsample;
            mod[is]=rey[is]*rey[is]+imy[is]*imy[is];
            pmod[ich]->Fill(f,mod[is]/df);
          }
          if(ievt==0)printf("CATIA%d, G10 noise : %.0f uV\n",CATIA_num[ich],sqrt(rms[ich])*1000.);
        }
      }
      for(Int_t ich=0; ich<N_CATIA; ich++)
      {
        G10_noise[ich][iRTIA*2+iLPF]=sqrt(G10_noise[ich][iRTIA*2+iLPF]/nevent)*1000.;
        hmod[ich][iRTIA*2+iLPF]->SetBinContent(1,0.);
        for(Int_t is=1; is<nsample/2; is++)
        {
          Double_t mod=0.;
          mod=pmod[ich]->GetBinContent(is+1);
          hmod[ich][iRTIA*2+iLPF]->SetBinContent(is+1,sqrt(2.*mod));
        }
      }
    }
  }
// Stop DAQ
  command = ((nsample+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();

//===========================================================================================================
// 3 : Send internal TP triggers for linearity measurements : G10
// calibration trigger setting :
  trigger_type=1;
  nevent=100;
  nsample=300;
  n_calib=32;
  calib_step=128;
  Double_t V_Iinj_slope[N_CATIA], INL_min[N_CATIA], INL_max[N_CATIA];
  TProfile *tp_inj1[N_CATIA][n_calib];
  TGraph *tg_inj1[N_CATIA], *tg_inj2[N_CATIA], *tg_inj3[N_CATIA];
  for(Int_t ich=0; ich<N_CATIA; ich++)
  {
    for(int iinj=0; iinj<n_calib; iinj++)
    {
      sprintf(hname,"CATIA%d_pulse_G10_DAC_%4.4d",CATIA_num[ich],calib_level+iinj*calib_step);
      tp_inj1[ich][iinj]=new TProfile(hname,hname,nsample,0.,dt*nsample);
    }
    tg_inj1[ich]=new TGraph();
    sprintf(hname,"CATIA%d_Inj_vs_iDAC_G10",CATIA_num[ich]);
    tg_inj1[ich]->SetTitle(hname);
    tg_inj1[ich]->SetName(hname);
    tg_inj1[ich]->SetMarkerStyle(20);
    tg_inj1[ich]->SetMarkerSize(1.0);
    tg_inj1[ich]->SetMarkerColor(kRed);
    tg_inj1[ich]->SetLineColor(kRed);
    tg_inj1[ich]->SetLineWidth(2);
    tg_inj2[ich]=new TGraph();
    sprintf(hname,"CATIA%d_Inj_vs_vDAC_G10",CATIA_num[ich]);
    tg_inj2[ich]->SetTitle(hname);
    tg_inj2[ich]->SetName(hname);
    tg_inj2[ich]->SetMarkerStyle(20);
    tg_inj2[ich]->SetMarkerSize(1.0);
    tg_inj2[ich]->SetMarkerColor(kRed);
    tg_inj2[ich]->SetLineColor(kRed);
    tg_inj2[ich]->SetLineWidth(2);
    tg_inj3[ich]=new TGraph();
    sprintf(hname,"CATIA%d_INL_vs_amplitude_G10",CATIA_num[ich]);
    tg_inj3[ich]->SetTitle(hname);
    tg_inj3[ich]->SetName(hname);
    tg_inj3[ich]->SetMarkerStyle(20);
    tg_inj3[ich]->SetMarkerSize(1.0);
    tg_inj3[ich]->SetMarkerColor(kRed);
    tg_inj3[ich]->SetLineColor(kRed);
    tg_inj3[ich]->SetLineWidth(2);
    tg_inj3[ich]->SetMaximum(0.003);
    tg_inj3[ich]->SetMinimum(-.003);
  }

  command=(calib_delay<<16) | (calib_width&0xffff);
  printf("Calibration trigger with %d clocks width and %d clocks delay : %x\n",calib_width,calib_delay,command);
  hw.getNode("CALIB_CTRL").write(command);
// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
// Start DAQ :
  command = ((nsample+1)<<16)+CAPTURE_START;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();

  for(Int_t ich=0; ich<N_CATIA; ich++)
  {
    device_number=1000 + (CATIA_num[ich]<<3);
    val=I2C_RW(hw, device_number, 1, 2,0, 3, debug); // Switch off temp sensor
    val=I2C_RW(hw, device_number, 3, (0x10<<8) | (0x10<<3) | (1<<2) | (1<<1) | 0,1, 3, debug); // R400, LPF35, Ped mid scale, 2.5V outputs
    //printf("Reg3 register value read : 0x%x, %d\n",val,val&0xFFF);
    val=I2C_RW(hw, device_number, 5, 0,1, 3, debug); // Switch off every thing 
    //printf("Reg5 register value read : 0x%x, %d\n",val,val&0xFFF);
    val=I2C_RW(hw, device_number, 6, 0x0b, 0, 3, debug); // Rconv 2471, TIA_dummy ON, TP ON, Vreg O
    //printf("Reg6 register value read : 0x%x, %d\n",val,val&0xFFF);
  }
  usleep(100000);

  calib_level=0;
  Double_t fit_min=-1, fit_max=-1;
  for(int istep=0; istep<n_calib; istep++)
  {
// Program DAC for this step
    for(Int_t ich=0; ich<N_CATIA; ich++)
    {
      device_number=1000 + (CATIA_num[ich]<<3);
      val=I2C_RW(hw, device_number, 4, 0x1000|calib_level,1, 3, debug); // TP with DAC1
      //printf("DAC register value read : 0x%x, %d\n",val,val&0xFFF);
    }
// Wait for Vdac to stabilize :
    usleep(10000);

    draw=1;
    for(Int_t ievt=0; ievt<nevent; ievt++)
    {
      timestamp=get_event(hw,trigger_type,nsample,mem,event,fevent,debug,draw);
      if(debug==0)
      {
        system("stty raw");
        char cdum=getchar();
        system("stty -raw");
        if(cdum=='c')draw=0;
      }
      else 
        draw=0;
      if(timestamp<0) printf("DAQ error on event %d : %d\n",ievt, timestamp);

      for(Int_t ich=0; ich<N_CATIA; ich++)
      {
        for(int is=0; is<nsample; is++)
        {
          tp_inj1[ich][istep]->Fill(dt*(0.5+is),fevent[ich][is]*dv);
        }
      }

    }
    for(Int_t ich=0; ich<N_CATIA; ich++)
    {
      // Pedestal
      tp_inj1[ich][istep]->Fit("pol0","wq","",0.e-9,400.e-9);
      f1=tp_inj1[ich][istep]->GetFunction("pol0");
      mean[ich]=f1->GetParameter(0);
      // top pulse
      tp_inj1[ich][istep]->Fit("pol1","wq","",800.e-9,1600.e-9);
      f1=tp_inj1[ich][istep]->GetFunction("pol1");
      // Back propagate to pulse start (500 ns)
      Double_t val0=f1->GetParameter(0)+f1->GetParameter(1)*500.e-9-mean[ich];
      //if(ich==0) printf("Ped %e, amplitude %e, val %e\n",mean[ich],f1->GetParameter(0),val0);
      // CATIA output vs DAC register content
      tg_inj1[ich]->SetPoint(istep,calib_level,val0);
      // CATIA output vs DAC voltage value
      tg_inj2[ich]->SetPoint(istep,fDAC[ich][0]->Eval(calib_level)*1000.,val0);
      if(fit_min<0 && val0>0.)fit_min=calib_level-calib_step/2.;
      if(val0<1200.)fit_max=calib_level+3*calib_step/2.;
    }
    calib_level+=calib_step;
    if(calib_level>4095)calib_level=4095;
  }
  for(Int_t ich=0; ich<N_CATIA; ich++)
  {
    INL_min[ich]=99999.;
    INL_max[ich]=-99999.;
    tg_inj1[ich]->Fit("pol1","WQ","",fit_min,fit_max);
    tg_inj2[ich]->Fit("pol1","WQ","",fDAC[ich][0]->Eval(fit_min)*1000.,fDAC[ich][0]->Eval(fit_max)*1000.);
    f1=tg_inj2[ich]->GetFunction("pol1");
    V_Iinj_slope[ich]=f1->GetParameter(1);
    printf("CATIA %d : V_Iinj slope = %.3f\n",CATIA_num[ich],V_Iinj_slope[ich]);
    f1=tg_inj1[ich]->GetFunction("pol1");
// Compute residuals :
    Int_t n=tg_inj1[ich]->GetN();
    for(Int_t is=0; is<n; is++)
    {
      Double_t x,y;
      tg_inj1[ich]->GetPoint(is,x,y);
      tg_inj3[ich]->SetPoint(is,y,(y-f1->Eval(x))/1200.);
      if(y>0. && y<1200. && (y-f1->Eval(x))/1200.>INL_max[ich])INL_max[ich]=(y-f1->Eval(x))/1200.;
      if(y>0. && y<1200. && (y-f1->Eval(x))/1200.<INL_min[ich])INL_min[ich]=(y-f1->Eval(x))/1200.;
    }
    printf("CATIA %d : INL min %.2f max %.2f\n",CATIA_num[ich],INL_min[ich]*1000., INL_max[ich]*1000.);
  }

// Stop DAQ
  command = ((nsample+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();

//===========================================================================================================
// 4 : Send G10 calibration pulses with R=500 and R=400
// Enabe CALIB_PULSE  :
  Double_t Rcalib=22000.; // Conversion resistor on PCB to compute CATIA gain
  Double_t Rconv[N_CATIA];
  Double_t CATIA_gain[N_CATIA][2];
  vice_ctrl=  ADC_CALIB_MODE        *0 |
              SEL_CALIB_PULSE       *1 |
              SELF_TRIGGER_MASK     *0 |
              SELF_TRIGGER_THRESHOLD*0 |
              SELF_TRIGGER          *0 |
              SOFT_TRIGGER          *1 |
              TRIGGER_MODE          *1 | // Always DAQ on trigger
              RESET                 *0;
  hw.getNode("VICE_CTRL").write(vice_ctrl);
  nsample=500;
  nevent=100;
  command=(calib_delay<<16) | (calib_width&0xffff);
  printf("Calibration trigger with %d clocks width and %d clocks delay : %x\n",calib_width,calib_delay,command);
  hw.getNode("CALIB_CTRL").write(command);
// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
// Start DAQ :
  command = ((nsample+1)<<16)+CAPTURE_START;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();

  for(Int_t ich=0; ich<N_CATIA; ich++)
  {
    device_number=1000 + (CATIA_num[ich]<<3);
    val=I2C_RW(hw, device_number, 6, 0x0,0, 3, debug); // TP stuff off
    val=I2C_RW(hw, device_number, 4, 0x0,1, 3, debug); // DAC1 and DAC2 off
  }
  TProfile *tp_calib[n_calib][2];
  TH1D *hcalib_t0[N_CATIA], *hcalib_rt[N_CATIA];
  for(Int_t ich=0; ich<N_CATIA; ich++)
  {
    sprintf(hname,"CATIA%d_R500_calib_pulse_response",CATIA_num[ich]);
    tp_calib[ich][0]=new TProfile(hname,hname,nsample,0.,nsample*dt);
    sprintf(hname,"CATIA%d_R400_calib_pulse_response",CATIA_num[ich]);
    tp_calib[ich][1]=new TProfile(hname,hname,nsample,0.,nsample*dt);
    sprintf(hname,"CATIA%d_t0_calib",CATIA_num[ich]);
    hcalib_t0[ich]=new TH1D(hname, hname,1000,1.175e-6,1.195e-6);
    sprintf(hname,"CATIA%d_RT_calib",CATIA_num[ich]);
    hcalib_rt[ich]=new TH1D(hname,hname,1000,2.e-9,12.e-9);
  }
  TF1 *fcalib=new TF1("fcalib","[0]+[1]*(1.+tanh((x-[2])/[3]))/2.",0.,2.e3);
  TF1 *fgaus =new TF1("fgaus","[0]*exp(-(x-[1])*(x-[1])/(2.*[2]*[2]))",-10.,10.);
  fcalib->SetParameter(0,2800.);
  fcalib->SetParameter(1,6000.);
  fcalib->SetParameter(2,1.18e-6);
  fcalib->SetParameter(3,9.e-9);

  trigger_type=4;
  for(int iRTIA=0; iRTIA<2; iRTIA++)
  {
    for(Int_t ich=0; ich<N_CATIA; ich++)
    {
      device_number=1000 + (CATIA_num[ich]<<3);
      val=I2C_RW(hw, device_number, 3, (0x10<<8) | (0x10<<3) | (iRTIA<<2) | (1<<1) | 0,1, 3, debug);
    }
    usleep(100000);
    draw=1;
    for(int ievt=0; ievt<nevent; ievt++)
    {
      timestamp=get_event(hw,trigger_type,nsample,mem,event,fevent,debug,draw);
      draw=0;
      for(Int_t ich=0; ich<N_CATIA; ich++)
      {
        Double_t loc_ped=0., loc_max=0., loc_tmax;
        Int_t    ns_ped=0.;
        for(int is=0; is<nsample; is++)
        {
          Double_t t=dt*(is+0.5);
          if(t>0.9e-6 && t<1.1e-6)
          {
            loc_ped+=fevent[ich][is];
            ns_ped++;
          }
          if(fevent[ich][is]>loc_max && t<1.5e-6){loc_max=fevent[ich][is]; loc_tmax=t;}
          tp_calib[ich][iRTIA]->Fill(t,fevent[ich][is]*(dv/1000.));
          if(iRTIA==1)tg_calib[ich]->SetPoint(is,dt*is,fevent[ich][is]);
        }
        if(iRTIA==1)
        {
          loc_ped/=ns_ped;
          fcalib->SetParameter(0,loc_ped);
          fcalib->SetParameter(1,loc_max-loc_ped);
          fcalib->SetParameter(2,loc_tmax-10.e-9);
          fcalib->SetParameter(3,7.e-9);
          tg_calib[ich]->Fit(fcalib,"QW","",1.0e-6,loc_tmax+5.e-9);
          hcalib_t0[ich]->Fill(fcalib->GetParameter(2));
          hcalib_rt[ich]->Fill(fcalib->GetParameter(3));
        }
      }
    }
    for(Int_t ich=0; ich<N_CATIA; ich++)
    {
      tp_calib[ich][iRTIA]->Fit("pol0","WQ","",0.,1.e-6);
      f1=tp_calib[ich][iRTIA]->GetFunction("pol0");
      mean[ich]=f1->GetParameter(0);
      tp_calib[ich][iRTIA]->Fit("pol1","WQ","",1.3e-6,2.1e-6);
      f1=tp_calib[ich][iRTIA]->GetFunction("pol1");
      CATIA_gain[ich][iRTIA]=(f1->Eval(1.2e-6)-mean[ich])/(2.50/Rcalib); // 2.5V pulse on 22k
      printf("CATIA %d, R_TIA %.0f, Total gain : %e V/A\n",CATIA_num[ich],RTIA[iRTIA],CATIA_gain[ich][iRTIA]);
    }
  }
  Double_t calib_pos[N_CATIA], calib_jitter[N_CATIA], RT[N_CATIA], eRT[N_CATIA];
  for(Int_t ich=0; ich<N_CATIA; ich++)
  {
    printf("G10*R500 = %.0f Ohm\n",CATIA_gain[ich][0]);
    printf("G10*R400 = %.0f Ohm\n",CATIA_gain[ich][1]);
    Rconv[ich]=CATIA_gain[ich][1]/V_Iinj_slope[ich]; // Iinj conversion resistor for G10-R400
    printf("CATIA %d, Rconv Iinj G10  (2471 Ohm)  = %.0f Ohm\n",CATIA_num[ich], Rconv[ich]);
    fgaus->SetParameter(0,hcalib_t0[ich]->GetMaximum());
    fgaus->SetParameter(1,hcalib_t0[ich]->GetMean());
    fgaus->SetParameter(2,hcalib_t0[ich]->GetRMS());
    hcalib_t0[ich]->Fit(fgaus,"LQ","",hcalib_t0[ich]->GetMean()-5.*hcalib_t0[ich]->GetRMS(),hcalib_t0[ich]->GetMean()+5.*hcalib_t0[ich]->GetRMS());
    calib_pos[ich]=fgaus->GetParameter(1);
    calib_jitter[ich]=fgaus->GetParameter(2);
    fgaus->SetParameter(0,hcalib_rt[ich]->GetMaximum());
    fgaus->SetParameter(1,hcalib_rt[ich]->GetMean());
    fgaus->SetParameter(2,hcalib_rt[ich]->GetRMS());
    hcalib_rt[ich]->Fit(fgaus,"LQ","",hcalib_rt[ich]->GetMean()-5.*hcalib_rt[ich]->GetRMS(),hcalib_rt[ich]->GetMean()+5.*hcalib_rt[ich]->GetRMS());
    RT[ich]=fgaus->GetParameter(1);
    eRT[ich]=fgaus->GetParameter(2);
    printf("Pulse jitter : %.1f ps, rise time %.2f ns\n",calib_jitter[ich]*1.e12,RT[ich]*1.e9);
  }

// Stop DAQ :
  command = ((nsample+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
// Switch on FE-adapter LEDs
  command = LED_ON*1+GENE_100HZ*0+GENE_TRIGGER*0;
  hw.getNode("FW_VER").write(command);
  hw.dispatch();
// Restart LVRB auto scan
  printf("Restart LVRB auto scan mode\n");
  hw.getNode("VFE_CTRL").write(LVRB_AUTOSCAN*1);
  hw.dispatch();

// Switch off CATIA LV to change chip :
// First ask to generate alert with overcurrent
  val=I2C_RW(hw,0x67,0x01,0x20,0,1,debug);
  val=I2C_RW(hw,0x6C,0x01,0x20,0,1,debug);
// Now, generate a current overflow :
  val=I2C_RW(hw,0x67,0x03,0x20,0,1,debug);
  val=I2C_RW(hw,0x6C,0x03,0x20,0,1,debug);
  usleep(1000000);
// Read current values for control :
  val=I2C_RW(hw, 0x67, 0x14, 0,1, 2, debug); // Read Dsense value
  printf("CATIA V1P2 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V1P2);
  val=I2C_RW(hw, 0x6C, 0x14, 0,1, 2, debug); // Read Dsense value
  printf("CATIA V2P5 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V2P5);

// Now compute Rconv for current injection should be around 272 and 2472 Ohms
  sprintf(output_file,"data/VFE_irrad/VFE_irrad_%.ld.root",tv.tv_sec);
  TFile *fd=new TFile(output_file,"recreate");
  for(Int_t ich=0; ich<N_CATIA; ich++)
  {
    tg_Vdac[ich][0]->Write();
    tg_Vdac[ich][1]->Write();
    tg_Vdac_resi[ich][0]->Write();
    tg_Vdac_resi[ich][1]->Write();
    tg_ped_DAC[ich]->Write();
    tg_pulse[ich]->Write();
    hmod[ich][0]->Write();
    hmod[ich][1]->Write();
    hmod[ich][2]->Write();
    hmod[ich][3]->Write();
    for(int iinj=0; iinj<n_calib; iinj++)
    {
      tp_inj1[ich][iinj]->Write();
    }
    tg_inj1[ich]->Write();
    tg_inj2[ich]->Write();
    tg_inj3[ich]->Write();
    tp_calib[ich][0]->Write();
    tp_calib[ich][1]->Write();
    tg_calib[ich]->Write();
    hcalib_t0[ich]->Write();
    hcalib_rt[ich]->Write();
  }
  fd->Close();

  sprintf(output_file,"data/VFE_irrad/VFE_irrad_%ld.txt",tv.tv_sec);
  FILE *fout=fopen(output_file,"w+");
  fprintf(fout,"# Temperature FPGA CATIA_X1 CATIA_X5\n");
  fprintf(fout,"%e ",Temp[0]);
  for(Int_t ich=0; ich<N_CATIA; ich++) fprintf(fout,"%e %e ",Temp[1+ich*2],Temp[2+ich*2]);
  fprintf(fout,"\n");

  fprintf(fout,"# Iinj DAC values : in, DAC1, DAC2\n");
  fprintf(fout,"%d\n",n_Vdac);
  for(Int_t i=0; i<n_Vdac; i++) fprintf(fout,"%e ",DAC_val[i]);
  fprintf(fout,"\n");
  for(Int_t ich=0; ich<N_CATIA; ich++)
  {
    for(Int_t i=0; i<n_Vdac; i++) fprintf(fout,"%e ",Vdac1[ich][i]);
    fprintf(fout,"\n");
    for(Int_t i=0; i<n_Vdac; i++) fprintf(fout,"%e ",Vdac2[ich][i]);
    fprintf(fout,"\n");
  }
  fprintf(fout,"# Iinj DAC slopes : DAC1, DAC2\n");
  for(Int_t ich=0; ich<N_CATIA; ich++)
  {
    fprintf(fout,"%e %e\n",Vdac_slope[ich][0],Vdac_slope[ich][1]);
  }
  fprintf(fout,"# PED DAC values : G10\n");
  fprintf(fout,"%d\n",n_PED_dac);
  for(Int_t ich=0; ich<N_CATIA; ich++)
  {
    for(Int_t i=0; i<n_PED_dac; i++)
    {
      Double_t x,y;
      tg_ped_DAC[ich]->GetPoint(i,x,y);
      fprintf(fout,"%e ",y);
    }
    fprintf(fout,"\n");
  }
  fprintf(fout,"# PED DAC parameters : G10_ped[0], G10_ped_slope\n");
  for(Int_t ich=0; ich<N_CATIA; ich++)
  {
    fprintf(fout,"%e %e\n",ped_DAC_par[ich][0],ped_DAC_par[ich][1]);
  }
  fprintf(fout,"# Noise : G10-R500-LPF50, G10-R500-LPF35 G10-R400-LPF50 G10-R400-LPF35\n");
  for(Int_t ich=0; ich<N_CATIA; ich++)
  {
    fprintf(fout,"%e %e %e %e\n", G10_noise[ich][0],G10_noise[ich][1],G10_noise[ich][2],G10_noise[ich][3]);
  }
  fprintf(fout,"# INL : G10-min G10-max\n");
  for(Int_t ich=0; ich<N_CATIA; ich++)
  {
    fprintf(fout,"%e %e\n",INL_min[ich],INL_max[ich]);
  }
  fprintf(fout,"# CATIA gains : G10-R500 G10-R400 (LPF35)\n");
  for(Int_t ich=0; ich<N_CATIA; ich++)
  {
    fprintf(fout,"%e %e\n",CATIA_gain[ich][0],CATIA_gain[ich][1]);
  }
  fprintf(fout,"# Timing with calib pulses : pos, jitter, rising_time, rising_time RMS\n");
  for(Int_t ich=0; ich<N_CATIA; ich++)
  {
    fprintf(fout,"%e %e %e %e\n",calib_pos[ich],calib_jitter[ich],RT[ich],eRT[ich]);
  }
  fclose(fout);

}

