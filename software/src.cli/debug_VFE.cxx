// // Launch DAQ in debug mode with all initialization but and sync links during ADC calibration :
// bin/debug_VFE.exe -fead 4 -trigger_type 0 -nevt 40000 -nsample 1000 -pwup_reset 1 -IO_reset 1 -init_DTU 1 -reset_ADC 3 -calib_ADC 3 -CATIA_Reg3_def 11fb7 -CATIA_Reg3_def 21f5f -CATIA_Reg3_def 31faf -CATIA_Reg3_def 41c7f -CATIA_Reg3_def 51d7f -ped_mux 1 -ADC_test_mode 0 -debug 1 -delay_auto_tune 1 -PLL_conf 1001a -PLL_conf 2001c -PLL_conf 3001d -PLL_conf 4001e -PLL_conf 5001d -DTU_force_G1 0 -corgain 0 -baseline_G1 1003
// You shoud see 5 TGraph around 3750 ADC counts. If you type "u", you change the calibration mux setting and you should get the pedestal signal (q to quit)
// If you relaunch the command putting "-ped_mux 0" instead of 1, you should pedestal values directly
//
// You can also use this command which read the VFE config from VFE_config.txt :
// bin/debug_VFE.exe -fead 15 -trigger_type 0 -nevt 40000 -nsample 1000 -reset_all 1 -ped_mux 0 -debug 1 -DTU_force_G1 0 -corgain 0 -read_conf_from_file 1
// 
// Take pedestal events to perform noise FFT free gain mode : 26622 is the max numer of words to be acquired, so ~100k samples in compressed mode.
// bin/debug_VFE.exe -fead 4 -trigger_type 0 -nevt 100 -nsample 26622 -pwup_reset 0 -IO_reset 0 -init_DTU 0 -reset_ADC 0 -calib_ADC 0 -CATIA_Reg3_def 11fb7 -CATIA_Reg3_def 21f5f -CATIA_Reg3_def 31faf -CATIA_Reg3_def 41c7f -CATIA_Reg3_def 51d7f -ped_mux 0 -ADC_test_mode 0 -delay_auto_tune 1 -PLL_conf 1001a -PLL_conf 2001c -PLL_conf 3001d -PLL_conf 4001e -PLL_conf 5001d -DTU_force_G1 0 -corgain 0 -baseline_G1 10030 -dump_data 0 -debug 0
//
// Same by forcing G1 gain in DTU :
// bin/debug_VFE.exe -fead 4 -trigger_type 0 -nevt 100 -nsample 26622 -pwup_reset 0 -IO_reset 0 -init_DTU 0 -reset_ADC 0 -calib_ADC 0 -CATIA_Reg3_def 11fb7 -CATIA_Reg3_def 21f5f -CATIA_Reg3_def 31faf -CATIA_Reg3_def 41c7f -CATIA_Reg3_def 51d7f -ped_mux 0 -ADC_test_mode 0 -delay_auto_tune 1 -PLL_conf 1001a -PLL_conf 2001c -PLL_conf 3001d -PLL_conf 4001e -PLL_conf 5001d -DTU_force_G1 1 -corgain 0 -baseline_G1 10030 -dump_data 0 -debug 0
//
// Take TP events
// bin/debug_VFE.exe -fead 4 -trigger_type 1 -nevt 100 -nsample 50 -pwup_reset 0 -IO_reset 0 -init_DTU 0 -reset_ADC 0 -calib_ADC 0 -CATIA_Reg3_def 110c7 -CATIA_Reg3_def 21077 -CATIA_Reg3_def 310c7 -CATIA_Reg3_def 41097 -CATIA_Reg3_def 51097 -ped_mux 0 -ADC_test_mode 0 -debug 1 -delay_auto_tune 1 -TP_level 400 -TP_step 32 -n_TP_step 80 -TP_width 10
// You take 100 events per each TP value from 400 to 400+32*80.
// Since you are in debug mode, if you press "q", you change to the next TP level without waiting for 100 events
//
// Take events by triggering on signal itself (trigger_mask = bit pattern of channels on which the trigger is done (10 for channel E, 1F for all channels) :
// bin/debug_VFE.exe -fead 4 -trigger_type 2 -nevt 1000 -nsample 50 -pwup_reset 0 -IO_reset 0 -init_DTU 0 -reset_ADC 0 -calib_ADC 0 -CATIA_Reg3_def 110c7 -CATIA_Reg3_def 21077 -CATIA_Reg3_def 310c7 -CATIA_Reg3_def 41097 -CATIA_Reg3_def 51097 -ped_mux 0 -ADC_test_mode 0 -debug 0 -self_trigger 1 -self_trigger_threshold 100 -self_trigger_mode 1 -self_trigger_mask 1f -self_trigger_loop 1
//
//
//
#include "uhal/uhal.hpp"
#include <signal.h>

#include "I2C_RW.h"
#include "LiTEDTU_def.h"

#include <vector>
#include <iostream>
#include <cstdlib>
#include <typeinfo>

#include <TApplication.h>
#include <TStyle.h>
#include <TProfile.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <TTree.h>
#include <TF1.h>
#include <TFile.h>

#define I2C_SHIFT_DEV_NUMBER   4

#define GENE_TRIGGER           (1<<0)
#define GENE_TP                (1<<1)
#define GENE_100HZ             (1<<2)
#define LED_ON                 (1<<3)
#define TP_MODE                (1<<4)

#define RESET                  (1<<0)
#define TRIGGER_MODE           (1<<1)
#define SELF_TRIGGER_LOOP      (1<<2)
#define SELF_TRIGGER           (1<<3)
#define CLOCK_PHASE            (1<<4)
#define CLOCK_RESET            (1<<7)
#define SELF_TRIGGER_THRESHOLD (1<<8)
#define SELF_TRIGGER_MASK      (1<<22)
#define CALIB_MUX_ENABLED      (1<<28)
#define BOARD_SN               (1<<28)
#define SELF_TRIGGER_MODE      (1<<31)

#define SEQ_CLOCK_PHASE        (1<<0)
#define IO_CLOCK_PHASE         (1<<4)
#define REG_CLOCK_PHASE        (1<<8)
#define MEM_CLOCK_PHASE        (1<<12)
#define RESYNC_PHASE           (1<<16)

#define I2C_TOGGLE_SDA         (1<<31)

#define PWUP_RESETB            (1<<31)
#define BULKY_I2C_DTU          (1<<28)
#define N_RETRY_I2C_DTU        (1<<14)
#define PED_MUX                (1<<13)
#define BUGGY_I2C_DTU          (1<<4)
#define INVERT_RESYNC          (1<<3)
#define LVRB_AUTOSCAN          (1<<2)

#define CAPTURE_START          1
#define CAPTURE_STOP           2
//#define SW_DAQ_DELAY           0x1800 // delay for laser with internal trigger
#define SW_DAQ_DELAY           (1<<16)  // delay for laser with external trigger
#define HW_DAQ_DELAY           (1<<0)   // Laser with external trigger
#define NSAMPLE_MAX            26624
#define MAX_PAYLOAD            1380
#define MAX_VFE                10
#define MAX_STEP               10000

#define DRP_WRb                (1<<31)
#define DELAY_TAP_DIR          (1<<5)     // Increase (1) or decrease (0) tap delay in ISERDES
#define DELAY_RESET            (1<<6)     // Reset ISERDES delay
#define IO_RESET               (1<<7)     // Reset ISERDES IO
#define BIT_SLIP               (1<<16)    // Slip serial bits by 1 unit (to align bits in ISERDES) (to multiply with 5 bits channel map)
#define BYTE_SLIP              (1<<21)    // Slip serial bytes by 1 unit (to align bytes in 40 MHz clock) (to multiply with 5 bits channel map)
#define DELAY_AUTO_TUNE        (1<<29)    // Launch autamatic IDELAY tuning to get the eye center of each stream

#define ADC_CALIB_MODE         (1<<0)
#define ADC_TEST_MODE          (1<<1)
#define ADC_MEM_MODE           (1<<5)
#define ADC_INVERT_DATA        (1<<8)
#define RESYNC_IDLE_PATTERN    (1<<0)
#define eLINK_ACTIVE           (1<<8)
#define START_IDELAY_SYNC      (1<<27)    // Force IDELAY tuning to get the eye center of each stream when idle patterns are ON (LiTE
#define STATIC_RESET           (1<<31)

#define I2C_LVR_TYPE           0
#define I2C_CATIA_TYPE         1
#define I2C_LiTEDTU_TYPE       2

#define NSAMPLE_PED            30

using namespace uhal;
Int_t n_active_channel, channel_number[5], eLink_active, calib_mux_enabled;
Int_t I2C_LiTEDTU_type, I2C_CATIA_type, I2C_shift_dev_number;
Int_t delay_val[5];    // Number of delay taps to get a stable R/O with ADCs (160 MHz)
Int_t bitslip_val[5];  // Number of bit to slip with iserdes lines to get ADC values well aligned
Int_t byteslip_val[5]; // Number of byte to slip to aligned ADC data on 40 MHz clock
Int_t old_read_address, shift_oddH_samples, shift_oddL_samples, ADC_invert_data, ADC_test_mode, ADC_MEM_mode, wait_for_ever;
Int_t I2C_DTU_nreg, buggy_DTU;  
Int_t trigger_mode;         // Use memory in FIFO mode or not
UInt_t DTU_bulk1, DTU_bulk2, DTU_bulk3, DTU_bulk4, DTU_bulk5;
Short_t *event[6], *gain[6];
Double_t *fevent[6], ped_G1[6], ped_G10[6];
Int_t firmware_version, DTU_version[5], DTU_test_mode[5], CATIA_version[5], CATIA_gain[5], CATIA_LPF[5];
Int_t all_sample[6], ns_g1[6];
Int_t dump_data, corgain, DTU_force_G1, DTU_force_G10;
ValVector< uint32_t > block,mem;
TGraph *tg[6], *tg_g1[6];
TGraph *tg_rms[6], *tg_resi[6], *tg_diff[2];
TGraphErrors *tg_mean[6];
TH1D *hmean[6], *hrms[6], *hdensity[6];
TCanvas *c1=NULL, *c2=NULL, *c3=NULL;
TTree *tdata=NULL;
TFile *fd=NULL;

struct timeval tv;
void end_of_run()
{
  gettimeofday(&tv,NULL);
  printf("%ld.%6.6ld : End of study\n",tv.tv_sec,tv.tv_usec);
  if(tdata!=NULL)tdata->Write();
  if(c1!=NULL)c1->Write();
  if(c2!=NULL)c2->Write();
  if(c3!=NULL)c3->Write();
  if(fd!=NULL)fd->Close();
  exit(1);
}

void intHandler(int)
{
  printf("Ctrl-C detected !\n");
  gettimeofday(&tv,NULL);
  printf("%ld.%6.6ld : Interupt detected\n",tv.tv_sec,tv.tv_usec);
  end_of_run();
}
void abortHandler(int)
{
  printf("Abort !\n");
  gettimeofday(&tv,NULL);
  printf("%ld.%6.6ld : Run aborted\n",tv.tv_sec,tv.tv_usec);
  end_of_run();
} 

Int_t get_event(uhal::HwInterface hw, int trigger_type, int nsample, int debug, int draw)
{
  static int ievt=0;
  static TGraph *tg_debug[6]={NULL};
  static TCanvas *cdebug=NULL;
  static int trigger_type_old=-1;
  int n_word;
  int n_transfer_max=463;
  int n_transfer;
  int n_last;
  int error      = 0;
  int command    = 0;
  Int_t loc_ch[6]={1,2,3,4,5,6};
  Double_t dt=6.25;
  char hname[80];

  if(trigger_type_old!=trigger_type)ievt=0;
  if(cdebug==NULL)
  {
    for(Int_t ich=0; ich<6; ich++)
    {
      tg_debug[ich]=new TGraph();
      tg_debug[ich]->SetMarkerStyle(20);
      tg_debug[ich]->SetMarkerSize(0.5);
      sprintf(hname,"ch%d",ich);
      tg_debug[ich]->SetTitle(hname);
      tg_debug[ich]->SetName(hname);
    }
    if(ADC_test_mode==1 && eLink_active==0x0f)
    {
      tg_debug[0]->SetLineColor(kCyan);
      tg_debug[1]->SetLineColor(kCyan);
      tg_debug[2]->SetLineColor(kMagenta);
      tg_debug[3]->SetLineColor(kMagenta);
      tg_debug[4]->SetLineColor(kBlue);
      tg_debug[5]->SetLineColor(kRed);
      tg_debug[0]->SetMarkerColor(kCyan);
      tg_debug[1]->SetMarkerColor(kCyan);
      tg_debug[2]->SetMarkerColor(kMagenta);
      tg_debug[3]->SetMarkerColor(kMagenta);
      tg_debug[4]->SetMarkerColor(kBlue);
      tg_debug[5]->SetMarkerColor(kRed);
      loc_ch[0]=1;
      loc_ch[1]=3;
      loc_ch[2]=2;
      loc_ch[3]=4;
      loc_ch[4]=5;
      loc_ch[5]=6;
    }
    else
    {
      tg_debug[0]->SetLineColor(kCyan);
      tg_debug[1]->SetLineColor(kBlue);
      tg_debug[2]->SetLineColor(kMagenta);
      tg_debug[3]->SetLineColor(kRed);
      tg_debug[4]->SetLineColor(kGreen);
      tg_debug[0]->SetMarkerColor(kCyan);
      tg_debug[1]->SetMarkerColor(kBlue);
      tg_debug[2]->SetMarkerColor(kMagenta);
      tg_debug[3]->SetMarkerColor(kRed);
      tg_debug[4]->SetMarkerColor(kGreen);
    }
    cdebug=new TCanvas("debug","debug",800,1000);
    cdebug->Divide(2,3);
  }

  ValWord<uint32_t> address,free_mem;
  address = hw.getNode("CAP_ADDRESS").read();
  hw.dispatch();
  if(debug>0)printf("Starting with address : %8.8x, trigger_type : %d\n",address.value(),trigger_type);

  ValVector< uint32_t > block;

  if(trigger_type !=2)
  {
    if(trigger_type == 0)
      command = TP_MODE*0+LED_ON*1+GENE_100HZ*0+GENE_TP*0+GENE_TRIGGER*1;
    else if(trigger_type == 1)
      command = TP_MODE*0+LED_ON*1+GENE_100HZ*0+GENE_TP*1+GENE_TRIGGER*0;

    if(debug>0)printf("Send trigger with command : 0x%8.8x\n",command);
// Read base address and send trigger
    hw.getNode("FW_VER").write(command);
    hw.dispatch();
// Read new address and wait for DAQ completion
    int nretry=0, new_write_address=-1, delta_address=-1;
    do
    {  
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      new_write_address=address.value()>>16;
      nretry++;
      delta_address=new_write_address-old_read_address;
      if(delta_address<0)delta_address+=NSAMPLE_MAX;
      if(debug>0) printf("ongoing R/W addresses    : old %d, new %d delta %d\n", old_read_address, new_write_address,delta_address);
    }
    while(delta_address < nsample+1 && nretry<100);
    if(nretry==100)
    {
      printf("Stop waiting for sample capture after %d retries\n",nretry);
      printf("R/W addresses    : old %8.8x, new %8.8x add 0x%8.8x\n", old_read_address, new_write_address, address.value());
      error=1;
    }
  }
  else
  {
    int nretry=0;
// Wait for external trigger to fill memory :
    do
    {
      free_mem = hw.getNode("CAP_FREE").read();
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      if(debug>0)printf("address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
      usleep(100);
      //if(wait_for_ever==1)
      //{
      //  usleep(1000);
      //  printf(".");
      //  fflush(stdout);
      //}
      nretry++;
    }
    while((free_mem.value()==NSAMPLE_MAX-1) && (nretry<100 || wait_for_ever==1));
    //if(wait_for_ever==1)printf("\n");
    if(nretry>=100 && wait_for_ever==0)
    {
      printf("Stop waiting for external trigger after %d retries\n",nretry);
      printf("R/W addresses    : add 0x%8.8x\n",address.value());
      error=1;
    }
  }

// Keep reading address for next event
  old_read_address=address.value()>>16;
  if(old_read_address==NSAMPLE_MAX-1)old_read_address=-1;

  mem.clear();

// Read event samples from FPGA
  n_word=(nsample+1)*6; // 6*32 bits words per sample to get the 5 channels data
  n_transfer=n_word/(MAX_PAYLOAD/4); // Max ethernet packet = 1536 bytes, max user payload = 1500 bytes
  n_last=n_word-n_transfer*(MAX_PAYLOAD/4);
  if(n_transfer > n_transfer_max)
  {
    printf("Event size too big ! Please reduce number of samples per frame.\n");
    printf("Max frame size : %d\n",NSAMPLE_MAX);
    printf("Error : ask for too many samples. Number of transfer %d greater than allowed max value %d\n",n_transfer, n_transfer_max);
    error=1;
  }
  for(int itrans=0; itrans<n_transfer; itrans++)
  {
    block = hw.getNode ("CAP_DATA").readBlock(MAX_PAYLOAD/4);
    hw.dispatch();
    for(int is=0; is<MAX_PAYLOAD/4; is++)mem.push_back(block[is]);
  }
  block = hw.getNode ("CAP_DATA").readBlock(n_last);
  address = hw.getNode("CAP_ADDRESS").read();
  free_mem = hw.getNode("CAP_FREE").read();
  hw.dispatch();
  if(debug>0)printf("After reading address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
  for(int is=0; is<n_last; is++)mem.push_back(block[is]);
  mem.valid(true);

// First sample should have bits 159 downto 64 at 1 and timestamp in bits 63 downto 0
  if(mem[3]!=0xffffffff && mem[4]!=0xffffffff && mem[5]!=0xffffffff)
  {
    printf("First samples not headers : %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",mem[5], mem[4], mem[3], mem[2], mem[1], mem[0]);
    error=1;
  }
  Long_t timestamp=mem[2];
  
  timestamp=(timestamp<<32)+mem[1];
  
  if(debug>0)
  {
    Int_t i0=0;
    printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",mem[i0+0],mem[i0+1],mem[i0+2],mem[i0+3],mem[i0+4],mem[i0+5]);
    i0+=6;
    printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",mem[i0+0],mem[i0+1],mem[i0+2],mem[i0+3],mem[i0+4],mem[i0+5]);
    i0+=6;
    printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",mem[i0+0],mem[i0+1],mem[i0+2],mem[i0+3],mem[i0+4],mem[i0+5]);
    i0+=6;
    printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",mem[i0+0],mem[i0+1],mem[i0+2],mem[i0+3],mem[i0+4],mem[i0+5]);
    i0+=6;
    printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",mem[i0+0],mem[i0+1],mem[i0+2],mem[i0+3],mem[i0+4],mem[i0+5]);
    i0+=6;
    printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",mem[i0+0],mem[i0+1],mem[i0+2],mem[i0+3],mem[i0+4],mem[i0+5]);
    printf("timestamp : %8.8x %8.8x %ld\n",mem[2],mem[1],timestamp);
  }
  for(int ich=0; ich<6; ich++) all_sample[ich]=0;
  for(int isample=0; isample<NSAMPLE_MAX*5; isample++)
  {
    event[0][isample]=-1;
    event[1][isample]=-1;
    event[2][isample]=-1;
    event[3][isample]=-1;
    event[4][isample]=-1;
    event[5][isample]=-1;
    gain[0][isample]=DTU_force_G1;
    gain[1][isample]=DTU_force_G1;
    gain[2][isample]=DTU_force_G1;
    gain[3][isample]=DTU_force_G1;
    gain[4][isample]=DTU_force_G1;
    gain[5][isample]=DTU_force_G1;
  }
  Int_t nsample_ped[6]={0};
  Double_t loc_ped_G10[6]={0.};

  FILE *fd;
  if(dump_data==1 && ievt==0) fd=fopen("data/dump.txt","w+");
  if(dump_data==1 && ievt>0) fclose(fd);

  for(int isample=0; isample<nsample; isample++)
  {
    Int_t j=(isample+1)*6;
    unsigned int loc_mem[5];
    for(int ich=0; ich<5; ich++)
    {
      loc_mem[ich]=mem[j+1+ich];
    }
    if(dump_data==1 && isample<100)
    {
      fprintf(fd,"%8.8x %8.8x %8.8x %8.8x %8.8x\n",loc_mem[0],loc_mem[1],loc_mem[2],loc_mem[3],loc_mem[4]);
    }

// Data in test mode :
    if(ADC_test_mode==1)
    {
      dt=12.5;
      event[0][all_sample[0]]  =(loc_mem[0]>>0 )&0xfff;
      event[0][all_sample[0]+1]=(loc_mem[0]>>16)&0xfff;
      event[1][all_sample[1]]  =(loc_mem[1]>>0 )&0xfff;
      event[1][all_sample[1]+1]=(loc_mem[1]>>16)&0xfff;
      event[2][all_sample[2]]  =(loc_mem[2]>>0 )&0xfff;
      event[2][all_sample[2]+1]=(loc_mem[2]>>16)&0xfff;
      event[3][all_sample[3]]  =(loc_mem[3]>>0 )&0xfff;
      event[3][all_sample[3]+1]=(loc_mem[3]>>16)&0xfff;
      event[4][all_sample[4]]  =(loc_mem[4]>>0 )&0xfff;
      event[4][all_sample[4]+1]=(loc_mem[4]>>16)&0xfff;
      if(ADC_invert_data==1)
      {
        event[0][all_sample[0]]  =4095-event[0][all_sample[0]];
        event[0][all_sample[0]+1]=4095-event[0][all_sample[0]+1];
        event[1][all_sample[1]]  =4095-event[1][all_sample[1]];
        event[1][all_sample[1]+1]=4095-event[1][all_sample[1]+1];
        event[2][all_sample[2]]  =4095-event[2][all_sample[2]];
        event[2][all_sample[2]+1]=4095-event[2][all_sample[2]+1];
        event[3][all_sample[3]]  =4095-event[3][all_sample[3]];
        event[3][all_sample[3]+1]=4095-event[3][all_sample[3]+1];
        event[4][all_sample[4]]  =4095-event[4][all_sample[4]];
        event[4][all_sample[4]+1]=4095-event[4][all_sample[4]+1];
      }
      fevent[0][all_sample[0]+0]=(double)event[0][all_sample[0]];
      fevent[0][all_sample[0]+1]=(double)event[0][all_sample[0]+1];
      fevent[1][all_sample[1]+0]=(double)event[1][all_sample[1]];
      fevent[1][all_sample[1]+1]=(double)event[1][all_sample[1]+1];
      fevent[2][all_sample[2]+0]=(double)event[2][all_sample[2]];
      fevent[2][all_sample[2]+1]=(double)event[2][all_sample[2]+1];
      fevent[3][all_sample[3]+0]=(double)event[3][all_sample[3]];
      fevent[3][all_sample[3]+1]=(double)event[3][all_sample[3]+1];
      fevent[4][all_sample[4]+0]=(double)event[4][all_sample[4]];
      fevent[4][all_sample[4]+1]=(double)event[4][all_sample[4]+1];
      if (eLink_active==0x0F)
      {
        event[4][all_sample[4]+0]  =  event[1][all_sample[1]];
        event[4][all_sample[4]+1]  =  event[0][all_sample[0]];
        event[4][all_sample[4]+2]  =  event[1][all_sample[1]+1];
        event[4][all_sample[4]+3]  =  event[0][all_sample[0]+1];
        event[5][all_sample[5]+0]  =  event[3][all_sample[3]];
        event[5][all_sample[5]+1]  =  event[2][all_sample[2]];
        event[5][all_sample[5]+2]  =  event[3][all_sample[3]+1];
        event[5][all_sample[5]+3]  =  event[2][all_sample[2]+1];
        fevent[4][all_sample[4]+0] = fevent[1][all_sample[1]];
        fevent[4][all_sample[4]+1] = fevent[0][all_sample[0]];
        fevent[4][all_sample[4]+2] = fevent[1][all_sample[1]+1];
        fevent[4][all_sample[4]+3] = fevent[0][all_sample[0]+1];
        fevent[5][all_sample[5]+0] = fevent[3][all_sample[3]];
        fevent[5][all_sample[5]+1] = fevent[2][all_sample[2]];
        fevent[5][all_sample[5]+2] = fevent[3][all_sample[3]+1];
        fevent[5][all_sample[5]+3] = fevent[2][all_sample[2]+1];
        all_sample[4]+=4;
        all_sample[5]+=4;
      }
      else
      {
        all_sample[4]+=2;
      }
      for(int ich=0; ich<4; ich++)all_sample[ich]+=2;
    }
    else
// Data in DTU mode :
    {
      dt=6.25;
      for(int ich=0; ich<5; ich++)
      {
        if(ped_G10[ich]<0. && nsample_ped[ich]==NSAMPLE_PED)
        {
          ped_G10[ich]=loc_ped_G10[ich]/NSAMPLE_PED;
          printf("G10 pedestal for channel %d : %f\n",ich,ped_G10[ich]);
        }
        if(debug>2)printf("Channel %d\n",ich);
        UInt_t tmp_mem=loc_mem[ich];
        Int_t type=(tmp_mem>>30);
        if(debug>2)printf("Compressed data : type %d\n",type);
        if(type==3)
        {
          Int_t sub_type=(tmp_mem>>28)&0x3;
          if(sub_type==1 && debug>0) // Frame delimiter
          {
            printf("Ch %d, Frame delimiter %d : %d samples, CRC12= 0x%x\n",ich,tmp_mem&0xff, (tmp_mem>>20)&0xff, (tmp_mem>>8)&0xfff);
          }
          //else if (sub_type==2) // idle pattern
          continue;
        }
        Int_t sample_map=5;
        if(type==2)sample_map=(tmp_mem>>24)&0x3F;
        if(type==2 && sample_map>4)
        {
          //if(debug>=0)printf("Ch %d, Strange sample map : 0x%x, certainly loss of sync !\n",ich,sample_map);
          sample_map=4;
          error|=(1<<(ich+8));
        }
  // pedestal data
        if(type==1 || type==2)
        {
          if(debug>2)printf("Baseline data : sample map 0x%x :",sample_map);
          for(Int_t i=0; i<sample_map; i++)
          {
            event[ich][all_sample[ich]]=tmp_mem&0x3F;
            fevent[ich][all_sample[ich]]=(double)event[ich][all_sample[ich]];
            if(debug>2)printf(" 0x%x",event[ich][all_sample[ich]]);
            if(ped_G10[ich]<0. && nsample_ped[ich]<NSAMPLE_PED)
            {
              loc_ped_G10[ich]+=fevent[ich][all_sample[ich]];
              nsample_ped[ich]++;
            }
            all_sample[ich]++;
            tmp_mem>>=6;
          }
          if(debug>2)printf("\n");
        }
        else
  // signal data
        {
          Int_t signal_type=(tmp_mem>>26)&0xF;
          if(debug>2)printf("Signal data : type 0x%x : ",signal_type);
          if(signal_type==0xA)
          {
            event[ich][all_sample[ich]]=tmp_mem&0xFFF;
            if(DTU_force_G1==0) gain[ich][all_sample[ich]]=(tmp_mem>>12)&0x1;
            //if(gain[ich][all_sample[ich]]==1 && event[ich][all_sample[ich]]>0xfa0)event[ich][all_sample[ich]]=0;
            fevent[ich][all_sample[ich]]=(double)event[ich][all_sample[ich]];
            //if(gain[ich][all_sample[ich]]==1 && corgain==1)fevent[ich][all_sample[ich]]*=10.5;
            if(gain[ich][all_sample[ich]]==1 && corgain==1 && ped_G1[ich]>0. && ped_G10[ich]>0.)
              fevent[ich][all_sample[ich]]=(fevent[ich][all_sample[ich]]-ped_G1[ich])*10.1+ped_G10[ich];
            if(debug>2)printf(" 0x%x",event[ich][all_sample[ich]]);
            all_sample[ich]++;
            tmp_mem>>=13;
            event[ich][all_sample[ich]]=tmp_mem&0xFFF;
            if(DTU_force_G1==0) gain[ich][all_sample[ich]]=(tmp_mem>>12)&0x1;
            //if(gain[ich][all_sample[ich]]==1 && event[ich][all_sample[ich]]>0xfa0)event[ich][all_sample[ich]]=0;
            fevent[ich][all_sample[ich]]=(double)event[ich][all_sample[ich]];
            //if(gain[ich][all_sample[ich]]==1 && corgain==1)fevent[ich][all_sample[ich]]*=10.5;
            if(gain[ich][all_sample[ich]]==1 && corgain==1 && ped_G1[ich]>0. && ped_G10[ich]>0.)
              fevent[ich][all_sample[ich]]=(fevent[ich][all_sample[ich]]-ped_G1[ich])*10.1+ped_G10[ich];
            if(debug>2)printf(" 0x%x",event[ich][all_sample[ich]]);
            all_sample[ich]++;
          }
          else if(signal_type==0xB)
          {
            event[ich][all_sample[ich]]=tmp_mem&0xFFF;
            if(DTU_force_G1==0) gain[ich][all_sample[ich]]=(tmp_mem>>12)&0x1;
            //if(gain[ich][all_sample[ich]]==1 && event[ich][all_sample[ich]]>0xfa0)event[ich][all_sample[ich]]=0;
            fevent[ich][all_sample[ich]]=(double)event[ich][all_sample[ich]];
            if(gain[ich][all_sample[ich]]==1 && corgain==1 && ped_G1[ich]>0. && ped_G10[ich]>0.)
              fevent[ich][all_sample[ich]]=(fevent[ich][all_sample[ich]]-ped_G1[ich])*10.1+ped_G10[ich];
            if(debug>2)printf(" 0x%x",event[ich][all_sample[ich]]);
            all_sample[ich]++;
          }
          if(debug>2)printf("\n");
        }
        if(debug>2)
        {
          for(Int_t is=0; is<all_sample[ich]; is++)
          {
            printf("%4d ",event[ich][is]);
          }
          printf("\n");
        }
      }
    }

    //Search for 0x3 and 0x9 in incoming data
    if(debug>0 && isample==0)
    {
      printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",mem[j+0],mem[j+1],mem[j+2],mem[j+3],mem[j+4],mem[j+5],mem[j+6]);
      UInt_t loc_mem=mem[j+1];
      printf("data : %8.8x ",loc_mem);
      for(int i=31; i>=0; i--) printf("%1.1d",(loc_mem>>i)&1);
      printf(" 0x3 and 0x9 at ");
      for(int i=31; i>=0; i--) {if((loc_mem&0xf000f000) == 0x30009000) printf(" %d",i);loc_mem=loc_mem<<1 | ((loc_mem>>31)&1);} 
      printf("\n");

      loc_mem=mem[j+2];
      printf("data : %8.8x ",loc_mem);
      for(int i=31; i>=0; i--) printf("%1.1d",(loc_mem>>i)&1);
      printf(" 0x3 and 0x9 at ");
      for(int i=31; i>=0; i--) {if((loc_mem&0xf000f000) == 0x30009000) printf(" %d",i);loc_mem=loc_mem<<1 | ((loc_mem>>31)&1);} 
      printf("\n");
      
      loc_mem=mem[j+3];
      printf("data : %8.8x ",loc_mem);
      for(int i=31; i>=0; i--) printf("%1.1d",(loc_mem>>i)&1);
      printf(" 0x3 and 0x9 at ");
      for(int i=31; i>=0; i--) {if((loc_mem&0xf000f000) == 0x30009000) printf(" %d",i);loc_mem=loc_mem<<1 | ((loc_mem>>31)&1);} 
      printf("\n");
      
      loc_mem=mem[j+4];
      printf("data : %8.8x ",loc_mem);
      for(int i=31; i>=0; i--) printf("%1.1d",(loc_mem>>i)&1);
      printf(" 0x3 and 0x9 at ");
      for(int i=31; i>=0; i--) {if((loc_mem&0xf000f000) == 0x30009000) printf(" %d",i);loc_mem=loc_mem<<1 | ((loc_mem>>31)&1);} 
      printf("\n");

      loc_mem=mem[j+5];
      printf("data : %8.8x ",loc_mem);
      for(int i=31; i>=0; i--) printf("%1.1d",(loc_mem>>i)&1);
      printf(" 0x3 and 0x9 at ");
      for(int i=31; i>=0; i--) {if((loc_mem&0xf000f000) == 0x30009000) printf(" %d",i);loc_mem=loc_mem<<1 | ((loc_mem>>31)&1);} 
      printf("\n");
      ValWord<uint32_t> reg = hw.getNode("DEBUG1").read();
      hw.dispatch();
      printf("Delay debug : 0x%x\n",reg.value());
    }

  }
  if(debug>0)
  {
    for(Int_t ich=0; ich<5; ich++)printf("Channel %d : %d samples\n",ich,all_sample[ich]);
  }

  if((trigger_type !=trigger_type_old && draw==0) || draw==1 )
  {
    Int_t ch_max=5;
    if(ADC_test_mode==1 && eLink_active==0xF)ch_max=4;
    for(int ich=0; ich<ch_max; ich++)
    {
      tg_debug[ich]->Set(0);
      for(int isample=0; isample<all_sample[ich]; isample++) tg_debug[ich]->SetPoint(isample,dt*isample,fevent[ich][isample]);
    }
    if(ADC_test_mode==1 && eLink_active==0xF)
    {
      for(int ich=4; ich<6; ich++)
      {
        tg_debug[ich]->Set(0);
        for(int isample=0; isample<all_sample[ich]; isample++) tg_debug[ich]->SetPoint(isample,6.25*isample,fevent[ich][isample]);
      }
    }
    for(int ich=0; ich<ch_max; ich++)
    {
      cdebug->cd(loc_ch[ich]);
      tg_debug[ich]->SetMaximum(100.);
      tg_debug[ich]->SetMinimum(0.);
      tg_debug[ich]->Draw("alp");
      tg_debug[ich]->Write();
      cdebug->Update();
    }
    if(ADC_test_mode==1 && eLink_active==0xF)
    {
      tg_debug[4]->SetMaximum(100.);
      tg_debug[5]->SetMinimum(0.);
      cdebug->cd(loc_ch[4]);
      tg_debug[4]->Draw("alp");
      cdebug->cd(loc_ch[5]);
      tg_debug[5]->Draw("alp");
      tg_debug[4]->Write();
      tg_debug[5]->Write();
      cdebug->Update();
    }

  }
  //if(debug>0)
  //{
  //  system("stty raw");
  //  char cdum=getchar();
  //  system("stty -raw");
  //  if(cdum=='q')exit(-1);
  //}

  trigger_type_old=trigger_type;
  ievt++;
  return error;
}

Int_t synchronize_links(uhal::HwInterface hw, Int_t debug)
{
  ValWord<uint32_t> address, DTU_bulk3;
  UInt_t command, DTU_bulk3_val;
  ValWord<uint32_t> free_mem;
  Int_t old_address;
  Int_t iret=0;
  Int_t debug_level=2;
  UInt_t VFE_control= DELAY_AUTO_TUNE*1 | INVERT_RESYNC*0 | LVRB_AUTOSCAN*0 | PED_MUX*1 | eLINK_ACTIVE*0x1f |
                   ADC_CALIB_MODE*0 | ADC_TEST_MODE*1 | ADC_MEM_MODE*ADC_MEM_mode;

  Int_t pll_conf;
  Int_t pll_conf1_min=0;
  Int_t pll_conf1_max=0x3f;
  Int_t pll_conf2_min=0;
  Int_t pll_conf2_max=0x7;

// First unlock the PLL and present the calibration voltages to the ADC :
  for(int ich=0; ich<5; ich++)
  {
    if(DTU_version[ich]<20)
    {
      iret=I2C_RW(hw, I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+2, 15, 0, 0, 2, 0);
      iret=(iret&0xfd) | (1<<1);
      iret=I2C_RW(hw, I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+2, 15, iret, 0, 1, 0);
    }
    else
    {
      iret=I2C_RW(hw, I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+2, 17, 0x3f, 0, 1, 0);
    }
    if(CATIA_version[ich]>=14)
    {
    iret=I2C_RW(hw, I2C_CATIA_type*1000+((ich+1)<<I2C_shift_dev_number)+3, 5, 0x3000, 1, 1, 0);
    }
  }

  Int_t first_good_conf[5]={-1,-1,-1,-1,-1}, last_good_conf[5]={-1,-1,-1,-1,-1};
  Int_t sync_good=0;
  Int_t sync_OK[512][5]={0};
  printf("PLL sync status for conf :\n");
  for(Int_t pll_conf1=pll_conf1_min; pll_conf1<=pll_conf1_max; pll_conf1=(pll_conf1<<1)+1)
  {
    printf("pll_conf [8:3] = 0x%2.2x : ",pll_conf1);
    for(Int_t pll_conf2=pll_conf2_min; pll_conf2<=pll_conf2_max; pll_conf2++)
    {
      pll_conf=(pll_conf1<<3)|pll_conf2;
      sync_good=0;
      Int_t I2C_retry=0;
      hw.getNode("VFE_CTRL").write(VFE_control);
      hw.dispatch();
      for(int ich=0; ich<5; ich++)
      {
        UInt_t device_number=I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+2;
        iret=I2C_RW(hw, device_number, 9, (pll_conf&0xFF), 0, 1, 0);
        iret=I2C_RW(hw, device_number, 8, 0, 0, 2, 0);
        iret&=0xFF;
        iret=I2C_RW(hw, device_number, 8, (iret&0xFE)|((pll_conf&0x100)>>8), 0, 1, 0);
      }

// Let some time for PLL to stabilize ?
      usleep(10000);

// First, reinit delay values :
      hw.getNode("DELAY_CTRL").write(DELAY_TAP_DIR*1 | DELAY_RESET*1);
      hw.dispatch();
      for(Int_t iadc=0; iadc<5; iadc++)
      {
        delay_val[iadc]=0;
      }

      Int_t nsample_sync=5000;

// first look for headers in each sample
// if not, change idelay and restart 
// idelay between 0 and 10 should be enough : 1280 Mhz -> 781.25 sp period, idelay tap=5ns/64=78.125 ps)
// Once we have teh header position, do bitslip to get them in postition 4, 12, 20 or 28
// Then do bytelip to get the headers in [b31..b28] and [b15..b12]
// 26623 = 0x67ff
// Reset idelay :
      command = ((nsample_sync+1)<<16)+CAPTURE_STOP;
      hw.getNode("CAP_CTRL").write(command);
      hw.getNode("CAP_ADDRESS").write(0);
      command = ((nsample_sync+1)<<16)+CAPTURE_START;
      hw.getNode("CAP_CTRL").write(command);
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      old_read_address=address.value()>>16;

      Int_t max_delay=10;
      Int_t loop_event=1;
      Int_t add_one_tap[5]={-1,-1,-1,-1,-1};
      Int_t good_delay[5]={0,0,0,0,0};
      Int_t good_bit[5]  ={0,0,0,0,0};
      Int_t good_byte[5] ={0,0,0,0,0};
      unsigned int h1[5]={0x3,0x3,0x3,0x3,0x3};
      unsigned int h2[5]={0x9,0x9,0x9,0x9,0x9};
      Int_t good_pos[5][32];
      Int_t best_pos[5];
      Int_t good_pos_max[5];

// Launch ADC calibration with autosync ON for LiTE-DTU_V1.2 or
// put LiTE-DTU in sync mode for >= V2.0
// and get an event to see if headers are at their place :
      VFE_control= DELAY_AUTO_TUNE*1 | INVERT_RESYNC*0 | LVRB_AUTOSCAN*0 | 
                   PED_MUX*1 | eLINK_ACTIVE*0x1f |
                   ADC_CALIB_MODE*0 | ADC_TEST_MODE*0 | ADC_MEM_MODE*ADC_MEM_mode;
      hw.getNode("VFE_CTRL").write(VFE_control);
      hw.getNode("DELAY_CTRL").write(DELAY_TAP_DIR*1 | DELAY_RESET*1);
      if(DTU_version[channel_number[0]]<20)
      {
        if(firmware_version<0x21090000)
        {
          hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCL_reset);
          hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCH_reset);
          hw.dispatch();
          hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCH_calib);
        }
        else
        {
          hw.getNode("DTU_RESYNC").write((LiTEDTU_ADCL_reset<<8) | LiTEDTU_ADCH_reset);
          hw.dispatch();
          hw.getNode("DTU_RESYNC").write((LiTEDTU_ADCL_calib<<8) | LiTEDTU_ADCH_calib);
        }
      }
      else
      {
        hw.getNode("DTU_RESYNC").write(LiTEDTU_SYNC_MODE);
        hw.getNode("DTU_SYNC_PATTERN").write(0x0ccccccf); // Default sync pattern for LiTEDTU-V2.0
      }
      hw.dispatch();
      usleep(1000);

      //hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCL_calib);
      //hw.dispatch();
      //usleep(1000);
// Read one event in test mode and look at headers
      VFE_control= DELAY_AUTO_TUNE*1 | INVERT_RESYNC*0 | LVRB_AUTOSCAN*0 | 
                   PED_MUX*1 | eLINK_ACTIVE*0x1f |
                   ADC_CALIB_MODE*0 | ADC_TEST_MODE*1 | ADC_MEM_MODE*ADC_MEM_mode;
      hw.getNode("VFE_CTRL").write(VFE_control);
      Int_t error=get_event(hw,0,nsample_sync,-1,0);

      if(ADC_test_mode==1)
      {
        for(int iadc=0; iadc<5; iadc++)
        {
          for(int ib=0; ib<32; ib++)good_pos[iadc][ib]=0;
          good_pos_max[iadc]=0;
          best_pos[iadc]=0;
        }
        for(int isample=0; isample<nsample_sync; isample++)
        {
          Int_t j=(isample+1)*6;
          for(int iadc=0; iadc<5; iadc++)
          {
            unsigned int loc_mem=0;
            loc_mem=mem[j+1+iadc];
            if(((loc_mem>>28)&0xf)==h1[iadc] && ((loc_mem>>12)&0xf)==h2[iadc])
            {
              good_pos[iadc][28]++;
            }
          }
        }
        sync_good=1;
        for(int iadc=0; iadc<5; iadc++)
        {
          if(good_pos[iadc][28]!=nsample_sync)sync_good=0;
        }
        if(sync_good==1)
        {
          sync_OK[pll_conf][0]=1;
          last_good_conf[0]=pll_conf;
          if(first_good_conf[0]<0 && pll_conf>0)first_good_conf[0]=pll_conf;
        }
      }
      else
      {
        sync_good=0;
        for(Int_t ich=0; ich<5; ich++)
        {
          sync_OK[pll_conf][ich]=1-((error>>(8+ich))&1);
          if(sync_OK[pll_conf][ich]==1)
          {
            sync_good=1;
            last_good_conf[ich]=pll_conf;
            if(first_good_conf[ich]<0 && pll_conf>0)first_good_conf[ich]=pll_conf;
          }
        }
      }

    //printf(" 0x%x : %d,",pll_conf,sync_good);
      if(sync_good==0)printf(".");
      else            printf("X");
      fflush(stdout);
    }
    printf("\n");
  }
  for(Int_t ipll1=0; ipll1<=0x3f; ipll1=(ipll1<<1)|1)
  {
    for(Int_t ipll2=0; ipll2<=7; ipll2++)
    {
      Int_t i=(ipll1<<3)|ipll2;
      printf("pll_conf 0x%3.3x : %2d %2d %2d %2d %2d\n",i,sync_OK[i][0],sync_OK[i][1],sync_OK[i][2],sync_OK[i][3],sync_OK[i][4]);
    }
  }
  for(Int_t ich=0; ich<5; ich++)
  {
    printf("Results for channel %d :\n",ich);
    printf("First good conf : [8..3]=0x%x, [2..0]=0x%x\n",first_good_conf[ich]>>3,first_good_conf[ich]&7);
    printf("Last  good conf : [8..3]=0x%x, [2..0]=0x%x\n", last_good_conf[ich]>>3, last_good_conf[ich]&7);
    UInt_t tmp_first1=first_good_conf[ich]>>3, tmp_first2=0;
    while(tmp_first1!=0){tmp_first1>>=1; tmp_first2++;}
    UInt_t tmp_last1 = last_good_conf[ich]>>3, tmp_last2 =0;
    while(tmp_last1 !=0){tmp_last1 >>=1; tmp_last2++;}
    UInt_t best_conf=(((tmp_last2<<3)+(last_good_conf[ich]&7)+(tmp_first2<<3)+(first_good_conf[ich]&7)))/2;
    UInt_t best_conf1_tmp=best_conf>>3;
    UInt_t best_conf1=0;
    for(Int_t i=0; i<best_conf1_tmp; i++){best_conf1=(best_conf1<<1)+1;}
    UInt_t best_conf2=best_conf&7;
    best_conf=(best_conf1<<3)+best_conf2;
    printf("Best  good conf : [8..3]=0x%x, [2..0]=0x%x, pll_conf=0x%x\n", best_conf1, best_conf2, (best_conf1<<3)+best_conf2);

    UInt_t device_number=I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+2;
    iret=I2C_RW(hw, device_number, 9, (best_conf&0xFF), 0, 1, 0);
    iret=I2C_RW(hw, device_number, 8, 0, 0, 2, 0);
    iret&=0xFF;
    iret=I2C_RW(hw, device_number, 8, (iret&0xFE)|((best_conf&0x100)>>8), 0, 1, 0);
  }

// Relock the PLL :
  for(int ich=0; ich<5; ich++)
  {
    iret=I2C_RW(hw, I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+2, 15, 0, 0, 2, 0);
    iret&=0xfd;
    iret=I2C_RW(hw, I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+2, 15, iret, 0, 1, 0);
  }

// Resync lines after tuning :
// Launch ADC calibration with autosync ON and then get an event to see if headers are at their place :
  VFE_control= DELAY_AUTO_TUNE*1 | INVERT_RESYNC*0 | LVRB_AUTOSCAN*0 | 
               PED_MUX*1 | eLINK_ACTIVE*0x1f |
               ADC_CALIB_MODE*0 | ADC_TEST_MODE*0 | ADC_MEM_MODE*ADC_MEM_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.getNode("DELAY_CTRL").write(DELAY_TAP_DIR*1 | DELAY_RESET*1);
  if(DTU_version[channel_number[0]]<20)
  {
    if(firmware_version<0x21090000)
    {
      hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCH_reset);
      hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCL_reset);
      hw.dispatch();
      hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCH_calib);
    }
    else
    {
      hw.getNode("DTU_RESYNC").write((LiTEDTU_ADCL_reset<<8) | LiTEDTU_ADCH_reset);
      hw.dispatch();
      hw.getNode("DTU_RESYNC").write((LiTEDTU_ADCL_calib<<8) | LiTEDTU_ADCH_calib);
    }
  }
  else
  {
    hw.getNode("DTU_RESYNC").write(LiTEDTU_SYNC_MODE);
  }
  hw.dispatch();
  usleep(1000);

  VFE_control= DELAY_AUTO_TUNE*0 | INVERT_RESYNC*0 | LVRB_AUTOSCAN*0 | 
               PED_MUX*0 | eLINK_ACTIVE*0x1f |
               ADC_CALIB_MODE*0 | ADC_TEST_MODE*0 | ADC_MEM_MODE*ADC_MEM_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();

// Put back physics signals at ADC inputs :
  for(int ich=0; ich<5; ich++)
  {
    if(CATIA_version[ich]>=14)
    {
      iret=I2C_RW(hw, I2C_CATIA_type*1000+((ich+1)<<I2C_shift_dev_number)+3, 5, 0x0000, 1, 1, 0);
    }
  }
  if(DTU_version[channel_number[0]]>=20)
  {
    hw.getNode("DTU_RESYNC").write(LiTEDTU_NORMAL_MODE);
  }
  usleep(100000); // Let some time for voltages to stabilize

  return sync_good;
}

void tune_pedestals(uhal::HwInterface hw, Int_t debug)
{
  UInt_t command=0;
  Int_t iret=0, nsample_ped=1000, n_ped_val=32;
  ValWord<uint32_t> address;
  Int_t old_address;
  UInt_t VFE_control= PED_MUX*0 | eLINK_ACTIVE*0x1f;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();

  if(CATIA_version[channel_number[0]]>=14)n_ped_val=64;
  UInt_t reg1[5]={0};
  Int_t ped_G10[5]={0}, ped_G1[5]={0};

  Int_t max_ped=32;
  if(CATIA_version[channel_number[0]]>=14)max_ped=64;
  Double_t *ped_val_G10[5];
  Double_t *ped_val_G1[5];
  Int_t sat_G1[5], sat_G10[5];

  for(int ich=0; ich<5; ich++)
  {
    Int_t device_number=I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+2;
    reg1[ich]=I2C_RW(hw, device_number, 1, 0, 0, 2, 0);
    reg1[ich]&=0xFF;
    ped_val_G1[ich]=(Double_t*)malloc(max_ped*sizeof(Double_t));
    ped_val_G10[ich]=(Double_t*)malloc(max_ped*sizeof(Double_t));
    for(Int_t i=0; i<max_ped; i++)
    {
      ped_val_G1[ich][i]=0.;
      ped_val_G10[ich][i]=0.;
    }
    sat_G1[ich]=-1;
    sat_G10[ich]=-1;
  }

  for(int G10=0; G10<=1; G10++)
  {
    for(int ich=0; ich<5; ich++)
    {
      Int_t device_number=I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+2;
      if(G10==0)
      {
        iret=I2C_RW(hw, device_number, 1, (reg1[ich]&0x3F)|0xc0, 0, 1, 0);
      }
      else
      {
        iret=I2C_RW(hw, device_number, 1, (reg1[ich]&0x3F)|0x40, 0, 1, 0);
      }
    }

    for(Int_t loc_ped=0; loc_ped<max_ped; loc_ped++)
    {
      for(int ich=0; ich<5; ich++)
      {
        UInt_t device_number=I2C_CATIA_type*1000+((ich+1)<<I2C_shift_dev_number)+3;
        if(ich==4 && I2C_shift_dev_number==4)   device_number = I2C_CATIA_type*1000+((ich+1)<<I2C_shift_dev_number)+0xb;
        UInt_t reg3=loc_ped<<8 | loc_ped<<3 | (CATIA_gain[ich]<<2) | CATIA_LPF[ich]<<1 | 1;
        if(CATIA_version[ich]>=14)
        {
          reg3=(CATIA_gain[ich]<<14) | loc_ped<<8 | loc_ped<<2 | (CATIA_LPF[ich]<<1) | 1;
        }
        iret=I2C_RW(hw, device_number, 3, reg3, 1, 1, 0);
      }
      command = ((nsample_ped+1)<<16)+CAPTURE_STOP;
      hw.getNode("CAP_CTRL").write(command);
      hw.getNode("CAP_ADDRESS").write(0);
      command = ((nsample_ped+1)<<16)+CAPTURE_START;
      hw.getNode("CAP_CTRL").write(command);
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      old_read_address=address.value()>>16;

      // Read 1 pedestal event
      Int_t error=get_event(hw,0,nsample_ped,-1,0);

      for(int ich=0; ich<5; ich++)
      {
        Double_t ped=0.;
        for(int isample=0; isample<all_sample[ich]; isample++)
        {
          ped+=(double)(event[ich][isample]&0xfff);
        }
        ped/=all_sample[ich];
        if(G10==0)
          ped_val_G1[ich][loc_ped]=ped;
        else
          ped_val_G10[ich][loc_ped]=ped;
        if(G10==1)printf(" %.3f/%.3f ",ped_val_G1[ich][loc_ped],ped_val_G10[ich][loc_ped]);
      }
      if(G10==1)printf("\n");
    }
  }

  for(int ich=0; ich<5; ich++)
  {
    for(Int_t loc_ped=1; loc_ped<max_ped; loc_ped++)
    {
      if(fabs(ped_val_G1[ich][loc_ped]-ped_val_G1[ich][loc_ped-1])<.2 && sat_G1[ich]<0)
      {
        sat_G1[ich]=loc_ped-1;
        printf("ch %d, G1 saturation at %d : %.3f\n",ich,loc_ped-1,ped_val_G1[ich][loc_ped-1]);
      }
      if(fabs(ped_val_G10[ich][loc_ped]-ped_val_G10[ich][loc_ped-1])<.2 && sat_G10[ich]<0)
      {
        sat_G10[ich]=loc_ped-1;
        printf("ch %d, G10 saturation at %d : %.3f\n",ich,loc_ped-1,ped_val_G10[ich][loc_ped-1]);
      }
    }
  }

  for(int ich=0; ich<5; ich++)
  {
    ped_G1[ich]=sat_G1[ich];
    ped_G10[ich]=sat_G10[ich];
    //ped_G1[ich]=0;
    //ped_G10[ich]=0;
    for(Int_t loc_ped=0; loc_ped<max_ped; loc_ped++)
    {
      if(ped_val_G1[ich][loc_ped]<30. && ped_val_G1[ich][loc_ped]>10.)
      {
        ped_G1[ich]=loc_ped;
      }
      if(ped_val_G10[ich][loc_ped]<65. && ped_val_G10[ich][loc_ped]>45.)
      {
        ped_G10[ich]=loc_ped;
      }
    }
  }
  printf("Final pedestal settings :\n");
  for(int ich=0; ich<5; ich++)
  {
    printf("ich %d : G10=0x%2x, G1=0x%2x\n",ich,ped_G10[ich],ped_G1[ich]);
    UInt_t device_number=I2C_CATIA_type*1000+((ich+1)<<I2C_shift_dev_number)+3;
    if(ich==4 && I2C_shift_dev_number==4)   device_number = I2C_CATIA_type*1000+((ich+1)<<I2C_shift_dev_number)+0xb;
    UInt_t reg3=(ped_G1[ich]&0x1f)<<8 | (ped_G10[ich]&0x1f)<<3 | CATIA_gain[ich]<<2 | CATIA_LPF[ich]<<1 | 1;
    if(CATIA_version[ich]>=14)
    {
      reg3=(CATIA_gain[ich]<<14) | ped_G1[ich]<<8 | ped_G10[ich]<<2 | (CATIA_LPF[ich]<<1) | 1;
    }
    iret=I2C_RW(hw, device_number, 3, reg3, 1, 1, 0);

// Restore DTU Reg1 content
    device_number=I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+2;
    iret=I2C_RW(hw, device_number, 1, reg1[ich], 0, 1, 0);
  }
}

Int_t main ( Int_t argc,char* argv[] )
{
  signal(SIGINT, intHandler);
  signal(SIGABRT, abortHandler);

  TF1 *f1;
  Int_t ngood_event=0;
  UInt_t soft_reset, full_reset, command;
  ValWord<uint32_t> address;
  for(Int_t ich=0; ich<6; ich++) event[ich]=(short int*)malloc(sizeof(short int)*5*NSAMPLE_MAX);
  for(Int_t ich=0; ich<6; ich++) gain[ich]=(short int*)malloc(sizeof(short int)*5*NSAMPLE_MAX);
  for(Int_t ich=0; ich<6; ich++) fevent[ich]=(double*)malloc(sizeof(double)*5*NSAMPLE_MAX);
  Double_t Vref_ref = 1.000; // Target Vref value for calibration level setting
  Double_t dv=1200./4096.;   // 12 bits on 1.2V
  Double_t display_min=-1.;
  Double_t display_max=-1.;
  Int_t debug=0;
  Int_t debug_draw=0;
  Int_t loc_ch[6]={1,2,3,4,5,6};

  Int_t read_conf_from_file=0;
// Define defaults for laser runs :
  Int_t nevent=1000;
  Int_t nsample=100;
  Int_t nsample_save;
  corgain=1;                    // Online gain correction when switching from G10 to G1
  trigger_mode=1;               // Use memory in FIFO mode
  calib_mux_enabled=0;
  Int_t trigger_type=0;         // pedestal by default
  Int_t self_trigger_loop=0;    // external trigger by default 
  Int_t self_trigger_mode=0;    // Absolute level for self trigger 
  Int_t self_trigger=0;         // No self trigger 
  Int_t self_trigger_threshold=0;
  Int_t self_trigger_mask     =0x1F; // trig on all channels amplitude
  Int_t ADC_calib_mode        = 0;   // Put (1) or not (0) the outputs of CATIA in ADC_calib_mode (outputs near VCM)
  ADC_test_mode               = 0;   // Put (1) or not (0) the LiTE-DTU in test mode (direct access to 2 ADC outputs)
  ADC_MEM_mode                = 0;   // Put (1) or not (0) the LiTE-DTU in test mode (direct access to 2 ADC outputs)
  ADC_invert_data             = 0;   // Invert (1) or not (0) LiTE-DTU data (usefull in case of AC coupling)
  Int_t delay_auto_tune       = 1;
  Int_t ADC_invert_clk        = 0;   // Invert (1) or not (0) LiTE-DTU clock
  Int_t seq_clock_phase       = 0;   // sequence clock phase by steps of 45 degrees
  Int_t IO_clock_phase        = 0;   // Capture clock phase by steps of 45 degrees
  Int_t reg_clock_phase       = 0;   // Shift register clock phase by steps of 45 degrees
  Int_t mem_clock_phase       = 0;   // memory clock phase by steps of 45 degrees
  Int_t clock_phase           = 0;
  Int_t n_TP_step=1;
  Int_t TP_gain=0;
  Int_t TP_step=128;
  Int_t TP_level=0;
  Int_t TP_width=16;
  Int_t TP_delay=50;
  Int_t TP_mode=0;
  Int_t fead=4;
  Int_t VICEPP_clk=-1;
  char cdum, output_file[256];
  Int_t channel_sel=0;
  Int_t reg_val=0;
  Int_t ADC_reg_val[2][76];
  Int_t sw_DAQ_delay=0;
  Int_t hw_DAQ_delay=50;
  Int_t iret;
  Int_t reset_all=0;
  Int_t use_ref_ADC_calib=0;
  shift_oddH_samples=0;
  shift_oddL_samples=0;
  Int_t ch_debug=1;
  Int_t debug_I2C=0;
  UInt_t device_number, val;

// CATIA settings if requested
  n_active_channel=0;
  Int_t Channel_num    = 0;
  Int_t CATIA_data     =-1; // Data to write in CATIA register
  Int_t CATIA_reg      =-1; // Register to write in with I2C protocol
  Int_t I2C_dir        = 0; // read(2)+write(1) with I2C protocol, 0=don't use I2C
  Int_t CATIA_Reg1_def = 0x02;   // Default content of Register 1 : SEU auto correction, no Temp output
  Int_t CATIA_Reg3_def[5] = {0x1087}; // Default content of Register 3 : 1.2V, LPF35, 400 Ohms, ped mid scale
  Int_t CATIA_Reg4_def = 0x1000; // Default content of Register 4 : DAC1 0, DAC1 ON, DAC2 OFF, no copy
  Int_t CATIA_Reg5_def = 0x4000; // Default content of Register 5 : DAC2 0, No Vref output
  Int_t CATIA_Reg6_def = 0x0b;   // Default content of Register 6 : Vreg ON, Rconv=2471, TIA_dummy ON, TP ON
  Int_t CATIA_scan_Vref= 0;

  UInt_t DAC_command[10];
  Int_t nDAC_command     = 0;
  Int_t ped_mux          = 0;
  Int_t do_calib_loop    = 0;
  Int_t nsample_calib_x4 = 0;
  Int_t dither           = 0;
  Int_t global_test      = 0;
  Int_t min_ack          = 15;

//DTU settings if requested
  I2C_CATIA_type         = I2C_CATIA_TYPE;
  I2C_LiTEDTU_type       = I2C_LiTEDTU_TYPE;
  I2C_shift_dev_number   = I2C_SHIFT_DEV_NUMBER;
  Int_t init_DTU         = 0;
  Int_t I2C_long         = 0;
  Int_t reset_ADC        = 0;
  Int_t calib_ADC        = 0;
  Int_t enable_40MHz_out = 0;
  Int_t LVRB_autoscan    = 0;
  Int_t invert_Resync    = 0;
  Int_t I2C_toggle_SDA   = 0;
  Int_t resync_phase     = 0;
  eLink_active           = 0x1f;
  Int_t DTU_G1_window16  = 0;
  DTU_force_G1           = 0;
  DTU_force_G10          = 0;
  Int_t DTU_force_PLL[5] = {0};
  dump_data              = 0;
  //UInt_t Resync_idle_pattern = 0x66;
  UInt_t Resync_idle_pattern = 0x00;
  UInt_t Resync_Hamming_data = 0;
  UInt_t static_DTU_reset    = 0;
  UInt_t pll_conf1[5]        = {4};
  UInt_t pll_conf2[5]        = {3};
  //UInt_t pll_conf         = (pll_conf2<<3)| pll_conf1;
  UInt_t pll_conf[5]         = {0x1c};
  UInt_t pll_loc;
  UInt_t pll_override_Vc     = 0;
  buggy_DTU                  = 0;
  DTU_bulk1                  = 0x0007038F;
  DTU_bulk2                  = 0x88000000;
  DTU_bulk3                  = 0x00553040;
  DTU_bulk4                  = 0x65040000;
  DTU_bulk5                  = 0x000FFF3C;
  UInt_t bs_loc;
  UInt_t baseline_G10[5]     = {0};
  UInt_t baseline_G1[5]      = {0};
  Int_t DAC_Vref_best[5]     = {0};
  I2C_DTU_nreg               = 20;
  wait_for_ever              = 0; // in laser trigger mode, we let wait for ever, waiting for a signal
  int IO_reset   = 0;
  int pwup_reset = 0;

  Double_t Rshunt_V1P2      = 0.255;
  Double_t Rshunt_V2P5      = 0.05;
  Double_t Imeas_V2P5, Imeas_V1P2;
  FILE *fcal;

  TProfile *pshape[MAX_STEP][6];
  sprintf(output_file,"");

  for(int k=1; k<argc; k++) 
  {    
    if(strcmp( argv[k], "-debug") == 0)
    {    
      sscanf( argv[++k], "%d", &debug );
      continue;
    }    
    if(strcmp( argv[k], "-debug_I2C") == 0)
    {    
      sscanf( argv[++k], "%d", &debug_I2C );
      continue;
    }    
    if(strcmp( argv[k], "-debug_draw") == 0)
    {    
      sscanf( argv[++k], "%d", &debug_draw );
      continue;
    }    
    else if(strcmp( argv[k], "-fead") == 0)
    {    
      sscanf( argv[++k], "%d", &fead );
      continue;
    }
    else if(strcmp( argv[k], "-Channel_num") == 0)
    {    
      sscanf( argv[++k], "%d", &Channel_num );
      continue;
    }
    else if(strcmp( argv[k], "-nevt") == 0)
    {    
      sscanf( argv[++k], "%d", &nevent );
      continue;
    }
    else if(strcmp(argv[k],"-nsample") == 0)
    {    
      sscanf( argv[++k], "%d", &nsample );
      continue;
    }    
    else if(strcmp(argv[k],"-corgain") == 0)
    {    
      sscanf( argv[++k], "%d", &corgain );
      continue;
    }    
    else if(strcmp(argv[k],"-self_trigger_loop") == 0)
    {    
// self_trigger_loop
// 0 : Use external trigger (from FE)
// 1 : Loop internally generated trigger from data 
      sscanf( argv[++k], "%d", &self_trigger_loop );
      continue;
    }    
    else if(strcmp(argv[k],"-trigger_type") == 0)
    {    
// trigger_type
// 0 : pedestal
// 1 : TP
// 2 : laser
      sscanf( argv[++k], "%d", &trigger_type );
      continue;
    }
    else if(strcmp(argv[k],"-trigger_mode") == 0)
    {    
// trigger_mode
// 0 : Read/write always in memory address 0
// 1 : Use meory in FIFO mode
      sscanf( argv[++k], "%d", &trigger_mode );
      continue;
    }
    else if(strcmp(argv[k],"-self_trigger_mode") == 0)
    {    
// self_trigger_mode
// 0 : Put absolute level on pulse to generate trigger
// 1 : Put threshold on delta signal
      sscanf( argv[++k], "%d", &self_trigger_mode );
      continue;
    }
    else if(strcmp(argv[k],"-self_trigger") == 0)
    {    
// self_trigger
// 0 : Don't generate trigger from data themselves
// 1 : Generate trigger if any data > self_trigger_threshold
      sscanf( argv[++k], "%d", &self_trigger );
      continue;
    }
    else if(strcmp(argv[k],"-self_trigger_threshold") == 0)
    {    
// self_trigger_threshold in ADC counts
      sscanf( argv[++k], "%d", &self_trigger_threshold );
      continue;
    }
    else if(strcmp(argv[k],"-self_trigger_mask") == 0)
    {    
// channel mask to generate self trigger lsb=ch0 ... msb=ch4
      sscanf( argv[++k], "%x", &self_trigger_mask );
      continue;
    }
    else if(strcmp(argv[k],"-hw_DAQ_delay") == 0)
    {
      sscanf( argv[++k], "%d", &hw_DAQ_delay );
      continue;
    }    
    else if(strcmp(argv[k],"-sw_DAQ_delay") == 0)
    {
      sscanf( argv[++k], "%d", &sw_DAQ_delay );
      continue;
    }    
    else if(strcmp(argv[k],"-TP_mode") == 0)
    {    
// TP mode : 0=external trigger does not generate TP
//           1=external trigger generate TP trigger
      sscanf( argv[++k], "%d", &TP_mode );
      continue;
    }    
    else if(strcmp(argv[k],"-TP_level") == 0)
    {    
// DAC_value 0 ... 4095
      sscanf( argv[++k], "%d", &TP_level );
      continue;
    }    
    else if(strcmp(argv[k],"-TP_width") == 0)
    {    
// TP trigger width 0 ... 65532
      sscanf( argv[++k], "%d", &TP_width );
      continue;
    }    
    else if(strcmp(argv[k],"-TP_delay") == 0)
    {    
// DAQ delay for TP triggers : 0..65532
      sscanf( argv[++k], "%d", &TP_delay );
      continue;
    }    
    else if(strcmp(argv[k],"-n_TP_step") == 0)
    {    
// Number of TP step for linearity study
      sscanf( argv[++k], "%d", &n_TP_step );
      continue;
    }    
    else if(strcmp(argv[k],"-TP_step") == 0)
    {    
// DAC step for linearity study
      sscanf( argv[++k], "%d", &TP_step );
      continue;
    }    
    else if(strcmp(argv[k],"-TP_gain") == 0)
    {    
// Rconv for current injection (0=2471 Ohms=G1, 1=272 Ohms=G10)
      sscanf( argv[++k], "%d", &TP_gain );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_data") == 0)
    {    
      sscanf( argv[++k], "%x", &CATIA_data );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_reg") == 0)
    {    
      sscanf( argv[++k], "%d", &CATIA_reg );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_scan_Vref") == 0)
    {    
      sscanf( argv[++k], "%d", &CATIA_scan_Vref );
      continue;
    }    
    else if(strcmp(argv[k],"-I2C_dir") == 0)
    {    
      sscanf( argv[++k], "%d", &I2C_dir );
      continue;
    }    
    else if(strcmp(argv[k],"-ADC_calib_mode") == 0)
    {    
      sscanf( argv[++k], "%d", &ADC_calib_mode );
      continue;
    }    
    else if(strcmp(argv[k],"-ADC_test_mode") == 0)
    {    
      sscanf( argv[++k], "%d", &ADC_test_mode );
      continue;
    }    
    else if(strcmp(argv[k],"-ADC_MEM_mode") == 0)
    {    
      sscanf( argv[++k], "%d", &ADC_MEM_mode );
      continue;
    }    
    else if(strcmp(argv[k],"-delay_auto_tune") == 0)
    {    
      sscanf( argv[++k], "%d", &delay_auto_tune );
      continue;
    }    
    else if(strcmp(argv[k],"-ADC_invert_data") == 0)
    {    
      sscanf( argv[++k], "%d", &ADC_invert_data );
      continue;
    }    
    else if(strcmp(argv[k],"-ADC_invert_clk") == 0)
    {    
      sscanf( argv[++k], "%d", &ADC_invert_clk );
      continue;
    }    
    else if(strcmp(argv[k],"-I2C_LiTEDTU_type") == 0)
    {    
      sscanf( argv[++k], "%d", &I2C_LiTEDTU_type );
      continue;
    }    
    else if(strcmp(argv[k],"-I2C_CATIA_type") == 0)
    {    
      sscanf( argv[++k], "%d", &I2C_CATIA_type );
      continue;
    }    
    else if(strcmp(argv[k],"-I2C_shift_dev_number") == 0)
    {    
      sscanf( argv[++k], "%d", &I2C_shift_dev_number );
      continue;
    }    
    else if(strcmp(argv[k],"-clock_phase") == 0)
    {    
      sscanf( argv[++k], "%d", &clock_phase );
      seq_clock_phase=(clock_phase>>0)&0x7;
      IO_clock_phase =(clock_phase>>4)&0x7;
      reg_clock_phase=(clock_phase>>8)&0x7;
      mem_clock_phase=(clock_phase>>12)&0x7;
      continue;
    }    
    else if(strcmp(argv[k],"-dump_data") == 0)
    {    
      sscanf( argv[++k], "%x", &dump_data );
      continue;
    }    
    else if(strcmp(argv[k],"-DTU_G1_window16") == 0)
    {    
      sscanf( argv[++k], "%x", &DTU_G1_window16 );
      continue;
    }    
    else if(strcmp(argv[k],"-DTU_force_G1") == 0)
    {    
      sscanf( argv[++k], "%x", &DTU_force_G1 );
      continue;
    }    
    else if(strcmp(argv[k],"-DTU_force_G10") == 0)
    {    
      sscanf( argv[++k], "%x", &DTU_force_G10 );
      continue;
    }    
    else if(strcmp(argv[k],"-DTU_bulk1") == 0)
    {    
      sscanf( argv[++k], "%x", &DTU_bulk1 );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_Reg1_def") == 0)
    {    
      sscanf( argv[++k], "%x", &CATIA_Reg1_def );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_Reg3_def") == 0)
    {    
      sscanf( argv[++k], "%x", &reg_val );
      Int_t ich=(reg_val>>16)&0x7;
      if(ich==0)
      {
        for(Int_t i=0; i<5; i++) CATIA_Reg3_def[i]=reg_val&0xffff;
      }
      else
        CATIA_Reg3_def[ich-1]=reg_val&0xffff;
      printf("ich %d, reg3 : 0x%4.4x\n",ich,CATIA_Reg3_def[ich-1]);
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_Reg4_def") == 0)
    {    
      sscanf( argv[++k], "%x", &CATIA_Reg4_def );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_Reg5_def") == 0)
    {    
      sscanf( argv[++k], "%x", &CATIA_Reg5_def );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_Reg6_def") == 0)
    {    
      sscanf( argv[++k], "%x", &CATIA_Reg6_def );
      continue;
    }    
    else if(strcmp(argv[k],"-reset_ADC") == 0)
    {    
      sscanf( argv[++k], "%d", &reset_ADC );
      reset_ADC&=3;
      continue;
    }    
    else if(strcmp(argv[k],"-use_ref_ADC_calib") == 0)
    {    
      sscanf( argv[++k], "%d", &use_ref_ADC_calib );
      continue;
    }    
    else if(strcmp(argv[k],"-shift_oddH_samples") == 0)
    {    
      sscanf( argv[++k], "%d", &shift_oddH_samples );
      continue;
    }    
    else if(strcmp(argv[k],"-shift_oddL_samples") == 0)
    {    
      sscanf( argv[++k], "%d", &shift_oddL_samples );
      continue;
    }    
    else if(strcmp(argv[k],"-calib_ADC") == 0)
    {    
      sscanf( argv[++k], "%d", &calib_ADC );
      calib_ADC&=3;
      continue;
    }    
    else if(strcmp(argv[k],"-static_DTU_reset") == 0)
    {
      sscanf( argv[++k], "%d", &static_DTU_reset );
      if(static_DTU_reset!=0) static_DTU_reset=1;
      continue;
    }
    else if(strcmp(argv[k],"-LVRB_autoscan") == 0)
    {
      sscanf( argv[++k], "%d", &LVRB_autoscan );
      continue;
    }
    else if(strcmp(argv[k],"-invert_Resync") == 0)
    {
      sscanf( argv[++k], "%d", &invert_Resync );
      continue;
    }
    else if(strcmp(argv[k],"-Resync_phase") == 0)
    {
      sscanf( argv[++k], "%d", &resync_phase );
      resync_phase&=3;
      continue;
    }
    else if(strcmp(argv[k],"-eLink_active") == 0)
    {
      sscanf( argv[++k], "%x", &eLink_active );
      eLink_active&=0x1f;
      continue;
    }
    else if(strcmp(argv[k],"-PLL_conf") == 0)
    {
      sscanf( argv[++k], "%x", &pll_loc );
      Int_t ich=(pll_loc>>16)&0x7;
      if(ich==0)
      {
        for(Int_t i=0; i<5; i++)
        {
          pll_conf[i]=pll_loc&0x01ff;
          pll_conf1[i]=pll_conf[i]&0x7;
          pll_conf2[i]=pll_conf[i]>>3;
        }
      }
      else
      {
        pll_conf[ich-1]=pll_loc&0x01ff;
        pll_conf1[ich-1]=pll_conf[ich-1]&0x7;
        pll_conf2[ich-1]=pll_conf[ich-1]>>3;
      }
      continue;
    }
    else if(strcmp(argv[k],"-baseline_G10") == 0)
    {
      sscanf( argv[++k], "%x", &bs_loc );
      Int_t ich=(bs_loc>>16)&0x7;
      if(ich==0)
      {
        for(Int_t i=0; i<5; i++)
        {
          baseline_G10[i]=bs_loc&0x00ff;
        }
      }
      else
      {
        baseline_G10[ich-1]=bs_loc&0x00ff;
      }
      continue;
    }
    else if(strcmp(argv[k],"-baseline_G1") == 0)
    {
      sscanf( argv[++k], "%x", &bs_loc );
      Int_t ich=(bs_loc>>16)&0x7;
      if(ich==0)
      {
        for(Int_t i=0; i<5; i++)
        {
          baseline_G1[i]=bs_loc&0x00ff;
        }
      }
      else
      {
        baseline_G1[ich-1]=bs_loc&0x00ff;
      }
      continue;
    }
    else if(strcmp(argv[k],"-I2C_toggle_SDA") == 0)
    {
      sscanf( argv[++k], "%x", &I2C_toggle_SDA );
      continue;
    }
    else if(strcmp(argv[k],"-init_DTU") == 0)
    {
      sscanf( argv[++k], "%x", &init_DTU );
      continue;
    }
    else if(strcmp(argv[k],"-do_calib_loop") == 0)
    {
      sscanf( argv[++k], "%d", &do_calib_loop );
      continue;
    }
    else if(strcmp(argv[k],"-dither") == 0)
    {
      sscanf( argv[++k], "%x", &dither );
      continue;
    }
    else if(strcmp(argv[k],"-global_test") == 0)
    {
      sscanf( argv[++k], "%x", &global_test );
      continue;
    }
    else if(strcmp(argv[k],"-min_ack") == 0)
    {
      sscanf( argv[++k], "%x", &min_ack );
      continue;
    }
    else if(strcmp(argv[k],"-nsample_calib_x4") == 0)
    {
      sscanf( argv[++k], "%x", &nsample_calib_x4 );
      continue;
    }
    else if(strcmp(argv[k],"-ped_mux") == 0)
    {
      sscanf( argv[++k], "%x", &ped_mux );
      continue;
    }
    else if(strcmp(argv[k],"-IO_reset") == 0)
    {
      sscanf( argv[++k], "%d", &IO_reset );
      continue;
    }
    else if(strcmp(argv[k],"-pwup_reset") == 0)
    {
      sscanf( argv[++k], "%d", &pwup_reset );
      continue;
    }
    else if(strcmp(argv[k],"-reset_all") == 0)
    {
      sscanf( argv[++k], "%d", &reset_all );
      if(reset_all>0)
      {
        IO_reset=1;
        pwup_reset=1;
        init_DTU=1;
        reset_ADC=3;
        calib_ADC=3;
      }
      continue;
    }
    else if(strcmp(argv[k],"-Resync_idle_pattern") == 0)
    {
      sscanf( argv[++k], "%x", &Resync_idle_pattern );
      Resync_idle_pattern&=0xff;
      continue;
    }
    else if(strcmp(argv[k],"-Resync_Hamming_data") == 0)
    {
      sscanf( argv[++k], "%x", &Resync_Hamming_data );
      Resync_Hamming_data&=0xffffff;
      continue;
    }
    else if(strcmp(argv[k],"-display_min") == 0)
    {
      sscanf( argv[++k], "%lf", &display_min );
      continue;
    }
    else if(strcmp(argv[k],"-display_max") == 0)
    {
      sscanf( argv[++k], "%lf", &display_max );
      continue;
    }
    else if(strcmp(argv[k],"-enable_40MHz_out") == 0)
    {
      sscanf( argv[++k], "%d", &enable_40MHz_out );
      if(enable_40MHz_out>1)enable_40MHz_out=1;
      continue;
    }
    else if(strcmp(argv[k],"-wait_for_ever") == 0)
    {
      sscanf( argv[++k], "%d", &wait_for_ever );
      continue;
    }
    else if(strcmp(argv[k],"-VICEPP_clk") == 0)
    {
      sscanf( argv[++k], "%x", &VICEPP_clk );
      continue;
    }
    else if(strcmp(argv[k],"-ch_debug") == 0)
    {
      sscanf( argv[++k], "%x", &ch_debug );
      continue;
    }
    else if(strcmp(argv[k],"-read_conf_from_file") == 0)
    {
      sscanf( argv[++k], "%x", &read_conf_from_file );
      continue;
    }
    else if(strcmp(argv[k],"-Vref_ref") == 0)
    {
      sscanf( argv[++k], "%lf", &Vref_ref );
      continue;
    }
    else if(strcmp(argv[k],"-h") == 0)
    {
      printf("Start DAQ with : \n");
      printf("-debug debug_level        : Set debug level for this session [0]\n");
      printf("-debug_draw n             : Draw (1) or not (0) DAQ plots at each event [0]\n");
      printf("-nevt n                   : Number of events to record  [1000]\n");
      printf("-nsample n                : Number of sample per event (max=28670) [150]\n");
      printf("-trigger type n           : 0=pedestal, 1=calibration, 2=laser [0]\n");
      printf("-self_trigger_loop n      : 0=externally triggered DAQ from FE, 1=use internally generated trigger from data [0]\n");
      printf("-self_trigger n           : 1=internal generated trigger if signal > threshold [0]\n");
      printf("-self_trigger_mode n      : 0=trig on absolute level, 1=trig on delta signal [0]\n");
      printf("-self_trigger_threshold n : minimal signal amplitude to generate self trigger [0]\n");
      printf("-self_trigger_mask x      : channel mask to generate self triggers, ch0=lsb    [0x1F]\n");
      printf("-TP_mode 0/1              : 1=external trigger generates calibration trigger in VFE [0]\n");
      printf("-TP_width n               : width of the calibration trigger sent to VFE [128]\n");
      printf("-TP_delay n               : delay between calibration trigger and DAQ start [0]\n");
      printf("-TP_level n               : DAC level to start linearity study [32768]\n");
      printf("-n_TP_step n              : number of calibration steps for linearity study [1]\n");
      printf("-TP_step n                : DAC step for linearity study [128]\n");
      printf("-TP_gain n                : Conversion gain for current pulse injection (0=2471 Ohms, 1=272 Ohms) [0]\n");
      printf("-ADC_invert_clk n         : Invert clock polirity in ADC core [0]\n");
      printf("-ADC_calib_mode n         : Put (1) or not (0) CATIA in ADC_calib_mode (outputs near VCM) [0]\n");
      printf("-ADC_test_mode n          : Put (1) or not (0) iLiTE-DTU in test_mode (direct access to both ADCs outputs) [0]\n");
      printf("-reset_ADC n              : Reset ADCH (1), ADCL (2) or both (3) in boot procedure [0]\n");
      printf("-use_ref_ADC_calib n      : Load (1) ADC registers with result of previous calibration [0]\n");
      printf("-calib_ADC n              : Launch auto calibration of ADCH (1), ADCL (2) or both (3) in boot procedure [0]\n");
      printf("-CATIA_data n             : data to write in CATIA register [-1]\n");
      printf("-CATIA_reg n              : CATIA register to read from/write in with I2C (1 to 6) [-1]\n");
      printf("                          : 1 : Slow Control reg [R/W]\n");
      printf("                          : 2 : SEU error counter reg [R]\n");
      printf("                          : 3 : TIA reg [R/W]\n");
      printf("                          : 4 : Inj DAC1 reg [R/W]\n");
      printf("                          : 5 : Inj DAC2 reg [R/W]\n");
      printf("                          : 6 : Inj Ctl reg [R/W]\n");
      printf("-CATIA_Reg1_def x         : default content of CATIA register 2 [0x02]\n");
      printf("-CATIA_Reg3_def x         : default content of CATIA n register 3 [0x11087]\n");
      printf("-CATIA_Reg4_def x         : default content of CATIA register 4 [0x1000]\n");
      printf("-CATIA_Reg5_def x         : default content of CATIA register 5 [0x0000]\n");
      printf("-CATIA_Reg6_def x         : default content of CATIA register 6 [0x0b]\n");
      printf("-ped_mux n                : force mux to be (1) or not (0) in DAC position [0]\n");
      printf("-TP_step n                : external DAC step for INL/DNL/missing codes study [-1]\n");
      printf("                            Increment/decrement DAC value at each event. Force multiplexer in CALIB position\n");
      printf("-I2C_dir n                : Read (2) + write (1) with I2C protocol. 0=don't use\n");
      printf("-I2C_long                 : Perform long (1) or short (0) I2C transfer [0]\n");
      printf("-static_DTU_reset         : Try I2C under (1) or not (0) DTU reset state [0]\n");
      printf("-init_DTU                 : Send (1) DTU init sequence or not (0) [1]\n");
      printf("-pwup_reset               : Generate a power up reset to VFE at startup [0]\n");
      printf("-IO_reset                 : Reset IO timing at startup [0]\n");
      printf("-reset_all                : Generate Pwup_reset, IO_reset, init_DTU, reset_ADC and calib_ADC [0]\n");
      printf("-LVRB_autoscan            : Run LVRB autoscan mode (1) or not (0) [0]\n");
      printf("-I2C_toggle_SDA           : Toggle SDA line on DTU I2C bus during idle time [0]\n");
      printf("-invert_Resync            : Invert (1) or not (0) Resync signal polarities [0]\n");
      printf("-Resync_phase             : Set the Phase of the resync signal wrt 160 MHz clock by 90 deg step (0..3) [0]\n");
      printf("-eLink_active             : Bit pattern of active eLinks (0x1f for full VFE board, 0x01 for test board and 0x0f for test board in test mode) [0x1f]\n");
      printf("-DTU_force_G1             : Force G1 data at ADC outputs [0]\n");
      printf("-DTU_force_G10            : Force G10 data at ADC outputs [0]\n");
      printf("-Resync_Hamming_data      : Send a custom ReSync code (3 bytes) [-1]\n");
      printf("-delay                    : Number of us to wait between cycles [100]\n");
      printf("-shift_oddH_samples n     : Shift odd samples by n units with respect to even samples for ADCH [0]\n");
      printf("-shift_oddL_samples n     : Shift odd samples by n units with respect to even samples for ADCL [0]\n");
      printf("-wait_for_ever      n     : Time out (0) or wait for ever (1) for a trigger [0]\n");
      printf("-corgain            n     : Correct (1) or not (0) G1 samples for gain ratio (10.5) [1]\n");
      printf("-VICEPP_clk         x     : Set the clock configurtaion of VICE++ board [0x0]\n");
      printf("                            0 = Use local oscillator for all clocks\n");
      printf("                            1 = Transmit clock fro FE board to VFE\n");
      printf("                            (xxxx)<<2|2 = Set custom clcok config for both cross-points : 18 bits word in hex = 2*8 bit+10/n");
      printf("-PLL_conf n0xxx           : Set PLL configuration register of DTU ich with value [0x1e]\n");
      printf("-baseline_G10 n00xx       : Set G10 baseline substraction register content for DTU n [0x00]\n");
      printf("-baseline_G1 n00xx        : Set G1 baseline substraction register content for DTU n [0x00]\n");
      printf("-ch_debug n               : Put debug info for channel n in DEBUGx registers [1]\n");
      printf("-read_conf_from_file n    : Read (1) or not (0) VFE configuration from external file (VFE_config.txt) [0]\n");

      exit(-1);
    }
    else
    {
      printf("---------------------------------------------------\n");
      printf("Option %s not defined !, stop here.\n",argv[k]);
      printf("---------------------------------------------------\n");
      exit(-99);
    }
  }
      
  printf("Start DAQ with cards : %d\n", fead);
  printf("Will address catias : %d with subaddress 3\n",(Channel_num<<I2C_shift_dev_number));
  printf("            and DTU : %d with subaddresses 0,1,2\n",(Channel_num<<I2C_shift_dev_number));

  nsample_save=nsample;

  DTU_bulk1=(DTU_bulk1&0xffff2fbf) | (ADC_invert_clk<<12) | ((DTU_force_G1*3)<<14)  | ((DTU_force_G10*1)<<14) | (enable_40MHz_out<<6);
  //DTU_bulk1=(DTU_bulk1&0xffff2fbf) | (ADC_invert_clk<<12) | (enable_40MHz_out<<6);
  if(ADC_test_mode==0) DTU_bulk1&=0xfffffff1;
  printf("Enabling 40 MHz clock : %d, 0x%8.8x\n",enable_40MHz_out, DTU_bulk1);
  printf("Parameters : \n");
  printf("Read ADCs for :\n");
  printf("  %d events \n",nevent);
  printf("  %d samples \n",nsample);
  printf("  trigger type  : %d (0=pedestal, 1=TP, 2=laser)\n",trigger_type);
  printf("  trigger loop  : %d (0=triggered from FE, 1=self triggered DAQ from data)\n",self_trigger_loop);
  printf("  self trigger  : %d (1=internal generated trigger if signal > threshold), mode : %d\n",self_trigger, self_trigger_mode);
  printf("  threshold     : %d (minimal signal amplitude to generate self trigger)\n",self_trigger_threshold);
  printf("  mask          : 0x%x (channel mask to generate self triggers)\n",self_trigger_mask);

  if(trigger_type==1)
  {
    printf("Generate TP triggers :\n");
    printf("  %d events \n",nevent);
    printf("  TP_width   : %d (width of the calibration trigger sent to VFE)\n",TP_width);
    printf("  TP_delay   : %d (delay between calibration trigger and DAQ start)\n",TP_delay);
    printf("  n_TP_steps : %d (number of calibration steps for linearity study)\n",n_TP_step);
    printf("  TP_step    : %d (DAC step for linearity study)\n",TP_step);
  }
  if(n_TP_step==0)n_TP_step=1;

  for(int ich=0; ich<5; ich++)
  {
    
    channel_number[ich]=-1;
    CATIA_version[ich] = 13;
    DTU_version[ich]   = 12;
    DTU_test_mode[ich] = 0;
    delay_val[ich]     = 0; 
    bitslip_val[ich]   = 0;  
    byteslip_val[ich]  = 0; 
    //ped_G1[ich]=-1.;
    //ped_G10[ich]=-1.;
  }
  ped_G10[0]=13.41;
  ped_G10[1]=16.08;
  ped_G10[2]=17.48;
  ped_G10[3]=16.16;
  ped_G10[4]=15.34;
  ped_G1[0]=14.51;
  ped_G1[1]= 9.93;
  ped_G1[2]=15.12;
  ped_G1[3]=14.81;
  ped_G1[4]=12.30;

  Int_t loc_argc=1;
  char *loc_argv[10];
  for(int i=0; i<10; i++)loc_argv[i]=(char *)malloc(132*sizeof(char));
  sprintf(loc_argv[0],"test");
  TApplication *Root_App=new TApplication("test", &loc_argc, loc_argv);
  gStyle->SetPadGridX(kTRUE);
  gStyle->SetPadGridY(kTRUE);

  if(read_conf_from_file==1)
  {
    char *line=NULL, fname[132];
    size_t len;
    Int_t loc_CATIA_version, loc_DTU_version;
    sprintf(fname,"VFE_config_%2.2d.txt",fead);
    FILE *fconf=fopen(fname,"r");
    Int_t eof=0;
    while((eof=getline(&line, &len, fconf)) != EOF)
    {   
      char c;
      int iret=sscanf(line,"%c",&c);
      if(c=='#')continue;
      if(c=='0')
      {
        sscanf(line,"%*d %x\n",&VICEPP_clk);
        if((VICEPP_clk&3)==3)VICEPP_clk=-1;
      }
      else
      {
        Int_t loc_ich, loc_ped10, loc_ped1, loc_bs10, loc_bs1, loc_pll, loc_Vref_reg, loc_CATIA_gain, loc_CATIA_LPF, loc_DTU_test_mode, loc_DTU_force_PLL;
        sscanf(line,"%d %d %d %x %x %x %x %x %d %d %d %d %d\n",
               &loc_ich, &loc_CATIA_version,&loc_DTU_version,&loc_ped10,&loc_ped1, &loc_bs10, &loc_bs1, &loc_pll,
               &loc_Vref_reg, &loc_CATIA_gain, &loc_CATIA_LPF, &loc_DTU_test_mode, &loc_DTU_force_PLL);
        if(loc_ich>=1 && loc_ich<=5)
        {
          baseline_G10[loc_ich-1] = loc_bs10;
          baseline_G1[loc_ich-1]  = loc_bs1;
          pll_conf[loc_ich-1]     = loc_pll&0x01ff;
          pll_conf1[loc_ich-1]    = pll_conf[loc_ich-1]&0x7;
          pll_conf2[loc_ich-1]    = pll_conf[loc_ich-1]>>3;
          CATIA_version[loc_ich-1]= loc_CATIA_version;
          CATIA_gain[loc_ich-1]   = loc_CATIA_gain;
          CATIA_LPF[loc_ich-1]    = loc_CATIA_LPF;
          DTU_version[loc_ich-1]  = loc_DTU_version;
          DTU_test_mode[loc_ich-1]= loc_DTU_test_mode;
          DTU_force_PLL[loc_ich-1]= loc_DTU_force_PLL;
          if(CATIA_version[loc_ich-1]>=14)DAC_Vref_best[loc_ich-1]=loc_Vref_reg;
          if(loc_CATIA_version>=14)
          {
            I2C_shift_dev_number=3;
            CATIA_Reg3_def[loc_ich-1]=(loc_CATIA_gain<<14) | (loc_ped1<<8) | (loc_ped10<<2) | (loc_CATIA_LPF<<1) | 1; // 340_Ohms/Ped_G1/Ped_G10/LPF_on/1.2V
          }
          else
          {
            CATIA_Reg3_def[loc_ich-1]=(loc_ped1<<8) | (loc_ped10<<3) | (loc_CATIA_gain<<2) | (loc_CATIA_LPF<<1) | 1;
          }
          printf("Load config for channel %d : CATIA version : %.1f, LiTE-DTU version %.1f, reg3 0x%4.4x, bs10 0x%3.3x, bs1 0x%3.3x, pll 0x%3.3x=0x%2.2x 0x%1.1x, Vref %d, CATIA gain %d, CATIA LPF %d, DTU_force_PLL %d\n",
                 loc_ich,CATIA_version[loc_ich-1]/10.,DTU_version[loc_ich-1]/10.,
                 CATIA_Reg3_def[loc_ich-1],baseline_G10[loc_ich-1],baseline_G1[loc_ich-1],pll_conf[loc_ich-1],pll_conf1[loc_ich-1],pll_conf2[loc_ich-1], DAC_Vref_best[loc_ich-1],
                 CATIA_gain[loc_ich-1], CATIA_LPF[loc_ich-1], DTU_force_PLL[loc_ich-1]);
          channel_number[n_active_channel++]=loc_ich-1;
          if(CATIA_version[loc_ich-1]==14)calib_mux_enabled=1;
        }
      }
    }
    fclose(fconf);
  }
  if(n_active_channel==1 && ADC_test_mode==1)eLink_active=0xF;

  if(trigger_type==0)
    sprintf(output_file,"data/CATIA_V1.4/ped_data.root");
  else if(trigger_type==1)
    sprintf(output_file,"data/CATIA_V1.4/TP_data.root");
  else if(trigger_type==2)
    sprintf(output_file,"data/CATIA_V1.4/laser_data.root");
  else
    sprintf(output_file,"data/CATIA_V1.4/fead_data.root");
  fd=new TFile(output_file,"recreate");

  c1=new TCanvas("c1","c1",1000,0,800.,1000.);
  c1->Divide(2,3);
  double rms[6];
  char hname[80];
  Int_t dac_val=TP_level;
  for(int ich=0; ich<6; ich++)
  {
    printf("ich = %d\n",ich);
    tg[ich] = new TGraph();
    tg[ich]->SetMarkerStyle(20);
    tg[ich]->SetMarkerSize(0.5);
    tg_g1[ich] = new TGraph();
    tg_g1[ich]->SetMarkerStyle(20);
    tg_g1[ich]->SetMarkerSize(1.0);
    sprintf(hname,"mean_ch%d",ich);
    hmean[ich]=new TH1D(hname,hname,100,150.,250.);
    sprintf(hname,"rms_ch%d",ich);
    hrms[ich]=new TH1D(hname,hname,200,0.,2.);
    sprintf(hname,"code_density_ch%d",ich);
    hdensity[ich]=new TH1D(hname,hname,4096,-0.5,4095.5);
    dac_val=TP_level;
    for(int istep=0; istep<n_TP_step; istep++)
    {
      sprintf(hname,"ch_%d_step_%d_%d",ich,istep,dac_val);
      pshape[istep][ich]=new TProfile(hname,hname,nsample*5,0.,6.25*nsample*5);
      dac_val+=TP_step;
    }
  }
  dac_val=TP_level;
  Int_t nch=5;
  if(ADC_test_mode==1 && eLink_active==0x0F)
  {
    tg[0]->SetLineColor(kCyan);
    tg[1]->SetLineColor(kCyan);
    tg[2]->SetLineColor(kMagenta);
    tg[3]->SetLineColor(kMagenta);
    tg[4]->SetLineColor(kBlue);
    tg[5]->SetLineColor(kRed);
    tg[0]->SetMarkerColor(kCyan);
    tg[1]->SetMarkerColor(kCyan);
    tg[2]->SetMarkerColor(kMagenta);
    tg[3]->SetMarkerColor(kMagenta);
    tg[4]->SetMarkerColor(kBlue);
    tg[5]->SetMarkerColor(kRed);
    tg_g1[0]->SetLineColor(kCyan);
    tg_g1[1]->SetLineColor(kCyan);
    tg_g1[2]->SetLineColor(kMagenta);
    tg_g1[3]->SetLineColor(kMagenta);
    tg_g1[4]->SetLineColor(kBlue);
    tg_g1[5]->SetLineColor(kRed);
    tg_g1[0]->SetMarkerColor(kCyan);
    tg_g1[1]->SetMarkerColor(kCyan);
    tg_g1[2]->SetMarkerColor(kMagenta);
    tg_g1[3]->SetMarkerColor(kMagenta);
    tg_g1[4]->SetMarkerColor(kBlue);
    tg_g1[5]->SetMarkerColor(kRed);
    loc_ch[0]=1;
    loc_ch[1]=3;
    loc_ch[2]=2;
    loc_ch[3]=4;
    loc_ch[4]=5;
    loc_ch[5]=6;
    nch=6;
  }
  else
  {
    tg[0]->SetLineColor(kCyan);
    tg[1]->SetLineColor(kBlue);
    tg[2]->SetLineColor(kMagenta);
    tg[3]->SetLineColor(kRed);
    tg[4]->SetLineColor(kGreen);
    tg[0]->SetMarkerColor(kCyan);
    tg[1]->SetMarkerColor(kBlue);
    tg[2]->SetMarkerColor(kMagenta);
    tg[3]->SetMarkerColor(kRed);
    tg[4]->SetMarkerColor(kGreen);
    tg_g1[0]->SetLineColor(kCyan);
    tg_g1[1]->SetLineColor(kBlue);
    tg_g1[2]->SetLineColor(kMagenta);
    tg_g1[3]->SetLineColor(kRed);
    tg_g1[4]->SetLineColor(kGreen);
    tg_g1[0]->SetMarkerColor(kCyan);
    tg_g1[1]->SetMarkerColor(kBlue);
    tg_g1[2]->SetMarkerColor(kMagenta);
    tg_g1[3]->SetMarkerColor(kRed);
    tg_g1[4]->SetMarkerColor(kGreen);
  }
  c1->Update();


  ConnectionManager manager ( "file://xml/FEAD/connection_file.xml" );
  char fead_str[80];
  sprintf(fead_str,"fead.udp.%d",fead);
  uhal::HwInterface hw=manager.getDevice( fead_str );

  ValWord<uint32_t> free_mem;
  ValWord<uint32_t> trig_reg;
  ValWord<uint32_t> delays;
  ValWord<uint32_t> reg;
  ValWord<uint32_t> debug1_reg[32];
  ValWord<uint32_t> debug2_reg[32];


  unsigned int VFE_control= INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                            PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                            ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode | ADC_MEM_MODE*ADC_MEM_mode;
// Set the clock tree :
  if(VICEPP_clk >=0)
  {
    reg = hw.getNode("VICEPP_CLK").read();
    hw.dispatch();
    printf("Clock configuration aleady present : 0x%x\n",reg.value());
    if((reg.value()&3) == (VICEPP_clk&3))
    {
      printf("Present XPoint setting 0x%8.8x already conform to request 0x%8.8x\n",reg.value(),VICEPP_clk);
      printf("Do nothing !\n");
    }
    else
    {
      printf("Present XPoint setting 0x%8.8x not conform to request 0x%8.8x\n",reg.value(),VICEPP_clk);
      printf("Set clock configuration for VICE++ board : 0x%8.8x\n",VICEPP_clk);
      hw.getNode("VICEPP_CLK").write(VICEPP_clk);
      hw.dispatch();
      usleep(100000);
      pwup_reset = 1; // Force LiTE-DTU reset if clock has changed
    }
  }

// Reset VFE board
  if(pwup_reset==1)
  {
    printf("Generate PowerUp reset\n");
    hw.getNode("VFE_CTRL").write(1*PWUP_RESETB);
    hw.getNode("VFE_CTRL").write(0*PWUP_RESETB);
    hw.dispatch();
    usleep(10000);
  }

  //hw.getNode("DEBUG1_0").write(ch_debug);
  //hw.dispatch();

// Init stage :
// Read FW version to check :
  reg = hw.getNode("FW_VER").read();
  hw.dispatch();
  firmware_version=reg.value();
// Switch to triggered mode + external trigger :
  command= 
             CALIB_MUX_ENABLED     *calib_mux_enabled                |
             SELF_TRIGGER_MODE     *self_trigger_mode                |
            (SELF_TRIGGER_MASK     *(self_trigger_mask&0x1F))        |
            (SELF_TRIGGER_THRESHOLD*(self_trigger_threshold&0xFFF))  |
            //(CLOCK_PHASE           *(clock_phase&0x7))               |
             SELF_TRIGGER          *self_trigger                     |
             SELF_TRIGGER_LOOP     *self_trigger_loop                |
             TRIGGER_MODE          *(trigger_mode&1)                 | // Always DAQ on trigger
             RESET                 *0;
  hw.getNode("FEAD_CTRL").write(command);
// Stop DAQ and ask for NSAMPLE per frame (+timestamp) :
  command = ((nsample+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
// Add laser latency before catching data ~ 40 us
  hw.getNode("TRIG_DELAY").write(SW_DAQ_DELAY*sw_DAQ_delay | HW_DAQ_DELAY*hw_DAQ_delay);
// Switch off FE-adapter LEDs
  command = TP_MODE*0+LED_ON*1+GENE_100HZ*0+GENE_TP*0+GENE_TRIGGER*0;
  hw.getNode("FW_VER").write(command);
  hw.dispatch();
// Set the clock phases
  command = MEM_CLOCK_PHASE*mem_clock_phase+REG_CLOCK_PHASE*reg_clock_phase+IO_CLOCK_PHASE*IO_clock_phase+SEQ_CLOCK_PHASE*seq_clock_phase;
  hw.getNode("CLK_SETTING").write(command);
  hw.dispatch();

// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
// Start DAQ :
  command = ((nsample+1)<<16)+CAPTURE_START;
  hw.getNode("CAP_CTRL").write(command);
// Read back delay values :
  delays=hw.getNode("TRIG_DELAY").read();
// Read back the read/write base address
  address = hw.getNode("CAP_ADDRESS").read();
  free_mem = hw.getNode("CAP_FREE").read();
  trig_reg = hw.getNode("FEAD_CTRL").read();
  hw.dispatch();

  //firmware_version=0x21083100;
  printf("Firmware version      : %8.8x\n",firmware_version);
  printf("Delays                : %8.8x\n",delays.value());
  printf("Initial R/W addresses : 0x%8.8x\n", address.value());
  printf("Free memory           : 0x%8.8x\n", free_mem.value());
  printf("Trigger mode          : 0x%8.8x\n", trig_reg.value());
  old_read_address=address&0xffff;
  if(old_read_address==NSAMPLE_MAX-1)old_read_address=-1;

// Put FEAD outputs with idle patterns :
  hw.getNode("OUTPUT_CTRL").write(0);
  hw.dispatch();
  
// TP trigger setting :
  command=(TP_delay<<16) | (TP_width&0xffff);
  printf("TP trigger with %d clocks width and %d clocks delay : %x\n",TP_width,TP_delay,command);
  hw.getNode("CALIB_CTRL").write(command);
  hw.dispatch();

// Put DTU resync in reset mode, stop uLVRB autoscan mod
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.getNode("RESYNC_IDLE").write(I2C_toggle_SDA*I2C_TOGGLE_SDA  | RESYNC_IDLE_PATTERN*Resync_idle_pattern | ADC_INVERT_DATA*ADC_invert_data );
  hw.getNode("CLK_SETTING").write(resync_phase*RESYNC_PHASE);
  hw.dispatch();
  usleep(200);

  if(init_DTU==1)
  {
    printf("Prepare LiTEDTU for safe running (generate ReSync start sequence)\n");
// DTU Resync init sequence:
    if(firmware_version<0x21090000)
    {
      hw.getNode("DTU_RESYNC").write(LiTEDTU_DTU_reset);
      hw.dispatch();
      hw.getNode("DTU_RESYNC").write(LiTEDTU_I2C_reset);
      hw.dispatch();
      hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCTestUnit_reset);
      hw.dispatch();
      hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCH_reset);
      hw.dispatch();
      hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCL_reset);
      hw.dispatch();
    }
    else
    {
      hw.getNode("DTU_RESYNC").write((LiTEDTU_ADCTestUnit_reset<<16) | (LiTEDTU_I2C_reset<<8) | LiTEDTU_DTU_reset);
      hw.getNode("DTU_RESYNC").write((LiTEDTU_ADCL_reset<<8) | LiTEDTU_ADCH_reset);
      hw.dispatch();
    }
    hw.getNode("DTU_BULK1").write(DTU_bulk1);
    hw.getNode("DTU_BULK2").write(DTU_bulk2);
    DTU_bulk3=(DTU_bulk3&0xffff00fe)|((pll_conf[2]&0xFF)<<8)|((pll_conf[2]&0x100)>>8);
    hw.getNode("DTU_BULK3").write(DTU_bulk3);
    hw.getNode("DTU_BULK4").write(DTU_bulk4);
    hw.getNode("DTU_BULK5").write(DTU_bulk5);
    hw.dispatch();

// Load default DTU registers
// Surprisingly, when we load LiTE-DTU registers, we have a calibration process. So put ped-mux in position :
    VFE_control= DELAY_AUTO_TUNE*delay_auto_tune | INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                 PED_MUX*1 | eLINK_ACTIVE*eLink_active |
                 ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode | ADC_MEM_MODE*ADC_MEM_mode;
    hw.getNode("VFE_CTRL").write(VFE_control);
    hw.dispatch();
    usleep(100000); // Let some time for calibration voltages to stabilize
    UInt_t val;
    for(int i=0; i<n_active_channel; i++)
    {
      Int_t data;
      Int_t ich=channel_number[i];
      device_number=I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+2;      // DTU sub-address
      printf("Device number : %d 0x%x\n",device_number,device_number);

      for(Int_t ireg=0; ireg<=0x19; ireg++)
      {
        iret=I2C_RW(hw, device_number, ireg, 0, 0, 2, 0);
        printf("LiTEDTU %d : default reg 0x%2.2x content : 0x%2.2x\n",ich,ireg,iret&0xFF);
      }

// Enable outputs
      printf("ch %d, DTU test mode : %d\n",ich,ADC_test_mode);
      if(ADC_test_mode==1)
        data=0x9F;
      else
        data=0x81;
      iret=I2C_RW(hw, device_number, 0, data, 0, 3, debug_I2C);
// Switch ON ADCs :
      data=0x03;
      iret=I2C_RW(hw, device_number, 1, data, 0, 3, debug_I2C);

      data=baseline_G10[ich];
      iret=I2C_RW(hw, device_number, 5, data, 0, 3, debug_I2C);
      data=baseline_G1[ich];
      iret=I2C_RW(hw, device_number, 6, data, 0, 3, debug_I2C);

      if(DTU_version[ich]<20)
      {
        printf("Setting G10/G1 gain switching level for channel %d to 0x%x\n",ich,data);
        data=0xff0-baseline_G10[ich]; // switch gain at 4080 - substrated baseline
        iret=I2C_RW(hw, device_number, 17, data&0xff, 0, 1, debug_I2C);
        iret=I2C_RW(hw, device_number, 18, (data>>8)&0xf, 0, 1, debug_I2C);

// PLL setting :
        iret=I2C_RW(hw, device_number, 9, (pll_conf[ich]&0xFF), 0, 1, 0);
        iret=I2C_RW(hw, device_number, 8, 0, 0, 2, 0);
        iret&=0xFF;
        iret=I2C_RW(hw, device_number, 8, (iret&0xFE)|(((pll_conf[ich]&0x100)>>8)&1), 0, 3, 0);
      }
      else
      {
// 00 9f [Main control registers]
// 01 03 [ADC control registers]
// 02 04 [CLPS drivers control]
// 03 20 [CLPS drivers pre-emphasis control]
// 04 00 [CLPS receivers control]
// 05 00 [Baseline subtraction H]
// 06 00 [Baseline subtraction L]
// 07 ff [ADC saturation value registers]
// 08 0f [Baseline subtraction H]
// 09 00 [Status register 0 - READ ONLY]
// 0a 3c [Status register 1 - READ ONLY]
// 0b 88 [Bias control register 0]
// 0c 44 [Bias control register 1]
// 0d 55 [Bias control register 2]
// 0e 55 [Bias control register 3]
// 0f 00 [Bias control register 4]
// 10 3c [Bias control register 5]
// 11 32 [PLL control register 0]
// 12 88 [PLL control register 1]
// 13 88 [PLL control register 2]
// 14 00 [PLL control register 3]
// 15 0c [Synchronization pattern 31-24]
// 16 cc [Synchronization pattern 23-16]
// 17 cc [Synchronization pattern 15-8]
// 18 cf [Synchronization pattern 7-0]
// 19 00 [Test pulse duration]

// PLL setting :
        iret=I2C_RW(hw, device_number, 15, (pll_conf[ich]&0xFF), 0, 1, 0);
        iret=I2C_RW(hw, device_number, 16, 0, 0, 2, 0);
        iret&=0xFF;
        iret=I2C_RW(hw, device_number, 16, (iret&0xFE)|(((pll_conf[ich]&0x100)>>8)&1), 0, 1, 0);
// PLL reg 0 :
        if(DTU_force_PLL[ich]==0)
          iret=I2C_RW(hw, device_number, 17, 0x32, 0, 1, 0);
        else
          iret=I2C_RW(hw, device_number, 17, 0x3e, 0, 1, 0);
// PLL reg 1 :
        iret=I2C_RW(hw, device_number, 18, 0x88, 0, 1, 0);
// PLL reg 2 :
        iret=I2C_RW(hw, device_number, 19, 0x88, 0, 1, 0);
// PLL reg 3 :
        iret=I2C_RW(hw, device_number, 20, 0x00, 0, 1, 0);

// Overwrite sync pattern word : 0xf0000000
        iret=I2C_RW(hw, device_number,21, 0x0c, 0, 1, debug_I2C);
        iret=I2C_RW(hw, device_number,22, 0xcc, 0, 1, debug_I2C);
        iret=I2C_RW(hw, device_number,23, 0xcc, 0, 1, debug_I2C);
        iret=I2C_RW(hw, device_number,24, 0xcf, 0, 1, debug_I2C);

        data=0xff0-baseline_G10[ich]; // switch gain at 4080 - substrated baseline
        printf("Setting G10/G1 gain switching level for channel %d to 0x%x\n",ich,data);
        iret=I2C_RW(hw, device_number, 7, data&0xff, 0, 3, debug_I2C);
        iret=I2C_RW(hw, device_number, 8, (data>>8)&0xf, 0, 3, debug_I2C);
      }
      for(Int_t ireg=0; ireg<=0x19; ireg++)
      {
        iret=I2C_RW(hw, device_number, ireg, 0, 0, 2, 0);
        printf("LiTEDTU %d : new content 0x%2.2x content : 0x%2.2x\n",ich,ireg,iret&0xFF);
      }
    }
    VFE_control= DELAY_AUTO_TUNE*delay_auto_tune | INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                 PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                 ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode | ADC_MEM_MODE*ADC_MEM_mode;
    hw.getNode("VFE_CTRL").write(VFE_control);
    hw.dispatch();
    usleep(100000); // Let some time for calibration voltages to stabilize
  }
  else
  {
    for(int i=0; i<n_active_channel; i++)
    {
      Int_t data;
      Int_t ich=channel_number[i];
      device_number=I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+2;      // DTU sub-address
      data=baseline_G10[ich];
      printf("Setting G10 baseline substraction for channel %d to 0x%x\n",ich,data);
      iret=I2C_RW(hw, device_number, 5, data, 0, 1, debug_I2C);
      data=baseline_G1[ich];
      printf("Setting G1 baseline substraction for channel %d to 0x%x\n",ich,data);
      iret=I2C_RW(hw, device_number, 6, data, 0, 1, debug_I2C);
      data=0xff0-baseline_G10[ich]; // switch gain at 4080 - substrated baseline
      printf("Setting G10/G1 gain switching level for channel %d to 0x%x\n",ich,data);
      if(DTU_version[ich]<20)
      {
        iret=I2C_RW(hw, device_number, 17, data&0xff, 0, 1, debug_I2C);
        iret=I2C_RW(hw, device_number, 18, (data>>8)&0xf, 0, 1, debug_I2C);
      }
      else
      {
        iret=I2C_RW(hw, device_number, 7, data&0xff, 0, 1, debug_I2C);
        iret=I2C_RW(hw, device_number, 8, (data>>8)&0xf, 0, 1, debug_I2C);
      }
    }
  }

  //printf("Send PLL_init signal\n");
  //hw.getNode("DTU_RESYNC").write(LiTEDTU_DTU_reset);
  //hw.dispatch();
  //usleep(1000);
  reg = hw.getNode("RESYNC_IDLE").read();
  hw.dispatch();
  printf("PLL lock values : 0x%x\n",(reg.value()>>9)&0x1f);
  for(int i=0; i<n_active_channel; i++)
  {
    Int_t data;
    Int_t ich=channel_number[i];
    device_number=I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+2;      // DTU sub-address
    if(DTU_version[ich]>=20)
    {
      iret=I2C_RW(hw, device_number, 9, 0, 0, 2, debug_I2C);
      printf("PLL status registers 0x09 : 0x%2.2x, ",iret&0xff);
      iret=I2C_RW(hw, device_number, 10, 0, 0, 2, debug_I2C);
      printf("0x0a : 0x%2.2x\n",iret&0xff);
    }
  }

// Once the PLL is set, reset ISERDES instances and get ready for synchronization
  if(IO_reset==1)
  {
    printf("Reset IDELAY\n");
    hw.getNode("DELAY_CTRL").write(DELAY_TAP_DIR*1 | DELAY_RESET*1);
    printf("Get lock status of IDELAY input stages : ");
    reg = hw.getNode("DELAY_CTRL").read();
    hw.dispatch();
    usleep(1000);
    printf("0x%x\n",reg.value());
// Launch link synchronization with LiTE-DTU-v2.0 :
    if(DTU_version[2]>=20)
    {
      printf("Synchronize links\n");
      hw.getNode("DTU_SYNC_PATTERN").write(0x0ccccccf);
      hw.getNode("DTU_RESYNC").write(LiTEDTU_SYNC_MODE);
      reg = hw.getNode("DTU_SYNC_PATTERN").read();
      hw.dispatch();
      printf("DTU SYNC pattern : 0x%8.8x\n",reg.value());

      hw.getNode("DELAY_CTRL").write(DELAY_TAP_DIR*1 | START_IDELAY_SYNC*0x1f);
      hw.dispatch();
      usleep(1000);
      hw.getNode("DELAY_CTRL").write(DELAY_TAP_DIR*1);
      hw.dispatch();
      hw.getNode("DTU_RESYNC").write(LiTEDTU_NORMAL_MODE);
      hw.dispatch();
      usleep(1000);
      hw.getNode("DELAY_CTRL").read();
      reg = hw.getNode("DELAY_CTRL").read();
      hw.dispatch();
      printf("Get sync status : 0x%8.8x, 0x%2.2x 0x%2.2x 0x%2.2x 0x%2.2x 0x%2.2x\n", reg.value(),
             (reg.value()>>0)&0x3f,(reg.value()>>6)&0x3f,(reg.value()>>12)&0x3f,(reg.value()>>18)&0x3f,(reg.value()>>24)&0x3f);
    }
    else
    {
      hw.getNode("DTU_SYNC_PATTERN").write(0xeaaaaaaa);
      reg = hw.getNode("DTU_SYNC_PATTERN").read();
      hw.dispatch();
      printf("DTU SYNC pattern : 0x%8.8x\n",reg.value());
    }
  }

  if(calib_ADC>0)
  {
    if(delay_auto_tune==1 && DTU_version[2]<20)
    {    
      hw.getNode("DELAY_CTRL").write(DELAY_TAP_DIR*1 | DELAY_RESET*1);
      hw.dispatch();
    }    

// Set CATIA output mux for calibation :
// and read Vref value with XADC.
// First : disconnect temp measurement
// Next : Set Output mux
// Then : Set Vref_mux for measurement

// Read XADC register 0x40 and set the requested average to 1 in XADC
    command=DRP_WRb*0 | (0x40<<16);
    hw.getNode("DRP_XADC").write(command);
    ValWord<uint32_t> ave  = hw.getNode("DRP_XADC").read();
    hw.dispatch();
    unsigned loc_ave=ave.value()&0xffff;
    printf("Old config register 0x40 content : %x\n",loc_ave);

    loc_ave=0x8000;
    command=DRP_WRb*1 | (0x40<<16) | loc_ave;
    hw.getNode("DRP_XADC").write(command);
    hw.dispatch();
    command=DRP_WRb*0 | (0x40<<16);
    hw.getNode("DRP_XADC").write(command);
    ave  = hw.getNode("DRP_XADC").read();
    hw.dispatch();
    loc_ave=ave.value()&0xffff;
    printf("New config register 0x40 content : %x\n",loc_ave);
    command=DRP_WRb*1 | (0x42<<16) | 0x0400;
    hw.getNode("DRP_XADC").write(command);
    hw.dispatch();

    for(int i=0; i<n_active_channel; i++)
    {
      Int_t ich=channel_number[i];
      device_number=I2C_CATIA_type*1000+((ich+1)<<I2C_shift_dev_number)+3;
      if(ich==4 && I2C_shift_dev_number==4)   device_number = I2C_CATIA_type*1000+((ich+1)<<I2C_shift_dev_number)+0xb;

// SEU auto correction, no Temp output
      val=I2C_RW(hw, device_number, 1, CATIA_Reg1_def, 0, 3, debug_I2C);
      printf("Put CATIA %d Reg1 content to 0x%x : 0x%x\n",ich+1,CATIA_Reg1_def,val);
    }    

    for(int i=0; i<n_active_channel; i++)
    {
      Int_t ich=channel_number[i];
      device_number=I2C_CATIA_type*1000+((ich+1)<<I2C_shift_dev_number)+3;
      if(ich==4 && I2C_shift_dev_number==4)   device_number = I2C_CATIA_type*1000+((ich+1)<<I2C_shift_dev_number)+0xb;

      if(CATIA_version[ich]>=14)
      {
// Set CATIA outputs with calibration levels
        iret=I2C_RW(hw, device_number, 5, 0x7000, 1, 3, debug_I2C);
        printf("Set CATIA outputs with calibration levels : 0x%4.4x\n\n",iret&0xFFFF);

        if(CATIA_scan_Vref==1)
        {
          printf("Scan CATIA setting to reach Vref_reg = %.3f\n",Vref_ref);
// present Vref to XADC
          if(CATIA_version[ich]==14)
          {
            printf("Setting Vref mux for channel %d, with calib_mux_enabled %d\n",ich,calib_mux_enabled);
            hw.getNode("VREF_MUX_CTRL").write(1<<ich);
            hw.dispatch();
          }
          else
          {
            hw.getNode("VREF_MUX_CTRL").write(0); // Remove all signals from other channels if any
            hw.dispatch();
            printf("Present Vref on Temp line\n");
            val=I2C_RW(hw, device_number, 1, 0x02, 0, 3, debug_I2C); // switch ON temp output
            val=I2C_RW(hw, device_number, 5, 0x3000, 1, 3, debug_I2C); // switch ON Vref output
          }
          usleep(100000); // Let some time for calibration voltages to stabilize
// And read Vref value on temp line :
          Int_t XADC_Temp=0x11;
          ValWord<uint32_t> temp; 
          Int_t average=128,n_Vref=15;
          printf("CATIA %d : default Vref value from XADC = ",ich+1);
          Double_t Vref_best=0., dVref=99999.;
          DAC_Vref_best[ich]=0;
          for(int imeas=0; imeas<n_Vref; imeas++)
          {
            double ave_val=0.;
            Int_t loc_meas=imeas;
            if(imeas>0)loc_meas=imeas+1;
            command= (loc_meas<<4) | 0xF; 
            val=I2C_RW(hw, device_number, 6, command,0, 3, 0);
            usleep(10000);

            for(int iave=0; iave<average; iave++)
            {
              command=DRP_WRb*0 | (XADC_Temp<<16);
              hw.getNode("DRP_XADC").write(command);
              temp  = hw.getNode("DRP_XADC").read();
              hw.dispatch();
              double loc_val=double((temp.value()&0xffff)>>4)/4096.;
              //printf("%f\n",loc_val);
              ave_val+=loc_val;
            }
            ave_val/=average;
            ave_val/=0.738;
            if(fabs(ave_val-Vref_ref)<dVref)
            {
              dVref=fabs(ave_val-Vref_ref);
              DAC_Vref_best[ich]=loc_meas;
              Vref_best=ave_val;
            }
            printf(" %.2f",ave_val*1000.);
            //system("stty raw");
            //char cdum=getchar();
            //system("stty -raw");
          }
          //if(ich>0)DAC_Vref_best[ich]=4;
          printf(" mV, best=%.2f, DAC=%d\n",Vref_best*1000.,DAC_Vref_best[ich]);
        }
        command= (DAC_Vref_best[ich]<<4) | 0xF;
        val=I2C_RW(hw, device_number, 6, command,0, 3, 0);

        //exit(-1);
      }
    }

    Int_t Vref_mux_ctrl_done=0;
    for(Int_t i=0; i<n_active_channel; i++)
    {
      Int_t ich=channel_number[i];
      if(CATIA_version[channel_number[ich]]>=14 && Vref_mux_ctrl_done==0)
      {
// Put back in High Z
        hw.getNode("VREF_MUX_CTRL").write(0);
        hw.dispatch();
        Vref_mux_ctrl_done=1;
      }
      else if(CATIA_version[channel_number[ich]]==20)
      {
        for(Int_t i=0; i<n_active_channel; i++)
        {
          Int_t ich=channel_number[i];
          device_number=I2C_CATIA_type*1000+((ich+1)<<I2C_shift_dev_number)+3;
          val=I2C_RW(hw, device_number, 1, 0x02, 0, 3, debug_I2C); // switch OFF temp output
          val=I2C_RW(hw, device_number, 5, 0x7000, 1, 3, debug_I2C); // switch OFF Vref output
        }
      }
    }
    
    VFE_control= DELAY_AUTO_TUNE*delay_auto_tune | INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                 PED_MUX*1 | eLINK_ACTIVE*eLink_active |
                 ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode | ADC_MEM_MODE*ADC_MEM_mode;
    hw.getNode("VFE_CTRL").write(VFE_control);
    hw.dispatch();
    usleep(500000); // Let some time for calibration voltages to stabilize
    for(int i=0; i<n_active_channel; i++)
    {
      Int_t ich=channel_number[i];
      device_number=I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number);
      if(dither==1)
      {
        val=I2C_RW(hw, device_number+0, 3, 0x01, 0, 1, debug_I2C);
        val=I2C_RW(hw, device_number+1, 3, 0x01, 0, 1, debug_I2C);
      }
      if(nsample_calib_x4==1)
      {
        val=I2C_RW(hw, device_number+0, 1, 0xfe, 0, 1, debug_I2C);
        val=I2C_RW(hw, device_number+1, 1, 0xfe, 0, 1, debug_I2C);
      }
      if(global_test==1)
      {
        val=I2C_RW(hw, device_number+0, 0, 0x01, 0, 1, debug_I2C);
        val=I2C_RW(hw, device_number+1, 0, 0x01, 0, 1, debug_I2C);
      }
    }

    printf("FW : %x DTU %d\n",firmware_version,DTU_version[channel_number[0]]);
    if(firmware_version<0x21090000)
    {
      hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCL_reset);
      hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCH_reset);
      hw.dispatch();
      usleep(100);
      printf("Launch ADC calibration old !\n");
      hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCL_calib);
      hw.dispatch();
      hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCH_calib);
      hw.dispatch();
      usleep(1000);
    }
    else
    {
      hw.getNode("DTU_RESYNC").write((LiTEDTU_ADCL_reset<<8) | LiTEDTU_ADCH_reset);
      hw.dispatch();
      usleep(100);
      printf("Launch ADC calibration new !\n");
      hw.getNode("DTU_RESYNC").write((LiTEDTU_ADCL_calib<<8) | LiTEDTU_ADCH_calib);
      hw.dispatch();
      usleep(1000);
    }
    //system("stty raw");
    //char cdum=getchar();
    //system("stty -raw");
    //exit(-1);
    if(CATIA_version[channel_number[0]]>=14)
    {
      for(int i=0; i<n_active_channel; i++)
      {
        Int_t ich=channel_number[i];
        device_number=I2C_CATIA_type*1000+((ich+1)<<I2C_shift_dev_number)+3;
        iret=I2C_RW(hw, device_number, 5, CATIA_Reg5_def, 1, 3, debug_I2C);
      }
    }
  }

// Reset Test unit to get samples in right order :
  hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCTestUnit_reset);
  hw.dispatch();

  if(use_ref_ADC_calib==1)
  {
    printf("Loading ADC calibration coefficient from ref file\n");
    fcal=fopen("ref_VFE_calib_reg.dat","r");
    for(int ich=0; ich<5; ich++)
    {
      for(int ireg=0; ireg<75; ireg++)
      {
        fscanf(fcal,"%d %d\n",&ADC_reg_val[0][ireg], &ADC_reg_val[1][ireg]);
      }
      for(int iADC=0; iADC<2; iADC++)
      {
        device_number=I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+iADC;
        for(int ireg=0; ireg<75; ireg++)
        {
          iret=I2C_RW(hw, device_number, ireg, ADC_reg_val[iADC][ireg], 0, 1, debug_I2C);
        }
      }
    }
    fclose(fcal);
  }

// Restore CAL mux in normal position after calibration
  VFE_control= DELAY_AUTO_TUNE*delay_auto_tune | INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
               PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
               ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode | ADC_MEM_MODE*ADC_MEM_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();
// And wait for voltage stabilization
  usleep(100000);

  Long_t timestamp=0;
  tdata=new TTree("data","data");
  tdata->Branch("timestamp",&timestamp,"timestamp/l");
  for(int ich=0; ich<nch; ich++)
  {
    char bname[80], btype[80];
    sprintf(bname,"ch%d",ich);
    sprintf(btype,"ch%d[%d]/S",ich,nsample_save*5);
    tdata->Branch(bname,event[ich],btype);
  }

// Program CATIA according to wishes :
// Setup default values for CATIA:
  for(int i=0; i<n_active_channel; i++)
  {
    Int_t ich=channel_number[i];
    device_number = I2C_CATIA_type*1000+((ich+1)<<I2C_shift_dev_number)+0x3;
    if(ich==4 && I2C_shift_dev_number==4)   device_number = I2C_CATIA_type*1000+((ich+1)<<I2C_shift_dev_number)+0xb;

// SEU auto correction, no Temp output
    val=I2C_RW(hw, device_number, 1, CATIA_Reg1_def, 0, 3, debug_I2C);
    printf("Put CATIA %d Reg1 content to 0x%x : 0x%x\n",ich+1,CATIA_Reg1_def,val);

// Gain 400 Ohm, Output stage for 1.2V, LPF35, 0 pedestal
    val=I2C_RW(hw, device_number, 3, CATIA_Reg3_def[ich], 1, 3, debug_I2C);
    printf("Put CATIA %d Reg3 content to 0x%x : 0x%x\n",ich+1,CATIA_Reg3_def[ich],val);

// DAC1 0, DAC2 0, DAC1 ON, DAC2 OFF, DAC1 copied on DAC2
    val=I2C_RW(hw, device_number, 4, CATIA_Reg4_def, 1, 3, debug_I2C);
    printf("Put CATIA %d Reg4 content to 0x%x : 0x%x\n",ich+1,CATIA_Reg4_def,val);

// DAC2 mid scale but OFF, so should not matter
  //val=I2C_RW(hw, device_number, 5, 0xffff, 1, 3, debug_I2C);
// DAC2 at 0 but OFF, so should not matter
    val=I2C_RW(hw, device_number, 5, CATIA_Reg5_def, 1, 3, debug_I2C);
    printf("Put CATIA %d Reg5 content to 0x%x : 0x%x\n",ich+1,CATIA_Reg5_def,val);

// TIA dummy ON, Rconv 2471 (G10 scale), Vref ON (0xb) OFF (0x3), Injection in CATIA
//    if(trigger_type!=1)
//      CATIA_Reg6_def=0x0; // switch off every thing for pedestal events
//    else
      CATIA_Reg6_def=(DAC_Vref_best[ich]<<4) | 0x0b | (TP_gain<<2);
//      CATIA_Reg6_def= 0x0b | (TP_gain<<2);
//      CATIA_Reg6_def=0x09 | (TP_gain<<2);

    val=I2C_RW(hw, device_number, 6, CATIA_Reg6_def, 0, 3, debug_I2C);
    printf("Put CATIA %d Reg6 content to 0x%x : 0x%x\n",ich+1,CATIA_Reg6_def,val);
  }

  if(n_TP_step<0)exit(-1);

// Force G1 output if required :
  if(DTU_force_G1==1) printf("Force DTU output with G1 samples\n");
  else if(DTU_force_G10==1) printf("Force DTU output with G10 samples\n");
  else printf("Read data in DTU free gain mode\n");
  for(int i=0; i<n_active_channel; i++)
  {
    Int_t ich=channel_number[i];
    device_number=I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+2;
    iret=I2C_RW(hw, device_number, 1, 0, 0, 2, 0);
    iret&=0xFF;
    if(DTU_force_G1==1)
    {
      iret=I2C_RW(hw, device_number, 1, (iret&0x3F)|0xc0, 0, 1, 0);
    }
    else if(DTU_force_G10==1)
    {
      iret=I2C_RW(hw, device_number, 1, (iret&0x3F)|0x40, 0, 1, 0);
    }
    else if(DTU_G1_window16==1)
    {
      iret=I2C_RW(hw, device_number, 1, (iret&0x3F)|0x80, 0, 1, 0);
    }
    else
    {
      iret=I2C_RW(hw, device_number, 1, (iret&0x3F), 0, 1, 0);
    }
  }

// If we aim to do data taking, we start with one pedestal event with Gain 1 forced at ADC output in order to get the G1 ped value for offline analysis.
  if(ADC_test_mode==0 && debug==0 && DTU_force_G1==0)
  {
    printf("Take one event with G1 forced for offline analysis\n");
    // Force G1 output :
    for(int i=0; i<n_active_channel; i++)
    {
      Int_t ich=channel_number[i];
      device_number=I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+2;
      iret=I2C_RW(hw, device_number, 1, 0, 0, 2, 0);
      iret&=0xFF;
      iret=I2C_RW(hw, device_number, 1, (iret&0x3F)|0xc0, 0, 1, 0);
    }
    // Init DAQ
    command = ((nsample+1)<<16)+CAPTURE_STOP;
    hw.getNode("CAP_CTRL").write(command);
    hw.getNode("CAP_ADDRESS").write(0);
    command = ((nsample+1)<<16)+CAPTURE_START;
    hw.getNode("CAP_CTRL").write(command);
    hw.dispatch();

    DTU_force_G1=1;
    Int_t error=get_event(hw,0,nsample,0,-1);
    DTU_force_G1=0;
    for(int i=0; i<n_active_channel; i++)
    {
      Int_t ich=channel_number[i];
      ped_G1[ich]=0.;
      for(int isample=0; isample<all_sample[ich] && isample<NSAMPLE_PED; isample++)
      {
        ped_G1[ich]+=(double)(event[ich][isample]&0xfff);
      }
      ped_G1[ich]/=NSAMPLE_PED;
      printf("G1 pedestal for channel %d : %f\n",ich,ped_G1[ich]);
    }
    for(Int_t is=0; is<nsample*5; is++)
    {
      for(Int_t ich=0; ich<nch; ich++)
      {
        if(event[ich][is]>=0)event[ich][is]=(gain[ich][is]<<12) | event[ich][is];
      }
    }
    tdata->Fill();
    // Stop DAQ :
    command = ((nsample+1)<<16)+CAPTURE_STOP;
    hw.getNode("CAP_CTRL").write(command);
    hw.dispatch();
    // Release DTU selection
    for(int i=0; i<n_active_channel; i++)
    {
      Int_t ich=channel_number[i];
      device_number=I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+2;
      iret=I2C_RW(hw, device_number, 1, 0, 0, 2, 0);
      iret&=0xFF;
      iret=I2C_RW(hw, device_number, 1, iret&0x3F, 0, 1, 0);
    }
  }

// Send triggers and wait between each trigger :
  Int_t draw=debug_draw;
  for(int istep=0; istep<n_TP_step; istep++)
  {
// Program TP-DAC for this step
    for(int i=0; i<n_active_channel; i++)
    {
      Int_t ich=channel_number[i];
      device_number = I2C_CATIA_type*1000+((ich+1)<<I2C_shift_dev_number)+0x3;
      if(ich==4 && I2C_shift_dev_number==4)   device_number = I2C_CATIA_type*1000+((ich+1)<<I2C_shift_dev_number)+0xb;
      if(TP_level>=0)
      {
        val=I2C_RW(hw, device_number, 4, (CATIA_Reg4_def&0xf000) | (TP_level&0xfff),1, 3, debug_I2C);
        printf("Put %d in DAC register : 0x%x\n",TP_level,val);
      }
      else
      {
  // switch off all the injection system
        val=I2C_RW(hw, device_number, 4, 0x0,1, 3, debug_I2C);
      }
    }
// Wait for Vdac to stabilize :
    usleep(500000);

    Int_t ievt=0;
    if(trigger_type != 2)
      printf("start sending triggers :\n"); 
    else
      printf("start waiting for triggers :\n"); 
    while(ievt<nevent)
    {
      if((ievt%100)==0)printf("%d\n",ievt); 
      //if(ievt==0)
      if(debug>0 || ievt==0)
      {
// In debug mode, we reinit DAQ buffer at each event :
        command = ((nsample+1)<<16)+CAPTURE_STOP;
        hw.getNode("CAP_CTRL").write(command);
        hw.getNode("CAP_ADDRESS").write(0);
        command = ((nsample+1)<<16)+CAPTURE_START;
        hw.getNode("CAP_CTRL").write(command);
        hw.dispatch();
        old_read_address=-1;
      }
      //usleep(10000);
      Int_t error=get_event(hw,trigger_type,nsample,debug,draw);
      //Int_t error=get_event(hw,trigger_type,nsample,2,draw);

      double ped[6]={0.}, ave[6]={0.}, rms[6]={0.};
      double max=0.;
      for(int ich=0; ich<nch; ich++)
      {
        tg[ich]->Set(0);
        tg_g1[ich]->Set(0);
        ns_g1[ich]=0;
      }

      for(int ich=0; ich<nch; ich++)
      {
        Double_t dt=6.25;
        if(ADC_test_mode==1 && eLink_active==0x0F && ich<4)dt=12.5;
        for(int isample=0; isample<all_sample[ich]; isample++)
        {
          tg[ich]->SetPoint(isample,dt*isample,fevent[ich][isample]);
          if(gain[ich][isample]==1)tg_g1[ich]->SetPoint(ns_g1[ich]++,dt*isample,fevent[ich][isample]);
          ave[ich]+=dv*fevent[ich][isample];
          if(isample<NSAMPLE_PED)ped[ich]+=dv*fevent[ich][isample];
          rms[ich]+=dv*fevent[ich][isample]*dv*fevent[ich][isample];
          hdensity[ich]->Fill(event[ich][isample]);
          if(ich==1 && dv*event[ich][isample]>max)max=dv*event[ich][isample];
        }
      }
      for(int ich=0; ich<nch; ich++)
      {
        int loc_sample=all_sample[ich];
        ave[ich]/=loc_sample;
        ped[ich]/=NSAMPLE_PED;
        rms[ich]/=loc_sample;
        rms[ich]=sqrt(rms[ich]-ave[ich]*ave[ich]);
        if(debug>0)printf("ich %d : ped=%f, ave=%f, rms=%f\n",ich,ped[ich],ave[ich],rms[ich]);
        hmean[ich]->Fill(ave[ich]);
        hrms[ich]->Fill(rms[ich]);
        for(int isample=0; isample<all_sample[ich]; isample++)
        {
          pshape[istep][ich]->Fill(6.25*isample+1.,dv*fevent[ich][isample]-ped[ich]);
        }
      }
      if(trigger_type==0 || max>0.)
      {
        for(Int_t is=0; is<nsample*5; is++)
        {
          for(Int_t ich=0; ich<nch; ich++)
          {
            if(event[ich][is]>=0)event[ich][is]=(gain[ich][is]<<12) | event[ich][is];
          }
        }
        tdata->Fill();
        if((ngood_event%200)==0)printf("%d events recorded\n",ngood_event);
        ngood_event++;
      }
      //if(debug>0 && max>0.)
      if(debug>0 || (ievt%100)==1)
      {
        
        if(display_min>=0)
        {
          tg[0]->SetMinimum(display_min);
          tg[1]->SetMinimum(display_min);
          tg[2]->SetMinimum(display_min);
          tg[3]->SetMinimum(display_min);
          tg[4]->SetMinimum(display_min);
          if(ADC_test_mode==1 && eLink_active==0x0F) tg[5]->SetMinimum(display_min);
        }
        if(display_max>=0)
        {
          tg[0]->SetMaximum(display_max);
          tg[1]->SetMaximum(display_max);
          tg[2]->SetMaximum(display_max);
          tg[3]->SetMaximum(display_max);
          tg[4]->SetMaximum(display_max);
          if(ADC_test_mode==1 && eLink_active==0x0F) tg[5]->SetMaximum(display_max);
        }
        c1->cd(loc_ch[0]);
        tg[0]->Draw("alp");
        if(ns_g1[0]>0)tg_g1[0]->Draw("lp");
        c1->cd(loc_ch[1]);
        tg[1]->Draw("alp");
        if(ns_g1[1]>0)tg_g1[1]->Draw("lp");
        c1->cd(loc_ch[2]);
        tg[2]->Draw("alp");
        if(ns_g1[2]>0)tg_g1[2]->Draw("lp");
        c1->cd(loc_ch[3]);
        tg[3]->Draw("alp");
        if(ns_g1[3]>0)tg_g1[3]->Draw("lp");
        c1->cd(loc_ch[4]);
        tg[4]->Draw("alp");
        if(ns_g1[4]>0)tg_g1[4]->Draw("lp");
        if(ADC_test_mode==1 && eLink_active==0x0F)
        {
          c1->cd(loc_ch[5]);
          tg[5]->Draw("alp");
          if(ns_g1[5]>0)tg_g1[5]->Draw("lp");
        }
        c1->Update();
        if(debug==1)
        {
          UInt_t utmp;
          Int_t redo_calib=0;
          Int_t inc_PLL=0;
          Int_t bit_slip=-1;
          Int_t byte_slip=-1;
          Int_t increase_delay=-1;
          Int_t io_reset=0;
          Int_t dump_ADC=0;
          Int_t set_clock=0, increase_seq_clock=0, increase_IO_clock=0, increase_reg_clock=0, increase_mem_clock=0;
          printf("Press any key to continue :\n");
          printf("z : reset line delays\n");
          printf("Z : reset ADCTestUnit\n");
          printf("1<=n<=5 : tune ADC n line delays\n");
          printf("- : decrease tap delay of ADC n lines\n");
          printf("+ : increase tap delay of ADC n lines %3d%3d%3d%3d%3d\n",delay_val[0],delay_val[1], delay_val[2], delay_val[3], delay_val[4]);
          printf("S : increase sequence clock phase by 45 deg (%d)\n",seq_clock_phase);
          printf("I : increase IO clock phase by 45 deg (%d)\n",IO_clock_phase);
          printf("R : increase register clock phase by 45 deg (%d)\n",reg_clock_phase);
          printf("M : increase memory clock phase by 45 deg (%d)\n",mem_clock_phase);
          printf("b : slip incoming bits by 1 unit (%3d%3d%3d%3d%3d)\n",bitslip_val[0],bitslip_val[1],bitslip_val[2],bitslip_val[3],bitslip_val[4]);
          printf("B : slip incoming bytes by 1 unit (%3d%3d%3d%3d%3d)\n",byteslip_val[0],byteslip_val[1],byteslip_val[2],byteslip_val[3],byteslip_val[4]);
          printf("p : increase PLL config register [2:0] content (%x)\n",pll_conf1[channel_sel]);
          printf("P : increase PLL config register [8:3] content (%x)\n",pll_conf2[channel_sel]);
          printf("m : Dump ADC register maps\n");
          printf("r : Load reference ADCs register map\n");
          printf("i : Reload I2C registers in bulky mode\n");
          printf("c : Redo ADC calibration\n");
          printf("d : Optimize pedestals for all channels\n"); 
          printf("u : Toggle CAL mux\n");
          printf("k : Invert ADC clock\n");
          printf("o : toggle Override PLL Vc bit (find PLL optimal tuning manually)\n");
          printf("T : Launch PLL scan to find config which lock and synchronize links\n");

          printf("g : Go ! remove debug\n");

          system("stty raw");
          if(do_calib_loop==1)
          {
            if((ievt%3)==0) cdum='c';
            if((ievt%3)==1) cdum='t';
            if((ievt%3)==2) cdum='t';
          }
          else
            cdum=getchar();
          system("stty -raw");
          while (cdum=='1' || cdum=='2' || cdum=='3' || cdum=='4' || cdum=='5')
          {
            if(cdum=='1')channel_sel=0;
            if(cdum=='2')channel_sel=1;
            if(cdum=='3')channel_sel=2;
            if(cdum=='4')channel_sel=3;
            if(cdum=='5')channel_sel=4;
            printf("Channel selected : %d ",channel_sel+1);
            system("stty raw");
            cdum=getchar();
            system("stty -raw");
          }
          if(cdum=='q' || cdum=='Q')
          {
            //hw.getNode("VFE_CTRL").write(STATIC_RESET*0 | ADC_TEST_MODE*ADC_test_mode);
            //hw.dispatch();
            printf("\n");
            ievt=nevent;
            break;
            //exit(-1);
          }
          Int_t data;
          switch(cdum)
          {
            case 'b':
              bitslip_val[channel_sel]=(bitslip_val[channel_sel]+1)%8;
              command=(1<<channel_sel)*BIT_SLIP | DELAY_TAP_DIR*1| DELAY_RESET*0;
              hw.getNode("DELAY_CTRL").write(command);
              hw.dispatch();
              printf("Get lock status of IDELAY input stages\n");
              reg = hw.getNode("DELAY_CTRL").read();
              hw.dispatch();
              printf("Value read : 0x%x\n",reg.value());
              break;

            case 'B':
              byteslip_val[channel_sel]=(byteslip_val[channel_sel]+1)%4;
              command=(1<<channel_sel)*BYTE_SLIP | DELAY_TAP_DIR*1 | DELAY_RESET*0;
              hw.getNode("DELAY_CTRL").write(command);
              hw.dispatch();
              printf("Get lock status of IDELAY input stages\n");
              reg = hw.getNode("DELAY_CTRL").read();
              hw.dispatch();
              printf("Value read : 0x%x\n",reg.value());
              break;

            case 'T':
              iret=synchronize_links(hw, 0);
              break;

            case '-':
              delay_val[channel_sel]=delay_val[channel_sel]-1;
              if(delay_val[channel_sel]<0)delay_val[channel_sel]+=32;
              command=DELAY_TAP_DIR*0 | DELAY_RESET*0 | (1<<channel_sel);
              hw.getNode("DELAY_CTRL").write(command);
              break;

            case '+':
            case '=':
              delay_val[channel_sel]=delay_val[channel_sel]+1;
              if(delay_val[channel_sel]>=32)delay_val[channel_sel]-=32;
              command=DELAY_TAP_DIR*1 | DELAY_RESET*0 | (1<<channel_sel);
              hw.getNode("DELAY_CTRL").write(command);
              break;

            case 'X':
              command=DELAY_TAP_DIR*1 | DELAY_RESET*1 | IO_RESET*1;
              hw.getNode("DELAY_CTRL").write(command);
              hw.dispatch();
              break;

            case 'Z':
              hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCTestUnit_reset);
              hw.dispatch();
              break;

            case 'z':
              command= 
                CALIB_MUX_ENABLED     *calib_mux_enabled                |
                SELF_TRIGGER_MODE     *self_trigger_mode                |
                (SELF_TRIGGER_MASK     *(self_trigger_mask&0x1F))       |
                (SELF_TRIGGER_THRESHOLD*(self_trigger_threshold&0xFFF)) |
                1<<5                                                    |
                SELF_TRIGGER          *self_trigger                     |
                SELF_TRIGGER_LOOP     *self_trigger_loop                |
                TRIGGER_MODE          *(trigger_mode&1)                 | // Always DAQ on trigger
                RESET                 *0;
              hw.getNode("FEAD_CTRL").write(command);
              hw.dispatch();
              command=DELAY_TAP_DIR*1 | DELAY_RESET*1;
              hw.getNode("DELAY_CTRL").write(command);
              hw.dispatch();
              printf("Get lock status of IDELAY input stages\n");
              reg = hw.getNode("DELAY_CTRL").read();
              hw.dispatch();
              break;

            case 'S':
              if(increase_seq_clock==1)seq_clock_phase=(seq_clock_phase+1)%8;
              command = MEM_CLOCK_PHASE*mem_clock_phase+REG_CLOCK_PHASE*reg_clock_phase+IO_CLOCK_PHASE*IO_clock_phase+SEQ_CLOCK_PHASE*seq_clock_phase;
              hw.getNode("CLK_SETTING").write(command);
              hw.dispatch();
              break;

            case 'I':
              if(increase_IO_clock==1) IO_clock_phase=(IO_clock_phase+1)%8;
              command = MEM_CLOCK_PHASE*mem_clock_phase+REG_CLOCK_PHASE*reg_clock_phase+IO_CLOCK_PHASE*IO_clock_phase+SEQ_CLOCK_PHASE*seq_clock_phase;
              hw.getNode("CLK_SETTING").write(command);
              hw.dispatch();
              break;

            case 'R':
              if(increase_reg_clock==1)reg_clock_phase=(reg_clock_phase+1)%8;
              command = MEM_CLOCK_PHASE*mem_clock_phase+REG_CLOCK_PHASE*reg_clock_phase+IO_CLOCK_PHASE*IO_clock_phase+SEQ_CLOCK_PHASE*seq_clock_phase;
              hw.getNode("CLK_SETTING").write(command);
              hw.dispatch();
              break;

            case 'M':
              if(increase_mem_clock==1)mem_clock_phase=(mem_clock_phase+1)%8;
              command = MEM_CLOCK_PHASE*mem_clock_phase+REG_CLOCK_PHASE*reg_clock_phase+IO_CLOCK_PHASE*IO_clock_phase+SEQ_CLOCK_PHASE*seq_clock_phase;
              hw.getNode("CLK_SETTING").write(command);
              hw.dispatch();
              break;

            case 'i':
              printf("Reload I2C register map of LiTE-DTU\n");
              VFE_control= DELAY_AUTO_TUNE*delay_auto_tune | INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                           PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                           ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode | ADC_MEM_MODE*ADC_MEM_mode;
              hw.getNode("VFE_CTRL").write(VFE_control);
              for(int i=0; i<n_active_channel; i++)
              {
                Int_t ich=channel_number[i];
                for(Int_t ireg=0; ireg<I2C_DTU_nreg; ireg++)
                {
                  if     (ireg<4) data=(DTU_bulk1>>((ireg-0)*8));
                  else if(ireg<8) data=(DTU_bulk2>>((ireg-4)*8));
                  else if(ireg<12)data=(DTU_bulk3>>((ireg-8)*8));
                  else if(ireg<16)data=(DTU_bulk4>>((ireg-12)*8));
                  else            data=(DTU_bulk5>>((ireg-16)*8));
                  if(ireg==5) data=baseline_G10[ich];
                  if(ireg==6) data=baseline_G1[ich];
                  iret=I2C_RW(hw, I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+2, ireg, data, 0, 3, 0);
                  if(debug>0)printf("Single write return code (1) : %8.8x -> %d ACK\n",iret,(iret>>23)&0xff);
                }
              }
              break;

            case 'k':
              VFE_control= DELAY_AUTO_TUNE*delay_auto_tune | INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                           PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                           ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode | ADC_MEM_MODE*ADC_MEM_mode;
              hw.getNode("VFE_CTRL").write(VFE_control);
              hw.dispatch();
              ADC_invert_clk=1-ADC_invert_clk;
              data= (DTU_bulk1&0xef00)>>8 | ADC_invert_clk;
              for(int i=0; i<n_active_channel; i++)
              {
                Int_t ich=channel_number[i];
                iret=I2C_RW(hw, I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+2, 1, data, 0, 3, 0);
              }
              break;

            case 'm':
              VFE_control= DELAY_AUTO_TUNE*delay_auto_tune | INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                           PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                           ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode | ADC_MEM_MODE*ADC_MEM_mode;
              hw.getNode("VFE_CTRL").write(VFE_control);
              hw.dispatch();
              fcal=fopen("last_VFE_calib_reg.dat","w+");
              for(int ich=0; ich<5; ich++)
              {
                for(int iADC=0; iADC<2; iADC++)
                {
                  device_number=I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+iADC;
                  for(int ireg=0; ireg<75; ireg++)
                  {
                    ADC_reg_val[iADC][ireg]=I2C_RW(hw, device_number, ireg, 0, 0, 2, debug_I2C);
                  }
                }
                for(int ireg=0; ireg<76; ireg++)
                {
                  printf("Register %d : 0x%2.2x 0x%2.2x\n",ireg,ADC_reg_val[0][ireg]&0xff, ADC_reg_val[1][ireg]&0xff);
                  fprintf(fcal,"%d %d\n",ADC_reg_val[0][ireg]&0xff, ADC_reg_val[1][ireg]&0xff);
                }
              }
              fclose(fcal);
              VFE_control= DELAY_AUTO_TUNE*delay_auto_tune | INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                           PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                           ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode | ADC_MEM_MODE*ADC_MEM_mode;
              hw.getNode("VFE_CTRL").write(VFE_control);
              hw.dispatch();
              break;

            case 'r':
              VFE_control= DELAY_AUTO_TUNE*delay_auto_tune | INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                           PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                           ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode | ADC_MEM_MODE*ADC_MEM_mode;
              hw.getNode("VFE_CTRL").write(VFE_control);
              hw.dispatch();
              fcal=fopen("ref_vfe_calib_reg.dat","r");
              for(int ich=0; ich<5; ich++)
              {
                for(int ireg=0; ireg<75; ireg++)
                {
                  fscanf(fcal,"%d %d\n",&ADC_reg_val[0][ireg], &ADC_reg_val[1][ireg]);
                }
                for(int iADC=0; iADC<2; iADC++)
                {
                  device_number=I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+iADC;
                  for(int ireg=0; ireg<76; ireg++)
                  {
                    iret=I2C_RW(hw, device_number, ireg, ADC_reg_val[iADC][ireg], 0, 1, debug_I2C);
                  }
                }
              }
              fclose(fcal);
              VFE_control= DELAY_AUTO_TUNE*delay_auto_tune | INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                           PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                           ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode | ADC_MEM_MODE*ADC_MEM_mode;
              hw.getNode("VFE_CTRL").write(VFE_control);
              hw.dispatch();
              break;

            case 'p':
            case 'P':
              if(cdum == 'p')
              {
                pll_conf1[channel_sel]++;
              }
              else
              {
                if(pll_conf2[channel_sel]==0x3F)
                  pll_conf2[channel_sel]=0;
                else
                  pll_conf2[channel_sel]=(pll_conf2[channel_sel]<<1)+1;
              }
              pll_conf1[channel_sel]&=0x7;
              pll_conf2[channel_sel]&=0x3F;
              pll_conf[channel_sel]=(pll_conf2[channel_sel]<<3) | pll_conf1[channel_sel];
              VFE_control= DELAY_AUTO_TUNE*delay_auto_tune | INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                           PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                           ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode | ADC_MEM_MODE*ADC_MEM_mode;
              hw.getNode("VFE_CTRL").write(VFE_control);
              hw.dispatch();
              if(DTU_version[channel_sel]<20)
              {
                iret=I2C_RW(hw, I2C_LiTEDTU_type*1000+((channel_sel+1)<<I2C_shift_dev_number)+2, 9, (pll_conf[channel_sel]&0xFF), 0, 1, debug_I2C);
                if(debug>0)printf("Single write return code (9) : %8.8x -> %d ACK\n",iret,(iret>>23)&0xff);
                iret=I2C_RW(hw, I2C_LiTEDTU_type*1000+((channel_sel+1)<<I2C_shift_dev_number)+2, 8, 0, 0, 2, debug_I2C);
                if(debug>0)printf("Single read return code (8) : %8.8x -> %d ACK\n",iret,(iret>>23)&0xff);
                iret&=0xFF;
                iret=I2C_RW(hw, I2C_LiTEDTU_type*1000+((channel_sel+1)<<I2C_shift_dev_number)+2, 8, (iret&0xFE)|((pll_conf[channel_sel]&0x100)>>8), 0, 1, debug_I2C);
                if(debug>0)printf("Single write return code (8) : %8.8x -> %d ACK\n",iret,(iret>>23)&0xff);
              }
              else
              {
                iret=I2C_RW(hw, I2C_LiTEDTU_type*1000+((channel_sel+1)<<I2C_shift_dev_number)+2, 16, (pll_conf[channel_sel]&0xFF), 0, 1, debug_I2C);
                if(debug>0)printf("Single write return code (16) : %8.8x -> %d ACK\n",iret,(iret>>23)&0xff);
                iret=I2C_RW(hw, I2C_LiTEDTU_type*1000+((channel_sel+1)<<I2C_shift_dev_number)+2, 15, 0, 0, 2, debug_I2C);
                if(debug>0)printf("Single read return code (15) : %8.8x -> %d ACK\n",iret,(iret>>23)&0xff);
                iret&=0xFF;
                iret=I2C_RW(hw, I2C_LiTEDTU_type*1000+((channel_sel+1)<<I2C_shift_dev_number)+2, 15, (iret&0xFE)|((pll_conf[channel_sel]&0x100)>>8), 0, 1, debug_I2C);
                if(debug>0)printf("Single write return code (15) : %8.8x -> %d ACK\n",iret,(iret>>23)&0xff);
                //iret=I2C_RW(hw, I2C_LiTEDTU_type*1000+((channel_sel+1)<<I2C_shift_dev_number)+2, 17, 0x32, 0, 1, debug_I2C);
                iret=I2C_RW(hw, I2C_LiTEDTU_type*1000+((channel_sel+1)<<I2C_shift_dev_number)+2, 17, 0x34, 0, 1, debug_I2C);
                hw.getNode("DTU_RESYNC").write(LiTEDTU_DTU_reset);
                hw.dispatch();
              }

              VFE_control= DELAY_AUTO_TUNE*delay_auto_tune | INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                           PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                           ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode | ADC_MEM_MODE*ADC_MEM_mode;
              hw.getNode("VFE_CTRL").write(VFE_control);
              hw.dispatch();
              break;

            case 'O':
              pll_override_Vc=1-pll_override_Vc;
              for(int i=0; i<n_active_channel; i++)
              {
                Int_t ich=channel_number[i];
                iret=I2C_RW(hw, I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+2, 15, 0, 0, 2, debug_I2C);
                if(debug>0)printf("Single read return code (15) : %8.8x -> %d ACK\n",iret,(iret>>23)&0xff);
                iret=(iret&0xfd) | (pll_override_Vc<<1);
                iret=I2C_RW(hw, I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+2, 15, iret, 0, 3, debug_I2C);
                if(debug>0)printf("Single write return code (15) : %8.8x -> %d ACK\n",iret,(iret>>23)&0xff);
              }
            break;
            case 'o':
              pll_override_Vc=1-pll_override_Vc;
              for(int ich=channel_sel; ich<=channel_sel; ich++)
              {
                iret=I2C_RW(hw, I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+2, 15, 0, 0, 2, debug_I2C);
                if(debug>0)printf("Single read return code (15) : %8.8x -> %d ACK\n",iret,(iret>>23)&0xff);
                iret=(iret&0xfd) | (pll_override_Vc<<1);
                iret=I2C_RW(hw, I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+2, 15, iret, 0, 3, debug_I2C);
                if(debug>0)printf("Single write return code (15) : %8.8x -> %d ACK\n",iret,(iret>>23)&0xff);
              }
            break;
            case 'c':
              VFE_control= DELAY_AUTO_TUNE*delay_auto_tune | INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                           PED_MUX*1 | eLINK_ACTIVE*eLink_active |
                           ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode | ADC_MEM_MODE*ADC_MEM_mode;
              hw.getNode("VFE_CTRL").write(VFE_control);
              hw.dispatch();
              //hw.getNode("DTU_RESYNC").write(LiTEDTU_ADC_reset);
              //hw.dispatch();
              for(int i=0; i<n_active_channel; i++)
              {
                Int_t ich=channel_number[i];
                if(global_test==1)
                {
                  val=I2C_RW(hw, I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+0, 0, 0x01, 0, 1, debug_I2C);
                  val=I2C_RW(hw, I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+1, 0, 0x01, 0, 1, debug_I2C);
                }
                if(nsample_calib_x4==1)
                {
                  val=I2C_RW(hw, I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+0, 1, 0xfe, 0, 1, debug_I2C);
                  val=I2C_RW(hw, I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+1, 1, 0xfe, 0, 1, debug_I2C);
                }
                if(dither==1)
                {
                  val=I2C_RW(hw, I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+0, 3, 0x01, 0, 1, debug_I2C);
                  val=I2C_RW(hw, I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+1, 3, 0x01, 0, 1, debug_I2C);
                }
// Set CATIA outputs with calibration levels
                if(CATIA_version[ich]>=14)
                {
                  device_number=I2C_CATIA_type*1000+((ich+1)<<I2C_shift_dev_number)+3;
                  iret=I2C_RW(hw, device_number, 5, CATIA_Reg5_def|0x3000, 1, 3, debug);
                }
              }
              usleep(500000); // Let some time for calibration voltages to stabilize
              //usleep(1000);
              //hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCL_calib);
              hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCH_calib);
              hw.dispatch();
              usleep(1000000);
              VFE_control= DELAY_AUTO_TUNE*delay_auto_tune | INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                           PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                           ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode | ADC_MEM_MODE*ADC_MEM_mode;
              hw.getNode("VFE_CTRL").write(VFE_control);
              for(int i=0; i<n_active_channel; i++)
              {
                Int_t ich=channel_number[i];
                if(CATIA_version[ich]>=14)
                {
                   device_number=I2C_CATIA_type*1000+((ich+1)<<I2C_shift_dev_number)+3;
                   iret=I2C_RW(hw, device_number, 5, CATIA_Reg5_def, 1, 3, debug);
                }
              }
              hw.dispatch();
              usleep(100000); // Let some time for voltages to stabilize
              break;
            case 'd':
              tune_pedestals(hw, 0);
              break;
            case 'u':
              ped_mux=1-ped_mux;
              VFE_control= DELAY_AUTO_TUNE*delay_auto_tune | INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | 
                           PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                           ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode | ADC_MEM_MODE*ADC_MEM_mode;
              hw.getNode("VFE_CTRL").write(VFE_control);
              usleep(100000); // Let some time for voltages to stabilize
              hw.dispatch();
              break;
            case 'g':
              debug=0;
              break;
          }
        }
        if(debug==2)usleep(500000);
      }
      if(debug>0)
      {
        //command = ((nsample+1)<<16)+CAPTURE_STOP;
        //hw.getNode("CAP_CTRL").write(command);
        //hw.dispatch();
        //old_read_address=-1;
      }

      ievt++;
    }
    TP_level+=TP_step;
  }

// Stop DAQ :
  command = ((nsample+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
  // Switch on FE-adapter LEDs
  command = LED_ON*1+GENE_100HZ*0+GENE_TRIGGER*0;
  hw.getNode("FW_VER").write(command);
  hw.dispatch();

  c2=new TCanvas("mean","mean",800.,1000.);
  c2->Divide(2,3);
  c2->Update();
  c3=new TCanvas("rms","rms",800.,1000.);
  c3->Divide(2,3);
  c3->Update();
  printf("RMS : ");
  for(int ich=0; ich<nch; ich++)
  {
    c2->cd(loc_ch[ich]);
    hmean[ich]->Draw();
    c2->Update();
    c3->cd(loc_ch[ich]);
    hrms[ich]->Draw();
    rms[ich]=hrms[ich]->GetMean();
    printf("%e, ",rms[ich]);
    c3->Update();
  }
  printf("\n");
  tdata->Write();
  c1->Write();
  c2->Write();
  c3->Write();
  for(int istep=0; istep<n_TP_step; istep++)
  {
    for(int ich=0; ich<nch; ich++)
    {
      pshape[istep][ich]->Write();
    }
  }
  for(int ich=0; ich<nch; ich++) hdensity[ich]->Write();
  fd->Close();
  printf("Finished with %d events recorded\n",ngood_event);
}


