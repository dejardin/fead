#include "uhal/uhal.hpp"
#include <signal.h>

#include <vector>
#include <iostream>
#include <cstdlib>
#include <typeinfo>

#include "I2C_RW.h"
#include "LiTEDTU_def.h"
#include <TApplication.h>
#include <TProfile.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TProfile.h>
#include <TVirtualFFT.h>
#include <TF1.h>
#include <TH1D.h>
#include <TTree.h>
#include <TFile.h>

#define SHIFT_DEV_NUMBER       3

#define GENE_TRIGGER           (1<<0)
#define GENE_TP                (1<<1)
#define GENE_100HZ             (1<<2)
#define LED_ON                 (1<<3)
#define TP_MODE                (1<<4)
#define GENE_G10_PULSE         (1<<5)
#define GENE_G1_PULSE          (1<<6)

#define RESET                  (1<<0)
#define TRIGGER_MODE           (1<<1)
#define SOFT_TRIGGER           (1<<2)
#define SELF_TRIGGER           (1<<3)
#define CLOCK_PHASE            (1<<4)
#define CLOCK_RESET            (1<<7)
#define SELF_TRIGGER_THRESHOLD (1<<8)
#define SELF_TRIGGER_MASK      (1<<22)
#define CALIB_PULSE_ENABLED    (1<<27)
#define CALIB_MUX_ENABLED      (1<<28)
#define BOARD_SN               (1<<28)
#define SELF_TRIGGER_MODE      (1<<31)

#define SEQ_CLOCK_PHASE        (1<<0)
#define IO_CLOCK_PHASE         (1<<4)
#define REG_CLOCK_PHASE        (1<<8)
#define MEM_CLOCK_PHASE        (1<<12)
#define RESYNC_PHASE           (1<<16)

#define I2C_TOGGLE_SDA         (1<<31)

#define PWUP_RESETB            (1<<31)
#define BULKY_I2C_DTU          (1<<28)
#define N_RETRY_I2C_DTU        (1<<14)
#define PED_MUX                (1<<13)
#define BUGGY_I2C_DTU          (1<<4)
#define INVERT_RESYNC          (1<<3)
#define LVRB_AUTOSCAN          (1<<2)

#define CAPTURE_START 1
#define CAPTURE_STOP  2
#define SW_DAQ_DELAY (1<<16)  // delay for laser with external trigger
#define HW_DAQ_DELAY (1<<0)   // Laser with external trigger

#define NSAMPLE_MAX 26624
#define MAX_PAYLOAD 1380
#define MAX_VFE     10
#define MAX_STEP    10000

#define G1_CALIB_TRIGGER       (1<<4)
#define G10_CALIB_TRIGGER      (1<<5)

#define DRP_WRb         (1<<31)
#define DELAY_INCREASE  (1<<5)     // Increase ADC signal delay by one tip
#define DELAY_RESET     (1<<6)     // Reset ADC signal delays
#define BIT_SLIP        (1<<16)    // Slip serial bits by 1 unit (to align bits in ISERDES) (to multiply with 5 bits channel map)
#define BYTE_SLIP       (1<<21)    // Slip serial bytes by 1 unit (to align bytes in 40 MHz clock) (to multiply with 5 bits channel map)
#define DELAY_AUTO_TUNE (1<<29)    // Launch autamatic IDELAY tuning to get the eye center of each stream

#define ADC_CALIB_MODE      (1<<0)
#define ADC_TEST_MODE       (1<<1)
#define ADC_MEM_MODE        (1<<5)
#define ADC_INVERT_DATA     (1<<8)
#define RESYNC_IDLE_PATTERN (1<<0)
#define eLINK_ACTIVE        (1<<8)
#define STATIC_RESET        (1<<31)

#define I2C_LVR_TYPE     0
#define I2C_CATIA_TYPE   1
#define I2C_LiTEDTU_TYPE 2

#define NSAMPLE_PED      30
#define N_ADC_REG        76

using namespace uhal;
Int_t I2C_LiTEDTU_type, I2C_CATIA_type;
Int_t delay_val[5];    // Number of delay taps to get a stable R/O with ADCs (160 MHz)
Int_t bitslip_val[5];  // Number of bit to slip with iserdes lines to get ADC values well aligned
Int_t byteslip_val[5]; // Number of byte to slip to aligned ADC data on 40 MHz clock
Int_t old_read_address, shift_oddH_samples, shift_oddL_samples, ADC_invert_data, ADC_test_mode, ADC_MEM_mode, wait_for_ever, eLink_active, calib_pulse_enabled;
Int_t I2C_DTU_nreg, buggy_DTU;
Int_t ADC_reg_val[2][N_ADC_REG];
UInt_t DTU_bulk1, DTU_bulk2, DTU_bulk3, DTU_bulk4, DTU_bulk5;
Short_t *event[6], *gain[6];
Double_t *fevent[6], ped_G1[6], ped_G10[6];
Int_t all_sample[6], ns_g1[6];
Int_t dump_data, corgain, DTU_force_G1, DTU_force_G10;
ValVector< uint32_t > block,mem;
TGraph *tg[6], *tg_g1[6];
TGraph *tg_rms[6], *tg_resi[6], *tg_diff[2];
TGraphErrors *tg_mean[6];
TH1D *hmean[6], *hrms[6], *hdensity[6];
TCanvas *cdebug, *c1, *c2, *c3;
TTree *tdata;
TFile *fd;

struct timeval tv;
void end_of_run()
{
  gettimeofday(&tv,NULL);
  printf("%ld.%6.6ld : End of study\n",tv.tv_sec,tv.tv_usec);
  tdata->Write();
  c1->Write();
  if(c2!=NULL)c2->Write();
  if(c3!=NULL)c3->Write();
  fd->Close();
  exit(1);
}

void intHandler(int)
{
  printf("Ctrl-C detected !\n");
  gettimeofday(&tv,NULL);
  printf("%ld.%6.6ld : Interupt detected\n",tv.tv_sec,tv.tv_usec);
  end_of_run();
}
void abortHandler(int)
{
  printf("Abort !\n");
  gettimeofday(&tv,NULL);
  printf("%ld.%6.6ld : Run aborted\n",tv.tv_sec,tv.tv_usec);
  end_of_run();
}

Int_t get_event(uhal::HwInterface hw, int trigger_type, int nsample, int debug, int draw)
{
  static int ievt=0;
  static TGraph *tg_debug[6]={NULL};
  static int trigger_type_old=-1;
  int n_word;
  int n_transfer_max=463;
  int n_transfer;
  int n_last;
  int error      = 0;
  int command    = 0;
  Double_t dt    =6.25;
  static Int_t loc_ch[6]={1,2,3,4,5,6};

  if(trigger_type_old!=trigger_type)ievt=0;
  if(cdebug==NULL)
  {
    for(Int_t ich=0; ich<6; ich++)
    {
      tg_debug[ich]=new TGraph();
      tg_debug[ich]->SetMarkerStyle(20);
      tg_debug[ich]->SetMarkerSize(0.5);
    }
    if(ADC_test_mode==1 && eLink_active==0x0F)
    {
      tg_debug[0]->SetLineColor(kCyan);
      tg_debug[1]->SetLineColor(kCyan);
      tg_debug[2]->SetLineColor(kMagenta);
      tg_debug[3]->SetLineColor(kMagenta);
      tg_debug[4]->SetLineColor(kBlue);
      tg_debug[5]->SetLineColor(kRed);
      tg_debug[0]->SetMarkerColor(kCyan);
      tg_debug[1]->SetMarkerColor(kCyan);
      tg_debug[2]->SetMarkerColor(kMagenta);
      tg_debug[3]->SetMarkerColor(kMagenta);
      tg_debug[4]->SetMarkerColor(kBlue);
      tg_debug[5]->SetMarkerColor(kRed);
      loc_ch[0]=1;
      loc_ch[1]=3;
      loc_ch[2]=2;
      loc_ch[3]=4;
      loc_ch[4]=5;
      loc_ch[5]=6;
    }
    else
    {
      tg_debug[0]->SetLineColor(kCyan);
      tg_debug[1]->SetLineColor(kBlue);
      tg_debug[2]->SetLineColor(kMagenta);
      tg_debug[3]->SetLineColor(kRed);
      tg_debug[4]->SetLineColor(kGreen);
      tg_debug[0]->SetMarkerColor(kCyan);
      tg_debug[1]->SetMarkerColor(kBlue);
      tg_debug[2]->SetMarkerColor(kMagenta);
      tg_debug[3]->SetMarkerColor(kRed);
      tg_debug[4]->SetMarkerColor(kGreen);
    }
    cdebug=new TCanvas("debug","debug",1060,0,800,1000);
    cdebug->Divide(2,3);
  }

  ValWord<uint32_t> address,free_mem;
  address = hw.getNode("CAP_ADDRESS").read();
  hw.dispatch();
  if(debug>0)printf("Starting with address : %8.8x, trigger_type : %d\n",address.value(),trigger_type);

  ValVector< uint32_t > block;

  if(trigger_type !=2)
  {
    if(trigger_type == 0)
      command = GENE_G1_PULSE*0 | GENE_G10_PULSE*0 | TP_MODE*0 | LED_ON*0 | GENE_100HZ*0 | GENE_TP*0 | GENE_TRIGGER*1;
    else if(trigger_type == 1)
      command = GENE_G1_PULSE*0 | GENE_G10_PULSE*0 | TP_MODE*0 | LED_ON*0 | GENE_100HZ*0 | GENE_TP*1 | GENE_TRIGGER*0;
    else if(trigger_type == 3)
      command = GENE_G1_PULSE*0 | GENE_G10_PULSE*1 | TP_MODE*0 | LED_ON*0 | GENE_100HZ*0 | GENE_TP*0 | GENE_TRIGGER*0;
    else if(trigger_type == 4)
      command = GENE_G1_PULSE*1 | GENE_G10_PULSE*0 | TP_MODE*0 | LED_ON*0 | GENE_100HZ*0 | GENE_TP*0 | GENE_TRIGGER*0;

    if(debug>0)printf("Send trigger with command : 0x%8.8x\n",command);
// Read base address and send trigger
    hw.getNode("FW_VER").write(command);
    hw.dispatch();
// Read new address and wait for DAQ completion
    int nretry=0, new_write_address=-1, delta_address=-1;
    do
    {  
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      new_write_address=address.value()>>16;
      nretry++;
      delta_address=new_write_address-old_read_address;
      if(delta_address<0)delta_address+=NSAMPLE_MAX;
      if(debug>0) printf("ongoing R/W addresses    : old %d, new %d delta %d\n", old_read_address, new_write_address,delta_address);
    }
    while(delta_address < nsample+1 && nretry<100);
    if(nretry==100)
    {
      printf("Trigger type 0/1 : Stop waiting for sample capture after %d retries\n",nretry);
      printf("R/W addresses    : old %8.8x, new %8.8x add 0x%8.8x\n", old_read_address, new_write_address, address.value());
      error=1;
    }
  }
  else
  {
    int nretry=0;
// Wait for external trigger to fill memory :
    do
    {
      free_mem = hw.getNode("CAP_FREE").read();
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      if(debug>0)printf("address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
      usleep(100);
      //if(wait_for_ever==1)
      //{
      //  usleep(1000);
      //  printf(".");
      //  fflush(stdout);
      //}
      nretry++;
    }
    while((free_mem.value()==NSAMPLE_MAX-1) && (nretry<100 || wait_for_ever==1));
    //if(wait_for_ever==1)printf("\n");
    if(nretry>=100 && wait_for_ever==0)
    {
      printf("Trigger type 2 : Stop waiting for external trigger after %d retries\n",nretry);
      printf("R/W addresses    : add 0x%8.8x\n",address.value());
      error=1;
    }
  }

// Keep reading address for next event
  old_read_address=address.value()>>16;
  if(old_read_address==NSAMPLE_MAX-1)old_read_address=-1;

  mem.clear();

// Read event samples from FPGA
  n_word=(nsample+1)*6; // 6*32 bits words per sample to get the 5 channels data
  n_transfer=n_word/(MAX_PAYLOAD/4); // Max ethernet packet = 1536 bytes, max user payload = 1500 bytes
  n_last=n_word-n_transfer*(MAX_PAYLOAD/4);
  if(n_transfer > n_transfer_max)
  {
    printf("Event size too big ! Please reduce number of samples per frame.\n");
    printf("Max frame size : %d\n",NSAMPLE_MAX);
    printf("Error : ask for too many samples. Number of transfer %d greater than allowed max value %d\n",n_transfer, n_transfer_max);
    error=1;
  }
  for(int itrans=0; itrans<n_transfer; itrans++)
  {
    block = hw.getNode ("CAP_DATA").readBlock(MAX_PAYLOAD/4);
    hw.dispatch();
    for(int is=0; is<MAX_PAYLOAD/4; is++)mem.push_back(block[is]);
  }
  block = hw.getNode ("CAP_DATA").readBlock(n_last);
  address = hw.getNode("CAP_ADDRESS").read();
  free_mem = hw.getNode("CAP_FREE").read();
  hw.dispatch();
  if(debug>0)printf("After reading address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
  for(int is=0; is<n_last; is++)mem.push_back(block[is]);
  mem.valid(true);


// First sample should have bits 159 downto 64 at 1 and timestamp in bits 63 downto 0
  if(mem[3]!=0xffffffff && mem[4]!=0xffffffff && mem[5]!=0xffffffff)
  {
    printf("First samples not headers : %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",mem[5], mem[4], mem[3], mem[2], mem[1], mem[0]);
    error=1;
  }
  Long_t timestamp=mem[2];

  timestamp=(timestamp<<32)+mem[1];

  if(debug>0)
  {
    Int_t i0=0;
    printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",mem[i0+0],mem[i0+1],mem[i0+2],mem[i0+3],mem[i0+4],mem[i0+5]);
    i0+=6;
    printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",mem[i0+0],mem[i0+1],mem[i0+2],mem[i0+3],mem[i0+4],mem[i0+5]);
    i0+=6;
    printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",mem[i0+0],mem[i0+1],mem[i0+2],mem[i0+3],mem[i0+4],mem[i0+5]);
    i0+=6;
    printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",mem[i0+0],mem[i0+1],mem[i0+2],mem[i0+3],mem[i0+4],mem[i0+5]);
    i0+=6;
    printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",mem[i0+0],mem[i0+1],mem[i0+2],mem[i0+3],mem[i0+4],mem[i0+5]);
    i0+=6;
    printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",mem[i0+0],mem[i0+1],mem[i0+2],mem[i0+3],mem[i0+4],mem[i0+5]);
    printf("timestamp : %8.8x %8.8x %ld\n",mem[2],mem[1],timestamp);
  }
  for(int ich=0; ich<6; ich++) all_sample[ich]=0;
  for(int isample=0; isample<NSAMPLE_MAX*5; isample++)
  {
    event[0][isample]=-1;
    event[1][isample]=-1;
    event[2][isample]=-1;
    event[3][isample]=-1;
    event[4][isample]=-1;
    event[5][isample]=-1;
    gain[0][isample]=DTU_force_G1;
    gain[1][isample]=DTU_force_G1;
    gain[2][isample]=DTU_force_G1;
    gain[3][isample]=DTU_force_G1;
    gain[4][isample]=DTU_force_G1;
    gain[5][isample]=DTU_force_G1;
  }
  Int_t nsample_ped[5]={0};
  Double_t loc_ped_G10[5]={0.};

  FILE *fd;
  if(dump_data==1 && ievt==0) fd=fopen("data/dump.txt","w+");
  if(dump_data==1 && ievt>0) fclose(fd);

  for(int isample=0; isample<nsample; isample++)
  {
    Int_t j=(isample+1)*6;
    unsigned int loc_mem[5];
    for(int ich=0; ich<5; ich++)
    {
      loc_mem[ich]=mem[j+1+ich];
    }
    if(dump_data==1 && isample<100)
    {
      fprintf(fd,"%8.8x %8.8x %8.8x %8.8x %8.8x\n",loc_mem[0],loc_mem[1],loc_mem[2],loc_mem[3],loc_mem[4]);
    }

// Data in test mode :
    if(ADC_test_mode==1)
    {
      dt=12.5;
      event[0][all_sample[0]]  =(loc_mem[0]>>0 )&0xfff;
      event[0][all_sample[0]+1]=(loc_mem[0]>>16)&0xfff;
      event[1][all_sample[1]]  =(loc_mem[1]>>0 )&0xfff;
      event[1][all_sample[1]+1]=(loc_mem[1]>>16)&0xfff;
      event[2][all_sample[2]]  =(loc_mem[2]>>0 )&0xfff;
      event[2][all_sample[2]+1]=(loc_mem[2]>>16)&0xfff;
      event[3][all_sample[3]]  =(loc_mem[3]>>0 )&0xfff;
      event[3][all_sample[3]+1]=(loc_mem[3]>>16)&0xfff;
      event[4][all_sample[4]]  =(loc_mem[4]>>0 )&0xfff;
      event[4][all_sample[4]+1]=(loc_mem[4]>>16)&0xfff;
      if(ADC_invert_data==1)
      {
        event[0][all_sample[0]]  =4095-event[0][all_sample[0]];
        event[0][all_sample[0]+1]=4095-event[0][all_sample[0]+1];
        event[1][all_sample[1]]  =4095-event[1][all_sample[1]];
        event[1][all_sample[1]+1]=4095-event[1][all_sample[1]+1];
        event[2][all_sample[2]]  =4095-event[2][all_sample[2]];
        event[2][all_sample[2]+1]=4095-event[2][all_sample[2]+1];
        event[3][all_sample[3]]  =4095-event[3][all_sample[3]];
        event[3][all_sample[3]+1]=4095-event[3][all_sample[3]+1];
        event[4][all_sample[4]]  =4095-event[4][all_sample[4]];
        event[4][all_sample[4]+1]=4095-event[4][all_sample[4]+1];
      }
      fevent[0][all_sample[0]+0]=(double)event[0][all_sample[0]];
      fevent[0][all_sample[0]+1]=(double)event[0][all_sample[0]+1];
      fevent[1][all_sample[1]+0]=(double)event[1][all_sample[1]];
      fevent[1][all_sample[1]+1]=(double)event[1][all_sample[1]+1];
      fevent[2][all_sample[2]+0]=(double)event[2][all_sample[2]];
      fevent[2][all_sample[2]+1]=(double)event[2][all_sample[2]+1];
      fevent[3][all_sample[3]+0]=(double)event[3][all_sample[3]];
      fevent[3][all_sample[3]+1]=(double)event[3][all_sample[3]+1];
      fevent[4][all_sample[4]+0]=(double)event[4][all_sample[4]];
      fevent[4][all_sample[4]+1]=(double)event[4][all_sample[4]+1];
      if (eLink_active==0x0F)
      {
        event[4][all_sample[4]+0]  =  event[1][all_sample[1]];
        event[4][all_sample[4]+1]  =  event[0][all_sample[0]];
        event[4][all_sample[4]+2]  =  event[1][all_sample[1]+1];
        event[4][all_sample[4]+3]  =  event[0][all_sample[0]+1];
        event[5][all_sample[5]+0]  =  event[3][all_sample[3]];
        event[5][all_sample[5]+1]  =  event[2][all_sample[2]];
        event[5][all_sample[5]+2]  =  event[3][all_sample[3]+1];
        event[5][all_sample[5]+3]  =  event[2][all_sample[2]+1];
        fevent[4][all_sample[4]+0] = fevent[1][all_sample[1]];
        fevent[4][all_sample[4]+1] = fevent[0][all_sample[0]];
        fevent[4][all_sample[4]+2] = fevent[1][all_sample[1]+1];
        fevent[4][all_sample[4]+3] = fevent[0][all_sample[0]+1];
        fevent[5][all_sample[5]+0] = fevent[3][all_sample[3]];
        fevent[5][all_sample[5]+1] = fevent[2][all_sample[2]];
        fevent[5][all_sample[5]+2] = fevent[3][all_sample[3]+1];
        fevent[5][all_sample[5]+3] = fevent[2][all_sample[2]+1];
        all_sample[4]+=4;
        all_sample[5]+=4;
      }
      else
      {
        all_sample[4]+=2;
      }
      for(int ich=0; ich<4; ich++)all_sample[ich]+=2;
    }
    else
// Data in DTU mode :
    {
      dt=6.25;
      for(int ich=0; ich<5; ich++)
      {
        if(ped_G10[ich]<0. && nsample_ped[ich]==NSAMPLE_PED)
        {
          ped_G10[ich]=loc_ped_G10[ich]/NSAMPLE_PED;
          printf("G10 pedestal for channel %d : %f\n",ich,ped_G10[ich]);
        }
        if(debug>1)printf("Channel %d\n",ich);
        UInt_t tmp_mem=loc_mem[ich];
        Int_t type=(tmp_mem>>30);
        if(debug>1)printf("Compressed data : type %d\n",type);
        if(type==3)
        {
          Int_t sub_type=(tmp_mem>>28)&0x3;
          if(sub_type==1 && debug>0) // Frame delimiter
          {
            printf("Ch %d, Frame delimiter %d : %d samples, CRC12= 0x%x\n",ich,tmp_mem&0xff, (tmp_mem>>20)&0xff, (tmp_mem>>8)&0xfff);
          }
          //else if (sub_type==2) // idle pattern
          continue;
        }
        Int_t sample_map=5;
        if(type==2)sample_map=(tmp_mem>>24)&0x3F;
        if(type==2 && sample_map>4)
        {
          if(debug>=0)printf("Strange saple map : 0x%x, certainly loss of sync !\n",sample_map);
          sample_map=4;
        }
  // pedestal data
        if(type==1 || type==2)
        {
          if(debug>1)printf("Baseline data : sample map 0x%x :",sample_map);
          for(Int_t i=0; i<sample_map; i++)
          {
            event[ich][all_sample[ich]]=tmp_mem&0x3F;
            fevent[ich][all_sample[ich]]=(double)event[ich][all_sample[ich]];
            if(debug>1)printf(" 0x%x",event[ich][all_sample[ich]]);
            if(ped_G10[ich]<0. && nsample_ped[ich]<NSAMPLE_PED)
            {
              loc_ped_G10[ich]+=fevent[ich][all_sample[ich]];
              nsample_ped[ich]++;
            }
            all_sample[ich]++;
            tmp_mem>>=6;
          }
          if(debug>1)printf("\n");
        }
        else
  // signal data
        {
          Int_t signal_type=(tmp_mem>>26)&0xF;
          if(debug>1)printf("Signal data : type 0x%x : ",signal_type);
          if(signal_type==0xA)
          {
            event[ich][all_sample[ich]]=tmp_mem&0xFFF;
            if(DTU_force_G1==0) gain[ich][all_sample[ich]]=(tmp_mem>>12)&0x1;
            //if(gain[ich][all_sample[ich]]==1 && event[ich][all_sample[ich]]>0xfa0)event[ich][all_sample[ich]]=0;
            fevent[ich][all_sample[ich]]=(double)event[ich][all_sample[ich]];
            //if(gain[ich][all_sample[ich]]==1 && corgain==1)fevent[ich][all_sample[ich]]*=10.5;
            if(gain[ich][all_sample[ich]]==1 && corgain==1 && ped_G1[ich]>0. && ped_G10[ich]>0.)
              fevent[ich][all_sample[ich]]=(fevent[ich][all_sample[ich]]-ped_G1[ich])*10.1+ped_G10[ich];
            if(debug>1)printf(" 0x%x",event[ich][all_sample[ich]]);
            all_sample[ich]++;
            tmp_mem>>=13;
            event[ich][all_sample[ich]]=tmp_mem&0xFFF;
            if(DTU_force_G1==0) gain[ich][all_sample[ich]]=(tmp_mem>>12)&0x1;
            //if(gain[ich][all_sample[ich]]==1 && event[ich][all_sample[ich]]>0xfa0)event[ich][all_sample[ich]]=0;
            fevent[ich][all_sample[ich]]=(double)event[ich][all_sample[ich]];
            //if(gain[ich][all_sample[ich]]==1 && corgain==1)fevent[ich][all_sample[ich]]*=10.5;
            if(gain[ich][all_sample[ich]]==1 && corgain==1 && ped_G1[ich]>0. && ped_G10[ich]>0.)
              fevent[ich][all_sample[ich]]=(fevent[ich][all_sample[ich]]-ped_G1[ich])*10.1+ped_G10[ich];
            if(debug>1)printf(" 0x%x",event[ich][all_sample[ich]]);
            all_sample[ich]++;
          }
          else if(signal_type==0xB)
          {
            event[ich][all_sample[ich]]=tmp_mem&0xFFF;
            if(DTU_force_G1==0) gain[ich][all_sample[ich]]=(tmp_mem>>12)&0x1;
            //if(gain[ich][all_sample[ich]]==1 && event[ich][all_sample[ich]]>0xfa0)event[ich][all_sample[ich]]=0;
            fevent[ich][all_sample[ich]]=(double)event[ich][all_sample[ich]];
            if(gain[ich][all_sample[ich]]==1 && corgain==1 && ped_G1[ich]>0. && ped_G10[ich]>0.)
              fevent[ich][all_sample[ich]]=(fevent[ich][all_sample[ich]]-ped_G1[ich])*10.1+ped_G10[ich];
            if(debug>1)printf(" 0x%x",event[ich][all_sample[ich]]);
            all_sample[ich]++;
          }
          if(debug>1)printf("\n");
        }
        if(debug>1)
        {
          for(Int_t is=0; is<all_sample[ich]; is++)
          {
            printf("%4d ",event[ich][is]);
          }
          printf("\n");
        }
      }
    }

    //Search for headers in incoming data
    if(debug>0 && isample==0)
    {
      printf("addr : %8.8x, data : %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",mem[j+0],mem[j+1],mem[j+2],mem[j+3],mem[j+4],mem[j+5],mem[j+6]);
      UInt_t loc_mem=mem[j+1];
      UInt_t ref_mem=0x30009000;
      printf("data : %8.8x ",loc_mem);
      for(int i=31; i>=0; i--) printf("%1.1d",(loc_mem>>i)&1);
      printf(" 0x3 and 0x9 at ");
      for(int i=31; i>=0; i--) {if((loc_mem&0xf000f000) == ref_mem) printf(" %d",i);loc_mem=loc_mem<<1 | ((loc_mem>>31)&1);}
      printf("\n");

      loc_mem=mem[j+2];
      if(ADC_test_mode==1)ref_mem=0x6000c000;
      printf("data : %8.8x ",loc_mem);
      for(int i=31; i>=0; i--) printf("%1.1d",(loc_mem>>i)&1);
      printf(" 0x3 and 0x9 at ");
      for(int i=31; i>=0; i--) {if((loc_mem&0xf000f000) == ref_mem) printf(" %d",i);loc_mem=loc_mem<<1 | ((loc_mem>>31)&1);}
      printf("\n");

      loc_mem=mem[j+3];
      if(ADC_test_mode==1)ref_mem=0xc0006000;
      printf("data : %8.8x ",loc_mem);
      for(int i=31; i>=0; i--) printf("%1.1d",(loc_mem>>i)&1);
      printf(" 0x3 and 0x9 at ");
      for(int i=31; i>=0; i--) {if((loc_mem&0xf000f000) == ref_mem) printf(" %d",i);loc_mem=loc_mem<<1 | ((loc_mem>>31)&1);}
      printf("\n");

      loc_mem=mem[j+4];
      if(ADC_test_mode==1)ref_mem=0x90003000;
      printf("data : %8.8x ",loc_mem);
      for(int i=31; i>=0; i--) printf("%1.1d",(loc_mem>>i)&1);
      printf(" 0x3 and 0x9 at ");
      for(int i=31; i>=0; i--) {if((loc_mem&0xf000f000) == ref_mem) printf(" %d",i);loc_mem=loc_mem<<1 | ((loc_mem>>31)&1);}
      printf("\n");

      loc_mem=mem[j+5];
      printf("data : %8.8x ",loc_mem);
      for(int i=31; i>=0; i--) printf("%1.1d",(loc_mem>>i)&1);
      printf(" 0x3 and 0x9 at ");
      for(int i=31; i>=0; i--) {if((loc_mem&0xf000f000) == 0x30009000) printf(" %d",i);loc_mem=loc_mem<<1 | ((loc_mem>>31)&1);}
      printf("\n");
      ValWord<uint32_t> reg = hw.getNode("DEBUG1").read();
      hw.dispatch();
      printf("Delay debug : 0x%x\n",reg.value());
    }

  }
  if(debug>0)
  {
    for(Int_t ich=0; ich<6; ich++)printf("Channel %d : %d samples\n",ich,all_sample[ich]);
  }

  if((trigger_type !=trigger_type_old && draw==0) || draw==1 )
  {
    Int_t ch_max=5;
    if(ADC_test_mode==1 && eLink_active==0xF)ch_max=4;
    for(int ich=0; ich<ch_max; ich++)
    {
      tg_debug[ich]->Set(0);
      for(int isample=0; isample<all_sample[ich]; isample++) tg_debug[ich]->SetPoint(isample,dt*isample,fevent[ich][isample]);
    }
    if(ADC_test_mode==1 && eLink_active==0xF)
    {
      for(int ich=4; ich<6; ich++)
      {
        tg_debug[ich]->Set(0);
        for(int isample=0; isample<all_sample[ich]; isample++) tg_debug[ich]->SetPoint(isample,6.25*isample,fevent[ich][isample]);
      }
    }
    for(int ich=0; ich<ch_max; ich++)
    {
      cdebug->cd(loc_ch[ich]);
      tg_debug[ich]->Draw("alp");
      cdebug->Update();
    }
    if(ADC_test_mode==1 && eLink_active==0xF)
    {
      cdebug->cd(loc_ch[4]);
      tg_debug[4]->Draw("alp");
      cdebug->cd(loc_ch[5]);
      tg_debug[5]->Draw("alp");
      cdebug->Update();
    }
  }
  if(debug>0)
  {
    system("stty raw");
    char cdum=getchar();
    system("stty -raw");
    if(cdum=='q')exit(-1);
  }

  trigger_type_old=trigger_type;
  ievt++;
  return error;
}

Int_t main ( Int_t argc,char* argv[] )
{

  signal(SIGINT, intHandler);
  signal(SIGABRT, abortHandler);

  FILE *fcal;
  Double_t NSPS=160.e6;
  Int_t timestamp, error;
  Int_t command, fead_ctrl;
  ValWord<uint32_t> address;
  ValVector< uint32_t > mem;
  for(Int_t ich=0; ich<6; ich++) event[ich]=(short int*)malloc(sizeof(short int)*5*NSAMPLE_MAX);
  for(Int_t ich=0; ich<6; ich++) gain[ich]=(short int*)malloc(sizeof(short int)*5*NSAMPLE_MAX);
  for(Int_t ich=0; ich<6; ich++) fevent[ich]=(double*)malloc(sizeof(double)*5*NSAMPLE_MAX);
  double dv=1200./4096.; // 12 bits on 1.2V
  Int_t debug=0;
  cdebug=NULL;
  char cdum;
  Int_t debug_draw=0;
// Define defaults for laser runs :
  Int_t nevent=1000;
  Int_t nsample=100;
  corgain=1;                    // Online gain correction when switching from G10 to G1
  Int_t trigger_type=0;         // pedestal by default
  Int_t soft_trigger=0;         // external trigger by default 
  Int_t self_trigger_mode=0;    // Absolute level for self trigger 
  Int_t self_trigger=0;         // No self trigger 
  Int_t self_trigger_threshold=0;
  Int_t self_trigger_mask     =0xF; // trig on all channels amplitude
  Int_t ADC_calib_mode        = 0;   // Put (1) or not (0) the outputs of CATIA in ADC_calib_mode (outputs near VCM)
  ADC_test_mode               = 1;   // Put (1) or not (0) the LiTE-DTU in test mode (direct access to 2 ADC outputs)
  ADC_MEM_mode                = 0;   // Put (1) or not (0) the LiTE-DTU in MEM mode (direct access to 2 ADC outputs at 80 MHz)
  ADC_invert_data             = 0;   // Invert (1) or not (0) LiTE-DTU data (usefull in case of AC coupling)
  Int_t delay_auto_tune       = 1;
  Int_t ADC_invert_clk        = 0;   // Invert (1) or not (0) LiTE-DTU clock
  Int_t seq_clock_phase       = 0;   // sequence clock phase by steps of 45 degrees
  Int_t IO_clock_phase        = 0;   // Capture clock phase by steps of 45 degrees
  Int_t reg_clock_phase       = 0;   // Shift register clock phase by steps of 45 degrees
  Int_t mem_clock_phase       = 0;   // memory clock phase by steps of 45 degrees
  Int_t clock_phase           = 0;
  Int_t n_TP_step=1;
  Int_t TP_step=128;
  Int_t TP_level=0;
  Int_t TP_width=150;
  Int_t TP_width_recovery=5;
  Int_t TP_delay=50;
  Int_t fead=4;
  char output_file[256];
  Int_t sw_DAQ_delay=0;
  Int_t hw_DAQ_delay=50;
  Int_t iret;
  Int_t reset_all=0;
  Int_t use_ref_ADC_calib=0;
  shift_oddH_samples=0;
  shift_oddL_samples=0;

  double ped[2], mean[2], rms[2];
  Double_t val_gain[2]={10.,1.};
  Double_t RTIA[3]={500.,400.,340.};
  Double_t Rshunt_V1P2=0.10;
  Double_t Rshunt_V2P5=0.10;

// CATIA settings if requested
  Int_t vfe            = 3;
  Int_t n_catia        = 1;
  Int_t Channel_num    = 3;
  Int_t CATIA_number   = 99999;
  Int_t CATIA_data     =-1; // Data to write in CATIA register
  Int_t CATIA_reg      =-1; // Register to write in with I2C protocol
  Int_t I2C_dir        = 0; // read(2)+write(1) with I2C protocol, 0=don't use I2C
  Int_t CATIA_Reg1_def = 0x02;   // Default content of Register 1 : SEU auto correction, no Temp output
  Int_t CATIA_Reg3_def[5] = {0xc003}; // Default content of Register 3 : 1.2V, LPF35, 340 Ohms, 0 ped
  Int_t CATIA_Reg4_def = 0x1000; // Default content of Register 4 : DAC1 0, DAC1 ON, DAC2 OFF, no copy, Vdac_buf ON
  Int_t CATIA_Reg5_def = 0x4000; // Default content of Register 5 : DAC2 0
  Int_t CATIA_Reg6_def = 0x0b;   // Default content of Register 6 : Vreg ON, Rconv=2471, TIA_dummy ON, TP ON
  Int_t DAC_buf_off    = 0;

  Int_t ped_mux        = 0; // No more used with CATIA-V1.4

//DTU settings if requested
  Int_t use_ref_calib    = 1;
  I2C_CATIA_type         = I2C_CATIA_TYPE;
  I2C_LiTEDTU_type       = I2C_LiTEDTU_TYPE;
  Int_t init_DTU         = 0;
  Int_t I2C_long         = 0;
  Int_t reset_ADC        = 0;
  Int_t calib_ADC        = 0;
  Int_t enable_40MHz_out = 0;
  int LVRB_autoscan      = 0;
  int invert_Resync      = 0;
  int I2C_toggle_SDA     = 0;
  int resync_phase       = 0;
  eLink_active       = 0xf;
  DTU_force_G1       = 0;
  DTU_force_G10      = 0;
  dump_data          = 0;
  UInt_t Resync_idle_pattern = 0x66;
  UInt_t Resync_Hamming_data = 0;
  UInt_t static_DTU_reset    = 0;
  UInt_t pll_conf[5]         = {0x3a, 0x3a, 0x1f, 0x3a, 0x3a};
  UInt_t pll_loc;
  UInt_t pll_override_Vc     = 0;
  buggy_DTU                  = 0;
  DTU_bulk1                  = 0x0007038F;
  DTU_bulk2                  = 0x88000000;
  DTU_bulk3                  = 0x00553040;
  DTU_bulk4                  = 0x65040000;
  DTU_bulk5                  = 0x000FFF3C;
  UInt_t bs_loc;
  UInt_t baseline_G10        = 0;
  UInt_t baseline_G1         = 0;
  I2C_DTU_nreg               = 19;

  wait_for_ever              = 1; // in laser trigger mode, we let wait for ever, waiting for a signal
  int VFE_reset  = 0;
  int IO_reset   = 0;
  int pwup_reset = 0;

  Int_t average        = 128;
  Int_t XADC_reg, XADC_Temp, XADC_Vdac, XADC_Vref, XADC_V2P5, XADC_V1P2;
  XADC_Vdac=0x10; // Buffered DAC signal for CATIA-V1-rev3 connected to APD_temp_in
  XADC_Temp=0x11; // Catia temperature connected to CATIA_temp_in
  //XADC_Vref=0x12; // Vref_reg connected to APD_temp_out (flying wire) (V1.4)
  XADC_Vref=0x11; // Vref_reg connected to CATIA_temp_in (V2.0)
  XADC_V2P5=0x13; // LVRB 2.5V connected to GPIO3
  XADC_V1P2=0x19; // LVRB 1.2V connected to GPIO2

  Int_t n_Vref=15, n_Vdac=33, n_PED_dac=64;

  TF1 *f1, *fDAC[2];
  TProfile *pshape[MAX_STEP];
  sprintf(output_file,"");

  Int_t wait=0, use_AWG=0;
  for(int k=1; k<argc; k++) 
  {    
    if(strcmp( argv[k], "-debug") == 0)
    {    
      sscanf( argv[++k], "%d", &debug );
      continue;
    }    
    else if(strcmp(argv[k],"-num") == 0)
    {
      sscanf( argv[++k], "%d", &CATIA_number );
      continue;
    }
    else if(strcmp(argv[k],"-wait") == 0)
    {
      sscanf( argv[++k], "%d", &wait );
      continue;
    }
    else if(strcmp(argv[k],"-use_AWG") == 0)
    {
      sscanf( argv[++k], "%d", &use_AWG );
      continue;
    }
    else if(strcmp(argv[k],"-use_ref_calib") == 0)
    {
      sscanf( argv[++k], "%d", &use_ref_calib );
      continue;
    }
    else if(strcmp(argv[k],"-h") == 0)
    {
      printf("Start DAQ with : \n");
      printf("-debug dbg_level            : Set debug level for this session [0]\n");
      printf("-num CATIA_number           : CATIA reference to identify the output file (rev*1000+i) [9999]\n");
      exit(-1);
    }
    else
    {
      printf("---------------------------------------------------\n");
      printf("Option %s not defined !, stop here.\n",argv[k]);
      printf("---------------------------------------------------\n");
      exit(-99);
    }
  }

  int loc_argc=1;
  char *loc_argv[10];
  for(int i=0; i<10; i++)loc_argv[i]=(char *)malloc(132*sizeof(char));
  sprintf(loc_argv[0],"test");
  TApplication *Root_App=new TApplication("test", &loc_argc, loc_argv);
  c1=new TCanvas();

  TGraph *tg[2];
  TH1D *hmean[2], *hrms[2];
  char hname[80];
  printf("start\n");
  for(int ich=0; ich<2; ich++)
  {
    printf("ich = %d\n",ich);
    tg[ich] = new TGraph();
    tg[ich]->SetMarkerStyle(20);
    tg[ich]->SetMarkerSize(0.5);
    sprintf(hname,"mean_ch%d",ich);
    hmean[ich]=new TH1D(hname,hname,100,150.,250.);
    sprintf(hname,"rms_ch%d",ich);
    hrms[ich]=new TH1D(hname,hname,200,0.,2.);
    printf("ich = %d\n",ich);
    fflush(stdout);
  }
  tg[0]->SetLineColor(kRed);
  tg[1]->SetLineColor(kBlue);
  tg[0]->SetMarkerColor(kRed);
  tg[1]->SetMarkerColor(kBlue);
  int dac_val=0;
  if(TP_level>0)dac_val=TP_level;

  ConnectionManager manager ( "file://xml/FEAD/connection_file.xml" );
  uhal::HwInterface hw=manager.getDevice( "fead.udp.3" );

  unsigned int ireg,device_number,val;
  ValWord<uint32_t> free_mem, trig_reg, delays, reg, current, fault;
  ValWord<uint32_t> debug1_reg[32];
  ValWord<uint32_t> debug2_reg[32];

// Read FW version to check :
  reg = hw.getNode("FW_VER").read();
  hw.getNode("FEAD_CTRL").write( RESET*1);
  hw.getNode("FEAD_CTRL").write( RESET*0);
  hw.dispatch();
  printf("Reset board with firmware version      : %8.8x\n",reg.value());

// Initialize current monitors
  for(int iLVR=0; iLVR<2; iLVR++)
  {
    printf("\nProgram LV sensor %d : ",iLVR);
    if(iLVR==0)
    {
      device_number=0x67;
      printf("V2P5\n");
    }
    else
    {
      device_number=0x6C;
      printf("V1P2\n");
    }
// Put LVRB in continuous ADC mode, monitor sense+, and power calculation with sense+
    int ireg=0; // control register
    command=0x5;
    val=I2C_RW(hw, device_number, ireg, command,0, 3, debug);
    printf("New content of control register : 0x%x\n",val&0xffff);
// Max power limit
    val=I2C_RW(hw, device_number, 0xe,  0xff,0, 1, debug);
    val=I2C_RW(hw, device_number, 0xf,  0xff,0, 1, debug);
    val=I2C_RW(hw, device_number, 0x10, 0xff,0, 1, debug);
// Min power limit
    val=I2C_RW(hw, device_number, 0x11, 0x00,0, 1, debug);
    val=I2C_RW(hw, device_number, 0x12, 0x00,0, 1, debug);
    val=I2C_RW(hw, device_number, 0x13, 0x00,0, 1, debug);
// Max ADin limit
    val=I2C_RW(hw, device_number, 0x2E, 0xFFF0,1, 3, debug);
// Min ADin limit
    val=I2C_RW(hw, device_number, 0x30, 0x0000,1, 3, debug);
// Max Vin limit
    val=I2C_RW(hw, device_number, 0x24, 0xFFF0,1, 3, debug);
// Min Vin limit
    val=I2C_RW(hw, device_number, 0x26, 0x0000,1, 3, debug);
// Max current limit
    val=I2C_RW(hw, device_number, 0x1A, 0xFFF0,1, 3, debug);
    printf("New max threshold on current 0x%x\n",val&0xffff);
// Min current limit
    val=I2C_RW(hw, device_number, 0x1C, 0x0000,1, 3, debug);
    printf("New min threshold on current 0x%x\n",val&0xffff);
// Read and clear Fault register :
    ireg=4; // Alert register
    val=I2C_RW(hw, device_number, ireg, command,0, 2, debug);
    printf("Alert register content : 0x%x\n",val&0xffff);
    val=I2C_RW(hw, device_number, ireg, command,0, 2, debug);
    printf("Alert register content : 0x%x\n",val&0xffff);
// Set Alert register
    ireg=1;
    command=0x20; // Alert on Dsense overflow
    val=I2C_RW(hw, device_number, ireg, command,0, 3, debug);
    printf("New alert bit pattern : 0x%x\n", val&0xffff);
  }
  printf("\n");
  printf("Redo pwup_reset and Start LVRB auto scan mode\n");
  hw.getNode("VFE_CTRL").write(PWUP_RESETB*1);
  hw.getNode("VFE_CTRL").write(0);
  hw.getNode("VFE_CTRL").write(LVRB_AUTOSCAN*1);
  hw.dispatch();

// Wait for voltage stabilization
  usleep(1000000);

// Read voltages measured by controler
  for(int iLVR=0; iLVR<2; iLVR++)
  {
    char alim[80];
    if(iLVR==0)
    {
      device_number=0x67;
      sprintf(alim,"V2P5");
    }
    else
    {
      device_number=0x6C;
      sprintf(alim,"V1P2");
    }
    val=I2C_RW(hw, device_number, 0x1E, 0,1, 2, 0);
    double Vmeas=((val>>4)&0xfff);
    Vmeas=Vmeas/4096.*102.4; // Convert to V (102.4 V full scale)
    printf("%s measured power supply voltage %7.3f V\n",alim,Vmeas);
    val=I2C_RW(hw, device_number, 0x28, 0,1, 2, 0);
    Vmeas=((val>>4)&0xfff);
    Vmeas=Vmeas/4096.*2.048; // Convert to V (2.048 V full scale)
    if(iLVR==0)Vmeas*=2.;
    printf("%s measured sense voltage %7.3f V\n",alim,Vmeas);
  }

  unsigned int VFE_control= INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan | eLINK_ACTIVE*eLink_active |
                            ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode | ADC_MEM_MODE*ADC_MEM_mode;

// Reset board
  printf("Generate Warm reset\n");
  hw.getNode("FEAD_CTRL").write(RESET*1);
  hw.getNode("FEAD_CTRL").write(RESET*0);
  hw.dispatch();
  usleep(10000);
  hw.getNode("DELAY_CTRL").write(1*DELAY_RESET);
  printf("Get lock status of IDELAY input stages\n");
  reg = hw.getNode("DELAY_CTRL").read();
  hw.dispatch();
  usleep(1000);
  printf("Value read : 0x%x\n",reg.value());
  
// Put DTU resync in reset mode, stop uLVRB autoscan mod
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.getNode("RESYNC_IDLE").write(I2C_toggle_SDA*I2C_TOGGLE_SDA  | RESYNC_IDLE_PATTERN*Resync_idle_pattern | ADC_INVERT_DATA*ADC_invert_data );
  hw.getNode("CLK_SETTING").write(resync_phase*RESYNC_PHASE);
  hw.dispatch();
  usleep(200);

  printf("Prepare LiTEDTU for safe running (generate ReSync start sequence)\n");
// DTU Resync init sequence:
  //hw.getNode("DTU_RESYNC").write(LiTEDTU_stop);
  //hw.getNode("DTU_RESYNC").write(LiTEDTU_start);
  hw.getNode("DTU_RESYNC").write(LiTEDTU_DTU_reset);
  hw.getNode("DTU_RESYNC").write(LiTEDTU_I2C_reset);
  hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCTestUnit_reset);
  if((reset_ADC&1) == 1) hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCH_reset);
  if((reset_ADC&2) == 2) hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCL_reset);
  hw.getNode("DTU_BULK1").write(DTU_bulk1);
  hw.getNode("DTU_BULK2").write(DTU_bulk2);
  DTU_bulk3=(DTU_bulk3&0xffff00fe)|((pll_conf[2]&0xFF)<<8)|((pll_conf[2]&0x100)>>8);
  hw.getNode("DTU_BULK3").write(DTU_bulk3);
  hw.getNode("DTU_BULK4").write(DTU_bulk4);
  hw.getNode("DTU_BULK5").write(DTU_bulk5);
  hw.dispatch();
  usleep(200);

// Put FEAD outputs with idle patterns :
  hw.getNode("OUTPUT_CTRL").write(0);
  hw.dispatch();

// TP trigger setting :
  command=(TP_delay<<16) | (TP_width&0xffff);
  printf("TP trigger with %d clocks width and %d clocks delay : %x\n",TP_width,TP_delay,command);
  hw.getNode("CALIB_CTRL").write(command);
  hw.dispatch();

// Init stage :
// Read FW version to check :
  reg = hw.getNode("FW_VER").read();
// Switch to triggered mode + external trigger :
  command=
             CALIB_PULSE_ENABLED   *0                                |
             CALIB_MUX_ENABLED     *1                                |
             SELF_TRIGGER_MODE     *self_trigger_mode                |
            (SELF_TRIGGER_MASK     *(self_trigger_mask&0x1F))        |
            (SELF_TRIGGER_THRESHOLD*(self_trigger_threshold&0xFFF))  |
             SELF_TRIGGER          *self_trigger                     |
             SOFT_TRIGGER          *soft_trigger                     |
             TRIGGER_MODE          *1  | // Always DAQ on trigger
             RESET                 *0;
  hw.getNode("FEAD_CTRL").write(command);
// Stop DAQ and ask for NSAMPLE per frame (+timestamp) :
  command = ((nsample/4+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
// Add laser latency before catching data ~ 40 us
  hw.getNode("TRIG_DELAY").write(SW_DAQ_DELAY*sw_DAQ_delay | HW_DAQ_DELAY*hw_DAQ_delay);
// Switch off FE-adapter LEDs
  hw.getNode("FW_VER").write(0);
  hw.dispatch();

// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
  old_read_address=-1;

// Set the clock phases
  command = MEM_CLOCK_PHASE*mem_clock_phase+REG_CLOCK_PHASE*reg_clock_phase+IO_CLOCK_PHASE*IO_clock_phase+SEQ_CLOCK_PHASE*seq_clock_phase;
  hw.getNode("CLK_SETTING").write(command);
  hw.dispatch();

// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
// Start DAQ :
  command = ((nsample/4+1)<<16)+CAPTURE_START;
  hw.getNode("CAP_CTRL").write(command);
// Read back delay values :
  delays=hw.getNode("TRIG_DELAY").read();
// Read back the read/write base address
  address = hw.getNode("CAP_ADDRESS").read();
  free_mem = hw.getNode("CAP_FREE").read();
  trig_reg = hw.getNode("FEAD_CTRL").read();
  hw.dispatch();

  printf("Firmware version      : %8.8x\n",reg.value());
  printf("Delays                : %8.8x\n",delays.value());
  printf("Initial R/W addresses : 0x%8.8x\n", address.value());
  printf("Free memory           : 0x%8.8x\n", free_mem.value());
  printf("Trigger mode          : 0x%8.8x\n", trig_reg.value());
  old_read_address=address&0xffff;
  if(old_read_address==NSAMPLE_MAX-1)old_read_address=-1;

// Load default DTU registers
// Surprisingly, when we load LitE-DTU registers, we have a calibration process. So put ped-mux in position :
  VFE_control= DELAY_AUTO_TUNE*delay_auto_tune | INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan |
               eLINK_ACTIVE*eLink_active | ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode | ADC_MEM_MODE*ADC_MEM_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();
  usleep(100000); // Let some time for calibration voltages to stabilize

  Int_t loc_data;
  device_number=I2C_LiTEDTU_type*1000+(Channel_num<<SHIFT_DEV_NUMBER)+2;      // DTU sub-address
  printf("Device number : %d 0x%x\n",device_number,device_number);
  for(Int_t ireg=0; ireg<I2C_DTU_nreg; ireg++)
  {
    if     (ireg<4) loc_data=(DTU_bulk1>>((ireg-0)*8))&0xFF;
    else if(ireg<8) loc_data=(DTU_bulk2>>((ireg-4)*8))&0xFF;
    else if(ireg<12)loc_data=(DTU_bulk3>>((ireg-8)*8))&0xFF;
    else if(ireg<16)loc_data=(DTU_bulk4>>((ireg-12)*8))&0xFF;
    else            loc_data=(DTU_bulk5>>((ireg-16)*8))&0xFF;
    if(ireg==5) loc_data=baseline_G10;
    if(ireg==6) loc_data=baseline_G1;
    iret=I2C_RW(hw, device_number, ireg, loc_data, 0, 3, debug);
    if(debug>0)printf("Single write return code (1) : %8.8x -> %d ACK\n",iret,(iret>>23)&0xff);
  }
  loc_data=0xff0-baseline_G10; // switch gain at 4090 - substrated baseline
  iret=I2C_RW(hw, device_number, 17, loc_data&0xff, 0, 1, debug);
  iret=I2C_RW(hw, device_number, 18, (loc_data>>8)&0xf, 0, 1, debug);

  VFE_control= DELAY_AUTO_TUNE*delay_auto_tune | INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan |
               eLINK_ACTIVE*eLink_active | ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode | ADC_MEM_MODE*ADC_MEM_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();

// Set CATIA outputs with calibration levels
  device_number=1000+(Channel_num<<SHIFT_DEV_NUMBER)+3;      // CATIA address
  iret=I2C_RW(hw, device_number, 5, CATIA_Reg5_def|0x3000, 1, 3, debug);
  printf("Set CATIA outputs with calibration levels : 0x%4.4x\n",iret&0xFFFF);
  usleep(100000); // Let some time for calibration voltages to stabilize

  hw.getNode("DELAY_CTRL").write(DELAY_RESET*1);
  hw.dispatch();
  VFE_control= DELAY_AUTO_TUNE*delay_auto_tune | INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan |
               eLINK_ACTIVE*eLink_active | ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode | ADC_MEM_MODE*ADC_MEM_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();
  usleep(100000); // Let some time for calibration voltages to stabilize
  hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCH_reset);
  hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCL_reset);
  hw.dispatch();
  usleep(1000);
  printf("Launch ADC calibration !\n");
  hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCH_calib);
  hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCL_calib);
  hw.dispatch();
  usleep(1000);

// Reset Test unit to get samples in right order :
  hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCTestUnit_reset);
  hw.dispatch();

// Restore CAL mux in normal position after calibration
  iret=I2C_RW(hw, device_number, 5, CATIA_Reg5_def&0x4FFF, 1, 3, debug);
  printf("Set CATIA outputs with physics levels : 0x%4.4x\n",iret&0xFFFF);
  usleep(100000); // Let some time for DC voltages to stabilize

// Read Temperature and others analog voltages with FPGA ADC
// Read XADC register 0x40 and set the requested average to 1 in XADC
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  ValWord<uint32_t> ave  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  unsigned loc_ave=ave.value()&0xffff;
  printf("Old config register 0x40 content : %x\n",loc_ave);

  loc_ave=0x8000;
  command=DRP_WRb*1 | (0x40<<16) | loc_ave;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  ave  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  loc_ave=ave.value()&0xffff;
  printf("New config register 0x40 content : %x\n",loc_ave);
  command=DRP_WRb*1 | (0x42<<16) | 0x0400;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();
// Switch off temp output on CATIA :
  val=I2C_RW(hw, device_number, 1, 0x2,0, 3, debug);

  Int_t reg_def[7]={0x0,0x2,0x0,0x1087,0xFFFF,0xFFFF,0xF};
  Int_t reg_error[7]={0};

    if(wait==1)
    {
      system("stty raw");
      cdum=getchar();
      system("stty -raw");
      if(cdum=='q')exit(-1);
    }
//===========================================================================================================
// 0 : Get Vdac vs DAC transfer function for current injection 
// Now, make the temp measurement on all catias, sequentially end for both current gain
TGraph *tg_Vref=new TGraph();
tg_Vref->SetLineColor(kRed);
tg_Vref->SetMarkerColor(kRed);
tg_Vref->SetMarkerStyle(20);
tg_Vref->SetMarkerSize(1.);
tg_Vref->SetName("Vref_vs_reg");
tg_Vref->SetTitle("Vref_vs_reg");
TGraph *tg_Vdac[2], *tg_Vdac_resi[2];
tg_Vdac[0] =new TGraph();
tg_Vdac[0]->SetLineColor(kRed);
tg_Vdac[0]->SetMarkerColor(kRed);
tg_Vdac[0]->SetMarkerStyle(20);
tg_Vdac[0]->SetMarkerSize(1.);
tg_Vdac[0]->SetName("Vdac1_vs_reg");
tg_Vdac[0]->SetTitle("Vdac1_vs_reg");
tg_Vdac[1] =new TGraph();
tg_Vdac[1]->SetLineColor(kBlue);
tg_Vdac[1]->SetMarkerColor(kBlue);
tg_Vdac[1]->SetMarkerStyle(20);
tg_Vdac[1]->SetMarkerSize(1.);
tg_Vdac[1]->SetName("Vdac2_vs_reg");
tg_Vdac[1]->SetTitle("Vdac2_vs_reg");
tg_Vdac_resi[0] =new TGraph();
tg_Vdac_resi[0]->SetLineColor(kRed);
tg_Vdac_resi[0]->SetMarkerColor(kRed);
tg_Vdac_resi[0]->SetMarkerStyle(20);
tg_Vdac_resi[0]->SetMarkerSize(1.);
tg_Vdac_resi[0]->SetName("Vdac1_resi_vs_reg");
tg_Vdac_resi[0]->SetTitle("Vdac1_resi_vs_reg");
tg_Vdac_resi[1] =new TGraph();
tg_Vdac_resi[1]->SetLineColor(kBlue);
tg_Vdac_resi[1]->SetMarkerColor(kBlue);
tg_Vdac_resi[1]->SetMarkerStyle(20);
tg_Vdac_resi[1]->SetMarkerSize(1.);
tg_Vdac_resi[1]->SetName("Vdac2_resi_vs_reg");
tg_Vdac_resi[1]->SetTitle("Vdac2_resi_vs_reg");
Double_t DAC_val[33], Vdac[2][33], Vdac_offset[2], Vdac_slope[2], Vref[15], Temp[3];
Int_t ref_best=0;
Double_t dref_best=99999.;
for(int XADC_meas=0; XADC_meas<5; XADC_meas++)
{
  XADC_reg=0;
  if(XADC_meas==1)XADC_reg=XADC_Temp; // Temperature measurement
  if(XADC_meas==2)XADC_reg=XADC_Vref; // CATIA Vref measurement (pin 22)
  if(XADC_meas==3)XADC_reg=XADC_Vdac; // CATIA Vdac1 measurement (pin 25)
  if(XADC_meas==4)XADC_reg=XADC_Vdac; // CATIA Vdac2 measurement (pin 25)

    Int_t nmeas=1;
    if(XADC_meas==1)nmeas=2;
    if(XADC_meas==2)nmeas=n_Vref;
    if(XADC_meas==3)nmeas=n_Vdac;
    if(XADC_meas==4)nmeas=n_Vdac;
    for(int imeas=0; imeas<nmeas; imeas++)
    {
      int loc_meas=imeas;
      if(XADC_meas==1) // CATIA Temp measurements
      {
        command= (imeas<<4) | 0xa;
        val=I2C_RW(hw, device_number, 1, command,0, 3, debug); // Switch ON temp output + temp gain
        val=I2C_RW(hw, device_number, 5, 0x4000, 1, 3, debug);   // switch OFF Vref output
        usleep(10000);
      }
      if(XADC_meas==2) // Scan Vref_reg values
      {
        val=I2C_RW(hw, device_number, 1, 0x02, 0, 3, debug); // switch ON temp output
        val=I2C_RW(hw, device_number, 5, 0x0000, 1, 3, debug); // switch ON Vref output
        if(imeas>0)loc_meas=imeas+1;
        command= (loc_meas<<4) | 0xF;
        val=I2C_RW(hw, device_number, 6, command,0, 3, debug);
        usleep(10);
      }
      if(XADC_meas==3) // Scan Vdac values
      {
        val=I2C_RW(hw, device_number, 1, 0x02, 0, 3, debug); // switch OFF temp output
        val=I2C_RW(hw, device_number, 5, 0x4000, 1, 3, debug);   // switch OFF Vref output
        if(imeas==0)
        {
          printf("Setting Vref to 1 V : %d\n",ref_best);
          val=I2C_RW(hw, device_number, 6, (ref_best<<4) | 0x0F, 0, 3, debug); // Enable Vref_reg, 1.0V in Vref_dac
        }
        loc_meas=imeas*128;
        if(loc_meas>4095)loc_meas=4095;
        command=(DAC_buf_off<<15) | 0x1000 | loc_meas; // Scan DAC1 values 
        val=I2C_RW(hw, device_number, 4, command,1, 3, debug);
        if(imeas==0)usleep(100000);
        usleep(1000);
      }
      if(XADC_meas==4) // Scan Vdac values
      {
        val=I2C_RW(hw, device_number, 6, (ref_best<<4) | 0xF,0, 3, debug); // Enable Vref_reg, 0 in Vref_dac
        val=I2C_RW(hw, device_number, 4, (DAC_buf_off<<15) | 0x2000,1, 3, debug); // Output on DAC2 and enable Vdac buffer on Rev3
        ireg=5; // DAC2 control register
        loc_meas=imeas*128;
        if(loc_meas>4095)loc_meas=4095;
        val=I2C_RW(hw, device_number, 5, 0x4000 | loc_meas,1, 3, debug);
        if(imeas==0)usleep(100000);
        usleep(1000);
      }

      double ave_val=0.;
      ValWord<uint32_t> temp;
      for(int iave=0; iave<average; iave++)
      {
        command=DRP_WRb*0 | (XADC_reg<<16);
        hw.getNode("DRP_XADC").write(command);
        temp  = hw.getNode("DRP_XADC").read();
        hw.dispatch();
        double loc_val=double((temp.value()&0xffff)>>4)/4096.;
        ave_val+=loc_val;
      }
      ave_val/=average;

      if(XADC_meas==0)
      {
        Temp[0]=ave_val*4096*0.123-273.;
        printf("FPGA temperature, meas %d : %.2f deg\n",imeas, Temp[0]);
      }
      else if(XADC_meas==1)
      {
        ave_val/=0.771; // 3.3k-1k Resistor bridge on PCB
        Temp[imeas+1]=ave_val;
        printf("CATIA 3, X5 %d, temperature : %.4f V\n", imeas,Temp[imeas+1]);
      }
      else if(XADC_meas==2)
      {
        //ave_val*=2.; // 1k-1k Resistor bridge on PCB
        ave_val/=0.771; // 3.3k-1k Resistor bridge on PCB
        printf("CATIA 0, Vref DAC %d, Vref : %.4f V\n", loc_meas,ave_val);
        Vref[imeas]=ave_val;
        tg_Vref->SetPoint(imeas,(Double_t)loc_meas,Vref[imeas]);
        if(fabs(Vref[imeas]-1.000)<dref_best)
        {
          dref_best=fabs(Vref[imeas]-1.000);
          ref_best=loc_meas;
          printf("%d %f %d\n",loc_meas,dref_best,ref_best);
        }
        if(CATIA_number<20000)ref_best=3;
        if(wait==1)
        {
          system("stty raw");
          cdum=getchar();
          system("stty -raw");
          if(cdum=='q')exit(-1);
        }
      }
      else if(XADC_meas==3)
      {
        //printf("CATIA 0, TP DAC1 %d, Vdac : %.4f V\n", loc_meas, ave_val);
        //ave_val*=2.; // 1k-1k Resistor bridge on PCB
        ave_val/=0.771; // 3.3k-1k Resistor bridge on PCB
        tg_Vdac[0]->SetPoint(imeas,(Double_t)loc_meas,ave_val);
        DAC_val[imeas]=loc_meas;
        Vdac[0][imeas]=ave_val;
        //printf("DAC %d : %f\n",loc_meas,ave_val);
      }
      else if(XADC_meas==4)
      {
        //ave_val*=2.; // 1k-1k Resistor bridge on PCB
        ave_val/=0.771; // 3.3k-1k Resistor bridge on PCB
        //printf("CATIA 0, TP DAC2 %d, Vdac : %.4f V\n", loc_meas, ave_val);
        tg_Vdac[1]->SetPoint(imeas,(Double_t)loc_meas,ave_val);
        Vdac[1][imeas]=ave_val;
      }
    }
  }
  c1->cd();
  for(int iDAC=0; iDAC<2; iDAC++)
  {
    tg_Vdac[iDAC]->Fit("pol1","WQ","",0.,3500.);
    fDAC[iDAC]=tg_Vdac[iDAC]->GetFunction("pol1");
    Int_t n=tg_Vdac[iDAC]->GetN();
    for(Int_t i=0; i<n; i++)
    {
      Double_t x,y;
      tg_Vdac[iDAC]->GetPoint(i,x,y);
      tg_Vdac_resi[iDAC]->SetPoint(i,x,y-fDAC[iDAC]->Eval(x));
    }
    Vdac_offset[iDAC]=fDAC[iDAC]->GetParameter(0)*1000.;
    Vdac_slope[iDAC]=fDAC[iDAC]->GetParameter(1)*1000.;
    printf("Vdac%d offset : %e mV, slope : %e mV/lsb\n",iDAC,Vdac_offset[iDAC],Vdac_slope[iDAC]); 
  }

// Read current values for control :
  printf("Consumption with DAC1 OFF and DAC2 ON :\n");
  val=I2C_RW(hw, 0x67, 0x14, 0,1, 2, debug); // Read Dsense value
  printf("CATIA V2P5 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V2P5);
  val=I2C_RW(hw, 0x6C, 0x14, 0,1, 2, debug); // Read Dsense value
  printf("CATIA V1P2 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V1P2);
  if(wait==1)
  {
    system("stty raw");
    cdum=getchar();
    system("stty -raw");
    if(cdum=='q')exit(-1);
  }

// Switch OFF DACs and re-measure power consumption :
  device_number=1000+(Channel_num<<SHIFT_DEV_NUMBER)+3;      // CATIA address
  iret=I2C_RW(hw, device_number, 1, 2, 0, 3, debug);
  iret=I2C_RW(hw, device_number, 4, 0x8000, 1, 3, debug);
  iret=I2C_RW(hw, device_number, 5, 0x4000, 1, 3, debug);
  usleep(200000);
// Read current values for control :
  printf("Consumption with DAC1 OFF and DAC2 OFF, Vref ON :\n");
  val=I2C_RW(hw, 0x67, 0x14, 0,1, 2, debug); // Read Dsense value
  printf("CATIA V2P5 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V2P5);
  val=I2C_RW(hw, 0x6C, 0x14, 0,1, 2, debug); // Read Dsense value
  printf("CATIA V1P2 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V1P2);
  if(wait==1)
  {
    system("stty raw");
    cdum=getchar();
    system("stty -raw");
    if(cdum=='q')exit(-1);
  }

// Prepare CATIA for calibration :
  iret=I2C_RW(hw, device_number, 1, 2, 0, 3, debug);
  iret=I2C_RW(hw, device_number, 4, 0x8000, 1, 3, debug);
  iret=I2C_RW(hw, device_number, 5, 0x4000, 1, 3, debug);
  iret=I2C_RW(hw, device_number, 6, (ref_best<<4) | 0x0B, 0, 3, debug);
  usleep(200000);
// Read current values for control :
  printf("Consumption before calibration : DAC1 OFF, DAC2 OFF, Vref ON\n");
  val=I2C_RW(hw, 0x67, 0x14, 0,1, 2, debug); // Read Dsense value
  printf("CATIA V2P5 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V2P5);
  val=I2C_RW(hw, 0x6C, 0x14, 0,1, 2, debug); // Read Dsense value
  printf("CATIA V1P2 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V1P2);

//===========================================================================================================
// 0.9 : Redo ADC calibration after Vref_reg setting :
// Stop LVRB auto scan, not to generate noise in CATIA during calibration
  VFE_control= DELAY_AUTO_TUNE*1 | LVRB_AUTOSCAN*0 |
               eLINK_ACTIVE*eLink_active | ADC_TEST_MODE*1;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();

  if(wait==1)
  {
    system("stty raw");
    cdum=getchar();
    system("stty -raw");
    if(cdum=='q')exit(-1);
  }
// Set CATIA outputs with calibration levels
  device_number=1000+(Channel_num<<SHIFT_DEV_NUMBER)+3;      // CATIA address
  iret=I2C_RW(hw, device_number, 5, 0x7000, 1, 3, debug);
  printf("Set CATIA outputs with calibration levels : 0x%4.4x\n",iret&0xFFFF);
  usleep(100000); // Let some time for calibration voltages to stabilize

// Read current values for control :
  printf("Consumption before calibration, mux ON :\n");
  val=I2C_RW(hw, 0x67, 0x14, 0,1, 2, debug); // Read Dsense value
  printf("CATIA V2P5 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V2P5);
  val=I2C_RW(hw, 0x6C, 0x14, 0,1, 2, debug); // Read Dsense value
  printf("CATIA V1P2 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V1P2);

  hw.getNode("DELAY_CTRL").write(DELAY_RESET*1);
  hw.dispatch();
  hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCH_reset);
  hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCL_reset);
  hw.dispatch();
  usleep(1000);
  printf("Launch ADC calibration !\n");
  hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCH_calib);
  hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCL_calib);
  hw.dispatch();
// Wait for ADC calibration :  
  usleep(200000);
  if(wait==1)
  {
    system("stty raw");
    cdum=getchar();
    system("stty -raw");
    if(cdum=='q')exit(-1);
  }
// And put back Physics signals at CATIA outputs
// Restore CAL mux in normal position after calibration and switch OFF Vref
  iret=I2C_RW(hw, device_number, 5, 0x4000, 1, 3, debug);
  printf("Set CATIA outputs with physics levels : 0x%4.4x\n",iret&0xFFFF);
  usleep(100000); // Let some time for DC voltages to stabilize

  if(wait==1)
  {
    system("stty raw");
    cdum=getchar();
    system("stty -raw");
    if(cdum=='q')exit(-1);
  }

  if(use_ref_calib==1)
  {
// Save ADC calibration (full list of registers :
    sprintf(output_file,"data/CATIA_calib/ADC_ref_calib.dat");
    fcal=fopen(output_file,"r");
    for(int ireg=0; ireg<N_ADC_REG; ireg++)
    {
      //printf("Register %d : 0x%2.2x 0x%2.2x\n",ireg,ADC_reg_val[0][ireg]&0xFF, ADC_reg_val[1][ireg]&0xFF);
      fscanf(fcal,"%d %d",&ADC_reg_val[0][ireg], &ADC_reg_val[1][ireg]);
    }
    fclose(fcal);
    for(int iADC=0; iADC<2; iADC++)
    {
      Int_t device_number=I2C_LiTEDTU_type*1000+(Channel_num<<SHIFT_DEV_NUMBER)+iADC;      // ADC sub-addresses
      for(int ireg=0; ireg<N_ADC_REG; ireg++)
      {
        ADC_reg_val[iADC][ireg]=I2C_RW(hw, device_number, ireg, ADC_reg_val[iADC][ireg], 0, 1, 0);
      }
    }
  }

// Reset Test unit to get samples in right order :
  hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCTestUnit_reset);

// Read current values for control :
  printf("After LiTE-DTU calibration :\n");
  val=I2C_RW(hw, 0x67, 0x14, 0,1, 2, debug); // Read Dsense value
  printf("CATIA V2P5 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V2P5);
  val=I2C_RW(hw, 0x6C, 0x14, 0,1, 2, debug); // Read Dsense value
  printf("CATIA V1P2 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V1P2);

//===========================================================================================================
// 1 : Pedestal value vs PED_DAC value.
// Read 1 event of 1000 samples
  printf("Start Pedestal study\n");
  TGraph *tg_ped_DAC[2];
  tg_ped_DAC[0]=new TGraph();
  tg_ped_DAC[0]->SetLineColor(kRed);
  tg_ped_DAC[0]->SetMarkerColor(kRed);
  tg_ped_DAC[0]->SetMarkerSize(kRed);
  tg_ped_DAC[0]->SetName("Ped_vs_DAC_G1");
  tg_ped_DAC[0]->SetTitle("Ped_vs_DAC_G1");
  tg_ped_DAC[1]=new TGraph();
  tg_ped_DAC[1]->SetLineColor(kBlue);
  tg_ped_DAC[1]->SetMarkerColor(kBlue);
  tg_ped_DAC[1]->SetMarkerSize(kBlue);
  tg_ped_DAC[1]->SetName("Ped_vs_DAC_G10");
  tg_ped_DAC[1]->SetTitle("Ped_vs_DAC_G10");
  Double_t ped_DAC_par[2][2];
  trigger_type = 0;
  nevent       = 1;
  nsample      = 250;
// Switch to triggered mode + internal trigger, reduce noise by disabling CALIB_PULSE  :
  fead_ctrl=  CALIB_PULSE_ENABLED   *0 |
              CALIB_MUX_ENABLED     *1 |
              ADC_CALIB_MODE        *0 |
              SELF_TRIGGER_MASK     *0 |
              SELF_TRIGGER_THRESHOLD*0 |
              SELF_TRIGGER          *0 |
              SOFT_TRIGGER          *1 |
              TRIGGER_MODE          *1 | // Always DAQ on trigger
              RESET                 *0;
  hw.getNode("FEAD_CTRL").write(fead_ctrl);
// Stop DAQ and ask for NSAMPLE per frame (+timestamp) :
  command = ((nsample/4+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();

// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
  old_read_address=-1;

// Start DAQ :
  command = ((nsample/4+1)<<16)+CAPTURE_START;
  hw.getNode("CAP_CTRL").write(command);
  reg=hw.getNode("CAP_CTRL").read();
  hw.dispatch();
  //printf("CAP_CTRL content : 0x%8.8x\n",reg.value());

  val=I2C_RW(hw, device_number, 1, 2,0, 3, debug); // Switch off temp sensor
  val=I2C_RW(hw, device_number, 4, 0x8000,1, 3, debug); // Switch off TP stuff except Vref 
  val=I2C_RW(hw, device_number, 5, 0x4000,1, 3, debug);
  val=I2C_RW(hw, device_number, 6, (ref_best<<4) | 0x0B, 0, 3, debug);
  usleep(100000);

  Int_t ped_ref[2]={-1,-1};
  for(int idac=0; idac<n_PED_dac; idac++)
  {
    mean[0]=0.;
    mean[1]=0.;
    //printf("VCM DAC : %d\n",idac);
    val=I2C_RW(hw, device_number, 3, 0xc000 | (idac<<8) | (idac<<2) | 3,1, 3, debug);
    usleep(100000);
    //debug_draw=1;
    debug_draw=(idac%4);
    for(int ievt=0; ievt<nevent; ievt++)
    {
      error=get_event(hw,trigger_type,nsample/4,debug,debug_draw);
      debug_draw=0;
      for(Int_t is=0; is<nsample; is++)
      {
        mean[0]+=fevent[4][is]*dv;
        mean[1]+=fevent[5][is]*dv;
      }
    }
    mean[0]/=(nsample*nevent);
    mean[1]/=(nsample*nevent);
    tg_ped_DAC[0]->SetPoint(idac,(Double_t)idac,mean[0]);
    tg_ped_DAC[1]->SetPoint(idac,(Double_t)idac,mean[1]);
    if(mean[0]/dv<40. && ped_ref[0]<0.)ped_ref[0]=idac;
    if(mean[1]/dv<40. && ped_ref[1]<0.)ped_ref[1]=idac;
    printf("%d %f %d, %f %d\n",idac,mean[0]/dv,ped_ref[0],mean[1]/dv,ped_ref[1]);
    if(wait==1)
    {
      system("stty raw");
      cdum=getchar();
      system("stty -raw");
      if(cdum=='q')exit(-1);
    }
  }
  ped_ref[0]&=0x3F;
  ped_ref[1]&=0x3F;
  c1->cd();
  tg_ped_DAC[0]->Fit("pol1","WQ","",0.,ped_ref[0]);
  f1=tg_ped_DAC[0]->GetFunction("pol1");
  ped_DAC_par[0][0]=f1->GetParameter(0);
  ped_DAC_par[0][1]=f1->GetParameter(1);
  printf("G10  pedestal offset %.2f mV, slope %.2f mV/lsb\n",ped_DAC_par[0][0],ped_DAC_par[0][1]);
  tg_ped_DAC[1]->Fit("pol1","WQ","",0.,ped_ref[1]);
  f1=tg_ped_DAC[1]->GetFunction("pol1");
  ped_DAC_par[1][0]=f1->GetParameter(0);
  ped_DAC_par[1][1]=f1->GetParameter(1);
  printf("G1  pedestal offset %.2f mV, slope %.2f mV/lsb\n",ped_DAC_par[1][0],ped_DAC_par[1][1]);
// Stop DAQ
  command = ((nsample/4+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();

//===========================================================================================================
// 2 : Noise for R=340, R=400 and R=500 and for LPF=50MHz and LPF=35 MHz
  Double_t G1_noise[3][2]={0.,0.,0.,0.,0.,0.};
  Double_t G10_noise[3][2]={0.,0.,0.,0.,0.,0.};
  trigger_type=0;
  nevent=1;
  //nevent=10;
  nsample=26622*4;
  Double_t *rex,*rey,*imx,*imy,*mod;
  rex=(Double_t*)malloc(nsample*sizeof(Double_t));
  imx=(Double_t*)malloc(nsample*sizeof(Double_t));
  rey=(Double_t*)malloc(nsample*sizeof(Double_t));
  imy=(Double_t*)malloc(nsample*sizeof(Double_t));
  mod=(Double_t*)malloc(nsample*sizeof(Double_t));
  TVirtualFFT *fft_f=TVirtualFFT::FFT(1,&nsample,"C2CF K");
  Double_t dt=1./NSPS;
  Double_t tmax=nsample*dt;
  Double_t fmax=1./dt;
  Double_t df=fmax/nsample;
  TProfile *pmod[2];
  pmod[0]=new TProfile("NDS_G10","NDS_G10",nsample,0,fmax);
  pmod[1]=new TProfile("NDS_G1","NDS_G1",nsample,0,fmax);
  TH1D *hmod[2][6];
  hmod[0][0]=new TH1D("NDS_G10_RTIA_500_LPF_50","NDS_G10_RTIA_500_LPF_50",nsample/2,0.,fmax/2.);
  hmod[0][1]=new TH1D("NDS_G10_RTIA_500_LPF_35","NDS_G10_RTIA_500_LPF_35",nsample/2,0.,fmax/2.);
  hmod[0][2]=new TH1D("NDS_G10_RTIA_400_LPF_50","NDS_G10_RTIA_400_LPF_50",nsample/2,0.,fmax/2.);
  hmod[0][3]=new TH1D("NDS_G10_RTIA_400_LPF_35","NDS_G10_RTIA_400_LPF_35",nsample/2,0.,fmax/2.);
  hmod[0][4]=new TH1D("NDS_G10_RTIA_340_LPF_50","NDS_G10_RTIA_340_LPF_50",nsample/2,0.,fmax/2.);
  hmod[0][5]=new TH1D("NDS_G10_RTIA_340_LPF_35","NDS_G10_RTIA_340_LPF_35",nsample/2,0.,fmax/2.);
  hmod[1][0]=new TH1D("NDS_G1_RTIA_500_LPF_50","NDS_G1_RTIA_500_LPF_50",nsample/2,0.,fmax/2.);
  hmod[1][1]=new TH1D("NDS_G1_RTIA_500_LPF_35","NDS_G1_RTIA_500_LPF_35",nsample/2,0.,fmax/2.);
  hmod[1][2]=new TH1D("NDS_G1_RTIA_400_LPF_50","NDS_G1_RTIA_400_LPF_50",nsample/2,0.,fmax/2.);
  hmod[1][3]=new TH1D("NDS_G1_RTIA_400_LPF_35","NDS_G1_RTIA_400_LPF_35",nsample/2,0.,fmax/2.);
  hmod[1][4]=new TH1D("NDS_G1_RTIA_340_LPF_50","NDS_G1_RTIA_340_LPF_50",nsample/2,0.,fmax/2.);
  hmod[1][5]=new TH1D("NDS_G1_RTIA_340_LPF_35","NDS_G1_RTIA_340_LPF_35",nsample/2,0.,fmax/2.);
  TGraph *tg_pulse[2];
  tg_pulse[0]=new TGraph();
  tg_pulse[0]->SetLineColor(kRed);
  tg_pulse[0]->SetMarkerColor(kRed);
  tg_pulse[0]->SetMarkerSize(0.1);
  tg_pulse[0]->SetTitle("G10_R340_LPF35_pedestal_frame");
  tg_pulse[0]->SetName("G10_R340_LPF35_pedestal_frame");
  tg_pulse[1]=new TGraph();
  tg_pulse[1]->SetLineColor(kRed);
  tg_pulse[1]->SetMarkerColor(kRed);
  tg_pulse[1]->SetMarkerSize(0.1);
  tg_pulse[1]->SetTitle("G1_R340_LPF35_pedestal_frame");
  tg_pulse[1]->SetName("G1_R340_LPF35_pedestal_frame");

// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
  old_read_address=-1;

// Start DAQ :
  command = ((nsample/4+1)<<16)+CAPTURE_START;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();
  val=I2C_RW(hw, device_number, 1, 2,0, 3, debug); // Switch off temp sensor
  val=I2C_RW(hw, device_number, 4, 0x8000,1, 3, debug); // Switch off every thing 
  val=I2C_RW(hw, device_number, 5, 0x4000,1, 3, debug);
  val=I2C_RW(hw, device_number, 6, (ref_best<<4) | 0x0B, 0, 3, debug);
  for(int iRTIA=0; iRTIA<3; iRTIA++)
  {
    Int_t jRTIA=iRTIA;
    if(iRTIA==2)jRTIA=3;
    for(int iLPF=0; iLPF<2; iLPF++)
    {
      pmod[0]->Reset();
      pmod[1]->Reset();
      val=I2C_RW(hw, device_number, 3, (jRTIA<<14) | (ped_ref[1]<<8) | (ped_ref[0]<<2) | (iLPF<<1) | 1,1, 3, debug);
      usleep(100000);
      debug_draw=1;
      for(int ievt=0; ievt<nevent; ievt++)
      {
        ped[0]=0.,rms[0]=0.;
        ped[1]=0.,rms[1]=0.;
        error=get_event(hw,trigger_type,nsample/4,debug,debug_draw);
        debug_draw=0;

        Double_t loc_rms=0., loc_ped=0.;
        Int_t ngood=0;
        for(Int_t is=0; is<nsample; is++)
        {
          loc_ped+=fevent[4][is]*dv;
          loc_rms+=fevent[4][is]*dv*fevent[4][is]*dv;
          Double_t t=dt*is;
          if(iRTIA==1 && iLPF==1 && ievt==0) tg_pulse[0]->SetPoint(is,t,fevent[4][is]*dv);
          rex[is]=fevent[4][is]*dv;
          imx[is]=0.;
        }
        loc_ped/=nsample;
        loc_rms/=nsample;
        loc_rms=sqrt(loc_rms-loc_ped*loc_ped);
        for(Int_t is=0; is<nsample; is++)
        {
          if(fabs(fevent[4][is]*dv-loc_ped)<loc_rms*5.)
          {
            ped[0]+=fevent[4][is]*dv;
            rms[0]+=fevent[4][is]*dv*fevent[4][is]*dv;
            ngood++;
          }
        }
        ped[0]/=ngood;
        rms[0]/=ngood;
        rms[0]=rms[0]-ped[0]*ped[0];

        G10_noise[iRTIA][iLPF]+=rms[0];
        fft_f->SetPointsComplex(rex,imx);
        fft_f->Transform();
        fft_f->GetPointsComplex(rey,imy);
        for(Int_t is=0; is<nsample; is++)
        {
          Double_t f=df*(is+0.5);
          rey[is]/=nsample;
          imy[is]/=nsample;
          mod[is]=rey[is]*rey[is]+imy[is]*imy[is];
          pmod[0]->Fill(f,mod[is]/df);
        }

        loc_ped=0.; loc_rms=0.; ngood=0;
        for(Int_t is=0; is<nsample; is++)
        {
          loc_ped+=fevent[5][is]*dv;
          loc_rms+=fevent[5][is]*dv*fevent[5][is]*dv;
          Double_t t=dt*is;
          if(iRTIA==1 && iLPF==1 && ievt==0)tg_pulse[1]->SetPoint(is,t,fevent[5][is]*dv);
          rex[is]=fevent[5][is]*dv;
          imx[is]=0.;
        }
        loc_ped/=nsample;
        loc_rms/=nsample;
        loc_rms=sqrt(loc_rms-loc_ped*loc_ped);
        for(Int_t is=0; is<nsample; is++)
        {
          if(fabs(fevent[5][is]*dv-loc_ped)<loc_rms*5.)
          {
            ped[1]+=fevent[5][is]*dv;
            rms[1]+=fevent[5][is]*dv*fevent[5][is]*dv;
            ngood++;
          }
        }
        ped[1]/=ngood;
        rms[1]/=ngood;
        rms[1]=rms[1]-ped[1]*ped[1];
        G1_noise[iRTIA][iLPF]+=rms[1];
        fft_f->SetPointsComplex(rex,imx);
        fft_f->Transform();
        fft_f->GetPointsComplex(rey,imy);
        for(Int_t is=0; is<nsample; is++)
        {
          Double_t f=df*(is+0.5);
          rey[is]/=nsample;
          imy[is]/=nsample;
          mod[is]=rey[is]*rey[is]+imy[is]*imy[is];
          pmod[1]->Fill(f,mod[is]/df);
        }
        if(ievt==0)printf("G10 noise : %.0f uV, G1 noise : %d good, %.0f uV\n",sqrt(rms[0])*1000.,ngood,sqrt(rms[1])*1000.);
        if(wait==1)
        {
          system("stty raw");
          cdum=getchar();
          system("stty -raw");
          if(cdum=='q')exit(-1);
        }
      }
      G1_noise[iRTIA][iLPF]=sqrt(G1_noise[iRTIA][iLPF]/nevent)*1000.;
      G10_noise[iRTIA][iLPF]=sqrt(G10_noise[iRTIA][iLPF]/nevent)*1000.;
      hmod[0][iRTIA*2+iLPF]->SetBinContent(1,0.);
      hmod[1][iRTIA*2+iLPF]->SetBinContent(1,0.);
      for(Int_t is=1; is<nsample/2; is++)
      {
        Double_t mod=0.;
        mod=pmod[0]->GetBinContent(is+1);
        hmod[0][iRTIA*2+iLPF]->SetBinContent(is+1,sqrt(2.*mod));
        mod=pmod[1]->GetBinContent(is+1);
        hmod[1][iRTIA*2+iLPF]->SetBinContent(is+1,sqrt(2.*mod));
      }
    }
  }
// Stop DAQ
  command = ((nsample/4+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();

  sprintf(output_file,"data/CATIA_calib/CATIA_%5.5d_calib.root",CATIA_number);
  TFile *fd=new TFile(output_file,"recreate");

//===========================================================================================================
// 3 : Send internal TP triggers for linearity measurements : G10 and G1
// calibration trigger setting :
  trigger_type=1;
  //nevent=100;
  nevent=10;
  nsample=100*4;
  n_TP_step=60;
  TP_step=64;
  TP_level=0.;
  Double_t V_Iinj_slope[2], INL_min[2]={9999.,9999.}, INL_max[2]={-9999.,-9999.};
  TProfile *tp_inj1[3][n_TP_step];
  TH1D     *hp_inj1[3][n_TP_step];
  for(int iinj=0; iinj<n_TP_step; iinj++)
  {
    sprintf(hname,"tpulse_G10_DAC_%4.4d",TP_level+iinj*TP_step);
    tp_inj1[0][iinj]=new TProfile(hname,hname,nsample,0.,dt*nsample);
    sprintf(hname,"tpulse_G1_DAC_%4.4d",TP_level+iinj*TP_step);
    tp_inj1[1][iinj]=new TProfile(hname,hname,nsample,0.,dt*nsample);
    sprintf(hname,"hpulse_G10_DAC_%4.4d",TP_level+iinj*TP_step);
    hp_inj1[0][iinj]=new TH1D(hname,hname,nsample,0.,dt*nsample);
    sprintf(hname,"hpulse_G1_DAC_%4.4d",TP_level+iinj*TP_step);
    hp_inj1[1][iinj]=new TH1D(hname,hname,nsample,0.,dt*nsample);
  }
  sprintf(hname,"pulse_G10_DAC_Rcal");
  TProfile *tp_inj_cal=new TProfile(hname,hname,nsample,0.,dt*nsample);
  TGraph *tg_inj1[2], *tg_inj2[2], *tg_inj3[2];
  tg_inj1[0]=new TGraph();
  tg_inj1[0]->SetTitle("Inj_vs_iDAC_G10");
  tg_inj1[0]->SetName("Inj_vs_iDAC_G10");
  tg_inj1[0]->SetMarkerStyle(20);
  tg_inj1[0]->SetMarkerSize(1.0);
  tg_inj1[0]->SetMarkerColor(kRed);
  tg_inj1[0]->SetLineColor(kRed);
  tg_inj1[0]->SetLineWidth(2);
  tg_inj1[1]=new TGraph();
  tg_inj1[1]->SetTitle("Inj_vs_iDAC_G1");
  tg_inj1[1]->SetName("Inj_vs_iDAC_G1");
  tg_inj1[1]->SetMarkerStyle(20);
  tg_inj1[1]->SetMarkerSize(1.0);
  tg_inj1[1]->SetMarkerColor(kBlue);
  tg_inj1[1]->SetLineColor(kBlue);
  tg_inj1[1]->SetLineWidth(2);
  tg_inj2[0]=new TGraph();
  tg_inj2[0]->SetTitle("Inj_vs_vDAC_G10");
  tg_inj2[0]->SetName("Inj_vs_vDAC_G10");
  tg_inj2[0]->SetMarkerStyle(20);
  tg_inj2[0]->SetMarkerSize(1.0);
  tg_inj2[0]->SetMarkerColor(kRed);
  tg_inj2[0]->SetLineColor(kRed);
  tg_inj2[0]->SetLineWidth(2);
  tg_inj2[1]=new TGraph();
  tg_inj2[1]->SetTitle("Inj_vs_vDAC_G1");
  tg_inj2[1]->SetName("Inj_vs_vDAC_G1");
  tg_inj2[1]->SetMarkerStyle(20);
  tg_inj2[1]->SetMarkerSize(1.0);
  tg_inj2[1]->SetMarkerColor(kBlue);
  tg_inj2[1]->SetLineColor(kBlue);
  tg_inj2[1]->SetLineWidth(2);
  tg_inj3[0]=new TGraph();
  tg_inj3[0]->SetTitle("INL_vs_amplitude_G10");
  tg_inj3[0]->SetName("INL_vs_amplitude_G10");
  tg_inj3[0]->SetMarkerStyle(20);
  tg_inj3[0]->SetMarkerSize(1.0);
  tg_inj3[0]->SetMarkerColor(kRed);
  tg_inj3[0]->SetLineColor(kRed);
  tg_inj3[0]->SetLineWidth(2);
  tg_inj3[0]->SetMaximum(0.003);
  tg_inj3[0]->SetMinimum(-.003);
  tg_inj3[1]=new TGraph();
  tg_inj3[1]->SetTitle("INL_vs_amplitude_G1");
  tg_inj3[1]->SetName("INL_vs_amplitude_G1");
  tg_inj3[1]->SetMarkerStyle(20);
  tg_inj3[1]->SetMarkerSize(1.0);
  tg_inj3[1]->SetMarkerColor(kBlue);
  tg_inj3[1]->SetLineColor(kBlue);
  tg_inj3[1]->SetLineWidth(2);
  tg_inj3[1]->SetMaximum(0.003);
  tg_inj3[1]->SetMinimum(-.003);

  command=(TP_delay<<16) | (TP_width&0xffff);
  printf("Calibration trigger with %d clocks width and %d clocks delay : %x\n",TP_width,TP_delay,command);
  hw.getNode("CALIB_CTRL").write(command);
// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
  old_read_address=-1;

// Start DAQ :
  command = ((nsample/4+1)<<16)+CAPTURE_START;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();

  val=I2C_RW(hw, device_number, 1, 2,0, 3, debug); // Switch off temp sensor
  val=I2C_RW(hw, device_number, 3, (3<<14) | (ped_ref[1]<<8) | (ped_ref[0]<<2) | 3,1, 3, debug);
  //printf("Reg3 register value read : 0x%x, %d\n",val,val&0xFFFF);
  val=I2C_RW(hw, device_number, 5, 0x4000,1, 3, debug); 
  //printf("Reg5 register value read : 0x%x, %d\n",val,val&0xFFFF);
  usleep(100000);

// First round to compute the correction to apply to G10 TP due to an un-understood bug in CATIA-V2.x
  Double_t y_low=-9999., y_up=9999., ped_cal=0.;;
  TP_level=450;
  val=I2C_RW(hw, device_number, 6, (ref_best<<4) | 0x0b, 0, 3, debug); // Rconv 2471 or 272, TIA_dummy ON, TP ON, Vreg ON
  if(wait==1)
  {
    system("stty raw");
    cdum=getchar();
    system("stty -raw");
    if(cdum=='q')exit(-1);
  }
  while(y_low<y_up-0.5)
  {
    TP_level++;
    val=I2C_RW(hw, device_number, 4, 0x9000|TP_level,1, 3, debug); // TP with DAC1
    usleep(100);
    tp_inj_cal->Reset();
    for(Int_t ievt=0; ievt<10; ievt++)
    {
      error=get_event(hw,trigger_type,nsample/4,debug,debug_draw);
      debug_draw=0;
      if(timestamp<0) printf("DAQ error on event %d : %d\n",ievt, timestamp);
      for(int is=0; is<nsample; is++)
      {
        Double_t y=fevent[4][is]*dv;
        if(y<1.e-6)y=1.e-6;
        tp_inj_cal->Fill(dt*(is+0.5),y);
      }
    }
    tp_inj_cal->Fit("pol0","wq","",1475.e-9,1500.e-9);
    f1=tp_inj_cal->GetFunction("pol0");
    y_low=f1->GetParameter(0);
    tp_inj_cal->Fit("pol0","wq","",1600.e-9,1625.e-9);
    f1=tp_inj_cal->GetFunction("pol0");
    y_up=f1->GetParameter(0);
    tp_inj_cal->Fit("pol0","wq","",0.,100.e-9);
    f1=tp_inj_cal->GetFunction("pol0");
    ped_cal=f1->GetParameter(0);
  }
  printf("level %d : low %.2f, up %.2f\n",TP_level,y_low,y_up);
  
  for(int igain=0; igain<2; igain++)
  {
    TP_level=0;
    //val=I2C_RW(hw, device_number, 6, 0x0b | ((1-igain)<<2),0, 3, debug); // Rconv 2471 or 272, TIA_dummy ON, TP ON, Vreg O
    val=I2C_RW(hw, device_number, 6, (ref_best<<4) | 0x08 | (igain<<2) | 0x03,0, 3, debug); // Rconv 2471 or 272, TIA_dummy ON, TP ON, Vreg ON
    //printf("Reg6 register value read : 0x%x, %d\n",val,val&0xFFFF);
    Double_t fit_min=-1, fit_max=-1;
    Int_t loc_step=0;
    for(int istep=0; istep<n_TP_step; istep++)
    {
// Program DAC for this step
      val=I2C_RW(hw, device_number, 4, 0x9000|TP_level,1, 3, debug); // TP with DAC1
      if((val&0xFFF)!=TP_level) printf("Error on DAC setting : 0x%4.4x written, 0x%4.4x read back\n",TP_level,val&0xFFF);
      //printf("DAC register value read : 0x%x, %d\n",val,val&0xFFFF);
// Wait for Vdac to stabilize :
      if(istep==0)
        usleep(1000);
      else
        usleep(100);

      debug_draw=(istep%4);
      for(Int_t ievt=0; ievt<nevent; ievt++)
      {
        error=get_event(hw,trigger_type,nsample/4,debug,debug_draw);
        debug_draw=0;
        if(timestamp<0) printf("DAQ error on event %d : %d\n",ievt, timestamp);

        for(int is=0; is<nsample; is++)
        {
          Double_t y=fevent[4+igain][is]*dv;
          if(y<1.e-6)y=1.e-6;
          tp_inj1[igain][istep]->Fill(dt*(is+0.5),y);
        }
        Double_t y_raw;
      }
      for(int is=0; is<nsample; is++)
      {
        Double_t y_raw, ey_raw, cal_cor;
        y_raw=tp_inj1[igain][istep]->GetBinContent(is+1);
        ey_raw=tp_inj1[igain][istep]->GetBinError(is+1);
        cal_cor=tp_inj_cal->GetBinContent(is+1);
        if(CATIA_number>=20000 && igain==0)
          hp_inj1[igain][istep]->SetBinContent(is+1,y_raw-cal_cor+ped_cal);
        else
          hp_inj1[igain][istep]->SetBinContent(is+1,y_raw);
        hp_inj1[igain][istep]->SetBinError(is+1,ey_raw);
      }

      if(wait==1)
      {
        system("stty raw");
        cdum=getchar();
        system("stty -raw");
        if(cdum=='q')exit(-1);
      }
      hp_inj1[igain][istep]->Fit("pol0","wq","",0.e-9,400.e-9);
      f1=hp_inj1[igain][istep]->GetFunction("pol0");
      mean[igain]=f1->GetParameter(0);
      hp_inj1[igain][istep]->Fit("pol1","wq","",800.e-9,1300.e-9);
      f1=hp_inj1[igain][istep]->GetFunction("pol1");
      Double_t val0=f1->GetParameter(0)+f1->GetParameter(1)*650.e-9-mean[igain];
      
      if(val0<2.){TP_level+=TP_step; continue;}
      if(val0>1150. || (igain==1 && istep>55))
      {
        if(igain==1)cdebug->Write();
        break;
      }

      tg_inj1[igain]->SetPoint(loc_step,TP_level,val0);
      tg_inj2[igain]->SetPoint(loc_step,fDAC[0]->Eval(TP_level)*1000.,val0);
      if(fit_min<0 && val0>0.)fit_min=TP_level-TP_step/2.;
      if(val0<1190.)fit_max=TP_level+TP_step/2.;
      loc_step++;
      TP_level+=TP_step;
    }
    tg_inj1[igain]->Fit("pol1","WQ","",fit_min,fit_max);
    tg_inj2[igain]->Fit("pol1","WQ","",fDAC[0]->Eval(fit_min)*1000.,fDAC[0]->Eval(fit_max)*1000.);
    f1=tg_inj2[igain]->GetFunction("pol1");
    if(f1!=NULL)V_Iinj_slope[igain]=f1->GetParameter(1);
    printf("Gain %.0f : V_Iinj slope = %.3f\n",val_gain[igain],V_Iinj_slope[igain]);
    f1=tg_inj1[igain]->GetFunction("pol1");
// Compute residuals :
    Int_t n=tg_inj1[igain]->GetN();
    for(Int_t is=0; is<n; is++)
    {
      Double_t x,y;
      tg_inj1[igain]->GetPoint(is,x,y);
      if(f1!=NULL)
      {
        tg_inj3[igain]->SetPoint(is,y,(y-f1->Eval(x))/1200.);
        if(y>0. && y<1200. && (y-f1->Eval(x))/1200.>INL_max[igain])INL_max[igain]=(y-f1->Eval(x))/1200.;
        if(y>0. && y<1200. && (y-f1->Eval(x))/1200.<INL_min[igain])INL_min[igain]=(y-f1->Eval(x))/1200.;
      }
    }
    printf("Gain %.0f INL min %.2f max %.2f\n",val_gain[igain],INL_min[igain]*1000., INL_max[igain]*1000.);
  }
// Stop DAQ
  command = ((nsample/4+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();

/*
//===========================================================================================================
// 3.1 : Send internal TP triggers for afterpulse recovery study in G10
// Send TP in G1 and look at G10 output
// calibration trigger setting :
  trigger_type=1;
  //nevent=100;
  nevent=1;
  nsample=26622*4;
  n_TP_step=60;
  TP_step=64;
  TP_level=0.;

  for(int iinj=0; iinj<n_TP_step; iinj++)
  {
    sprintf(hname,"recovery_G10_DAC_G1_%4.4d",TP_level+iinj*TP_step);
    tp_inj1[2][iinj]=new TProfile(hname,hname,nsample,0.,dt*nsample);
  }

  command=(TP_delay<<16) | (TP_width_recovery&0xffff);
  printf("Calibration trigger with %d clocks width and %d clocks delay : %x\n",TP_width_recovery,TP_delay,command);
  hw.getNode("CALIB_CTRL").write(command);
// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
  old_read_address=-1;

// Start DAQ :
  command = ((nsample/4+1)<<16)+CAPTURE_START;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();

  val=I2C_RW(hw, device_number, 1, 2,0, 3, debug); // Switch off temp sensor
  val=I2C_RW(hw, device_number, 3, (3<<14) | (0<<8) | (0<<2) | (1<<1) | 1,1, 3, debug);
  //printf("Reg3 register value read : 0x%x, %d\n",val,val&0xFFFF);
  val=I2C_RW(hw, device_number, 5, 0x4000,1, 3, debug); 
  //printf("Reg5 register value read : 0x%x, %d\n",val,val&0xFFFF);
  usleep(10000);
  for(int igain=1; igain<2; igain++)
  {
    TP_level=0;
    //val=I2C_RW(hw, device_number, 6, 0x0b | ((1-igain)<<2),0, 3, debug); // Rconv 2471 or 272, TIA_dummy ON, TP ON, Vreg O
    val=I2C_RW(hw, device_number, 6, (ref_best<<4) | 0x0b | (igain<<2),0, 3, debug); // Rconv 2471 or 272, TIA_dummy ON, TP ON, Vreg O
    //printf("Reg6 register value read : 0x%x, %d\n",val,val&0xFFFF);
    for(int istep=0; istep<n_TP_step; istep++)
    {
// Program DAC for this step
      val=I2C_RW(hw, device_number, 4, 0x1000|TP_level,1, 3, debug); // TP with DAC1
      if((val&0xFFF)!=TP_level) printf("Error on DAC setting : 0x%4.4x written, 0x%4.4x read back\n",TP_level,val&0xFFF);
      printf("DAC register value read : 0x%x, %d\n",val,val&0xFFFF);
// Wait for Vdac to stabilize :
      if(istep==0)
        usleep(10000);
      else
        usleep(1000);

      debug_draw=1;
      Int_t nped_before=0, nped_after=0;
      Double_t level_before=0., level_after=0.;
      for(Int_t ievt=0; ievt<nevent; ievt++)
      {
        error=get_event(hw,trigger_type,nsample/4,debug,debug_draw);
        debug_draw=0;
        if(timestamp<0) printf("DAQ error on event %d : %d\n",ievt, timestamp);

        for(int is=0; is<nsample; is++)
        {
          Double_t t=dt*(is+0.5);
          Double_t y=fevent[4][is]*dv;
          tp_inj1[2][istep]->Fill(dt*(is+0.5),y);
          if(t<400.e-9)
          {
            level_before+=y;
            nped_before++;
          }
          if(t>900.e-9 && t<1200.e-9)
          {
            level_after+=y;
            nped_after++;
          }
        }
      }
      level_before/=nped_before;
      level_after /=nped_after;
      printf("TP %d, before : %e, after %e\n",TP_level,level_before,level_after);
      TP_level+=TP_step;
    }
  }
// Stop DAQ
  command = ((nsample/4+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();
*/
//===========================================================================================================
// 3.2 : Send AWG pulses to saturate G10 and look at recovery
  if((use_AWG&1)==1)
  {
    fead_ctrl=  ADC_CALIB_MODE        *0 |
                CALIB_PULSE_ENABLED   *0 |
                CALIB_MUX_ENABLED     *1 |
                SELF_TRIGGER_MASK     *3 |
                SELF_TRIGGER_THRESHOLD*100 |
                SELF_TRIGGER          *3 |
                SOFT_TRIGGER          *0 |
                TRIGGER_MODE          *1 | // Always DAQ on trigger
                SELF_TRIGGER_MODE     *0 | // trig on level
                RESET                 *0;
    hw.getNode("FEAD_CTRL").write(fead_ctrl);
    hw.dispatch();
    trigger_type=2;
    nevent=100;
    nsample=50*4;
    Int_t n_AWG_step=34;
    Double_t level_before[34], level_after[34];
    TP_step=100.;
    TP_level=0.;

    TProfile *tp_awg[n_TP_step];
    for(int istep=0; istep<n_AWG_step; istep++)
    {
      TP_level+=TP_step;
      sprintf(hname,"pulse_G10_AWG_G1_%4.4d",TP_level);
      tp_awg[istep]=new TProfile(hname,hname,nsample,0.,dt*nsample);
    }

    val=I2C_RW(hw, device_number, 1, 2,0, 3, debug); // Switch off temp sensor
    val=I2C_RW(hw, device_number, 3, (3<<14) | (ped_ref[1]<<8) | (ped_ref[0]<<2) | 3,1, 3, debug);
    //printf("Reg3 register value read : 0x%x, %d\n",val,val&0xFFFF);
    val=I2C_RW(hw, device_number, 5, 0x4000,1, 3, debug); 
    //printf("Reg5 register value read : 0x%x, %d\n",val,val&0xFFFF);

    TP_level=0;
    val=I2C_RW(hw, device_number, 6, (ref_best<<4) | 0xF,0, 3, debug);
    val=I2C_RW(hw, device_number, 4, 0x8000,1, 3, debug); // DAC1 and DAC2 off
    for(int istep=0; istep<n_AWG_step; istep++)
    {
      TP_level+=TP_step;
      printf("Please switch ON the pulse generator, set width to 500 ns and level to %d mV\n",TP_level);
      system("stty raw");
      cdum=getchar();
      system("stty -raw");
      if(cdum=='q')exit(-1);

// Reset the reading base address :
      hw.getNode("CAP_ADDRESS").write(0);
      old_read_address=-1;

// Start DAQ :
      command = ((nsample/4+1)<<16)+CAPTURE_START;
      hw.getNode("CAP_CTRL").write(command);
      hw.dispatch();

      debug_draw=1;
      Int_t nped_before=0, nped_after=0;
      level_before[istep]=0.;
      level_after[istep]=0.;
      for(Int_t ievt=0; ievt<nevent; ievt++)
      {
        error=get_event(hw,trigger_type,nsample/4,debug,debug_draw);
        debug_draw=0;
        if(timestamp<0) printf("DAQ error on event %d : %d\n",ievt, timestamp);
        for(int is=0; is<nsample; is++)
        {
          Double_t t=dt*(is+0.5);
          Double_t y=fevent[4][is]*dv;
          tp_awg[istep]->Fill(dt*(is+0.5),y);
          if(t<300.e-9)
          {
            level_before[istep]+=y;
            nped_before++;
          }
          if(t>500.e-9 && t<600.e-9)
          {
            level_after[istep]+=y;
            nped_after++;
          }
        }
      }
      level_before[istep]/=nped_before;
      level_after[istep] /=nped_after;
      printf("TP %d, before : %d %e, after %d %e\n",TP_level, nped_before, level_before[istep], nped_after, level_after[istep]);
// Stop DAQ
      command = ((nsample/4+1)<<16)+CAPTURE_STOP;
      hw.getNode("CAP_CTRL").write(command);
      hw.dispatch();
    }
    for(int istep=0; istep<n_AWG_step; istep++)
    {
      printf("%e, ",level_before[istep]);
    }
    printf("\n");
    for(int istep=0; istep<n_AWG_step; istep++)
    {
      printf("%e, ",level_after[istep]);
    }
    printf("\n");
    for(int istep=0; istep<n_AWG_step; istep++)
    {
      tp_awg[istep]->Write();
    }
  }

//===========================================================================================================
// 4 : Send G10 and G1 calibration pulses with R=500 and R=400
// 4.1 : Using FPGA LVCMOS pulse :
// Enabe CALIB_PULSE  :
  TGraph *tg_calib[2];
  tg_calib[0]=new TGraph();
  tg_calib[0]->SetLineColor(kRed);
  tg_calib[0]->SetMarkerColor(kRed);
  tg_calib[0]->SetMarkerSize(0.1);
  tg_calib[0]->SetTitle("G10_R340_LPF35_calib_pulse");
  tg_calib[0]->SetName("G10_R340_LPF35_calib_pulse");
  tg_calib[1]=new TGraph();
  tg_calib[1]->SetLineColor(kRed);
  tg_calib[1]->SetMarkerColor(kRed);
  tg_calib[1]->SetMarkerSize(0.1);
  tg_calib[1]->SetTitle("G1_R340_LPF35_calib_pulse");
  tg_calib[1]->SetName("G1_R340_LPF35_calib_pulse");
  Double_t Rcalib[2]={10000.,10000.}; // Conversion resistor on PCB to compute CATIA gain
  Double_t Rconv[2];
  Double_t CMOS_amp=1.20;
  Double_t CATIA_gain[2][3], TIA_gain[3], gain_stage;
// Enable G10 and G1 calibration pulse from FPGA :
  fead_ctrl=  ADC_CALIB_MODE        *0 |
              CALIB_PULSE_ENABLED   *1 |
              CALIB_MUX_ENABLED     *1 |
              SELF_TRIGGER_MASK     *0 |
              SELF_TRIGGER_THRESHOLD*0 |
              SELF_TRIGGER          *0 |
              SOFT_TRIGGER          *1 |
              TRIGGER_MODE          *1 | // Always DAQ on trigger
              RESET                 *0;
  hw.getNode("FEAD_CTRL").write(fead_ctrl);
  hw.dispatch();
  //reg=hw.getNode("FEAD_CTRL").read();
  //hw.dispatch();
  //printf("Calib_pulse_enable : 0x%8.8x -> %d\n",reg.value(),(reg.value()>>27)&1);
  nsample=100*4;
  nevent=100;
  //nevent=1000;
  //command=(TP_delay<<16) | (TP_width&0xffff);
  command=(0<<16) | (TP_width&0xffff);
  printf("Calibration trigger with %d clocks width and %d clocks delay : %x\n",TP_width,TP_delay,command);
  hw.getNode("CALIB_CTRL").write(command);

  val=I2C_RW(hw, device_number, 6, (ref_best<<4) | 0xb,0, 3, debug); // TP stuff off
  val=I2C_RW(hw, device_number, 4, 0x8000,1, 3, debug); // DAC1 and DAC2 off
  TProfile *tp_calib[2][3];
  tp_calib[0][0]=new TProfile("G10_R500_calib_pulse_response","G10_R500_calib_pulse_response",nsample,0.,nsample*dt);
  tp_calib[0][1]=new TProfile("G10_R400_calib_pulse_response","G10_R400_calib_pulse_response",nsample,0.,nsample*dt);
  tp_calib[0][2]=new TProfile("G10_R340_calib_pulse_response","G10_R340_calib_pulse_response",nsample,0.,nsample*dt);
  tp_calib[1][0]=new TProfile("G1_R500_calib_pulse_response","G1_R500_calib_pulse_response",nsample,0.,nsample*dt);
  tp_calib[1][1]=new TProfile("G1_R400_calib_pulse_response","G1_R400_calib_pulse_response",nsample,0.,nsample*dt);
  tp_calib[1][2]=new TProfile("G1_R340_calib_pulse_response","G1_R340_calib_pulse_response",nsample,0.,nsample*dt);
  TF1 *fcalib=new TF1("fcalib","[0]+[1]*(1.+tanh((x-[2])/[3]))/2.",0.,2.e3);
  fcalib->SetParameter(0,0.);
  fcalib->SetParameter(1,2000.);
  fcalib->SetParameter(2,0.35e-6);
  fcalib->SetParameter(3,9.e-9);
  TH1D *hcalib_t0=new TH1D("t0_calib","t0_calib",1000,0.80e-6,1.00e-6);
  TH1D *hcalib_rt=new TH1D("RT_calib","RT_calib",1000,7.e-9,12.e-9);

  trigger_type=3;
  for(int iRTIA=0; iRTIA<3; iRTIA++)
  {
    Int_t jRTIA=iRTIA;
    if(iRTIA==2)jRTIA=3;
    val=I2C_RW(hw, device_number, 3, (jRTIA<<14) | (ped_ref[1]<<8) | (ped_ref[0]<<2) | (1<<1) | 1,1, 3, debug);
    usleep(10000);

// Reset the reading base address :
    hw.getNode("CAP_ADDRESS").write(0);
    old_read_address=-1;
// Start DAQ :
    command = ((nsample/4+1)<<16)+CAPTURE_START;
    hw.getNode("CAP_CTRL").write(command);
    hw.dispatch();

    debug_draw=1;
    for(int ievt=0; ievt<nevent; ievt++)
    {
      error=get_event(hw,trigger_type,nsample/4,debug,debug_draw);
      debug_draw=0;
      for(int is=0; is<nsample; is++)
      {
        Double_t t=dt*(is+0.5);
        tp_calib[0][iRTIA]->Fill(t,fevent[4][is]*(dv/1000.));
        tp_calib[1][iRTIA]->Fill(t,fevent[5][is]*(dv/1000.));
        if(iRTIA==2)tg_calib[0]->SetPoint(is,dt*is,fevent[4][is]);

      }
      if(iRTIA==2)
      {
        //tg_calib[0]->Fit(fcalib,"WQ","",0.8e-6,1.00e-6);
        tg_calib[0]->Fit(fcalib,"WQ","",0.3e-6,0.40e-6);
        hcalib_t0->Fill(fcalib->GetParameter(2));
        hcalib_rt->Fill(fcalib->GetParameter(3));
      }
    }
    tp_calib[0][iRTIA]->Fit("pol0","WQ","",0.,.8e-6);
    //tp_calib[0][iRTIA]->Fit("pol0","WQ","",0.,.2e-6);
    f1=tp_calib[0][iRTIA]->GetFunction("pol0");
    mean[0]=f1->GetParameter(0);
    tp_calib[0][iRTIA]->Fit("pol1","WQ","",1.1e-6,1.8e-6);
    //tp_calib[0][iRTIA]->Fit("pol1","WQ","",0.6e-6,1.1e-6);
    f1=tp_calib[0][iRTIA]->GetFunction("pol1");
    CATIA_gain[0][iRTIA]=(f1->Eval(0.9e-6)-mean[0])/(CMOS_amp/Rcalib[0]);
    //CATIA_gain[0][iRTIA]=(f1->Eval(0.35e-6)-mean[0])/(CMOS_amp/Rcalib[0]);
    printf("Gain %.0f, R_TIA %.0f, Total gain : %e V/A\n",val_gain[0],RTIA[iRTIA],CATIA_gain[0][iRTIA]);

    tp_calib[1][iRTIA]->Fit("pol0","WQ","",0.,.8e-6);
    //tp_calib[1][iRTIA]->Fit("pol0","WQ","",0.,.2e-6);
    f1=tp_calib[1][iRTIA]->GetFunction("pol0");
    mean[1]=f1->GetParameter(0);
    tp_calib[1][iRTIA]->Fit("pol1","WQ","",1.1e-6,1.8e-6);
    //tp_calib[1][iRTIA]->Fit("pol1","WQ","",0.6e-6,1.1e-6);
    f1=tp_calib[1][iRTIA]->GetFunction("pol1");
    CATIA_gain[1][iRTIA]=(f1->Eval(0.9e-6)-mean[1])/(CMOS_amp/Rcalib[1]);
    //CATIA_gain[1][iRTIA]=(f1->Eval(0.35e-6)-mean[1])/(CMOS_amp/Rcalib[1]);
    printf("Gain %.0f, R_TIA %.0f, Total gain : %e V/A\n",val_gain[1],RTIA[iRTIA],CATIA_gain[1][iRTIA]);

// Stop DAQ :
    command = ((nsample/4+1)<<16)+CAPTURE_STOP;
    hw.getNode("CAP_CTRL").write(command);
    if(wait==1)
    {
      system("stty raw");
      cdum=getchar();
      system("stty -raw");
      if(cdum=='q')exit(-1);
    }
  }
  // (G10-R500/G1-R500 + G10-R400/G1-R400 +G10-R340/G1-R340)/3
  gain_stage=(CATIA_gain[0][0]/CATIA_gain[1][0]+CATIA_gain[0][1]/CATIA_gain[1][1]+CATIA_gain[0][2]/CATIA_gain[1][2])/3.;
  TIA_gain[0]=CATIA_gain[1][0]; // G1-R500
  TIA_gain[1]=CATIA_gain[1][1]; // G1-R400
  TIA_gain[2]=CATIA_gain[1][2]; // G1-R340
  printf("G10 with R500 : %.2f, with R400 : %.2f, with R340 : %2f, mean : %.2f\n",
         CATIA_gain[0][0]/CATIA_gain[1][0],CATIA_gain[0][1]/CATIA_gain[1][1],CATIA_gain[0][2]/CATIA_gain[1][2],gain_stage);
  printf("R500 = %.0f Ohm\n",TIA_gain[0]);
  printf("R400 = %.0f Ohm\n",TIA_gain[1]);
  printf("R340 = %.0f Ohm\n",TIA_gain[2]);
  Rconv[0]=CATIA_gain[0][1]/V_Iinj_slope[0]; // Iinj conversion resistor for G1
  Rconv[1]=CATIA_gain[1][1]/V_Iinj_slope[1]; // Iinj conversion resistor for G10
  printf("Rconv Iinj G10 (2471 Ohm) = %.0f Ohm\n",Rconv[0]);
  printf("Rconv Iinj G1  (272 Ohm)  = %.0f Ohm\n",Rconv[1]);

// 4.2 : Using AWG pulse :
  TGraph *tg_calib_AWG[2];
  tg_calib_AWG[0]=new TGraph();
  tg_calib_AWG[0]->SetLineColor(kRed);
  tg_calib_AWG[0]->SetMarkerColor(kRed);
  tg_calib_AWG[0]->SetMarkerSize(0.1);
  tg_calib_AWG[0]->SetTitle("G10_R340_LPF35_AWG_calib_pulse");
  tg_calib_AWG[0]->SetName("G10_R340_LPF35_AWG_calib_pulse");
  tg_calib_AWG[1]=new TGraph();
  tg_calib_AWG[1]->SetLineColor(kRed);
  tg_calib_AWG[1]->SetMarkerColor(kRed);
  tg_calib_AWG[1]->SetMarkerSize(0.1);
  tg_calib_AWG[1]->SetTitle("G1_R340_LPF35_AWG_calib_pulse");
  tg_calib_AWG[1]->SetName("G1_R340_LPF35_AWG_calib_pulse");
  Double_t Rcalib_AWG[2]={10000.,10000.}; // Conversion resistor on PCB to compute CATIA gain
  Double_t Rconv_AWG[2];
  Double_t CATIA_gain_AWG[2][3], TIA_gain_AWG[3], gain_stage_AWG;
  TProfile *tp_calib_AWG[2][3];
  tp_calib_AWG[0][0]=new TProfile("G10_R500_AWG_calib_pulse_response","G10_R500_AWG_calib_pulse_response",nsample,0.,nsample*dt);
  tp_calib_AWG[0][1]=new TProfile("G10_R400_AWG_calib_pulse_response","G10_R400_AWG_calib_pulse_response",nsample,0.,nsample*dt);
  tp_calib_AWG[0][2]=new TProfile("G10_R340_AWG_calib_pulse_response","G10_R340_AWG_calib_pulse_response",nsample,0.,nsample*dt);
  tp_calib_AWG[1][0]=new TProfile("G1_R500_AWG_calib_pulse_response","G1_R500_AWG_calib_pulse_response",nsample,0.,nsample*dt);
  tp_calib_AWG[1][1]=new TProfile("G1_R400_AWG_calib_pulse_response","G1_R400_AWG_calib_pulse_response",nsample,0.,nsample*dt);
  tp_calib_AWG[1][2]=new TProfile("G1_R340_AWG_calib_pulse_response","G1_R340_AWG_calib_pulse_response",nsample,0.,nsample*dt);
  TH1D *hcalib_AWG_t0=new TH1D("t0_AWG_calib","t0_AWG_calib",1000,0.80e-6,1.00e-6);
  TH1D *hcalib_AWG_rt=new TH1D("RT_AWG_calib","RT_AWG_calib",1000,7.e-9,12.e-9);

  if((use_AWG&2)==2)
  {
// Enable G10 and G1 calibration pulse from FPGA :
    fead_ctrl=  ADC_CALIB_MODE        *0 |
                CALIB_PULSE_ENABLED   *0 |
                CALIB_MUX_ENABLED     *1 |
                SELF_TRIGGER_MASK     *0xf |
                SELF_TRIGGER_THRESHOLD*1000 |
                SELF_TRIGGER          *1 |
                SOFT_TRIGGER          *0 |
                TRIGGER_MODE          *1 | // Always DAQ on trigger
                SELF_TRIGGER_MODE     *0 | // trig on Delta sample
                RESET                 *0;
    hw.getNode("FEAD_CTRL").write(fead_ctrl);
    hw.dispatch();
    //reg=hw.getNode("FEAD_CTRL").read();
    //hw.dispatch();
    //printf("Calib_pulse_enable : 0x%8.8x -> %d\n",reg.value(),(reg.value()>>27)&1);
    nsample=100*4;
    nevent=100;

    printf("Please switch ON the pulse generator with 1.2V pulse, 945 ns width\n");
    system("stty raw");
    cdum=getchar();
    system("stty -raw");
    if(cdum=='q')exit(-1);

    val=I2C_RW(hw, device_number, 6, (ref_best<<4) | 0xb,0, 3, debug); // TP stuff off
    val=I2C_RW(hw, device_number, 4, 0x8000,1, 3, debug); // DAC1 and DAC2 off
    fcalib->SetParameter(0,0.);
    fcalib->SetParameter(1,2000.);
    fcalib->SetParameter(2,0.35e-6);
    fcalib->SetParameter(3,9.e-9);

    trigger_type=2;
    for(int iRTIA=0; iRTIA<3; iRTIA++)
    {
      Int_t jRTIA=iRTIA;
      if(iRTIA==2)jRTIA=3;
      val=I2C_RW(hw, device_number, 3, (jRTIA<<14) | (ped_ref[1]<<8) | (ped_ref[0]<<2) | (1<<1) | 1,1, 3, debug);
      usleep(10000);

// Reset the reading base address :
      hw.getNode("CAP_ADDRESS").write(0);
      old_read_address=-1;
// Start DAQ :
      command = ((nsample/4+1)<<16)+CAPTURE_START;
      hw.getNode("CAP_CTRL").write(command);
      hw.dispatch();

      debug_draw=1;
      for(int ievt=0; ievt<nevent; ievt++)
      {
        error=get_event(hw,trigger_type,nsample/4,debug,debug_draw);
        debug_draw=0;
        for(int is=0; is<nsample; is++)
        {
          Double_t t=dt*(is+0.5);
          tp_calib_AWG[0][iRTIA]->Fill(t,fevent[4][is]*(dv/1000.));
          tp_calib_AWG[1][iRTIA]->Fill(t,fevent[5][is]*(dv/1000.));
          if(iRTIA==2)tg_calib_AWG[0]->SetPoint(is,dt*is,fevent[4][is]);
        }
        if(iRTIA==2)
        {
          //tg_calib_AWG[0]->Fit(fcalib,"WQ","",0.8e-6,1.00e-6);
          tg_calib_AWG[0]->Fit(fcalib,"WQ","",0.3e-6,0.40e-6);
          hcalib_AWG_t0->Fill(fcalib->GetParameter(2));
          hcalib_AWG_rt->Fill(fcalib->GetParameter(3));
        }
      }
      tp_calib_AWG[0][iRTIA]->Fit("pol0","WQ","",0.,.8e-6);
      //tp_calib_AWG[0][iRTIA]->Fit("pol0","WQ","",0.,.2e-6);
      f1=tp_calib_AWG[0][iRTIA]->GetFunction("pol0");
      mean[0]=f1->GetParameter(0);
      tp_calib_AWG[0][iRTIA]->Fit("pol1","WQ","",1.1e-6,1.8e-6);
      //tp_calib_AWG[0][iRTIA]->Fit("pol1","WQ","",0.6e-6,1.1e-6);
      f1=tp_calib_AWG[0][iRTIA]->GetFunction("pol1");
      CATIA_gain_AWG[0][iRTIA]=(f1->Eval(0.9e-6)-mean[0])/(1.2/Rcalib_AWG[0]);
      //CATIA_gain_AWG[0][iRTIA]=(f1->Eval(0.35e-6)-mean[0])/(1.2/Rcalib_AWG[0]);
      printf("AWG : Gain %.0f, R_TIA %.0f, Total gain : %e V/A\n",val_gain[0],RTIA[iRTIA],CATIA_gain_AWG[0][iRTIA]);

      tp_calib_AWG[1][iRTIA]->Fit("pol0","WQ","",0.,.8e-6);
      //tp_calib_AWG[1][iRTIA]->Fit("pol0","WQ","",0.,.2e-6);
      f1=tp_calib_AWG[1][iRTIA]->GetFunction("pol0");
      mean[1]=f1->GetParameter(0);
      tp_calib_AWG[1][iRTIA]->Fit("pol1","WQ","",1.1e-6,1.8e-6);
      //tp_calib_AWG[1][iRTIA]->Fit("pol1","WQ","",0.6e-6,1.1e-6);
      f1=tp_calib_AWG[1][iRTIA]->GetFunction("pol1");
      CATIA_gain_AWG[1][iRTIA]=(f1->Eval(0.9e-6)-mean[1])/(1.2/Rcalib_AWG[1]);
      //CATIA_gain_AWG[1][iRTIA]=(f1->Eval(0.35e-6)-mean[1])/(1.2/Rcalib_AWG[1]);
      printf("AWG : Gain %.0f, R_TIA %.0f, Total gain : %e V/A\n",val_gain[1],RTIA[iRTIA],CATIA_gain_AWG[1][iRTIA]);

// Stop DAQ :
      command = ((nsample/4+1)<<16)+CAPTURE_STOP;
      hw.getNode("CAP_CTRL").write(command);
    }
    gain_stage_AWG=(CATIA_gain_AWG[0][0]/CATIA_gain_AWG[1][0]+CATIA_gain_AWG[0][1]/CATIA_gain_AWG[1][1]+CATIA_gain_AWG[0][2]/CATIA_gain_AWG[1][2])/3.;
    TIA_gain_AWG[0]=CATIA_gain_AWG[1][0]; // G1-R500
    TIA_gain_AWG[1]=CATIA_gain_AWG[1][1]; // G1-R400
    TIA_gain_AWG[2]=CATIA_gain_AWG[1][2]; // G1-R340
    printf("G10 with R500 : %.2f, with R400 : %.2f, with R340 : %2f, mean : %.2f\n",
           CATIA_gain_AWG[0][0]/CATIA_gain_AWG[1][0],CATIA_gain_AWG[0][1]/CATIA_gain_AWG[1][1],CATIA_gain_AWG[0][2]/CATIA_gain_AWG[1][2],gain_stage);
    printf("R500 = %.0f Ohm\n",TIA_gain_AWG[0]);
    printf("R400 = %.0f Ohm\n",TIA_gain_AWG[1]);
    printf("R340 = %.0f Ohm\n",TIA_gain_AWG[2]);
    Rconv_AWG[0]=CATIA_gain_AWG[0][1]/V_Iinj_slope[0]; // Iinj conversion resistor for G1
    Rconv_AWG[1]=CATIA_gain_AWG[1][1]/V_Iinj_slope[1]; // Iinj conversion resistor for G10
    printf("Rconv Iinj G10 (2471 Ohm) = %.0f Ohm\n",Rconv_AWG[0]);
    printf("Rconv Iinj G1  (272 Ohm)  = %.0f Ohm\n",Rconv_AWG[1]);
  }

  hcalib_t0->Fit("gaus","LQ","");
  f1=hcalib_t0->GetFunction("gaus");
  Double_t calib_pos=f1->GetParameter(1);
  Double_t calib_jitter=f1->GetParameter(2);
  hcalib_rt->Fit("gaus","LQ","");
  f1=hcalib_rt->GetFunction("gaus");
  Double_t RT=f1->GetParameter(1);
  Double_t eRT=f1->GetParameter(2);
  printf("Pulse jitter : %.1f ps, rise time %.2f ns\n",calib_jitter*1.e12,RT*1.e9);

// Stop DAQ :
  command = ((nsample/4+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
// Switch on FE-adapter LEDs
  command = LED_ON*1+GENE_100HZ*0+GENE_TRIGGER*0;
  hw.getNode("FW_VER").write(command);
  hw.dispatch();
// Restart LVRB auto scan
  printf("Restart LVRB auto scan mode\n");
  VFE_control= DELAY_AUTO_TUNE*delay_auto_tune | INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*1 |
               eLINK_ACTIVE*eLink_active | ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode | ADC_MEM_MODE*ADC_MEM_mode;
  hw.getNode("VFE_CTRL").write(VFE_control);
  hw.dispatch();

// Save ADC calibration (full list of registers :
  sprintf(output_file,"data/CATIA_calib/CATIA_%5.5d_ADC_calib.dat",CATIA_number);
  fcal=fopen(output_file,"w+");
  for(int iADC=0; iADC<2; iADC++)
  {
    Int_t device_number=I2C_LiTEDTU_type*1000+(Channel_num<<SHIFT_DEV_NUMBER)+iADC;      // ADC sub-addresses
    for(int ireg=0; ireg<N_ADC_REG; ireg++)
    {
      ADC_reg_val[iADC][ireg]=I2C_RW(hw, device_number, ireg, 0, 0, 2, 0);
    }
  }
  for(int ireg=0; ireg<N_ADC_REG; ireg++)
  {
    //printf("Register %d : 0x%2.2x 0x%2.2x\n",ireg,ADC_reg_val[0][ireg]&0xFF, ADC_reg_val[1][ireg]&0xFF);
    fprintf(fcal,"%d %d\n",ADC_reg_val[0][ireg]&0xff, ADC_reg_val[1][ireg]&0xff);
  }
  fclose(fcal);

// Switch off CATIA LV to change chip :
// First ask to generate alert with overcurrent
  val=I2C_RW(hw,0x67,0x01,0x20,0,1,debug);
  val=I2C_RW(hw,0x6C,0x01,0x20,0,1,debug);
// Now, generate a current overflow :
  val=I2C_RW(hw,0x67,0x03,0x20,0,1,debug);
  val=I2C_RW(hw,0x6C,0x03,0x20,0,1,debug);
  usleep(1000000);
// Read current values for control :
  val=I2C_RW(hw, 0x67, 0x14, 0,1, 2, debug); // Read Dsense value
  printf("CATIA V2P5 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V2P5);
  val=I2C_RW(hw, 0x6C, 0x14, 0,1, 2, debug); // Read Dsense value
  printf("CATIA V1P2 current : %8.8x, %.2f mA\n",val,((val>>4)&0xfff)/4096.*102.4/Rshunt_V1P2);

// Now compute Rconv for current injection should be around 272 and 2472 Ohms
  tg_Vref->Write();
  tg_Vdac[0]->Write();
  tg_Vdac[1]->Write();
  tg_Vdac_resi[0]->Write();
  tg_Vdac_resi[1]->Write();
  tg_ped_DAC[0]->Write();
  tg_ped_DAC[1]->Write();
  tg_pulse[0]->Write();
  tg_pulse[1]->Write();
  hmod[0][0]->Write();
  hmod[0][1]->Write();
  hmod[0][2]->Write();
  hmod[0][3]->Write();
  hmod[0][4]->Write();
  hmod[0][5]->Write();
  hmod[1][0]->Write();
  hmod[1][1]->Write();
  hmod[1][2]->Write();
  hmod[1][3]->Write();
  hmod[1][4]->Write();
  hmod[1][5]->Write();
  tp_inj_cal->Write();
  for(int iinj=0; iinj<n_TP_step; iinj++)
  {
    tp_inj1[0][iinj]->Write();
    tp_inj1[1][iinj]->Write();
    hp_inj1[0][iinj]->Write();
    hp_inj1[1][iinj]->Write();
    //tp_inj1[2][iinj]->Write();
  }
  //for(int iinj=0; iinj<n_AWG_step; iinj++)
  //{
  //  tp_awg[iinj]->Write();
  //}
  tg_inj1[0]->Write();
  tg_inj1[1]->Write();
  tg_inj2[0]->Write();
  tg_inj2[1]->Write();
  tg_inj3[0]->Write();
  tg_inj3[1]->Write();
  tp_calib[0][0]->Write();
  tp_calib[0][1]->Write();
  tp_calib[0][2]->Write();
  tp_calib[1][0]->Write();
  tp_calib[1][1]->Write();
  tp_calib[1][2]->Write();
  tg_calib[0]->Write();
  tg_calib[1]->Write();
  hcalib_t0->Write();
  hcalib_rt->Write();
  fd->Close();

  sprintf(output_file,"data/CATIA_calib/CATIA_%5.5d_calib.txt",CATIA_number);
  FILE *fout=fopen(output_file,"w+");
  fprintf(fout,"# Temperature FPGA CATIA_X1 CATIA_X5\n");
  fprintf(fout,"%e %e %e\n",Temp[0],Temp[1],Temp[2]);
  fprintf(fout,"# Vref DAC values\n");
  fprintf(fout,"%d\n",n_Vref);
  for(Int_t i=0; i<n_Vref; i++) fprintf(fout,"%e ",Vref[i]);
  fprintf(fout,"\n");
  fprintf(fout,"# Iinj DAC values : in, DAC1, DAC2\n");
  fprintf(fout,"%d\n",n_Vdac);
  for(Int_t i=0; i<n_Vdac; i++) fprintf(fout,"%e ",DAC_val[i]);
  fprintf(fout,"\n");
  for(Int_t i=0; i<n_Vdac; i++) fprintf(fout,"%e ",Vdac[0][i]);
  fprintf(fout,"\n");
  for(Int_t i=0; i<n_Vdac; i++) fprintf(fout,"%e ",Vdac[1][i]);
  fprintf(fout,"\n");
  fprintf(fout,"# Iinj DAC : slope DAC1 DAC2, offset DAC1 DAC2\n");
  fprintf(fout,"%e %e %e %e\n",Vdac_slope[0],Vdac_slope[1],Vdac_offset[0],Vdac_offset[1]);
  fprintf(fout,"# PED DAC values : G1, G10\n");
  fprintf(fout,"%d\n",n_PED_dac);
  for(Int_t i=0; i<n_PED_dac; i++)
  {
    Double_t x,y;
    tg_ped_DAC[0]->GetPoint(i,x,y);
    fprintf(fout,"%e ",y);
  }
  fprintf(fout,"\n");
  for(Int_t i=0; i<n_PED_dac; i++)
  {
    Double_t x,y;
    tg_ped_DAC[1]->GetPoint(i,x,y);
    fprintf(fout,"%e ",y);
  }
  fprintf(fout,"\n");
  fprintf(fout,"# PED DAC parameters : G1_ped[0], G1_ped_slope, G10_ped[0], G10_ped_slope\n");
  fprintf(fout,"%e %e %e %e\n",ped_DAC_par[1][0],ped_DAC_par[1][1],ped_DAC_par[0][0],ped_DAC_par[0][1]);
  fprintf(fout,"# Noise : G1-R500-LPF50, G1-R500-LPF35 G1-R400-LPF50 G1-R400-LPF35 G1-R340-LPF50 G1-R340-LPF35 G10-R500-LPF50, G10-R500-LPF35 G10-R400-LPF50 G10-R400-LPF35 G10-R340-LPF50 G10-R340-LPF35\n");
  fprintf(fout,"%e %e %e %e %e %e %e %e %e %e %e %e\n",G1_noise[0][0], G1_noise[0][1], G1_noise[1][0], G1_noise[1][1], G1_noise[2][0], G1_noise[2][1],
                                                       G10_noise[0][0],G10_noise[0][1],G10_noise[1][0],G10_noise[1][1], G10_noise[2][0], G10_noise[2][1]);
  fprintf(fout,"# INL : G1-min G1-max G10-min G10-max\n");
  fprintf(fout,"%e %e %e %e\n",INL_min[1],INL_max[1],INL_min[0],INL_max[0]);
  fprintf(fout,"# CATIA gains : G1-R500 G1-R400 G1-R340 G10-R500 G10-R400 G10-R340 (LPF35)\n");
  fprintf(fout,"%e %e %e %e %e %e\n",CATIA_gain[1][0],CATIA_gain[1][1],CATIA_gain[1][2],CATIA_gain[0][0],CATIA_gain[0][1],CATIA_gain[0][2]);
  fprintf(fout,"# CATIA parameters : G10 R500 R400 R340 Rconv_G1 Rconv_G10\n");
  fprintf(fout,"%e %e %e %e %e %e\n",gain_stage,TIA_gain[0],TIA_gain[1],TIA_gain[2],Rconv[1],Rconv[0]);
  fprintf(fout,"# Timing with calib pulses : pos, jitter, rising_time, rising_time RMS\n");
  fprintf(fout,"%e %e %e %e\n",calib_pos,calib_jitter,RT,eRT);
  fclose(fout);
}

