#include "uhal/uhal.hpp"

#include <signal.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <cstdlib>
#include <typeinfo>
#include "I2C_RW.h"
#include "Test_SEU.h"

#define GENE_TRIGGER    (1<<0)
#define GENE_CALIB      (1<<1)
#define GENE_100HZ      (1<<2)
#define LED_ON          (1<<3)
#define CALIB_MODE      (1<<4)

#define RESET                  (1<<0)
#define TRIGGER_MODE           (1<<1)
#define SOFT_TRIGGER           (1<<2)
#define SELF_TRIGGER           (1<<3)
#define CLOCK_PHASE            (1<<4)
#define CLOCK_RESET            (1<<7)
#define SELF_TRIGGER_THRESHOLD (1<<8)
#define SELF_TRIGGER_MASK      (1<<22)
#define ADC_CALIB_MODE         (1<<27)
#define BOARD_SN               (1<<28)
#define SEQ_CLOCK_PHASE        (1<<0)
#define IO_CLOCK_PHASE         (1<<4)
#define REG_CLOCK_PHASE        (1<<8)
#define MEM_CLOCK_PHASE        (1<<12)

#define CAPTURE_START 1
#define CAPTURE_STOP  2
//#define SW_DAQ_DELAY 0x1800 // delay for laser with internal trigger
#define SW_DAQ_DELAY 0x0 // delay for laser with external trigger
#define HW_DAQ_DELAY 100 // Laser with external trigger
#define NSAMPLE_MAX 28672
#define MAX_PAYLOAD 1380
#define MAX_VFE     10
#define MAX_STEP    10000

#define DRP_WRb         (1<<31)
#define I2C_RWb         (1<<31)
#define I2C_LONG        (1<<30)
#define I2C_DEVICE      (1<<23)
#define I2C_REG         (1<<16)
#define ADC_WRITE       (1<<24)

#define DELAY_INCREASE  (1<<5)     // Increase ADC signal delay by one tip
#define DELAY_RESET     (1<<6)     // Reset ADC signal delays

using namespace uhal;

log_struct test_log;
struct timeval tv;
static bool keepRunning = true;
FILE *fout=NULL;
void end_of_run()
{
  gettimeofday(&tv,NULL);
  printf("%ld.%6.6ld : End of study\n",tv.tv_sec,tv.tv_usec);
  printf("%ld.%6.6ld : SEU summary per CATIA :\n",tv.tv_sec,tv.tv_usec);
  fprintf(fout,"%ld.%6.6ld : End of study\n",tv.tv_sec,tv.tv_usec);
  fprintf(fout,"%ld.%6.6ld : SEU summary per CATIA :\n",tv.tv_sec,tv.tv_usec);
  for(int icatia=1; icatia<=5; icatia++)
  {
    printf("%ld.%6.6ld : Catia %d : SEU : %3d\n",tv.tv_sec,tv.tv_usec, icatia,test_log.n_SEU[icatia]);
    fprintf(fout,"%ld.%6.6ld : Catia %d : SEU : %3d\n",tv.tv_sec,tv.tv_usec, icatia,test_log.n_SEU[icatia]);
  }
  printf("%ld.%6.6ld : Summary of errors for each register :\n",tv.tv_sec,tv.tv_usec);
  fprintf(fout,"%ld.%6.6ld : Summary of errors for each register :\n",tv.tv_sec,tv.tv_usec);
  int tot_error=0;
  for(int icatia=1; icatia<=5; icatia++)
  {
    printf("%ld.%6.6ld : Catia %d : ",tv.tv_sec,tv.tv_usec,icatia);
    fprintf(fout,"%ld.%6.6ld : Catia %d : ",tv.tv_sec,tv.tv_usec,icatia);
    for(int ireg=1; ireg<=6; ireg++)
    {
      printf("%.6d ",test_log.error[icatia][ireg]);
      fprintf(fout,"%.6d ",test_log.error[icatia][ireg]);
      tot_error+=test_log.error[icatia][ireg];
    }
    printf("\n");
    fprintf(fout,"\n");
  }
  printf("Total number of errors : %d\n",tot_error);
  fprintf(fout,"%ld.%6.6ld : Total number of errors : %d\n",tv.tv_sec,tv.tv_usec,tot_error);

  printf("%ld.%6.6ld : SEL summary per power supply : ",tv.tv_sec,tv.tv_usec);
  printf("V2P5 : %d, V1P2 :%d\n",test_log.n_SEL[0],test_log.n_SEL[1]);
  fprintf(fout,"%ld.%6.6ld : SEL summary per power supply : ",tv.tv_sec,tv.tv_usec);
  fprintf(fout,"V2P5 : %d, V1P2 : %d\n",test_log.n_SEL[0],test_log.n_SEL[1]);

  fflush(stdout);
  fclose(fout);
  exit(1);
}
void intHandler(int)
{
  keepRunning = false;
  printf("Ctrl-C detected !\n");
  if(fout != NULL)
  {
    gettimeofday(&tv,NULL);
    printf("%ld.%6.6ld : Interupt detected\n",tv.tv_sec,tv.tv_usec);
    fprintf(fout,"%ld.%6.6ld : Interupt detected\n",tv.tv_sec,tv.tv_usec);
    end_of_run();
  }
}
void abortHandler(int)
{
  keepRunning = false;
  printf("Abort !\n");
  if(fout != NULL)
  {
    gettimeofday(&tv,NULL);
    printf("%ld.%6.6ld : Run aborted\n",tv.tv_sec,tv.tv_usec);
    fprintf(fout,"%ld.%6.6ld : Run aborted\n",tv.tv_sec,tv.tv_usec);
    end_of_run();
  }
} 

int main ( int argc,char* argv[] )
{
  signal(SIGINT, intHandler);
  signal(SIGABRT, intHandler);
  unsigned int device_number, command, val;

  double Rshunt=0.255; // 2x0.51 Ohm in parallel
  int debug=0;
  int n_vfe=0;
  int vfe[MAX_VFE];

// CATIA settings if requested
  int n_catia=0;
  int CATIA_num[5]   ={-1,-1,-1,-1,-1}; // CATIA number to address
  int CATIA_nums     =12345;  // CATIA numbers present on I2C bus (xyzuv) -1=no CATIA)
  int n_reg=0;
  int CATIA_reg[6]   ={-1,-1,-1,-1,-1,-1}; // CATIA number to address
  int CATIA_regs     =123456; // CATIA register to loop on (xyzuvw) -1=no reg)
  int n_loop         = 1;     // Number of loop to check I2C protocol
  int stop_on_error  = 0;
  int pause_on_error = 0;
  int reset_on_error = 0;
  int correct_on_error = 1;
  int delay          = 100;
  int I2C_rw         = 2;     // 2*read+write for the test loops
  double I_margin    = 1.0;   // 1 mA margin on current to generate alert
  char comment[132];
  comment[0]='\0';
  test_log.n_SEL[0]=0;
  test_log.n_SEL[1]=0;

  unsigned int reg_def[7] = {0x00, 0x02, 0x00, 0x1087, 0xffff, 0xffff, 0x0f};

  for(int k=1; k<argc; k++) 
  {    
    if(strcmp(argv[k],"-vfe") == 0)
    {
      sscanf( argv[++k], "%d", &vfe[n_vfe++] );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_nums") == 0)
    {    
      sscanf( argv[++k], "%d", &CATIA_nums );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_regs") == 0)
    {    
      sscanf( argv[++k], "%d", &CATIA_regs );
      continue;
    }    
    else if(strcmp(argv[k],"-Def_data1") == 0)
    {    
      sscanf( argv[++k], "%x", &reg_def[1] );
      continue;
    }    
    else if(strcmp(argv[k],"-Def_data2") == 0)
    {    
      sscanf( argv[++k], "%x", &reg_def[2] );
      continue;
    }    
    else if(strcmp(argv[k],"-Def_data3") == 0)
    {    
      sscanf( argv[++k], "%x", &reg_def[3] );
      continue;
    }    
    else if(strcmp(argv[k],"-Def_data4") == 0)
    {    
      sscanf( argv[++k], "%x", &reg_def[4] );
      continue;
    }    
    else if(strcmp(argv[k],"-Def_data5") == 0)
    {    
      sscanf( argv[++k], "%x", &reg_def[5] );
      continue;
    }    
    else if(strcmp(argv[k],"-Def_data6") == 0)
    {    
      sscanf( argv[++k], "%x", &reg_def[6] );
      continue;
    }    
    else if(strcmp(argv[k],"-I_margin") == 0)
    {    
      sscanf( argv[++k], "%lf", &I_margin );
      continue;
    }    
    else if(strcmp(argv[k],"-n_loop") == 0)
    {    
      sscanf( argv[++k], "%d", &n_loop );
      continue;
    }    
    else if(strcmp(argv[k],"-stop_on_error") == 0)
    {    
      sscanf( argv[++k], "%d", &stop_on_error );
      continue;
    }    
    else if(strcmp(argv[k],"-pause_on_error") == 0)
    {    
      sscanf( argv[++k], "%d", &pause_on_error );
      continue;
    }    
    else if(strcmp(argv[k],"-reset_on_error") == 0)
    {    
      sscanf( argv[++k], "%d", &reset_on_error );
      continue;
    }    
    else if(strcmp(argv[k],"-correct_on_error") == 0)
    {    
      sscanf( argv[++k], "%d", &correct_on_error );
      continue;
    }    
    else if(strcmp(argv[k],"-debug") == 0)
    {    
      sscanf( argv[++k], "%d", &debug );
      continue;
    }    
    else if(strcmp(argv[k],"-delay") == 0)
    {    
      sscanf( argv[++k], "%d", &delay );
      continue;
    }    
    else if(strcmp(argv[k],"-I2C_rw") == 0)
    {    
      sscanf( argv[++k], "%d", &I2C_rw );
      continue;
    }    
    else if(strcmp(argv[k],"-comment") == 0)
    {
      strcpy(comment,argv[++k]);
      continue;
    }    
    else if(strcmp(argv[k],"-h") == 0)
    {
      printf("Start DAQ with : \n");
      printf("-vfe                      : FE_adapter board to access [1]\n");
      printf("-CATIA_nums               : CATIA present on I2C bus which will be read (xyzuv) (-1=no catia) [12345]\n");
      printf("-CATIA_regs               : CATIA registers which will be read (xyzuvw) (1..6) [123456]\n");
      printf("-Def_data1                : Default data for register 1 : Slow Control reg [0x2]\n");
      printf("-Def_data2                : Default data for register 2 : SEU error counter reg [0x0]\n");
      printf("-Def_data3                : Default data for register 3 : TIA reg [0x1087]\n");
      printf("-Def_data4                : Default data for register 4 : Inj DAC1 reg [0xffff]\n");
      printf("-Def_data5                : Default data for register 5 : Inj DAC2 reg [0xffff]\n");
      printf("-Def_data6                : Default data for register 6 : Inj Ctl reg [0x0f]\n");
      printf("-I_margin                 : Current margin to generate alert en mA [1.0]\n");
      printf("-I2C_rw                   : read (2), write (1) or write_read (3) data on I2C bus [2]\n");
      printf("-n_loop                   : number of R/W cycle (-1=loop for ever) [1]\n");
      printf("-stop_on_error            : Stop (1) the R/W cycles when an error is detected [0]\n");
      printf("-pause_on_error           : Pause (1) the R/W cycles when an error is detected [0]\n");
      printf("-reset_on_error           : reset (1) VFE board when an error is detected [0]\n");
      printf("-correct_on_error         : correct (1) CATIA register content when an error is detected [1]\n");
      printf("-delay                    : Number of us to wait between cycles [100]\n");
      printf("-debug                    : Print debugging infos [0]\n");
      printf("-comment                  : Add comments in the log file [\"\"]\n");
        

      exit(-1);
    }
    else
    {
      printf("---------------------------------------------------\n");
      printf("Option %s not defined !, stop here.\n",argv[k]);
      printf("---------------------------------------------------\n");
      exit(-99);
    }
  }

  int I_margin_lsb   = int(I_margin*4096/102.4*Rshunt);
  
  if(n_vfe==0)
  {
    n_vfe=1;
    vfe[0]=1;
  }

  ConnectionManager manager ( "file:///data/cms/ecal/fe/fead_v2/software/connection_file.xml" );
  std::vector<uhal::HwInterface> devices;
  int i_vfe;
  for(i_vfe=0; i_vfe<n_vfe; i_vfe++)
  {
    char vice_str[80];
    sprintf(vice_str,"vice.udp.%d",vfe[i_vfe]);
    devices.push_back(manager.getDevice( vice_str ));
  }

  FILE *frun=NULL;
  frun=fopen("last_run_is","r");
  test_log.numrun=1;
  if(frun==NULL)
  {
    printf("Last_run_number not found. Start with run 1\n");
  }
  else
  {
    int last_run=0;
    int eof=fscanf(frun,"%d",&last_run);
    if(eof==1)test_log.numrun=last_run+1;
    fclose(frun);
    frun=fopen("last_run_is","w");
    fprintf(frun,"%d",test_log.numrun);
    fclose(frun);
  }
  char fname[80];
  sprintf(fname,"data/SEU_tests/run%6.6d.log",test_log.numrun);
  fout=fopen(fname,"w+");
  if(fout==NULL)
  {
    printf("Cannot create log file %s, stop here\n",fname);
    exit(0);
  }

  gettimeofday(&tv,NULL);
  fprintf(fout,"%ld.%6.6ld : start of run %d\n",tv.tv_sec,tv.tv_usec,test_log.numrun);
  printf("%ld.%6.6ld : start of run %d\n",tv.tv_sec,tv.tv_usec,test_log.numrun);
  fprintf(fout,"%ld.%6.6ld : %s\n",tv.tv_sec,tv.tv_usec,comment);
  printf("%ld.%6.6ld : %s\n",tv.tv_sec,tv.tv_usec,comment);


  printf("%ld.%6.6ld : Start DAQ with %d cards : ",tv.tv_sec,tv.tv_usec,n_vfe);
  fprintf(fout,"%ld.%6.6ld : Start DAQ with %d cards : ",tv.tv_sec,tv.tv_usec,n_vfe);
  for(int i=0; i<n_vfe; i++)
  {
    printf("%d, ",vfe[i]);
    fprintf(fout,"%d, ",vfe[i]);
  }
  printf("\n");
  fprintf(fout,"\n");

  n_catia=0;
  while(CATIA_nums>0)
  {
    CATIA_num[n_catia]=CATIA_nums%10;
    CATIA_nums/=10;
    n_catia++;
  }
  n_reg=0;
  while(CATIA_regs>0)
  {
    CATIA_reg[n_reg]=CATIA_regs%10;
    CATIA_regs/=10;
    n_reg++;
  }

  for(int icatia=0; icatia<=5; icatia++)
  {
    for(int ireg=1; ireg<=6; ireg++)test_log.error[icatia][ireg]=0;
  }

  printf("%ld.%6.6ld : Will ",tv.tv_sec,tv.tv_usec);
  fprintf(fout,"%ld.%6.6ld : Will ",tv.tv_sec,tv.tv_usec);
  if((I2C_rw&2)>0)
  {
    printf("read");
    fprintf(fout,"read");
  }
  if((I2C_rw&1)>0)
  {
    printf("/write");
    fprintf(fout,"/write");
  }
  printf(" %d catias : ",n_catia);
  fprintf(fout," %d catias : ",n_catia);
  for(int icatia=0; icatia<n_catia; icatia++)
  {
    printf("%d ",CATIA_num[icatia]);
    fprintf(fout,"%d ",CATIA_num[icatia]);
  }
  printf("\n%ld.%6.6ld : Will access %d registers : ",tv.tv_sec,tv.tv_usec,n_reg);
  fprintf(fout,"\n%ld.%6.6ld : Will access %d registers : ",tv.tv_sec,tv.tv_usec,n_reg);
  for(int ireg=0; ireg<n_reg; ireg++)
  {
    printf("%d ",CATIA_reg[ireg]);
    fprintf(fout,"%d ",CATIA_reg[ireg]);
  }
  printf("\n");
  fprintf(fout,"\n");
  printf("\n%ld.%6.6ld : Default register contents : ",tv.tv_sec,tv.tv_usec);
  fprintf(fout,"\n%ld.%6.6ld : Default register contents : ",tv.tv_sec,tv.tv_usec);
  for(int ireg=1; ireg<=6; ireg++)
  {
    if(ireg==3 || ireg==4 || ireg==5)
    {
      printf("%d : 0x%4.4x, ",ireg,reg_def[ireg]);
      fprintf(fout,"%d : 0x%4.4x, ",ireg,reg_def[ireg]);
    }
    else
    {
      printf("%d : 0x%2.2x, ",ireg,reg_def[ireg]);
      fprintf(fout,"%d : 0x%2.2x, ",ireg,reg_def[ireg]);
    }
  }
  printf("\n");
  fprintf(fout,"\n");

// Init stage :
  i_vfe=0;
  ValWord<uint32_t> busy,reg,fault,current;
  for(auto & hw : devices)
  {
  // Read FW version to check :
    reg = hw.getNode("FW_VER").read();
    hw.dispatch();
    printf("%ld.%6.6ld : Firmware version      : %8.8x\n",tv.tv_sec,tv.tv_usec,reg.value());
    fprintf(fout,"%ld.%6.6ld : Firmware version      : %8.8x\n",tv.tv_sec,tv.tv_usec,reg.value());
    i_vfe++;
  }

  printf("%ld.%6.6ld : Reset VFE board\n",tv.tv_sec,tv.tv_usec);
  fprintf(fout,"%ld.%6.6ld : Reset VFE board\n",tv.tv_sec,tv.tv_usec);
  for(auto & hw : devices)
  {
    hw.getNode("VICE_CTRL").write(RESET*1);
    hw.getNode("VICE_CTRL").write(RESET*0);
    hw.dispatch();
  }
  usleep(100);

  ValWord<uint32_t> meas, meas_V1P2,meas_V2P5, meas_temp;
  unsigned int I2C_long=0;
  for(auto & hw : devices)
  {
// Setup default values for CATIAs :
    for(int icatia=0; icatia<n_catia; icatia++)
    {
      int loc_num=CATIA_num[icatia];
      for(int ireg=1; ireg<=6; ireg++)
      {
        I2C_long=0;
        if(ireg==3 || ireg==4 || ireg==5)I2C_long=1;
        device_number=1000+((loc_num<<3)&0x7F);
        unsigned int loc_data=reg_def[ireg];
        if(icatia==0 && ireg==1) loc_data|=0x8; // swith ON temp for irradiates CATIA or first one in the list
        val=I2C_RW(hw, device_number, ireg, loc_data,I2C_long, 3, debug);
      }
    }
 
// Initialize current monitors
    for(int iLVR=0; iLVR<2; iLVR++)
    {
      printf("\n%ld.%6.6ld : Program LV sensor %d : ",tv.tv_sec,tv.tv_usec,iLVR);
      fprintf(fout,"\n%ld.%6.6ld : Program LV sensor %d : ",tv.tv_sec,tv.tv_usec,iLVR);
      unsigned int device_number;
      if(iLVR==0)
      {
        device_number=0x67;
        printf("V2P5\n");
        fprintf(fout,"2.5V\n");
      }
      else
      {
        device_number=0x6C;
        printf("V1P2\n");
        fprintf(fout,"1.2V\n");
      }

// Put LVRB in continuous ADC mode, monitor sense+, and power calculation with sense+
      int ireg=0; // control register
      command=0x5;
      val=I2C_RW(hw, device_number, ireg, command,0, 3, debug);
      printf("%ld.%6.6ld : New content of control register : 0x%x\n",tv.tv_sec,tv.tv_usec,val&0xffff);
      fprintf(fout,"%ld.%6.6ld : New content of control register : 0x%x\n",tv.tv_sec,tv.tv_usec,val&0xffff);
// Max power limit
      val=I2C_RW(hw, device_number, 0xe,  0xff,0, 1, debug);
      val=I2C_RW(hw, device_number, 0xf,  0xff,0, 1, debug);
      val=I2C_RW(hw, device_number, 0x10, 0xff,0, 1, debug);
// Min power limit
      val=I2C_RW(hw, device_number, 0x11, 0x00,0, 1, debug);
      val=I2C_RW(hw, device_number, 0x12, 0x00,0, 1, debug);
      val=I2C_RW(hw, device_number, 0x13, 0x00,0, 1, debug);
// Max ADin limit
      val=I2C_RW(hw, device_number, 0x2E, 0xFFF0,1, 3, debug);
// Min ADin limit
      val=I2C_RW(hw, device_number, 0x30, 0x0000,1, 3, debug);
// Max Vin limit
      val=I2C_RW(hw, device_number, 0x24, 0xFFF0,1, 3, debug);
// Min Vin limit
      val=I2C_RW(hw, device_number, 0x26, 0x0000,1, 3, debug);
// Max current limit
      val=I2C_RW(hw, device_number, 0x1A, 0xFFF0,1, 3, debug);
      printf("%ld.%6.6ld : New max threshold on current 0x%x\n",tv.tv_sec,tv.tv_usec,val&0xffff);
      fprintf(fout,"%ld.%6.6ld : New max threshold on current 0x%x\n",tv.tv_sec,tv.tv_usec,val&0xffff);
// Min current limit
      val=I2C_RW(hw, device_number, 0x1C, 0x0000,1, 3, debug);
      printf("%ld.%6.6ld : New min threshold on current 0x%x\n",tv.tv_sec,tv.tv_usec,val&0xffff);
      fprintf(fout,"%ld.%6.6ld : New min threshold on current 0x%x\n",tv.tv_sec,tv.tv_usec,val&0xffff);
// Read and clear Fault register :
      ireg=4; // Alert register
      val=I2C_RW(hw, device_number, ireg, command,0, 2, debug);
      printf("%ld.%6.6ld : Alert register content : 0x%x\n",tv.tv_sec,tv.tv_usec,val&0xffff);
      fprintf(fout,"%ld.%6.6ld : Alert register content : 0x%x\n",tv.tv_sec,tv.tv_usec,val&0xffff);
      val=I2C_RW(hw, device_number, ireg, command,0, 2, debug);
      printf("%ld.%6.6ld : Alert register content : 0x%x\n",tv.tv_sec,tv.tv_usec,val&0xffff);
      fprintf(fout,"%ld.%6.6ld : Alert register content : 0x%x\n",tv.tv_sec,tv.tv_usec,val&0xffff);
// Set Alert register
      ireg=1;
      command=0x20; // Alert on Dsense overflow
      val=I2C_RW(hw, device_number, ireg, command,0, 3, debug);
      printf("%ld.%6.6ld : New alert bit pattern : 0x%x\n",tv.tv_sec,tv.tv_usec, val&0xffff);
      fprintf(fout,"%ld.%6.6ld : New alert bit pattern : 0x%x\n",tv.tv_sec,tv.tv_usec, val&0xffff);
    }
    printf("\n");
    fprintf(fout,"\n");

    usleep(500000);

// Read voltages measured by controler
    for(int iLVR=0; iLVR<2; iLVR++)
    {
      char alim[80];
      if(iLVR==0)
      {
        device_number=0x67;
        sprintf(alim,"V2P5");
      }
      else
      {
        device_number=0x6C;
        sprintf(alim,"V1P2");
      }
      val=I2C_RW(hw, device_number, 0x1E, 0,1, 2, 0);
      double Vmeas=((val>>4)&0xfff);
      Vmeas=Vmeas/4096.*102.4; // Convert to V (102.4 V full scale)
      printf("%ld.%6.6ld : %s measured power supply voltage %7.3f V\n",tv.tv_sec,tv.tv_usec,alim,Vmeas);
      fprintf(fout,"%ld.%6.6ld : %s measured power supply voltage %7.3f V\n",tv.tv_sec,tv.tv_usec,alim,Vmeas);
      val=I2C_RW(hw, device_number, 0x28, 0,1, 2, 0);
      Vmeas=((val>>4)&0xfff);
      Vmeas=Vmeas/4096.*2.048; // Convert to V (2.048 V full scale)
      if(iLVR==0)Vmeas*=2.;
      printf("%ld.%6.6ld : %s measured sense voltage %7.3f V\n",tv.tv_sec,tv.tv_usec,alim,Vmeas);
      fprintf(fout,"%ld.%6.6ld : %s measured sense voltage %7.3f V\n",tv.tv_sec,tv.tv_usec,alim,Vmeas);
    }

// Read voltages measured by FPGA
    for(int iXADC=0; iXADC<2; iXADC++)
    {
      unsigned int XADC_reg=0x13;
      if(iXADC==1) XADC_reg=0x19;
      double loc_val=0., mean=0.;
      for(int iave=0; iave<16; iave++)
      {
        hw.getNode("DRP_XADC").write(DRP_WRb*0 | (XADC_reg<<16));
        hw.dispatch();
        meas  = hw.getNode("DRP_XADC").read();
        hw.dispatch();
        loc_val=((meas.value()&0xffff)>>4)/4096.;
        mean+=loc_val;
      }
      mean/=16.;
      if(iXADC==0)mean*=4.3; // (1k+3.3k divider)
      if(iXADC==1)mean*=2.;  // (1k+1k   divider)
      printf("%ld.%6.6ld : XADC average measurement : %.4f V\n",tv.tv_sec,tv.tv_usec,mean);
      fprintf(fout,"%ld.%6.6ld : XADC average measurement : %.4f V\n",tv.tv_sec,tv.tv_usec,mean);
    }

// Start monitoring SEUs
    for(int iloop=0; (iloop<n_loop || n_loop<0) && keepRunning; iloop++)
    {
      gettimeofday(&tv,NULL);

// Read status and currents and voltages
      fault     = hw.getNode("LVRB_FAULT").read();
      current   = hw.getNode("LVRB_CURRENT").read();
      hw.dispatch();
      test_log.Imeas_V2P5=((current.value()>>4)&0xfff)/4096.*102.4/Rshunt;
      test_log.Imeas_V1P2=((current.value()>>20)&0xfff)/4096.*102.4/Rshunt;
      test_log.V2P5_val=0.;
      test_log.V1P2_val=0.;
      test_log.temp_val=0.;
      for(int iave=0; iave<32; iave++)
      {
        hw.getNode("DRP_XADC").write(DRP_WRb*0 | (0x13<<16));
        meas_V2P5 = hw.getNode("DRP_XADC").read();
        hw.getNode("DRP_XADC").write(DRP_WRb*0 | (0x19<<16));
        meas_V1P2 = hw.getNode("DRP_XADC").read();
        hw.getNode("DRP_XADC").write(DRP_WRb*0 | (0x11<<16));
        meas_temp = hw.getNode("DRP_XADC").read();
        hw.dispatch();
        test_log.V2P5_val+=((meas_V2P5.value()&0xffff)>>4)/4096.*4.3;
        test_log.V1P2_val+=((meas_V1P2.value()&0xffff)>>4)/4096.*2.;
        test_log.temp_val+=((meas_temp.value()&0xffff)>>4)/4096.;
      }
      test_log.V2P5_val/=32.;
      test_log.V1P2_val/=32.;
      test_log.temp_val/=32.;
      test_log.temp_val=(0.640-test_log.temp_val)/0.002048;

// Look for SEL :
// V2P5 alert
      if((fault.value()&0x20) != 0 || (fault.value()&0x200000) != 0 || iloop==0 || test_log.V2P5_val<2.2 || test_log.V1P2_val<1.0)
      {
        printf("\n");
        fprintf(fout,"\n");
        //usleep(500000);
        //current   = hw.getNode("LVRB_CURRENT").read();
        //hw.dispatch();
        //test_log.Imeas_V2P5=((current.value()>>4)&0xfff)/4096.*102.4/Rshunt;
        //test_log.Imeas_V1P2=((current.value()>>20)&0xfff)/4096.*102.4/Rshunt;
        if((fault.value()&0x20)     != 0 || test_log.V2P5_val<2.2)
        {
          test_log.n_SEL[0]++;
          printf("%ld.%6.6ld : Alert seen on V2P5 : V=%7.3f, I=%7.3f mA\n",tv.tv_sec,tv.tv_usec,test_log.V2P5_val, test_log.Imeas_V2P5);
          fprintf(fout,"%ld.%6.6ld : Alert seen on V2P5 : V=%7.3f, I=%7.3f mA\n",tv.tv_sec,tv.tv_usec, test_log.V2P5_val, test_log.Imeas_V2P5);
        }
        if((fault.value()&0x200000) != 0 || test_log.V1P2_val<1.0)
        {
          test_log.n_SEL[1]++;
          printf("%ld.%6.6ld : Alert seen on V1P2 : V=%7.3f, I=%7.3f mA\n",tv.tv_sec,tv.tv_usec, test_log.V1P2_val, test_log.Imeas_V1P2);
          fprintf(fout,"%ld.%6.6ld : Alert seen on V1P2 : V=%7.3f, I=%7.3f mA\n",tv.tv_sec,tv.tv_usec, test_log.V1P2_val, test_log.Imeas_V1P2);
        }

// We had troubles, send a reset :
        hw.getNode("VICE_CTRL").write(RESET*1);
        hw.getNode("VICE_CTRL").write(RESET*0);
        hw.dispatch();

// Remove alert bit on both power supplies (register 1)
        printf("%ld.%6.6ld : Clear alert bit pattern\n",tv.tv_sec,tv.tv_usec);
        fprintf(fout,"%ld.%6.6ld : Clear alert bit pattern\n",tv.tv_sec,tv.tv_usec);
        command=0x00; // No more alert on Dsense overflow
        val=I2C_RW(hw, 0x67, 0x01, command,0, 3, debug);
        val=I2C_RW(hw, 0x6C, 0x01, command,0, 3, debug);
// Clear fault (register 4)
        //val=I2C_RW(hw, 0x67, 0x04, command,0, 2, debug);
        //printf("%ld.%6.6ld : V2P5 Fault register content : 0x%x cleared\n",tv.tv_sec,tv.tv_usec,val&0xffff);
        //fprintf(fout,"%ld.%6.6ld : V2P5 Fault register content : 0x%x cleared\n",tv.tv_sec,tv.tv_usec,val&0xffff);
        //val=I2C_RW(hw, 0x6C, 0x04, command,0, 2, debug);
        //printf("%ld.%6.6ld : V1P2 Fault register content : 0x%x cleared\n",tv.tv_sec,tv.tv_usec,val&0xffff);
        //fprintf(fout,"%ld.%6.6ld : V1P2 Fault register content : 0x%x cleared\n",tv.tv_sec,tv.tv_usec,val&0xffff);
// Wait for current stabilization
        printf("%ld.%6.6ld : Wait for current stabilization...\n",tv.tv_sec,tv.tv_usec);
        fprintf(fout,"%ld.%6.6ld : Wait for current stabilization...\n",tv.tv_sec,tv.tv_usec);
        usleep(1000000);

        hw.getNode("VICE_CTRL").write(RESET*1);
        hw.getNode("VICE_CTRL").write(RESET*0);
        hw.dispatch();
// Put back Catias default values
        for(int icatia=0; icatia<n_catia; icatia++)
        {
          int loc_num=CATIA_num[icatia];
          for(int ireg=1; ireg<=6; ireg++)
          {
            I2C_long=0;
            if(ireg==3 || ireg==4 || ireg==5)I2C_long=1;
            device_number=1000+((loc_num<<3)&0x7F);
            unsigned int loc_data=reg_def[ireg];
            if(icatia==0 && ireg==1) loc_data|=0x8; // swith ON temp for irradiates CATIA or first one in the list
            val=I2C_RW(hw, device_number, ireg, loc_data,I2C_long, 3, debug);
          }
        }
        usleep(500000);
// Measure current
        current = hw.getNode("LVRB_CURRENT").read();
        hw.dispatch();
        test_log.Imeas_V2P5=((current.value()>>4)&0xfff)/4096.*102.4/Rshunt;
        test_log.Imeas_V1P2=((current.value()>>20)&0xfff)/4096.*102.4/Rshunt;
// Set new threshold
        printf("%ld.%6.6ld : Present V2P5 current : %7.3f mA (0x%x)\n",tv.tv_sec,tv.tv_usec,test_log.Imeas_V2P5,(current.value()>>4)&0xfff);
        fprintf(fout,"%ld.%6.6ld : Present V2P5 current : %7.3f mA (0x%x)\n",tv.tv_sec,tv.tv_usec,test_log.Imeas_V2P5,(current.value()>>4)&0xfff);
        unsigned int thres=(((current.value()>>4)&0xfff)+I_margin_lsb+1)<<4;
        printf("%ld.%6.6ld : Set alert threshold at %7.3f mA (0x%x)\n",tv.tv_sec,tv.tv_usec,test_log.Imeas_V2P5+I_margin,thres);
        fprintf(fout,"%ld.%6.6ld : Set alert threshold at %7.3f mA (0x%x)\n",tv.tv_sec,tv.tv_usec,test_log.Imeas_V2P5+I_margin,thres);
        val=I2C_RW(hw, 0x67, 0x1A, thres,1, 3, debug);
        printf("%ld.%6.6ld : New V2P5 max threshold on current 0x%x\n",tv.tv_sec,tv.tv_usec,val&0xffff);
        fprintf(fout,"%ld.%6.6ld : New V2P5 max threshold on current 0x%x\n",tv.tv_sec,tv.tv_usec,val&0xffff);

        printf("%ld.%6.6ld : Present V1P2 current : %7.3f mA (0x%x)\n",tv.tv_sec,tv.tv_usec,test_log.Imeas_V1P2,(current.value()>>20)&0xfff);
        fprintf(fout,"%ld.%6.6ld : Present V1P2 current : %7.3f mA (0x%x)\n",tv.tv_sec,tv.tv_usec,test_log.Imeas_V1P2,(current.value()>>20)&0xfff);
        thres=(((current.value()>>20)&0xfff)+I_margin_lsb+1)<<4;
        printf("%ld.%6.6ld : Set alert threshold at %7.3f mA (0x%x)\n",tv.tv_sec,tv.tv_usec,test_log.Imeas_V1P2+I_margin,((current.value()>>20)&0xfff)+I_margin_lsb);
        fprintf(fout,"%ld.%6.6ld : Set alert threshold at %7.3f mA (0x%x)\n",tv.tv_sec,tv.tv_usec,test_log.Imeas_V1P2+I_margin,((current.value()>>20)&0xfff)+I_margin_lsb);
        val=I2C_RW(hw, 0x6C, 0x1A, thres,1, 3, debug);
        printf("%ld.%6.6ld : New V1P2 max threshold on current 0x%x\n",tv.tv_sec,tv.tv_usec,val&0xffff);
        fprintf(fout,"%ld.%6.6ld : New V1P2 max threshold on current 0x%x\n",tv.tv_sec,tv.tv_usec,val&0xffff);
// Reset fault register :
        val=I2C_RW(hw, 0x67, 0x04, command,0, 2, debug);
        printf("%ld.%6.6ld : V2P5 Fault register content : 0x%x cleared\n",tv.tv_sec,tv.tv_usec,val&0xffff);
        fprintf(fout,"%ld.%6.6ld : V2P5 Fault register content : 0x%x cleared\n",tv.tv_sec,tv.tv_usec,val&0xffff);
        val=I2C_RW(hw, 0x6C, 0x04, command,0, 2, debug);
        printf("%ld.%6.6ld : V1P2 Fault register content : 0x%x cleared\n",tv.tv_sec,tv.tv_usec,val&0xffff);
        fprintf(fout,"%ld.%6.6ld : V1P2 Fault register content : 0x%x cleared\n",tv.tv_sec,tv.tv_usec,val&0xffff);
// Put back alert bit
        printf("%ld.%6.6ld : Put back alert bit pattern\n",tv.tv_sec,tv.tv_usec);
        fprintf(fout,"%ld.%6.6ld : Put back alert bit pattern\n",tv.tv_sec,tv.tv_usec);
        command=0x20; // Alert on Dsense overflow
        val=I2C_RW(hw, 0x6C, 0x01, command,0, 3, debug);
        val=I2C_RW(hw, 0x67, 0x01, command,0, 3, debug);
        if(iloop==0)
        {
          printf("%ld.%6.6ld : Start SEU/SEL monitoring\n",tv.tv_sec,tv.tv_usec);
          fprintf(fout,"%ld.%6.6ld : Start SEU/SEL monitoring\n",tv.tv_sec,tv.tv_usec);
        }
        printf("\n");
        fprintf(fout,"\n");
      }

// Read SEU counters
      for(int icatia=1; icatia<=5; icatia++)
      {
        device_number=1000+((icatia<<3)&0x7F);
        val=I2C_RW(hw, device_number, 0x2, 0, 0, 2, debug);
        if((val&0xfff)!=test_log.n_SEU[icatia])
        {
          printf("%ld.%6.6ld : New SEU detected on Catia %d : #SEU : %3d\n",tv.tv_sec,tv.tv_usec,icatia,val&0xffff);
          fprintf(fout,"%ld.%6.6ld : New SEU detected on Catia %d : #SEU : %3d\n",tv.tv_sec,tv.tv_usec,icatia,val&0xffff);
        }
        test_log.n_SEU[icatia]=val&0xffff;
      }


      if((iloop%100)==0)
      {
        printf("\r%ld.%6.6ld : %.6d : V2P5 fault 0x%2.2x V=%7.3f V I=%7.3f mA SEL=%2d, V1P2 fault 0x%2.2x, V=%7.3f V I=%7.3f mA SEL=%2d, SEU : %3d %3d %3d %3d %3d, T=%.2f C",
                tv.tv_sec,tv.tv_usec,
                iloop,fault.value()&0xff,      test_log.V2P5_val,test_log.Imeas_V2P5,test_log.n_SEL[0],
                     (fault.value()>>16)&0xff, test_log.V1P2_val,test_log.Imeas_V1P2,test_log.n_SEL[1],
                test_log.n_SEU[1],test_log.n_SEU[2],test_log.n_SEU[3],test_log.n_SEU[4],test_log.n_SEU[5],
                test_log.temp_val);
        fflush(stdout);
      }
      if((iloop%1000)==0)
      {
        fprintf(fout,"%ld.%6.6ld : %.6d : V2P5 fault 0x%2.2x V=%7.3f V I=%7.3f mA, V1P2 fault 0x%2.2x, V=%7.3f V I=%7.3f mA, SEU : %3d %3d %3d %3d %3d, T=%.2f C\n",
                tv.tv_sec,tv.tv_usec,
                iloop,fault.value()&0xff,      test_log.V2P5_val,test_log.Imeas_V2P5,
                     (fault.value()>>16)&0xff, test_log.V1P2_val,test_log.Imeas_V1P2,
                test_log.n_SEU[1],test_log.n_SEU[2],test_log.n_SEU[3],test_log.n_SEU[4],test_log.n_SEU[5],
                test_log.temp_val);
        fflush(fout);
      }

      for(int icatia=0; icatia<n_catia; icatia++)
      {
        int loc_num=CATIA_num[icatia];
        for(int ireg=0; ireg<n_reg; ireg++)
        {
          I2C_long=0;
          int loc_reg=CATIA_reg[ireg];
          if(loc_reg==3 || loc_reg==4 || loc_reg==5)I2C_long=1;

          device_number=1000+((loc_num<<3)&0x7F);
          unsigned int loc_data=reg_def[loc_reg];
          if(icatia==0 && loc_reg==1)loc_data|=0x8;
          val=I2C_RW(hw, device_number, loc_reg, loc_data,I2C_long, I2C_rw, debug);
 
          int loc_error=0;
          if((val&0xffff)!= loc_data)
          {
            loc_error=1;
            test_log.error[loc_num][loc_reg]++;
          }
          if(debug>1 || loc_error==1)
          {
            printf("%ld.%6.6ld : I2C : Read CATIA %d register %d.",tv.tv_sec,tv.tv_usec,loc_num, loc_reg);
            fprintf(fout,"%ld.%6.6ld : I2C : Read CATIA %d register %d.",tv.tv_sec,tv.tv_usec,loc_num, loc_reg);
            printf("%ld.%6.6ld : Value read : 0x%.8x, should be 0x%.8x, error=%d\n",tv.tv_sec,tv.tv_usec,val, loc_data,loc_error);
            fprintf(fout,"%ld.%6.6ld : Value read : 0x%.8x, should be 0x%.8x, error=%d\n",tv.tv_sec,tv.tv_usec,val, loc_data,loc_error);
          }

          if(loc_error==1 && loc_reg==2)reg_def[loc_reg]=val&0xffff;
          if(loc_error==1 && pause_on_error==1)
          {
            system("stty raw");
            char cdum=getchar();
            system("stty -raw");
            if (cdum=='q' || cdum=='Q') exit(-1);
          }

          if(loc_error==1 && reset_on_error==1)
          {
            printf("%ld.%6.6ld : Reset VFE board\n",tv.tv_sec,tv.tv_usec);
            fprintf(fout,"%ld.%6.6ld : Reset VFE board\n",tv.tv_sec,tv.tv_usec);
            for(auto & hw_loc : devices)
            {
              hw_loc.getNode("VICE_CTRL").write(RESET*1);
              hw_loc.getNode("VICE_CTRL").write(RESET*0);
              hw_loc.dispatch();
            }
          }
          if(loc_error==1 && (correct_on_error==1 || reset_on_error==1))
          {
            I2C_long=0;
            if(loc_reg==3 || loc_reg==4 || loc_reg==5)I2C_long=1;
            device_number=1000+((loc_num<<3)&0x7F);
            loc_data=reg_def[loc_reg];
            if(icatia==0 && loc_reg==1)loc_data|=0x8;
            val=I2C_RW(hw, device_number, loc_reg, loc_data,I2C_long, 1, debug);
          }
          if(loc_error==1 && stop_on_error==1)
          {
            printf("%ld.%6.6ld : Value read : 0x%.8x, should be 0x%.8x, error=%d\n",tv.tv_sec,tv.tv_usec,val, reg_def[loc_reg],loc_error);
            fprintf(fout,"%ld.%6.6ld : Value read : 0x%.8x, should be 0x%.8x, error=%d\n",tv.tv_sec,tv.tv_usec,val, reg_def[loc_reg],loc_error);
            exit(-1);
          }
          usleep(delay);
        }
      }
    }
    printf("\n");
  }
}
