#include "uhal/uhal.hpp"

#include <vector>
#include <iostream>
#include <cstdlib>
#include <typeinfo>

#include "I2C_RW.h"
#include <TApplication.h>
#include <TProfile.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TH1D.h>
#include <TF1.h>
#include <TTree.h>
#include <TFile.h>

#define ACTIVE_CHANNELS        0x1B
#define GENE_TRIGGER           (1<<0)
#define TP_TRIGGER             (1<<1)
#define GENE_100HZ             (1<<2)
#define LED_ON                 (1<<3)
#define G1_CALIB_TRIGGER       (1<<4)
#define G10_CALIB_TRIGGER      (1<<5)
#define INVERT_FE_TRIGGER      (1<<6)

#define RESET                  (1<<0)
#define TRIGGER_MODE           (1<<1)
#define SOFT_TRIGGER           (1<<2)
#define SELF_TRIGGER           (1<<3)
#define CLOCK_PHASE            (1<<4)
#define CLOCK_RESET            (1<<7)
#define SELF_TRIGGER_THRESHOLD (1<<8)
#define SELF_TRIGGER_MASK      (1<<22)
#define SELF_TRIGGER_MODE      (1<<31)
#define SEL_CALIB_PULSE        (1<<23)
#define ADC_CALIB_MODE         (1<<27)
#define BOARD_SN               (1<<28)
#define TRIGGER_PHASE          (1<<29)
#define SEQ_CLOCK_PHASE        (1<<0)
#define IO_CLOCK_PHASE         (1<<4)
#define REG_CLOCK_PHASE        (1<<8)
#define MEM_CLOCK_PHASE        (1<<12)

#define CAPTURE_START 1
#define CAPTURE_STOP  2
//#define SW_DAQ_DELAY 0x1800 // delay for laser with internal trigger
#define SW_DAQ_DELAY 0x0 // delay for laser with external trigger
//#define HW_DAQ_DELAY  100 // Laser with external trigger
#define HW_DAQ_DELAY  0 // Laser with external trigger
//#define NSAMPLE_MAX 100
#define NSAMPLE_MAX 28672
#define MAX_PAYLOAD 1380
#define MAX_STEP    100

#define I2C_RWb         (1<<31)
#define I2C_LONG        (1<<30)
#define SPI_CATIA       (1<<26)
#define I2C_CATIA       (1<<25)
#define ADC_WRITE       (1<<24)
#define DAC_FULL_RESET  (0xF<<16)
#define DAC_SOFT_RESET  (0x7<<16)
#define DAC_VAL_REG     (0x3<<16)
#define DAC_CTRL_REG    (0x4<<16)

#define ADC_OMODE_REG   (0x14<<16) // ADC register to define Output mode of the ADC
#define ADC_ISPAN_REG   (0x18<<16) // ADC register to define input span of the ADC from 1.383V to 2.087V
#define ADC_CLK_DIVIDE  (0x0B<<16) // ADC register to define clock division of the ADC
#define ADC_OTEST_REG   (0x0D<<16) // ADC register to define test mode of the ADC

#define DELAY_INCREASE  (1<<5)     // Increase ADC signal delay by one tip
#define DELAY_RESET     (1<<6)     // Reset ADC signal delays

#define DRP_WRb         (1<<31)    // Acces to FGPA internal ADCs

using namespace uhal;
int nsample_max;
TCanvas *c1;

int get_event(uhal::HwInterface hw, int trigger_type, int nsample, ValVector< uint32_t >mem, short int *event[5], double *fevent[5], int debug, TCanvas *cdraw)
{
  static int old_address=-1, ievt=0;
  static TGraph *tg[5]={0};
  static int trigger_type_old=-1;
  int n_word;
  int n_transfer_max=249; // 3*32 bits words per sample to get the 5 channels data
  int n_transfer;
  int n_last;
  int max_address= NSAMPLE_MAX;
  int error      = 0;
  int command    = 0;
  int nsample_DAQ=nsample;

  int n_channel=0;
  int active[5]={0};
  for(Int_t ich=0; ich<5; ich++)if(((ACTIVE_CHANNELS>>ich)&1)==1){n_channel++; active[ich]=n_channel;}
  if(trigger_type_old!=trigger_type)ievt=0;
  if(tg[0]==NULL)
  {
    for(Int_t ich=0; ich<5; ich++)
    {
      tg[ich]=new TGraph();
      tg[ich]->SetMarkerStyle(20);
      tg[ich]->SetMarkerSize(0.5);
    }
    tg[0]->SetLineColor(kRed);
    tg[1]->SetLineColor(kYellow);
    tg[2]->SetLineColor(kBlue);
    tg[3]->SetLineColor(kMagenta);
    tg[4]->SetLineColor(kCyan);
    tg[0]->SetMarkerColor(kRed);
    tg[1]->SetMarkerColor(kYellow);
    tg[2]->SetMarkerColor(kBlue);
    tg[3]->SetMarkerColor(kMagenta);
    tg[4]->SetMarkerColor(kCyan);
  }

  ValWord<uint32_t> address,free_mem;
  address = hw.getNode("CAP_ADDRESS").read();
  hw.dispatch();
  if(debug>0)printf("Starting with address : %8.8x, trigger_type : %d\n",address.value(),trigger_type);
  old_address=address.value()>>16;
  if(old_address==max_address-1)old_address=-1;

  ValVector< uint32_t > block;
  if(trigger_type !=2)
  {
    if(trigger_type == 0)
      command = G10_CALIB_TRIGGER*0 | G1_CALIB_TRIGGER*0 | LED_ON*0 | GENE_100HZ*0 | TP_TRIGGER*0 | GENE_TRIGGER*1;
    else if(trigger_type == 1)
      command = G10_CALIB_TRIGGER*0 | G1_CALIB_TRIGGER*0 | LED_ON*0 | GENE_100HZ*0 | TP_TRIGGER*1 | GENE_TRIGGER*0;
    else if(trigger_type == 3)
      command = G10_CALIB_TRIGGER*0 | G1_CALIB_TRIGGER*1 | LED_ON*0 | GENE_100HZ*0 | TP_TRIGGER*0 | GENE_TRIGGER*1;
    else if(trigger_type == 4)
      command = G10_CALIB_TRIGGER*1 | G1_CALIB_TRIGGER*0 | LED_ON*0 | GENE_100HZ*0 | TP_TRIGGER*0 | GENE_TRIGGER*1;

    if(debug>0)printf("Send trigger with command : 0x%8.8x\n",command);
// Read base address and send trigger
    free_mem = hw.getNode("CAP_FREE").read();
// Read new address and wait for DAQ completion
    address = hw.getNode("CAP_ADDRESS").read();
    hw.getNode("FW_VER").write(command);
    hw.dispatch();
    if(debug>0)printf("Free memory : 0x%8.8x\n",free_mem.value());
    if(debug>0)printf("Start address : 0x%8.8x\n",address.value());
    int new_address=address.value()>>16;
    int loc_address=new_address;
    int nretry=0;
    while(((loc_address-old_address)%max_address) != nsample_DAQ+1 && nretry<100)
    {
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      new_address=address.value()>>16;
      loc_address=new_address;
      if(new_address<old_address)loc_address+=max_address;
      nretry++;
      if(debug>0)printf("ongoing R/W addresses    : old %d, new %d diff %d add 0x%8.8x\n",
                        old_address, new_address, (loc_address-old_address)%max_address, address.value());
    }
    if(nretry==100)
    {
      printf("Stop waiting for sample capture after %d retries\n",nretry);
      printf("R/W addresses    : old %8.8x, new %8.8x add 0x%8.8x\n", old_address, new_address, address.value());
      error=1;
    }
    old_address=new_address;
    if(old_address==max_address-1)old_address=-1;
    if(debug>0)printf("R/W addresses    : old %8.8x, new %8.8x add 0x%8.8x\n", old_address, new_address, address.value());
  }
  else
  {
    int nretry=0;
// Wait for external trigger to fill memory :
    free_mem = hw.getNode("CAP_FREE").read();
    address = hw.getNode("CAP_ADDRESS").read();
    hw.dispatch();
    while((free_mem.value()==max_address-1) && nretry<100)
    {
      free_mem = hw.getNode("CAP_FREE").read();
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      //printf("address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
      if(debug>0)printf("address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
      usleep(1000);
      nretry++;
    }
    if(nretry==100)
    {
      printf("Stop waiting for external trigger after %d retries\n",nretry);
      printf("R/W addresses    : add 0x%8.8x\n",address.value());
      error=1;
    }
  }
  mem.clear();

// Read event samples from FPGA
  n_word=(nsample_DAQ+1)*3;     // 3*32 bits words per sample to get the 5 channels data n G10
  n_transfer=n_word/(MAX_PAYLOAD/4); // Max ethernet packet = 1536 bytes, max user payload = 1380 bytes
  n_last=n_word-n_transfer*(MAX_PAYLOAD/4);
  if(n_transfer>n_transfer_max)
  {
    printf("Error : ask for too many samples. Number of transfer %d greater than allowed max value %d\n",n_transfer, n_transfer_max);
    error=1;
  }
  for(int itrans=0; itrans<n_transfer; itrans++)
  {
    block = hw.getNode ("CAP_DATA").readBlock(MAX_PAYLOAD/4);
    hw.dispatch();
    for(int is=0; is<MAX_PAYLOAD/4; is++)mem.push_back(block[is]);
  }
  block = hw.getNode ("CAP_DATA").readBlock(n_last);
  address = hw.getNode("CAP_ADDRESS").read();
  free_mem = hw.getNode("CAP_FREE").read();
  hw.dispatch();
  if(debug>0)printf("After reading address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
  for(int is=0; is<n_last; is++)mem.push_back(block[is]);
  mem.valid(true);

  if(debug>0)
  {
    printf("%8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",
        mem[0],mem[1],mem[2],mem[3],mem[4],mem[5],mem[6],mem[7],mem[8],mem[9]);
    printf("%8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",
        mem[10],mem[11],mem[12],mem[13],mem[14],mem[15],mem[16],mem[17],mem[18],mem[19]);
    printf("%8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",
        mem[20],mem[21],mem[22],mem[23],mem[24],mem[25],mem[26],mem[27],mem[28],mem[29]);
    printf("%.4d %.4d %.4d %.4d %.4d %.4d %.4d %.4d\n",
        mem[2]&0x3fff,mem[3]&0x3fff,mem[4]&0x3fff,mem[5]&0x3fff,mem[6]&0x3fff,mem[7]&0x3fff,mem[8]&0x3fff,mem[9]&0x3fff);
    printf("%.4d %.4d %.4d %.4d %.4d %.4d %.4d %.4d\n",
        (mem[2]>>14)&0x3ff,(mem[3]>>14)&0x3fff,(mem[4]>>14)&0x3fff,(mem[5]>>14)&0x3fff,
        (mem[6]>>14)&0x3ff,(mem[7]>>14)&0x3fff,(mem[8]>>14)&0x3fff,(mem[9]>>14)&0x3fff);
  }

  unsigned long int t1=0, t2=0, t3=0, t4=0, t5=0;
// First sample should have bit 70 at 1
  if((mem[0] >> 31) != 1)
  {
    printf("Sample 0 not a header : %8.8x\n",mem[0]);
    error=1;
  }
  t1= mem[0]     &0xFFFF;
  t2= mem[1]     &0xFFFF;
  t3=(mem[1]>>16)&0xFFFF;
  t4= mem[2]     &0xFFFF;
  t5=(mem[2]>>16)&0x00FF;
  unsigned long int timestamp=(t5<<56)+(t4<<42)+(t3<<28)+(t2<<14)+t1;

  if(debug>0)
  { 
    printf("timestamp : %8.8x %8.8x %8.8x\n",mem[2],mem[1],mem[0]);
    printf("timestamp : %ld %4.4lx %4.4lx %4.4lx %4.4lx %4.4lx\n",timestamp,t5,t4,t3,t2,t1);
  }

  Double_t loc_rms[5]={0.,0.,0.,0.,0.};
  Double_t loc_ave[5]={0.,0.,0.,0.,0.};
  Double_t loc_ped[5]={0.,0.,0.,0.,0.};
  Double_t dv=1750./16384.; // 14 bits on 1.75V
  Double_t dt=6.25;         // 160 MHz clock
  for(Int_t ich=0; ich<5; ich++)
  {
    loc_ped[ich]=0.;
    loc_ave[ich]=0.;
    loc_rms[ich]=0.;
  }
  for(int isample=0; isample<nsample; isample++)
  {
    int j;
    j=(isample+1)*3;
    event[0][isample]= mem[j]       &0x3FFF;
    event[1][isample]= mem[j+1]     &0x3FFF;
    event[2][isample]=(mem[j+1]>>16)&0x3FFF;
    event[3][isample]= mem[j+2]     &0x3FFF;
    event[4][isample]=(mem[j+2]>>16)&0x3FFF;
    fevent[0][isample]=double(event[0][isample]);
    fevent[1][isample]=double(event[1][isample]);
    fevent[2][isample]=double(event[2][isample]);
    fevent[3][isample]=double(event[3][isample]);
    fevent[4][isample]=double(event[4][isample]);
    for(Int_t ich=0; ich<5; ich++)
    {
      if(isample<4)loc_ped[ich]+=fevent[ich][isample]*dv;
      loc_ave[ich]+=fevent[ich][isample]*dv;
      loc_rms[ich]+=fevent[ich][isample]*dv*fevent[ich][isample]*dv;
    }
  }
  for(Int_t ich=0; ich<5; ich++)
  {
    loc_ped[ich]/=4.;
    loc_ave[ich]/=nsample;
    loc_rms[ich]/=nsample;
    loc_rms[ich]=loc_rms[ich]-loc_ave[ich]*loc_ave[ich];
    if(cdraw!=NULL && active[ich]>0)printf("ch %d : ped= %.2f mV, ave= %.2f mV, rms= %.2f uV\n",ich,loc_ped[ich], loc_ave[ich], sqrt(loc_rms[ich])*1000.);
  }

  if(debug>0 || trigger_type !=trigger_type_old || cdraw!=NULL)
  {
    for(int ich=0; ich<5; ich++)
    {
      tg[ich]->Clear();
      tg[ich]->Set(0);
      for(int isample=0; isample<nsample; isample++)
      {
        tg[ich]->SetPoint(isample,dt*isample,fevent[ich][isample]);
      }
      if(cdraw!=NULL && active[ich]>0)
      {
        cdraw->cd(active[ich]);
        tg[ich]->Draw("alp");
      }
    }
    cdraw->Update();
  }
  //if(debug>0)
  //{
  //  system("stty raw");
  //  cdum=getchar();
  //  system("stty -raw");
  //  if(cdum=='q')exit(-1);
  //}

  trigger_type_old=trigger_type;
  ievt++;
  if(error==0)
    return timestamp;
  else
    return -1;
}

Int_t main ( Int_t argc,char* argv[] )
{
  Int_t command, vice_ctrl=0;
  ValWord<uint32_t> address;
  ValWord<uint32_t> data[3*NSAMPLE_MAX];
  //ValVector< uint32_t > mem[256];
  ValVector< uint32_t > block,mem;
  short int *event[5]={0};
  double *fevent[5]={0};
  for(Int_t ich=0; ich<5; ich++)
  {
    event[ich]=(short int *)malloc(NSAMPLE_MAX*sizeof(short int));
    fevent[ich]=(double*)malloc(NSAMPLE_MAX*sizeof(double));
  }
  double dv=1750./16384.; // 14 bits on 1.75V
  double dt=6.25;         // 160 MHz clock
  Int_t debug=0;
  double ped[5], rms[5];

// Define defaults for laser runs :
  Int_t sw_DAQ_delay=SW_DAQ_DELAY;
  Int_t hw_DAQ_delay=HW_DAQ_DELAY;
  Int_t nevent=0;
  Int_t nsample=100;
  Int_t trigger_type=0;         // pedestal by default
  Int_t invert_FE_trigger=0;    // DOn't invert signal by default
  Int_t soft_trigger=1;         // external trigger by default 
  Int_t self_trigger=0;         // No self trigger 
  Int_t self_trigger_mode=0;    // 0 : trigger on absolute signal level, 1 : trigger on delta signal level
  Int_t self_trigger_threshold=3000;
  Int_t self_trigger_mask=0x18;    // don't trig on any channels amplitude
  Int_t trigger_phase=0;        // For MGPA, choose the pahse between 40 MHz and 160 MHz clock to generate trigger
  Int_t ADC_calib_mode = 0;     // Put (1) or not (0) the outputs of CATIA in ADC_calib_mode (outputs near VCM)
  Int_t seq_clock_phase    = 0; // sequence clock phase by steps of 45 degrees
  Int_t IO_clock_phase     = 5; // Capture clock phase by steps of 45 degrees for CATIA test board
  Int_t reg_clock_phase    = 0; // Shift register clock phase by steps of 45 degrees
  Int_t mem_clock_phase    = 0; // memory clock phase by steps of 45 degrees
  Int_t clock_phase        = 0;
  Int_t sel_calib_pulse    = 1; // Default calib pulse enable. Otherwise High_Z
  Int_t n_calib=-1;
  Int_t calib_step=9999;
  Int_t calib_level=-1;
  Int_t calib_width=-1;
  Int_t calib_delay=50;
  Int_t n_vfe=1;
  Int_t vfe=3;
  char cdum, output_file[256];
  Int_t delay_val[5]={20,20,20,20,20}; // Number of delay taps to get a stable R/O with ADCs (250 MHz)
  //Int_t delay_val[5]={25,25,25,20,15}; // Number of delay taps to get a stable R/O with ADCs (250 MHz)
  //Int_t delay_val[5]={0,0,0,0,0}; // Number of delay taps to get a stable R/O with ADCs (250 MHz)
  //Int_t delay_val[5]={20,10,0,0,0}; // Number of delay taps to get a stable R/O with ADCs (250 MHz)

// ADC settings if any :
  Int_t ADC_reg=-1;   // ADC register to write in.
  Int_t ADC_data=-1;  // data to write in ADC register.
  Int_t negate_data=0;    // Don't negate the ADC output by default (positive signal from 0 not to disturb people)
  Int_t signed_data=0;    // Don't use 2's complement. Use normal binary order (0...16383 full scale)
  Int_t input_span=0;     // Use default ADC input span (1.75V) can be tuned from 1.383 V to 2.087 V by steps of 0.022V
                        // this is 2's complemented on 5 bits. So from -16 to +15 -> 0x10..0x1f 0x00 0x01..0x0f -> 16..31 0 1..15
  Int_t output_test=0;    // Put ADC outputs in test mode (0=normal conversion values, 0xF=ramp)
  Int_t clock_divide=0;   // Put ADC clock division (0=take FE clock, 1=divide by 2)

// CATIA settings if reuested
  Int_t n_catia=0;
  Int_t CATIA_num[5]   ={-1,-1,-1,-1,-1}; // CATIA number to address
  Int_t CATIA_nums     =-1; // CATIA number to address (xyzuv) -1=no CATIA)
  Int_t CATIA_data     =-1; // Data to write in CATIA register
  Int_t CATIA_reg      =-1; // Register to write in with SPI or I2C protocol
  Int_t I2C_dir        =-1; // read(1)/write(0) with I2C protocol, -1=don't use I2C
  Int_t Reg1_def       = 0x2;    // Default content of Register 1
  Int_t Reg3_def       = 0x1087; // Default content of Register 3 : 2.5V, LPF35, 400 Ohms, ped mid scale
  Int_t Reg4_def       = 0x1000; // Default content of Register 4 : DAC1 0, DAC1 ON, DAC2 OFF, no copy
  Int_t Reg5_def       = 0x0000; // Default content of Register 5 : DAC2 0
  Int_t Reg6_def       = 0x0000; // Default content of Register 6 : Vreg OFF, Rconv=2471, TIA_dummy ON, TP OFF
  Int_t average        = 400;
  Int_t XADC_reg, XADC_Temp, XADC_Vdac, XADC_Vref;
  Int_t Monacal_num    = 9999;
  Int_t n_channel=0;
  Int_t active[5]={0};
  Int_t color[5]={kRed,kMagenta,kBlack,kBlue,kCyan};
  for(Int_t ich=0; ich<5; ich++)if(((ACTIVE_CHANNELS>>ich)&1)==1){active[n_channel]=ich;n_channel++;}


  TProfile *pshape[MAX_STEP][5]={0};
  TF1 *f0, *f1, *f2;
  sprintf(output_file,"");

  printf("Start calib_Monacal with %d arguments : ",argc);
  for(Int_t iarg=0; iarg<argc; iarg++)printf("%s, ",argv[iarg]);
  printf("\n");

  for(int k=1; k<argc; k++) 
  {    
    if(strcmp( argv[k], "-dbg") == 0)
    {    
      sscanf( argv[++k], "%d", &debug );
      continue;
    }    
    if(strcmp( argv[k], "-num") == 0)
    {    
      sscanf( argv[++k], "%d", &Monacal_num );
      continue;
    }    
    else if(strcmp(argv[k],"-h") == 0)
    {
      printf("Start DAQ with : \n");
      printf("-dbg dbg_level            : Set debug level for this session [0]\n");
      exit(-1);
    }
    else
    {
      printf("---------------------------------------------------\n");
      printf("Option %s not defined !, stop here.\n",argv[k]);
      printf("---------------------------------------------------\n");
      exit(-99);
    }
  }
  n_vfe=1;
  vfe=3;
  n_catia=1;
  CATIA_num[0]=1;

  nsample_max=NSAMPLE_MAX-2;
  Int_t max_address=nsample_max;
  Int_t nsample_DAQ=nsample;

  printf("Start DAQ to calibrate MONACAL #%3.3d\n",Monacal_num);
  XADC_Temp=0x14;
  XADC_Vref=-1;
  XADC_Vdac=-1;

  char hname[80];
  int loc_argc=1;
  char *loc_argv[10];
  for(int i=0; i<10; i++)loc_argv[i]=(char *)malloc(132*sizeof(char));
  sprintf(loc_argv[0],"test");
  TApplication *Root_App=new TApplication("test", &loc_argc, loc_argv);
  sprintf(hname,"VFE_0");

  c1=new TCanvas(hname, hname,920,0,1000.,800.);
  c1->Divide(2,2);
  c1->Update();

  TGraph *tg[5]={0};
  TH1D *hmean[5]={0}, *hrms[5]={0};
  for(int jch=0; jch<n_channel; jch++)
  {
    Int_t ich=active[jch];
    tg[ich] = new TGraph();
    tg[ich]->SetMarkerStyle(20);
    tg[ich]->SetMarkerSize(0.5);
    tg[ich]->SetLineColor(color[ich]);
    tg[ich]->SetMarkerColor(color[ich]);
    sprintf(hname,"mean_ch%d",ich);
    hmean[ich]=new TH1D(hname,hname,100,150.,250.);
    sprintf(hname,"rms_ch%d",ich);
    hrms[ich]=new TH1D(hname,hname,200,0.,2.);
  }

  ConnectionManager manager ( "file:///data/cms/ecal/fe/fead_v2/software/xml/VICE/connection_file.xml" );
  char vice_str[80];
  sprintf(vice_str,"vice.udp.%d",vfe);
  uhal::HwInterface hw=manager.getDevice( vice_str );

  ValWord<uint32_t> free_mem;
  ValWord<uint32_t> trig_reg;
  ValWord<uint32_t> delays;

// Reset board
  hw.getNode("VICE_CTRL").write((vice_ctrl&0xfffffffe) | RESET*1);
  hw.getNode("VICE_CTRL").write((vice_ctrl&0xfffffffe) | RESET*0);
  hw.dispatch();
  usleep(500);
  
  unsigned int ireg,device_number,val;
// Program ADCs
  printf("Program_ADCs...\n");

// Put ADC in two's complement mode (if no pedestal bias) and invert de conversion result
  negate_data=(negate_data&1)<<2;
  signed_data&=3;
  command=ADC_WRITE | ADC_OMODE_REG | negate_data | signed_data;
  printf("Put ADC coding : 0x%x\n",command);
  hw.getNode("DTU_CTRL").write(command);
  hw.dispatch();

// Set ADC input range (default=1.75V)
  input_span&=0x1F;
  command=ADC_WRITE | ADC_ISPAN_REG | input_span;
  printf("Set ADC input span range : 0x%x\n",command);
  hw.getNode("DTU_CTRL").write(command);
  hw.dispatch();

// Set ADC output test mode
  output_test&=0x0F;
  command=ADC_WRITE | ADC_OTEST_REG | output_test;
  printf("Set ADC output test mode : 0x%x\n",command);
  hw.getNode("DTU_CTRL").write(command);
  hw.dispatch();

  if(ADC_reg>=0)
  {
    command=ADC_WRITE | (ADC_reg<<16) | ADC_data;
    printf("Set ADC register 0x%x with data 0x%x\n",ADC_reg, ADC_data);
    hw.getNode("DTU_CTRL").write(command);
    hw.dispatch();
  }

// Set delay tap value for each ADC
  for(int i=0; i<5; i++)
  {
    command=DELAY_RESET*1;
    hw.getNode("DELAY_CTRL").write(command);
    hw.dispatch();
    usleep(1000);
  }
  printf("Get lock status of IDELAY input stages\n");
  ValWord<uint32_t> reg = hw.getNode("DELAY_CTRL").read();
  hw.dispatch();
  printf("Delay lock value : 0x%x\n",reg.value());

  for(int iADC=0; iADC<5; iADC++)
  {
    command=0;
    command=DELAY_RESET*0 | DELAY_INCREASE*1 | (1<<iADC);
    for(Int_t itap=0; itap<delay_val[iADC]; itap++)
    {
      hw.getNode("DELAY_CTRL").write(command);
      hw.dispatch();
    }
    printf("Get lock status of IDELAY input stages\n");
    ValWord<uint32_t> reg = hw.getNode("DELAY_CTRL").read();
    hw.dispatch();
    printf("Value read : 0x%x\n",reg.value());
  }

// Init stage :
  printf("Init stage...\n");

// Read FW version to check :
  reg = hw.getNode("FW_VER").read();
  command = ((nsample_DAQ+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
// Add laser latency before catching data ~ 40 us
  hw.getNode("TRIG_DELAY").write((sw_DAQ_delay<<16)+hw_DAQ_delay);
// Switch off FE-adapter LEDs
  command = INVERT_FE_TRIGGER*invert_FE_trigger | G10_CALIB_TRIGGER*0 | G1_CALIB_TRIGGER*0 | LED_ON*0 | GENE_100HZ*0 | TP_TRIGGER*0 | GENE_TRIGGER*0;
  hw.getNode("FW_VER").write(command);
  hw.dispatch();
// Set the clock phases
  command = MEM_CLOCK_PHASE*mem_clock_phase+REG_CLOCK_PHASE*reg_clock_phase+IO_CLOCK_PHASE*IO_clock_phase+SEQ_CLOCK_PHASE*seq_clock_phase;
  printf("Set clock phases : 0x%x\n",command);
  hw.getNode("CLK_SETTING").write(command);
  hw.dispatch();

// Read back delay values :
  delays=hw.getNode("TRIG_DELAY").read();
// Read back the read/write base address
  address = hw.getNode("CAP_ADDRESS").read();
  free_mem = hw.getNode("CAP_FREE").read();
  trig_reg = hw.getNode("VICE_CTRL").read();
  hw.dispatch();

  printf("Firmware version      : %8.8x\n",reg.value());
  printf("Delays                : %8.8x\n",delays.value());
  printf("Initial R/W addresses : 0x%8.8x\n", address.value());
  printf("Free memory           : 0x%8.8x\n", free_mem.value());
  printf("Trigger mode          : 0x%8.8x\n", trig_reg.value());

  unsigned long int timestamp=0;

// Setup default values for CATIA:
  printf("Setup CATIAs...\n");
  unsigned int I2C_long=0;
// Stop LVRB auto scan, not to genrate noise in CATIA during calibration
  printf("Stop LVRB auto scan mode\n");
  command=0;
  hw.getNode("VFE_CTRL").write(command);
  hw.dispatch();

  printf("Load CATIA registers\n");
  for(Int_t icatia=0; icatia<n_catia; icatia++)
  {
    device_number=1000+(CATIA_num[icatia]&0xF);
    ireg=1; // control register
    val=I2C_RW(hw, device_number, ireg, Reg1_def,0, 3, debug);
    ireg=3; // control register
// Gain 400 Ohm, Output stage for 2.5V, LPF35, 0 pedestal
    val=I2C_RW(hw, device_number, ireg, Reg3_def,1, 3, debug);
    ireg=4; // control register
// DAC1 0, DAC2 0, DAC1 ON, DAC2 OFF, DAC1 copied on DAC2
    val=I2C_RW(hw, device_number, ireg, Reg4_def,1, 3, debug);
    ireg=5; // control register
// DAC2 at 0 but OFF, so should not matter
    val=I2C_RW(hw, device_number, ireg, Reg5_def,1, 3, debug);
    ireg=6; // control register
// TIA dummy ON, Rconv 2471 (G10 scale), Vref ON (0xb) OFF (0x3), Injection in CATIA
    val=I2C_RW(hw, device_number, ireg, Reg6_def,0, 3, debug);

// Custom I2C settings
    if(I2C_dir>=0)
    {
      printf("Access CATIA with I2C :\n");
//        for(auto & hw : devices)
      I2C_long=0;
      if(CATIA_reg==3 || CATIA_reg==4 || CATIA_reg==5)I2C_long=1;

      device_number=1000+(CATIA_num[icatia]&0xF);
      val=I2C_RW(hw, device_number, CATIA_reg, CATIA_data,I2C_long, I2C_dir, debug);
      printf("Value read : 0x%x\n",val);
    }
  }

// Read Temperature and others analog voltages if requested

// Read XADC register 0x40 and set the requested average to 1 in XADC
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  ValWord<uint32_t> meas  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  unsigned loc_ave=meas.value()&0xffff;
  printf("Old config register 0x40 content : %x\n",loc_ave);

  loc_ave=0x8000;
  command=DRP_WRb*1 | (0x40<<16) | loc_ave;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  meas  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  loc_ave=meas.value()&0xffff;
  printf("New config register 0x40 content : %x\n",loc_ave);
  command=DRP_WRb*1 | (0x42<<16) | 0x0400;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();
// Switch off temp output on all CATIAs :
  for(int icatia=0; icatia<n_catia; icatia++)
  {
    device_number=1000+(CATIA_num[icatia]&0xF);
    ireg=1; // control register
    command=0x2;
    val=I2C_RW(hw, device_number, ireg, command,0, 3, debug);
  }

// Now, make the temp measurement on all catias, sequentially end for both current gain
  for(int XADC_meas=0; XADC_meas<4; XADC_meas++)
  {
    XADC_reg=0;
    if(XADC_meas==1)XADC_reg=XADC_Temp; // Temperature measurement
    if(XADC_Vref<0) continue;
    if(XADC_meas==2)XADC_reg=XADC_Vref; // CATIA Vref measurement (pin 22)
    if(XADC_meas==3)XADC_reg=XADC_Vdac; // CATIA Vdac measurement (pin 25)

    device_number=1000 + (CATIA_num[0]);
    Int_t nmeas=1;
    if(XADC_meas==1)nmeas=2;
    if(XADC_meas==2)nmeas=15;
    if(XADC_meas==3)nmeas=33;
    for(int imeas=0; imeas<nmeas; imeas++)
    {
      int loc_meas=imeas;
      if(XADC_meas==1) // Temp measurements
      {
        ireg=1; // control register
        command= (imeas<<4) | 0xa;
        val=I2C_RW(hw, device_number, ireg, command,0, 3, debug);
        usleep(100000);
      }
      if(XADC_meas==2) // Scan Vref_reg values
      {
        ireg=6; // DAC control register
        if(imeas>0)loc_meas=imeas+1;
        command= (loc_meas<<4) | 0xF;
        val=I2C_RW(hw, device_number, ireg, command,0, 3, debug);
        usleep(10);
      }
      if(XADC_meas==3) // Scan Vref_reg values
      {
        ireg=6; // DAC control register
        command= 0xF; // Enable Vref_ref
        val=I2C_RW(hw, device_number, ireg, command,0, 3, debug);
        ireg=4; // DAC control register
        loc_meas=imeas*128;
        if(loc_meas>4095)loc_meas=4095;
        command=0x1000 | (imeas*128); // Scan DAC1 values 
        val=I2C_RW(hw, device_number, ireg, command,1, 3, debug);
        usleep(100);
      }

      double ave_val=0.;
      ValWord<uint32_t> temp;
      for(int iave=0; iave<average; iave++)
      {
        command=DRP_WRb*0 | (XADC_reg<<16);
        hw.getNode("DRP_XADC").write(command);
        temp  = hw.getNode("DRP_XADC").read();
        hw.dispatch();
        double loc_val=double((temp.value()&0xffff)>>4)/4096.;
        ave_val+=loc_val;
      }
      ave_val/=average;
      if(XADC_meas==0)
        printf("FPGA temperature, meas %d : %.2f deg\n",imeas, ave_val*4096*0.123-273);
      else if(XADC_meas==1)
        printf("CATIA 0, X5 %d, temperature : %.4f V\n", imeas,ave_val);
      else if(XADC_meas==2)
        printf("CATIA 0, Vref DAC %d, Vref : %.4f V\n", loc_meas,ave_val);
      else if(XADC_meas==3)
        printf("CATIA 0, TP DAC %d, Vdac : %.4f V\n", loc_meas, ave_val);
    }
  }

// 1 : Noise measurement
  nsample_DAQ=NSAMPLE_MAX-2;
  nsample=nsample_DAQ;
  nevent=101;
  self_trigger=0;
  soft_trigger=1;
  trigger_type=0;

  vice_ctrl=  (TRIGGER_PHASE         *trigger_phase)                   |
              (ADC_CALIB_MODE        *(ADC_calib_mode&1))              |
              (SEL_CALIB_PULSE       *sel_calib_pulse)                 |
              (SELF_TRIGGER_MASK     *(self_trigger_mask&0x1F))        |
              (SELF_TRIGGER_THRESHOLD*(self_trigger_threshold&0x3FFF)) |
            //(CLOCK_PHASE           *(clock_phase&0x7))               |
               SELF_TRIGGER_MODE     *self_trigger_mode                |
               SELF_TRIGGER          *self_trigger                     |
               SOFT_TRIGGER          *soft_trigger                     |
               TRIGGER_MODE          *1                                | // Always DAQ on trigger
               RESET                 *0;
  hw.getNode("VICE_CTRL").write(vice_ctrl); // Master trigger board
// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
// Start DAQ :
  command = ((nsample_DAQ+1)<<16)+CAPTURE_START;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();

  for(int ich=0; ich<5; ich++)
  {
    ped[ich]=0.;
    rms[ich]=0.;
  }
  for(Int_t ievt=0; ievt<nevent; ievt++)
  {
    if((ievt%100)==0)printf("%d\n",ievt); 
    double loc_ped[5], loc_rms[5];
    for(int ich=0; ich<5; ich++)
    {
      loc_ped[ich]=0.;
      loc_rms[ich]=0.;
    }

    if((ievt%100)==0 || debug>0)
      timestamp=get_event(hw,trigger_type,nsample_DAQ,mem,event,fevent,debug,c1);
    else
      timestamp=get_event(hw,trigger_type,nsample_DAQ,mem,event,fevent,debug,NULL);
    for(int jch=0; jch<n_channel; jch++)
    {
      Int_t ich=active[jch];
      for(int isample=0; isample<nsample; isample++)
      {
        tg[ich]->SetPoint(isample,dt*isample,fevent[ich][isample]);
        ped[ich]+=dv*fevent[ich][isample];
        rms[ich]+=dv*fevent[ich][isample]*dv*fevent[ich][isample];
        loc_ped[ich]+=dv*fevent[ich][isample];
        loc_rms[ich]+=dv*fevent[ich][isample]*dv*fevent[ich][isample];
      }
    }
    for(int jch=0; jch<n_channel; jch++)
    {
      Int_t ich=active[jch];
      loc_ped[ich]/=nsample;
      loc_rms[ich]/=nsample;
      loc_rms[ich]=sqrt(loc_rms[ich]-loc_ped[ich]*loc_ped[ich]);
      if(debug>0)printf("- ped=%f, rms=%f ",loc_ped[ich],loc_rms[ich]);
      hmean[ich]->Fill(loc_ped[ich]);
      hrms[ich]->Fill(loc_rms[ich]);
    }
    if(debug>0)printf("\n");
  }
  printf("Global noise : ");
  for(int jch=0; jch<n_channel; jch++)
  {
    Int_t ich=active[jch];
    ped[ich]/=nsample*nevent;
    rms[ich]/=nsample*nevent;
    rms[ich]=sqrt(rms[ich]-ped[ich]*ped[ich]);
    printf("%d : %.2fmV/%.2fuV ",ich,ped[ich],rms[ich]*1000.);
  }
  printf("\n");
// Stop DAQ :
  command = ((nsample_DAQ+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
  hw.dispatch();

// 2 : TP/linearity measurement
  nsample_DAQ=1000;
  nsample=nsample_DAQ;
  trigger_type=1;
  nevent=80;
  soft_trigger=1;
  self_trigger=0;
  n_calib=62;
  calib_step=-10;
  calib_level=3220;
  Int_t calib_level_ref=calib_level;
  calib_width=255;
  //Reg6_def= 0x0e; // Rconv 2471 (useless), TIA_dummy ON, TP ON, Vreg ON with max gain (0)
  Reg6_def= 0x08; // Vreg ON with max gain (0)

// Calibration trigger setting :
  command=(calib_delay<<16) | (calib_width&0xffff);
  printf("Calibration trigger with %d clocks width and %d clocks delay : %x\n",calib_width,calib_delay,command);
  hw.getNode("CALIB_CTRL").write(command);
  hw.dispatch();


  TGraph *tg_inj1[5]={0}, *tg_inj2[5]={0};
  tg_inj1[0]=new TGraph();
  tg_inj1[0]->SetTitle("Inj_vs_iDAC_G1_1");
  tg_inj1[0]->SetName("Inj_vs_iDAC_G1_1");
  tg_inj1[0]->SetMarkerStyle(20);
  tg_inj1[0]->SetMarkerSize(1.0);
  tg_inj1[0]->SetMarkerColor(kRed);
  tg_inj1[0]->SetLineColor(kRed);
  tg_inj1[0]->SetLineWidth(2);
  tg_inj1[1]=new TGraph();
  tg_inj1[1]->SetTitle("Inj_vs_iDAC_G1_2");
  tg_inj1[1]->SetName("Inj_vs_iDAC_G1_2");
  tg_inj1[1]->SetMarkerStyle(20);
  tg_inj1[1]->SetMarkerSize(1.0);
  tg_inj1[1]->SetMarkerColor(kMagenta);
  tg_inj1[1]->SetLineColor(kMagenta);
  tg_inj1[1]->SetLineWidth(2);
  tg_inj1[2]=new TGraph();
  tg_inj1[3]=new TGraph();
  tg_inj1[3]->SetTitle("Inj_vs_iDAC_G12_1");
  tg_inj1[3]->SetName("Inj_vs_iDAC_G12_1");
  tg_inj1[3]->SetMarkerStyle(20);
  tg_inj1[3]->SetMarkerSize(1.0);
  tg_inj1[3]->SetMarkerColor(kBlue);
  tg_inj1[3]->SetLineColor(kBlue);
  tg_inj1[3]->SetLineWidth(2);
  tg_inj1[4]=new TGraph();
  tg_inj1[4]->SetTitle("Inj_vs_iDAC_G12_2");
  tg_inj1[4]->SetName("Inj_vs_iDAC_G12_2");
  tg_inj1[4]->SetMarkerStyle(20);
  tg_inj1[4]->SetMarkerSize(1.0);
  tg_inj1[4]->SetMarkerColor(kCyan);
  tg_inj1[4]->SetLineColor(kCyan);
  tg_inj1[4]->SetLineWidth(2);

  tg_inj2[0]=new TGraph();
  tg_inj2[0]->SetTitle("INL_vs_amplitude_G1_1");
  tg_inj2[0]->SetName("INL_vs_amplitude_G1_1");
  tg_inj2[0]->SetMarkerStyle(20);
  tg_inj2[0]->SetMarkerSize(1.0);
  tg_inj2[0]->SetMarkerColor(kRed);
  tg_inj2[0]->SetLineColor(kRed);
  tg_inj2[0]->SetLineWidth(2);
  tg_inj2[1]=new TGraph();
  tg_inj2[1]->SetTitle("INL_vs_amplitude_G1_2");
  tg_inj2[1]->SetName("INL_vs_amplitude_G1_2");
  tg_inj2[1]->SetMarkerStyle(20);
  tg_inj2[1]->SetMarkerSize(1.0);
  tg_inj2[1]->SetMarkerColor(kMagenta);
  tg_inj2[1]->SetLineColor(kMagenta);
  tg_inj2[1]->SetLineWidth(2);
  tg_inj2[2]=new TGraph();
  tg_inj2[3]=new TGraph();
  tg_inj2[3]->SetTitle("INL_vs_amplitude_G12_1");
  tg_inj2[3]->SetName("INL_vs_amplitude_G12_1");
  tg_inj2[3]->SetMarkerStyle(20);
  tg_inj2[3]->SetMarkerSize(1.0);
  tg_inj2[3]->SetMarkerColor(kBlue);
  tg_inj2[3]->SetLineColor(kBlue);
  tg_inj2[3]->SetLineWidth(2);
  tg_inj2[4]=new TGraph();
  tg_inj2[4]->SetTitle("INL_vs_amplitude_G12_2");
  tg_inj2[4]->SetName("INL_vs_amplitude_G12_2");
  tg_inj2[4]->SetMarkerStyle(20);
  tg_inj2[4]->SetMarkerSize(1.0);
  tg_inj2[4]->SetMarkerColor(kCyan);
  tg_inj2[4]->SetLineColor(kCyan);
  tg_inj2[4]->SetLineWidth(2);
  tg_inj2[0]->SetMaximum(0.3);
  tg_inj2[0]->SetMinimum(-.3);
  tg_inj2[1]->SetMaximum(0.3);
  tg_inj2[1]->SetMinimum(-.3);
  tg_inj2[3]->SetMaximum(0.3);
  tg_inj2[3]->SetMinimum(-.3);
  tg_inj2[4]->SetMaximum(0.3);
  tg_inj2[4]->SetMinimum(-.3);

  Int_t dac_val=calib_level;
  for(Int_t istep=0; istep<n_calib; istep++)
  {
    for(Int_t jch=0; jch<n_channel; jch++)
    {
      Int_t ich=active[jch];
      char pname[132];
      sprintf(pname,"ch_%d_step_%d_%d",ich,istep,dac_val);
      pshape[istep][ich]=new TProfile(pname,pname,nsample,0.,dt*nsample);
    }
    if(istep<32)
      dac_val+=calib_step;
    else
      dac_val+=calib_step*10.;
  }

  vice_ctrl=  (TRIGGER_PHASE         *trigger_phase)                   |
              (ADC_CALIB_MODE        *(ADC_calib_mode&1))              |
              (SEL_CALIB_PULSE       *sel_calib_pulse)                 |
              (SELF_TRIGGER_MASK     *(self_trigger_mask&0x1F))        |
              (SELF_TRIGGER_THRESHOLD*(self_trigger_threshold&0x3FFF)) |
            //(CLOCK_PHASE           *(clock_phase&0x7))               |
               SELF_TRIGGER_MODE     *self_trigger_mode                |
               SELF_TRIGGER          *self_trigger                     |
               SOFT_TRIGGER          *soft_trigger                     |
               TRIGGER_MODE          *1                                | // Always DAQ on trigger
               RESET                 *0;
  hw.getNode("VICE_CTRL").write(vice_ctrl); // Master trigger board
// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
// Start DAQ :
  command = ((nsample_DAQ+1)<<16)+CAPTURE_START;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();

// Send triggers and wait between each trigger :
  for(int istep=0; istep<n_calib; istep++)
  {
// Program DAC for this step
//    for(auto & hw : devices)
    for(Int_t icatia=0; icatia<n_catia; icatia++)
    {
      if(calib_level>=0)
      {
        device_number=1000+(CATIA_num[icatia]&0xF);
        ireg=6; // Iinj control register
        val=I2C_RW(hw, device_number, ireg, Reg6_def,0, 3, debug);
        ireg=4; // DAC1 register
        printf("Put %d in DAC register 4\n",calib_level);
        val=I2C_RW(hw, device_number, ireg, 0x1000|calib_level,1, 3, debug);
        printf("DAC register value read : 0x%x\n",val);
      }
      else
      {
// switch off all the injection system
        device_number=1000+(CATIA_num[icatia]&0xF);
        ireg=6; // Iinj control register
        val=I2C_RW(hw, device_number, ireg, 0,0, 3, debug);
      }
    }
// Wait for Vdac to stabilize :
    usleep(100000);

    printf("start sending calibration triggers :\n"); 
    for(Int_t ievt=0; ievt<nevent; ievt++)
    {
      if((ievt%100)==0)printf("%d\n",ievt); 

      if((ievt%100)==0 || debug>0)
        timestamp=get_event(hw,trigger_type,nsample_DAQ,mem,event,fevent,debug,c1);
      else
        timestamp=get_event(hw,trigger_type,nsample_DAQ,mem,event,fevent,debug,NULL);

      for(int jch=0; jch<n_channel; jch++)
      {
        Int_t ich=active[jch];
        for(int isample=0; isample<nsample; isample++)
        {
          pshape[istep][ich]->Fill(dt*isample+1.,dv*fevent[ich][isample]);
        }
      }
// not much than 100 Hz :
      usleep(10000);
    }
    for(int jch=0; jch<n_channel; jch++)
    {
      Int_t ich=active[jch];
      pshape[istep][ich]->Fit("pol0","WQ","",0.,200.);
      f0=pshape[istep][ich]->GetFunction("pol0");
      Double_t loc_ped=f0->GetParameter(0);
      Double_t qmax=pshape[istep][ich]->GetMaximum();
      if(qmax-loc_ped<1200.)
      {
        //for(int is=0; is<nsample; is++){printf("ich %d is %d y %f\n",ich,is, pshape[istep][ich]->GetBinContent(is+1));}
        pshape[istep][ich]->Fit("pol2","","",1450.,1850.);
        f2=pshape[istep][ich]->GetFunction("pol2");
        if(f2!=NULL)
        {
          Double_t qmax=f2->GetMaximum();
          Double_t tmax=f2->GetMaximumX();
          pshape[istep][ich]->Fit("pol2","WQ","",tmax-65.,tmax+65.);
          qmax=f2->GetMaximum();
          Double_t y=qmax-loc_ped;
          if(fabs(y)<1.e-6)y=1.e-6;
          tg_inj1[ich]->SetPoint(istep,calib_level_ref-calib_level,y);
        }
        else
        {
          printf("Strange, no function available\n");
          for(int is=0; is<nsample; is++){printf("ich %d is %d y %f\n",ich,is, pshape[istep][ich]->GetBinContent(is+1));}
        }
      }
      printf("step %d : %d %d %.2f %.2f %.2f\n",istep,calib_level,ich,loc_ped,qmax,qmax-loc_ped);
    }
    if(istep<32)
      calib_level+=calib_step;
    else
      calib_level+=calib_step*10.;
  }

  Double_t INL_min[5]={0}, INL_max[5]={0};
  for(Int_t jch=0; jch<n_channel; jch++)
  {
    Int_t ich=active[jch];
    Int_t n=tg_inj1[ich]->GetN();
    Int_t ifirst=-1, ilast=-1;
    Double_t xlow, xhigh;
    for(Int_t i=0; i<n; i++)
    {
      Double_t x,y;
      tg_inj1[ich]->GetPoint(i,x,y);
      if(y>0. && ifirst<0)
      {
        ifirst=i;
        xlow=x;
      }
      if(y<1200.)
      {
        ilast=i;
        xhigh=x;
      }
    }
    printf("will fit linearity between %d, %.2f and %d, %.2f\n",ifirst,xlow,ilast, xhigh);
    tg_inj1[ich]->Fit("pol1","","",xlow,xhigh);
    f1=tg_inj1[ich]->GetFunction("pol1");
    for(Int_t i=0; i<n; i++)
    {
      Double_t x,y,z,resi;
      tg_inj1[ich]->GetPoint(i,x,y);
      z=f1->Eval(x);
      resi=y-z;
      Double_t INL=resi/1200.;
      tg_inj2[ich]->SetPoint(i,y,INL*100.);
      if(i>=ifirst && i<=ilast && INL>INL_max[ich])INL_max[ich]=INL;
      if(i>=ifirst && i<=ilast && INL<INL_min[ich])INL_min[ich]=INL;
    }
  }
// Stop DAQ :
  command = ((nsample_DAQ+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();
// Restart LVRB auto scan
  printf("Stop LVRB auto scan mode\n");
  command=1;
  hw.getNode("VFE_CTRL").write(command);
  hw.dispatch();

// 3 : AWG for absolute gain calibration
  nsample_DAQ=1000;
  nsample=nsample_DAQ;
  trigger_type=2;
  nevent=101;
  soft_trigger=0;
  self_trigger=1;
  self_trigger_mode=0;
  self_trigger_threshold=3000;
  self_trigger_mask=0x18;

  vice_ctrl=  (TRIGGER_PHASE         *trigger_phase)                   |
              (ADC_CALIB_MODE        *(ADC_calib_mode&1))              |
              (SEL_CALIB_PULSE       *sel_calib_pulse)                 |
              (SELF_TRIGGER_MASK     *(self_trigger_mask&0x1F))        |
              (SELF_TRIGGER_THRESHOLD*(self_trigger_threshold&0x3FFF)) |
            //(CLOCK_PHASE           *(clock_phase&0x7))               |
               SELF_TRIGGER_MODE     *self_trigger_mode                |
               SELF_TRIGGER          *self_trigger                     |
               SOFT_TRIGGER          *soft_trigger                     |
               TRIGGER_MODE          *1                                | // Always DAQ on trigger
               RESET                 *0;
  hw.getNode("VICE_CTRL").write(vice_ctrl); // Master trigger board
  hw.dispatch();
// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
  hw.dispatch();

  printf("Please, start The AWG with 100mV/2us pulse @ 100 Hz\n"); 
  system("stty raw");
  cdum=getchar();
  system("stty -raw");
  if(cdum=='q')exit(-1);
  usleep(100000);

  TProfile *ppulse[5]={0};
  for(Int_t jch=0; jch<n_channel; jch++)
  {
    Int_t ich=active[jch];
    char pname[132];
    sprintf(pname,"ch_%d_AWG_pulse",ich);
    ppulse[ich]=new TProfile(pname,pname,nsample,0.,dt*nsample);
  }
// Start DAQ :
  command = ((nsample_DAQ+1)<<16)+CAPTURE_START;
  hw.getNode("CAP_CTRL").write(command);
  hw.dispatch();

  for(Int_t ievt=0; ievt<nevent; ievt++)
  {
    if((ievt%100)==0)printf("%d\n",ievt); 

    if((ievt%100)==0 || debug>0)
      timestamp=get_event(hw,trigger_type,nsample_DAQ,mem,event,fevent,debug,c1);
    else
      timestamp=get_event(hw,trigger_type,nsample_DAQ,mem,event,fevent,debug,NULL);
    for(int jch=0; jch<n_channel; jch++)
    {
      Int_t ich=active[jch];
      for(int isample=0; isample<nsample; isample++)
      {
        ppulse[ich]->Fill(dt*isample+1.,fevent[ich][isample]);
      }
    }
  }
  Double_t gain[5];
  for(int jch=0; jch<n_channel; jch++)
  {
    Int_t ich=active[jch];
    ppulse[ich]->Fit("pol0","wq","",0.,200.);
    f0=ppulse[ich]->GetFunction("pol0");
    Double_t loc_ped=f0->GetParameter(0);
    Double_t qmax=ppulse[ich]->GetMaximum();
    Double_t tmax=ppulse[ich]->GetXaxis()->GetBinCenter(ppulse[ich]->GetMaximumBin());
    ppulse[ich]->Fit("pol2","WQ","",tmax-125.,tmax+125.);
    f2=ppulse[ich]->GetFunction("pol2");
    qmax=f2->GetMaximum();
    tmax=f2->GetMaximumX();
    ppulse[ich]->Fit("pol2","WQ","",tmax-65.,tmax+65.);
    qmax=f2->GetMaximum();
    tmax=f2->GetMaximumX();
    // 100 mV on 10 pF :
    gain[ich]=(qmax-loc_ped)/(10.e-12*0.1/1.e-15);
    printf("ch %d : tmax %.1f, qmax %.2f mV, gain %.2f mV/fC\n",ich,tmax,qmax-loc_ped,gain[ich]);
  }

// Stop DAQ :
  command = ((nsample_DAQ+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
// Switch on FE-adapter LEDs
  command = INVERT_FE_TRIGGER*invert_FE_trigger | LED_ON*1 | GENE_100HZ*0 | GENE_TRIGGER*0;
  hw.getNode("FW_VER").write(command);
  hw.dispatch();
// Restart LVRB auto scan
  printf("Stop LVRB auto scan mode\n");
  command=1;
  hw.getNode("VFE_CTRL").write(command);
  hw.dispatch();

  TCanvas *c2, *c3;
  c2=new TCanvas("mean","mean",800.,800.);
  c2->Divide(2,2);
  c3=new TCanvas("rms","rms",800.,800.);
  c3->Divide(2,2);
  c2->Update();
  c3->Update();
  printf("RMS : ");
  for(int jch=0; jch<n_channel; jch++)
  {
    Int_t ich=active[jch];
    c2->cd(jch+1);
    hmean[ich]->Draw();
    c2->Update();
    c3->cd(jch+1);
    hrms[ich]->Draw();
    printf("%e, ",rms[ich]);
    c3->Update();
  }
  printf("\n");
  sprintf(output_file,"data/Monacal/monacal_%5.5d_calib.root",Monacal_num);

  TFile *fd=new TFile(output_file,"recreate");
  c1->Write();
  c2->Write();
  c3->Write();
  for(int istep=0; istep<n_calib; istep++)
  {
    for(int jch=0; jch<n_channel; jch++)
    {
      Int_t ich=active[jch];
      pshape[istep][ich]->Write();
    }
  }
  for(int jch=0; jch<n_channel; jch++)
  {
    Int_t ich=active[jch];
    tg_inj1[ich]->Write();
    tg_inj2[ich]->Write();
    ppulse[ich]->Write();
  }
  fd->Close();

  sprintf(output_file,"data/Monacal/monacal_%5.5d_calib.txt",Monacal_num);
  FILE *fout=fopen(output_file,"w+");
  fprintf(fout,"# Noise G1-1 G1-2 G12-1 G12-2 (all event samples merged), G1-1, G1-2, G12-1, G12-2 (mean event rms) [mV]\n");
  fprintf(fout,"%e %e %e %e %e %e %e %e\n",rms[0],rms[1],rms[3],rms[4],hrms[0]->GetMean(),hrms[1]->GetMean(),hrms[3]->GetMean(),hrms[4]->GetMean());
  fprintf(fout,"# INL : G1-1-min G1-1-max G1-2-min G1-2-max G12-1-min G12-1-max G12-2-min G12-2-max\n");
  fprintf(fout,"%e %e %e %e %e %e %e %e\n",INL_min[0],INL_max[0],INL_min[1],INL_max[1],INL_min[3],INL_max[3],INL_min[4],INL_max[4]);
  fprintf(fout,"# INL data\n");
  fprintf(fout,"%d %d\n",tg_inj2[0]->GetN(), tg_inj2[3]->GetN());
  for(Int_t jch=0; jch<n_channel; jch++)
  {
    Int_t ich=active[jch];
    for(Int_t i=0; i<tg_inj2[ich]->GetN(); i++)
    {
      Double_t x,y;
      tg_inj2[ich]->GetPoint(i,x,y);
      fprintf(fout,"%e %e ",x,y);
    }
    fprintf(fout,"\n");
  }
  fprintf(fout,"# Gains : G1-1 G1-2 G12-1 G12-2 G12/G1-1 G12-G1-2\n");
  fprintf(fout,"%e %e %e %e %e %e\n",gain[0],gain[1],gain[3],gain[4],gain[3]/gain[0],gain[4]/gain[1]);
  fclose(fout);

}
