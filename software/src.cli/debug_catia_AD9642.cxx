// Add some mods to be able to read legacy VFE boards.
// Requires VICE boards with special firmware : VFE_5C_MGPA_FE_clock_202109081.mcs
// VFE num is equal to VICE number + 100 (to tell that we are ready MGPA boards)
// then we read 4 times more samples @ 160 MHz than requested and keep one out of 4 (40 MHz)
// I2C is also working : see MGPA pedestal setting section. setting between 0x40 and 0x50 gives reasoonable pedestal
// Search for MGPA in the code to see specific part for legacy electronics :
// nsample_DAQ vs nsample and get_event function.
// Exemple of commands :
// Pedestal event with soft trigger :
// bin/debug_catia_AD9642.exe -vfe 104 -trigger_type 0 -nsample 5000 -soft_trigger 1 -dbg 1 -nevt 100 -MGPA_nums 12345 -has_ADC 1
// Event with trigger from FE :
// bin/debug_catia_AD9642.exe -vfe 104 -trigger_type 2 -nsample 100 -soft_trigger 0 -dbg 1 -nevt 100 -MGPA_nums 12345 -has_ADC 1
// Event with self generated trigger (threshold on signal amplitude
// bin/debug_catia_AD9642.exe -vfe 108 -trigger_type 2 -nsample 100 -soft_trigger 0 -dbg 1 -nevt 100 -MGPA_nums 12345 -has_ADC 1 -self_trigger 1 -self_trigger_threshold 5000 -self_trigger_mask 1f -self_trigger_mode 0 -hw_DAQ_delay 100
// Event with self generated trigger (threshold on delta signal amplitude)
// bin/debug_catia_AD9642.exe -vfe 108 -trigger_type 2 -nsample 100 -soft_trigger 0 -dbg 1 -nevt 100 -MGPA_nums 12345 -has_ADC 1 -self_trigger 1 -self_trigger_threshold 50 -self_trigger_mask 1f -self_trigger_mode 1 -hw_DAQ_delay 100
#include "uhal/uhal.hpp"

#include <vector>
#include <iostream>
#include <cstdlib>
#include <typeinfo>

#include "I2C_RW.h"
#include <TApplication.h>
#include <TProfile.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TH1D.h>
#include <TTree.h>
#include <TFile.h>

#define N_CATIA                5
#define MAX_BOARD              5
#define GENE_TRIGGER           (1<<0)
#define TP_TRIGGER             (1<<1)
#define GENE_100HZ             (1<<2)
#define LED_ON                 (1<<3)
#define G1_CALIB_TRIGGER       (1<<4)
#define G10_CALIB_TRIGGER      (1<<5)
#define INVERT_FE_TRIGGER      (1<<6)

#define RESET                  (1<<0)
#define TRIGGER_MODE           (1<<1)
#define SOFT_TRIGGER           (1<<2)
#define SELF_TRIGGER           (1<<3)
#define CLOCK_PHASE            (1<<4)
#define CLOCK_RESET            (1<<7)
#define SELF_TRIGGER_THRESHOLD (1<<8)
#define SELF_TRIGGER_MASK      (1<<22)
#define ADC_CALIB_MODE         (1<<27)
#define SEL_CALIB_PULSE        (1<<28)
#define BOARD_SN               (1<<28)
#define TRIGGER_PHASE          (1<<29)
#define SELF_TRIGGER_MODE      (1<<31)
#define SEQ_CLOCK_PHASE        (1<<0)
#define IO_CLOCK_PHASE         (1<<4)
#define REG_CLOCK_PHASE        (1<<8)
#define MEM_CLOCK_PHASE        (1<<12)

#define CAPTURE_START 1
#define CAPTURE_STOP  2
//#define SW_DAQ_DELAY 0x1800 // delay for laser with internal trigger
#define SW_DAQ_DELAY 0x0 // delay for laser with external trigger
#define HW_DAQ_DELAY  100 // Laser with external trigger
//#define NSAMPLE_MAX 100
//#define NSAMPLE_MAX 28672
#define NSAMPLE_MAX 65536
#define MAX_PAYLOAD 1380
#define MAX_VFE     10
#define MAX_STEP    10000

#define I2C_RWb         (1<<31)
#define I2C_LONG        (1<<30)
#define SPI_CATIA       (1<<26)
#define I2C_CATIA       (1<<25)
#define ADC_WRITE       (1<<24)
#define DAC_FULL_RESET  (0xF<<16)
#define DAC_SOFT_RESET  (0x7<<16)
#define DAC_VAL_REG     (0x3<<16)
#define DAC_CTRL_REG    (0x4<<16)

#define ADC_OMODE_REG   (0x14<<16) // ADC register to define Output mode of the ADC
#define ADC_ISPAN_REG   (0x18<<16) // ADC register to define input span of the ADC from 1.383V to 2.087V
#define ADC_CLK_DIVIDE  (0x0B<<16) // ADC register to define clock division of the ADC
#define ADC_OTEST_REG   (0x0D<<16) // ADC register to define test mode of the ADC

#define DELAY_INCREASE  (1<<5)     // Increase ADC signal delay by one tip
#define DELAY_RESET     (1<<6)     // Reset ADC signal delays

#define DRP_WRb         (1<<31)    // Acces to FGPA internal ADCs

using namespace uhal;
int nsample_max;
TCanvas *c1[5];

int get_event(uhal::HwInterface hw, int trigger_type, int nsample, ValVector< uint32_t >mem, short int *event[N_CATIA], double *fevent[N_CATIA], int debug, TCanvas *cdraw, int use_mgpa)
{
  static int old_address=-1, ievt=0;
  static TGraph *tg[N_CATIA]={NULL, NULL, NULL, NULL, NULL};
  static int trigger_type_old=-1;
  int n_word;
  int n_transfer_max=249; // 3*32 bits words per sample to get the 5 channels data
  int n_transfer;
  int n_last;
  int max_address= nsample_max;
  int error      = 0;
  int command    = 0;
  int nsample_DAQ=nsample;
  if(use_mgpa>0)
  {
    nsample_DAQ=nsample*4;
    if(nsample_DAQ>nsample_max-2)
    {
      nsample_DAQ=nsample_max-2;
      nsample=nsample_DAQ/4;
      nsample_DAQ=nsample*4;
    }
  }

  if(trigger_type_old!=trigger_type)ievt=0;
  if(tg[0]==NULL)
  {
    for(Int_t ich=0; ich<N_CATIA; ich++)
    {
      tg[ich]=new TGraph();
      tg[ich]->SetMarkerStyle(20);
      tg[ich]->SetMarkerSize(0.5);
    }
    tg[0]->SetLineColor(kRed);
    tg[1]->SetLineColor(kYellow);
    tg[2]->SetLineColor(kBlue);
    tg[3]->SetLineColor(kMagenta);
    tg[4]->SetLineColor(kCyan);
    tg[0]->SetMarkerColor(kRed);
    tg[1]->SetMarkerColor(kYellow);
    tg[2]->SetMarkerColor(kBlue);
    tg[3]->SetMarkerColor(kMagenta);
    tg[4]->SetMarkerColor(kCyan);
  }

  ValWord<uint32_t> address,free_mem;
  address = hw.getNode("CAP_ADDRESS").read();
  hw.dispatch();
  if(debug>0)printf("Starting with address : %8.8x, trigger_type : %d\n",address.value(),trigger_type);
  old_address=address.value()>>16;
  if(old_address==max_address-1)old_address=-1;

  ValVector< uint32_t > block;
  if(trigger_type !=2)
  {
    if(trigger_type == 0)
      command = G10_CALIB_TRIGGER*0 | G1_CALIB_TRIGGER*0 | LED_ON*0 | GENE_100HZ*0 | TP_TRIGGER*0 | GENE_TRIGGER*1;
    else if(trigger_type == 1)
      command = G10_CALIB_TRIGGER*0 | G1_CALIB_TRIGGER*0 | LED_ON*0 | GENE_100HZ*0 | TP_TRIGGER*1 | GENE_TRIGGER*0;
    else if(trigger_type == 3)
      command = G10_CALIB_TRIGGER*0 | G1_CALIB_TRIGGER*1 | LED_ON*0 | GENE_100HZ*0 | TP_TRIGGER*0 | GENE_TRIGGER*1;
    else if(trigger_type == 4)
      command = G10_CALIB_TRIGGER*1 | G1_CALIB_TRIGGER*0 | LED_ON*0 | GENE_100HZ*0 | TP_TRIGGER*0 | GENE_TRIGGER*1;

    if(debug>0)printf("Send trigger with command : 0x%8.8x\n",command);
// Read base address and send trigger
    free_mem = hw.getNode("CAP_FREE").read();
    hw.getNode("FW_VER").write(command);
    hw.dispatch();
    if(debug>0)printf("Free memory : 0x%8.8x\n",free_mem.value());
// Read new address and wait for DAQ completion
    address = hw.getNode("CAP_ADDRESS").read();
    hw.dispatch();
    if(debug>0)printf("Start address : 0x%8.8x\n",address.value());
    int new_address=address.value()>>16;
    int loc_address=new_address;
    int nretry=0;
    while(((loc_address-old_address)%max_address) != nsample_DAQ+1 && nretry<100)
    {
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      new_address=address.value()>>16;
      loc_address=new_address;
      if(new_address<old_address)loc_address+=max_address;
      nretry++;
      if(debug>0)printf("ongoing R/W addresses    : old %d, new %d add 0x%8.8x\n", old_address, new_address,address.value());
    }
    if(nretry==100)
    {
      printf("Stop waiting for sample capture after %d retries\n",nretry);
      printf("R/W addresses    : old %8.8x, new %8.8x add 0x%8.8x\n", old_address, new_address, address.value());
      error=1;
    }
    old_address=new_address;
    if(old_address==max_address-1)old_address=-1;
    if(debug>0)printf("R/W addresses    : old %8.8x, new %8.8x add 0x%8.8x\n", old_address, new_address, address.value());
  }
  else
  {
    int nretry=0;
// Wait for external trigger to fill memory :
    free_mem = hw.getNode("CAP_FREE").read();
    address = hw.getNode("CAP_ADDRESS").read();
    hw.dispatch();
    while((free_mem.value()==max_address-1) && nretry<100)
    {
      free_mem = hw.getNode("CAP_FREE").read();
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      //printf("address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
      if(debug>0)printf("address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
      usleep(1000);
      nretry++;
    }
    if(nretry==100)
    {
      printf("Stop waiting for external trigger after %d retries\n",nretry);
      printf("R/W addresses    : add 0x%8.8x\n",address.value());
      error=1;
    }
  }
  mem.clear();

// Read event samples from FPGA
  n_word=(nsample_DAQ+1)*3;     // 3*32 bits words per sample to get the 5 channels data n G10
  n_transfer=n_word/(MAX_PAYLOAD/4); // Max ethernet packet = 1536 bytes, max user payload = 1380 bytes
  n_last=n_word-n_transfer*(MAX_PAYLOAD/4);
  if(n_transfer>n_transfer_max)
  {
    printf("Error : ask for too many samples. Number of transfer %d greater than allowed max value %d\n",n_transfer, n_transfer_max);
    error=1;
  }
  for(int itrans=0; itrans<n_transfer; itrans++)
  {
    block = hw.getNode ("CAP_DATA").readBlock(MAX_PAYLOAD/4);
    hw.dispatch();
    for(int is=0; is<MAX_PAYLOAD/4; is++)mem.push_back(block[is]);
  }
  block = hw.getNode ("CAP_DATA").readBlock(n_last);
  address = hw.getNode("CAP_ADDRESS").read();
  free_mem = hw.getNode("CAP_FREE").read();
  hw.dispatch();
  if(debug>0)printf("After reading address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
  for(int is=0; is<n_last; is++)mem.push_back(block[is]);
  mem.valid(true);

  if(debug>0)
  {
    printf("%8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",
        mem[0],mem[1],mem[2],mem[3],mem[4],mem[5],mem[6],mem[7],mem[8],mem[9]);
    printf("%8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",
        mem[10],mem[11],mem[12],mem[13],mem[14],mem[15],mem[16],mem[17],mem[18],mem[19]);
    printf("%8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",
        mem[20],mem[21],mem[22],mem[23],mem[24],mem[25],mem[26],mem[27],mem[28],mem[29]);
    printf("%.4d %.4d %.4d %.4d %.4d %.4d %.4d %.4d\n",
        mem[2]&0x3fff,mem[3]&0x3fff,mem[4]&0x3fff,mem[5]&0x3fff,mem[6]&0x3fff,mem[7]&0x3fff,mem[8]&0x3fff,mem[9]&0x3fff);
    printf("%.4d %.4d %.4d %.4d %.4d %.4d %.4d %.4d\n",
        (mem[2]>>14)&0x3ff,(mem[3]>>14)&0x3fff,(mem[4]>>14)&0x3fff,(mem[5]>>14)&0x3fff,
        (mem[6]>>14)&0x3ff,(mem[7]>>14)&0x3fff,(mem[8]>>14)&0x3fff,(mem[9]>>14)&0x3fff);
  }

  unsigned long int t1=0, t2=0, t3=0, t4=0, t5=0;
// First sample should have bit 70 at 1
  if((mem[0] >> 31) != 1)
  {
    printf("Sample 0 not a header : %8.8x\n",mem[0]);
    error=1;
  }
  t1= mem[0]     &0xFFFF;
  t2= mem[1]     &0xFFFF;
  t3=(mem[1]>>16)&0xFFFF;
  t4= mem[2]     &0xFFFF;
  t5=(mem[2]>>16)&0x00FF;
  unsigned long int timestamp=(t5<<56)+(t4<<42)+(t3<<28)+(t2<<14)+t1;

  if(debug>0)
  { 
    printf("timestamp : %8.8x %8.8x %8.8x\n",mem[2],mem[1],mem[0]);
    printf("timestamp : %ld %4.4lx %4.4lx %4.4lx %4.4lx %4.4lx\n",timestamp,t5,t4,t3,t2,t1);
  }

  Double_t loc_rms[5]={0.,0.,0.,0.,0.};
  Double_t loc_ave[5]={0.,0.,0.,0.,0.};
  Double_t loc_ped[5]={0.,0.,0.,0.,0.};
  Double_t dv=1750./16384.; // 14 bits on 1.75V
  Double_t dt=6.25;         // 160 MHz clock
  if(use_mgpa>0)
  {
    dv=1780./4096;
    dt=25.; //40 MHz clock
  }
  for(Int_t ich=0; ich<5; ich++)
  {
    loc_ped[ich]=0.;
    loc_ave[ich]=0.;
    loc_rms[ich]=0.;
  }
  for(int isample=0; isample<nsample; isample++)
  {
    int j;
    j=(isample+1)*3;
    if(use_mgpa>0)j=(isample*4+2)*3;
    event[0][isample]= mem[j]       &0x3FFF;
    event[1][isample]= mem[j+1]     &0x3FFF;
    event[2][isample]=(mem[j+1]>>16)&0x3FFF;
    event[3][isample]= mem[j+2]     &0x3FFF;
    event[4][isample]=(mem[j+2]>>16)&0x3FFF;
    if(use_mgpa>0)
    {
      Int_t gain=(event[0][isample]>>12)&0x3;
      event[0][isample]&=0xFFF;
      if(gain==0)printf("Strange gain : %d\n",gain);
      if(gain==2)event[0][isample]*=2;
      if(gain==3)event[0][isample]*=12;
      gain=(event[1][isample]>>12)&0x3;
      event[1][isample]&=0xFFF;
      if(gain==0)printf("Strange gain : %d\n",gain);
      if(gain==2)event[1][isample]*=2;
      if(gain==3)event[1][isample]*=12;
      gain=(event[2][isample]>>12)&0x3;
      event[2][isample]&=0xFFF;
      if(gain==0)printf("Strange gain : %d\n",gain);
      if(gain==2)event[2][isample]*=2;
      if(gain==3)event[2][isample]*=12;
      gain=(event[3][isample]>>12)&0x3;
      event[3][isample]&=0xFFF;
      if(gain==0)printf("Strange gain : %d\n",gain);
      if(gain==2)event[3][isample]*=2;
      if(gain==3)event[3][isample]*=12;
      gain=(event[4][isample]>>12)&0x3;
      event[4][isample]&=0xFFF;
      if(gain==0)printf("Strange gain : %d\n",gain);
      if(gain==2)event[4][isample]*=2;
      if(gain==3)event[4][isample]*=12;
    }
    fevent[0][isample]=double(event[0][isample]);
    fevent[1][isample]=double(event[1][isample]);
    fevent[2][isample]=double(event[2][isample]);
    fevent[3][isample]=double(event[3][isample]);
    fevent[4][isample]=double(event[4][isample]);
    for(Int_t ich=0; ich<5; ich++)
    {
      if(isample<4)loc_ped[ich]+=fevent[ich][isample]*dv;
      loc_ave[ich]+=fevent[ich][isample]*dv;
      loc_rms[ich]+=fevent[ich][isample]*dv*fevent[ich][isample]*dv;
    }
  }
  for(Int_t ich=0; ich<5; ich++)
  {
    loc_ped[ich]/=4.;
    loc_ave[ich]/=nsample;
    loc_rms[ich]/=nsample;
    loc_rms[ich]=loc_rms[ich]-loc_ave[ich]*loc_ave[ich];
    if(cdraw!=NULL)printf("ch %d : ped= %.2f mV, ave= %.2f mV, rms= %.2f uV\n",ich,loc_ped[ich], loc_ave[ich], sqrt(loc_rms[ich])*1000.);
  }

  if(debug>0 || trigger_type !=trigger_type_old || cdraw!=NULL)
  {
    for(int ich=0; ich<N_CATIA; ich++)
    {
      tg[ich]->Clear();
      tg[ich]->Set(0);
      for(int isample=0; isample<nsample; isample++)
      {
        tg[ich]->SetPoint(isample,dt*isample,fevent[ich][isample]);
      }
      if(cdraw!=NULL)
      {
        cdraw->cd(ich+1);
        tg[ich]->Draw("alp");
      }
    }
    cdraw->Update();
  }
  //if(debug>0)
  //{
  //  system("stty raw");
  //  char cdum=getchar();
  //  system("stty -raw");
  //  if(cdum=='q')exit(-1);
  //}

  trigger_type_old=trigger_type;
  ievt++;
  if(error==0)
    return timestamp;
  else
    return -1;
}

Int_t main ( Int_t argc,char* argv[] )
{
  Int_t ngood_event=0;
  Int_t soft_reset, full_reset, command, vice_ctrl;
  ValWord<uint32_t> address;
  ValWord<uint32_t> data[3*NSAMPLE_MAX];
  //ValVector< uint32_t > mem[256];
  ValVector< uint32_t > block,mem;
  short int *event[25];
  double *fevent[25];
  for(Int_t ich=0; ich<25; ich++)
  {
    event[ich]=(short int *)malloc(NSAMPLE_MAX*sizeof(short int));
    fevent[ich]=(double*)malloc(NSAMPLE_MAX*sizeof(double));
  }
  double dv=1750./16384.; // 14 bits on 1.75V
  double dt=6.25;         // 160 MHz clock
  Int_t debug=0;
// Define defaults for laser runs :
  Int_t sw_DAQ_delay=SW_DAQ_DELAY;
  Int_t hw_DAQ_delay=HW_DAQ_DELAY;
  Int_t nevent=0;
  Int_t nsample=-1;
  Int_t trigger_type=0;         // pedestal by default
  Int_t invert_FE_trigger=0;    // Don't invert signal by default
  Int_t soft_trigger     =0;    // external trigger by default 
  Int_t self_trigger     =0;    // No self trigger 
  Int_t self_trigger_mode=0;
  Int_t self_trigger_threshold=0;
  Int_t self_trigger_mask=0;    // don't trig on any channels amplitude
  Int_t trigger_phase=0;        // For MGPA, choose the phase between 40 MHz and 160 MHz clock to generate trigger
  Int_t ADC_calib_mode = 0;     // Put (1) or not (0) the outputs of CATIA in ADC_calib_mode (outputs near VCM)
  Int_t seq_clock_phase    = 0; // sequence clock phase by steps of 45 degrees
  Int_t IO_clock_phase;         // Capture clock phase by steps of 45 degrees for CATIA test board
  Int_t IO_clock_phase_1   = 5; // Capture clock phase by steps of 45 degrees for VFE
  Int_t IO_clock_phase_2   = 7; // Capture clock phase by steps of 45 degrees for CATIA test board
  Int_t IO_clock_phase_3   = 4; // Capture clock phase by steps of 45 degrees for MGPA VFE
  Int_t reg_clock_phase    = 0; // Shift register clock phase by steps of 45 degrees
  Int_t mem_clock_phase    = 0; // memory clock phase by steps of 45 degrees
  Int_t clock_phase        = 0;
  Int_t sel_calib_pulse    = 1; // Default calib pulse enable. Otherwise High_Z
  Int_t n_calib=-1;
  Int_t calib_gain=0;
  Int_t calib_step=-1;
  Int_t calib_level=-1;
  Int_t calib_width=-1;
  Int_t calib_delay=50;
  Int_t n_vfe=0;
  Int_t n_channel=5;
  Int_t vfe[MAX_VFE];
  char cdum, output_file[256];
  Int_t tune_delay=1;
  Int_t delay_val[5]={20,22,14,14,15}; // Number of delay taps to get a stable R/O with ADCs (250 MHz)
  //Int_t delay_val[5]={0,0,0,0,0}; // Number of delay taps to get a stable R/O with ADCs (250 MHz)
  //Int_t delay_val[5]={20,10,0,0,0}; // Number of delay taps to get a stable R/O with ADCs (250 MHz)

// ADC settings if any :
  Int_t ADC_reg=-1;   // ADC register to write in.
  Int_t ADC_data=-1;  // data to write in ADC register.
  Int_t has_ADC=0;        // Put 1 here if you want to read ADCs, if there are any.
  Int_t negate_data=0;    // Don't negate the ADC output by default (positive signal from 0 not to disturb people)
  Int_t signed_data=0;    // Don't use 2's complement. Use normal binary order (0...16383 full scale)
  Int_t input_span=0;     // Use default ADC input span (1.75V) can be tuned from 1.383 V to 2.087 V by steps of 0.022V
                        // this is 2's complemented on 5 bits. So from -16 to +15 -> 0x10..0x1f 0x00 0x01..0x0f -> 16..31 0 1..15
  Int_t output_test=0;    // Put ADC outputs in test mode (0=normal conversion values, 0xF=ramp)
  Int_t clock_divide=0;   // Put ADC clock division (0=take FE clock, 1=divide by 2)

// CATIA settings if reuested
  Int_t n_catia=0;
  Int_t n_mgpa=0;
  Int_t MGPA_nums     =-1; // MGPA number to address (xyzuv) -1=no MGPA)
  Int_t MGPA_num[5]    ={-1,-1,-1,-1,-1}; // MGPA number to address
  Int_t CATIA_num[5]   ={-1,-1,-1,-1,-1}; // CATIA number to address
  Int_t CATIA_nums     =-1; // CATIA number to address (xyzuv) -1=no CATIA)
  Int_t CATIA_data     =-1; // Data to write in CATIA register
  Int_t CATIA_reg      =-1; // Register to write in with SPI or I2C protocol
  Int_t I2C_dir        =-1; // read(1)/write(0) with I2C protocol, -1=don't use I2C
  Int_t SPI_dir        =-1; // read(1)/write(0) with SPI protocol, -1=don't use SPI
  Int_t Reg1_def       = 0x2;    // Default content of Register 1
  Int_t Reg3_def       = 0x1087; // Default content of Register 3 : 2.5V, LPF35, 400 Ohms, ped mid scale
  Int_t Reg4_def       = 0x1000; // Default content of Register 4 : DAC1 0, DAC1 ON, DAC2 OFF, no copy
  Int_t Reg5_def       = 0x0000; // Default content of Register 5 : DAC2 0
  Int_t Reg6_def       = 0x0000; // Default content of Register 6 : Vreg OFF, Rconv=2471, TIA_dummy ON, TP OFF
  Int_t average        = 400;
  Int_t XADC_reg, XADC_Temp, XADC_Vdac, XADC_Vref;


  TProfile *pshape[MAX_STEP][25];
  sprintf(output_file,"");
  if(argc==1)
  {
    printf("Please run with -h option to get some help !\n");
    exit(-1);
  }

  for(int k=1; k<argc; k++) 
  {    
    if(strcmp( argv[k], "-dbg") == 0)
    {    
      sscanf( argv[++k], "%d", &debug );
      continue;
    }    
    else if(strcmp( argv[k], "-nevt") == 0)
    {    
      sscanf( argv[++k], "%d", &nevent );
      continue;
    }
    else if(strcmp(argv[k],"-nsample") == 0)
    {    
      sscanf( argv[++k], "%d", &nsample );
      continue;
    }    
    else if(strcmp(argv[k],"-soft_trigger") == 0)
    {    
// soft_trigger
// 0 : Use external trigger (GPIO)
// 1 : Generate trigger from software (1 written in FW register 
      sscanf( argv[++k], "%d", &soft_trigger );
      continue;
    }    
    else if(strcmp(argv[k],"-trigger_type") == 0)
    {    
// trigger_type
// 0 : pedestal
// 1 : calibration
// 2 : laser
// 3 : Calibrated TP for G1 (1.2 V on 1 kOhm)
// 4 : Calibrated TP for G10 (1.2 V on 10 kOhm)
      sscanf( argv[++k], "%d", &trigger_type );
      continue;
    }
    else if(strcmp(argv[k],"-invert_FE_trigger") == 0)
    {    
      sscanf( argv[++k], "%d", &invert_FE_trigger );
      continue;
    }
    else if(strcmp(argv[k],"-self_trigger") == 0)
    {    
// self_trigger
// 0 : Don't generate trigger from data themselves
// 1 : Generate trigger if any data > self_trigger_threshold
      sscanf( argv[++k], "%d", &self_trigger );
      continue;
    }
    else if(strcmp(argv[k],"-self_trigger_mode") == 0)
    {    
// self_trigger_mode : 0 = trig on absolute level, 1 = trig on step level
      sscanf( argv[++k], "%d", &self_trigger_mode );
      continue;
    }
    else if(strcmp(argv[k],"-self_trigger_threshold") == 0)
    {    
// self_trigger_threshold in ADC counts
      sscanf( argv[++k], "%d", &self_trigger_threshold );
      continue;
    }
    else if(strcmp(argv[k],"-self_trigger_mask") == 0)
    {    
// channel mask to generate self trigger lsb=ch0 ... msb=ch4
      sscanf( argv[++k], "%x", &self_trigger_mask );
      continue;
    }
    else if(strcmp(argv[k],"-trigger_phase") == 0)
    {    
      sscanf( argv[++k], "%d", &trigger_phase );
      trigger_phase&=0x3;
      continue;
    }
    else if(strcmp(argv[k],"-vfe") == 0)
    {
      sscanf( argv[++k], "%d", &vfe[n_vfe++] );
      continue;
    }    
    else if(strcmp(argv[k],"-sel_calib_pulse") == 0)
    {    
      sscanf( argv[++k], "%d", &sel_calib_pulse );
      continue;
    }    
    else if(strcmp(argv[k],"-calib_level") == 0)
    {    
// DAC_value 0 ... 4095
      sscanf( argv[++k], "%d", &calib_level );
      continue;
    }    
    else if(strcmp(argv[k],"-calib_width") == 0)
    {    
// Calib trigger width 0 ... 65532
      sscanf( argv[++k], "%d", &calib_width );
      continue;
    }
    else if(strcmp(argv[k],"-calib_delay") == 0)
    {    
// DAQ delay for calib triggers : 0..65532
      sscanf( argv[++k], "%d", &calib_delay );
      continue;
    }    
    else if(strcmp(argv[k],"-sw_DAQ_delay") == 0)
    {    
// DAQ delay for SW triggers
      sscanf( argv[++k], "%d", &sw_DAQ_delay );
      continue;
    }    
    else if(strcmp(argv[k],"-hw_DAQ_delay") == 0)
    {    
// DAQ delay for HW triggers
      sscanf( argv[++k], "%d", &hw_DAQ_delay );
      continue;
    }    
    else if(strcmp(argv[k],"-n_calib") == 0)
    {    
// Number of calibration step for linearity study
      sscanf( argv[++k], "%d", &n_calib );
      continue;
    }    
    else if(strcmp(argv[k],"-calib_step") == 0)
    {    
// DAC step for linearity study
      sscanf( argv[++k], "%d", &calib_step );
      continue;
    }    
    else if(strcmp(argv[k],"-calib_gain") == 0)
    {    
// Rconv for current injection (0=2471 Ohms=G1, 1=272 Ohms=G10)
      sscanf( argv[++k], "%d", &calib_gain );
      calib_gain&=1;
      continue;
    }    
    else if(strcmp(argv[k],"-negate_data") == 0)
    {    
// Put ADC in invert mode
      sscanf( argv[++k], "%d", &negate_data );
      continue;
    }    
    else if(strcmp(argv[k],"-signed_data") == 0)
    {    
// Put ADC in 2's complement
      sscanf( argv[++k], "%d", &signed_data );
      continue;
    }    
    else if(strcmp(argv[k],"-input_span") == 0)
    {    
// Set ADC input SPAN from 0x1f - 0 - 0x0f
      sscanf( argv[++k], "%d", &input_span );
      continue;
    }    
    else if(strcmp(argv[k],"-output_test") == 0)
    {    
      sscanf( argv[++k], "%x", &output_test );
      continue;
    }    
    else if(strcmp(argv[k],"-clock_divide") == 0)
    {    
      sscanf( argv[++k], "%x", &clock_divide );
      continue;
    }    
    else if(strcmp(argv[k],"-ADC_reg") == 0)
    {    
      sscanf( argv[++k], "%x", &ADC_reg );
      continue;
    }    
    else if(strcmp(argv[k],"-ADC_data") == 0)
    {    
      sscanf( argv[++k], "%x", &ADC_data );
      continue;
    }    
    else if(strcmp(argv[k],"-MGPA_nums") == 0)
    {    
      sscanf( argv[++k], "%d", &MGPA_nums );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_nums") == 0)
    {    
      sscanf( argv[++k], "%d", &CATIA_nums );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_data") == 0)
    {    
      sscanf( argv[++k], "%x", &CATIA_data );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_reg") == 0)
    {    
      sscanf( argv[++k], "%d", &CATIA_reg );
      continue;
    }    
    else if(strcmp(argv[k],"-I2C_dir") == 0)
    {    
      sscanf( argv[++k], "%d", &I2C_dir );
      continue;
    }    
    else if(strcmp(argv[k],"-SPI_dir") == 0)
    {    
      sscanf( argv[++k], "%d", &SPI_dir );
      continue;
    }    
    else if(strcmp(argv[k],"-ADC_calib_mode") == 0)
    {    
      sscanf( argv[++k], "%d", &ADC_calib_mode );
      continue;
    }    
    else if(strcmp(argv[k],"-clock_phase") == 0)
    {    
      sscanf( argv[++k], "%d", &clock_phase );
      seq_clock_phase=(clock_phase>>0)&0x7;
      IO_clock_phase =(clock_phase>>4)&0x7;
      reg_clock_phase=(clock_phase>>8)&0x7;
      mem_clock_phase=(clock_phase>>12)&0x7;
      continue;
    }    
    else if(strcmp(argv[k],"-has_ADC") == 0)
    {    
      sscanf( argv[++k], "%d", &has_ADC );
      continue;
    }    
    else if(strcmp(argv[k],"-Reg1_def") == 0)
    {    
      sscanf( argv[++k], "%x", &Reg1_def );
      continue;
    }    
    else if(strcmp(argv[k],"-Reg3_def") == 0)
    {    
      sscanf( argv[++k], "%x", &Reg3_def );
      continue;
    }    
    else if(strcmp(argv[k],"-Reg4_def") == 0)
    {    
      sscanf( argv[++k], "%x", &Reg4_def );
      continue;
    }    
    else if(strcmp(argv[k],"-Reg5_def") == 0)
    {    
      sscanf( argv[++k], "%x", &Reg5_def );
      continue;
    }    
    else if(strcmp(argv[k],"-Reg6_def") == 0)
    {    
      sscanf( argv[++k], "%x", &Reg6_def );
      continue;
    }    
    else if(strcmp(argv[k],"-h") == 0)
    {
      printf("Start DAQ with : \n");
      printf("-dbg dbg_level            : Set debug level for this session [0]\n");
      printf("-vfe n                    : Do DAQ on VFE n, 100+n if MGPA board, 1000+n if slave board for trigger [4]\n");
      printf("-nevt n                   : Number of events to record  [1000]\n");
      printf("-nsample n                : Number of sample per event (max=28670 for VFE, 65534 for CATIA tests) [150]\n");
      printf("-trigger_type n           : 0=pedestal, 1=calibration, 2=laser, 3=G1 calib, 4=G10 calib [0]\n");
      printf("-invert_FE_trigger n      : invert (1) or not (0) trigger signal coming for FE board [0]\n");
      printf("-soft trigger n           : 0=externally triggered DAQ, 1=softwared triggered DAQ [0]\n");
      printf("-self_trigger n           : 1=use internal generated trigger if signal > threshold or take trigger from FE [0]\n");
      printf("-self_trigger_threshold n : minimal signal amplitude to generate self trigger [0]\n");
      printf("-self_trigger_mask x      : channel mask to generate self triggers, ch0=lsb    [0x1F]\n");
      printf("-calib_width n            : width of the calibration trigger sent to VFE [128]\n");
      printf("-calib_delay n            : delay between calibration trigger and DAQ start [0]\n");
      printf("-calib_level n            : DAC level to start linearity study (0..4095) [0]\n");
      printf("-sel_calib_pulse n        : Enable G1/G10 calibration pulses (1) or High Z (0) [1]\n");
      printf("-n_calib n                : number of calibration steps for linearity study [1]\n");
      printf("-calib_step n             : DAC step for linearity study [128]\n");
      printf("-calib_gain n             : Conversion gain for current pulse injection (0=2471 Ohms, 1=272 Ohms) [0]\n");
      printf("-negate_data 0/1          : Negate (1) or not (0) the converted data in the ADC [0]\n");
      printf("-signed_data 0/1          : Set ADC in normal binary mode (0) or 2's complement (1) [0]\n");
      printf("-input_span n             : Set ADC input span (0x10 = 1.383V, 0x1f=1.727V, 0=1.75V, 0x01=1.772V, 0x0f=2.087V) [0]\n");
      printf("-ADC_reg                  : ADC register to write in [-1]\n");
      printf("-ADC_data                 : data to write in ADC register [-1]\n");
      printf("-ADC_calib_mode           : Put (1) or not (0) CATIA in ADC_calib_mode (outputs near VCM) [0]\n");
      printf("-output_test              : Define output test mode of ADCs (1=midscale, 2=max, 3=min, F=ramp) [0]\n");
      printf("-MGPA_nums                : MGPA numbers to read (xyzuv) [-1=no mgpa]\n");
      printf("-CATIA_nums               : CATIA numbers to address (xyzuv) [-1=no catia]\n");
      printf("-CATIA_data               : data to write in CATIA register [-1]\n");
      printf("-CATIA_reg                : CATIA register to read from/write in with I2C or SPI (1 to 6) [-1]\n");
      printf("                          : 1 : Slow Control reg [R/W]\n");
      printf("                          : 2 : SEU error counter reg [R]\n");
      printf("                          : 3 : TIA reg [R/W]\n");
      printf("                          : 4 : Inj DAC1 reg [R/W]\n");
      printf("                          : 5 : Inj DAC2 reg [R/W]\n");
      printf("                          : 6 : Inj Ctl reg [R/W]\n");
      printf("-Reg1_def                 : default content of register 1 [0x2]\n");
      printf("-Reg3_def                 : default content of register 3 [0x1087]\n");
      printf("-Reg4_def                 : default content of register 4 [0x1000]\n");
      printf("-Reg5_def                 : default content of register 5 [0x0000]\n");
      printf("-Reg6_def                 : default content of register 6 [0x0000]\n");
      printf("-SPI_dir                  : Read (1) or write (0) with SPI protocol. -1=don't use\n");
      printf("-I2C_dir                  : Read (2) + Write (1) with I2C protocol. -1=don't use\n");
      printf("-has_ADC n                : Debug analog board (0) or board with ADCs (1) or CATIA_test board (2) [0]\n");
      exit(-1);
    }
    else
    {
      printf("---------------------------------------------------\n");
      printf("Option %s not defined !, stop here.\n",argv[k]);
      printf("---------------------------------------------------\n");
      exit(-99);
    }
  }
  if(n_vfe==0)
  {
    n_vfe=1;
    vfe[0]=4;
  }
  n_mgpa=0;
  while(MGPA_nums>0)
  {
    MGPA_num[n_mgpa]=MGPA_nums%10;
    MGPA_nums/=10;
    n_mgpa++;
  }
  n_catia=0;
  while(CATIA_nums>0)
  {
    CATIA_num[n_catia]=CATIA_nums%10;
    CATIA_nums/=10;
    n_catia++;
  }
  printf("Will address %d catias : ",n_catia);
  for(Int_t icatia=0; icatia<n_catia; icatia++) printf("%d ",CATIA_num[icatia]);
  printf("\n");
  if(n_mgpa>0)
  {
    printf("Will address %d mgpas : ",n_mgpa);
    for(Int_t imgpa=0; imgpa<n_mgpa; imgpa++) printf("%d ",MGPA_num[imgpa]);
    printf("\n");
  }

  if(calib_step<0)calib_step=128;
  //if(calib_level<0)calib_level=0;
  if(calib_width<0)calib_width=200;
  if(calib_delay<0)calib_delay=50;

  nsample_max=NSAMPLE_MAX;
  if(has_ADC!=2)
  {
    nsample_max=28672;
  }
  if(trigger_type==0)
  {
    if(nsample<0)nsample=nsample_max-2;
    if(n_calib<0)n_calib=1;
    if(nevent==0)nevent=500;
    soft_trigger=1;
  }
  if(trigger_type==1)
  {
    if(nsample<0)nsample=300;
    if(n_calib<0)n_calib=32;
    Reg6_def=(Reg6_def&0x00f0) | 0x000b | (calib_gain<<2); // Rconv 2471 or 272, TIA_dummy ON, TP ON, Vreg ON
    if(nevent==0)nevent=100;
    soft_trigger=1;
  }
  if(trigger_type==2)
  {
    if(nsample<0)nsample=300;
    if(n_calib<0)n_calib=1;
    if(self_trigger_threshold==0)self_trigger_threshold=200;
    //self_trigger=1;
    //if(self_trigger_mask==0)self_trigger_mask=0x1f;
    soft_trigger=0;
    if(nevent==0)nevent=10000;
  }
  if(trigger_type==3 || trigger_type==4)
  {
    if(nsample<0)nsample=300;
    if(n_calib<0)n_calib=1;
    soft_trigger=1;
    if(nevent==0)nevent=1000;
  }
  if(nsample>nsample_max-2)nsample=nsample_max-2;
  int max_address=nsample_max;
  if(has_ADC==2)n_channel=2;
  Int_t nsample_DAQ=nsample;
  if(n_mgpa>0)
  {
    //hw_DAQ_delay=24;
    dt=25.; // 40 MHz sampling
    dv=1780./4096.; // ~400 uV step with MGPA boards
    nsample_DAQ=nsample*4;
    if(nsample_DAQ>nsample_max-2)
    {
      nsample_DAQ=nsample_max-2;
      nsample=nsample_DAQ/4;
      nsample_DAQ=nsample*4;
    }
  }

  //if(trigger_type==0 || trigger_type==1)
  //{
  //  soft_trigger=1;
  //  self_trigger=0;
  //}
  //else if(trigger_type==2)
  //{
  //  soft_trigger=0;
  //  self_trigger=1;
  //  self_trigger_threshold=14000;
  //}
  printf("Start DAQ with %d cards : ",n_vfe);
  for(int i=0; i<n_vfe; i++)printf("%d, ",vfe[i]);
  printf("\n");
  printf("Parameters : \n");
  if(has_ADC>=1)
  {
    printf("Read ADCs for :\n");
    printf("  %d events \n",nevent);
    printf("  %d samples \n",nsample);
    printf("  trigger type  : %d (0=pedestal, 1=calibration, 2=laser, 3=G1 calib, 4=G10 calib)\n",trigger_type);
    printf("  soft trigger  : %d (0=externally triggered DAQ, 1=softwared triggered DAQ)\n",soft_trigger);
    printf("  self trigger  : %d (1=internal generated trigger if signal > threshold)\n",self_trigger);
    printf("  threshold     : %d (minimal signal amplitude to generate self trigger)\n",self_trigger_threshold);
    printf("  mask          : 0x%x (channel mask to generate self triggers)\n",self_trigger_mask);
  }
  IO_clock_phase=IO_clock_phase_3;
  XADC_Temp=0x14;
  XADC_Vref=-1;
  XADC_Vdac=-1;
  if(has_ADC==1)
    IO_clock_phase=IO_clock_phase_1;
  else if(has_ADC==2)
  {
    IO_clock_phase=IO_clock_phase_2;
    XADC_Temp=0x11;
    XADC_Vref=0x10;
    XADC_Vdac=0x1c;
  }
  if(trigger_type==1)
  {
    printf("Generate calibration triggers :\n");
    printf("  %d events \n",nevent);
    printf("  calib_width   : %d (width of the calibration trigger sent to VFE)\n",calib_width);
    printf("  calib_delay   : %d (delay between calibration trigger and DAQ start)\n",calib_delay);
    printf("  n_calib       : %d (number of calibration steps for linearity study)\n",n_calib);
    printf("  calib_step    : %d (DAC step for linearity study)\n",calib_step);
  }
  if(n_calib==0)n_calib=1;

  char hname[80];
  int loc_argc=1;
  char *loc_argv[10];
  for(int i=0; i<10; i++)loc_argv[i]=(char *)malloc(132*sizeof(char));
  sprintf(loc_argv[0],"test");
  TApplication *Root_App=new TApplication("test", &loc_argc, loc_argv);
  for(Int_t ivfe=0; ivfe<n_vfe; ivfe++)
  {
    sprintf(hname,"VFE_%d",ivfe);
    if(has_ADC==1)
    {
      c1[ivfe]=new TCanvas(hname,hname,1000,0,800.,1000.);
      c1[ivfe]->Divide(2,3);
    }
    else
    {
      c1[ivfe]=new TCanvas(hname, hname,920,0,1000.,600.);
      c1[ivfe]->Divide(1,2);
    }
    c1[ivfe]->Update();
  }
  TGraph *tg[25];
  TH1D *hmean[25], *hrms[25];
  double rms[25];
  for(Int_t ivfe=0; ivfe<n_vfe; ivfe++)
  {
    for(int ich=0; ich<5; ich++)
    {
      tg[ivfe*5+ich] = new TGraph();
      tg[ivfe*5+ich]->SetMarkerStyle(20);
      tg[ivfe*5+ich]->SetMarkerSize(0.5);
      sprintf(hname,"mean_vfe%d_ch%d",ivfe,ich);
      hmean[ivfe*5+ich]=new TH1D(hname,hname,100,150.,250.);
      sprintf(hname,"rms_vfe%d_ch%d",ivfe,ich);
      hrms[ivfe*5+ich]=new TH1D(hname,hname,200,0.,2.);
    }
    tg[ivfe*5+0]->SetLineColor(kRed);
    tg[ivfe*5+1]->SetLineColor(kYellow);
    tg[ivfe*5+2]->SetLineColor(kBlue);
    tg[ivfe*5+3]->SetLineColor(kMagenta);
    tg[ivfe*5+4]->SetLineColor(kCyan);
    tg[ivfe*5+0]->SetMarkerColor(kRed);
    tg[ivfe*5+1]->SetMarkerColor(kYellow);
    tg[ivfe*5+2]->SetMarkerColor(kBlue);
    tg[ivfe*5+3]->SetMarkerColor(kMagenta);
    tg[ivfe*5+4]->SetMarkerColor(kCyan);
  }
  int dac_val=0;
  if(calib_level>0)dac_val=calib_level;

  for(int istep=0; istep<n_calib; istep++)
  {
    for(int ich=0; ich<5*n_vfe; ich++)
    {
      char pname[132];
      sprintf(pname,"ch_%d_step_%d_%d",ich,istep,dac_val);
      pshape[istep][ich]=new TProfile(pname,pname,nsample,0.,dt*nsample);
    }
    dac_val+=calib_step;
  }

  ConnectionManager manager ( "file:///data/cms/ecal/fe/fead_v2/software/xml/VICE/connection_file.xml" );
  std::vector<uhal::HwInterface> devices;
  for(Int_t ivfe=0; ivfe<n_vfe; ivfe++)
  {
    char vice_str[80];
    sprintf(vice_str,"vice.udp.%d",vfe[ivfe]%100); // Board with MGPA = 100+n, slave board for trigger generation : 1000+n
    devices.push_back(manager.getDevice( vice_str ));
  }

  ValWord<uint32_t> free_mem;
  ValWord<uint32_t> trig_reg;
  ValWord<uint32_t> delays;
  int old_address[MAX_VFE], old_read_address[MAX_VFE];

// Reset board
  for(Int_t ivfe=0; ivfe<n_vfe; ivfe++)
  {
    uhal::HwInterface hw=devices[ivfe];
    hw.getNode("VICE_CTRL").write((vice_ctrl&0xfffffffe) | RESET*1);
    //hw.getNode("VICE_CTRL").write((vice_ctrl&0xfffffffe) | RESET*0);
    hw.dispatch();
  }
  usleep(500);
  //usleep(50);
  //}
  
  unsigned int ireg,device_number,val;
// Program ADCs
  printf("Program_ADCs...\n");
  for(Int_t ivfe=0; ivfe<n_vfe; ivfe++)
  {
    if((vfe[ivfe]%1000)>100)continue; // Skip if MGPA board

    uhal::HwInterface hw=devices[ivfe];
// Put ADC in two's complement mode (if no pedestal bias) and invert de conversion result
    negate_data=(negate_data&1)<<2;
    signed_data&=3;
    command=ADC_WRITE | ADC_OMODE_REG | negate_data | signed_data;
    printf("Put ADC coding : 0x%x\n",command);
    hw.getNode("DTU_CTRL").write(command);
    hw.dispatch();

// Set ADC input range (default=1.75V)
    input_span&=0x1F;
    command=ADC_WRITE | ADC_ISPAN_REG | input_span;
    printf("Set ADC input span range : 0x%x\n",command);
    hw.getNode("DTU_CTRL").write(command);
    hw.dispatch();

// Set ADC output test mode
    output_test&=0x0F;
    command=ADC_WRITE | ADC_OTEST_REG | output_test;
    printf("Set ADC output test mode : 0x%x\n",command);
    hw.getNode("DTU_CTRL").write(command);
    hw.dispatch();

// Divide clock by 2.
    clock_divide&=0x0F;
    command=ADC_WRITE | ADC_CLK_DIVIDE | clock_divide;
    printf("Set ADC clk division to : 0x%x\n",command);
    hw.getNode("DTU_CTRL").write(command);
    hw.dispatch();

    if(ADC_reg>=0)
    {
      command=ADC_WRITE | (ADC_reg<<16) | ADC_data;
      printf("Set ADC register 0x%x with data 0x%x\n",ADC_reg, ADC_data);
      hw.getNode("DTU_CTRL").write(command);
      hw.dispatch();
    }

// Calibration trigger setting :
    command=(calib_delay<<16) | (calib_width&0xffff);
    printf("Calibration trigger with %d clocks width and %d clocks delay : %x\n",calib_width,calib_delay,command);
    hw.getNode("CALIB_CTRL").write(command);
    hw.dispatch();

// Set delay tap value for each ADC
    for(int i=0; i<5; i++)
    {
      command=DELAY_RESET*1;
      hw.getNode("DELAY_CTRL").write(command);
      hw.dispatch();
      usleep(1000);
    }
    printf("Get lock status of IDELAY input stages\n");
    ValWord<uint32_t> reg = hw.getNode("DELAY_CTRL").read();
    hw.dispatch();
    printf("Delay lock value : 0x%x\n",reg.value());

    for(int iADC=0; iADC<n_channel; iADC++)
    {
      command=0;
      if(has_ADC==1) command=DELAY_RESET*0 | DELAY_INCREASE*1 | (1<<iADC);
      if(has_ADC==2) command=DELAY_RESET*0 | DELAY_INCREASE*1 | (0x7F<<(iADC*7));
      for(Int_t itap=0; itap<delay_val[iADC]; itap++)
      {
        hw.getNode("DELAY_CTRL").write(command);
        hw.dispatch();
      }
      printf("Get lock status of IDELAY input stages\n");
      ValWord<uint32_t> reg = hw.getNode("DELAY_CTRL").read();
      hw.dispatch();
      printf("Value read : 0x%x\n",reg.value());
    }
  }

// Init stage :
  printf("Init stage...\n");
//  for(auto & hw : devices)
  for(Int_t ivfe=0; ivfe<n_vfe; ivfe++)
  {
    uhal::HwInterface hw=devices[ivfe];

  // Read FW version to check :
    ValWord<uint32_t> reg = hw.getNode("FW_VER").read();
  // Switch to triggered mode + external trigger :
    vice_ctrl=  (TRIGGER_PHASE         *trigger_phase)                   |
                (ADC_CALIB_MODE        *(ADC_calib_mode&1))              |
                (SEL_CALIB_PULSE       *sel_calib_pulse)                 |
                (SELF_TRIGGER_MODE     *self_trigger_mode)               |
                (SELF_TRIGGER_MASK     *(self_trigger_mask&0x1F))        |
                (SELF_TRIGGER_THRESHOLD*(self_trigger_threshold&0x3FFF)) |
              //(CLOCK_PHASE           *(clock_phase&0x7))               |
                 SELF_TRIGGER          *self_trigger                     |
                 SOFT_TRIGGER          *soft_trigger                     |
                 TRIGGER_MODE          *1                                | // Always DAQ on trigger
                 RESET                 *0;
    //if(vfe[ivfe]<1000)
    //{
      printf("Set master VFE\n");
      hw.getNode("VICE_CTRL").write(vice_ctrl); // Master trigger board
    //}
    //else
    //  hw.getNode("VICE_CTRL").write(vice_ctrl&0xf83fffff); // Slave trigger board
  // Stop DAQ and ask for NSAMPLE per frame (+timestamp) :
    command = ((nsample_DAQ+1)<<16)+CAPTURE_STOP;
    hw.getNode("CAP_CTRL").write(command);
  // Add laser latency before catching data ~ 40 us
    hw.getNode("TRIG_DELAY").write((sw_DAQ_delay<<16)+hw_DAQ_delay);
  // Switch off FE-adapter LEDs
    command = INVERT_FE_TRIGGER*invert_FE_trigger | G10_CALIB_TRIGGER*0 | G1_CALIB_TRIGGER*0 | LED_ON*0 | GENE_100HZ*0 | TP_TRIGGER*0 | GENE_TRIGGER*0;
    hw.getNode("FW_VER").write(command);
    hw.dispatch();
  // Set the clock phases
    command = MEM_CLOCK_PHASE*mem_clock_phase+REG_CLOCK_PHASE*reg_clock_phase+IO_CLOCK_PHASE*IO_clock_phase+SEQ_CLOCK_PHASE*seq_clock_phase;
    printf("Set clock phases : 0x%x\n",command);
    hw.getNode("CLK_SETTING").write(command);
    hw.dispatch();

  // Read back delay values :
    delays=hw.getNode("TRIG_DELAY").read();
  // Read back the read/write base address
    address = hw.getNode("CAP_ADDRESS").read();
    free_mem = hw.getNode("CAP_FREE").read();
    trig_reg = hw.getNode("VICE_CTRL").read();
    hw.dispatch();

    printf("Firmware version      : %8.8x\n",reg.value());
    printf("Delays                : %8.8x\n",delays.value());
    printf("Initial R/W addresses : 0x%8.8x\n", address.value());
    printf("Free memory           : 0x%8.8x\n", free_mem.value());
    printf("Trigger mode          : 0x%8.8x, mask : %x\n", trig_reg.value(),(trig_reg.value()>>22)&0x1f);
    old_address[ivfe]=address>>16;
    if(old_address[ivfe]==max_address-1)old_address[ivfe]=-1;
    old_read_address[ivfe]=address&0xffff;
    if(old_read_address[ivfe]==max_address-1)old_read_address[ivfe]=-1;
  }

  unsigned long int timestamp=0;
  TTree *tdata=new TTree("data","data");
  tdata->Branch("timestamp",&timestamp,"timestamp/l");
  for(Int_t ivfe=0; ivfe<n_vfe; ivfe++)
  {
    for(int ich=0; ich<5; ich++)
    {
      char bname[80], btype[80];
      sprintf(bname,"ch%d",ivfe*5+ich);
      sprintf(btype,"ch%d[%d]/S",ivfe*5+ich,nsample);
      tdata->Branch(bname,event[ivfe*5+ich],btype);
    }
  }

// Program CATIA according to wishes :
// With SPI protocol
  if(SPI_dir>=0)
  {
//    for(auto & hw : devices)
    for(Int_t ivfe=0; ivfe<n_vfe; ivfe++)
    {
      if((vfe[ivfe]%1000)>100)continue; // Skip MGPA board
      uhal::HwInterface hw=devices[ivfe];

      for(Int_t icatia=0; icatia<n_catia; icatia++)
      {
        unsigned int loc_reg=0;
    // Address to decode on the VFE board (0..3 are used for the ADCs)
        if(CATIA_reg==1)loc_reg=8;
        if(CATIA_reg==3)loc_reg=4;
        if(CATIA_reg==4)loc_reg=5;
        if(CATIA_reg==5)loc_reg=6;
        if(CATIA_reg==6)loc_reg=7;
        loc_reg+=(CATIA_num[icatia]-1)*5;
    // Write CATIA register with SPI protocol (no readback)
        output_test= (loc_reg<<16) | (CATIA_data & 0xFFFF);
        command=I2C_RWb*0 | SPI_CATIA*1 | output_test;
        printf("SPI : set CATIA %d register %d with data %d. Command = 0x%x\n",CATIA_num[icatia], CATIA_reg,CATIA_data,command);
        hw.getNode("CATIA_CTRL").write(command);
        hw.dispatch();
      }
    }
  }

// Setup default values for CATIA:
  printf("Setup CATIAs...\n");
  unsigned int I2C_long=0;
//  for(auto & hw : devices)
  for(Int_t ivfe=0; ivfe<n_vfe; ivfe++)
  {
    uhal::HwInterface hw=devices[ivfe];
// Stop LVRB auto scan, not to genrate noise in CATIA during calibration
    printf("Stop LVRB auto scan mode\n");
    command=0;
    hw.getNode("VFE_CTRL").write(command);
    hw.dispatch();

    if((vfe[ivfe]%1000)<100)
    {
      printf("Load CATIA registers\n");
      for(Int_t icatia=0; icatia<n_catia; icatia++)
      {
        device_number=(CATIA_num[icatia]&0xF)<<3;
        device_number+=1000;
        ireg=1; // control register
        val=I2C_RW(hw, device_number, ireg, Reg1_def,0, 3, debug);
        ireg=3; // control register
// Gain 400 Ohm, Output stage for 2.5V, LPF35, 0 pedestal
        val=I2C_RW(hw, device_number, ireg, Reg3_def,1, 3, debug);
        ireg=4; // control register
// DAC1 0, DAC2 0, DAC1 ON, DAC2 OFF, DAC1 copied on DAC2
        val=I2C_RW(hw, device_number, ireg, Reg4_def,1, 3, debug);
        ireg=5; // control register
// DAC2 at 0 but OFF, so should not matter
        val=I2C_RW(hw, device_number, ireg, Reg5_def,1, 3, debug);
        ireg=6; // control register
// TIA dummy ON, Rconv 2471 (G10 scale), Vref ON (0xb) OFF (0x3), Injection in CATIA
        val=I2C_RW(hw, device_number, ireg, Reg6_def,0, 3, debug);

// Custom I2C settings
        if(I2C_dir>=0)
        {
          printf("Access CATIA with I2C :\n");
//        for(auto & hw : devices)
          for(Int_t ivfe=0; ivfe<n_vfe; ivfe++)
          {
            uhal::HwInterface hw=devices[ivfe];
            I2C_long=0;
            if(CATIA_reg==3 || CATIA_reg==4 || CATIA_reg==5)I2C_long=1;

            device_number=(CATIA_num[icatia]&0xF)<<3;
            device_number+=1000;
            val=I2C_RW(hw, device_number, CATIA_reg, CATIA_data,I2C_long, I2C_dir, debug);
            printf("Value read : 0x%x\n",val);
          }
        }
      }
    }
    else
    {
      printf("Load MGPA registers\n");
      for(Int_t imgpa=0; imgpa<n_mgpa; imgpa++)
      {
        device_number=0x78 | (MGPA_num[imgpa]&0x7);
        device_number+=1000;
// TP enable register
        Int_t MGPA_data=0;
        Int_t val0=I2C_RW(hw, device_number, 0, MGPA_data, 0, 3, debug);
// pedestal registers
        MGPA_data=0x40;
        Int_t val1=I2C_RW(hw, device_number, 2, MGPA_data, 0, 3, debug);
        Int_t val2=I2C_RW(hw, device_number, 4, MGPA_data, 0, 3, debug);
        Int_t val3=I2C_RW(hw, device_number, 6, MGPA_data, 0, 3, debug);
// spare
        MGPA_data=0xF0;
        Int_t val4=I2C_RW(hw, device_number, 8, MGPA_data, 0, 3, debug);
// TP amplitude
        MGPA_data=0x08;
        Int_t val5=I2C_RW(hw, device_number, 10, MGPA_data, 0, 3, debug);
        printf("MGPA TP registers (0, 5) and spare content (4) : 0x%x 0x%x 0x%x\n",val0&0xFF, val5&0xFF, val4&0xFF);
        printf("MGPA baseline register content : 0x%x, 0x%x, 0x%x\n",val1&0xFF,val2&0xFF,val3&0xFF);
      }
    }
  }

// Read Temperature and others analog voltages if requested
//  for(auto & hw : devices)
  for(Int_t ivfe=0; ivfe<n_vfe; ivfe++)
  {
    uhal::HwInterface hw=devices[ivfe];
    if((vfe[ivfe]%1000)>100)continue;

// Read XADC register 0x40 and set the requested average to 1 in XADC
    command=DRP_WRb*0 | (0x40<<16);
    hw.getNode("DRP_XADC").write(command);
    ValWord<uint32_t> ave  = hw.getNode("DRP_XADC").read();
    hw.dispatch();
    unsigned loc_ave=ave.value()&0xffff;
    printf("Old config register 0x40 content : %x\n",loc_ave);

    loc_ave=0x8000;
    command=DRP_WRb*1 | (0x40<<16) | loc_ave;
    hw.getNode("DRP_XADC").write(command);
    hw.dispatch();
    command=DRP_WRb*0 | (0x40<<16);
    hw.getNode("DRP_XADC").write(command);
    ave  = hw.getNode("DRP_XADC").read();
    hw.dispatch();
    loc_ave=ave.value()&0xffff;
    printf("New config register 0x40 content : %x\n",loc_ave);
    command=DRP_WRb*1 | (0x42<<16) | 0x0400;
    hw.getNode("DRP_XADC").write(command);
    hw.dispatch();
// Switch off temp output on all CATIAs :
    for(int icatia=0; icatia<n_catia; icatia++)
    {
      device_number=(CATIA_num[icatia]&0xF)<<3;
      device_number+=1000;
      ireg=1; // control register
      command=0x2;
      val=I2C_RW(hw, device_number, ireg, command,0, 3, debug);
    }

// Now, make the temp measurement on all catias, sequentially end for both current gain
    for(int XADC_meas=0; XADC_meas<4; XADC_meas++)
    {
      XADC_reg=0;
      if(XADC_meas==1)XADC_reg=XADC_Temp; // Temperature measurement
      if(XADC_Vref<0) continue;
      if(XADC_meas==2)XADC_reg=XADC_Vref; // CATIA Vref measurement (pin 22)
      if(XADC_meas==3)XADC_reg=XADC_Vdac; // CATIA Vdac measurement (pin 25)

      device_number=1000 + (CATIA_num[0]<<3);
      Int_t nmeas=1;
      if(XADC_meas==1)nmeas=2;
      if(XADC_meas==2)nmeas=15;
      if(XADC_meas==3)nmeas=33;
      for(int imeas=0; imeas<nmeas; imeas++)
      {
        int loc_meas=imeas;
        if(XADC_meas==1) // Temp measurements
        {
          ireg=1; // control register
          command= (imeas<<4) | 0xa;
          val=I2C_RW(hw, device_number, ireg, command,0, 3, debug);
          usleep(100000);
        }
        if(XADC_meas==2) // Scan Vref_reg values
        {
          ireg=6; // DAC control register
          if(imeas>0)loc_meas=imeas+1;
          command= (loc_meas<<4) | 0xF;
          val=I2C_RW(hw, device_number, ireg, command,0, 3, debug);
          usleep(10);
        }
        if(XADC_meas==3) // Scan Vref_reg values
        {
          ireg=6; // DAC control register
          command= 0xF; // Enable Vref_ref
          val=I2C_RW(hw, device_number, ireg, command,0, 3, debug);
          ireg=4; // DAC control register
          loc_meas=imeas*128;
          if(loc_meas>4095)loc_meas=4095;
          command=0x1000 | (imeas*128); // Scan DAC1 values 
          val=I2C_RW(hw, device_number, ireg, command,1, 3, debug);
          usleep(100);
        }

        double ave_val=0.;
        ValWord<uint32_t> temp;
        for(int iave=0; iave<average; iave++)
        {
          command=DRP_WRb*0 | (XADC_reg<<16);
          hw.getNode("DRP_XADC").write(command);
          temp  = hw.getNode("DRP_XADC").read();
          hw.dispatch();
          double loc_val=double((temp.value()&0xffff)>>4)/4096.;
          ave_val+=loc_val;
        }
        ave_val/=average;
        if(XADC_meas==0)
          printf("FPGA temperature, meas %d : %.2f deg\n",imeas, ave_val*4096*0.123-273);
        else if(XADC_meas==1)
          printf("CATIA 0, X5 %d, temperature : %.4f V\n", imeas,ave_val);
        else if(XADC_meas==2)
          printf("CATIA 0, Vref DAC %d, Vref : %.4f V\n", loc_meas,ave_val);
        else if(XADC_meas==3)
          printf("CATIA 0, TP DAC %d, Vdac : %.4f V\n", loc_meas, ave_val);
      }
    }
  }

  if(n_calib<0)exit(-1);
  usleep(100000);

  for(Int_t ivfe=0; ivfe<n_vfe; ivfe++)
  {
    uhal::HwInterface hw=devices[ivfe];
  // Reset the reading base address :
    hw.getNode("CAP_ADDRESS").write(0);
  // Start DAQ :
    command = ((nsample_DAQ+1)<<16)+CAPTURE_START;
    hw.getNode("CAP_CTRL").write(command);
  }

// Send triggers and wait between each trigger :
  for(int istep=0; istep<n_calib; istep++)
  {
// Program DAC for this step
//    for(auto & hw : devices)
    for(Int_t ivfe=0; ivfe<n_vfe; ivfe++)
    {
      uhal::HwInterface hw=devices[ivfe];
      for(Int_t icatia=0; icatia<n_catia && (vfe[ivfe]%1000)<100; icatia++)
      {
        if(calib_level>=0)
        {
          device_number=(CATIA_num[icatia]&0xF)<<3;
          device_number+=1000;
          ireg=6; // Iinj control register
          val=I2C_RW(hw, device_number, ireg, Reg6_def,0, 3, debug);
          ireg=4; // DAC1 register
          printf("Put %d in DAC register 4\n",calib_level);
          val=I2C_RW(hw, device_number, ireg, 0x1000|calib_level,1, 3, debug);
          printf("DAC register value read : 0x%x\n",val);
        }
        else
        {
  // switch off all the injection system
          device_number=(CATIA_num[icatia]&0xF)<<3;
          device_number+=1000;
          ireg=6; // Iinj control register
          val=I2C_RW(hw, device_number, ireg, 0,0, 3, debug);
        }
      }
// Wait for Vdac to stabilize :
      if(calib_level>=0) usleep(1000);
    }
    usleep(100000);
    int ievt=0;
    printf("start sending calibration triggers :\n"); 
    while(ievt<nevent)
    {
      Int_t nretry;
      if((ievt%100)==0)printf("%d\n",ievt); 
      for(Int_t ivfe=0; ivfe<n_vfe; ivfe++)
      {
        uhal::HwInterface hw=devices[ivfe];
        double max=0.;
        double ped[5], ave[5], rms[5];
        for(int ich=0; ich<5; ich++)
        {
          ped[ich]=0.;
          ave[ich]=0.;
          rms[ich]=0.;
        }

        if(has_ADC>=1)
        {
        // In debug mode, we reinit DAQ buffer at each event :
          if(debug>0)
          {
            command = ((nsample_DAQ+1)<<16)+CAPTURE_STOP;
            hw.getNode("CAP_CTRL").write(command);
            hw.getNode("CAP_ADDRESS").write(0);
            command = ((nsample_DAQ+1)<<16)+CAPTURE_START;
            hw.getNode("CAP_CTRL").write(command);
            address = hw.getNode("CAP_ADDRESS").read();
            hw.dispatch();
            //old_read_address=address.value()>>16;
          }

          if((ievt%100)==0 || debug>0)
            timestamp=get_event(hw,trigger_type,nsample,mem,event,&fevent[ivfe*5],debug,c1[ivfe],n_mgpa);
          else
            timestamp=get_event(hw,trigger_type,nsample,mem,event,&fevent[ivfe*5],debug,NULL,n_mgpa);
          int nsample_loc=nsample;
          if(has_ADC==2) nsample_loc=nsample-1;
          for(int ich=0; ich<5; ich++)
          {
            for(int isample=0; isample<nsample_loc; isample++)
            {
              //tg[ich]->SetPoint(isample,dt*isample,dv*event[ivfe*5+ich][isample]);
              tg[ivfe*5+ich]->SetPoint(isample,dt*isample,fevent[ivfe*5+ich][isample]);
              ave[ivfe*5+ich]+=dv*fevent[ivfe*5+ich][isample];
              if(isample<4)ped[ich]+=dv*fevent[ivfe*5+ich][isample];
              rms[ivfe*5+ich]+=dv*fevent[ivfe*5+ich][isample]*dv*fevent[ivfe*5+ich][isample];
              pshape[istep][ivfe*5+ich]->Fill(dt*isample+1.,dv*fevent[ivfe*5+ich][isample]);
            }
          }
          for(int ich=0; ich<5; ich++)
          {
            ave[ivfe*5+ich]/=nsample_loc;
            ped[ivfe*5+ich]/=4.;
            rms[ivfe*5+ich]/=nsample_loc;
            rms[ivfe*5+ich]=sqrt(rms[ivfe*5+ich]-ave[ivfe*5+ich]*ave[ivfe*5+ich]);
            //if(debug>0)printf("ich %d : ped=%f, ave=%f, rms=%f\n",ich,ped[ivfe*5+ich],ave[ivfe*5+ich],rms[ivfe*5+ich]);
            hmean[ivfe*5+ich]->Fill(ave[ivfe*5+ich]);
            hrms[ivfe*5+ich]->Fill(rms[ivfe*5+ich]);
            for(int isample=0; isample<nsample_loc; isample++)
            {
              if(dv*fevent[ivfe*5+ich][isample]-ped[ivfe*5+ich]>max)max=dv*fevent[ivfe*5+ich][isample]-ped[ivfe*5+ich];
            }
          }
          if(trigger_type==0 || max>10.)
          {
            tdata->Fill();
            if((ngood_event%200)==0)printf("%d events recorded\n",ngood_event);
            ngood_event++;
          }
          //if(debug>0 && max>0.)
          if(debug>0 || (ievt%100)==1)
          {
            if(debug==1)
            {
              Int_t increase_delay=-1;
              Int_t delay_reset=0;
              Int_t set_clock=0, increase_seq_clock=0, increase_IO_clock=0, increase_reg_clock=0, increase_mem_clock=0;
              printf("Press any key to continue :\n");
              printf("x/X : exit program\n");
              printf("q/Q : loop to next event\n");
              printf("0/z : reset line delays\n");
              printf("1<n<5 : tune ADC n line delays\n");
              printf("+ : increase delay of ADC n lines\n");
              printf("- : decrease delay of ADC n lines\n");
              printf("s : increase sequence clock phase by 45 deg (%d)\n",seq_clock_phase);
              printf("i : increase IO clock phase by 45 deg (%d)\n",IO_clock_phase);
              printf("r : increase register clock phase by 45 deg (%d)\n",reg_clock_phase);
              printf("m : increase memory clock phase by 45 deg (%d)\n",mem_clock_phase);

              system("stty raw");
              cdum=getchar();
              system("stty -raw");
              while (cdum=='1' || cdum=='2' ||cdum=='3' ||cdum=='4' ||cdum=='5')
              {
                if(cdum=='1' && has_ADC==1)tune_delay=1<<0;
                if(cdum=='1' && has_ADC==2)tune_delay=0x7F;
                if(cdum=='2' && has_ADC==1)tune_delay=1<<1;
                if(cdum=='2' && has_ADC==2)tune_delay=0x7F<<7;
                if(cdum=='3')tune_delay=1<<2;
                if(cdum=='4')tune_delay=1<<3;
                if(cdum=='5')tune_delay=1<<4;
                system("stty raw");
                cdum=getchar();
                system("stty -raw");
              }
              if(cdum=='+'|| cdum=='=') increase_delay=1;
              if(cdum=='-') increase_delay=0;
              if(cdum=='0' || cdum=='z') delay_reset=1;
              if(cdum=='s'|| cdum=='S') {set_clock=1; increase_seq_clock=1;}
              if(cdum=='i'|| cdum=='I') {set_clock=1; increase_IO_clock=1;}
              if(cdum=='r'|| cdum=='R') {set_clock=1; increase_reg_clock=1;}
              if(cdum=='m'|| cdum=='M') {set_clock=1; increase_mem_clock=1;}
              if(cdum=='q' || cdum=='Q') continue;
              if(cdum=='x' || cdum=='X') {printf("\n");exit(-1);}
              printf("Tune delay for ADC %d ",tune_delay);
              printf("in direction %d (0=down, 1=up, -1=skip)\n",increase_delay);
              if(increase_delay>=0)
              {
                if(has_ADC==1)command=DELAY_RESET*0 | DELAY_INCREASE*increase_delay | tune_delay;
                if(has_ADC==2)command=(1<<31)*0 | (1<<30)*increase_delay | tune_delay;
                hw.getNode("DELAY_CTRL").write(command);
                hw.dispatch();
                printf("Get lock status of IDELAY input stages\n");
                ValWord<uint32_t> reg = hw.getNode("DELAY_CTRL").read();
                hw.dispatch();
                printf("Value read : 0x%x\n",reg.value());
              }
              if(delay_reset==1)
              {
                command=DELAY_RESET*delay_reset;
                hw.getNode("DELAY_CTRL").write(command);
                hw.dispatch();
                printf("Get lock status of IDELAY input stages\n");
                ValWord<uint32_t> reg = hw.getNode("DELAY_CTRL").read();
                hw.dispatch();
                printf("Value read : 0x%x\n",reg.value());
              }
              if(set_clock==1)
              {
                if(increase_seq_clock==1)seq_clock_phase=(seq_clock_phase+1)%8;
                if(increase_IO_clock==1) IO_clock_phase=(IO_clock_phase+1)%8;
                if(increase_reg_clock==1)reg_clock_phase=(reg_clock_phase+1)%8;
                if(increase_mem_clock==1)mem_clock_phase=(mem_clock_phase+1)%8;
                command = MEM_CLOCK_PHASE*mem_clock_phase+REG_CLOCK_PHASE*reg_clock_phase+IO_CLOCK_PHASE*IO_clock_phase+SEQ_CLOCK_PHASE*seq_clock_phase;
                hw.getNode("CLK_SETTING").write(command);
              }
            }
            if(debug==2)usleep(100000);
            if(cdum=='q' || cdum=='Q') break;
          }
          //if(debug>0)
          //{
          //  command = ((nsample_DAQ+1)<<16)+CAPTURE_STOP;
          //  hw.getNode("CAP_CTRL").write(command);
          //  hw.dispatch();
          //  old_address[ivfe]=-1;
          //}
        }
        if(cdum=='q' || cdum=='Q') break;
      }
      ievt++;
    }
    calib_level+=calib_step;
  }

//  for(auto & hw : devices)
  for(Int_t ivfe=0; ivfe<n_vfe; ivfe++)
  {
    uhal::HwInterface hw=devices[ivfe];
// Stop DAQ :
    command = ((nsample_DAQ+1)<<16)+CAPTURE_STOP;
    hw.getNode("CAP_CTRL").write(command);
  // Switch on FE-adapter LEDs
    command = INVERT_FE_TRIGGER*invert_FE_trigger | LED_ON*1 | GENE_100HZ*0 | GENE_TRIGGER*0;
    hw.getNode("FW_VER").write(command);
    hw.dispatch();
// Restart LVRB auto scan
    printf("Stop LVRB auto scan mode\n");
    command=1;
    hw.getNode("VFE_CTRL").write(command);
    hw.dispatch();
  }

  if(has_ADC>=1)
  {
    TCanvas *c2, *c3;
    if(has_ADC==1)
    {
      c2=new TCanvas("mean","mean",800.,1000.);
      c2->Divide(2,3);
      c3=new TCanvas("rms","rms",800.,1000.);
      c3->Divide(2,3);
    }
    else
    {
      c2=new TCanvas("mean","mean",800.,600.);
      c2->Divide(1,2);
      c3=new TCanvas("rms","rms",800.,600.);
      c3->Divide(1,2);
    }
    c2->Update();
    c3->Update();
    printf("RMS : ");
    for(int ich=0; ich<n_channel; ich++)
    {
      c2->cd(ich+1);
      hmean[ich]->Draw();
      c2->Update();
      c3->cd(ich+1);
      hrms[ich]->Draw();
      rms[ich]=hrms[ich]->GetMean();
      printf("%e, ",rms[ich]);
      c3->Update();
    }
    printf("\n");
    if(trigger_type==0)
      sprintf(output_file,"data/ped_data.root");
    else if(trigger_type==1)
      sprintf(output_file,"data/TP_data.root");
    else if(trigger_type==2)
      sprintf(output_file,"data/laser_data.root");
    else if(trigger_type==3)
      sprintf(output_file,"data/G1_data.root");
    else if(trigger_type==4)
      sprintf(output_file,"data/G10_data.root");
    else
      sprintf(output_file,"data/vice_data.root");

    TFile *fd=new TFile(output_file,"recreate");
    tdata->Write();
    for(Int_t ivfe=0; ivfe<n_vfe; ivfe++) c1[ivfe]->Write();
    c2->Write();
    c3->Write();
    for(int istep=0; istep<n_calib; istep++)
    {
      for(Int_t ivfe=0; ivfe<n_vfe; ivfe++)
      {
        for(int ich=0; ich<n_channel; ich++)
        {
          pshape[istep][ivfe*5+ich]->Write();
        }
      }
    }
    fd->Close();
    printf("Finished with %d events recorded\n",ngood_event);
  }
}
