open_hw_manager
connect_hw_server -allow_non_jtag
open_hw_target
set_property PROGRAM.FILE {/data/cms/ecal/fe/fead_v2/firmware/FEAD.runs/impl_1/FEAD.bit} [get_hw_devices xc7k70t_0]
current_hw_device [get_hw_devices xc7k70t_0]
refresh_hw_device -update_hw_probes false [lindex [get_hw_devices xc7k70t_0] 0]
create_hw_cfgmem -hw_device [get_hw_devices xc7k70t_0] -mem_dev [lindex [get_cfgmem_parts {mt25ql512-spi-x1_x2_x4}] 0]

