#!/bin/bash
now=`date +%s`
reply=""
declare -i vfe=0
if [[ "x"$1 != "x" ]]; then
  vfe=$1
fi
declare -i fe_adapter=5
if [[ "x"$2 != "x" ]]; then
  fe_adapter=$2
fi
declare -i ip
ip=$fe_adapter+16
echo "Mettre en route les basses tensions"
## echo "Mettre en route la haute tension et regler a ~330 V"
echo "Attendre le chargement du firmware dans le FPGA"
## echo "Mettre le laser en attente"
## echo "Allumer le géné HP"
echo "Allumer le gene Lecroy"
echo "Regler sur ampl=1V, duty cycle=2\%, period=100us"
echo "Verifier que la liaison ethernet est sur private_network"
while [[ $reply != "o" && $reply != "O" && $reply != "q" && $reply != "Q" ]]; do
  echo -n "Tout est prêt ? [o/n/q] "
  read reply
done
if [[ $reply == "q" || $reply == "Q" ]]; then
  exit 1
fi
declare -i retcode=1
declare -i ntry=0
echo $retcode $ntry
while [[ $retcode -ne 0 && $ntry -lt 10 ]]; do 
  ping -W 1 -c 1 10.0.0.$ip
  retcode=$?
  ((ntry++))
done
if [[ $ntry -eq 10 ]]; then
  echo "Impossible de pinguer la carte FE-adapter après 10 essais."
  echo "Vérifiez la connection et les alimentations des interfaces réseau."
  echo "Relancer le programme après vérification"
  exit 1
fi

reply="r"
while [[ $reply == "r" || $reply == "R" ]]; do
  echo "**********************************************"
  echo "Mesure du bruit sur les TIA3-G10"
  echo "**********************************************"
  bin/debug_catia_AD9642.exe -vfe $fe_adapter -nevt 500 -nsample 28670 -trigger_type 0 -soft_trigger 1 -has_ADC 1 -CATIA_nums 12345 -Reg3_def 1086 2>&1 \
                  | tee data/VFE_$vfe.log
  root -q -e '.L utils/ana_ped.C+' -e 'ana_ped("data/ped_data.root") '
  mv data/ped_data.root          data/ped_data_VFE_$vfe.${now}.root
  mv data/ped_data_analized.root data/ped_data_VFE_$vfe.${now}_analized.root
  mv data/ped_data_analized.txt  data/ped_data_VFE_$vfe.${now}_analized.txt
  echo -n "On continue ou on recommence ? [c/r] "
  read reply
done

reply="r"
while [[ $reply == "r" || $reply == "R" ]]; do
  echo "**********************************************"
  echo "Mesure des gains sur les TIA3-G10"
  echo "**********************************************"
  bin/debug_catia_AD9642.exe -vfe $fe_adapter -nevt 100 -nsample 600 -trigger_type 1 -soft_trigger 1 -calib_width 300 -calib_level 0 -n_calib 31 -calib_step 128 \
                             -calib_gain 0 -has_ADC 1 -CATIA_nums 12345 -Reg3_def 1086 2>&1 \
                  | tee -a data/VFE_$vfe.log 
  root -q -e '.L utils/linearity.C' -e 'linearity("data/TP_data.root")'
  mv data/TP_data.root          data/TP_data_VFE_$vfe.${now}.root
  mv data/TP_data_analized.root data/TP_data_VFE_$vfe.${now}_analized.root
  mv data/TP_data_analized.txt  data/TP_data_VFE_$vfe.${now}_analized.txt
  echo -n "On continue ou on recommence ? [c/r] "
  read reply
done

#echo "Mettre en route le laser"
#reply=""
#while [[ $reply != "o" && $reply != "O" && $reply != "q" && $reply != "Q" ]]; do
#  echo -n "OK ? [o/n/q] "
#  read reply
#done
#if [[ $reply == "q" || $reply == "Q" ]]; then
#  exit 2
#fi
#reply="r"
#while [[ $reply == "r" || $reply == "R" ]]; do
#  echo "**********************************************"
#  echo "Mesure des fonctions de transfert sur les TIA3-G10"
#  echo "**********************************************"
#  bin/loc_daq.exe -vfe $fe_adapter -trigger_type 2 -nsample 200 -dbg 0 -nevt 10000 -n_calib 1 -self_trigger 1 -self_trigger_threshold 650 -self_trigger_mask 4 \
#                  -soft_trigger 0 -negate_data 1 -signed_data 0 -input_span 0 2>&1 \
#                  | tee -a /data/cms/ecal/fe/TB_2018/vfe_$vfe/laser.$now.log
#  mv /data/cms/ecal/fe/TB_2018/test/laser_data.root /data/cms/ecal/fe/TB_2018/vfe_$vfe/laser_data_TIA3_G10.$now.root
#  echo -n "On continue ou on recommence ? [c/r] "
#  read reply
#done

echo "Mettre le gene Lecroy en route (verifier le reglage)"
reply=""
while [[ $reply != "o" && $reply != "O" && $reply != "q" && $reply != "Q" ]]; do
  echo -n "OK ? [o/n/q] "
  read reply
done
if [[ $reply == "q" || $reply == "Q" ]]; then
  exit 2
fi
reply="r"
while [[ $reply == "r" || $reply == "R" ]]; do
  echo "**********************************************"
  echo "Mesure du gain"
  echo "**********************************************"
  bin/debug_catia_AD9642.exe -vfe $fe_adapter -nevt 1000 -nsample 600 -trigger_type 2 -soft_trigger 0 -calib_level 0 -self_trigger 1 -self_trigger_threshold 1000 \
                             -self_trigger_mask 4 -has_ADC 1 -CATIA_nums 12345 -Reg3_def 1086 2>&1 \
                  | tee -a data/VFE_$vfe.log
  root -q -e '.L utils/ana_Iinj.C+' -e 'ana_Iinj("data/laser_data.root",100.,800,1000)'
  mv data/laser_data.root          data/Iinj_data_VFE_$vfe.${now}.root
  mv data/laser_data_analized.root data/Iinj_data_VFE_$vfe.${now}_analized.root
  mv data/laser_data_analized.txt  data/Iinj_data_VFE_$vfe.${now}_analized.txt
  echo -n "On continue ou on recommence ? [c/r] "
  read reply
done

## echo "Couper le laser"
## echo "Couper la haute tension et attendre le retour à 0"
echo "Couper les basses tensions"
## echo "Couper le géné HP"
echo "Mettre le gene Lecroy en pause"
echo "Vous pouvez récuperer la carte !"
echo "Liste des fichiers générés :"
echo "data/ped_data_VFE_$vfe.$now.root"
echo "data/TP_data_VFE_$vfe.$now.root"
echo "data/Iinj_data_VFE_$vfe.$now.root"
