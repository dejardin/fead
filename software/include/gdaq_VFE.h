#ifndef EXTERN
  #define EXTERN
#endif

#include "gtk/gtk.h"
#include "uhal/uhal.hpp"
#include <signal.h>
#include "limits.h"

#include "I2C_RW.h"
#include "LiTEDTU_def.h"

#include <vector>
#include <iostream>
#include <cstdlib>
#include <typeinfo>

#include <TApplication.h>
#include <TStyle.h>
#include <TProfile.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <TTree.h>
#include <TF1.h>
#include <TFile.h>

#define I2C_SHIFT_DEV_NUMBER   3

#define GENE_TRIGGER           (1<<0)
#define GENE_TP                (1<<1)
#define GENE_WTE               (1<<2)
#define LED_ON                 (1<<3)
#define TP_MODE                (1<<4)
#define GENE_G10_PULSE         (1<<5)
#define GENE_G1_PULSE          (1<<6)
#define GENE_RESYNC            (1<<7)
#define GENE_AWG               (1<<8)
#define CRC_RESET              (1<<31)

#define RESET                  (1<<0)
#define FIFO_MODE              (1<<1)
#define SELF_TRIGGER_LOOP      (1<<2)
#define SELF_TRIGGER           (1<<3)
#define CLOCK_PHASE            (1<<4)
#define CLOCK_RESET            (1<<7)
#define SELF_TRIGGER_THRESHOLD (1<<8)
#define SELF_TRIGGER_MASK      (1<<22)
#define CALIB_PULSE_ENABLED    (1<<27)
#define CALIB_MUX_ENABLED      (1<<28)
#define BOARD_SN               (1<<28)
#define SELF_TRIGGER_MODE      (1<<31)

#define SEQ_CLOCK_PHASE        (1<<0)
#define IO_CLOCK_PHASE         (1<<4)
#define REG_CLOCK_PHASE        (1<<8)
#define MEM_CLOCK_PHASE        (1<<12)
#define RESYNC_PHASE           (1<<16)

#define I2C_TOGGLE_SDA         (1<<31)

#define PWUP_RESETB            (1<<31)
#define BULKY_I2C_DTU          (1<<28)
#define I2C_LPGBT_MODE         (1<<27)
#define N_RETRY_I2C_DTU        (1<<14)
#define PED_MUX                (1<<13)
#define BUGGY_I2C_DTU          (1<<4)
#define INVERT_RESYNC          (1<<3)
#define LVRB_AUTOSCAN          (1<<2)

#define CAPTURE_START          1
#define CAPTURE_STOP           2
//#define SW_DAQ_DELAY           0x1800 // delay for laser with internal trigger
#define SW_DAQ_DELAY           (1<<16)  // delay for laser with external trigger
#define HW_DAQ_DELAY           (1<<0)   // Laser with external trigger
#define NSAMPLE_MAX            26624
#define MAX_PAYLOAD            1380
#define MAX_VFE                10
#define MAX_STEP               4096

#define DRP_WRb                (1<<31)
#define DELAY_TAP_DIR          (1<<5)     // Increase (1) or decrease (0) tap delay in ISERDES
#define DELAY_RESET            (1<<6)     // Reset ISERDES delay
#define IO_RESET               (1<<7)     // Reset ISERDES IO
#define BIT_SLIP               (1<<16)    // Slip serial bits by 1 unit (to align bits in ISERDES) (to multiply with 5 bits channel map)
#define BYTE_SLIP              (1<<21)    // Slip serial bytes by 1 unit (to align bytes in 40 MHz clock) (to multiply with 5 bits channel map)
#define DELAY_AUTO_TUNE        (1<<29)    // Launch automatic IDELAY tuning to get the eye center of each stream on calibartion streams
#define START_IDELAY_SYNC      (1<<27)    // Force IDELAY tuning to get the eye center of each stream when idle patterns are ON (LiTE-DTU-V2.0)

#define DTU_CALIB_MODE         (1<<0)
#define DTU_TEST_MODE          (1<<1)
#define DTU_MEM_MODE           (1<<5)

#define RESYNC_IDLE_PATTERN    (1<<0)
#define DTU_PLL_LOCK           8
#define TE_POS                 (1<<13)
#define TE_COMMAND             (1<<27)
#define TE_ENABLE              (1<<31)

#define eLINK_ACTIVE           (1<<8)
#define STATIC_RESET           (1<<31)

#define I2C_LVR_TYPE           0
#define I2C_CATIA_TYPE         1
#define I2C_LiTEDTU_TYPE       2

#define NSAMPLE_PED            5
#define N_ADC_REG              76

using namespace uhal;

typedef struct
{
  GtkWidget *main_window;
  GtkBuilder *builder;
  gpointer user_data;
} SGlobalData;
EXTERN SGlobalData gtk_data;

typedef struct
{
  Int_t CATIA_version[5], CATIA_Vref_val[5], CATIA_ped_G10[5], CATIA_ped_G1[5], CATIA_DAC1_val[5], CATIA_DAC2_val[5];
  Int_t CATIA_gain[5], CATIA_LPF35[5], CATIA_Rconv[5], CATIA_temp_out[5], CATIA_temp_X5[5], CATIA_Vref_out[5], CATIA_DAC_buf_off[5];
  Int_t CATIA_reg_def[5][7], CATIA_reg_loc[5][7];
  Int_t DTU_version[5], DTU_BL_G10[5], DTU_BL_G1[5], DTU_PLL_conf[5], DTU_PLL_conf1[5], DTU_PLL_conf2[5];
  Int_t DTU_CRC_errors[5], DTU_n_orbit_samples[5], DTU_PE_width[5],DTU_PE_strength[5], DTU_driver_current[5];
  Double_t CATIA_Temp_offset[5];
  bool DTU_dither[5], DTU_nsample_calib_x4[5], DTU_global_test[5], DTU_override_Vc_bit[5], DTU_force_PLL[5];
  bool DTU_VCO_rail_mode[5], DTU_bias_ctrl_override[5];
  bool DTU_force_G1[5], DTU_force_G10[5], DTU_invert_clock[5], DTU_G1_window16[5], DTU_clk_out[5], DTU_eLink[5];
  bool CATIA_DAC1_status[5], CATIA_DAC2_status[5], CATIA_SEU_corr[5];
  Int_t CATIA_number[5], DTU_number[5];
} AsicSetting_s;
EXTERN AsicSetting_s asic;

typedef struct
{
  Int_t FEAD_number, VFE_number, firmware_version, nsample, nevent, numrun, cur_evt;
  Int_t VICEPP_clk, VICEPP_clk_tree, n_active_channel, eLink_active, channel_number[5], daq_initialized;
  Int_t I2C_LiTEDTU_type, I2C_CATIA_type, I2C_shift_dev_number, I2C_lpGBT_mode;
  Int_t delay_auto_tune, odd_sample_shift;
  Int_t delay_val[5];    // Number of delay taps to get a stable R/O with ADCs (160 MHz)
  Int_t bitslip_val[5];  // Number of bit to slip with iserdes lines to get ADC values well aligned
  Int_t byteslip_val[5]; // Number of byte to slip to aligned ADC data on 40 MHz clock
  Int_t old_read_address, shift_oddH_samples, shift_oddL_samples;
  Int_t I2C_DTU_nreg, buggy_DTU, DTU_test_mode;
  Int_t trigger_type, self_trigger, self_trigger_mode, self_trigger_threshold, self_trigger_mask, self_trigger_loop;
  Int_t fifo_mode;         // Use memory in FIFO mode or not
  Int_t sw_DAQ_delay, hw_DAQ_delay, resync_idle_pattern, resync_phase;
  Int_t TP_step, n_TP_step, TP_width, TP_delay, TP_level;
  Int_t TE_pos, TE_command;
  bool TE_enable;
  Int_t debug_DAQ, debug_I2C, debug_GTK, debug_draw, debug_ana, selected_channel, zoom_draw;
  Int_t LVRB_autoscan, error, calib_pulse_enabled, calib_mux_enabled, use_AWG;
  Int_t SEU_test_mode, SEU_current_limit, SEU_pattern;
  Int_t MEM_pos, dump_data;
  UInt_t ReSync_data;
  Long_t timestamp;
  Short_t *event[6], *gain[6];
  Double_t *fevent[6], ped_G1[6], ped_G10[6];
  Double_t Rshunt_V2P5, Rshunt_V1P2, Tsensor_divider;
  Int_t all_sample[6], ns_g1[6];
  bool is_VICEPP, CATIA_scan_Vref, use_ref_ADC_calib, CATIA_Vcal_out, MEM_mode, half_mode;
  bool corgain, wait_for_ever, daq_running, SEU_simul;
  ValVector< uint32_t > mem;
  TGraph *tg[6], *tg_g1[6];
  TH1D *hmean[6], *hrms[6], *hdensity[6];
  TProfile *pshape[MAX_STEP][6];
  TCanvas *c1, *c2, *c3;
  TTree *tdata;
  TFile *fd;
  FILE *fout_SEU;
  char xml_filename[PATH_MAX], comment[1024];
} DAQSetting_s;
EXTERN DAQSetting_s daq;

typedef struct
{
  bool test_running;
  Int_t error[5][7], n_reg, reg_number[7];
  Int_t n_SEU[5],n_SEL[2];
  Double_t V2P5_XADC, V1P2_XADC, V2P5_sense, V1P2_sense, FPGA_temp_val, CATIA_temp_val;
  Double_t Imeas_V2P5, Imeas_V1P2;
} SEU_s;
EXTERN SEU_s SEU;

using namespace uhal;

EXTERN struct timeval tv;
EXTERN void read_conf_from_xml_file(char*);
EXTERN void write_conf_to_xml_file(char*);
EXTERN void enable_channel(int, bool);
EXTERN Int_t synchronize_links(uhal::HwInterface, Int_t);
EXTERN void tune_pedestals(uhal::HwInterface, Int_t);
EXTERN std::vector<uhal::HwInterface> devices;
EXTERN void init_gDAQ();
EXTERN void init_VFE();
EXTERN void reset_DTU_sync();
EXTERN void send_ReSync_command(UInt_t command);
EXTERN void calib_ADC();
EXTERN void synchronize_links();
EXTERN void dump_ADC_registers();
EXTERN void load_ADC_registers();
EXTERN UInt_t update_CATIA_reg(Int_t, Int_t);
EXTERN UInt_t update_LiTEDTU_reg(Int_t, Int_t);
EXTERN void get_single_event();
EXTERN void take_run(void);
EXTERN void get_event(Int_t, Int_t);
EXTERN void get_single_event(Int_t);
EXTERN void optimize_pedestal(void);
EXTERN void CATIA_calibration(void);
EXTERN void end_of_run(void);
EXTERN void update_trigger(void);
EXTERN void get_temp(void);
EXTERN void get_CRC_errors(int);
EXTERN void get_n_orbit_samples(void);
EXTERN void SEU_test(void);
EXTERN void PLL_scan(void);
EXTERN void switch_power(bool);
EXTERN void get_consumption(void);

extern "C" G_MODULE_EXPORT void delete_event(GtkMenuItem*, gpointer);
extern "C" G_MODULE_EXPORT void callback_about(GtkMenuItem*, gpointer);
extern "C" G_MODULE_EXPORT void my_exit(GtkMenuItem*, gpointer);
extern "C" G_MODULE_EXPORT void button_clicked(GtkWidget*, gpointer);
extern "C" G_MODULE_EXPORT void switch_toggled(GtkWidget*, gpointer);
extern "C" G_MODULE_EXPORT void Entry_modified(GtkWidget*, gpointer);
extern "C" G_MODULE_EXPORT void read_config(GtkMenuItem*, gpointer);
extern "C" G_MODULE_EXPORT void save_config(GtkMenuItem*, gpointer);
extern "C" G_MODULE_EXPORT void save_config_as(GtkMenuItem*, gpointer);
