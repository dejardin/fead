EESchema Schematic File Version 4
LIBS:FEAD-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 8325 5250 2    39   Input ~ 0
SFP_Present
Text HLabel 8325 5350 2    39   Input ~ 0
SFP_LOS
Text HLabel 8325 5450 2    39   Input ~ 0
SFP_Tx_Fault
Text HLabel 8325 5550 2    39   Output ~ 0
SFP_SCL
Text HLabel 8325 5650 2    39   BiDi ~ 0
SFP_SDA
Text HLabel 8325 4675 2    39   Output ~ 0
I2C_clk_out
Text HLabel 8325 4600 2    39   BiDi ~ 0
I2C_data_out
Text HLabel 10250 4675 2    39   Input ~ 0
I2C_clk_in
Text HLabel 10250 4600 2    39   BiDi ~ 0
I2C_data_in
Text HLabel 10250 5250 2    39   Input ~ 0
CLK_in+
Text Label 875  1150 2    30   ~ 0
I2C_data_out
Text Label 875  1200 2    30   ~ 0
I2C_clk_out
Text HLabel 10250 4750 2    39   Input ~ 0
Pwup_Reset_in
Text HLabel 8325 4750 2    39   Output ~ 0
Pwup_Reset_out
$Comp
L Device:C C408
U 1 1 57948AA0
P 9000 1650
F 0 "C408" V 9100 1800 50  0000 C CNN
F 1 "47uF" V 9125 1650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9000 1650 50  0001 C CNN
F 3 "" H 9000 1650 50  0000 C CNN
F 4 "Digi-key" V 9000 1650 50  0001 C CNN "Supplier"
F 5 "490-6542-1-ND	" V 9000 1650 50  0001 C CNN "Supplier Part"
	1    9000 1650
	0    1    1    0   
$EndComp
$Comp
L Device:C C414
U 1 1 57948AAC
P 9000 3550
F 0 "C414" V 9100 3700 50  0000 C CNN
F 1 "0.47uF" V 9175 3650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9000 3550 50  0001 C CNN
F 3 "" H 9000 3550 50  0000 C CNN
F 4 "Digi-key" V 9000 3550 50  0001 C CNN "Supplier"
F 5 "490-6416-1-ND	" V 9000 3550 50  0001 C CNN "Supplier Part"
	1    9000 3550
	0    1    1    0   
$EndComp
$Comp
L Device:C C409
U 1 1 57948AB6
P 9000 2025
F 0 "C409" V 9100 2175 50  0000 C CNN
F 1 "10uF" V 9150 2025 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9000 2025 50  0001 C CNN
F 3 "" H 9000 2025 50  0000 C CNN
F 4 "Digi-key" V 9000 2025 50  0001 C CNN "Supplier"
F 5 "490-6479-1-ND	" V 9000 2025 50  0001 C CNN "Supplier Part"
	1    9000 2025
	0    1    1    0   
$EndComp
$Comp
L Device:C C410
U 1 1 57948ABE
P 9000 2275
F 0 "C410" V 9100 2425 50  0000 C CNN
F 1 "10uF" V 9150 2275 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9000 2275 50  0001 C CNN
F 3 "" H 9000 2275 50  0000 C CNN
F 4 "Digi-key" V 9000 2275 50  0001 C CNN "Supplier"
F 5 "490-6479-1-ND	" V 9000 2275 50  0001 C CNN "Supplier Part"
	1    9000 2275
	0    1    1    0   
$EndComp
$Comp
L Device:C C411
U 1 1 57948AC6
P 9000 2650
F 0 "C411" V 9100 2800 50  0000 C CNN
F 1 "0.47uF" V 9125 2650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9000 2650 50  0001 C CNN
F 3 "" H 9000 2650 50  0000 C CNN
F 4 "Digi-key" V 9000 2650 50  0001 C CNN "Supplier"
F 5 "490-6416-1-ND	" V 9000 2650 50  0001 C CNN "Supplier Part"
	1    9000 2650
	0    1    1    0   
$EndComp
$Comp
L Device:C C412
U 1 1 57948ACE
P 9000 2950
F 0 "C412" V 9100 3100 50  0000 C CNN
F 1 "0.47uF" V 9125 2950 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9000 2950 50  0001 C CNN
F 3 "" H 9000 2950 50  0000 C CNN
F 4 "Digi-key" V 9000 2950 50  0001 C CNN "Supplier"
F 5 "490-6416-1-ND	" V 9000 2950 50  0001 C CNN "Supplier Part"
	1    9000 2950
	0    1    1    0   
$EndComp
$Comp
L Device:C C413
U 1 1 57948AD6
P 9000 3250
F 0 "C413" V 9100 3400 50  0000 C CNN
F 1 "0.47uF" V 9125 3250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9000 3250 50  0001 C CNN
F 3 "" H 9000 3250 50  0000 C CNN
F 4 "Digi-key" V 9000 3250 50  0001 C CNN "Supplier"
F 5 "490-6416-1-ND	" V 9000 3250 50  0001 C CNN "Supplier Part"
	1    9000 3250
	0    1    1    0   
$EndComp
$Comp
L power1:GND #PWR0409
U 1 1 57948AE2
P 8850 3575
F 0 "#PWR0409" H 8850 3325 50  0001 C CNN
F 1 "GND" H 8850 3425 50  0000 C CNN
F 2 "" H 8850 3575 50  0000 C CNN
F 3 "" H 8850 3575 50  0000 C CNN
	1    8850 3575
	1    0    0    -1  
$EndComp
$Comp
L Device:C C401
U 1 1 57949719
P 8000 1650
F 0 "C401" V 8100 1800 50  0000 C CNN
F 1 "47uF" V 8125 1650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8000 1650 50  0001 C CNN
F 3 "" H 8000 1650 50  0000 C CNN
F 4 "Digi-key" V 8000 1650 50  0001 C CNN "Supplier"
F 5 "490-6542-1-ND	" V 8000 1650 50  0001 C CNN "Supplier Part"
	1    8000 1650
	0    1    1    0   
$EndComp
$Comp
L Device:C C407
U 1 1 57949725
P 8000 3550
F 0 "C407" V 8100 3700 50  0000 C CNN
F 1 "0.47uF" V 8175 3650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8000 3550 50  0001 C CNN
F 3 "" H 8000 3550 50  0000 C CNN
F 4 "Digi-key" V 8000 3550 50  0001 C CNN "Supplier"
F 5 "490-6416-1-ND	" V 8000 3550 50  0001 C CNN "Supplier Part"
	1    8000 3550
	0    1    1    0   
$EndComp
$Comp
L Device:C C402
U 1 1 5794972F
P 8000 2025
F 0 "C402" V 8100 2175 50  0000 C CNN
F 1 "10uF" V 8150 2025 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8000 2025 50  0001 C CNN
F 3 "" H 8000 2025 50  0000 C CNN
F 4 "Digi-key" V 8000 2025 50  0001 C CNN "Supplier"
F 5 "490-6479-1-ND	" V 8000 2025 50  0001 C CNN "Supplier Part"
	1    8000 2025
	0    1    1    0   
$EndComp
$Comp
L Device:C C403
U 1 1 57949737
P 8000 2275
F 0 "C403" V 8100 2425 50  0000 C CNN
F 1 "10uF" V 8150 2275 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8000 2275 50  0001 C CNN
F 3 "" H 8000 2275 50  0000 C CNN
F 4 "Digi-key" V 8000 2275 50  0001 C CNN "Supplier"
F 5 "490-6479-1-ND	" V 8000 2275 50  0001 C CNN "Supplier Part"
	1    8000 2275
	0    1    1    0   
$EndComp
$Comp
L Device:C C404
U 1 1 5794973F
P 8000 2650
F 0 "C404" V 8100 2800 50  0000 C CNN
F 1 "0.47uF" V 8125 2650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8000 2650 50  0001 C CNN
F 3 "" H 8000 2650 50  0000 C CNN
F 4 "Digi-key" V 8000 2650 50  0001 C CNN "Supplier"
F 5 "490-6416-1-ND	" V 8000 2650 50  0001 C CNN "Supplier Part"
	1    8000 2650
	0    1    1    0   
$EndComp
$Comp
L Device:C C405
U 1 1 57949747
P 8000 2950
F 0 "C405" V 8100 3100 50  0000 C CNN
F 1 "0.47uF" V 8125 2950 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8000 2950 50  0001 C CNN
F 3 "" H 8000 2950 50  0000 C CNN
F 4 "Digi-key" V 8000 2950 50  0001 C CNN "Supplier"
F 5 "490-6416-1-ND	" V 8000 2950 50  0001 C CNN "Supplier Part"
	1    8000 2950
	0    1    1    0   
$EndComp
$Comp
L Device:C C406
U 1 1 5794974F
P 8000 3250
F 0 "C406" V 8100 3400 50  0000 C CNN
F 1 "0.47uF" V 8125 3250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8000 3250 50  0001 C CNN
F 3 "" H 8000 3250 50  0000 C CNN
F 4 "Digi-key" V 8000 3250 50  0001 C CNN "Supplier"
F 5 "490-6416-1-ND	" V 8000 3250 50  0001 C CNN "Supplier Part"
	1    8000 3250
	0    1    1    0   
$EndComp
$Comp
L power1:VccO #PWR0408
U 1 1 57949755
P 8150 1475
F 0 "#PWR0408" H 8150 1325 50  0001 C CNN
F 1 "VccO" H 8150 1625 50  0000 C CNN
F 2 "" H 8150 1475 50  0000 C CNN
F 3 "" H 8150 1475 50  0000 C CNN
	1    8150 1475
	1    0    0    -1  
$EndComp
$Comp
L power1:GND #PWR0407
U 1 1 5794975B
P 7850 3575
F 0 "#PWR0407" H 7850 3325 50  0001 C CNN
F 1 "GND" H 7850 3425 50  0000 C CNN
F 2 "" H 7850 3575 50  0000 C CNN
F 3 "" H 7850 3575 50  0000 C CNN
	1    7850 3575
	1    0    0    -1  
$EndComp
$Comp
L Device:C C415
U 1 1 5794A207
P 10025 1650
F 0 "C415" V 10125 1800 50  0000 C CNN
F 1 "47uF" V 10150 1650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 10025 1650 50  0001 C CNN
F 3 "" H 10025 1650 50  0000 C CNN
F 4 "Digi-key" V 10025 1650 50  0001 C CNN "Supplier"
F 5 "490-6542-1-ND	" V 10025 1650 50  0001 C CNN "Supplier Part"
	1    10025 1650
	0    1    1    0   
$EndComp
$Comp
L Device:C C421
U 1 1 5794A213
P 10025 3550
F 0 "C421" V 10125 3700 50  0000 C CNN
F 1 "0.47uF" V 10200 3650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10025 3550 50  0001 C CNN
F 3 "" H 10025 3550 50  0000 C CNN
F 4 "Digi-key" V 10025 3550 50  0001 C CNN "Supplier"
F 5 "490-6416-1-ND	" V 10025 3550 50  0001 C CNN "Supplier Part"
	1    10025 3550
	0    1    1    0   
$EndComp
$Comp
L Device:C C416
U 1 1 5794A21D
P 10025 2025
F 0 "C416" V 10125 2175 50  0000 C CNN
F 1 "10uF" V 10175 2025 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10025 2025 50  0001 C CNN
F 3 "" H 10025 2025 50  0000 C CNN
F 4 "Digi-key" V 10025 2025 50  0001 C CNN "Supplier"
F 5 "490-6479-1-ND	" V 10025 2025 50  0001 C CNN "Supplier Part"
	1    10025 2025
	0    1    1    0   
$EndComp
$Comp
L Device:C C417
U 1 1 5794A225
P 10025 2275
F 0 "C417" V 10125 2425 50  0000 C CNN
F 1 "10uF" V 10175 2275 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10025 2275 50  0001 C CNN
F 3 "" H 10025 2275 50  0000 C CNN
F 4 "Digi-key" V 10025 2275 50  0001 C CNN "Supplier"
F 5 "490-6479-1-ND	" V 10025 2275 50  0001 C CNN "Supplier Part"
	1    10025 2275
	0    1    1    0   
$EndComp
$Comp
L Device:C C418
U 1 1 5794A22D
P 10025 2650
F 0 "C418" V 10125 2800 50  0000 C CNN
F 1 "0.47uF" V 10150 2650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10025 2650 50  0001 C CNN
F 3 "" H 10025 2650 50  0000 C CNN
F 4 "Digi-key" V 10025 2650 50  0001 C CNN "Supplier"
F 5 "490-6416-1-ND	" V 10025 2650 50  0001 C CNN "Supplier Part"
	1    10025 2650
	0    1    1    0   
$EndComp
$Comp
L Device:C C419
U 1 1 5794A235
P 10025 2950
F 0 "C419" V 10125 3100 50  0000 C CNN
F 1 "0.47uF" V 10150 2950 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10025 2950 50  0001 C CNN
F 3 "" H 10025 2950 50  0000 C CNN
F 4 "Digi-key" V 10025 2950 50  0001 C CNN "Supplier"
F 5 "490-6416-1-ND	" V 10025 2950 50  0001 C CNN "Supplier Part"
	1    10025 2950
	0    1    1    0   
$EndComp
$Comp
L Device:C C420
U 1 1 5794A23D
P 10025 3250
F 0 "C420" V 10125 3400 50  0000 C CNN
F 1 "0.47uF" V 10150 3250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10025 3250 50  0001 C CNN
F 3 "" H 10025 3250 50  0000 C CNN
F 4 "Digi-key" V 10025 3250 50  0001 C CNN "Supplier"
F 5 "490-6416-1-ND	" V 10025 3250 50  0001 C CNN "Supplier Part"
	1    10025 3250
	0    1    1    0   
$EndComp
$Comp
L power1:GND #PWR0411
U 1 1 5794A249
P 9875 3575
F 0 "#PWR0411" H 9875 3325 50  0001 C CNN
F 1 "GND" H 9875 3425 50  0000 C CNN
F 2 "" H 9875 3575 50  0000 C CNN
F 3 "" H 9875 3575 50  0000 C CNN
	1    9875 3575
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 1475 9150 1650
Wire Wire Line
	8850 1650 8850 2025
Connection ~ 9150 1650
Connection ~ 8850 3550
Wire Wire Line
	8150 1475 8150 1650
Wire Wire Line
	7850 1650 7850 2025
Connection ~ 8150 1650
Connection ~ 7850 3550
Wire Wire Line
	10175 1475 10175 1650
Wire Wire Line
	9875 1650 9875 2025
Connection ~ 10175 1650
Connection ~ 9875 3550
Connection ~ 7850 3250
Connection ~ 8150 3250
Connection ~ 8150 2950
Connection ~ 7850 2950
Connection ~ 7850 2650
Connection ~ 8150 2650
Connection ~ 7850 2025
Connection ~ 7850 2275
Connection ~ 8150 2275
Connection ~ 8150 2025
Connection ~ 9150 2025
Connection ~ 8850 2025
Connection ~ 8850 2275
Connection ~ 8850 2650
Connection ~ 8850 2950
Connection ~ 8850 3250
Connection ~ 9150 3250
Connection ~ 9150 2950
Connection ~ 9150 2650
Connection ~ 9150 2275
Connection ~ 10175 3250
Connection ~ 9875 3250
Connection ~ 9875 2950
Connection ~ 10175 2950
Connection ~ 10175 2650
Connection ~ 9875 2650
Connection ~ 10175 2275
Connection ~ 9875 2275
Connection ~ 9875 2025
Connection ~ 10175 2025
Wire Wire Line
	925  1150 875  1150
Text HLabel 10250 5475 2    39   Output ~ 0
LED[0..3]
Text Label 3800 3000 2    30   ~ 0
LED0
Text Label 3800 3050 2    30   ~ 0
LED1
Text Label 3800 3100 2    30   ~ 0
LED2
Text Label 3800 3150 2    30   ~ 0
LED3
Wire Wire Line
	3800 3100 3950 3100
Wire Wire Line
	3800 3150 3950 3150
Wire Wire Line
	3800 3000 3950 3000
Wire Wire Line
	3800 3050 3950 3050
Text Label 3800 3200 2    30   ~ 0
GPIO0
Text Label 3800 3250 2    30   ~ 0
GPIO1
Text Label 3800 3300 2    30   ~ 0
GPIO2
Text Label 3800 3350 2    30   ~ 0
GPIO3
Wire Wire Line
	3800 3200 3950 3200
Wire Wire Line
	3800 3250 3950 3250
Wire Wire Line
	3800 3300 3950 3300
Wire Wire Line
	3800 3350 3950 3350
Text HLabel 10250 5550 2    39   Input ~ 0
ADDR[0..3]
Text Label 3800 3400 2    30   ~ 0
ADDR0
Text Label 3800 3450 2    30   ~ 0
ADDR1
Text Label 3800 3500 2    30   ~ 0
ADDR2
Text Label 3800 3550 2    30   ~ 0
ADDR3
Wire Wire Line
	3800 3550 3950 3550
Wire Wire Line
	3800 3500 3950 3500
Wire Wire Line
	3800 3450 3950 3450
Wire Wire Line
	3800 3400 3950 3400
Wire Wire Line
	9150 1650 9150 2025
Wire Wire Line
	8850 3550 8850 3575
Wire Wire Line
	8150 1650 8150 2025
Wire Wire Line
	7850 3550 7850 3575
Wire Wire Line
	10175 1650 10175 2025
Wire Wire Line
	9875 3550 9875 3575
Wire Wire Line
	8150 3250 8150 3550
Wire Wire Line
	8150 2950 8150 3250
Wire Wire Line
	7850 2950 7850 3250
Wire Wire Line
	7850 2650 7850 2950
Wire Wire Line
	8150 2650 8150 2950
Wire Wire Line
	7850 2025 7850 2275
Wire Wire Line
	7850 2275 7850 2650
Wire Wire Line
	8150 2275 8150 2650
Wire Wire Line
	8150 2025 8150 2275
Wire Wire Line
	9150 2025 9150 2275
Wire Wire Line
	8850 2025 8850 2275
Wire Wire Line
	8850 2275 8850 2650
Wire Wire Line
	8850 2650 8850 2950
Wire Wire Line
	8850 2950 8850 3250
Wire Wire Line
	9150 3250 9150 3550
Wire Wire Line
	9150 2950 9150 3250
Wire Wire Line
	9150 2650 9150 2950
Wire Wire Line
	9150 2275 9150 2650
Wire Wire Line
	10175 3250 10175 3550
Wire Wire Line
	9875 2950 9875 3250
Wire Wire Line
	10175 2950 10175 3250
Wire Wire Line
	10175 2650 10175 2950
Wire Wire Line
	9875 2650 9875 2950
Wire Wire Line
	10175 2275 10175 2650
Wire Wire Line
	9875 2275 9875 2650
Wire Wire Line
	9875 2025 9875 2275
Wire Wire Line
	10175 2025 10175 2275
Wire Wire Line
	8850 3250 8850 3550
Wire Wire Line
	7850 3250 7850 3550
Wire Wire Line
	9875 3250 9875 3550
Text HLabel 8325 3925 2    39   Input ~ 0
Ch_in_P[1..5]
Text HLabel 8325 4000 2    39   Input ~ 0
Ch_in_N[1..5]
Text HLabel 8325 4075 2    39   Input ~ 0
SEUA_in_[1..5]
Text HLabel 8325 4150 2    39   Input ~ 0
SEUD_in_[1..5]
Text HLabel 10250 3925 2    39   Output ~ 0
Ch_out_P[1..5]
Text HLabel 10250 4000 2    39   Output ~ 0
Ch_out_N[1..5]
Text HLabel 10250 4075 2    39   Output ~ 0
SEUA_out_[1..5]
Text HLabel 10250 4150 2    39   Output ~ 0
SEUD_out_[1..5]
Text HLabel 10250 5325 2    39   Input ~ 0
CLK_in-
Wire Wire Line
	2225 900  2225 950 
$Comp
L FEAD-rescue:VccO_VFE-MDJ_compo #PWR0401
U 1 1 5C1317C8
P 1525 900
F 0 "#PWR0401" H 1525 750 50  0001 C CNN
F 1 "VccO_VFE" H 1525 1050 50  0000 C CNN
F 2 "" H 1525 900 50  0000 C CNN
F 3 "" H 1525 900 50  0000 C CNN
	1    1525 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	1525 900  1525 950 
Wire Wire Line
	2875 5075 2825 5075
Wire Wire Line
	2875 5025 2825 5025
Text Label 2875 5075 0    30   ~ 0
Ch_out_N1
Text Label 2875 5025 0    30   ~ 0
Ch_out_P1
Wire Wire Line
	2875 6975 2825 6975
Wire Wire Line
	2875 6925 2825 6925
Text Label 2875 6975 0    30   ~ 0
Ch_out_N5
Text Label 2875 6925 0    30   ~ 0
Ch_out_P5
Wire Wire Line
	2875 6775 2825 6775
Wire Wire Line
	2875 6725 2825 6725
Text Label 2875 6775 0    30   ~ 0
Ch_out_N4
Text Label 2875 6725 0    30   ~ 0
Ch_out_P4
Wire Wire Line
	2875 5775 2825 5775
Wire Wire Line
	2875 5725 2825 5725
Text Label 2875 5775 0    30   ~ 0
Ch_out_N3
Text Label 2875 5725 0    30   ~ 0
Ch_out_P3
Wire Wire Line
	2875 5575 2825 5575
Wire Wire Line
	2875 5525 2825 5525
Text Label 2875 5575 0    30   ~ 0
Ch_out_N2
Text Label 2875 5525 0    30   ~ 0
Ch_out_P2
Wire Wire Line
	925  1200 875  1200
Wire Wire Line
	5150 900  5150 950 
$Comp
L FEAD-rescue:VccO_FE-MDJ_compo #PWR0406
U 1 1 5CC4ED8F
P 5150 900
F 0 "#PWR0406" H 5150 750 50  0001 C CNN
F 1 "VccO_FE" H 5165 1073 50  0000 C CNN
F 2 "" H 5150 900 50  0000 C CNN
F 3 "" H 5150 900 50  0000 C CNN
	1    5150 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 900  4550 950 
Wire Wire Line
	1525 4525 1525 4575
Wire Wire Line
	2225 4525 2225 4575
$Comp
L FEAD-rescue:XC7K70T-FBG484-MDJ_compo U301
U 1 1 5C912CE9
P 1875 2200
F 0 "U301" H 1875 428 50  0000 C CNN
F 1 "XC7K70T-FBG484" H 1875 337 50  0000 C CNN
F 2 "Package_BGA:BGA-484_23.0x23.0mm_Layout22x22_P1.0mm" H 1875 2200 50  0001 C CNN
F 3 "" H 1875 2200 50  0000 C CNN
	1    1875 2200
	1    0    0    -1  
$EndComp
$Comp
L FEAD-rescue:XC7K70T-FBG484-MDJ_compo U301
U 2 1 5C912F1A
P 4900 2200
F 0 "U301" H 4900 428 50  0000 C CNN
F 1 "XC7K70T-FBG484" H 4900 337 50  0000 C CNN
F 2 "Package_BGA:BGA-484_23.0x23.0mm_Layout22x22_P1.0mm" H 4900 2200 50  0001 C CNN
F 3 "" H 4900 2200 50  0000 C CNN
	2    4900 2200
	1    0    0    -1  
$EndComp
$Comp
L FEAD-rescue:XC7K70T-FBG484-MDJ_compo U301
U 3 1 5C9133EC
P 1875 5825
F 0 "U301" H 1875 4053 50  0000 C CNN
F 1 "XC7K70T-FBG484" H 1875 3962 50  0000 C CNN
F 2 "Package_BGA:BGA-484_23.0x23.0mm_Layout22x22_P1.0mm" H 1875 5825 50  0001 C CNN
F 3 "" H 1875 5825 50  0000 C CNN
	3    1875 5825
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 2950 3800 2950
Wire Wire Line
	3950 2900 3800 2900
Text Label 3800 2900 2    30   ~ 0
SFP_SDA
Text Label 3800 2950 2    30   ~ 0
SFP_SCL
Text Label 3800 2850 2    30   ~ 0
SFP_LOS
Text Label 3800 2800 2    30   ~ 0
SFP_Present
Wire Wire Line
	1975 4575 2025 4575
Wire Wire Line
	1525 4575 1575 4575
Wire Wire Line
	875  6775 925  6775
Wire Wire Line
	875  6725 925  6725
Text Label 875  6775 2    30   ~ 0
Ch_in_N1
Text Label 875  6725 2    30   ~ 0
Ch_in_P1
Wire Wire Line
	875  5175 925  5175
Wire Wire Line
	875  5125 925  5125
Text Label 875  5175 2    30   ~ 0
Ch_in_N2
Text Label 875  5125 2    30   ~ 0
Ch_in_P2
Wire Wire Line
	875  4975 925  4975
Wire Wire Line
	875  4925 925  4925
Text Label 875  4975 2    30   ~ 0
Ch_in_N3
Text Label 875  4925 2    30   ~ 0
Ch_in_P3
Wire Wire Line
	875  4875 925  4875
Wire Wire Line
	875  4825 925  4825
Text Label 875  4875 2    30   ~ 0
Ch_in_N5
Text Label 875  4825 2    30   ~ 0
Ch_in_P5
Wire Wire Line
	875  5075 925  5075
Wire Wire Line
	875  5025 925  5025
Text Label 875  5075 2    30   ~ 0
Ch_in_N4
Text Label 875  5025 2    30   ~ 0
Ch_in_P4
Connection ~ 1525 4575
Connection ~ 1575 4575
Wire Wire Line
	1575 4575 1625 4575
Connection ~ 1625 4575
Wire Wire Line
	1625 4575 1675 4575
Connection ~ 1675 4575
Wire Wire Line
	1675 4575 1725 4575
Connection ~ 1725 4575
Wire Wire Line
	1725 4575 1775 4575
Connection ~ 2025 4575
Wire Wire Line
	2025 4575 2075 4575
Connection ~ 2075 4575
Wire Wire Line
	2075 4575 2125 4575
Connection ~ 2125 4575
Wire Wire Line
	2125 4575 2175 4575
Connection ~ 2175 4575
Wire Wire Line
	2175 4575 2225 4575
Connection ~ 2225 4575
Wire Wire Line
	1975 950  2025 950 
Wire Wire Line
	1525 950  1575 950 
Wire Wire Line
	5000 950  5050 950 
Wire Wire Line
	4550 950  4600 950 
Connection ~ 2025 950 
Wire Wire Line
	2025 950  2075 950 
Connection ~ 2075 950 
Wire Wire Line
	2075 950  2125 950 
Connection ~ 2125 950 
Wire Wire Line
	2125 950  2175 950 
Connection ~ 2175 950 
Wire Wire Line
	2175 950  2225 950 
Connection ~ 2225 950 
Connection ~ 1525 950 
Connection ~ 1575 950 
Wire Wire Line
	1575 950  1625 950 
Connection ~ 1625 950 
Wire Wire Line
	1625 950  1675 950 
Connection ~ 1675 950 
Wire Wire Line
	1675 950  1725 950 
Connection ~ 1725 950 
Wire Wire Line
	1725 950  1775 950 
Connection ~ 4550 950 
Connection ~ 4600 950 
Wire Wire Line
	4600 950  4650 950 
Connection ~ 4650 950 
Wire Wire Line
	4650 950  4700 950 
Connection ~ 4700 950 
Wire Wire Line
	4700 950  4750 950 
Connection ~ 4750 950 
Wire Wire Line
	4750 950  4800 950 
Connection ~ 5050 950 
Wire Wire Line
	5050 950  5100 950 
Connection ~ 5100 950 
Wire Wire Line
	5100 950  5150 950 
Connection ~ 5150 950 
Wire Wire Line
	5150 950  5150 975 
$Comp
L power1:VccO #PWR0405
U 1 1 5D24BBA4
P 4550 900
F 0 "#PWR0405" H 4550 750 50  0001 C CNN
F 1 "VccO" H 4550 1050 50  0000 C CNN
F 2 "" H 4550 900 50  0000 C CNN
F 3 "" H 4550 900 50  0000 C CNN
	1    4550 900 
	1    0    0    -1  
$EndComp
$Comp
L FEAD-rescue:VccO_VFE-MDJ_compo #PWR0410
U 1 1 5D40898F
P 9150 1475
F 0 "#PWR0410" H 9150 1325 50  0001 C CNN
F 1 "VccO_VFE" H 9150 1625 50  0000 C CNN
F 2 "" H 9150 1475 50  0000 C CNN
F 3 "" H 9150 1475 50  0000 C CNN
	1    9150 1475
	1    0    0    -1  
$EndComp
$Comp
L FEAD-rescue:VccO_VFE-MDJ_compo #PWR0402
U 1 1 5D408B85
P 1525 4525
F 0 "#PWR0402" H 1525 4375 50  0001 C CNN
F 1 "VccO_VFE" H 1525 4675 50  0000 C CNN
F 2 "" H 1525 4525 50  0000 C CNN
F 3 "" H 1525 4525 50  0000 C CNN
	1    1525 4525
	1    0    0    -1  
$EndComp
$Comp
L FEAD-rescue:VccO_FE-MDJ_compo #PWR0412
U 1 1 5D4090C2
P 10175 1475
F 0 "#PWR0412" H 10175 1325 50  0001 C CNN
F 1 "VccO_FE" H 10190 1648 50  0000 C CNN
F 2 "" H 10175 1475 50  0000 C CNN
F 3 "" H 10175 1475 50  0000 C CNN
	1    10175 1475
	1    0    0    -1  
$EndComp
$Comp
L FEAD-rescue:VccO_FE-MDJ_compo #PWR0404
U 1 1 5D40915D
P 2225 4525
F 0 "#PWR0404" H 2225 4375 50  0001 C CNN
F 1 "VccO_FE" H 2240 4698 50  0000 C CNN
F 2 "" H 2225 4525 50  0000 C CNN
F 3 "" H 2225 4525 50  0000 C CNN
	1    2225 4525
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 2800 3800 2800
Wire Wire Line
	3950 2850 3800 2850
Text HLabel 8325 4225 2    39   Output ~ 0
ReSync_out+
Text HLabel 8325 4300 2    39   Output ~ 0
ReSync_out-
Text HLabel 8325 4825 2    39   Output ~ 0
CalibMode_out
Text HLabel 8325 4900 2    39   Output ~ 0
TP_trigger_out
Text HLabel 8325 4975 2    39   Output ~ 0
TestMode_out
Text HLabel 8325 4375 2    39   Input ~ 0
PllLock_in_[1..5]
Text HLabel 8325 4450 2    39   Input ~ 0
CalBusy_in_[1..5]
Text HLabel 8325 4525 2    39   Input ~ 0
OVF_in_[1..5]
Text HLabel 10250 4375 2    39   Output ~ 0
PllLock_out_[1..5]
Text HLabel 10250 4450 2    39   Output ~ 0
CalBusy_out_[1..5]
Text HLabel 10250 4525 2    39   Output ~ 0
OVF_out_[1..5]
Text HLabel 10250 4825 2    39   Input ~ 0
CalibMode_in
Text HLabel 10250 4900 2    39   Input ~ 0
TP_trigger_in
Text HLabel 10250 4975 2    39   Input ~ 0
TestMode_in
Text HLabel 10250 4225 2    39   Input ~ 0
ReSync_in+
Text HLabel 10250 4300 2    39   Input ~ 0
ReSync_in-
Text HLabel 8325 5050 2    39   Input ~ 0
Temp_APD_in
Text HLabel 8325 5125 2    39   Input ~ 0
Temp_CATIA_in
Text HLabel 10250 5050 2    39   Output ~ 0
Temp_APD_out
Text HLabel 10250 5125 2    39   Output ~ 0
Temp_CATIA_out
Text HLabel 10250 5400 2    39   Output ~ 0
Clk_Select
Wire Wire Line
	875  6975 925  6975
Wire Wire Line
	875  6925 925  6925
Text Label 875  6975 2    30   ~ 0
ReSync_out-
Text Label 875  6925 2    30   ~ 0
ReSync_out+
Wire Wire Line
	2875 4975 2825 4975
Wire Wire Line
	2875 4925 2825 4925
Text Label 2875 4975 0    30   ~ 0
ReSync_in-
Text Label 2875 4925 0    30   ~ 0
ReSync_in+
Text Label 875  1250 2    30   ~ 0
PllLock_in_1
Text Label 875  1300 2    30   ~ 0
PllLock_in_2
Wire Wire Line
	925  1250 875  1250
Wire Wire Line
	925  1300 875  1300
Text Label 875  1350 2    30   ~ 0
PllLock_in_3
Text Label 875  1400 2    30   ~ 0
PllLock_in_4
Wire Wire Line
	925  1350 875  1350
Wire Wire Line
	925  1400 875  1400
Text Label 875  1450 2    30   ~ 0
PllLock_in_5
Text Label 875  1500 2    30   ~ 0
CalBusy_in_1
Wire Wire Line
	925  1450 875  1450
Wire Wire Line
	925  1500 875  1500
Text Label 875  1550 2    30   ~ 0
CalBusy_in_2
Wire Wire Line
	925  1550 875  1550
Text Label 875  1600 2    30   ~ 0
CalBusy_in_3
Wire Wire Line
	925  1600 875  1600
Text Label 875  1650 2    30   ~ 0
CalBusy_in_4
Wire Wire Line
	925  1650 875  1650
Text Label 875  1700 2    30   ~ 0
CalBusy_in_5
Wire Wire Line
	925  1700 875  1700
Text Label 875  1750 2    30   ~ 0
OVF_in_1
Wire Wire Line
	925  1750 875  1750
Text Label 875  1800 2    30   ~ 0
OVF_in_2
Wire Wire Line
	925  1800 875  1800
Text Label 875  1850 2    30   ~ 0
OVF_in_3
Wire Wire Line
	925  1850 875  1850
Text Label 875  1900 2    30   ~ 0
OVF_in_4
Wire Wire Line
	925  1900 875  1900
Text Label 875  1950 2    30   ~ 0
OVF_in_5
Wire Wire Line
	925  1950 875  1950
Text Label 875  2000 2    30   ~ 0
SEUA_in_1
Wire Wire Line
	925  2000 875  2000
Text Label 875  2050 2    30   ~ 0
SEUA_in_2
Wire Wire Line
	925  2050 875  2050
Text Label 875  2100 2    30   ~ 0
SEUA_in_3
Wire Wire Line
	925  2100 875  2100
Text Label 875  2150 2    30   ~ 0
SEUA_in_4
Wire Wire Line
	925  2150 875  2150
Text Label 875  3600 2    30   ~ 0
SEUA_in_5
Wire Wire Line
	925  3600 875  3600
Text Label 875  2250 2    30   ~ 0
SEUD_in_1
Wire Wire Line
	925  2250 875  2250
Text Label 875  2300 2    30   ~ 0
SEUD_in_2
Wire Wire Line
	925  2300 875  2300
Text Label 875  2350 2    30   ~ 0
SEUD_in_3
Wire Wire Line
	925  2350 875  2350
Text Label 875  2400 2    30   ~ 0
SEUD_in_4
Wire Wire Line
	925  2400 875  2400
Text Label 875  3550 2    30   ~ 0
SEUD_in_5
Wire Wire Line
	925  3550 875  3550
Text Label 5900 1150 0    30   ~ 0
I2C_data_in
Text Label 5900 1200 0    30   ~ 0
I2C_clk_in
Wire Wire Line
	5850 1150 5900 1150
Wire Wire Line
	5850 1200 5900 1200
Text Label 5900 1250 0    30   ~ 0
PllLock_out_1
Text Label 5900 1300 0    30   ~ 0
PllLock_out_2
Wire Wire Line
	5850 1250 5900 1250
Wire Wire Line
	5850 1300 5900 1300
Text Label 5900 1350 0    30   ~ 0
PllLock_out_3
Text Label 5900 1400 0    30   ~ 0
PllLock_out_4
Wire Wire Line
	5850 1350 5900 1350
Wire Wire Line
	5850 1400 5900 1400
Text Label 5900 1450 0    30   ~ 0
PllLock_out_5
Text Label 5900 1500 0    30   ~ 0
CalBusy_out_1
Wire Wire Line
	5850 1450 5900 1450
Wire Wire Line
	5850 1500 5900 1500
Text Label 5900 1550 0    30   ~ 0
CalBusy_out_2
Wire Wire Line
	5850 1550 5900 1550
Text Label 5900 1600 0    30   ~ 0
CalBusy_out_3
Wire Wire Line
	5850 1600 5900 1600
Text Label 5900 1650 0    30   ~ 0
CalBusy_out_4
Wire Wire Line
	5850 1650 5900 1650
Text Label 5900 1700 0    30   ~ 0
CalBusy_out_5
Wire Wire Line
	5850 1700 5900 1700
Text Label 5900 1750 0    30   ~ 0
OVF_out_1
Wire Wire Line
	5850 1750 5900 1750
Text Label 5900 1800 0    30   ~ 0
OVF_out_2
Wire Wire Line
	5850 1800 5900 1800
Text Label 5900 1850 0    30   ~ 0
OVF_out_3
Wire Wire Line
	5850 1850 5900 1850
Text Label 5900 1900 0    30   ~ 0
OVF_out_4
Wire Wire Line
	5850 1900 5900 1900
Text Label 5900 1950 0    30   ~ 0
OVF_out_5
Wire Wire Line
	5850 1950 5900 1950
Text Label 5900 2000 0    30   ~ 0
SEUA_out_1
Wire Wire Line
	5850 2000 5900 2000
Text Label 5900 2050 0    30   ~ 0
SEUA_out_2
Wire Wire Line
	5850 2050 5900 2050
Text Label 5900 2100 0    30   ~ 0
SEUA_out_3
Wire Wire Line
	5850 2100 5900 2100
Text Label 5900 2150 0    30   ~ 0
SEUA_out_4
Wire Wire Line
	5850 2150 5900 2150
Text Label 5900 2200 0    30   ~ 0
SEUA_out_5
Wire Wire Line
	5850 2200 5900 2200
Text Label 5900 2250 0    30   ~ 0
SEUD_out_1
Wire Wire Line
	5850 2250 5900 2250
Text Label 5900 2300 0    30   ~ 0
SEUD_out_2
Wire Wire Line
	5850 2300 5900 2300
Text Label 5900 2350 0    30   ~ 0
SEUD_out_3
Wire Wire Line
	5850 2350 5900 2350
Text Label 5900 2400 0    30   ~ 0
SEUD_out_4
Wire Wire Line
	5850 2400 5900 2400
Text Label 5900 2450 0    30   ~ 0
SEUD_out_5
Wire Wire Line
	5850 2450 5900 2450
Text Label 875  2500 2    30   ~ 0
Pwup_Reset_out
Wire Wire Line
	925  2500 875  2500
Text Label 5900 2500 0    30   ~ 0
Pwup_Reset_in
Wire Wire Line
	5850 2500 5900 2500
Text Label 875  2550 2    30   ~ 0
TP_trigger_out
Wire Wire Line
	925  2550 875  2550
Text Label 875  2600 2    30   ~ 0
TestMode_out
Wire Wire Line
	925  2600 875  2600
Text Label 875  2650 2    30   ~ 0
CalibMode_out
Wire Wire Line
	925  2650 875  2650
Text Label 5900 2550 0    30   ~ 0
TP_trigger_in
Wire Wire Line
	5850 2550 5900 2550
Text Label 5900 2600 0    30   ~ 0
TestMode_in
Wire Wire Line
	5850 2600 5900 2600
Text Label 5900 2650 0    30   ~ 0
CalibMode_in
Wire Wire Line
	5850 2650 5900 2650
Text Label 3800 2400 2    30   ~ 0
CLK_in+
Text Label 3800 2450 2    30   ~ 0
CLK_in-
Wire Wire Line
	3800 2450 3950 2450
Wire Wire Line
	3800 2400 3950 2400
$Comp
L power1:VccO #PWR0403
U 1 1 5D16FD52
P 2225 900
F 0 "#PWR0403" H 2225 750 50  0001 C CNN
F 1 "VccO" H 2225 1050 50  0000 C CNN
F 2 "" H 2225 900 50  0000 C CNN
F 3 "" H 2225 900 50  0000 C CNN
	1    2225 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 1200 3800 1200
Text Label 3800 1200 2    30   ~ 0
Temp_APD_in
Wire Wire Line
	3950 1600 3800 1600
Text Label 3800 1600 2    30   ~ 0
Temp_APD_out
Wire Wire Line
	3950 1400 3800 1400
Text Label 3800 1400 2    30   ~ 0
Temp_CATIA_in
Wire Wire Line
	3950 2100 3800 2100
Text Label 3800 2100 2    30   ~ 0
Temp_CATIA_out
NoConn ~ 2825 1150
NoConn ~ 2825 1300
NoConn ~ 2825 1350
NoConn ~ 2825 1400
NoConn ~ 2825 1450
NoConn ~ 2825 1500
NoConn ~ 2825 1550
NoConn ~ 2825 1600
NoConn ~ 2825 1650
NoConn ~ 2825 1750
NoConn ~ 2825 1800
NoConn ~ 2825 1850
NoConn ~ 2825 1900
NoConn ~ 2825 1950
NoConn ~ 2825 2000
NoConn ~ 2825 2050
NoConn ~ 2825 2100
NoConn ~ 2825 2150
NoConn ~ 2825 2200
NoConn ~ 2825 2250
NoConn ~ 2825 2300
NoConn ~ 2825 2350
NoConn ~ 2825 2400
NoConn ~ 2825 2450
NoConn ~ 2825 2500
NoConn ~ 2825 2550
NoConn ~ 2825 2600
NoConn ~ 2825 2650
NoConn ~ 2825 2700
NoConn ~ 2825 2750
NoConn ~ 2825 2800
NoConn ~ 2825 2850
NoConn ~ 2825 2900
NoConn ~ 2825 2950
NoConn ~ 2825 3000
NoConn ~ 2825 3050
NoConn ~ 2825 3100
NoConn ~ 2825 3150
NoConn ~ 2825 3200
NoConn ~ 2825 3250
NoConn ~ 2825 3300
NoConn ~ 2825 3350
NoConn ~ 2825 3400
NoConn ~ 2825 3450
NoConn ~ 2825 3500
NoConn ~ 2825 3600
NoConn ~ 3950 3600
NoConn ~ 3950 2650
NoConn ~ 3950 2600
NoConn ~ 3950 2550
NoConn ~ 3950 2500
NoConn ~ 3950 2350
NoConn ~ 3950 2300
NoConn ~ 3950 2250
NoConn ~ 3950 2200
NoConn ~ 3950 2050
NoConn ~ 3950 2000
NoConn ~ 3950 1950
NoConn ~ 3950 1850
NoConn ~ 3950 1800
NoConn ~ 3950 1750
NoConn ~ 3950 1700
NoConn ~ 3950 1550
NoConn ~ 3950 1500
NoConn ~ 3950 1350
NoConn ~ 3950 1300
NoConn ~ 5850 2700
NoConn ~ 5850 2750
NoConn ~ 5850 2850
NoConn ~ 5850 2800
NoConn ~ 925  2700
NoConn ~ 925  2750
NoConn ~ 925  2800
NoConn ~ 925  2850
NoConn ~ 925  2900
NoConn ~ 925  2950
NoConn ~ 925  3000
NoConn ~ 925  3050
NoConn ~ 925  3100
NoConn ~ 925  3150
NoConn ~ 925  3200
NoConn ~ 925  3250
NoConn ~ 925  3300
NoConn ~ 925  3350
NoConn ~ 925  3400
NoConn ~ 925  3450
NoConn ~ 925  3500
Text HLabel 10250 5625 2    39   Input ~ 0
GPIO[0..3]
NoConn ~ 925  5375
NoConn ~ 925  6025
NoConn ~ 925  6075
NoConn ~ 925  6125
NoConn ~ 925  6875
NoConn ~ 925  5225
NoConn ~ 925  5275
NoConn ~ 925  5325
NoConn ~ 925  5425
NoConn ~ 925  5475
NoConn ~ 925  5525
NoConn ~ 925  5575
NoConn ~ 925  5625
NoConn ~ 925  5675
NoConn ~ 925  5725
NoConn ~ 925  5775
NoConn ~ 925  5825
NoConn ~ 925  5875
NoConn ~ 925  5975
NoConn ~ 925  5925
NoConn ~ 925  6175
NoConn ~ 925  6225
NoConn ~ 925  6275
NoConn ~ 925  6325
NoConn ~ 925  6375
NoConn ~ 925  6425
NoConn ~ 925  6475
NoConn ~ 925  6525
NoConn ~ 925  6575
NoConn ~ 925  6625
NoConn ~ 925  6675
NoConn ~ 925  6825
NoConn ~ 925  7025
NoConn ~ 925  7075
NoConn ~ 925  7175
NoConn ~ 925  7125
NoConn ~ 2825 4825
NoConn ~ 2825 4875
NoConn ~ 2825 5125
NoConn ~ 2825 5175
NoConn ~ 2825 5225
NoConn ~ 2825 5275
NoConn ~ 2825 5325
NoConn ~ 2825 5375
NoConn ~ 2825 5425
NoConn ~ 2825 5475
NoConn ~ 2825 5625
NoConn ~ 2825 5675
NoConn ~ 2825 5825
NoConn ~ 2825 5875
NoConn ~ 2825 5925
NoConn ~ 2825 5975
NoConn ~ 2825 6025
NoConn ~ 2825 6075
NoConn ~ 2825 6125
NoConn ~ 2825 6175
NoConn ~ 2825 6225
NoConn ~ 2825 6275
NoConn ~ 2825 6325
NoConn ~ 2825 6375
NoConn ~ 2825 6425
NoConn ~ 2825 6475
NoConn ~ 2825 6525
NoConn ~ 2825 6575
NoConn ~ 2825 6625
NoConn ~ 2825 6675
NoConn ~ 2825 6825
NoConn ~ 2825 6875
NoConn ~ 2825 7025
NoConn ~ 2825 7075
NoConn ~ 2825 7125
NoConn ~ 2825 7175
NoConn ~ 3950 1150
NoConn ~ 3950 1900
NoConn ~ 925  2450
NoConn ~ 925  2200
$Comp
L Device:R R?
U 1 1 5C89AD8F
P 650 4625
AR Path="/5BAC43A6/5C89AD8F" Ref="R?"  Part="1" 
AR Path="/57195CF6/5C89AD8F" Ref="R401"  Part="1" 
F 0 "R401" V 725 4625 50  0000 C CNN
F 1 "100" V 650 4625 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" H 650 4625 50  0001 C CNN
F 3 "" H 650 4625 50  0000 C CNN
	1    650  4625
	1    0    0    -1  
$EndComp
$Comp
L FEAD-rescue:VccO_VFE-MDJ_compo #PWR0115
U 1 1 5C8A7974
P 650 4475
F 0 "#PWR0115" H 650 4325 50  0001 C CNN
F 1 "VccO_VFE" H 650 4625 39  0000 C CNN
F 2 "" H 650 4475 50  0000 C CNN
F 3 "" H 650 4475 50  0000 C CNN
	1    650  4475
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5C8A7B86
P 650 7375
AR Path="/5BAC43A6/5C8A7B86" Ref="R?"  Part="1" 
AR Path="/57195CF6/5C8A7B86" Ref="R402"  Part="1" 
F 0 "R402" V 725 7375 50  0000 C CNN
F 1 "100" V 650 7375 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" H 650 7375 50  0001 C CNN
F 3 "" H 650 7375 50  0000 C CNN
	1    650  7375
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5C8C2BD3
P 3075 4625
AR Path="/5BAC43A6/5C8C2BD3" Ref="R?"  Part="1" 
AR Path="/57195CF6/5C8C2BD3" Ref="R403"  Part="1" 
F 0 "R403" V 3150 4625 50  0000 C CNN
F 1 "100" V 3075 4625 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" H 3075 4625 50  0001 C CNN
F 3 "" H 3075 4625 50  0000 C CNN
	1    3075 4625
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5C8D95F6
P 3125 7375
AR Path="/5BAC43A6/5C8D95F6" Ref="R?"  Part="1" 
AR Path="/57195CF6/5C8D95F6" Ref="R404"  Part="1" 
F 0 "R404" V 3200 7375 50  0000 C CNN
F 1 "100" V 3125 7375 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" H 3125 7375 50  0001 C CNN
F 3 "" H 3125 7375 50  0000 C CNN
	1    3125 7375
	-1   0    0    -1  
$EndComp
$Comp
L FEAD-rescue:VccO_FE-MDJ_compo #PWR0117
U 1 1 5C8FD426
P 3075 4475
F 0 "#PWR0117" H 3075 4325 50  0001 C CNN
F 1 "VccO_FE" H 3090 4648 39  0000 C CNN
F 2 "" H 3075 4475 50  0000 C CNN
F 3 "" H 3075 4475 50  0000 C CNN
	1    3075 4475
	1    0    0    -1  
$EndComp
Wire Wire Line
	650  4775 925  4775
Text Label 725  4775 0    30   ~ 0
Vr_vfe_N
Wire Wire Line
	650  7225 925  7225
Text Label 700  7225 0    30   ~ 0
Vr_vfe_P
Wire Wire Line
	2825 7225 3125 7225
Text Label 2875 7225 0    30   ~ 0
Vr_fe_P
Text Label 2850 4775 0    30   ~ 0
Vr_fe_N
Wire Wire Line
	2825 4775 3075 4775
$Comp
L power1:GND #PWR0116
U 1 1 5C981397
P 3125 7525
F 0 "#PWR0116" H 3125 7275 50  0001 C CNN
F 1 "GND" H 3125 7375 50  0000 C CNN
F 2 "" H 3125 7525 50  0000 C CNN
F 3 "" H 3125 7525 50  0000 C CNN
	1    3125 7525
	1    0    0    -1  
$EndComp
$Comp
L power1:GND #PWR0118
U 1 1 5C98159A
P 650 7525
F 0 "#PWR0118" H 650 7275 50  0001 C CNN
F 1 "GND" H 650 7375 50  0000 C CNN
F 2 "" H 650 7525 50  0000 C CNN
F 3 "" H 650 7525 50  0000 C CNN
	1    650  7525
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 1250 3900 1250
Wire Wire Line
	3900 1250 3900 1450
Wire Wire Line
	3950 2150 3900 2150
Connection ~ 3900 2150
Wire Wire Line
	3900 2150 3900 2200
Wire Wire Line
	3950 1650 3900 1650
Connection ~ 3900 1650
Wire Wire Line
	3900 1650 3900 2150
Wire Wire Line
	3950 1450 3900 1450
Connection ~ 3900 1450
Wire Wire Line
	3900 1450 3900 1650
$Comp
L power1:GND #PWR0119
U 1 1 5C93B016
P 3900 2200
F 0 "#PWR0119" H 3900 1950 50  0001 C CNN
F 1 "GND" H 3900 2050 39  0000 C CNN
F 2 "" H 3900 2200 50  0000 C CNN
F 3 "" H 3900 2200 50  0000 C CNN
	1    3900 2200
	1    0    0    -1  
$EndComp
Text Label 3800 2750 2    30   ~ 0
SFP_Tx_Fault
Wire Wire Line
	3950 2750 3800 2750
Text Label 3800 2700 2    30   ~ 0
Clk_Select
Wire Wire Line
	3950 2700 3800 2700
NoConn ~ 2825 3550
$EndSCHEMATC
