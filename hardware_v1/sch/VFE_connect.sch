EESchema Schematic File Version 4
LIBS:FEAD-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L FEAD-rescue:ERNI-154742-MDJ_compo J1001
U 1 1 57059304
P 2200 2200
F 0 "J1001" H 2200 750 50  0000 C CNN
F 1 "ERNI-154742" V 2200 2200 50  0000 C CNN
F 2 "MDJ_mod:ERNI-154742" H 2200 625 50  0000 C CNN
F 3 "" H 2100 2200 50  0000 C CNN
	1    2200 2200
	-1   0    0    -1  
$EndComp
$Comp
L power1:GND #PWR01004
U 1 1 57059436
P 1525 3625
F 0 "#PWR01004" H 1525 3375 50  0001 C CNN
F 1 "GND" H 1525 3475 50  0000 C CNN
F 2 "" H 1525 3625 50  0000 C CNN
F 3 "" H 1525 3625 50  0000 C CNN
	1    1525 3625
	-1   0    0    -1  
$EndComp
Text HLabel 1450 1000 0    50   Input ~ 0
I2C_clk_out
Text HLabel 1450 1100 0    50   BiDi ~ 0
I2C_data_out
Text HLabel 2950 1300 2    50   Input ~ 0
ReSync_out-
Text HLabel 2975 6400 2    50   Input ~ 0
Pwup_rstb_out
$Comp
L power1:VFE_VD #PWR01006
U 1 1 5B3CCD9E
P 875 3400
F 0 "#PWR01006" H 875 3250 50  0001 C CNN
F 1 "VFE_VD" H 875 3540 50  0000 C CNN
F 2 "" H 875 3400 50  0000 C CNN
F 3 "" H 875 3400 50  0000 C CNN
	1    875  3400
	-1   0    0    -1  
$EndComp
$Comp
L power1:VFE_VD #PWR01001
U 1 1 5B3CCDDC
P 3525 3400
F 0 "#PWR01001" H 3525 3250 50  0001 C CNN
F 1 "VFE_VD" H 3525 3540 50  0000 C CNN
F 2 "" H 3525 3400 50  0000 C CNN
F 3 "" H 3525 3400 50  0000 C CNN
	1    3525 3400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1525 3500 1525 3600
Wire Wire Line
	2750 1300 2950 1300
Wire Wire Line
	2950 1200 2750 1200
Wire Wire Line
	3375 2100 2750 2100
Wire Wire Line
	3375 1800 2750 1800
Wire Wire Line
	2750 1900 3375 1900
Wire Wire Line
	2750 1500 3375 1500
Wire Wire Line
	3375 2900 2750 2900
Wire Wire Line
	3375 2600 2750 2600
Wire Wire Line
	1650 2900 1025 2900
Wire Wire Line
	2750 2700 3375 2700
Wire Wire Line
	2750 6400 2975 6400
Wire Wire Line
	1650 3400 875  3400
Wire Wire Line
	2750 3400 3525 3400
Text HLabel 2950 1200 2    50   Input ~ 0
ReSync_out+
Text Label 3375 2100 2    50   ~ 0
CalBusy_in_1
Text Label 3375 1800 2    50   ~ 0
Ch_in_P1
Text Label 3375 1500 2    50   ~ 0
CLK_out_P1
Text Label 3375 2700 2    50   ~ 0
Ch_in_N2
Text Label 3375 2300 2    50   ~ 0
CLK_out_P2
Text Label 3375 2900 2    50   ~ 0
CalBusy_in_2
Text HLabel 4675 3700 2    50   Output ~ 0
Ch_in_P[1..5]
$Comp
L FEAD-rescue:ERNI-154742-MDJ_compo J?
U 1 1 5BC577B4
P 2200 5400
AR Path="/570DF051/5BC577B4" Ref="J?"  Part="1" 
AR Path="/57059335/5BC577B4" Ref="J1002"  Part="1" 
AR Path="/5BC577B4" Ref="J1002"  Part="1" 
F 0 "J1002" H 2200 3950 50  0000 C CNN
F 1 "ERNI-154742" V 2200 5400 50  0000 C CNN
F 2 "MDJ_mod:ERNI-154742" H 2200 3825 50  0000 C CNN
F 3 "" H 2100 5400 50  0000 C CNN
	1    2200 5400
	-1   0    0    -1  
$EndComp
$Comp
L power1:GND #PWR?
U 1 1 5BC577BB
P 1525 6850
AR Path="/570DF051/5BC577BB" Ref="#PWR?"  Part="1" 
AR Path="/57059335/5BC577BB" Ref="#PWR01005"  Part="1" 
F 0 "#PWR01005" H 1525 6600 50  0001 C CNN
F 1 "GND" H 1525 6700 50  0000 C CNN
F 2 "" H 1525 6850 50  0000 C CNN
F 3 "" H 1525 6850 50  0000 C CNN
	1    1525 6850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1525 6700 1525 6800
Connection ~ 1525 6700
Wire Wire Line
	1650 6500 1450 6500
Wire Wire Line
	1650 6600 1450 6600
Wire Wire Line
	2750 6600 2975 6600
Wire Wire Line
	1650 4200 875  4200
Wire Wire Line
	875  4200 875  4150
Wire Wire Line
	1525 4100 1650 4100
Wire Wire Line
	2750 4200 3550 4200
Wire Wire Line
	3550 4200 3550 4150
Wire Wire Line
	2750 4100 2900 4100
$Comp
L power1:VFE_VD #PWR?
U 1 1 5BC577F6
P 875 4150
AR Path="/570DF051/5BC577F6" Ref="#PWR?"  Part="1" 
AR Path="/57059335/5BC577F6" Ref="#PWR01007"  Part="1" 
F 0 "#PWR01007" H 875 4000 50  0001 C CNN
F 1 "VFE_VD" H 875 4290 50  0000 C CNN
F 2 "" H 875 4150 50  0000 C CNN
F 3 "" H 875 4150 50  0000 C CNN
	1    875  4150
	-1   0    0    -1  
$EndComp
$Comp
L power1:VFE_VD #PWR?
U 1 1 5BC577FC
P 3550 4150
AR Path="/570DF051/5BC577FC" Ref="#PWR?"  Part="1" 
AR Path="/57059335/5BC577FC" Ref="#PWR01002"  Part="1" 
F 0 "#PWR01002" H 3550 4000 50  0001 C CNN
F 1 "VFE_VD" H 3550 4290 50  0000 C CNN
F 2 "" H 3550 4150 50  0000 C CNN
F 3 "" H 3550 4150 50  0000 C CNN
	1    3550 4150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1525 6700 1650 6700
Wire Wire Line
	2750 3100 3375 3100
Text Label 3375 3100 2    50   ~ 0
CLK_out_P3
Wire Wire Line
	1650 5500 1025 5500
Wire Wire Line
	2750 5300 3400 5300
Wire Wire Line
	2750 5000 3400 5000
Text Label 3400 5500 2    50   ~ 0
CalBusy_in_4
Wire Wire Line
	1650 6300 1025 6300
Wire Wire Line
	2750 6100 3400 6100
Wire Wire Line
	2750 5700 3400 5700
Text Label 3400 6300 2    50   ~ 0
CalBusy_in_5
Wire Wire Line
	3400 5500 2750 5500
Wire Wire Line
	3400 5200 2750 5200
Wire Wire Line
	3400 4900 2750 4900
Text Label 3400 5200 2    50   ~ 0
Ch_in_P4
Text Label 3400 4900 2    50   ~ 0
CLK_out_P4
Wire Wire Line
	3400 6300 2750 6300
Wire Wire Line
	3400 6000 2750 6000
Text Label 3400 6000 2    50   ~ 0
Ch_in_P5
Text Label 3400 5700 2    50   ~ 0
CLK_out_P5
Text Label 3375 1900 2    50   ~ 0
Ch_in_N1
Text Label 3375 1600 2    50   ~ 0
CLK_out_N1
Text Label 3400 4400 2    50   ~ 0
Ch_in_P3
Text Label 3400 4500 2    50   ~ 0
Ch_in_N3
Text Label 3400 4700 2    50   ~ 0
CalBusy_in_3
Text Label 3375 2400 2    50   ~ 0
CLK_out_N2
Text Label 3375 2600 2    50   ~ 0
Ch_in_P2
Text Label 3375 3200 2    50   ~ 0
CLK_out_N3
Text Label 3400 5000 2    50   ~ 0
CLK_out_N4
Text Label 3400 5800 2    50   ~ 0
CLK_out_N5
Text HLabel 4675 3775 2    50   Output ~ 0
Ch_in_N[1..5]
Wire Wire Line
	3375 2400 2750 2400
Wire Wire Line
	3375 3200 2750 3200
Wire Wire Line
	3400 4700 2750 4700
Wire Wire Line
	3400 4400 2750 4400
Wire Wire Line
	1650 1400 1025 1400
Wire Wire Line
	1650 2200 1025 2200
Wire Wire Line
	1650 3000 1025 3000
Wire Wire Line
	1650 4700 1025 4700
Wire Wire Line
	2750 4500 3400 4500
Wire Wire Line
	3400 5800 2750 5800
Wire Wire Line
	1650 4900 1025 4900
Wire Wire Line
	1650 5700 1025 5700
Wire Wire Line
	2750 2300 3375 2300
Wire Wire Line
	1650 1000 1450 1000
Wire Wire Line
	1650 1100 1450 1100
Text HLabel 2975 6600 2    50   Input ~ 0
CalibMode_out
Text HLabel 1450 6600 0    50   Input ~ 0
TestMode_out
Text HLabel 4675 4050 2    50   Output ~ 0
CalBusy_in_[1..5]
Text HLabel 1450 6500 0    50   Output ~ 0
Temp_CATIA_in
Text HLabel 4675 4150 2    50   Output ~ 0
OVF_in_[1..5]
Text HLabel 4675 4250 2    50   Output ~ 0
PllLock_in_[1..5]
Text HLabel 4675 4350 2    50   Output ~ 0
SEUA_in_[1..5]
Text HLabel 4675 4450 2    50   Output ~ 0
SEUD_in_[1..5]
Wire Wire Line
	1650 1700 1025 1700
Text Label 1025 2000 0    50   ~ 0
SEUA_in_1
Text Label 1025 1700 0    50   ~ 0
SEUD_in_1
Wire Wire Line
	1650 2500 1025 2500
Text Label 1025 2800 0    50   ~ 0
SEUA_in_2
Text Label 1025 2500 0    50   ~ 0
SEUD_in_2
Text Label 1025 4600 0    50   ~ 0
SEUA_in_3
Text Label 1025 3300 0    50   ~ 0
SEUD_in_3
Wire Wire Line
	1650 3300 1025 3300
Text Label 1025 5400 0    50   ~ 0
SEUA_in_4
Text Label 1025 5100 0    50   ~ 0
SEUD_in_4
Wire Wire Line
	1650 5100 1025 5100
Wire Wire Line
	1025 4600 1650 4600
Text Label 1025 4700 0    50   ~ 0
OVF_in_3
Text Label 1025 3000 0    50   ~ 0
PllLock_in_3
Wire Wire Line
	1025 5400 1650 5400
Text Label 3400 5300 2    50   ~ 0
Ch_in_N4
Text Label 1025 5500 0    50   ~ 0
OVF_in_4
Text Label 1025 4900 0    50   ~ 0
PllLock_in_4
Wire Wire Line
	1025 6200 1650 6200
Text Label 3400 6100 2    50   ~ 0
Ch_in_N5
Text Label 1025 6300 0    50   ~ 0
OVF_in_5
Text Label 1025 5700 0    50   ~ 0
PllLock_in_5
Text Label 1025 2100 0    50   ~ 0
OVF_in_1
Text Label 1025 1400 0    50   ~ 0
PllLock_in_1
Wire Wire Line
	1025 2800 1650 2800
Text Label 1025 2900 0    50   ~ 0
OVF_in_2
Text Label 1025 2200 0    50   ~ 0
PllLock_in_2
Wire Wire Line
	1650 5900 1025 5900
Text Label 1025 6200 0    50   ~ 0
SEUA_in_5
Text Label 1025 5900 0    50   ~ 0
SEUD_in_5
Text Notes 7375 7500 0    50   ~ 0
Version 0
Text Notes 8150 7625 0    50   ~ 0
2019/02/06
Wire Wire Line
	1650 6400 1450 6400
Text HLabel 1450 6400 0    50   Output ~ 0
Temp_APD_in
Wire Wire Line
	2750 6500 2975 6500
Text HLabel 2975 6500 2    50   Input ~ 0
TP_trigger_out
Text HLabel 4675 3950 2    50   Input ~ 0
CLK_out_N[1..5]
Text HLabel 4675 3875 2    50   Input ~ 0
CLK_out_P[1..5]
Wire Wire Line
	2750 1600 3375 1600
Wire Wire Line
	1025 2000 1650 2000
Wire Wire Line
	1650 2100 1025 2100
Wire Wire Line
	1525 4100 1525 4300
Connection ~ 1525 6800
Wire Wire Line
	1525 6800 1525 6850
Wire Wire Line
	1650 1200 1525 1200
Wire Wire Line
	1525 1200 1525 1300
Connection ~ 1525 3500
Wire Wire Line
	1525 3500 1650 3500
Wire Wire Line
	2750 1400 2875 1400
Wire Wire Line
	2875 1400 2875 1700
Wire Wire Line
	2750 3500 2875 3500
Connection ~ 2875 3500
Wire Wire Line
	2875 3500 2875 3600
Wire Wire Line
	1525 3600 2875 3600
Connection ~ 1525 3600
Wire Wire Line
	1525 3600 1525 3625
Wire Wire Line
	1650 3200 1525 3200
Connection ~ 1525 3200
Wire Wire Line
	1525 3200 1525 3500
Wire Wire Line
	1650 3100 1525 3100
Connection ~ 1525 3100
Wire Wire Line
	1525 3100 1525 3200
Wire Wire Line
	1650 1900 1525 1900
Connection ~ 1525 1900
Wire Wire Line
	1650 1800 1525 1800
Connection ~ 1525 1800
Wire Wire Line
	1525 1800 1525 1900
Wire Wire Line
	1650 1600 1525 1600
Connection ~ 1525 1600
Wire Wire Line
	1525 1600 1525 1800
Wire Wire Line
	1650 1500 1525 1500
Connection ~ 1525 1500
Wire Wire Line
	1525 1500 1525 1600
Wire Wire Line
	2750 1000 2875 1000
Wire Wire Line
	2875 1000 2875 1100
Connection ~ 2875 1400
Wire Wire Line
	2750 1100 2875 1100
Connection ~ 2875 1100
Wire Wire Line
	2875 1100 2875 1400
Wire Wire Line
	2750 1700 2875 1700
Connection ~ 2875 1700
Wire Wire Line
	2875 1700 2875 2000
Wire Wire Line
	2750 2000 2875 2000
Connection ~ 2875 2000
Wire Wire Line
	2875 2000 2875 2200
Wire Wire Line
	2750 2200 2875 2200
Connection ~ 2875 2200
Wire Wire Line
	2875 2200 2875 2500
Wire Wire Line
	2750 2500 2875 2500
Connection ~ 2875 2500
Wire Wire Line
	2875 2500 2875 2800
Wire Wire Line
	2750 2800 2875 2800
Connection ~ 2875 2800
Wire Wire Line
	2875 2800 2875 3000
Wire Wire Line
	2750 3000 2875 3000
Connection ~ 2875 3000
Wire Wire Line
	2875 3000 2875 3300
Wire Wire Line
	2750 3300 2875 3300
Connection ~ 2875 3300
Wire Wire Line
	2875 3300 2875 3500
Wire Wire Line
	1650 1300 1525 1300
Connection ~ 1525 1300
Wire Wire Line
	1525 1300 1525 1500
Wire Wire Line
	1525 1900 1525 2300
Wire Wire Line
	2900 4100 2900 4300
Wire Wire Line
	2750 4300 2900 4300
Connection ~ 2900 4300
Wire Wire Line
	2900 4300 2900 4600
Wire Wire Line
	2750 4600 2900 4600
Connection ~ 2900 4600
Wire Wire Line
	2900 4600 2900 4800
Wire Wire Line
	2900 4800 2750 4800
Connection ~ 2900 4800
Wire Wire Line
	2900 4800 2900 5100
Wire Wire Line
	2750 5100 2900 5100
Connection ~ 2900 5100
Wire Wire Line
	2900 5100 2900 5400
Wire Wire Line
	2750 5400 2900 5400
Connection ~ 2900 5400
Wire Wire Line
	2900 5400 2900 5600
Wire Wire Line
	2750 5600 2900 5600
Connection ~ 2900 5600
Wire Wire Line
	2900 5600 2900 5900
Wire Wire Line
	2750 5900 2900 5900
Connection ~ 2900 5900
Wire Wire Line
	2900 5900 2900 6200
Wire Wire Line
	2750 6200 2900 6200
Connection ~ 2900 6200
Wire Wire Line
	2900 6200 2900 6700
Wire Wire Line
	1650 6100 1525 6100
Connection ~ 1525 6100
Wire Wire Line
	1525 6100 1525 6700
Wire Wire Line
	1650 6000 1525 6000
Connection ~ 1525 6000
Wire Wire Line
	1525 6000 1525 6100
Wire Wire Line
	1650 5800 1525 5800
Connection ~ 1525 5800
Wire Wire Line
	1525 5800 1525 6000
Wire Wire Line
	1650 5600 1525 5600
Connection ~ 1525 5600
Wire Wire Line
	1525 5600 1525 5800
Wire Wire Line
	1650 5300 1525 5300
Connection ~ 1525 5300
Wire Wire Line
	1525 5300 1525 5600
Wire Wire Line
	1650 5200 1525 5200
Connection ~ 1525 5200
Wire Wire Line
	1525 5200 1525 5300
Wire Wire Line
	1650 5000 1525 5000
Connection ~ 1525 5000
Wire Wire Line
	1525 5000 1525 5200
Wire Wire Line
	1650 4800 1525 4800
Connection ~ 1525 4800
Wire Wire Line
	1525 4800 1525 5000
Wire Wire Line
	1650 4500 1525 4500
Connection ~ 1525 4500
Wire Wire Line
	1525 4500 1525 4800
Wire Wire Line
	1650 4400 1525 4400
Connection ~ 1525 4400
Wire Wire Line
	1525 4400 1525 4500
Connection ~ 1525 4300
Wire Wire Line
	1525 4300 1525 4400
Wire Wire Line
	2750 900  2875 900 
Wire Wire Line
	2875 900  2875 1000
Connection ~ 2875 1000
Wire Wire Line
	1650 900  1525 900 
Wire Wire Line
	1525 900  1525 1200
Connection ~ 1525 1200
Wire Wire Line
	1525 6800 2900 6800
Wire Wire Line
	2750 6700 2900 6700
Connection ~ 2900 6700
Wire Wire Line
	2900 6700 2900 6800
Wire Wire Line
	1650 2300 1525 2300
Connection ~ 1525 2300
Wire Wire Line
	1525 2300 1525 2400
Wire Wire Line
	1650 2400 1525 2400
Connection ~ 1525 2400
Wire Wire Line
	1525 2400 1525 2600
Wire Wire Line
	1650 2600 1525 2600
Connection ~ 1525 2600
Wire Wire Line
	1525 2600 1525 2700
Wire Wire Line
	1650 2700 1525 2700
Connection ~ 1525 2700
Wire Wire Line
	1525 2700 1525 3100
Wire Wire Line
	1525 4300 1650 4300
Text Notes 2925 7600 0    100  ~ 0
BUG !!!!!!\nNot the correct connections for\nTemp_APD, Temp_CATIA, Calib_mode,\nTest_mode and Pwup_rstb\nTo be corrected if another version is made
$EndSCHEMATC
