# FEAD project :

## hardware : Kicad files to produce the FEAD PCB.<br>
- schematicis based on VICE board schematics from Dan Gastler.<br>
Compatible with LiTE-DTU signals : e-link+control signals <br>
I2C using 1.2V signals

- pcb : 8 layers

- fab files

## firmware : Vivado files to generate the firmware
- Based on firmware developped by Dan Gastler for the VICE board and modified for CMS-ECAL TB in 2016-2017 and 2018.
Then modified for all test boards and finally adapt to VICE++ boards for TB 2021.
- Use IPbus firmware from cern. Make sure you have the latest version before compiling :
  - cd firmware
  - git clone https://github.com/ipbus/ipbus-firmware.git
  - cd ipbus-firmware
  - git checkout v1.9
- If you have already made a git pull of the project, you can :
  - git submodule update --init
  - and verfiy the version : git submodule status
- Already implemented :
  - IPBus interface with 1Gb/s ethernet links (updated to V1.9)
  - I2C master in standard mode or in restricted lpGBT mode
  - XADC interface to read CATIA temperature and other analog quantities
  - Double access RAM to acquire data streams and read them through IPbus
  - Input e-link capture @ 1280 Mb/s. Using Idelay to set in middle of data "eye", bitslip and byteslip to position the header synchronous with LHC clock
  - Automatic e-links synchronization n LiTE-DTU test mode, searching for correct words headers
  - Output e-link generation @ 1280 Mb.s to loop to input e-links. Now generating iddle patterns when calib_busy signal of LiTE-DTU is active 
  - Second I2C bus to control uLVRB. Will be used during irradiation tests to look for latch-ups and monitor the VFE consumption (use GPIO pins). 
  - Possiblity of self triggering to start DAQ upon signal reception (look at absolute signal level or at delta of signal)
  - Can drive 16-bit DAC for ADC testing using GPIO pins (incompatible with the use of uLVRB monitoring)
  - Compatible with VICE++ board : 
    - Deselect fead.xdc 
    - Select vicepp.xdc
    - Put USE_VICEPP to "true" in FEAD_IO.vhd file 
  - Finally, get rid of timing violations by putting proper clock domains asynchronous and correcting some processes which were using 2 clocks.

  - Put one more firmware version to be able to read legacy electronics with VICE board. We have SDR lines

  - Verson to drive CATIA test board (20230207)
  
## software : Code samples to access the FEAD board 
- You need to install the cactus project libraries
  - Have a look at https://ipbus.web.cern.ch
  - You have to install :
    - boost and boost-devel
    - libxml2 libxml2-devel
    - pugixml pugixml-devel
    - erlang
- There are 2 tyes of code : one version in command line and on version with a GUI. In order to compile, you have to simlink src to src.cli or src.gui.
The GUI version is much more developped than the CLI. You can define the VFE settings in an xml file (have a look in the software/xml directory).
- src.gui:
  - gdaq.VFE.cxx : main program
  - gdaq.menu.cc : definition of GTK menus
  - config_IO.cc : mange xml files and program CATIAs and LiTE-DTUs accordingly
  - init_gdaq.cc : Init IPbus connection with FEAD/VICEPP board
  - init_VFE.cc  : Init VFE board after register settings (link synchronization, ADC calibration, etc)
  - ADC_registers.cc : Dump or load ADC registers in/from file to restore "golden" calibration
  - CATIA_calibration.cc : Complete procedure to calibrate CATIAs (to be used with dedicated test board)
  - CRC_calc.cc : Compute CRC from incoming data and compare with LiTE-DTU one
  - get_temp.cc : Read temperature in CATIAs and FPGA
  - optimize_pedestal.cc : Get the best DAC code to define CATIA baseline around 20 ADC counts
  - reset_DTU_sync.cc 
  - SEU_test.cc : Loop over CATIA registers to detect SEU problem (to be used under irradiation)
  - sync_links.cc : Synchronize links between LiTE-DTUs and FPGA
  - calib_ADC.cc : Manage ADC calibration setting CATIA outputs to correct values
  - get_event.cc : Get events according to trigger setings
  - I2C_RW.cc : Manage I2C protocol
  - PLL_scan.cc : Scan all PLL codes and define the best one for link stability
  - send_ReSync_command.cc : Send command through ReSync line to LiTE-DTU
  - switch_power.cc : Manage uLVRB power

- src.cli :
  - src/debug_XADC.cxx : used to access XADC in FPGA
  - src/debug_elink.cxx : test auto-synchro on elinks idle patterns
  - src/debug_I2C.cxx : test I2 protocol
  - src/Test_SEU.cxx : program to monitor SEU/SEL under irradiations (mainly I2C accesses)
  - src/debug_catia_AD9642.cxx : program to read VFE boards with commercial ADCs <br>
  This code is also usable to read legacy electronics. See top of cxx file.
  - src/debug_catia_LiTE-DTU.cxx : program to read VFE boards with LiTE-DTU
  - src/debug_VFE_ETHZ.cxx : program to read 5-channels VFE boards with CATIA and LiTE-DTU
  - src/debug_VFE.cxx : program to read 5-channels VFE boards with CATIA and LiTE-DTU
  - src/debug_TT.cxx : program to read 5 VFE boards with CATIA and LiTE-DTU<br>

At the end, the executable is in "bin" directory. <br>

The GUI has been built using glade.<br>

Nothink is perfect, so any feedback is welcome.<br>

M.D.

Initial creation : 2019/05/24 <br>
Last Modification : 2023/02/07
