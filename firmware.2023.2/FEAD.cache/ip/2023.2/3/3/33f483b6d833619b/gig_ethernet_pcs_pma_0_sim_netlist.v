// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// Copyright 2022-2023 Advanced Micro Devices, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2023.2 (lin64) Build 4029153 Fri Oct 13 20:13:54 MDT 2023
// Date        : Wed Jan 17 16:49:37 2024
// Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 38 (Thirty Eight)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ gig_ethernet_pcs_pma_0_sim_netlist.v
// Design      : gig_ethernet_pcs_pma_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7k70tfbg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* EXAMPLE_SIMULATION = "0" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "gig_ethernet_pcs_pma_v16_2_15,Vivado 2023.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (gtrefclk_p,
    gtrefclk_n,
    gtrefclk_out,
    gtrefclk_bufg_out,
    txp,
    txn,
    rxp,
    rxn,
    resetdone,
    userclk_out,
    userclk2_out,
    rxuserclk_out,
    rxuserclk2_out,
    pma_reset_out,
    mmcm_locked_out,
    independent_clock_bufg,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    configuration_vector,
    status_vector,
    reset,
    signal_detect,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out);
  input gtrefclk_p;
  input gtrefclk_n;
  output gtrefclk_out;
  output gtrefclk_bufg_out;
  output txp;
  output txn;
  input rxp;
  input rxn;
  output resetdone;
  output userclk_out;
  output userclk2_out;
  output rxuserclk_out;
  output rxuserclk2_out;
  output pma_reset_out;
  output mmcm_locked_out;
  input independent_clock_bufg;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  input [4:0]configuration_vector;
  output [15:0]status_vector;
  input reset;
  input signal_detect;
  output gt0_qplloutclk_out;
  output gt0_qplloutrefclk_out;

  wire \<const0> ;
  wire [4:0]configuration_vector;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg_out;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire independent_clock_bufg;
  wire mmcm_locked_out;
  wire pma_reset_out;
  wire reset;
  wire resetdone;
  wire rxn;
  wire rxp;
  wire rxuserclk2_out;
  wire rxuserclk_out;
  wire signal_detect;
  wire [6:0]\^status_vector ;
  wire txn;
  wire txp;
  wire userclk2_out;
  wire userclk_out;
  wire [15:7]NLW_U0_status_vector_UNCONNECTED;

  assign status_vector[15] = \<const0> ;
  assign status_vector[14] = \<const0> ;
  assign status_vector[13] = \<const0> ;
  assign status_vector[12] = \<const0> ;
  assign status_vector[11] = \<const0> ;
  assign status_vector[10] = \<const0> ;
  assign status_vector[9] = \<const0> ;
  assign status_vector[8] = \<const0> ;
  assign status_vector[7] = \<const0> ;
  assign status_vector[6:0] = \^status_vector [6:0];
  GND GND
       (.G(\<const0> ));
  (* EXAMPLE_SIMULATION = "0" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support U0
       (.configuration_vector({1'b0,configuration_vector[3:1],1'b0}),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg_out(gtrefclk_bufg_out),
        .gtrefclk_n(gtrefclk_n),
        .gtrefclk_out(gtrefclk_out),
        .gtrefclk_p(gtrefclk_p),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_locked_out(mmcm_locked_out),
        .pma_reset_out(pma_reset_out),
        .reset(reset),
        .resetdone(resetdone),
        .rxn(rxn),
        .rxp(rxp),
        .rxuserclk2_out(rxuserclk2_out),
        .rxuserclk_out(rxuserclk_out),
        .signal_detect(signal_detect),
        .status_vector({NLW_U0_status_vector_UNCONNECTED[15:7],\^status_vector }),
        .txn(txn),
        .txp(txp),
        .userclk2_out(userclk2_out),
        .userclk_out(userclk_out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD
   (txn,
    txp,
    rxoutclk,
    txoutclk,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i,
    gtxe2_i_0,
    gtxe2_i_1,
    gtxe2_i_2,
    gtxe2_i_3,
    mmcm_reset,
    data_in,
    rx_fsm_reset_done_int_reg,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gtxe2_i_4,
    TXPD,
    RXPD,
    Q,
    gtxe2_i_5,
    gtxe2_i_6,
    gtxe2_i_7,
    out,
    gtxe2_i_8,
    gtxe2_i_9,
    data_sync_reg1,
    data_out);
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i;
  output [1:0]gtxe2_i_0;
  output [1:0]gtxe2_i_1;
  output [1:0]gtxe2_i_2;
  output [1:0]gtxe2_i_3;
  output mmcm_reset;
  output data_in;
  output rx_fsm_reset_done_int_reg;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gtxe2_i_4;
  input [0:0]TXPD;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_5;
  input [1:0]gtxe2_i_6;
  input [1:0]gtxe2_i_7;
  input [0:0]out;
  input gtxe2_i_8;
  input gtxe2_i_9;
  input data_sync_reg1;
  input data_out;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire data_in;
  wire data_out;
  wire data_sync_reg1;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire [15:0]gtxe2_i;
  wire [1:0]gtxe2_i_0;
  wire [1:0]gtxe2_i_1;
  wire [1:0]gtxe2_i_2;
  wire [1:0]gtxe2_i_3;
  wire gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire [1:0]gtxe2_i_7;
  wire gtxe2_i_8;
  wire gtxe2_i_9;
  wire independent_clock_bufg;
  wire mmcm_reset;
  wire [0:0]out;
  wire reset;
  wire reset_out;
  wire rx_fsm_reset_done_int_reg;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_init U0
       (.D(D),
        .Q(Q),
        .RXBUFSTATUS(RXBUFSTATUS),
        .RXPD(RXPD),
        .TXBUFSTATUS(TXBUFSTATUS),
        .TXPD(TXPD),
        .data_in(data_in),
        .data_out(data_out),
        .data_sync_reg1(data_sync_reg1),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(gtxe2_i),
        .gtxe2_i_0(gtxe2_i_0),
        .gtxe2_i_1(gtxe2_i_1),
        .gtxe2_i_2(gtxe2_i_2),
        .gtxe2_i_3(gtxe2_i_3),
        .gtxe2_i_4(gtxe2_i_4),
        .gtxe2_i_5(gtxe2_i_5),
        .gtxe2_i_6(gtxe2_i_6),
        .gtxe2_i_7(gtxe2_i_7),
        .gtxe2_i_8(gtxe2_i_8),
        .gtxe2_i_9(gtxe2_i_9),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out),
        .reset(reset),
        .reset_out(reset_out),
        .rx_fsm_reset_done_int_reg(rx_fsm_reset_done_int_reg),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_GT
   (gtxe2_i_0,
    gt0_cpllrefclklost_i,
    txn,
    txp,
    rxoutclk,
    gtxe2_i_1,
    txoutclk,
    gtxe2_i_2,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i_3,
    gtxe2_i_4,
    gtxe2_i_5,
    gtxe2_i_6,
    gtxe2_i_7,
    independent_clock_bufg,
    cpll_pd0_i,
    cpllreset_in,
    gtrefclk_bufg,
    gtrefclk_out,
    SR,
    gt0_gttxreset_in0_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gt0_rxuserrdy_t,
    gtxe2_i_8,
    TXPD,
    gt0_txuserrdy_t,
    RXPD,
    Q,
    gtxe2_i_9,
    gtxe2_i_10,
    gtxe2_i_11);
  output gtxe2_i_0;
  output gt0_cpllrefclklost_i;
  output txn;
  output txp;
  output rxoutclk;
  output gtxe2_i_1;
  output txoutclk;
  output gtxe2_i_2;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i_3;
  output [1:0]gtxe2_i_4;
  output [1:0]gtxe2_i_5;
  output [1:0]gtxe2_i_6;
  output [1:0]gtxe2_i_7;
  input independent_clock_bufg;
  input cpll_pd0_i;
  input cpllreset_in;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input [0:0]SR;
  input gt0_gttxreset_in0_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gt0_rxuserrdy_t;
  input gtxe2_i_8;
  input [0:0]TXPD;
  input gt0_txuserrdy_t;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_9;
  input [1:0]gtxe2_i_10;
  input [1:0]gtxe2_i_11;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]SR;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire cpll_pd0_i;
  wire cpllreset_in;
  wire gt0_cpllrefclklost_i;
  wire gt0_gttxreset_in0_out;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gt0_rxuserrdy_t;
  wire gt0_txuserrdy_t;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtxe2_i_0;
  wire gtxe2_i_1;
  wire [1:0]gtxe2_i_10;
  wire [1:0]gtxe2_i_11;
  wire gtxe2_i_2;
  wire [15:0]gtxe2_i_3;
  wire [1:0]gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire [1:0]gtxe2_i_7;
  wire gtxe2_i_8;
  wire [1:0]gtxe2_i_9;
  wire gtxe2_i_n_0;
  wire gtxe2_i_n_10;
  wire gtxe2_i_n_16;
  wire gtxe2_i_n_170;
  wire gtxe2_i_n_171;
  wire gtxe2_i_n_172;
  wire gtxe2_i_n_173;
  wire gtxe2_i_n_174;
  wire gtxe2_i_n_175;
  wire gtxe2_i_n_176;
  wire gtxe2_i_n_177;
  wire gtxe2_i_n_178;
  wire gtxe2_i_n_179;
  wire gtxe2_i_n_180;
  wire gtxe2_i_n_181;
  wire gtxe2_i_n_182;
  wire gtxe2_i_n_183;
  wire gtxe2_i_n_184;
  wire gtxe2_i_n_27;
  wire gtxe2_i_n_3;
  wire gtxe2_i_n_38;
  wire gtxe2_i_n_39;
  wire gtxe2_i_n_4;
  wire gtxe2_i_n_46;
  wire gtxe2_i_n_47;
  wire gtxe2_i_n_48;
  wire gtxe2_i_n_49;
  wire gtxe2_i_n_50;
  wire gtxe2_i_n_51;
  wire gtxe2_i_n_52;
  wire gtxe2_i_n_53;
  wire gtxe2_i_n_54;
  wire gtxe2_i_n_55;
  wire gtxe2_i_n_56;
  wire gtxe2_i_n_57;
  wire gtxe2_i_n_58;
  wire gtxe2_i_n_59;
  wire gtxe2_i_n_60;
  wire gtxe2_i_n_61;
  wire gtxe2_i_n_81;
  wire gtxe2_i_n_83;
  wire gtxe2_i_n_84;
  wire gtxe2_i_n_9;
  wire independent_clock_bufg;
  wire reset;
  wire reset_out;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;
  wire NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED;
  wire NLW_gtxe2_i_PHYSTATUS_UNCONNECTED;
  wire NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED;
  wire NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED;
  wire NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED;
  wire NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXDATAVALID_UNCONNECTED;
  wire NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXELECIDLE_UNCONNECTED;
  wire NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED;
  wire NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED;
  wire NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED;
  wire NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXQPISENN_UNCONNECTED;
  wire NLW_gtxe2_i_RXQPISENP_UNCONNECTED;
  wire NLW_gtxe2_i_RXRATEDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED;
  wire NLW_gtxe2_i_RXVALID_UNCONNECTED;
  wire NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED;
  wire NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED;
  wire NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED;
  wire NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED;
  wire NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED;
  wire NLW_gtxe2_i_TXQPISENN_UNCONNECTED;
  wire NLW_gtxe2_i_TXQPISENP_UNCONNECTED;
  wire NLW_gtxe2_i_TXRATEDONE_UNCONNECTED;
  wire [15:0]NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXCHARISK_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXCHBONDO_UNCONNECTED;
  wire [63:16]NLW_gtxe2_i_RXDATA_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXDISPERR_UNCONNECTED;
  wire [2:0]NLW_gtxe2_i_RXHEADER_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED;
  wire [2:0]NLW_gtxe2_i_RXSTATUS_UNCONNECTED;
  wire [9:0]NLW_gtxe2_i_TSTOUT_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  GTXE2_CHANNEL #(
    .ALIGN_COMMA_DOUBLE("FALSE"),
    .ALIGN_COMMA_ENABLE(10'b0001111111),
    .ALIGN_COMMA_WORD(2),
    .ALIGN_MCOMMA_DET("TRUE"),
    .ALIGN_MCOMMA_VALUE(10'b1010000011),
    .ALIGN_PCOMMA_DET("TRUE"),
    .ALIGN_PCOMMA_VALUE(10'b0101111100),
    .CBCC_DATA_SOURCE_SEL("DECODED"),
    .CHAN_BOND_KEEP_ALIGN("FALSE"),
    .CHAN_BOND_MAX_SKEW(1),
    .CHAN_BOND_SEQ_1_1(10'b0000000000),
    .CHAN_BOND_SEQ_1_2(10'b0000000000),
    .CHAN_BOND_SEQ_1_3(10'b0000000000),
    .CHAN_BOND_SEQ_1_4(10'b0000000000),
    .CHAN_BOND_SEQ_1_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_1(10'b0000000000),
    .CHAN_BOND_SEQ_2_2(10'b0000000000),
    .CHAN_BOND_SEQ_2_3(10'b0000000000),
    .CHAN_BOND_SEQ_2_4(10'b0000000000),
    .CHAN_BOND_SEQ_2_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_USE("FALSE"),
    .CHAN_BOND_SEQ_LEN(1),
    .CLK_CORRECT_USE("TRUE"),
    .CLK_COR_KEEP_IDLE("FALSE"),
    .CLK_COR_MAX_LAT(36),
    .CLK_COR_MIN_LAT(33),
    .CLK_COR_PRECEDENCE("TRUE"),
    .CLK_COR_REPEAT_WAIT(0),
    .CLK_COR_SEQ_1_1(10'b0110111100),
    .CLK_COR_SEQ_1_2(10'b0001010000),
    .CLK_COR_SEQ_1_3(10'b0000000000),
    .CLK_COR_SEQ_1_4(10'b0000000000),
    .CLK_COR_SEQ_1_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_1(10'b0110111100),
    .CLK_COR_SEQ_2_2(10'b0010110101),
    .CLK_COR_SEQ_2_3(10'b0000000000),
    .CLK_COR_SEQ_2_4(10'b0000000000),
    .CLK_COR_SEQ_2_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_USE("TRUE"),
    .CLK_COR_SEQ_LEN(2),
    .CPLL_CFG(24'hBC07DC),
    .CPLL_FBDIV(4),
    .CPLL_FBDIV_45(5),
    .CPLL_INIT_CFG(24'h00001E),
    .CPLL_LOCK_CFG(16'h01E8),
    .CPLL_REFCLK_DIV(1),
    .DEC_MCOMMA_DETECT("TRUE"),
    .DEC_PCOMMA_DETECT("TRUE"),
    .DEC_VALID_COMMA_ONLY("FALSE"),
    .DMONITOR_CFG(24'h000A00),
    .ES_CONTROL(6'b000000),
    .ES_ERRDET_EN("FALSE"),
    .ES_EYE_SCAN_EN("TRUE"),
    .ES_HORZ_OFFSET(12'h000),
    .ES_PMA_CFG(10'b0000000000),
    .ES_PRESCALE(5'b00000),
    .ES_QUALIFIER(80'h00000000000000000000),
    .ES_QUAL_MASK(80'h00000000000000000000),
    .ES_SDATA_MASK(80'h00000000000000000000),
    .ES_VERT_OFFSET(9'b000000000),
    .FTS_DESKEW_SEQ_ENABLE(4'b1111),
    .FTS_LANE_DESKEW_CFG(4'b1111),
    .FTS_LANE_DESKEW_EN("FALSE"),
    .GEARBOX_MODE(3'b000),
    .IS_CPLLLOCKDETCLK_INVERTED(1'b0),
    .IS_DRPCLK_INVERTED(1'b0),
    .IS_GTGREFCLK_INVERTED(1'b0),
    .IS_RXUSRCLK2_INVERTED(1'b0),
    .IS_RXUSRCLK_INVERTED(1'b0),
    .IS_TXPHDLYTSTCLK_INVERTED(1'b0),
    .IS_TXUSRCLK2_INVERTED(1'b0),
    .IS_TXUSRCLK_INVERTED(1'b0),
    .OUTREFCLK_SEL_INV(2'b11),
    .PCS_PCIE_EN("FALSE"),
    .PCS_RSVD_ATTR(48'h000000000000),
    .PD_TRANS_TIME_FROM_P2(12'h03C),
    .PD_TRANS_TIME_NONE_P2(8'h19),
    .PD_TRANS_TIME_TO_P2(8'h64),
    .PMA_RSV(32'h00018480),
    .PMA_RSV2(16'h2050),
    .PMA_RSV3(2'b00),
    .PMA_RSV4(32'h00000000),
    .RXBUFRESET_TIME(5'b00001),
    .RXBUF_ADDR_MODE("FULL"),
    .RXBUF_EIDLE_HI_CNT(4'b1000),
    .RXBUF_EIDLE_LO_CNT(4'b0000),
    .RXBUF_EN("TRUE"),
    .RXBUF_RESET_ON_CB_CHANGE("TRUE"),
    .RXBUF_RESET_ON_COMMAALIGN("FALSE"),
    .RXBUF_RESET_ON_EIDLE("FALSE"),
    .RXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .RXBUF_THRESH_OVFLW(61),
    .RXBUF_THRESH_OVRD("FALSE"),
    .RXBUF_THRESH_UNDFLW(8),
    .RXCDRFREQRESET_TIME(5'b00001),
    .RXCDRPHRESET_TIME(5'b00001),
    .RXCDR_CFG(72'h03000023FF10100020),
    .RXCDR_FR_RESET_ON_EIDLE(1'b0),
    .RXCDR_HOLD_DURING_EIDLE(1'b0),
    .RXCDR_LOCK_CFG(6'b010101),
    .RXCDR_PH_RESET_ON_EIDLE(1'b0),
    .RXDFELPMRESET_TIME(7'b0001111),
    .RXDLY_CFG(16'h001F),
    .RXDLY_LCFG(9'h030),
    .RXDLY_TAP_CFG(16'h0000),
    .RXGEARBOX_EN("FALSE"),
    .RXISCANRESET_TIME(5'b00001),
    .RXLPM_HF_CFG(14'b00000011110000),
    .RXLPM_LF_CFG(14'b00000011110000),
    .RXOOB_CFG(7'b0000110),
    .RXOUT_DIV(4),
    .RXPCSRESET_TIME(5'b00001),
    .RXPHDLY_CFG(24'h084020),
    .RXPH_CFG(24'h000000),
    .RXPH_MONITOR_SEL(5'b00000),
    .RXPMARESET_TIME(5'b00011),
    .RXPRBS_ERR_LOOPBACK(1'b0),
    .RXSLIDE_AUTO_WAIT(7),
    .RXSLIDE_MODE("OFF"),
    .RX_BIAS_CFG(12'b000000000100),
    .RX_BUFFER_CFG(6'b000000),
    .RX_CLK25_DIV(5),
    .RX_CLKMUX_PD(1'b1),
    .RX_CM_SEL(2'b11),
    .RX_CM_TRIM(3'b010),
    .RX_DATA_WIDTH(20),
    .RX_DDI_SEL(6'b000000),
    .RX_DEBUG_CFG(12'b000000000000),
    .RX_DEFER_RESET_BUF_EN("TRUE"),
    .RX_DFE_GAIN_CFG(23'h020FEA),
    .RX_DFE_H2_CFG(12'b000000000000),
    .RX_DFE_H3_CFG(12'b000001000000),
    .RX_DFE_H4_CFG(11'b00011110000),
    .RX_DFE_H5_CFG(11'b00011100000),
    .RX_DFE_KL_CFG(13'b0000011111110),
    .RX_DFE_KL_CFG2(32'h301148AC),
    .RX_DFE_LPM_CFG(16'h0904),
    .RX_DFE_LPM_HOLD_DURING_EIDLE(1'b0),
    .RX_DFE_UT_CFG(17'b10001111000000000),
    .RX_DFE_VP_CFG(17'b00011111100000011),
    .RX_DFE_XYD_CFG(13'b0000000000000),
    .RX_DISPERR_SEQ_MATCH("TRUE"),
    .RX_INT_DATAWIDTH(0),
    .RX_OS_CFG(13'b0000010000000),
    .RX_SIG_VALID_DLY(10),
    .RX_XCLK_SEL("RXREC"),
    .SAS_MAX_COM(64),
    .SAS_MIN_COM(36),
    .SATA_BURST_SEQ_LEN(4'b0101),
    .SATA_BURST_VAL(3'b100),
    .SATA_CPLL_CFG("VCO_3000MHZ"),
    .SATA_EIDLE_VAL(3'b100),
    .SATA_MAX_BURST(8),
    .SATA_MAX_INIT(21),
    .SATA_MAX_WAKE(7),
    .SATA_MIN_BURST(4),
    .SATA_MIN_INIT(12),
    .SATA_MIN_WAKE(4),
    .SHOW_REALIGN_COMMA("TRUE"),
    .SIM_CPLLREFCLK_SEL(3'b001),
    .SIM_RECEIVER_DETECT_PASS("TRUE"),
    .SIM_RESET_SPEEDUP("TRUE"),
    .SIM_TX_EIDLE_DRIVE_LEVEL("X"),
    .SIM_VERSION("4.0"),
    .TERM_RCAL_CFG(5'b10000),
    .TERM_RCAL_OVRD(1'b0),
    .TRANS_TIME_RATE(8'h0E),
    .TST_RSV(32'h00000000),
    .TXBUF_EN("TRUE"),
    .TXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .TXDLY_CFG(16'h001F),
    .TXDLY_LCFG(9'h030),
    .TXDLY_TAP_CFG(16'h0000),
    .TXGEARBOX_EN("FALSE"),
    .TXOUT_DIV(4),
    .TXPCSRESET_TIME(5'b00001),
    .TXPHDLY_CFG(24'h084020),
    .TXPH_CFG(16'h0780),
    .TXPH_MONITOR_SEL(5'b00000),
    .TXPMARESET_TIME(5'b00001),
    .TX_CLK25_DIV(5),
    .TX_CLKMUX_PD(1'b1),
    .TX_DATA_WIDTH(20),
    .TX_DEEMPH0(5'b00000),
    .TX_DEEMPH1(5'b00000),
    .TX_DRIVE_MODE("DIRECT"),
    .TX_EIDLE_ASSERT_DELAY(3'b110),
    .TX_EIDLE_DEASSERT_DELAY(3'b100),
    .TX_INT_DATAWIDTH(0),
    .TX_LOOPBACK_DRIVE_HIZ("FALSE"),
    .TX_MAINCURSOR_SEL(1'b0),
    .TX_MARGIN_FULL_0(7'b1001110),
    .TX_MARGIN_FULL_1(7'b1001001),
    .TX_MARGIN_FULL_2(7'b1000101),
    .TX_MARGIN_FULL_3(7'b1000010),
    .TX_MARGIN_FULL_4(7'b1000000),
    .TX_MARGIN_LOW_0(7'b1000110),
    .TX_MARGIN_LOW_1(7'b1000100),
    .TX_MARGIN_LOW_2(7'b1000010),
    .TX_MARGIN_LOW_3(7'b1000000),
    .TX_MARGIN_LOW_4(7'b1000000),
    .TX_PREDRIVER_MODE(1'b0),
    .TX_QPI_STATUS_EN(1'b0),
    .TX_RXDETECT_CFG(14'h1832),
    .TX_RXDETECT_REF(3'b100),
    .TX_XCLK_SEL("TXOUT"),
    .UCODEER_CLR(1'b0)) 
    gtxe2_i
       (.CFGRESET(1'b0),
        .CLKRSVD({1'b0,1'b0,1'b0,1'b0}),
        .CPLLFBCLKLOST(gtxe2_i_n_0),
        .CPLLLOCK(gtxe2_i_0),
        .CPLLLOCKDETCLK(independent_clock_bufg),
        .CPLLLOCKEN(1'b1),
        .CPLLPD(cpll_pd0_i),
        .CPLLREFCLKLOST(gt0_cpllrefclklost_i),
        .CPLLREFCLKSEL({1'b0,1'b0,1'b1}),
        .CPLLRESET(cpllreset_in),
        .DMONITOROUT({gtxe2_i_n_177,gtxe2_i_n_178,gtxe2_i_n_179,gtxe2_i_n_180,gtxe2_i_n_181,gtxe2_i_n_182,gtxe2_i_n_183,gtxe2_i_n_184}),
        .DRPADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPCLK(gtrefclk_bufg),
        .DRPDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPDO({gtxe2_i_n_46,gtxe2_i_n_47,gtxe2_i_n_48,gtxe2_i_n_49,gtxe2_i_n_50,gtxe2_i_n_51,gtxe2_i_n_52,gtxe2_i_n_53,gtxe2_i_n_54,gtxe2_i_n_55,gtxe2_i_n_56,gtxe2_i_n_57,gtxe2_i_n_58,gtxe2_i_n_59,gtxe2_i_n_60,gtxe2_i_n_61}),
        .DRPEN(1'b0),
        .DRPRDY(gtxe2_i_n_3),
        .DRPWE(1'b0),
        .EYESCANDATAERROR(gtxe2_i_n_4),
        .EYESCANMODE(1'b0),
        .EYESCANRESET(1'b0),
        .EYESCANTRIGGER(1'b0),
        .GTGREFCLK(1'b0),
        .GTNORTHREFCLK0(1'b0),
        .GTNORTHREFCLK1(1'b0),
        .GTREFCLK0(gtrefclk_out),
        .GTREFCLK1(1'b0),
        .GTREFCLKMONITOR(NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED),
        .GTRESETSEL(1'b0),
        .GTRSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .GTRXRESET(SR),
        .GTSOUTHREFCLK0(1'b0),
        .GTSOUTHREFCLK1(1'b0),
        .GTTXRESET(gt0_gttxreset_in0_out),
        .GTXRXN(rxn),
        .GTXRXP(rxp),
        .GTXTXN(txn),
        .GTXTXP(txp),
        .LOOPBACK({1'b0,1'b0,1'b0}),
        .PCSRSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDIN2({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDOUT(NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED[15:0]),
        .PHYSTATUS(NLW_gtxe2_i_PHYSTATUS_UNCONNECTED),
        .PMARSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PMARSVDIN2({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLCLK(gt0_qplloutclk_out),
        .QPLLREFCLK(gt0_qplloutrefclk_out),
        .RESETOVRD(1'b0),
        .RX8B10BEN(1'b1),
        .RXBUFRESET(1'b0),
        .RXBUFSTATUS({RXBUFSTATUS,gtxe2_i_n_83,gtxe2_i_n_84}),
        .RXBYTEISALIGNED(gtxe2_i_n_9),
        .RXBYTEREALIGN(gtxe2_i_n_10),
        .RXCDRFREQRESET(1'b0),
        .RXCDRHOLD(1'b0),
        .RXCDRLOCK(NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED),
        .RXCDROVRDEN(1'b0),
        .RXCDRRESET(1'b0),
        .RXCDRRESETRSV(1'b0),
        .RXCHANBONDSEQ(NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED),
        .RXCHANISALIGNED(NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED),
        .RXCHANREALIGN(NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED),
        .RXCHARISCOMMA({NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED[7:2],gtxe2_i_4}),
        .RXCHARISK({NLW_gtxe2_i_RXCHARISK_UNCONNECTED[7:2],gtxe2_i_5}),
        .RXCHBONDEN(1'b0),
        .RXCHBONDI({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RXCHBONDLEVEL({1'b0,1'b0,1'b0}),
        .RXCHBONDMASTER(1'b0),
        .RXCHBONDO(NLW_gtxe2_i_RXCHBONDO_UNCONNECTED[4:0]),
        .RXCHBONDSLAVE(1'b0),
        .RXCLKCORCNT(D),
        .RXCOMINITDET(NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED),
        .RXCOMMADET(gtxe2_i_n_16),
        .RXCOMMADETEN(1'b1),
        .RXCOMSASDET(NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED),
        .RXCOMWAKEDET(NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED),
        .RXDATA({NLW_gtxe2_i_RXDATA_UNCONNECTED[63:16],gtxe2_i_3}),
        .RXDATAVALID(NLW_gtxe2_i_RXDATAVALID_UNCONNECTED),
        .RXDDIEN(1'b0),
        .RXDFEAGCHOLD(1'b0),
        .RXDFEAGCOVRDEN(1'b0),
        .RXDFECM1EN(1'b0),
        .RXDFELFHOLD(1'b0),
        .RXDFELFOVRDEN(1'b0),
        .RXDFELPMRESET(1'b0),
        .RXDFETAP2HOLD(1'b0),
        .RXDFETAP2OVRDEN(1'b0),
        .RXDFETAP3HOLD(1'b0),
        .RXDFETAP3OVRDEN(1'b0),
        .RXDFETAP4HOLD(1'b0),
        .RXDFETAP4OVRDEN(1'b0),
        .RXDFETAP5HOLD(1'b0),
        .RXDFETAP5OVRDEN(1'b0),
        .RXDFEUTHOLD(1'b0),
        .RXDFEUTOVRDEN(1'b0),
        .RXDFEVPHOLD(1'b0),
        .RXDFEVPOVRDEN(1'b0),
        .RXDFEVSEN(1'b0),
        .RXDFEXYDEN(1'b1),
        .RXDFEXYDHOLD(1'b0),
        .RXDFEXYDOVRDEN(1'b0),
        .RXDISPERR({NLW_gtxe2_i_RXDISPERR_UNCONNECTED[7:2],gtxe2_i_6}),
        .RXDLYBYPASS(1'b1),
        .RXDLYEN(1'b0),
        .RXDLYOVRDEN(1'b0),
        .RXDLYSRESET(1'b0),
        .RXDLYSRESETDONE(NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED),
        .RXELECIDLE(NLW_gtxe2_i_RXELECIDLE_UNCONNECTED),
        .RXELECIDLEMODE({1'b1,1'b1}),
        .RXGEARBOXSLIP(1'b0),
        .RXHEADER(NLW_gtxe2_i_RXHEADER_UNCONNECTED[2:0]),
        .RXHEADERVALID(NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED),
        .RXLPMEN(1'b1),
        .RXLPMHFHOLD(1'b0),
        .RXLPMHFOVRDEN(1'b0),
        .RXLPMLFHOLD(1'b0),
        .RXLPMLFKLOVRDEN(1'b0),
        .RXMCOMMAALIGNEN(reset_out),
        .RXMONITOROUT({gtxe2_i_n_170,gtxe2_i_n_171,gtxe2_i_n_172,gtxe2_i_n_173,gtxe2_i_n_174,gtxe2_i_n_175,gtxe2_i_n_176}),
        .RXMONITORSEL({1'b0,1'b0}),
        .RXNOTINTABLE({NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED[7:2],gtxe2_i_7}),
        .RXOOBRESET(1'b0),
        .RXOSHOLD(1'b0),
        .RXOSOVRDEN(1'b0),
        .RXOUTCLK(rxoutclk),
        .RXOUTCLKFABRIC(NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED),
        .RXOUTCLKPCS(NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED),
        .RXOUTCLKSEL({1'b0,1'b1,1'b0}),
        .RXPCOMMAALIGNEN(reset_out),
        .RXPCSRESET(reset),
        .RXPD({RXPD,RXPD}),
        .RXPHALIGN(1'b0),
        .RXPHALIGNDONE(NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED),
        .RXPHALIGNEN(1'b0),
        .RXPHDLYPD(1'b0),
        .RXPHDLYRESET(1'b0),
        .RXPHMONITOR(NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED[4:0]),
        .RXPHOVRDEN(1'b0),
        .RXPHSLIPMONITOR(NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED[4:0]),
        .RXPMARESET(1'b0),
        .RXPOLARITY(1'b0),
        .RXPRBSCNTRESET(1'b0),
        .RXPRBSERR(gtxe2_i_n_27),
        .RXPRBSSEL({1'b0,1'b0,1'b0}),
        .RXQPIEN(1'b0),
        .RXQPISENN(NLW_gtxe2_i_RXQPISENN_UNCONNECTED),
        .RXQPISENP(NLW_gtxe2_i_RXQPISENP_UNCONNECTED),
        .RXRATE({1'b0,1'b0,1'b0}),
        .RXRATEDONE(NLW_gtxe2_i_RXRATEDONE_UNCONNECTED),
        .RXRESETDONE(gtxe2_i_1),
        .RXSLIDE(1'b0),
        .RXSTARTOFSEQ(NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED),
        .RXSTATUS(NLW_gtxe2_i_RXSTATUS_UNCONNECTED[2:0]),
        .RXSYSCLKSEL({1'b0,1'b0}),
        .RXUSERRDY(gt0_rxuserrdy_t),
        .RXUSRCLK(gtxe2_i_8),
        .RXUSRCLK2(gtxe2_i_8),
        .RXVALID(NLW_gtxe2_i_RXVALID_UNCONNECTED),
        .SETERRSTATUS(1'b0),
        .TSTIN({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .TSTOUT(NLW_gtxe2_i_TSTOUT_UNCONNECTED[9:0]),
        .TX8B10BBYPASS({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX8B10BEN(1'b1),
        .TXBUFDIFFCTRL({1'b1,1'b0,1'b0}),
        .TXBUFSTATUS({TXBUFSTATUS,gtxe2_i_n_81}),
        .TXCHARDISPMODE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtxe2_i_9}),
        .TXCHARDISPVAL({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtxe2_i_10}),
        .TXCHARISK({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtxe2_i_11}),
        .TXCOMFINISH(NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED),
        .TXCOMINIT(1'b0),
        .TXCOMSAS(1'b0),
        .TXCOMWAKE(1'b0),
        .TXDATA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q}),
        .TXDEEMPH(1'b0),
        .TXDETECTRX(1'b0),
        .TXDIFFCTRL({1'b1,1'b0,1'b0,1'b0}),
        .TXDIFFPD(1'b0),
        .TXDLYBYPASS(1'b1),
        .TXDLYEN(1'b0),
        .TXDLYHOLD(1'b0),
        .TXDLYOVRDEN(1'b0),
        .TXDLYSRESET(1'b0),
        .TXDLYSRESETDONE(NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED),
        .TXDLYUPDOWN(1'b0),
        .TXELECIDLE(TXPD),
        .TXGEARBOXREADY(NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED),
        .TXHEADER({1'b0,1'b0,1'b0}),
        .TXINHIBIT(1'b0),
        .TXMAINCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXMARGIN({1'b0,1'b0,1'b0}),
        .TXOUTCLK(txoutclk),
        .TXOUTCLKFABRIC(gtxe2_i_n_38),
        .TXOUTCLKPCS(gtxe2_i_n_39),
        .TXOUTCLKSEL({1'b1,1'b0,1'b0}),
        .TXPCSRESET(1'b0),
        .TXPD({TXPD,TXPD}),
        .TXPDELECIDLEMODE(1'b0),
        .TXPHALIGN(1'b0),
        .TXPHALIGNDONE(NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED),
        .TXPHALIGNEN(1'b0),
        .TXPHDLYPD(1'b0),
        .TXPHDLYRESET(1'b0),
        .TXPHDLYTSTCLK(1'b0),
        .TXPHINIT(1'b0),
        .TXPHINITDONE(NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED),
        .TXPHOVRDEN(1'b0),
        .TXPISOPD(1'b0),
        .TXPMARESET(1'b0),
        .TXPOLARITY(1'b0),
        .TXPOSTCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPOSTCURSORINV(1'b0),
        .TXPRBSFORCEERR(1'b0),
        .TXPRBSSEL({1'b0,1'b0,1'b0}),
        .TXPRECURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPRECURSORINV(1'b0),
        .TXQPIBIASEN(1'b0),
        .TXQPISENN(NLW_gtxe2_i_TXQPISENN_UNCONNECTED),
        .TXQPISENP(NLW_gtxe2_i_TXQPISENP_UNCONNECTED),
        .TXQPISTRONGPDOWN(1'b0),
        .TXQPIWEAKPUP(1'b0),
        .TXRATE({1'b0,1'b0,1'b0}),
        .TXRATEDONE(NLW_gtxe2_i_TXRATEDONE_UNCONNECTED),
        .TXRESETDONE(gtxe2_i_2),
        .TXSEQUENCE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXSTARTSEQ(1'b0),
        .TXSWING(1'b0),
        .TXSYSCLKSEL({1'b0,1'b0}),
        .TXUSERRDY(gt0_txuserrdy_t),
        .TXUSRCLK(gtxe2_i_8),
        .TXUSRCLK2(gtxe2_i_8));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_init
   (txn,
    txp,
    rxoutclk,
    txoutclk,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i,
    gtxe2_i_0,
    gtxe2_i_1,
    gtxe2_i_2,
    gtxe2_i_3,
    mmcm_reset,
    data_in,
    rx_fsm_reset_done_int_reg,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gtxe2_i_4,
    TXPD,
    RXPD,
    Q,
    gtxe2_i_5,
    gtxe2_i_6,
    gtxe2_i_7,
    out,
    gtxe2_i_8,
    gtxe2_i_9,
    data_sync_reg1,
    data_out);
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i;
  output [1:0]gtxe2_i_0;
  output [1:0]gtxe2_i_1;
  output [1:0]gtxe2_i_2;
  output [1:0]gtxe2_i_3;
  output mmcm_reset;
  output data_in;
  output rx_fsm_reset_done_int_reg;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gtxe2_i_4;
  input [0:0]TXPD;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_5;
  input [1:0]gtxe2_i_6;
  input [1:0]gtxe2_i_7;
  input [0:0]out;
  input gtxe2_i_8;
  input gtxe2_i_9;
  input data_sync_reg1;
  input data_out;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire [13:1]data0;
  wire data_in;
  wire data_out;
  wire data_sync_reg1;
  wire gt0_cpllrefclklost_i;
  wire gt0_cpllreset_t;
  wire gt0_gtrxreset_in1_out;
  wire gt0_gttxreset_in0_out;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire [13:0]gt0_rx_cdrlock_counter;
  wire gt0_rx_cdrlock_counter0_carry__0_n_0;
  wire gt0_rx_cdrlock_counter0_carry__0_n_1;
  wire gt0_rx_cdrlock_counter0_carry__0_n_2;
  wire gt0_rx_cdrlock_counter0_carry__0_n_3;
  wire gt0_rx_cdrlock_counter0_carry__1_n_0;
  wire gt0_rx_cdrlock_counter0_carry__1_n_1;
  wire gt0_rx_cdrlock_counter0_carry__1_n_2;
  wire gt0_rx_cdrlock_counter0_carry__1_n_3;
  wire gt0_rx_cdrlock_counter0_carry_n_0;
  wire gt0_rx_cdrlock_counter0_carry_n_1;
  wire gt0_rx_cdrlock_counter0_carry_n_2;
  wire gt0_rx_cdrlock_counter0_carry_n_3;
  wire \gt0_rx_cdrlock_counter[0]_i_2_n_0 ;
  wire \gt0_rx_cdrlock_counter[13]_i_2_n_0 ;
  wire \gt0_rx_cdrlock_counter[13]_i_3_n_0 ;
  wire \gt0_rx_cdrlock_counter[13]_i_4_n_0 ;
  wire [13:0]gt0_rx_cdrlock_counter_0;
  wire gt0_rx_cdrlocked_i_1_n_0;
  wire gt0_rx_cdrlocked_reg_n_0;
  wire gt0_rxuserrdy_t;
  wire gt0_txuserrdy_t;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtwizard_i_n_0;
  wire gtwizard_i_n_5;
  wire gtwizard_i_n_7;
  wire [15:0]gtxe2_i;
  wire [1:0]gtxe2_i_0;
  wire [1:0]gtxe2_i_1;
  wire [1:0]gtxe2_i_2;
  wire [1:0]gtxe2_i_3;
  wire gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire [1:0]gtxe2_i_7;
  wire gtxe2_i_8;
  wire gtxe2_i_9;
  wire independent_clock_bufg;
  wire mmcm_reset;
  wire [0:0]out;
  wire reset;
  wire reset_out;
  wire rx_fsm_reset_done_int_reg;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;
  wire [3:0]NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED;
  wire [3:1]NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED;

  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry
       (.CI(1'b0),
        .CO({gt0_rx_cdrlock_counter0_carry_n_0,gt0_rx_cdrlock_counter0_carry_n_1,gt0_rx_cdrlock_counter0_carry_n_2,gt0_rx_cdrlock_counter0_carry_n_3}),
        .CYINIT(gt0_rx_cdrlock_counter[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[4:1]),
        .S(gt0_rx_cdrlock_counter[4:1]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry__0
       (.CI(gt0_rx_cdrlock_counter0_carry_n_0),
        .CO({gt0_rx_cdrlock_counter0_carry__0_n_0,gt0_rx_cdrlock_counter0_carry__0_n_1,gt0_rx_cdrlock_counter0_carry__0_n_2,gt0_rx_cdrlock_counter0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[8:5]),
        .S(gt0_rx_cdrlock_counter[8:5]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry__1
       (.CI(gt0_rx_cdrlock_counter0_carry__0_n_0),
        .CO({gt0_rx_cdrlock_counter0_carry__1_n_0,gt0_rx_cdrlock_counter0_carry__1_n_1,gt0_rx_cdrlock_counter0_carry__1_n_2,gt0_rx_cdrlock_counter0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[12:9]),
        .S(gt0_rx_cdrlock_counter[12:9]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry__2
       (.CI(gt0_rx_cdrlock_counter0_carry__1_n_0),
        .CO(NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED[3:1],data0[13]}),
        .S({1'b0,1'b0,1'b0,gt0_rx_cdrlock_counter[13]}));
  LUT2 #(
    .INIT(4'h2)) 
    \gt0_rx_cdrlock_counter[0]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[0]_i_2_n_0 ),
        .I1(gt0_rx_cdrlock_counter[0]),
        .O(gt0_rx_cdrlock_counter_0[0]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFB)) 
    \gt0_rx_cdrlock_counter[0]_i_2 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I1(gt0_rx_cdrlock_counter[4]),
        .I2(gt0_rx_cdrlock_counter[5]),
        .I3(gt0_rx_cdrlock_counter[7]),
        .I4(gt0_rx_cdrlock_counter[6]),
        .I5(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .O(\gt0_rx_cdrlock_counter[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[10]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[10]),
        .O(gt0_rx_cdrlock_counter_0[10]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[11]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[11]),
        .O(gt0_rx_cdrlock_counter_0[11]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[12]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[12]),
        .O(gt0_rx_cdrlock_counter_0[12]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[13]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[13]),
        .O(gt0_rx_cdrlock_counter_0[13]));
  LUT5 #(
    .INIT(32'hFFFFFFEF)) 
    \gt0_rx_cdrlock_counter[13]_i_2 
       (.I0(gt0_rx_cdrlock_counter[1]),
        .I1(gt0_rx_cdrlock_counter[12]),
        .I2(gt0_rx_cdrlock_counter[13]),
        .I3(gt0_rx_cdrlock_counter[3]),
        .I4(gt0_rx_cdrlock_counter[2]),
        .O(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \gt0_rx_cdrlock_counter[13]_i_3 
       (.I0(gt0_rx_cdrlock_counter[4]),
        .I1(gt0_rx_cdrlock_counter[5]),
        .I2(gt0_rx_cdrlock_counter[7]),
        .I3(gt0_rx_cdrlock_counter[6]),
        .O(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFF7F)) 
    \gt0_rx_cdrlock_counter[13]_i_4 
       (.I0(gt0_rx_cdrlock_counter[9]),
        .I1(gt0_rx_cdrlock_counter[8]),
        .I2(gt0_rx_cdrlock_counter[10]),
        .I3(gt0_rx_cdrlock_counter[11]),
        .O(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[1]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[1]),
        .O(gt0_rx_cdrlock_counter_0[1]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[2]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[2]),
        .O(gt0_rx_cdrlock_counter_0[2]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[3]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[3]),
        .O(gt0_rx_cdrlock_counter_0[3]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[4]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[4]),
        .O(gt0_rx_cdrlock_counter_0[4]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[5]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[5]),
        .O(gt0_rx_cdrlock_counter_0[5]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[6]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[6]),
        .O(gt0_rx_cdrlock_counter_0[6]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[7]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[7]),
        .O(gt0_rx_cdrlock_counter_0[7]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[8]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[8]),
        .O(gt0_rx_cdrlock_counter_0[8]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[9]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[9]),
        .O(gt0_rx_cdrlock_counter_0[9]));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[0]),
        .Q(gt0_rx_cdrlock_counter[0]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[10] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[10]),
        .Q(gt0_rx_cdrlock_counter[10]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[11] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[11]),
        .Q(gt0_rx_cdrlock_counter[11]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[12] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[12]),
        .Q(gt0_rx_cdrlock_counter[12]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[13] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[13]),
        .Q(gt0_rx_cdrlock_counter[13]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[1]),
        .Q(gt0_rx_cdrlock_counter[1]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[2]),
        .Q(gt0_rx_cdrlock_counter[2]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[3]),
        .Q(gt0_rx_cdrlock_counter[3]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[4] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[4]),
        .Q(gt0_rx_cdrlock_counter[4]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[5] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[5]),
        .Q(gt0_rx_cdrlock_counter[5]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[6] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[6]),
        .Q(gt0_rx_cdrlock_counter[6]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[7] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[7]),
        .Q(gt0_rx_cdrlock_counter[7]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[8] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[8]),
        .Q(gt0_rx_cdrlock_counter[8]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[9] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[9]),
        .Q(gt0_rx_cdrlock_counter[9]),
        .R(gt0_gtrxreset_in1_out));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    gt0_rx_cdrlocked_i_1
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(gt0_rx_cdrlocked_reg_n_0),
        .O(gt0_rx_cdrlocked_i_1_n_0));
  FDRE gt0_rx_cdrlocked_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlocked_i_1_n_0),
        .Q(gt0_rx_cdrlocked_reg_n_0),
        .R(gt0_gtrxreset_in1_out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_RX_STARTUP_FSM gt0_rxresetfsm_i
       (.\FSM_sequential_rx_state_reg[0]_0 (gt0_rx_cdrlocked_reg_n_0),
        .SR(gt0_gtrxreset_in1_out),
        .data_in(rx_fsm_reset_done_int_reg),
        .data_out(data_out),
        .data_sync_reg1(gtwizard_i_n_5),
        .data_sync_reg1_0(data_sync_reg1),
        .data_sync_reg1_1(gtwizard_i_n_0),
        .data_sync_reg6(gtxe2_i_4),
        .gt0_rxuserrdy_t(gt0_rxuserrdy_t),
        .gtxe2_i(gtxe2_i_8),
        .independent_clock_bufg(independent_clock_bufg),
        .out(out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_TX_STARTUP_FSM gt0_txresetfsm_i
       (.data_in(data_in),
        .data_sync_reg1(gtxe2_i_4),
        .data_sync_reg1_0(gtwizard_i_n_7),
        .data_sync_reg1_1(data_sync_reg1),
        .data_sync_reg1_2(gtwizard_i_n_0),
        .gt0_cpllrefclklost_i(gt0_cpllrefclklost_i),
        .gt0_cpllreset_t(gt0_cpllreset_t),
        .gt0_gttxreset_in0_out(gt0_gttxreset_in0_out),
        .gt0_txuserrdy_t(gt0_txuserrdy_t),
        .gtxe2_i(gtxe2_i_9),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt gtwizard_i
       (.D(D),
        .Q(Q),
        .RXBUFSTATUS(RXBUFSTATUS),
        .RXPD(RXPD),
        .SR(gt0_gtrxreset_in1_out),
        .TXBUFSTATUS(TXBUFSTATUS),
        .TXPD(TXPD),
        .gt0_cpllrefclklost_i(gt0_cpllrefclklost_i),
        .gt0_cpllreset_t(gt0_cpllreset_t),
        .gt0_gttxreset_in0_out(gt0_gttxreset_in0_out),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gt0_rxuserrdy_t(gt0_rxuserrdy_t),
        .gt0_txuserrdy_t(gt0_txuserrdy_t),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(gtwizard_i_n_0),
        .gtxe2_i_0(gtwizard_i_n_5),
        .gtxe2_i_1(gtwizard_i_n_7),
        .gtxe2_i_10(gtxe2_i_7),
        .gtxe2_i_2(gtxe2_i),
        .gtxe2_i_3(gtxe2_i_0),
        .gtxe2_i_4(gtxe2_i_1),
        .gtxe2_i_5(gtxe2_i_2),
        .gtxe2_i_6(gtxe2_i_3),
        .gtxe2_i_7(gtxe2_i_4),
        .gtxe2_i_8(gtxe2_i_5),
        .gtxe2_i_9(gtxe2_i_6),
        .independent_clock_bufg(independent_clock_bufg),
        .reset(reset),
        .reset_out(reset_out),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt
   (gtxe2_i,
    gt0_cpllrefclklost_i,
    txn,
    txp,
    rxoutclk,
    gtxe2_i_0,
    txoutclk,
    gtxe2_i_1,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i_2,
    gtxe2_i_3,
    gtxe2_i_4,
    gtxe2_i_5,
    gtxe2_i_6,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    SR,
    gt0_gttxreset_in0_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gt0_rxuserrdy_t,
    gtxe2_i_7,
    TXPD,
    gt0_txuserrdy_t,
    RXPD,
    Q,
    gtxe2_i_8,
    gtxe2_i_9,
    gtxe2_i_10,
    gt0_cpllreset_t);
  output gtxe2_i;
  output gt0_cpllrefclklost_i;
  output txn;
  output txp;
  output rxoutclk;
  output gtxe2_i_0;
  output txoutclk;
  output gtxe2_i_1;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i_2;
  output [1:0]gtxe2_i_3;
  output [1:0]gtxe2_i_4;
  output [1:0]gtxe2_i_5;
  output [1:0]gtxe2_i_6;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input [0:0]SR;
  input gt0_gttxreset_in0_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gt0_rxuserrdy_t;
  input gtxe2_i_7;
  input [0:0]TXPD;
  input gt0_txuserrdy_t;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_8;
  input [1:0]gtxe2_i_9;
  input [1:0]gtxe2_i_10;
  input gt0_cpllreset_t;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]SR;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire cpll_pd0_i;
  wire cpllreset_in;
  wire gt0_cpllrefclklost_i;
  wire gt0_cpllreset_t;
  wire gt0_gttxreset_in0_out;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gt0_rxuserrdy_t;
  wire gt0_txuserrdy_t;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtxe2_i;
  wire gtxe2_i_0;
  wire gtxe2_i_1;
  wire [1:0]gtxe2_i_10;
  wire [15:0]gtxe2_i_2;
  wire [1:0]gtxe2_i_3;
  wire [1:0]gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire gtxe2_i_7;
  wire [1:0]gtxe2_i_8;
  wire [1:0]gtxe2_i_9;
  wire independent_clock_bufg;
  wire reset;
  wire reset_out;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_cpll_railing cpll_railing0_i
       (.cpll_pd0_i(cpll_pd0_i),
        .cpllreset_in(cpllreset_in),
        .gt0_cpllreset_t(gt0_cpllreset_t),
        .gtrefclk_bufg(gtrefclk_bufg));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_GT gt0_GTWIZARD_i
       (.D(D),
        .Q(Q),
        .RXBUFSTATUS(RXBUFSTATUS),
        .RXPD(RXPD),
        .SR(SR),
        .TXBUFSTATUS(TXBUFSTATUS),
        .TXPD(TXPD),
        .cpll_pd0_i(cpll_pd0_i),
        .cpllreset_in(cpllreset_in),
        .gt0_cpllrefclklost_i(gt0_cpllrefclklost_i),
        .gt0_gttxreset_in0_out(gt0_gttxreset_in0_out),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gt0_rxuserrdy_t(gt0_rxuserrdy_t),
        .gt0_txuserrdy_t(gt0_txuserrdy_t),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i_0(gtxe2_i),
        .gtxe2_i_1(gtxe2_i_0),
        .gtxe2_i_10(gtxe2_i_9),
        .gtxe2_i_11(gtxe2_i_10),
        .gtxe2_i_2(gtxe2_i_1),
        .gtxe2_i_3(gtxe2_i_2),
        .gtxe2_i_4(gtxe2_i_3),
        .gtxe2_i_5(gtxe2_i_4),
        .gtxe2_i_6(gtxe2_i_5),
        .gtxe2_i_7(gtxe2_i_6),
        .gtxe2_i_8(gtxe2_i_7),
        .gtxe2_i_9(gtxe2_i_8),
        .independent_clock_bufg(independent_clock_bufg),
        .reset(reset),
        .reset_out(reset_out),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_RX_STARTUP_FSM
   (data_in,
    gt0_rxuserrdy_t,
    SR,
    independent_clock_bufg,
    data_sync_reg6,
    out,
    gtxe2_i,
    \FSM_sequential_rx_state_reg[0]_0 ,
    data_sync_reg1,
    data_sync_reg1_0,
    data_out,
    data_sync_reg1_1);
  output data_in;
  output gt0_rxuserrdy_t;
  output [0:0]SR;
  input independent_clock_bufg;
  input data_sync_reg6;
  input [0:0]out;
  input gtxe2_i;
  input \FSM_sequential_rx_state_reg[0]_0 ;
  input data_sync_reg1;
  input data_sync_reg1_0;
  input data_out;
  input data_sync_reg1_1;

  wire \FSM_sequential_rx_state[0]_i_2_n_0 ;
  wire \FSM_sequential_rx_state[1]_i_3_n_0 ;
  wire \FSM_sequential_rx_state[2]_i_2_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_10_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_3_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_7_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_9_n_0 ;
  wire \FSM_sequential_rx_state_reg[0]_0 ;
  wire GTRXRESET;
  wire RXUSERRDY_i_1_n_0;
  wire [0:0]SR;
  wire check_tlock_max_i_1_n_0;
  wire check_tlock_max_reg_n_0;
  wire data_in;
  wire data_out;
  wire data_out_0;
  wire data_sync_reg1;
  wire data_sync_reg1_0;
  wire data_sync_reg1_1;
  wire data_sync_reg6;
  wire gt0_rxuserrdy_t;
  wire gtrxreset_i_i_1_n_0;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire \init_wait_count[0]_i_1__0_n_0 ;
  wire \init_wait_count[6]_i_1__0_n_0 ;
  wire \init_wait_count[6]_i_3__0_n_0 ;
  wire [6:0]init_wait_count_reg;
  wire init_wait_done_i_1__0_n_0;
  wire init_wait_done_reg_n_0;
  wire \mmcm_lock_count[2]_i_1__0_n_0 ;
  wire \mmcm_lock_count[3]_i_1__0_n_0 ;
  wire \mmcm_lock_count[4]_i_1__0_n_0 ;
  wire \mmcm_lock_count[5]_i_1__0_n_0 ;
  wire \mmcm_lock_count[6]_i_1__0_n_0 ;
  wire \mmcm_lock_count[7]_i_2__0_n_0 ;
  wire \mmcm_lock_count[7]_i_3__0_n_0 ;
  wire [7:0]mmcm_lock_count_reg;
  wire mmcm_lock_i;
  wire mmcm_lock_reclocked;
  wire mmcm_lock_reclocked_i_1_n_0;
  wire mmcm_lock_reclocked_i_2__0_n_0;
  wire [0:0]out;
  wire [6:1]p_0_in__2;
  wire [1:0]p_0_in__3;
  wire reset_time_out_i_3_n_0;
  wire reset_time_out_i_4_n_0;
  wire reset_time_out_reg_n_0;
  wire run_phase_alignment_int_i_1__0_n_0;
  wire run_phase_alignment_int_reg_n_0;
  wire run_phase_alignment_int_s3_reg_n_0;
  wire rx_fsm_reset_done_int_i_5_n_0;
  wire rx_fsm_reset_done_int_i_6_n_0;
  wire rx_fsm_reset_done_int_s2;
  wire rx_fsm_reset_done_int_s3;
  wire [3:0]rx_state;
  wire [3:0]rx_state__0;
  wire rxresetdone_s2;
  wire rxresetdone_s3;
  wire sync_cplllock_n_0;
  wire sync_data_valid_n_0;
  wire sync_data_valid_n_1;
  wire sync_data_valid_n_5;
  wire sync_mmcm_lock_reclocked_n_0;
  wire time_out_100us_i_1_n_0;
  wire time_out_100us_i_2_n_0;
  wire time_out_100us_i_3_n_0;
  wire time_out_100us_reg_n_0;
  wire time_out_1us_i_1_n_0;
  wire time_out_1us_i_2_n_0;
  wire time_out_1us_i_3_n_0;
  wire time_out_1us_reg_n_0;
  wire time_out_2ms_i_1_n_0;
  wire time_out_2ms_i_2_n_0;
  wire time_out_2ms_i_3__0_n_0;
  wire time_out_2ms_i_4_n_0;
  wire time_out_2ms_reg_n_0;
  wire time_out_counter;
  wire \time_out_counter[0]_i_3_n_0 ;
  wire [18:0]time_out_counter_reg;
  wire \time_out_counter_reg[0]_i_2__0_n_0 ;
  wire \time_out_counter_reg[0]_i_2__0_n_1 ;
  wire \time_out_counter_reg[0]_i_2__0_n_2 ;
  wire \time_out_counter_reg[0]_i_2__0_n_3 ;
  wire \time_out_counter_reg[0]_i_2__0_n_4 ;
  wire \time_out_counter_reg[0]_i_2__0_n_5 ;
  wire \time_out_counter_reg[0]_i_2__0_n_6 ;
  wire \time_out_counter_reg[0]_i_2__0_n_7 ;
  wire \time_out_counter_reg[12]_i_1__0_n_0 ;
  wire \time_out_counter_reg[12]_i_1__0_n_1 ;
  wire \time_out_counter_reg[12]_i_1__0_n_2 ;
  wire \time_out_counter_reg[12]_i_1__0_n_3 ;
  wire \time_out_counter_reg[12]_i_1__0_n_4 ;
  wire \time_out_counter_reg[12]_i_1__0_n_5 ;
  wire \time_out_counter_reg[12]_i_1__0_n_6 ;
  wire \time_out_counter_reg[12]_i_1__0_n_7 ;
  wire \time_out_counter_reg[16]_i_1__0_n_2 ;
  wire \time_out_counter_reg[16]_i_1__0_n_3 ;
  wire \time_out_counter_reg[16]_i_1__0_n_5 ;
  wire \time_out_counter_reg[16]_i_1__0_n_6 ;
  wire \time_out_counter_reg[16]_i_1__0_n_7 ;
  wire \time_out_counter_reg[4]_i_1__0_n_0 ;
  wire \time_out_counter_reg[4]_i_1__0_n_1 ;
  wire \time_out_counter_reg[4]_i_1__0_n_2 ;
  wire \time_out_counter_reg[4]_i_1__0_n_3 ;
  wire \time_out_counter_reg[4]_i_1__0_n_4 ;
  wire \time_out_counter_reg[4]_i_1__0_n_5 ;
  wire \time_out_counter_reg[4]_i_1__0_n_6 ;
  wire \time_out_counter_reg[4]_i_1__0_n_7 ;
  wire \time_out_counter_reg[8]_i_1__0_n_0 ;
  wire \time_out_counter_reg[8]_i_1__0_n_1 ;
  wire \time_out_counter_reg[8]_i_1__0_n_2 ;
  wire \time_out_counter_reg[8]_i_1__0_n_3 ;
  wire \time_out_counter_reg[8]_i_1__0_n_4 ;
  wire \time_out_counter_reg[8]_i_1__0_n_5 ;
  wire \time_out_counter_reg[8]_i_1__0_n_6 ;
  wire \time_out_counter_reg[8]_i_1__0_n_7 ;
  wire time_out_wait_bypass_i_1_n_0;
  wire time_out_wait_bypass_i_2__0_n_0;
  wire time_out_wait_bypass_i_3__0_n_0;
  wire time_out_wait_bypass_i_4__0_n_0;
  wire time_out_wait_bypass_reg_n_0;
  wire time_out_wait_bypass_s2;
  wire time_out_wait_bypass_s3;
  wire time_tlock_max;
  wire time_tlock_max1;
  wire time_tlock_max1_carry__0_i_1_n_0;
  wire time_tlock_max1_carry__0_i_2_n_0;
  wire time_tlock_max1_carry__0_i_3_n_0;
  wire time_tlock_max1_carry__0_i_4_n_0;
  wire time_tlock_max1_carry__0_i_5_n_0;
  wire time_tlock_max1_carry__0_i_6_n_0;
  wire time_tlock_max1_carry__0_n_0;
  wire time_tlock_max1_carry__0_n_1;
  wire time_tlock_max1_carry__0_n_2;
  wire time_tlock_max1_carry__0_n_3;
  wire time_tlock_max1_carry__1_i_1_n_0;
  wire time_tlock_max1_carry__1_i_2_n_0;
  wire time_tlock_max1_carry__1_i_3_n_0;
  wire time_tlock_max1_carry__1_n_3;
  wire time_tlock_max1_carry_i_1_n_0;
  wire time_tlock_max1_carry_i_2_n_0;
  wire time_tlock_max1_carry_i_3_n_0;
  wire time_tlock_max1_carry_i_4_n_0;
  wire time_tlock_max1_carry_i_5_n_0;
  wire time_tlock_max1_carry_i_6_n_0;
  wire time_tlock_max1_carry_i_7_n_0;
  wire time_tlock_max1_carry_i_8_n_0;
  wire time_tlock_max1_carry_n_0;
  wire time_tlock_max1_carry_n_1;
  wire time_tlock_max1_carry_n_2;
  wire time_tlock_max1_carry_n_3;
  wire time_tlock_max_i_1_n_0;
  wire \wait_bypass_count[0]_i_1__0_n_0 ;
  wire \wait_bypass_count[0]_i_2__0_n_0 ;
  wire \wait_bypass_count[0]_i_4_n_0 ;
  wire [12:0]wait_bypass_count_reg;
  wire \wait_bypass_count_reg[0]_i_3__0_n_0 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_1 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_2 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_3 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_4 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_5 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_6 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_7 ;
  wire \wait_bypass_count_reg[12]_i_1__0_n_7 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_0 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_1 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_2 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_3 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_4 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_5 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_6 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_7 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_0 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_1 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_2 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_3 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_4 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_5 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_6 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_7 ;
  wire [0:0]wait_time_cnt0__0;
  wire \wait_time_cnt[1]_i_1__0_n_0 ;
  wire \wait_time_cnt[2]_i_1__0_n_0 ;
  wire \wait_time_cnt[3]_i_1__0_n_0 ;
  wire \wait_time_cnt[4]_i_1__0_n_0 ;
  wire \wait_time_cnt[5]_i_1__0_n_0 ;
  wire \wait_time_cnt[6]_i_1_n_0 ;
  wire \wait_time_cnt[6]_i_2__0_n_0 ;
  wire \wait_time_cnt[6]_i_3__0_n_0 ;
  wire \wait_time_cnt[6]_i_4__0_n_0 ;
  wire [6:0]wait_time_cnt_reg;
  wire [3:2]\NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED ;
  wire [3:3]\NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED ;
  wire [3:0]NLW_time_tlock_max1_carry_O_UNCONNECTED;
  wire [3:0]NLW_time_tlock_max1_carry__0_O_UNCONNECTED;
  wire [3:2]NLW_time_tlock_max1_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_time_tlock_max1_carry__1_O_UNCONNECTED;
  wire [3:0]\NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED ;
  wire [3:1]\NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h2222AAAA00000C00)) 
    \FSM_sequential_rx_state[0]_i_2 
       (.I0(time_out_2ms_reg_n_0),
        .I1(rx_state[2]),
        .I2(rx_state[3]),
        .I3(time_tlock_max),
        .I4(reset_time_out_reg_n_0),
        .I5(rx_state[1]),
        .O(\FSM_sequential_rx_state[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000AABF000F0000)) 
    \FSM_sequential_rx_state[1]_i_3 
       (.I0(reset_time_out_reg_n_0),
        .I1(time_tlock_max),
        .I2(rx_state[2]),
        .I3(rx_state[3]),
        .I4(rx_state[1]),
        .I5(rx_state[0]),
        .O(\FSM_sequential_rx_state[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000050FF2200)) 
    \FSM_sequential_rx_state[2]_i_1 
       (.I0(rx_state[1]),
        .I1(time_out_2ms_reg_n_0),
        .I2(\FSM_sequential_rx_state[2]_i_2_n_0 ),
        .I3(rx_state[0]),
        .I4(rx_state[2]),
        .I5(rx_state[3]),
        .O(rx_state__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_rx_state[2]_i_2 
       (.I0(reset_time_out_reg_n_0),
        .I1(time_tlock_max),
        .O(\FSM_sequential_rx_state[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_rx_state[3]_i_10 
       (.I0(reset_time_out_reg_n_0),
        .I1(time_out_2ms_reg_n_0),
        .O(\FSM_sequential_rx_state[3]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0000000050005300)) 
    \FSM_sequential_rx_state[3]_i_3 
       (.I0(\FSM_sequential_rx_state[3]_i_10_n_0 ),
        .I1(\wait_time_cnt[6]_i_4__0_n_0 ),
        .I2(rx_state[0]),
        .I3(rx_state[1]),
        .I4(wait_time_cnt_reg[6]),
        .I5(rx_state[3]),
        .O(\FSM_sequential_rx_state[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000023002F00)) 
    \FSM_sequential_rx_state[3]_i_7 
       (.I0(time_out_2ms_reg_n_0),
        .I1(rx_state[2]),
        .I2(rx_state[1]),
        .I3(rx_state[0]),
        .I4(\FSM_sequential_rx_state[2]_i_2_n_0 ),
        .I5(rx_state[3]),
        .O(\FSM_sequential_rx_state[3]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT5 #(
    .INIT(32'h80800080)) 
    \FSM_sequential_rx_state[3]_i_9 
       (.I0(rx_state[0]),
        .I1(rx_state[1]),
        .I2(rx_state[2]),
        .I3(time_out_2ms_reg_n_0),
        .I4(reset_time_out_reg_n_0),
        .O(\FSM_sequential_rx_state[3]_i_9_n_0 ));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[0] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[0]),
        .Q(rx_state[0]),
        .R(out));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[1] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[1]),
        .Q(rx_state[1]),
        .R(out));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[2] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[2]),
        .Q(rx_state[2]),
        .R(out));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[3] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[3]),
        .Q(rx_state[3]),
        .R(out));
  LUT5 #(
    .INIT(32'hFFFB4000)) 
    RXUSERRDY_i_1
       (.I0(rx_state[3]),
        .I1(rx_state[0]),
        .I2(rx_state[2]),
        .I3(rx_state[1]),
        .I4(gt0_rxuserrdy_t),
        .O(RXUSERRDY_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    RXUSERRDY_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(RXUSERRDY_i_1_n_0),
        .Q(gt0_rxuserrdy_t),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT5 #(
    .INIT(32'hFFFB0008)) 
    check_tlock_max_i_1
       (.I0(rx_state[2]),
        .I1(rx_state[0]),
        .I2(rx_state[1]),
        .I3(rx_state[3]),
        .I4(check_tlock_max_reg_n_0),
        .O(check_tlock_max_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    check_tlock_max_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(check_tlock_max_i_1_n_0),
        .Q(check_tlock_max_reg_n_0),
        .R(out));
  LUT5 #(
    .INIT(32'hFFEF0100)) 
    gtrxreset_i_i_1
       (.I0(rx_state[3]),
        .I1(rx_state[1]),
        .I2(rx_state[2]),
        .I3(rx_state[0]),
        .I4(GTRXRESET),
        .O(gtrxreset_i_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gtrxreset_i_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gtrxreset_i_i_1_n_0),
        .Q(GTRXRESET),
        .R(out));
  LUT3 #(
    .INIT(8'hEA)) 
    gtxe2_i_i_2
       (.I0(GTRXRESET),
        .I1(data_in),
        .I2(gtxe2_i),
        .O(SR));
  LUT1 #(
    .INIT(2'h1)) 
    \init_wait_count[0]_i_1__0 
       (.I0(init_wait_count_reg[0]),
        .O(\init_wait_count[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \init_wait_count[1]_i_1__0 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .O(p_0_in__2[1]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \init_wait_count[2]_i_1__0 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .O(p_0_in__2[2]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \init_wait_count[3]_i_1__0 
       (.I0(init_wait_count_reg[1]),
        .I1(init_wait_count_reg[2]),
        .I2(init_wait_count_reg[0]),
        .I3(init_wait_count_reg[3]),
        .O(p_0_in__2[3]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \init_wait_count[4]_i_1__0 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .O(p_0_in__2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \init_wait_count[5]_i_1__0 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .I5(init_wait_count_reg[5]),
        .O(p_0_in__2[5]));
  LUT4 #(
    .INIT(16'hFEFF)) 
    \init_wait_count[6]_i_1__0 
       (.I0(\init_wait_count[6]_i_3__0_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(\init_wait_count[6]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT4 #(
    .INIT(16'hBF40)) 
    \init_wait_count[6]_i_2__0 
       (.I0(\init_wait_count[6]_i_3__0_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(p_0_in__2[6]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \init_wait_count[6]_i_3__0 
       (.I0(init_wait_count_reg[3]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .I3(init_wait_count_reg[5]),
        .O(\init_wait_count[6]_i_3__0_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(\init_wait_count[0]_i_1__0_n_0 ),
        .Q(init_wait_count_reg[0]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[1]),
        .Q(init_wait_count_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[2]),
        .Q(init_wait_count_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[3]),
        .Q(init_wait_count_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[4]),
        .Q(init_wait_count_reg[4]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[5]),
        .Q(init_wait_count_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[6]),
        .Q(init_wait_count_reg[6]));
  LUT5 #(
    .INIT(32'hFFFF0010)) 
    init_wait_done_i_1__0
       (.I0(\init_wait_count[6]_i_3__0_n_0 ),
        .I1(init_wait_count_reg[4]),
        .I2(init_wait_count_reg[6]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_done_reg_n_0),
        .O(init_wait_done_i_1__0_n_0));
  FDCE #(
    .INIT(1'b0)) 
    init_wait_done_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .CLR(out),
        .D(init_wait_done_i_1__0_n_0),
        .Q(init_wait_done_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[0]_i_1__0 
       (.I0(mmcm_lock_count_reg[0]),
        .O(p_0_in__3[0]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \mmcm_lock_count[1]_i_1__0 
       (.I0(mmcm_lock_count_reg[0]),
        .I1(mmcm_lock_count_reg[1]),
        .O(p_0_in__3[1]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \mmcm_lock_count[2]_i_1__0 
       (.I0(mmcm_lock_count_reg[1]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[2]),
        .O(\mmcm_lock_count[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \mmcm_lock_count[3]_i_1__0 
       (.I0(mmcm_lock_count_reg[2]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[1]),
        .I3(mmcm_lock_count_reg[3]),
        .O(\mmcm_lock_count[3]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \mmcm_lock_count[4]_i_1__0 
       (.I0(mmcm_lock_count_reg[3]),
        .I1(mmcm_lock_count_reg[1]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[2]),
        .I4(mmcm_lock_count_reg[4]),
        .O(\mmcm_lock_count[4]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \mmcm_lock_count[5]_i_1__0 
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(\mmcm_lock_count[5]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \mmcm_lock_count[6]_i_1__0 
       (.I0(mmcm_lock_reclocked_i_2__0_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .O(\mmcm_lock_count[6]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'hBF)) 
    \mmcm_lock_count[7]_i_2__0 
       (.I0(mmcm_lock_reclocked_i_2__0_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \mmcm_lock_count[7]_i_3__0 
       (.I0(mmcm_lock_count_reg[6]),
        .I1(mmcm_lock_reclocked_i_2__0_n_0),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_3__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(p_0_in__3[0]),
        .Q(mmcm_lock_count_reg[0]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(p_0_in__3[1]),
        .Q(mmcm_lock_count_reg[1]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[2]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[2]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[3]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[3]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[4]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[4]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[5]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[5]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[6]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[6]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[7] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[7]_i_3__0_n_0 ),
        .Q(mmcm_lock_count_reg[7]),
        .R(sync_mmcm_lock_reclocked_n_0));
  LUT5 #(
    .INIT(32'hAAEA0000)) 
    mmcm_lock_reclocked_i_1
       (.I0(mmcm_lock_reclocked),
        .I1(mmcm_lock_count_reg[7]),
        .I2(mmcm_lock_count_reg[6]),
        .I3(mmcm_lock_reclocked_i_2__0_n_0),
        .I4(mmcm_lock_i),
        .O(mmcm_lock_reclocked_i_1_n_0));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    mmcm_lock_reclocked_i_2__0
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(mmcm_lock_reclocked_i_2__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    mmcm_lock_reclocked_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(mmcm_lock_reclocked_i_1_n_0),
        .Q(mmcm_lock_reclocked),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT2 #(
    .INIT(4'hE)) 
    reset_time_out_i_3
       (.I0(rx_state[2]),
        .I1(rx_state[3]),
        .O(reset_time_out_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT5 #(
    .INIT(32'h34347674)) 
    reset_time_out_i_4
       (.I0(rx_state[2]),
        .I1(rx_state[3]),
        .I2(rx_state[0]),
        .I3(\FSM_sequential_rx_state_reg[0]_0 ),
        .I4(rx_state[1]),
        .O(reset_time_out_i_4_n_0));
  FDSE #(
    .INIT(1'b0)) 
    reset_time_out_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(sync_data_valid_n_0),
        .Q(reset_time_out_reg_n_0),
        .S(out));
  LUT5 #(
    .INIT(32'hFEFF0010)) 
    run_phase_alignment_int_i_1__0
       (.I0(rx_state[2]),
        .I1(rx_state[1]),
        .I2(rx_state[3]),
        .I3(rx_state[0]),
        .I4(run_phase_alignment_int_reg_n_0),
        .O(run_phase_alignment_int_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(run_phase_alignment_int_i_1__0_n_0),
        .Q(run_phase_alignment_int_reg_n_0),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_s3_reg
       (.C(data_sync_reg6),
        .CE(1'b1),
        .D(data_out_0),
        .Q(run_phase_alignment_int_s3_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT2 #(
    .INIT(4'hB)) 
    rx_fsm_reset_done_int_i_5
       (.I0(rx_state[1]),
        .I1(rx_state[0]),
        .O(rx_fsm_reset_done_int_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT2 #(
    .INIT(4'h2)) 
    rx_fsm_reset_done_int_i_6
       (.I0(rx_state[3]),
        .I1(rx_state[2]),
        .O(rx_fsm_reset_done_int_i_6_n_0));
  FDRE #(
    .INIT(1'b0)) 
    rx_fsm_reset_done_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(sync_data_valid_n_1),
        .Q(data_in),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    rx_fsm_reset_done_int_s3_reg
       (.C(data_sync_reg6),
        .CE(1'b1),
        .D(rx_fsm_reset_done_int_s2),
        .Q(rx_fsm_reset_done_int_s3),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxresetdone_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(rxresetdone_s2),
        .Q(rxresetdone_s3),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_10 sync_RXRESETDONE
       (.data_out(rxresetdone_s2),
        .data_sync_reg1_0(data_sync_reg1),
        .independent_clock_bufg(independent_clock_bufg));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_11 sync_cplllock
       (.\FSM_sequential_rx_state_reg[1] (sync_cplllock_n_0),
        .Q(rx_state[3:1]),
        .data_sync_reg1_0(data_sync_reg1_1),
        .independent_clock_bufg(independent_clock_bufg),
        .rxresetdone_s3(rxresetdone_s3));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_12 sync_data_valid
       (.D({rx_state__0[3],rx_state__0[1:0]}),
        .E(sync_data_valid_n_5),
        .\FSM_sequential_rx_state_reg[0] (\FSM_sequential_rx_state[3]_i_3_n_0 ),
        .\FSM_sequential_rx_state_reg[0]_0 (\FSM_sequential_rx_state[3]_i_7_n_0 ),
        .\FSM_sequential_rx_state_reg[0]_1 (\FSM_sequential_rx_state_reg[0]_0 ),
        .\FSM_sequential_rx_state_reg[0]_2 (\FSM_sequential_rx_state[0]_i_2_n_0 ),
        .\FSM_sequential_rx_state_reg[0]_3 (init_wait_done_reg_n_0),
        .\FSM_sequential_rx_state_reg[1] (sync_data_valid_n_0),
        .\FSM_sequential_rx_state_reg[1]_0 (\FSM_sequential_rx_state[1]_i_3_n_0 ),
        .\FSM_sequential_rx_state_reg[3] (\FSM_sequential_rx_state[3]_i_9_n_0 ),
        .Q(rx_state),
        .data_in(data_in),
        .data_out(data_out),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_lock_reclocked(mmcm_lock_reclocked),
        .reset_time_out_reg(sync_cplllock_n_0),
        .reset_time_out_reg_0(reset_time_out_i_3_n_0),
        .reset_time_out_reg_1(reset_time_out_i_4_n_0),
        .reset_time_out_reg_2(reset_time_out_reg_n_0),
        .rx_fsm_reset_done_int_reg(sync_data_valid_n_1),
        .rx_fsm_reset_done_int_reg_0(rx_fsm_reset_done_int_i_5_n_0),
        .rx_fsm_reset_done_int_reg_1(time_out_100us_reg_n_0),
        .rx_fsm_reset_done_int_reg_2(time_out_1us_reg_n_0),
        .rx_fsm_reset_done_int_reg_3(rx_fsm_reset_done_int_i_6_n_0),
        .time_out_wait_bypass_s3(time_out_wait_bypass_s3));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_13 sync_mmcm_lock_reclocked
       (.SR(sync_mmcm_lock_reclocked_n_0),
        .data_out(mmcm_lock_i),
        .data_sync_reg1_0(data_sync_reg1_0),
        .independent_clock_bufg(independent_clock_bufg));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_14 sync_run_phase_alignment_int
       (.data_in(run_phase_alignment_int_reg_n_0),
        .data_out(data_out_0),
        .data_sync_reg1_0(data_sync_reg6));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_15 sync_time_out_wait_bypass
       (.data_in(time_out_wait_bypass_reg_n_0),
        .data_out(time_out_wait_bypass_s2),
        .independent_clock_bufg(independent_clock_bufg));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_16 sync_tx_fsm_reset_done_int
       (.data_in(data_in),
        .data_out(rx_fsm_reset_done_int_s2),
        .data_sync_reg6_0(data_sync_reg6));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000100)) 
    time_out_100us_i_1
       (.I0(time_out_2ms_i_4_n_0),
        .I1(time_out_counter_reg[17]),
        .I2(time_out_counter_reg[16]),
        .I3(time_out_100us_i_2_n_0),
        .I4(time_out_100us_i_3_n_0),
        .I5(time_out_100us_reg_n_0),
        .O(time_out_100us_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000002000000000)) 
    time_out_100us_i_2
       (.I0(time_out_counter_reg[10]),
        .I1(time_out_counter_reg[12]),
        .I2(time_out_counter_reg[5]),
        .I3(time_out_counter_reg[7]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[14]),
        .O(time_out_100us_i_2_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    time_out_100us_i_3
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[0]),
        .I2(time_out_counter_reg[1]),
        .I3(time_out_counter_reg[15]),
        .I4(time_out_counter_reg[13]),
        .O(time_out_100us_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_100us_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_100us_i_1_n_0),
        .Q(time_out_100us_reg_n_0),
        .R(reset_time_out_reg_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF00100000)) 
    time_out_1us_i_1
       (.I0(time_out_2ms_i_2_n_0),
        .I1(time_out_1us_i_2_n_0),
        .I2(time_out_counter_reg[3]),
        .I3(time_out_counter_reg[2]),
        .I4(time_out_1us_i_3_n_0),
        .I5(time_out_1us_reg_n_0),
        .O(time_out_1us_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'hE)) 
    time_out_1us_i_2
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[17]),
        .O(time_out_1us_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    time_out_1us_i_3
       (.I0(time_out_counter_reg[9]),
        .I1(time_out_counter_reg[11]),
        .I2(time_out_counter_reg[6]),
        .I3(time_out_counter_reg[8]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[12]),
        .O(time_out_1us_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_1us_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_1us_i_1_n_0),
        .Q(time_out_1us_reg_n_0),
        .R(reset_time_out_reg_n_0));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT4 #(
    .INIT(16'hFF01)) 
    time_out_2ms_i_1
       (.I0(time_out_2ms_i_2_n_0),
        .I1(time_out_2ms_i_3__0_n_0),
        .I2(time_out_2ms_i_4_n_0),
        .I3(time_out_2ms_reg_n_0),
        .O(time_out_2ms_i_1_n_0));
  LUT5 #(
    .INIT(32'hFFFFFEFF)) 
    time_out_2ms_i_2
       (.I0(time_out_counter_reg[10]),
        .I1(time_out_counter_reg[14]),
        .I2(time_out_counter_reg[5]),
        .I3(time_out_counter_reg[7]),
        .I4(time_out_100us_i_3_n_0),
        .O(time_out_2ms_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT4 #(
    .INIT(16'hDFFF)) 
    time_out_2ms_i_3__0
       (.I0(time_out_counter_reg[12]),
        .I1(time_out_counter_reg[16]),
        .I2(time_out_counter_reg[18]),
        .I3(time_out_counter_reg[17]),
        .O(time_out_2ms_i_3__0_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEFFFFFF)) 
    time_out_2ms_i_4
       (.I0(time_out_counter_reg[2]),
        .I1(time_out_counter_reg[3]),
        .I2(time_out_counter_reg[8]),
        .I3(time_out_counter_reg[9]),
        .I4(time_out_counter_reg[11]),
        .I5(time_out_counter_reg[6]),
        .O(time_out_2ms_i_4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_2ms_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_2ms_i_1_n_0),
        .Q(time_out_2ms_reg_n_0),
        .R(reset_time_out_reg_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFF7FF)) 
    \time_out_counter[0]_i_1 
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[18]),
        .I2(time_out_counter_reg[16]),
        .I3(time_out_counter_reg[12]),
        .I4(time_out_2ms_i_2_n_0),
        .I5(time_out_2ms_i_4_n_0),
        .O(time_out_counter));
  LUT1 #(
    .INIT(2'h1)) 
    \time_out_counter[0]_i_3 
       (.I0(time_out_counter_reg[0]),
        .O(\time_out_counter[0]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[0] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_7 ),
        .Q(time_out_counter_reg[0]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[0]_i_2__0 
       (.CI(1'b0),
        .CO({\time_out_counter_reg[0]_i_2__0_n_0 ,\time_out_counter_reg[0]_i_2__0_n_1 ,\time_out_counter_reg[0]_i_2__0_n_2 ,\time_out_counter_reg[0]_i_2__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\time_out_counter_reg[0]_i_2__0_n_4 ,\time_out_counter_reg[0]_i_2__0_n_5 ,\time_out_counter_reg[0]_i_2__0_n_6 ,\time_out_counter_reg[0]_i_2__0_n_7 }),
        .S({time_out_counter_reg[3:1],\time_out_counter[0]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[10] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[10]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[11] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[11]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[12] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[12]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[12]_i_1__0 
       (.CI(\time_out_counter_reg[8]_i_1__0_n_0 ),
        .CO({\time_out_counter_reg[12]_i_1__0_n_0 ,\time_out_counter_reg[12]_i_1__0_n_1 ,\time_out_counter_reg[12]_i_1__0_n_2 ,\time_out_counter_reg[12]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[12]_i_1__0_n_4 ,\time_out_counter_reg[12]_i_1__0_n_5 ,\time_out_counter_reg[12]_i_1__0_n_6 ,\time_out_counter_reg[12]_i_1__0_n_7 }),
        .S(time_out_counter_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[13] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[13]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[14] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[14]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[15] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[15]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[16] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[16]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[16]_i_1__0 
       (.CI(\time_out_counter_reg[12]_i_1__0_n_0 ),
        .CO({\NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED [3:2],\time_out_counter_reg[16]_i_1__0_n_2 ,\time_out_counter_reg[16]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED [3],\time_out_counter_reg[16]_i_1__0_n_5 ,\time_out_counter_reg[16]_i_1__0_n_6 ,\time_out_counter_reg[16]_i_1__0_n_7 }),
        .S({1'b0,time_out_counter_reg[18:16]}));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[17] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[17]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[18] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[18]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[1] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_6 ),
        .Q(time_out_counter_reg[1]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[2] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_5 ),
        .Q(time_out_counter_reg[2]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[3] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_4 ),
        .Q(time_out_counter_reg[3]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[4] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[4]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[4]_i_1__0 
       (.CI(\time_out_counter_reg[0]_i_2__0_n_0 ),
        .CO({\time_out_counter_reg[4]_i_1__0_n_0 ,\time_out_counter_reg[4]_i_1__0_n_1 ,\time_out_counter_reg[4]_i_1__0_n_2 ,\time_out_counter_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[4]_i_1__0_n_4 ,\time_out_counter_reg[4]_i_1__0_n_5 ,\time_out_counter_reg[4]_i_1__0_n_6 ,\time_out_counter_reg[4]_i_1__0_n_7 }),
        .S(time_out_counter_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[5] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[5]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[6] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[6]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[7] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[7]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[8] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[8]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[8]_i_1__0 
       (.CI(\time_out_counter_reg[4]_i_1__0_n_0 ),
        .CO({\time_out_counter_reg[8]_i_1__0_n_0 ,\time_out_counter_reg[8]_i_1__0_n_1 ,\time_out_counter_reg[8]_i_1__0_n_2 ,\time_out_counter_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[8]_i_1__0_n_4 ,\time_out_counter_reg[8]_i_1__0_n_5 ,\time_out_counter_reg[8]_i_1__0_n_6 ,\time_out_counter_reg[8]_i_1__0_n_7 }),
        .S(time_out_counter_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[9] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[9]),
        .R(reset_time_out_reg_n_0));
  LUT4 #(
    .INIT(16'hAB00)) 
    time_out_wait_bypass_i_1
       (.I0(time_out_wait_bypass_reg_n_0),
        .I1(rx_fsm_reset_done_int_s3),
        .I2(time_out_wait_bypass_i_2__0_n_0),
        .I3(run_phase_alignment_int_s3_reg_n_0),
        .O(time_out_wait_bypass_i_1_n_0));
  LUT5 #(
    .INIT(32'hFBFFFFFF)) 
    time_out_wait_bypass_i_2__0
       (.I0(time_out_wait_bypass_i_3__0_n_0),
        .I1(wait_bypass_count_reg[1]),
        .I2(wait_bypass_count_reg[11]),
        .I3(wait_bypass_count_reg[0]),
        .I4(time_out_wait_bypass_i_4__0_n_0),
        .O(time_out_wait_bypass_i_2__0_n_0));
  LUT4 #(
    .INIT(16'hDFFF)) 
    time_out_wait_bypass_i_3__0
       (.I0(wait_bypass_count_reg[9]),
        .I1(wait_bypass_count_reg[4]),
        .I2(wait_bypass_count_reg[12]),
        .I3(wait_bypass_count_reg[2]),
        .O(time_out_wait_bypass_i_3__0_n_0));
  LUT6 #(
    .INIT(64'h0000000400000000)) 
    time_out_wait_bypass_i_4__0
       (.I0(wait_bypass_count_reg[5]),
        .I1(wait_bypass_count_reg[7]),
        .I2(wait_bypass_count_reg[3]),
        .I3(wait_bypass_count_reg[6]),
        .I4(wait_bypass_count_reg[10]),
        .I5(wait_bypass_count_reg[8]),
        .O(time_out_wait_bypass_i_4__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_reg
       (.C(data_sync_reg6),
        .CE(1'b1),
        .D(time_out_wait_bypass_i_1_n_0),
        .Q(time_out_wait_bypass_reg_n_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_wait_bypass_s2),
        .Q(time_out_wait_bypass_s3),
        .R(1'b0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 time_tlock_max1_carry
       (.CI(1'b0),
        .CO({time_tlock_max1_carry_n_0,time_tlock_max1_carry_n_1,time_tlock_max1_carry_n_2,time_tlock_max1_carry_n_3}),
        .CYINIT(1'b0),
        .DI({time_tlock_max1_carry_i_1_n_0,time_tlock_max1_carry_i_2_n_0,time_tlock_max1_carry_i_3_n_0,time_tlock_max1_carry_i_4_n_0}),
        .O(NLW_time_tlock_max1_carry_O_UNCONNECTED[3:0]),
        .S({time_tlock_max1_carry_i_5_n_0,time_tlock_max1_carry_i_6_n_0,time_tlock_max1_carry_i_7_n_0,time_tlock_max1_carry_i_8_n_0}));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 time_tlock_max1_carry__0
       (.CI(time_tlock_max1_carry_n_0),
        .CO({time_tlock_max1_carry__0_n_0,time_tlock_max1_carry__0_n_1,time_tlock_max1_carry__0_n_2,time_tlock_max1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({time_out_counter_reg[15],time_tlock_max1_carry__0_i_1_n_0,1'b0,time_tlock_max1_carry__0_i_2_n_0}),
        .O(NLW_time_tlock_max1_carry__0_O_UNCONNECTED[3:0]),
        .S({time_tlock_max1_carry__0_i_3_n_0,time_tlock_max1_carry__0_i_4_n_0,time_tlock_max1_carry__0_i_5_n_0,time_tlock_max1_carry__0_i_6_n_0}));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry__0_i_1
       (.I0(time_out_counter_reg[12]),
        .I1(time_out_counter_reg[13]),
        .O(time_tlock_max1_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    time_tlock_max1_carry__0_i_2
       (.I0(time_out_counter_reg[8]),
        .I1(time_out_counter_reg[9]),
        .O(time_tlock_max1_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry__0_i_3
       (.I0(time_out_counter_reg[14]),
        .I1(time_out_counter_reg[15]),
        .O(time_tlock_max1_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry__0_i_4
       (.I0(time_out_counter_reg[13]),
        .I1(time_out_counter_reg[12]),
        .O(time_tlock_max1_carry__0_i_4_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    time_tlock_max1_carry__0_i_5
       (.I0(time_out_counter_reg[10]),
        .I1(time_out_counter_reg[11]),
        .O(time_tlock_max1_carry__0_i_5_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry__0_i_6
       (.I0(time_out_counter_reg[9]),
        .I1(time_out_counter_reg[8]),
        .O(time_tlock_max1_carry__0_i_6_n_0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 time_tlock_max1_carry__1
       (.CI(time_tlock_max1_carry__0_n_0),
        .CO({NLW_time_tlock_max1_carry__1_CO_UNCONNECTED[3:2],time_tlock_max1,time_tlock_max1_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,time_out_counter_reg[18],time_tlock_max1_carry__1_i_1_n_0}),
        .O(NLW_time_tlock_max1_carry__1_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,time_tlock_max1_carry__1_i_2_n_0,time_tlock_max1_carry__1_i_3_n_0}));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry__1_i_1
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[17]),
        .O(time_tlock_max1_carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    time_tlock_max1_carry__1_i_2
       (.I0(time_out_counter_reg[18]),
        .O(time_tlock_max1_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry__1_i_3
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[16]),
        .O(time_tlock_max1_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_1
       (.I0(time_out_counter_reg[6]),
        .I1(time_out_counter_reg[7]),
        .O(time_tlock_max1_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    time_tlock_max1_carry_i_2
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[5]),
        .O(time_tlock_max1_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_3
       (.I0(time_out_counter_reg[2]),
        .I1(time_out_counter_reg[3]),
        .O(time_tlock_max1_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_4
       (.I0(time_out_counter_reg[0]),
        .I1(time_out_counter_reg[1]),
        .O(time_tlock_max1_carry_i_4_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_5
       (.I0(time_out_counter_reg[7]),
        .I1(time_out_counter_reg[6]),
        .O(time_tlock_max1_carry_i_5_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry_i_6
       (.I0(time_out_counter_reg[5]),
        .I1(time_out_counter_reg[4]),
        .O(time_tlock_max1_carry_i_6_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_7
       (.I0(time_out_counter_reg[3]),
        .I1(time_out_counter_reg[2]),
        .O(time_tlock_max1_carry_i_7_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_8
       (.I0(time_out_counter_reg[1]),
        .I1(time_out_counter_reg[0]),
        .O(time_tlock_max1_carry_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    time_tlock_max_i_1
       (.I0(check_tlock_max_reg_n_0),
        .I1(time_tlock_max1),
        .I2(time_tlock_max),
        .O(time_tlock_max_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_tlock_max_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_tlock_max_i_1_n_0),
        .Q(time_tlock_max),
        .R(reset_time_out_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_1__0 
       (.I0(run_phase_alignment_int_s3_reg_n_0),
        .O(\wait_bypass_count[0]_i_1__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \wait_bypass_count[0]_i_2__0 
       (.I0(time_out_wait_bypass_i_2__0_n_0),
        .I1(rx_fsm_reset_done_int_s3),
        .O(\wait_bypass_count[0]_i_2__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_4 
       (.I0(wait_bypass_count_reg[0]),
        .O(\wait_bypass_count[0]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[0] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_7 ),
        .Q(wait_bypass_count_reg[0]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[0]_i_3__0 
       (.CI(1'b0),
        .CO({\wait_bypass_count_reg[0]_i_3__0_n_0 ,\wait_bypass_count_reg[0]_i_3__0_n_1 ,\wait_bypass_count_reg[0]_i_3__0_n_2 ,\wait_bypass_count_reg[0]_i_3__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\wait_bypass_count_reg[0]_i_3__0_n_4 ,\wait_bypass_count_reg[0]_i_3__0_n_5 ,\wait_bypass_count_reg[0]_i_3__0_n_6 ,\wait_bypass_count_reg[0]_i_3__0_n_7 }),
        .S({wait_bypass_count_reg[3:1],\wait_bypass_count[0]_i_4_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[10] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_5 ),
        .Q(wait_bypass_count_reg[10]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[11] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_4 ),
        .Q(wait_bypass_count_reg[11]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[12] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[12]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[12]_i_1__0 
       (.CI(\wait_bypass_count_reg[8]_i_1__0_n_0 ),
        .CO(\NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED [3:1],\wait_bypass_count_reg[12]_i_1__0_n_7 }),
        .S({1'b0,1'b0,1'b0,wait_bypass_count_reg[12]}));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[1] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_6 ),
        .Q(wait_bypass_count_reg[1]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[2] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_5 ),
        .Q(wait_bypass_count_reg[2]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[3] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_4 ),
        .Q(wait_bypass_count_reg[3]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[4] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[4]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[4]_i_1__0 
       (.CI(\wait_bypass_count_reg[0]_i_3__0_n_0 ),
        .CO({\wait_bypass_count_reg[4]_i_1__0_n_0 ,\wait_bypass_count_reg[4]_i_1__0_n_1 ,\wait_bypass_count_reg[4]_i_1__0_n_2 ,\wait_bypass_count_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[4]_i_1__0_n_4 ,\wait_bypass_count_reg[4]_i_1__0_n_5 ,\wait_bypass_count_reg[4]_i_1__0_n_6 ,\wait_bypass_count_reg[4]_i_1__0_n_7 }),
        .S(wait_bypass_count_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[5] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_6 ),
        .Q(wait_bypass_count_reg[5]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[6] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_5 ),
        .Q(wait_bypass_count_reg[6]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[7] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_4 ),
        .Q(wait_bypass_count_reg[7]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[8] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[8]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[8]_i_1__0 
       (.CI(\wait_bypass_count_reg[4]_i_1__0_n_0 ),
        .CO({\wait_bypass_count_reg[8]_i_1__0_n_0 ,\wait_bypass_count_reg[8]_i_1__0_n_1 ,\wait_bypass_count_reg[8]_i_1__0_n_2 ,\wait_bypass_count_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[8]_i_1__0_n_4 ,\wait_bypass_count_reg[8]_i_1__0_n_5 ,\wait_bypass_count_reg[8]_i_1__0_n_6 ,\wait_bypass_count_reg[8]_i_1__0_n_7 }),
        .S(wait_bypass_count_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[9] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_6 ),
        .Q(wait_bypass_count_reg[9]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \wait_time_cnt[0]_i_1__0 
       (.I0(wait_time_cnt_reg[0]),
        .O(wait_time_cnt0__0));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[1]_i_1__0 
       (.I0(wait_time_cnt_reg[0]),
        .I1(wait_time_cnt_reg[1]),
        .O(\wait_time_cnt[1]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'hE1)) 
    \wait_time_cnt[2]_i_1__0 
       (.I0(wait_time_cnt_reg[1]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[2]),
        .O(\wait_time_cnt[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT4 #(
    .INIT(16'hFE01)) 
    \wait_time_cnt[3]_i_1__0 
       (.I0(wait_time_cnt_reg[2]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[1]),
        .I3(wait_time_cnt_reg[3]),
        .O(\wait_time_cnt[3]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT5 #(
    .INIT(32'hFFFE0001)) 
    \wait_time_cnt[4]_i_1__0 
       (.I0(wait_time_cnt_reg[3]),
        .I1(wait_time_cnt_reg[1]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[2]),
        .I4(wait_time_cnt_reg[4]),
        .O(\wait_time_cnt[4]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000001)) 
    \wait_time_cnt[5]_i_1__0 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[5]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \wait_time_cnt[6]_i_1 
       (.I0(rx_state[0]),
        .I1(rx_state[1]),
        .I2(rx_state[3]),
        .O(\wait_time_cnt[6]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \wait_time_cnt[6]_i_2__0 
       (.I0(\wait_time_cnt[6]_i_4__0_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(\wait_time_cnt[6]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[6]_i_3__0 
       (.I0(\wait_time_cnt[6]_i_4__0_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(\wait_time_cnt[6]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \wait_time_cnt[6]_i_4__0 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[6]_i_4__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(wait_time_cnt0__0),
        .Q(wait_time_cnt_reg[0]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[1]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[1]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[2]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[2]),
        .S(\wait_time_cnt[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[3]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[3]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[4]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[4]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[5]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[5]),
        .S(\wait_time_cnt[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[6]_i_3__0_n_0 ),
        .Q(wait_time_cnt_reg[6]),
        .S(\wait_time_cnt[6]_i_1_n_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_TX_STARTUP_FSM
   (mmcm_reset,
    gt0_cpllreset_t,
    data_in,
    gt0_txuserrdy_t,
    gt0_gttxreset_in0_out,
    independent_clock_bufg,
    data_sync_reg1,
    out,
    gtxe2_i,
    gt0_cpllrefclklost_i,
    data_sync_reg1_0,
    data_sync_reg1_1,
    data_sync_reg1_2);
  output mmcm_reset;
  output gt0_cpllreset_t;
  output data_in;
  output gt0_txuserrdy_t;
  output gt0_gttxreset_in0_out;
  input independent_clock_bufg;
  input data_sync_reg1;
  input [0:0]out;
  input gtxe2_i;
  input gt0_cpllrefclklost_i;
  input data_sync_reg1_0;
  input data_sync_reg1_1;
  input data_sync_reg1_2;

  wire CPLL_RESET_i_1_n_0;
  wire CPLL_RESET_i_2_n_0;
  wire \FSM_sequential_tx_state[0]_i_2_n_0 ;
  wire \FSM_sequential_tx_state[0]_i_3_n_0 ;
  wire \FSM_sequential_tx_state[2]_i_2_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_3_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_4_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_6_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_7_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_8_n_0 ;
  wire GTTXRESET;
  wire MMCM_RESET_i_1_n_0;
  wire TXUSERRDY_i_1_n_0;
  wire clear;
  wire data_in;
  wire data_out;
  wire data_sync_reg1;
  wire data_sync_reg1_0;
  wire data_sync_reg1_1;
  wire data_sync_reg1_2;
  wire gt0_cpllrefclklost_i;
  wire gt0_cpllreset_t;
  wire gt0_gttxreset_in0_out;
  wire gt0_txuserrdy_t;
  wire gttxreset_i_i_1_n_0;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire \init_wait_count[0]_i_1_n_0 ;
  wire \init_wait_count[6]_i_1_n_0 ;
  wire \init_wait_count[6]_i_3_n_0 ;
  wire [6:0]init_wait_count_reg;
  wire init_wait_done_i_1_n_0;
  wire init_wait_done_reg_n_0;
  wire \mmcm_lock_count[2]_i_1_n_0 ;
  wire \mmcm_lock_count[3]_i_1_n_0 ;
  wire \mmcm_lock_count[4]_i_1_n_0 ;
  wire \mmcm_lock_count[5]_i_1_n_0 ;
  wire \mmcm_lock_count[6]_i_1_n_0 ;
  wire \mmcm_lock_count[7]_i_2_n_0 ;
  wire \mmcm_lock_count[7]_i_3_n_0 ;
  wire [7:0]mmcm_lock_count_reg;
  wire mmcm_lock_i;
  wire mmcm_lock_reclocked;
  wire mmcm_lock_reclocked_i_1_n_0;
  wire mmcm_lock_reclocked_i_2_n_0;
  wire mmcm_reset;
  wire [0:0]out;
  wire [6:1]p_0_in__0;
  wire [1:0]p_0_in__1;
  wire pll_reset_asserted_i_1_n_0;
  wire pll_reset_asserted_i_2_n_0;
  wire pll_reset_asserted_reg_n_0;
  wire refclk_stable_count;
  wire \refclk_stable_count[0]_i_3_n_0 ;
  wire \refclk_stable_count[0]_i_4_n_0 ;
  wire \refclk_stable_count[0]_i_5_n_0 ;
  wire \refclk_stable_count[0]_i_6_n_0 ;
  wire \refclk_stable_count[0]_i_7_n_0 ;
  wire \refclk_stable_count[0]_i_8_n_0 ;
  wire \refclk_stable_count[0]_i_9_n_0 ;
  wire [31:0]refclk_stable_count_reg;
  wire \refclk_stable_count_reg[0]_i_2_n_0 ;
  wire \refclk_stable_count_reg[0]_i_2_n_1 ;
  wire \refclk_stable_count_reg[0]_i_2_n_2 ;
  wire \refclk_stable_count_reg[0]_i_2_n_3 ;
  wire \refclk_stable_count_reg[0]_i_2_n_4 ;
  wire \refclk_stable_count_reg[0]_i_2_n_5 ;
  wire \refclk_stable_count_reg[0]_i_2_n_6 ;
  wire \refclk_stable_count_reg[0]_i_2_n_7 ;
  wire \refclk_stable_count_reg[12]_i_1_n_0 ;
  wire \refclk_stable_count_reg[12]_i_1_n_1 ;
  wire \refclk_stable_count_reg[12]_i_1_n_2 ;
  wire \refclk_stable_count_reg[12]_i_1_n_3 ;
  wire \refclk_stable_count_reg[12]_i_1_n_4 ;
  wire \refclk_stable_count_reg[12]_i_1_n_5 ;
  wire \refclk_stable_count_reg[12]_i_1_n_6 ;
  wire \refclk_stable_count_reg[12]_i_1_n_7 ;
  wire \refclk_stable_count_reg[16]_i_1_n_0 ;
  wire \refclk_stable_count_reg[16]_i_1_n_1 ;
  wire \refclk_stable_count_reg[16]_i_1_n_2 ;
  wire \refclk_stable_count_reg[16]_i_1_n_3 ;
  wire \refclk_stable_count_reg[16]_i_1_n_4 ;
  wire \refclk_stable_count_reg[16]_i_1_n_5 ;
  wire \refclk_stable_count_reg[16]_i_1_n_6 ;
  wire \refclk_stable_count_reg[16]_i_1_n_7 ;
  wire \refclk_stable_count_reg[20]_i_1_n_0 ;
  wire \refclk_stable_count_reg[20]_i_1_n_1 ;
  wire \refclk_stable_count_reg[20]_i_1_n_2 ;
  wire \refclk_stable_count_reg[20]_i_1_n_3 ;
  wire \refclk_stable_count_reg[20]_i_1_n_4 ;
  wire \refclk_stable_count_reg[20]_i_1_n_5 ;
  wire \refclk_stable_count_reg[20]_i_1_n_6 ;
  wire \refclk_stable_count_reg[20]_i_1_n_7 ;
  wire \refclk_stable_count_reg[24]_i_1_n_0 ;
  wire \refclk_stable_count_reg[24]_i_1_n_1 ;
  wire \refclk_stable_count_reg[24]_i_1_n_2 ;
  wire \refclk_stable_count_reg[24]_i_1_n_3 ;
  wire \refclk_stable_count_reg[24]_i_1_n_4 ;
  wire \refclk_stable_count_reg[24]_i_1_n_5 ;
  wire \refclk_stable_count_reg[24]_i_1_n_6 ;
  wire \refclk_stable_count_reg[24]_i_1_n_7 ;
  wire \refclk_stable_count_reg[28]_i_1_n_1 ;
  wire \refclk_stable_count_reg[28]_i_1_n_2 ;
  wire \refclk_stable_count_reg[28]_i_1_n_3 ;
  wire \refclk_stable_count_reg[28]_i_1_n_4 ;
  wire \refclk_stable_count_reg[28]_i_1_n_5 ;
  wire \refclk_stable_count_reg[28]_i_1_n_6 ;
  wire \refclk_stable_count_reg[28]_i_1_n_7 ;
  wire \refclk_stable_count_reg[4]_i_1_n_0 ;
  wire \refclk_stable_count_reg[4]_i_1_n_1 ;
  wire \refclk_stable_count_reg[4]_i_1_n_2 ;
  wire \refclk_stable_count_reg[4]_i_1_n_3 ;
  wire \refclk_stable_count_reg[4]_i_1_n_4 ;
  wire \refclk_stable_count_reg[4]_i_1_n_5 ;
  wire \refclk_stable_count_reg[4]_i_1_n_6 ;
  wire \refclk_stable_count_reg[4]_i_1_n_7 ;
  wire \refclk_stable_count_reg[8]_i_1_n_0 ;
  wire \refclk_stable_count_reg[8]_i_1_n_1 ;
  wire \refclk_stable_count_reg[8]_i_1_n_2 ;
  wire \refclk_stable_count_reg[8]_i_1_n_3 ;
  wire \refclk_stable_count_reg[8]_i_1_n_4 ;
  wire \refclk_stable_count_reg[8]_i_1_n_5 ;
  wire \refclk_stable_count_reg[8]_i_1_n_6 ;
  wire \refclk_stable_count_reg[8]_i_1_n_7 ;
  wire refclk_stable_i_1_n_0;
  wire refclk_stable_i_2_n_0;
  wire refclk_stable_i_3_n_0;
  wire refclk_stable_i_4_n_0;
  wire refclk_stable_i_5_n_0;
  wire refclk_stable_i_6_n_0;
  wire refclk_stable_reg_n_0;
  wire reset_time_out;
  wire reset_time_out_i_2__0_n_0;
  wire run_phase_alignment_int_i_1_n_0;
  wire run_phase_alignment_int_reg_n_0;
  wire run_phase_alignment_int_s3;
  wire sel;
  wire sync_cplllock_n_0;
  wire sync_cplllock_n_1;
  wire sync_mmcm_lock_reclocked_n_0;
  wire time_out_2ms_i_1_n_0;
  wire time_out_2ms_i_2__0_n_0;
  wire time_out_2ms_i_3_n_0;
  wire time_out_2ms_i_4__0_n_0;
  wire time_out_2ms_i_5_n_0;
  wire time_out_2ms_reg_n_0;
  wire time_out_500us_i_1_n_0;
  wire time_out_500us_i_2_n_0;
  wire time_out_500us_reg_n_0;
  wire time_out_counter;
  wire \time_out_counter[0]_i_3__0_n_0 ;
  wire \time_out_counter[0]_i_4_n_0 ;
  wire [18:0]time_out_counter_reg;
  wire \time_out_counter_reg[0]_i_2_n_0 ;
  wire \time_out_counter_reg[0]_i_2_n_1 ;
  wire \time_out_counter_reg[0]_i_2_n_2 ;
  wire \time_out_counter_reg[0]_i_2_n_3 ;
  wire \time_out_counter_reg[0]_i_2_n_4 ;
  wire \time_out_counter_reg[0]_i_2_n_5 ;
  wire \time_out_counter_reg[0]_i_2_n_6 ;
  wire \time_out_counter_reg[0]_i_2_n_7 ;
  wire \time_out_counter_reg[12]_i_1_n_0 ;
  wire \time_out_counter_reg[12]_i_1_n_1 ;
  wire \time_out_counter_reg[12]_i_1_n_2 ;
  wire \time_out_counter_reg[12]_i_1_n_3 ;
  wire \time_out_counter_reg[12]_i_1_n_4 ;
  wire \time_out_counter_reg[12]_i_1_n_5 ;
  wire \time_out_counter_reg[12]_i_1_n_6 ;
  wire \time_out_counter_reg[12]_i_1_n_7 ;
  wire \time_out_counter_reg[16]_i_1_n_2 ;
  wire \time_out_counter_reg[16]_i_1_n_3 ;
  wire \time_out_counter_reg[16]_i_1_n_5 ;
  wire \time_out_counter_reg[16]_i_1_n_6 ;
  wire \time_out_counter_reg[16]_i_1_n_7 ;
  wire \time_out_counter_reg[4]_i_1_n_0 ;
  wire \time_out_counter_reg[4]_i_1_n_1 ;
  wire \time_out_counter_reg[4]_i_1_n_2 ;
  wire \time_out_counter_reg[4]_i_1_n_3 ;
  wire \time_out_counter_reg[4]_i_1_n_4 ;
  wire \time_out_counter_reg[4]_i_1_n_5 ;
  wire \time_out_counter_reg[4]_i_1_n_6 ;
  wire \time_out_counter_reg[4]_i_1_n_7 ;
  wire \time_out_counter_reg[8]_i_1_n_0 ;
  wire \time_out_counter_reg[8]_i_1_n_1 ;
  wire \time_out_counter_reg[8]_i_1_n_2 ;
  wire \time_out_counter_reg[8]_i_1_n_3 ;
  wire \time_out_counter_reg[8]_i_1_n_4 ;
  wire \time_out_counter_reg[8]_i_1_n_5 ;
  wire \time_out_counter_reg[8]_i_1_n_6 ;
  wire \time_out_counter_reg[8]_i_1_n_7 ;
  wire time_out_wait_bypass_i_1_n_0;
  wire time_out_wait_bypass_i_2_n_0;
  wire time_out_wait_bypass_i_3_n_0;
  wire time_out_wait_bypass_i_4_n_0;
  wire time_out_wait_bypass_i_5_n_0;
  wire time_out_wait_bypass_reg_n_0;
  wire time_out_wait_bypass_s2;
  wire time_out_wait_bypass_s3;
  wire time_tlock_max_i_1_n_0;
  wire time_tlock_max_i_2_n_0;
  wire time_tlock_max_i_3_n_0;
  wire time_tlock_max_i_4_n_0;
  wire time_tlock_max_reg_n_0;
  wire tx_fsm_reset_done_int_i_1_n_0;
  wire tx_fsm_reset_done_int_s2;
  wire tx_fsm_reset_done_int_s3;
  wire [3:0]tx_state;
  wire [3:0]tx_state__0;
  wire txresetdone_s2;
  wire txresetdone_s3;
  wire \wait_bypass_count[0]_i_2_n_0 ;
  wire \wait_bypass_count[0]_i_4__0_n_0 ;
  wire [16:0]wait_bypass_count_reg;
  wire \wait_bypass_count_reg[0]_i_3_n_0 ;
  wire \wait_bypass_count_reg[0]_i_3_n_1 ;
  wire \wait_bypass_count_reg[0]_i_3_n_2 ;
  wire \wait_bypass_count_reg[0]_i_3_n_3 ;
  wire \wait_bypass_count_reg[0]_i_3_n_4 ;
  wire \wait_bypass_count_reg[0]_i_3_n_5 ;
  wire \wait_bypass_count_reg[0]_i_3_n_6 ;
  wire \wait_bypass_count_reg[0]_i_3_n_7 ;
  wire \wait_bypass_count_reg[12]_i_1_n_0 ;
  wire \wait_bypass_count_reg[12]_i_1_n_1 ;
  wire \wait_bypass_count_reg[12]_i_1_n_2 ;
  wire \wait_bypass_count_reg[12]_i_1_n_3 ;
  wire \wait_bypass_count_reg[12]_i_1_n_4 ;
  wire \wait_bypass_count_reg[12]_i_1_n_5 ;
  wire \wait_bypass_count_reg[12]_i_1_n_6 ;
  wire \wait_bypass_count_reg[12]_i_1_n_7 ;
  wire \wait_bypass_count_reg[16]_i_1_n_7 ;
  wire \wait_bypass_count_reg[4]_i_1_n_0 ;
  wire \wait_bypass_count_reg[4]_i_1_n_1 ;
  wire \wait_bypass_count_reg[4]_i_1_n_2 ;
  wire \wait_bypass_count_reg[4]_i_1_n_3 ;
  wire \wait_bypass_count_reg[4]_i_1_n_4 ;
  wire \wait_bypass_count_reg[4]_i_1_n_5 ;
  wire \wait_bypass_count_reg[4]_i_1_n_6 ;
  wire \wait_bypass_count_reg[4]_i_1_n_7 ;
  wire \wait_bypass_count_reg[8]_i_1_n_0 ;
  wire \wait_bypass_count_reg[8]_i_1_n_1 ;
  wire \wait_bypass_count_reg[8]_i_1_n_2 ;
  wire \wait_bypass_count_reg[8]_i_1_n_3 ;
  wire \wait_bypass_count_reg[8]_i_1_n_4 ;
  wire \wait_bypass_count_reg[8]_i_1_n_5 ;
  wire \wait_bypass_count_reg[8]_i_1_n_6 ;
  wire \wait_bypass_count_reg[8]_i_1_n_7 ;
  wire [0:0]wait_time_cnt0;
  wire wait_time_cnt0_0;
  wire \wait_time_cnt[1]_i_1_n_0 ;
  wire \wait_time_cnt[2]_i_1_n_0 ;
  wire \wait_time_cnt[3]_i_1_n_0 ;
  wire \wait_time_cnt[4]_i_1_n_0 ;
  wire \wait_time_cnt[5]_i_1_n_0 ;
  wire \wait_time_cnt[6]_i_3_n_0 ;
  wire \wait_time_cnt[6]_i_4_n_0 ;
  wire [6:0]wait_time_cnt_reg;
  wire [3:3]\NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED ;
  wire [3:2]\NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED ;
  wire [3:0]\NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:1]\NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'hFFFFFF1F0000001F)) 
    CPLL_RESET_i_1
       (.I0(pll_reset_asserted_reg_n_0),
        .I1(gt0_cpllrefclklost_i),
        .I2(refclk_stable_reg_n_0),
        .I3(CPLL_RESET_i_2_n_0),
        .I4(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I5(gt0_cpllreset_t),
        .O(CPLL_RESET_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT2 #(
    .INIT(4'hE)) 
    CPLL_RESET_i_2
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .O(CPLL_RESET_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    CPLL_RESET_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(CPLL_RESET_i_1_n_0),
        .Q(gt0_cpllreset_t),
        .R(out));
  LUT6 #(
    .INIT(64'hF3FFF3F0F5F0F5F0)) 
    \FSM_sequential_tx_state[0]_i_1 
       (.I0(\FSM_sequential_tx_state[2]_i_2_n_0 ),
        .I1(\FSM_sequential_tx_state[0]_i_2_n_0 ),
        .I2(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I3(tx_state[2]),
        .I4(time_out_2ms_reg_n_0),
        .I5(tx_state[1]),
        .O(tx_state__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_tx_state[0]_i_2 
       (.I0(reset_time_out),
        .I1(time_out_500us_reg_n_0),
        .O(\FSM_sequential_tx_state[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_tx_state[0]_i_3 
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .O(\FSM_sequential_tx_state[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT5 #(
    .INIT(32'h005A001A)) 
    \FSM_sequential_tx_state[1]_i_1 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[0]),
        .I3(tx_state[3]),
        .I4(\FSM_sequential_tx_state[2]_i_2_n_0 ),
        .O(tx_state__0[1]));
  LUT6 #(
    .INIT(64'h04000C0C06020C0C)) 
    \FSM_sequential_tx_state[2]_i_1 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[3]),
        .I3(\FSM_sequential_tx_state[2]_i_2_n_0 ),
        .I4(tx_state[0]),
        .I5(time_out_2ms_reg_n_0),
        .O(tx_state__0[2]));
  LUT3 #(
    .INIT(8'hFD)) 
    \FSM_sequential_tx_state[2]_i_2 
       (.I0(time_tlock_max_reg_n_0),
        .I1(reset_time_out),
        .I2(mmcm_lock_reclocked),
        .O(\FSM_sequential_tx_state[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT5 #(
    .INIT(32'hF4FF4444)) 
    \FSM_sequential_tx_state[3]_i_2 
       (.I0(time_out_wait_bypass_s3),
        .I1(tx_state[3]),
        .I2(reset_time_out),
        .I3(time_out_500us_reg_n_0),
        .I4(\FSM_sequential_tx_state[3]_i_8_n_0 ),
        .O(tx_state__0[3]));
  LUT6 #(
    .INIT(64'h00BA000000000000)) 
    \FSM_sequential_tx_state[3]_i_3 
       (.I0(txresetdone_s3),
        .I1(reset_time_out),
        .I2(time_out_500us_reg_n_0),
        .I3(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I4(tx_state[2]),
        .I5(tx_state[1]),
        .O(\FSM_sequential_tx_state[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000300FF00AA)) 
    \FSM_sequential_tx_state[3]_i_4 
       (.I0(init_wait_done_reg_n_0),
        .I1(\wait_time_cnt[6]_i_4_n_0 ),
        .I2(wait_time_cnt_reg[6]),
        .I3(tx_state[0]),
        .I4(tx_state[3]),
        .I5(CPLL_RESET_i_2_n_0),
        .O(\FSM_sequential_tx_state[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0404040400040000)) 
    \FSM_sequential_tx_state[3]_i_6 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I3(reset_time_out),
        .I4(time_tlock_max_reg_n_0),
        .I5(mmcm_lock_reclocked),
        .O(\FSM_sequential_tx_state[3]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT4 #(
    .INIT(16'h1000)) 
    \FSM_sequential_tx_state[3]_i_7 
       (.I0(tx_state[2]),
        .I1(tx_state[3]),
        .I2(tx_state[0]),
        .I3(tx_state[1]),
        .O(\FSM_sequential_tx_state[3]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    \FSM_sequential_tx_state[3]_i_8 
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .O(\FSM_sequential_tx_state[3]_i_8_n_0 ));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[0] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[0]),
        .Q(tx_state[0]),
        .R(out));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[1] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[1]),
        .Q(tx_state[1]),
        .R(out));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[2] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[2]),
        .Q(tx_state[2]),
        .R(out));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[3] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[3]),
        .Q(tx_state[3]),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT5 #(
    .INIT(32'hFFF70004)) 
    MMCM_RESET_i_1
       (.I0(tx_state[2]),
        .I1(tx_state[0]),
        .I2(tx_state[3]),
        .I3(tx_state[1]),
        .I4(mmcm_reset),
        .O(MMCM_RESET_i_1_n_0));
  FDRE #(
    .INIT(1'b1)) 
    MMCM_RESET_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(MMCM_RESET_i_1_n_0),
        .Q(mmcm_reset),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT5 #(
    .INIT(32'hFFFD2000)) 
    TXUSERRDY_i_1
       (.I0(tx_state[0]),
        .I1(tx_state[3]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .I4(gt0_txuserrdy_t),
        .O(TXUSERRDY_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    TXUSERRDY_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(TXUSERRDY_i_1_n_0),
        .Q(gt0_txuserrdy_t),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT5 #(
    .INIT(32'hFFEF0100)) 
    gttxreset_i_i_1
       (.I0(tx_state[3]),
        .I1(tx_state[1]),
        .I2(tx_state[2]),
        .I3(tx_state[0]),
        .I4(GTTXRESET),
        .O(gttxreset_i_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gttxreset_i_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gttxreset_i_i_1_n_0),
        .Q(GTTXRESET),
        .R(out));
  LUT3 #(
    .INIT(8'hEA)) 
    gtxe2_i_i_3
       (.I0(GTTXRESET),
        .I1(data_in),
        .I2(gtxe2_i),
        .O(gt0_gttxreset_in0_out));
  LUT1 #(
    .INIT(2'h1)) 
    \init_wait_count[0]_i_1 
       (.I0(init_wait_count_reg[0]),
        .O(\init_wait_count[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \init_wait_count[1]_i_1 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \init_wait_count[2]_i_1 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .O(p_0_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \init_wait_count[3]_i_1 
       (.I0(init_wait_count_reg[1]),
        .I1(init_wait_count_reg[2]),
        .I2(init_wait_count_reg[0]),
        .I3(init_wait_count_reg[3]),
        .O(p_0_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \init_wait_count[4]_i_1 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .O(p_0_in__0[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \init_wait_count[5]_i_1 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .I5(init_wait_count_reg[5]),
        .O(p_0_in__0[5]));
  LUT4 #(
    .INIT(16'hFEFF)) 
    \init_wait_count[6]_i_1 
       (.I0(\init_wait_count[6]_i_3_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(\init_wait_count[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT4 #(
    .INIT(16'hBF40)) 
    \init_wait_count[6]_i_2 
       (.I0(\init_wait_count[6]_i_3_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(p_0_in__0[6]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \init_wait_count[6]_i_3 
       (.I0(init_wait_count_reg[3]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .I3(init_wait_count_reg[5]),
        .O(\init_wait_count[6]_i_3_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(\init_wait_count[0]_i_1_n_0 ),
        .Q(init_wait_count_reg[0]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[1]),
        .Q(init_wait_count_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[2]),
        .Q(init_wait_count_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[3]),
        .Q(init_wait_count_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[4]),
        .Q(init_wait_count_reg[4]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[5]),
        .Q(init_wait_count_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[6]),
        .Q(init_wait_count_reg[6]));
  LUT5 #(
    .INIT(32'hFFFF0010)) 
    init_wait_done_i_1
       (.I0(\init_wait_count[6]_i_3_n_0 ),
        .I1(init_wait_count_reg[4]),
        .I2(init_wait_count_reg[6]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_done_reg_n_0),
        .O(init_wait_done_i_1_n_0));
  FDCE #(
    .INIT(1'b0)) 
    init_wait_done_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .CLR(out),
        .D(init_wait_done_i_1_n_0),
        .Q(init_wait_done_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[0]_i_1 
       (.I0(mmcm_lock_count_reg[0]),
        .O(p_0_in__1[0]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \mmcm_lock_count[1]_i_1 
       (.I0(mmcm_lock_count_reg[0]),
        .I1(mmcm_lock_count_reg[1]),
        .O(p_0_in__1[1]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \mmcm_lock_count[2]_i_1 
       (.I0(mmcm_lock_count_reg[1]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[2]),
        .O(\mmcm_lock_count[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \mmcm_lock_count[3]_i_1 
       (.I0(mmcm_lock_count_reg[2]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[1]),
        .I3(mmcm_lock_count_reg[3]),
        .O(\mmcm_lock_count[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \mmcm_lock_count[4]_i_1 
       (.I0(mmcm_lock_count_reg[3]),
        .I1(mmcm_lock_count_reg[1]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[2]),
        .I4(mmcm_lock_count_reg[4]),
        .O(\mmcm_lock_count[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \mmcm_lock_count[5]_i_1 
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(\mmcm_lock_count[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \mmcm_lock_count[6]_i_1 
       (.I0(mmcm_lock_reclocked_i_2_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .O(\mmcm_lock_count[6]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hBF)) 
    \mmcm_lock_count[7]_i_2 
       (.I0(mmcm_lock_reclocked_i_2_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \mmcm_lock_count[7]_i_3 
       (.I0(mmcm_lock_count_reg[6]),
        .I1(mmcm_lock_reclocked_i_2_n_0),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(p_0_in__1[0]),
        .Q(mmcm_lock_count_reg[0]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(p_0_in__1[1]),
        .Q(mmcm_lock_count_reg[1]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[2]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[2]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[3]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[3]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[4]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[4]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[5]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[5]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[6]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[6]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[7] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[7]_i_3_n_0 ),
        .Q(mmcm_lock_count_reg[7]),
        .R(sync_mmcm_lock_reclocked_n_0));
  LUT5 #(
    .INIT(32'hAAEA0000)) 
    mmcm_lock_reclocked_i_1
       (.I0(mmcm_lock_reclocked),
        .I1(mmcm_lock_count_reg[7]),
        .I2(mmcm_lock_count_reg[6]),
        .I3(mmcm_lock_reclocked_i_2_n_0),
        .I4(mmcm_lock_i),
        .O(mmcm_lock_reclocked_i_1_n_0));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    mmcm_lock_reclocked_i_2
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(mmcm_lock_reclocked_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    mmcm_lock_reclocked_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(mmcm_lock_reclocked_i_1_n_0),
        .Q(mmcm_lock_reclocked),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000CD55CCCCCCCC)) 
    pll_reset_asserted_i_1
       (.I0(tx_state[3]),
        .I1(pll_reset_asserted_reg_n_0),
        .I2(gt0_cpllrefclklost_i),
        .I3(refclk_stable_reg_n_0),
        .I4(tx_state[1]),
        .I5(pll_reset_asserted_i_2_n_0),
        .O(pll_reset_asserted_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'h02)) 
    pll_reset_asserted_i_2
       (.I0(tx_state[0]),
        .I1(tx_state[3]),
        .I2(tx_state[2]),
        .O(pll_reset_asserted_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    pll_reset_asserted_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pll_reset_asserted_i_1_n_0),
        .Q(pll_reset_asserted_reg_n_0),
        .R(out));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFFFF)) 
    \refclk_stable_count[0]_i_1 
       (.I0(\refclk_stable_count[0]_i_3_n_0 ),
        .I1(\refclk_stable_count[0]_i_4_n_0 ),
        .I2(\refclk_stable_count[0]_i_5_n_0 ),
        .I3(\refclk_stable_count[0]_i_6_n_0 ),
        .I4(\refclk_stable_count[0]_i_7_n_0 ),
        .I5(\refclk_stable_count[0]_i_8_n_0 ),
        .O(refclk_stable_count));
  LUT6 #(
    .INIT(64'hFFDFFFFFFFFFFFFF)) 
    \refclk_stable_count[0]_i_3 
       (.I0(refclk_stable_count_reg[13]),
        .I1(refclk_stable_count_reg[12]),
        .I2(refclk_stable_count_reg[10]),
        .I3(refclk_stable_count_reg[11]),
        .I4(refclk_stable_count_reg[9]),
        .I5(refclk_stable_count_reg[8]),
        .O(\refclk_stable_count[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFDF)) 
    \refclk_stable_count[0]_i_4 
       (.I0(refclk_stable_count_reg[19]),
        .I1(refclk_stable_count_reg[18]),
        .I2(refclk_stable_count_reg[16]),
        .I3(refclk_stable_count_reg[17]),
        .I4(refclk_stable_count_reg[15]),
        .I5(refclk_stable_count_reg[14]),
        .O(\refclk_stable_count[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \refclk_stable_count[0]_i_5 
       (.I0(refclk_stable_count_reg[30]),
        .I1(refclk_stable_count_reg[31]),
        .I2(refclk_stable_count_reg[28]),
        .I3(refclk_stable_count_reg[29]),
        .I4(refclk_stable_count_reg[27]),
        .I5(refclk_stable_count_reg[26]),
        .O(\refclk_stable_count[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \refclk_stable_count[0]_i_6 
       (.I0(refclk_stable_count_reg[24]),
        .I1(refclk_stable_count_reg[25]),
        .I2(refclk_stable_count_reg[22]),
        .I3(refclk_stable_count_reg[23]),
        .I4(refclk_stable_count_reg[21]),
        .I5(refclk_stable_count_reg[20]),
        .O(\refclk_stable_count[0]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \refclk_stable_count[0]_i_7 
       (.I0(refclk_stable_count_reg[0]),
        .I1(refclk_stable_count_reg[1]),
        .O(\refclk_stable_count[0]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF7)) 
    \refclk_stable_count[0]_i_8 
       (.I0(refclk_stable_count_reg[6]),
        .I1(refclk_stable_count_reg[7]),
        .I2(refclk_stable_count_reg[4]),
        .I3(refclk_stable_count_reg[5]),
        .I4(refclk_stable_count_reg[3]),
        .I5(refclk_stable_count_reg[2]),
        .O(\refclk_stable_count[0]_i_8_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \refclk_stable_count[0]_i_9 
       (.I0(refclk_stable_count_reg[0]),
        .O(\refclk_stable_count[0]_i_9_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_7 ),
        .Q(refclk_stable_count_reg[0]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\refclk_stable_count_reg[0]_i_2_n_0 ,\refclk_stable_count_reg[0]_i_2_n_1 ,\refclk_stable_count_reg[0]_i_2_n_2 ,\refclk_stable_count_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\refclk_stable_count_reg[0]_i_2_n_4 ,\refclk_stable_count_reg[0]_i_2_n_5 ,\refclk_stable_count_reg[0]_i_2_n_6 ,\refclk_stable_count_reg[0]_i_2_n_7 }),
        .S({refclk_stable_count_reg[3:1],\refclk_stable_count[0]_i_9_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[10] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[11] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[12] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[12]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[12]_i_1 
       (.CI(\refclk_stable_count_reg[8]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[12]_i_1_n_0 ,\refclk_stable_count_reg[12]_i_1_n_1 ,\refclk_stable_count_reg[12]_i_1_n_2 ,\refclk_stable_count_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[12]_i_1_n_4 ,\refclk_stable_count_reg[12]_i_1_n_5 ,\refclk_stable_count_reg[12]_i_1_n_6 ,\refclk_stable_count_reg[12]_i_1_n_7 }),
        .S(refclk_stable_count_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[13] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[14] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[15] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[16] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[16]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[16]_i_1 
       (.CI(\refclk_stable_count_reg[12]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[16]_i_1_n_0 ,\refclk_stable_count_reg[16]_i_1_n_1 ,\refclk_stable_count_reg[16]_i_1_n_2 ,\refclk_stable_count_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[16]_i_1_n_4 ,\refclk_stable_count_reg[16]_i_1_n_5 ,\refclk_stable_count_reg[16]_i_1_n_6 ,\refclk_stable_count_reg[16]_i_1_n_7 }),
        .S(refclk_stable_count_reg[19:16]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[17] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[18] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[19] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_6 ),
        .Q(refclk_stable_count_reg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[20] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[20]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[20]_i_1 
       (.CI(\refclk_stable_count_reg[16]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[20]_i_1_n_0 ,\refclk_stable_count_reg[20]_i_1_n_1 ,\refclk_stable_count_reg[20]_i_1_n_2 ,\refclk_stable_count_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[20]_i_1_n_4 ,\refclk_stable_count_reg[20]_i_1_n_5 ,\refclk_stable_count_reg[20]_i_1_n_6 ,\refclk_stable_count_reg[20]_i_1_n_7 }),
        .S(refclk_stable_count_reg[23:20]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[21] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[22] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[23] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[24] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[24]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[24]_i_1 
       (.CI(\refclk_stable_count_reg[20]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[24]_i_1_n_0 ,\refclk_stable_count_reg[24]_i_1_n_1 ,\refclk_stable_count_reg[24]_i_1_n_2 ,\refclk_stable_count_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[24]_i_1_n_4 ,\refclk_stable_count_reg[24]_i_1_n_5 ,\refclk_stable_count_reg[24]_i_1_n_6 ,\refclk_stable_count_reg[24]_i_1_n_7 }),
        .S(refclk_stable_count_reg[27:24]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[25] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[26] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[27] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[28] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[28]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[28]_i_1 
       (.CI(\refclk_stable_count_reg[24]_i_1_n_0 ),
        .CO({\NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED [3],\refclk_stable_count_reg[28]_i_1_n_1 ,\refclk_stable_count_reg[28]_i_1_n_2 ,\refclk_stable_count_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[28]_i_1_n_4 ,\refclk_stable_count_reg[28]_i_1_n_5 ,\refclk_stable_count_reg[28]_i_1_n_6 ,\refclk_stable_count_reg[28]_i_1_n_7 }),
        .S(refclk_stable_count_reg[31:28]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[29] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_5 ),
        .Q(refclk_stable_count_reg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[30] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[31] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_4 ),
        .Q(refclk_stable_count_reg[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[4]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[4]_i_1 
       (.CI(\refclk_stable_count_reg[0]_i_2_n_0 ),
        .CO({\refclk_stable_count_reg[4]_i_1_n_0 ,\refclk_stable_count_reg[4]_i_1_n_1 ,\refclk_stable_count_reg[4]_i_1_n_2 ,\refclk_stable_count_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[4]_i_1_n_4 ,\refclk_stable_count_reg[4]_i_1_n_5 ,\refclk_stable_count_reg[4]_i_1_n_6 ,\refclk_stable_count_reg[4]_i_1_n_7 }),
        .S(refclk_stable_count_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[7] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[8] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[8]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[8]_i_1 
       (.CI(\refclk_stable_count_reg[4]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[8]_i_1_n_0 ,\refclk_stable_count_reg[8]_i_1_n_1 ,\refclk_stable_count_reg[8]_i_1_n_2 ,\refclk_stable_count_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[8]_i_1_n_4 ,\refclk_stable_count_reg[8]_i_1_n_5 ,\refclk_stable_count_reg[8]_i_1_n_6 ,\refclk_stable_count_reg[8]_i_1_n_7 }),
        .S(refclk_stable_count_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[9] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    refclk_stable_i_1
       (.I0(\refclk_stable_count[0]_i_7_n_0 ),
        .I1(refclk_stable_i_2_n_0),
        .I2(refclk_stable_i_3_n_0),
        .I3(refclk_stable_i_4_n_0),
        .I4(refclk_stable_i_5_n_0),
        .I5(refclk_stable_i_6_n_0),
        .O(refclk_stable_i_1_n_0));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    refclk_stable_i_2
       (.I0(refclk_stable_count_reg[4]),
        .I1(refclk_stable_count_reg[5]),
        .I2(refclk_stable_count_reg[2]),
        .I3(refclk_stable_count_reg[3]),
        .I4(refclk_stable_count_reg[7]),
        .I5(refclk_stable_count_reg[6]),
        .O(refclk_stable_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000200000000000)) 
    refclk_stable_i_3
       (.I0(refclk_stable_count_reg[10]),
        .I1(refclk_stable_count_reg[11]),
        .I2(refclk_stable_count_reg[8]),
        .I3(refclk_stable_count_reg[9]),
        .I4(refclk_stable_count_reg[12]),
        .I5(refclk_stable_count_reg[13]),
        .O(refclk_stable_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    refclk_stable_i_4
       (.I0(refclk_stable_count_reg[16]),
        .I1(refclk_stable_count_reg[17]),
        .I2(refclk_stable_count_reg[14]),
        .I3(refclk_stable_count_reg[15]),
        .I4(refclk_stable_count_reg[18]),
        .I5(refclk_stable_count_reg[19]),
        .O(refclk_stable_i_4_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    refclk_stable_i_5
       (.I0(refclk_stable_count_reg[22]),
        .I1(refclk_stable_count_reg[23]),
        .I2(refclk_stable_count_reg[20]),
        .I3(refclk_stable_count_reg[21]),
        .I4(refclk_stable_count_reg[25]),
        .I5(refclk_stable_count_reg[24]),
        .O(refclk_stable_i_5_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    refclk_stable_i_6
       (.I0(refclk_stable_count_reg[28]),
        .I1(refclk_stable_count_reg[29]),
        .I2(refclk_stable_count_reg[26]),
        .I3(refclk_stable_count_reg[27]),
        .I4(refclk_stable_count_reg[31]),
        .I5(refclk_stable_count_reg[30]),
        .O(refclk_stable_i_6_n_0));
  FDRE #(
    .INIT(1'b0)) 
    refclk_stable_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(refclk_stable_i_1_n_0),
        .Q(refclk_stable_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h440000FF50505050)) 
    reset_time_out_i_2__0
       (.I0(tx_state[3]),
        .I1(txresetdone_s3),
        .I2(init_wait_done_reg_n_0),
        .I3(tx_state[1]),
        .I4(tx_state[2]),
        .I5(tx_state[0]),
        .O(reset_time_out_i_2__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    reset_time_out_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(sync_cplllock_n_0),
        .Q(reset_time_out),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT5 #(
    .INIT(32'hFFFB0002)) 
    run_phase_alignment_int_i_1
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .I4(run_phase_alignment_int_reg_n_0),
        .O(run_phase_alignment_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(run_phase_alignment_int_i_1_n_0),
        .Q(run_phase_alignment_int_reg_n_0),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_s3_reg
       (.C(data_sync_reg1),
        .CE(1'b1),
        .D(data_out),
        .Q(run_phase_alignment_int_s3),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_4 sync_TXRESETDONE
       (.data_out(txresetdone_s2),
        .data_sync_reg1_0(data_sync_reg1_0),
        .independent_clock_bufg(independent_clock_bufg));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_5 sync_cplllock
       (.E(sync_cplllock_n_1),
        .\FSM_sequential_tx_state_reg[0] (\FSM_sequential_tx_state[3]_i_3_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_0 (\FSM_sequential_tx_state[3]_i_4_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_1 (\FSM_sequential_tx_state[3]_i_6_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_2 (time_out_2ms_reg_n_0),
        .\FSM_sequential_tx_state_reg[0]_3 (\FSM_sequential_tx_state[3]_i_7_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_4 (pll_reset_asserted_reg_n_0),
        .\FSM_sequential_tx_state_reg[0]_5 (refclk_stable_reg_n_0),
        .\FSM_sequential_tx_state_reg[0]_6 (\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .Q(tx_state),
        .data_sync_reg1_0(data_sync_reg1_2),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_lock_reclocked(mmcm_lock_reclocked),
        .reset_time_out(reset_time_out),
        .reset_time_out_reg(sync_cplllock_n_0),
        .reset_time_out_reg_0(reset_time_out_i_2__0_n_0),
        .reset_time_out_reg_1(init_wait_done_reg_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_6 sync_mmcm_lock_reclocked
       (.SR(sync_mmcm_lock_reclocked_n_0),
        .data_out(mmcm_lock_i),
        .data_sync_reg1_0(data_sync_reg1_1),
        .independent_clock_bufg(independent_clock_bufg));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_7 sync_run_phase_alignment_int
       (.data_in(run_phase_alignment_int_reg_n_0),
        .data_out(data_out),
        .data_sync_reg6_0(data_sync_reg1));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_8 sync_time_out_wait_bypass
       (.data_in(time_out_wait_bypass_reg_n_0),
        .data_out(time_out_wait_bypass_s2),
        .independent_clock_bufg(independent_clock_bufg));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_9 sync_tx_fsm_reset_done_int
       (.data_in(data_in),
        .data_out(tx_fsm_reset_done_int_s2),
        .data_sync_reg1_0(data_sync_reg1));
  LUT4 #(
    .INIT(16'h00AE)) 
    time_out_2ms_i_1
       (.I0(time_out_2ms_reg_n_0),
        .I1(time_out_2ms_i_2__0_n_0),
        .I2(time_out_2ms_i_3_n_0),
        .I3(reset_time_out),
        .O(time_out_2ms_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000800)) 
    time_out_2ms_i_2__0
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[18]),
        .I2(time_out_counter_reg[10]),
        .I3(time_out_counter_reg[12]),
        .I4(time_out_counter_reg[5]),
        .I5(time_tlock_max_i_3_n_0),
        .O(time_out_2ms_i_2__0_n_0));
  LUT4 #(
    .INIT(16'hFFFD)) 
    time_out_2ms_i_3
       (.I0(time_out_counter_reg[7]),
        .I1(time_out_counter_reg[14]),
        .I2(time_out_2ms_i_4__0_n_0),
        .I3(time_out_2ms_i_5_n_0),
        .O(time_out_2ms_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    time_out_2ms_i_4__0
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[3]),
        .I2(time_out_counter_reg[8]),
        .I3(time_out_counter_reg[6]),
        .O(time_out_2ms_i_4__0_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFEF)) 
    time_out_2ms_i_5
       (.I0(time_out_counter_reg[0]),
        .I1(time_out_counter_reg[13]),
        .I2(time_out_counter_reg[9]),
        .I3(time_out_counter_reg[2]),
        .I4(time_out_counter_reg[1]),
        .O(time_out_2ms_i_5_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_2ms_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_2ms_i_1_n_0),
        .Q(time_out_2ms_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000AAAAEAAA)) 
    time_out_500us_i_1
       (.I0(time_out_500us_reg_n_0),
        .I1(time_out_500us_i_2_n_0),
        .I2(time_out_counter_reg[5]),
        .I3(time_out_counter_reg[10]),
        .I4(time_out_2ms_i_3_n_0),
        .I5(reset_time_out),
        .O(time_out_500us_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    time_out_500us_i_2
       (.I0(time_out_counter_reg[15]),
        .I1(time_out_counter_reg[16]),
        .I2(time_out_counter_reg[11]),
        .I3(time_out_counter_reg[12]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[17]),
        .O(time_out_500us_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_500us_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_500us_i_1_n_0),
        .Q(time_out_500us_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFBFFFF)) 
    \time_out_counter[0]_i_1__0 
       (.I0(time_tlock_max_i_3_n_0),
        .I1(\time_out_counter[0]_i_3__0_n_0 ),
        .I2(time_out_2ms_i_3_n_0),
        .I3(time_out_counter_reg[10]),
        .I4(time_out_counter_reg[12]),
        .I5(time_out_counter_reg[5]),
        .O(time_out_counter));
  LUT2 #(
    .INIT(4'h8)) 
    \time_out_counter[0]_i_3__0 
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[18]),
        .O(\time_out_counter[0]_i_3__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \time_out_counter[0]_i_4 
       (.I0(time_out_counter_reg[0]),
        .O(\time_out_counter[0]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[0] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_7 ),
        .Q(time_out_counter_reg[0]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\time_out_counter_reg[0]_i_2_n_0 ,\time_out_counter_reg[0]_i_2_n_1 ,\time_out_counter_reg[0]_i_2_n_2 ,\time_out_counter_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\time_out_counter_reg[0]_i_2_n_4 ,\time_out_counter_reg[0]_i_2_n_5 ,\time_out_counter_reg[0]_i_2_n_6 ,\time_out_counter_reg[0]_i_2_n_7 }),
        .S({time_out_counter_reg[3:1],\time_out_counter[0]_i_4_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[10] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_5 ),
        .Q(time_out_counter_reg[10]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[11] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_4 ),
        .Q(time_out_counter_reg[11]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[12] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_7 ),
        .Q(time_out_counter_reg[12]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[12]_i_1 
       (.CI(\time_out_counter_reg[8]_i_1_n_0 ),
        .CO({\time_out_counter_reg[12]_i_1_n_0 ,\time_out_counter_reg[12]_i_1_n_1 ,\time_out_counter_reg[12]_i_1_n_2 ,\time_out_counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[12]_i_1_n_4 ,\time_out_counter_reg[12]_i_1_n_5 ,\time_out_counter_reg[12]_i_1_n_6 ,\time_out_counter_reg[12]_i_1_n_7 }),
        .S(time_out_counter_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[13] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_6 ),
        .Q(time_out_counter_reg[13]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[14] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_5 ),
        .Q(time_out_counter_reg[14]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[15] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_4 ),
        .Q(time_out_counter_reg[15]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[16] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1_n_7 ),
        .Q(time_out_counter_reg[16]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[16]_i_1 
       (.CI(\time_out_counter_reg[12]_i_1_n_0 ),
        .CO({\NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED [3:2],\time_out_counter_reg[16]_i_1_n_2 ,\time_out_counter_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED [3],\time_out_counter_reg[16]_i_1_n_5 ,\time_out_counter_reg[16]_i_1_n_6 ,\time_out_counter_reg[16]_i_1_n_7 }),
        .S({1'b0,time_out_counter_reg[18:16]}));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[17] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1_n_6 ),
        .Q(time_out_counter_reg[17]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[18] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1_n_5 ),
        .Q(time_out_counter_reg[18]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[1] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_6 ),
        .Q(time_out_counter_reg[1]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[2] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_5 ),
        .Q(time_out_counter_reg[2]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[3] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_4 ),
        .Q(time_out_counter_reg[3]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[4] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_7 ),
        .Q(time_out_counter_reg[4]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[4]_i_1 
       (.CI(\time_out_counter_reg[0]_i_2_n_0 ),
        .CO({\time_out_counter_reg[4]_i_1_n_0 ,\time_out_counter_reg[4]_i_1_n_1 ,\time_out_counter_reg[4]_i_1_n_2 ,\time_out_counter_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[4]_i_1_n_4 ,\time_out_counter_reg[4]_i_1_n_5 ,\time_out_counter_reg[4]_i_1_n_6 ,\time_out_counter_reg[4]_i_1_n_7 }),
        .S(time_out_counter_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[5] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_6 ),
        .Q(time_out_counter_reg[5]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[6] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_5 ),
        .Q(time_out_counter_reg[6]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[7] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_4 ),
        .Q(time_out_counter_reg[7]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[8] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_7 ),
        .Q(time_out_counter_reg[8]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[8]_i_1 
       (.CI(\time_out_counter_reg[4]_i_1_n_0 ),
        .CO({\time_out_counter_reg[8]_i_1_n_0 ,\time_out_counter_reg[8]_i_1_n_1 ,\time_out_counter_reg[8]_i_1_n_2 ,\time_out_counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[8]_i_1_n_4 ,\time_out_counter_reg[8]_i_1_n_5 ,\time_out_counter_reg[8]_i_1_n_6 ,\time_out_counter_reg[8]_i_1_n_7 }),
        .S(time_out_counter_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[9] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_6 ),
        .Q(time_out_counter_reg[9]),
        .R(reset_time_out));
  LUT4 #(
    .INIT(16'hAB00)) 
    time_out_wait_bypass_i_1
       (.I0(time_out_wait_bypass_reg_n_0),
        .I1(tx_fsm_reset_done_int_s3),
        .I2(time_out_wait_bypass_i_2_n_0),
        .I3(run_phase_alignment_int_s3),
        .O(time_out_wait_bypass_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFEFFFFFFFFF)) 
    time_out_wait_bypass_i_2
       (.I0(time_out_wait_bypass_i_3_n_0),
        .I1(time_out_wait_bypass_i_4_n_0),
        .I2(wait_bypass_count_reg[5]),
        .I3(wait_bypass_count_reg[13]),
        .I4(wait_bypass_count_reg[11]),
        .I5(time_out_wait_bypass_i_5_n_0),
        .O(time_out_wait_bypass_i_2_n_0));
  LUT4 #(
    .INIT(16'hFF7F)) 
    time_out_wait_bypass_i_3
       (.I0(wait_bypass_count_reg[16]),
        .I1(wait_bypass_count_reg[9]),
        .I2(wait_bypass_count_reg[12]),
        .I3(wait_bypass_count_reg[10]),
        .O(time_out_wait_bypass_i_3_n_0));
  LUT4 #(
    .INIT(16'hDFFF)) 
    time_out_wait_bypass_i_4
       (.I0(wait_bypass_count_reg[4]),
        .I1(wait_bypass_count_reg[15]),
        .I2(wait_bypass_count_reg[6]),
        .I3(wait_bypass_count_reg[0]),
        .O(time_out_wait_bypass_i_4_n_0));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    time_out_wait_bypass_i_5
       (.I0(wait_bypass_count_reg[8]),
        .I1(wait_bypass_count_reg[1]),
        .I2(wait_bypass_count_reg[7]),
        .I3(wait_bypass_count_reg[14]),
        .I4(wait_bypass_count_reg[2]),
        .I5(wait_bypass_count_reg[3]),
        .O(time_out_wait_bypass_i_5_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_reg
       (.C(data_sync_reg1),
        .CE(1'b1),
        .D(time_out_wait_bypass_i_1_n_0),
        .Q(time_out_wait_bypass_reg_n_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_wait_bypass_s2),
        .Q(time_out_wait_bypass_s3),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000AAAAAAEA)) 
    time_tlock_max_i_1
       (.I0(time_tlock_max_reg_n_0),
        .I1(time_tlock_max_i_2_n_0),
        .I2(time_out_counter_reg[5]),
        .I3(time_tlock_max_i_3_n_0),
        .I4(time_tlock_max_i_4_n_0),
        .I5(reset_time_out),
        .O(time_tlock_max_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    time_tlock_max_i_2
       (.I0(time_out_counter_reg[14]),
        .I1(time_out_counter_reg[12]),
        .I2(time_out_counter_reg[10]),
        .I3(time_out_counter_reg[7]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[17]),
        .O(time_tlock_max_i_2_n_0));
  LUT3 #(
    .INIT(8'hEF)) 
    time_tlock_max_i_3
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[15]),
        .I2(time_out_counter_reg[11]),
        .O(time_tlock_max_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    time_tlock_max_i_4
       (.I0(time_out_2ms_i_5_n_0),
        .I1(time_out_counter_reg[6]),
        .I2(time_out_counter_reg[8]),
        .I3(time_out_counter_reg[3]),
        .I4(time_out_counter_reg[4]),
        .O(time_tlock_max_i_4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_tlock_max_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_tlock_max_i_1_n_0),
        .Q(time_tlock_max_reg_n_0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFFF1000)) 
    tx_fsm_reset_done_int_i_1
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[0]),
        .I3(tx_state[3]),
        .I4(data_in),
        .O(tx_fsm_reset_done_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    tx_fsm_reset_done_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(tx_fsm_reset_done_int_i_1_n_0),
        .Q(data_in),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    tx_fsm_reset_done_int_s3_reg
       (.C(data_sync_reg1),
        .CE(1'b1),
        .D(tx_fsm_reset_done_int_s2),
        .Q(tx_fsm_reset_done_int_s3),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    txresetdone_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(txresetdone_s2),
        .Q(txresetdone_s3),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_1 
       (.I0(run_phase_alignment_int_s3),
        .O(clear));
  LUT2 #(
    .INIT(4'h2)) 
    \wait_bypass_count[0]_i_2 
       (.I0(time_out_wait_bypass_i_2_n_0),
        .I1(tx_fsm_reset_done_int_s3),
        .O(\wait_bypass_count[0]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_4__0 
       (.I0(wait_bypass_count_reg[0]),
        .O(\wait_bypass_count[0]_i_4__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[0] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_7 ),
        .Q(wait_bypass_count_reg[0]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\wait_bypass_count_reg[0]_i_3_n_0 ,\wait_bypass_count_reg[0]_i_3_n_1 ,\wait_bypass_count_reg[0]_i_3_n_2 ,\wait_bypass_count_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\wait_bypass_count_reg[0]_i_3_n_4 ,\wait_bypass_count_reg[0]_i_3_n_5 ,\wait_bypass_count_reg[0]_i_3_n_6 ,\wait_bypass_count_reg[0]_i_3_n_7 }),
        .S({wait_bypass_count_reg[3:1],\wait_bypass_count[0]_i_4__0_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[10] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[10]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[11] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[11]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[12] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[12]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[12]_i_1 
       (.CI(\wait_bypass_count_reg[8]_i_1_n_0 ),
        .CO({\wait_bypass_count_reg[12]_i_1_n_0 ,\wait_bypass_count_reg[12]_i_1_n_1 ,\wait_bypass_count_reg[12]_i_1_n_2 ,\wait_bypass_count_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[12]_i_1_n_4 ,\wait_bypass_count_reg[12]_i_1_n_5 ,\wait_bypass_count_reg[12]_i_1_n_6 ,\wait_bypass_count_reg[12]_i_1_n_7 }),
        .S(wait_bypass_count_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[13] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[13]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[14] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[14]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[15] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[15]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[16] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[16]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[16]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[16]_i_1 
       (.CI(\wait_bypass_count_reg[12]_i_1_n_0 ),
        .CO(\NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED [3:1],\wait_bypass_count_reg[16]_i_1_n_7 }),
        .S({1'b0,1'b0,1'b0,wait_bypass_count_reg[16]}));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[1] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_6 ),
        .Q(wait_bypass_count_reg[1]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[2] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_5 ),
        .Q(wait_bypass_count_reg[2]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[3] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_4 ),
        .Q(wait_bypass_count_reg[3]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[4] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[4]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[4]_i_1 
       (.CI(\wait_bypass_count_reg[0]_i_3_n_0 ),
        .CO({\wait_bypass_count_reg[4]_i_1_n_0 ,\wait_bypass_count_reg[4]_i_1_n_1 ,\wait_bypass_count_reg[4]_i_1_n_2 ,\wait_bypass_count_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[4]_i_1_n_4 ,\wait_bypass_count_reg[4]_i_1_n_5 ,\wait_bypass_count_reg[4]_i_1_n_6 ,\wait_bypass_count_reg[4]_i_1_n_7 }),
        .S(wait_bypass_count_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[5] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[5]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[6] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[6]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[7] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[7]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[8] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[8]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[8]_i_1 
       (.CI(\wait_bypass_count_reg[4]_i_1_n_0 ),
        .CO({\wait_bypass_count_reg[8]_i_1_n_0 ,\wait_bypass_count_reg[8]_i_1_n_1 ,\wait_bypass_count_reg[8]_i_1_n_2 ,\wait_bypass_count_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[8]_i_1_n_4 ,\wait_bypass_count_reg[8]_i_1_n_5 ,\wait_bypass_count_reg[8]_i_1_n_6 ,\wait_bypass_count_reg[8]_i_1_n_7 }),
        .S(wait_bypass_count_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[9] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[9]),
        .R(clear));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \wait_time_cnt[0]_i_1 
       (.I0(wait_time_cnt_reg[0]),
        .O(wait_time_cnt0));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[1]_i_1 
       (.I0(wait_time_cnt_reg[0]),
        .I1(wait_time_cnt_reg[1]),
        .O(\wait_time_cnt[1]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hE1)) 
    \wait_time_cnt[2]_i_1 
       (.I0(wait_time_cnt_reg[1]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[2]),
        .O(\wait_time_cnt[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT4 #(
    .INIT(16'hFE01)) 
    \wait_time_cnt[3]_i_1 
       (.I0(wait_time_cnt_reg[2]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[1]),
        .I3(wait_time_cnt_reg[3]),
        .O(\wait_time_cnt[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT5 #(
    .INIT(32'hFFFE0001)) 
    \wait_time_cnt[4]_i_1 
       (.I0(wait_time_cnt_reg[3]),
        .I1(wait_time_cnt_reg[1]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[2]),
        .I4(wait_time_cnt_reg[4]),
        .O(\wait_time_cnt[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000001)) 
    \wait_time_cnt[5]_i_1 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0700)) 
    \wait_time_cnt[6]_i_1__0 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[3]),
        .I3(tx_state[0]),
        .O(wait_time_cnt0_0));
  LUT2 #(
    .INIT(4'hE)) 
    \wait_time_cnt[6]_i_2 
       (.I0(\wait_time_cnt[6]_i_4_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(sel));
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[6]_i_3 
       (.I0(\wait_time_cnt[6]_i_4_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(\wait_time_cnt[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \wait_time_cnt[6]_i_4 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[6]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[0] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(wait_time_cnt0),
        .Q(wait_time_cnt_reg[0]),
        .R(wait_time_cnt0_0));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[1] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[1]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[1]),
        .R(wait_time_cnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[2] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[2]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[2]),
        .S(wait_time_cnt0_0));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[3] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[3]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[3]),
        .R(wait_time_cnt0_0));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[4] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[4]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[4]),
        .R(wait_time_cnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[5] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[5]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[5]),
        .S(wait_time_cnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[6] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[6]_i_3_n_0 ),
        .Q(wait_time_cnt_reg[6]),
        .S(wait_time_cnt0_0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_block
   (gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    status_vector,
    resetdone,
    txn,
    txp,
    rxoutclk,
    txoutclk,
    mmcm_reset,
    out,
    signal_detect,
    CLK,
    data_in,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    configuration_vector,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    gtxe2_i);
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  output [6:0]status_vector;
  output resetdone;
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output mmcm_reset;
  input [0:0]out;
  input signal_detect;
  input CLK;
  input data_in;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  input [2:0]configuration_vector;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input gtxe2_i;

  wire CLK;
  wire [2:0]configuration_vector;
  wire data_in;
  wire enablealign;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire mgt_rx_reset;
  wire mgt_tx_reset;
  wire mmcm_reset;
  wire [0:0]out;
  wire powerdown;
  wire resetdone;
  wire rx_reset_done_i;
  wire rxbuferr;
  wire rxchariscomma;
  wire rxcharisk;
  wire [1:0]rxclkcorcnt;
  wire [7:0]rxdata;
  wire rxdisperr;
  wire rxn;
  wire rxnotintable;
  wire rxoutclk;
  wire rxp;
  wire signal_detect;
  wire [6:0]status_vector;
  wire transceiver_inst_n_11;
  wire transceiver_inst_n_12;
  wire txbuferr;
  wire txchardispmode;
  wire txchardispval;
  wire txcharisk;
  wire [7:0]txdata;
  wire txn;
  wire txoutclk;
  wire txp;
  wire NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED;
  wire [9:0]NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED;
  wire [15:0]NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED;
  wire [63:0]NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED;
  wire [31:0]NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED;
  wire [47:0]NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED;
  wire [1:0]NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED;
  wire [31:0]NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED;
  wire [1:0]NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED;
  wire [1:0]NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED;
  wire [15:7]NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED;
  wire [9:0]NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED;

  (* B_SHIFTER_ADDR = "10'b0101001110" *) 
  (* C_1588 = "0" *) 
  (* C_2_5G = "FALSE" *) 
  (* C_COMPONENT_NAME = "gig_ethernet_pcs_pma_0" *) 
  (* C_DYNAMIC_SWITCHING = "FALSE" *) 
  (* C_ELABORATION_TRANSIENT_DIR = "BlankString" *) 
  (* C_FAMILY = "kintex7" *) 
  (* C_HAS_AN = "FALSE" *) 
  (* C_HAS_AXIL = "FALSE" *) 
  (* C_HAS_MDIO = "FALSE" *) 
  (* C_HAS_TEMAC = "TRUE" *) 
  (* C_IS_SGMII = "FALSE" *) 
  (* C_RX_GMII_CLK = "TXOUTCLK" *) 
  (* C_SGMII_FABRIC_BUFFER = "TRUE" *) 
  (* C_SGMII_PHY_MODE = "FALSE" *) 
  (* C_USE_LVDS = "FALSE" *) 
  (* C_USE_TBI = "FALSE" *) 
  (* C_USE_TRANSCEIVER = "TRUE" *) 
  (* GT_RX_BYTE_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_v16_2_15 gig_ethernet_pcs_pma_0_core
       (.an_adv_config_val(1'b0),
        .an_adv_config_vector({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .an_enable(NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED),
        .an_interrupt(NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED),
        .an_restart_config(1'b0),
        .basex_or_sgmii(1'b0),
        .configuration_valid(1'b0),
        .configuration_vector({1'b0,configuration_vector,1'b0}),
        .correction_timer({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dcm_locked(data_in),
        .drp_daddr(NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED[9:0]),
        .drp_dclk(1'b0),
        .drp_den(NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED),
        .drp_di(NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED[15:0]),
        .drp_do({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drp_drdy(1'b0),
        .drp_dwe(NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED),
        .drp_gnt(1'b0),
        .drp_req(NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED),
        .en_cdet(NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED),
        .enablealign(enablealign),
        .ewrap(NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gtx_clk(1'b0),
        .link_timer_basex({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_sgmii({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_value({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .loc_ref(NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED),
        .mdc(1'b0),
        .mdio_in(1'b0),
        .mdio_out(NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED),
        .mdio_tri(NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED),
        .mgt_rx_reset(mgt_rx_reset),
        .mgt_tx_reset(mgt_tx_reset),
        .phyad({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pma_rx_clk0(1'b0),
        .pma_rx_clk1(1'b0),
        .powerdown(powerdown),
        .reset(out),
        .reset_done(resetdone),
        .rx_code_group0({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_code_group1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_gt_nominal_latency({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1,1'b1,1'b0,1'b0,1'b0}),
        .rxbufstatus({rxbuferr,1'b0}),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .rxclkcorcnt({1'b0,rxclkcorcnt}),
        .rxdata(rxdata),
        .rxdisperr(rxdisperr),
        .rxnotintable(rxnotintable),
        .rxphy_correction_timer(NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED[63:0]),
        .rxphy_ns_field(NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED[31:0]),
        .rxphy_s_field(NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED[47:0]),
        .rxrecclk(1'b0),
        .rxrundisp(1'b0),
        .s_axi_aclk(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED),
        .s_axi_awvalid(1'b0),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED),
        .s_axi_rdata(NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_resetn(1'b0),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wready(NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED),
        .s_axi_wvalid(1'b0),
        .signal_detect(signal_detect),
        .speed_is_100(1'b0),
        .speed_is_10_100(1'b0),
        .speed_selection(NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED[1:0]),
        .status_vector({NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED[15:7],status_vector}),
        .systemtimer_ns_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .systemtimer_s_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .tx_code_group(NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED[9:0]),
        .txbuferr(txbuferr),
        .txchardispmode(txchardispmode),
        .txchardispval(txchardispval),
        .txcharisk(txcharisk),
        .txdata(txdata),
        .userclk(1'b0),
        .userclk2(CLK));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block sync_block_rx_reset_done
       (.CLK(CLK),
        .data_in(transceiver_inst_n_12),
        .data_out(rx_reset_done_i));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_0 sync_block_tx_reset_done
       (.CLK(CLK),
        .data_in(transceiver_inst_n_11),
        .resetdone(resetdone),
        .resetdone_0(rx_reset_done_i));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_transceiver transceiver_inst
       (.CLK(CLK),
        .D(txchardispmode),
        .Q(rxclkcorcnt),
        .SR(mgt_rx_reset),
        .data_in(transceiver_inst_n_11),
        .data_sync_reg1(data_in),
        .enablealign(enablealign),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(gtxe2_i),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out),
        .powerdown(powerdown),
        .reset_sync5(mgt_tx_reset),
        .rx_fsm_reset_done_int_reg(transceiver_inst_n_12),
        .rxbufstatus(rxbuferr),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .\rxdata_reg[7]_0 (rxdata),
        .rxdisperr(rxdisperr),
        .rxn(rxn),
        .rxnotintable(rxnotintable),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .status_vector(status_vector[1]),
        .txbuferr(txbuferr),
        .txchardispval_reg_reg_0(txchardispval),
        .txcharisk_reg_reg_0(txcharisk),
        .\txdata_reg_reg[7]_0 (txdata),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_clocking
   (gtrefclk_out,
    gtrefclk_bufg,
    mmcm_locked,
    userclk,
    userclk2,
    rxuserclk2_out,
    gtrefclk_p,
    gtrefclk_n,
    txoutclk,
    mmcm_reset,
    rxoutclk);
  output gtrefclk_out;
  output gtrefclk_bufg;
  output mmcm_locked;
  output userclk;
  output userclk2;
  output rxuserclk2_out;
  input gtrefclk_p;
  input gtrefclk_n;
  input txoutclk;
  input mmcm_reset;
  input rxoutclk;

  wire clkfbout;
  wire clkout0;
  wire clkout1;
  wire gtrefclk_bufg;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire mmcm_locked;
  wire mmcm_reset;
  wire rxoutclk;
  wire rxuserclk2_out;
  wire txoutclk;
  wire txoutclk_bufg;
  wire userclk;
  wire userclk2;
  wire NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED;
  wire NLW_mmcm_adv_inst_DRDY_UNCONNECTED;
  wire NLW_mmcm_adv_inst_PSDONE_UNCONNECTED;
  wire [15:0]NLW_mmcm_adv_inst_DO_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_gtrefclk
       (.I(gtrefclk_out),
        .O(gtrefclk_bufg));
  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_txoutclk
       (.I(txoutclk),
        .O(txoutclk_bufg));
  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_userclk
       (.I(clkout1),
        .O(userclk));
  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_userclk2
       (.I(clkout0),
        .O(userclk2));
  (* box_type = "PRIMITIVE" *) 
  IBUFDS_GTE2 #(
    .CLKCM_CFG("TRUE"),
    .CLKRCV_TRST("TRUE"),
    .CLKSWING_CFG(2'b11)) 
    ibufds_gtrefclk
       (.CEB(1'b0),
        .I(gtrefclk_p),
        .IB(gtrefclk_n),
        .O(gtrefclk_out),
        .ODIV2(NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED));
  (* box_type = "PRIMITIVE" *) 
  MMCME2_ADV #(
    .BANDWIDTH("OPTIMIZED"),
    .CLKFBOUT_MULT_F(16.000000),
    .CLKFBOUT_PHASE(0.000000),
    .CLKFBOUT_USE_FINE_PS("FALSE"),
    .CLKIN1_PERIOD(16.000000),
    .CLKIN2_PERIOD(0.000000),
    .CLKOUT0_DIVIDE_F(8.000000),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT0_USE_FINE_PS("FALSE"),
    .CLKOUT1_DIVIDE(16),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUT1_USE_FINE_PS("FALSE"),
    .CLKOUT2_DIVIDE(1),
    .CLKOUT2_DUTY_CYCLE(0.500000),
    .CLKOUT2_PHASE(0.000000),
    .CLKOUT2_USE_FINE_PS("FALSE"),
    .CLKOUT3_DIVIDE(1),
    .CLKOUT3_DUTY_CYCLE(0.500000),
    .CLKOUT3_PHASE(0.000000),
    .CLKOUT3_USE_FINE_PS("FALSE"),
    .CLKOUT4_CASCADE("FALSE"),
    .CLKOUT4_DIVIDE(1),
    .CLKOUT4_DUTY_CYCLE(0.500000),
    .CLKOUT4_PHASE(0.000000),
    .CLKOUT4_USE_FINE_PS("FALSE"),
    .CLKOUT5_DIVIDE(1),
    .CLKOUT5_DUTY_CYCLE(0.500000),
    .CLKOUT5_PHASE(0.000000),
    .CLKOUT5_USE_FINE_PS("FALSE"),
    .CLKOUT6_DIVIDE(1),
    .CLKOUT6_DUTY_CYCLE(0.500000),
    .CLKOUT6_PHASE(0.000000),
    .CLKOUT6_USE_FINE_PS("FALSE"),
    .COMPENSATION("INTERNAL"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKINSEL_INVERTED(1'b0),
    .IS_PSEN_INVERTED(1'b0),
    .IS_PSINCDEC_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER1(0.010000),
    .REF_JITTER2(0.000000),
    .SS_EN("FALSE"),
    .SS_MODE("CENTER_HIGH"),
    .SS_MOD_PERIOD(10000),
    .STARTUP_WAIT("FALSE")) 
    mmcm_adv_inst
       (.CLKFBIN(clkfbout),
        .CLKFBOUT(clkfbout),
        .CLKFBOUTB(NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED),
        .CLKFBSTOPPED(NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED),
        .CLKIN1(txoutclk_bufg),
        .CLKIN2(1'b0),
        .CLKINSEL(1'b1),
        .CLKINSTOPPED(NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED),
        .CLKOUT0(clkout0),
        .CLKOUT0B(NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(clkout1),
        .CLKOUT1B(NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED),
        .CLKOUT2(NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED),
        .CLKOUT2B(NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED),
        .CLKOUT3(NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED),
        .CLKOUT3B(NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED),
        .CLKOUT4(NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED),
        .CLKOUT5(NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED),
        .CLKOUT6(NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_mmcm_adv_inst_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_mmcm_adv_inst_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(mmcm_locked),
        .PSCLK(1'b0),
        .PSDONE(NLW_mmcm_adv_inst_PSDONE_UNCONNECTED),
        .PSEN(1'b0),
        .PSINCDEC(1'b0),
        .PWRDWN(1'b0),
        .RST(mmcm_reset));
  (* box_type = "PRIMITIVE" *) 
  BUFG rxrecclkbufg
       (.I(rxoutclk),
        .O(rxuserclk2_out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_cpll_railing
   (cpll_pd0_i,
    cpllreset_in,
    gtrefclk_bufg,
    gt0_cpllreset_t);
  output cpll_pd0_i;
  output cpllreset_in;
  input gtrefclk_bufg;
  input gt0_cpllreset_t;

  wire cpll_pd0_i;
  wire cpll_reset_out;
  wire \cpllpd_wait_reg[31]_srl32_n_1 ;
  wire \cpllpd_wait_reg[63]_srl32_n_1 ;
  wire \cpllpd_wait_reg[94]_srl31_n_0 ;
  wire cpllreset_in;
  wire \cpllreset_wait_reg[126]_srl31_n_0 ;
  wire \cpllreset_wait_reg[31]_srl32_n_1 ;
  wire \cpllreset_wait_reg[63]_srl32_n_1 ;
  wire \cpllreset_wait_reg[95]_srl32_n_1 ;
  wire gt0_cpllreset_t;
  wire gtrefclk_bufg;
  wire \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED ;

  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[31]_srl32 " *) 
  SRLC32E #(
    .INIT(32'hFFFFFFFF)) 
    \cpllpd_wait_reg[31]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(1'b0),
        .Q(\NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllpd_wait_reg[31]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[63]_srl32 " *) 
  SRLC32E #(
    .INIT(32'hFFFFFFFF)) 
    \cpllpd_wait_reg[63]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllpd_wait_reg[31]_srl32_n_1 ),
        .Q(\NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllpd_wait_reg[63]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[94]_srl31 " *) 
  SRLC32E #(
    .INIT(32'h7FFFFFFF)) 
    \cpllpd_wait_reg[94]_srl31 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b0}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllpd_wait_reg[63]_srl32_n_1 ),
        .Q(\cpllpd_wait_reg[94]_srl31_n_0 ),
        .Q31(\NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED ));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b1)) 
    \cpllpd_wait_reg[95] 
       (.C(gtrefclk_bufg),
        .CE(1'b1),
        .D(\cpllpd_wait_reg[94]_srl31_n_0 ),
        .Q(cpll_pd0_i),
        .R(1'b0));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[126]_srl31 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[126]_srl31 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b0}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllreset_wait_reg[95]_srl32_n_1 ),
        .Q(\cpllreset_wait_reg[126]_srl31_n_0 ),
        .Q31(\NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED ));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \cpllreset_wait_reg[127] 
       (.C(gtrefclk_bufg),
        .CE(1'b1),
        .D(\cpllreset_wait_reg[126]_srl31_n_0 ),
        .Q(cpll_reset_out),
        .R(1'b0));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[31]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h000000FF)) 
    \cpllreset_wait_reg[31]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(1'b0),
        .Q(\NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[31]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[63]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[63]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllreset_wait_reg[31]_srl32_n_1 ),
        .Q(\NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[63]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[95]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[95]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllreset_wait_reg[63]_srl32_n_1 ),
        .Q(\NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[95]_srl32_n_1 ));
  LUT2 #(
    .INIT(4'hE)) 
    gtxe2_i_i_1
       (.I0(cpll_reset_out),
        .I1(gt0_cpllreset_t),
        .O(cpllreset_in));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_gt_common
   (gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    gtrefclk_out,
    independent_clock_bufg,
    out);
  output gt0_qplloutclk_out;
  output gt0_qplloutrefclk_out;
  input gtrefclk_out;
  input independent_clock_bufg;
  input [0:0]out;

  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_out;
  wire gtxe2_common_i_n_2;
  wire gtxe2_common_i_n_5;
  wire independent_clock_bufg;
  wire [0:0]out;
  wire NLW_gtxe2_common_i_DRPRDY_UNCONNECTED;
  wire NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED;
  wire NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED;
  wire [15:0]NLW_gtxe2_common_i_DRPDO_UNCONNECTED;
  wire [7:0]NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  GTXE2_COMMON #(
    .BIAS_CFG(64'h0000040000001000),
    .COMMON_CFG(32'h00000000),
    .IS_DRPCLK_INVERTED(1'b0),
    .IS_GTGREFCLK_INVERTED(1'b0),
    .IS_QPLLLOCKDETCLK_INVERTED(1'b0),
    .QPLL_CFG(27'h06801C1),
    .QPLL_CLKOUT_CFG(4'b0000),
    .QPLL_COARSE_FREQ_OVRD(6'b010000),
    .QPLL_COARSE_FREQ_OVRD_EN(1'b0),
    .QPLL_CP(10'b0000011111),
    .QPLL_CP_MONITOR_EN(1'b0),
    .QPLL_DMONITOR_SEL(1'b0),
    .QPLL_FBDIV(10'b0000100000),
    .QPLL_FBDIV_MONITOR_EN(1'b0),
    .QPLL_FBDIV_RATIO(1'b1),
    .QPLL_INIT_CFG(24'h000006),
    .QPLL_LOCK_CFG(16'h21E8),
    .QPLL_LPF(4'b1111),
    .QPLL_REFCLK_DIV(1),
    .SIM_QPLLREFCLK_SEL(3'b001),
    .SIM_RESET_SPEEDUP("FALSE"),
    .SIM_VERSION("4.0")) 
    gtxe2_common_i
       (.BGBYPASSB(1'b1),
        .BGMONITORENB(1'b1),
        .BGPDB(1'b1),
        .BGRCALOVRD({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DRPADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPCLK(1'b0),
        .DRPDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPDO(NLW_gtxe2_common_i_DRPDO_UNCONNECTED[15:0]),
        .DRPEN(1'b0),
        .DRPRDY(NLW_gtxe2_common_i_DRPRDY_UNCONNECTED),
        .DRPWE(1'b0),
        .GTGREFCLK(1'b0),
        .GTNORTHREFCLK0(1'b0),
        .GTNORTHREFCLK1(1'b0),
        .GTREFCLK0(gtrefclk_out),
        .GTREFCLK1(1'b0),
        .GTSOUTHREFCLK0(1'b0),
        .GTSOUTHREFCLK1(1'b0),
        .PMARSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLDMONITOR(NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED[7:0]),
        .QPLLFBCLKLOST(NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED),
        .QPLLLOCK(gtxe2_common_i_n_2),
        .QPLLLOCKDETCLK(independent_clock_bufg),
        .QPLLLOCKEN(1'b1),
        .QPLLOUTCLK(gt0_qplloutclk_out),
        .QPLLOUTREFCLK(gt0_qplloutrefclk_out),
        .QPLLOUTRESET(1'b0),
        .QPLLPD(1'b1),
        .QPLLREFCLKLOST(gtxe2_common_i_n_5),
        .QPLLREFCLKSEL({1'b0,1'b0,1'b1}),
        .QPLLRESET(out),
        .QPLLRSVD1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLRSVD2({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .RCALENB(1'b1),
        .REFCLKOUTMONITOR(NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync
   (reset_out,
    CLK,
    enablealign);
  output reset_out;
  input CLK;
  input enablealign;

  wire CLK;
  wire enablealign;
  wire reset_out;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(CLK),
        .CE(1'b1),
        .D(1'b0),
        .PRE(enablealign),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(enablealign),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(enablealign),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(enablealign),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(enablealign),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_reset_sync" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_1
   (reset_out,
    independent_clock_bufg,
    SR);
  output reset_out;
  input independent_clock_bufg;
  input [0:0]SR;

  wire [0:0]SR;
  wire independent_clock_bufg;
  wire reset_out;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(SR),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(SR),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(SR),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(SR),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(SR),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_reset_sync" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_2
   (reset_out,
    independent_clock_bufg,
    reset_sync5_0);
  output reset_out;
  input independent_clock_bufg;
  input [0:0]reset_sync5_0;

  wire independent_clock_bufg;
  wire reset_out;
  wire [0:0]reset_sync5_0;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_wtd_timer
   (reset,
    independent_clock_bufg,
    data_out);
  output reset;
  input independent_clock_bufg;
  input data_out;

  wire \counter_stg1[5]_i_1_n_0 ;
  wire \counter_stg1[5]_i_3_n_0 ;
  wire [5:5]counter_stg1_reg;
  wire [4:0]counter_stg1_reg__0;
  wire \counter_stg2[0]_i_3_n_0 ;
  wire [11:0]counter_stg2_reg;
  wire \counter_stg2_reg[0]_i_2_n_0 ;
  wire \counter_stg2_reg[0]_i_2_n_1 ;
  wire \counter_stg2_reg[0]_i_2_n_2 ;
  wire \counter_stg2_reg[0]_i_2_n_3 ;
  wire \counter_stg2_reg[0]_i_2_n_4 ;
  wire \counter_stg2_reg[0]_i_2_n_5 ;
  wire \counter_stg2_reg[0]_i_2_n_6 ;
  wire \counter_stg2_reg[0]_i_2_n_7 ;
  wire \counter_stg2_reg[4]_i_1_n_0 ;
  wire \counter_stg2_reg[4]_i_1_n_1 ;
  wire \counter_stg2_reg[4]_i_1_n_2 ;
  wire \counter_stg2_reg[4]_i_1_n_3 ;
  wire \counter_stg2_reg[4]_i_1_n_4 ;
  wire \counter_stg2_reg[4]_i_1_n_5 ;
  wire \counter_stg2_reg[4]_i_1_n_6 ;
  wire \counter_stg2_reg[4]_i_1_n_7 ;
  wire \counter_stg2_reg[8]_i_1_n_1 ;
  wire \counter_stg2_reg[8]_i_1_n_2 ;
  wire \counter_stg2_reg[8]_i_1_n_3 ;
  wire \counter_stg2_reg[8]_i_1_n_4 ;
  wire \counter_stg2_reg[8]_i_1_n_5 ;
  wire \counter_stg2_reg[8]_i_1_n_6 ;
  wire \counter_stg2_reg[8]_i_1_n_7 ;
  wire counter_stg30;
  wire \counter_stg3[0]_i_3_n_0 ;
  wire \counter_stg3[0]_i_4_n_0 ;
  wire \counter_stg3[0]_i_5_n_0 ;
  wire [11:0]counter_stg3_reg;
  wire \counter_stg3_reg[0]_i_2_n_0 ;
  wire \counter_stg3_reg[0]_i_2_n_1 ;
  wire \counter_stg3_reg[0]_i_2_n_2 ;
  wire \counter_stg3_reg[0]_i_2_n_3 ;
  wire \counter_stg3_reg[0]_i_2_n_4 ;
  wire \counter_stg3_reg[0]_i_2_n_5 ;
  wire \counter_stg3_reg[0]_i_2_n_6 ;
  wire \counter_stg3_reg[0]_i_2_n_7 ;
  wire \counter_stg3_reg[4]_i_1_n_0 ;
  wire \counter_stg3_reg[4]_i_1_n_1 ;
  wire \counter_stg3_reg[4]_i_1_n_2 ;
  wire \counter_stg3_reg[4]_i_1_n_3 ;
  wire \counter_stg3_reg[4]_i_1_n_4 ;
  wire \counter_stg3_reg[4]_i_1_n_5 ;
  wire \counter_stg3_reg[4]_i_1_n_6 ;
  wire \counter_stg3_reg[4]_i_1_n_7 ;
  wire \counter_stg3_reg[8]_i_1_n_1 ;
  wire \counter_stg3_reg[8]_i_1_n_2 ;
  wire \counter_stg3_reg[8]_i_1_n_3 ;
  wire \counter_stg3_reg[8]_i_1_n_4 ;
  wire \counter_stg3_reg[8]_i_1_n_5 ;
  wire \counter_stg3_reg[8]_i_1_n_6 ;
  wire \counter_stg3_reg[8]_i_1_n_7 ;
  wire data_out;
  wire eqOp;
  wire independent_clock_bufg;
  wire [5:0]plusOp;
  wire reset;
  wire reset0;
  wire reset_i_2_n_0;
  wire reset_i_3_n_0;
  wire reset_i_4_n_0;
  wire reset_i_5_n_0;
  wire reset_i_6_n_0;
  wire [3:3]\NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \counter_stg1[0]_i_1 
       (.I0(counter_stg1_reg__0[0]),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \counter_stg1[1]_i_1 
       (.I0(counter_stg1_reg__0[0]),
        .I1(counter_stg1_reg__0[1]),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \counter_stg1[2]_i_1 
       (.I0(counter_stg1_reg__0[1]),
        .I1(counter_stg1_reg__0[0]),
        .I2(counter_stg1_reg__0[2]),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \counter_stg1[3]_i_1 
       (.I0(counter_stg1_reg__0[2]),
        .I1(counter_stg1_reg__0[0]),
        .I2(counter_stg1_reg__0[1]),
        .I3(counter_stg1_reg__0[3]),
        .O(plusOp[3]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \counter_stg1[4]_i_1 
       (.I0(counter_stg1_reg__0[3]),
        .I1(counter_stg1_reg__0[1]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[2]),
        .I4(counter_stg1_reg__0[4]),
        .O(plusOp[4]));
  LUT5 #(
    .INIT(32'hFFFF2000)) 
    \counter_stg1[5]_i_1 
       (.I0(reset_i_2_n_0),
        .I1(counter_stg3_reg[0]),
        .I2(reset_i_3_n_0),
        .I3(\counter_stg1[5]_i_3_n_0 ),
        .I4(data_out),
        .O(\counter_stg1[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \counter_stg1[5]_i_2 
       (.I0(counter_stg1_reg__0[4]),
        .I1(counter_stg1_reg__0[2]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[1]),
        .I4(counter_stg1_reg__0[3]),
        .I5(counter_stg1_reg),
        .O(plusOp[5]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \counter_stg1[5]_i_3 
       (.I0(counter_stg1_reg__0[3]),
        .I1(counter_stg1_reg__0[1]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[2]),
        .I4(counter_stg1_reg__0[4]),
        .O(\counter_stg1[5]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[0]),
        .Q(counter_stg1_reg__0[0]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[1]),
        .Q(counter_stg1_reg__0[1]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[2]),
        .Q(counter_stg1_reg__0[2]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[3]),
        .Q(counter_stg1_reg__0[3]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[4] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[4]),
        .Q(counter_stg1_reg__0[4]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[5] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[5]),
        .Q(counter_stg1_reg),
        .R(\counter_stg1[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter_stg2[0]_i_1 
       (.I0(counter_stg1_reg__0[4]),
        .I1(counter_stg1_reg__0[2]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[1]),
        .I4(counter_stg1_reg__0[3]),
        .I5(counter_stg1_reg),
        .O(eqOp));
  LUT1 #(
    .INIT(2'h1)) 
    \counter_stg2[0]_i_3 
       (.I0(counter_stg2_reg[0]),
        .O(\counter_stg2[0]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[0] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_7 ),
        .Q(counter_stg2_reg[0]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg2_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\counter_stg2_reg[0]_i_2_n_0 ,\counter_stg2_reg[0]_i_2_n_1 ,\counter_stg2_reg[0]_i_2_n_2 ,\counter_stg2_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\counter_stg2_reg[0]_i_2_n_4 ,\counter_stg2_reg[0]_i_2_n_5 ,\counter_stg2_reg[0]_i_2_n_6 ,\counter_stg2_reg[0]_i_2_n_7 }),
        .S({counter_stg2_reg[3:1],\counter_stg2[0]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[10] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_5 ),
        .Q(counter_stg2_reg[10]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[11] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_4 ),
        .Q(counter_stg2_reg[11]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[1] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_6 ),
        .Q(counter_stg2_reg[1]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[2] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_5 ),
        .Q(counter_stg2_reg[2]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[3] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_4 ),
        .Q(counter_stg2_reg[3]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[4] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_7 ),
        .Q(counter_stg2_reg[4]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg2_reg[4]_i_1 
       (.CI(\counter_stg2_reg[0]_i_2_n_0 ),
        .CO({\counter_stg2_reg[4]_i_1_n_0 ,\counter_stg2_reg[4]_i_1_n_1 ,\counter_stg2_reg[4]_i_1_n_2 ,\counter_stg2_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg2_reg[4]_i_1_n_4 ,\counter_stg2_reg[4]_i_1_n_5 ,\counter_stg2_reg[4]_i_1_n_6 ,\counter_stg2_reg[4]_i_1_n_7 }),
        .S(counter_stg2_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[5] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_6 ),
        .Q(counter_stg2_reg[5]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[6] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_5 ),
        .Q(counter_stg2_reg[6]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[7] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_4 ),
        .Q(counter_stg2_reg[7]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[8] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_7 ),
        .Q(counter_stg2_reg[8]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg2_reg[8]_i_1 
       (.CI(\counter_stg2_reg[4]_i_1_n_0 ),
        .CO({\NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED [3],\counter_stg2_reg[8]_i_1_n_1 ,\counter_stg2_reg[8]_i_1_n_2 ,\counter_stg2_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg2_reg[8]_i_1_n_4 ,\counter_stg2_reg[8]_i_1_n_5 ,\counter_stg2_reg[8]_i_1_n_6 ,\counter_stg2_reg[8]_i_1_n_7 }),
        .S(counter_stg2_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[9] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_6 ),
        .Q(counter_stg2_reg[9]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \counter_stg3[0]_i_1 
       (.I0(\counter_stg3[0]_i_3_n_0 ),
        .I1(\counter_stg3[0]_i_4_n_0 ),
        .I2(counter_stg2_reg[0]),
        .I3(\counter_stg1[5]_i_3_n_0 ),
        .O(counter_stg30));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter_stg3[0]_i_3 
       (.I0(counter_stg2_reg[3]),
        .I1(counter_stg2_reg[4]),
        .I2(counter_stg2_reg[1]),
        .I3(counter_stg2_reg[2]),
        .I4(counter_stg2_reg[6]),
        .I5(counter_stg2_reg[5]),
        .O(\counter_stg3[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter_stg3[0]_i_4 
       (.I0(counter_stg2_reg[9]),
        .I1(counter_stg2_reg[10]),
        .I2(counter_stg2_reg[7]),
        .I3(counter_stg2_reg[8]),
        .I4(counter_stg1_reg),
        .I5(counter_stg2_reg[11]),
        .O(\counter_stg3[0]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \counter_stg3[0]_i_5 
       (.I0(counter_stg3_reg[0]),
        .O(\counter_stg3[0]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[0] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_7 ),
        .Q(counter_stg3_reg[0]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg3_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\counter_stg3_reg[0]_i_2_n_0 ,\counter_stg3_reg[0]_i_2_n_1 ,\counter_stg3_reg[0]_i_2_n_2 ,\counter_stg3_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\counter_stg3_reg[0]_i_2_n_4 ,\counter_stg3_reg[0]_i_2_n_5 ,\counter_stg3_reg[0]_i_2_n_6 ,\counter_stg3_reg[0]_i_2_n_7 }),
        .S({counter_stg3_reg[3:1],\counter_stg3[0]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[10] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_5 ),
        .Q(counter_stg3_reg[10]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[11] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_4 ),
        .Q(counter_stg3_reg[11]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[1] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_6 ),
        .Q(counter_stg3_reg[1]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[2] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_5 ),
        .Q(counter_stg3_reg[2]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[3] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_4 ),
        .Q(counter_stg3_reg[3]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[4] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_7 ),
        .Q(counter_stg3_reg[4]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg3_reg[4]_i_1 
       (.CI(\counter_stg3_reg[0]_i_2_n_0 ),
        .CO({\counter_stg3_reg[4]_i_1_n_0 ,\counter_stg3_reg[4]_i_1_n_1 ,\counter_stg3_reg[4]_i_1_n_2 ,\counter_stg3_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg3_reg[4]_i_1_n_4 ,\counter_stg3_reg[4]_i_1_n_5 ,\counter_stg3_reg[4]_i_1_n_6 ,\counter_stg3_reg[4]_i_1_n_7 }),
        .S(counter_stg3_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[5] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_6 ),
        .Q(counter_stg3_reg[5]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[6] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_5 ),
        .Q(counter_stg3_reg[6]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[7] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_4 ),
        .Q(counter_stg3_reg[7]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[8] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_7 ),
        .Q(counter_stg3_reg[8]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg3_reg[8]_i_1 
       (.CI(\counter_stg3_reg[4]_i_1_n_0 ),
        .CO({\NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED [3],\counter_stg3_reg[8]_i_1_n_1 ,\counter_stg3_reg[8]_i_1_n_2 ,\counter_stg3_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg3_reg[8]_i_1_n_4 ,\counter_stg3_reg[8]_i_1_n_5 ,\counter_stg3_reg[8]_i_1_n_6 ,\counter_stg3_reg[8]_i_1_n_7 }),
        .S(counter_stg3_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[9] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_6 ),
        .Q(counter_stg3_reg[9]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h20)) 
    reset_i_1
       (.I0(reset_i_2_n_0),
        .I1(counter_stg3_reg[0]),
        .I2(reset_i_3_n_0),
        .O(reset0));
  LUT6 #(
    .INIT(64'h0000001000000000)) 
    reset_i_2
       (.I0(counter_stg3_reg[9]),
        .I1(counter_stg3_reg[10]),
        .I2(counter_stg3_reg[7]),
        .I3(counter_stg3_reg[8]),
        .I4(counter_stg2_reg[0]),
        .I5(counter_stg3_reg[11]),
        .O(reset_i_2_n_0));
  LUT3 #(
    .INIT(8'h80)) 
    reset_i_3
       (.I0(reset_i_4_n_0),
        .I1(reset_i_5_n_0),
        .I2(reset_i_6_n_0),
        .O(reset_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    reset_i_4
       (.I0(counter_stg2_reg[3]),
        .I1(counter_stg2_reg[4]),
        .I2(counter_stg2_reg[1]),
        .I3(counter_stg2_reg[2]),
        .I4(counter_stg2_reg[6]),
        .I5(counter_stg2_reg[5]),
        .O(reset_i_4_n_0));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    reset_i_5
       (.I0(counter_stg2_reg[10]),
        .I1(counter_stg2_reg[9]),
        .I2(counter_stg2_reg[8]),
        .I3(counter_stg2_reg[7]),
        .I4(counter_stg1_reg),
        .I5(counter_stg2_reg[11]),
        .O(reset_i_5_n_0));
  LUT6 #(
    .INIT(64'h0002000000000000)) 
    reset_i_6
       (.I0(counter_stg3_reg[4]),
        .I1(counter_stg3_reg[3]),
        .I2(counter_stg3_reg[1]),
        .I3(counter_stg3_reg[2]),
        .I4(counter_stg3_reg[6]),
        .I5(counter_stg3_reg[5]),
        .O(reset_i_6_n_0));
  FDRE reset_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset0),
        .Q(reset),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_resets
   (out,
    independent_clock_bufg,
    reset);
  output [0:0]out;
  input independent_clock_bufg;
  input reset;

  wire independent_clock_bufg;
  (* async_reg = "true" *) wire [3:0]pma_reset_pipe;
  wire reset;

  assign out[0] = pma_reset_pipe[3];
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset),
        .Q(pma_reset_pipe[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[0]),
        .PRE(reset),
        .Q(pma_reset_pipe[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[1]),
        .PRE(reset),
        .Q(pma_reset_pipe[2]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[2]),
        .PRE(reset),
        .Q(pma_reset_pipe[3]));
endmodule

(* EXAMPLE_SIMULATION = "0" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support
   (gtrefclk_p,
    gtrefclk_n,
    gtrefclk_out,
    gtrefclk_bufg_out,
    txp,
    txn,
    rxp,
    rxn,
    userclk_out,
    userclk2_out,
    rxuserclk_out,
    rxuserclk2_out,
    pma_reset_out,
    mmcm_locked_out,
    resetdone,
    independent_clock_bufg,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    configuration_vector,
    status_vector,
    reset,
    signal_detect,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out);
  input gtrefclk_p;
  input gtrefclk_n;
  output gtrefclk_out;
  output gtrefclk_bufg_out;
  output txp;
  output txn;
  input rxp;
  input rxn;
  output userclk_out;
  output userclk2_out;
  output rxuserclk_out;
  output rxuserclk2_out;
  output pma_reset_out;
  output mmcm_locked_out;
  output resetdone;
  input independent_clock_bufg;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  input [4:0]configuration_vector;
  output [15:0]status_vector;
  input reset;
  input signal_detect;
  output gt0_qplloutclk_out;
  output gt0_qplloutrefclk_out;

  wire \<const0> ;
  wire [4:0]configuration_vector;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg_out;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire independent_clock_bufg;
  wire mmcm_locked_out;
  wire mmcm_reset;
  wire pma_reset_out;
  wire reset;
  wire resetdone;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire rxuserclk2_out;
  wire signal_detect;
  wire [6:0]\^status_vector ;
  wire txn;
  wire txoutclk;
  wire txp;
  wire userclk2_out;
  wire userclk_out;

  assign rxuserclk_out = rxuserclk2_out;
  assign status_vector[15] = \<const0> ;
  assign status_vector[14] = \<const0> ;
  assign status_vector[13] = \<const0> ;
  assign status_vector[12] = \<const0> ;
  assign status_vector[11] = \<const0> ;
  assign status_vector[10] = \<const0> ;
  assign status_vector[9] = \<const0> ;
  assign status_vector[8] = \<const0> ;
  assign status_vector[7] = \<const0> ;
  assign status_vector[6:0] = \^status_vector [6:0];
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_clocking core_clocking_i
       (.gtrefclk_bufg(gtrefclk_bufg_out),
        .gtrefclk_n(gtrefclk_n),
        .gtrefclk_out(gtrefclk_out),
        .gtrefclk_p(gtrefclk_p),
        .mmcm_locked(mmcm_locked_out),
        .mmcm_reset(mmcm_reset),
        .rxoutclk(rxoutclk),
        .rxuserclk2_out(rxuserclk2_out),
        .txoutclk(txoutclk),
        .userclk(userclk_out),
        .userclk2(userclk2_out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_gt_common core_gt_common_i
       (.gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_out(gtrefclk_out),
        .independent_clock_bufg(independent_clock_bufg),
        .out(pma_reset_out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_resets core_resets_i
       (.independent_clock_bufg(independent_clock_bufg),
        .out(pma_reset_out),
        .reset(reset));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_block pcs_pma_block_i
       (.CLK(userclk2_out),
        .configuration_vector(configuration_vector[3:1]),
        .data_in(mmcm_locked_out),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg_out),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(userclk_out),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(pma_reset_out),
        .resetdone(resetdone),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .signal_detect(signal_detect),
        .status_vector(\^status_vector ),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block
   (data_out,
    data_in,
    CLK);
  output data_out;
  input data_in;
  input CLK;

  wire CLK;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_0
   (resetdone,
    resetdone_0,
    data_in,
    CLK);
  output resetdone;
  input resetdone_0;
  input data_in;
  input CLK;

  wire CLK;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire resetdone;
  wire resetdone_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    resetdone_INST_0
       (.I0(data_out),
        .I1(resetdone_0),
        .O(resetdone));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_10
   (data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_11
   (\FSM_sequential_rx_state_reg[1] ,
    Q,
    rxresetdone_s3,
    data_sync_reg1_0,
    independent_clock_bufg);
  output \FSM_sequential_rx_state_reg[1] ;
  input [2:0]Q;
  input rxresetdone_s3;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire \FSM_sequential_rx_state_reg[1] ;
  wire [2:0]Q;
  wire cplllock_sync;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;
  wire rxresetdone_s3;

  LUT5 #(
    .INIT(32'h008F0080)) 
    \FSM_sequential_rx_state[3]_i_5 
       (.I0(Q[0]),
        .I1(rxresetdone_s3),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(cplllock_sync),
        .O(\FSM_sequential_rx_state_reg[1] ));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(cplllock_sync),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_12
   (\FSM_sequential_rx_state_reg[1] ,
    rx_fsm_reset_done_int_reg,
    D,
    E,
    reset_time_out_reg,
    reset_time_out_reg_0,
    Q,
    reset_time_out_reg_1,
    reset_time_out_reg_2,
    data_in,
    \FSM_sequential_rx_state_reg[1]_0 ,
    rx_fsm_reset_done_int_reg_0,
    rx_fsm_reset_done_int_reg_1,
    \FSM_sequential_rx_state_reg[0] ,
    \FSM_sequential_rx_state_reg[0]_0 ,
    \FSM_sequential_rx_state_reg[0]_1 ,
    mmcm_lock_reclocked,
    \FSM_sequential_rx_state_reg[0]_2 ,
    time_out_wait_bypass_s3,
    \FSM_sequential_rx_state_reg[3] ,
    \FSM_sequential_rx_state_reg[0]_3 ,
    rx_fsm_reset_done_int_reg_2,
    rx_fsm_reset_done_int_reg_3,
    data_out,
    independent_clock_bufg);
  output \FSM_sequential_rx_state_reg[1] ;
  output rx_fsm_reset_done_int_reg;
  output [2:0]D;
  output [0:0]E;
  input reset_time_out_reg;
  input reset_time_out_reg_0;
  input [3:0]Q;
  input reset_time_out_reg_1;
  input reset_time_out_reg_2;
  input data_in;
  input \FSM_sequential_rx_state_reg[1]_0 ;
  input rx_fsm_reset_done_int_reg_0;
  input rx_fsm_reset_done_int_reg_1;
  input \FSM_sequential_rx_state_reg[0] ;
  input \FSM_sequential_rx_state_reg[0]_0 ;
  input \FSM_sequential_rx_state_reg[0]_1 ;
  input mmcm_lock_reclocked;
  input \FSM_sequential_rx_state_reg[0]_2 ;
  input time_out_wait_bypass_s3;
  input \FSM_sequential_rx_state_reg[3] ;
  input \FSM_sequential_rx_state_reg[0]_3 ;
  input rx_fsm_reset_done_int_reg_2;
  input rx_fsm_reset_done_int_reg_3;
  input data_out;
  input independent_clock_bufg;

  wire [2:0]D;
  wire [0:0]E;
  wire \FSM_sequential_rx_state[0]_i_3_n_0 ;
  wire \FSM_sequential_rx_state[1]_i_2_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_4_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_6_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_8_n_0 ;
  wire \FSM_sequential_rx_state_reg[0] ;
  wire \FSM_sequential_rx_state_reg[0]_0 ;
  wire \FSM_sequential_rx_state_reg[0]_1 ;
  wire \FSM_sequential_rx_state_reg[0]_2 ;
  wire \FSM_sequential_rx_state_reg[0]_3 ;
  wire \FSM_sequential_rx_state_reg[1] ;
  wire \FSM_sequential_rx_state_reg[1]_0 ;
  wire \FSM_sequential_rx_state_reg[3] ;
  wire [3:0]Q;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_valid_sync;
  wire independent_clock_bufg;
  wire mmcm_lock_reclocked;
  wire reset_time_out_i_2_n_0;
  wire reset_time_out_reg;
  wire reset_time_out_reg_0;
  wire reset_time_out_reg_1;
  wire reset_time_out_reg_2;
  wire rx_fsm_reset_done_int;
  wire rx_fsm_reset_done_int_i_3_n_0;
  wire rx_fsm_reset_done_int_i_4_n_0;
  wire rx_fsm_reset_done_int_reg;
  wire rx_fsm_reset_done_int_reg_0;
  wire rx_fsm_reset_done_int_reg_1;
  wire rx_fsm_reset_done_int_reg_2;
  wire rx_fsm_reset_done_int_reg_3;
  wire time_out_wait_bypass_s3;

  LUT5 #(
    .INIT(32'hFFEFEFEF)) 
    \FSM_sequential_rx_state[0]_i_1 
       (.I0(\FSM_sequential_rx_state_reg[0]_2 ),
        .I1(\FSM_sequential_rx_state[0]_i_3_n_0 ),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[3]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT4 #(
    .INIT(16'h0200)) 
    \FSM_sequential_rx_state[0]_i_3 
       (.I0(Q[3]),
        .I1(reset_time_out_reg_2),
        .I2(data_valid_sync),
        .I3(rx_fsm_reset_done_int_reg_1),
        .O(\FSM_sequential_rx_state[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF24200400)) 
    \FSM_sequential_rx_state[1]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(\FSM_sequential_rx_state[1]_i_2_n_0 ),
        .I5(\FSM_sequential_rx_state_reg[1]_0 ),
        .O(D[1]));
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_rx_state[1]_i_2 
       (.I0(data_valid_sync),
        .I1(rx_fsm_reset_done_int_reg_1),
        .O(\FSM_sequential_rx_state[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEEE)) 
    \FSM_sequential_rx_state[3]_i_1 
       (.I0(\FSM_sequential_rx_state_reg[0] ),
        .I1(\FSM_sequential_rx_state[3]_i_4_n_0 ),
        .I2(Q[0]),
        .I3(reset_time_out_reg),
        .I4(\FSM_sequential_rx_state[3]_i_6_n_0 ),
        .I5(\FSM_sequential_rx_state_reg[0]_0 ),
        .O(E));
  LUT6 #(
    .INIT(64'hFFFFFFFFCCC0C4C4)) 
    \FSM_sequential_rx_state[3]_i_2 
       (.I0(time_out_wait_bypass_s3),
        .I1(Q[3]),
        .I2(Q[1]),
        .I3(\FSM_sequential_rx_state[3]_i_8_n_0 ),
        .I4(Q[0]),
        .I5(\FSM_sequential_rx_state_reg[3] ),
        .O(D[2]));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAEFEA)) 
    \FSM_sequential_rx_state[3]_i_4 
       (.I0(\FSM_sequential_rx_state[0]_i_3_n_0 ),
        .I1(\FSM_sequential_rx_state_reg[0]_1 ),
        .I2(Q[2]),
        .I3(\FSM_sequential_rx_state_reg[0]_3 ),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\FSM_sequential_rx_state[3]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h0CE20CCC)) 
    \FSM_sequential_rx_state[3]_i_6 
       (.I0(mmcm_lock_reclocked),
        .I1(Q[3]),
        .I2(data_valid_sync),
        .I3(Q[1]),
        .I4(Q[0]),
        .O(\FSM_sequential_rx_state[3]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hFD)) 
    \FSM_sequential_rx_state[3]_i_8 
       (.I0(rx_fsm_reset_done_int_reg_1),
        .I1(data_valid_sync),
        .I2(reset_time_out_reg_2),
        .O(\FSM_sequential_rx_state[3]_i_8_n_0 ));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_out),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_valid_sync),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hEEEFFFFFEEEF0000)) 
    reset_time_out_i_1__0
       (.I0(reset_time_out_i_2_n_0),
        .I1(reset_time_out_reg),
        .I2(reset_time_out_reg_0),
        .I3(Q[1]),
        .I4(reset_time_out_reg_1),
        .I5(reset_time_out_reg_2),
        .O(\FSM_sequential_rx_state_reg[1] ));
  LUT6 #(
    .INIT(64'h0FF30E0E0FF30202)) 
    reset_time_out_i_2
       (.I0(\FSM_sequential_rx_state_reg[0]_1 ),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(data_valid_sync),
        .I4(Q[3]),
        .I5(mmcm_lock_reclocked),
        .O(reset_time_out_i_2_n_0));
  LUT4 #(
    .INIT(16'hABA8)) 
    rx_fsm_reset_done_int_i_1
       (.I0(rx_fsm_reset_done_int),
        .I1(rx_fsm_reset_done_int_i_3_n_0),
        .I2(rx_fsm_reset_done_int_i_4_n_0),
        .I3(data_in),
        .O(rx_fsm_reset_done_int_reg));
  LUT5 #(
    .INIT(32'h00040000)) 
    rx_fsm_reset_done_int_i_2
       (.I0(Q[0]),
        .I1(data_valid_sync),
        .I2(Q[2]),
        .I3(reset_time_out_reg_2),
        .I4(rx_fsm_reset_done_int_reg_2),
        .O(rx_fsm_reset_done_int));
  LUT6 #(
    .INIT(64'h0400040004040400)) 
    rx_fsm_reset_done_int_i_3
       (.I0(rx_fsm_reset_done_int_reg_0),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(data_valid_sync),
        .I4(rx_fsm_reset_done_int_reg_1),
        .I5(reset_time_out_reg_2),
        .O(rx_fsm_reset_done_int_i_3_n_0));
  LUT6 #(
    .INIT(64'h0008000808080008)) 
    rx_fsm_reset_done_int_i_4
       (.I0(rx_fsm_reset_done_int_reg_3),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(data_valid_sync),
        .I4(rx_fsm_reset_done_int_reg_2),
        .I5(reset_time_out_reg_2),
        .O(rx_fsm_reset_done_int_i_4_n_0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_13
   (SR,
    data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output [0:0]SR;
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire [0:0]SR;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[7]_i_1__0 
       (.I0(data_out),
        .O(SR));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_14
   (data_out,
    data_in,
    data_sync_reg1_0);
  output data_out;
  input data_in;
  input data_sync_reg1_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_15
   (data_out,
    data_in,
    independent_clock_bufg);
  output data_out;
  input data_in;
  input independent_clock_bufg;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_16
   (data_out,
    data_in,
    data_sync_reg6_0);
  output data_out;
  input data_in;
  input data_sync_reg6_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg6_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_3
   (data_out,
    status_vector,
    independent_clock_bufg);
  output data_out;
  input [0:0]status_vector;
  input independent_clock_bufg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire independent_clock_bufg;
  wire [0:0]status_vector;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(status_vector),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_4
   (data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_5
   (reset_time_out_reg,
    E,
    reset_time_out_reg_0,
    reset_time_out,
    \FSM_sequential_tx_state_reg[0] ,
    \FSM_sequential_tx_state_reg[0]_0 ,
    \FSM_sequential_tx_state_reg[0]_1 ,
    \FSM_sequential_tx_state_reg[0]_2 ,
    \FSM_sequential_tx_state_reg[0]_3 ,
    Q,
    reset_time_out_reg_1,
    mmcm_lock_reclocked,
    \FSM_sequential_tx_state_reg[0]_4 ,
    \FSM_sequential_tx_state_reg[0]_5 ,
    \FSM_sequential_tx_state_reg[0]_6 ,
    data_sync_reg1_0,
    independent_clock_bufg);
  output reset_time_out_reg;
  output [0:0]E;
  input reset_time_out_reg_0;
  input reset_time_out;
  input \FSM_sequential_tx_state_reg[0] ;
  input \FSM_sequential_tx_state_reg[0]_0 ;
  input \FSM_sequential_tx_state_reg[0]_1 ;
  input \FSM_sequential_tx_state_reg[0]_2 ;
  input \FSM_sequential_tx_state_reg[0]_3 ;
  input [3:0]Q;
  input reset_time_out_reg_1;
  input mmcm_lock_reclocked;
  input \FSM_sequential_tx_state_reg[0]_4 ;
  input \FSM_sequential_tx_state_reg[0]_5 ;
  input \FSM_sequential_tx_state_reg[0]_6 ;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire [0:0]E;
  wire \FSM_sequential_tx_state[3]_i_5_n_0 ;
  wire \FSM_sequential_tx_state_reg[0] ;
  wire \FSM_sequential_tx_state_reg[0]_0 ;
  wire \FSM_sequential_tx_state_reg[0]_1 ;
  wire \FSM_sequential_tx_state_reg[0]_2 ;
  wire \FSM_sequential_tx_state_reg[0]_3 ;
  wire \FSM_sequential_tx_state_reg[0]_4 ;
  wire \FSM_sequential_tx_state_reg[0]_5 ;
  wire \FSM_sequential_tx_state_reg[0]_6 ;
  wire [3:0]Q;
  wire cplllock_sync;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;
  wire mmcm_lock_reclocked;
  wire reset_time_out;
  wire reset_time_out_i_3__0_n_0;
  wire reset_time_out_i_4__0_n_0;
  wire reset_time_out_reg;
  wire reset_time_out_reg_0;
  wire reset_time_out_reg_1;

  LUT6 #(
    .INIT(64'hFFFFFFFEFFFEFFFE)) 
    \FSM_sequential_tx_state[3]_i_1 
       (.I0(\FSM_sequential_tx_state_reg[0] ),
        .I1(\FSM_sequential_tx_state_reg[0]_0 ),
        .I2(\FSM_sequential_tx_state[3]_i_5_n_0 ),
        .I3(\FSM_sequential_tx_state_reg[0]_1 ),
        .I4(\FSM_sequential_tx_state_reg[0]_2 ),
        .I5(\FSM_sequential_tx_state_reg[0]_3 ),
        .O(E));
  LUT6 #(
    .INIT(64'h0000000000F00008)) 
    \FSM_sequential_tx_state[3]_i_5 
       (.I0(\FSM_sequential_tx_state_reg[0]_4 ),
        .I1(\FSM_sequential_tx_state_reg[0]_5 ),
        .I2(cplllock_sync),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(\FSM_sequential_tx_state_reg[0]_6 ),
        .O(\FSM_sequential_tx_state[3]_i_5_n_0 ));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(cplllock_sync),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hEFE0)) 
    reset_time_out_i_1
       (.I0(reset_time_out_reg_0),
        .I1(reset_time_out_i_3__0_n_0),
        .I2(reset_time_out_i_4__0_n_0),
        .I3(reset_time_out),
        .O(reset_time_out_reg));
  LUT6 #(
    .INIT(64'h020002000F000200)) 
    reset_time_out_i_3__0
       (.I0(cplllock_sync),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(Q[0]),
        .I4(mmcm_lock_reclocked),
        .I5(Q[1]),
        .O(reset_time_out_i_3__0_n_0));
  LUT6 #(
    .INIT(64'h0505FF040505F504)) 
    reset_time_out_i_4__0
       (.I0(Q[1]),
        .I1(reset_time_out_reg_1),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(Q[3]),
        .I5(cplllock_sync),
        .O(reset_time_out_i_4__0_n_0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_6
   (SR,
    data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output [0:0]SR;
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire [0:0]SR;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[7]_i_1 
       (.I0(data_out),
        .O(SR));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_7
   (data_out,
    data_in,
    data_sync_reg6_0);
  output data_out;
  input data_in;
  input data_sync_reg6_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg6_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_8
   (data_out,
    data_in,
    independent_clock_bufg);
  output data_out;
  input data_in;
  input independent_clock_bufg;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_9
   (data_out,
    data_in,
    data_sync_reg1_0);
  output data_out;
  input data_in;
  input data_sync_reg1_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_transceiver
   (txn,
    txp,
    rxoutclk,
    txoutclk,
    rxchariscomma,
    rxcharisk,
    rxdisperr,
    rxnotintable,
    rxbufstatus,
    txbuferr,
    mmcm_reset,
    data_in,
    rx_fsm_reset_done_int_reg,
    Q,
    \rxdata_reg[7]_0 ,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    gtxe2_i,
    SR,
    CLK,
    powerdown,
    reset_sync5,
    D,
    txchardispval_reg_reg_0,
    txcharisk_reg_reg_0,
    out,
    status_vector,
    enablealign,
    data_sync_reg1,
    \txdata_reg_reg[7]_0 );
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output [0:0]rxchariscomma;
  output [0:0]rxcharisk;
  output [0:0]rxdisperr;
  output [0:0]rxnotintable;
  output [0:0]rxbufstatus;
  output txbuferr;
  output mmcm_reset;
  output data_in;
  output rx_fsm_reset_done_int_reg;
  output [1:0]Q;
  output [7:0]\rxdata_reg[7]_0 ;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input gtxe2_i;
  input [0:0]SR;
  input CLK;
  input powerdown;
  input [0:0]reset_sync5;
  input [0:0]D;
  input [0:0]txchardispval_reg_reg_0;
  input [0:0]txcharisk_reg_reg_0;
  input [0:0]out;
  input [0:0]status_vector;
  input enablealign;
  input data_sync_reg1;
  input [7:0]\txdata_reg_reg[7]_0 ;

  wire CLK;
  wire [0:0]D;
  wire [1:0]Q;
  wire [0:0]SR;
  wire data_in;
  wire data_sync_reg1;
  wire data_valid_reg2;
  wire enablealign;
  wire encommaalign_int;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtwizard_inst_n_6;
  wire gtwizard_inst_n_7;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire mmcm_reset;
  wire [0:0]out;
  wire p_0_in;
  wire powerdown;
  wire reset;
  wire [0:0]reset_sync5;
  wire rx_fsm_reset_done_int_reg;
  wire [0:0]rxbufstatus;
  wire [0:0]rxchariscomma;
  wire [1:0]rxchariscomma_double;
  wire rxchariscomma_i_1_n_0;
  wire [1:0]rxchariscomma_int;
  wire [1:0]rxchariscomma_reg__0;
  wire [0:0]rxcharisk;
  wire [1:0]rxcharisk_double;
  wire rxcharisk_i_1_n_0;
  wire [1:0]rxcharisk_int;
  wire [1:0]rxcharisk_reg__0;
  wire [1:0]rxclkcorcnt_double;
  wire [1:0]rxclkcorcnt_int;
  wire [1:0]rxclkcorcnt_reg;
  wire \rxdata[0]_i_1_n_0 ;
  wire \rxdata[1]_i_1_n_0 ;
  wire \rxdata[2]_i_1_n_0 ;
  wire \rxdata[3]_i_1_n_0 ;
  wire \rxdata[4]_i_1_n_0 ;
  wire \rxdata[5]_i_1_n_0 ;
  wire \rxdata[6]_i_1_n_0 ;
  wire \rxdata[7]_i_1_n_0 ;
  wire [15:0]rxdata_double;
  wire [15:0]rxdata_int;
  wire [15:0]rxdata_reg;
  wire [7:0]\rxdata_reg[7]_0 ;
  wire [0:0]rxdisperr;
  wire [1:0]rxdisperr_double;
  wire rxdisperr_i_1_n_0;
  wire [1:0]rxdisperr_int;
  wire [1:0]rxdisperr_reg__0;
  wire rxn;
  wire [0:0]rxnotintable;
  wire [1:0]rxnotintable_double;
  wire rxnotintable_i_1_n_0;
  wire [1:0]rxnotintable_int;
  wire [1:0]rxnotintable_reg__0;
  wire rxoutclk;
  wire rxp;
  wire rxpowerdown;
  wire rxpowerdown_double;
  wire rxpowerdown_reg__0;
  wire rxreset_int;
  wire [0:0]status_vector;
  wire toggle;
  wire toggle_i_1_n_0;
  wire toggle_rx;
  wire toggle_rx_i_1_n_0;
  wire txbuferr;
  wire [1:1]txbufstatus_reg;
  wire [1:0]txchardispmode_double;
  wire [1:0]txchardispmode_int;
  wire txchardispmode_reg;
  wire [1:0]txchardispval_double;
  wire [1:0]txchardispval_int;
  wire txchardispval_reg;
  wire [0:0]txchardispval_reg_reg_0;
  wire [1:0]txcharisk_double;
  wire [1:0]txcharisk_int;
  wire txcharisk_reg;
  wire [0:0]txcharisk_reg_reg_0;
  wire [15:0]txdata_double;
  wire [15:0]txdata_int;
  wire [7:0]txdata_reg;
  wire [7:0]\txdata_reg_reg[7]_0 ;
  wire txn;
  wire txoutclk;
  wire txp;
  wire txpowerdown;
  wire txpowerdown_double;
  wire txpowerdown_reg__0;
  wire txreset_int;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD gtwizard_inst
       (.D(rxclkcorcnt_int),
        .Q(txdata_int),
        .RXBUFSTATUS(gtwizard_inst_n_7),
        .RXPD(rxpowerdown),
        .TXBUFSTATUS(gtwizard_inst_n_6),
        .TXPD(txpowerdown),
        .data_in(data_in),
        .data_out(data_valid_reg2),
        .data_sync_reg1(data_sync_reg1),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(rxdata_int),
        .gtxe2_i_0(rxchariscomma_int),
        .gtxe2_i_1(rxcharisk_int),
        .gtxe2_i_2(rxdisperr_int),
        .gtxe2_i_3(rxnotintable_int),
        .gtxe2_i_4(gtxe2_i),
        .gtxe2_i_5(txchardispmode_int),
        .gtxe2_i_6(txchardispval_int),
        .gtxe2_i_7(txcharisk_int),
        .gtxe2_i_8(rxreset_int),
        .gtxe2_i_9(txreset_int),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out),
        .reset(reset),
        .reset_out(encommaalign_int),
        .rx_fsm_reset_done_int_reg(rx_fsm_reset_done_int_reg),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync reclock_encommaalign
       (.CLK(CLK),
        .enablealign(enablealign),
        .reset_out(encommaalign_int));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_1 reclock_rxreset
       (.SR(SR),
        .independent_clock_bufg(independent_clock_bufg),
        .reset_out(rxreset_int));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_2 reclock_txreset
       (.independent_clock_bufg(independent_clock_bufg),
        .reset_out(txreset_int),
        .reset_sync5_0(reset_sync5));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_wtd_timer reset_wtd_timer
       (.data_out(data_valid_reg2),
        .independent_clock_bufg(independent_clock_bufg),
        .reset(reset));
  FDRE rxbuferr_reg
       (.C(CLK),
        .CE(1'b1),
        .D(p_0_in),
        .Q(rxbufstatus),
        .R(1'b0));
  FDRE \rxbufstatus_reg_reg[2] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(gtwizard_inst_n_7),
        .Q(p_0_in),
        .R(1'b0));
  FDRE \rxchariscomma_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxchariscomma_reg__0[0]),
        .Q(rxchariscomma_double[0]),
        .R(SR));
  FDRE \rxchariscomma_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxchariscomma_reg__0[1]),
        .Q(rxchariscomma_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxchariscomma_i_1
       (.I0(rxchariscomma_double[1]),
        .I1(toggle_rx),
        .I2(rxchariscomma_double[0]),
        .O(rxchariscomma_i_1_n_0));
  FDRE rxchariscomma_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxchariscomma_i_1_n_0),
        .Q(rxchariscomma),
        .R(SR));
  FDRE \rxchariscomma_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxchariscomma_int[0]),
        .Q(rxchariscomma_reg__0[0]),
        .R(1'b0));
  FDRE \rxchariscomma_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxchariscomma_int[1]),
        .Q(rxchariscomma_reg__0[1]),
        .R(1'b0));
  FDRE \rxcharisk_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxcharisk_reg__0[0]),
        .Q(rxcharisk_double[0]),
        .R(SR));
  FDRE \rxcharisk_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxcharisk_reg__0[1]),
        .Q(rxcharisk_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxcharisk_i_1
       (.I0(rxcharisk_double[1]),
        .I1(toggle_rx),
        .I2(rxcharisk_double[0]),
        .O(rxcharisk_i_1_n_0));
  FDRE rxcharisk_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxcharisk_i_1_n_0),
        .Q(rxcharisk),
        .R(SR));
  FDRE \rxcharisk_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxcharisk_int[0]),
        .Q(rxcharisk_reg__0[0]),
        .R(1'b0));
  FDRE \rxcharisk_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxcharisk_int[1]),
        .Q(rxcharisk_reg__0[1]),
        .R(1'b0));
  FDRE \rxclkcorcnt_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxclkcorcnt_reg[0]),
        .Q(rxclkcorcnt_double[0]),
        .R(SR));
  FDRE \rxclkcorcnt_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxclkcorcnt_reg[1]),
        .Q(rxclkcorcnt_double[1]),
        .R(SR));
  FDRE \rxclkcorcnt_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxclkcorcnt_double[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE \rxclkcorcnt_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxclkcorcnt_double[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE \rxclkcorcnt_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxclkcorcnt_int[0]),
        .Q(rxclkcorcnt_reg[0]),
        .R(1'b0));
  FDRE \rxclkcorcnt_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxclkcorcnt_int[1]),
        .Q(rxclkcorcnt_reg[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[0]_i_1 
       (.I0(rxdata_double[8]),
        .I1(toggle_rx),
        .I2(rxdata_double[0]),
        .O(\rxdata[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[1]_i_1 
       (.I0(rxdata_double[9]),
        .I1(toggle_rx),
        .I2(rxdata_double[1]),
        .O(\rxdata[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[2]_i_1 
       (.I0(rxdata_double[10]),
        .I1(toggle_rx),
        .I2(rxdata_double[2]),
        .O(\rxdata[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[3]_i_1 
       (.I0(rxdata_double[11]),
        .I1(toggle_rx),
        .I2(rxdata_double[3]),
        .O(\rxdata[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[4]_i_1 
       (.I0(rxdata_double[12]),
        .I1(toggle_rx),
        .I2(rxdata_double[4]),
        .O(\rxdata[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[5]_i_1 
       (.I0(rxdata_double[13]),
        .I1(toggle_rx),
        .I2(rxdata_double[5]),
        .O(\rxdata[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[6]_i_1 
       (.I0(rxdata_double[14]),
        .I1(toggle_rx),
        .I2(rxdata_double[6]),
        .O(\rxdata[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[7]_i_1 
       (.I0(rxdata_double[15]),
        .I1(toggle_rx),
        .I2(rxdata_double[7]),
        .O(\rxdata[7]_i_1_n_0 ));
  FDRE \rxdata_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[0]),
        .Q(rxdata_double[0]),
        .R(SR));
  FDRE \rxdata_double_reg[10] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[10]),
        .Q(rxdata_double[10]),
        .R(SR));
  FDRE \rxdata_double_reg[11] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[11]),
        .Q(rxdata_double[11]),
        .R(SR));
  FDRE \rxdata_double_reg[12] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[12]),
        .Q(rxdata_double[12]),
        .R(SR));
  FDRE \rxdata_double_reg[13] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[13]),
        .Q(rxdata_double[13]),
        .R(SR));
  FDRE \rxdata_double_reg[14] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[14]),
        .Q(rxdata_double[14]),
        .R(SR));
  FDRE \rxdata_double_reg[15] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[15]),
        .Q(rxdata_double[15]),
        .R(SR));
  FDRE \rxdata_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[1]),
        .Q(rxdata_double[1]),
        .R(SR));
  FDRE \rxdata_double_reg[2] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[2]),
        .Q(rxdata_double[2]),
        .R(SR));
  FDRE \rxdata_double_reg[3] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[3]),
        .Q(rxdata_double[3]),
        .R(SR));
  FDRE \rxdata_double_reg[4] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[4]),
        .Q(rxdata_double[4]),
        .R(SR));
  FDRE \rxdata_double_reg[5] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[5]),
        .Q(rxdata_double[5]),
        .R(SR));
  FDRE \rxdata_double_reg[6] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[6]),
        .Q(rxdata_double[6]),
        .R(SR));
  FDRE \rxdata_double_reg[7] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[7]),
        .Q(rxdata_double[7]),
        .R(SR));
  FDRE \rxdata_double_reg[8] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[8]),
        .Q(rxdata_double[8]),
        .R(SR));
  FDRE \rxdata_double_reg[9] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[9]),
        .Q(rxdata_double[9]),
        .R(SR));
  FDRE \rxdata_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[0]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [0]),
        .R(SR));
  FDRE \rxdata_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[1]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [1]),
        .R(SR));
  FDRE \rxdata_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[2]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [2]),
        .R(SR));
  FDRE \rxdata_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[3]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [3]),
        .R(SR));
  FDRE \rxdata_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[4]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [4]),
        .R(SR));
  FDRE \rxdata_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[5]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [5]),
        .R(SR));
  FDRE \rxdata_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[6]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [6]),
        .R(SR));
  FDRE \rxdata_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[7]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [7]),
        .R(SR));
  FDRE \rxdata_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[0]),
        .Q(rxdata_reg[0]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[10] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[10]),
        .Q(rxdata_reg[10]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[11] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[11]),
        .Q(rxdata_reg[11]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[12] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[12]),
        .Q(rxdata_reg[12]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[13] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[13]),
        .Q(rxdata_reg[13]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[14] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[14]),
        .Q(rxdata_reg[14]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[15] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[15]),
        .Q(rxdata_reg[15]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[1]),
        .Q(rxdata_reg[1]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[2] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[2]),
        .Q(rxdata_reg[2]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[3] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[3]),
        .Q(rxdata_reg[3]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[4] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[4]),
        .Q(rxdata_reg[4]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[5] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[5]),
        .Q(rxdata_reg[5]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[6] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[6]),
        .Q(rxdata_reg[6]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[7] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[7]),
        .Q(rxdata_reg[7]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[8] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[8]),
        .Q(rxdata_reg[8]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[9] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[9]),
        .Q(rxdata_reg[9]),
        .R(1'b0));
  FDRE \rxdisperr_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdisperr_reg__0[0]),
        .Q(rxdisperr_double[0]),
        .R(SR));
  FDRE \rxdisperr_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdisperr_reg__0[1]),
        .Q(rxdisperr_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxdisperr_i_1
       (.I0(rxdisperr_double[1]),
        .I1(toggle_rx),
        .I2(rxdisperr_double[0]),
        .O(rxdisperr_i_1_n_0));
  FDRE rxdisperr_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxdisperr_i_1_n_0),
        .Q(rxdisperr),
        .R(SR));
  FDRE \rxdisperr_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdisperr_int[0]),
        .Q(rxdisperr_reg__0[0]),
        .R(1'b0));
  FDRE \rxdisperr_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdisperr_int[1]),
        .Q(rxdisperr_reg__0[1]),
        .R(1'b0));
  FDRE \rxnotintable_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxnotintable_reg__0[0]),
        .Q(rxnotintable_double[0]),
        .R(SR));
  FDRE \rxnotintable_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxnotintable_reg__0[1]),
        .Q(rxnotintable_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxnotintable_i_1
       (.I0(rxnotintable_double[1]),
        .I1(toggle_rx),
        .I2(rxnotintable_double[0]),
        .O(rxnotintable_i_1_n_0));
  FDRE rxnotintable_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxnotintable_i_1_n_0),
        .Q(rxnotintable),
        .R(SR));
  FDRE \rxnotintable_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxnotintable_int[0]),
        .Q(rxnotintable_reg__0[0]),
        .R(1'b0));
  FDRE \rxnotintable_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxnotintable_int[1]),
        .Q(rxnotintable_reg__0[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_double_reg
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxpowerdown_reg__0),
        .Q(rxpowerdown_double),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_reg
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxpowerdown_double),
        .Q(rxpowerdown),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(powerdown),
        .Q(rxpowerdown_reg__0),
        .R(SR));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_3 sync_block_data_valid
       (.data_out(data_valid_reg2),
        .independent_clock_bufg(independent_clock_bufg),
        .status_vector(status_vector));
  LUT1 #(
    .INIT(2'h1)) 
    toggle_i_1
       (.I0(toggle),
        .O(toggle_i_1_n_0));
  FDRE toggle_reg
       (.C(CLK),
        .CE(1'b1),
        .D(toggle_i_1_n_0),
        .Q(toggle),
        .R(reset_sync5));
  LUT1 #(
    .INIT(2'h1)) 
    toggle_rx_i_1
       (.I0(toggle_rx),
        .O(toggle_rx_i_1_n_0));
  FDRE toggle_rx_reg
       (.C(CLK),
        .CE(1'b1),
        .D(toggle_rx_i_1_n_0),
        .Q(toggle_rx),
        .R(SR));
  FDRE txbuferr_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txbufstatus_reg),
        .Q(txbuferr),
        .R(1'b0));
  FDRE \txbufstatus_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(gtwizard_inst_n_6),
        .Q(txbufstatus_reg),
        .R(1'b0));
  FDRE \txchardispmode_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txchardispmode_reg),
        .Q(txchardispmode_double[0]),
        .R(reset_sync5));
  FDRE \txchardispmode_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(D),
        .Q(txchardispmode_double[1]),
        .R(reset_sync5));
  FDRE \txchardispmode_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispmode_double[0]),
        .Q(txchardispmode_int[0]),
        .R(1'b0));
  FDRE \txchardispmode_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispmode_double[1]),
        .Q(txchardispmode_int[1]),
        .R(1'b0));
  FDRE txchardispmode_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(D),
        .Q(txchardispmode_reg),
        .R(reset_sync5));
  FDRE \txchardispval_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txchardispval_reg),
        .Q(txchardispval_double[0]),
        .R(reset_sync5));
  FDRE \txchardispval_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txchardispval_reg_reg_0),
        .Q(txchardispval_double[1]),
        .R(reset_sync5));
  FDRE \txchardispval_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispval_double[0]),
        .Q(txchardispval_int[0]),
        .R(1'b0));
  FDRE \txchardispval_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispval_double[1]),
        .Q(txchardispval_int[1]),
        .R(1'b0));
  FDRE txchardispval_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txchardispval_reg_reg_0),
        .Q(txchardispval_reg),
        .R(reset_sync5));
  FDRE \txcharisk_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txcharisk_reg),
        .Q(txcharisk_double[0]),
        .R(reset_sync5));
  FDRE \txcharisk_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txcharisk_reg_reg_0),
        .Q(txcharisk_double[1]),
        .R(reset_sync5));
  FDRE \txcharisk_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txcharisk_double[0]),
        .Q(txcharisk_int[0]),
        .R(1'b0));
  FDRE \txcharisk_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txcharisk_double[1]),
        .Q(txcharisk_int[1]),
        .R(1'b0));
  FDRE txcharisk_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txcharisk_reg_reg_0),
        .Q(txcharisk_reg),
        .R(reset_sync5));
  FDRE \txdata_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[0]),
        .Q(txdata_double[0]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[10] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [2]),
        .Q(txdata_double[10]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[11] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [3]),
        .Q(txdata_double[11]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[12] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [4]),
        .Q(txdata_double[12]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[13] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [5]),
        .Q(txdata_double[13]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[14] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [6]),
        .Q(txdata_double[14]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[15] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [7]),
        .Q(txdata_double[15]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[1]),
        .Q(txdata_double[1]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[2] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[2]),
        .Q(txdata_double[2]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[3] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[3]),
        .Q(txdata_double[3]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[4] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[4]),
        .Q(txdata_double[4]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[5] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[5]),
        .Q(txdata_double[5]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[6] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[6]),
        .Q(txdata_double[6]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[7] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[7]),
        .Q(txdata_double[7]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[8] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [0]),
        .Q(txdata_double[8]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[9] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [1]),
        .Q(txdata_double[9]),
        .R(reset_sync5));
  FDRE \txdata_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[0]),
        .Q(txdata_int[0]),
        .R(1'b0));
  FDRE \txdata_int_reg[10] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[10]),
        .Q(txdata_int[10]),
        .R(1'b0));
  FDRE \txdata_int_reg[11] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[11]),
        .Q(txdata_int[11]),
        .R(1'b0));
  FDRE \txdata_int_reg[12] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[12]),
        .Q(txdata_int[12]),
        .R(1'b0));
  FDRE \txdata_int_reg[13] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[13]),
        .Q(txdata_int[13]),
        .R(1'b0));
  FDRE \txdata_int_reg[14] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[14]),
        .Q(txdata_int[14]),
        .R(1'b0));
  FDRE \txdata_int_reg[15] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[15]),
        .Q(txdata_int[15]),
        .R(1'b0));
  FDRE \txdata_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[1]),
        .Q(txdata_int[1]),
        .R(1'b0));
  FDRE \txdata_int_reg[2] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[2]),
        .Q(txdata_int[2]),
        .R(1'b0));
  FDRE \txdata_int_reg[3] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[3]),
        .Q(txdata_int[3]),
        .R(1'b0));
  FDRE \txdata_int_reg[4] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[4]),
        .Q(txdata_int[4]),
        .R(1'b0));
  FDRE \txdata_int_reg[5] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[5]),
        .Q(txdata_int[5]),
        .R(1'b0));
  FDRE \txdata_int_reg[6] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[6]),
        .Q(txdata_int[6]),
        .R(1'b0));
  FDRE \txdata_int_reg[7] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[7]),
        .Q(txdata_int[7]),
        .R(1'b0));
  FDRE \txdata_int_reg[8] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[8]),
        .Q(txdata_int[8]),
        .R(1'b0));
  FDRE \txdata_int_reg[9] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[9]),
        .Q(txdata_int[9]),
        .R(1'b0));
  FDRE \txdata_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [0]),
        .Q(txdata_reg[0]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [1]),
        .Q(txdata_reg[1]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [2]),
        .Q(txdata_reg[2]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [3]),
        .Q(txdata_reg[3]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [4]),
        .Q(txdata_reg[4]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [5]),
        .Q(txdata_reg[5]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [6]),
        .Q(txdata_reg[6]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [7]),
        .Q(txdata_reg[7]),
        .R(reset_sync5));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_double_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txpowerdown_reg__0),
        .Q(txpowerdown_double),
        .R(reset_sync5));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_reg
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txpowerdown_double),
        .Q(txpowerdown),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(powerdown),
        .Q(txpowerdown_reg__0),
        .R(reset_sync5));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2023.2"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
TCSZ2Ye5IRfHavlF8Mno1PI9xixWuSiNh3ssU1FQtkjW1fmNtc2c3x12slL242UQayI0rzZTqe6S
edtecLHTOnzxXpCZjjU8NFmgLPerTSDZ1W5YhyIi9j0Ap4YBpvaA1ojM0+r0Cx+dMOXohQGeyljq
+fnTaFTUe2678DxpqHk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
NIzZVKMB1/8UX2qb4BB+VXCafEKXsrn7axB1cJDPqDCzSyt/2KG1NEEZTDHZVzIr8Bf9501PyXmL
VowTAAXX/RopKyKOM1xJN/qLtqXxegH2a4dIkUxDIIclIcbv/smna9VCwI7m6JhrnKsNciTTilgR
27S/h6JPpZsZAEmsNxxTC70WQhQSM8TlHJjZg3KDc5KTnvC/mVTk6I05U6x0Bdd1YR9GBvhwRqhP
B1ukL/1JVOwR9Ce9p+EHFE/xyApypCjQPGwq+8IFQgS8wltVZHX6eSMw17Q0wGCY+LHduRTA+abV
LvAR0NPf7PKQUSCECe2mBbLPO7wD4BO5RAkJeA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
udw4XxxzcaDNM1qWnRgZ2JEM1MMqnKwKVqha/krU9EyUAsyATjQEMBqjlOHw5QXMU2jjizlL20Nl
h2pF7iKo1S+7TS54Y/UIJANp+Dl46V/qfy6/yBnE4YclHON1k0jRao4C6T951tgXuCAIQEmXbr87
aJfL2dNqORH+TDKUBdc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
JFe89rPDaiIUicPDNoXCg8pJUoYRyVDoW/5yE4T0Cp9xDHtuIyKQVbC7jVb92OsgJ5GHDm7DH2D2
rYZKrdCIqPt2jo7DG6bcJuDFcisZb11HLlYWNsK2Vqs9DdsTPViykeE05CD5AgfDxb983x8F1meK
w8zjeGoD44djsaRA+lvP1zLhl24q5LWFJdPSyIT7uWZwhxHqlyJu85ToXLuwZQZO76Mp+1mitxDy
vleizC5rnk/4hqxfEFS21Qi1TwCz5hdU+H3nA3dTe1KRY+obbFP7sRWKfmr9Rcf9enRvbaEbLoJA
9ADkl72jc1Aqlnd+YCGq4EmbElbWLxblpamncA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IV5qOrW4jXfiGt6hz/YcNm+/H3ij0v503CF3Kvut91tUWldzNzyLt4pIZEWEzSmn6RcpcLNN88po
1kt45UdSBz+mL5HDQaw4J+VGD/cCBmW1jnOclCf82kwju1MIDfa2EKicjqaykCUROxV7cwg07FFp
clLfIwd4kxgSWnGzeZi1IGezx7OpBsAkBTz9ha4WttEm0+D29DF9O4GaQl6q8IBeA0QIrO10EESt
slfRi2evxdOeTZBVFoXU91OszneH/prZqyCsHeyvTa8PABTZ+Y4CH6ICZCXRn7QTNJgoYSGABuPs
87saNJgzomjyaO6IzGl1fBgMIsIurKw90DE8Jw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2022_10", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Eek/EeBa5kEiakvxzHC3fZ0NXCsvWnLN8FYKLyImepfBUv2jdVDY2j6Qs928DJaMO8pBuO9SGcu3
47rhcN1DAjZza5Ac83W24fngY1+YWblivVc8AoggXS1t2Y7Dy1vf9+ZxUdOvq63sje+fDJxapZwK
3HQGdtBX86RTaUS5K+HyI1FTmcIhUYmJWmxQjIxLla7FF1QZ4XpTCfqAG5i7ZKlYSoDFb8sjCRG4
XWFuk1dbL2UfZPxXZ7XHIm+03Ck/JsHtsjLCc8oTB/9MLom2HX9SjX8H6tFbEXR1NatCFWQ04JKL
kHSYD/xDlwjhN9CRvowRhNJaYSmKQT646hlNoA==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
gaID+cgqOdyoJPDKM9JAyimEjoxNakxuNjYf52N14HEyn4NQksF7thq/bXWc82vmfdc8aodx1+ky
i8uuKszW1WwV+apGSqk7YXBCxx3ACfMsPzNzeDQ2HVzGfznpQD80Eu7I7iwtz3k5Mr31iaeM1kQa
oddk6CkVESI8CD21PQHMVeu0LKLZJp8k8NHf3i0UOXsP5o768iecieYQh2VYXZ6HORDDyd+IpDB1
CAFBZctXco8C1w74wCB0LXUSYInc5ythxBURkPPTJ1GBuXpoQGZD2sNiI2Htl0y1toEdfgExWZ+0
+4Docnd9TgOGhAhZzUcj3c+6cQNbgCB847/G6w==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
tEBTlBoXowC0cOrrCu9pEZ6t9FjMS6GWThbzsXMvR2xb9HBcccdvXT7EfASM09KkNzvlYoyNBWct
0TRl1BNzzlomu3G857U6kezS+CCRF/K2qOhhxHFxEfuM0qblRVdNHoCGGMM4PkE/rt9M7IqYoXQg
WOHI1ydpZZn08aVL9QYJgz6ZuVHNLwSpL6rjFFDXV1cB82gVFBkRP/0NxpGW2WH6YA/MJ0czV0ji
o0umOWluEwUObdytKX1lfuNYimI0ziWrovqq2osL8J7NBKDUl2R6gJ51DObsTBgC8uyUHVibyNHy
nhzTpwcBeeXdtAueCg1BlHDIwglcMUdy0sBZEyHM/CLzpxgr1A+uUcmzlWx1drrc8lRNwGMFDDJQ
9OzoHBABtNt8N3bbO8A+rE9HtsjMVr1TxHhUTxBhWcypwra+xzsGykln/IP3JBwwQR0+d+V8/Vec
5Bh03crJTvJZUbYidozNoaPOfnHi0NxFDNdL7L1i75T+H6bqeE1ADR/4

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GM0AtcDH85MjCjnA/pijf0ZCJap8X+OYUX7W1StOwznqG2XPd9DhtvTyqD0c8/7BTdeCzGUK5iqe
QiGwEcy1dCrSVZW0KtjFXllkYV2ai3/Qn7Bgg1YuzxifEFKe6ClTsByfgjqRdyZeNSAldwvx9ZtT
0ZhijV96K37zXwfXFeKDmxOZOV553ovWfXGekaS1EPmSluoDYBMQKc2XV+ZUXR7n5NI/6E3QdK7K
utsZyrFYyJdYW8Po28hQf1nWeQP6+PxQB6wi/P6sUzudntNcQ7uLRr4PTz6twPPqYwUF+7YW8baL
p/2EFPf8y6fBb+DOBCnzmGZvmq+M2qQot14r+g==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Ot2lizvbpt8emXxoZl10oi4H/aTQTzrHDg0mf7DDs3BS7iyFsRmaQCG/BRF/mwjlw+EbST4/x/DJ
d6Hf6LIa6mllnMD6G+uVHQ1z31eASHPw3m+WnPMr/zCNuebPcDitgiXWmq3SAS9byYvxiwcDvjn6
CMh89pvlx8xLiFUoo5j/lAPe4cPBJwSMleQLLB989s6rByi0lVW0QiLTzakaB8DHBMvhIYEfi74m
Lxby6+nYRGrAUKPOemP0Ag/LW83Eup/Wa0jVOtxzlj3foiYhg2mWCt2zyFhgQsDA+oEsDa/KZc0F
OUzOI8vFDrwPmYRwd2ejFI9Nz3/1mb05VQmDRA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
OamBwohUIPOIa5bsJvu3upqnGl4f3jNYS35bg4S059C9qVDgQJcw/it81deNA5zFvzX3Cq9CiO9K
zl02VsdpFgNPjSwEO8F47LJZ4fHx99EmESBogsNwUNitzkuYTLCb7F7ZF2WSJExQ0KsYt+TRp2UH
yQEvpM1lHQYUXxzjw08qUI5ssSnOsQFydvP8BwA/6aGrVJ+LuEgPVdMqLBn0EeAmRsynxJ7OhPGV
DlvHQwtVuBrkvjQHED/Ye43ZIeWPm/xOcjNfZjYeOvdEJqTbaviR0Fo1LFx8EX80uvdFeK55ywDN
wzoZ4cVH87f6VwR1xHdo0JpVxrajZpSZ0jPudw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 153808)
`pragma protect data_block
XGxIRhRYvMxJ2ch9JOi5bAiiQbN9qnWRxIfAaOW3nBztMJ5TTAmKanx+2u6UwZMujtXOc8MLG4X9
pC2B9lTAy2Mu1DwTSLSRIjIwyM3YowdNFeoP1A/MuUCKidZHvfS2+PN6Cr9+J2rC8RCszW/foQRu
zptbyJnoZdJc0l/h+WUh5TQ1TmTqHMlCipqqTV6m++RsYjCCulBecIivrbfkKxCiLRRDzsAZ82qy
LGKDW9Vj0TBYs7UlC3Ukzyr8UFJMFgPbioevYNZLdNl4pgi1TKz3TOg3LULasWfa17tE+J+dgf+f
LMBH1xWHozOri7fUEGcsM3TNsew4asPHJLrpLd33Klq3A1k5Fj3ZXTcw1L2xdS395ZEVENNi2Gqy
jqKxIyt/P32u8A3vF6MdUWBx5h50V8JB6GfcaNjcqpvFoM8nS7wr/3yraDaqMqhV1HOxFAb3XzrK
KTlAf1GSsGz7h3goWPBfW071dx3e2avPVZKue/4VO+lq06xDI2HmL/kDtBquLs63ZdNIF7VQIB5r
1B8HmrH7uuj6cY+uagBTqGjtPXZ8QmvwGttqXQAn6GlnmPE0kEFnrfNTDRGzYEgDSWO+H9+FQXcY
1x0nmiKDMYIUXDxFhAVEKHx010Jd1S3UpMBoNkM8ZR6AOT7G4rzYmbWwPqEunojT0ff1qxI2vfyO
xF7V0eO5nZ79gJBFpFz27Xg7CBVz/7vpW5MVWmCC0i58a/RQuKYQRjeAWovUdVzYuSxu+tMZ/kGX
eAadSwQBUYCfQTgxNEOxCBAmxq8N+BdHsBWqCeLL4PMc4BIhiqKxRN51oQj+xoFpkzDY0a48qx/p
/exz6le7rK9e8bM54Az78+pe87W4IWNgCxIz+t4K2+66zCwvK2lvyNbCSzhIML+sp0iBQF62o76I
1vPCXTMSIRsHHCSoSSm0j9N0USuuBb9uR/TSw9azgeOUJhr1PxLVl8EVOdBLlV2Ltqm9yfCe5i1I
zY83Iry/6uJT97MSbXS8vW8rb5SkkQtjCn4if9ty8cA6b77llBrG6n35AQdbvSdQ8Kf6du8tU02t
xbWTX1bOLXYHpW7X3GMK6FNOZ2yrbQzh5HZ16W+koI2/TMX48yjpRx1zANnd2B3Qcw2Kcqo6fcvX
mVQcsUfs/O5tNZxaiU7XmvjT8xGFIFz2xIGUg2cvhH5pEN5qVpHjRe/Fu8igzzkL2bylI4KLCIhD
4uBZl8xL5yNLWkpZEo9uz7bpRqsr6t509UuLvYR8Qf6DJJ5AFown53WbMlWun8LSaq1cefa7RfwX
U81ePyT0UmpoEgov5OIT3+lb4Fn67FR+XCKmWkg4MJXy+fwFrKOMMNvplWHObFdOSsbAE3S1Z/l4
22EFWuSDfrVWpQTzaOxvBf5FHYjbLHPLeaXb7yfNJPM2euwPARIGE8lGtHaV9K+vQetcc0F7UKCz
cm91xm1sLYE7Tkp/8J2+0/O7sEba/i5KM9834MDxjy8p20DR6npf5ok1C9T5gpRgDbg0RfvrP1/r
Ooksbx5n12I5vR/KhkoZmFVDNuIsUaQSEe9PFAz7QV/STZpmw1dpCMWa5XmFlIXcNq8S2f3cEeQC
C09vKL+SmI4OvgjjTa0qn/X4gKPzk/32tvbs6DhlS6LSbafkvCpAqQZploObMiWSCai/yR2wf7zj
XYwMwhiXg+ATZYdP+I5zEWEchbFHcSLS4JSXndo1gXBEwnpErZjaE2Mi77B/s7CB0qsyQJUAOtvF
DOiUcvtdsTjnCRgMec02zYwEjp2U3r10WjYOWeJ3BktVLkXP60qubEYNH7+dJ/V1P7OHkgDOMDwR
TqUEIT9x4ULSzfZUqmkvf0m56ofIjqyPrm3b/R/LzmAcZh8v5sMEAom4xw2Gur4czB6AwyuQNRFi
LZAAMeYWvs3qPTd1X3/z6P2tA/0MIKKsd/FPTVDi2X3/bRP1pI86ogpxGQcns6Q71VNvxLaqilox
GK/XG7DFT9ncFhR/60DL2HRMtjIJOyrmPwQaAD5L5dxnlgNMrYNzgnlMaxawEc6ImtkAmL0K/a6f
6cWloiBljJTmtcSxgw88s4RcxSE9QhcDLcK0AGVpXyrR3CHceuEGbhu3JgNUES5OxCDCPOQAY1Vq
Ejo/6lgE1k1UB/mzjbKML0uIhhx0wx94VPwtbhmrPXcinLIoL9orm9xwi3s8/4TiQVBDW0eKt0lz
kxChkDk4osXSkeiFwC5PKDXPSPS2GdHCpkCeY2W4W7mS+7i1B09QQWVC6KWKlbGNM+djCC0hFT7x
9jSu9SUqm5SQwr44DlXJwzZ+/a+0shorHACgw6UevZiAf8xvsIQJhRrsHCKExp7l/7GrIB2Y6/TU
/jX5y/KkiupsS3EBkchgxTj6HCM/SBeQLS6TAD7lVqnCH0cv4wDa3ixpjiibquAGXakEggRIBIJ+
TwhX1AXniNFkAo1/gY4jX+zpa3zGFTd+aZwvneXRUl/x4pTBlkf4wRvXS6/A0nkETh+Z30JNyUwU
u9YO4XNoeORZPMSZvtC3x/bhHH4zwWN2aLjCuIdhQWzWR65HCb1bQjohHC7HJyrNPnVNMpDZZJW2
48aA7O9N5JgrQLGbOoo+2c/ddJ+kDMufacFva6et+g66iZ6mXERdSrUp/GmLtXMcyMVo3SZeg3VL
pAluIaOS7OfppKyyQ3A5TlnM5iTVN9V5iCabmyQJ7cls/l/h0UAhNO4/bRyLr3b6ZyFpbGkoMsyD
ZunNtMjbdJeyidGtFat3LYTNfu2ZvhZUohT9B+PYDe8ZqrTmQSXTszY8mdiVLQwOJu1Q9PY6vPox
kf6Mn0sYtIqJREVERrkUHzNJW6uhsg+SWCdIqyZl0R6BEl/fJWUn7oWXVSlfeN0zdZ5WWP9Weaep
sgol1LCWlI3CQrcrALDKiUs/48pbpn0njHlm6Dm/v9QoxkXHzs4GU2awO33nmNzW+IHLNCyDCjfc
pOrdaKxWYpHTQwHGqkDZnHGsVZXJ1k7IMMEvZxtPYhwheJVP/hoxUWBvxzz+2fWgarBZYl4kKM6R
8+PzGW3/scPKlqg4qYOd9faUgTGAzFyM8YgGPtlzXvbQKsH/nm//13U/VBkbufEZSYXePD8Xtruy
zhI5nFJImzloHbsCNJ/wp0EGiwKJ7xZXsZax/FU2CkVuLtSvJLOWKNd4wx1Gpc4/LhGw64YmpY2U
78+uXFCXS0jkfVqfC1jv2mHkWQtJfMq4vc1geycCJboZXDIi2W0IVvWvMPpgUldw0TMBDUE7nDTx
7H9NI8sv9/TuA/V7RrkctpW1wfk7AK3vfw6ihEJdcRX7FtGYJqmhICIN8C6zVlVpni67Xu5CE2r8
u1ZNOIJackV/sywRugOrRALWOFY/efNKbcav96jJjdmVOQ/DfvEZMY+dd+6uzRmSi5JIpJNlgnkM
dhUf9ks0ntai7x/ZblT4US2wGB8VnWP8x11sqCWYsK9mp8K9J0YKCNcKM5d/gm3nbXhIglz4adgA
sTh3MwVV47gNX0nZiRl5iEbNEGYgU0Pn3harR9R/K55bUpwLIPu/f1+gA5Ep4VVUz50FByxlfGMp
xDXY4nfAq9rwIFImK+C8nbAdJuh/V/8/5hnXhdjJIuuiZVkWCRw+LLyLDK6hUe82lhdZrYyfqSWX
5Z22a7xUwrd9Wn99F6Y01lCWKsa/skgygWDH5VTGAYqdM0kBAzAn7Ba5tgziBL6rZc3zE4k1yjb7
C8YICItt/aeGzCP5767hu1dWMa9RjV2i6SVWNDphuEomljz+zNOhni5Uz+WcVTMhhTmm0FgSxxqP
nbWRQqehGiM9E1NJa9CuiSDXNFoXJNz5WiwWbb2KIouoLiLyZgUbNBERk19I47E3r+mvrT5UHGG3
7vZFUn+RGHY6/2wm2MvbjZ/jh3YPTn7vzmwvIzjFKRcx+FADIi1AzCWFw82NVs9NjNKw+YpaIi0A
tHbYWVJnMPYbeHSyYq8P4GJT9UZbUHg++Vm54SY1MSuRl8HlHZMOyobavFvgN7Kru+zSYOAA6lHj
ldjE4nLp9hlkbG+1W7PlkWcPjNvOZ3JrlnZ498mL+4dbWV2xHhs0AfZ+hrS9rTbWXFnzDw0ncRxf
b9brLs0lR9It9qKXub6ZZln3okxiNWxXgxCRjsd+JNLeWKNi3kun6PAZA7oFfUUxkbHdIo6ao6mB
nrZXD9G12R3oITcE3FPypvbzI8GGb+/rNMJVDfnwikjgX6OecIYIp+G9A/JqHZK4Jek12lhM2KSX
C1TIOgv/F1PqYCw8ajdO9qz0Vce07TsMrcwEvtEIrLPBugwC9FzhN/Xp/PYXJ7VOhXnPOm4WS3i1
Xvk71uQQeR1DL/csRjpKOx4FxJ4tQ+38gQq1y2b2MKRBp9+56Up9C+HdO77f8k9R42a6IfUHi9dw
5DI7kitQev4HD7mqKvvX5vs+Qu+R4Y8XnGYtohmPhJhyPz8Atvt7KRFp7y2qxHh8XdxTB/4MCbGZ
PfMwT7xJ0XAnt1NEobORBn4mHy1mhvGuTa7rUTf4l3pRNtiXsxJCJqgOpHD7iM5/XThHWZ72qmjA
xNNfsQw3U+Ar2tpm6VTWzinyaCztL8hGnRzDGSX/fqCympVsK/wYTZ4N7T13rVS4fhI0jsGhItQK
lDabQR6wAyt/TZnsFwTlqX+C8ovk5XZXGJ6yTUzc9FcMifV3TXPeiYgtgnSzv30PuJ/15+ydeSX2
H+Z/exoGLG/XwjB7xqOQRivIih7iifm4+ids5PQiuBy74bd9lg/4goTxHJDtfr382htPr6ScHgIx
YeKrG+lVVvyYTWXWXTBDVAaokw6Tv80WH7hYadHU9o7n/5wmBfGddj1sEjI9QTpXhOqRasNxAIXL
WKh+Mb/W/Yt6grU2CveJkeifMGt7tW7M9vnUqDx6N6ZbqkWKlM6XEUUIgHry8DUOy7yLgBeFMID/
M5ylHJusUPPRQjqok9KZAuFGiYIgv6lGhluwwegmzNt9WR/LiRRemZpOxXq2EZRUSllfFHJt4sSa
Sdc2OZgNXX9bE6ETILwL8IiMjHHqYNN1UxAg//gqDhLVI4+GSy56po1RcZFW1ytUlmEvuw7HSYQY
utHtZ13rfZ1lBdunKyxnI9CywWS+w6sZDRKyHuAMoiQmz4z7QgVtxIBA86UVAGImvIORtLSULu4e
iVh1KCSjeKYYCY4pv4VdxKJthl/KUfuA4CquvQBH/AV1cZnhVdvGTcflB4aO5YuG0ycvKMUywSEi
QP1dTKf77wVDWyg2hjXGyD6BVtuI/q9WvcKy8sXnjaGYKxKBR6PL1H+0q1gljAn1J7KpgNOOsG4p
afOQfG+yaLKildlLJqVDHJIC3cAXJlMUzCj0TMyZrC7Evk0SXvFpNx7zDkvyvSqfyK3wv1EXq2e/
GuaMNnKXETM5dZGM+dYvA2PoLvGBdjtWQ87XlEEDWEYOs0JjRvDrFHoc+1geMR5agJX6JVtbntJO
5Gi7AZa/ynwbt0gCAhy6paDFmbRzMDYxyWwhfMUO3O6x6ELzM4aehZkywJFHGN9jx6DTHXIpBBE1
apLM8GAxdICrUpCV7E5aOuNS/mBWaUfv9Fjz2T+GcyNSXi75hHl3r8BWuEqBBTTgkIs6FX6F8Oql
W63ARfoZlVPq4cN0Y2brTpXId3Ohx28pmIxjv0YFs887bYtNl6NdJaXStwnG3ktq+cfIv4O4oRMg
6ycCe8R08PbUgjD1eIqDheXo8aQMw4TgKO0SvGAsYQRh2T1XVlErxGQ8bFLGKNyGhPgBsaKoyOeP
PsuOIufzSOhD1No5q34vYzKeA8dDcUXWcLcGI9cbDNkdTcHMV+vCh8Ny7OR9HlCYgXMfhuGPbin5
t0xblvioooUCCcAQCP9rgGN+Mfj2idxof+7w7OGTqzrMAzyofG9NOEhxXlgV2yIuzh68VZR8QXzb
cFS95ug8bbKpAod/a4vHHMJ/LCibLFhM7ybjLaB9zFSlSFoyd+9KXaIxpuQmA6WDu9LDItiY4Fpc
YIWL+kGRnw0w2NI2vdZ1Mn08oneupwEoi8JWLeNS7zokHwYw9VM16pt8MDNq3xjQ2QyW+97rLUO4
PS3EpKLdQYoAM5sO7vyw6BZ+gjHYbs1hU08dOf7QxaONkuQZwbPqe46Zs2t+0KZ4VaXFa4LQ0dhI
S6FNWGORcWWvJn2QE5zp9syNUnBQniMhgbA8iXOXDFN0ERwMGa8lr/SOgUH3GGW6+iip1MFoVrRi
vhOITSO0WD5A5e2qdTCWwqdWvghZAlV/Kj25XhICdgzpV9B39LUbIevdDBrNCcvcR5UhPFYRP0WU
95xnUziQoTBoqvThro1+WKy5NyDFDCJynaFdYjk6vmaPiim6+yt3e+ZrbY40EUEFNsHXxbLYaJ1I
DnxhyM0w8Zjpvid8y7D/WhjtGFHcWEyIdN09+S81L9YPyndQtQSkAgUlis+ej5jWVSwuSZnvIwNF
PikDJA9ljDlEiOzj+nPsJYkFXhqhZLMWbZHH6/v92r5CqTI2WYr212z7ANX0LJpBv0dDlfOv8ONY
5ZVbHgBOAHAAS+b/Hetii7CJLK/nz2PV86vm71tiIGXj3tEDhj+WWOjvfCWGrOOb3gyFK6FhiWy7
SvSWEfeK/uEHdB/OQ/DrBOR+h8jxkqUREqOzQjvLXVtuqSboJisElAbTcjPJ/R6F5IX4s4eeIWLc
Ow1s8a2IvTDVwOvttmjsxwCzUCLcLW8vSxHdLwqrcF5YoRT/UXUZEzGsOTgYlu/1zoeg3oUvl1Zx
6EOqJA1qlnkc7M0sXL2UDspScK7qkWUcZxnVYF6SMitMCCcdV9l58lhek6t98ybnJcZMUrcxoO8S
QHFqtcTacZhkoVXN21efBlz+A+e6/8wG6ETbDt80yAKYJDhPOwRq5aBEDfE4a15qngQaXZcdUAgB
jIDq6ddiT1juI4Lhc0LyGCQvoll2gEOnwOK8vQtyUJFMtaJj4/WORca1ql4yH05wlOIc+JfESHqI
YKi26xbKCZ6+xdckvoaDi5wCGfHhhO9RIgYIbWBseKwV66aMGD8liPWUyJPr04cEwSIs9WUgEvHP
RVmFbEOB3amq5MWmAXYaPaHRl5N03IvpfyvrWJj+cU2jq70FFalJc4L4iJ3YyEYKj8tdw/l2DKcq
nmVhnXaB+hVJp8/PrqQ0Pe1+t77fmV1fRqscJz8uW636GeIAH1mUIEWiWE0jj4O9Z0YJVGKGovQg
2In8zxwbc34EXpDbCkJ/fPG4BdTfg8cYjEBOmRA6ddWsZSc8ht9vsSTN/xbW+T9cdOnMDbw9Bp1j
kNE9hAxIK1g84T9O7Mb5Uyq44jtK2JilQjkZ7UJytWWWIoBmk/v0f2rgY0+5igHAM34aYAb0MeWr
LbftJDjOh2DMX1L+BW9nU41Dz1Rr2lLRGAnaJYaKi2rrrW6HJDoxHHG1gUVbIGywA0pgQUZAMLL8
6aj/Mwe+6R7Onrp9FCVwn6wR1j8R/wYpjZrlO6kcuwr/upINR+YCI45LCGB6BrDgqQfsP8WU+ekO
BHvvdJbLaUdrhxZhOgn+zlsXsN+6YLYj0mrlkoNOvAhed9faqi7dpqCyKckOOVpxd5+RBXTKBFuo
NYxJ8Uj0C9/y1AbFbYxgBzmeRFIgmQxyRYf2SDDLrMnCb8eteZNCKbUeje5I5c4jlLEA3LQidTXr
RyqBca+fxP4fIpcDWU5Zo44aWfLPnYceZzAdecZutZUL1JEDnK5k1cBHAz3q0VAScbsMM6ryfZZ0
Z/Bb4n/lwFUh/3df5GpnvVnxsHu3Vtgcad+XgqnChiBgqmB1++CBQN8yOkCnyhD0ahfFhA04fKgX
21Man2NsIIWk/G1UviBbKdeQdvuddqgDCeQY+1ZlPLKKwCg5r+o2HSdPgyHx+uVycO/fKqSbrdR5
l2XdH/0osfunqIOs1xuxuyGO8gH3kIY26jrEdy1EKkgPqfIZgm0guGC6+4+l38ebcQnlttDq8pTt
blvtllS7MZaclGHMKshRAvtQEwPu7It8yqdvm0T+h5ox9hNQFbbRC6crTKu4wgCMM6EnNxJn9N61
yE18RUAMDZpRede2soRCgSF0JQqvVh88chBLlywTEirQZmAjssqGVdFh1AyC2ZXfaHduOY2dggQO
Fmo267x2lpQoxoh04XPre9rRojpfg6kbl9v2vx0eO/wbVtGBVZKphKUQLTszKNBL9m1KzIPwRvaY
NfQvx5kKkrbDFsEBMXxIzIfgdREOjUj/8ztx1eQ39OEnEFfnNT78TrjWgNfz2ar8D5+LOMiHPT8F
jt6Hnm5/bOjtsk+BJSrtxI74VCJaGjyW061kO/lr0FLXqADEYZMQuXBDIzqcbB3JycUykyWSH10q
du4t3JUv4a/ZRusCSUqirxCvclxtiaCD+Odhi+4efaMzcYOT5ZFl+UP0+VsXwhJlZAXX3stV2lnh
t1xFRPdrJm4JDKqgKCvrnqckakrDTif/HM70budhfSgfJfIW68Ta4Oc0ws4pduYM846fw8/vh+cD
3wc43LgMmp3mcZCkWh5LeHM+z7KJOL0XCM45WnN2P3Z/9X+oEGoEb50RBSaS85CrzSkZFxEY2Oic
WumLBoqSt6X7K6NZ9eSGqWHH/RHHmv6rYVe124b2QHQ4qKV7tthHUAlE5x9svXGY7oL/N6RUB0Ya
E4n4JxP1TBF31FsFRtgbyGK45PAFFXFbL8jAAdkvvIw/8cnOb9RWuj9JwMJS5VmuQLRxHvP3JLWp
hbaDiBcDc/x25+xPFlPKg1lwCA5PEnbpXu+iACYb1/4Nr3rfZ28agNnEICHcFPRn1A1b+lft/sV6
UXdDliPL7UzVrRENY9kRcmzvfBJ2lagsAgQP+v+LzfWvAV/IVIkvf1nZRzXeiUh3H5MXm/zfuJuj
eeec8hsiss0z4v9H7T33FRU//ATM8ATXMwqsXbEcZo9gM3NICfoWX2D3mNaq/wRiAFxhxochoMqG
PT5qTtDsiYAWHkAseo0p9CYcga9TUfGNkLDf096sF0UWaaUKp2udsbs1QUpNQJLwAN+QiOMpd4PF
CG659GUxNRGpf7lWkgV0+jOn25Fj45aMW1UrximZXB7pEa+Ae5GE6ao2CoVyA9bGFFkm/2tH3/Wx
n10fQ1LsQGExef7zHZmk7A8HUEtA75BcSaEBnvKkukkJH47ICgeoZKF17EuOXQEkf1jM3eL0ANrg
1TRgY9QviCKdtWzf4E7HxdQ7c+JydFlfUK7c/0ZAd4/xJC4pqeg8pdDPmm329oJh4y2fwFOR1f/k
xEu0oeQNTngkb4TjHAz4aTB2vm6F3lcCzmMW1CxRxVkhr7kYJ8RJ+gYV4bnlAk14PiERW6VWkHhP
+CPA23ZtfXdFf/TYhBpfbj1Jg5Psoqo9rjf8HEE7Bz0IMIlXIMAzFM7Bt3xrnUhA2u84Ob13PTzo
dcynhHmXKOtFJ7hAP1j9ew01YY8zsM1jnqFLYeAEngDzkM8kCOJlEgPaZbX0yQQ8Hrh7q9/lC8dk
J8s6TZO44XWTTPxMJGUrGMg6/GjKfhf70ZWTTLe5h+MPKMT21NrCXwOI5wec84gN1rhr9knL3YAm
rqGsT03tt8PLPV++sYxNR2ht5HKqrG0X9orw7GHopC8C99k5K9ekq0BJe7JLo22/CQg7GtwewfQQ
TkjvscRwUNmyXtOmURhj3AQkKWo9xs3xcCmwYJTUmeMW41RcgW18KtkChNjqNB6Q/2Gufymj7vhK
xerBV/g96kAjbuq5xyVekISzovHTg0bJs40xeBIv0ZoKkq2K2wGgEvObFGpI+ZWh+CbDbueAucZs
vSywZ1KggXuLwvq/bKz/RquK5hNopif9RN6H3NfeMUxY/LNYibly1HSGg9j7EV6hJI9IbnRdR9s3
xOHl5/nNFpaMo3MI2aQ6fNesDNn1DtYg9GLzpur775oFWlrBITrICEfjfOz1m2aWK2hQGOBrQVOo
yX0tXfS2tihIKHwsvdLHsENJv1oOa0zESVISLqdlm/VC15mnYWq0VaSQ45xANuJUwJLbfX/GXPka
j2vhsdX8BCYufZ3kLGN8LEIkiBzZC1DqeQRem5tS5kMcImvtG+HyIoRMx66c2s2o0kGccMRZyXtc
yqtuyRxoq3DNUsj2vaxdwd5gQs3t7uIyr2qeQv0J+M0ablqlIlblOgtEdQC4ligkye1JAPPtWoaw
EgpUDCVfo/50RBJEM5jpgPR0iLkF4/40m01wk6x79hU4LIkh9Z49K7EM0LHNEDR3ciI4/s1cmC60
5dtZvlUIMyhlsiFpHDFVDtyRpKSUITCyT+mzAimk9vYNA+ABQueuxLmi+rwff9trAReErJBRfn3H
vU3+f8CKc21boWZeKme6WPo/rPyk8RQclih050ZLfeygz6EhSjW5QbvC50lSUofglV1Cz/aSg+U7
PsrpxMInRhkb2DqCDlTldoeuvgu3kZ7qkNyLzI7dWDiC3cpYhCnHoOxMlsKNxh7XIUtJrolKmMC+
DpZQlUkGK/cWrVdUqgFt+gMRyYGrbpKCjcjGamtxZungrDriqZN3qmXxfJXMpZvQ12rw0VD75eJP
xzHkmEDh8MbxV5eiYs679Ve876zneyS3+mBdkjGGbDrR/y4yQrhPTPPzidnH7upmzD/W0CwQG+o9
sQQ6oHNM18lVZtHyGxiX6f98qPr7WZS17NYPH1BquJJ+oxa8FiQY6g2TsP9pZDM4modkwZ8NHh36
t6tIyB/UB53VLHx8Hsy+M6Vb51Fxr85ug+EDjWKzzwUwxWixEQdQEbV/ZxURXJTEA2QWLym4nyZM
ILkDZ4oP1Wxu9EWlWmjR+BE3CFspKFyXvwzYh52iD8mkO5EOMAJ84s9U/q8n7ZvivQseaDwVFhHn
f3mjCQGtClq4d6YDAiQH5I1gRrIEn2FLTxnZZ4uSn+QdQ94ZobQZ6s/m6d7PLUvx4ez/di8aTBrk
hPMQWPjfDzV/Tv3G27HyZqiPBtYUG6JV7ti0u2saiAJ5MR2pBXvh+Gc7/rNssGE62iMRwOv84DNe
4gTyA1mgHM9EdoyIv5EiMP+5fQO9GXZ5PzdWte42e5Fldde9mzQ0r6rYtjTUBm7hqDuWV5KsW6b1
bpgi8UxhtYa19Cnq6f8Mfs3NP2b2DnyDEyYsLLevLGf7m/hwxJxkvXRJnFvuEbXFACihQ3hCUcpD
H3DxX10wNhiikuZjLWH0D70MBPPQRhg8fUAuEG0XMHtNEd5IfeX5D0fVlOt6efUh3j4HcIPbZ447
e/W8iYXJZZJ8Iv4WH9c7gRk4i5kUZLlpQSOYawPnf1a8rV9k7KTVRYzmhStTOmgEsZGExKrHnvCI
2Jv6dP8GxeM8s7ZXD6FmFx23UWINpQnjjn1OVQHSagv/h20IWG3cZI4pLv89kVg5CCMb+WWxZHm7
z0DJPDwFWibWxjejsG3eYoj+LIJtFkbD5H3stgJj3ih9nhh6L681mxqWZAjqbsUMf78toPH5PtwK
RZmAzdAkcuWMaglLzr3pXNbaD5JFOBdvSFd/DsYoNXBYnTK5tucJ93ICFtzduDXxYKeMqR9epiSO
Z+G/mYTOQuLR1s3cnlfbRoR3gMSWtHXRJesDLjgb/fBb4VWaZD6184oy/rdMWFub8Y4VcX1Gx8u9
yAPf/BL0qPtjjkHV+Gthgj5gXV8xDt5+t/aJMezLFnQoCPauhS5IkAIcbuivJZl3c121vA/Ffo7f
idp0LGObHIQBxJA5JfPbe2VEUeH23Z/6eeVZGdlsZKXzmNsI7ayBvqLApMfcrFIjGcVywtKVr2tB
3tSb9BXa1chUHdvXnfSaXYsB2y5NBvtzEmKOvGDnfs9xIYcG5yIbn1HiYYUZWBbrFEeqNIXl2zhc
Nbco4vEM8AbfYulDj4zOzk/EcSql0pnFvXhAyJ7RNUlr12DBcQJUn/B8Lem4lg4R8OKg3RiFMEze
1ZBCSVwDP/Dus3OfDZhEBsnu3Wo5pKbtI/b1FWdN+D7nfXapM9z3+HtxKCfREpX9KgX1otEVInbi
rV7WI2vi1iEXfphIJ9kn0NE1uFB9osZ3O379erRbl+ZP3bWofgoRdB1JHTzLEb/XYQoq5FLrLPsO
Y63rGNDGws3wdOzPfqxYBn93C0trbpaFXFlNYF2YETtRfDCUBTJRNrMtCE8VkdBqnTMy+ZDo+tIR
IZdisX8WdkEoE5cFNiOEiy+w9gZ43uEglulpJ+WlK7L00WhDfPbDPyR20FqU/6onXpbK6Ry2+hSn
OToWL60W4QZrhjN45pWBPtP9YelCxQDUsa0y+RhAjo7H2Z4MaSvdTcUaAMgYGL+cmsQB/dvDbuxL
6QviVfvtH6sDxlw/sW59Et56oisHbEgkMzuNKYAxeZkfl3jUqna8mhPrxo6F/KB43Hb1JIPRQUYy
wqKL86QaooEI3PzQGzIvXBTXPvgxuaJRAHPnruGGmXo0skT9ei/myhBlP1K0PeRRbWY7gqSHK5yZ
9PGx9IzvcyDR8B0H44yb96nb4jFH6ET62PaOCLImyLS95mqmkyVDxOuRRKbq8SeVMgu4zzGNl6Tj
1t89cYc9FXbBaPH6jtEDjQtaIh3ypJetqvIjnpbuVYkwsoonVLtI0xKI6/6C6CBwaoR9v5c2K9Vc
uuAtpdKp2uFpvRUSyXqoXam45PRpnvkLzFbsEj5B7IKRwxEOmEsmNuohiG664pVmlkoS0m+c8vgk
urFu/Zk38RVTJ/YNBMgM0f5RjNy29i9DhMEXGu0kaV6d5ic0t4zhF4lQpVqsBlKsVD+11kBQPcuZ
7D5uyhzzyaBxl3JkgLcFHVnux3IxRjQ7exneiEh7U+MAHyLIYJCnmyDRN8KC+O/6xHiTE93uCId9
SfgqZ3Vcd+ca3uF0YiDr/euUHrqyC2PK4Hb5AFtJoI/+TE3UUFelrfaLnZrlbwD/gcKzcxA5it2N
578jVE9h4dvZJgQBVr2RWudsWyz/ud7zQxnQ/hOrFAQCG/EmfhR6LkxBFwm1CCliu1R23p2ekIxh
xPJqpxqubR1+g33qfiRzuzbAE+nZz3MzpbqX/jk6ts0ywoACj73deRbmvsk4qQZ6eyMy5Dq8TYB8
rKQXhFVjpTqShNmgdPgnCW/xHX5VH997SqrqpIiZzT0ncYbaZ/6FH8mf6nU//sVmc+h16A41/KPr
QIjMrmfhrfSALCnVyVm1T4bebjzPOAxYJIUNJzCGhU3CPbPbIr9Mepc93cO0JMCBj7mZ5znClThM
wt8Ay4h24/S6AYadvvH3YND9IrH2EYkTg1nrG3tQtImjizz8jGBa/q9QmxK4s9Mx0E8aWv/sJt3Z
4gAkOtLeWTQSpJIMAbT9esQdRIbv5mIz7FKhUDhdVjCFE0iNvxq+8IwBipG1cWym8sQyEMuL07uu
1ZcIj7eTx3tiJl7LsDtrXcRiylb5jMdHNC6uAyiFLT6kdufkNSNaMkEyPnrLYuarjfMGqnddroJC
HVpZHNHZR23RXE/nf7/Zmvhim/Et+LKxLFZ8GoHC0e0Z30kaSpGH1QJfIVlzRTrB9us1Qjl1vx/u
nUW67qXDIuTEFxAKN+KBQ+4Gs8na6WtZFHCnQrUQkdWnpiWF3O/Mari/kx/MmXOq//qsiD2+/BxA
DzB9QJiE2awEwR9KYNg97Ofx1tVedZqPb537z1FmwPvy7UWtbi9+NDPpAjLE+Man1qHQcnFPOOtV
Doh4rQ0S6QJjpDlo5eSFFWZoHgl2MJoJzGqt8+7tTGD2UdPmvzG8WMvakl2pIM8jYY7p6YdAujza
ZyMzw4T/OGMHUZLbdx3ThhEYB6PyOer5E9It9/8xT4IBm1mfmMZO5nJ2H5Rz6bA+phvanG5V2Ub+
aS7GXA4sLY0j5TreuwWj7nXrqFiBVYhOkvZcmOso2Eqb2T1FHgaTR9xGOzJMQJPpLqFgS3Ix7K6W
Qx+RTK2r3luSQc2AdSdpgdZmI5qFMBNp8Q5qwikPG/lQD1wN9y8iRtWTW0TlOuj7pnC/bcqeUkUJ
/a/xpXJXD479HFFk8iJDxhXV1oMZ3e1UpZ/OMfCIUzd4Kc3okgMSpQ9Ie2P6L6xUTZorsCzOrkZW
SrLzRyhWV9f4MS97ujxzxoYnyEHp1mwnQN5evMEwERzqQRw9ZuBYmp7kZrY8jQ2CAg3R9TE+SuO0
buVFt9BB+RD1re2IqVIR+3KOlxfFhibdxfImk8kM4Af3rtnhwL+o/Ckmdkri59dl7XFJVLDNU6qz
wlbSpv7kdBmAa9slUdWRL0p2Ivyn732YuronjvhETHJsDvTjOx787gMGEI4lYZ6Ey2OhJOgevDUU
YYflY1b70uuPP5tzercKCv2NUsKaLbmyabnHA7QzBcJz0NE8LrerIkhkZUMd87+0zivPQzkBPFa2
QByTJhNqRHGzAh8sE3EaaWOqwL7hGE11KycQ7R0qfLslVCx6J7k6IdHcC1MbWbwU76eI9kpGx3Ev
dUJSa/9cIC/4R/+IPhnrjUkBpu7n5PKpWadoI2Z0q+Drg3Nxwq85gz4b/nHy9OUBoTAcOW1I3spb
nLoPQjPRMHhBJ5Uj1bFua3IxLThH7eq5xJb/ASACZkQiwBsF/mK3MzSSYtrZ8vkctaB+HgDkSL1z
YuCE1LGlW4B+VZmAXFMMqcWCTa9I3SjoS8YpPTA0SC2Y4yY4gA0v/z8mUtjVqzzoTfAwGCu/ndjp
PzoqCT0MEPPD+9ZmsGkCBReUuogtRiO6wONzOOaRaJA0noFaEt1NTBAPmIphPu+TEwagzdSoxgVD
GbO8dILFRmM6D7ZGAclM4DMHzegjb47yZbNktwkncPlRJ3A2mlol1GWbH5zeyk5euCIZNtVbgivW
8AMoFNlqiFT+Y6gh0Hli7p8gsOGvoXDWfQ2YDBsUsIkkhVxO73MU3UsFkpgGIcWNocMPMF4Q5cqm
PVSKVAtkICIHrCHg6NXTSCEZYHhpIA9xyi/RrvSSowaE1HXHDqPUmI8/SMYn2kIGhpT2DKuOyvBW
XRXuUMxin49OKI1q3zY9O3/BNrWl/OI01vHJOFXqF3t9xxMpIQbjx0NRpOKV7xz8sX1G3/PxaCmW
ZcTJyk6s4yB/wxgF0uuR1nWR6MuUfZ9LdO25QjUNifjFeV5fhyB/xyqgNvZSpFhKkDgOBLNbYuBl
Rg1N1BJzwmt1303zyyrFW6/yQyTFWkODcdjz4W/5BGgQXVpTA/hdqmHdPSwhcM2amMxGgHA9ozim
7rwK0sUoy+5doKqvyFRADATIO6VcQdFJ9W4+Oprigw7rNIvuIUdbQyotohlkszDKZZ6tr01WmZFM
hANYyS7GWAa4uP1LaREgFxN10R70b5FJMHsRXt0JXeJozQihcaGRHyplWNBR1H6J45wU66mIR6xh
SnIdmXiQaWLCRTQ7Qs9iUEazcmSzZkZagbrDNUdFTLF1gt7GHnYfYIQPScyDd5Y7fAfFxtvMn8wS
hdiwyGLokrjPMXvIc8SLHcsOrhB2DJ+z72Pb/eF2fEJjbqzL72ASnuHC00qiyL+O1meVDThZ3VvO
qxG5U25qrepFNIAxqlfQFtMJHqd3/QsxPDwoF5U1yjSuO6IGlf/tKa5xbfFTHKXgcUR/f2yqp3M6
8s/JZfkACCqAmMIMEfS3iROSuZsbj3weiiKbThv1Oovlndb/ZPbNQSprWib7/EdMI6KY9Lzh93iM
WM3kEKtIX7FV6WocFcHL7WsKf6/0/SjFGD/ScRz0143maZV/2aQ21FMFIemzn0IHQMIKT6FuZIcu
s3WvdD7IklN0Pp6kn04b+28JHC7mC0vhu/I3Yw6bdhc18FI0rWzWNG6B2iv7HDQM/gMaR1ZYeFgV
G9Jugj6wWV9JgyvukS8Fv/IqPUwr+Sy8wftXvt5aeuUoWh2cMlsjsx32mb8t0k80w1XWInd6MZYi
KXoK6kiKEcwRonSrNYz7PwbTGbI4IgQ9CJbgHFNlzFnFRlnPRMhJ4rov89skR321qrjzmu7vpy0E
2oJMF8f8hbZqrOtTAhHjJufI2FF/4Utt+SYODTWqLw0gwrOKohnJ+Qfgq2xVVoXawb9GXYP0zGkt
8dZwFBMBFs3mEmvyDIOos7ZaKxibptVzvFkEP7f8/k7XU6Kh/FcJr9DbT5NF8ybz7VpNvuwAkkiH
T/Olw71F949+9A5SpGRlrNqGzvgS/GQzCwsvt+ylsPaVA+xTmmWLTpky7cpWQ1NRSU+mXDO3vhZu
LbQ1LTjsliZKxkPU6WEP7C3ePG5yhi6ELEHNOrUPqDK8LERNV7Ov9/5RAferfvJZV1tGUE11uwV8
siEu78q2ruwmIAmEh5A5sx1X2Gbab8nhvA8nzu1SsIbuwPRq5wl1kWNWbgRTLnrvkZryYvEgiau/
Wx03qelA9ypq2p7VsVEqDDFWM+eFiRB/dxEou4QKfB2gDKg2Y1ej20IR/kpwAiNMaCOvx1uA7tSU
tH2VEOlKVdQEXBLXxBpMg10xacNKr3cO7DSamfYrQgnnH0Ms2Me37t+XKN91Yy3fPrCj5yey2EBs
Bj+kD0jzKKa0GTNCoe7Du98wJQkRplt8hiBAep8xii0JkFfb/lqO21cPUfQoSXzLgUK0oNUysxaO
vme7mNRR44CVFdaC+Q71GynwpETA8LxdD4v+ddTFXn94VEJVkXWUElwX/0IzXqmLCxsP22RB/IHT
FfhWKpypKOVFtC85Ujsz6BKF7QyJk+m+RiUfncFmeZNeQkA9vwtC76RO0h/rhqc3pbhUNoZ6KJc1
8O7GD6+etJfGliDa6AejuhkZ5c9w+Mh0m4CqA1ffQ8qvZ6aiOqoNCpHGywhYATqiuIoEo20spxs9
lLXHwlDVlZzz0N8PL9at9AK4xTbOJt7FU31YWdOe/4S9sGZXV+v29Oy9p+LbV0CuP3XqwtRIvmww
dAxB8YKiQTdN/Uk+kWBiVkQ7FuvljFLlnHjEtFHol0j1UGAsseRSG/W+Vldk3l6JOHAmd0Bz11MN
0RKzNqdGSgKmtulb2ibDHvK02w6KHfFmOEB6Y4mBjdM4MOU3q2M0jV635mxzU3HWj744S15HS9Ud
wO5KYWuPsBEAahzMKHDjnMI847j9Rk6nmQ6TvnRkZv5CyK2LXjdt7IFzX9p+cw/IWhVeD4+zAhQZ
TYBtfKGRn6NGIqy6PKXhx1e4Z5QxkNWVtLYw6kh4XhlgcBJmP81zCnRRHrvRkP9Kr37Y37+AGSP6
rlkX0inqax8OM4ZUkI/xDhjpfwPmfd5jKql109DjO59GNjhU8+baQ7U1Z1KGixr/52uVW6/ZiQ4r
Q9Q+jsK+6HLM1cq5RTfw/UdeiCJoEX5WCM73ZJRb7Zn6PQvudbW4GMA4MfRX5GjYESJM11rtIcn+
2JKPvIEbZ2rud2JCOEytMhvN1miuWdMBbk10B7uPghEdJ2FFyUeKwYSk48t24dkNe0mj1W8kH09b
I2idz/0Rl6gumOabMD8KH2aAHRrPGs6/DThw1074K+nLMc2yyJB3lIrwVGHSLVrDQb9dzI4xfC8Y
qk8zcrltNHIyVXooX59R2szrC6Y+a87fFZqvJ9jadzkG5BZ1O8SJlPfgMJ2x3A/mbEZkF1oyfvuO
2tFEM8d8pkKrz9IsOFJO/2owcL4GjQ4CQAFjovdwVjxKG/8RW8cIl9FNoOJq4AnZfB1c0I982r/3
jetQONkyRbj4YPBhhvvzOWMIhFynxlzyU+VxXWWE71hcpLk7IccVfdJQZ3Zz49H++/h8WsT8wSzr
99d08xsYrM/rHRYUf/3a36jIkz00tSIqlaw+ECfzuyKswZG/4KCu98nSReuGJu2oThtovw2xA6FU
gAzStiU3G2pYMl0MHGzbwcIzH5jfU40PvwXlD1MolgyLtG8unlZTV3DSz5w3nwINSyrIX9SE1iKc
vlZiWa1iRdVMCAv1r1S6wD/WaNDr2EsBQukZ1N8KEtRjg/zL8X1Fqe3OIVonkYQqAeof4mCB2Jmm
ezpwRQld8txniB1s35hN98EyY+d2t3zSBRfRep1RDnUw4k01Y/JbxeClwUSBWlZgn0AiAt33gx0n
bOnM0N1HFl6LSaasOUh5HHy+4h4kyZA2zJKEn00fvzqQuyT/6v+mstI8wQQGE+gLwNMfWxfczL9H
TeygChmGacdFdC34AS5YcmW+RQtz+YL9mx+xy3PVLHc1tufLqZY4IlK6K3Px4udTDrmSD4E4VF8I
cjz+7aITkC41ZOui2nTm2F9leCVQVwCgMbQFLsQzluTFZZGXCeWBLatBzICrzopWeNQGajAimSIZ
qtetc025t2qLKyusi+/WEknU+5/cbzNawXnSLJ1Svn11+4/o0ODmP3JL0WztHJyh4/sJgV3Bdjto
qzLhp9UiVCCOgDU226X3Oyoi03+UkJyA3EcBCFchRHShKsZDS9eGmxjjwrHlZp9AgFTMne1fKmCZ
nUaGTGh2xb9Yl8IYRL8grJfcow+W8hdeqTebqqMEFwuuxy+KPFE4okbd9AMrdY7V2h54WjSKuNHz
FpCxZTo1/IVxGjtp8LZHNaAVM8AoHN2/UYWDJi0t49pXYTeQyEshvCj/lqtmrHNuIi/VuTr7M8iI
Sto2Sp5sOCxWs/jguYpEJg/VLfE8J9R0X1b5BjSE1wfoTdRMOBhn7933GRpQWLRHDD3woVVmPpbO
JPJcoWYK8bs2I1BXZySIeLpkDUkugANwj1XWaLXFi44ENOk7NmGA+/LYZSU3mvPDQl9YE8JOwie2
hOeF8Z8dHl2JXSfUf8rzIIexcHIDqnPjbT8pe4kblqdADkxUxvyntSooVnXTcCZ7vZhLWwO4LjSJ
lJh+adVjAMk8Sy+kqeiROuBknbkq+mdNO9EsxKhGvP8yBId1qn0PYQZGVOBPbYKDudoshocKYjhQ
n8szJ9XHLZ7a7CeOSprFTYx2f1ZpuJRMc6tccNyyA1tkZA6Ov5iBCrVkjjd4vNQjc6S363hFv09v
ZX2MlWfmgM2UT4V4+QhFlCZw8Yhi5x0wjf1hIxuTbbyyq0KthnzH1CIkywzciCYhjykzgfmmAmMR
uMNduKPj8ozvxXBaCxtYc7TGjLqaaBjMNh5KR4s+jRwMokL16iOpVhadZrTy5TrNkFFoxbL5gLju
4GqvEM+pzBy9ncMWIYUi+xO79SOekQVCmNLjNFq967iuax1AqnElBVEW+UuQBdsKCM8wODzbcKqL
3I0YUFGUy5PY1sV/4mWJCYE7mF+r8ZYkz0Qn13NuOAGlTFNwziDkIIJfoqn1P+Z6wzpM+1biP1vh
fvb6UdxflL4J1rZEYnBICo4RIxg2eWaypUgESdK5h/pTH3yXPKHtGK2xfwH38MI9LRmOQQ/e6Jde
r4SAwok3Dc4QUN636+Z5q4qL/5ihNkbAt+YLOefJGkPMZNKSAGX+qOVqA+eB7j+wCOaWOViD6nKG
Z8NbojT9RBXSBlB10xTD5lhLMUolmAg0f2FBs5evXZ/bpDrdwkzyB07UhPxhhG/6p19HcVADV8mh
ttZmr71qvHacNr+f5nf9RlfKBXXL1le3HQc4igtCc3qTXYTsk0wuIlpervNtj2LLbg/CvcYD2IUE
IVs3a6MPrpSKMEiLnWtaO7aXYcCo8ILBeb86bAd6YvFQZnSIqIsvxzgaqDjC/ehcxsYC1Bp5p8sF
IlZMi9BOsJ3Wua6vk5qCFDHS3ctKIT7Z6fB8drlJ99ccXy0kDbvRo9bhuICdG1fB49MbNQCMJAkj
iNKVH9YzwFPKlu3/FJFuvKmI/1SZFkNadfgUTj35DZUN89uUt3dbEiwnCmoa5z59Z8y09PI6fZAE
3m3ScXFbpOCg7EyfrjtlwUOqWAJeeN9RB9cza9729FdoqSeEd9jP4EMcwYJnX3GhVIQC67jYYNlz
yy9Le0uyfZOE4ZyzYEio7ct3eT0LrF305+E3WAVgtMGeBKxPQK/Dc/KVLNYC87IZZmi91gKieKhQ
pv7yRLcLFceKQPWZdb59qdo4aDZjJmy9BUA54hZZR3f8KK/AMbOGYS844YdEgZaHc0lE16chjXnZ
BI0ojb4uz3SjDb8Q4AinchLTxT8BkHONADRq3eiUpRHHJtk+kTlJD3u6Q7Lva0iUeJc0+dnuwf/e
RqDMBiGX6M+247n4LqD7HFCTln6M23kjelIMc2AqPDbNS+BuW99vfRlou4aFfRXsJHNEHRlMb4NN
OpRAvFaKFYrJqabN6gFJ/TThArXjpf5s13guqigUL3S/wzZG0g5fAITxboUsF/YabxOzEuedNSOl
qJlJ8ivl5Z7PF58MG8vdeSMpLWi8y04vocMSVoSIaegmQnfojfrDbUoIhJpnY0pbJctFZlfSzvu7
JkOJq2yeAxLo2Cu7fX/LJO8hq9+mTXz9Rph8XS0WzPw7xg8ORRf7GgHYZ58/QwnHYfIq0uXlIfbp
UeCVo1rp6egBs33It32G7iALCjat2T5QPZ5OMbspTf35crVJKDZVy0tfOti//jALfMn3v1Fy0hpI
oc6JET9mrS6nK9AY4eB6u5IDWzDjKnStJCGjKrbkbLgGHdyGjary5jLthr4qM6sqMVqIQNQoVI6f
j8XtaQvfLe4BtRjZKNAlwDfmnGAuVCDxIvpR9XHTD0lEGQcIQJW1jfJQiCm1m+Z2C5S/QBFKGK27
oVWetWlMFBb7PF9N898kCqh/CIZUc2DONGBjyFlyNwUDlVV1wbAfer3ms3sn48qetZMho8RhoB/t
deIZ9pJcHwIpdN8MpRuovoUwRrDcCHMj9Xec3aPohBWxHM+wifKG0YAOKOiuS6EPqxct3WnkUIBH
D8AnA2F+bm2dm/BjKk0V/HAcsIuOIwMsicuSVCyqKs8JTV0iT1zf+jixn3vdyUXvsF6WUT0gMYun
UXCw1tYmZFMsaLBtFByOa62p3ipDP8F7rRDKRLl7nfXS55dFtK7fBM0jfBTzVtV1Eg3UhI18vjdf
JnqBs2xEMJURfxZ4+FjyR9wTmP04YuaOPzk3ZlfSYju5Q2avirsXNUgCNQCNfbNwQc4qi9RYk0pM
2A6xtjWbKqMmRuZBP6yS6IpgEtNj6+3W5AFCCSnk/4gCeMxbYh3ccS75moK7B5Buqtl5x4qgsrz6
JJ+7cAj/8wRVMYOlQN9NDn9K/9S1FgpnaUNEDeSu59Vw6b6Xxc7wJlZCxBzIbuYxET9cCKKAArdU
XjMtx5P7uo6eRjEkvgTlBP8K6LIJyOxMSRxQBjnU+fvuQIK8zwU8HadBfwEylCe+yyyBBA4qSa4U
V3t6ka2D/7vckhE5JK5d9Ac3lTK7EUXpkNou4Hv1UWx9JnRNPmYBUQ7Edf7rREr6rBo2X8vlUpqS
O/+LJEl/XoaLd7m7y3/LSN3SmHaPP0LjJdyTWcCrw2/dfsUtcBYV/naxwO+u98hgNxKuUa+PfAHp
ywnDHHqb32Jc6vUA/kg0kZEn5cFilhvF3RBodac1nUGcI3AIMIZJFb7qDhmKYu0Kf16ZSG5hr3gK
V8hwJXBOb84jDvzJTzlzOQ9/wFiJeKqHQc0EKoCDFmv81WpjQDnpCR6+aihVmgJgqzwD+m/YEYCG
FKjrg+snhO8bg8sRWsOOG4u9AFpGabwCXYL0pJJ3IX1GsORY8nEb/kwfqsuQ4y7EQl4/YDUFpKfB
/O5dWriTG0d46DTZ33PPWa1gcJY6ST8N6mfyssGlrURnXlbR5y02+Qv1/K3giK2yS74jrHksmt+F
ZycnrZGFqd9ddvdmLI04a4weqbKBLfZL11wHbzK9+yZkRu8uERbtzJaH1uiFdAS/R6qvibRp+g34
qn08IPsd8sw49B8eYhTrX8g8uVLBnIOmzJuwmUAv1N2mTwAzfdfGGR7R0wTnhNK70k7SwYAH0qDz
DEp1g2nXEgWaprAlmgVjAIpSZMrzpPyxanlTCUR4KRE3btJXdVKn5Uuv0dVoZ7waYUj7WViAyr8n
VgMofuBwTIk30cslt2V3KIluGqsnhhFILwf89egxOnTdJFhrPJ3TPma+8o0fEwGdVIpaUgZFifFq
9qq2gtNDidWkb1tbmMb1z37onBQahQ+Fng5MA6Fb1rZibCYeeyghNmb0ma+vP/GQ06Mc3JonVOvA
kmSCuswNogjOXFeQYem9xXo/wCqVIdlgbbffoPCOFl4NBd3bBM67wvMyroja9NwuYK01VzD+CqvD
xj7r8DswRo4wVw2G9XCXDKhC7OjFtbsco+55NntqfzHL4mLTVkAkZryXdQ06yLjdGPFn8e7tj53F
GJdB9osTJOX2OkpdvTOVBLfOQyUPxtNrhn2wk+2D70oOWIPgr+ImdGjJwlrtQGCI0pOCilOnFycX
3q2HvgZZ5xTP+NZKazmLiid0AmJyzJe2vUnuwocmasVOTf6J95XJMr8yiTzNyilrSL/FEuAjHJ+J
uTttOu63lMWZnDycoMuHA+CKtLDbrgMUetfx3q9hPFc/8V85X2dE2wNlABf/beZ5yc8PAIM5nEM8
UTWDDAj+8Xp1CfnyUXdmHQKv80cZSna2nfsxlAs4ZTTi67UY3leqqEBCok0+as8Myr2R6lEP9Rv5
FlKybMUtlkc16BNJjIon2jfVhbEYuuRRPfvm+PedlOxuSLsyaMvICDxc7aEoYzd6Hj33guJN3ADL
ELGh5QhJmDZicyHprlYpYyuuzC+UASbtpgizLRZ8440KqvqdtFK+fbMfxoXhnKICBteh+9nRwtk6
5vQb5XMEcm5NmU4pVcDMbt1xr8c3A3i+7xcW0IIRZiaT2Q4QsXQhFhp1Y4kX1aM6CntZF+DKLJ4F
IfPAUIIUe/l6ZtnPGk0w80BMoI/El/9/PubxIJOUT2j+LXA3Y9q47cu+0fUJRJhqLyacaydaRzqb
b9JM1kj0aBHofptfbJEhWOzU4zkvUmhHGMk7BN4cWnxh7A/oG6zvHds7dUqegcLUmFFpsCOMzIQJ
bLjJzULZJV9SfUZp2upM88ZK8wizH9oOf6Er0l+Ghx2cAxK+ZDbQeQ11ljqF3Bpg91jhb/zDPnQC
Qf8UcSarnf1/CituPUVOuHnnkDnNXE3hXUfPqtkTam492wJzps4F+jFb4HJqiaMgKh3ncwH+mnK/
lAt7N3gmZjdram9heeBucYqunlZw91t8Ec8WU8h3N3LGIm9OPtl2AZuKNVgh6PBTBmII/9SS0GMo
Oq28q1M7Yf6ySHIZDwgzpIojdwc7Cdr5i6rx+2yWRwCOS0iz16t0iAX78EgeS7TsFr2u9odH+3cg
Xrdt/PIzhczD5JFwyIEBnBaYg/URW3k5TXj/Pt52YynbYJYppwAyRI7164Rk519YxVm6OjsIL8vK
isCDN2Cz3iFe++xSdv7QrvrwigoEauyn649LwMz+FQ1M2yx9cOHMf+JBfzOVSnhedlxrkWRGsru4
XZBqwBHr4gKeqCE8VkkpEmUNHkJ7gaPS5+nWRapX5g0FEJD6fs4iS6KmSvE8Ip07XpSlZ1tamyjS
E5Ov43pnBJv9sjjlRxlUkqr11iDjqvxef4uIftWGcMLnqx47mNg0kAh2HFgIjtL5N+m6DzXLbIWC
UWggp2d1YMmoIw42hXgYvo2gRv3qAG6Ugnrf4dBXy0DqIYjCCQdm3+m6e4CF9FEsjAUuOPF1JeD0
FpatcpP3EF5V0s+QzokrqrC/+QyNrzby0yJsHAx5I8Ai6W8We3nF0Smk/ec8ayJ538vcJcT2Cfbx
RmIlp45JsLMm48OsDqPqdir64x63b4YMyFM7VGYh+rTdJzLITXkuHU8J10iOyltE17P/YtJW/c/P
CZ7B/01CQm76xFIdz4CWLvKuOn773qBHGkrrFSs4p9t+1w0dyLUI12GjbKm8ms2ccl854gmx65El
IpEVXdMh30PRr+N/vAgRXNMwiYzzhZ3PBrA7FHUxIPaZ0Eu0V6yQ331HKRfktytvW1udghIuuyRd
RYX926sTlXaAVD6c4sA1iARnb72M2wL4NzqCtJAHqESgPt7AxEXOcX+yvANph8r+ENllGw43Osoq
4oKAzhyGr4qI8pvqBDgKSZhjY76CAQ7Jda9feHojO7DFUh+tgg8QGNMmxjTIViqn0Zv0ZM4kr9Hc
/k2kit5HcjVGxu5zFMfacUCoAx9OAzN8NopEBK0GDoh5OKnxEvxT3wsxL8vwCmJIai0F8LsCa5vn
lMCsu1/oWqNPH47+7z+fa6XcP0x9qYm27E8vj+7ya7cCp1IblGV0jftspMBA86x03bnDcnFXAv+F
rmhxmMpo+Y2aEwkumoagjZQDfZQdidSp9apgpSU1Sh7NcJJTFRq1R80Fi5jPi5ttn2y/kT10v+Ez
/58BgA6bhd33P8NXcJ2hOZ3tk1ivfebFBLhwrtBiunfKbFt85FJHgjsanLdVYzevDYP61WALFtOn
9N+NJBChPRlL1MCb6u0i8aZO/jc+lFdtjQSRxXIEcz9235pluppEiQzMXekKC5Yrcc6y7i6TwDbM
AwaiJd3zK2r5I0q4ec+1487D8Vz444Z+Wu5K0WxhfQTmWNWsa81qu03k6lBQoVkH0OZbmhgQM8B/
2GlEipGB1AC6Pm4lXFaY0REoT9TjZpy+o6ANaQm809G+xAuKuOlwDb7Wj0yr1+Dj9LKnQ4b4ryNz
2AuGSPSC5CF84WwL8KokX9YaA91ECv2z5XjB1bcG2pqvAoOC9WanqDU0OGRI2v4FDWYwJG4D7SC/
zg2LzZMpm/k99KENJ921JlXjFLY3PDuHJCfao8IWab9lwIn3agoqEptdZHP+0vXZ9lIw1MV0Cj8l
1gqxVsYIBcfQ1pRDYOteIeUfU/etKIZN4g/hZb9/7PPlUBoXLOfeZiVPS31iuaDWWV6WpX02p7Ed
4shrBV4MByNQqBDWI2v/go6QVAWKyv+wTmrVff/6dST6ySO4elmCJwB7YnDGaaazp5NZ+h9N78WL
H/ELoeG59P1MAO+5pqpUe2CgzWTKy5r2LWypZiXdYWO64CivNDzJEAU5qNGbajXxPEAOx5NKRzMg
JC47a+1ExQF07JgM8m23Y0/W3owqd2d7KBkgcT4jkr0v6gDn9U6su1X6EsC9p/hooZ7eWs8YMsFR
7jcmpDBLK4JuUYPwYfLk3P3NOFHnS5hOINDyidED68lgRXU2n2tsnlas/PdjXR3S0fjSuqU4subk
IAbEz/j5jmbKy77z8RnAJrWWUTYaOfa+i7GeIyPkkNLJeuP2l4rWlOIoYPpV6a2nSsPRbVMSkUgC
T9PVD/dVOK1zPZ3QlvyPy6P04GeznUAsWyk1cIwVpFeszg7JeuNe6icjSO8lPuzQQZcYgp7/PL+V
T2Va2LM/eYdPF7zgLVnBK0PshhJpw4Z9V+jF8aTyx1sy6HSgUN6W5ZYw2RrYMx0594sqAff5VCqR
1jjE/xttsErAJW+2oxrhYcaSgpCkvFwVM2huJ3+QaAtydmsHrLNb6kA3zoaA+rGpxmDzBnUl+HvT
HMglGBL39DwJALn2gjtVEt8Cm1TOcnHEUNGd2VTqsgSai/vKV2jPa9VvJ/OdWUjgF54+y0l55I3C
td2f7VenfA6Gf8HoMt4LyLP0SgaXTRPbJRZdBTmEpNKHzX207rkNecfA2MhENgPPlbdp1tCA37Jl
M2/fFrBApynHEIkFW5g+AYfkmqXW3lfi+5ZPSReerFKNJ7VxCGDBWKOHhAMGv0qz89ZiKaywf7XU
BlFjWvwW0B6a/wcEymQAnBxLGpPps46xnNDz6dda6nzhT3MQ9T26fRWSMPpI5s5thhPy4aoAjzkf
W8ZG3QIcUxRGNncPDEXQ1oOaCPCmoQkfFvAtMY20B/VyAZCGUYhKBHE6550OhrIAOmPqXrJqp75q
QYMNR2nculnS2WwHa8MGGcnLdC63L8jDw8rQK7xANOt6mlyTK9ooIopZn3fJbefmc+Empx+3ZtI4
obYBHplYeq/rhm/IDoAyxDLko9LSSJDwSI6ku+PwLFffWTRpciX01mNjwlnbbRbTp5n7eZyfnt6G
FOtZJUIbnmT0XXNcG6UbtbjOJ7kOBC/X+cQUaxUtuYaX/9fwIh15Tcq5d3eQjdfcvkbnEqOeFoSo
GsEjtsOjqaCZiasj3u/9es57qniarlzkf0iXEbG6vYnLQ+8eOlP94eqWSw9J2MLHjiHiM0++CsZl
p0769DKPb9lFuyDqh/0gc8YeqJuxC/cbB5WEXV8azoIQpA2GnuORiU3zT2PTxsh9jea4fUH+lvqK
sDiDLNzZR3ucIbzcrxC3b7YGTl2phXLq5uWfZ1WHj4mKj3OPMoAvyIh65C7jprF88oPzA1Zj9XEV
Ovl0QKQOruP31oP8MsrtDp9t7rz9YvvsnT1eRgXrszLQf7Ti/cBUX/EDA73rWw+VgyHtuOnhUhly
bf8jsmeiTQzqMPSrlawwnZH9a6PZh9hSWfhsPCYcilZh5pZaQ3rOVljDj018Yh2gtghiXN+NkFat
tpqKVldAkbfFAO1Srb5AK7KfB15QHJD/bEVhywALmpNXJb2XZYRXv39arsupA0cgokqaFA8nbLjs
aiuLnbx9TvbkfFLAE0sJ9bCjY0TQxm2EMJKVI5I5VDLhgwkThfDDKCUsilXY9ZmhrO7kp2hqQEqg
lE4lWXw1S0NrgmLWCzxeVPJIteczeX+M/8uNqiQMteWMOxl+0WJj9BBF5SR9jEWalmQWrBCtVTGQ
E6HLbuvfmCPPTeLjACqU4v70wxvMVoWoAFEs8ryohQFmCAoWeGgX8aBI0ey+dG3CFEynJ2zDzxwI
PuQNw6GTGd8JPdQwRtCw1MgI/e+Fhe7rA/L/OWvh20GjbmAfzfWmJDeA5Oez1roZTrL3k90E86az
GNyonNEl+furLFcKL4oQ6Ug7gE8Yf0/7czOO9tO9RZILD+wSV4L1WPD52+9Py4O7A5f2NaHOuoI5
TD04DkBUktszMmMY3EoJE0W9+L+7CGrX3tlCLvhBfdhwvY9u5VV6oHzVniM3WBM+26+zPnUzCGHg
2XtVJqdc2+HEmo/zX/SQ6bTbL40bRzeq3u4vp3ECP+3b7IU5Cb/mZ/Pkg5DSWzedjTwNTz7NNesO
rb56sjNW4PGwsOeHSLa8uWTNQxPX8XxxF/Jt0kFo7RNhMz0+nkkL0HJk1xhOUD2yTYVywChCRdRS
2n6mFlAVZuLEP/TdCP87ii71o9ZnSjNB93qbDeiCI1VzvCVVrEi5PGEUL224uyRa+Ey2TM0EXkii
BZhBaamny2uiR4wG/W0gYTzOjlEhClr4BdFS0mnUQxd/3kJI3Z7gFO0bNfRdZZyKyX/KamJWcsDQ
Givx3G+6RhkOfFEl6SyNEDaw57RPWwSUjQ2viP0196CMFsFy+69Hi0syA1MhjQCy+uTONJers8m1
k6lg54zuzm0Iidjff8eusSM4KE9u5sZBo4WCkrsO1UxYI+ZjEp8ZhZDv6O1veOH+g7iUuLpXgvZa
0ZZlCcDIPPqi1ofBBR+exP3RSaePLcVwZrtcvfmhx40XDeFEThfcS5abZXQOu/0hfgeDlEndiaBE
z7utnYlNvg9Ol1OLZWy84BgjNftEs9XUYKjPBOrzrWIKqOrkapBtIu8zCCNdcKoYZTHWmHRiUQRo
L26BGOnxqvDc/FMQ9h/btPOr7fxXA9++J2Oq/aLvboFjxraMIl2oKbMGjMjGIcbHX55BL5OmY2N5
mkA6+0RtaLdphKLHDoaNm9Zox9/nchHoioPfQltlnellWEb506g4bMxP5QN8ykxTUqYxbaFKbXsc
AqkZTOK4Ps/3YtKJsV013ixDzZ3wFxKoXy2RCTTEesFBQDTPe1PE9rsUlWOwLt581UeigbvHw1lw
YAXxpF4+c13Gj5hIAjQWQ5/YJUPUxqyVnbjMubP5RQrWe8Jha+7zfPAIyMIqZb0jxWjs5anJCyc1
BT/s4KdkFeIaeCBdPsBHU9HEOOwz0cME58T9dyFDS0aBN8fcninp+/cOhsGNOkv3UMmTbL554gwm
oaqlGGDeRE3kJ5yc66Mok6Hkg+Pt7pzPsi3FkaFZCSQMJUEwpkPhKuq4lyRHbnfIq0FvNVFzogPf
HuvH5LJu+sr9gqhf1d9ho8Ct4XJOmdFJAJP/a6ZlxJOtZWpyyj/9DT9s11mkJt1YZ0Cv/5+7XwZ9
/EokYaFzLL9UIg9P651qxaetFVpfwoGs2AXsE0EziK1XmeDFG9FpGnQ1kTTsPekg7gkW8e+NjGAL
Rqmwm/HdGGnZZ+7sO7LXHIRJSudzHzvCgNyY/h79TBw7HEGeP3x3W84PhqsFt+xNxmvjx5KsNGGm
RFIemnR76WLWhz5pUS+bOYvtugbOUjQaPH3RUtcK9tuSV9cyDRsoYHSfupwyoPJO5V7ftjJv6wwd
HQzpYJmdpXN3ZooXhmt28xhRETU/Aryc3cy9EIW/TRA1In/pcJTSzf8qRl8NM+hBBDB4RdyKQqRG
Gfc7dGA8pAqOxEHXXN1GiRDasCR1SKz3dROkYZYzlT4fWEWmSJi0T+tyEu026mpPDvNudueQDs3a
JYSB/520WwVPOq3uAdQzoiObw7nsM5jCZJE36TrZNUNk2CSsdDm+55s7pV8wlS9w0JOecvWUoGJQ
k5gP/LmNN7HTqg1dIhVMEQBSY9kUA9A8y/YaADrp3YaXbHyy58W7v5YBOCQ2Y5JEEal/bvAMRykS
CdYeiCcCqDjHG92PQP0zTJUvbE8pb1UoGL6SYMLUNQCHHuo9uTbhdQkfUq59pzYwLeQolBA/BAtD
+E++8tpwZU3NzfgNzS5w1kCRwNo5A8bU4bvmpQYIQ8jpK3bi0wxzSq5EXLnu9fcQBaaa+DzxOpBN
tgqw0BEEps1vydZna9fbLo3byn14yM0fy6A69gNGs3BASQ7p6aaZaTrBFyEbjR1k1UCw7dWaZ0xX
oKryZ/s4ooKjI7Dgt+JhmRVThhKzp/f0kJSmYYopf33I9dZCpZqieeLxxjaU9mA4+02s0nKYQTz9
lvqv9UaGJEt3KyOUXw/GRvoqhOnXp0UwoZeO5AHr0v4OsQmwfEdy64mAMMekZymLT2UitFmRyJH7
MLFt0KD/gp3oidcyApOkQxLhKUkinHCLrjU19sj0YlexSZoXX8/Uv5t2hbn+V0zYPK6ZIY6nnoOZ
FNO6L14cRctTwR4UM+S+HSj0emmaX2FqWdG0HF0jEufVPEh6nxbvl7wZP7S4932OJIxcyAB9CBWf
YakysWxS1yNN5QAGp2LrcXsmqPgwJQYz0EoIuOpTcfBOf95xXUKAuxuLBvd8gqbLjWTYm4jCwqFW
PWU1/woLMLUnA/VcaLwYw2v/idhv0wY9yEkHGQNagGrJr655u6/pM5B3DrdvvmHubznq8NoC97mo
Ff9x1O3Kw3osEHeUO4swAB/ImUJhHtf5/MUxucdAyU/Rp0CgIbHHn12gTIRwF9szokIrszQdHDgh
9mZstfsnEXDI9brqyi3yxvUng6+sAzXmzms51PNE6nK0nuAOIUPYk3O7d9JiWm5Zc/mGkyDP1Uaj
MgOC3LTzOf0nAcLd3Q9Y//yNYq9Tb2FhI9rs2bPtfzvA4j4+JVmpW8BLKVZ0GrnIHetPoBIe+xA4
ye0tU5rgdg03KcQ8543bTWwrM11R2gZsDEtNZqUC8qZ8g/J1+7yyM0OgNqSn5cKehaDOPG08eKw7
kfEVPVqys4Foduh0ayTCzL7OqIO+eRhrY5feyMebgliLQCvZajXjHlYsauZfm5wArm0aheIrfjNV
WdBsysPznMafSnO7Vqd92qInuAvwc718/Y8jo6cZj76pAi2kfrlLEYozYvCj4Kpkqu8EzvlnneJU
6YIj2Fyho3lQBn9lH5Q7KCZ4AdQ/SzrOUtsJ8RhLgtJpuJ7u3A7hGd4X2L/e1DosQRwYdkzqbapb
OdvWwBEWAnesd3KE5NeOF2BOZ5tbAR86RgdnCBy7aBKWpU7bVz7vrU1C32PIr0e+Ra1KwjMgtpU9
fH/v7bnzcnW/O/c0WRQwjb9w/imjNzc7wdndQjXtKsAYSslvX0bGpIJOoUWgVF+iKsISL0pL0wBO
jKHLS24BYA5ZHA6oq2rdfpzd76xCrTixewea5V+ZInlXWPvpnBiyHVNyVmCm+uqC3VLUNwi8Gvvc
4cd9hMGpsvGwNaxYGWScQp781OSvgGyih6AGdtFpmXINqpCI7rj6oxyY8a26X/TIFv307VJFCdTw
uUrc6Ye+xdAUs5uGFDS4hVgSk2ahbJFhNSpC8kGEpWdowh6FiRgayXadlLjX/e2O+UDbNTWE0q6e
ThbmLJeEE37mvAroRZPjzyicbgRQcQt+UOcZd681QoCJyekAsSB4QDaiFU8wI5DENEp6Xx0U6H2V
R8pb3LgFTj+GC4Dg+NuD/quartaRAwHvTl4ZNCiLHPR5kDdqsTflVRblM7lNscmhWcvPtNT3LpBe
I57DOFdCdbXAPc5OXEyrfO8h54RmpO0uP4dYsheuLUBNHjvcJWWDRzzByGljVG/mPNQoZIJDAHWn
LG/ShWFjiOImLqJbrbXaJLTCbT4qlMuP8ypmWlxmyNPhna463nBUGyRxMieMHZYvheHR0aWZj0gt
n2kecaYEaV9xWWohtszE9OH0lJYbxcqagfw6k/53xo8zyRxtYcpeyR0pYPqCx5Q3gO0PonELNIh+
3mgh9wtITFIkTYJc+o0tOwhJ3Lmj8yLTWcRfNBrbjnK+zqfbHU2lvZvkeoWsbtOkdAPAPJclVB1n
tdWZD1j8E9AtOBLVOcmfVHP+mcPG2mdUxQVJwZFmhVN+fi7IEvafMqCbj3wF3lAfrR6w1H/1JJ6G
EsrKxHnOB139YCU5tACoWW7rMmRyOtg/jSw/Krl3cB0NzUedC1AuVdOin+Cjv/VSXZN9toO5VNkJ
LpnE+Fe7nM96CXI+KJyQYU/4636dkfv4e1G+PUfYxLPoaN4ZcGohzZOZi0X3HxhqY2LcBcxfAvH1
YaUJd4sWCcN6qcfORTy2B552BbBJv0DT9EFcPJA2gUm73X1KQNEvMQ2M1E1YSo4Jxq7T9XjHbL3D
5tJPAM9BSTHujyBRGqsp7GjYyAhqPWFdxi7iKOVA5hjgBMgGRSWOGEBJVPgbNc5wLaoIdGXrC4Be
tcSI4/do3dA/KgdYVCXG9XNB4VMgQlKSzsGjY/sBKN909iwX8BKJSK5HEqaSrBfdSqTxIGYuWidZ
D54oJgNpUaxEmEcVl695i7ZKnz/6G7v6GzprzmceNqMwZ2oTgjEO1+aW64Do/FNUt6LzvNCI3vUt
hX0PFXqHRhjKevnxa0/3cBbxKCqt7bknv2nbgQumLRz2RbEgct9tnKEaHjEDZTeV7UnC2xHoRM1s
wwmByf4JBUouho42DZv/BzfavCgkq/Kgb0OuIauqPMQRAkN36jVOEaNLxU4tduyVdkdpubPHT0ed
mfDcBbbpDt3MdUaY7H3G8AMXqORPco+O1AIXEav4jILKMMnx4oiEwlan7kf9R5bLrV8Ejv6byGgz
j3XXhR/8fZsUQex3y9x2GRaATCGofxUAGvTZfCHllZem35hSUNjBGQX7yxldDeXn6q3KZHSDOiCt
k3b7LxO4RFMp5G1bI3+lICbBMQsUsj0BZb6sBzvCT1mFaZXYafO4ghVapH61D7eQ9vvV+uBMFYgZ
5GUZTV7DQ9rnVWtsz0JfKYWh+X3aC8QsjdM8JBBOk+4uFcmZoQehbMdPj2u209GTa8efoTBhcvD7
qbEEIj9sn2pcUjff50nX7YiCtSt5/rUrE/SfolFtSe5hQRgSL8E2W2mbpsS9dN7kustW24z9PGj4
mP9+3uWm/moVsBNy8r7/R61mqp+YxzH7kgYCDrPY/qMJeM73KLI5lhWe4AXapfMoAnU5fJOJ86GN
riBKufWqKLW8DoYyK01jrwjJk6pgG2FtXhTrEETJxdFx3lT4sqTrlQ3MUBujn/eI8bohyQNDQ96S
8t6gSG4urfbq4VRfM92IshbLKGFgrYcsPpBa5J5aV/+pvAhQqWQ1Y044PnIiu5OmBCbVBF6wAQYX
ClWiqJNRjvqmUxunZWStPFKUIFYpVx2I1kdcjmnLd9yNa9DEWmScdfp3d16WfAdSbJcKWHcgpLIV
OunbLuM7ueSoiu2Uqew2NdEabwC+TND/QQsabz7L2dbMk+LkJv8Q9ZMxdZ88k2kGPr7vRcr5/tdH
NxmqgCBWYgrgL6srv57NFSmULwNTOutYzw4qdpH53qFADZRg3ntzSU5rv2xjfVQeNI83aN/Md+qW
Fi5MAR23U9aGniwnYwQ7/2xOCgnoukVAcELBeqK06Gv8sKT5vXVnYP3Wrx6brqZzYLwBTkw1qqIn
Tl//C19daOC0GNtkXa55sAVMZlZmRV/BmsGw6bYqzkuLW2giRVbdKg2lRy7i3uQHyc5XyTgSNVvB
Xlz8kzOxTrtIlGLaljaQ0IWaGfaC6Ao31G6nbA00dVxiEeo8i1UQHgPUUvl54MTJk/O5GqhUlo2Q
3EHL8VdWmL+aUTwrs7xSYW4t+YcbVXsHajd+7KSN5XyvwRG6j1EXu8kxRqWUM5KLAJN0Yd+h20vH
5pMyTAvWkDxU1LScBR+UP/v4z1wnYPMQgyUgv9VZK6WnTM2tIdFNc8MT3iygAmY2qPQk3LMx8dQk
BjKGPamdFeW8StPMyjrpCkPaVoWzKbwjDtQ3/J76d3DBnyGKytMGn5LoJ5xK89mL3DsjXVftxOaz
48c1nzDOqU+qz+xKNKAQtTuoR+WRwu/YCbQQZtJWgyIgtrQd3acnDJLMLIUoGjbq2PHPMCapieKR
UoAOfXkjTxI4Go61MQFI9op/EdzM2grsHuiwMQoK03SfL2d4YxGJGxR+3PV2RiGZ/Aji2rJAXvY0
y4flqcM0VL2590ZFCWtE+YEVEPbbh7+QCzeZPQ65UV2wh7QYi+dUz7TYz7F8xNthDbIrHPUXcpHb
Xg8b34VR3cGz57LKDdh1TnqdPcp1ztl1j0uLsBl45jL5P7JakT8nPYH86RgpHrabStrec5lWMIeW
/HlgCszbyns7yENdcv5nvQBeKXKdkw00HVpmqmhnRMcVn85T53BNcT0FtoBYBsBk8Ipxkuujnuk5
gCQ24R0ecMxxQoRsJ+ph9mvl9XT90Agvgh34LWAVIdVDv7OqIoNtBscak2FSnYvCbv8SOkChi2m8
zqRo3QunasZvd25J17wYqpkWCvKEIK1dqcVYxs15I9FpSm4WxrLTbDLb75DyT/N273VfYiXpzu6k
g4/HDF4yoK/gsSOyz93RSkFTRjL7FycyrcXWuiZrlbKv6nk87Q0+iKRNqY48O2J8TqpU2407H4bq
SbJQDkFQoA1rHr897OCw4pLAu9oglJrtvvx1EShHUJRLHz11NJGXt2k2H0vFosiEkGRb3694fUlT
BcvfHNV8IOpvvlsaPJVQk6eUT6wKxY/6brWSnTwklllQ6sk5WYgU7K/f9pa4wAOvZd8pNfbUm5YP
RciyYMBHp/R5mZN1KVV9Z3tDKblO4zY9WwT5Ttel31+ARWPnJWCPKLwzrFWWqxoLRHNdHmrPsAjJ
xMveAXIH/f6jpEG2/uroMU2yi+qzakKVMNLhu4gF5QDmDnobuTxHSHcupeNLKKwYB0/8GUBKtzFm
t5f2eonpeTmbLZXzfBLuMAQV6ywqkXiYwa5r6q1wi9516MUtDIrUj4TgNOsFeHKW3URYC2JPfktb
wy2ZbzEbVa01iVqmlTJz4oO8PmbRhbhwbG6CnzIg3dgve6u12zWX4ttDC+E9x+seR9co/wA2eNKp
9GJ3ez4BcSDU3Fveiaps9G1EX4COuEfCpjhd54S1D4L+U8QFAydvOBys1KY4gqgg7j5pMN6DhiFT
3nBFUMiCxDFs8nYLGnFn4SsRPrmrgaUrePFC9M26igdnRPGVA5TBOlIxsjaahTV+Ek7huaRRPVdO
AHIbwRQkgbYj+j/r62kPF0blEr0br55EREeA7pipunk7jV+k6Ulac12qKVt1sTbQ3dz+YqxOcs9K
SLTOuy0DFYOtn2pImbYcrUbtjw99rfMjfWVWC1e8qweL1X63gzzkDBq4ObKfGQTs5cawFFmIjwsB
fmiKq1e7NKmros7B7FRAOkPpDndbcT+ywJyWXjNQI4TmIqk6LNUIS9krYzKmNKT8U8KB8B4Wfq0o
e8uaR35/hqFa/PmUQFOGanOhYjrUu1NsS4WCXu0ThdyzYQDG0Ri8ElZEydpWJmXaZvvnJsbXB3gn
iqE5NHJAHJ04sGb0jWMfo+TUM4vMVcIW0LyWbiyEqVzs3oZQeUI3xMu/UoQSnI4BaFuanX44eYHd
6uAxNN+ZYu6GG6FE4NkJX857tLNuMoX6u5D0iEgrjM1aZKtKwlCbfI3yqwUTpCZLuRsD7RseBT1J
67/3dMZ4c4PwVhhsfXvlAWwbu6G704Z/QBy55PBDSNYFy7lnSgX9VElSUh11nZO2H8Ahg1aozpcn
z/HOgYH3zpHlbUpn2Auk6XJiZRehVYO8Pu3N1Gzl9oYY4MksChVQkLrklEWiO3+XKgku4A+ePR5G
xMUQZqQAOr9pSUSVWTYE0ULd6sJkUx0SpW9ndmEDSTX2gHbvjJRSqcEA19/cDsA2EoQrwl/DLhCB
MjASLIV1VD4CB+ZuqY8I1lI4lGFwkOQc8QriCoxCgA0HMwIFHAQ8k97bZbzuiHwS3MkJ5Xytr+GC
bVVwfZpc7TFHcPDT9AWpl2vk4Z5s3ghF+LeHXOWaSay/cPbVKOPZNsRlYI+h13G1iq14Sr9nUcTu
+iacqQ5nTOK4DFkeYZ5TTWjXEoCDP1wNFyJJBhJRd/9VyKpr1sEIfSozxG0crBpOCr3Ds41D6Y8f
OlvxiKzrfdEpH2WC7bOOc5p3FwwfyTdP8ZjQjqglGw3zwRo5Fg9VA8cwkzdR8AMgbfUCAG8imtUU
puGGLoIVeOG+dz86CgsIkrCASKckFjYolweOrfHEE6GZhgmL6LZdZDbMvslm7Ot7zZP7YjgGAmfL
JOTn6chzgRNcSkGKkV1nZka4w7z2a1g12+hfO49L/GkELzF3IS2gzIgOam1xrQWEnEKc8/0dZI2X
YXKu78eaH96iHTzUSo4EJLaT4xQz2W0Y2S0CAC6d0BiorhRktMpY0Zo1pzGxjL4VcuUq5p/5X+E0
IRoSWfGMeznEInStT5yjulTAOm/rynswiLXFSkyVXLtdZ6c4A2MzRWucNTbRafqtjaNUSoel9NVr
rpfkyMu+atlWpyCAdmWOR9FqwIBHrYUbuWAleLVS1kV5WrZgCm8vE+3F57S5mOeiPYLTbrZMW1wD
LYhkYOzD2GAFToPqZFcsuCyK9fk9jnBtvmQscy7HOlTbt0oZFBWQlSqaNUsLFRruc3r+PgZssJ7X
v3pBE3wcFoS5LahhbFVrNYIh3PcoY6WAPNkV6I/MYqhHEt20iiuevJXp/9NjT2hLr6My3yAB1M0s
Yt3GQ3Adicpr6nqMfsUXEkN6E9lXmj1C2chMESnlms5VT5Y/el1tm6WGfSf8Q+ZmTDvvTbddBIwH
aBprXrXMYWm/Cl2C6ZI9HYc43aAWS09+XsLh5Mvo05g8cVTw5LeA8PySB4dCg6yErP3MvcoJTmcc
uZPBxwJkq9YLdcfBZ09o8zU3edUwSGZsMw7Q74a7Xlf4DWO4+tDS0b0kANqc0u1WoIKhk/CeL7bk
NKur+orXx83YJXxCtiA1byjTA+L22Xw+9VRJ7xKKoq8M6TuSv7DKvU5no09mmE7pQfXses3R1u31
Y4XnPtKCR0YgloCXDvmb73u3S+Mo8OmNle7RMiaAyDJ8RTzLxCMv1KPe6bmLeF/hNKxOiklxL7ex
JQ9yjGKoXyatkGadI0myUqvZJeR7YDWAKJK8tpnAux4HvIEcAHvm0EYiew7HLo7eX8ju/ODOkiEZ
ck6AEQcJY3SUNiGaCE9MYDIvmOEqxBAviU0Enm3ADnJgvTyLy7L93mLJ4Zwm1/0089mdlLnQ3ZaA
D4bJWIZAsvrJNxgIKTpUzJDavAJ3VAAknXSbROHT8vyLbTjprLSGE0h06cg357MLtVFlrJq23qwx
wFYlkDAtNigXR+urC/K/KTMay/ad7jh3xR9YmCTCRs9LsUL0OvlgUZFqerCQHQTz9gEoWDU1qwrS
xeOjttg6dzfuCk6w4tzrqAdCh+f1J21S31ceLrvaeKGRV9K7N0wEgvp2GeOqTLcHLRQ/8zOMrmYg
P7asCGFXnO6FXl7uk79UTFmjCSnmkNOtTR55lfuB6vhW74SsTmeDjun2WB4++CIkPfzh0SRNkPBo
xZfLki34R2MmUg+aGbRZeWhZArbqDiaol/iveJWe6z97ahfScT5zLtNhb3LSMgeS3G8owxpFIrYa
OqQ8rbVmY8pFcT5MRKLwAbfzQAm5vz0V0YicpIo3EJNa/qUCSnZODcJaf6qL3mFRt7xobCnKmIii
dTKymi0VnR8FZoS1oOw6tm6ShSuf5XMYRbAmUiwsZPDql7PX7tpNxS5cyj7+S94Sk6ZxDl+iOy8L
7EYkn/C9HjxYk7DusiN3JEJ5ceBsmH7Po2UQOmJaYzp03VyKWSN6o9UvCd83f+gHhPC++ssd0kFa
YOjJ+cffZH31HR1upgwxfnezXD8uXYEwdFObXUy04p0dw20k0/NkHXBMZoy2n8jEUPinIL65BrrN
EJ+jhSSvW+0okRJfzgRVQJk4wOf2Ru0abSeqsEUDzQQuEBUhvPDbaJ5LZG57HpiOa2p9xwAknDmD
NLJI7z1O2X91kWFYKTnHkH2YemyCvAT/JThxomlEayU6LhS0m3SjJ8elo6rCgBvQy/wLt8MiSWaJ
mGBCy2SM2qv8spAXyzGEZEM+jIfJWoPRAC6IBdfc1aDgMOOABBgs89DQgwemOne2S6PBtlEmUzMO
hpwG13rDmvrDJXPn1VMGVTFc7+eddwHBPJ8RzBBR4SYS1pQxcUOe770gs8NU/TiN1pS+rrOARZPd
jS5mpbXUr5w4MNCKPLAcWunlwl3+ZnNfeZMOKkr+yNhdVut5WqARcYrugqHVT6qhlnHihWjKMU8X
XsuJdNtKR3bg/JVNyWBkQCbHl9qUGe/LDOqd31OHra7mzuDh7E2pe1uOQcnjt497DZPOrNf7qFzn
qWN2hdOhKOA8T4hMsDEr2Q4g5zrif3gqo2kGg7UYaxVW8HKup3CwmW+7xT/jtJc9lTFF4EVjA/4H
bfOq8uucXEP1pG+sCUrigIDnfQ00a10nLeVgG0qkVmMWeg/tBaqptlxXWujUAszl1yUOa3YrbAY3
07OcEiLD8ZgpiCjjpLVbTldVUeHXZkXz0i648H0jcrib661vUc49EHPcTHp5DkpV9KIn5SMUB880
2cP4CnUxZQIRnXNgXWYVI2iK4J+iTnnAGHyHsmk9+EAm65X4e05pYm2IfagRQ4uVKQZkbLfgBlqi
uMFxJ3faXwl/UuFv9oeYfRxbJXR+/T5qdcE03E98lOgG2dpM2hTK+doX7TKf+3j6phOdjODxsRPW
BMzqSdWavFZIATC42lVSFIjzP/LbEilgSvU5aPqr9qGlrr2ojAViddgXIsIAvrjSeBuK0jxNZ+W/
tDR5+JKkwq1j4yIx1bELyQmDMn7H/8OFylmJY++WTzxqghzLhCD22mZOPjS26Q+hTJKsmL9k7XtG
pPEOgsenyBWIrMC9+tRuUoo3+2GX79qWSbdccHOUeu1HgtqcZWV2C+Vo97MzAhod3DQwA9WYs8Wl
6ATgyxekNvlmLOB1CRMG7B6SfzTHpInzZvOb0Ya9lqtjqKLNymrBcuZjIuA+Mth+O47in+otu8uA
wPhsxfe5yBTtnxpqMv/KLdkbKmzfdjbSxxXKT70QUjJEXdsHGj1t6x4jZr1i1+jmHifuRfUllXX6
K2304Ej2CCixijGPbvm5VfU6yyoRRWF/LI9zG3eQTRK4HMRwd975iQSrOaC6wqEgY1AuzQGTZ0mt
R0VlQt0j9gEtzJq6g5QBfv9m2H7DGp7N6eIIDMXMgFEa7RDcQjWxvFmnPhTksLov68Tha8ZSxAKG
37eo5Ye1XcwRBpwObGpLuW2a+uUEb2bxGtDvsPiWZ7SRDfU1hI+boCJryk7Bb9aQk5xXoDFRo/rv
SBEErrCuO3Hi5ZQzOKGUbJf3OKnRSdhBvZTEDRempARKgl5/KsA/BlKbULgOurD5hR17P5RmudrD
56bv5iRW7QZIFaXtcs9FWO7iGLCxIp3h7PdWh5KmA8N6mBU1HwVRsxVLKkpCKdRhfABpqCcI5vA3
wC/p1Dijs1609AYHMQyV9jldkmXXD4sDpuLi0U7g5X/UNj4+7RGOwsF7EKjMe+aG40rva753v/OM
S8j/TEKRLBghK+1bHye+8PGfGkwulypoZzuniTaOzr6tkzrT8YcnVXj3tuZJjhsLntWK/h74XHqA
JUW9AopfcCKDi7Pri1zj/J1RB3AKv9lbhfRK6iS36iNnvG0vTjWT+uOyUsc5jDT6D5LYzlSyxRt4
2gQTzeODdUPCRtGneR6im+Y2eSEnK1bnDMyfGnFl/xtgoThlBRwFY+84g7nGHaKgrqQuGET4rVns
lU+sPyYAZb9f4RGkIx2gvUkW1YJ2RrIhVmOauaFXEIiwIJJfqig1SpMzfDCwk9BknjT9wcqNERcf
xQGnh8JiFnNsVxds7YX5LkUJjjjJoRPWalZu5QTL3+8hQeZQOxSigN0D0oHo46yY57HWlrN+EtrK
2EbSArrL9HQyk5Qa3bAcfWmhI5WsumNJ55/zADkh3lxUtVKeKwsnTcL8CKVY22gICL0XM4H+MMGr
jxwxQKLbwoHDyQ4giJ/JvPJN8uaE2Ux23+RNAgIifhmd06ipXwtZOK9InJYOZwth7jtzxSaOtNSZ
AzW/SVWJz82PFDdbO9GON+0rmbnNl/E+oA7SKVox0A3e/yToA0sn2St5Ks/p5BPAnVsXyDIQvrNM
Sdn3iJ7Jvsoa2bQ2b88Xke+R2ipaGp/iufVZn/k7HWRf64eb+YlaLaGweaTucBCHOo9rsId8Ps8y
3+Qtoq16eooLfVDxnNjghNHl15EeKVQves8A4CEWW39Bjq046I9XJryCEB57QUTHaVLCeisH4dlZ
18idm4EcdV8RdsBD9ZxlcC3fUICrWZhnaHldhrq85y2siWYs54Ct0KwFs1O/FQ8RZx1wx80f2xoq
QP1XGq2ECqtlXTXJtUdqV6ByrwGil/X7XV6aD0McyxAiGhAQt7s6NX9OUzPDI4liSByvvbrDyn/m
dx81wXjV0KpnHU8Z+dMKQMt4XRJjBUOPdgnN4F47mNBObZPmgzU8AWUs73rbGr/dXTdBNFa/gO/J
zzilY08CWxD6Tp008b/s9a/V9QhKfopTS9+pP4SAVgQ165s90rUHsn5zk4nEdRBKIMWQ7X3cQiIq
xlB1Vx9Ew21mjfK2/zFKIZBjnketWiuQX/T0KPu8qKvtt2ZIzMyMJrbBDZRv7S7NAw/3q41orMNy
m0OC2JKrNGNDNyV9Ph9Z5gj3ddGtfZCY9dfnSY5xyVRoIiBt2niW8Jidu7es4Azp9XWqd32ak45t
4vVCOvuZdBa4gfrXQ9KZE6fXprCavF2XC3cL+k62qtU+l5gODurf6iS7XG6dxSNF5FzyMZAos+VQ
Y4u2e716eK8yu8OuFvncNBsgnt2C5wL41ISlFNSNRYSdhX9X6VdCvyD0DnoyKKCLCNz1Z+qY3ag3
0WFFGCncYpSetrNpnaTGLIaMUL4uprJdzUxy/DG+zJKQIwozuic6XkE203/cEkGt6X3oA016/GNU
adwRYWT7uiXUJwTJrWeFgLkmI1RCvVz+IldtduTtYN/N9PM/O1JfYyjdNAsNmQrHyTQ0s/aH6ywR
TDCa7g67pgZsUyoz97MkXd+/lXtabZdrikBiYJthpHvC8x4tI9yKa1GHruot2rvYyCx7xxiay8KU
sWCZMOOnHCsAYnpTxkf2UoVZ0/yetPrHaw7qwse3IkVr6KHvM3JTaKqxTy2FQSx8Zt2/BlvNo+DL
IgAE7Thv6wlU6vl80X+LidIkOWbJIoPYuFZWbZg6Cg8jCu5CHu0VlUfpLp0bkNoHpt3NXUKqh5vc
k4akOocLqSFa825uOfPPV7BDg9QzJ59xCsT/7YMWZvnmgkL4KOm7mAhg+eX4nB7/nLhFY5pHiSdR
8Bwy9GFQ2sHTyZZtRBXIkaWynR7T43jPCNzvWFDh4ZT5LuqOXxR9x7yL/tA/6ylZ0m7rUvUdbZil
wk4zDW2wADbVuvd9ttV4e/MUBrvMsLQo52c66gFLMXVydd8wcFKF1hsOYJ5mC9NDh4AwVrxlPlfm
EsDeN+jO3NzKeJ8htfIEw9tfBczz2JFIZlE9+zimnRoZbtamkNVU5Wu3mT0s9y3XYKEATYAUddPf
hu55Hw7VzUQupPh/450co4LgqCpDHWtM9JdMf4wBBIhFnSb9e0eQfT8dRyDbu0BmtPyf9OT+T6U7
TRi3+VW7hEIrPLp8lGze0wo+9Thjolq1cZtbJ9Pp0In1S3yH+0lyr08+KfW4xP6qtqMAVTbzF4bt
OOQ22ib0ZsA3SXOPlh4DbdUdJ+LlOEbhOpngqYK6yhIYezq3z0VaGAS32puExLfzGiJHrl1AwJDl
pnu9WyFlEaYr+zLt8zMCNAEubnN4lr3Ct/HpKfEK0ma/8ibv69xyXLyUc866rtsU5dC9adKDeauI
KNPxsTd73UO+c076gCwnNHx7ANmF3fQBtPYvPf4d+Aw36aOUQpj3J2O6Z2jbQapYJFmLdPtoaRLp
6z/U64WoxZMSLoAnRecyE5BjcZRFqVjUk5ydZHZ3ZTQulsFDblKHgpsCSU9xwptslKFXWMoATrOn
vGVNgCgJnSXaQjAdGvnZpU9XI0X/G+Sc2eYEbeebw+bLYTnh8K1YldPEDGRss61FXtYnBTY4it3l
8Syr2tG+xb8+GyP9DN6oaCSU0tQLNf0gRHeDg3bp/aUNrf1CjMQUPyskkJnvHdPrf58c1FJCPTUg
QvDSlxN+opL1wMGWvGkkq5d4aF5gAzaeTAKofwJ7GSnHAxBgAGwokybqMYLJRdXyJwhvAU+lKWDs
07JzR7oBklmhyWRk+s7gREL71rIjgtxV/tJ3m91cQj4vGLePPVG0v9ZmwAuU20vGHmaGdm9fn3CZ
Bac4FLm3zGAizLv4ptk9EwAfOWhEhcRLbbClXJW1L2bCStSE6M6j9nsKOxWkcJ5PGU8PjpqKdL9c
hkb8uYdk306DhZvMUxYHQRs6IOkIu13qpjHmZe5NBDg+NzjprDFUCV01NIoQ9JtqyUpMPxG3Vwch
0RlreavC4RZSSFy6UIypqrTzJIFu4I1h/T9G+oqLaoL1WOdiJAdpjk9kOII0NhLv3hC9sTwAoxGj
nbFi/qDVNeRLCTuLs89X2uIU9T+ropSu5JTcbL+ut4+TlU+YUAH0qRRF3zyraHEd/dYZqobCly+2
qWvK/3hfnZIl/QKlOu5g5hyOQDgdM2nJ3SPNhL2snIZKHSFfHeWkKo4+ra5ar0frGq9MyoZ4Ej6K
hmGyyGBm8kC4UCDwdApCZCI2zuwojNz/uboM1tdGwoVj911PX5VW4M6Mv7mw6GLy20LUvXdzuoKa
uwWfdApglr0zZd1bWkaPvlpIKCfTB88KB2K+jjKX0H4OzRxa52uM6OQKdGnywg/utXZBMoYb4YAv
jQ4eWNQK1h2j6zFxCZvc8BrudBS5YTyvEqh48OOeMy0LtgfmUlJAnFr9gU41sKUSIZcWwxqobqGl
2HTHy5N2afj/f1wIu/QALTIqqs7fEqoWXBgS2gPXnfNsaqngaj5Tws37esGYC3fQY96qHJNCyMeo
Hrwbl+SCji4zs2/xblOVoj2cO9ynbrVt0KzXyEKfR3XxhB8MvOtj1GsO69RTv6m3Jcmm/5z2S+H/
+aJNf0wdpINtFVuXOienT70MBaqWqidKC3jgpa+rP8m/uT7JUC7UAJErEGBOYerhbNfSPpq1dXZN
aSD6e1e2pTOoEKkz6ULwUPhNGOzlYPSo2jDAcnR+hKCn/fvbNn5Z/NADvSnhVXqyaSYU6Ws7Lhnz
qBbk06cBy6x54nGnS3Z443lR8eKeTKNUFb3dRD4Wcxde7ymQORAEINd+442alfaekO9Yq+H0994g
FhDfO0MMIDI8I7pXu9HtsyXz68sxUg9YRT57fFkazU4dnQc1XmE56Zh3ZTnlYhSijHWEDsg7Vl50
dqLmdIuc094zGOyWuw2rY8iCnpMQQhu+alYJqewvuDmnhpTPn73LvzViMDg3ljl6yLaSqVg3j27K
ayYhIy3ZQyRfwElI/LYt3kEHW+3sXmdTjHNkVWsXGDVQtWMTZVjL2Yp4C53uxyp4MF8m0jNi7FW+
5kXs5CcqW9YoekIbiK60IygqB4l8ps7Bv7dyDvqTLJq3AoPl/ELH3DF+pB2V8EiVzsTnTr5Fme61
K5EKhh6neYG98FbUJTLvNezjisLplyOBbMDlx9f4bLXSuX11SydJVg+Gyl9VvcY8t1jVW8ZffP39
PWCY73OLskKB2cRESZ4XKz0nrNXBJyeWAX4nHE/3SjjZP+jsb4nopJAdfGdOtO+OO3Oxu4imWMQS
WeQewrTDqABL+hLdA373Py146d8EnvTk145H/e/8oAKwVVRN8g6bcF8dzxgUExpKkpkJPJEZmr+L
IxfP3NFMlxNTCNjwIxDx58j7e9sgFSmzGpUgfl0MmeHJ+wIjrhtB8jCeo2QhvpKfqFK9LhJioQj7
hsCt39KjRs7UdUi6yS3LwWViJmxAqROGoTKivcGgtDDaHJfpLezjXL3Eg9UCP3XZ5IbDv8+SnZBb
1lG5qcxj+AQxa7hlCwCvgrugL5Cxnp6oXd9m41JLlXg6zIIVki3LZb1MZ7kbI3dN8oBM1aUqXZhC
L4sKcPucsE5PJyiPN2P0S7Ls9CAZFRAa3O4M3RWO4BMST1977FZBL/u6SHNCvytkFPsLCjxuojt/
tJv3wX9QvW1jMgcMjceFT5yXoW8PDHA9h9DvT/80beN2dy3smDsolyJ2XpQnEslw/8v/7a8vNGt0
zFHDKPUzdRsEQrL1b9odQ+317+keBqLMTurhEKJ5rD1DgFhzJgqHzXYDXpqfXxSr3z7JrW8Qw5cO
Lbl6TgwySsfXIB2SDCp7zA4axLnrDxBRBLBMFWNqBlTxyZNPRKFQwDOX8aVBYpuTIWu+PR0B8a7C
+Wg65ZTvX3d7ynqG/O0ZKp5sCv4lqfzZHVBX6kefb5YIAK5sg640oYYuOvb1eLtHIPvIhYFmKM48
W4C1avAY3RyfA64s0r0KiqqBvQ11jl1fZ4LEmeLohbWgbE5BuaB/oCOGXnPUnkpd4wQcDwN+39gJ
/JxHmd5QIwND4z3lzg71ACtGwVOxkd/at1FX1/nNRWRJkysk/LSu/3g57cSF+bkp5Nd3OqtgAbxj
CjQJYgHLT8EbNzixa45zokb97G/F1NSBhrHKWHnVayi0ImUKxQEmwytSzvjdH/g8ktK+cP0LJE8m
ZTk8AuHOG+ioEceLvLDMdPUm8ywj3v1pd+1kD7xUx4is5CqoSu5rztRrBTOLzg7UrCckAh9lHDsR
eGR1X69jivLkFD3Ng/YpGPaknzsIzROkgYkzCL6aIcZY9EMvne9huNBDrXQfZ0qTxG48bYYm81ad
TF8+rdSTkQ96p32QyvqfSm+7djktCD6d3kyAvASyPRb+KkoU9nIlOXaWSXsoozc5g/Af+nrINgP+
m/wxM3AsZt7u/qbAYS3AezckjmCEttHycS7dZhBBKIOPGXgJxS7N1fnmxqR9qs2Js81jzcGqg5j6
antD59bDCv7Xe65ySqlGcG+Yv3nErKl1ylzbiXiu6cnl3Yvuz7q6Pr4h55Md6/mKIWd9Bv8ooUIe
d5Fkru+UAR5/Aa2u8qojYQWjQhifhvJCNrM/1ZMtPm+jF9d5OF6T+BgXl40kX2lvKt1nz1YhMU31
pJZsZzeLMnMQBlErxKxBw7ERcdLiTFDQOyAbG87IwvAiD0Vg5lOTc1gsuqAugjSugJ1hF2bNYYFV
irtfXnt4K+kF09h9/B7Q11yLZ4OB2j3p10ASnx5wjt7enD88mJ4IF/TFVEpqItaSTbdfftZgXdPX
Z8VqW+uVTeevcMO1dMcbZDnFtq/dVqprD3GuKmeK/FeU8XOYi9lIBtm76SpriPrv717FbAaAal8h
RfAGsBHHvez7igvErDe9JNZwX4SA15D9C4MVUVLS9++xNXk1ZSkMvBAxSeh951QivS5J3r9NDG6c
nIB3u2Sfv26NX8oliFqHq3KwCcHUfDTcfJp22U83nn0a3ODoflLXBC5nNshR9vzcXPlRtNgknMDE
rZ7b02iRWxLEPVeL7Y28u/t9PaHrH4teEfNZCilw67Lad4KMk31eJiNu8z53SkXYBbDZaqup3IUo
gq1CFk/zDPHE/X/gLniEoGJDPZhA0jY2zYFNSaigygwFgymZmruHA12bGWNfaXiCzgwY04fxDTmL
CBmZZl3YegXMnwHpvmaheSm/B4lLxkDLTJQnCHzkkeyki32uY8ClfPEBo8wLvPncUQMxTrQ+rUX9
7LS4h8HTn+20NOu38gTiNUsXMIl7uFLm/4p+PkChr344kwPIPtJ5RxQXZp/secE9YSfNejXJBBMi
0jzbIbS6gk4gfNeWUJoBuVmeD0JODmByPYBqX8olfSq6xaRsAG2vWygWaPu2WB3KA6wcRZBJcVaJ
iZq1PZA3UZWKnHC4xLlHS9hvy4PawITVBUh2UMQ9lbtg7ffTsDOQ9fLE3sRd7Q06Zf6zbtmfsKp7
Wx5Onmy6LieQwW7KVx6Zs79YfjC5ChFUNSSIdjEIXS/eUjZlEXClSkYQes2lB2+xixMoP7nya0DF
HXcd7sA+EBDKRJhuOZMBa6aeFFri3pxPeB7higvYbeGZ4ncPMDcyJxWmJ0FKkUfNoqjVH1n83/Kr
njw/KhXOK9XQtWjixSVqFmhF62fUxVkuu6CFi9pBgKGTyrSHVS2k6zFZr/m1o4FgN2jzEcW8XVgh
s1WeWnyGvCpWD/U4C8geY6X9ZXI4oEFgppAEZxdyvNqUq5NFWjxEhBjjZRTju4zA5NjSHTs0lyDN
1dd3PFx5YmpJJms572dha4OySMyh/IStUDfogIKrm3TlXreGtFyN9ZHnCo+Df6xB0LPU37tIi26x
gOL+Fz6IMON+QF02h1sA1p3th2pUhjXLL9Zfcfomtnj6BmUVZyX9qduPnWTim42MtsCvKlydFfH2
YkEgzl9cYRJoecdL+bujOljarBisqs3T+KgV0C9Gs0eCC7UTeAIhAN53h/cSwe4x9f0VuEg+FtDF
Qz8vY/qvpYwSAjwmwJSC7wi++Rg9clC77Na+sM757a5MNC6JVPfWqzSjJuGQ4gzSDPDlt56XIk4E
H1bL8ZZNHyM2i5izkCpIQ5+jWxKjhh5ojVRzpopji1G9KkWysqLa0eaUTKpWD4Wop0AKkIntRanY
OV44xobgkI+1EimktQm6PWTlxcN/zkwbKzU8WQp+AysfU30TJwvQNmTDDDj+xK8Wp1JaDFYNN6nn
RL97qINEW9bJzRbLMMvOlxu6NcDIILjB8GS80jC6fWoOr67JQS8ttbURvTQ69fb9gHvTHA1u0tfC
amxgm4JiSLOMw++lqXmCHTL9XDxpueAvHZoO/qXIlviCKmf3MmWcpBN3GEy010kwmexM5iQgIcvh
nsK+RhwFXlnNU6TEUc8FrmIDySZJVH5O/5YQ7/3vlfBrxoQWt1Lo9m/xOm8IC/DCtePryyuZa+My
0I/jQlisHWMP97PVU2IXH40fD3M1DP2DbljgTtVwYIoU3mcQfl6DWeGeLD+8bUeLQ2RXB1uCTMsa
/MD3UC9rWEv8++2R9d36qGQRhyNAgzkv5Dia+Tfgzy4AO6PUN/N4T7PnsAfjpHWcGz5b3m7awXjS
Po3QQEhaFl9al7+z4RzsIYELPDvKioj383DTH4FQJJKStoOiaEDnUKIbVHq98VybDmRsqZEnL6Tb
DR6/OzrT3yVdp8ymStWqx6kVJ1gsAEiHZ/Dlvrr+Oq7r1/sx7eRPU+OsjwfLUz8MMkk+/Lb5uNcG
uiSz6ZVRKx6XTEtiy32i5QZdkeRS1v32d9Q7HAHe+mVIES8QwB1OTUbso8OB7zn+1EuU3E5OSIIk
rGerRzHLntBMBDKyx/jemTJ6g3mllnDtW8gfnhiRTFe6HmJGTmuQxFs6sLsvOe0ZE45wJrh2PKYg
ulkgYtEvICkoHW0ConIjWKvskpRex+ozL+sg5vcvRajQ0LsnXUZFjhXR+jq/nfwW0THDtp5OADdc
rfAVLV/ylRYZZ3XCcEJV8W9dhsobYLNUdwaN1l+HgGU2zt/5BBnYdi92WK1F+FiD6mQFkBCklRx5
jgYXJW+i9IVTjCsaQzI05W9fEFtS9hRs9LVOOkeVYTDBekoWxmN2GNrTac91tFCDdIy/5EN099hF
kOukbkekdcOUb2ny/JbpCrBFWrXSk62Ynkt2rq/Xlb5257c6RBMK0vZUfwxEzfheqLzsyHmGfdKQ
nhm81sLl8JClL4iXiQ0UYMMDPZGuV82ABntyJnKp08joq5T1yip/pvJ1GnG4IrDF1z6BvvWhxXm1
PBFQioBz3LP9i1rgCE9aYsHPg1+MCRHwSz0vHWqSsXFq6Rcb8zL8Rqkhxk3WtZ03iEGILiIyEpp3
6prFGweq45LXAQaJrku7xzxdp++b4CNm75Aaya9IfxYRmK+a+dKuyKC7uY0I5OX3TH4njvqcCdZc
8D43otMWYSPzlpY9Qjv8XqgRNMc6r219xOHoSBMjjkw/Iqa7nbszhLa+ZDyOZJE+X97TO2FODIYp
9jBFL6zLgH26zTMWK6A7P5t/r3KyXXJDMEkTTjNRIM6xQkWOsef0ZO0DS6RJulSLR8ksfFJWfw26
iWpmhQJaW7KOPluAOKZQf0Z9I2kZRRzjaqBa5GmjFSfIwBB/3iKz1F3YTURjpk6eEECHoYjaPXbR
99ExbFqfiI8hga3f4EPhHGo5kJGhs4xFAu1R239NpQaKJ0sIfBFFfjTcId49KxSQkvdScUO+VE6F
CPW3GKwHqhfDLAbZxORKqwU/9acT4ngRh1ZI23ljaKuVWZAPRPQdmRTtJSKDwlYfcbMMUk0xq+qe
buOZpVUJ0J1mJbsxKTp1XdkBLgIvue6Joe4ussoyQt/E2fXImooLT/Tzdkbt+5HIvaxiZyOjb3Mi
Um55/J1XXslPWDbKg5l/h8vfOPdwAvcEWrB1+fTlNYkyyMfmKmEuucyIm4mXoLtd9RbcMw9UK9oJ
wiwt2woN1JcVLyc2ur7GiueJLa0iw7t0YBYCXTZlVGGQ+xutiRyHrDsGP0GDFDHIVzbFpwkP+7MD
Y6cuBzkL3KpJ6NIX+XisYODRJt9oNNWSJlWpVF28V90pa0pL1VhlN1/N7EMtt3+DFM4LX48N9PEt
R4KoLX2JX9ghtXr+tUj4SIstezQVDQhb1CobcRuiVmQHq4oByw1fvirxrbVESY6hh6lrioSXW4jS
kQKU3w1Nml/TmEIjemNdos8gxcXJL1orK74wu0PLdVt3mr+SDAdryswN/8HkqE/YerfYAZe8N2UP
fmEKjPXgXZki8Ah9ujvF1YGlDPcD5r8VDnmN5c+mh0vfBrZovr8458sIl+JYl/qHZ0vTdYUefLJe
Z3GwXR+HDq+fFQ0ENh+lX+lRahxHANn/r1B13+Sd+yrU9UX56MNMUPJFpuUWZ0qjCxuJQz0ckOLu
W/XwCK9XI4f6FjBUmEI2X0HBC+UetTRhyHhBE67R8umGS/cUQ8CstjdIC/A+8HveVru9uEAh38YF
pKRXadsLkK6zr8aNzjDF2A6Sd8gopcOiGyG/6i2OUGVNUlED6fsIx0z6nerQyqqEPe8jPuFmkZVH
9BHZ8Ixlr85vNwf2IT5/pe/f10xE4deLF8abwjVLnL72Ybc0oeqownDMnCoMQ8zDDDEEHxHCSsMv
LbyXPbv8qReDnicDHYG7GTCJLjims98fZ0ebkXeihSWvta0VLAhtitRmnpkL8Ic9NOnXEnX96HQ3
NvJ1L1jchQNMzom+tcOEzcT5yhBRvKM2u3tlVzFDJkICdk68M87zz6u2pujNwVp+yLh+bCkbeVCZ
cb55U0NNiXuBj4iJ5/gIEBW73ZKO6/VAIOhwZ+2zJVBIGbrE4RQNKYduecxqAI92x4QQcIU7zfEa
aRAgwT+AJOje+jo103P4UC/JlvbwP8fP6XyoZS9cxn1IHRDEgFqDjx6cwGVVLZlVWixaFpzAcfh4
Ixe2F5vOW5zdBj8H/mRkFKEu8LkFXsJ2IH0/HKM82VM1a3F2qYdDAoDI+JSlVIZHMg4+6ItYhaRU
brpHppZmHIVHBXC8WToh30pxjiU36gPhZJNwKJGt1sl/9qfHlCDvts+zAsGEdXm7ZFBWVsHQv/SP
OjsQ37ZUFBW4gPDuPLnGCP2shfqG1r8CKR6s2yJd56/+j2ErdWe1ZnYhjghutNDPyjuRhuK5NxSq
V7lbefCgHQlIEgm8HMOQUkb8cMmqaXwQqyCUdNR2M8mmcZSFuLaomtJ2YAixeSYRB7wH94N6itqQ
lkdeqLtiwSUUvD1O6SR5374H0SJDJKdLNe7ve8XLPnrPbPjYjrBj0UYX6KCHFPgisSO4yJwcuu4t
Zg7UZeRua2SaB5f3rH+U8Njo4UAK0xs6fhLXUr/SFp4z5puIpejF5VTpixPf7pC8izeyT447ngtc
rqx4+dZT0oQA1ARhdzlYVLtoXEktKdwrpwpqYV/bpRAiRrUD0asbZd4bHhoIWidvGEzd6p3l6g8Z
tvRxkpAmWYN67meR+7yTv6QJRGcz/e+17wDwfmQF6IFA8zdvBH4LQAOGoo6GRj/TTKIrql7KlePF
W2Xez2WMzLqMDpL7NS2cwQ3y0vgKul7Q4eKb+PfFppdCOFIumFg70Un5X4W7aLuaypISBDOxLOSc
+niWhnEJTWbmyNRR9cEMzJtnKLGO+soTMO8xT/v2KIqVEnttUkykvCkToCgPI539x7DcaH4u3z1t
5LBtKdbyxM2LdYmaabe2UBdguFONpDxYKHkuLH59vtrO+rmo/zVUm91hbYkpeKG88YZKWAKItY/v
Ztw884U0AqGZv41QyN2GBjj7jU4EKBiNnltdr28Dh4+++feYy2YmIck9cDMwXWilLyol0oDQ0RLi
n5EZ2UJPB4nOf+YdEMqmsNIN7F1UKYwnzqINvJvfsTD6a7Rj1/k72y5hvbh5wgLI4L7ysnbCdhvx
i7r6MJTMQmOCAwq/l2KbimOIhdmD7ghUMicEXoaL7CiNpzQQ4LUPIKxDLwTED/NYjTQOSxdy+qFk
SjhgfCHnnInitF8Ki2JyxvcZA0Z8GCM9RCfDOmHKvzx76LuUNyycV+dDB4jXcnvdnIn4kM8eeNB6
cJQHCIgNwjpa/NjCldpuWoGPxEzQNwSdtCe0C7yjPVDp1nFOqHs06j3q4hRwep0JsMtJ5di43+Ik
AYTqOCYYeLn9etuO7ExQ9xCnriBf/Mnss3yqMp6uGUdqkqfmY/HMUN2CsXgBWDLoarY+1aN+frXb
a8IDOLNr8G8AROz+XS2Wns28soc9VB6WTmnKYi+nhoh+miZ2WXt4yLa4zP4AQfdW0bRJlLT0Eo3+
pz6Bpa4qwHRiUZac6+aRefnSTjw5IGmKLF15uTtNljWzp11G8juQ+M8lnKQgTuawtpwou09xeY+a
6upsuJSrYtqbbRkUP/qmyT9TwJpV/nAI3D52oEB3e6HndxbTZ3w15tZgbup0hSiUmkLCRCYPIOqa
Sjzxs4L7J1u2xfkAByKRyz2leFRwvN1fHOB5Tvnw8WjcRKdqDm9sd6o7B8aQhrT7MmlrV9ofb6V0
2N1fgYHM/qbPp2dXcWx9/6wPXX8tTK7qvkabczMk/fxg42M5AP0RS0A21fWMufRvbx5zh56iC25A
EtaUYRoS4wv0K8uGKAbjEcH+TJ2a7f9jsqBQ3Ab/g/kJ0Iv2yr0T4kMwn2FCU43NdbkWQmDDPURF
ppQKy4S6EFOpQMAgSJ2kl78ipf8dX9BEkJzreI3KNXL8aKVbUPlc+R8QoMlDSSVkhPV2WRlWml/m
wvHCUp/Jrk6qTXaRaLky1iCpXdlDQLjHt3eawk+D6fl9CrepucKMtVzMGD9oj1ZIp3AmqLn4qFcy
MEIM4Cttd92uJQm8EIKDIhPwAUP0zZlV7NbQpIRu3MDzou7pmg+LYBsCsgGJORxJH/Kky8CCF3tO
LHbGDZl6mT7ClCRKf/9RI9TntSktvb4W06txeGmcg6ffE4IJrF0ENAuVpmwwX5faByvoU2F6sNK3
8TM+F278S1gLZl3FCeQ9vu9zE9lvBPH4qZnMu1t/F7mnTxkxX1JcV1uin5CIzUVKrRuW+MHiLCLA
ADcgEctuT3KT4rl8u4SWa3NIz9PNAL7KfYU8fWP9Vw04q7DBuLuErA1fcbszhpqMJMH8KW4vlXwE
8nCFs+izaedNpVjLdvmXeAe3T3WozXRbYJOONxrtz//NiAXM5xdr4otNw1WkCLscNsqJECzGi66k
oNbMbJrHHFLLoTEXQvhpvBrSTERMUAOAQZn9rAAGX7aN9Sh5IevwnpibtGwCd4xdMITOM4+KdblL
lNKCD5iTBeb0wdCd3scgKqMwu5fJXNTOyU4X/QdVx6I2el6iAAHjfDPdikPsNrAI6POPYQDfPX8U
WYE8R36fET+vY3Qm9jsOvPXkwKfahnpgcZizOz/gh85KTHeHdsfrdaxKbg31lZMsK9topSYua5A9
Dqd6smLpKoUfxsmvQGlyC7xKlLX/xqA/wIEMlAp0jSk1KlSUT/wZ3RJtV3n5RlijSAjtRhBxf3eS
vM3NX2SV2nhVNw6M8lXYvYyKVm0nyXosiW7PFWR92pDWN/8zMFXGorfaVNZH/hWB1JvsaSJbeV7D
iNCMYOMEwKH7Ph12vi8ZmubcYLJutmzRf3dLR0csGpCQ0H7ipVlbi4Dx8Aru5T/g90J39sbIOpNC
pvkBS4uuZh0ZFsvcGO4+nAmjJHGl1OVQuwVAAX/CFuosEWk5FNF28j8GDn3tYAUxHgR62jzobVFk
fhkKh944IuthCz2LtlyRNnrO7Y3PbG4ra8fYw7vm1/QiVg2XHyNy6mQU6lOaj5iIMs7Ic2orqYwj
7T2ZdRaSBg3faSIjp97XB9Kk0r9ZR433foel0RJw0PlotEWaAw8Qin1ML7atquVFvS5AG3c/bRDd
CLL1LKYONZJM521Zajms62InBDmyNYsd3M7n5P9c8FfcFuWobhcroXizhekl1Na4qjNsAygMu0Bl
Z2yBSnIxoChYb4gjbDL5hRsb1tmLJBEoO87eIofTTqFxeAwX7Oy1SFCwctqqhIt7g+ImekqnxKlo
G9W0qiD4irc5IAfjYyLAqVluGcgz9fFCTxGednfLZVkj5sHfWDMg+OIzGUDooICRY8jvycJ4nR57
C637ujJ7gR7dAALTZ1nE8m9qR70mxCmwDGzh42UkaAieiko0iATt9itxOFHkSxWBWMlCZzbFF3Kc
Azg6DFAm8wpzDj2wL/2MhtrvuUy/x+YmIDYZKwTZ5i0pt2f/q0goUMroI1ufdeb2kJul2/IA8nov
UyTm39daIwgCac5DZqIPNz06vTJBHWh7alUG+PWyCzym/OXuTewLL8BvOiUY2mnkVtGeLxiDwDpR
gqlihA33iHodeUGWIyMaq1x9LK8xltk3oYAGuoUWFNfZ856cRJtC4q6CaSLjSGFUNSx84lxopTK2
NpQU8fwjAOv4Kw5bUfGmARrPb/71EZQWkLomZu0V/+GnjzorX7iZnbwXZCfGqVwGGd0DYi6x1UI6
qXF1ot9YVuajjsQBELHf489K9odHIdnET7ZnEz4W5/euoJnFI2qT0P+dtGR5CiiD+OWcxmXdqBum
xMvVJ0SN4afJsktjRfXF8XtorP6t1oSv7qBAG5UzNFbnxI4s24JFy6UeOI7DVkU0WBEpUkzpZ9K2
dFWPtIqrpQKSJvRY90M0IZeou6rMXDxbdyvXw+Ng0lPWbq4mz2T7BGTzgg/emoOABB4iJnTc/2S4
Q/r9+yFJJMr6gCtKIDFE/LrCLe1Bkhc1dXSzjV5YrLdKH3dIiJpKktJhJAJ7WrwqWbkxvAgmO4i/
xglWakcgs38qZZEKXPTDHXdWOp5jicLBhikkl90dITkmhk5w1FL0aXBCfwacTeQt+VChp+lOIW65
z0O9E68/NdY91cJooel+wUHLIuLSosfU1xI2RmV18fLvsyyHyM4WMG809AWKUHPa8k5TgRg26Pn6
Iy3NMS8k5HdYJb68spyGgCt+T9lx9BU1up6yXfoy25ab3Uapn9l+3ZnU80DnrwO2WDtoBfHI7rrA
lGnNJO388wLrj64xqsCIFxCQQ+H1aJMCu3HIK+kyzQRmgfSZvs2MjmBb8UlxfkjziKTmwxYQA+7w
b/pfhulZFs9eSg/M3rnx4+xEu+Z4Q3Gi6DEajaig1xZGAjGP21mt5F+jn4VGHEL5ucJoPwcQxJEv
xfzUynzzDc2A3+n+19bGIHmF06ba7T/P5FFnFN6WPBbuG4b9BeegGjT9vqeR2YzVD3dZwWc7kMeL
ZA8yOAPCIHVoMVi6n7/PiOPuAcCOTaIqwOUsrZA+9/soayhCCKgm82AYmRBcAG5KZfg4fY9FGBjN
wHnXbDq3/0jVhnkX35DeSomDE3fprx27Hpiheqj8kcOKJkAQjCoXrrEQ8C3ZoRBmJWyudKv+f1yH
OTVf+5Z0AuQmnAT4ifO7gdy/F10BdfWwbxFAq50W2aVicSTwlPLrCD8anyt0JxZdGF8rtE2ROqTv
WTV8z2nigaH1zk2x9RgHsFiruIgMTJmI8gC0EFOlO4AZbnbmUuFOyiMVcMYRaweTeNyRDOLNP7B0
Cfmsest+SaZwHOIKfb77SCbpA5+VKA3lI0MPHnSOrEAcf0AolpSd99BB9mrZZUn0o0j/4qmRltbR
7nVehdaNTuEbn+hIM5xqWK+tind8N2LqiYOvgT9eFkqOGN3cMmeuPjBXqGQ577zDxWKXHY7Buvyu
KbrG5/yZnsAD3BLBXQg9DpkEC0lp25WysaMW8ne6LaC3mWZC6OFRvJf7+Mwx/rzUncaspp7RTNnO
x1wrO+JrX/T1/dUXdpX10OSnxAqyOf/SQVSLQkg1p9slho9UDmN+824u0RIpWbSUvMKyuxCx2veg
QUKVL9rp3de3M5XcadFoRc7SpetDCs5+8/VLmIXXuD16IGhseVKf90k3cb5kcQe5FgnbQAJM9inJ
z8kkNWGWPu1Y5uKb8KabpfewSyh20IMMBZLdpX3diggD5/RPuTkO58ZMZc5DsaTrZK+rVuiNE7Z+
CxNJKgWYlqWFK55YJ8EJMDmPzvy+5NipYdHcTb6kKK6teduiJClTM6DspWOXx4nJ8/eDhI+yfuvy
74SqmoHdVqVfOkEzwZA7GekgZIOFCfFH1N8uqvr8se10oTLoIqv+JlWUmw9KF0uikQeG3OrCj7wz
d+HM0QZ+h7LsCIYMyY+A5ZRLtfNazHZ52/DnZGvA46NwDmF9IJtiey1HEqSWqgACu0vbUSDW6vuq
XRn/LCgZYSMbIHtJ2kGO3tkTm3U5vxfeczS9oEARO3touZyYz6PstV69tQF43HZtt9HCVuM6ASmU
mYEt2kad3wYev3h4hdoL94/e31J+kD9Y/XKGLthSzQftQmGscFTGDtlvO8kjT52RmS+VUrY2KEG3
3SJyu7Lrkck9GP7Hm0Rk5zyiDnoxhlB2koGD5LUTczwUXFrxht0e/4FNSuZpLZfbeLulRRMPNRaE
X6IseBogbYAw1Uy+p3M4bE70MpXPtkgk7y0GVcNtIqxEbLPFRqoySxMQUMCLCy/RERpUhrNVoUPc
bCc10pVnjdgDOPll093dgLFkfxwulgcMI9JdqDras3UDMHYg064P9JLDxD9NNCykdlhcj3c8nsI5
GyaS1wOUDGho8f5UoRJuogH+wsdHM81t48bYR6z4NeZFFbTOeynaIX5Pa75J/4oMGn2ozUCoQT39
LTEB55V6pZb9c8zDhiliGCEZZa4KfRW+QR8M7wYCuv839fmATueAKVdEmo465CNssB/FbYYquuOU
ghaKL+sy1ciICveLtbEEO57aXD8ukNFiokplGYGb5YS+QsY1PLYTnOMVqxGbk2tsczfqGCgLXSq1
sKsVDBwyN2DFCPtbm0Sk1P40/QQw3Q2PtAAbe79jFGsiNPabitrajUnJBB/kdhv3bRepVrriyBXl
ZSfIXpSxRCzntrcLgJErTZp+LMDDgnrAXN/TPa5ZMV7Qjw92zQm9ZrPBFzxJh1tNk+vfAtRLemi6
L3RUvA0EI5EUuVJFwKW26rIYdy1S0d2TsunXXoEHP2+vIAsiECnfF8nW47QWtxtEQrVZ06VNCQ4i
VGtpXAHN0DMl4t+AqHLUQYomYop6xny0okllmAyMI69fo5Y4yy0aeFh8XOBT4pbp8gXGaO/yCMuC
4SBMmM5NKFyCArbILwY0KKtby7oG5QR5bYgrYD/QGy0bt03YkLOptCtm5wPF+AzmtWpYUdQ2lGh3
smMoTLdDsVr/yB1qvOXE34EqdylmfSElH2X9MoA/ZaAmu/hMH3WIHiaUXDfWg1DIahVBhtfaZNuE
0CjKLn0TQyceSsnC7UWLjZOd/LLrfsAewcpRUSp4Ei1LcWQDmeIw8bRI6vTIhYaQVunTY7vwelVv
FQzcMHNgbfUCDzNFRjNFx3dSxlPlhahqpl594ln57tBKs1akaqQWfGpjTZlr2SuUPSyc00DFCYMV
xC+I0kL5cLmNYcBEo8g3Kf22FKii7bKoDrYS/UfQDr5KvR0TyfAY8yR5sr6Wv6pfTG6RPGlvDKVd
W9Nl0sAr0jC16+h8/s0flKMMq3UI52oSHlCoJCeUpvy8tEQbvdGR558Z+1Dza0g3UxicEBEuUo3I
YCV01jJ/Ty75wC6RdeCD7gbMyTgzykQ+CxUP7CB26Xu65NY1zPKJmBI2bdzeZjeHd3XZqpnSIPoG
tcDjhVX51SXfhvp8bZbtiKiOQQvzbYVS3AZDsfk20MtQqSeA91uBsro12sKFQqG7+5Qz46xeT3yy
rss1iG/iDgCLWJCLTB+1M3jFRZ4qFKZoHQVwGnUoTPLzY+UVCQ/DpN9Luza8JDkI+FaM7Ws6Eed7
vdrsS0GIJGAQ2R3qUCEiTxErZ1NdYfdQQloW61Bo4MjiwPe9DmRC5l1cyab7iYGeqoAi8xzZ2CtY
I+Q/+lEwZsKK2dCL4/qsyQg8kgl+T29ET48ukScuSNfSsHcmLfwt211SFNj6+zy1PcS1jXho8Hoz
ljqTmxpuuRdxJlkv2bPyas9Qor4ja6YmLZ26aehReM4irjpoFduTzKQml2M1GwcLTmRpCsU9cPid
gyBN+7Yo/wlUxzVSb0qFLUpso7pEYpUiO49Cq+vadCrf4uxYBOQdt8haRJfuMWN8fLMCzKkgEA8W
DaUguN2ZokIJuiPzfzDED8RaN9lisROFrTQuUc68ci/ey64+sCfFTUFfKDUvjjtpagG00pFpTcYX
HM6D5DSywuI+/RGTFe3zq6CXxFYRVwj6Q0J4bXF+cyRUM11QYUBFMCBTnp7OG0KYKsaKdHlRGD0k
C/zclW/HVnuVgp6kdz7luSthAXvP4QzFlab0lPFVx81I2iWPbKSb+xj4SSGxTRYJc9iSihnD8uqP
+/TFKznngxDHEEC/xTg321g+WAMAf8+2E8I/+cNiVdpchnTnT1o/+mBfOqpjXcWSQMZa0ivfY018
JDqHJOQtE08Amx2FVWBYeKMuOsfIqZk21hyvrzgyDbFGiPGWwae+bGmAh0ML5ejM7ehvtZAL0g/P
3EawQNwCxcKi1O9y81dN/Lagaxh/TSnQoog8zuXVY70SRO23DRi8rwglWYV2t/qxd3nD/FCsteAb
FUaZ9JQ36u1sgixVkhZ8gLZdODMEoGolyRpQLeNfnj0v4S8oqaeZ3o4t2zz7Y8VWMfG01sqZJMXX
3gajDtVcnINAQWv39TW8aqxOx0U3r157yiQzgwKB2gM6kubgtYqlj9InbEy5dwfdiHXnnCvSeU/M
3nNcdVWddM6MUtiBXjiW/tmZ+uf/hTLl0kjjlWRWnG67JPPVYYrb37qnxvsHQ6/QLuU/Y6wCd0Iw
qPQVKPTkq0DtrPkSmZ5o21EJ5ZOnio/Svxixie2awfIr7uh4I8hnApGGCj/AC9xQm/08VSrswaCu
fykZAuXLbx8JaUNZeecV4xdWr1mRHOHJlIktjCx1StRZVy9i5+/7PICEtGo8ZD6wjtzX4PZbGarb
XU93y36gLeiBgls39zqFEBBsK9Ybq8JSjmenR9mRnS/hc/zSEDJaMsY9PDcYafA7Gw4vPRG79I80
H9D4bbFoaVhvdfNjiPkVSQMfKyEySTleNKgy+/7ixnGiZzWTKP+O6OK3t+kW9Mv2DW+7hLk+r0ep
V5xF67PxCPiXl5WnaxuJg9jiXa6KPANdiAMd+0xbmzPfTdx2jHpdCNaY49z6PN63DBJ+Tjp0jCRA
ODeaB/FeRIX4H5x5Wtm8UzGJQ5F/p5+hmT13vdH+MyqVLT1PfC+Sp3d2POeMk5VLDo5dAXHEN6bX
uNjTgE8LMba5BG5PSPjtWoJVfUSB1SJfig9/gJbEagnwG1lz9QU1078a6m4zqFOHwDCsjKkemesB
0ox5LkrxPrPQA/YKiOKnqiRuiUtgodFzAIsv2fQ+2ZjsYQElnWIiWgqKM1/aEAbjAi0YFYkgeERZ
W5kEJ/avJCRmMnIidr03dV03fhzAt+Evk/u5iv1WcQc3RZeztjMBfFMXKy1vkoAo/oyT3UhLpu8Y
mvOU/faJUDydZ0vWPh5xRQPV/gr08DwtKHjcgn2MAk+jJGilS1KBrXh0Mtlfksd4T5blF93pUn4F
oPxXEBs64YCvGMzfmN35YJpKkdVzZzLZwkXjVsa5dWdkkJb1tLmcZZCwwl2AVcNrRwtLpKviCtxK
SXu5NWqc9wxyRBC3h73Db1mLj/K1zjwUR63YE04dlHYY8nh9vfBKAUckrBvWGvQIq0gPiIeBurek
dXI5bmE/coj9SUoNDIdriTtYJfhrguUh2m6bXW6LAKO8A9FsJYI80bg4vqSAfi3aXBy85rnMh6P2
sQ0BAZt5AFtxQZWQoliKXA9MCOA9AeaYABD6aSRB1FKG7JLJa1sCcJ/8jcq3JtCr/gF6RV9RTNGh
8BlamuA2hicrl1knwJYVKD5h5khuBe3trVRpe17dGqvX4qqFOJ4tmJdDxhgUuu531O2mC5TW00Jv
zxLEt+qBKNyH/8bo1JY2/54U5I9HV9jytCE58Y38k2kcF/xC0qwcXZXxhgR1SGrhuGGnW9kSI9d1
HOhB3KmrkvRGPap3yHqPoptYAZn4ADyIs5I31Mlt82yGdZSPKUaCs85KSXZUlvyAAlFwJtkvJe7+
m4u5hAIyFneC4N5WcQyjIr0nZBXVP9jKfLX6GRtN/Y3XyFyaIoBiXU16hoQcZU5QUI5LL/SUK1kb
7j0nNQnZzoIj6ceGSoO95CpS1R7dwFkjUr5AKgTu4WaMF67rsyrB3GB8Zrsy3PAtSH8Jokw9f5rn
rbVT+CVsXDvtTMnUhyVyTpk+V80R8uCHaM9reBZQVVZcKKmBqPBMCFgsH2MFB5QiXOhLSIRaBp9A
e8nHSbZQ4QoLSjMScyvRRW7gJK33rofr4SNPsQK4k2SZ4oO/ETI4IM9R7HAdwGMHiG2bcoXo5O8a
G8y70pio0mmaBMLa+BcPRJFse8ZTTDp0e65/xzUHiSn71jlddRHxK3zVVnR7LCHTG8AL1vyMGsNI
aevud716JKlCAxr30D7/Vdyh6W1ksHVMCgI4dqMO3UTm98UesORiZu60aL4ckkCzXHWWMtsT7JuX
1RHu+tcNfCAhEAMVjd83P3AGqiadZ8QaYZ2D2LQWPLNPNofgT+EDDYbHhiArgJs1O0hVKbOkH0TB
/Vii0zBDY8gOK8rLim62esv6z0VIYK99T+fVfUP/Fo/o/QC30pJAFCFJKo6H0XimwZgihncKRdxC
sUq9F63LkTpUtzysLWy8CF9J7FWB0FdVIOBd5qLCg6trg5BNVl3qY8SQldUNq0B6r6F8kp+/ISN7
KEnQ3eR26ESSUPHsjUiXhDQGIbA53haYHHPFNDnTRyvB0tbVNrb+ifM1WO2DbyBFqxUuA/Wi9FYS
VjNXwxk3kvnvG2i780Lbu04Mwz36qXb9e2mFfZeChS15nyiDWXsziSN3idkbUDGPeCKW02DopZyX
VVLnZbqFbpXSjaW0G0MydRlxtE49ob9GOQm5/fe5cwbSKbCWX9NudjA6zatATBXo1vPIaQsUTJBl
N5z6JDZrvdQBmc/ZAn6wuEfggW5p4gmDdfpq0KTpaYE4UhanKyo6wzr38iJ5DE5hp8joxcjwZZfU
bnILDQq/+wSqEqSQ5yNuXmUJdioj9j8i/e2547QzLs7X6RwvKqDJ4sjDPpzrYyFiFGZEgEG0MOmN
s2FApwNJZv9+h7fKovoT19ckvHroWjnBnrt5Yjb9gRE1NG635WgkTWxNiobwsP1/9PjrPyJnXkaw
gRWm7TSbUVC+xg8ingcShSfOspU91HlJD1G78ALee6aGmmK/GGXMvV/6mT/QHl6kb3fibglEUJn9
i87XW+SIFJrEwLSbnMjwqem4VZ9fQBajD9ZshVWgTlVgR6C1hSPlZvgO8htSA+nXJuA3ia8S73cx
WjBzY+RnbrsRVupSOCjcYdVuS+1h/xC3tM6meaaz71U8rzlkYFXVQBdO7uIWm5vtmsAfBdURaM1O
9cAjm82BSuCIxHLn1Qdvp5qRNxxRdy7TGgWinuqkEvPvL+xkyDhvd0Wc6vDjx1ObByjOdBOuscNk
EfnBJ6qUQ03ekDPxsjfa+MwKOkmqoXt9/ynPhlILJNgUSKaaOC0yupSF0V9o7V3iOzj3tqAclGwS
A2g28hkNoHqTh0me2x4l3B2Lzl5q2jMkGHAbqwFy327ELpTc7G2d4/bloGRHarWPKuBB50cFAWTZ
c63UfxqCmgJp7ZUX00Tjf/YO4a2AOit3dpKHe926CPB69Sm7oIyZ9RFfZZm8krK8yKJHTT/VCxBZ
trn5wwn+Jd1CM5Wjyp21gwIdcZuP7eUDgS5b/DdEGKKrJ2tcu1W8iYfoID+tPcRGIoYyypHaxTA5
h+THoNnm5bJ6PkTULFpVq5yEEeRd+qz13BaqQ+YN7hoVQVip/dKNgI3C2NQBmCSr4vFH3FHX/Xet
5vmxSRP5UfR8fqh7P/LMYqZhBfEaRFiZdcn32P8ghO3L0RR9Cif0TnocA5BPbuBM8cbcO1Zqo2+x
naqwQVJ6TytO69dgU43Uo6vAAbfmVfz8ooWaL9mFxmOkZkCApLIPnfT5vFiQFgQ4GLpXE2o+Jr/q
4D+eGbQ/0hc4SDEmhpkqRxQl0MccwGeuHEKUhEzGObSJ7QOlmTbfXkod0JR1TH3SyMKmdzbkIOqC
JqwUTL9fZh/pDsGD55+7EccvPB76hki/Hu3+gRBlv9xRDgLVhOwyeC1AOt4AEADW5b9icXxjGcj/
QWM99Msl7lC58uyb95WAF5jQKHb5F6ppZI9dlHrTWgowqPgm9s1TDaav8ySaHJ2+CdZKC8gDq1b+
30n4YZpr8uruyY4IJkQ2yuMUUWtrRDfTgnshxtwdXA27ofJAAAWCn50LRdffnk0BfhEzLVKERAuM
HHg0qYY/xsu3MrFVDgGrXkbnLEpA0r6xZXKdNA/ZSz2nZWw5iFzdUMAqVpLbE1daPcZ7/RIOJ056
3SODo05d3Rzti6gSpDp69h1ZPECzMyfl7s9vbYp4O1ji3ZgDgopjg2zH5VMdcmP42apqBtf4Q2OP
36TiDvywGHoED6xRksBGnvIyXdQdIUmHoL62/ifOLXpuIReWdWYPbmCbbPPcxgxEenT1rlCABvXh
hRhRtT6/Fj3zAX36WBsjntG5jNevzDgzf1KGN8VrmKVzLZVy/6q5xnS/hpojjloAcWmbbiwUSY+p
1o6DRveQ2xVhaPeIcRsp9pvnyKYmbiP1SXMxVxLu3kKCP/ITR/QAoitCPlim8Fgj0e3W/Wke+W2u
ZCsbsPjWHujNgk7SPsEiJIutkNPGBEoDLN6ElRjhZvbIaGLgncIeFzYVFrsV4wSftFl/K782X0pN
Zbl0lbcOcrlCnzSvajSySfCrAMx/hlPMDpNDDBXtTpZi+bcrTpPAX3pwcNy/x4nnc0FzupGu81Z8
h8gUAb1wyO8PvGe3Ums1OwY0N1MfRRINv/OYLY2JXERiBjp2WTct/iRxUkiOzot5+jINf50DxDsR
OUXYUNwhO7kDSq4COsy2SabcNMvNekefBQWhEHNRho6vD1JeJdNekRzX0vZ1Ly92TDgsA3UB/BzT
ulxb0aZAH2c6kQAxpm63Nl6nnEolzJryRwEbtKlTN6YDLuzsF8ZLNoBbTaEIJpP6/qer8aYCFlZb
yd2HhXLRCUDD1RH9QebjXeU+T0HzhwUKgRp9aRY+R7huG7qklgPvmcVe10Q8wyYcpftFnBFROV3D
MO4yBlocVDwSe/nLVxwq7CMRoZJEMwiDw1x6ukL7rpRz+uHqZdDTSNHS2dJL7xfdjrVG5IdXstxh
LkUFoOvn7Uo8LlaXjMtTCeWCbE2M3x7zTbi/lRyfSOlREHjhiF2dYf/UPSY2E1ZHnYL1F9KlJb2R
8rCwG76S2dHIqccI/k7FJ58hHAVXw6qmQ85t7XRATDEZnuu1daiE/TE3fNN25hd3+LyPYvrAN6qV
3z8WclTAgQ2uvwcNn+h8KdFdVo20ZoeAkPxiL/6Hf72FT+pmfjN9VvVKnMhEmYcG6FbEofp1x1JR
ATwtlhQTzGEcSXw119f9/TATmU6k5omdLxslz+6vaEjzT9c3TImCHAYJ8nmGS7wJP75QNTnRWnS1
/oYKfGOWUyE8A0NR0ak99Ghp8o6rMavtVBJFT1773QS9B/zU8EGKR/tD9d937ePtLGaq8y+n8bBy
L0FiHf0K+NlSwazKje75dPGn9ppQcpRN2hemo1ElVqkLU8C1tkvmEiFWn91fEBObAiKkx1m1c6NZ
h9XUtLIx+8BU2nxBogZ2eDdF4hQiNYPzj7/PFaV5OUAS6Z10t1cRXsyJEg9v4LAsBAb2UbPpTVfy
f0pyCay4QT0GL2M6UaIWu7OAAmtNCHh2O2JCFQvzTWNrLNfQ6BR5gfpgjeShdgNAy4AqqyFh3b9Q
Ouv0RJfZdYz949RRGOVPHxtQtv0KfndZdA03lTeFK6/2U/UQUytjtteuwJtQqkrsDblW9msaXEBJ
u/K2h/OdguHuA4uAZoLyu3rASdzSuEjokxnNlHOe3BSDx4kXBz2q6iyTpRx0qBOkI+P2gqhIP4PP
wLUh0Q16TJMpytTP6M2hBOjifZTEHeZOX6r1fHHFlN/wA6ya4BklP9LBZCrHvU8vZhFMvLlxcZc1
l2p1CSAyVIIr1sNyKmPuek7OmWoGZPJNIdFHrPLVk6DsH51sQu9Eupr8Y7m292ESRWLg1jY4YuoO
wNRAl//v574MWR4g5C7gmjDrw4CigqwAG72VnSwM56XWQLKboNSyiiv7CdoSKS4d40oktV88mVMX
bBkJ9KA/KbhQEgr4tEwmULiyuXFt5Kl5BMiWedgWSNghcHaJR+M/kvyloG4/B86G49tkVRv7+n8/
IAmWwsYtzegqTp4b8DsIleOmn1CYUsGudXusnJl79VfvUe9HPhD3DwidNnDaf96TbER9Iafs6+6f
9LQ2cYpEsHV0dGoENk0oRolawwg7bNNh7MdgSE8hZ15uqw3xyu9gv1RBwwNZbjnr7IUgsQ9uehAx
htrFYJG5YtJtl8kcMi3D0Vd2nZTeqc/Ea2A+KNr1gOcFPG+89Vq5IV99RQuFe8GTyCT7b5iJm07T
ICZ7w+At0kZjapfgYTDE2PA8FfEbXCiq80M/DDSeF959aP3sgKhuzjVdYX/aN+G1Q6mJbtosLn2J
Fi/RyIDInZdyHxP4b48/0Pb9v/PZ3cReiqjATQeoUrbEm0HB7s0F/49vKxwdpPiAVeFGw/oJSRv+
Ew+NSteWJC6q/Mg8zFJHr2ZhAa5PF6zpWrCAFFNQrc1zNxIiYGghNmmey7XolE8TV6MqO3hiFMK1
zaY3/vrctjenZRHDGdVwUGOMM0gtvOheuPXluYlps4e9/CWCJxGJTDKZ5eSAA/lKsl1wQysG5cb4
P51uM9iydnalCUASlusCy74nJjKyi0MYwyZ0Q9V+O2RprfyVAPT4YTUgXqsVLQPlta44CgBiZvm6
aoOf4h+W9c/tmWzrwYCbX56o0kDuIa3VFKOd/2pGhICRRnuoVWVXnY1dL1Z5NyI3M3mewdyyhtT+
dCm8LAcm6wj80/Wrc5aVid6zY8lVaYIRBK1iyWGvhaOEAHLn0a96QlEq9pi3xeXgcxQsoDEJb8U8
Q/yhxCc5kXwtTC4MImp5NSLhuLITjnbMBwJu6YEXH90+lN6WsFcnb40gub1thC3WTFOR3buBW3tp
M99pokI1WBoZKwfr8B93OCWm683BSlml0WecEyaO1Nuh+X8LfNmjOGl06LDlahDdyAqgZEDzrDpn
T9Qn5g39Tww7NlG8t/YjTUAVs74WPlzUzBOrixXll7pthQ501EUCRGvGkCj3WUbGpK5Rwk6wqoJ6
PDrMAHpT+K72+T3Q+WuJk2wQLAaZFIKgvFDCmTwjtV+ujqZD3uCvrmX7fx/2FFBK5k8xZGo2v3Bz
o5phLa1srkQnIgchfr+Lt9FPIhU2nV5zFyLFzkDjkrv3791yf2ysp9WI0OntdIGSsDs/rJlfSn6o
8tUzMNPSfAQ+shZxIpqz72gAT+uvYnDrv+uiRSHnAhde9IFzvhpiV3osEjz8/ISQU6Wq3ckqeENM
dCffNDq1Glyslo7ja4wqTdYcF5/I73EMc5cJ9M+chvbkrw0pTX2f7oIxJ5WEO1loyhBXHEX8sluV
pLP3kp2yqyPDPq3YwpZ9KAB5xtR/Vi+daSP3ZCbt3dLXQ4qUb2xRG2AOlkIahrRPq/44xn+aQm9d
sKM8Ewq6k6dUgh7CLSeSdb0f7/X3T/nQTNyrV7KUX8OanEchoVO/ycHBFIQcEDNEyUapVdMwml0T
KN3Frw6GG1rRqp8wF4hKAkuepTtYer+hd7qL7o9QfoSRJ2x9qt4vGoj56q95kV9PkCGkx1WPM8qZ
G7/mXaT9lxtbQI7wkC4KDrBjeu/szv8uahYQ8rEWYk59ShXjnCgf0/qbqjRDnZKUBZjTyPCoQf7q
mOL06MqewVNLI2y2TxgLl+eKT/03VLm6ER7ICLq1PDPMvUS6VMrtYI8Hss1mUsQ4FSpd+tvgiVeo
WiqPiG9QOxGk3g72hJnmXb/bA0rr5Tk8j3lrItLrUeKCP4tI7ZsRpmRPCNXEpwYz3nVIexfIksS9
2Oj3XXdXGTuROayhNa6dAsyLhHMZhMfD2m459ptkcdB0gvfRc9fB379bkPHdIRGvhLVP/QTkJGMn
ovB1el2OdsneuPl9/3R0mF8KJ9GwbmIBS1PLzK6sCZq5S9uGJYSvQjl4FWUm/7cpzrknGNIxx1Hn
V6UfJTYguwZ/U80GjOTtjhp9AN2WQbaxsWbYIHQnqCUAoe/JZ9PCipscUeYzUXBDeTAa1lW4Zid6
WMqCctarPZbQTz9KXom3bTJt/DRniaybJ5WjS8dZ+4xYB9eiUJsHnoXbRM8Dn/1Is9ZNe8eu2Re2
g5tq4Gl7N5CcHRqYRmoXKXlVM3MQlxAfI8Z7xybO07Jm8M2Jc/6o7L4JlsaPv7g+3utw7kthwSoG
2+Zjw5IDaqprVj39wwlT/fHsHTQ3wXEPf9KCvTs9GP6y2f8vXeI7rE6GpQZGNs5gMOYEnYlCzlMw
N+ygVvU39YPlljltjS2fQLPApYo6mZSkUTik4zTEu0PdRBAouSW0Gaz3+AVN5nTd0vhE7bw7m6Bl
CbpWCwZV3NJ9PImeQ1G7RZFEe582q9vu5qJucwcZjluWl6sNUxFas09NEYpK09ZKsvAw5T5u1gtp
m98IMkCfgtw0YGv/UGgLk+mGlCDsH2Dmpt5d+RZ0fWOwS0U6YLqDFeP7/vfSedksH2SyhEXXEAW4
cWm0jsh7l/CRtno03CbpOiuvM3TQXU577M0LMtOGRnhksxldt2aXiFxkMbAxlrwVcrfFaXciCiXf
kWQmvTcQ+HuKfCxc8kTNJiHhF4nnZ67eOYkH0BM1tr0q/kx0VamCZ+UqetjM5tQ1IpGHFdgUsoh5
k0f+iMFbT3iG2eGgIhUCPKR8nW29hYkmrzkruWmpQQYg2GsXNDPoI1/SpqlWywB+DKC0BOHdeBVB
2U6cfjsxrpAbkdl7PTWyHCE0f1PTZ4/+VdBfMvTeAvJBqBNw+7YZOXzvpo94dkyUfewoc/xDZXW7
JKAZoVU0pxoFSJL5nfPb5g0kZMSize6orfRoLxvxidfntD4qet17lQ22GGSmBALJx6L9o/Q3WGIM
RhrIAz8lFwJjLmCHQaUEeWq6YV7Nvmwvqn5Z0zlFL1+fU3mNK/xULx1p5gBDFFA5GN6y2Q9SzCIV
NlIK8+jCLg+c1nchf0jjuTPTVPf6itmZkVX4q/W4l4N8ZNz/huAHvYrSCIQZdfZxfOYQyfMbwwxl
KEFMZxGLRua+BLIDcTGi2/Ob5ne4XkqNCnoVCTTe8XOTaQfuVEGQXGsAPNDrY/F/YQyNxILi3llG
ugLorNsmGepIZbLDymvNPLAmv8LPWZEvOcDVskClR4VfmnnmdY91RC5sUsNRMHlFlpZo7MCbRfTT
YSpszkeTbtVVWAB9NUng/oSrstAiFsUfWISQwm+5MsglU1oId1n+6CWSCrChoPOiAivHJCqPFtBv
qnfORYR9OvkpLswuz/ypDuunoA5J++YNvEhk/+OwFVO4uP41RokvjuypJmwi3W4ZBmvGjj+Ji46q
KqPr8CS1SOknRLILKO0QXm7wSxYlY4na8o9jXRj05O6pVzh6Syx1wEOA4R1z5ZX2JVZ4mjdn4z6r
vTenajhOVzAony/k0x+L4KdDbksSj7F1Nfj/69YZVhFfa+aerDEI7c6AxoRpnj82CUJbCAjJ3jTL
s9+fLCMZGJ+hgnsupKYUCR8NG0gWJqAZcdOhEtPLz3pvKczIJpDfNqIl59G1xWBtpbvsxhYtOcTF
w73fqnHRM+riVqgmUqPflATpQ7B5+HGkO7hq39ZDH6dCEUqyeJrgmE8Cd8t8aJqn75yn6x7fENtw
ZVM8gJEqicpnv+Z42lu+XLHfdJOetBZ9oZCXZoloJ9PztgSgBsaKlwqLrAZaAE/tx0ilZwvGPhZh
k/4t4MnTw9Ov7z5sODCAJkKQwAQU66qBZUbuWqi7ESuw8UpTbwpfCuwhR0JsyGGCy6dsvwzBxYnM
0ucA+/8atzAxMhoplxLlRn6AWc4DtoJMG2Uz1uVepLBP+Jfy87wToDlIqMhjslrDYz5EBvJkF57L
fHIfOVFU7EvK1PoT984fWgECc1OGQXHUeRvVZq16G6oFErXZ/viC2ZWupXnCEB3RoIHazYj5HPiz
qiGjE/9gKcRJth0qZO1MqUdEkJxmw7KWAegi23mPDi27RU0jggX7pxOUm2n0zZxx9WTEKWOs7DHZ
+PMaJfG5Wv2BglpqOepv7QX7NXN4diGFYDlgwnRNpPMO+SADrlLtua+PtGGl1b0paEvqoqW67MET
fiFyOUgzyk2FgfS0lVzjUcdJuIZl7w6e8W4Ac1Rj43SHVnJMgscJPmkVpNmBshXS8nnVcQuNs8S2
jrTcGFl4NvPEBzz6UJIQfEXvMb5XVXjjACIOBUubVDNMzNcDY3Zb0UMfqCNttLR7bnpW8/BrxIre
y+5bkY/FImGIY+Dc80U014rnkXcQK3E2Ivhh/G36k/rvyRuCEm75GXzdR9LsjDuYrtn//EG/8afX
rPOcUwKC1sE/w0v5QlHK0q65O7kHpTSRDFioILcG08Q9VmFK8cKhVclxI4y0Ut1Gesdm5TZz9hnp
rXWSrM7TPrye0i5HhrInn1cc6kcvAX1IKcW5ZUghPVS2aE6f5yTrrumeYe++Zts0bskYuRrC2D2e
GKZUIipkE+PUTRE1Gs8B1S9WEwfHUtHLZI8IXXVvKrDt6w5D/6/pkYKYbgNJsJcwN11LhZVIxYbm
N+ttEjFlZB9tTtCa70aFhLF8xWvTJM/WWcTZVr4zg/0QuA36dzoRK9tfXEIxlwkz5Y96wtCkXPHs
kcTBbIaX/WNjBFfDp6h6OmmyrggL0q6+3tf+yc/OlV0bEhPMOHmNOg64sR3Rw7feeAci8M5bWekX
DlIBCkpX3qZ1Ae7t0t5MeAY1TYv39+44yuJgWPZUgYAKMhqnLTwHqDh0HOG9P5NvIChWW3fTZRTb
oW7isXjbxhfjKI0gHj+7wizsGQBcc7VSg4sOSvw2WJLdkCIedHzyZ6Kb+wJbvv5VaF6Lvs7aOk9t
Uy7ojBG/Cjy7ESmL5QMe2tvU7DAJbZrgHK8domoHI/bf5OmFZsdfguCu/NqcPfnYnjh7mDBcq8wO
2z74lh2j3Q9BcIlL6z+Kg0OtxoqT9jLLfjWTgtInTE9U4tt1OT4K/xGQYqIz8djwH6GRx+/EqkoV
9dPdZg82Zi7xCP+glasQH0A+WgFa0hvkJqw8yCkK22e/tohmriJhWxYycdwlC1OyruJWBszsdeRl
EV2sqW+o25YFHUe//hR71BZnRCWThgl7FcsN8qG+SeJC/RFEt0p6ItWgKzm96+fw7GxcmAQgvpn9
CsxE8RSbTkVDKgRHfBqFejgavWtIu1HHSz01wrQfB4V8KXAR5piZ6Mn8vSAzzn83aiz6BYeN7J2M
684og8IDZDpg1Inw6KiVte6TTmLVk1b6fgg0kbGFwGswO7CLJ9VGflKhH6qm2JncU8sqBai9qIap
bfWQZqDx1/3RMHDX3nJK3w2DSsHMbz/kDX0GhP6nUQKpMjsNc+bpXnJf151T+cD1B7Vi+N9le449
ich9OpaeTeO6lSLXLgTcIb5pItGigHzZ9NYqHMo9oSPKxyKsumACxf7tA3QrlYwFYNblkG6Lqm8q
UYH//7F4o10bWKdByYTnYpvYWlA5ixu0OCz2cbynOvm5zH8J6rQJeoVggephvneM6a8gGPlVqTn4
K5pJsLSzdebfFzKLtTu/Uig5IjzMWrpeLI8aR7M2OIAfuMjOq/PaQxctdKrWCUezXFJ7eNUAsAto
APNeU6EIPzCet30oVapCH0Q0lJUZWy+5U90yHS0MskLMRRbxoIaGkDHRWtLL5pVISdrBh2cac75A
1QpNE1yaUNS1SGh8UDMqNNId/cNgVaxVFOXEiRGULnbOqtUVjpn32dISZet3Y17F2fh0f2QDXKIg
OInXyluGRJ9nFD3FmwwZTt0Gv+NXlSQsh7e/mkx24TGZSbjD7xEUygB4n1u9T2adx0eBzbyR3ZNH
7EW15XqEHF5UfvmqGYwAZGxOINZqnCeMGKkhy8eIWEw/Ju2QF3RhW+FpXXWzOI/nSa1E0cHM/1Kq
wWQ1epXb/PZkq2JPK+sf21MKJUqKpWYNFvyEn2zK5V+15ePe77r/KWDVkhqT0+QgVeblG3n2ATDb
BZXU0PMvTsRQMSye12rVDEq+ZrbrxiV+CmGdBEt2SJLQ1Q1HFP0fYfKC7n5i0m+xE5Bipc07iDnm
I/oAjVljMp/i4e9aHMKNCcbUMhyDW1KNGIT7jHNrjlBZqtjGGrE3cAOK/fRW+/GR4KFSfkFOIEAq
iKspSRwYFihSjXrq1x1C2XA9oMkhgEk9hWgWFkIGu6yHbrE+BRO/5pFBSlxEFNrJ9uLcgeLcCyO4
sKlesfW0woMBGx8mbssNxw4gbkdknbuIMq/lTia18h1ATNP0kD2H7z5YLGUqzU8UtwlG6kKDgoRo
THZ1EFNy/gI5KvCzjiUsNfiCFRn611BztWHsxZKIPgeQ47FkcVsF7rLAEXVUjKVVqIHFENvtzZfT
e8pUHxXu/THdcIntFxWUTCTZSjd2xNMF90aAMW8KDXNo8AHcqx+ue8ycneVZyJg4P7rhTacRdQPH
hExsxXs1puoTlamn4jn4YKXAaxhKPgEd9uKNljleQzkxJF0gYx3OycRmlpKbd2DrqRplt0Q6UN54
Lxa8Pl1CRqxJJZ3U+HO+J3rOUcbbH4ViZf6XzFwoB7MSecmVjY5CHNp7rxPP3MoaHQbCLvSLvyku
HxgcKpxtY5IOnJGXC+d70BpSeAiz+tdOjp8JFFVqkwy1h85c9pJGOJLdO1vxqQZXrqoJgAhq3j+C
d5fNxDuCRsA96Wp2nGiXT9YoLN2wf+vsEXuFKz/rnW1oByLN9+c20bXcCFaLbyxIKITUd5WNL4I8
4Fr5SiuS+Ci7xvx7WuEgKmqNdSMhYRDfGmx2cqZHuW3esf7LIA7HLl8pOHaSNUn99e9DBjwL/H7m
WHhxCGpeGzULGrOp2tpTpYAGDGOFspkram64joJWiggdi7BP4jnEecrNb6Ugsvv1bTq7riZISqVb
Lt2nokBHIupZruXVrtmOKIfGBEDSL2FcyagxVzeZq0foNrnj5kJvFWJqOli49TAT6WThscL+7XXs
oJZlVRvR6Js43GNKQYHU6pEGtEUPNPBx6leroPYP0QYg3xU52+c9V6f3emnVeFCh6xrBbduxylVb
U1oLw9JmXdVrHlyOhJ1OsMt3em74O5qOxhzrwsZBx3c9cNHbvRPjpPgt5nFG7mJnBbTLFLfkzton
CVfo8ERxH/fXISD6d/UQBeqxKFS+fp5NuJQS6vaBJpK7ho/oR3/zBhVElQ5Te9oEe8oyVyzWENw1
/9wUapwxi5f+6zi6pRdEgvNucX3uZ18mUbwyl4PfTusqhwO6g+cOiwoANV7cdmS0aiyBZQV7yiLJ
+ew2Dp3sgq7XeMr1Pzy9RcPkcDC418eDzZXCipzE7cCjqrrXyMWt2aGlC+5ONfUJSgglNjzJIGef
H9ENGDS1Dffqbl7j8FI+n5cPg/MUHi/gM1DuGnFPxt68KFyAE8ME94IrVcebf6uKtzc+o/5WwT9V
vCi/EeDkgsb8bl4EvTbqzbfVqaUMB3XEj/cZlM9Y8/SJ+OOUATszy3JZfSEecLQK67dOWaFj17uY
RWgApY1YMqWbzLN8odX+jCsNcGJsWyuOIOUcr3hgRaaFaW4Sg4bvSOXeqlejfS5Vu+Ju0YLzvWnI
Wvlqt047uP0zsA7iDXkxJQPiRXlKgwt/VKjYlDaJAwdHyxzTjhzsBD4Cmk19E/QGBMcKvUzrxymz
YYAwPaUC2iAwjLT8e4rX/rcdn0EkYbXzw8e21hPuVvDop9dPc8Ebr7xG67f19PwQYXNXOFFE3TNY
UY9v0INQ5LBWTUT05dRgljzlRvZi0bAjuThqfZ5MHGRyBuExMvrEK+bO+SfV/okWK5rM0/a1DlKo
UeDPDnWxHvjF2gKrd8fJ5MCjc23iREK148ZBmh2NU590nAWbetoCWrWj802EN5syiG53AnTbfeLA
4gxi5r22F87byvY9dJWTmPGieAdZDXcOuKJTpTHRBn4jP6oF3XkL/qLlTLbHrWuXOI0/8+ILk8r3
P2xb6JsBvluoLcC32mehOuIeotqQG9HguAlQpCA5thi6BmIeFQquQBkGvQpGYJdLHGOL6OeGLV5n
/xDvMzrrDoToureaP9cZYJpXAu0ba98MBCnL0ou8tQeTHfUU7Iu9Tp0VgPffG+Y2NZ+ixWpyZicG
JvdYZHL5AyrdCa5nlim6kW4S7qPMB4N5BPXHXWw8ls21h8+VU9VfsqjuD3hfWyG+rQOUisO8OqHI
/8LzAB/tUJjqPLaO96GoF9RyIxlRaPGSDQuv4vtWJ5TnTdc1NOolzGF/OCms8Fa21Y4YLlmZhVp8
c1besCXSq1Rh2+pdHILQLYwGam66+MVCx5VuqRQxOnSoaIh5e2aE+H2riXyra8E3j1Ai2RATEeTD
nl8PpsinikdHEGB2t7IWdNPBnbQgxmcCCdeMawrDJZB+fQBko21CTC0RiROoai0pTqp70M/fnLdl
Lvr+BLlx0Nm/7hqKdaaCEbgQWpK9KFLyuhqV0mcbpWIQ1tLw085iP5bH0jSh+56PJtakpuLWJctU
aVs4CkL5P63fU7ldakmDAMrqY560i6f8U6xESjxgKM+lfHTxMUpIQzCWpgO+DrCgvm4le4h6FEV7
jWbqEhL6PS6TDvapZG3Zci9SocAlGCruVmiGGBNRUt3Abgy+jYFkfBOa3SMuMgEl49esBSd8cI69
HtGwg84+fklnH2RI3yHUlnJUV5p8QffFMCPxIJhmUEwmutYkf1E4mpzqsxqik0b7tDOcwpt31iW0
P+z8zkREI3sg1WMPsy5IfJh2v/2LdDF9niHTcUsqnBcQZ7wv8Mvd6Plsj867UUvTMpE9tbbv+b2c
QWEB7ESXr5fwMqg9T2AbuFu7K9GPwhYojOSHDqp/pK4CzPD4H5LurK9OuXr+OQm17TzymVZu6nhX
yBozIWFt9tIg7BnvzI8xfohyDTAnFbFS+35kc33h76aEay7xMF/ZvyXRpV7v6U0Ba5l6b+9U+zcz
09TT6cGWJIP1Z9nps2TsCef08ZQM8cn4b6QF3SexyfjQOTtRtzrIdoaU2butlEeVHUIt1nQVCxRq
cnli9IvYIQuneKrTSAQmRDe0djSrkNq3zHyY5hNlYCOPUL12chmAtR7m83B+zxas45zLt1zPhdIb
9ttN+Ws7qzXbTPs/cdBVz/VBf3LO3EILMQ/wUf/k2cClvvMPkIEqSZkrMz6w9cwg2ArsiYnJuoxR
JMZLXVKAygE9YcqPGKpP/qOGJvRnX/Jbz4pKHCvXbQDdrHEPa9oTjzIqqqOiUea00ghv3hFa+bVb
cuJfwuON/DRStJGnL0PzN0fD++rA1MmdZmabAcXuRSBtoJTvQcr3U/3fqE7v0AUC70tZ+utGe1k1
q2aVWk98PvG2KaR8A2CnUA31XokIiElEzsMyyxg5/X9LuEjrE9YIoOhIXHDZ/R4gJyxgfC/BpQka
SEUHzhiDPbY97vm7htLKCl8Nb39TFYCnVbUmhVLcI9plpjk5mm/i22QmKc5Mc5duXIcE9xJcLTlv
S0fFMzM3WITjQ17DOGyoq0ehg6ane5rAfpG7KVpbHeyQU0u4tsuCfvteLuzIRl5//OS4P6bB5wUZ
YAUO8PlOuHm909N8CXqKCNF6/VxIeRU4NHe4u3pd7RvfEIsH3trpghb4Olrspq6CXS67vIrAPjSu
j//sntFlGOFCdUNGmWTEeDuuSdif6+YWuynejX8mQUlmAJStZgipAs/Vac5P6g9Cyb3vSVATwU8j
6vnKJDMxnwxXKxSu225vH3ThNycrHOzoAl2oQniiUKpPuiqxiAw5hGgUgh312Vn9LXFTS+u5DqxU
Ihw7PFzscP4wZNqpuUcZJhlDFTEQ7qIr3JSjY6sApvWOh5oMOB3E0fFM+AecGvYUmNlJOPNeLFEe
GO1GeywS2HkrryEm3yrieIOwhzrRn6Nl4K5RPk47ZLM3zuzgwYjiXEx9KgIqidi/yXQUSZm2vs2h
BL99m89HJU0BP3+GkIPtwj6ho1xlHe++yt/8oUhp8Vx/D3XU+N3TjiRq5YtAte9daRhIdGa1TwCW
P9AiQYpwFvPDaMHeDfBm+30CRIErwb7x/81ufhFsknGPiXsf1rv69f48nVnIxvNL6xl9gQFgy6D/
YaOIPJo3yYcMLRRycMeugpMgGDk0Q82NXPM6lVftAqicSk0CDbvtN2Q4L5ZeZaXjtZo1+qTgPMNi
Fj40e5XRAzB+HIc3m/0yIgAYf7Ba5y2SrBh+XyuQGZp15yRDF/SBbKH9v8BtPSOLlZaK+tOUd9O0
b8wX3L/R1C3iCvTXguKfESXv/0vRATODha05OVQQqljVyoh2NOph1A9xbbSB1SfTeubXLUmN0ER0
S8FzMA/sitgSwZWlA/BYviDvjUFLRFWY/tbKsRXLqpbQOP2pSFoAtBg+g6t6yNc0U924hffWAK3Y
U0dHW7h/l830ol/wzIRnWZYhtK1ejGksA+CcfBy6oOFYQ8Lub51AWyFkPvBbXibWAXFnPMIta0Z4
PifSsQGBMQ6YrQiCnAd5WpHxf9QIW0TLpgbwBInsZR+TVMwBq2eYA7fm4YksA92z2nx8GgZ/K0Rx
e2rnBo74CfN0Zfx62rNmwhCO6o37/ICbaQ+G2cJSWzLXCK0w/kLS/iDgPbiTgzRBkzMV4YEYmqdU
wZaqOxSlqnSgLncivsxMGsjJfvWrYn2IqvpHa9jtxL6Vg42C8OT3+WgphxxFbPMohcZykPcIGNaH
Cjk5n+h70YR9K2dH75DpJVZM5d+O7GO+Fg5ZsGDfy6UcFvHFaNZdxdykMNBCanahmrUvWWzrd9l2
sMQ65PbbkF0tJeF4dmAkfEE/4P7aaM+bSiWfaJn1fALECMz2p3s7PeQ8koYofQLLUyxaHd2Aq2mP
oDAhg2nQCfX+JAnAgs942vojJTRxfRsKy7wyf33CPSUPy/IgCmaP5v3N6qMJ/SCc22gMMDnBtbkV
LImqp52Rjwl1VWwQsI7OkyDKe5SxcbwO8fmtDd4pkZKsF0dsiLQEJu3Q0Z4ONTb0fitjM9U69yCj
Rv0oJwh+4vPeB6MF6WzYaQBJlGCaT5/BnQegYY7j+8B2eISqWd43jeKuCKLpy6qvpfWSxKUY4fPX
uXBr+WfZoSgrIYUHtivVRn4/atoCTbR4esQVvGGGMm5Z+uUfrTJDDgT20GaSoLj0nE6N5l4wS8E9
2el6PZ0DBep9PBSMed3gIKE4ObGw5sk1HmhPvPosF3/9ACg+fP3DIeDPSdWYsgLoUk/E2tO9+zwj
L+Cg2I/dEF/sa+Slm8C7bI6/9CeE4plCfbGs+9smAYoyxzXFS3v8mnepbi5rB3yeowsRlb8IpYkS
mTpdN5qy3TKo0+UC7zA8B/DUl/3kmds7GBzfhX12ZiB3B40Di8NI+yvCjgPufZrvXwAtmf+wbeKH
svPQPtb17+x/187718pnyxe0BfBL5SplyMQWPotf7mtxWasfdJv1s9I9fqbui0wO0VWmg+adA6tn
6wg4bf4rTuZ00qL2uXVTg4RxgR88rj1k8sOezCMnG3pD58/bsMswUMDnc3KQL3WfwOFxGS2xw32Y
98EmFcNaXmviqWydTGgSQoqTuR6o06kFOy1LpSHsSZygWD7yKl26zPFnxF0Frlw3zkr/kYlhJC9Z
JrGZVu47K2GiTIdbfBncznQ3B67OLiHf8mE/hT4RS+q3do1S0fntyYJ954gzwxySNmks5V+aRT4m
hbi1gqKETkIZbIB6JAbi8Loo3wLvLaGmyP3/hlYiv4o0SqiVv1ffU95OTdt989kO7SdZNlFbK6Rc
+2cA1IoX+HoLGyDMEAASeONpVV+IKwB9Btk75dKsag3MtPucf4dvwfY+veX+cMAXHBYcYp1SXcVQ
eFFP3/nRiBF28iqdnYJqeM8CVxfIxf7MVI26ZhdDsGlHxrYNniVLnOMKlc+7cajQsX0u7QBc1ypa
ZSNMelOl0I18PcwORwm5IWFaAPRFhzjVg3twLiwXPxGvSglmHUBN0fzZnAcJWkQvSRNA+MItb3jU
dv7Wdcs2omxw+QcHMuxyHuhjkNyAD7bmGjHWXBfzsm3tBLMCDGg1ISL+1W6MmYi3xr0EVq+RyuWJ
8wAX3Rhc4CoR2XyeCTRebJWwB2Nq/JIFFH5CZWY9sl2QZXQMAsthM0f5gU9dAyn0WMNEpliq0amS
2mwr6m3XVg5VyFtIy896TSrGBAFCxkFJpp5c6yr3NQUSE863dN3oHllw8jKeUAU2pFcBtX9fDoWT
j000neZy/jOXpzSjaCqu6byV7TckXnT6dfpFIAg/FvNIeiS4YPjnrVDFkh6wFVIJzPU4ZXpgN/5y
lvkvVHZgMIKVeLlMo380b3BuvbSsUXhhkq1GO0FQUE3DYC1W8j3jPhDb+wOqaK6y347FychXmtfo
ojOb15Ia1ITmZ/efNlMrZEa/D7NjX32E8hs6HYadvjBaJQsIa5/tkRMXOvcBPj3eji9LmOBCewFh
axStv1CtB30FxdhiGn0/7eLeMTJG3p79VYHGG8Qar/P88B0WyeMUy2agUAAdhwQ8qR+R9TXC9+98
QPIz3D3xk1fWyEH8x8DpVzh4lGTV585f6Gnmn3vVRRacKxBqtf6LoWW5Z1UyaM20pIKvxn5vBSXF
14BC/4THTNSckgt1JA5WjpoCw5r0oxfIabmMBRhVsWTbh+MHgl8HKQvOH8D4lfxsO6LzjPRgFNFl
Y5NPz+BYTX9SBYaH4wxujMIA4wOevbbZOGJCx4Cb9HAklvgqw8/Y6Ju/3j6XqHupWdv/jcDG7D6a
xTpmMmGOfLt3acatb3Sl+/9WS8ImdeWf+QYkzCZvGWCDdgoPgLTj0L0IXz9BoyXbJPCIuQJ+UnFx
T3QIYChG5BtRJHEYw4WQWq+2KiJARGxaBdIgv0H8WXhRo6UivWOc33IKCAnSDqPqcykG1f+JMUv7
ziIk8IZFYjE4QUWAUXgtLs997tK2Xj2WvcyGn3GzYWXS5WPWWmDkLBv7jXTfATU6TzkIC8/gDVno
GuhvOItBAAni0yVcK83HwkGeOyQrBMW871t3PYOW1m/VRHSAF7gxmLLJCrod8nK2i0hqglL4hQaF
lXf6GXysoTCc+G5vrUyA7ChAVlRxbpT2PBWKUYfSdVcWjWoPv2iULP33qehO+qJQ+pCDfYaRuFeM
inEmOYtHSkSRTETlxkljgY8Sly+vtfHtyZ+GZ96eegVApzc8ESWN4Iu3ghXgRlPGW7yhSjVUso4E
4xxgbGqHzKdJZr8YZkAPy943fNe1i5uOOQPE70XXNDkwbzYLM5vxYflh7TZQAlsifqnPKqbSWxf1
C6CKqs9bZ1GnAsWKzA6/GFyG3j0gXJ0DFed575w8sAKrqzPEODdRWydmaVHEjwg29jBH8nm02QRQ
8QXRqRs6uq1lGFoCwvrypS9l7uCy5lhva28nB2zlfa6+TMtRbG4JpAkNbwp1RD5AJOma0bqI46GE
MsuwnSCsMxqueEoAvnPSN8xL26Czu++rqRnNA5yc/Mrei6Rl8EBBObS9rYJ5kIxX+vO9RHLIG24W
qJEtWkB91prBiGQrfKzBlRaFG/iacZK/bbp1am9OM/hqwkTUEfWuU48q5t6uvPo+oZFP8Ekxee9w
dRUtfFnn72ONvcRCh0Fanbzjh3LtExSZIZpflQl++GuiadM3ecqMwO9YfMKO4IdXbdzQk/t/onHf
DgR5u1AUAptPoy/BvW99l+nwSbI+hocPkIC1skpDNEqwRXT7ah7swyULnlYqUtbLXd8rkuIrhbE3
uAvBPX9mRVZLK9tIoigstRMU2MkkYIBKp1zVnirhdybNaSCr4VS2dnOYHGHZCE38dxSRRj9lzP9K
Q5lTr0xBDEKrhGjfjLnojOjm5DLqevSicVjGX4Etzwd+kZbFawsp2U2FqLefI/F9GXMwwDx5B/DY
/BeQb/RO1YMEXdErJIgImUB0UNFbLa3Js2Z7OE7IfV32fWv6ZN2a3ZbX5dnrG5BbWaI61/lbB9JD
NyPWHiR1kBCE5StqrQ2xCrtJXvQ8FxSEZbjp0lxpvT910NiyicWVfhUKiEMIIL78/othwVY1AWgs
2qKwu8igd3fZoU+TqsFE+nM1oe1grfSjDosYDG6NX4zBR1y0oKAqSCcuWRD6d+s6mPsNKeLbFYiW
RqHFKBsZZidihne3go3SXtwy78NVJ0mstzYKtCMltONttCv7X6/LE4hTJYaL7lj6D/JR/l0iUGY1
3OeQtA+0htKo86aNJ0NQI6ygWoKbHQoADt/61pKSNq0P1OPhijFotwKWzoRwqpYMgvnWh/wI2YwM
c/Etmv9oujxp0XUIo6s2KqEV+HBWJpps9bNk5var9mQHhQ98yc8dOUWtxMDTkYZJRDp8EF81Ln2W
hXoIkD0wsxy75pXdpSkkfGcXNJZ/9GmHw+uq4v8Pgoto7rf2iYO06tm06IUf4mm3PbFUEQT9lY9a
7x+5kyvKgFJDkRbS8AiBZeE7NCkuTn5QMqZx+l2/5qo4vXhFFp6t44TDd8AJW0Cy1b60VwtIfYF+
lJrz6cI0IkFVNapymlKwFetkh9t6BiqoEXsTO82GX1vsfynnGI1LPkFXuDe5RaKNf4+7UfH4L0m/
KAUfqpLyZbnGuycGFX71Aij/imMWA8ayS4C2gsrk7vhtHRbGIaTHGOxxOTq+FjCHBuMhXLbXKxR6
hsgT+0C8ptSWx1kOwMxiF9AafK2x5V+35dG+ZCgCSTEthiDhqE7dChOjd7gpRBhqTxJ8h9r4jcax
CQM3/Vhp5p2xzT+KnVGrN46wv7Z3NwhkE2VUSQJ0KqCphYnEO5fuhLWxwnyqS7N+pxWllCKdmOLJ
HfYJ4nSmoNja8GPFb1QDhzEUlYzuK1TYmRhYOJCMv5suB/YYUKelZxL0FGZuyqXDfUSJ4yIxgTyf
xcqTStAr7U062HtGkpe8+wojSJC3WFDBq/pQ91IbTt0+RZSRNi04fVMtzasG1r+UhZRjCf3lQLTv
2oUvX9dPMpIP3KLQJhZg9dNNYTKXq7rHYrh6D9IT0AYry9ZBInTd458qsfIEkZAVCDBcW1E1gm+N
1og5LkbZ4MxlXpAy42G29DLn6vUcGZ1U7p4f6eJDonn316CSPB6XkIAqKaJXnfwz/FDAA8ua+Utd
+AaPdIJZc1Mvkc0SL2BfOmrbehCWG8xaMdgFFRNnexP4d9FlQUWZkRFuolzAy+119q4dhR7+Zwr7
2seEgOKmNB0NXvCRdIxS0K0A4fdwgAD1S4f9wsNtRH+QT+IrYPGPb04+4UYmTFZmhi+o8r6T2ul7
HXIUoS+7xgf2SvSUDnn9ZogToxYjeL7fz3EsqXoeZZUMHtZAYhZW/DwZQh8GeVuq1h/gNYcHqD/R
zPATmQytlp7KByiYAX4EUbCsYy267l+oLmjOLkYE1CwQvPWds/u08EdsQX+zr4XIsKB6UgYHnft1
FxhwyUD/hBgl7W4+1rIwl/65IRiQSzVJobsmK9hbruG73YTawMdKPPIknhdHwxStlJFNVT00KPnV
fwcCrp3JSZ05Kh0VT+zlI1I8mbXf0m9LZR9IOmtwfQwgaSfUkg8M1F/Xy+tDi0HlxFlgL1WvFJQI
vJ8LfSHe7rYhFoy7TN6LbNmZNCDNL4al5krDP7RyHSsrlR+kdAhwBMMaOFI3ulsVkGoXQ0yjqb1J
b/bg/XbyacYNqupbF4AdBx7/Bjb+ySXZe9UxGr9b8a2HgyhJyMkMmdoJ4pJdIpBjTz3kki8T7jUE
W9Dh4GgvkEi24Q/rawRSmOIq0z2UJ4iF0BCdXnN7AianG3qS6wBc+ZvpfOPe70s/GnwH6tTH2KZ7
yQAdChRTeHcbtSMlTFFGTWuD0FijLKW0Q6qcP0o72atuItQCqvn4e6KWOEyVG5l22rVW0YT4ZAX2
DBuaIxTWlQxXyfy4PMmoFd9LlRDhb976J+JtkdR5yD/fRwriUB8x+ZTu7Yc6IHieX7nvdEHN1/qY
mn8crKywm7XMPCjorEaCDoGjjSuhztn1vJtullwtQyNzSEaQKM+iXwNxqtYIn09Xdq3+mTvEDRQU
x2nY3HWxonMwHl3AGum5hTGTTUpHRVnwVTJ+xYy20uSoqQzjjaFAcmGen+MwsINx4M2qDgWugp+4
JVfeFyv+V+YpBG503k6BxBTLQPQIeLIL8i0AivjuVzkFktlSgS9+5j00U2GgS1PiZ+0avP0Ck/Ep
ZHeIBNmrMJc/nTexdsDlaNFaK0HEpZPDc2BdJ7LMwlgzjHUuDn/9F2LaPJ3PcKpxryN/3H26ieAQ
1TmVjywktbBO5sET1uj54OAORBsTWOZVnjRPeHH4k8IPTL1L2F+ajHPz7spxn2sQKfXu7aB8+LFI
2Y+bq6rNTstujLVHPiVKytypoj8Vc2ahRCjbS7+1o9CYgQjGUGC39Nd7oq6VI3t2gM6ynD92eQqp
NzBgGgd6ud0z+ePBmZ2zgM/fxyLUJ10WTacCyMATYV85m4T4ITW/A1Y8Gb+1EnWN44ZTlTs+Hn8c
T/jdaf0ydyLKn3qeBjiPZjAGchBqeoMy80wGUJCbM5++2esZXWzKzLO6BLjuXvDOcmAbXjkGk1Kb
qkDrurA4Wnv1twRIZLLvUkztYX1AlcQaFilPS3odJxnZcLV88qFvgXQBta2+Mb1Jyf66h73aqMM5
va56+N8K3+IS/NeuHfaYLi6ChnW99EWxDnY/wCQZ0GrYESpi5SHS4UOIP+CfB9U0JysOxinIK+Pl
hlT34Gs4mlEZ+SeC4js0p3Mquqb0P+RgfY1swVBgEIQHNL++/GXwJ2GsNjATFUXQ0shBsu0jONbl
2bvaWHXNkNwttxzmKyV9RnTPCKox0MV1J648eidmXsQsDengN+ufJoQOwIA+nmEToyMDVv2QJ8Kn
0yUMC08ZujAD8hlEdA8ChoYULrXZQei0ynDsSf82AJxz7mvpWr3A+AM/CpTEFh8If2Neu2CvJYEW
k4Yc/8vUjlas6XNb5FUoy8ETDDgNz+n6c9qRU2fP1frXqhsXH561WnkPZ+sYZHzkpjeSGpUPqvdV
r6pA1TTHpiqvTOiiO+9ODz3VsvK3vw1mxqT3kynMeaxWIE5zFIvEnJGoz5z0+whaljDjCI5mMUYt
67AXClbA7wyUyuwIxspCGPi3GC/Eh+gMK+I57NdTvzLUUxsDDqJ+rfmvvdQoo4zfOxI/F1WL9BpC
ij21O/eoJVAtkTZ5b8ZZeW4p810rFnHZA8Y54NArwxMgq/+N4/L7SCcgcyfwGYljimNEucU0NrHc
AADhvJ8QKgY6y+yzI+ya7uPj4anNPvYg7/RR/wRP/rFbd8Q6Xa1aKWk8lWmVlP8hidybkBm2Z72q
WnMaHZWZ9XXUpaJQpAz57CuDnQw1R1cMzJTU5fZ9GbtCpcmnYLr8aIhKeDQH1Mlq4waqOUpAncU9
8KC66LB/beoyC7oZKKMEslmjjpfnDKEbzIK9ACdUTtpwLihZ3NV8RccIYFhUCpNNdP8jelzBi6hm
NwIq6FVnlJRXotUtfweJ7uHWnE+vcE2xTwajGZZwF980XfszrzooG/Z9lqC19Fp7gZwd1fR75h/H
/7gDY9AGSLPe7s1qhNsGbbegvwqHnejZ8PV+0OC31C/mxptTzjHaPyD0jrzqmiVZLkpDGeDyKZc7
AUVcmV33g9lyo0XDuBCqqYDEgmBMiTZdXU6QVuKBJYnvfupfpw9XesHZGNYJB68K5oV82KO74cZ2
U5QY+DkJfQCxdtSyU5F7o+2Skvot5FVeiDkWjT2pj1aWZW627k0bEOgTUr3Oaq0f39ODrssLE2N+
2BaPQBED56882pCNvA+GVfAbKi2ZLl3qEycxWWQbejBMSgHAGYTEb/Zb6ZTne2xV/eMmiLTKm7hc
DkaPjLQObhn3cbF0GqW+DQ3cVivXLv96i0UNynudOsBWUleajsbFXpIUtpNW7TAe5f3PqCg2vfdW
SeSSVa9qN4RCSVzgZjHge+WNbSxAHbeta2rH3xszz2w0dJFSNjWPD6JyGIP/PnCmakjpXFsaaTj7
JSZinFr6HzY7gWoJO24WuI5pNHdoGITqqjyutZMv97vh3X84mQUGDdvuGr5CEPZflsfD4RYrcQiy
5oK3z7OHzGU88T37RV8bJdVpg8bl4mEmGc4Hwb6HmlIklNBnKnwmOsbrsa/poId+wizRfvWvyJmB
kNTyV+cgOJjWaxrSr6VA34T31UR3c/8CpYk4yJxFcL1g1yXH3iO4JLWeMI6sq/H1UzhSuLjidUhc
dOPNTPIukU0xF4g/burrEjNmp67aurzqScsX4Ki2AcuRgTk/a9xdKrz1cEyxV4uppWEFIgzE23Pu
3IrQ+HEB0rtdTLk/0b5g5YaRz3RL1FmYZGlM37a3IlVzRGpzbP/y563XQ9rvsn1W8mG1l6oDqLnf
a9pUSt/DK4ylUhwtHPoz6ctVZT64VbzjJMkDk8BFZOI/zWilFV3lOAI29fKzfdpKd0r+4YmBbme3
mZa4W1+qWAu+DOwNrykSsWfIbqBM+ZotP2Tp+u0/3+0AWV86vBqcHAASwL3WS97dc8GRK+AHnsN4
2mb3jZMTyN8Y9ir+HVIKuvvfPRGeX2fXZbbxPyXqgU5xMoo4FFpxofp+gLX5d8PDvvlnjVQwEY/q
zTazzQFAMu/UXw0+6Shl7rX7gNppPaBOuZeKbTv4YFtD3GlRbbQELemhm1DP/4GIYR8BH95vY0Fl
nuViwHVYw/6qdYj9hHxVphc0xovPBwFK12WSAYqLiMYC9N75aT5DfZKl2nh5aaA4D+xhkSQKZMvi
D8HU9Ezm1eOY5SUm6zva+rUzIzWPInO/lOLcbfHmul3L2LQqp2WJ7ezzEj4jdOl2+5MhcdDuhgSC
OvXKQWm7LFcEyafhiOtXy00kTFtKu+9C2xy4N5G13zlm1hd22FN44rVnJrWGzYqr99eYRanJ+n/q
SoA+NZdpmd7yh/8kfJZCeULhqlEarujtnuDhgiY9aH7CUhD+nYl5bN1EEkwMmaRkB8kAI2I/Adgm
uAtmAw4U1Nsqsry1e3UamUwCJcU1s/TRzMA1b2jg2f+K+p6hOovs7sjWz8bSz2q6IHHoe929u1zN
fTIoWdhI4mwYbo028pUYOL3rtSdITNP078a6PERPiN6UAvOAdTfZmiReYiNSFJz4dNhB14q8xLv4
lfTk0IKIKrM2Q3/r8pQ7VfbWoDOMYOSQmA8WkxPQcfGjQTtTeeaOE410YAervOjN2eiTYIUdR2tA
Bp83GjYKZkpi1nQWLaMueGafePIAqOkHqCR30dmiA1tuFQbfjFJ5T0MAY4dA3ShRNZ05NT2B0Zj2
s0w+C0yE32p+itV/Ao6XuAsBOSx4NUu+ZYPdQNRTa1jvVWptQLCV+bkq+j+VY6SwymZvfi6BHbD8
syFmm3SpUH/01JmClQAwWN2clSXgIR3BI9wq6TvEg6CrBdc8hyP2yV2sSmJETW9FcGp9TzQHg/Da
vQr4t85OQlfB5rhsjpySyPXOYMDZ6j4+gho3UglwTm1vmGpl1qyZd3BpTkp3d47bwSu+Au41yMii
FK/FFDLBiuGjmfmw0m/p+YFOLwSlNeA2tjOtPm0t74FPkGgmlAPjltOEIGZgEdtdKDUwRWMAvP2A
CzitpKMPBMYMTUI9UyxTPwU26pVpgC7ov93BuLCj532QCKKJZ2TUtmevd1BZDl9MQPiPBy0KyS4O
bsD03HhV9rO7IyuVtZAAnVTsLkYyRoh1/cYmZAaVeBfxZuv/4sjkhSrvV7DSfa5ZrgUwLsfrtIbq
WaG88wbeSKdK3GbRoc8NegFt3gZy4vBuWG2o+1A4nWD3+R++U4CtbW5KxlAQIZjCQQ2fPV6BhxjE
ROEmeV4NHqLxtQckjWT9ViiRToCi/W+7c24GzvpObdVivgvsvM/qhPOBuclFQR+96NIaWDiuMAOH
YhvpJ0gP7axe+0qUaP+n1eRQvb82IXWEhpqvffOiaJso05jyTTeB1g7qOX1ebbdPTEUVhrPDgDxp
/FJKzyoY1/RlnnhhIHFi6B8cEXUcOOwv2e/yiw8iEC+z4kffzO+roG+I4UefLt0xkmXVIjiKRFbX
JBciV1PQZmQ7gvONQgDIcfswmEmpEDfQwHYm3va6hu3d25iy8VUr8IpgyrvARVQR9BoN0WBXDj8F
lBE+2DyXiOcdkELqGRPWOlTOuOJK++h/nuuItrOajgz8nWsYXLHc+JzB0gBF1zWf4bgDI88tdm77
NcXZmdgG6ovTLc0z0cdgFtBwPj1YUR7PwxNPiuxlaJTfBAnVtet1qMVa8KGWSO7/w4sW3Eomms7r
Cd+xvtsg2bWNvrfTW2uB6SzvNWVyVSBI4r9yMYPmNFZxzrpjie6h2VtvcVHaz+WNlw02xKZVHD3E
pjZTY06a9ZKD/FE2gRKHzwhGoF3fQFWtEjpmZ44KzPsMGpiu0QjzUG5DY/r8jPc7PGsErGjJUr1e
Ir4gb7ngY8yuG3LwfccRO64cqJ/DU/Gmp8aIJ0DtmfF9QAwrZkWDmiHQxCkXTtN9gYrQjiDYSw5o
ZWD5tRv/mhEZeGs40nJ+qvkVOY0+wGKnEkUQVlWgmJ/KvKw/+wPhr7Af2RiqL9h4GgdZ8g1cpJXl
HRAJizzFVmg30rg6l2/GLISMkylLZ4CM2VVPm0x89r6DC3MAQy7k8t/GExND5WQQAyp6UHS3ug56
/KAAIUogdddxGjhTySxggL22tUFwYHHJUmfCyNbn2xM3toEE6ootVLbd505xr3GfViTLcm4EGYOy
QNCQ8a2baD2mizVUJ0T0QK7RNfP7RQEyOZD82kUaA5I9uGuzae/h2f9oA1QAe7AJ0Y19BbJv4/hx
zx5XEzzkomiklbDjE3UmS4lz/8rWH1z4C92zgWzMhOWppxyEOcFQlXmcVW7PPkgO7CiQMVAJwdNF
L7axAki8/zDEt4PExPNmrYcHGjxM6djRp6/leZ/Ea85iFk7+f3Rt0G4YkfPGa25TzfbeizuKw7YF
qb8VjWJ5aDplZUR/5p2w/fekF8pHzRxeac7HpUc5w+qtZEktSlo5jExyVtp/fdN9RTV5pVM+IhGS
4LQowKiz6O1Xq3WSdut8pPj2aQEqqr2v+llFZiFJRDGI3OSylSap1f/acQrBdBiJ83A6EuRU9l0c
iUTsDiKLpLE+sUKgD8jktAwfijC8UpeyOQOEcFLwv5KSXlgzZ0KBLnOT/DbOSAieCQNg3yN2lW5X
DTessYjJRVirFwIOFzdZoVgVhMZrlDI2zlMxC5+vteQVV7gibWpDSKJBzk9Ipb1xTtTcMNlc1vJo
bdwN/IUGFvqBNV8sLbNwJrlN2yOSuHfeuSLMobSSp3c32gp86qca6IlxynP1Z2nn6vGoqXSdwmjD
Z7271mwuzwV4JohoSKAJLSSfH7yqNkFXDUV4E16bVOY1/lRG1Eghg7xIkEJXKhya323xe6AUZ3bU
VgpMwOvOG3OJlEoOgN+ebMxca4bqhHVhnGgIrVNRwQSmGP1OGU4LdoScH0pIxAZK9Xjo+dx7AT+1
JJAx7yvrB1HGG0sbzglPYrHNBnqV2awMaEQ3UeDeBaGDMSudTnTaGXV+XQnPFqb+ga3NxHQZgHNp
KguNfZpp02mN3GJl5cqFr7rn9dHBLglTsEP+Sq62NZ0tOlcVWRo3Fzw8I86xlSdLLdmdHlcuM+3t
V51vgI+Xuc+RAJm3ofPGVM2F50OZ6NcMt2YndDK5NuZAgjWWav0nKDYn5DD2mYCcvGYd7ftrf2c1
I+5FaWmbTm3FTeUF6CjpMkBA519M8RjwG9aiCYBG3vWnJyw+zLMm/lmcitYdZPCK82FgdCGdJPAM
X7UEzSpFW/+2UUuAlh1jnvmXuZTOy1x4wHxlh//83vo9aKQSMxHpQNCnoq5w7rRAnvz3l4gAoeOX
W/LEK/KbTRhVdAsmvY3gyA70rO6HWjYaxoAsniISI4Sw1uk8mHS+88O6FBICWlo2etXZEvuvACAg
xxhM4j7UhJACwOLPzgqqRuTM2I6dNmMN8h8GSoPQs9MmozYvlyjNIFSdX19jiRezgI6WkXnu2GU7
Q8iQwbI9j+rqW55Jq4U2enMZ3xabELpg+Y83KLOnZPcBXWb9/M6alNGZ3xdTijVBXlIXM7eQw4W4
PBqyAKOiP6coQCO8E3NWyfMaz/zx7QuRGjm8ORtUVxxfUjSzSBJAAfALYMg33FQmHWplapmh27cc
ngB5A3tTkgsiJybQ/B47BEmJfUsJUfXmQq/5PbfPn1hd31P7SQKWUHkUMY5T/5kuiQc6p4F3gBaP
TjSA4L7VB3NhXaehaslQT2Rhwqjulcr89zFxl/ukT26jhnhXgfmeMesslQtKaJACNq8HO7pZGl1S
EfiNkDeHfykBEMaoTNZqVKRDvUg3Mi4kQwLtwecR3zEhe1DHlS4dVahghg6JGXryAKYfV89FMawu
4ezeXUU9SXVG2nkiuz1+Zj8+QbZMomXQyaNZPaf87wYiN3zsHHJRGwu2WvZxDyaiHDbgXpfU/x7B
mQMllieAw4MFzCZ+qYMe87K2O7Pv7kiYFoj7MEdPbzW/qWJyDXzg+BwTRponRkfXwDEGDAgX80c9
BCBQTAZjz2CqgcGXqyJe3kp8ns2636mi3ESwm9BDTe/5ce9uaazoVCFNLrt74w78qH6NRCGI5FWQ
Fnxk6VGm83b5ECRrCYdCKqeieNCRX4t5V+jqBHAJKXrEeb6XHl9IVPKwcMWk5wldlA5dmn0qnxUt
tv3Es0PmtF/tbxYCH4KuxqN1hz8fOzGY3zl+oFubDdrCf8BzBn3+wHdOY6IFLHEZSe0rabC/1Plz
3UMR/nlBTV+wUtEFjQsCeQbj01zd/mrhnIH+sYOc+/MSe1qhKYlgBlvhIsGaXaz09giKzAzzWnUe
axAMujDLqvDe4sE6gN2FKssUOMV18LuZKEKj3Z4/LYVpRzDl0A+AT9824NAaVf9F1Jc/u3F6pxtF
u4QBSItcicAbujys2odnplt9c9bOjGPAnzDmiX8djFINQUzjdl0gz5+4+BCKBUlowByzXdVX93It
GU7OTLIrbUo1Zx1HQYVK5uwaUR9W7vLda4Eng9uPg+suXLPp+GPRQVDCXSTOlG0jcRtMgF8ojZtk
pextOeyROdF3bcQqIbERntR65mWK2ON5CiriQYq6p3BJRjiu8T5m6dxhxgJJt2Fz9DALSQHFejPo
Sj3k2s7AzYbr20tVQSSbgTB+Piom/nBkNWl50sglx0XXZalxhIbqXS5kaR9UNSUEJP+Pm7nYaZI8
Wzb0W5wU3hYiSogmKJbOr3mgt7j76AZIM5cHihUWM0AMRMR8vOTiXoM2ERP/LNnnNGqOBXErYBfm
m4MCX4FNTZxMugZw/329rBrGxe6PmFRn40MIhBVP52iJ1WKuYy2mqmrOnE92d3w/LESKHP5bhIIi
L2ZhsbCA0Go/U4wrsaK/BkGmFPA/XkRvZRmXIvbq/yIg8T+Wr5/WqjQFb6iUNh1qW++ZriWI+cxd
i+ARzr/XGaEHwpiWuFQxbCk29KEFnueQVAUcEypEHmzGlzOC93ICibRkwFFYDWyDZdzON1SYo5uf
LSBGp0p+6muYHoCPt3MqGrEDHR/lxskxMKd3DNcDohe9sPFe2EVeqgyr1liGDCDFo3rRxF7aYPZv
XcWisN9/RwzwYY1yklmvOer78i7/d/v/cBCgqFvNL5mstuvjSJUX8bIXY8XG7qAKM5NmzNHgL8fH
PqsSZ/Xwaw9wzuE9o526ToLyF8VqUO3RLpJ41ibbyiRWB1TxCoHq9PzLeZE750WW6BcaZtVCz2QW
xUWKpoZnSMNUAGrz5yUyad/Mdw6stDMTFRyVL54ggzEcbeWvtHMcxH3ENT+GsZJFSp+Xjo7qmVSh
zFRxOkK6COcslYlasXYoq6OIVHpTSwm0tNm1+Myx/jzQd0qEVjW/NMu1xbMTzv7y7MScFmcRwpf1
PAv740TvDvIlbVBKkdWRb7N0PGKUqm8OFHc0A12uZTcQJewvz2p9OqdWMHoAIAivmK1N5m5zVETK
DAunNszTI5ajSrLZmvG7rLtg3urmSOX+NHJ8OZUu/WGWifSNSshQeg7X4P0x6smkzA98/XWFP8di
adKSq78/up35JmP0ctA1EZS8ix56DGmEGFKWn4n5xESKroOnyzcIRi984jnBzVC0xx+T/BuLxgbl
bD1SV4v/t+y0JXGIMqZE0UHbOe4Vph46Vrkk9rMjB9djAkk40AjXnwEYgJ2e4ejr3m1kYRhYYLnG
p/17K3+hZL6uRxm8kNiBNhWLb+bx/DGnqRdxFrvuutv37MR2xinpo3cuuQs/SUeKuvQz37T5h8k6
X9eTzNqXo1S+9P4gZcitXY1DkhemxP+Pxfz6lDWtmmUeZe3a/WJlJt514zK8Kfh34ueJ+bS6TOnB
gqpCmu1Z3dR5aZ0P5o3Xaf0cPbbFadaw6wvv0pZ3FuhdlYNjI6yGcGtiIVox/IpIJ+V6aq6/HhWD
VqBpbYaV+k9YcncscylB/lrln66EZUnfSX6qd30OeEkwRbmxLDm75DOwqvdueCR5LE2Qt4mPnepu
KEYJKRm/Jind1ZOghzoW0vwSSzS+tAqSyiFbVnaQIhhs+K/2Si+btK4jMxJ3uImmIWyjUWYVkavf
S6BWDKOiDkZ+XsksnSLUb1PFe5EbtWot+t52CePwZrfBmtKgk0iwZiR1rpGb58T72lmq1howsSqa
1rK8aBZloWUNg2+dGHTMBKVuZEnBhaYRqTcRzTMRX7L8yZrfEm6KJVc+hBZvAgb6etrBtvIjWSAD
eGGJ8SwC/EWAvYlsWOQu4JLqVzTKmsJn4H87ztM8hT9ytfancIIT+LACtIoWnEeQpsc+XVipbkF9
nPbYCTfKrftj6N+XCIKiL+L31od36BiGbFz6vPZGtl0aQIe575bfmA81WITfHPLkLS31Kjziv4KR
elmK+rRKLwzxAWufxLMJLdEMRCRnwVIfvTnmTSu7GWIaMduhxQib9ZAf3Sn5dbVkc1EgmubI4BWf
CTG5ZkE9DilsQcIHDIQteiMgPrwfEMUsTYLrtPCw1/emHJk4u1q1sKlmz4NSIHRDNi1r4O4XEDKn
eG2+yP8+ClflReQ2m3eJnCsp7aOgGqXl/mPjl9vMZm5xpsTcEJYkgInI2y2Xj4Ur1bR6vpqbbTxj
8FL2QnAttu8IlS56Dk+Qoy5wndVl01OlQmY4WT1/Kom6yyALxMaL8MbodECULw4y9HfqmGIjXfpn
7iRkP6G/0FPYfdw+Rs0IqdE6vOMW+gNIvXRB9wCXuzG8rXz3jC8+nTuXLEx25Kz7N82ennPhj7GR
mRw8+ifh2tjheXJk3JgKKgKcRI9W04b4JmJEcboOBlrLCqhwIJCIKE01Un9UtHDIaQCXMvdE9OBY
Tdxn5zwmFjrEeLyFcF0jvxc0+t6iImIFGlFy4phmfKGhd4nKk4qd0kra27p4ucSx6Cz22ZAbjsY8
vJr23eFr4ZyYXhbEocVway9X8tomGtY/EK2lIYNd6lgwHJQwsYqt5zQJcno9Y/Yso5j1cen3C9B9
9tcRgq9Bm0W9BWLoXHnoC4Uj00TayToG53pAydCw+dRkRyJfzAgNqvYsy71vLLROIsK9f16eN+Fl
zMH18RtZYgSKldaaeJEotypIT6vVhF2WlKf9UuOtVCJLP1FrMIaVcTowhfp22UqkpEkjlUBvkQFa
QIKF41fkYOtQEvhL2dblX+Q24fZfA7ovpnCH/q1ugGy20gUqyOsVvg/FamfjueOhjpozTITrDMEM
xruXQ4Y3+8KOv/q0T7mTfXvM+cwNHV3eWw+GLJQBtVesTvcjeHLe637Oj0Jbm5+l2ENrXE2LY6Zp
bEVSnst8Q38CjiqGogqP3hVnyyjypqbVZwdZ+LtU/A+ZORKeNJOq+Y2kpxmky+b41F8HL0zKq4fX
9ECFBwIIOZwm3/95UifrnrSgTfS5OSrg/HLiLjQAQgKFn4a2ZCF2J9wB+OpxVfsXiFCRM914TUM4
ZdRYcUaI4Y02aVj8HeAqA2VldUAAGtL+ylkYTs7a9tnnBFj0ohbQhqsDWEGYljm3ZCmzCYD4TPIu
Ft+XHdSYR3zkkubMeafJ+oUGymMSech1inMrDxq7o88leDPdtHz2AJG4UsX/kkAg0OLaI+MzcexC
aUDFMr0BluRnSgCucVKY/pRQKuSYYcNeufUW3h3yXzgEj1FwefDuWLuQc5G68/xKY+elSuFYhBLY
WegAD0neDqCTY/hG0UND6hQrxooAf2O/6qMcbOeAAx5ewoZzloEH+rXzSoxNLva/fDmsyUKqj3Mw
TmQmJtAYKjhiI7UIU7wbI8lOEi8pqroS4AJ3UCh8R5H5iUFS5kDDImhum8K/FNVMI1SnziX5Tdbj
yoNLhnP4WRll8bO5mE8YjTJDDa/euv7ZvVWix04tthfLfgnwFQhEP8Gd3Mdm5xwS/WGhRUacWCGg
tEL0jcqasXDxQvZMmtMg2pHVsj1vJjCpxxIWSaYSV8GEGs1FPZiwYxHCadL3S4NT7U1hSFq7o8kX
pWVWeDXUJ9AUvPAAZs4aXStcM21USyVkdY8p1UHeTP/fU3RJ9RvhCMofgulnopRfV2E2e+osqDdA
bc4FlpPRI1PMc0ehUyNi3RXVTM2MMGwZrKVCdQkgg+27y3UacFoOB4/VznGoh8qTOtWOoypNDBH1
tCvMhwnkXDR5vs1JTYRFp1iv4mp4q67KvVwDaVEqIjmiJpQCNNW3WvjoGwgX9O/jEXcod68kyOA+
24UCid5irDkC/cZfiV2AMxigwY4o9/qcxYrwHkJU0dg0rc6ZcffTstCMErRb11I2agU2OlKHGCir
0YVnt8YpPYvPavc1fSVmadFbb50jQbLMB21TGMu+nGAs/cw621+1dA27UnCCwWZntG2hoKkt7lY5
GgpQ3pFBViEjhu8fkuSvGwWVQe/4oy/zTpsDObbyRHb9WbCCa4XtyZ2DCaircRO1EfQXKzdxW9nb
a0/2Hbjgdu5ea0CTQuWCNMlQZS4Z1sLm3FRE++htSzmkN6CSEnjdd8BBur6WoppN8++8OUXBHF4D
41WcOwRjJcz9LX1wDfM9+2HSorEZFHpB6LT2lZlz7im1yMrxPYr/jSwZ/4AurothoNfyQ0qribg1
9EtPfM8cF1EZAwhZlRVstBFLF1vWAnOfuNQV2fL1YBOb4pG16L8pvucilfVNIGpn4FDKscxCxvOt
VGFiFDd3kZ0Ocl9yl57wmOiaXtmdy7WEGTRkMBWl7eiBvgRbofuMchV+cGmH7dOOr4+1eTj5i8n8
mnTDULMK0QOlPJG2gLZxJmDrQbELvDTq0HRvgBcQk6oAag31+ouBqq1X2QGg4DbgLSf2mz9k3Au7
ufSnS1Q5REkKwOtMta3/bdFRxgaEnb5vj1TIBPc3QPTYsXj/HVDCtaK6pxQkgHBPQrcSfuTNWjt6
QgnmstIvWBe1LwKEeSs43YTpG2xXYLj6lwGKzyFpbxUkCd8Do2SiCdw2l14mD0KwdgJMKem3rhy1
ohdLPeHLFikINCOZgSHGVSk+T8b8ous3CecgMaGe2HCqh685HrJOf1au55Oq+vfabP/SpCUF87U/
ZBUqJzPcToKBvgoa4JR0IewpQF3zOkPMKlGHsd6SHUjCFYnfVx6KdVihCsPUL5fxZJvm/P5gMd8l
zvwIqMkTML9aSgX8JnCoUI10R9kTSWZy6az8C1g5jV/YhrymGCKTwF/Gi8jCFioDOnTqXugzTGe+
oTEzEvRjSAHketxqHoJuem1qLBpgWjLS1+8/YsIbYy9VemGficbmZls7+IClQb0kZ1iUhMqtIEBw
Go2eNHDetptVwamWDCLEAgJmdKP4Q6Jbka2gMuc0aj5x70czWfV5PBkr+k59VcNKjYkMmTKvCPGd
Ht2VU/o2xR3a7QVQta3LEtw71HG7KP/ZJ46j9dIIujqVXtBFC3R90Pz2GdjWly+wosGT8Soy1Q1H
oiRDTMVC0ahyB/X0DE0Rjny7hKFRC+TYFspfD+getEYpD+IMsfRBakzsW35uHuK8E8nBLErEQQpv
RffOk4KZ0BcJnnPNS5uHs7WdlEszAseas9m+ZE7dFub80qk3ab0oVrCkeXzW1qMDcnav/VHLq9Pf
1pYkVf9piN8qUmk9AxEYQgRpPV0JAo+GGeQJ6gv7memo3gA2M3HEBz7NARqZ2lx/jjIFofqyP1RA
ey8AVBGVhHaokzaVYkiDf2//uueR17JB86KYrvMRu/8B812RD3b/sWkELpW4JRLMoUiNvm/Vd3LY
yuitoRGbiagvzh2gjde+wUXiMYotJ6hBd5pAePfoJCW+2A60+FyrXyH8m9EnZCcI8D0K4bAJeqQC
Bqg88FqxI16x26cWh5pasbpLtQGRHkHJ94+loI9e2HbbQRxLO5MwtJzTcUi5pweWEL3l21KPKPF5
lBCbu5xx/B92C3rfPOkmRuiSKkKtZ7EBAWiP+5MxZ72XE2nfRMApuN0dNnUx28GFcQnLhe/Qnas2
ofeID73ClwlUgHc60DCv01J6wggiz1hr4KCU3qvt8l2VHiwmjgCbMparbrU4pjd2wSmetfgS3MVe
DuFwZ4/nx+CD4ajjK79e2FOSVxZMeOuhzFida2EYX9ceYnM9FMypFwuBDHRiOiAZ5/AptQim02zI
yjY4YDxSWYLlBro+Xw4mEGBndYIxSLSH6/Z+BHPjjeM8L8DpEB2aMrtsjnzX7fz4aHQJG5Txbwz2
b4VZfdKijSltPKOfZA4eg/ZCnL6lher+mKzwk4owy2J9kGGcVDNhDHTdzvzlEsnewM1QJvO8eP8l
qJOF6y4Grsm0Fu2fZ0h0Au/k6Siz80/xeXGzHgbbj4jgW8ml1HpSDkpuIcaCLllFA45t162j4Cz1
VsRsTyxsu8xGGJM8WRd4u40s20FvMzCmpVJgBBqKDC871+nnlOv5YV4MbPNGYp3a1I7GZ93v78jw
nBWJ1EIv3gTQhoykzvuTrS5vlDNvp5RCKSPfZBo2Ixoq52oGlWzpeJYKCcs7yq2raFfH6W0e/6Ws
WseGtCO8nma8AijBUr9GqcFUGDssZbfZftYZB6Jn4QFaUACCZ/fQ1Q1FBxjWnqf00bYS++aJ5JMc
Opb3BBx0meMR8B5PkEilYnAYEfeRQMvyODz7//8jNMa7svotu71Fp3Ab7vbqPPWpyx7jqeJrmya7
btllUm13sQaV1rLCh9jh219ySxZGuR9veFcI1PPGMk1vdkcJ1+bmCP8cjhJMNHIxpBesuAxSCriU
PlgHcZyBskpJp5M9kcgQGidWByL6lWziKMiPk3J8/H2RyZzxgrukBm9mpCKS7utHWEO5O28sdx0u
ucHe9D63COp34fI+tMAE6Coxm39NU9kU6/Dq3uSsF6y5PCbIfoyP+UQMyct+vdEaCZRTfURKCL0L
8MuLTWvgJSEoznTLvtqcyxnW9mz23prghuhF14TtKLGc4hE+G3iEMRxjlGagQ4H1GyqnlyK2vCu0
5bRP3KLchgEAhWWs80ozXXO7n3F/SoSTUDxridH6aCcv+URjxHQjhyiXkBY8QZkoHSPorVaE2AsE
1ovHn6L5ypMEQWEtgE9pJ0eaFpWt1zX+eP5pui6W6OQJJPwXe04onxsW48Pg9mU8laFj9xbouxLT
t9PN8zm3jF5gO7A7rky0D0nRhQkB7MtqjMg9xahJ8QLmigPtSa8Sg2FTescvnRjo0q4SknEmRDGF
Gqi7o0z6vzEoAnpoIZJjL9ponrIv3X6q7uKlOdzYlqqBEtdxLgTv4X4mjMSqxN1GGa8Bc/E4tsMT
zvTvttU2gVZ3sNb0gQ7Hhg5WyTBWd6YeIGmyvwxD9Y7JJuf4vGORuDv7Sn/NKYrbn2siwwB4W/bw
ej4CsGx8J/00puTyY4C4JQmfgjWPIjOwKBCqh8ixYlfnAVCAQGICGWolyVETk7QLXby/JzGNx87k
FrU13tI2XbbXcx7LE/OHK+Xfn7miWcYSFs9wup0IsjRCWObDrOys1i8T+diug3SoUtUtDJEEipkJ
C6Hf6i+V+mhELRK5Dictr80Vbm/dHFqo2TBcxM11WelGuAhye5csWkRJcbx+WCcp1TUqUFcrZVOX
T1wAjRByK9ZieAvcUOOjkAZGK2CFCnhofUlRyRI9UMHn+QToKKEPk+sUqEKhvAM/6Is1bnMgVRtQ
wXbq51chlW/dlCBa4P3IHazRVLKG5RO/PGxD3k3rNz+FG/i35pYGewSoYlzcO46EmA29REPWwX4N
K3QG/TeK9fwqKLJsiKzWvwrqKSOLaFj37s1AOUQ/UL84o7yPjahNlQQn2Jo1/xT1TV3z2Qj/c9ht
1c9wIJHnC6gD2C9XwlSPvqyy/IdTBTkgACpkdrqmWN06jCioYGeMVlXDqJkRC0KPE00duGIK+Xhy
YGSMaZsMoNTFd/vYEdMMRkOMuCvbPIAfWVMWZnKzZHP4gif9XcNCQRELt9ViCloR2XFa0EQ18og4
oobONEuV4PXF0hr8esVbeAR+XvE0bgrpIvRo5dmHbC3bfJSVAP6WAt5XxphR5VZfXpUavZ5eUDVZ
+gty69jegTXUTprldfMrFrtLXA6fWXQg8/yBD2lXFWFHVDEblgoIgcJ1mkNvzFJx45N/Fe2dLiYa
pfp6MhykFRdlj4/UUGfyK28X41nIs345AQMgRNwc5DOr8VP3nyqxI1V87ZEYg/wF0CxLARBEZucJ
oxbJliY4IAH3tiHdbEJGMdPQGLM4PzPclhsLpprXQqyp+N0taoj3Dx1c9FCnxMBTGSc79GyZCtqw
3x2g9Du3pXFCzxkY1+rxcryO9stNTV95cteySXD+14kITB7M7dOg+pxGQhUwryIOk027UuB2SAui
xD2dkutBSnmyntdHxTM4LIp3CAW4knHfQIvQKwEStC4P/h6jkkaVd6lavjWodt+FKWvFYZ/BMxNh
5B/u82d4vm+FccnkgQrgJFpw7hXkmUOPwWoLdSW/qqA68ctNj2yFN3+7DTecND3ntezBe1+Vi6V6
jmm+Jp+5XtJLoypeDhNpCJWCJAgbqbpUIDVmNqQ7jZvvN88weTJwRNE7nZr06/QnffFSPe9/EljD
SxF15eeqfeVRnt3hQMO+WW+J5bQkxDMRAKK7hFwTXrumF3f97bh6m0FaG4iu7ZS5Rh5PK6CuKFJw
g8JetNoNlOB8o+fRJktNhKcqV1NHbfbnYrierunsmAvIrVWydAIjLqQ3El4Y6e3xSX0ZzQaLDGdz
ywhjQgdvivqBBtousbmDphBovIY87AHZlNizcKeb2HJOOcJWdPPry0o5K/OIdZ9km4NpOLPy+hs5
luTbAPvYPjot9yu11uBat047sTB98I5x4ESr76gtOZ/1hV7tJRC05QpusrzXH4DaCpfAbpZmjcDp
DmQADUiDxkrIWyJzRBA2evs1HQ3lmgLMm7Sf/hvcL1OW5xlzSdSzo8w/qxJBzshIWRCuLTjH4908
JBoYu0nMPOQ3iEL72tI3eThyhPmr2NmINKRL3QXGDFblh98HwVxTZhQB0tL8lRSe5bLK+J3dtGF7
KeG3NRIO6+ATX2mU2vLCwtPRJTti89jTzHRQlLInawb5pI3eSJdEPmw/NcdYVeFgnvWi7ZE47hfb
7Rt0wrJkA8XNFs168FT9qjRvAAa8HnfSFW8sb8XqHLe5Xnu3bxx4B+FbHs5lJ/vmkSviKgnoyvrx
vkX4PIz0Pdluzl/p5k7kEHcfvTIFTnkts2UKRQjir8ZKCwYrLh7WCekV2MFp76VPuwuU846sAoc/
6/5TPmFbWN5EO/E7ZsqXOLphVtEy3nGp8tlAQF3tp3kPTWadorwmSaNkiiCHFz3SJ0SnJXSdGyfI
xEwi6I4gFDfvG35YkWirQWZMZiwgkPSy2dxnHW0Tv+jTzDQozqJAeXZvKDJgJD2Niz7kPR97KBWL
cJj5fHgDg+kFsm5ba+6gzFaNlqWvWFy+TRD/VhwDCd56Eg3n/eGH3uPjOWjPWlfUZNqQ/AdkPWn9
EGTDon8sjo0jpVJ5eiJIGy6epinqOehpHxJyIKFvi0NHraCN645os2mZ/nHVVhv3jP3MbffQuuDE
AcDGDW/GcJwKCP7d467McE4NkxQqeEtFM/vV1f09xxp2SplkuQm0Fz0yZVkPOyJzvpXvTZrW5NPX
42gHn2WaLIbRmC2ECOmLa0//Kj9BNjeueJhTEyw8situavUtBB/EVjKKfH2GjPsU6W+OlksyBGYY
WE0Uw4/1EfWwUXjtwS6Z83ctJKr06pZGQ1UhOmvibRfV3yb9/sbEQ9NST1+3FWXd8VQJuP3/LT1x
sspsNrCGPJDUkqB6wP2Amtx97/ayJVAnKYf2qjnLcaofRxMvzVKZHv9Hed6/iRFSHcs24SexFwFy
+RSsE5fi6+M2PudYQf/32riEbjSMeZ9FJrplbpc3rvbGgzbZXXEBN/AXW7RrM5kRtSSKAhjO7PBF
9rJQNyvlF90qgv5bDGtc+5XIeXidoftGKgHJtX8BQ3VVx0PWzYzMSJfJz97Xozgki9YhhnIadtcd
OgXWwQSn/GVlKDxTVSgCShWEz2oE7R5Bb5cQEy7uhCAdOrWzNH/LuoZU4d+Q8tsKEo6wOxE8QYli
zBCu02aGiDZI0IDL0YM230RNSaram0bLUF5rmV8HstSwoT5HBKTFO77fxadCRMCxyotru7iLbS5p
I975eb4nrDtF7QgLoGqESUVdyFBledF27Ilr1/9QFN3gSlrgiY7gkVGBRipGU0nzoYvr82iz4ayz
EvJyVQOMr+p9BTSjDCjSGnO8v7XpQbzkd0d8kfbYXPxqosCsQgjZsOAC+l3XoMcMjC+pTpNguYdQ
siZ+RwDUqb2NJ/PnajhdDXcdFVc8nGLh6JGbThvpGgl+kv3W0TQRo9ctbDDhkUZPnO3vsFufaeSP
h3KpNjeEaCT8hvU2NAObUSsxz92jbqabaGq1wpV4I5JaVjSWYr8bG6qPeI8T/jhu1+nJG8hV98XP
Q5dzfkug7ioqv4160QA4pQjb3+tnow7O8EURyVh6Hat9g2D155+z2d5sgdoEKC203JXyctTAqSWf
RFpsdUMvqMBM1bbzgnRzhs++QkF8hWBKyseGpQ6y2ZPJ60mrX1qdp18jND/1zz9o5jMTleDh/E+x
aBHtgyqIivjiKjfeFcv9/FuUQgGbBzxvbKVU5fYNmNJc0MaNAV2z8+CtZd8/Ur88ivwdVx1TeQWL
Put90groONMuoaulYTMJGpm5KVR3Xlde/I7vAd6wBUy790HKew2RdZCjmJrI/jOifDK7c8WLFkP4
+zSToFWmYu52zC+/d6uzoCILnxWBrBQ5rlNhhZ9Ph06gGb+4/jFGlFsCh13oJ5rx1IHHJFdNPlwY
oJg+aewOSvEIE7xDsq2suV973yeVfL+R/Y4sVjPnx2PEPeTLiq2z8qdBOzAxjeORBpwhOxD/tlUh
4+MRXeTEXDpSXDo+kMFuz23tfE7Pim2bzgoMroR5ziZeXqOtgTH8SAyi5l8ZSUAH29shllxTfP8/
Gofky8zkNwy+i6fVhyKh2lMp9Qdsp6QjP3pKPsXSE416DdRQ3QGO/4DViNF3TbQlWyQVyutLhpFy
7Yiy8oyJ+/XY4KqFeYY60U0OUc0LMoM0eZjJJtNt6D01/1XMGA8nzmc0AIa2/uIDLRdEgfNjzGPk
1vvLd5qUruKGHOJVsH0RzFMrBvEIk7B2Ttfod7bDOpDgzDLsYNlsmkKV8JYOfziiYOXfIoldh5h1
A5o43nxtDiAtXQpWV68vxMtw1vzTUyKqk5+3jD2ZDNe2pD/b3JOsYNnrrCzZ1KWwebR9EiIGE+oQ
JAvHtKqOmhW6p+Mte4zRdoDWSglMPm4vuNaBZ+i/aBexGPVwKJCt60Z57PZ+Wk2G5TJ3lxmGHUMZ
mIfJVNKYXiJneVSzC/oaUj/ug+4A4vHXyoDJevLAf51GM51RcYSC+jWf8Q2MN/RX3zL78rAN5tFt
s/FtHocEJ0iqudKnc4IkcVpKY4ccU3xyS1C8iRPHfn0UkqwawOfYAzt59yVWN8Uh7oqKUEdXo06V
FrQN7hshT2GbYuvzNdUGi62hVQY8dgmcrjExw1axzIRmZ0VqbJm0wfK1IS5TI03IvxvkSWxYGTcI
q+cGcoYkC5djeyZMSRAU888qTq3/eCvqKBoziOXhITyNGzICr8NHK47z8VIYX6c9cBy0ktfFTSLN
xxWC9O6okVGaK0FoAM/aq41dZwfZPTJ5eZ1UjZaGs3jJZsw5EAVFPfRW0VzQpjeJ5Qdpf5kwlBqT
WolQSYk5UFNqHZP2ZlqS/sSJ5tvGcPr+6Xl0XrzB6L4/cVy9FrsDDO+a4lInwxINhbYYLDE8moYL
0S+W7udtdi9bq5m4/OihrfkOi5ffi/2FED1XOBj5yYXHDvpJiwdBGFZK9n3bbQzsBgsEnNQediwC
++NqV6NV1aDuKw9jTfQa0o7WTVBPpsfoC15SoXwraTkj8SgGoKU4tTCShdN/vVqEWLYbmEb9qSa1
4XpY6cIBAD8z9PSAsHaPYuo7bJx9A9OXs183Q7panpWsAk487YIR9X0nwDyNZFKnU6TbsMTpA34N
cDDxZTmtvncIgPKGV1JoropuLjIPnBZkN8hH4Y7YcO9XtwDfaJzeiBD6/4pZ/sRXpbDacOZPJYbk
aClAZ0Nj4JNQxFWGsP3ST0q/2jgKkGG6f0doFlpwWn8wcrW1DY8W13tT/EWhF052V6KQEsd9Dgd7
s1bLZ63xyWI/DZDqSptQ6GikPxzg35WhExUMpLrv66ipJvvNyuf6Vq49zUeubS2AC4Gp96B+VRXk
drwxXDc6wJc4P32Jh1GN7CHUuVpiiSq+P20yAndjiZXxh+2EV6iQ4YQ/US95YTsP/bvmDO45iXbY
KDyn4BIO0Jv+119hz2Zjzu8wttCUX5W8KXu1DRVg9lQtM1iTDQzJ338PnJjEmenuL7iAT151sLmM
M/70d4ZFYvIn2t1ciJXnBpLyTMNsMZyWXt1eMuHNkrTt8V7I4JIZHNQnmJ0GMgsbjgWUiCoEh1IA
J6x7NwbmsJMquQPZ2AlGACba8WpXvvgylKUBbfQdjc9CxHifAyeLxi43duQDO4zDuIasObcs6XZF
edn0TmlnviVKOujX+smzRZVP5YmvoQIze8p8NI31jexUUV2OwfrIRCqqWepNIWjHzPyPcZ0mhtGS
IXsNqn8Vss7A1tB//xMVStJIvwsWrG0QQatRaTIrVxZZQESTSTzHG2523WlOvPEwT3O7BfjMxTt/
pWpu3adGskGTGm6mWCUV+oPvoqiK4CC5AaG4r2d4nlT68UekcmJ/7ISRHXvdIMsrfv52/TqFSHqz
hWTQoMVBpalZfHSWPOSmS4az0ImtzFua6p4n2kguQYfqRzCDe/Lr4FmlhadWqspH6RCZc2GVqT9f
FnhhsPF6UeVuVO1lICFNxu/vTmkzMnFmeQESS5jEqSj23Q0zDx1M/lYR3vhQdYPwM0rysoIVv53s
yP65NEJxL+WQt9p9SeD3ugxU+OvkenOxpCvLcRiL/kudNDb5xdC7ove2n3t4a3WBE9Oidq3LjzEH
aWkrazhREIbS4UYs0E5H18AUEG7d3ZqJOXz2Te+1hO/xNvMsDRdCuw3oAGpgIrIrDlfdNczX0OM7
GhYzMwdQJY10pm9OBPPR/Jmk2W8nUJoPFzK0jm5aljNZyuTHtR61dip+TKtaLaLCY1ryWRTRo/K3
HV5mZn8nbzBL3Eycvpxw1s70LWAiwaEGvjoSjbFhqRQzpwjPayQORakvWBHA1jRa/EIya4MNpKse
nOzKTFd2wwd6nXFpUzDmuZ3BcPzbLONDgbt/It9J5WUw4ihwlyhBvxYHGkQNywFf9rdTQqGAMKLP
wo0JnWjM13mNwlIAWWdtcTm1T5ED7nvkwiLI1NpLdFqwdMc0rPiN+946LvutYouR5+JchM60yNZX
B1xLCupxqc/Rb2VrCytvVgOxpHuHt3ZgGdqCXYsDYF4+3xLiHgwUIRQ1l2AepQbvktPdYUYmOtkT
uOOFwdl0BVVL6SQhnH7Y43fU89oVZgMDSIt6IZv2dFZjVJQjbRycpWs8EGFlihuLe6nVFG5NZbmF
dAtOJdncPfBd2mv2RQHI7tdLPrm3dDigVX8ntQksNHpbfu7hzeMZkCHCXA76jbVTv5j4e+KVYNPA
XAh5B4xsq7z4dy7wWiRcBJcak0VjWGaUPwaaggQRmU1YXPWEaOVwRqeLRqN/egkFk+knDVLw7MNY
OTDVbGaF7vI61oOKsQ/J8f60sn+4P+tG9zq1Vc8L3DUhQNWLWnqlni1Vdl/hAoDR2KsX1Jjx3epK
5dc1vvYfoUKDMghb2o+DYnDXthWWt0PZsolaCseyvtPPrtzjzZZjTWcnXbcLk8WEpP+CELYjsPFg
I5WZJ+8pFhsHgC2UT2GqbVlr65JlvDp+fzFtcTs2jPnwIK0uVzJEvAexwB1Z3riHMAFqEogbYpsE
CnsgOSj6m+4oOwxKJumHlqiYBCGpKOzvl6xZf03QF4c7dnX/bvJmSNDmAM3Aey8/GaNQjfQZLshC
b3NHE7htcbBkLMUdfxPl1RCAOdvwltlT6dqKLKDZI0VUnNBEGspts7i7GBIK33tP6C0VpZlzsF9b
TDXSBFqh14tNqiJIhh7IJjG9SdKB6kXCfY3HC2M7sT4G7vjHXpvTrdtXPbUQE4khbYllbOVJBj8X
H9L9yjSVvY+udsC6hhsNbqsD/fRPZmsUz5uRwAEmqVTJ+C+IONX5VG3WfQX0r6Fp6j27Tlh7E/y2
bCYIvCDCznoCACiT6bbgX7fWQy3jHYk0qW5ZrVz+EIrcAyD1Y1SG2tVpFmHLDItlgBia4AhQHK26
na5bg0grMoy1q7FmBjmlSIexntcqt1gAY1mmL7LZrbgzyEDOPCppm7JNnl5G5q8aQkg/Evoum0n7
LDeL+EsgKI+zn2sySO5TmXj1wsowUX+XBI1j5nNTG3ZwHflTvOfx8EpC4xinezWA4Czti8eocIVr
Le+62p3h3ifC4qU2zmkMhi28G6g60LrLpRoGaDUStmysdOBRUt+mFf3UjAWHDQ6ESXltjBEc+kMD
JwPK8glTgP9w1iA8J+VmbZz9IkbtWD4qQUjutsJFccb3r2xPE+oh6Hm1WYl0OQPuWqmRpS91D1jh
hV297xqrlXlosowJSr+VtfZ9UF2/ZTMxCC88leySvXSgJefabPeHYHDQeaHqVI2hDRGlD2lCykN0
euyuj+ortvjouMFuFKgrSWPEPidpP8pBSY0idpwwIJWhYkn24h3CYb/Qje5mwD/R+9/uhcUL3ae6
eIXcHmi3m0PMC1NI1hXQ8lExDqIp7HwccscV+NI9JdGlMmafr/Z+woTwgyHokK/2RXbGVzDynf+x
vZTES7/bGfbSe0c/a2aAGFqcVZ70JHbt/MzRLkkHWJHe8Ua9rDz1pBZt9NAS5RubhW0OEDyf6l2Q
FIHnaNfYBCbyHflv9wDe7V4Y9NTNPwSpCXF+Z+kEcnmuERwYmz4f2Qt5cmLCDSZz/wizP+w4MDXi
ZCCr2ZUy+uPqbT7ZboSXKgmPHiZm+Zu+9AWBXCBFsCv9jgcxf0TB+x2IKliHWBGks8g17IVfFubX
FEnLqo5gse7/yztFnKCiCstvOnH2+rO/OeYKliruL2BMMNVl/ydojdmiWsv4its4zln++pLzdRqJ
Zea+bohAXVZ+u3Mf5uob+cFa/vw5sSuKAJhC+LjF+4xYJHfnihTJgBFS9qFA9N0sY9rkbf6hXOPh
lfZsZyWjhumx7CHw474c1FYfyCr3hMmYaA9L9JLI6CZA9mWTGDD+hArxmHjfLU8K6jgq4LFp4UhG
cRZLiBYHOdAPk0pW6X07UtH2RTW+oCb4ZjmyPI8PexmZYr7Ibj70BYfkc9z1O1cvnT9W3qybF9Vo
ARkaEgP2FCCjNMWj4xst8C7ct8W48c6qK16XuzdlbfzL91XVZiHigKBsXh4nz+YXKVW+ZuGFOKw5
/ehJjkZmXgL6uNpm5YbcHl87TkQGVykZujwvA/sTtzOg5Ek2dNZl2WuAy8TDe4wCj2XqxBFWSNNP
ShBgaGfzCb4y7Aq4KucLi0PsbB+3xpjMUTik/ed0PWh6J0x+lHgrhgl+QuO9/3crYQBN1A5OQZps
KPpayICMZqNVonuUiiOtuOKJF9h+tkDIQELcY1T0Y0nuC71LEVjKlCgBcIHcPwTrhXbFeQqDdD5U
63DrioAHvDZiATDaPy7RXg5Y4XUKy1WThDUC8CqWacNWUXFkcFW18CtD0C+PSl76kQtOwRQKhNTT
KP74Ae5RBEhiJnEC3LYHu7zqswIwA0HD3nxgbVvXoh9QIhzuBiuix4jWREOz73YbnHzBw2WCiyIz
KDBtj/+AZRkY9W/A3arNJ0Xt4zifgDboODezCpdovX5gH+ZQEDVfjy7c6LtVSSAn7nwd1ZyjOlXx
/jcn1SHrin892s1hh69506ilXKcdO8+r0Y/55sCMoXuXDNlmpuZjepGzsPd8RK4TD/LDFDm/TycQ
FPz83eGdSwaRDuHcmNq6WWZF95Xkn3ZQracpAKPzNcEBb9BINNgchC6KNPtqVZVKDxUHvL/qDu8X
C5DACPTBL/X1X5h4gKD2uCcAP6BkNnpJB4mBQWqDzyDL5rB0pAhoPWoau5FLBJPuU7BYb4OwZcn3
dUytJPl66Djrw04dtZhjuh/nJlIeU6Mt361QNllTKgx6QlAdbSeGYjg4KcCGEVJ0LXWjFlkG7Rdq
827Hv08qqtJwM1k+H04vRr+lLRnwvTikXDrGD0SiL7R7k9CG4f+0mxNo7vZWL+z0MxO3pJPI83ft
MAX1Jc8ZyoekfPeJ/rQCwU6526EJ2OdQ82AF0/gmeHJ+ttR8sc7jKznRNafpaICPOD1+0u2RP/rd
Q8TayzJ9BWethXEXmElrPrLHiSCIL3Rx5GTXk4cnngaqRdALd5siuNYzICtqbIvYt/PVF01cF46v
2778Em5uJu5ySpw7NFMv1GxVR6ZcM8Xeurs3FTiNK+bi3VHbdd5itkkFaQAPi/fymoJnlq1fHOd0
1fThydkxc36CuTkuVkKURS8nqhdfcyRyeQi10JQBW0JrV+LVK7SgwY6SKhE18AMIeDL2bUrnq/yn
hZufVnyfpOtOegNytEZEVJ7akYHc5hSIqDZJrdrIMoLifvlGehJlrsZBkmZiYjh0EWrfnF7zLfhM
DzFdAaNjyuwaL3RDkemgiMFlcXy7/XHBTWB/dftqaDCAE6b3BANZyLwK/CZ2y6hPGfCmBKs9NgMX
KBCCurwD11JRUgIxMTOCzQ3yPx85wJK+/SOlxjCsNx+1xmZ3YagKq31uH03Wr0jeC7ePoouDyjzL
Af1XAI3wcmQ+qdycSnPLtpLRM2Kzueew3HYyhNv851rHWouAP8XMTho2cyOWDmnEH7H1/RiLqg4E
fm4zYfFkWR8M4SFApXo3koxfQCW5YcgkIEK0RfbMM3D7NSAfS0WHDiENHjNs1+JfBASR52U6TiWR
NT2SqAZFOSGLao+Ldf7GYoZbu0IkaeMHp816Q0bTsGG+x/6BaPzKdBeX1+6DTp1zLHOdRcHlJOCP
/4DYC/qbNb1pU976VaARK1Vq5VvR+8Z1/iAE3PDn+SQYXlM0Xl0GU7BF7Rk54KSXOfI1gy4/+97g
n5RYc8XnHEWDfCL+88stWcO/jIlPxv1lFsZf7vzty03d96MdqloNk8orbFnyle58JGs/aAgwGrgR
Rz0rhh97vLdtsSPkIyuNqBs020DKGYsH2s7fUWOY3F3QPbRxjrfFlTewk8lIEudaAboLJXRE+lv9
kUJd+OpQddXOeoTx9TlUavgs67muN2Yxu4oUbQzhllnw+4xi4dUSCuUxATEfUPM7fSUg9gSom4pC
BDr/9x8qY8EHhe9L2NbKgP0YBpxnb6+qGz92/ITExOqx9l42QVwnimZNbpi+Jv9XCX6IUUawbWoE
4Ul5oyqy8COThPryv7KDNwJOTt3egzqUfKnaO1eAatBWHmg7puzzkhHyoILgWg4g/7ZDU2TXxmOx
dIHnJ0Xdw522o5pna+7LfAM31TU66zx1o3r+esSdGcjz10mpHihyOt9AQWmPhrspyyyvxL0nBCsa
94eVCKVuJHLhiMwbhhOnj9O0HqbewiUmff9MkZsg4QRpzMUlvJrgxO2ufIdfmO5w1ovDCzKXnD4W
xchEqBZhMknf/jiB7Zu2bn0P0xBNCsapsFB9doHChMWkEeVfFUk8W0X2BJirxD+J2NKlBXCdHcLL
lTjcYPjWdnfXVuKaBK/U2wlHHu/Dis21YSM2bC5+VhkXeVoc+/Xq7AgKbpsPd/O2MBz0A/WGlY9b
Gs2EF0j6E9faSEWPHsqrJJxrqr6aBx9TeFXmdtLsjVHQEjaMotuy+VYYVmlj6rymV98vpxFwrQ5B
n2wjgGy3zOk4RW31GQ1jSa23vAfte4RV2UpXyOPWLwDeWTIdVtnMIhW5KDvjcK5opI3bOU0w43/k
zJrsC9QLu3q3EYXVSAYnxC1aFitLSDbAq+P58GkuatIZyBezMeL5C/AftvNBj+jfF+HqKxcZdQRG
xgBYQAVVfOnDX/O1GRIyj7CZHGUKZdy6uBnybWrWV41QSHM3irEus1/te1Ghrsr9vQCbF231Putn
Cup5YXNXkFBR7e0YoGmosZne5SM0a51rtLZUV0D49cJE5I4flGFxm3WGr3DIUVhyf2Lndd1ad6xc
e9+dYsNsUOTf0t++F8kUSKQHv0ya6vEI6Pwn8VRw04Wma2Ecwdn2lj68lBiOzd7FBkWkOpfHuOmm
vEc6Fyv/0wQDNAzpJStqEOEgCaMonufeaA8zWy+qLrUSA6/l3lvSyuMHvombIA/eL0t3Ko6DytU5
TO8bu/3BfzKuXEP18NbDffYkouztFyRE7xZdfCz1Jbndz0D4tqP3L/6/cGubAW+ls27sS5D1ZYNM
PXdT2fs6MsVgYOqUy6hwEa0ZQgubet0dbDi1OPw2vsG063XI85Qafxc2AQe2EJdbsm6TnSrm88mQ
kPeksR2Tgd7M6tXeLkrW6jQjJtzq9Mo5ycXUav4R+HxYZgJUmaA4lasMdLb/FZducDI7alNY7ky+
ZS0wS3KQXVSuDCfPNAsaaTUbXcJJnbJR77xYJlG3R/IvGdRHmiB5Fnj/NIC1bY3dZWDCJbakivRU
vD+anw160EVgssAAzwqiBSx9mNLUSVOW5UFuQdYLDk3GWDjajwWXrAQG0WMk7YR1p6kus+iLffTS
3QL67Hfg1Qto6cBPDR2GuwGuDA1EOb7VxIr/bfcqyoqL3QWFdQ2n9RYuKISJF2wIVchp14Fh/hAQ
OC3GoQvOg7bwFy8anPSACEQiidhqQLBUnqiUaRonLPpdGhmeKuof8tYKYs2mQg3/oddfGAvUGqIu
oPXI+fQhov1X012+zzx75UmSCUOPFe1ySnTeBHzjvrZKqZh2J9G9RRUZFYjDhUzqisXbg2QPG+0/
SFUvh+1QLAnlLX3wDveyjIsEfiJW7Y/I0rbm3WcOKOcQnqzIBU66tO6EQbn0IN2Fliv3c7TXQC1B
PTAaEH/3V2FZjUlcd9G983WZGLgqFCKGICTJEpAWCl/8gexEo9nf0PFjLZebCYCPUI1ckmhguQTc
iNxXY72QJExEsvJHf7TY7meaXRGqhFOOnrcCb+UanJr31KReEAiHvOSwtDrdt1/g79FU6LAobwVz
yTuHpOrstz9BvuYjJHeGCHuzVHAbM3XJfT2vH8UTI5s3nuhF6QhGQ9+/Rp0SKANtCEs1YPTT78Aj
829J/HIAV85TsW0Uk3xy7pBFgraP/r2WuBJtaOa6FGlNEugg/2Wnal5XHInMzzHQcVzy8Q22dwXr
7hLJNMyHS/IYCCauijZtNCEe8TOVl89AofJqwC/5J9CziX3DlEXpJAXmVGAiT6KzMRzhF0NJR4me
eWgeNcM4sAKOC0WGyZIU09DDE/fQ3sVM1oaUJi+LHGjjLsvlJ3fksNAibiGO960eJEk+fvXRwdiZ
s5xUz+Qv1KnTGsamzVsUCBTPdC+aVxr3Dz0PBpNzmhxSUTK1jd5c3Ft0VJtcxka9wsR6KYA3PhVy
5E16IAXlQIHG+4/yGwMSOSPLkDT6IJJtw8LfXTH7ul/KGIwNWxnOJ38D+jY5iyHmXBcCA1TWiHTX
qO+KWH+PtbiYn0h7j6qjju8tT6Bz+ceQ9VoieL821kC43Nh81XOMfk6CrwEdsDUgJKMIghfuVDxX
aLa7jlaD0pn6HzzlHzsETGCs9iU/dImc+4W6TisFX9c8190XJb0vhrltMlGAPMyur0o7ZA5Kn9bj
FU7hEaaZ51AJ0+7ql9zbaYDFgYKj2Ki9BJ/F3z7k4E1pxMiZsB38HuatS0BcRAJZhwwt3mn0FMp6
clO269l7I0PBSNeiAJWpRl+2atSJGP0Zvk9lsA5lpbhVTDF2wv+kEyJTvz2AizUJWYV753HF4l/w
5cb6Moc9jzxFXU0m5iiVfqKZLpA8ribGNY659toqP+Kf0uUHr3n1Z+XZBLZi7c1EQ4BcLeDaaTSf
NvAjISujPMQOUj2tJruibOdrNeTPwLqn5HEPM6ckTSyxOCfy87HstxNCzXFUrhyc8qbYiIGHD8OR
y7dNm8PyBszXgYcBzbIDRpagwxfuYxkRO762xl/PMVF7HkNL8BZ1eFYOHjlbBZ7PgF2bxEKN/N9D
qhOwTwF1Mdr038xfwAnwRxksHSFJvcyX28T5nRklzt1PJbBd+JKNxvRyb41uGvJ3vv9oyQZgKwnP
J3IcZ42ORhOh2F1CaV7R+XSahz4JKFR4Y6RVX/hZ0BpgcHxB+SbhOCSTJgILVd7MSTPbcpynaQId
sFS/+BezkeDwEiPFkPTv+ytISgMow0Ien6dkAAJMDNh/BAzMb76QICj8VeYJexsxn44WAKq5SeqB
XCIfPB9tFAc2jKWViaQRH6YaASILEJJHbXmqpq84Yx1IUXT77KP6BpidvkPGA4nIolx6XE7EwmTz
3Dcm2Z4KIhy20x0KY5kH+LzHKBXmnbECpcQhLhmcZi5GGXRs7P3S+pWUuoglS8cfYmjWnMt1t153
wX6CE5ngYVPmVqxO2V1IkTyYkjMdvFm/42Rr3odeij4bRMcS24w2Du9g1AwVR+oBnzb7ip91n/fQ
WX4f5aBVN1lgfsf6cwe1LaAO5HZu5lmuGvyCjB0KfuIYPiIf8n4WtY+oVcG25bRhWepUbqGJ2mZp
nu2j85EN6ZxfA0fIcd2fXUG79ynYMgCu9qYGFI8PvRwzE6QNgbrabG4q3WtACE2I+uaVIR7Q3BzA
2nMr+voFTSeck1vchR8aB9MycZBsUzWMGqV8N4+XenDud3HNG7ej+BAIgMevqMPil8nye+zXlClI
8LBsf7hIKI2w9RVBwxRj7f8/5eJ24+KBUOyKgHNzUFSE+tXFvBBDO5JCT/PXvZKvB7Z1eJ9+4NeU
wsl21Z4Pjy5zb8R4uWsR8iDqwIX1P6M2PS+DfYlXb5x8kNUu9b9FVo1tP9VFgRx/Ra+Df8YpmctT
KTnh85tPDkU6/N861bp+yygHtrxJpUOkYWRX0VdjAgRMSx+1WOX0iWIbEZFkS/425Er+GLjlaz8c
NY/+8Cdx6Re1hoxDd7jJIqvNgfm/VGwdF5r1eb6tli1mymT4GbCK7AzRnsbe5qPE16DiP+n2p/Sa
id9Jh8nIQ5ivgySVuO9eoRcgM4PINN484DcYQ/6PDSOSzVigtz+e0G59vWg4T3YAYz+YRycXVaOf
E4ny/2blUDOiiPfVCNlXl7ZswqBwJ/kF1YdfmMvhzCYstonwd75x33er0bJSzY83r1ULaF40fjXD
xefktBla/9e6EYJX1DtSMHM/uLVupxs0LJ+z3uFyZJYm8DsbFQQZXySDUcySh8xdlKde3VqczOcz
paS78IIkrDCQ/D+PStGkoSMHYbda61W+9LJYb43iTlLPJdVSIx2a4OHQaTpqUTFIvhmcxiPAkOGE
cy/R/ltfArHD6oryx8zzXGn/WONpovmXlP8x6DItqivjirSgJsaFcSSYQ5mx2pvgq8MZjdyPmYfE
HJ7FShVo65o47L2rkTWs2ObP+b4Bpu6Comns5+8IxMizA147+9R8a4e6scJz+Dhi56OmRsXDafrP
/0OxLmdSOlKBmFPeow2O+YnA8zVSi5tRcXBMpK7GOiEFFYXzK7Td8co3/RpV/+TKbE8GSaj5TOAA
EGdWdjGR+06JQ1B1O9IjgbbTjrVAcLzfCfP1ztTpzafhf09sDCnNY56udb6KevcOmDdBuKs3FNFj
JtO4UsHHUBOpkjVo6oTahY0mEvWT9WfAiLhz/UNWxna2LGi+F5cVQuitMOR38PssrwFjbBLllHip
56KN6X40scRCaxrzJTj53jvCh7UUQ+75YpK33NJJ0FqpAtuKDMi5RNbrHL5vXQgv1R7sp4gP7giw
P1Nzv6iy6ut/hfyJ5XSH/64OqKa1oW7jfraJbNsgrJDAxAMZJ1VlvAfBRUeJdqecJfhIuUEOZdZM
+Msk5HqgU2o3gVV9BEAuopILV6rmgSqPafiWErB3Y0DoPpKUsegj62vbpyL0SHo0l3Eox1l0N/Wz
ZesY7e31YW3KSc6I272ty9v+68j6KIQ1NFnXulFT/Nyiqm/OrrG7HGBG/lGoNF+8/eRyRbguzDz0
GzjLXUy01LjXrvbwiytAKkWYnpD7uFaDaXM2X0LqLx5VA59ttb6D5HIOIU5PPWW0y/JLMIBEgINM
tmAavLHLpoP6FptsqyjJwCfndaIb6YZr5raV6wIoB07JiltsV4e28ivlZF/8v1m2TIbXQnSCfnpw
DROtzPPJ6CngijjJZL7NqZkfCqeCd+/5iGIoXYYF5I5kHfoxgR7I0X1Gk2fHzOZ3sshqsAGHhRCJ
+pt9bBFVt7NsC3gpxjlz/kXJj9Ycny6p5DTYxAtjtKAHVDIlE+OTDiPcpKqZ6w7ZPfBWtxBMuXEo
lODGoo1kGX6lwDa/yFNdytbKCScRiVW4/Hg71g9ij2WVX7qHxEidZlwoHHrx7FHtI40G8yOX1VGZ
6wEzsNml6+XdPOuTLIqxVg0XnumYdpwK8MmPm2qCR6/jWap+Wdsta2J0JFqaTK89x/xeyKkWiNMn
2tjGeH0P+DQ46ghVF5eQTDKlcm8DeWzhhXnjr8aCs0YqkrljzGXnslbxRDtjtHOlGANUqhCJYJuO
65BKa27NiCTVAopiDiMJC++9hnQfTXw4EAJ8RzVaaLN7d4IRIuWRpMRo/coiesyFG0ZzPifBbX4Q
m8ZXOVcMBxsY5OYxBLoN4YTLRpfeBJr5xSEciwCI9Cs0qow9l2tUhptlBlVf3T1ao6sx4LM+z+3T
J8pI/s2ZGOEzh4/sAl4EYF6oSb+W5jK4s6v5Ic9oOdqrdPAg/nUeXVJpdTKzVmIhciMbMWbVLwGm
HVvEsVmRFG1IfamhyuKQffv3B3AUSEptqp9+YisOueG7zxOvkDvpgTfTnz2da4etWnzPpZ0OGWYz
mTg1cpIkOfZ4netSXe962DOu5j91dy7CquapM6UcT2elP5c/BA5anVjsCOrESXsgqwlcJNwbhJLJ
5vUneXmoPTAAv4DH2fAAw07naJ+icEaKVUIa//wUFFhL3exb1lGUgj9O6tImHrLfLyu3I6h5P4L7
9miFx+zU4CXhNswNcyNTyLGBLIS165lcGidclxGsmIWQQpW6BTB0MIxdwJ2ASl1Jr7vb/3J6slHr
mxh+zTLGwHmqkk/LxQWkXaFrjDxoFzgqzllYitntL8LbM61lt858Hv0PctCW+Io1Gv1sVazg/zXX
WioIcbouNO7jixv/WjKiE5ABOAU4lhOtc5TVFhsWBxilXPYlSWErX6h9feRhUI41+e8LZbrZfLaL
+c6quaOUEh2Bm9EoRcbUHkU1vtW/uIHMcNI2ZWz2hdKpj3l0Rn9lChpymRgMt/tvDheVVfIF1H/w
uCee6QwA56CX88S9Nm4X25TNu/v50M+bmswMZ3pUkmph3bz6Jw5lBszDukLze+JCqfk1of8NqRde
ecZc4h8lWxOhKXCt6+TjLhcJQ1Ck08zoh345quwnMv9wLlceinuI8qdZjahjIrEOP/fUDbJNO6AZ
r2WqBwPVolTAjPeGiWkV5nmCC7GIbjZeQR9LtkLrT3ReLa2QYlIo57E0GPThPwFMJGVkjDEs1GLo
qwKU9NW3EY0G2Mc2kO5sS+uy3epmpFNq6UtWXqWKPPpBUz+Vj6YqwollTN68TSZDQeaEulhmTMaH
F5c0y1wYko3gg3EpDLgeLB7DK9Fkatb2RUjLLG6/UIfFDymI+9T3/MrY47jGhoKBDyEIwUN74QkC
f4kzGzlY1MVDpfZZ/fef/h0tV4uCTfFnn9xmhKwgD3amTJ7sW1eB40sFgpiVczkny87/6tqFNrtY
YIV8BBnpmqqAkC7KpykfjwB9zJyM6sC4hnC1yjajMFPJ/ckGPv7ljB+kNTGGXfFu2kQ+XE0IAblL
9E8UwSfudT7i2miXi7SKPF0+VV2UEu6aqlLe6UE9aHAk6Zaug9Dhevyq3savu2AT0clhgoE1896Y
NEs1MlsZOvhlIUtuY/MaxMw3VVg2RxWrtvQfrw5AVbNtUYFBTL1N77hJZqGopuI+Ecjw1QmAuUYe
RLXFL+/Xl69MI6eQ6YHjTjnn2M2QEpl/PM7jpLKdU5hmTt1aJOysMHtJhEEG2qmkPhniCULA0U4b
0ahDoXcnyGCtx7WHCG/h0ELXCZfAYAg4Yc8yPPEQMQi+O6x+qfIEZsgcCD/Jq/JzTYpNXISBUn5/
yX44voMxqJwJ7QCp1n3BW8vg6vjRZISt01QVjQXGrTm03UCr/YiUTgAbrZykX3JF80/Va2jAhv4y
XHf0WTImBA1dVHaP+dTUqCMYtuWHUrH08ZVOEOg/o898znOGyuFB3SdC9ewq5t9Rq7OXrnz/Suq6
8coLHs46eTSsI5xzn6Wzvz9tqiJHZK3qE9FUQiqvimKU88HDleNSQKYFY6xe7D5eZBX7yLDYrlKB
6GSXxseDwbsWGd3qkfGiOqqHcThIWusPViTVlWTFjAHZfLwNeQlC52GErn/G3Izwrm1TX+0lP0k7
1MZIEST6IzO5nea7eCo3xCyQmJdGtykXcBgJ5LD0h6P0BO/Ezq4j5dsHVsQ/0UdSc9MJtTFuYyFJ
Wh25axu1y+MPA2VhcE+kjJSD/bW/+fxC7Qay2Rok4ij9fUb/GsTJM58Us2H7evdz2hiE2/feVGln
VjK/0IlNmL/T3c6IRk0et+QOWeUXfXdn0O/BFQ24ObxHGk9CUnty9BQD0rHf6iuDivT9FGu1Q9LN
9av10Ih1q47E4u6co+gi8yggQjJ31rce8ePMWxdlKjSgqdUDUHgopjP6JtXH3o6ZAXdPPaeGZc4f
ooRI0D57tL4yU9eikmXp85Pv2RN2DOKmy1ouO6GLBUE1yA9TVxgVJrr3jWMBys9wsjVvMpCmweHa
Ah3IkHu34FNH5wNCJELnoCwavMzno21lD0QdVJUBl383RLOwecu3xabKSrjLhtg2lZe3L7V0ArPs
//sKCryQq/cKh0Ti6JZTG2ceK28PtQebWTBVbT8G2mvJhoYy11TCo5ldQ6PO9zdCOE5B4Bw35Eew
u5fI2FX0f7WKmfzaT8VVT5og1yRRr2VhpWfpprHYmvCKUVFE+O61ighuPoY9yoANTc8M+mZa5m4U
FWqhCtopmHz1qJ9vN+MYzPAc3mnDXQLjBMp0n8F9hxgy03gSbSUBOzflsRPeDdqJm0lkeDCzftWn
XZ2QlKSu6lTULDlhpI+rfzMSxokaUA5cFZM5L9j2m0Xxx1einBg3ZzoWiXFfP17py1N+dLMWWqo1
Q41Y4XoABg6b5iKsl15RQbLEHZMxmDAWtCRqnoD4FptA8bzQDMIW+RSHxbg8d6VLAcuDzXESYGdG
yknpbcLTlGTpugFli95CIP7FV98GkUmu/RGfSMpbW6c+z23KGLrkkJx92/WpQfGuQta8cSfxGWHt
foapZLmV6qnWUp+kyXlKl8JeJxbSFx0Ha9JlEbty3fIducdkTIQ5AOdD5d5xk+Otux2V4Xh2SPi3
2wYSZXb8vwJed+6UU4KKmkTaoCWRG2XON0/e2FAD+vz6xK0T0//5MXANa2TR57/jqYsUGvFRs6Jz
/1Vtmeb6S58Y4jWPPLn/EOO6lvrW9CAEoMXiWyGSezIWo1Tg2agNsxveTfQYdd6jGVgruhwlTR7s
LUotHPSUw8bbFdWxkVguaH5+SDuNEmoYsNknL/9NE3lM5PZZ9KddXr2jqaz9KIYxOev14pNdB/4B
DDZS0c5o8Ph8VJUmfaOsuO1b/tY/c6aB3zdZefT2u6EoM/ShL7j/ZwIA/iLTA6vz0z2WClT0YhG3
SJnj7S56QLc6rlVZre0fCw/LdCyDlP2Ay6+Rp6EW0dpibSVQVwqrwLDl99Bhd9wYAIIpfrm+ohPp
PhLrFyFQUWGEACmyvopLxM8ziqupk/RdNYhcdNzJdm7helUYyUSqs3VnWGMIlgG12cvWbuZecA88
iP4qRU6FJCCuQchyf0w97va0VyfDbeAUjmqFLnmH2n+NUjPC3+Npct2rlwGBcUbahaitI9lHXYMZ
rdnf0VAvIP1PjN41vPyCFeuN9UrxV6+8r+tBqz0gqHYHX2oaGqFwErUXIQl13fYmPfTLutGfvkvB
Y2t6PsRtUt+xaxz3qh6KlrYjxBuA3PT8/PUcQPE3Q+WzPYXPiJ6cE1HupEnhZ492WMIiHA+U4QVY
sFy23P/JJk7NuzauJnZRhglpRkggc6AMQQBvux1I0qKS7GkqoFjvTzHOcdtvSoAccq0p3NI7DTkC
wKWhSFJjpRqNpIcuW11piIdBTVp9xePkc+I9i2cu5jzL+ZKQhCTG3CxfBsACDJa1qewvaDlWw5XA
UeK6g59Nk5suy55cTNgtiLDzRkzG4beei8lDWb8ji3EwoYSnQxWyfYk3k1tP3tE7/nqAn/oLlBQu
wcYfYYxmbporSJzMwofan8obbRU5Afh99rYPy5EX3T7PI03AsfN2J+eZ5SwSsSChdQCJSyc64KIP
s/XbxJuTncntr3O7HxjE0UHeihwXPGzGvnbGZ6DsnTnd0U8m5yEYApWqztITsOZndtoXLyWdDlC5
hljSyXgptOe7E+LsubNgYvdcCr5JQQ/kSkpvSW64Gv7ouIJDJXzQBF0fDr+KCQ7x2s3oovqzjZlG
mQNnLwimlcK40Q5Os/+Zulds+M8f6YpkKnEQYomAQcATCVEo7F61dDRYsYP/wIaDORrsv0A+mz3N
uu9CUXEaai6A09f0p8z9to2I6rJcQEzJh/gH3qg/1INuMIi73ETLtJItRtTehr+q7sJPKn9H1JVl
ZKdX/a0LAVqMNRfpoZvZZp82Ama2FaJ0ybo2ECH4zoFHESyy/WCzG8Y7OekWYnbWnFetGEguJkmb
c0ehHpBxcmu5KSdWDJgIqmIuGg1Y1kOFH+TiYJTfFfXu4jVZFzn/n/Vkf12feu5YIRfJ3S3P0QUN
lQtnjXPw5LuqxQlQsIg3PoKwHKe/pvVL7K51r/hUb7b0E8lG3TMWiPxaA/6JFdOHp0RZ1t3hS/d5
Eqmxe1+3ALR6QsdSZMChaNtmclo1PBmJ8L3RIqCSHk1kgesJbW+l32gXUtxT0RZkHZoii5AFNTuk
W7UOmEo/tAEtfj051p3bt6GUeRjR2OnZhfaRsLtT1BPwG66HhUASX3rHJ1glzr/oyZ/aFfurrGWV
MOATFhl3L6XFF1THoJqmLTfKiQXzGHYvMBQmv1asLgILRp9siodc5FSt7LlL+uUMZvkEZOy6PAps
a2kMD8K4PN74SMWy3cKDWG+B7tRQkyhNMo5hNHpuTm2bNYv/lAD0Ud2JNiJMpjUk/rszN/3QzCUf
FEApSTq2AECv7xrXL8SwDmw1fQOPgBzg945Xdh5gYe70OlgKZuKi9J+V0Yk4A7dzFgfNKB0xRecE
+YUbudm0uyCEYNwY1BqFBX6hbrabSF2Y9o3KJRVjC0U4yMGrPAEgkxH4M5ch9lmunpBxJqjkomin
R6LdMItnbNTi69NByiNeV8ktH6vxgaNwgPUXwOzJ/qCqcpyFRb7d207DvLmCPv/9YzMbYz6G/sIl
KBbd9gyeYw6T5AngllbNFbWwRtpVCYWm1DSvLQ9yOewXPuKxt2xq6k4FXj8Yf+1KMvxHwKiz2ny8
q2P9nq/yfViB/ZQSQt/khi8aXbUTgSXiwOwfw0gSaOpftlA3JDL+D8RyXT7d9tjmTxOc8mowJaqh
M1BDI339opi91Ph/bGP8+7JO+cFB9C9a/+r7ykhHyk7m3cGKeAHK9A0rARY5XzlRP8/mhpk61P3I
R6bZxuISn7iG6HzYbrf+7qMzl+cSTv+hZdJ1Ci+6grEpDPwNX34iZdkW5/rFHp9peDp1MjCS4ovS
Zap9nj4XLN41RRYYxbsTgitSt0F6OfJyMblmGqbtDkvlPz6LFxXeB0//yNs/eWHO0ZCTzz+Gh3Rl
W/33/3zyLpe6Mu7IW1/BWuXjsOlBcYl9JOsviDck8edZbSANl1EqnWyXTecQmGVF8jTGIiBNk5z1
uvMjz+QhQAU73qYa4/hGktw8tEfrdHwr6MVsuWbaDoPhbYZcu+xekw9j25OEYSAmP2vMYyFpr3ll
3s8KwFlZZ2jjZ7wCz2nNX9B159hK54S7DSykGeZQgX/WijBS/0w5eHVI6H5kWkAuMrDkZ4ohRI5i
m+sm5oTkRIa7szH8AMEMpEnazVlk3MBx99tD8llOlR5VLVmRuQ8aNg75Aa3ma/gD3wzhjZrFQWP1
/12mfkxMGFErSaDjAjPlANTZ6YiVtqHH+J4NOTBouTtfJrxFBGfyTs5xA9sCz5LOhN33hsdKKb1A
C2znM6BNT4nWQVGD1vVvbE7LpPdCA4L7jIkblus3nIY98M+9u4E3/K21/xAT2nANkFmazR7Pu1eW
16eKy21U1RANU6S9BX7BYepTTtJzBeM8v+KxhxuRbKjC89Q5rkkWbdGnw9TeQPTbXE3OJ6Q//ltu
QvIiHjlz7w+1pL/vjQqPcpiLFvnbZjheXSAMRIWftnkC6QeCQh6JYghiA7008i6m5ftWGgFGQ0uJ
Zpwe+WRkWbLVKTnm1Gk4uxzpYODerb4lnApsYViitwAUsjx6AvYZpeszwS1U+S58hnX1vImdGOsx
foDx4PriNSsBuZuMxDoeViSDvF5heVdYkG/Eca5gTeuoISJ4KKB/052p6baJvWlghLCQJIiQe9Uy
DExBl6hseMejfS+h1jEEoKnYyel9mSe8bYPWcJHgL4223KYl0T20xy847LMA8uu5dc1ykoWWH846
0Ak6oexdmkzO5uU/lfcWVLAkq5IIOaovqSP20QXW82c1T4FLDy4KaUthQYpEWDu7BMgAhMQL9uBa
JGmkah6ChijKRtEnhdcvwfl/gVQ9lvQ5uyyM0KFqAY88+VDW7hUICEY3gUmLsz7ffwWgFclotj4U
RyLJegsDTVpHLkJNvYw6Gbn0rxpf8BUyhAMeZEBM7pZnAtxF95HB7BZFBSr9h0EQtEQEUemyYGrX
ihahh/egQC74+6EXs6u/vXYnVZNGrVd/+DI38znwvwCmjiSn3xcBWrr+VZ7jDamhVdQyaGK62RT2
9uocOmK5oAhpMH+QBvPOM2kqtJcQRCW8C0lO2ZHnFlc19tH8nIU4uC1esmiShsVEYxMM9WtmY3UH
h41wQVJ3VIwVbBbG9q4FuvYRXRVcdDZN9HY0T3L230k3lNYiT94bF9w4thXX8CRqR8nrtxlIGlQv
pcpaYKe1/0vv2Cio1AkWq3qZan9r9YGwycuKHrM3TrbHspzfTC7gPm1qEjFdxDIIB6RxoHuBcqBh
aa6OfzzR+kLkqOP5OpJSooatCvSmran/a7LqaeIO/FXjC2S5PHM2YyAV84JAeUIqmFYYjwLDuh/3
bbxDzBJVz+NPkAx35xSEz4sq2HWlGL1UVtv/c7/4tzCMUxlQV3v+7RFiSl4Wg4VCgA8bGXSq04kt
YDj0n1bvNLWt78bFbRUirHcKG0MxjHNhOPzoSMwR/aL2++S8/rTgG2lxvcmqMGDq4CbZww1uNwQR
Ho3cW439KGyh+ofzpFZ5WBVzmePvV/3aN7VTX6ZQ95Xuykn6tlIHeF86TYyEOUTBHAy/Y8Wtqj0z
yJcFYPtdNXWJSE6ZCAe9wEUNAC+WuYCg71NjEjLu+hP4aaUluPY4QO3Agwu0ojHcPT5ONudVJEHZ
7aoZG9a12ULtTmFniUHPzWJwCZ1JdMuEcI91z+i0309wBXklufmIoY1LVTYzMwaIKwnykf7hxFD8
kYo7NA+M/lpEG2CYUsySb64GO7Lks7TL/KI4ccnfcSqf3uLLi2EWO5WY8SplsbQ0KJcMM6rH/2fN
ikS7r8X+FDeV7uqJSRCdG/1L72f4v7qthvetTYYi2fx09NgRxokbQ4eybVh8WbNNsf5WgrJ0Tfg/
52RILkCSr3VttYBElkO2HgI27Hg+asLjY7Yn15QvGlN0vuPVG4HJduupiCwPi0dsIhKHrnObiJkV
5UnyrJpApjqmVE4FJqKmmuK3mp5NI7ALgE/VZSQbEreQAQvXnlxK2evGcHfGV1fCTRiIO7+l6NF/
UdlYheEodBZmuMfDlapHMbDiLD7rU5Yh+Es+OdGCSKkCqSeZpUCsvE6RLkCmiBJ7KRWjY0a+PRga
M5r//O0tOwCLxXo4I4+Mi/5nzbsm3bKzmaQLcUsfQ9zUmfTRY2FlYsMENDpqsC5G/qAO2AOt2tSZ
vfXsZnglQi1WYukMxsa5jJijNSX04G6lOfvX9HojOnI8aGvUXssQwnMKgwh5+yXgLNxCqnj9eMZa
ZcHnvZwyTWrxaIsPkjnaIh7hYlgaeH51MxApKVZdJsmtRzB8XFo3EZKdwU/q14/xtG+QSseSObIL
lZJmpTQUeD+I3FoKMf2/DdsYesC7gVG6dEoSke3/KeZuMLfaEkeEF4ieQPBklD8KEmv5Oq0LcLkr
HuS2fakx5jzr3CnVrU47r52X1KTfVSghantIgWYcZno4l7fj3B84SLUNMuv0qVT/OMGSwEGNKXCE
nguFQsKr/u+DFBUYMws5GeIgyH4amqjGe0JlTu0P4/npHBBFRJZ77TlMyjv3qGCwpfGyf9+QqzdV
+gDApxS08yOQ0EyZeJC0rCz+a/RaDvJm0SRtoGfKJGCokwvkdRNsVV0JJmzL0d9UbVYLqriOeLcS
IfkdtEIzg/S6krQjoVtUhdzAgBaREIovFPyeAW4NUr49NkEowvBbJhbIZBZKQTpDcBg58zusOCQc
tuijTvJGd9mNYueQkbF+A6c0tx2zKTlQfl5nFCsoPHgMxUEAIRj4C+8XhjKEjir8H/91mrYBEmRm
/2S5b4Gbo8LT+dzr4fqrroEuaPh6NLfsdfJcvCTW6Q7Bx3QulIf3KYx8ihgbca8C7kYt/OHCJMTc
L6Di1NBamPgKwBJwM3AE7EgSANoVFWhAwi9OrQHWi4j98zcvvcIhvq17eSV2yKYcjLvriqY+h0qO
c6C7ouf+aHa3V+lC5eVWP991Vqq/Gj/UqA1aXIyFGsC8zuW2uoAy6bFt3abzFppJS9R9NbnNo3q5
M6oq2EebCvHW6vAP72VEQ7hVBH83Q6vzRfpuA+r/dz485KXM4jFR+Ts3GKxMHnHfvlJezEit07Mj
J3F8r9C5CDb1MIUy4F5yC0cOEaFPtwJZ0EnrqvMf+OnHRYZRawo6Aw+GBKAJWwaXTjDzgbgoXuyS
nPIdLsBiEK9EhUUYmp9rH0M0WqEKCUUg3jGsRBIIBDCleBmKbiyZqM4p9DlFCF6J7qCFoPjPk84V
y2TRV/BJTJ/N3Bjw3XHlwz0kkNEwDlCii+qDBUVcMsC3SAF4wYIf0pcmWdSl2eMw6YRG+xx9yQgY
T7suFL6KuYQOSW1sEqf6ZBq2gk8SmTeU3sQyJpj6gbOJsaYeu9MUmYORjaEqWkgrGdgB7c5NNDoP
hxDtUSfwRyNuuDTy2u2RLORj5HmeTmEl2jxIpr5shB4wEm3iusTf1rgzSMjoSUqrHT0rG/P/Yvel
Bl/vcOHIJI8Q6Z7Y0Y7DZkciVvC627GSB4/benw8kl3ivtMameaeB7uTLZxQA9/F+QQ8zh+13SQj
v83hUr4h4O4Y0nP7JFr12cm1hpzMb9pXsivN1nrxU4xoPVD8Oxee+eU9MsaPVJxY7TBZeI/HcLAj
okKbtLBu11zVeBWtepesy2InOdEPHguQr7FJHfGINfgDu5SyMZb/ouDb+qPEadxbWG2a1Kk3Znnb
Nn2W+OQTEUeKhlHVBcYikLuQCxP2SW9tD/oVwibhq/UKV2SOaI/vvatBUka+9forz/h21pzAlHV3
0cKu3VnUan9d4m9odN6GLErYSQIymx5jYuaZCUU+u1u2vfoU1UG82VLgQcdBla7ASzDteZ3cvZ+w
JnhjKcogIWWcRbdz7jMc5rkFYegItvLH3cscLa3Rk+dTcZgm7Y3Gghu7keYVF0Bx1mcz4JTXg4LY
jlGrs/ZuEsC7sGEcdqg7T8LPt7ORp6LX0rCTq8uP6bDoqpKfL8JMMPFxqM8NzeQZXcXde3XjzKBW
qQcSLCEeqAFCzqVT0tziW+CnyyMThxLYXyDgSUG3F2kJRjHh38LScMD0fE+VogDdauxTckVU+roP
ZHZsq4o2kKnc2wXS0uVYM3TVNIfxbWW50bqpqbYAV23mKVJVRDKFE3j2rBFrvuQqZvnIt3Qx7cIf
ugPFpMZbi3uL5ad+pB7/MmhEJQJPRFXnJjhcj+QTLyG+78kZeh53U49DPRY88oo8U8CTtVlRnjV3
Twot3suM9C5X2QElcTqpZihe5c3O3kXzFAvTK9vgDNWOrpUHPnEHEOziMMlfQmKg3pTjUUXePkP6
5n6S8W4k8aq2XBUZG2OntcJnmwet+Z26+Hf/Bf56TF8dje+LwlpHej54c5/96/jn09HSJ+2jMkt/
ZWU0kWXQDuqjrTp2E4PlHpbeMeMKgF2Q0lH84PVbgJVKAhzymAsRXmcCj9GK9pAHEq7+EthArtGO
uXN7M2SwICQzmxYpGKNcyeNmggYkoj/m3G+6jnh1SzwoaKsV4FzvTUxTQX+O4DV+r+bQx4js33qH
e9dp7uvYa9K2mqq4Y0y1qt3ShoLfNXF5KTwqYvuUYdj9fHkS6waBR8yK31mPWhd5aeGCc/qj+MmY
YFsshiGVYkSLn8TAxY9v2f9Dg1eOgsbgfxgcB+/JilDOcfyEJSpW+7jhe4WILyj8d6yUP8XW3/IM
liC7oegzptogB7XaNB437NvfxMbI/6PCTxa1T/Z7e5Bn19xZIuQpEajblXTq7uYn+eEbHANIHTfA
p3tNDnXsDEOrX8czis1gODe0BgzzRCGM44c1LemZ4XIGdCSdT4DYLm8xkRSBjnV544lMc69RHhUM
VHL7sIQq5ZIzRSP2FoYaXfBaSwL9OUGX1+Is6uF6kR2q6LL6XE8RJCyAsUNSBeh1LvAeElLr77aC
lUaL/Dt2zTVBNvMNgrNvAphSj9/2ACg+C/F0+zHt0R18K9oXWi6LUXb3cfmEkEWXt4BRFcIcud9I
cFwr3vBJ5b5FSNJK08XnRPN20YmrW+4IQWECvQqfEdsurTri1Bike64+dEmuI//74mEamgyNFgS6
yWSVG0Y78PPkoDxJWXS4SqY+kFeRAloEKeHngisJ+RSpfatg64x4acWwzMNNeC+9wUDjK12jdH/f
BLnGAbVZXPmwz1qxRVILlRPk26tBeCAq4sqUrqxLRFfasRmFx4CjWxWj0+a5RCPd7H7/n3BnbG5r
dTVffZN92uIPt6755gF4UdJJUT68yG8SV+rXI+yPKKsnojPOvyQuqF4k4I+/2juGFBCCPuH06dUN
VOCZrjxz0+uT4iYm5z37UzlFbofEfkY/Z6XYf9boSXlkAX7LxAihkho6pYGXuYCwnkDki5yGU1FY
yV0e0y8oyoBVQ9bB/k/T6sHJ+uC0Mqenl/jqKe+eEaYxmuws9PxnMJFPoni/Z8WyH6O9OHxPsFdM
QG5jEAr7Rod+Vj2t/uagzeU4AGWwN+EPPYua7fReK8VPr62pDyfudVmlzBMKvUCPXBM129rUyq0n
X1kIjzZ1qPT8YWN37kTf7n5J+NsGig54o3O36qH8GSMF88jj9wuTp01HuI/7jlF0l05YDEkstl4V
Jg6mSxBq/rkfS0L29wIoa2qmYQV58tYU5sAEplWPWNnYaprERUfhS9KyO+xk4xsfuF9nEoyCgfGc
n9GNK8JMkIkGy8K4bNYrLdSWXXnk04yOjgxP/oC/jDbQ0hSPMGP9To2JUZdeVOErYS9P6eZMt97K
Qmr5vGBAzQ5cVJnoD3jzBLOB95lRZdx3+LMnguVfHM7l9jJov7FVH9x/au/eCHbPXLC/vnZa3ylQ
QYyAIAj2t3fbV4Uje9XkWjMxrOtEmO8c4AYEWk6Ww4syB0BLZn88qHwP6UDniIuflwhsAMFMPKmI
7j+aiPzkgp7MK900WSZP45/TfGp2Y9ka2mj0oe1lMgXQSChKGUzvOK+Ovaghsfz4fCQHRHSigZL7
ZtocD1u0QMOUnWLNYZBG/3K+bjJvJbyL28qaG1zyi9h6mFaKZQNKemh26ujt0J6pqnA28kSxHO5m
4SI4vz1iToZpH5cwXQVUVzr9fZ+FLudHHBPtxkn9s6Zwri/ymTtX0ZQTiTic2pC/lx5zbLVn2se7
Qn2o+av+UHSSgQYF4mKQ/NXc9BoKbw1bQ6yiMwdMBAnbnJRZPWEOO2hdEwNn8/9wGU3XpQlQG0li
27NCZIHzOunNWBOr5BTkkbXfGZGL6cT9jA4O8TGmm4zFQlhAhbPr/vPHZ9vvK8xR3mFkkIel9Waa
Y2eKXQ4QakAmQj5b3k94bCoJmNv8uK7fQ2/Ww8gRaFDzYrYq/FuoZWms5ZSaNxC/3BchPtt0iEWw
kiHqsEsw3wpl8za7Ic/VGZJh9bWvINJ/ZI16EMF7iuUwHscxffoM9rx2Zsy2xF7P32EvKTR1/EzZ
NYqhp9U1NvCYa3dU1XURmNi+pBkUxKOSWSxHiTQGGA3NB6fYgqxThaBJS/e1NVUcnwjXSTU4kYGR
TIHlvxSPFXX8N3xZfpfPA1KImOrDkB4+TlpuCoIoZF3YwOllizREAPBT68H1IZBUuRpXwPxi1ngQ
Lya7+A78bAt0idIMeIwFFjXiYaAbMZPY9ARtGuskE9WOCFdYGvwL7/U9V0ym9pWeUIpTGkqRqjaK
x0srxYLJagoZ2gC0kJ6d1W5c6rkOvsCpzYwmk0H7eySlNOmkb0xcp2nRBtf2yU4NsMuG5Scz8Ugl
fIsLT78bH9gWW6HE0LNkq+mM4/XuSg0SdT1bYQTDl3+qi51/+9sRE0JGUKsk414gojdiFgso9KSI
yeyOv2hWyqSLoJnY8js4U8433876GEAojGImWJCM1xKsa9bGLvgaPVTKsTWl8jSGV4NrtvyZXoBw
U25LpkSRV28LhcmFfUSlKc0QaP+gyCUuVmjvGRDgC/Z8XzgqFpfRXYpwXfKT9cYqatu10sgDzzBh
DY/AQw2JH2+6ssKWLcnys4wY5Jj4w2unxnsxjayXfcrI3HCWsBO1hF5wFlkIqjLBI5y9pOXw4KRX
y7wBHXfugSa+TPgL5GyuLZigCZsRDMTMtH5UHfEDD58X3rM4bU5KmLKXp7BQfzRg5R/WJEIdBb2i
q54+dn1W+J64kCFJzNxTXobdtHMunVRgrFDq1U9bd2i0X+Lk/bYfTqDv2M0iILTU8hI0AUzBocGy
NbkDLQERXUkSH2/7IaTtZQ1r2MRpAVyQELAzgf+ocUrg+oZrwKdB/rg0XL3t4qWWEnKBpxB9W+l3
IGWrnZX9t31apnGqxhSnF+swwon5hEMVevrCiSJpGifuFv8If0MMCXndiGTew+pAym4VA+ZjBH7V
zmNhMDA89aSUHkpLwk+hxhQD0vous5wofA597K6tQYRMAGJ1adlamr2vMVosqnYGHTdI0Wblruv6
fj94I0iiR/QzsxtubRu1cCeGqWKVnh7TPNX2L4jW6Y4c9QXHDtBt9r+kRF3IZxr27qyvs0sqfl2b
5sGettybwLNIVBMtwg9UgH1mEzDa6LD3R++z1Qf9rbAafcIuaqQuiCSAw2xGewTav8iqR9CgKSC1
CbxiA2KYn6uvMC/bF1BYucCTtRiOz6Upv++tDpPIvTiJWW3GvlVZQNf0ArZv3FHbtmAEX5h3THND
dLM/UKbwQZddBcnUkOrftJ2kipwhzXciONOwC5H9GG3n0pUfh5XTqpCPt/hkkGerbtt6hH+6S46P
AL+Go7FkUNgvr7CNtSxdXeGfhk2Gg8o2W0cn4iUogpZ8ICFYoineN0t+1nc9cCCbh/P5VEvFSDdL
8/MDoiDsLBOm3Vy+JQH60CdeGVmo1kMGE3algyGCFwvXgU1HEIddLBzkwVHHeyNAnq8o7QkB1feY
hnoq+B3njta+4pfI6uWjZrQFtURJ07Jke3Rw0mIZDPViBuWVvhgJ31b4oDLcJEunVzDYwwlzxT2f
MPPP4tTZvDk7i4UP4/Ib/QwzRsT6DOSuwiFXmdN8uIQNGCbR8xTFkStiIzIWUv6lPH9pVRJqBTmc
IdcCf1VLhWmuz5PlUKYl721K8v/pgN76w5xgXjTCLpSWFLvH/T/NvKjM0r3e8iY1b76EJnKleFNG
MJJUrR5P9xWjtiGah10ElmH/7qenFKGCc5ZiwuhpkG/JyBA+NzCOE7nNhAZ7nMwA6lIYbbjGbIYS
9zvq7l3gdhHuDjMRDgdo22ls4IVQ5++92u/Hm6nisQKotI+NBUl8FVYZphC2qdtI1LyCXuzkkB16
Afbe+S74s9LBYs1hQRI44tAqjAh4UAQDjKEcSxCopw9dAgixKKLbbjJewzJzMQ8wDSwYTlz5t916
KiK+ls2D3+5HPQYJMdDxB8fysDH9h9UfvtF2YScYa2Xycn4LT1rMOyA4FRIDLkAWy7GEMaDR2B1N
UsFYs/L8A0OaJKv4yZQM/qCd6/METpW5fQH70lli6RBwOFOKFvBn/ltnEi7pF3htMpQJTwSuFNEo
c1vsk0N2Alc0stbXl9Z3BWJIcYOc0TFe0zvtaVOAqtNGf56FdvCwDWKuh+vSPHN1e64o9D84K0e2
E/Q2SdurhGHEzgdXpj2oP95cBES7Du/C1sI5qZ8wrAwYf1Lozdl25twLKAwMkKYYZGMXHSdGXgxe
nftQrN2LwjwTMdugDx/30DkEgiUTwZ34GUGY/bCx+faMA8or+tI5SnSFx+6/7oKIUsIWZsI+KJlL
LrAnVesvCicCA8HLN2webiQx44XJzvj85aZR7xRbGERkSJyssa0v629buMWHAL37T+gJMSll91H/
SeLezYgjDvA8RUvYM1XbVzfuxLBw+MlnZlltK0pGdSvl5b7znKnV4dcPOJudDc6EzwKG6wXoC/zk
C9X2D0ULdmfdVd5wNb1Qxihx7/9OBMynNyE1nCfxIBtsnR/9PJjx6XPI4p8gXV5YTRUShJxIPj4l
METY05wDQ+8FTJoixNEYHFO3XoFH5VIvI0apnf5iATZTbkTRIjn+vr8zZ+YA1zVxkxdtnoLPa64n
6GBorgLHadf+QJXS6rEifDpgdIEbIDm/kA2sOMmwp654IrjiTROtLmqh0GSXzmEn/OXYOunjbymi
dkb6tSr6BdlvNrSqDXfKbGZ1c2ACRVMF/Gxboh3zzPomhJc5FFIKvBLDnTkXIemx+k54YttQhQTl
XqOrBTD1cvcA0y/j6fk7ZlbbsvQu8LMhh+hRhA+xGWvWVBytWR4oW35MA4I6L/Q5q4wGqf4QEaXJ
XCSWfyiaOtMfhHnurIcG/qS66Vd2nfuoKtxpc7aWSwD1OAek8S9ql+dAV2E+FKN5M5FLUNN9BNwA
BHGEsMJtXhzQYGhVqQQZTjo3LY5zc79Abc3/Dli2SdrN1BEDm+MjydiuhAY+05+9rHE6rb16dbDP
jmvXsS+p64hyTZ87PHzxR5Zlq8VItOwZgyzD/sw9Kxb3oaM8GSTt9ev7PfEoNVcWQOLt02IbaEoz
L1JaneBixIA6C+mZBW5dO2SgNpq1R9vtzS/jeuHhk+oido11tKSD/X8pUbNje4slwD+RyT3T1qri
HvzpObe5CtAjeT0xYeHI8+gzdDATbUF6trKRRrcfNjlvrErsyQANzaekehVnA5ydBRaw9QdFuOnz
jVDPLkhylI0yB8VVp5rWCtxCk0+ZA82K5a9k8mR++zVZkHfBAV1ZezlvaTxf5gwko7eIYpKGD/tJ
nQuDp3N5XnQrb+9bXHVnmQxlDkeFj+DiHcVAgQbyNDjaiugZTD4BY4wMcDwqhHjAKdUcI/Ojvdnv
FPoXgHtq/noA4Rz6GIwxK/GXHP1RVdJ/8dAwS3ix8Huz7w8x5ZiLLUvQ2AKdLhTeWUjNxRetC+Ga
OSJ1ROS/45PfM2Mo4aWCruxmpwdiFNEGC6Wlf0Dlmg7JDgO9/2QYsxZSlaOPQ10lpZi1UE1E96er
fcyNqT3746qRWGKoN7DYR4dm6CigOEAtaZCGs4LLRfMbUwXK3yUDUhlkoOrahRf05P6RF/kRGImM
BowkWl37QhGdUSfShjWUhdpMm+Ft0Y5nqEB7uedFSRHN7ShZifgfivthDBbJHttv+Nmuf4/dSUUf
lusGTmCc3/M0tY6cSDonywZhsWRsYf+xtLSfeQmWn7r7/dnkmUrJAYEcpE6lJfGuIlQ41Y/y1zRE
oYOqOR3sTtoY3JXb3q8JspDvlAXKHK+vTH3OEBqBHj98otDyigSCULLCRSG4/wqowUYDGRPj4Tsv
ehUVRSfUzDQ40F6R4beEWFC861ojf6AExeaq/aKjlK0x6BlGZwV2/dwenjtKer6mvdgvYgUVrOnr
xX/5jfVpM9LGuJAE9ogYYWwbB3NuXraytd6Hw//3KHPM3Db55a6/piRAVtzqx58A4Bg7+ONEhCmK
/NGf15pDSikCzCwmrXLxuf7BrvTzCyi8Jm+QFnRbREPLsL+zanglrwwdU0DPCJKnYTPJ7KVib1zb
QZGn6SCcRbPdPji1FAG/Mj74XRddblXz0dJQ0nt86CPsWtJGUEYmNYx/yDmVXA2HPMJtjTdS4lf2
4wEvbU5udGPKjjpGe9tqfqORwkv39SyADOCSnfNj8sRrvTMOKgnPMg7HRC/s8+XAHBodLxLdYSO9
ZKNZBGZQLIO3n9Pz9HVPef+HG0SU0t0HaYz+w8Jv8TdyvANwjwLT0aV8g3TdPUK4A374aehhywRy
SUKPrBzcd6XF31YaQaSBCyE5gTYqwrYwM3Ep7BDfbqfOJ36neS4JMKs/6AyQZamOtNK4djXEFVyN
kDh5NAH2AjN7FhBHZRSDAZKE5tHtiTZx2TP1WpnzYg5bbmxU3WsYr1H6h6oJZAOCP2T1s4U02+/4
5q+aiOPz4Yomwe+83U8JWOJqXNmpYbwoVnGKfp7g9NRw4HiYhS6B4R6p8Ml8Ru+jODI0AxPTf+A4
zRNbm7rysyQRAKigRk1msYNCFM70l9Afd5ZOSGaNG9DoQ+BjP4UF0nEQUX3tX1xJKyAOt/zquXpV
a4Ewb1+aeJjBLhtXU2bGBXvSAdn9VHe9B5XUYMH9Eus9Vf2DtoBeJPPpqeLZ/vIIaYmcvSCJElwW
m1wxO03a0svhQSto/Kk5fOdkWfgXtHwLEhdwUPS+6J+2U6MEpEvJhEIGHcIoUwtsU/CQSSenIYOu
ibkPjAuZQLabAYhW1al1gvduAsIIoqB+a9nfIKoT3vCa30PDxkocd0iVk55+R4hKVVAa0becJU9C
SD2ENdfgAtZGTsC0sNDvVpEBjU6aYiEi/PC9UEDh2QEUm4jSOBRcwjKvnTFIhjqclMrcBv7HvUcn
9j+hv+DNMOcPCSztEL18ASPDwKzDb9FiYhkIbYfLZlUhJyf0dlFMnHkpbYutckYYp28jgrXPTr6h
YJYqrP51pKAPGPmKOLEtbzRUJqcZ9oDg2oBpdwmiO4AbOMeu164k6CyYReSbS2xYQrO8mGXQR/ue
cFf6kyjTfPlGDHioDEtR8cLzy5pIhK7fcNrThCoWq3GEdwQKkwUFnBhU0LRJYY7mJCRHlGC3ouno
x49IiSfPpnk6nKCVii+K/pgO4sqLxyR3mYW9lEViCQtMIyc1hUmfiZfZfbrR9kV8BR2Qd0+MlJzV
9bsirHO62g3MG/r5v0y/aRthXoiWtAkMsyFVKMweEBsLr2oB0sv/WfG8Zx3DPR/EBekxST8qZvcV
06tOQ8UcsmVGzy0cHV1Jb9A2sRUdUG4LPe1v1VLvk3GELPeuJ0Hs7nGJnkHPB7KXpaXh4DDLwS2e
WkPT59nfYiIOEKB/ODagmvvsXhA9QjG6ZKRg2ysIaZC5KeTtApCgHS+uMlHrbWLfbQlKbWbHSrrR
KlcVc/IWqCMCUkVVIbDpzZSodneR6lhznvQ7BBVYqjb0bVnuHuKB4WW4qySs+3oD/oNuo29gwzH7
YvVKClU2Lkf4ZL1P7KMTGpStinCgso87HvACsumxJ7Tfm05xKE+QgajfQeJ2tYTBy3+EHIxIki7e
x8QJI3LAO4Bra2dw8Fs14q6a5afX8AyKmbAhbcy7dku865O/lv4rC0ftBJUgna0s744ThitSa5Ic
lg4j82Gglze32chOM873FoJz/jGT1nGkJ5hhaIvEznWzoHBCCbs7jq4MWzJlmWJDVfVdV14lboDX
fbyICh1fi4SWDfxdPobMGuUj44jPKSGvRFM5h5k0LDg5rF7MA8UPMloRqoUgOaawOitMEgANjn0n
HWd/8MgPE2IcXgg8bPjlm9fSpah8mgygiomaALLcCwL7++7BSCCXeXF/J7Gmye9TrmWaVmTwo1CX
a+Gasu9xbaegZE/P2YqghwKXZEz7SKh8F10c/lrgrw5LWARaKIPKaspDJeOyb9NJKJzVssXDqjW1
kief4ptg40KPX55JpNC11xGRanPfbfwu9NcjP3VlUxOi6qCKJCzD1PDggtdpbupvZ2XRLvOG5FoU
Bhi6uU9Y9kudGrNG4QXGBkLFtasduL/9cmYVQuYkpL3GMwiV9p19HFTaWxRknNoXixk2kPnpZiVr
oU0gYukQ5KgmwGS04Z3c85PeDU+rH3syOJGcgJDSoKRiZReLs9N7Gy3rfb0Z1PLf1sE9ILvXY+oj
sFqag7yQkUatpllWvvNkHuZSWJp2wbhQ9G3twgM928ga0WbG+4fmZHKwycZz+qkSivsMfC2G5UTy
o5yIQuzrHS9QvEgbr7n6uhRzzashoKiFyiS2QpqIsNM0RbFa/0JgmM8C5KRHXO9oDkrj6idGpg1V
shAW2kFyEanEHVPq0MjQ0I3io+5QzSMJx1EDq0cLV/reNc5BSZsWjuE09uJroMSRUu7x1P2gZyDn
6MTdGSevw2yyXXdmvZH2GTADiHMaGEjoa3rtI0cEWrHJqQBC+kKN7zBDfWFwUM+mOQ1Tp98/CaOP
TTEcK1MVv7P4m7uyHL097Fc94KcLeiWrvXu+R7Ds1fDRA1ud0cpVSXY7YEQGG4sVHvnceMoOFR1u
7FEmHjBfBu+S+l9E4K/PTvYtfz0+wNEbfshx7pR3A4mf3BpThfL5SdMVm7Q6QkXG1YPG0Uy90iBe
uaymE4/JCPyEhrnt8pzbJlaHnYKx7MvkjXcNMV6YtVzQ+kncxzRq1dlZpKeA1GIMHPodjBieBs7A
XsbtULQhy7rcXfWgXn1gVwfNmKOGDLY8/4cj6h8ONEgPKTLAzBfbyrHLbdFxQnB1q7dalLiwUkCh
xO0X0Ok4f4VMJAoifPjUTGFS+ecWia3PCjXHxWtxRLOlNYLjVPjYf/Hq6tRhTShCUjsSm9qidsiM
5q9KCn4tZaxhn4FOr0UishM5XKhD5bOIjwF6yNAtMHz2DQRf2JZa108eJY1Dlo4NbLLGBGyZnezR
lPDL3TcPRehL8eAo0Fs30RU1j/CQ3IVb6KVcxWfq+q8ac74UrxLaN40wMdNhFtR3P4d7b50BM1C2
HTASZ7mIJabzRRUNv+KZ05kitg7gKBmtrOEpOW2hwm0eip14n+Dp8fREjpQ3SlJUeLM1Pag9OkTq
BpjBzNOYrYx5RFBqk62s4bUK7Bpnojm+zdITW7WONIvK8Y5E7azj7AjtideAymolRE2ZnUBzWal/
AvXlXrxVq8cCxOIpeRCE/89fgxQXjPubTv9ng4hLTEhvTrbdlwJWsKALW/Tg9Hil59EY9Ep6yVno
vP48diG5BvUqrb3hPDHAErBVua2un4tglI15fOsgzCI/26yiW8VKz/35DvunLCsvMjsbWGpTZ/dn
WXb4F44WOGnJ29Sb1FJKEsauZrRlMVFdCny0Ux2HDnEoONakm7iCP3IBZ1efInMUfW3yyuc1sgpY
V7NITA38AwZB1ZmiR/8hlnHlh7nroAbrzwJxoKK7nzN6fVN1YX0Kc7YFgtYcGhWM8+mW1iAhdtaB
DInfPKFtG54TotwczHMF3KjzEqQ5rHfaAYpUUJFQ/5WSgnFzEwNJWO53nJDeE/ssFv22mDrrZwG/
PfWNiKSumxe+1uXPAkzrNLeafV9ipot+vaTuDfatXsAQ8SH7TMa8910rifaXp9V9MTebhIE1sCVB
tUM+wHIKitJXF6QpqvOCsazQyx3uxeWw6LnPt5BhIaBWFzv2Lcy5DJnbj0hw92mEUCH7EJqRa5tw
u+hBbA0BsVngoh7v8hotS+7Ag/yaBJUeSNOFwUcp61tmPdKrl8tHCH0vGhwws5s/5x495RtBzqDs
U0JkVyV4Uqpc60iGmm9E3SqRcJOwEtv64mCj/Zno3FwsjoqL59tDCBaNHclMz/yVOrAvGzNM0ncK
0TGx3nN+D39TjYcDmEz4kbb3hBi7QBB1jUPmLd/fzSsNBhoMxoq1jrMYMBOiiiEvVusNCywDP3/V
wWl1QyOdjlCijIXfp4rY1eGp0c+PjDQK4nVLsdhiVdalY6kxIA/BoTGZ07K/rVDB04RNkqbK4Pus
0LElGzIpjt+tley9w3tn4XBS1csaBux8reU4hHNOpPGF1dqEirG145mkCNy28YrQt3xbhi9p2XmE
UDJK2tQaCG8d6xPk9K+zKIb8fIarZq6RaQRDmR8rPIIjNGiGl/M6SH9FXr8Bx94bhH6JFMqrMYM9
gQ5XhOiUaq3ZBNTwQwkkWdpY4VEcZcYbdHyI5c9bQ8Zy6xD3Q5Evvntiq7nB1PlPLWr8MdXbDQlL
Q7/SAAvRqMqBq/iS0tr1C4fp/TAlVsnCyYU1hpCIKAOD+qYJmHbCkwUgakE9RaXDkJol+P5C3L2m
ruuA73o2XYuVvoQCrero3Gu2EGVn1FOGnTJNoJxCm/Q2cHM49Id3lqgYodCvjz76Fuu2EL+xiZ+D
58kxuJpv8WBnuBpjaGskdHzxg/ca5kFyUKnz+qJyYZ3OwLnhp3PrS/GZz1APEG/4GhgxAlxiu+/Q
Pmz3wPGIEdQlC23VBmaT3/0GVfEPCCBWVj07ajAtAp+og2qDEc9bCHroxzhILpeblcT0i5UZp+WO
2wCz0sor0naE2Aa6uHzDB7XIM0y5UGW/6yp0Lz43NYrCxxi2nDWwmK4u2iB59P6F6+2aZvs9w9h5
L3nm5CrN2nptV2jVeCBC4XSc6BV5C23+f8eebulWG3UB5oa0RNZ1ZfLh2Ofuh4kAlWnO6mewt3Ni
ohJ41UsCgQ8JUVg/Ioz6bQNG4MKf/h+yxKvToAtmUGQVDbb7xUlFNTaIdAi2K2y28/U0dzQUFnjl
C5zsn50X/IBGPg5RSJQ7Bn9uOUjflT5/moHsgRpbCXuU/y2dkiUV/N6626j1ZkSu7wKB5cq7AXiV
MtB30x5evL77TRC/VeUgnlGLcCpHHYUHpI5YdZGWv7Lu9JjWO2f2xBK8OpsJASQVb2EkXxmDwsi+
2wZUEVI/oc0Ti4PHdB1QKpMRFXmz8qCtZG8kUy7CuiKbEQXOZRY1vQ6bNNqtqLUVeBVDS9Eh4jXi
l+Rdht8Mpb5s5WyAdGaI5oVcNMyyJ6Jko4mnGtC1Y5MgkwPSLhK5CmkhIyHg62ezL51b4P/fliy+
9NuqBm9v4af+zIT/K7e4gAY9U6GUqO0RZJfKiQIOL3ZfPH1vYcDpE8HiqFZnWMHC36kJLMhkleZi
K78JtLNoXsyBcRMG5TJfGSdPEJ1Ef2/wIOQIGQ0WpdiYPhenEPssouWYMAkacpYEy+z1O6/gGSap
I0kAqnt+mWI+Wsfimf+VESkSFnhxP80SkC+lob1nDtR6L8VsFFZTekmJ27o+feJ29dOH2vlQqGkK
+V3HKn1PYKWGQUtjj/f7KdLJ+1JhHCnLUN3HGmGkec3osTy1HwAVhBmDyKjuulz1oFhm2MyCoTcv
qwlxKxlhHJjXf84v6eiXg8cjRcZLTYkKSsCfXVpg1TyWG1u6qbX8+KU9X6pz4/LxHT5UMJ40C69f
iFUDmzcwATtSkPxuGmxoIeOpE96FQYokAqKgoYrlECwKd1g8YeLPIf6srn4p/hZEqWUzjp4TGp8T
6Bxryg5E3GSa5++NvI9HzJK+3dVlVTa9rW8w98OUstu4y8+xwB3IlkRDslloRW9zSg4nBHkWo4+5
MB2znWI4PZW0jiYteE4beMWpGjZtmK1yJ/rQEgJGwcPyFys1PKBWGkRQFoXsHxMnEo6iJ/fbNNB3
JWoeVyMttHm/28VpjJidqV9ksrATE/Ax1KDixf7fLtNzRzgSYHiEZDrkRChKqoi75tJb3iLk+jui
taAmlyGDgwONvCAuZAVnff1vuDbAp/9DPU+c5w+I8blh5kKoVF/dkuSAx4luf4hdsCOhyQGlP1T8
DDsGHw7ZGukx0mhbKzHQLxKaLNzQBvKRw/eFWmKxplsAzREIYZJKgMqLOurb9Mmm01u/NPBTA15f
9/ZdffYBVfu7ThF0VuzbXKQxFJqjzkhMsbHOBeYjIPN3GZyeSmLCbLvLczQGPpfOe6rq8pLWGZhj
6rUWKuFsUcHGSyJ2IMWCIqSKdVKQVhu5C/Wjplc/j0NBaIgC/Fc/i9fPhoAHjQOKpxPEg523T/6E
ADXwOUVhiS0Jrql+NRf8lW4SJCjrL/5j+dZanV6pNsefq3We52Ye0mOyNP5euyGfE7RG9l1OzZ3c
l1Uw0TchSH5dulrzGsHgD0ogBnRLhdsDfQlujYrCxw/t+7EKWQe7wNnAGI7yLci+5ldlhaHRwGnY
4pvfurcz3DSJCTAlBltXz587OuI3eyZmd29iR4huDW9HkM/Lvvlc6UK+xu6I7dprDnm5H2bVVNFg
hcPu57T41xK6LAIUh379/c0QSa3PtstocESQSyJ6fAK4xJF8F5SMmnO1Ye0fBoTEwQadbZjrjHju
bBr2eMIEnl0cEFhD9JV1GcEtdoFlDSCRGtT2lxuidlWMSCsRHece1BMUUl0xF7HyUnN0opocNsgm
xFjDvCsNCj1ZpI9wNvpMqXncfW4KrdTJWSYk7t6KI0U6y/oWPjYPKhY6D6X6ZrFAncOWAIoDZMSF
Y0FwWA7S2wfNQFe8xEZtTGZsv6yduXedvH+kM8RvVT+pspCHJ9T7CNDyTprxjwWgMF/nzGQRH6ze
O0Yl/AH459/cL6zZJwh5K2SiL4qZFb3O8m07TrU09NhgQDlaBHSbUqrYHcP7/5qO/3UNA8xjhXsD
M4J7BkCpr+fsI97Sp0U8nEU4eKlTanUdHtDYx/TSMcyylhR5t3izu8vPFubwFH2fF0BzbZRG/m3F
BQ21RgozADaWkP3qnspDCYuu/gGcnnXekZ0KfxlmSNR/juDED3Pb8g9+3/syCHRtIDIHdrfBON0U
MaIZCZr8pInayjgsYC50AYMuHYC7P2xEYfvRFo12iPAlNgGE8nfOvZu0b65tMNw+0g8egjPFuGam
y+FZdkE+RjBluGsTiqfSx6CotmtyK5gNpvjDKpLP1DEXKJUcXbCCZ3sF+uiK2xwR3QV3RtLEpotZ
0U1ht6FMFHMjTyvwJaXIkhPBClIg47P0f9Tr4vVGriER2y4hr6SQls+4L44O52AQQJWoho5ceo2I
ElsmoFlTJ48rmqpyFZah0hjrYGVyZPJFWXrLC4P1SJAd8ARvLmSQdRDCgo0x1+4e1nhKNq99eguS
/BfMsxAvKOwzuFihgZEi6JMQWjtGgeFC4Yk+DilKW9W+CMqP9/bMwxrA1wXyY36ZuKcffqteBk7A
Mll0i923SJDwbtiIvgMRhvU58RX+gOQy8CkuUPU85VT2KjySJILbVrUnxYRav4ZiSitwsRGobdjo
0+3MbdKxWX+nc4WKtVBOHBfEBnD9WWkvHwgxpxw32zu1g7+F+MHwAsU90EBnWkMeZrNDfUNDTlwF
kmcCqQWwQS/XZ8gcRLYiyRqM981vji346VfUAtW6kWD3r2N5jKYvCUf8wuoqDd0Ly6z+0P6PjKb0
sFfrFIcb5k7mB46F7rbI6VVsndvYJg2e/wKIqnJTDI6MRXgW0e6/lQ9sqryGAIycoQFNl1HK4smh
wA/pD1WJqE2xv0EZTqNTCFNiytSOSkm0OusjX9VGNOTFAiAxCgxA82gyLtP2KGIWeXXpOBNih7g3
gRDQSyCr1WSXsX1S5jUoHpVJ8q9WcrPYPeieHgRzuD/3DGXRWR6nbhu7X8oNdQPwVHPkbXtugl0q
zu8bSUg58XJ0Y9oomC7WpdKy1lb1AnFr87Ww6LRWDbmZFpxgVBnZDuYt56o9t1c8LUmj0zHlGLpH
JCVlEgkTSKMiB/IXnIWcPVItYIdnwK9Dvr7GK4qGHKRIxZXmlEpeN0G8+2CEBFuKt0QdlnclJ3ii
vtjzaKnTa4a9ZhsOr29lPQVn7dXJExBk+olnOCY1kJPR3JqFq4ov9o76ogxUOvWQhyTBWlLPRadT
LGyWzxKueqn9hha7g7wc49MIkNHXrRjXo3x5CaIUm/43wcmUYCOz/LIl7PpijCEPx7GDCvlQ+6fT
ZgomX4IT/7aO7NP1+oO66YpbcOKKnKT3Mug5bdPegVKBKK2nUg0tmPVMYJhH/yLziNHciTrR9MUk
OonP8Qv+h+Nklm6vtM14ozSWzynTfmR4VyldJD2TTzSeSjBiRY+Nkga89Zg1BN9Do+/ujwZcqkRg
sI8T7+qTECz64nlW+8QMRNr7gHq97xOexWSfekbqYAeW+73apadWoVpBG2M+zdST91UwhqNxK16y
sCOFBQ2nbMgi5spcmPyw8NYo47lat2YYJeoV2ZQoQJbOxc5Bck6aw2A7mamvd69hL7SMjpCsBN+C
ZLV7B32CWvarY8e4R4gLg/occrhaL5pRnwW04hlZaTbkkx44JHZqlt7srPZ1zL94AfxHyTz7yJfr
5dwzttwQLTa3F2bSFYSG9u2WTNeLmpX4C3CKv/bQ6Rnatwh4oCWbYEBmHjGRwb/CoNYpcssBq9dC
DpLPgAx9/FUA8WpjPPRmDNFF3ENoDg08FNOUS2cKpzQijmyb2Ekx64vDVqa7JCcvgwcUfXD2bw5e
9qL8BvsE/jNhIilt65D+R3SdL8qshIJmkO10PeUYMGryZ+g2tNU4HSvkpsp24rhR0ApbYfMTwuDa
nSSwyG09dBeOKc9ly/m5VSx3ly9hrHJgEMvI6hN7gn4dKqopJPb8yk2PIM/Xhi6CP11QumCRlT9z
d6Mo+DjX3mJYUnCrssHEb4hIWfaL5ojptRjgGFKf9m5iBi6Bmad7lXxNsRM8LLA+nINoNvLvI5DT
1HFnC+Vi9MaFIS5m3THODdQOOS1NP7SJlGbAwD850HotKj51N1W9QjWJf8IrXzvlJc5JfhVtQlJ+
HGFjmIXeKVN2G4t1HcgKRM0lEPU4nNgI9GZAvi7fDx28aJh7FYFIHyvrdjU1WaavWiXch6E44O7K
biBuML2vwvhopRVtKBr3Jx++x0NxvZFoSO58tkxTWUXEDGPXclzwXRc2pkDYQm5snt3pDo4Ab0J/
Qrl3l5vfv9PP7IKlVjWD9hKxcIzfTfM84F5Xa7+zxJw3Don0+oNj5ODuh2/0WidGLLuEuZhgZneV
AFK9Z5kjQvmBwtdtHHMf4mXDmL9a5w2NwdXFSqgy68S59RNMQs6WCqA99jiMYPctbEIkriUoI09E
S/J7pfFyZCLJFOoPfIGgEFo+KvXZ+JPE3pyW7SuMR8Me6clqUHa8CirvPjx+bfMsHpYjnUkDkg0v
CPtM8lw6+HyYtX8a+Ue6uZOsFxHJBYkQvROgnEIHAmtMRxIue96prBz4xWtAdUz8vXsdA15b4JdT
hEC51pQvb+v05reety1MSb5ct3sbs1UmCweyBFnRoi1JNEyPGDfR+324dJ0fK7g5bNmyBd7BmwXk
Si89cPgdxXIge0VJ57TzUT27iaN2rjZ5mjs8ftdxRVymuq0ITQYJA/bDPKeKBQC6wJMY/EAXmRoK
RWpVm0P3i8sdXTlXa1Y352KgrAu0SeBmM8E1SJ139Fp51hs8kmqP/XOQIZvfeHE7xJO52vLzTrBM
84lCtQvxsH8zPMZw6UfFy0B0Dc92zXmXSNnsydbzDn6XEy6jo/a5ydgr4z7ulriqVSFt9fw0n1MM
biYSfytj9zvq9uZo8xVecodiQ+HVfOTDVnsbvC+3WHRYinHfLuwtx68nPEDhIPRqqt0TXITk0oX/
fmrgnD+d0GzcXKJSewk0gC59CE4JBDCq3Ts/LBQCwVwvbkMwJpu/sdljxbh34ybhLtd7kq1OQWHM
aE06G5mIVLndofUXMhXHNMpwuHLAYLVY2Toe9HqIdNmL/dbVTvPnKoHxyZaDPGvSdE6CujI4gHrq
G4Qt750fHLwA4diSuP3poNIyIAdBdXDYjjIMfC+g+hiryUhmEjrgeKXUTJWT5zXig4tKbRtnHefn
n85/dVQDOS1bCPvxqKBPjGByN7tLgJKXpWKx2KECxyoBhY5r+GFSwDfDIDFkO4qeoRsGq+Za9vbv
Xwm3eVplmC4sceutmLZFbAHrLXRCMWBQT4zVFRNkcLSV7+fHXpKcVZsjYXRt9YsgqVVr4IdMu/cA
+MXdEwtQS5F986nNsFPv7wnDa4sXhqM3wkzlnZIiPDU3R5GMNvwte6P1yBI8ZPqoiwEH2iaatWse
Otj+BAZhE0DG3vW82ZNmP+HT9xiciCU3S3GdHxB0UFxUnsIfZcOFFOAbM/YUTufDKtSqVOIZP4Pe
bfQtXrm68LtKVIzgFMgxJ6gwdF/+WjMEcZA8Afj7tR4CHU48Oo0rOpC2q+WX7f78+AGcaCjuqHez
9HUD9j1kuvkchglkSWRAE5S21hjrZkxx6crFqu1xm/Ba4eyYVSWLUfsb98s8jQqG9rdaR+kzApZU
OePkyZA09Sipa4IaQ6pdLtbiE5ELT24ldmPtRdZfEsYNOJ7O59KiirftzdOJ/Vfct+xxqkLZ7K48
xq7L4Tx8HJSdhLSgsMCbBLkqsmdvoAsZ2sPNOzpt49Gbmwru71G8htzLSbEtORnysXg1Gt37RTu9
iABjCyFm6S1RzyQukvovXyQQvU2QCphZuPL3E4kbe7Pk+A87U9OrowY2XtLxvETKK/IeyV4UUr6L
hVDhYBXs4+LSgNqM+IsntJ7XFxHUjIFnCQDNlWhvBwNJ6SGZF9lWaGzA1MK5A4MRMrR6At2EMzUv
PJYUnVxJsUJFO0k2N8Lp293JLsuzOpZl5v7MAmbSWtDd0EjuiKJWxCToBAbqIorZlgqXD2TZn4m4
IV9Pxwjfe5iJaUNv627FfML0nnIxBFsf+IUp5PYOc7jcxcc6qS9l8V+Q4F9VbjoPmDusuYaQswFs
ZNz49vThZDMO5uJ4JeUVgv6bnAV37tSpvmJYD9UPqkiQja+xNpa+w97X9nzlJg6jkcGPvBuckybP
mfl+ABqhcMiWa9McfntEhJOPUDNpGivOOdmH4rBAQb4ta+hti3Eoo/21FHPE7EO4z24Yo/ImE7OB
j+cMr21uGZ60pJKSAHAJTAus220uDmBP3QPFUsm57jbVOoNZ4TuihOqm9LPB5Ig85UfEhJs5hfyK
d/KrukvqJQ8gKl8MkuFr0wMWjOIsUWl7KAeyQh2zqOD+k0NIfStsDz8Ipm6A+nYX63mKcsayV2sS
IyMiVnzDcv5YLDkaBYb2cyhzOyIlOtot+qWDPqAYLGcrqjAo44S859Bmcw20VPjZt67X3Dh19rDB
68BgWkD43MhXfEoWrbVNboW3JzSf3YWQQcxvj5nD/C5HLO386qR9S1EQo9PD3dGKyhCjBEs8K0R6
n6uqQHSEUOMyTcqQMFuqe+nLKn8pjRongyBVlETg8UJYww5onTb35alLvFQ4iWH3PuJ4kG3/r7T7
9U2zMOQkV4oP7CBd6fdiW9otK8J1B2d9HY7hngZUVwH2tTfAo2fYY2pDupFRBFIZbGBpeyGN7jRh
hSgi0MZO4WoPtbmwENXVxAkj5af0QfPvs/ZQP70DGcjb3xCXACtcyY/LhntwFUEsz9AKtVIHfoYA
xe0kKEeJkYYULN3KNLrBd3T2/JVHX2b4VWDBLklUSS0VY69TJ8c6kYiXtBm1McIaQDMNg+m0+Spu
Mf3BG32V4vkUuO4pqa9Ku6UkPHqvSzCEcE+R0N5PubIswuK3vBrzeAWunO6xWH2xNyO+YO28ZqBx
nKD0ozyAvDRlJrbIYHr8F2QZBVC4x29Ed+hMJUDjRLKock5jOX+6pkBmDwC3IKXQQAs2CeXeDwR8
ZdwFIM99hkNd4ufnqBDInyBtQBoP5DNdVmZUWrplvJnCrBRGIKL+v+QuhtMx7dLSHu1nmPhkQus0
q5YqFZvMjLzU0DYcx+sz415Qb9uyz+cuNvAf07wf7qHgIY3yh5SVaAO7b76h58p9AfTOPeHDBbOr
W+dew5LnZ1LbdUFHiT4foDqBMsRmlktWvX598XJz5MP7OPFWAXYuX8zX6gGEIe2p0haR1lE9pwzy
qTgRRwNhqQM9hgU+3jtBo7wmH/Aa6peqAg8H8dRe8BKITeJZ//vCXFSt1YqeG83SsVGZctx3xFJS
Gyop9IFgJvg/oRhZVkYYkvqJCxf1pJy5q5IpUrmAqzX9NO5wMzSbGn6U3E4Nlhnsq82FcO/JSFZb
lTM2B+iHvREmACIU7YUfu+SKijRbeh2eDI1g5WTQT2xo5utNEs+kwTKwapDkQqLf5eRaQiRXMG/L
rYko+VUTH98qtgNUiY5OHcIpnlOBGwJBYScmGvhluMwgXzswLReL6kf1cthVfj0qyxCkVTfQBTJE
GADBUhncppnkUTraDae0563tUcSxGcQ9g5dtE8Yos4TPPjSXQkApVsiO37aIWs9ZPFUhov16vrWB
VsDsOL4FWbeCyuN/DmsI3VEoTSqqw554vuqh2jw8Z8rkqpauGGiJpysKMSoSb9ln3jTXj4dZ4F6D
fufsmvi2xqyvDMNNNLK+i0qa/FfiMN7A/Gxqfg0wpdSldlHT234SiXSaREPngLDxNBEvtxYyDNnc
gxwpo6a6ZUCZZcmEHnD63H5YP64t9frsoIGSX5JECw2lf9ggoCNAr6aDLyfLCeEPpJ2lXvQYHvvP
TOVBhZ5kkCfhv6Cr0hEVUTeZG/9X/HYi7zcHJtmV4yZ5CtHqgPBtsI2KCG3+ol8VQiPftZG3NsI7
jv/ikiN7T0P0+CL2tiBJwTAKEuvkW30Va675P16/ggaPkAg9vOkXcgBuS2pQhnYMW70lUTT9CQHh
ThytbU1ExNnKGYZSurGJdZKy6TGZNsQJPq2Q0zxJxi9BJNwYvcH7NJDMtwvbrNwMTgnQOiWelVAH
LeFnLAem7fNbLHA22O3xZKVYzciDnSPnNvRYODwG8k06EqMIqhQzOYQxqLlc9z2cSyZ6LWRWXcZ0
pnEwLiZyeMQKd3XxQDb4E/RycG+HVNpOQ7IPvUhVBdpF/r0pdGLashiS4bGH7eVh/ueT63jBHuvF
Klv5pskuL801rtGBy5WO/OhmmqxhixKO2c9q51OU8lL1FgQ+oKq0R0tuFpJ44U9j8im94LohBMkB
nsyOx35pSceQTiuRwtKyK88sCJKg1hKtGIdBxSo8d9LR/GIZ65YaTF7X9j25kVV4NFfA6wAKqU+N
rguz6CgP+0R0RTsxMiIJDofEr42bNQTrK39jMeNEHE86FKkGpvMX/rDLgp6vpub57gvdQiCodTvE
dtLYRf/W3zjMyEXfup6P0HoRwH+hGkRWWkbhahNWFTIVI4XPMlzyJK4uPVyHQ+lZ6D2mDojSG5kc
umsUP/SVFvhyYMYrwOFZX76c+opJM9u4lciyjSZhchNN+ral6yTR4oy4CCt/a2p9Y0ONxm3L4bhQ
aaoD7i/AJeRuXmSrNWViTqoEelZylsEkuvO87qx1zK5X09BB0BMSm0+NO+9Ly35oe7upVwKVl6Gk
6S8h4zthsHvdsBafMhxqso52PP2VHDZSQGr9miywntp6wUzeJNVdCSKlYB4onePsOES7UrYxcuT3
PPgGbh0DPxN3Xjrky4Z11l2cIM9IUB91uteQY+u45ZNvTBidwvj7PA9CWD4oEO/ZCZk1qVYt+g5S
BrMaglvIwsrpQMwTBESM8FjWDmsLtlimrJ7FvT/YzHO+t98VN+aA3+3rKWj3p7zdBY4bwghNeDlY
vvqi4YQ1sjz11sbxNwLdjIRVassjJVcP+RmYn7slAgbyvZNWPpf5SPGW8ev2w4QbnysyDtAjtepC
sVOqfNXs72IIhC8g/5xf2lhZfvS5NWOXoCBMXEcmOR5CsOx9t4dDb42ZSsgUATSUp1q4eKC0px/b
JF6fSXeVQ2SDUxOSp+uUUCJbQNKghgSkG+tS8R3BRsYMcBN2f+iDFmVGRMdeRfwt+fFDCB4INCQu
7kNYAwME0kEiT6F/dRo29LWWYCbNl+exGlYwGT88zsW9TFzy02qjdwvqCUyLSnYz7xk4T4ThGwq2
tjVvWo1hwtngtEz9z7Y4WMX8hM2XgbxlzvvHs7UrzN8v3zJZNZqFJNGIQvS1SFkxrLPYQYbR/Pez
ZYVGOr9uo8O4O+qd7KnSIKkiZ8xmAVUrye64GPmbD/L94SN8CGVGqhRRJVk2GPk49+crfMBZel3p
SKc9pvxww4jOWh/LyVF7saY7G92waTMEpzA/dDnb7QwWqnkQ99tt7BKgn+88t5+xTqD1qYISHhXZ
Rthh8LMoHzG5//9fE+fqRgUycrARQ2PHvk/uKFBpWyUNevruqbRLiQ+0XCZjiPwiGTDp4XMahApX
VdvHyEgSOH4XEj8HdEV0CsrmvgeDiHrax7T0yspRJBIji6CQF8Mxj/npjFwOK3QVhccr24hPihWB
n5BaEPqXu49XCqWj01EF53ZQGVrXwFSKLlM1VZw69lKUs0BOh2NToOGlwfih1UFiJ6aS5UIQMErp
1T/2bwYsmh5y3JrAQspCExHdP5HknFDN7jLo+jNXrA7JbbXT3dTitgI7eZ4F9XkUzFvKAAyhb7h2
05i1PTvJjkl29sibk0UmmJtlDE1s4WLEtJ37NEOfwrQqgVReScmh66ncop6fe1W2efzW2xLB0Woq
xnkbMVoF66ttIULQtNI83sLx7Fe4iAtQ5/BkIQzp0wXsL/Ig1jiqhIIYd50hKwm+NDly+8zretY7
4aJx7XTcpC0haVkFCpfO3L/7BIsMtffBaWRdHtznOdWz5KRLYBT5vOyYyYDEXEvKnI0rwAROF3hH
AyhRZeEWg9YcNBReeAIXNBPtAXiAKAyOxB8OCJO0WYWAJFKizoXqZbABhJDgQYXsmrUfY61n3cpE
RtNKRK1ceYovR4Th0sbZATxH4e0V/I/9SJNODX7k04cIhJrYOVvRxmTZ1gCuvVwwBO5rIj8Ti3I1
3/NTKhBmnEGL31ATpj3OohmZR2HRhmUWXn8dLbiY8ZEZziTent6G/GL/CW9CBow5R8WreMMYgAf7
pu4lPpGYJuYhkRBq+NOyNXyBd4hxBYZsd/gSEDWzsnRllF3mj3V8wut6WVbK06aTEBsq8s6T66uP
jj2y36hMjdCUfREzr153XseQP5qE3ZpubQZNi4Lpc62J1CZ7kVRMHp8fyvMkVU9uyeCaEG83DGJS
2yQ5vXQHilouVTJxOdGZ93q1NeROgajopBGK/MJahnaehriFX575DmB1m74PE/lpDGO3V0D7s7k0
87nCkjTBgYVEnps+GB6L73uiNe2IbWuLuTs5MGXSurqSJrymhiLiq3vVxt4+zZMhKLgaFoyqZra1
2X3YKUOKcLbHgd9I4WCTkFgDBRt1HfqotSAHvexAgC/IJtDo6Ch/bRHXNTTOsGVPvy3nWzKId7m1
4WcF3mXtZQH/d2rggbKcN9ZUDM9A7xDd3EIfkHRObWYfoR7aLFz8Gfy660upzAPRpe30irj4V1B7
CLf9UG93xSCYLGNh2wM83h5vZIqxjTj8tcrKWJS7F2Mk6x1+CMSjKC8dDCH3hRRTlmV2GpRIqmDK
j9vc4aH1fR95/HCuOqSue85EKvKIEDFn63OW6lFLaNbg46i4mjSZAsKOyhpeWQIg1sk5D5fG1SeQ
iLy5CwMcB2nHBXM1A8NgyIjn3TyoyaT7tqlPt7wc0F47ZBT0BLWLjoshUrEIzMqTvAwBqLjf6a0I
V6CVSB5xxce69ciR4ComhKpILNSPV6JkT9Ms9/xktH0LKZ/+uNjM69dH8uUubd71NqdMVmtSzkfn
arQEWKrYfCONqTCxFvbc0P3RTy6XIJbQJsgrdsEBhGXyOGcZhljcIEPmcGQvBwy6UCLErCnJ6ows
UbR3LP6NTyTRKSgRzxbDhM06UAMdKtVsR2mMnr5Kg5mks08eohvbywJ9XA14C29fKpp5STm26V5F
Awl0PblqyuWJZsGWEpA2EH94Q9Y9coohj5PStjLuzvN0IW1ckZk+mxFOKPYThQwRVyGb3sFBh1/4
RhaN5BKS9kGO213GENcCrNTTFJxQ2Tm0Ke4pN7p+CIctyrG9Z5Zzwj4gL2BGvhC4fc6EY9tto6mf
7ck3sl9dvh7y4VRHaKdQoWL0LebpgohEjRc0ydxk4/AvUT9lJmerCuR79E4wuGKM0WQ6LbGIJot4
A5l9iILVx5/0oR5d+VSd7uspnf6Tu2aCoJzeks15JE8TU4eeP/mXOrpeYRkjokhhVkjWo6x3HlSe
xIs0+gS1Z5KAwVreGOrKCkTmnJ98V7dLIXoLVmxjJ9N7LsGOO0LM1wPRit8p7Wh4XLXhJG6oqEhs
MsO7DgtT0amq4wEMFGzhUV4+uCakfq+8G9QmeyyiAv4/x2F7VMx2e2gyL/50hjjnBRSGyIMUHGYb
BmzxFtyJ4F9naM7iGB3nEW4Ekjp2Vy8I+xBSuTH32jZycstslTacNWD1MuqGrr/yZtBeA+bJDtP9
/dsMNPW0N2j4KjudwKMD/mnO8QY+XPNn4vwmdK8xD6Eb2Rli1A45lF315Hv+IwXcEwgImKOw8zmr
QVpqLc80x1T0Ss0icyl8wnYjIq/ApkjW6k5JpAnulgoHAdvzgeNtvGbD6Jff6XpYe9Pv1nYqVc8W
QhRXlpy9PcHJS+4dQ8DJ5nUC3Z1UAeGEf4YxAEBskpoY2KEB7kfKG1uwc0h1A2fNvCdBGMNhHUvn
0rSkSevBcfHJbmfavT5WyDBKMJfovTr/KoBGc3BNW7rlGLlukjjhqD4TPRqcm+Kb8QH8Cc6XONb9
wbLq7oGd28YhEv+/IvPDZ6a4VeXzHcjP89O6kcpoOhHNVUV9Q3dCZOgxbQ6oNQHg12gR8QNwQoFb
e3E5ftI+pHEgvH/Bfk+O6fDREIyCXuGmCNr3DEjpVZhSznE/I85MbLb2PU/hr/H8wbpbHpDEvE6+
2GZ4hkh46rNzj9FJ21XWc6o2g0Qs2A7y4ifIlSkEL9A3GkFyJgAWxHONHck5iyoP5PZdRh8dn8Ob
D7DtG+RMZIW7KZm/leoV05crG2/ZTW9wG0AmG2/wjPtxpIZ2yjF0x1wuq4EQRwiK43E80z+d2EEp
n/CMxUrh6YIaUw4cG64o0aU1nglJxyHfs2uhsKh/8A+vlxDrCC6oSglEpUeFZshnISdBLszIpA/h
2C1h47Gse6kDPN61XH+UFqw29SEuwUmfG04YcGibmwhWYggJGRTbfefloDLZQAHHMrR2ygdVXlGC
6iQ5TLvdgfkBI5GRbjCGvk3yr80d8nbKp12WB90WDzEL4rdKAqPX4W85uB/qaQCZKKQDbiUgepJy
x5cQR4dCAPeq4gjcuOovt2Il3Ud0BQq2VHfK5i/l29IRiqd9BQne8sQTdtalrE8r/66Pdqccm6q6
im5iKoM+Kc+A9AIGMFMtFSuw91dpfNO2WFRPDG/zZWB3WM4ZjABtBffplDZD2ZBnUXRjr8Gv9YuU
63Y0ta4TA2Nrb8moE12rOyEbFL86I4eFXKMm697kcnbX774cERsiXzyUuo3pHh3//qbOP5XC7V6V
cGNkTmqlAxTIh0YRUI3A9JI8ZnZuIt+ELDEoarS9gz1PW+Iw6ec0/5bKgACyOO2ySXx1RLWKrutE
ED8HSF8OXG5kitdyEZiagWPQangRExPHJh/bXZ+hg4CIbhfs1InHgur+lSENG/vdMGgk00rxcSn4
llOTyx7hBdbCYjTXePNVXA3T7Gp3ymdqpa0hKzDEFcIC+NYHdLnemUQynpxV/wxyJArhZBk54U+n
rLwav5PgcYgAvbsdLioff7e5jvn5c10y1MYGo0b7qjXtqzcVGRAfoOP8qFKTroSYHdra7UEbCMud
HdTKvgYDbIF2ROuUrMOpo8wf468oV3O+M06n4Vfy4XC+F5DeZAbH69OzVujuXWSQpQ+oUR4MI05r
OQradYCVDE//oA9WrXkRgnO2kre+674oPe5HF5i6dX/D0QDT0JoNEQ0XvR5FVD9zpXkEMc0Wv3T5
jgkK5yW+4jX4yfo1M79/Vt0KTjgu0SKi7QNR8bwfjqyfkMm1pRexJvdPvgwpFex5wBp/bslVsqpN
mdZqWJ5OYjjwyOdssqNkXvsPDKTDFeEpRCcDKms8vN686HKvdKyWuwS5GNKx63aSS8MAS8KBxDsk
9VKNH4Y6ylGZVLDy2h+cL8DnDo4N4TWyDIiLTn5l+0OmhMvoBtB2h3x87GfF6aqfDg8Vxpsr1LFa
CEp+HA4Y5FKNoUqPGdOp9NPE0w4NUy0qRTG6hO//u0IaW3IbtKjAh1OC9Bxph23MTjQVkRvU3W2i
diWswxmDdGua3uyJgO68NZ+iXcP1c+bAyAc+oGLvImbq6ZNEvrrnOX/eEKZSroz8omLStfObvbtG
w70ZHLY+Jk0Vn9fFD80ZyVMtN5nTgWL9Na9gbPRtkY+xC0yuzpRU+ibja7pXrD/skPc1X4zWE+GJ
mgZY+M7IHHZhbVcr1QNI6NwYH+gabUfwwxK+edQP3wmks8/3kE0rhDPJMqmcDnal8P2o86oZBbjn
BLs5OGV28UkS38tkHe4Qa03hZFYWt0pF/r4CFvBCwyJ4m9/nbo+nUe29ybHmMnb7VWQKrbgnABBg
XCCV4/rreiW+ZV1+Pip41f4vMG/pFP0hNNDoQm4zU+6NTpqPhiW9uauuD5GzhjseNI0dyKXAepHQ
YNiTeaTDW0P+RU5xNm32uq7Hvk0zYmZwG0SSsF5nHfS+Bvz8QMV8nPBK2aodp1i4ZXfvR2FZ54bT
7dHjkrU6k7KrHL9JsJjU4OMb3rwsS6fi8Jsji82UkQzf/dai2L4K1LMlm3tPTCkYce/+ZJv4NZuI
z2WLviA5GkxG29R29vkrLaAi75Gy2CqIRkKphcpjC+E776TTvSrck2bhZEZfrw31mNZQNcALPXcD
VsDvD/Y1ku/sjueGEwEFgaJFCJEDdTq480ESFpxOEgcn2/aGz3DCV2IDn796p5dEZ8ZbVliAf0LX
JhpNqtUAu/mSd1dVJjdeGZZZh5ZLK0nc1CT24Z3mlYDnhXgyI25ESWrTMXXZ1UYCW7fKBzQ+pvjd
hJUaB09D9phOex4dT3vuVMHLiKlHGg4LwErzPSsXnMF30o+WPfPSmHNsqM7+7NMTU4UyXfTUUoXs
2shLqvLpAJ6isrz7IVHY1u7zTnHWnux9Y0PKrQk3DXHOftCZ2+nHTW0u+Y/k8lQ92LAMGjFyyg+C
E/y0soeMnKI/gQK7d6muKBdqIHKjtYNvc1QRY963HeGOhLuUm3p61JZIo70ZXuwN2lj30Toik1MQ
bx2CZ0QgOaBauRl3LvK3d1KR8D/39CgYkfHpOCg411+KM+20eQecMyUIZ6Ebb669LQBkSPtZDLaC
hQ7FbDl6bWyrmGu8sDwzd+XFMxksfKhARKnWXir2vvSxzV7P5bTBx2gY9DBnKNV39/Ggvl1duB8o
08DjrmSQsg5n+bKJaNZFNGXM1ys4QlEwV52E+U0cI8mrMkSsJjAAeEzsyuvomO/D3Es4aELWBiWg
VTHF9AidNin7KKwH5d+B4a3v5yufcUzKqz1HYKn0VuR9Ez9TJH0ENpkx3YClakWzLoYZEOPYEEr0
bD0PCWylw8ZqZhrVHWR1tAiLhDCVYhqZ105Ltxli/6o/0KcqbgqiFYu8gFA7810j9bi5cKxzTgnh
aVU6qG2xjupX0FHBAM8Sg5VHxiLmdWW9bwBSJCCzTt4HbWyAv+KcMHEM8MgdjIUkU6onYcdJG8Dc
hbGh0t9quz3LZuVcNqCPfr7QrZsslxRmBLv+dKITynkt9fNpVBkb2JgEuYabAeAG7smZp675j9Tx
6Jj6w9KkV2gceTj+v5Lcm1220aTwBlOgNy3MKB5aAVdDDbWj5TOupJ5Dh77bJpywKe8OxcHp3q0F
62iNDpUwQRNItjzBmDvx8Pt5OwYVxaOGtmTsgIofsabf3Dv/4rEQmslcVvTa07oi7UwPp1dnkfj3
WxxUsd02pb9lJ0lZtcGsm4IbDU68yeMRc4b0XP62jjE3u5IlfFvGW2Ci6h/Z0po6CERyBoLi2t0E
lmgt/XeymO2Dm/VE2F30Wozfl4zF3DM0F/sMQ2grpLOEU07vJdEgNGHNcSfV+FjbxdpYGPszcmTJ
UX6OcKXSrQGpHaHu/dGr7hChr+/vxz9ySqFOCIzT1sKz8eSe4lDpo1q/HxX94Y1dL0XLdt/TCtxh
Nrv6g1+tqm7zWEbDQo9oPna8Q32l+Na9cagCdlfoClz1d+go1inviaXY5Sm07RpzZ6SE2shqTVCg
EkjLgOBanVAm7fLQ/pFqwM7cX0hdXonXnvASaAmTeoUAphKYs9LrKL7tbdY6QY3dtuTgHLF84Rdg
pPK/SLVO6zUGBJl8gLEmmMLj5fY9wBek7aF0E2YueYwklA4HN3vXlABv7XVlj05LJP/lgT6ox2c6
oPl4f+U5hyRMwyQJ35DMm1xRcqk6huzgGnSwUgm25wTUS4HTgmwbYOHtL1lJhVVQQTvzmuXhc/m7
HlO27YVVSx3/cIvUcTHVhA2+hTlctvd3/BRKZbnGXkgrfaTXSgf5IUTSCNU1rQlWy6bYLzd6XSJg
qnj0QZHxgnLPK5/bgPYz3719+vOGaMK2YkNfHZznHQSqH9+56EE95mz8fJpzR3TWuBC7x8Zz/VYQ
ctxEEYZRdPgmhgVqsSl1kdjbmZw0dxXLTfT0W0kqHWKOAqIfe+1K8om0Eb6XX1yBZ+z85nF0Yu49
5ji2HqMOMDeDJi5ilQilZsL93w34aQkCpBK2F3+1SxixadvnL3yBsfy8BF7yLECLqg1dWiQDG2gf
B6hvRERmjj5h23utJ6FDnjfY9Q2nCPzlSKbFoK67mATpdPZYZN+TwhZLHoehOhv6NiLU82EV1wIl
3n9xjKMBGZBaThlmMAZWsdRHFITY5lxX/nSrR6h3a8GJihJaS8xexn8qVYK2vfz0R/GAZDmVz1pC
neZOUDwMeHaDLctEd2eRgDdifu4A3HAbFZClE4xYV9KsB8u3AHFqpusRQP1GLKJa1m0EiEtQfj0D
c8WiIBCN5fr5btAiyEmrCuCl3v8zN/wFyf8qvrxiDrUQt/GHv5567RAOZ8pHULnjnhXmNgFNw4kx
gUp30jr30awUfTV2XtHZOoTwCn48+sWfdMwvG7yVwdhBwBJuI9KALN9fJeA9Z0Ljds0DbO/Qvn1I
RoS6K+nrbggH81rYYwRUZlddcjCxGxNFHUMaoThjSerC3SOpsQqok9jqxIc+EYzOkKXSQ+PKT7TV
gJt8Qcgd1xM/STX2M4bf1WKNV0rfLns0LNQ017JFnnybu3Hnv2LkE1Gt70ZrGRmbnVzbh2pEXF8r
kom4kLkpqGw2XVPtEhMkgQgU7AqRjM7IyhGprOkOT7g2CPlsXKspl+RXsMLtYK3nj7V9EK76NKez
oPARh9Jd1Tdx5MlOjQqfjCqCJjsuXg3ZPdyiVleoQmIcYFi90R556k9eat70/aARnhsCPBZWAsBy
REGF2vE3nB/VARxkUDFWT8Jdm9YfOEVdBTgSPZCOC7viFnY8XKubTxR8CB/tY5h7Aum96zW8Vpba
5kal6Re4bIAKBoMt57zW5ImUP6bL+tupLWjvRBkyum6UOf1kQINrmTZq5l5z7jC/x+1YL1vfuXt2
hfGt+xg+HeJt6SGAuiYOev177NV30WPuTl24F31ha0l3tRz6CY0lMzWcb2bWYBZmNf7oJ98jcUzE
Sa3qQ9YwIBTMVSdroy7n2W5c+RlIu6k/vXuH6m8mdA5aCzrqvjIBuQy8XoMPq8ZAJVkfgHPbrMI0
b6CbH1qepFWtn1ZkwCcWl9GMx4SbAihKAsmrjRerszTOYNmljq6g1p2t6xFidEHc4eqIuHU6pxw5
Rga0q915R4JkBLETC1qFKG/Qrc7knid2A87osefn+5dushFgPent4dGBWT+JZVOs04o6432cW5gD
l1/rgLJWYugrf7Izh220lt5o2q+QZQ++7nBFHZGg2kOiFP+6i4AHGQnkBJDm2N8qQ3Gjuk+7qF/A
cr9uAYLkCr8d/DabwWwg/SZUf4qG4awMsP/DnWcOeaiX3Qx/yZBRkDBpjEAO0s1LupoGod1psVWV
DxwmAruJIc7dyhD5/Q4Nun8DiSX59huJGjHhW7gEp/Wk0xjszOAIqI/4TBfZq2l/pZNW+MrshDBb
yKyBH61k47Jtm5o5/W0YN72bcny5/5W209qAEJIJtQzG07X6wJgIdK8ENWNM82ux+lLnpQ/+EMZl
mwnrQFwE6DsQX6g4x5UZKmTCVTX3WbOwlwlx+gV0gOfvVB07/o61/0Wz0kuFrEHQ0kBmk6x69Fsr
LgbH8cGfAsPv4vyuCL2V1UfUjkh27g13cCQJSP8M1sxqV0jzk1/kHdeS0VXdBp3kIuEYfsXpkx7Q
HqXvL0kvevUE4ZtidKyj50UXspNbq6aQaouklTDEahMK9QrC4d1PHd4QQ8mKjtX+VbYca7aSuOR7
c17Pg7W/4F49hwod4FavGegNEuaAEduHSH9ofe8NvstaVgwelO05ObQSalJDYOV7+a+GTs2voJsE
K6saGPyAVjeyZjJQrUWndhndFhhIl4hr71U7oQFvOo4upjXvsASIkmrzXivfeaxwkwoJOGIRRIj2
PSQslSOjcMfr/de+04W1MV5+H6/cgoKCcmhV9XWcc4L3jlmDjPdOBbp9ABdoA4KI8FNsiW1/deoH
Y9Lm5ZWoe2y3oXieUujpiW3tNFk/NR2JNpEBmuwVTWaaCkc/SNxrDcjAPTxbnLQn0IRbTmzeMw+Y
71fpIlHIL2v6ld3U+/5yJ2I2mWasoDMYUKk6B8cysCMf3EnGwoK9Y6F2Gp9Wp2XW2+AS7VDWriRK
wrUL3n2prX0z+zzGzSSVV2qkvHozPPGqMIShWnBRE+e2ecH7b1DMxc8e+tZYW9c3Y+GhvdE1vtpo
LvOSEOJUgPsllQZtDUOFP/lJmn1oKQ5/AjV6unLcdwLq7KnTVUFDrWpjv7s3ylydFVwigqe/MzDR
1aW4+8qw1MufpAIhkqP/77vdsO+7ZP5lbiiEp6M5wLWWiGV8GP6r4+O725ryGxInL0dJm+6PlVX0
EREVA42zj1DvDSWXaFMAEHK/AXRXs5YtLeIUVyr7UkKm6OyKP7fmUY9q1KlKiKf4EnwL16jwxWNv
NCSVnrIFRZPrkYQzLXcf2YDRq4DVOl4QIhWiM8b1wrAbZap1AiwWyUfpx8zNOxoVj/2CX/ZIbmn3
oxcijWQWMm4rOtYCqHCoknk0EDpF5UP2cUQO3Mm9Ge+Qcf57Yx98Yb2tb8QKlSNsDWgFKlwFuiQV
asqu5yBjc52fJ6Vq82zIwZdF5mE6MsmxI16nNdOn4Suc4nrzsI2V5ZuTz+ckA5NnKq/1liw6iZtU
OTFvrWJQhz9E5orvNHLrpkr90HHqfKyPjhQ54Iiu1BbjwkgmY9D48i6EnWVdBpNxAWNiSWteKopj
UPqkmCt5T38ZkiBY51v+Di1OtPw0yNDa3S+H2IQl3lNQ1ECrr1+Ao+qvuAdTPMAE4Syg3nzTtcG3
dfUGWiPrL8MOcu0DlNwMArS+WfrxF206gQ52uD3xy936MdPWPAAsPdsiKqFH5gGVnZlI+W3Lk0CI
j+xp3M0v6VBQGdirmw4mcLy7Dg5I4AvDO2tj8Mzu1oiixBS/Yy1HzSwHZGo5zeZpTTIshvcmVGdI
O9iQAEdH87O03E5OqrQvpyEYrI6WYCTU7NDThMBJrcfkwf/07244jcPbWXixlXbXD6QzyQIBNUyv
J9V0NkUi1uhFP5bHBvmklWJN4kN9fln8DdG29vr+BXSX52zsU9S8M0LZ/ql8XQ7nPkv2k+As0/PB
MdNVJxN41hRIJZ5WHb3h/ntvMsqnZa9dxg463RAmQWUS+4EtDf25PExxNQgQcIzCMaD2Q2UzWfhx
2dMUNLQHSIc07eOl7czOTbDnEUD3x1pgivDgWi7IPZEK01hUjEwjCWvDwl/BFpnAp51SxS1x4p6X
FGZ+iIWXp2HDzYKW2jnBxFMrWgl86HiFybMQLMtU7PwoG3Ph9uPKBnfjwLIIlfi1ra0RRVlNtUGX
4p2gHHYwvTb1MmvH2vvCMWYia36BsrbEeSRoZ/0uITkCtpLraHCxuUiiffg81xEkFUVrEHyLTuyP
uEOAJwxSB4SjO5G8iabdUul9CW0Py2rN2EUK51UEioPef3lHrNTjwAVbFEIYaGHWW4LjM1JpTNHY
UPytXPfk/jME8zsZWMnXxzePe/yNA3pBP/TXF1zqKgiWRRq7hcjGqaGp7JKWbnGyacH/w0CTL55j
4SRyfGdPc6vUgMczYjP8x6zfNmQ73wS3S1ROR8SaQT4GFFhIIgZZQEGcpPbOXCgtu19JGSqRwVLh
GvMf9qyc5wfEnnuL3qJtuP4kGP5Ef+KIg7JgNQ2hlF+iHRQfD6S9iENP/lAJsk8WFIqWMuMOdDGc
vm017fhgV2P2eO0NXgp43lHmy3y8GbX4+MhTDu9VRbKsAqreYy2nyHK5Q2O8x+4QQHJaSjw6BIX8
ZWW7irKhtuoxRvUwFnIrow46qi4r7c+B75Oz4nrCsuUsnaqMRMBTWhqbbmdfuV5QA2uLQgznY5FG
ViFu80qsNYokxnOQfTOQbzDNQductE+A4VX+1ZSYloqAo7clBSA2ILtdpZIMWTktADGIPAsV7z1D
Sb5XSdjr6NGghCmeQWQFopSvKP9uzw/pAnovZtUKjlC4pnFQj3Zp1EEaJp+iUEhoxnEyIV42PXve
Ue07BUl/SBDtSm8dSMSOtiQ+gxqmLJE5yc06+p8aygJxsleBbXoj/FZqOSuaj1xze8/vbWrjPVdw
Swzxg1aurjtyA/3TtVmrQHLidEt0RabOkbFSQbvuQJmNfnREftlzLoP7VmT1bz1DcKUI07YMuszA
nKWL/hlAx2fPGYJx93ZJL8P78EcOOBu+FkQNR5X/vEd3A/Q7sPbRZK+2d5dcYaATT4Xb3z59Mosp
z19NDEfkDK7ZSPaV/M9PggPjzswXsTkzexALyy4bXxf1yeXyftNUZbCfacR68n4acWvyokBNbRBe
C/ZxDolmMXmIIFH/bUVw+L1MrmHN1RDJR6jlOfeTkHhDRazMSXBfSspWGhpC6DZg8c3Iv6xTpNjv
+ujBmDjYDfqBUgVVW+G53aMNes3OAv935WR6nUCcSOB7XPHWKIsV4kDg2rbdSBGxnc046luNJdE7
rGvi3YU1DoBvzOKHpM5dAwWyT7aI1sjg9hjMMwuNxntM0YTyaP3xY//Av4nxZaE+5s5VmqHrUM96
vJyYWYDxUd7c3idspVrCg7oSyOhRK/Ymy+xtCXd3yKYcenw2KsT9zsGBjm6XbGO+/9Wnz0dtZSRr
+ao/2qQ11Xw/oQOVHfsOIbic5+jg+N4AIva9nc4hmXmTRgBCJfBwcFW8K7AwEGxfWUJyO7iGscCS
jOFoS+VlWca1tA+0A3+OFz/AYOiuiYSvamT0xXdRV/UAZJ9lQgIm7BCPNEoWGjblYE3zjibqVmTE
B9b71GZRtHCXKx6zR049js0qtWHuXonwY8CDnL55Ushi7Zqy+bNeeNXxtLQ55oTWPhA6gjpAPGbe
PQcz7qvhr5q34ogbOmVkgl6wrb6nKdyX9k9q5QzxK+amzP4BrxLeF15B0mn5Ybr/7tIM+YV2LHPy
5nbp2AgxWoHgGRHf1vyasrjO5XLxd42nKnQZYVAY1ffNiGqGaspsc3eT4dt+KXeHJVdNfHCIOXTc
zLHpstCgPE2rmQgG1RURdKdnmewHtD+ozHWG16wRixoVU2oGnPucUwCnNp/MEuop+jfhnNblrqLh
Io1eX65bit6Cx3wb8nN5Mdx0NGjIvpAqRjyBdb/t6n0HTwBXlmUrOh/62xb0nFgdlIF/WxXZtZ0l
DDGshb4yaubUVHrocSWzu+myyEv2T268LLRm2Dh2rN/qLtUtDIJscRKqY7bjxHgAK4DDsQzQImRO
GqH+slKKbKh9s69dvv8phezfaJQfdWSIrMBy/J1rCxa6C+onqz1saWBEiSHFFmFmspXogBtYzTPG
wYEkeKV/4r0FM9WbwD8rhACIk8LDfebmhtSEIYfJHZExyvLny/l8CZuMXYe8nu4+fbKeGufJSBh9
Hxe0X4qBDgWAA2tEzALRnsab5XtbjMWyzDKszen3slLuMNqrsjKjpC4RIFBVC2z9/dSYIx2cQ682
KMbDb38DqaaGpZ9jJEWDw19uW7t/vxyQAQt/ATlHM5SGzHriKXl2vD9AxhEcLQMnX7Oo6RxCz87p
+j1WxjSESr20tJuzPtOObWVyoU9/moCVc0mtGGNkTjWDokcoNbwCJG86NWLpiqwLsN4C7h4rIhpc
LFxeXY8WkaolP1mutg1sSXeML9i2FSRG2G90MGXFharS5gb1yPV2KxsNjeJMchStn3POlBLzDSXV
J0DCozCwKaaFNu6T9I0c1AYNPmgA5W5nzT9gWpqXsodObXL2a0nuGsrmmI2BSCenx8xGgMZ3cci7
Zkm+P7yt91B0yReHWuWkTct1JdMDH7PPdX04qcMyEOo7hEyCmiM57v7xO5Ta2aiq6I8vxiDd9gf5
OC4XfOPZRXQ0jk9lxydkGlqmFX6EbsmOLrdS2kCE6reC7Pl8KkY/vwJgzlzeRgQkDWy+/hR2fno8
0+u1iePm3+/NgsYgNTOKsKR9yWtasidCf5zyIKKLHcMqvRwGcGzDzvRxhAsXbVNX2MIFEqhnJxG1
0tHI/oLOnlT2ZicSTeUvsKdOdyU99qWhudW9NVRAvwGSvwsMAd2EbfOJNwSLpJSIBci4rsCE+PPn
epxGHgRW21ZRUtAkqoswkpk4aQal5XpN1W2T8+CrqJ8uoPhIT4fzJdMUBweDwiZRSDoUVVcukJCB
3sU3bl44KGMNXJ1As91UgbcINp+ILEvJCEOAZBr4bGmkgYB0tMg46KedEB2kyHXXcHesFflMhOdL
aaT/lk7/n9BKcCxhVhnJWSO7zBFCVMY70+RHmKBPmGirLtXdjRpchn/xfqei0uZOpEMJnqUlurbA
aHUH55ju0vV8mndyCHNHkF0ET4x0pYNffKdx8ST/ZEDN4TkOq2MBi/AbB3KjDSSdlBCVl8APVGDe
PeR7uGeVTt4NtGlZddqIz+TdHQESLKhLEvwcvrlyuuMjjtBqt4pX+ZHPOTSwqXUoFnqFTOazox8p
QrL7z3b1B/BQkEcB24OMQP2T9MeMgRhFPJ19EK62UEp2kBZIVZMmhNGYS9l7adPMPPaEw71qKJuq
T0Y9PemT/lgQcbr6VbEqVp41tDiQPMspl8QfqXfHJrbr6uAhrQVMjVx3XpVXrobuPU+izZ5WTQTx
xSRzsb88Au2ol1kbvw5OyPXHASBS+i7BFx/5ILz2GQ79kZxJM5AZB7kgU34jFy3BwK7Qnqmryb3N
HlMl8IlLzuPKmB2AVxqUwgDS69vCeQwZEgdpHIqxE+jHazBi/zGkdZKLAmZ0ENtfh/4NWWeSWiPW
jAQfcEEojxgA942sMnzgu51aUHL75A+QqFnpsbnUvApJijgSzmPUyY2R54ONiVLVc/rAVLZgTOm2
2r4ytNFeVwFn/nUDVhl7bIKaidPcfPizbqITah+b6HL8iQUgbvUxoyl4oyRpExf6eoa6RClI8gp8
BuZlpEQMD4b+Blf+hrN2+rDy6rH5tGI9M25CMUfHlzch6uCVfCaywN+LqHBObOPww/EM/SL0L1gi
qlBVq5JPdrCNvueEbD9G0OXUpQzDdLDs4z2ItN+XrIutvNzATbKKT/rfFMT4KZkjpqo82PAziodX
IhMC5w7n+GAXNO5LhFQO9nUA06KP08E3lpcG5chfrynUtPVTDCl+2eNsZOKjPB3wM51c1O9EjMMG
75nh3XytUqN0Jb9xyq28oKcy5BDUloe0TR+8AQRJbcTWlrhpNDpSrw4oJhISgGjsBCyYsbhQpVsw
kIlhZ3OVkyLtxT1uzr/JvjatCTCe/HSuodmSArteEQufgrky7EdsJDd7Gh/8wWX3eZ83rthJ5kxW
+UrEKeZN6fELXjEaf9QRFn8GvEAUbGif2T+5V13tb4+w/2WHQdlZlf1Q8HSCoeKjYA5vhQ6bFVgL
ei1mFIL1JSrIZKqrx09iLp1U2lRxEVKbSzPBexE8dRkKL048Ah4ZZ+gxXv4hJgYkkb7LP0fL765x
HIUAe4Bd22s/MirPyLAJYkvfKm8Yt8iDxUhzNPZifzM2JibSYHG4zMrzU+aLqVrmIfPv8fbTuNik
qzmy9uxOqIhKBVNQXgoG/rDqqsX/vPGM6JTFy+xepy7Jwuyw0Ti/YzIpavqWarft3Ua+xIwDoPB4
5leO0WVO7YfReLJPbl6H1xMC/r9/GWO1Nj96mB4ltmJgNPLhZxgRCuryy4VYoNn14MQJeI6bN92a
zey36/7q2zs2Pdwe24BzBqrKx2WkqcKjtDi7RDcbor/iYUboOjkDJ5A0ENR+MR+ck+86IPXgGwSC
26XP9p12NS488CaFAJ8aGQ/0xzBvvn2+VEzE0rKIX86XwTMecpb7q0AAOxZfIP7e+aAe24ms+ksW
jMZcIDRXpDqK8r8Ms2n/WnSu/WJWqIjYNiUjK2V/AD/f0zvsD665lhIeARRjiMJFuGEgThTKH1cf
+GdSWTrhCSOHGl/HH19Qac7rYnqY1uFyNl29HW/BdR0PkP1AnyLIrL61cGeGbuuyapKCcN4MJdWi
zNVVtM5a79RNeKhNCl6iR40Vo8Nesq1IvopL3p36D+vUTJwN/hn36WWuQzyr8hU1+YoHBeLJWpX9
DFPzmhu+boh9EKrGN1X+1HtCEDIt6khPPu1lo8flt8ktGV4D5k9h1mNFLFY/lWxkghnMITHm27p3
hu7SsLFqdJFphm2NH4QdsBo954IL4Q2n7tFsEOqr1c+YmpWmouoZ6ilds9aN15JkymYqAJjJKI8P
yeTs3xPm2r+jIJCkHZlCxK92bbBXWUkWmQxOltxDoC83hmB8avUhdfBvpkdyN5GsM0JP7drsMb8e
HqEpTbUP5CYpo1mAnpeWzbHDSZipgon+No4V8ss3vhB3v3i6o95qDFJdwDBDJJ5NTctkw4gDHKY8
D2XzvzQ/GCg3nDtJ0IQM5rEC/lSA7VglewVyUmGAWpNFR/p9A0dnXHpeyXo+kHiuHYnlpRanYSXM
U13v4sbjUATRuIVILqEgGF81QFl76fSq0PEaIsOo5kEc2Hq7nBCmEtTtlP3AUMIGtazS/WRmNFBH
4vPRDLtJKUWt66LFFDEzYoSHXl09KiJxAhI3QY1JCApiBeTJvasBwQlZ5FU6dj2Xg2JaW/ICvM4w
V7VCaQqUJP44XRh/a1IP1+xYy09g+PLBouDJLJvSzd+Gbs0u1H+73i8JlSRykG5aKqjx/VU/7SeA
ejR+VxvrMvYeBkchfSDGxjPLr0ZXerZArku0Py69ddOrekc4vEQOgr3Jt4j5T/Nb9LF3XzqQlIvV
8X/whrDapTD++lN2pIB6j3WHs0yi1xqZHQ8wle5B1YN6KUxIhXlQvvkjfLzExfEIhgOjRW9tO88z
GzMI4QsFYvMwBv50NKqueD6/21kE21lTviMZMrdyNPE/eag+EznLIIcdzv56pKCa9uKN5+3ur35u
KpUn3wT0epMM1bjFQxj58OQKrk+DCyrpsEi9gP6CrLm0FOhvgbc1W0uhChIWHnXGdKLaHjPmF4yE
3itEdWYk2N7ClI30bW8Ycryg+aK9qFjsklb7ryT4qRol31KHRt0y9JXFUG175k2T0CFsQzYyCThm
XC/DYM6Sh8/P7MZAcVa9lHTsf4ihRqWe7RqBJd77/tLkaQHPX9aweWWXa+paYI1qJwcienxET4Qk
YNUQfzAg9XZGGyeW0F5gpcmUvsJPOdwYorh9IZzCSf2ktPZ2yV6VQw8dbVtoEwrceGGoRH57EthW
eXZV8T3u0Puim4ZiF+UabvsufctM7BZTOqEaPU++KQOHHppekH61dde0QuNABL5iBujWT9PS1sUe
cMjBviqXM9/+XhoFjmi+mG2oTOm6TZODBmNDOss73bJJ3yPz86/hZoKgu1itAIf2jvcim7nFcf+q
Hjy+xDFn9huJYOVn0irQ20/uq49MGKIrpBcm53aF41Oo/Jttmk3Ms/PioXYW2RxWbuCfesoa/EmG
yoGSHI415uBjlR3L6a8jBr0SbmYi+7uNdqF05hyjrC8o66RBEESIWWn9f/D1Duj3eFKM87WN2H1Z
9nKuAWnHdcE2XYMtUcr5GAert7PYY9bxLmliQbgjgKKXSxgUX4FVYbUmoHMkuh0yn6dhYE8foqIH
tW6hus4gk/cYR6/to0Pst5Zynggf4uHNIzB+dD+p8KXNFQTrv2i+pw4c7LXZO9ljA0c/xh5xPnAn
QDd2U+WWGHIzU5uMgZCuta2Tvz71rbYYRLljIo2qqEsib9gPNHOWfz6B9rpTs7f/PBnM2yKi6Xgp
Rnotjs1l5vaKOZZCkH7nb3U3Gh9QvVk9TFx9N6eJxGhqRXKlojOxlQGVrwoijokql7SRLJR1t2TG
O6/7vYt3JcFeCTXltTzn+vKtMmoVmyGkljuC2FrB9BJThchU2ozDQURe7m8Lw+BJnYcEujjswlr/
/0epr/HVAMZeZRtYfercKb7x1V0WiIrpe82I93L/SnQ2IXFXn3wuXogdTwrJxvFmLbUODxfH4vNT
EkJv8op37a3+I1nrLOJhhYWTSLVvLF7PgfCfLhMpBYOSieAW96DmgS1liG9CRS04pAGyMiIqx5M0
xbm2L3Ltwr8EZkAyo80XfimkmyjONrLBkUmHLcY5V8s7SDIjXEq7HU+D+PH1CiflSeWh/tdiGlah
wFXEB8fcQZyd8zWnN0JljRCYMir7wHP3DJTEsx2IrLokdSgJpbNqXZZpx6ZW+fiVuBLr1p8W55sK
rHTOYsE6dO1HdwoDqAkwrRSpWVuLzL1rELi/NlBqiazWg9fRjhB6W7YjaR/NOFWeD5HpoaIqvpVl
jKtiMOhDYyI/A2AJgh0QzPi4+N8N+yI4cNyZYNxpv80/Jwj9mHrjn50gYjVEjPHd3tm9/PNuDTl0
sQNPFIzwNopDZ8j4L9TtWE1YdtWL5a/HsuUN7hEZUttg0R4Byv4f0nU7LaUqinNYoQL6ujB6TLx2
mOsqv2wLm0eBMi/rXqmb89ttQUgjhhHzbcK8Q/dvq1YRYcxLkTaVYFGt7foWOuWCLSqdpQ15WKGx
BnJoxGEWiiJ7jzU08yyoQfPAPIhLaThTg1ayCgOFMqXjG88gfwDxEK6lblzjMcsD7WEx4Fg3t3/a
TZ1hVGnGoGfFtKbKxC4hcX71FlqZqtxfuj5jdCArMipzgl45nQ6+uQsDH4SqOkZn1cIZFep4tXEL
8feTEW5UeBysoUq6kKYIu2WwkHK3pSOVNcnTBa7eIsw3/2xecB+as/7zBdfA6cJiICF/FrxvLFfV
IzQMqZJwwTDqpDHEx28+Uw6QWt/kJ/K5/9mffK/gzOQ6yKJzFP4kcZmhTAMqlnEnJeQH17dw+Rz3
ZJKTliLYB7uh0zuv5OELW9mEYWCKGASPtX1LrE9S1ASHJxVxIULPXtv59k0f00zWucjx2GkXfIVj
FmxcZ5FZ+uY/hkutJNejh66Y99MnTo9NqOsjpxSMBHnlxw9FxCczIzYI5DJYYJ8Psh9HPIf0D+Jl
fJIYkbFyABeP4PqLcVaz8z1TTuJ7oXTdJCvCyLbgQXTseXmAK2r2oZwtofifkDnUsG6dx13Oc5m+
961ablDWlf88kAk0+73SaE/XMe6pOG+88yqzk+t+2TkMpkjRbwq87amJP2hIcoYD49IxenkmxZYh
IizkD34+NYjWq4EFHUtmWyMXXuKdNpwJtmjChZF76FL2fe4UffTjgnYqMz2+H72djz3t2L3wOX41
MbH/hXqQLOFaK5Yps/q+YXOF+sNHSGyo8yf7OL8PEkUI5wQ2rTnH3dU9Y+/vTEV1/qNApOGGPAhd
PT6ERkD6ZqFEDxXVAO4y8En0+0HKgwBf2U5wREUf/YWiPM9Zyx6HVV1wTCmVjmH2uur4jy3EeqaJ
blUD2WLO+hE+6/hgR670F1D3nOZr6a1ytnw2bWMGKqHHD+8/9V1MM+RjlSvV8c8myap/zJkQJV8T
fM+E/+QBVE97wtJB0xVHu/bInxhopl8lLSdj5D8OlHllELK2WClOg3OInMzwpyjTFDvgOX+w6KEZ
z4tc5gKI05sY2tGvt4+IbOEQQDU82PaJ2PVvM1GCvF91VM4FhY6ONIHR7Aq4LdXoJmKmQKlNOmNF
PaRUci3oT6SEPuZEfz3InHWIW6V2wTR281cRlEginjIU6OHmgxwFhI64PRrGXVYLGlfgGtxNj6uE
6za6MrdKio5kVofoeb38mhP4NS7kP9MoJx6EG1z+c9S7lxNv14lnrX0+LzF+dTBfPYC7Sxu/OWfP
bcr51AlhTgwOw4kqA43QWaSO9EnMT9dxnZ74p48E8AgDwGfw489jpMldu+1C9TFdrhMVV5gOFEZc
cymAQrtgQzPAze62cvL8VcpoxBwOjcXxeCMnOdm6uAlEzB2Sr7okseg9R8zrx0kGwsSWxr8oMRdN
bMENw7v+EQfHyYlJf7bYp37es55DPU2SY01f3AXn7QA4zb7XETg7r7EtOpI38behdsQ4SlIzA87w
Hen1TBzmc8WgIe0a2t+TAa0+E+ivXVyUGp+fTbOoMApDSqfgbHZWfZ6wKOgnGdo2Gdm3cfbZAZqj
gB0CDEigz2nWFSkhSYrvEIGw+/5PxY+VDrqfH5Odf6RE0JHk2a1NTlfsgq2Mt0AIfnj+aDJ7MsFi
Tyy15A+OD/FGjKPwCI2jl94xPc2QM4zEXdgFSkeABLoamQafRdXpcFR3yPDC5yd/ji3mTCY9SbD4
8Mj3L/abJtqfBHGXa3j2oo6DJc4qbvYHE/sz9uxE6Cbq8L0YamZcvn6X1ROSjp8LdPSaXj/Nx29u
Td4yMneVb3/9B1cBuU+1m1keJPy4I3RvlTjF85JH+3NwJdni3GhAKARI6j2e5DzPdbil8LcRUlPb
qZVTQ8chVZwTYYDlPruYLVJAdPWHyZF1SnQS9qq5S+H1VlNv5CSvl4IL3v1Yl7GboxwF/z9enAHs
vwlxA9QLKzEELzD+fcWCQ0fJL9liFoI1E+lzdrULRAc0SzaqpLOwFyyBI2tVXkiLn8yTskAkGXZP
uxlVIxZZKLzur4fSUv4j3JJgYzj/mc0n2Tn+eSWFp/8lk7siBfw7t8IO+AjvHYU1PX3OOYsDXV1P
fAnVY80Wbm2TGtYOZsjcehs5kzfSRg7aH+9XyRmB2DRvE0XDrPAeCkZN5VEYLBpEdcYC/cN5tMsu
z2OwG6AnlGQWpKUbOYuDGZZWM/V/Zn4aTi6Hxvdblh9/TvZB/jAuAfy/EFeR0T1Q35xpMl0uutxb
l/YXb/xxxkkbAWF9feQy3a8vs8piY6PYm2l5sPBrDfyulK/CF3kUDjkfYGtD7qOM0IBqKGJW2oTo
4+YPPxtFxrsjBB9/zYenYVd+3Wv7+svhhwG/keji9r29JPFINiCd5mLNwtCAJ1HXhCIz9PxBxvRp
pgY37fk+hhNiF481h2Xs373XqXdxK7fYT3txkuvBzaEGch9TcCntDV2oOU7TabNc2ofGxshOyRk2
A8EYWo3bAc82MRbtYzGljT9MHcelOJPudy7sCIGhf/qCKHvNLyPyYieWNUytOEfH352QqKJ7hNqB
WS7KS4xDS8Ydcxqlqa/VDXqp5AQqKHzgquDWrwRs3ARX0mqGsAH88FTLvwKcBU5hV/KkYbOqDnqU
dxYw9UrRLWwN5HdTrmZpks0BmPzzcEddvzn+AOcLlpSmbOtFgmvWfURhG/X70u2zMA7NtzGKB/Bs
zeMdfh+5f6U+lh6ZTv0XtzmFuD4xyAygabtczrgAfUGnr49cv9IV/kKIrcXygtcRMWBNJu4a9iQx
eJTeR87BOhIRcMsX0Ap2DXJooM7nIfkmz9XspoFnClGzcVBKctKf0/G1/StPZnwUP+WBhja6TTkO
furKcusjMH8kQUnc+pTwFfli+AMFLPRaCkXkngVcOiU/n650W1O6BV6/woLeqRJ1TMWnzadDr3g1
lr4k+s4Hv8Op2s+ftRSjeEsOZykDg0WIYMsi2PReSZXPdpeXU8WCSCWWgI1mH7mUyxNsKDNDgL/T
xfDYsaqV9AA/Czw26mGg2m+dd09g8lW05Xu9ZAfOlgino2eWQSUynbrHtGutHPiK5fPnXtPPZNe0
S0hwVObl6NlwnkvpR7BySYULYELiIXETclstTAKCxa+KQ6Z98d6Tl/JzNJNpNVLzmWHmr804w3M5
uqqS1i+0f2g/lYMCbe1B0EhOCELaUTiESNWyjO9J+hRXGX/FIEWDl2PciQXWvK9UL9vyBC9dej3Y
Oz+FQLsWg87UfP5qJrW+XlfXqcvJL05ObYx3I/0cPDHYP8H9k11fR5TaxnGnrGd9X6xRZrfrrJ2N
rk5Pzow+0I+X/Km2w65jGU5fRzRWtxF7ZlshGTeOXFm3AAuQPI+68ah+qcf+/6LZN9o5NhsBV0TE
JkTplOihWr32WxrnYfy4DnPP6Uwv/BiavBgFgV3bpYKT/qWB5Z0FPSrF9/slUmT1uDZ4G6D8vzlF
nfRAUGfC6lWSr0Lk5g+J/UcA6vuw3jzeo0B/x0dERvg391ly55HW5z3jNMdJocUlO7nUNolb8kXR
lpT1vhntuf9z7/eBe+2GVylzH1JdFBK5p3VDmij5X1CncwFXD2MIoLASi29RAqC1tSYC0BWHTs2C
sjcWvlhCnlQ13sEaPlL5L7NnniDBa1tgpPsLTdrdRMkIzd4my7SgK9cPvDx/CFbjCG3XGmJXkRNy
wxKIXY8zaEhr1Be5kNi608uown2CaTUOg8rdaYf+DLh8mFgH4WTTbOzxwsqFaCNYxXJkL1chRuBP
SmeicrSCBIELIetD3jSv+IKHL1z0zCAmTpEh2AfffuyVGlDWAMCmA82hZjqvOfANxaDcpHpnrxJV
mQ6A4ClVhcONFZAPg05W3EABA2nz9m6zn5mfyoFX2NKnYoJXJCDvq+7PTZc28kZd5ddroVZCcYVO
iwE+a5IfQKcaUBBAWN7oFCKMjzy5/UWMvsWNi+GHFDWdXUQ1eErLnf2JfDo0cS7VrQSjQ305aGii
I0+aAGF+Kce0ht3fvq0xKYf+idonOia6PxaZj8Tfeaf3ZG3/tix4IPuV9mK1n9Fbnb8wasluSiQO
lBCWG6T9L5QwokWHJ+hSRT98UG1nsIotbg/Jb1NpfMy85kj5FJsSPPo8A4EFG9mZmMBVVpDpkeMm
hDK3rVdZDlccnpavdGdg5jR1piCDmOZs4/3cYIq5wRSUjkm2WnmQcvWtBk1vFkY7JqRAO7dkJAIm
L/dXnILyysQd1nXfAOiGru7huK9mDovRIpgI6MpMCf2Q3ZPw21v8hCzH1q9CIt7WWfMIXBXRDpWG
aURzKuBBojX1mOIyEDH7uuBvUfKUWevTRtnai4sjRMon1wWQg/YQ5WxPLKHJph4RwqbtbLHyqKmD
2CL57+heTYcm3IlYQqig3JDM7ew08oi/nfq50euh/ZDziAlGHo8pr0vHDpccaxClDJqfV/NM/qEr
ift8hwqKxjohN3sqSnc8opGZas1hxA/2z7RivIX50FKXiEmKdbijKMTVNRtehhyL3WfLub0uOYJj
lIJLfMbwbsEYAGDmxenfeeHmw7lz6BI94mP8q85TddUlATLiJ4EcO+VfNQIfikHsE28Ujn56LWP+
fDG5vsEUZol0bg8hh9xUpvHom4gfdITiGmQjS/hxmUKu9aV1VTxXIxpEbZG8+a3ciLmTglp7bU9X
a8+7Ao/c80MXU0iLMAPrnP0VDhHqDJaYb+0Rw86Xb2xueihuVtesusl2W5GbyuUOMoSppgHLsp60
bgo1JsDOrjBY/e5QJOy+3d119p7QunMg9aVD7YmLI0QbHo0HCpsDR8IyuvwIcPR4HpIRcdsrGjdF
iFih3NsgQANK5Aq3ydSlQV5qnQExuHoY0+InnY0wadrvmFcVHknDEiapsILPtbAjVKE4Ruw71Uku
wqBHTABsDKkRdP8snFxQDNds6WT5MlmhUA/tSiAIr4Qc6POZxUVSBzTi149PGbcuGc7nN7eQf4us
GPQmHr1yygzaxcmOQhXJy4pzTOmBsdIfEkEzjU+ZcwsZDhIqWr/y7YNixcJsAUiwd3TPsHsqqpHW
L/qA51YoRrS9xSXHui30+jXiGgJuwvZ6RAUtRNo85oen5gYhDQOZk934fxgd1OTnWD+zJo0kGZYy
k/gQARyhFll8F4bwPoP2J4Ft4rjJKwef0VTWgtsqaEGfGMnGJxviPwQUXgDn0JSeisATK0xSG+fK
Veu/INdXYdvrG+WwXYjjtZ5cUSmwb/AOar8VpGYA5lbM6rjLvfKY1Ob8j1gmQGr+wxDMyWV59+iM
PCqPreVfhybvkTgHJccC6xrDtTcl/DZvBRv14BOYsTBIt9V1fO5H00+I3HCJXh7FVfmQGvQeKoFR
OZLbqJn647PwAkL8KuptvdGZ95ysRot2+hk0N4OmztCPSUDT/LgiQd1Dm4ibk3IQYuDXcGvSeAmr
jXeDrt+9oLHNd/rwHZ4HH2IN4Bb2XH0icRZNgdRmgigzfkXV0a5KtJSfLE2C65zB6Nd2DquejULH
QaXgi2JCsw9hhWEkPlxY28H4YDaJGUYi32PL+hIIxUtD4pAv4rxAZ5tySNo5fP9aTPDiCdLcKMXA
TmWArU/uJBpBlf9DFPpM+doI02dxX7a0bFg2Pqjsf08bdv3VWimMsS44CswnzFmDTBJ7KbctNXgN
c4eYiuxFaQvK+Hh4l4Mv1f+qIuAMW2o1D33qfQAgXHDd7E0C1VizjlRYmGmv4/4B37EVQ16+KQ6b
LcfqhShlthb0jfw6r73cSG7MMx2GzHqh2eC3so+AsmgO3+0XdQWt49RBMbcl5FX6vTJyibAF0rDF
64R2fpKJndJYpSavulO5TGOSMyFB0X4+RkTwadt6nlR9lmMJeQDWqKaYq+MjoyGqrvq5TJ7Ujg01
zO3/9iOdJ612Nqb2PcGBQ92HuhNfPJuHVovps+SDg5fBpb01EVnEfWmWbrBm5XVMQGFjscEqPEeR
01uhqkIYKeMcmmZYFp4JGJ/y8ekw0LqDB+ULeedb1BP5h/e0oqjlfRwnwPAX/tGZamV9EflIL1V8
Zz+nDlBXDFBzvGhFfQmYze5C8SwdaDSAqR9jhYky2salk5g2O9j8Gztn5dO2VNKnA8xU0mJaY3S8
wd5STgzASy7yUyjKlnSaIhamYhDhS7czdyqeDvPtYG8eO6fMcjcAyMKbZriRLJuNR11d4ihwtuZ8
IQBnx11fVTupGGxVE890jGPZptOTIFG0LmuX03k7BT14tyaLlaLYghU5N+krUQ91ybYBenVFjFxP
x7vJD7pE6Jl0cZ1lcZtE80VLvEyloWCu0C/f4U7qjkaUQtrcOn6SsO06vvIDy8LDva7nhyO22ymz
n9R9oZDpz6nNwobonlNyL+pEn2u5SMj8IGIImwRDNMfGtRbKjwPBOW27C6VynDC46VqcZyoazQ6V
6PYJaNACFPOX/RAyvK6iF+FID1Kd9I05BVCdYOQbxA9EFJP51qS49gSf7SLiQnYKSJEO8ErF7pru
UAD++ZT51C/kX1VqLQkSK9J7rQANwkxjqOvAZEabOsiO4dkegENDWQ5YU4/Ss5Ws3JBgC+OYxsPh
8FDRpjiZOmbyjfSDNgG8OrPbksiN0tFrcuPKPiwpk+fzJAjilz1Bbotpiosd9ogq2LdpQK8dSQSm
6EJTtI8FIeB1NXONt+9+wPVuxd8aUfJbLhB7huYCUeYwt4VzBs4JzmnH3B4sLWt7krdSjNYxkZSe
shqP3Yyfw6cX/1QyW1AOn6jg1iA7+OwWYz0RnNI8OEvsbmxTcHKVztfkLBfZMbt4ScDhMT7GB3rX
0VcuWPftLwQ1NkteKnqHT82DaHk+chNjUKNG3R+pkYf9jA0N6kNGI8vDcBVG8nH/0MOgGW6/RFeU
UXhRSESuYOr/DnNUsH1Dm4dcWoWT6xfJeoRMXma9nXLyqYqWovYv08DZvEPHKfNkNiqJT2LO4vfM
2/fJr8YYJyr8AlsOtZT0nTu2lCnD1yBtfWE87i+d1vY9YX8ImVGerVIRMPCp0/aB/anE/NaRFvCr
HfwSimbHSfsktiftHSBWvj59igkz3DoUgWDF/uSZCpa3tCE7nAu7BPF7+qwcB5Nkhl4ntjXvuk++
OZmfuLRGQ5NBaGn6DzDbdqFpN3+nVfT3gyulTmqKoxgWBIQX2F2o27yPKkvYN3ezBSzLzmCWPy5v
stEmivsDzM4urI3Gao6Sdb04i9GDOlzSpHcCQJtNTCY7GGrSsKFtZtagEDGuMelq+X15GEuo94LW
LVdTEhkaXfoKG6zBWaCNrIoK1kGzJq9p94NYXoAmq6iwNXCvQGCCqm2t7R3zLj5P11qDhGQS6NL1
e1IO03Rhr2kiX7q2TSp8MLSt6x8Ce7ReQsuz8G7s5bzLRrhQdiyciljIob0CUXSzct4Fb/heqEhs
gwZfeBEC+DZyES/vQ3jN+O/T9sRdLviSRVXbQ67xteAp6n8exe1dxV3/rcRLyomXX4gQSA2WrNCy
FkHcikkxJucJS2GrMI5WhWb580F10ViGFU/glfZfM0fdvyZzDwA9zBfo9tvXuHW1U5potNDxDpId
AAnqTKZV+4PEnsNUr4RPkcoUFBzyKp/pXjqirxemheyaltHFgCQ4J6oPUWKv77KeTgWTmHApGt9j
OebZaHxkUIxnNe7P9Sjp1Nbkxf5DRhf75FXL5Qabs1Q8rpc1XR1ZaUZ68czKNbovCh7NLyVqCXT+
VGQhKL5v+V+2rc2xrEz7qoJWoE1+XXvQ7we8q8G9otdLTkSLal2li9vpqenGd19TpjGmEM0o/qGN
DO4dM5RH7wuSyXojOEU962/TBlbNirzvAMKvMHZADQNBZcW8UUZY5T/HDJKlJgRS59y0pkHOzBFG
ZvEU7xbleqefn/Xx48x1VYZYAdoH1moG5ZiBKFyjhkrt+zV4R1Ibb95s60rVcN0zCMzjs6V20q3n
JoMJSG24HG9iHVZtkZFReIarB5PMmVL6h6uR7X2JPDg+WbUMPVPt+BKnn8exhsDqgMUKV/dBrt0J
2HwVUjVOr2IhYWTM3sXZCpcB7U2Nq8j78B+flx28SQeqvehU3aJuuM0jFhfLtznVY1DXNmlLpnMZ
VeRM2LpE/CLIyRmg4mtmtlHlt33VrF0dmDKm3w37cymFD0lK5sOO6jFAcE8DPs7ETqZSaD0Y9ED6
XHxcmmdeIbBV4LfEG/pgSg9I6UEF6RWOcGA+X5lKwwziuNKTkJSSUZvw3pJDOMoInQEwzH2P6PLv
mzBdhwNPSzs+lfjDc0LY/A8m9krGkGpAAqgjaKqIrABBGu/Gow7F3zh3A0QO7vn0fI4jo0k97v6b
JrDkHWxfNhlIm28yMICRGLhvSdfzQkqAhi/B+VdYZO0jAmWPLvNsKiSxK9E/aFWY2aQ9PI2AXhzZ
8rOWR+fu0KFa+gh1ac04KB9jmDdUogT37axpfIg/6dPm+KVzNMPDe20d8ZTwrW1ouvUvVY2vT2sW
nFZBvHw8Kjhrf4kM72yRNXH9zT486Pg1QWY2FO4coQDON5ErxIMwWhwZsCE0godP1JJVkgkr3ixU
rmvMc5pUS6a1ARJ7k4iuJnJh9ofLjXqILvex0OAgR4yGjClMgZyeO0y16huRTUtPQ+Rh2e0somjw
/GDhQhUpY/pnUK0lVCBimw9vrPcdiLp4bhaRgIgeH+a8x7PsPG7ngNIkTQVWF65DkG+Ty76TwS57
f5rtKqYp6y1YebMRPbfyLcizT54w21E8885EHAPFiCBgcjEPAvvl8UKM2rVqBWLtZ7oj2nwndp7y
snoR0xhRR4fR6cWaSQy/2vA2ke9GWWBS4gCVKEhpdR9VM/EYdBViSQwfE9/5UcVm6ANwdSnXnZoA
fmepfFMT4sCVt14x6cQzycCDwtw45WoMbkXaKd9+M3MvHyWJ2G4lWIMOE/mAgy8W84i3X/3oLT7f
5FloA+Vn5gmboN6BjFXoTSOkVrcsCTldTaRzygVulDwphLNYcGTz8K2R18uChvPQj9NQPsXrNCQZ
w2er4LVDDPCm3VWRB2+TCVgesNsTmCojxdY3ShPJqiy6qKkUc5/SDjRlxZAPIQpAhrcXNd0fXR6a
QtgbKKGq8uiIZBzB/YXbU032YCl4SnDLm1ioq7Yd80dDm6L+V1Ug0wUFrCkLkIMeEyU81txRKuCf
jSu0uXRIKuVrfX3p4y1fa1SdWvug49R5xbaFUygzyyBuQL4WpHMzBo7sBIQu6fIq8eDYO+kw5CyR
Yyb7wa4pj/RENhXQ17Q38FMg4eFc7GK4426sOytE/EYgug4R5NNKCcX0apfUdo1Jb/pGw3Kd31y+
S4hYiAyE8fl3RWKV18EAYf4G9XCMTZCzGisfxyX/xAJeGJaY/uLBoOjWcicOn4J0lP/2EaJ2DjTn
Kbz6BfSQPAYrp1ZjdjPh3DE53WVUBJCcOFD5vaMUqsKTUzaGCxfAZYQAc3f6GC3RP/ojeZD3fy+2
3JWavwE3cY50vZrNKwSWcbCybs/vB/9+0GwETOIJb5AW/4m8hX333YfIe/8oawF7JJbYPocMjoqL
CmB2sAmGRsC2WBd8pgi8uKtPVxhcC9grXfOXTnbTeWe9NghQctwlsYzVyZgOIOJdoEVNPpA+/BMy
5ECrDRab5HzBrWI5yLuYZcNkKvTfcFHGs91gJtc5lI66hP2px/y7QKKCJVeoM7EgCwt3mILJK7Al
PIq4O7FRYkQu14Tqe3VVxbBvt/A70FccjeBiO4JlJYqlXXMJYSQ0VnlZnArAOYr74aDi6bhkwbcd
LbAk4dZxmOSBR0bF2LAyK8F0q7cJ2wsVklGhDsfXHO1Y2H6bRx9zjXTrRTfGLnODM/oGklqaMbx4
3krHZZD7IX1f3feHqDsXzBIBZ/1dAnk7qvimNP8RSDacugE9K6qg5SlIZAsbbtpkkPfQjXfI2kEZ
gWxiVHlPRgS+YCTO9J8tzOTeGwOU/DtoP/SeyPV5uUtNyE4QHUS9z7/yicF09Sv74D22kdK3zAGf
hT/zg81DMGsVNrPUp5WBxdmIsOawGbLVNPspLrGaJdj7BoPgwovQeOaapLJ6XKuvIYFLo1Gjoaoy
++t0H+YjXAvYsiD9Mnuq8y64YS//AHmBwOjf0csss4QWVKFL4kGCn1ksJgaha6t8eRUUAhRlfqr8
Rq5eaBWrY3YBSQ9hwhiHRZTlbom1dJT+6JD68kGWmxA657lLzdD8rFe79vA0/3e+1WdcgFd6Jhh/
1eaaXS6GJwAHjUkgIcxoFDGXPcenAfsc0wMzT4Cil9YcH7kgNO8KWWyIBKaUBpl+mE6Iz1fA5RXL
TJasjXTx46vK+EfA5asIxqzdEjUKpj6QrjPGhsGv5jxwRcmUF6Fkl+4dhN5BQQAbDWkTUu6lPlGh
Ogad24h0nC2dXSh6hoUYQvtXklXsQk9bHLBGuKLYrjqEH9CIpYbpNE4om37dQ5uwPIcJgAWETUQK
0O8ZLCbO8nOZcU2r6koAcCti5WMVBrRGAZ/nz02i7YE6h85n5Yyd+Ggo4l1uw/lgtD6tR1wEa0+M
zoQjtMuFW4agUzsuUpiBFh9w41QlZ9y+waHnwHMdOyv5yIrtyNn2+og3OtYH/50aY18cZsOiM0ns
JRnvuPYE9sjJhsltuh7LLwE8NpHDtR6kXWiyj9BvNX6SXjKGNPvTxeRybh37CTpdSHx8n6kmfptj
gmZgEG4Vvpu+SqIDsfPfK4kErO0AbVqA4eM8pnpDXFFhK66wn/BE6dCBQp/N4OQDc2fe/EGQqggI
jeeUBb9j9x3fNN5lAK4d+T1PaJHqxntJhh1K5O9fXnFi4Ci+JGok5RcvrkiYeAMavTfCtoTmXaDZ
yorb3+VkgmkOLZlhDA2qbtty+/97cVAx+CXVBpsce8yfzf0ZQOU4D6anWCue5BACa7y2/T8wuDCM
TeQBd3OpDvY5Z5mKzX1wnnzZZH3UGI7M/odamo3a194czsoDhaewGr0CZCK5flZ2GFiQ6q5nwYiy
03R54Pzwn4Cd+y0A48dwqfCySkGeULjHVSfIusnQgEjfttsm7kJcIpR6a0KR9J7TMSz+THUhbDy7
8xuZNDR3RgrjiQLx51SDxBsMiDTWkWfiNzCkzPGxhYfNl9qJvKSNUW8d1fMKOikDRnl96T3V+c2Q
ysJXPXKsf8Bi/Xl65ZweJmT0RYnY00KpI9DXRaNPPNcEnkFiR1cBZwNqCsTGcWuKl31oz5tofNPP
Qsnqr3Sr9zEnOCH3HPz4o/gU0O//yA0q2AHsNs84RGmZeXt0icmxwQIsP2thzluXnehuTXW4x9Vy
9Qvk9LFQXxiaWv0FoFkukZp7iaatxdXYNqIfEAkVhDCQ1yPoU/T6kIH9FKPhVfZ1vH0bFGaJRzsO
U0impciNyxy3GK6nDSNjLuynQvqFcvEXwSovnCmNW7ja1hiWEwLJB/49hMFq66o33z84bNZ7KRPp
KRRVc5amEw54nl8BnUKY+amWRmdLXe7JdIMa+X+cOccReb8oyrbDZc3GzwsiIOc6T2AT8O4iCSwN
c5IkODT36THsKrm3ZvV60mtho51GNYHJnUKMowzgmA6+eHCjFxZAL48nfYGjsFMe1m7O2eUBWW0w
/OF0ql5ByYlp79nzdNeZG7iiw1+CilYM0cUrdY80OMX0902YBLVJHqfDylYtedfKaTVlPETCDcxs
48f29v9HchssBmRofyWiE3hzLJ/QS7KVo8dwObvQadTWfkomQXwXUnDqDRdqBDttbtzZJ0kX8NGr
WfKfHmcsRh31hKB6YV0+bk/Sw3/FsAMRohid97dtKTyBzs3RFMacV5GMhkP1/eDLdCnsKftdP3Wj
nqXdjUF/DmWIVCKTRk4HTBQCCV0ufrKg6eiPs2FPVBFx3ookPLs6/hx9Nx8RwkfHmzC8Cd5Wir/t
s7us17YHv0wBb377TSuqFp7eFh7ulQXOuC/2mYrvf5POCBualWwzE7yO6jPIvcJohTUG/lKmoSCI
sX9gqPWKafr2zghP1EiSCx16SKK0371MnTk+HMIHH/wmcShf0hMuq6X39WiMWBgaq0LiC6xc/+xZ
MJ17SV6kkd/EN+hOhT9GXhtufy5YY3SXweh5v3WRMxbgop5acnXKhkcVZ4aSZ4yiL+E52t2Lmby1
6qSszTSOYVMjACvMLdjSwSLd5SP1gU96q9xntatrUjO9OmWsZQLZcPVyJBrQuQS/3MdTkvP/RVPO
vOR4BpYo/1irVEOo2W9ugtrCOBJM0OWnnveTkkfbgk94GqEvJkDj2NvFJ4v9e5YsSf3auxK2p097
U7YVdVananQFOfooVMKMeUOPoHiydt4UpSp+G43MyRaBfGB5R/m4hU/f0XuhPvsIgTI9k9Wfh9/2
YfSNUcR/UvaQnGfaxNrS+tl0/bNi2BNx0pw4eGCpRimNVOaBfrVB/aPybML82HG9lqLRPItP6zds
28TwesQ2U1D9ZTAuFhzWbfobqdGxbtHv4Rd6rc3ATAvXc1jDr9xUYn3FSr8eBuVe9cokuQE7S8OX
LEaZchvBMt4/02LD/tBLZ7kXpviv57dCFEwBRV9uXkjrVto1DRvLTt58DGVVCY1NJs+zbLcCFrXX
5+2wHGZLubQU40Tu665iHFbEx8w2es1Bt3AT8iuBmoDFR7ksC7fP+w9Xk9UH8k6x3kAYv4U2//iH
sVhnD9ttl/dmH4BRDIVwaLClxXUKsD1cz7LyiK71xVLx7je61Fm6hCz5JFUQ4ZvAo75gtWghElwZ
7OUcgJO70lRVo0qCWLU229NnpfepiGmJJQpgF5FLWV9rKcTDjyrGdmrQq8Ibuqd6yTwQccmb5wVr
vablRtcRLOVmyY8Lhs5JbBez1kFViyxRsYU9QQ2xCNo8dbpNB4UZhDBrgmVSgNmZb+MdaD9472wn
4tXVcEbNqADtwC+oWZ2PkbZg5M26C/NPHOpqXge5pUeZlTkFHKOrDPPIkzeZpNBbsmBGtqMpREyU
KFLupUDExql+nncul1dDDXkW+y9/UydkwGWpknpLyx01Bmm1JMgQzAuUKwXK0RC0By0NhoKHLfC3
rexGaFKWDA28mOHzRJBNC0+0q2uRKqIJn+wqxqyDDHAkh75mOEO4DmYcGHAjf8FVrcqRnwhoOSUe
msRF/ZC8zuOmkmepybp3KLT1irmvDh5fC+Cbg3Mqa8TMit3hF64QhrMQqlwhstP33mXgwoswlVZR
WFqSquHpenCR09L96fu8K90Lk8dObPGDPnPaTvwnVfh+4ofXK23phBCnr1s1BC1q/9SsJdPXcKJc
UDH2HpFQafhq2L1WnOvfDewOjBnyZruthSepUSVvcRjZ7UnmxRklFTDgrRCluERluwicWjJmYiUR
THdleOKOuTj71MFFMjcSWUeg0h0cYtw76Z5BXg5VsGvokniWp9mTUSm10H4/E5znJFpU90F/4DMu
bT0x4pI6EnnIQMcaKj+6uiavJi4AhpXEWhBv3Grem9Ybk9xL+FtGORL0KDbAAmarTdc1XHM4Mp+y
yXEr3VMUP06xn87vRgTPXgBIwdx4pBllGxs9FGx7vHfIJW2Zb40B6StwioI3SfCugRNV55WTjy04
RjqjjfZdiZDMIcWHc2cuTxv+PItFYZm5PB1hPbw2UIgPGDhfGkkMCnqc3zT8M0+6gLHxo2rNWV5n
r15pRi3R8DliIDlg1YotTT7oDl/+7QVujd6dDXzMCSS3QaLVC9MM55Jy0G1OAni5aadSQK2AWwdg
tNZ706A8U4U10ATQHauuD+CDEYShf1Y7+BhAndlN05UGyZudalO4CUinWU37jl0F9+371KoHot24
58dGjuaO85nKcCIbqg8zuR+Q/qQGwW58Z5GRErFyDgshVcN8GITFWaX4IrFXjs5Shqnu+0qemffs
bZ7qOroFDdX3hunOqpZ+x2jyt5bUNckThOwtlIu+xCfLIH4B2mp7oSuExC+xw5D+jM9UHM1HVsuN
dTrYDs6YEjUnA197oCUSe1BSu4bLA90UKDLej2TrFWc9ddeCeuHEbPpl4xQUDWgqW2/SeKaimTMz
TztXNx91wyxPLuyXZIRR7YJ3YukpbEeW4GhIRzydxRRMJoT01nL4iR/VpltJRYJjGfeiAJGBl+Yb
O0v69m7CNDN5RYIXJlp3RkgT6vx081gXRKqeeAPCUjqU3dUKu4SwjcMMjlNKwFUbFLRUsZXP1Ban
STTuf8ZgXug0Q40uHzGnfoBp22gfKq/DqZ+8zawL1EMDUla2sr6pBSTn+UlyW+8SU0TAPmi1LsPr
KsWYIniKH3xBfIE53Cw83F9oGqxjvsAe9ogCFFEeDKeQD4IbGst5njR0QWzqjwDSkJWbAZWaVhnJ
KhWJSgk6BnRdXDQQJ6qQIqTufPYQOa5R1hiYWlEENEfDNrk3ArUNc8Nx5WkC+Wxt0DMYi58EXQ+0
i1rWoW2ocSyQJQ+UXYh/AUfh7lA8jW+nv/QNTS9so9pPkkNq0W2jo+dVCXNYKKT6AXNyj+fBIDum
zaJuB3KRsK2Z2ew2TS6/nytchLIzP+dzYXLEu1IgqQNPNOl7yX97zYeHu535vB9qdWrXms1Ysc1W
7bOwaEnR/QPA3M4CI1jEdPa1rkMLhTEWBPApeDFRGKm2wCebkYPy073WCEB+USYfDp7cUuoeMjur
VKy1xSvgBw7zSViiC8JSz9IF4tidLz/AAOMHgehNFHj03gX9SkB6zGxnv9ORZaiiD3E/bID706+1
8JE9dqioxhTCaLmQxJ36SrqqAKm1cehKCDCvXwNDG7cJCktZOe4L0JMWXe29IEzHZaXlmCDtnQm6
UPDuv4lW2Seiea3Xi6cNpRq5pWmf9I6axV8x+HqNbfMe/e66bLTisWuvKwCZIPyEGUTJPS7D8qD3
KtpEZRPsKQUvdo4q84UGUhiCNStrjFlmncg3IFqaOGeCvgVGfUdK+cH2Kb6kPTCbp6U0sCIZqMhy
CMe3vqg0LS65Uu8ultaJzlSxpX38q1BoSnJmTEozyF+uxc5Xlrh6xibCL9nflKD9pojmVFxpdhUi
X+Eo1GG96hYZnz2wQ7+9SZ98dekg8nlLMSXQYG1LZb3fCimEI4g7rqY2gbA9Awy/sjIoIx2tEjA9
yjkvVXHPWMOYjCMtahkLm6T1ckXEzrUnMZCiJuimD8qPJGqIaUoh7EFBbi7GGeL9VnXV/xuuQQP6
cL3kpjEYoaZNTso898VZpzyPZtyRlb3p3z+3mYeEuUWTX3wjIWYSBHb9IJx4DAN5GB2X32uOGWcq
OOzb5+wY5y/Mqb+6N8zyUX9auXRjcG50jr95074RXdnFMuU/HVufFl4msWo4KsF48hJxTjTZYgI+
rv1H6M2u8Z6xP8UUak3Zf70RIaQGi8DqTZFGHgtw/9GNBa5ujDmp9gtwGDnc2bXVUnUj5wBwlMw4
8zvcYdeR1OQLj3YhiUPk/Vs8gXj8CKdZt6ecFtx9Ogzi7weF0x4MO2Sif2IAVnFg9K8BUt+es9LZ
zu+MOi8p5qNbHE5C+ZgSpsZ9KEh6drRAzP3vBN+9ok53fMHz+gI3Jey83r1LYQA0RTjv8mDLgJQ7
n2qqFqlI/TOvaaf40SdLO/Am9wwE0ZceBzKYwQ9XAAlF6Nmf9xwWLJVTzi/7biGL88+DEicWzVxb
SHFu69r/g7x/Cs5jJg+RfCzaH2ZIo16d0Fo9VQm4vsWJoGyjhsxGZ6PQ6odaB7ll0RNrMwes6xqQ
tQwVKKKWfTl101iatzFdn3Xz73U3W7RIMYN43gc/b6xQ9dTqlC+bLNwlhJA7K0k2NXOMeo2qyQxJ
gPM37IPZN5rzF9uS5PYkCfgIKgfa0TJtB4Mm0+yBT7NjR7LvnunndcbbWiyAC/DOBN3quFifYCz/
pceUwcF30ZjSCk5zH1eN4qbS7c5qRn5GO+iP4SwrZXqrHs+BkYwW9QoSbPwz5+H4NGYpjmC/D4sr
GmcDo7twAN74oXL+/P18tP9/WyhUDmDUxL7V1YGgZphuZFEr2f/KGRJUgHPYt6qqOmqkSwhPHtrf
Uo0WT3l8cr0c6ThTFiYWPNw+vV7BqYdI10D2Dzgxwk/gTo1kCvyfFK6JVBh+iTp9ZDuO723jCZTY
TIS+00AkOJOI/VQL68y/s5LDUVdibWROZVc6SZzV4ZaZ0H69IVvVUpfvgUuTxA8GQp1dsja/7b9R
zSB/tkPZGW0A5/TC2+LNgKzgMhSwLhWYAjNYg+skUz82nj7r1OG+hOzwZUbvANABzNNpawhDt1EW
pnq8aQ2em0SfcdNRX9dmk7+TmUxVrTQVlaaOc35lgk7nneD7PkOcV0ntsdeiG2VATBEVKXgi67rH
rehpO/x+gmz1VfCVw2UffNEx1aSR6R+MzkkrjmhhpmRwGY/CB+RzdvsTAFgydOdaRsx6niXMbu/a
xzETGF5LZqydNDqS3xrN2Bu8P0spdGlJcnudY+GPG8bJyV/MPXgoM7rYxitDCQbEmNhQHWEL+9YM
KWv0LnQBxaBYqyzPnCgT6BVsWPFSwcNDYTS6i+oY49AzpcTmjjyBEG6ov4yg3HpvOw0OHejWol8y
HL3IqIApcY9EYwzsu6HtAYdHYokFQT+ZAcxRrxWonfgbQ/QjDAmCUNfZfQkkBDpSzVLiKp1Cc+11
ThszMW6oheiCRiyiPXIt9ohR4Wac2r+n42B4pOegxDgdLfIuxtlt21P+FM8ke22DJk+dMSJzbV4m
8qpDS1Sl9FlqAYOOXwnNo6lOYrT1e/2C3WdVjugp2arb184xFz2RFCOISeThMZNaZi6VlLfcImPC
1cHEr37bN5OlA5CHGTshVZNEAF3NskeJH4k3cHIegoZo3X0hzQ9bi3go0uF+zYWsrYwdWAGjQWm6
ovn7jFOY8BsUcWBo1ETG7OO5WTynzLnF3bSr8mrDqO3zl3bNMWQQfqckaF+g1Oe3AK2j3SP8//MG
uY58zmtRNUFiBm+TSqr19V5T8LbWgZlKxCcVtE3naumDGDyyr3zNIqKfgSsfhhHMeAns5aidUYbE
B7+EzOp6eo+aO97RJPW0prI8ufU5ZZsex2tCZoMuKqpNCNm8x/lmJZK82qI1BKxJ3KhKKCExEjDb
DS3EUGCUmUL4PFVhHtqaiSOxjBcPfh7/tNRXxeexv80UcVMB+WOzJBkI9qNhaZCUW3SUOXga0QQG
CBMsOyLCjVPexU7SKps9dVRDhdGLhPXoNcC9hDB7sRygvO061fbtwot2peXAoKRM1TB31CG452Zc
Olt7Dc6/b7eSzjJEWxLHZRmcRQw/Ls9E8a3Eem10nxLza3PFXme83isO4sfINHoFysh6eUxcC4Ud
+GzAjO3zubfPnzKlMA2lT9V+EJ2SBeDE8F9IxZ5F/R/3zRfsQ4l9/dzn/nS/qKxwwI75OoNAtMvy
fH74d7ZEXMc4lvgM9jPdJ9VxYK1gsjUgsYZkGwc+g+iYgjJiQ8gPHHy9g44K2ht+C2CbyxKwCij9
0AudRL3C8U4HLC94d+mNV/0FK1kK2d6XbP2GNvHTnpYUs4LpAny27Pg5pB862Hj7uS4J/Tq+8tio
KGcJHf1jRBgQkaqVnE3zfwLL+mtEtqxyaCiMwu2winIMsoCBk+hEkFu3mPjjWTIeyYN8SvBFClEC
qN3R2bTjlvIrdojjAvCc3rC2OvzcZ5FQtsCOWXVVWu2Zg7JX9IK2ntiHp1tC1CaQHIw/dYeMb937
XlLWkA6hTzKQG6RTUg8ZQQaEAMyoCWe2OzqeZmsdEjC6RfFXU7JyEPLVLH9tPFDRzlafq2mRIAn1
nR6/KkbAcSBCR4PMHXxZmwDg5iqFRQwJmCgOe1lE34NuEOt8qT/JXymCAD4ykLWfvW2t5UXeMRzS
BdIgVV7zLm82RcXLmlOsoYTR/kRBKmYlv6n5Lw3pESE9lLsmZhBxKXpObSMTIDxEzq0GDpj3ESPc
Obhu4WF/cxtEeK00ctOuVpsbpKGtBSlpdZZ4J4PZ72vDDuBbphZAZuXTi3tH8+rcTHfvZIN1r2bg
A9wA37ZGWhpl9L4xlAzTSWHWPs4PYI3j1riB25/qu+cmsgcPawqTqx0m+zFLEfUhRnSR1WW8HsT9
agC0ZbiMIU3plxFUml/Baomq8jMZJZMDQy2B8Rx//8O+w+XXUgjGDqzRVzpwnkXq+aUDamHi5uG5
CoI/yn/3OvnCcEcC8OAog1ITalgvYDKw+WM2+vPRxNdOyvn+ErhRic5pyjNE6GaHwQjtm05C+tYJ
lHpXYqPV2cPBd9ynX7bwQgnz5DFOfaY6wk9dr8RGMjLZ9QsmTrGDwI4PNZmHyRxQnVIP7KFzB4vy
aA7m95sqc3DM0BLyiq5dWaJO6gWSgxB83Mq4iOcapcRwwyOd+fbMA7ftUl/6wKM1adenqH5K7sjY
/MGrZgRWuwaM9wMjQxXASOszkYfYUhO1lI4RZ6St1tQLCGPuKMZwp7p9fPiDQX9feZmcMPRMcfjY
G29FxZNhd8CYrgMAXQWLbCTrNcfWwuFWmONhYvjYNxik+CxRB/sShU65Wy9MO0t/akagnjOYiCJ1
27UZSNBCCDs8xhYxkdTzyOpen/xyKKvC/tWHKNtrWmwY1P5MSwCsoUHGfHWs9SeLNQ+sMHjp+4ey
TK8lDN0wflMu6b6nK10mZi+OPBXWtHWQYXwT8jkCRjnKSPPy6z8Arcq4E/5yml9v8hLKL7BoKngq
qUAhTWytG0+7SKtMnZgrzSERhOsEgINaCfniSSXJAkGhuyybpfj1ld2PDGG+LZ3/hqEk7z17c6tZ
KAOdSu3rn2DG5Qg5eppsTwNOKaaYy7ok12N9t3LwZS5aGHRAEyfqIAQ+9Bqht17y4N8UxGiYvP2M
5tGv0qXk1Gp3SPjmQoIe0INtAhkS7QaHYo7xZ9btFIPy8M1LKBcb4yVpPcAnq4BRF+6VRLDOj5nD
bG9MoJfbpQAiu7e/W7RqwJhKV+XYRcZMb3lWlEdQLnZATVM6hjAItpUu2zSLjPHQ0FCCMq1vPzg1
PkvD0cTGZ6VvRQBQwAYZtcqrm63j6J3qxcs2w1JqQ+xInCsGDhSPnvsLfBIBbzdFjPs5BlQ610nY
NLJZBuWo3ZM6wYP+pkseAzsC0RL7Y1+HXd5cS0t8xJm1UKys2nrJpKIf+itcoI9v6RP0K3y1Qbgr
BEBGmC0Xl6nnSjTPIc9HVQwc55nTF9/CZRzAbjAwxr2/xxhC9T3qVg+8ZdCDOj2BbmQz3SNsisl7
U7/MQL/wW4aEqc3oU/vEkZJZWwNRDwdQYkpblURu7H00KZoMAIlXHYu6xIA32ok2L5yhID5Rno33
3PPzHvy//kK7zdyInF12eirqkfDei4QfH4WoYy1rhJLp+LEqR4C2IIzeAXxMkM1+yc2SWDWScgm5
3Cr87FqvE3CYX5hXG6nFeVz51k/Bqd6zLvfVV905HA0r/b5u0vFt0KPX2qvZKPj/RP8HSpIYClnP
cZfuByEuObuvT/6YiBeipJRcImCUTRv6Un/RzJ78ykAW6H6ciaLIwfO6bMkKnFhbmHfAvqUvDhlR
UZUZXwkNpdys11mf/sDc3pGOz9T9C4eTZt5Iq+e/a98IZG/mPY9o1oRl6vlpflQDOeBJR5hTWQnH
+luWjKmsGtryJNda2ztXDTjvEvaGVOjjgvyNC+k8mAhExbaC+IV4kL8zT5wyAqfc6FQw/pjf4Q4q
PB9jkTgeaI3zk5G4eYgLI9qLn5qClsU3wZTAZpr5idhpSALRkwlC1obX1PhqUTzOs7Emzs/0yujk
AOUfTB30sjc9WY44b1Lt75DTEXDuqdcF3sv/bh4AobcL6PL2642KWCFVzGtrBlzLbLq/BJUqnjMh
YsNTdbh6zbBbi0A6GsdKFjYIOx8sLw9hgACMVreL2C9mXIAl87X0vSRXna6uu3klks418Kr9bgbs
WtluB1JFbBD2MHpDZ9oW3eFn2Em40GtLz1L6XW0ng9hpr4ESws08lrpsaMr5gpNTPxTAGGoYf0w6
owRg7Ue6lrsWZl2Os3iJvaDzbgqx/+AWHf1yINMX7s1ezA+gG26X02CU+jGk7uZFV5v2aCeJh2FP
/9bHREeWPShKcn0urv+fqpVZ51t0tQJFnVD8afekdtUnHbBb6EEwUmWHSZcP94hKJzObf6WjC4I6
RkxlvzLJF04cF+BlOT+dJbp3LvmNjbHPK6yAjLeVreP4uro6OiFz1oYEU12bxQ7rSAvp3tugtozq
if+2J7zYAVed3M5o8PqO23yFgC/dtImr3WEPVmJ9tQCMGSlDin0nsdpLwFVKYnqGIa73e+ThEeCM
Q30CKPV36Kid5m0qYvJJeYHnbjZZzc3NGJHG9L5d1dMjM1pUonqGC5YkGGqoT/NnHVfFPkJrctQx
jXJs4YkYUwoxhhoG2uXiEiAt2F6d/DmJvsrdxPkV2QHH1JLpwRPstjN/Wgd/FA3UrH/D6c0hpBNj
ZKOmidOU+BRwoFr3OCo6BDqo5FQWyxldIJS2ULtaYnWb+TBDJspPCvindIZf+nQs6Do7lh89De1N
YsS7lkU2xO6ECCUlY5dTxdRy/qP330sXNCU1BheP2mjtD7r63Mkt8X5WMUj+7XHcu8d6B/qc38Oq
oBLyB1VwFn3MH1UlxnBzdyb0a1HptesshPPX8x41gWn1c2IUfCY26ZxifzlaJSuQwBlgnbEGXab7
bu8b6VegTKExtiJgvfWzIoMAoCD56Hi6ZaVre/l4O296feXDZRijTvzrMVtH/iB2Bvg8DsY/2r4p
+9QFzQIMZ+0TKxwA19g2JpWlrwkMG4LONH0zCvQI/gZZ4ytuDYS+ipwZg9AkgQztpto9lpkm81ZZ
2y2gPrWGD4ufDN9OGgrjA/7IRcUm+KJrqVnO2+TNLThhySRxB2MUP/M7oc4E060MRmoA4YwzE/6m
62kMWReCjXDDad8vWhF/sSDMJ5Z6wHtQntR3cQaXJ/CGuxrLLg9BFDdoHItorKmAn+nfJqOZEo+8
hxJZklNzC19oKU8Zew/bi0Pz/CZuTvzkLq/NelJtrbgwpsU5mwjxi14MxRA6km3X4a4Iz/6GnCau
qsNWlFR1m0/XItaCMmQQkgAXE5NCPxyK19WSzQXxb/k3bRfFuE259NwGpBfyATwVwxvjTSf8DOdz
STP1OsgsKCdo1IpEq1nrTvO1x8tVuII2mLpwG05J3RC4xkvm1Hp5HVEITzAzcoE0GZwNPllw+y/L
m9I9vm+6StQY2QrrdELy8lySiTS38t+R4UxK/U8haUjszuP5OYRupKehlgmw0T9fAa6U5te2RKIG
9xhR8KFuxVaIJlGoYczSm77JFt4zQit6zc9EBsAS+QrAd+LGbXv+JtqoblbzM0fxoABj+miLEhLU
+RSNWy9t449GVuNlYU2QMfkoNenAek6Yy/3l6aEanZzkMwBwr9qbN0O0QOcheydccx/Eu9aMdblW
m27Hu3H45uZRNJ3aEPxRjQsXUnWr+rjVadntclKSaylJmU+xyvGNcA9FsVJ4fppbJTauRBKMAx/D
gwG6Tu8AzUmHSaGdvr2PhgE747gOWgvlCSD0ft3IwYMaFUBNFNsvRljkiwlhsVpZE3jcWRR3zUYO
mrAMx3KtWDfJH2QV+lTwlOo7lO3Yv06rS/2xDDKsIIBr0EEFWrcoPm8gQMkZhPniQsYOHn2HiTZ3
woOwE4UvVVS9DU6y+0m0NC/8a3HW9GZUXBNtlBvyRDR2ewIHYx28vy+D6ASrPx4uTjCOQk5zcgSy
dfhBiokYqJZ3HIWV5tlJzeU5WYQ6HEoNReALc4zKGB7MVj4PUJRQe1RGKLdRh83St4QgfR8qOO2T
dg+LAKoS+z+OxmsPXQxgdfyewuZXFe19vk+BU3giGFkWyQheOkSuGZbuT7hFZZi2XTPctrgPe9KL
zLcUTZnG0NsXhrsdxp8pYgZgpT9/lS6KwVGA4xWtvT2Ci5QaEwrkcrhJ9B44CQlCN5+ngz5QIqVm
SwvefU5PTSOnjH2Xkq8vawF5kjPFUOBYgXp/19cBPQivLm6rDk06BuIEqjKN+v4StRMffOPZVyv/
EmJMVYgecUtYwkopfjdaHwiUuU+OTqMHXutq7Ez16VeKf1TvyV36fDPBqfa/+P4lI2PVfcBK6RMA
LNAp3O9KeJitIL13ZQfr5IZgntc7+XMWBmSHzx2a00AKQ/3xiBXVUKJgjo/SMVhrWMJa7I/h08iQ
R68XIi/D+agfyXX4SFHasTTNV7dGjoEpIV9xFngTTIjxxd1VFAq8fkaLIriaahpqnI2fTdnyPa1o
w8LjxJzv7hIUvd+3SSJYaIYr/ucRRtnI6KHOpzrs0YCnBnF8o7RtjGazFmAYhb49LR/AGws3jlCh
PGC6DH6Codbm+Z0jt9zlY4o3r3s3u1/EDciM0XDZ9EhoJpdT5M8nTF+fIalc/XHOoeoR7qR6Kp/M
GPrZQKOTffSQXQWQhavHzKuqkh9UsOBgEGobo3QKtT5AIKI46tOg4hwiFHXJJFcLAnARK4TeeAhH
zMZjP2cFA29NQXQ/uW6+0tOioVAleVqVhfQ/8fJELOZe/qbEC3T2nd2HpYRGc7h8b5x0fyUs0ttx
gZgFMga8cFuFLUEIOYF1lo12qTfH2K8KJP2uSQxjl2Otd5tpC4QyhMMwWG4zDnQ6hA90iSACq9e+
qqfkwuEvdTl4RVBSAMbiiPyPqD2yFeRQezrRy2G8T1CCeyTY+M0b3zhYeYbI1FxEQgNZNXJ8HYeY
2vgrOT7+2wurbYD09LvJLUMlDDPaC1sR0N8r7sr6cR4rptcV20ufxXgEbXQPWdt7ILZ1olRKROxo
R7AhiHw6uzZGa0sEHAy9DGZiaG4mbsozIbdL0Tbv9F2+8COUN+gCagU31twyu+iHMuDrKYcqtRvp
N1a22GPsYm+nq53uizZFToGqtl4zbbK6PQUVLh4MSD7BOokPNr+OR/MVmaJOAvqrISyTF5f1bwL3
t9aVwBiS249PhrrP+Wzj/yBjWwz8imc47kZB3xEH7kvXPnclG7lSoM1YuvUMUu7kwQ+/fKZNv9bg
LDPrfnEoemeHBXsLZzZA9zZGCtZDbvy64baG72yIzyzXDZ5K6OqBBp+LortH6iGzThWK4q81UdW3
IPG5Q6ZCS+nZGez9GVH6pByiZ0E2924N8u+Pi764/H44S3tQ5L1HGm18DVzFjRxbAVOWMJ9ngdMD
aGRlJLUdB59tqzhnWTP0So2u53y3bIkOz/d3jlG954Lo4KEYi4AMrAy+3vq3GQzbDgr3OqN+fnW1
ysElpb10nCMt0IxTYcAzvPQhkBMP2/Q/J2pl+tCa+IrF4yWVYwfX7MrGfPlGwpFVMsXYXRBbPoMr
8TvrThig+RPfBvbduZZWRc3+sRajxSBBr8hOPLNSKxsUvJVgwmYJeciV0QZiFRUOYimWAnZ7wKgd
cKdY13ZK8ZsLflSohTToKxkEWR/xBQoDeaBoXGwz/rERWuUVapkF48xp7UXgwKfmxBhxCQZGc2Un
vQq/pt1e5M6yu9oYfDc6n+4Xj6i5WQBs3VtppPKXgAYXpWEvwi6zBTqHLnWtlv1KZdDJ73Sngvid
uUELnvmYj/PsCanp53RodfLCRPSuqUCJMyAYmGXPp/3OCWkTbB3BABmE+xA5jvLU/EMWXxLS8p0L
fpW25RnGXefSdkBMmlL6eOVc6fs0YXZ5PbrTFpXpb2TrS5z7+TKZ+s+22/dSwZ+6r+97yslrydPj
NyYfAxMQ9zpYD8NU15WZ7o4e1g9fQzEtxEY75SmjQpc50aOEwC0LuFBN29SayUmIq8n9hYh1ovDn
xHvzyoG+7DBfdY4VGaMxroWIY9dQ4sUf+5tgXneM3kp38MQVzUqTpLGaWVCMyf8y4zsslPsN2f3N
awNZjO9Qz0/x/VY8RPRBktJayTbQmMg922AdD0iwHoEoQ0cxXP/7K66weBbmXqVEaItmHNkqRgco
GQbGvAT/tZnllhv9DMHN6sedwJp0XETTfj9DHfMl/Fzc/MWH+rBJp3N5OvbJuNlBEdOPYtJJnRqU
0PgmeSKz+9fUn65yrp3TX+CItYlAncfB3Fyijme1h5BtbGUstQFGpwvbeubbysKdSf7Iper/r5qc
T4hs5dtqKrbH8xcG+cJ1AjNCbLLK4wXoLxi4r9J8OUDtqHaSl3RVdmlcSW0atkV6WQiyWI23PCeX
OIZ0ktEVBh8C4JGfdK4S6Dlm1fS0Y72EXuLEEu0J3rGqlfxRIvFk3TWg+uTk1ywc+KU1e2teSf7n
YpL6ipU7DpCNjMumJhN1hHAQotfIvPLN4QoVKJHF+v67hyrZWNCwKpDi1SDVNjN7mFqtdFj0E/xM
Sk9yuVVTpV0eS3HnHd0IQeuXcEogaTaDHhT3StQ4djP60AcBPhag3p0c3QdFebyJHkai8VytVA8K
pNcCcidefIpMBUGTpbz2Kifv6oUuNFLMzgqZFb8giWwbslU0k1rIrSZ96Nq9bHc8trSWeqAvzQd1
srDhEItsuaXQdqiF+6Jz2S9u+p5UNIXf72jDDaTeaViTpdKVQngZv1aNsQSYVohmOPkcwNulyON8
Y7GZHN7P3JJg/LDEUcGgtdShIqSV+PIGx3eXXKld8w/FBC5U5YpBf2h5zHZ6Hs6fomtTDai5hJfu
MZHDaiIiyMeP1XRv2eSNfNjZNxQOgEfUuYLhpMAw5hj9pfnlGQRar7mTkN9Myxlw8d2N/U5qbPLp
6ghKbZuqRjICpA/zuNBrUH79eIue7v0e8v46NAXeNu7nUNpYA1LfYJbQwB+B3XgZuHrqACbtDSXH
XL041vc8+j1hUpUna0MP4/UpFXJWYbyUEA1jLI9iTjoz+2NZHClrhVIHLlf1SdLJ8SRzW/5mhaKA
iVoIyDHsQDA65bFrm8jLOzxAtJG85KMSGRhTfV3Xz/KaGtiQHaBh14grDblYmnCHBjZVH/dyik3Q
NnzdDjjs1zfcy/zHoqwcSDfYhrgG5KOdpKdo1RyUOYAwsiGrPxRp1v3V9KcQJp8l7p5oP3JiNCe9
CoxtyeMLg7S2zJ1ujM+tJk0x/wgdRxQ+KvIojfHHJAA2QdX91o2ik8BaQE4A9RKlhHK9U6lqzTQ3
S0SCUws3r5ye6KHFpcQ6TaBnKncJc8hRiBVvS9Ar4ClDM7Dy4w+Jl5VUAhGduyIgqLna8Wr1Wc0v
ry8cdt0Z4A0DSx70k/M83d0hDBXDVJ6aFkhvjMUClGO65dHBhMtr/pdMnZRVCeEwQlikstvzxwnj
ivCOJ0UTFYT/RwWwhEjyiL45Ch8oHDelIdDRq1/Vo7pi0qmw9DZan2a1QQtEgr238jX8VCBe5QON
y8Jez6NsOdqYBeiAYQGJUG6B48epqCbSQSRkV2+/ayULMSJBL2ycrJlzhQ42Uq0WCC4Rjqg6NXp8
SZu93YdAqv3hXep5JGU+vNcflGe9LKpCsRWdfex1iOhj92/2gdeRw+bewVuc4tLE3bu8xAtpZABz
S0JwgZkoyxvQWgXt2j6B15QLSDZfW2ICbrVvfgGakQH1XDEnTadINF08BPoHpeJfjMxy52T0jxGi
7sUpGAKzTUm0OhhEGxELzAocXyVflPshtPzx9T4lJ+PnYpal/E1os1+M9qnjhw9f4tFAQXU3YVpa
fL39HM+BxuhMND4ZtdWhbLCvbeHmxrpf5atLvsNtNsEXVtb0OGr14BTZ7Vf7zAe2JRpQE/G0kXVX
XzcldZKqws4ngZaaolYvYLeXTdTX4lB7hheBLYlB+synlXky83Sb7Dq2EzBqbb8nMQRIV7eqKpYy
ksu6UfyZnV//xP/2KijLJ2e1OelrWvmQaoBpeOPxlFr/YEk1l6gN2b1FwLZBAwFVNIPO7xYrnA9n
iMXqcf9PplQ9uhhKpsWtw6M1Zecf+bI/YKxdYt4pxA44ZAVvZxJiBvjx7lrR1E6hjy75+b81Aoeb
VPS/SBr8byG3agncJPDqyvrjFlk+0aPnr95ltCJfOjsroMQXkSmoodIDZX6714eGqjLBScRrv8Qn
1/Qlljbd6G3DRScOPAAv8W9/u1ZmycEIXGH21epDVeBf9kBHoswLRP9SBvIvXpYc82VYyxYzaQbF
TDHG5vAwDFEfLw8KRBfKhdFLdAiD2IZOpMxRg+62yxfqcTOPdIRZ8IlySbi54qv1M13GUfbCT1/W
08ctH+GO62t45DgFPS2Q0MQBstfxMDJ3Oeb6XiT57wcVb6qxRYTRk00nO/4WvVQ8Q3gH77lwllb1
IAIhV4Dfxk763mcVrYQohWM8UZXED6FLUdLmSWe9cD00nRyRUacblDUsaATc0xwJm97O0o9TOzBN
f/1hxrllkGmQw0X1s3wzoB70GFfxCwG9+mhPXL7oem7DTAsSGD8iG3MTAq+mQYoHlPmOEZrud48v
OtXxLOq3ZeZcXDVbmqmCK6A+ghthvToV4tPp97iNmMGV4RK7bs7i+RqaAhb5hdHWIWQPlLbyMOFJ
5gThzRwRjUlAuELh6ygh4Lcd8Mrmt5+fNg8unM0GTnOX94GWa7AH1MTirb+cbCaxFErk7rDNJp4F
OiJLXj5NEml82Mgm3qSuPNxO+uX7GVsY6/mRsWp31yUjaih2IVmEoMZj9o9Oa1O/klm/CsQggE9q
KaBQVNkdrWfgTzmqrpxibPeSpvoLwe878tvCTyJJE8sJ7iQLphQ8oE8M+bZyPnu17xYgEDOwTmsj
KPvbyoOECH0er2B/aGMgtG5vCrrxx4tg71x9MMipuIEpYlHdJSKTkucRTX9AjpMrFr150GBX46U8
x5EmASuDZWbbv7tZMOyNv4zQDwnkPTI/WwnpngoHwuxRFLcijA7of9kCLeQZWyWz1kTN4ha31mAi
/rbJqAh5jy5o+gzuPZjqSln4GRRAv9+7adG8XgQzg/1JYbS5Ol8e2iGgThi/X371xpScdYZF6/Tl
hXYDfJFxSWZoC7WDwfHYfZKU2E4v28XqU43Ji2OBG/sPloKWZ1f2xuf8N9n4Cvg9VJ6OCXqFmAZ1
jZrg8mAxeaGhtmDCmLjsZuEr+r/1gf+34m+eEYP/zt7U5SUvgVfQa3+BoxUw/R8Cn/ZsEDCtC/fj
vPLNxkNzIK0i3CUT2YxNUO0L4zEpDwxrLdTlIO0AKIcGzn1NwrodfXz46c2UUT1MLG2M/t59R6l8
CHnlxCYwhvRuQNEg8dJUcGax677HWXqCvaoomMoT2WEwiAokbu60YCgCnK3XxwSntkM1Npa9yals
HEm+sd1f5q0Tlq5fW8xc5A86R2mQ439ET9kKS8EiblozAggpGk3BpaF3QjltXCznrKpT33EyzzX9
91TjS96apq9VqsBXA+BlYe5RzKpxw0Fzr9VZ2LlwpSOtmCthSmGT6qGcxdfmGJZA06daBDXnEl3t
vTQ6bOptvKNqPpZsz+Wrm95yYmqnQyUZvtF54ULiBTg2EkQM5kN7Qc910kqc9opMTNn0lzt4Prk/
O7Dq04DKmwoKceKVr622aNWBopctUdOzSpIbEOfxgD1xp9jU8sMNwI7u2J5Gquyk/kuOU4fWAQC4
oaP2zP+RUJbqO4fvNBOJB3Kf9SmuGxFsZDLCPLzF765fdNDTv6JPZyWl+DhJ6WpT4H5Ed9XtZPVr
YCpy0nyPaRAKyLcKWQemdrVkcBsRChAZATu4mJP3c1Rs5YPyeBML1FUnToCoG/d1wPMmC9MyY3/v
N3BTYLyHNG8k6zH8d9PgObTpfOdL+oEYb39twWevXBA2xdWWb3iW00cesqb+3cacAorNmPORfFE0
Fs9e42ur46lNqZPZ1a3ZJ/Ub6ORAkQc2912Q39Kxy3U/MdN8KelSsDELq2N1KfC85SVjoPsiqh32
7qTTFVMS9sGNyA0uUjpK0BXMVyPE1OP2iM+bxi17sXuh7QsCdH5NxOdki+sGC2lfNLn3pXca08rp
q/NXxsfY0egzpNxAXhbhKZJCCDbZYj82/pKAwtDjaISChZAoMevFWj1W0YnklBu763LfD8ZaDFV6
1USJcw2eKjPbsBz6uQVmEgnaF9hf9k7DO2MAipfsRpZSl/Q8LKoZx+pItfoIMrUe2Uh9i54ZIE+7
fvLFpHblfhYAkL4rDnZqq3pFFRBZpEaBhN6qegqcYtVWNE6EfhqYwrxyvtHM99+jWcJFfcCTWqcU
XChGxxsqiCUls6JNlttc9ztv4S74IhCVbvQXeXCzpDu/hTKcn2XK5LpGV1vBdqf//KUPJ6wGeia7
soh/kra+uZesgshem2cTPeQLCzQZ1V4Z6OHx5UyHYdcs7FVgRey8IlEzXYBNQo4AYTy0nQV6B+IA
UAPCDMsT2NAjNijV2MvgvmFc2KA4mO3fvn3dK45kjNcd4eCVSRZN9IPveXI3NHwbvpz5FL13nd6y
xo9ew5KKmOdTS3zIrvMXheaqVIdyu/p3CSwraz3vNtNYC743mtDB5+BnNdpp5KXWIWQswf/ylhJ+
/MeADjAAAwKjX6FFZb+OtMq6UIxpv6uFlurghoSDu1S/mCBQSneFbbINn3R8ck35TTzH2U9ACmu0
SkLk5LLQ991gShtTz9SCFDInmUBCTGVgnKcQQTDHfwnrlP44Bm1rHIUYHBd/HYD/Cz6DwKTn71d2
8RincVj5HoCYJmNWrdYAy8m7MzgiZjRoTIDd1si1gSFL3FCoOvUKtLwGCnHitW2UA54cDSzaKlPl
bMY9LrWXySrEAgALLnNP4OlvSwueC42hjMkTZ04QpegJXIyy0nY6Ve+ALV3WldOVwx0m508wUetF
VH6gdmgFSbNJF5W2omEg8ZOBzoU/sqpLq2ZTmKpLP4chBUKVjkxb3VIG90Xz2DVLQqtpW2Nhu5jy
dfYxzJ5u0UgxXr0bdZaY41Dvdi/5OA0hKmHbLkw+nT6C6E4h9z7aDrFNQDaTaJ9auhFN0U8bUkjb
Fuaol88t3AYDiUDGAE3dY9zZNsusvMoOZxv79w/3s9HGSYCYP4PA9optvx/HWiqdrVBHTbxM/GPW
RE6C5luUceqHc8vMTwxL35qcSVyows/svyVZQD1Qnu8Ut8MJkVOc/KbYzru99C/PP4zaaRmXdD48
fBK8V5UX7vZV/hf/2pRS7L1gP/Lvkxxdqnpwawyolh3Ezy/kIHWj0l0foeNvPwlrvYMh9NFacKYe
zVqgIH7giTOwCqmyUzmV2RnVPvj2s9dfQt/Lym4MxDO92uPmuE9iCWNAlrA3KxJGw25HBXitNP/i
O03Mq2GiyZlPJcjURWDJYfogrPSJiq0WYsUHYgD1AZ1KntRXO0F6AkX79qr17svDIwJHdkogaeFV
wvFWZucG9IPsDOVNbEa8ZiuzJI0vlrqfgTW3iuLMdPW4OzTxo1vLorX/y3iAZbS2l/NjkF5lH4MI
xnFKBEVPTAtlOwGmifU8DTEf2iaXu47vWcyeDHtG7eT2Q2Vpq4uf6KMe8WhiTN+YnskSmu/ACDd/
6trueBFNRSsHakyTIzE84i9jgFC8AExPN7LsX2cbM0eRZgRJfcWFZILLtdZ9XQD8L3c4g4hAQ0P3
rJd11Oqhxd+GwhYAEm8enofQbh5Q6XqQA/ei6T8HsC4o6LD9meCKgHR8rkbRQeJUmemyw/tlXyMc
8jJp+JLQ4YrNfk5QXiWM4nKvKkIJQVgo7ZYuofY1IzkbMupPgzsxvH2uZQcrAYEbETMxvWfJYp0H
afBZy2/A8yQppvR0QeYsv4OjclewbkEtf0hmW6tHwiEzWYhf+X+OMZ+tqSWCQ0ZQNVMOU9Du/GJZ
8uhtst+9vLYl4m0iCCyuab5czoHW/x9aPZ2g+g8jsi8WXMu7kJUjOFZXGSr9KLnOSPSAOf5//GD+
NNRyfdGRm+0DrkVrTi4B5desArMzRaQw6OyCNPXXSo/v0jG17jCbWslAxoWpgiuYFwpB8xnxIHeB
UldRbWRfmpqMEfAvcMHbxnYwTagOhypgc3P7ki/kSRg1yx0LEjeuVBG748T7ZgvLV3fF40w80awy
TgpFU/JyyFPwje78jxxUvlIPXV80Fc4TQNugSzS35ZtarHSPtd4JuQyLMyNWZZao0KlJ0bbzceh2
2vHlQlg/b5N/j26baHBUhanS+QxwKQwU0OaWFRobRVFuJzenqXmp5MVpbAMJRLkGKFR4RGwhYdNB
sD/cXw+kpI9UJSng9M2Va5sztiog6+HGE6C+lgxgWZmBroEE4LUh+pGvgYvecuzTx6UVP5K0vL72
9IkM7KvlorNFIh24VtALVQIfFc8axZ4NLBedT/Q4M0Q82pL9sx9E82Jof5tHUbKrCXJ1jtVNy36l
hML2eHzgv/jI2S7GpursDDLlpQtoejmM7UtfWR0KWR1P8PymDhHBpvRz405XV0QbDm2MgMMikSI5
B4DMjLpswV8r/IXi+tne+SK8V+Uy7E5AkpJ203GnZs4pjHAjvwTn5sBL0mrw9tnU+3mpjDsYgHrO
VJZBE6BvKptU06Opj5icVDirOb7Q54B6ojz5NoCiqlFfqZIbgQYVGg8bgfMhSxCyj32vtfroqUr5
x3oDnTTIyk0fmnExCQpLIerV5Ii2CAyImHuozRhlx1CJlCM2zFOLGvnQ50+FUYdDEAghVgdJFWRk
Ko4M/hhgQxG9ouRQlqSGloO/2zpD3AdmOhM3SiaPtSPa+Au4oLzNx+w6zPz3JL1WOibR5qoBH7eb
9YLTwX7sDDW6U3eeBu9SUrxmU+lEHnkTeJZwLjJ95jGf3L3L0dF6sFXbg6qJv3q3Mci3nu+NUnmP
r7kqqAWXAUZ3jP3PSHtDew5+i6viWuKt86tgS/telUFOhCs70iwRpV6OCj9INnUIMNuqyEUWqrFO
3JNblvnBIFZLfY2ofa4JOzBMTKRWhJWOJbOc/I0QpkBNDSZ6C6Vq76G23Xfv14zEy5R5NRwxTykb
TASG/36olm/Sk2idLXK121tRTW6Or3n2kkDVYYUXNhYE/vknnbhA489QB2McYlBuyJ3AgEAsoDPO
DiCG4kKGSEc2gLGjoW8q2naoEOGcQSt0prntU3J5b9uRbd5Hf33o0gx1isXRc+K48GuwltPkhNcM
HQp1rTLbctyf0NrV0YQGencywKsuZ7l+SvAnoOmt/OyIa2xjYGXUHmZTaC+fIZcr17tPhfr00uBO
657xw86Nsyuz9z85u+MOeoMHZcBZq+qnZcgNhplhzaI2Vbcr1wsb0BiMSLXZYsXieSdgngtliCcq
jDEyAmb5mj1NbMSfnapS1FW59Y/U0IPqwWaKFUc3S4N0g1+oiiV/tx9Ey7KmgX8WTH+Vro/lmtro
dyYWVi85cIJ/BXpfIqdBXtDl0tXhK+3T5dYPOQUGnjcDZmccOlKdk5EbA1k2A1xUNl5idpJKVx+4
Ev5jI8g64WAlKGqEtI7VDvoyO6bEU9ze+NWJDiQmi3ETqOeM3gfqvLOrnwVg37mc2MWND1ePJezv
LSDG4JKB60ivUYmwyGz1RBu6Xa8KwROLc4lfqL434FaoHl5D0lBQOVEoIYTCTsl+h/mkvD3czCWN
7P2eOdjr973aYZ0RG2eR+aEnkGHfFHr/eWCifU1Y3POedlm9L7Hdb5uaovF4Mu/CXpwD5p0zJV2M
ng3BxS/6egL0FMsQyX4xPKIopBE2uzpb6tCtfZEPB9loHMbqn9nDY+oR0dRLSIerfALgRSngfdo+
c0BSGRdIZIcL8Lrugf0ssCnVbooKuqrJ3Lv9Mv3dQ6KMoR3Sp21iUj/rfnGF5/v8QF2uUXECiAyu
nQEbm02Pvwyj+AOt58jOScBcvFygGJD/13p2IGu82Q6mizO7osNF/m8aPZlVSL69OLFhaFchSkwn
s8Di6TfpmLAJ6/h7p/Iqr12NyMiTU7nhEUV2a4veUNE69BoKabEmOKs/O0LlIl01TmASrlVtyfKF
YS56xyNz2aKnXKcM8L3fwlT2BE/nlvmDdrbje3SEE2coKvrHvS346tNx/tBLu6GP2iUOUIePncdQ
B/xrV5f8zY/Yip6pyvyEN4UsqjC+bZ5SIMxoQXZ9Gs25/lzzDUiGZhAgre9od+I9jHhGjfsiDPnK
pMUeh+tHYerjvrKYaNKozrx8EdQ3rlipb8PaoaVSjzS+xroDI6+ti43LwXfocMvRTwddFIbR0ydt
IIrBuX7SJ/qfti3P1D785ShedE8MFbJcm10j7be8OkSI3m6wRyNsS2IqnxltAYe9aMqhKjeeEcWO
On+dfYVQZAVSyzQvOA1zlHlsESoS8VgcFY0Kdqa2Dhw4gb1HDrvOVl7adFTCMTjGDcTb+yKEPq96
mmDduDAOvgUHnZHV2UcIMbZnd5Y+zGiWezGn9R1fXaDdRTefD3/tiNoxixnI4F/JcP1YRDSt5HYD
YS5K2MYQJwDimh3Tje/2gYhCsAcrxhd3/gyf3KKoWTSgRA/VSqfFFFaEsMjGqFimZlEnG7Rm8SJw
gD30QORJqkeEAbS2YSBcuKlV5g2p4kisRSKEjPnd2Eo7DTfZNu9diDKRTX+F7dOOD3nDowSKURTx
KFepOa9ELxwY7/SGWP28l9R+Ll56iHn/cCJMItMqAzqx/1oh5Z9KXd1KTv3GjaCxq1g2q684ftaP
ikttoNCV3iIloehZsX0BgbJ9kcOeu4jJIzry4jiYJTH9epNu/Vf1siQmZx+693IcOhu16oHZ6Eug
bYbhjFtmRy5vuFRZPjdA37AKSa0MoX+HUiVXu+wlYmoXL5IqOYr4j3y6qNdV4cN8kMIvV75oqnY1
DTNZzBV0M8Wz8k9p5FeR2K8ZNRXr2gSGBcmty+ODrf/oLswBYwA5UEK56ysWcz6s8cW1tDMjVTJw
tKkoyMsb7xfLiQJh4WDwt7DQuqGSqIOSrXAyFybYEjVaRJ0M5s+httWqsrcNLx5OGWEf71tQI0j4
6yeaXSmlKNeSx/IdVYF46h/V/mKYc6U9KsFC5KQQgo/Yi9sihKw0bniFMbTJ/Q3C3+e/z2RhXMUB
cLX701GkaqW0fL+J5LPQ1RPFUJiGxsLCcHHr7rgx1bmCxMQdLSytyfS/TTqaN4ArCoSAl4oDXgQT
YJ270wif7j0ueOnBAZrsvHFW/VIfaOQRls2SJ4saDvtNn6MU19oIxRh8wgRmDRbIKnjsr51XMIO5
w1lEpnGpOR4/cVKXgIiemkjHJU8BkR4eOUJQ4SOz+yxVcX8Uh+uSqeg9q8zCIzwbsQIg8dBsrb0l
67X9k9y0I6T0ImDSyXnzn4GvzzTM+s6DMoXStOgguZpHTdVEXQYL76npE4VfzPiZkCFECltShYpC
YJY5Kk6xtQn4RME/7Fyw5dTQ7smiKVmYilHyjx7GQoqDwK/l7W8diVF+vnI4M0vEyB42GQFgGOkP
s6RrrbgGk31YfVCg9UiTkhVu4C6H6TbO+Qjk9KuAuTG154BjdYVumuR3rBSO0e//PNELJGgKBJd2
xuX39aGm4RMT2gS64TqQ7xDblFko9ryRBcpa73DnBQ1YZf+uqR6QFPbzni3L6tS6ppJb6NJQlRcm
JDHNamXQeIcsI5QqvU+XTeBC4trZLcHDRqsjpywtk+FC5I4C3OpAvVYKGEHPwxUi8JRMqS/Nepke
ETpT8z7JidscavPyS9b+8occK+Nc+qjzV9v/GfgzHZoEJAwBKN86IaCedoWEhjuU34ZoafwjacDf
/8A7Nys7BOjwrk8j8rG8xt0aKBItxxFxMgI32GAmucjOlcwaJ8fftRi5wiT4lsqNPigDDhz+9Mcs
BeeLMyiDgcIvblaEdD7rixG2C3mc180G7JYTO6dQUBnMSCv9IzkIGPP45BL1SXCoyvL8mLCC4AWT
FvXMtm0IrwQLcQsGQHPOVvTpUzhXuhPU0xvg/6WfWPaIQlNkkn6lux4WvlERVn84J1ocMz3NbUd7
80yOWxnahVZR8em6pyazOoqSpzHcRFjO3Ue4y0itMhswVKEtTP/1j3su5o+5rIBHCOk9xz48WigU
hFNT7Wijzql1kjvuXyiBDOuWqfOw97+Od+CFOUJUP7LH/aHrhKuHOEwMrVvWpntcrdHzZkmTSkfG
y2f4PwAOKs3aWJApKqIxQRl1yfE3wxewg6WseQtiJDnsOFSRLlzFVR9/Do9AT6QGuG/Tdp+Mwoaa
3CpALqo5MQLHbU1jKVWAN1cm0wFro10/DXZvWJnEgQ4yIdzYcxpZcflEYHinW6dNGOwln3XHKnwO
zbZPQ01YLlEG5Rd1RrnE2Ut2qDXpgC+sS6uD26u533NH5GYGfXO4Vd8jTVKgvBk1R5lpP87XcJ3U
iJkCgDF7i8ajPg3PiHupvI91gLnn9jcyHMDGq8JwxMwYqqKQOYsxEwj3XBEpnMsI89hcU4q6mnxk
DHZdFaEQNhM2iU0cfK9xKtIq9Unv7KgUnRln9QzT/YvyIpiibe8tsisTGDAYEm0vSoXL4scw+HPR
iN6a61zwQUcHRFD5XIptp7O56dfiBWXGlEtPHVyTSVJhYG46wQjgzPd8aBJClGIDrHn6muy06mUS
WovDPQeJI+uaOd4KTm9kC0RGKnnjXD1chK1ZtqwVy8yXpKtAR9oY3xDOibqUTi2OGQZRWjxnzu2H
DYzt5SocXYHGoY2O1ZKTW/h4FaQ4Z4rkjpiVI1R1aLXJ4h3ev7sYU1WYSBJTExm8+txtqJVZQv31
F5Q7kkQOZCmStCOi52mqT1uYnHPSO0oTojxGZo/psW8QRjG8DX9Gv5YldNqESqnMwyjoPBl3SDBs
3JFtWvb+szkbaEac6AfpaOsMzvWHMaB8HptpQkbH+dFXIrm6Tg/z1N8+ftAypSNbvsEE2A9rhuEU
Uj5nLcUk+JeMTPtrE7UUxnX4xWF3OfepU7a+lKtR0Ju6KIzwV+yneN5gjM29n3w9qWognWBAPuDQ
DbcuxmRJgOzb3IQvp1bMIBlTCRJlD+Pn1tL9XWflFpvzbSARt2LPuzbLzb5Y4w0pq5nsokCbRnUZ
wM6idnO1r397S/S95fj2GyaEklUe+w/7Orv/kNi9RkFW1ZDOybmrXqGdbN8b5lzMI49MNYy15L7k
eW+l3a7Xyh7Wm/l9O0gdaz/j8aKUUmoce/M5NddLAbRQjxFEEGvp59TIlILJmsVSHJptbIr6uOej
aEp2Mc5uDtz06vFaZOBAVg3vZsEJjinnQbkeHv9MgiP+SrO7AlNBQPa7+XNzxKvFv8V+WoJy+Le6
fFem13mJCbGyvenU1BguQcqMNWcUOT3Zb9NT8N/KxTXqzAneKpcZtAT8g5FNduRcafWOP9UCOubB
EA8VUNpA+adJiKRvTj343y5ZDzVxVV1SjHGBFjTwNWOSdeESOsEq9snzI9/lPptKZKc7JOWuXXW6
EMT/l7Ph0aJUKq/VIMYdL9BV/iudRXV4elXwqQclW6gyjNTE3MWwsh/F35UeMEz0rrpRhGKL7QxB
GhDWs8eSW1tGyOiJMPNXWxABuk7cSjiL8EacXj7k/pkL8MlCgjgW3qMzXI9HA7f74C7RH3KEUnc/
8U/psTanyDZI1AZMU8mHQEoVuVRXqQtI5eR8IhQkWZdakwoWP5iI9NVcg/IYtc2Y4oWNRvNJ4MI1
CDasltT1+CcR0qIT2Vdj0WB90CMmaiXlkKek54VagaQkOhAVW1Y1k811BaBFXTwGo5izB2s5QO2y
VP1BQ5jJnm9hbsmQ2ywx/I/T/H58GoUYitoYhGIXxVtaNrd+t4ixEpelFIIB7WNHYfoGgyatOlC+
VgSoqmk07h47WWLCxD08uuQLnTPXek3idISJk3YsDwEl5Zs+QSXDVjseSH8w8uj20Mwzm+VpEZKA
+PCBBjaZOC19EVT7zVN0XwK49zr0hUkSbIoGJ+FEyVUZZabFrdKoH9jw0pnfS6CVsomuc04ZOyjw
2oQXsKXWOD6dyCpd4lkKVq9bZMkDAacRsvc10mVC0zilyWyrPoQypYnk6XWY4rvd+hxYlNNsuisf
lL00ZdzVSaoMci14o65KKeE+x3OtTETLdG3pJ1MbFsoiImjlp5Valra9hYzYm1hGlMoCVcm4nIFY
tY7PDltC3QA8R+LtiP/9sCZidTZ/g0HLWR/xBuX//36IHIg1S9mTIYEaZYhP3YFYFxAgmlSe6DJn
LlQ2icZLV84h6r9tjz2nQNpAr/8jfyXZakRCSb9BmuUrrI/RfAJmBB5UT2wVAFBwnT3RaB0kWlTU
yF/g3b8g4TJaiSdrrfRRrGy88sQrCSpApMQUcUkJFBnmaqXVjd8zAzvgH/7Wu+pgEC+ojcyhJHhI
4CXZNDbXDhUC/CKz1XgdS3DcEzD6Kchy8eXawb5jmR11K7dXQLD/jxrMxqrP0tiPtKQXSndFrcMq
gJcdROIXgjY3dA8Zwc5fHSgEbWQ9+uTmiPpLqKvRpRNOq6MIfIXmAjOG+0eOh+ddckVuXNdtj4aR
NXHmyM8ic6bVnLbb78RgM9KOoILQdXC4Va2WICnY5hziT6TLoaA/WOaRCTnle6SQrHtcC0YtdNYx
Nusy8MhQYsQyZFdptUfmfN9HRFV1dwbyzH9LxkqAmmm8LK2esn6c4UxamfX6UfDWYsWDL1ikUH/S
3RvX2aje+oMcmkalp4U5xxxPVOewdz0V3MHkcfjJWJDge8ga8AI7y90Px3L0yVLeoy0CX3VeVvtI
eMbNo0O6LfrrWCrJYYH577GhCQBB/3/q4w7ZUWleTGMZLKZ5K8+1T7gi7wDFPApgV1+uMNtptywT
BSy0sE6XzJGCtG11eSNego5GaebIdFBiwTD2+PFAV562g3xpwEvKT8L+m4MbvmW0kAWqMlBAtI6X
3Vm3Y3lakENA5e9rlgTALGA1ZBB3EPXm/efwHTrQFJP5rf/FudGgYJ3icvcC+5oKx6p2g+wm8a/4
6jA4CSRf5vNFeIKOmsq72De+hLqXi7Bk8YKyOaoK2HdbIrZpxQXFdkIvsMn8kB+Np3IwW6T08Dsn
8wbLeeMEExXPjJX1H0fwDj0Ks9YU5ZB5iUKS2aUW21ezMXpfPqm5IZOGkdcZt9YSvHAJrds/qp9j
HNC+3b9UzbWk7YqGAwojq1ky76DKOKGB200ihgAZ9UdssKpvahJ/ufFGwPtu8IcV9oeyi1I/xgwo
hc+9A5PxsRJVNGgCHrvdodVFFSsd3qUqrm+QIpyskCtsiY0wX1pjJ7WENvYfuZI9vgkkRpnXPgfI
MA/xxjV/tCHVTCm/GhaA4AHzNMxoqSiVQFiYatkh6zpwt5/d7tPa3dHvKhsXDRGcPp5y0kGUxVa8
WNKJRIGMi7QaaW8ka6YIJ7/DDzssia8vIoEH66VFN1o2rCh7eAFkiyKKVIlUVBiCmHJYiP5gb4cJ
YoBE9P70LOEDnyTioYoVwm65gtDr0bs75P/npp5T59JUQ08nI6oS+I4Q+mzr8Xji/gEOX5poGWdJ
cqF6KHEZIsFvIrATrP0U0AkpsMzN6QiHomMvw+GWAFQQQp/fR1yhFKp8Z4vpbDlk7cxIeKqXE/iD
wr39cXCLQEiE3gx9jZsdM8ykyLVGfzfnkoj8UueZD0mAL45z21CI4RHVg46rBzC0aSdH34n8v0CZ
Le5LRlmeQJkC/cKI0r6BLJORw7UdTJSLONPrXaNPWjM4qqOwt+V7V5b0rSHSLZtgBKXZx/vN3iH9
sU7EyUb0mFU7vXo9j1UjfFhXLxuhfcuVUR0Fyw2uEE1o18JzgmkDFy7WH7GU/olsB806jVlvD1iT
WG8gZaKFNXrVPcGBPj4/PiBbfVmE+EH3sin8Bzn/Bi1RkCZzM2EDLsi8Hk+bkEvkvsxb3nTM6OCr
oe5LJhCpXwM6YTUq89/pB/rTiMVucHfdxbWGq2xyIsmL+SORU5sGD2wiK/W2ZabKnBjqBfDbzLCq
BeF6r4TNyW2Yd3wWduFgtpeM9MZg5hMcxt1Z5ouGPXLkg2JWmV6p9jOAL2BdBue6LyHI0NZ+3L0N
zD/EYX3Gcwcr5AvrC8FiC9sAedvlXDpF6QAtdSG8Q3wJ8cQTUtqQFHAdTdgfzogH4wJsqyUCP4z7
GWQhxShISx4Hq18iBAkjbJBtluzK2sW3wmCCog5L78Kj9AQ5pLpSpZeZr4uDYd8sY53zLEPvtCnA
DsyRRJ5RJ6m5wYPkWhu3esd6qCFA+P0syElXhGisHhgvUhoGTi8YapohBrWO47PNOTMr9xnyOHUJ
EtU/MsJh1U9jX0RIMfZArcW1KXtEtXHp7BlJlRkR7WzsHGU53umzPU7eJJbrUFN1C/KSYZpK2Mdv
9eSRU5YmAVJi9+ZRfYHRlCcGcpZlRXi25scK0KJu9ZN27WdgXmxtD11wCP/Bm88v8QADW6AgTd5Z
iRj+QzUugflrGrhG11j3JY9cKg7SDhS/bkeb94AmAuM2gCwpGsR5CUa/zHz5x+YXbkyWtOU9B9WS
7cHs9SCiLkcTHKj45ENft369Qe0X+3cKyriZFf3Pnpl+Yvl9aKMaxMnn9Ve9C1Ykb9BVs7lQp3K7
k2P3Z6MuNC6xAb7/X2rxn6q1PJINfnzaEnBA7tUoy0B+wk93axP52nDnJQ2lehpMZe0TfNP4SSHC
oihcMbEyn08CDTXpb280bocfAReoyRquvfes9hdpKTm9MyTFpRTPrpacV8w0vhErpmAqZ5L3kPDB
Fy7QsYyeG9r5BQoRK/fgKb0a7Ey+RzsraclCjo3NB++evdeZEayOe3xRpUYBUDFICLby4JJHACGd
FZ8TAu81uWP88HjTjqsmuBqZSO+n4mli+ZF2J+S+28YNmbjKhb83Pwtze79WK6P6Tt/R4lSLderk
HljyoTan/bld0TgS7rICLjhBv0ZsfVcJ45Xa/GrqNVyjIZa0Vu+XooW64TmW5ikz5qiB4Pf3V65r
GsV2YV2PITfVhlvxJRgw3+Gph+qDCXEkwXt+Thql1m11lsLvbbGo3DR9Ou+DdQDUgAVHffpgVOLd
LW7XUGDQKiIdrD176tGDkGfUVoLQ7vqqLstGv2W8132nUanG8qFNq8M3Iq94k2kF0HoxqwDhjqA7
vq744hLpeB4Ku2Rh6mFWoL63A+IxPP0RETueD/3g2/J59sW9/XZlifegRen1evicNDcjXcxQyqg3
IQgG62yt8R1Z0iisH+usRl5YkCYL7dTdRYmnkFnQLkW5b4ce+bm7xQdRC+Axua6H4VSAHpMvdx7D
+4tKKE9HJk2V+9Irh94oRA8GPEiMSSGqiIvzrTI3vdDLkOScCGlB1i1JmNIX2VoLkpFSmT4INGw6
MygFGVvfu9b8pXdFMQZi82YWQwQMBUq9Dma92Q1WiBeB2SE4YCsllQrm4pg8U6wtru8aTdxA4Mdo
2kwI0yaHmkCQZl8/lWpDMFSHnMj4FBzUG+ZVU4YTiXjnBiiAoi9zt7BGaEjvWAo52I4vcwgDgKdO
Gi77CJe5Lf/Sk9Oca5DUQSVhhxQluExkqTIThGdrF3QCDHGmUGDOm4eaynht/ylP590bXdZSkC4/
9vvJN/VvgwbaNGOJywxCHvMGZeJ4Nmsyvrr60YCe6VSBPPWUSXNPE5vdIkI79UJPa2bCieTFty52
pcUi3jX3VR8uwQ812n0fs/MLfn3XCv5Sn8Rjy0Q30+OxUoQdrwwoHXGxdL0iNorjJRXixizQBTdf
4g+w3eYjmnMI3eRDuAte1BaXdALtvaPUHLe+CeMDIs/NAWblc4lr2JyYrQdCqQmeu/qs/qJeRlOD
I4Ox3GpZRaQvh3IOBNU6unM0viMNj4iqH4z5FelKagUlw4zUCYILf57aA9VUcn8yDPZA1LM+AzV5
hX/bZrl6POl8xvUx7rIqE5522YtoTi1ACGEdAIsvoqIaOrF9AC/LGiSsrwSlFVABvkSMtz3kNluu
sWH3XJKh0T+qGBDTm0B9C15XIJl/2s6u4v1SD/LqAAEKQdTXN6quYjYFXQ4LeOIQ+ckBzDbmG0nB
d3RoR8m5b2ywObWNZxWIpOlETdJwyjhhC6QskuzeLTvHS3ZNR8xfUvVpL2uJ6gFRIaQhsv+zzZsI
Yr2JdHZsqErXfPKwfqEU/cU2TIzr66qjGGqfkbEuQZgZYLOQ5DqM/+wG+01SDjlr7kZmwjoMPe5X
WkwZPhpFZuxP1r9vo8+Q431ZLfsXi7P0ZPLFXCErJnKoOZ1A96iQ52Xe9pusOjQo9py0nKs/agod
G46qtUlRb9LUIuG9M+ZirdfRql8hK9CocRipgIzQoKP/20MWjxjBsi9RAbPcbtDixRBlD/CMbFCB
+BNupHD5Hn+jYVlt2jDmU7dEH2YiJIB9JaWAJw6nX5xhpqMz4Mp1wPqLY+VMYSM+EeJHuy+75/8R
7+MmblS9nhTy5mXrDBEmNlmOv6PIHbTixOONXBiS6C2x57t2G2BFScOEsvlQhmo/AL90MxOvlDnV
ZbEKUzgzg9P54mwizmd7kfcnAu7XTe2fi1axra5PpSKqNKNR6THy6fZPrlwSsRPrkfWx4u8E6onK
eYcJtX0Yeklmv8q/gox9T4TwWCecJvoIYRuMmALqli21xACoZe0JAUzqY6kBybbaDn6bK7/YJXYH
/6xIV6bwaYjv0n8mfS9Rgx02ZwO/MqGBHj0Ek9dE3a7S8nGOeTkykJv1kL55r0TkBG3p5LZhHcOn
TRMu77zy+/AG1NojDEutUytcQaubmudhOiaoIy0q4zPyOe5Q/p1iHty/uRDSIyhhArG+obr3qqt9
u6v/J31+iuM3hR4BJ0rjOsovnj2osgk3qW8R/sOp5LwOsGx0ATLd0T5LD9rTzIR7BiRm7+y3MFlU
cmBMK2fsPhCdwueAk6k0cEOvIew92U1IMvcOgfddY4/rmhzh+vlMkBpEFbZ+JrGvA4EMRbDFyBOZ
X0dmx5uJp1mFF4lsSjccGiFVULUkwpZ6Qj0T744CIeTwFIzsjmXEr9DA+XJ4KuCSBZKCBNEf4N+Q
FsnBcFwroYQNvw1Dd1PPqG00sikNnG5eEvKCcQ0EWGdPfAryiip5arDzjn/bn1c0uDKtDURrLa12
ipZ0ijucTV1Qm+1IoklSQjt8oBO34mxklNMPR+ECsshriryD/GwZfpyopPdbucDOyOlEDZkjV8ff
ukphioih9L8uRGmlewdNJWwPgua9MF/Di4dqjmkimMKc+5mE29oPYgnG6rQuHbYae0RjbZAL6vQW
v/c+mGmQeg48P7++ZN9T/VGZe0ny6U4nVZ62PtMC13Vu3sP1ujUy8et7g4J4G6FUYrlghXWsG36V
pPcNKW6K2my0L0VTyZSLKW4sjBQXrrMinEUdLmxMVXOOa3+iQwCwSyGk+RZ6N6u1qtvybEXJEth8
4fVR9hA6GJ4rFw6veo921D+ReP0AnquPxMaye7flM8wWVdWUlxmHc5x2Nj+1rJMN3uOrcFuAPibZ
KFVSQ2iTxEF4AIz8FSAOHAcigqYuPLaj2Mw9DMLcpSx2hzO4v+paj+n1elctuI5ihWCokIN05waN
r+nQk3mVmE8n3vmzhmYKvDB7y+WFyAGkUo5FCXmAhDxv/xKF0RmLv81GBwdzt/ME9Il7td2R8CqA
GHWv9KMz64QlYEW7vzF7wz7ezGD3Mddh9AlNa6QskIx5ocezkN8/w7QC2a0EpRw3Sn1EMkibb4Ca
DCf8bL5iMimiLQ3ca4yBy1/IfiIOVhue/thiJUtHGrF9VV/L+mUy6/9v7u3GDUgpzycuLIkFvqwA
9E2PGHRd6bazhgaM1MhGayCoESMzr6UtRHRZS5Ypx5/Cy34wvKV+UVzCWBzoKTGoi9m9jvvMGfI9
ZJDMAv/TvFtb67gns+TI67VGps2y/Du2P/jHPrfwzlgfGD7xC6Ob4Z3AioTyeHP669NewrHh66UW
LMb8TD66vYS4q3+V8cri78x4RmNXgJ+N+NGAXqr8RmfwEMGQnqcKOpY17t/1d8Yf1fnkmOEOZ5nK
K8p/dXLp/9N6+WF4b+lipyhnSHUGpUsxsMfW/Mw3InS2nxJDrKr9PGtHsABsLMLH8vHHY1zp1hz7
S96laukpIntucj/YYHaU6f+WfUstMFNuN8UWVd4L/mKUhKF1JRNlHERH6dR9I0gIKdhfOI0MKThi
Li3K/Dkyem39WD9GuCPCmqPCxTcbv+WR+I9P6X78FguGnwG617hok1llz/PIk++LpZc8GcGDNDZu
nuLuA89l7oAsG4mtBqeQFWOdhIxVF6b4JrGmDjLA8wZF0AAwPRD/+448aWO8XOrN7oCnDqtD3ccN
dRN1T4TPCyxoXjt2rGb8SekyU6efLOco0OFv1OjqpVP9TGtOs+VzAefrY6L7oRTASRCv3GW4LjY2
TYxIQOeGibuF5DEUqshBV7x1sMNYa75KhRE6Vf+Te1h9zgEfScvhRJgKVi/GwerlrflyW5dZexAk
O4rSrbQzs8CDsummxFPrkqoiTk0iOhPT5flIThXNZcV/3IUfr4bZ+rxwfm3QFRvFpWGHQwbl5Djp
QOHMV0T+i2uYckdxNJQZJetlGgnFD6PSRiyqRKaztQbGHrkGKMAtUbhsh9Xb9Krvg5HooYFTbMYi
Rvzh4BG/ioknqNFcMfCFO3qAOiAJsBrzIk2ErTtNpg5KEwQjmnJbyGAIzh2gAWc+ZcSOjNf0U5ao
eVDGcAA0VVKuUdCkHow7/jsnokDAU4zi9/wnfwVy3TNTQoGHXU7ehKLPAX64jRQ1uAJHf+drt6p9
J9tm78KhoJ+ZkVspDYoIAXZZ6hQiukm+Gm3zdyleL2SrjXn+Mw8VvqTk4Aa+nAfWg2rl+QR6/GyD
DmSfQnSTnDNHU2R624PkC4886THgFwfFbq8kv3OLkncq9+CosvEEl/l9gQqlxrkKJaNWwiLahsYh
0dx2seiFXgt6VNTXk3O+HZER8lehdrjaRlHoObduBuZRvnnuj8yLW9sp6hdaPvZ4nMKRvaREcBxR
DM1Hb3wtvoLTgKv4uxcuPDnGXTl0PmH6gmjURyDrLNDdKdCXNUfdlUHX2s++s57YRynwAUqbn82e
BVRWT1mYpBVHK0d3gSlbePhB+zRBumsjQaoiJgK5IIIR4wowgOqEgdZiZNQHmY+fqj3hdBkl4TnQ
FQcmEMupkIbWdop6zO91yvHlLJHCGbNyWWkIviMWyEc4fEYjv+fxTX6F8mgrVxPJ4Nsiu2dWD3XJ
5HISM9acFbjwqXKNhwmhPgTDPADw18/sId7HVJGoI4H+Tycr+Ua5POpu6i2b27L/d15dzPW00kWu
1kfwFHtPvTWnyRKLhL4Oo9fN9KiMcXyM9k6igBE46HdPkGRAZC3DdpTnqX5LIXvg8USrHGUyOdDn
ypT7MZpXdGBtZIibZUykqmvpsFAG3HO4GjC/mt8CWMzv8aidTxFC0bkXmUZfHi/nkZIWnHpV5M/x
1el9HtzGi8JZJXpNFYnaxfb0c5PHFWykybXmNioC8xP49YI0w5lOBGRgKch06Pae6yCRMRzipX4u
Y1c4I7bgo2PLBKJh//sUeV3ZbMPZiyIanMephT4ZhRP59t1S3dUHu8PNJOwC11x0kpMf6xDEwYJN
mR24ncI1UydV6jhwl/BWkBPlvwclzzgC0ylyH3YXB3viFF1m0z8VcpOe/7gsPWe9xpgH7qCqgDpG
jtPV/pvhBWzxKfDDsihBlNIlQVANVI/PtU0ALAzaHzwT1EvF/i8pEiifiI8lr5it0d0YAIm9OUR/
aaXDqWKOAsbrXLgC7B5+5hkAm+4Q0H2Vi1P5of77dE6c1vnini5ywIao9CYf0l2EF9UHtBjbgHE+
PL8PqDhh2d/eMZlzNQwtSLSUMn6O9NBOVsuUMfoGDTRJuw0cskyS/DIWb7SK0xDvWPdqfHeS8ZCn
Ak7iUE+gToOa6iWXJCtCI5FJTCr1ppUdn0ZWiRzXmSzcMa077OCXBxB5GiRAqsa63zRMN0aIAj72
j1OCktANSZHGHo6EEf6lRF0wdXIS+LddJMRFloHTRm/7e3AIlVrUIEhu0hQrd/DwpYV5tATlu9w3
E7A8ys3iE9ggXd2rrI53INnyaltQlMAQmKEKDRqERbhV7CsDcC6eFBdnUPF88XZgInQor3GwCJBW
o/YW/3M/LsYwdPRe/dBT8Kobo0I/3rr9Q5EnTydUIdbJABL8nmB1GG9lcyNJqYGO4MllGeEyLTFp
PEVsNGDXiEoeg6mdHzZe5F2YigqyTUpKxswKTIMXhSyc2JAWxdDX1yz5vxse34e5fwLWCgaZY5ta
3U1Byi0elC3b1UqqUW+lOni3fjD6FqaT2tsRQkY75avosRvcqGX+L3TIGOiXyeg9Ew3snaYMkBW3
VM8eBdrgwY9sHtjDoMOggvo94XAns7joHzTtumPtDyrAr2upvt+gwcIQvgzilzajT/aj3oO98vhY
95h/qccqULCVQ2Lmu/M8jLhrX6mxnhJnBtoIgkqTutwmwR+4p3cXR68VcaG/Ij88JhlkM9iRnjv+
0I/l0t0FLGSFkLMmQ9lGjvqOleeC5aJtsxwVNrsAGqV3JW6d/on72bghxi2k9C4pfsjtstpP/338
zivGOHQ96BKg3rxk376t/BlrrEouojIaC4QhtJLsw7hNquFKC0G4G0+krl2QznUX4/rqzBY9phx/
7SwvavwuVZHNns4OI2EdG/DMZpDOsEZJ9ew1drS5JOEn7s5jFpKtVIIFrjVPk5sEoPuRuVykskbX
gsxy7CYVxCzzCGVzEQP4vPnKM8AShrIOzLuOpvfk7dpXZ6wD8UF9faz8do2I8QX90W1oxO/tgQmc
AOtTnETSLEHO4AXPuBm9rzNVCkQeu/mMf8x/sM69yQJGEXwUAO43a79zenbpeQlGrqDh/7ST7Lzj
J5x+LDbCVGdU1xehGw1emIeAz3i4/pnq4J9gftwBc6WlfzMuHjUbS9PgWf0FnrMbCxVpwrE3nOyU
EddfugsjaJ9YdeewOuW4tTDiqRMndUEVb2QmzsCdV6F2njncmgFEMfT1PdMcuwWIC/Mf+8vifLTj
yZ1lSkz36fiwwJgiVbJ6aXCgSpLJISwX5nL01pYSSmUiHvWQXpzsVA/Z9ITDwPpDs+4APeem34qN
jKnfPQWINEJQH5tLvnx9GKoRMQs6g4E2rhtR++vnMQbTz4EGof6RCx7uXSG+62Db01UoW47oqjcG
b5Oob/OPOzeR4U6FVxf0689Onm0vf8yjCER7cj1UrEZs3+EIVE9vdfvooX95nCOYw8ERzZiLhKno
zrUuJmruzE5ToAdfcK9C+nYPvf5Ya5IWle91S9UTkNNjh4qBXluP00S4vCwJNrJc4fQA8NUszZpu
ALHfNL0cDH/E/jhxWxI5GEvaBArSqbTGgEo9oP2fY2gJyYBImzF/h4M5obRkcUHudkxs4n6apHuE
fJ61O1MzqE3PaEVFi3EFSfxVVLfQldiMYYRH1+WzZItaWcI7swByeB4g2ZXyRTMuuJnWalHdkKJ9
tyDG6c2tYBdTywogs65SuKY9sRmHZQ+SZu9atskA1D9XqvgitiyCq9+3jDMPiH8CLKfio17NHU8u
dJsbbuiLHe8xnOBiRf5rxIdYuzSPnH31i643gPwo7CqLal1xdskYbHYzu6csvBMRtTXyQMiu8hyl
73x1q9lFfl7A/l4GXIrtpsLieXIS4+0ZaRQadsBtmPvpw2HjW54/ZD6DLasGtyrRnPXkCrsQXPgy
A9gAAAbD7434yeTjiZfoEuVN1T11pf6qZ04WQot7sK/WZLn53u9OOWEDDFQ0HQW71gqf9qBbKOCR
U58nnQj97E4h7mSaoqsiT4npkxDdWgQCQli5pjKWaKTyF5Si94ywy5E/sUp9f0Fgbt4OLSuY2jQ1
R5ZrunW+EBChweN7AV5yIe95W8AT/7w1cwWFfwqPLHTlLPzSfWc6GIj3y6mp7TZJR1rexzCjnCZ0
1k5C6qEvtd6xPMs4TOadfc8KDlsCJAKLFLBX1YrWq2qMtkh2Y34sbIC4TuS/WbB0QYRouT1u2uT0
WaFkj552Gt6XNkvwoiiIpKmbS7nXTGlmIL0DZ0iMkufR303M6QrnxM4QRqcMaWVz86j9a1PDs8WR
DBwdRzpYaj0zXYp5dNcPxl4JRH61iToXuFL/PfxZtISiRoKQcFq+DQ9878BfMVWy5U+ScrUA9qRW
Culz6VjJUm0ClYBV/Zejpg3OC0zZVYSsJ+mDTJ+/IlP8v/wNkL6mfbcYWgoFABsBnnUluOMhw6B1
sB46GSqia9fIO9K1vK/ZOHV4Duq3mc9VwgBA06MQ6WZR50wC7JzP4YDVhUmVvx5x7ZxhkkWj0vKx
nMy7uu5yAVv2P5LocqgSJdJBZAnlFHl1q1dF+UUy0xJuSXLr2XqeVG6oyUDkppVyYWmbAk316ZW5
KQy8DPohZHQ4y1M2zPqDZdb5w8b1VpOKFVCVIwO5HMYLhINlLT/B13rxM/QPmKrXXrdAXnRT8+QZ
seldT+ifHfLptHnAgYTglGKN5NWA4A56WxLAJsYtJ/voNxeHDRG/JosqIR+Vp+qB2HwRRDMVFf1m
zA35xsW8AuTZjdjPix5nMCHsJj6BbFMuFETv7vkpHN3dmzvSFkGePg0cRsx9hcunXgEPgPlxILev
L47Xs5T7uFu9W/U9khXQzw2iaP2QY/FS4Nc3Xy1zYPiCBDE5XHtVHkvbraMUzp6/7h6wwt36ULrY
jX5K5VtDEjZ7HdhQxGgmNHGv5iBMK8jv2oQbVxKQLVlvz5/C6taOAlEJXr84PnAwMnh6gZRAeSgr
3tJJnWOO3Vigvvz3ZxfURwlJZTkf+CkEVcMYeFeU7thNhZE4Fl6lMhP4iEyqYLAwBoT/keQ0PhZN
CIyippCfDiPpEODlTQs70g4N5rN+mHJehiM1AIEsVDtqA7SzMBkMZh9jJTVoQ69gU/EYVuFDnOMd
bH6gWWiLQppGlNfQpoGJIepPI3O4OVSVi3gTzn9baAVbdlokiuyBEu8jwCLi8hrPwe2baNX5u4CD
0bYkjX6hkajSTFPD7OoanPxlMNBO4nYk/9W5YZT/STQSZFMgVIJbI1vMLZSnv4Tj7En3LzFqxPql
jTda4Pjgrjrlnl5TSWjE24ni0e0/N2K55SY4VEcppJU7fCQeAGJlUsn+ON4eL0V8KA4CnQ/ak0bh
Me5SfSNoK9yqP2OPoZUJ9ax7zQEv8b4YZUGtvVc8HGRaQG1CrafZs2idJpzZ3mNoXpC/EDP5dEBN
DveSu3UGjcQ2zQ8Dsw8De3FmzPmBuGzuxDgciOEw5MlRg1vEA2tLDXHFUVSwoLMsUSXMD76/APzS
JJdbFBYl/fNsrxpKRtxzfGJ4+1YMxnhDUg6nB6HUH0hjuWcoQAFPhGhYl5OrD1Wc7OFzwN+YO0pL
0fu+lFVx0x5kflNDkyAuGWZ/26LwTnsviw8daBjrQsbr9Dvi7TcAwyhY9SaGO5DEx96GMJk4CLMg
PDItLMA6lVcQhTZN+RAiltY1e7M/Ndf3YygRIXX5CW+O0UAmdKMP/jS+IwbfCNHRzSMFpxm2iEEJ
zd8ByoFkQz2pWdnmvIKGvFYYoh4z+U0hxGQ4LUw0YAYiBXgP5CtCpEF3UWx86zTcS3uRONuPBgOa
oSBYTRL2K1lKh13fehSd0OKtM5Z9Gdg2jC4GTy9HAyIeLFZbcJzLat/9JTkxW9HJzFehNq2SyJ7c
LrlcBHkxs2NqW6/Xm4pS+6/p/CasMCkF0yVmCkJ7iyKlriLD54PxGASVDH8sWHwHQjtu2URVadR4
2Q+zrPqeubT0iX23KjpRW3MNZ9CKJsx950JsjegwxIc12xRy6I4DS9J4Dy9ghzE9q/XMEYrULdjH
ipiETuhMMUG1gPjI0Zl6A/HU4vIvOIsY98QDlOnbc0YHrYYKgJ51ez/VxPsWXivPtR9REGVuvCXv
ytDoCEWkqufRfmdNiQNrjiPIxeqrkAT9akZkDj9hChf2qu7bS3+Sq87PoWbcOGWWQ6McfnpH6CWj
Dt3M0w3Nz/MlyDJ9x7Gjo5jhjRj/XWQlwVXGp86gsHo2bJEsFkh7wjB58yE/AwFeyETKEi/uJ3Ac
3+EDE4sZLdRll+DvWeU0WEBbPFTNNBvbF0JzxhqGDvuwYb4DSdaR1zXEE9UueMAkZ9GQU/M0RU5y
8LvHLzRAeShLxiQzo73gNR5EndaViCJUptgrYaej4bE+aVCuEJSfFJXkiFFEWcsG8XwXzFL/8nXJ
peCXFG9nyMZl0q73ZZ+YGIiEjDRJgUreIQzK9LCdxtchlO1Iw9CcpGWkbASKT9+iudCCMet4CxZl
eP64CDMjIBH9aFykmoAqzLhouYHpOGbmuCQM1TOA/i9if0yHmyJUiV3vEwPsN/syRA4e4ml+s8GP
7FGP9St3xWvx3/ZGmlahiLKD73zPr4zezPbG7ZHrAD1Vn3fuYngQCBqHMLOK8/wqW/nyC2CG8sPO
cY4sr6gR+TpVuy/fBu8z7zTr8byoBb4OcQSnN/+q/aIiJiTud3vnF6ZictcGmjZOdcg5ySevj5ua
Zd3zZ0jYrUq4FCQHjC/vciw2za6mmEHM4qu2e+sYISSgQH7vKBAv01d19uB1cXjqCD7Q8YvqREhn
0IbyWiYm9zJdBNQKGVUrHN++88VvMe5RXAhvlLPsdgct7cCvgUFAyziR+RMMSqvkThjeNpvRHv0D
2ZsP1nG09UGnr1E0rKvg49yrWGFEe1FVg0Lu0eDdmhwGPHZpTHC23Fv/Tf8x9NkswPqnOz6KP5Xq
4ikTjjNJZqjnh+QF1RcWXBkOXMXm40babP1MdvXRf7S8FKC/PKGhmZxKHfZ1FAMTSnU62gl1MinM
HHODyMRjpsGZcvDivyTxYVDLwcwJzlaeTT1HHXtRxrmTVNrZ9ugg2BlULu8EpQRWl8SHdoQb1fNb
6o2ZPVC5+JODz9ISxJpIcOQNhVmlj1O17fBPYeR8fDJXAQSXc5/m+Qe7lB22XEk4Hr3EcDOjT6X4
idEoVejokq68MCmb35ZJmP9Chxv0N4s/KrdiWATZyiCeK7qYfuSssLTMxRLYY84AE5wEi6US8Uu3
IFIPJyvxMDeJ5UQbF1QewEplziJLPaKcAd+4jZBRSTU3U+iKWRvE+h/dMUt1cA/cjQuyw2aZM2ml
HgFuYUxm5Y2A043bh65a3p0s93Xyc9l99x/3+gG5Me7DKq0bOD81kf7h8bQ76SDYfmwOFHUohasc
Se7HGqw3rmultqYUMpyNtulJSChp1piKed1w8lXL22U5/HpZ11h7x7JWzy3TEEHeYkPOVZ4wp3ZZ
zJGPSpONIuCdgKLMLqel11/DT5hUhURpgL/EWX0JcEQQJvoe+0rMcjOwfDcX/9Su3gykGG5LXTq5
WCqtrhFhVPOMiI2xfcvUdLdD4PeB0jwplbGLGCoKckxcm2dSeAkcLsQfxJL0lX45bCrg1WizNi5p
01yJcL18u85y5S5y/PPqXbJkkYEo9NUR7jELPc4i/3lN02a+txbS3gvaPJE0P7BK3+7xjS3hbBp5
yftAy8AOVN3n7oA7aco9CHfkfoYDzl6g5hEygW/X/DPT9JCw5+gyj0Pujd57alfQF57vkgW8uFiA
D4MvZJ7hJiQlVq8Zaw0xmuDlig5ygxz356dGa4UCQIdj+3gg6EQIf0sKM2TUf6qVtQD+Od6A1x+2
pCXQZm7EQCNYcufzM3XhRbIPk7QYrfyVCQqYiS6B4Se6OY1mfyBsMDhLjDOllIcNzH0FWTM1ggVV
P6Y1WCqb4Sn/5LB1ornOamRNwfI2xB0AbCznFDxXQD7T2P1ABOThXegG/vx0kwxJnq7TTfx0NccH
Xp+lBSo2+CG/LasU69P8sOXToD5/vBdsjP+7KclNqwMyGXKTXhNKkQ/O7m/dkDMctBX7OO2bYZv4
d8ih4wOI0ghL8LnVIBazePAxC9tRsCGTGLPBpuYXDpoqZFfVhTsQL0R23n0qu3XomInN187UIMkw
spBnExcgzJNPsxBqKenA45qzyxLjIXCti5TzU+x6zhTd3h7qowonQg7rlSmm8IRJCgsLxtpzSU+l
5nNhVuorAsiKVpl8foGBuH/GajwWo1B9gnuX1Y2mo1oYgUWXBM23xaOXZhwQv3oLlVzIk1Mn0sQI
Qf4e+JIG88lEb9uRc0kxv8zQufbYZTjPzhvrcpcJhdHsYeU6EUkYXDqsoySgGMNfyJzCRmyIaaoY
rGOUzl7z/yeNSx4+sbcFuaO0ktqfTwkJBQuz1Jk/khYL9WEwkUbN02gh7mVmQDl04Tkdc5K2ioNQ
or17JP9hKmhQdAudgY7Ex/UZMHbc4ikHeF3C8ORtxXunRyR/hFx8/HD8n5KkcnK4kkV2XUELzEwK
OfKiPp6yRjRfFudcz0+7NxqXnQZQAJj4pjScwCu/kWIkQ14frhCEwKLPfelQPZpGGkMwzsWBrh1z
+OGRCGbYvXCQNXQL09en+2PZjn5dffWgVMQYkrm2DuGWoD63ornBL8T8UH43lxdBsiY71S8/d0CS
bb7LnqFkA1bRdF2fNYF/Law16XLDtt+O0s0v2zbpoAd+GJDghUhOyxOZpIqU10+DAxkBswRD7y4o
DJ0r/4DAb5WIFCi0mWjc5W8iGWYW3jx2kCtgIUf4TH6+tpARqPPSd7SCSc77VBGuTcc8fbeOBD/t
3L0+eFYnzicFfAMhTA6SONsHZUbrTKB2InSmKCBlz3SBamZykERT+Qg512ZcC21d8Gav63/jEg8H
ZRiSdKRXjpVsvEwS8qVQfIebfj+QG+3pFxyqe+WjW51KZGMQw+AcP2PYYtOFd8IKJiGlyz25F0V7
+V87eZp1RbQ/lIHW0X0e2y2Al3QLdYYUmV/bMaHbaRg5XFj1YXBB0xtKLkYCHKkL1atbk9F8K2J2
a9HNJn2P9H5UvgzC7cl3YFYCRvV2fTyEiGH8lfxOr6AZqFHGLzs1q9DPV6zGWdYwb+4W1v9Qvgkd
Hd/7LUOhY8RFOdDnWhukLd5BwNYRE2GL7jG64OQhmKRuE+0Gyfr1NUvtzufFZ8egDxfqFoMwKyxl
9nO5WJVb7NG5ZCHb2TRDalrs+VQFEyVFOa+C4039uFng6zyDmNQ8z6kSNxRDcn1LSGbGaN6j58B5
ZFAcHG0Oq2jAfLg8ElokBhe8W0mf7XN1RJtaeBGZKVbVaXmYnkTVBU3Dp03idvHj7QQlH3O21nzF
pVLJPIsJWHisTd+p5TuhRBP1vTzHthYt3eeAYx8ciWkaL7VsUouDg0XnNJ6xZKR08+d11/Z6Q8jT
q4glH2cfqRw4j7f0MP4uXnQqv5NeD6jhHKJrjA8lnHuETl4sXbtL+OeV1Q4MhI7xJ/ZtP7cOVzp6
KePaPPGvYrErB3vnX5bd++KZWLJAQjUQrkieg3WeB/upLpEh+6hVjsgK/mUugXiOh9BmTRf3COP5
yXieejDK16Zsi5/+MCLv7EVJyEhYxI+Emu1n2MnuJF3DKFeN+ap/MSvQXzSQyMRCxowhZH9Lxw8I
fA4w+XFKJve3ql1WsD0olrR47QjbzcT9VU409etLU44GmS7TpB2T3SE9KJ9Ph2K6qGEs3h1meZy3
SrOZrbIYe32FgEj9BOn5ZORpBwXl8oOqKbYP132prKplpEOuGf14ZhiSRdHJPfFCyzw8OgplAg4R
sUapIoqVc1bkO6l2cDeQodxejGed+Q==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
