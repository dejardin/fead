-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- Copyright 2022-2023 Advanced Micro Devices, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2023.2 (lin64) Build 4029153 Fri Oct 13 20:13:54 MDT 2023
-- Date        : Wed Jan 17 16:49:38 2024
-- Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 38 (Thirty Eight)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ gig_ethernet_pcs_pma_0_sim_netlist.vhdl
-- Design      : gig_ethernet_pcs_pma_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7k70tfbg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_GT is
  port (
    gtxe2_i_0 : out STD_LOGIC;
    gt0_cpllrefclklost_i : out STD_LOGIC;
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    gtxe2_i_1 : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    gtxe2_i_2 : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_4 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_5 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_7 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    cpll_pd0_i : in STD_LOGIC;
    cpllreset_in : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_gttxreset_in0_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gt0_rxuserrdy_t : in STD_LOGIC;
    gtxe2_i_8 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_txuserrdy_t : in STD_LOGIC;
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_9 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_10 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_11 : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_GT;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_GT is
  signal gtxe2_i_n_0 : STD_LOGIC;
  signal gtxe2_i_n_10 : STD_LOGIC;
  signal gtxe2_i_n_16 : STD_LOGIC;
  signal gtxe2_i_n_170 : STD_LOGIC;
  signal gtxe2_i_n_171 : STD_LOGIC;
  signal gtxe2_i_n_172 : STD_LOGIC;
  signal gtxe2_i_n_173 : STD_LOGIC;
  signal gtxe2_i_n_174 : STD_LOGIC;
  signal gtxe2_i_n_175 : STD_LOGIC;
  signal gtxe2_i_n_176 : STD_LOGIC;
  signal gtxe2_i_n_177 : STD_LOGIC;
  signal gtxe2_i_n_178 : STD_LOGIC;
  signal gtxe2_i_n_179 : STD_LOGIC;
  signal gtxe2_i_n_180 : STD_LOGIC;
  signal gtxe2_i_n_181 : STD_LOGIC;
  signal gtxe2_i_n_182 : STD_LOGIC;
  signal gtxe2_i_n_183 : STD_LOGIC;
  signal gtxe2_i_n_184 : STD_LOGIC;
  signal gtxe2_i_n_27 : STD_LOGIC;
  signal gtxe2_i_n_3 : STD_LOGIC;
  signal gtxe2_i_n_38 : STD_LOGIC;
  signal gtxe2_i_n_39 : STD_LOGIC;
  signal gtxe2_i_n_4 : STD_LOGIC;
  signal gtxe2_i_n_46 : STD_LOGIC;
  signal gtxe2_i_n_47 : STD_LOGIC;
  signal gtxe2_i_n_48 : STD_LOGIC;
  signal gtxe2_i_n_49 : STD_LOGIC;
  signal gtxe2_i_n_50 : STD_LOGIC;
  signal gtxe2_i_n_51 : STD_LOGIC;
  signal gtxe2_i_n_52 : STD_LOGIC;
  signal gtxe2_i_n_53 : STD_LOGIC;
  signal gtxe2_i_n_54 : STD_LOGIC;
  signal gtxe2_i_n_55 : STD_LOGIC;
  signal gtxe2_i_n_56 : STD_LOGIC;
  signal gtxe2_i_n_57 : STD_LOGIC;
  signal gtxe2_i_n_58 : STD_LOGIC;
  signal gtxe2_i_n_59 : STD_LOGIC;
  signal gtxe2_i_n_60 : STD_LOGIC;
  signal gtxe2_i_n_61 : STD_LOGIC;
  signal gtxe2_i_n_81 : STD_LOGIC;
  signal gtxe2_i_n_83 : STD_LOGIC;
  signal gtxe2_i_n_84 : STD_LOGIC;
  signal gtxe2_i_n_9 : STD_LOGIC;
  signal NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_PHYSTATUS_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXDATAVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXELECIDLE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXQPISENN_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXQPISENP_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXRATEDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXQPISENN_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXQPISENP_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXRATEDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXCHARISK_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXCHBONDO_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gtxe2_i_RXDATA_UNCONNECTED : STD_LOGIC_VECTOR ( 63 downto 16 );
  signal NLW_gtxe2_i_RXDISPERR_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXHEADER_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gtxe2_i_RXSTATUS_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_gtxe2_i_TSTOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  attribute box_type : string;
  attribute box_type of gtxe2_i : label is "PRIMITIVE";
begin
gtxe2_i: unisim.vcomponents.GTXE2_CHANNEL
    generic map(
      ALIGN_COMMA_DOUBLE => "FALSE",
      ALIGN_COMMA_ENABLE => B"0001111111",
      ALIGN_COMMA_WORD => 2,
      ALIGN_MCOMMA_DET => "TRUE",
      ALIGN_MCOMMA_VALUE => B"1010000011",
      ALIGN_PCOMMA_DET => "TRUE",
      ALIGN_PCOMMA_VALUE => B"0101111100",
      CBCC_DATA_SOURCE_SEL => "DECODED",
      CHAN_BOND_KEEP_ALIGN => "FALSE",
      CHAN_BOND_MAX_SKEW => 1,
      CHAN_BOND_SEQ_1_1 => B"0000000000",
      CHAN_BOND_SEQ_1_2 => B"0000000000",
      CHAN_BOND_SEQ_1_3 => B"0000000000",
      CHAN_BOND_SEQ_1_4 => B"0000000000",
      CHAN_BOND_SEQ_1_ENABLE => B"1111",
      CHAN_BOND_SEQ_2_1 => B"0000000000",
      CHAN_BOND_SEQ_2_2 => B"0000000000",
      CHAN_BOND_SEQ_2_3 => B"0000000000",
      CHAN_BOND_SEQ_2_4 => B"0000000000",
      CHAN_BOND_SEQ_2_ENABLE => B"1111",
      CHAN_BOND_SEQ_2_USE => "FALSE",
      CHAN_BOND_SEQ_LEN => 1,
      CLK_CORRECT_USE => "TRUE",
      CLK_COR_KEEP_IDLE => "FALSE",
      CLK_COR_MAX_LAT => 36,
      CLK_COR_MIN_LAT => 33,
      CLK_COR_PRECEDENCE => "TRUE",
      CLK_COR_REPEAT_WAIT => 0,
      CLK_COR_SEQ_1_1 => B"0110111100",
      CLK_COR_SEQ_1_2 => B"0001010000",
      CLK_COR_SEQ_1_3 => B"0000000000",
      CLK_COR_SEQ_1_4 => B"0000000000",
      CLK_COR_SEQ_1_ENABLE => B"1111",
      CLK_COR_SEQ_2_1 => B"0110111100",
      CLK_COR_SEQ_2_2 => B"0010110101",
      CLK_COR_SEQ_2_3 => B"0000000000",
      CLK_COR_SEQ_2_4 => B"0000000000",
      CLK_COR_SEQ_2_ENABLE => B"1111",
      CLK_COR_SEQ_2_USE => "TRUE",
      CLK_COR_SEQ_LEN => 2,
      CPLL_CFG => X"BC07DC",
      CPLL_FBDIV => 4,
      CPLL_FBDIV_45 => 5,
      CPLL_INIT_CFG => X"00001E",
      CPLL_LOCK_CFG => X"01E8",
      CPLL_REFCLK_DIV => 1,
      DEC_MCOMMA_DETECT => "TRUE",
      DEC_PCOMMA_DETECT => "TRUE",
      DEC_VALID_COMMA_ONLY => "FALSE",
      DMONITOR_CFG => X"000A00",
      ES_CONTROL => B"000000",
      ES_ERRDET_EN => "FALSE",
      ES_EYE_SCAN_EN => "TRUE",
      ES_HORZ_OFFSET => X"000",
      ES_PMA_CFG => B"0000000000",
      ES_PRESCALE => B"00000",
      ES_QUALIFIER => X"00000000000000000000",
      ES_QUAL_MASK => X"00000000000000000000",
      ES_SDATA_MASK => X"00000000000000000000",
      ES_VERT_OFFSET => B"000000000",
      FTS_DESKEW_SEQ_ENABLE => B"1111",
      FTS_LANE_DESKEW_CFG => B"1111",
      FTS_LANE_DESKEW_EN => "FALSE",
      GEARBOX_MODE => B"000",
      IS_CPLLLOCKDETCLK_INVERTED => '0',
      IS_DRPCLK_INVERTED => '0',
      IS_GTGREFCLK_INVERTED => '0',
      IS_RXUSRCLK2_INVERTED => '0',
      IS_RXUSRCLK_INVERTED => '0',
      IS_TXPHDLYTSTCLK_INVERTED => '0',
      IS_TXUSRCLK2_INVERTED => '0',
      IS_TXUSRCLK_INVERTED => '0',
      OUTREFCLK_SEL_INV => B"11",
      PCS_PCIE_EN => "FALSE",
      PCS_RSVD_ATTR => X"000000000000",
      PD_TRANS_TIME_FROM_P2 => X"03C",
      PD_TRANS_TIME_NONE_P2 => X"19",
      PD_TRANS_TIME_TO_P2 => X"64",
      PMA_RSV => X"00018480",
      PMA_RSV2 => X"2050",
      PMA_RSV3 => B"00",
      PMA_RSV4 => X"00000000",
      RXBUFRESET_TIME => B"00001",
      RXBUF_ADDR_MODE => "FULL",
      RXBUF_EIDLE_HI_CNT => B"1000",
      RXBUF_EIDLE_LO_CNT => B"0000",
      RXBUF_EN => "TRUE",
      RXBUF_RESET_ON_CB_CHANGE => "TRUE",
      RXBUF_RESET_ON_COMMAALIGN => "FALSE",
      RXBUF_RESET_ON_EIDLE => "FALSE",
      RXBUF_RESET_ON_RATE_CHANGE => "TRUE",
      RXBUF_THRESH_OVFLW => 61,
      RXBUF_THRESH_OVRD => "FALSE",
      RXBUF_THRESH_UNDFLW => 8,
      RXCDRFREQRESET_TIME => B"00001",
      RXCDRPHRESET_TIME => B"00001",
      RXCDR_CFG => X"03000023FF10100020",
      RXCDR_FR_RESET_ON_EIDLE => '0',
      RXCDR_HOLD_DURING_EIDLE => '0',
      RXCDR_LOCK_CFG => B"010101",
      RXCDR_PH_RESET_ON_EIDLE => '0',
      RXDFELPMRESET_TIME => B"0001111",
      RXDLY_CFG => X"001F",
      RXDLY_LCFG => X"030",
      RXDLY_TAP_CFG => X"0000",
      RXGEARBOX_EN => "FALSE",
      RXISCANRESET_TIME => B"00001",
      RXLPM_HF_CFG => B"00000011110000",
      RXLPM_LF_CFG => B"00000011110000",
      RXOOB_CFG => B"0000110",
      RXOUT_DIV => 4,
      RXPCSRESET_TIME => B"00001",
      RXPHDLY_CFG => X"084020",
      RXPH_CFG => X"000000",
      RXPH_MONITOR_SEL => B"00000",
      RXPMARESET_TIME => B"00011",
      RXPRBS_ERR_LOOPBACK => '0',
      RXSLIDE_AUTO_WAIT => 7,
      RXSLIDE_MODE => "OFF",
      RX_BIAS_CFG => B"000000000100",
      RX_BUFFER_CFG => B"000000",
      RX_CLK25_DIV => 5,
      RX_CLKMUX_PD => '1',
      RX_CM_SEL => B"11",
      RX_CM_TRIM => B"010",
      RX_DATA_WIDTH => 20,
      RX_DDI_SEL => B"000000",
      RX_DEBUG_CFG => B"000000000000",
      RX_DEFER_RESET_BUF_EN => "TRUE",
      RX_DFE_GAIN_CFG => X"020FEA",
      RX_DFE_H2_CFG => B"000000000000",
      RX_DFE_H3_CFG => B"000001000000",
      RX_DFE_H4_CFG => B"00011110000",
      RX_DFE_H5_CFG => B"00011100000",
      RX_DFE_KL_CFG => B"0000011111110",
      RX_DFE_KL_CFG2 => X"301148AC",
      RX_DFE_LPM_CFG => X"0904",
      RX_DFE_LPM_HOLD_DURING_EIDLE => '0',
      RX_DFE_UT_CFG => B"10001111000000000",
      RX_DFE_VP_CFG => B"00011111100000011",
      RX_DFE_XYD_CFG => B"0000000000000",
      RX_DISPERR_SEQ_MATCH => "TRUE",
      RX_INT_DATAWIDTH => 0,
      RX_OS_CFG => B"0000010000000",
      RX_SIG_VALID_DLY => 10,
      RX_XCLK_SEL => "RXREC",
      SAS_MAX_COM => 64,
      SAS_MIN_COM => 36,
      SATA_BURST_SEQ_LEN => B"0101",
      SATA_BURST_VAL => B"100",
      SATA_CPLL_CFG => "VCO_3000MHZ",
      SATA_EIDLE_VAL => B"100",
      SATA_MAX_BURST => 8,
      SATA_MAX_INIT => 21,
      SATA_MAX_WAKE => 7,
      SATA_MIN_BURST => 4,
      SATA_MIN_INIT => 12,
      SATA_MIN_WAKE => 4,
      SHOW_REALIGN_COMMA => "TRUE",
      SIM_CPLLREFCLK_SEL => B"001",
      SIM_RECEIVER_DETECT_PASS => "TRUE",
      SIM_RESET_SPEEDUP => "TRUE",
      SIM_TX_EIDLE_DRIVE_LEVEL => "X",
      SIM_VERSION => "4.0",
      TERM_RCAL_CFG => B"10000",
      TERM_RCAL_OVRD => '0',
      TRANS_TIME_RATE => X"0E",
      TST_RSV => X"00000000",
      TXBUF_EN => "TRUE",
      TXBUF_RESET_ON_RATE_CHANGE => "TRUE",
      TXDLY_CFG => X"001F",
      TXDLY_LCFG => X"030",
      TXDLY_TAP_CFG => X"0000",
      TXGEARBOX_EN => "FALSE",
      TXOUT_DIV => 4,
      TXPCSRESET_TIME => B"00001",
      TXPHDLY_CFG => X"084020",
      TXPH_CFG => X"0780",
      TXPH_MONITOR_SEL => B"00000",
      TXPMARESET_TIME => B"00001",
      TX_CLK25_DIV => 5,
      TX_CLKMUX_PD => '1',
      TX_DATA_WIDTH => 20,
      TX_DEEMPH0 => B"00000",
      TX_DEEMPH1 => B"00000",
      TX_DRIVE_MODE => "DIRECT",
      TX_EIDLE_ASSERT_DELAY => B"110",
      TX_EIDLE_DEASSERT_DELAY => B"100",
      TX_INT_DATAWIDTH => 0,
      TX_LOOPBACK_DRIVE_HIZ => "FALSE",
      TX_MAINCURSOR_SEL => '0',
      TX_MARGIN_FULL_0 => B"1001110",
      TX_MARGIN_FULL_1 => B"1001001",
      TX_MARGIN_FULL_2 => B"1000101",
      TX_MARGIN_FULL_3 => B"1000010",
      TX_MARGIN_FULL_4 => B"1000000",
      TX_MARGIN_LOW_0 => B"1000110",
      TX_MARGIN_LOW_1 => B"1000100",
      TX_MARGIN_LOW_2 => B"1000010",
      TX_MARGIN_LOW_3 => B"1000000",
      TX_MARGIN_LOW_4 => B"1000000",
      TX_PREDRIVER_MODE => '0',
      TX_QPI_STATUS_EN => '0',
      TX_RXDETECT_CFG => X"1832",
      TX_RXDETECT_REF => B"100",
      TX_XCLK_SEL => "TXOUT",
      UCODEER_CLR => '0'
    )
        port map (
      CFGRESET => '0',
      CLKRSVD(3 downto 0) => B"0000",
      CPLLFBCLKLOST => gtxe2_i_n_0,
      CPLLLOCK => gtxe2_i_0,
      CPLLLOCKDETCLK => independent_clock_bufg,
      CPLLLOCKEN => '1',
      CPLLPD => cpll_pd0_i,
      CPLLREFCLKLOST => gt0_cpllrefclklost_i,
      CPLLREFCLKSEL(2 downto 0) => B"001",
      CPLLRESET => cpllreset_in,
      DMONITOROUT(7) => gtxe2_i_n_177,
      DMONITOROUT(6) => gtxe2_i_n_178,
      DMONITOROUT(5) => gtxe2_i_n_179,
      DMONITOROUT(4) => gtxe2_i_n_180,
      DMONITOROUT(3) => gtxe2_i_n_181,
      DMONITOROUT(2) => gtxe2_i_n_182,
      DMONITOROUT(1) => gtxe2_i_n_183,
      DMONITOROUT(0) => gtxe2_i_n_184,
      DRPADDR(8 downto 0) => B"000000000",
      DRPCLK => gtrefclk_bufg,
      DRPDI(15 downto 0) => B"0000000000000000",
      DRPDO(15) => gtxe2_i_n_46,
      DRPDO(14) => gtxe2_i_n_47,
      DRPDO(13) => gtxe2_i_n_48,
      DRPDO(12) => gtxe2_i_n_49,
      DRPDO(11) => gtxe2_i_n_50,
      DRPDO(10) => gtxe2_i_n_51,
      DRPDO(9) => gtxe2_i_n_52,
      DRPDO(8) => gtxe2_i_n_53,
      DRPDO(7) => gtxe2_i_n_54,
      DRPDO(6) => gtxe2_i_n_55,
      DRPDO(5) => gtxe2_i_n_56,
      DRPDO(4) => gtxe2_i_n_57,
      DRPDO(3) => gtxe2_i_n_58,
      DRPDO(2) => gtxe2_i_n_59,
      DRPDO(1) => gtxe2_i_n_60,
      DRPDO(0) => gtxe2_i_n_61,
      DRPEN => '0',
      DRPRDY => gtxe2_i_n_3,
      DRPWE => '0',
      EYESCANDATAERROR => gtxe2_i_n_4,
      EYESCANMODE => '0',
      EYESCANRESET => '0',
      EYESCANTRIGGER => '0',
      GTGREFCLK => '0',
      GTNORTHREFCLK0 => '0',
      GTNORTHREFCLK1 => '0',
      GTREFCLK0 => gtrefclk_out,
      GTREFCLK1 => '0',
      GTREFCLKMONITOR => NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED,
      GTRESETSEL => '0',
      GTRSVD(15 downto 0) => B"0000000000000000",
      GTRXRESET => SR(0),
      GTSOUTHREFCLK0 => '0',
      GTSOUTHREFCLK1 => '0',
      GTTXRESET => gt0_gttxreset_in0_out,
      GTXRXN => rxn,
      GTXRXP => rxp,
      GTXTXN => txn,
      GTXTXP => txp,
      LOOPBACK(2 downto 0) => B"000",
      PCSRSVDIN(15 downto 0) => B"0000000000000000",
      PCSRSVDIN2(4 downto 0) => B"00000",
      PCSRSVDOUT(15 downto 0) => NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED(15 downto 0),
      PHYSTATUS => NLW_gtxe2_i_PHYSTATUS_UNCONNECTED,
      PMARSVDIN(4 downto 0) => B"00000",
      PMARSVDIN2(4 downto 0) => B"00000",
      QPLLCLK => gt0_qplloutclk_out,
      QPLLREFCLK => gt0_qplloutrefclk_out,
      RESETOVRD => '0',
      RX8B10BEN => '1',
      RXBUFRESET => '0',
      RXBUFSTATUS(2) => RXBUFSTATUS(0),
      RXBUFSTATUS(1) => gtxe2_i_n_83,
      RXBUFSTATUS(0) => gtxe2_i_n_84,
      RXBYTEISALIGNED => gtxe2_i_n_9,
      RXBYTEREALIGN => gtxe2_i_n_10,
      RXCDRFREQRESET => '0',
      RXCDRHOLD => '0',
      RXCDRLOCK => NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED,
      RXCDROVRDEN => '0',
      RXCDRRESET => '0',
      RXCDRRESETRSV => '0',
      RXCHANBONDSEQ => NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED,
      RXCHANISALIGNED => NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED,
      RXCHANREALIGN => NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED,
      RXCHARISCOMMA(7 downto 2) => NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED(7 downto 2),
      RXCHARISCOMMA(1 downto 0) => gtxe2_i_4(1 downto 0),
      RXCHARISK(7 downto 2) => NLW_gtxe2_i_RXCHARISK_UNCONNECTED(7 downto 2),
      RXCHARISK(1 downto 0) => gtxe2_i_5(1 downto 0),
      RXCHBONDEN => '0',
      RXCHBONDI(4 downto 0) => B"00000",
      RXCHBONDLEVEL(2 downto 0) => B"000",
      RXCHBONDMASTER => '0',
      RXCHBONDO(4 downto 0) => NLW_gtxe2_i_RXCHBONDO_UNCONNECTED(4 downto 0),
      RXCHBONDSLAVE => '0',
      RXCLKCORCNT(1 downto 0) => D(1 downto 0),
      RXCOMINITDET => NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED,
      RXCOMMADET => gtxe2_i_n_16,
      RXCOMMADETEN => '1',
      RXCOMSASDET => NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED,
      RXCOMWAKEDET => NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED,
      RXDATA(63 downto 16) => NLW_gtxe2_i_RXDATA_UNCONNECTED(63 downto 16),
      RXDATA(15 downto 0) => gtxe2_i_3(15 downto 0),
      RXDATAVALID => NLW_gtxe2_i_RXDATAVALID_UNCONNECTED,
      RXDDIEN => '0',
      RXDFEAGCHOLD => '0',
      RXDFEAGCOVRDEN => '0',
      RXDFECM1EN => '0',
      RXDFELFHOLD => '0',
      RXDFELFOVRDEN => '0',
      RXDFELPMRESET => '0',
      RXDFETAP2HOLD => '0',
      RXDFETAP2OVRDEN => '0',
      RXDFETAP3HOLD => '0',
      RXDFETAP3OVRDEN => '0',
      RXDFETAP4HOLD => '0',
      RXDFETAP4OVRDEN => '0',
      RXDFETAP5HOLD => '0',
      RXDFETAP5OVRDEN => '0',
      RXDFEUTHOLD => '0',
      RXDFEUTOVRDEN => '0',
      RXDFEVPHOLD => '0',
      RXDFEVPOVRDEN => '0',
      RXDFEVSEN => '0',
      RXDFEXYDEN => '1',
      RXDFEXYDHOLD => '0',
      RXDFEXYDOVRDEN => '0',
      RXDISPERR(7 downto 2) => NLW_gtxe2_i_RXDISPERR_UNCONNECTED(7 downto 2),
      RXDISPERR(1 downto 0) => gtxe2_i_6(1 downto 0),
      RXDLYBYPASS => '1',
      RXDLYEN => '0',
      RXDLYOVRDEN => '0',
      RXDLYSRESET => '0',
      RXDLYSRESETDONE => NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED,
      RXELECIDLE => NLW_gtxe2_i_RXELECIDLE_UNCONNECTED,
      RXELECIDLEMODE(1 downto 0) => B"11",
      RXGEARBOXSLIP => '0',
      RXHEADER(2 downto 0) => NLW_gtxe2_i_RXHEADER_UNCONNECTED(2 downto 0),
      RXHEADERVALID => NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED,
      RXLPMEN => '1',
      RXLPMHFHOLD => '0',
      RXLPMHFOVRDEN => '0',
      RXLPMLFHOLD => '0',
      RXLPMLFKLOVRDEN => '0',
      RXMCOMMAALIGNEN => reset_out,
      RXMONITOROUT(6) => gtxe2_i_n_170,
      RXMONITOROUT(5) => gtxe2_i_n_171,
      RXMONITOROUT(4) => gtxe2_i_n_172,
      RXMONITOROUT(3) => gtxe2_i_n_173,
      RXMONITOROUT(2) => gtxe2_i_n_174,
      RXMONITOROUT(1) => gtxe2_i_n_175,
      RXMONITOROUT(0) => gtxe2_i_n_176,
      RXMONITORSEL(1 downto 0) => B"00",
      RXNOTINTABLE(7 downto 2) => NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED(7 downto 2),
      RXNOTINTABLE(1 downto 0) => gtxe2_i_7(1 downto 0),
      RXOOBRESET => '0',
      RXOSHOLD => '0',
      RXOSOVRDEN => '0',
      RXOUTCLK => rxoutclk,
      RXOUTCLKFABRIC => NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED,
      RXOUTCLKPCS => NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED,
      RXOUTCLKSEL(2 downto 0) => B"010",
      RXPCOMMAALIGNEN => reset_out,
      RXPCSRESET => reset,
      RXPD(1) => RXPD(0),
      RXPD(0) => RXPD(0),
      RXPHALIGN => '0',
      RXPHALIGNDONE => NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED,
      RXPHALIGNEN => '0',
      RXPHDLYPD => '0',
      RXPHDLYRESET => '0',
      RXPHMONITOR(4 downto 0) => NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED(4 downto 0),
      RXPHOVRDEN => '0',
      RXPHSLIPMONITOR(4 downto 0) => NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED(4 downto 0),
      RXPMARESET => '0',
      RXPOLARITY => '0',
      RXPRBSCNTRESET => '0',
      RXPRBSERR => gtxe2_i_n_27,
      RXPRBSSEL(2 downto 0) => B"000",
      RXQPIEN => '0',
      RXQPISENN => NLW_gtxe2_i_RXQPISENN_UNCONNECTED,
      RXQPISENP => NLW_gtxe2_i_RXQPISENP_UNCONNECTED,
      RXRATE(2 downto 0) => B"000",
      RXRATEDONE => NLW_gtxe2_i_RXRATEDONE_UNCONNECTED,
      RXRESETDONE => gtxe2_i_1,
      RXSLIDE => '0',
      RXSTARTOFSEQ => NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED,
      RXSTATUS(2 downto 0) => NLW_gtxe2_i_RXSTATUS_UNCONNECTED(2 downto 0),
      RXSYSCLKSEL(1 downto 0) => B"00",
      RXUSERRDY => gt0_rxuserrdy_t,
      RXUSRCLK => gtxe2_i_8,
      RXUSRCLK2 => gtxe2_i_8,
      RXVALID => NLW_gtxe2_i_RXVALID_UNCONNECTED,
      SETERRSTATUS => '0',
      TSTIN(19 downto 0) => B"11111111111111111111",
      TSTOUT(9 downto 0) => NLW_gtxe2_i_TSTOUT_UNCONNECTED(9 downto 0),
      TX8B10BBYPASS(7 downto 0) => B"00000000",
      TX8B10BEN => '1',
      TXBUFDIFFCTRL(2 downto 0) => B"100",
      TXBUFSTATUS(1) => TXBUFSTATUS(0),
      TXBUFSTATUS(0) => gtxe2_i_n_81,
      TXCHARDISPMODE(7 downto 2) => B"000000",
      TXCHARDISPMODE(1 downto 0) => gtxe2_i_9(1 downto 0),
      TXCHARDISPVAL(7 downto 2) => B"000000",
      TXCHARDISPVAL(1 downto 0) => gtxe2_i_10(1 downto 0),
      TXCHARISK(7 downto 2) => B"000000",
      TXCHARISK(1 downto 0) => gtxe2_i_11(1 downto 0),
      TXCOMFINISH => NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED,
      TXCOMINIT => '0',
      TXCOMSAS => '0',
      TXCOMWAKE => '0',
      TXDATA(63 downto 16) => B"000000000000000000000000000000000000000000000000",
      TXDATA(15 downto 0) => Q(15 downto 0),
      TXDEEMPH => '0',
      TXDETECTRX => '0',
      TXDIFFCTRL(3 downto 0) => B"1000",
      TXDIFFPD => '0',
      TXDLYBYPASS => '1',
      TXDLYEN => '0',
      TXDLYHOLD => '0',
      TXDLYOVRDEN => '0',
      TXDLYSRESET => '0',
      TXDLYSRESETDONE => NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED,
      TXDLYUPDOWN => '0',
      TXELECIDLE => TXPD(0),
      TXGEARBOXREADY => NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED,
      TXHEADER(2 downto 0) => B"000",
      TXINHIBIT => '0',
      TXMAINCURSOR(6 downto 0) => B"0000000",
      TXMARGIN(2 downto 0) => B"000",
      TXOUTCLK => txoutclk,
      TXOUTCLKFABRIC => gtxe2_i_n_38,
      TXOUTCLKPCS => gtxe2_i_n_39,
      TXOUTCLKSEL(2 downto 0) => B"100",
      TXPCSRESET => '0',
      TXPD(1) => TXPD(0),
      TXPD(0) => TXPD(0),
      TXPDELECIDLEMODE => '0',
      TXPHALIGN => '0',
      TXPHALIGNDONE => NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED,
      TXPHALIGNEN => '0',
      TXPHDLYPD => '0',
      TXPHDLYRESET => '0',
      TXPHDLYTSTCLK => '0',
      TXPHINIT => '0',
      TXPHINITDONE => NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED,
      TXPHOVRDEN => '0',
      TXPISOPD => '0',
      TXPMARESET => '0',
      TXPOLARITY => '0',
      TXPOSTCURSOR(4 downto 0) => B"00000",
      TXPOSTCURSORINV => '0',
      TXPRBSFORCEERR => '0',
      TXPRBSSEL(2 downto 0) => B"000",
      TXPRECURSOR(4 downto 0) => B"00000",
      TXPRECURSORINV => '0',
      TXQPIBIASEN => '0',
      TXQPISENN => NLW_gtxe2_i_TXQPISENN_UNCONNECTED,
      TXQPISENP => NLW_gtxe2_i_TXQPISENP_UNCONNECTED,
      TXQPISTRONGPDOWN => '0',
      TXQPIWEAKPUP => '0',
      TXRATE(2 downto 0) => B"000",
      TXRATEDONE => NLW_gtxe2_i_TXRATEDONE_UNCONNECTED,
      TXRESETDONE => gtxe2_i_2,
      TXSEQUENCE(6 downto 0) => B"0000000",
      TXSTARTSEQ => '0',
      TXSWING => '0',
      TXSYSCLKSEL(1 downto 0) => B"00",
      TXUSERRDY => gt0_txuserrdy_t,
      TXUSRCLK => gtxe2_i_8,
      TXUSRCLK2 => gtxe2_i_8
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_clocking is
  port (
    gtrefclk_out : out STD_LOGIC;
    gtrefclk_bufg : out STD_LOGIC;
    mmcm_locked : out STD_LOGIC;
    userclk : out STD_LOGIC;
    userclk2 : out STD_LOGIC;
    rxuserclk2_out : out STD_LOGIC;
    gtrefclk_p : in STD_LOGIC;
    gtrefclk_n : in STD_LOGIC;
    txoutclk : in STD_LOGIC;
    mmcm_reset : in STD_LOGIC;
    rxoutclk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_clocking;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_clocking is
  signal clkfbout : STD_LOGIC;
  signal clkout0 : STD_LOGIC;
  signal clkout1 : STD_LOGIC;
  signal \^gtrefclk_out\ : STD_LOGIC;
  signal txoutclk_bufg : STD_LOGIC;
  signal NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_PSDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute box_type : string;
  attribute box_type of bufg_gtrefclk : label is "PRIMITIVE";
  attribute box_type of bufg_txoutclk : label is "PRIMITIVE";
  attribute box_type of bufg_userclk : label is "PRIMITIVE";
  attribute box_type of bufg_userclk2 : label is "PRIMITIVE";
  attribute box_type of ibufds_gtrefclk : label is "PRIMITIVE";
  attribute box_type of mmcm_adv_inst : label is "PRIMITIVE";
  attribute box_type of rxrecclkbufg : label is "PRIMITIVE";
begin
  gtrefclk_out <= \^gtrefclk_out\;
bufg_gtrefclk: unisim.vcomponents.BUFG
     port map (
      I => \^gtrefclk_out\,
      O => gtrefclk_bufg
    );
bufg_txoutclk: unisim.vcomponents.BUFG
     port map (
      I => txoutclk,
      O => txoutclk_bufg
    );
bufg_userclk: unisim.vcomponents.BUFG
     port map (
      I => clkout1,
      O => userclk
    );
bufg_userclk2: unisim.vcomponents.BUFG
     port map (
      I => clkout0,
      O => userclk2
    );
ibufds_gtrefclk: unisim.vcomponents.IBUFDS_GTE2
    generic map(
      CLKCM_CFG => true,
      CLKRCV_TRST => true,
      CLKSWING_CFG => B"11"
    )
        port map (
      CEB => '0',
      I => gtrefclk_p,
      IB => gtrefclk_n,
      O => \^gtrefclk_out\,
      ODIV2 => NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED
    );
mmcm_adv_inst: unisim.vcomponents.MMCME2_ADV
    generic map(
      BANDWIDTH => "OPTIMIZED",
      CLKFBOUT_MULT_F => 16.000000,
      CLKFBOUT_PHASE => 0.000000,
      CLKFBOUT_USE_FINE_PS => false,
      CLKIN1_PERIOD => 16.000000,
      CLKIN2_PERIOD => 0.000000,
      CLKOUT0_DIVIDE_F => 8.000000,
      CLKOUT0_DUTY_CYCLE => 0.500000,
      CLKOUT0_PHASE => 0.000000,
      CLKOUT0_USE_FINE_PS => false,
      CLKOUT1_DIVIDE => 16,
      CLKOUT1_DUTY_CYCLE => 0.500000,
      CLKOUT1_PHASE => 0.000000,
      CLKOUT1_USE_FINE_PS => false,
      CLKOUT2_DIVIDE => 1,
      CLKOUT2_DUTY_CYCLE => 0.500000,
      CLKOUT2_PHASE => 0.000000,
      CLKOUT2_USE_FINE_PS => false,
      CLKOUT3_DIVIDE => 1,
      CLKOUT3_DUTY_CYCLE => 0.500000,
      CLKOUT3_PHASE => 0.000000,
      CLKOUT3_USE_FINE_PS => false,
      CLKOUT4_CASCADE => false,
      CLKOUT4_DIVIDE => 1,
      CLKOUT4_DUTY_CYCLE => 0.500000,
      CLKOUT4_PHASE => 0.000000,
      CLKOUT4_USE_FINE_PS => false,
      CLKOUT5_DIVIDE => 1,
      CLKOUT5_DUTY_CYCLE => 0.500000,
      CLKOUT5_PHASE => 0.000000,
      CLKOUT5_USE_FINE_PS => false,
      CLKOUT6_DIVIDE => 1,
      CLKOUT6_DUTY_CYCLE => 0.500000,
      CLKOUT6_PHASE => 0.000000,
      CLKOUT6_USE_FINE_PS => false,
      COMPENSATION => "INTERNAL",
      DIVCLK_DIVIDE => 1,
      IS_CLKINSEL_INVERTED => '0',
      IS_PSEN_INVERTED => '0',
      IS_PSINCDEC_INVERTED => '0',
      IS_PWRDWN_INVERTED => '0',
      IS_RST_INVERTED => '0',
      REF_JITTER1 => 0.010000,
      REF_JITTER2 => 0.000000,
      SS_EN => "FALSE",
      SS_MODE => "CENTER_HIGH",
      SS_MOD_PERIOD => 10000,
      STARTUP_WAIT => false
    )
        port map (
      CLKFBIN => clkfbout,
      CLKFBOUT => clkfbout,
      CLKFBOUTB => NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED,
      CLKFBSTOPPED => NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED,
      CLKIN1 => txoutclk_bufg,
      CLKIN2 => '0',
      CLKINSEL => '1',
      CLKINSTOPPED => NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED,
      CLKOUT0 => clkout0,
      CLKOUT0B => NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED,
      CLKOUT1 => clkout1,
      CLKOUT1B => NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED,
      CLKOUT2 => NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED,
      CLKOUT2B => NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED,
      CLKOUT3 => NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED,
      CLKOUT3B => NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED,
      CLKOUT4 => NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED,
      CLKOUT5 => NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED,
      CLKOUT6 => NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED,
      DADDR(6 downto 0) => B"0000000",
      DCLK => '0',
      DEN => '0',
      DI(15 downto 0) => B"0000000000000000",
      DO(15 downto 0) => NLW_mmcm_adv_inst_DO_UNCONNECTED(15 downto 0),
      DRDY => NLW_mmcm_adv_inst_DRDY_UNCONNECTED,
      DWE => '0',
      LOCKED => mmcm_locked,
      PSCLK => '0',
      PSDONE => NLW_mmcm_adv_inst_PSDONE_UNCONNECTED,
      PSEN => '0',
      PSINCDEC => '0',
      PWRDWN => '0',
      RST => mmcm_reset
    );
rxrecclkbufg: unisim.vcomponents.BUFG
     port map (
      I => rxoutclk,
      O => rxuserclk2_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_cpll_railing is
  port (
    cpll_pd0_i : out STD_LOGIC;
    cpllreset_in : out STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gt0_cpllreset_t : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_cpll_railing;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_cpll_railing is
  signal cpll_reset_out : STD_LOGIC;
  signal \cpllpd_wait_reg[31]_srl32_n_1\ : STD_LOGIC;
  signal \cpllpd_wait_reg[63]_srl32_n_1\ : STD_LOGIC;
  signal \cpllpd_wait_reg[94]_srl31_n_0\ : STD_LOGIC;
  signal \cpllreset_wait_reg[126]_srl31_n_0\ : STD_LOGIC;
  signal \cpllreset_wait_reg[31]_srl32_n_1\ : STD_LOGIC;
  signal \cpllreset_wait_reg[63]_srl32_n_1\ : STD_LOGIC;
  signal \cpllreset_wait_reg[95]_srl32_n_1\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  attribute srl_bus_name : string;
  attribute srl_bus_name of \cpllpd_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name : string;
  attribute srl_name of \cpllpd_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[31]_srl32 ";
  attribute srl_bus_name of \cpllpd_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name of \cpllpd_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[63]_srl32 ";
  attribute srl_bus_name of \cpllpd_wait_reg[94]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name of \cpllpd_wait_reg[94]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[94]_srl31 ";
  attribute equivalent_register_removal : string;
  attribute equivalent_register_removal of \cpllpd_wait_reg[95]\ : label is "no";
  attribute srl_bus_name of \cpllreset_wait_reg[126]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[126]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[126]_srl31 ";
  attribute equivalent_register_removal of \cpllreset_wait_reg[127]\ : label is "no";
  attribute srl_bus_name of \cpllreset_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[31]_srl32 ";
  attribute srl_bus_name of \cpllreset_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[63]_srl32 ";
  attribute srl_bus_name of \cpllreset_wait_reg[95]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[95]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[95]_srl32 ";
begin
\cpllpd_wait_reg[31]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"FFFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => '0',
      Q => \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllpd_wait_reg[31]_srl32_n_1\
    );
\cpllpd_wait_reg[63]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"FFFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllpd_wait_reg[31]_srl32_n_1\,
      Q => \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllpd_wait_reg[63]_srl32_n_1\
    );
\cpllpd_wait_reg[94]_srl31\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11110",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllpd_wait_reg[63]_srl32_n_1\,
      Q => \cpllpd_wait_reg[94]_srl31_n_0\,
      Q31 => \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED\
    );
\cpllpd_wait_reg[95]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => gtrefclk_bufg,
      CE => '1',
      D => \cpllpd_wait_reg[94]_srl31_n_0\,
      Q => cpll_pd0_i,
      R => '0'
    );
\cpllreset_wait_reg[126]_srl31\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11110",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllreset_wait_reg[95]_srl32_n_1\,
      Q => \cpllreset_wait_reg[126]_srl31_n_0\,
      Q31 => \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED\
    );
\cpllreset_wait_reg[127]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gtrefclk_bufg,
      CE => '1',
      D => \cpllreset_wait_reg[126]_srl31_n_0\,
      Q => cpll_reset_out,
      R => '0'
    );
\cpllreset_wait_reg[31]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"000000FF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => '0',
      Q => \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[31]_srl32_n_1\
    );
\cpllreset_wait_reg[63]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllreset_wait_reg[31]_srl32_n_1\,
      Q => \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[63]_srl32_n_1\
    );
\cpllreset_wait_reg[95]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllreset_wait_reg[63]_srl32_n_1\,
      Q => \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[95]_srl32_n_1\
    );
gtxe2_i_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => cpll_reset_out,
      I1 => gt0_cpllreset_t,
      O => cpllreset_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_gt_common is
  port (
    gt0_qplloutclk_out : out STD_LOGIC;
    gt0_qplloutrefclk_out : out STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_gt_common;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_gt_common is
  signal gtxe2_common_i_n_2 : STD_LOGIC;
  signal gtxe2_common_i_n_5 : STD_LOGIC;
  signal NLW_gtxe2_common_i_DRPRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_common_i_DRPDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute box_type : string;
  attribute box_type of gtxe2_common_i : label is "PRIMITIVE";
begin
gtxe2_common_i: unisim.vcomponents.GTXE2_COMMON
    generic map(
      BIAS_CFG => X"0000040000001000",
      COMMON_CFG => X"00000000",
      IS_DRPCLK_INVERTED => '0',
      IS_GTGREFCLK_INVERTED => '0',
      IS_QPLLLOCKDETCLK_INVERTED => '0',
      QPLL_CFG => X"06801C1",
      QPLL_CLKOUT_CFG => B"0000",
      QPLL_COARSE_FREQ_OVRD => B"010000",
      QPLL_COARSE_FREQ_OVRD_EN => '0',
      QPLL_CP => B"0000011111",
      QPLL_CP_MONITOR_EN => '0',
      QPLL_DMONITOR_SEL => '0',
      QPLL_FBDIV => B"0000100000",
      QPLL_FBDIV_MONITOR_EN => '0',
      QPLL_FBDIV_RATIO => '1',
      QPLL_INIT_CFG => X"000006",
      QPLL_LOCK_CFG => X"21E8",
      QPLL_LPF => B"1111",
      QPLL_REFCLK_DIV => 1,
      SIM_QPLLREFCLK_SEL => B"001",
      SIM_RESET_SPEEDUP => "FALSE",
      SIM_VERSION => "4.0"
    )
        port map (
      BGBYPASSB => '1',
      BGMONITORENB => '1',
      BGPDB => '1',
      BGRCALOVRD(4 downto 0) => B"11111",
      DRPADDR(7 downto 0) => B"00000000",
      DRPCLK => '0',
      DRPDI(15 downto 0) => B"0000000000000000",
      DRPDO(15 downto 0) => NLW_gtxe2_common_i_DRPDO_UNCONNECTED(15 downto 0),
      DRPEN => '0',
      DRPRDY => NLW_gtxe2_common_i_DRPRDY_UNCONNECTED,
      DRPWE => '0',
      GTGREFCLK => '0',
      GTNORTHREFCLK0 => '0',
      GTNORTHREFCLK1 => '0',
      GTREFCLK0 => gtrefclk_out,
      GTREFCLK1 => '0',
      GTSOUTHREFCLK0 => '0',
      GTSOUTHREFCLK1 => '0',
      PMARSVD(7 downto 0) => B"00000000",
      QPLLDMONITOR(7 downto 0) => NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED(7 downto 0),
      QPLLFBCLKLOST => NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED,
      QPLLLOCK => gtxe2_common_i_n_2,
      QPLLLOCKDETCLK => independent_clock_bufg,
      QPLLLOCKEN => '1',
      QPLLOUTCLK => gt0_qplloutclk_out,
      QPLLOUTREFCLK => gt0_qplloutrefclk_out,
      QPLLOUTRESET => '0',
      QPLLPD => '1',
      QPLLREFCLKLOST => gtxe2_common_i_n_5,
      QPLLREFCLKSEL(2 downto 0) => B"001",
      QPLLRESET => \out\(0),
      QPLLRSVD1(15 downto 0) => B"0000000000000000",
      QPLLRSVD2(4 downto 0) => B"11111",
      RCALENB => '1',
      REFCLKOUTMONITOR => NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync is
  port (
    reset_out : out STD_LOGIC;
    CLK : in STD_LOGIC;
    enablealign : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync is
  signal reset_sync_reg1 : STD_LOGIC;
  signal reset_sync_reg2 : STD_LOGIC;
  signal reset_sync_reg3 : STD_LOGIC;
  signal reset_sync_reg4 : STD_LOGIC;
  signal reset_sync_reg5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of reset_sync1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of reset_sync1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of reset_sync1 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of reset_sync1 : label is "VCC:CE";
  attribute box_type : string;
  attribute box_type of reset_sync1 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync2 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync2 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync2 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync2 : label is "VCC:CE";
  attribute box_type of reset_sync2 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync3 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync3 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync3 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync3 : label is "VCC:CE";
  attribute box_type of reset_sync3 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync4 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync4 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync4 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync4 : label is "VCC:CE";
  attribute box_type of reset_sync4 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync5 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync5 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync5 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync5 : label is "VCC:CE";
  attribute box_type of reset_sync5 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync6 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync6 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync6 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync6 : label is "VCC:CE";
  attribute box_type of reset_sync6 : label is "PRIMITIVE";
begin
reset_sync1: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => '0',
      PRE => enablealign,
      Q => reset_sync_reg1
    );
reset_sync2: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg1,
      PRE => enablealign,
      Q => reset_sync_reg2
    );
reset_sync3: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg2,
      PRE => enablealign,
      Q => reset_sync_reg3
    );
reset_sync4: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg3,
      PRE => enablealign,
      Q => reset_sync_reg4
    );
reset_sync5: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg4,
      PRE => enablealign,
      Q => reset_sync_reg5
    );
reset_sync6: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg5,
      PRE => '0',
      Q => reset_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_1 is
  port (
    reset_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_1 : entity is "gig_ethernet_pcs_pma_0_reset_sync";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_1;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_1 is
  signal reset_sync_reg1 : STD_LOGIC;
  signal reset_sync_reg2 : STD_LOGIC;
  signal reset_sync_reg3 : STD_LOGIC;
  signal reset_sync_reg4 : STD_LOGIC;
  signal reset_sync_reg5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of reset_sync1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of reset_sync1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of reset_sync1 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of reset_sync1 : label is "VCC:CE";
  attribute box_type : string;
  attribute box_type of reset_sync1 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync2 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync2 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync2 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync2 : label is "VCC:CE";
  attribute box_type of reset_sync2 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync3 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync3 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync3 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync3 : label is "VCC:CE";
  attribute box_type of reset_sync3 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync4 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync4 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync4 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync4 : label is "VCC:CE";
  attribute box_type of reset_sync4 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync5 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync5 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync5 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync5 : label is "VCC:CE";
  attribute box_type of reset_sync5 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync6 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync6 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync6 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync6 : label is "VCC:CE";
  attribute box_type of reset_sync6 : label is "PRIMITIVE";
begin
reset_sync1: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => '0',
      PRE => SR(0),
      Q => reset_sync_reg1
    );
reset_sync2: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg1,
      PRE => SR(0),
      Q => reset_sync_reg2
    );
reset_sync3: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg2,
      PRE => SR(0),
      Q => reset_sync_reg3
    );
reset_sync4: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg3,
      PRE => SR(0),
      Q => reset_sync_reg4
    );
reset_sync5: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg4,
      PRE => SR(0),
      Q => reset_sync_reg5
    );
reset_sync6: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg5,
      PRE => '0',
      Q => reset_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_2 is
  port (
    reset_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    reset_sync5_0 : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_2 : entity is "gig_ethernet_pcs_pma_0_reset_sync";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_2;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_2 is
  signal reset_sync_reg1 : STD_LOGIC;
  signal reset_sync_reg2 : STD_LOGIC;
  signal reset_sync_reg3 : STD_LOGIC;
  signal reset_sync_reg4 : STD_LOGIC;
  signal reset_sync_reg5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of reset_sync1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of reset_sync1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of reset_sync1 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of reset_sync1 : label is "VCC:CE";
  attribute box_type : string;
  attribute box_type of reset_sync1 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync2 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync2 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync2 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync2 : label is "VCC:CE";
  attribute box_type of reset_sync2 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync3 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync3 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync3 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync3 : label is "VCC:CE";
  attribute box_type of reset_sync3 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync4 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync4 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync4 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync4 : label is "VCC:CE";
  attribute box_type of reset_sync4 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync5 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync5 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync5 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync5 : label is "VCC:CE";
  attribute box_type of reset_sync5 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync6 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync6 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync6 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync6 : label is "VCC:CE";
  attribute box_type of reset_sync6 : label is "PRIMITIVE";
begin
reset_sync1: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => '0',
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg1
    );
reset_sync2: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg1,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg2
    );
reset_sync3: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg2,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg3
    );
reset_sync4: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg3,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg4
    );
reset_sync5: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg4,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg5
    );
reset_sync6: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg5,
      PRE => '0',
      Q => reset_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_wtd_timer is
  port (
    reset : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    data_out : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_wtd_timer;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_wtd_timer is
  signal \counter_stg1[5]_i_1_n_0\ : STD_LOGIC;
  signal \counter_stg1[5]_i_3_n_0\ : STD_LOGIC;
  signal counter_stg1_reg : STD_LOGIC_VECTOR ( 5 to 5 );
  signal \counter_stg1_reg__0\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \counter_stg2[0]_i_3_n_0\ : STD_LOGIC;
  signal counter_stg2_reg : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal \counter_stg2_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal counter_stg30 : STD_LOGIC;
  signal \counter_stg3[0]_i_3_n_0\ : STD_LOGIC;
  signal \counter_stg3[0]_i_4_n_0\ : STD_LOGIC;
  signal \counter_stg3[0]_i_5_n_0\ : STD_LOGIC;
  signal counter_stg3_reg : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal \counter_stg3_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal eqOp : STD_LOGIC;
  signal plusOp : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal reset0 : STD_LOGIC;
  signal reset_i_2_n_0 : STD_LOGIC;
  signal reset_i_3_n_0 : STD_LOGIC;
  signal reset_i_4_n_0 : STD_LOGIC;
  signal reset_i_5_n_0 : STD_LOGIC;
  signal reset_i_6_n_0 : STD_LOGIC;
  signal \NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \counter_stg1[0]_i_1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \counter_stg1[1]_i_1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \counter_stg1[2]_i_1\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \counter_stg1[3]_i_1\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \counter_stg1[4]_i_1\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \counter_stg1[5]_i_3\ : label is "soft_lutpair71";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \counter_stg2_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg2_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg2_reg[8]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg3_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg3_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg3_reg[8]_i_1\ : label is 11;
begin
\counter_stg1[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \counter_stg1_reg__0\(0),
      O => plusOp(0)
    );
\counter_stg1[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \counter_stg1_reg__0\(0),
      I1 => \counter_stg1_reg__0\(1),
      O => plusOp(1)
    );
\counter_stg1[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \counter_stg1_reg__0\(1),
      I1 => \counter_stg1_reg__0\(0),
      I2 => \counter_stg1_reg__0\(2),
      O => plusOp(2)
    );
\counter_stg1[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \counter_stg1_reg__0\(2),
      I1 => \counter_stg1_reg__0\(0),
      I2 => \counter_stg1_reg__0\(1),
      I3 => \counter_stg1_reg__0\(3),
      O => plusOp(3)
    );
\counter_stg1[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(3),
      I1 => \counter_stg1_reg__0\(1),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(2),
      I4 => \counter_stg1_reg__0\(4),
      O => plusOp(4)
    );
\counter_stg1[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF2000"
    )
        port map (
      I0 => reset_i_2_n_0,
      I1 => counter_stg3_reg(0),
      I2 => reset_i_3_n_0,
      I3 => \counter_stg1[5]_i_3_n_0\,
      I4 => data_out,
      O => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(4),
      I1 => \counter_stg1_reg__0\(2),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(1),
      I4 => \counter_stg1_reg__0\(3),
      I5 => counter_stg1_reg(5),
      O => plusOp(5)
    );
\counter_stg1[5]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(3),
      I1 => \counter_stg1_reg__0\(1),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(2),
      I4 => \counter_stg1_reg__0\(4),
      O => \counter_stg1[5]_i_3_n_0\
    );
\counter_stg1_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(0),
      Q => \counter_stg1_reg__0\(0),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(1),
      Q => \counter_stg1_reg__0\(1),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(2),
      Q => \counter_stg1_reg__0\(2),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(3),
      Q => \counter_stg1_reg__0\(3),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(4),
      Q => \counter_stg1_reg__0\(4),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(5),
      Q => counter_stg1_reg(5),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(4),
      I1 => \counter_stg1_reg__0\(2),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(1),
      I4 => \counter_stg1_reg__0\(3),
      I5 => counter_stg1_reg(5),
      O => eqOp
    );
\counter_stg2[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => counter_stg2_reg(0),
      O => \counter_stg2[0]_i_3_n_0\
    );
\counter_stg2_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_7\,
      Q => counter_stg2_reg(0),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \counter_stg2_reg[0]_i_2_n_0\,
      CO(2) => \counter_stg2_reg[0]_i_2_n_1\,
      CO(1) => \counter_stg2_reg[0]_i_2_n_2\,
      CO(0) => \counter_stg2_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \counter_stg2_reg[0]_i_2_n_4\,
      O(2) => \counter_stg2_reg[0]_i_2_n_5\,
      O(1) => \counter_stg2_reg[0]_i_2_n_6\,
      O(0) => \counter_stg2_reg[0]_i_2_n_7\,
      S(3 downto 1) => counter_stg2_reg(3 downto 1),
      S(0) => \counter_stg2[0]_i_3_n_0\
    );
\counter_stg2_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_5\,
      Q => counter_stg2_reg(10),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_4\,
      Q => counter_stg2_reg(11),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_6\,
      Q => counter_stg2_reg(1),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_5\,
      Q => counter_stg2_reg(2),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_4\,
      Q => counter_stg2_reg(3),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_7\,
      Q => counter_stg2_reg(4),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg2_reg[0]_i_2_n_0\,
      CO(3) => \counter_stg2_reg[4]_i_1_n_0\,
      CO(2) => \counter_stg2_reg[4]_i_1_n_1\,
      CO(1) => \counter_stg2_reg[4]_i_1_n_2\,
      CO(0) => \counter_stg2_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg2_reg[4]_i_1_n_4\,
      O(2) => \counter_stg2_reg[4]_i_1_n_5\,
      O(1) => \counter_stg2_reg[4]_i_1_n_6\,
      O(0) => \counter_stg2_reg[4]_i_1_n_7\,
      S(3 downto 0) => counter_stg2_reg(7 downto 4)
    );
\counter_stg2_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_6\,
      Q => counter_stg2_reg(5),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_5\,
      Q => counter_stg2_reg(6),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_4\,
      Q => counter_stg2_reg(7),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_7\,
      Q => counter_stg2_reg(8),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg2_reg[4]_i_1_n_0\,
      CO(3) => \NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \counter_stg2_reg[8]_i_1_n_1\,
      CO(1) => \counter_stg2_reg[8]_i_1_n_2\,
      CO(0) => \counter_stg2_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg2_reg[8]_i_1_n_4\,
      O(2) => \counter_stg2_reg[8]_i_1_n_5\,
      O(1) => \counter_stg2_reg[8]_i_1_n_6\,
      O(0) => \counter_stg2_reg[8]_i_1_n_7\,
      S(3 downto 0) => counter_stg2_reg(11 downto 8)
    );
\counter_stg2_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_6\,
      Q => counter_stg2_reg(9),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \counter_stg3[0]_i_3_n_0\,
      I1 => \counter_stg3[0]_i_4_n_0\,
      I2 => counter_stg2_reg(0),
      I3 => \counter_stg1[5]_i_3_n_0\,
      O => counter_stg30
    );
\counter_stg3[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => counter_stg2_reg(3),
      I1 => counter_stg2_reg(4),
      I2 => counter_stg2_reg(1),
      I3 => counter_stg2_reg(2),
      I4 => counter_stg2_reg(6),
      I5 => counter_stg2_reg(5),
      O => \counter_stg3[0]_i_3_n_0\
    );
\counter_stg3[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => counter_stg2_reg(9),
      I1 => counter_stg2_reg(10),
      I2 => counter_stg2_reg(7),
      I3 => counter_stg2_reg(8),
      I4 => counter_stg1_reg(5),
      I5 => counter_stg2_reg(11),
      O => \counter_stg3[0]_i_4_n_0\
    );
\counter_stg3[0]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => counter_stg3_reg(0),
      O => \counter_stg3[0]_i_5_n_0\
    );
\counter_stg3_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_7\,
      Q => counter_stg3_reg(0),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \counter_stg3_reg[0]_i_2_n_0\,
      CO(2) => \counter_stg3_reg[0]_i_2_n_1\,
      CO(1) => \counter_stg3_reg[0]_i_2_n_2\,
      CO(0) => \counter_stg3_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \counter_stg3_reg[0]_i_2_n_4\,
      O(2) => \counter_stg3_reg[0]_i_2_n_5\,
      O(1) => \counter_stg3_reg[0]_i_2_n_6\,
      O(0) => \counter_stg3_reg[0]_i_2_n_7\,
      S(3 downto 1) => counter_stg3_reg(3 downto 1),
      S(0) => \counter_stg3[0]_i_5_n_0\
    );
\counter_stg3_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_5\,
      Q => counter_stg3_reg(10),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_4\,
      Q => counter_stg3_reg(11),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_6\,
      Q => counter_stg3_reg(1),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_5\,
      Q => counter_stg3_reg(2),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_4\,
      Q => counter_stg3_reg(3),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_7\,
      Q => counter_stg3_reg(4),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg3_reg[0]_i_2_n_0\,
      CO(3) => \counter_stg3_reg[4]_i_1_n_0\,
      CO(2) => \counter_stg3_reg[4]_i_1_n_1\,
      CO(1) => \counter_stg3_reg[4]_i_1_n_2\,
      CO(0) => \counter_stg3_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg3_reg[4]_i_1_n_4\,
      O(2) => \counter_stg3_reg[4]_i_1_n_5\,
      O(1) => \counter_stg3_reg[4]_i_1_n_6\,
      O(0) => \counter_stg3_reg[4]_i_1_n_7\,
      S(3 downto 0) => counter_stg3_reg(7 downto 4)
    );
\counter_stg3_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_6\,
      Q => counter_stg3_reg(5),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_5\,
      Q => counter_stg3_reg(6),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_4\,
      Q => counter_stg3_reg(7),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_7\,
      Q => counter_stg3_reg(8),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg3_reg[4]_i_1_n_0\,
      CO(3) => \NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \counter_stg3_reg[8]_i_1_n_1\,
      CO(1) => \counter_stg3_reg[8]_i_1_n_2\,
      CO(0) => \counter_stg3_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg3_reg[8]_i_1_n_4\,
      O(2) => \counter_stg3_reg[8]_i_1_n_5\,
      O(1) => \counter_stg3_reg[8]_i_1_n_6\,
      O(0) => \counter_stg3_reg[8]_i_1_n_7\,
      S(3 downto 0) => counter_stg3_reg(11 downto 8)
    );
\counter_stg3_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_6\,
      Q => counter_stg3_reg(9),
      R => \counter_stg1[5]_i_1_n_0\
    );
reset_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => reset_i_2_n_0,
      I1 => counter_stg3_reg(0),
      I2 => reset_i_3_n_0,
      O => reset0
    );
reset_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000001000000000"
    )
        port map (
      I0 => counter_stg3_reg(9),
      I1 => counter_stg3_reg(10),
      I2 => counter_stg3_reg(7),
      I3 => counter_stg3_reg(8),
      I4 => counter_stg2_reg(0),
      I5 => counter_stg3_reg(11),
      O => reset_i_2_n_0
    );
reset_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => reset_i_4_n_0,
      I1 => reset_i_5_n_0,
      I2 => reset_i_6_n_0,
      O => reset_i_3_n_0
    );
reset_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000008"
    )
        port map (
      I0 => counter_stg2_reg(3),
      I1 => counter_stg2_reg(4),
      I2 => counter_stg2_reg(1),
      I3 => counter_stg2_reg(2),
      I4 => counter_stg2_reg(6),
      I5 => counter_stg2_reg(5),
      O => reset_i_4_n_0
    );
reset_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0020000000000000"
    )
        port map (
      I0 => counter_stg2_reg(10),
      I1 => counter_stg2_reg(9),
      I2 => counter_stg2_reg(8),
      I3 => counter_stg2_reg(7),
      I4 => counter_stg1_reg(5),
      I5 => counter_stg2_reg(11),
      O => reset_i_5_n_0
    );
reset_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0002000000000000"
    )
        port map (
      I0 => counter_stg3_reg(4),
      I1 => counter_stg3_reg(3),
      I2 => counter_stg3_reg(1),
      I3 => counter_stg3_reg(2),
      I4 => counter_stg3_reg(6),
      I5 => counter_stg3_reg(5),
      O => reset_i_6_n_0
    );
reset_reg: unisim.vcomponents.FDRE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset0,
      Q => reset,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_resets is
  port (
    \out\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    independent_clock_bufg : in STD_LOGIC;
    reset : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_resets;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_resets is
  signal pma_reset_pipe : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg : string;
  attribute async_reg of pma_reset_pipe : signal is "true";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \pma_reset_pipe_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[1]\ : label is std.standard.true;
  attribute KEEP of \pma_reset_pipe_reg[1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[2]\ : label is std.standard.true;
  attribute KEEP of \pma_reset_pipe_reg[2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[3]\ : label is std.standard.true;
  attribute KEEP of \pma_reset_pipe_reg[3]\ : label is "yes";
begin
  \out\(0) <= pma_reset_pipe(3);
\pma_reset_pipe_reg[0]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => '0',
      PRE => reset,
      Q => pma_reset_pipe(0)
    );
\pma_reset_pipe_reg[1]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pma_reset_pipe(0),
      PRE => reset,
      Q => pma_reset_pipe(1)
    );
\pma_reset_pipe_reg[2]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pma_reset_pipe(1),
      PRE => reset,
      Q => pma_reset_pipe(2)
    );
\pma_reset_pipe_reg[3]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pma_reset_pipe(2),
      PRE => reset,
      Q => pma_reset_pipe(3)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    CLK : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_0 is
  port (
    resetdone : out STD_LOGIC;
    resetdone_0 : in STD_LOGIC;
    data_in : in STD_LOGIC;
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_0 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_0 is
  signal data_out : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
resetdone_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => data_out,
      I1 => resetdone_0,
      O => resetdone
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_10 is
  port (
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_10 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_10;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_10 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_11 is
  port (
    \FSM_sequential_rx_state_reg[1]\ : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 2 downto 0 );
    rxresetdone_s3 : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_11 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_11;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_11 is
  signal cplllock_sync : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
\FSM_sequential_rx_state[3]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"008F0080"
    )
        port map (
      I0 => Q(0),
      I1 => rxresetdone_s3,
      I2 => Q(1),
      I3 => Q(2),
      I4 => cplllock_sync,
      O => \FSM_sequential_rx_state_reg[1]\
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => cplllock_sync,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_12 is
  port (
    \FSM_sequential_rx_state_reg[1]\ : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 2 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    reset_time_out_reg : in STD_LOGIC;
    reset_time_out_reg_0 : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    reset_time_out_reg_1 : in STD_LOGIC;
    reset_time_out_reg_2 : in STD_LOGIC;
    data_in : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[1]_0\ : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_0 : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_1 : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]\ : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_0\ : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_1\ : in STD_LOGIC;
    mmcm_lock_reclocked : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_2\ : in STD_LOGIC;
    time_out_wait_bypass_s3 : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[3]\ : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_3\ : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_2 : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_3 : in STD_LOGIC;
    data_out : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_12 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_12;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_12 is
  signal \FSM_sequential_rx_state[0]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[1]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_8_n_0\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  signal data_valid_sync : STD_LOGIC;
  signal reset_time_out_i_2_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_3_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_4_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[0]_i_3\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[3]_i_8\ : label is "soft_lutpair41";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
\FSM_sequential_rx_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEFEFEF"
    )
        port map (
      I0 => \FSM_sequential_rx_state_reg[0]_2\,
      I1 => \FSM_sequential_rx_state[0]_i_3_n_0\,
      I2 => Q(0),
      I3 => Q(1),
      I4 => Q(3),
      O => D(0)
    );
\FSM_sequential_rx_state[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => Q(3),
      I1 => reset_time_out_reg_2,
      I2 => data_valid_sync,
      I3 => rx_fsm_reset_done_int_reg_1,
      O => \FSM_sequential_rx_state[0]_i_3_n_0\
    );
\FSM_sequential_rx_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF24200400"
    )
        port map (
      I0 => Q(0),
      I1 => Q(1),
      I2 => Q(3),
      I3 => Q(2),
      I4 => \FSM_sequential_rx_state[1]_i_2_n_0\,
      I5 => \FSM_sequential_rx_state_reg[1]_0\,
      O => D(1)
    );
\FSM_sequential_rx_state[1]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => data_valid_sync,
      I1 => rx_fsm_reset_done_int_reg_1,
      O => \FSM_sequential_rx_state[1]_i_2_n_0\
    );
\FSM_sequential_rx_state[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFEEE"
    )
        port map (
      I0 => \FSM_sequential_rx_state_reg[0]\,
      I1 => \FSM_sequential_rx_state[3]_i_4_n_0\,
      I2 => Q(0),
      I3 => reset_time_out_reg,
      I4 => \FSM_sequential_rx_state[3]_i_6_n_0\,
      I5 => \FSM_sequential_rx_state_reg[0]_0\,
      O => E(0)
    );
\FSM_sequential_rx_state[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFCCC0C4C4"
    )
        port map (
      I0 => time_out_wait_bypass_s3,
      I1 => Q(3),
      I2 => Q(1),
      I3 => \FSM_sequential_rx_state[3]_i_8_n_0\,
      I4 => Q(0),
      I5 => \FSM_sequential_rx_state_reg[3]\,
      O => D(2)
    );
\FSM_sequential_rx_state[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAEFEA"
    )
        port map (
      I0 => \FSM_sequential_rx_state[0]_i_3_n_0\,
      I1 => \FSM_sequential_rx_state_reg[0]_1\,
      I2 => Q(2),
      I3 => \FSM_sequential_rx_state_reg[0]_3\,
      I4 => Q(0),
      I5 => Q(1),
      O => \FSM_sequential_rx_state[3]_i_4_n_0\
    );
\FSM_sequential_rx_state[3]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0CE20CCC"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => Q(3),
      I2 => data_valid_sync,
      I3 => Q(1),
      I4 => Q(0),
      O => \FSM_sequential_rx_state[3]_i_6_n_0\
    );
\FSM_sequential_rx_state[3]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => rx_fsm_reset_done_int_reg_1,
      I1 => data_valid_sync,
      I2 => reset_time_out_reg_2,
      O => \FSM_sequential_rx_state[3]_i_8_n_0\
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_out,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_valid_sync,
      R => '0'
    );
\reset_time_out_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEFFFFFEEEF0000"
    )
        port map (
      I0 => reset_time_out_i_2_n_0,
      I1 => reset_time_out_reg,
      I2 => reset_time_out_reg_0,
      I3 => Q(1),
      I4 => reset_time_out_reg_1,
      I5 => reset_time_out_reg_2,
      O => \FSM_sequential_rx_state_reg[1]\
    );
reset_time_out_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0FF30E0E0FF30202"
    )
        port map (
      I0 => \FSM_sequential_rx_state_reg[0]_1\,
      I1 => Q(0),
      I2 => Q(1),
      I3 => data_valid_sync,
      I4 => Q(3),
      I5 => mmcm_lock_reclocked,
      O => reset_time_out_i_2_n_0
    );
rx_fsm_reset_done_int_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"ABA8"
    )
        port map (
      I0 => rx_fsm_reset_done_int,
      I1 => rx_fsm_reset_done_int_i_3_n_0,
      I2 => rx_fsm_reset_done_int_i_4_n_0,
      I3 => data_in,
      O => rx_fsm_reset_done_int_reg
    );
rx_fsm_reset_done_int_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00040000"
    )
        port map (
      I0 => Q(0),
      I1 => data_valid_sync,
      I2 => Q(2),
      I3 => reset_time_out_reg_2,
      I4 => rx_fsm_reset_done_int_reg_2,
      O => rx_fsm_reset_done_int
    );
rx_fsm_reset_done_int_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0400040004040400"
    )
        port map (
      I0 => rx_fsm_reset_done_int_reg_0,
      I1 => Q(3),
      I2 => Q(2),
      I3 => data_valid_sync,
      I4 => rx_fsm_reset_done_int_reg_1,
      I5 => reset_time_out_reg_2,
      O => rx_fsm_reset_done_int_i_3_n_0
    );
rx_fsm_reset_done_int_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0008000808080008"
    )
        port map (
      I0 => rx_fsm_reset_done_int_reg_3,
      I1 => Q(1),
      I2 => Q(0),
      I3 => data_valid_sync,
      I4 => rx_fsm_reset_done_int_reg_2,
      I5 => reset_time_out_reg_2,
      O => rx_fsm_reset_done_int_i_4_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_13 is
  port (
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_13 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_13;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_13 is
  signal \^data_out\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
  data_out <= \^data_out\;
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => \^data_out\,
      R => '0'
    );
\mmcm_lock_count[7]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^data_out\,
      O => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_14 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_14 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_14;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_14 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_15 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_15 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_15;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_15 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_16 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg6_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_16 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_16;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_16 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_3 is
  port (
    data_out : out STD_LOGIC;
    status_vector : in STD_LOGIC_VECTOR ( 0 to 0 );
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_3 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_3;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_3 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => status_vector(0),
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_4 is
  port (
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_4 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_4;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_4 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_5 is
  port (
    reset_time_out_reg : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    reset_time_out_reg_0 : in STD_LOGIC;
    reset_time_out : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_0\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_1\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_2\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_3\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    reset_time_out_reg_1 : in STD_LOGIC;
    mmcm_lock_reclocked : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_4\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_5\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_6\ : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_5 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_5;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_5 is
  signal \FSM_sequential_tx_state[3]_i_5_n_0\ : STD_LOGIC;
  signal cplllock_sync : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  signal \reset_time_out_i_3__0_n_0\ : STD_LOGIC;
  signal \reset_time_out_i_4__0_n_0\ : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
\FSM_sequential_tx_state[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFEFFFE"
    )
        port map (
      I0 => \FSM_sequential_tx_state_reg[0]\,
      I1 => \FSM_sequential_tx_state_reg[0]_0\,
      I2 => \FSM_sequential_tx_state[3]_i_5_n_0\,
      I3 => \FSM_sequential_tx_state_reg[0]_1\,
      I4 => \FSM_sequential_tx_state_reg[0]_2\,
      I5 => \FSM_sequential_tx_state_reg[0]_3\,
      O => E(0)
    );
\FSM_sequential_tx_state[3]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000F00008"
    )
        port map (
      I0 => \FSM_sequential_tx_state_reg[0]_4\,
      I1 => \FSM_sequential_tx_state_reg[0]_5\,
      I2 => cplllock_sync,
      I3 => Q(2),
      I4 => Q(1),
      I5 => \FSM_sequential_tx_state_reg[0]_6\,
      O => \FSM_sequential_tx_state[3]_i_5_n_0\
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => cplllock_sync,
      R => '0'
    );
reset_time_out_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => reset_time_out_reg_0,
      I1 => \reset_time_out_i_3__0_n_0\,
      I2 => \reset_time_out_i_4__0_n_0\,
      I3 => reset_time_out,
      O => reset_time_out_reg
    );
\reset_time_out_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"020002000F000200"
    )
        port map (
      I0 => cplllock_sync,
      I1 => Q(2),
      I2 => Q(3),
      I3 => Q(0),
      I4 => mmcm_lock_reclocked,
      I5 => Q(1),
      O => \reset_time_out_i_3__0_n_0\
    );
\reset_time_out_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0505FF040505F504"
    )
        port map (
      I0 => Q(1),
      I1 => reset_time_out_reg_1,
      I2 => Q(2),
      I3 => Q(0),
      I4 => Q(3),
      I5 => cplllock_sync,
      O => \reset_time_out_i_4__0_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_6 is
  port (
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_6 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_6;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_6 is
  signal \^data_out\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
  data_out <= \^data_out\;
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => \^data_out\,
      R => '0'
    );
\mmcm_lock_count[7]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^data_out\,
      O => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_7 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg6_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_7 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_7;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_7 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_8 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_8 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_8;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_8 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_9 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_9 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_9;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_9 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2023.2"
`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
TCSZ2Ye5IRfHavlF8Mno1PI9xixWuSiNh3ssU1FQtkjW1fmNtc2c3x12slL242UQayI0rzZTqe6S
edtecLHTOnzxXpCZjjU8NFmgLPerTSDZ1W5YhyIi9j0Ap4YBpvaA1ojM0+r0Cx+dMOXohQGeyljq
+fnTaFTUe2678DxpqHk=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
NIzZVKMB1/8UX2qb4BB+VXCafEKXsrn7axB1cJDPqDCzSyt/2KG1NEEZTDHZVzIr8Bf9501PyXmL
VowTAAXX/RopKyKOM1xJN/qLtqXxegH2a4dIkUxDIIclIcbv/smna9VCwI7m6JhrnKsNciTTilgR
27S/h6JPpZsZAEmsNxxTC70WQhQSM8TlHJjZg3KDc5KTnvC/mVTk6I05U6x0Bdd1YR9GBvhwRqhP
B1ukL/1JVOwR9Ce9p+EHFE/xyApypCjQPGwq+8IFQgS8wltVZHX6eSMw17Q0wGCY+LHduRTA+abV
LvAR0NPf7PKQUSCECe2mBbLPO7wD4BO5RAkJeA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
udw4XxxzcaDNM1qWnRgZ2JEM1MMqnKwKVqha/krU9EyUAsyATjQEMBqjlOHw5QXMU2jjizlL20Nl
h2pF7iKo1S+7TS54Y/UIJANp+Dl46V/qfy6/yBnE4YclHON1k0jRao4C6T951tgXuCAIQEmXbr87
aJfL2dNqORH+TDKUBdc=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
JFe89rPDaiIUicPDNoXCg8pJUoYRyVDoW/5yE4T0Cp9xDHtuIyKQVbC7jVb92OsgJ5GHDm7DH2D2
rYZKrdCIqPt2jo7DG6bcJuDFcisZb11HLlYWNsK2Vqs9DdsTPViykeE05CD5AgfDxb983x8F1meK
w8zjeGoD44djsaRA+lvP1zLhl24q5LWFJdPSyIT7uWZwhxHqlyJu85ToXLuwZQZO76Mp+1mitxDy
vleizC5rnk/4hqxfEFS21Qi1TwCz5hdU+H3nA3dTe1KRY+obbFP7sRWKfmr9Rcf9enRvbaEbLoJA
9ADkl72jc1Aqlnd+YCGq4EmbElbWLxblpamncA==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IV5qOrW4jXfiGt6hz/YcNm+/H3ij0v503CF3Kvut91tUWldzNzyLt4pIZEWEzSmn6RcpcLNN88po
1kt45UdSBz+mL5HDQaw4J+VGD/cCBmW1jnOclCf82kwju1MIDfa2EKicjqaykCUROxV7cwg07FFp
clLfIwd4kxgSWnGzeZi1IGezx7OpBsAkBTz9ha4WttEm0+D29DF9O4GaQl6q8IBeA0QIrO10EESt
slfRi2evxdOeTZBVFoXU91OszneH/prZqyCsHeyvTa8PABTZ+Y4CH6ICZCXRn7QTNJgoYSGABuPs
87saNJgzomjyaO6IzGl1fBgMIsIurKw90DE8Jw==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2022_10", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Eek/EeBa5kEiakvxzHC3fZ0NXCsvWnLN8FYKLyImepfBUv2jdVDY2j6Qs928DJaMO8pBuO9SGcu3
47rhcN1DAjZza5Ac83W24fngY1+YWblivVc8AoggXS1t2Y7Dy1vf9+ZxUdOvq63sje+fDJxapZwK
3HQGdtBX86RTaUS5K+HyI1FTmcIhUYmJWmxQjIxLla7FF1QZ4XpTCfqAG5i7ZKlYSoDFb8sjCRG4
XWFuk1dbL2UfZPxXZ7XHIm+03Ck/JsHtsjLCc8oTB/9MLom2HX9SjX8H6tFbEXR1NatCFWQ04JKL
kHSYD/xDlwjhN9CRvowRhNJaYSmKQT646hlNoA==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
gaID+cgqOdyoJPDKM9JAyimEjoxNakxuNjYf52N14HEyn4NQksF7thq/bXWc82vmfdc8aodx1+ky
i8uuKszW1WwV+apGSqk7YXBCxx3ACfMsPzNzeDQ2HVzGfznpQD80Eu7I7iwtz3k5Mr31iaeM1kQa
oddk6CkVESI8CD21PQHMVeu0LKLZJp8k8NHf3i0UOXsP5o768iecieYQh2VYXZ6HORDDyd+IpDB1
CAFBZctXco8C1w74wCB0LXUSYInc5ythxBURkPPTJ1GBuXpoQGZD2sNiI2Htl0y1toEdfgExWZ+0
+4Docnd9TgOGhAhZzUcj3c+6cQNbgCB847/G6w==

`protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`protect key_block
tEBTlBoXowC0cOrrCu9pEZ6t9FjMS6GWThbzsXMvR2xb9HBcccdvXT7EfASM09KkNzvlYoyNBWct
0TRl1BNzzlomu3G857U6kezS+CCRF/K2qOhhxHFxEfuM0qblRVdNHoCGGMM4PkE/rt9M7IqYoXQg
WOHI1ydpZZn08aVL9QYJgz6ZuVHNLwSpL6rjFFDXV1cB82gVFBkRP/0NxpGW2WH6YA/MJ0czV0ji
o0umOWluEwUObdytKX1lfuNYimI0ziWrovqq2osL8J7NBKDUl2R6gJ51DObsTBgC8uyUHVibyNHy
nhzTpwcBeeXdtAueCg1BlHDIwglcMUdy0sBZEyHM/CLzpxgr1A+uUcmzlWx1drrc8lRNwGMFDDJQ
9OzoHBABtNt8N3bbO8A+rE9HtsjMVr1TxHhUTxBhWcypwra+xzsGykln/IP3JBwwQR0+d+V8/Vec
5Bh03crJTvJZUbYidozNoaPOfnHi0NxFDNdL7L1i75T+H6bqeE1ADR/4

`protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GM0AtcDH85MjCjnA/pijf0ZCJap8X+OYUX7W1StOwznqG2XPd9DhtvTyqD0c8/7BTdeCzGUK5iqe
QiGwEcy1dCrSVZW0KtjFXllkYV2ai3/Qn7Bgg1YuzxifEFKe6ClTsByfgjqRdyZeNSAldwvx9ZtT
0ZhijV96K37zXwfXFeKDmxOZOV553ovWfXGekaS1EPmSluoDYBMQKc2XV+ZUXR7n5NI/6E3QdK7K
utsZyrFYyJdYW8Po28hQf1nWeQP6+PxQB6wi/P6sUzudntNcQ7uLRr4PTz6twPPqYwUF+7YW8baL
p/2EFPf8y6fBb+DOBCnzmGZvmq+M2qQot14r+g==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Ot2lizvbpt8emXxoZl10oi4H/aTQTzrHDg0mf7DDs3BS7iyFsRmaQCG/BRF/mwjlw+EbST4/x/DJ
d6Hf6LIa6mllnMD6G+uVHQ1z31eASHPw3m+WnPMr/zCNuebPcDitgiXWmq3SAS9byYvxiwcDvjn6
CMh89pvlx8xLiFUoo5j/lAPe4cPBJwSMleQLLB989s6rByi0lVW0QiLTzakaB8DHBMvhIYEfi74m
Lxby6+nYRGrAUKPOemP0Ag/LW83Eup/Wa0jVOtxzlj3foiYhg2mWCt2zyFhgQsDA+oEsDa/KZc0F
OUzOI8vFDrwPmYRwd2ejFI9Nz3/1mb05VQmDRA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
OamBwohUIPOIa5bsJvu3upqnGl4f3jNYS35bg4S059C9qVDgQJcw/it81deNA5zFvzX3Cq9CiO9K
zl02VsdpFgNPjSwEO8F47LJZ4fHx99EmESBogsNwUNitzkuYTLCb7F7ZF2WSJExQ0KsYt+TRp2UH
yQEvpM1lHQYUXxzjw08qUI5ssSnOsQFydvP8BwA/6aGrVJ+LuEgPVdMqLBn0EeAmRsynxJ7OhPGV
DlvHQwtVuBrkvjQHED/Ye43ZIeWPm/xOcjNfZjYeOvdEJqTbaviR0Fo1LFx8EX80uvdFeK55ywDN
wzoZ4cVH87f6VwR1xHdo0JpVxrajZpSZ0jPudw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 119184)
`protect data_block
Rz1ZuXu+08StTOUttvRdzFvV0kIzl8YJvYNWWsnt4fgBoB3S0R4Ma7zLz0sbk/FeiU2Morj9ZyFh
+LGXeTLGOyYFj/Mp0hr6UC3hU3SLKJyJOyBLLpHOJRt1aYfpdczoFFPy8JXBvyWSFC7JOndJ+Uk6
eEsiddzIENDh67uFqvlbBvLCpydBddDsc856whpwUz3rdLqzPe/Qxijiglrz8kX0LCbiAYaCGBXB
hnJOOrFpXtH30VCJQG+WyylnSI2eXKwB12TlB9h8V4dawI3zNiYBUZAjxxGJBDy65BNNE0WWjpD0
NCiWSF9I9W2XPTqVFmKfwEyPXqs/IF72iokGg0K8q+t/CSkHGhHg6AQ0VVbgkpLFTjuKiyJqazCl
j1VBtcVtcQKJCRUMnJUYjnp+yVzvrGBHyQBkkvp51ecbQsULfKroiBAlZaaHttSRMh6L++aS7GsX
cy12O6o+sAiRNiHCLqqcKG+kR+29Wsg8pwxsBCYutrUjVFFlDpfa/bantTc7TNv8ygJp19JNBFkJ
a5v53atnQgFfI+5IiRUl66qtuJ/aE7/HPpGtBRj91Rfh6hN4nHGAHK0Wa/YJGPJTpvXE4nfJ98DL
er5j1l9OeVAdlky9clK9tB1qkAZHXWHihJ3HEVTIsZ7QqaeUe5Lo2wVD6xE+Aqjvs0B27b2J5cRl
vA+54PkQQddsGcIN0UVqpT1lxBP5jVSyVY1BmctVEkZ8rBg0IifK0CNIA37otuT8p7E8uKmABRKB
wLdqjz29zQcYB1M/C8frXKaWI68DDlKzYuUaBdrzRxs9/nF7oLiNP2zkVGqFWO50UuNWd4guL4Ui
KH762EyTbP4qU250IKstylXEsMylHeierHofBBVLoIZeNmEsbHlwX0MeKvtbND9AbO9RgtjmDQqs
+dFofdDbrq+sz1IdTxayeT5ZH8MTXZCccI0AgBc9Y+qj0J88DeNTsBiH6XK8b9QEvGKeezPAk+3M
qDfxLL6ghzLthL08IgoU+V5tc/qUrR7Xl9kgpwMI6SgzARQG+wGbvnaaZcTR1Szc/QxfyTtKPLsd
WjnpKHdlaXzC/VidtnGf30PPvGETa6wFy0ord0WAdl75dKCnvHQXhf2eFEMx097vkmJ57M81EYKn
ctoAirbGlMfhRPiRSoCgOouTGTZB9uMOBxF3et9S064zhkJVsILPVokEbfXJYbjHy9N8dj3Po8aj
fX7LHpNCMfICKPjwMcb2A13B+5PA2OU8WE83XgtJ1aXn+CAN7841uSOiVXmwOpEOOFjUowu5puaM
qjaMcAV6h6pLl2v3YIqthyYziM7p0S7YmU8r6OhEHLnvmsrqskTcnPvwBSD32roK4LAsDIDk84+M
rWe9ofy0Tf/qaM95DYkiuZRWPw6IRubj1aMcFr55W5HUm2eX41oklsx/e4o7fQjRDzHKU/Z+BOd1
Gi76x0VZlTF6KxJu8nCN9w3yFVe7UkiMb0tNYVMa/3P6x6g7Ctlvjyu+Dv/fliJ53TOg+HLbSPQ3
0aBMl/RNnupYJ4ygfsSp7RpwBmL3O+WggShnKZ1pDF8+weVvWBy6hKwlf1asm4ljK/RyH2yOzWWg
ghNwFvLzFMfWfBl6IWIm7h6vMlQfaH3ucXtxEYaYXO1sGdzWeHupxppX+j6VljFnIC7XCmQ9GM5a
tztzJYlY7bWEs4uzYy5lD/54rd5Qe7eVfFcuap/s5TyaZE/R/FRIHWaiz5g50muXW+1zBjhF6CSC
xK9pIcYgMPxEigbrz+poovrq4SAloq0dombCupD6gjL0Yc0sJQ/p6GH1Q7ZC8k3+92+nswRj1QWj
/+GQEaoLJ2fBOarwdGRzS3xdjwCwlPkmXT+dBJzmu8wwo7K+fQYOil5cGu2+QAS67P3hvw6KO5+P
OGLpw2j0sD4H2lgvpXHbvoDRqORRzBrYwa0S0qU/1rBc1lckWEytnGBxuG48IoP/joBcu21rp/k5
OLcgNTEDAPT9zg4AmPDyC5wFPQPjg91d/8Yyjm8nmjs0EjhpHxl4bBl6jftGnTHpLccqg6k/qQpn
yFaB6c080FjYxbOi8uKwwS7Gow8UF8gkHPOnfh9eMJPf0/dC6YAlfGXOm+zlltJZ9Xdu5851y8Hv
RYIyzHBNUYpqrCkLhIPktjLCHJT0X8M6eIGJYykmxhLa8kjGuvbu4isNXGpaWSU6wcqrgDmdzyGm
yqAgfycvUAYaLRlMaNV2GK1IUwo69TzegMISG0zWcjNuRE6y2lFepAlM07qohBtlSo+ZGIcrx515
0C0NWCTX1hQ8kOcN/l8DPv8kDBRUgq6yzWqS81PG0HMcqLylJOYZJAOo999ALm49J9q8sneUY97t
+alau7/hv6Ch81gtNWVji4G+ccWkc5USGHFzf7wtLvSf/LUHfZzG8V7qVIDMKIYsNFfMA8bKkBaO
sGmEhvR8PDVbRkrVOAepEn2ItDhjZ/vq+gJYzKx03vz37Sc7hqJo3rZIjbxEkYbyMYVWabOUOH14
ABLO9kqyBWUZkEFiGsEoTIcSxIKQyrTAuNLUaBhBi6jdlPYMMF/dsgFcDVLD2AzTxLE2T+1Yw7vf
rCs902ycxQNvm7J9b2opFtLADO+oo2nBK/mkqwfAEItHENqcd9qKnm2jsQt90k5oYFb/CoJ/k/rs
4eaAibdUrh75mFL3T+7FP6rDYADFjkkNjj2xXSzYlr8PbW80+LZ5ttbpOCDCrHu8Vc/Oii6qOMzX
l06PVOl6KDz7hMIEaY0qtrsaN1mfVAi4zWF8a+3FhvAZkyf1S4eymIFqUBpujbDeZBm8sxt5OHVK
c8POaU5orPbco7Wb6M+5d9//oK2jRx2yIZNR47C1uNc9AKy4rluSiuHwJnHyvXucpq3vWx6/82ix
yAQ9M+2G5vDUgTMy/ePlS+99U6+ciLxOHu2AVS6USDfT1A1U7hRCAn3lurdx8KCCMJyXGzEtNrPY
oFmUyrcqaEgzZI4K+5CJNIKUOi2HFyckWQVj5B2UfIwI7GgPu51kclIIEWVhpH94LYhIXSUzlj6d
RFNAdOAZoIdJsyzlOsv8pOtzj6V7MHt3cN73tb73/08s4OVpeQjincZULVHipGx2/72ydVWSGV/G
+V9Dqhg+hxjZYcJEvLTHguuvPmzs7BGYfRuAqxcTSOe4rH7u8TK8JPB5a6LmQA78sGeYCJw9lGzP
ghQASgIPdsXjujSWOg8XGnUAfY0tNfz1MPAbPDEzNjs9+UFSUfRTzmK96KJlwwwX4ZHqioWljjuk
nU9g3oYrWldhh+kPo0JnGDIBZPiBG/oSYrh7bMTUIXMPmO6yEEwsdZBfhId7FNQFnPV4WMfSHEq8
SyMVhx3LCgA09oJxq8kLGvmQqOVNTugk+dZKbgDQf+NCNEKWm8ZwGsqJQ1GusV3iV4jL07Nx4Ru6
Wayzo2JAfsRJF5E5BSgXk83GtR1inuHDvmEpfAII8MwAWEaVl8vtGEKdX4uhCutt5kA7boOmczZg
MiouUd6pl+7UmMl143uI31RqSdFiiFW80xbzIuHL1P5Y4vwdB4+2lduRdo5uz1z7jTuqlEi8NLBZ
mCyfXdbh8bKwXeuo86duf9Xl0LMUif/eOQ98Jq2z0Mih5wBdKVOFOkbQvlsPH8kVSYFg2U7ilfvu
QvosP+pY4JWnUMdFo2HmRWplBGxAr5N9HcGQnjbKuDXCkNXFpCWCuZon1vZbY3t9r943Lcir6HYr
rawXJSTtUnmziDJbDazBipJSvv2YgMbX5rMOJNqcfHrhASVBsdcboZuroLC0pLSBb/XxhlyjgZk+
LEp74rl8+2UzODIrgewwIjwSmvgxp5dNKhF7XBg7UN8aiv+aNL1hrDrYZevmc/l0hj7JdiIzhilh
taa18lsIG+xOPhtDNTz0iI99FWlavEaoiJG7zJEiIiSDqe/Yd6wNsonpEym7MIeKEmN4Z3M/oQ5V
EqOr8b+QRN3wnyYOv6FVzsPVxQE5uvFUwb/+Qw7gaEWBNdhwOCeNvMK891DdT6wGHPStX0+oC3uX
kZRcrxa+K1DRsJJB2+grtO29Okk66ESQFE1P7+6KWs8rVgR2lX0d66byDr20MVh983fqTGHyXUS1
ECv2UV6H3IAMoYHfGoIXZ2B+worVvzg/OYtooSmkYJXWv5Lg/AAmH0mR/5Sb9RbozejAhbNSmBaY
2mZv2RKNGurAo9KnwavPV2QLm09lJQk289/a8JWZiedq8lRA3KD+XLlipZPppv3zZIkrae4GMk7i
2EXyO6r2lObNVJsG+QGvz91uD5KgO1jCxJDOyi/rH4R+dfMIr7/88217XElAhXVJvatKrh2Nly4R
aSfCAqDnhfsuZ2vhtIy9tyvOv1rBcicLAu4Ka+81kTXKeu+Q/m/Ch+GiR/Jcq+f6+j5uiO5aPw+y
rK55tUbRXuly6ORoAqcewtDfvvZdw/D9XTtNdPYgf017F27MEJ8x73Q/ZiIDl13HieWm+lJw2lHb
SLXVu5kDSuLdGMrggEOwbhe2CPlYFhG3WOnhuhnWW9SZVPKpHvrZUOiidzrDEBPKagXPuQ8pfma0
h3AfW24mQu/6Z1lqMW6vwPs8B2RGEagb7AdMNLGc8BoX5sOlmjn7bD3BEetCORHftHU4YdTouS4m
MILcQFio+duggkVmFTXKhUbOs1uP1L812rNNUFL1nO9prQ8QrnCkWmJ1tdGI1zHv35lvE6CUfin0
b1GdEOqs2FGhVcW24dvrebgiV1adqSYEHkoJEtNNOFyJkiFmacZ59k+s8HFH8r4/ZRL5/K3OkI9b
xkTBnN5FgJqO9vJlaF8mq6AEPZCtznuv3ehRZx8xc1yePJ7ERG1hkDzIk3YbJXNEAJ/I9RrTqz7g
5+e42a3TH4rkJ4DfKIdnVjJ07/LeukK+8LW56jQRE2qwCV5Pxe9PY4PvzkHxsfpFWUgUAVKhr09O
9v839IyG3MsL+QCM9upzC2CsafvOyBtcXTpE6QK2hdwPRQ5HrQLV8sIoOWk9I+VtjDpGOaW8T9fv
Ph48LMkSl9f+5xmJFAqka4XpirWUPgy16oQZ1zttua1nDvoWVUaQZo8Kdug8IpXgCSXv4408aOpk
BGYtV8pGORVH4vc0N6e3edGF1vv9+uCNPp11Cv2iwulBbHbpS1xV9b6yj/2j4V798y4mvqGdq2Zy
cjQAd7MPolbCjPpv1ReXJcXvB8jNG6jB+ldvf2Akj+Bhe0wxxbjdeDpY5BvntUg8zxqSNQ8cdj8H
WpD0tMj3xGxuR+JNH3rJmqBQzodj7ySPFWDFKw0iaR7oaTDdR5ne/aUaxbvgI4wC1o+QwwJ2DG9h
CxzDPF/uaak5gSaN2fJqbHxDQeSZV2eY93Kit+j5cliT1WG7x3bpDN/DJyH0WSMphagZzOFpBK7o
JFnMvU6piKr9f77OMuUqKBvtUTQohjgdeaqtkkZGdHUl8si/0/r5joTlHPkNMVmOfsCYhSIXRQA7
bfhlG1WcHWULCvMIcQBc4Rss96q3VY6p79hFp3foobZJVwkkQUbbWKAM/D2SpholxHhAR6503o48
y8rr5vf8MOqJ5nXIEQGitvx7nw7MmGRqkwJjCz904QQedSYyvX/nTW94cXRMgHV6lgIOQBj186OK
uPUBp0+cCniqV7kMw0tZ5oNMy2qfLH04YT1LH2tduSEZWI3hRWF1KagY1q3EWry8s+6iu2wTDezO
0S1wjAV8XAhBVEAKzI02zTLQuu3++ctXGCmVfjCKH/huVDZedDvASBlY/2tsDt5H3/uT7lUWOZzu
9jgAgTYYK1/LxeHfir96/ztzQAvhx/fNSCI8RiP7k4l13NS45pJ2fDyK0joFFcbdpdMsEIkF/Dkd
G14TXFawoad8efrVrRNK80kCayG5GQQNGMjbxzYt+lg+HhTD4M4eGFDvFdnVc6bUPk9LtMztaBYJ
H6Lbh+ctKRrYFdrKFifSvd+FxdXmZ1EfeAWKO/pkv98pbf3VEHbdIiu1qtXYVo/8eVeSQdMcSE51
8nXMjRR+IC5z0xDrDKiGO9SH4ssRcbTiUqBmlCQPHy9DVMylo1mmF7WNdvP8ehSesLch2LlShHiq
8P1jBBy5HTdcCf9UBTwFO9pwPlqRV2a2ZEnvpPlfXr6knlB1CHrek/FCz26/K81Y/0xJQw82jz5L
6HhRaQlWpAvITs+VPjvKH+QSuVHGyviJnPv/rD4pJkflARmuaDoxesEJGRsPMDmkcoBkqhmxstwt
jV6Xht7aORlQ/MwEZvcC1i/14cQSHnwyNseC72KOJTZsMSTbZ+2kqdb0/aVYhJDMa88aYjz67TAE
7jotIdEFe1ijuaKPaAWy/k6IZDoNDQZ0mpvDknybJc9ocFahSFTFxe0Swlhw7jv/nsrCbN7s9i0p
tSjQdS3t4c0DOrGNCEDpsW2KqGp+5xiYZiaOo7M/+WIgKE26sWNinwm7grA7FgDv2+u52gGaV3Fb
AtTygdfYwEQGa9UptTO3qEwvXp1K5FLI/nPrzVihBoni47NwdfQ5oWbTt/+ihigUi2OE3V0q43R2
MRjwPzIZz8YTTjZRFetOaD4T9SwaAe/AWTwLIfJEtAjx2+6esAkoAMzt9t1Jpe3bzBo/5go/2fUT
61WiLKrHg5akktC9M2diDSH1L6wStbjUKRRQiGjfY7xGZvSkjIfVJUkSZJYyFOAZm/nsTYfFGo6J
5Jz7b1JhYInODKAq1dMekjgMrwVoSZDIJ0IorF7AKeYQlQD7Zt2tWe26E0oW3mTegkOv6kKtMZQh
EnCZrkm70Q3ZFr6A5yQpQoJcsQamTfyHeLQh8X/8T8f/mrdLHTfzwaE07d1TeuIg9brPmBdn05QJ
7VhfmwvMuVS/G67UAF6M/mc9JsCMXh93bychMhmGwkMyf1Ke7CZZ6vv0dYmiEj2LHUDLP9rF8+bd
qx+iHfOqjqYKUQ5Qi86LtnymPx/zCt0C9pSdV6Um13kJZEqCQLKwTTBRx+b69efoq2pb6OQF49S2
eoegtHA4i8i5P488Se5eidURhC5qXeceOP/SKIpXHu7TJSmIzP8dUMygu+M0V5sF5IvzpcZleQr6
4e0Gsxhc0Jgdmza9MgLqr/gUcAsW4I6xpJJuKgvLvEuyPQ8ueoTTcnvgG8rTRbRUXy9FoBPp1DKF
tIZEdTi/2e4B7VhlP6NT2nPAVZDTDDy/4OWcRu1V+TcZjPMu8TYqNfJoqEUh4bi+hRwyCA9mRS3K
WbXz0tZ4rwahi8ORPPrJnEuZN/MUWTUjpNp6yOnX3FJiBdD2FRrV1nYEwaReJv02pNNN3PNaJrq/
YOytVrOvdPTdAiNkIsnh7SJ0q9IISfFKSKztLPwiO1tSohbRPwrE7eF++3BgH41397uWwBSp90R8
V9tO9MFN/5DKyMW2TcjwSMOFjan9GZHIhDkh2qIT809AxHVbgEKNs8//GHSYDqsb8GCEP4f9tsLu
jLnddMwsXhw0AgTV1PvjHVoqVEIa68JSLnnGqbL8FmvwbBcEoaHaQnnX1sf1PIlHKjMQCV4YzSGm
mjCNZaZgrgW1jwPcjmEsMKikmrd+OgHD7WQs+bqe/57JWXpOCskoXhgpuUcBldeGvKcmudb5TNp5
UNJFifSVt0nAGuC4DDMvbemjGTnNyqB2rUNN1EAm2SfEOPAX+fW94EGSX7G99Q0EY1PQrOTNrgom
S6dcWu5scMfnY5R84rsgHwgcxxHwS0/wxs87X8nZPHle9iRoNw71ueaZNBV+P0DRnHUN/Z/8LlMD
3t055F8N/GaZ7c4rrkwPjcfDozO6M+cQpNZUHL1VQVLHCbHkboaL8YX6guo5R3TTJpw75kwH2fgY
rmHjwEb/NCuL1PaKayfGdyOzo589J0lRka+NyEdY2QwPOTn1kY+IPfUdaOQZhG9fZFM6HZ3lw9pB
24DaWCUX0ED7cWA1y1XXBm/XVfSCPkI++Rg0bWgGPhwFaS/NaF4Z2p2Z5oTC3vuYVW/9T5mhvm+9
YnXDIOSPw3msE0pAyJAsEdXBJWvcodHXeZFIT2tVslaRVKKXNNPkS8XWmIb8MjUbJf5PYZm1Sgd+
mZzZUPGZyaLNe6LLXgvoUkGWk+LiJfdqg8pA1sSvtNAj4+wuAdOgsAIuOdrb90lSdJ/0NnWiuF+k
rhfYwTtoDXkTmO74dpgZVAcjWpU7r+FyCPcqpaWs2w1c4oJSlLqhIZ85RHXXEtKoVQMmhM48355R
70736MDijd5++UXhzD7HHH9sHAZGmnasp8gruhXCIEcaYotxLqYSLg28+Ik2MeUCTAJV/rAv98h7
XjgpZNmJJleG3rHfVQbVgirDoSfdB9kulRwjmuWMhyfs12rEUibzX6f0dMd90gIh7bn5uj4VXdIF
hX6M4ZwyEsM/tQj8fwYiv/UjzsYKmChrxEQ7/dKY7CEZzop3UjpFZ7TrPVQ3CYvMQ5SXAOGf2T2w
CjqGx/N5uij7Fn9JCom4nJ7bJK/XWFRQoL9Bur8i/p35UkQp5w6PNzkqyMTnRt5Ri1VShDs5DvWh
mVO5I6+ELSRJTIrhQ4jxwK7fSA6xpjb5CSdI5FuMDRN3hycVBlbYfmXUY52d0J++WVSlSj09y84R
BclqG5ihBI0rIbvvSJ4ubsrc0spMrQ6cHZ4CA7isxgt7ZmF1iCtMsTgBvJKwAs6q65mvfxhCno5F
ye2xwnuFm/tLkVru7bXaqTvdgcMI1rN5sJQbL3qnRcw4kIFD04MfmVIqbP0BmrFN/bPBtbd7xE6y
b/fL5hRPydwfKFza5RCmCMwLg9ULGFB08QvkePlY7sbGAi+PPdLTt0ES6QbrYzBkPYTgndrRu9yX
vf+OllSa2/O0PlgJnh9JJjW7g4odzeY+siK+8i2LK0Z+NhRLmHXCCfXxKUwKkAwRPt0yjL2YoNbw
dX8dtT2Cz6/EzBeWYkQ49FXy52ra59d1amKzUNS7IkUGEWm9ujgEEPLEMe1w1KJOkkJ2TgQQ4p4s
wHac+oijhYwnPJB9njso5XkQyyUmNdHFHaSdT3RZV7b7YKhbb5YNwihedVtFaxIcefDdCbZijvfH
2hgBOkM21xGM9mRJGzOugtkjqJ3clfI/+pAQxGkwi6nc7at7+jveNsR27TPFm1hyEsKDfgk8GHlt
/FaJkThIYWMrrh9yqIEAFMUu/pJ1N44g6E+r4LF025XoVZngBGGxJu94bZCZ71S8MwTtpus3I+S/
iHDQH2pJPbdmoK2RpBa10HULlWXLE/rEI+ZAhHGgX0Nq3Z7KrkVTt3fGo1h47S4w/eyupu7wzfyG
P/bq1NsR/5kbLjHTz13c8QiGIU0SPvkWptIYLuTFj1oc2uhBbCCQmBhj1rtJyEiJmhbJLGILVRv9
qx5uiNeVVgR5oupb13lYJdI/zy8GkBc75ZI4ofYowMT6XX9ZiyTyU1kzMNdztkIImT7lYKcfxxss
3pkCN/RGkao6j7PgrE0XdaNfeYN0vImRnKObvbHVYu/qgyP5//d5NnX03hXNXBZwt/itrxWRYsfj
ha5ttx+LlOQSA06foM3JJudJ4i8wwsMX+DeJiL7kcYV0aBXmOxb9cYyTzJhAWtIjm8X2snNK1PlT
Z7Cz003bYa0FYa7GRPu+ix8KOApO24JOOIISqGNlTkEUYVWdnnPJhVU9A1Rp9DhJ2xevAbynbMMQ
5yoLsXZCMYebzolWhq4lwVS3YO3972hXjIpr5T/Rt+/zsS6BfcH1hfjNmf0V8CQXm9kWua19L2nr
7zln32gM+hfqzn3f7xWvkAyVb/icdhMWhIEnkU1eLmX0ZVLOuzW5o4D+pt9zoHhkw+5ZdUVfOgBo
Cq7MmJ/vFamcBHa0/NP/j+9Dg2qNMzFOGY64uzMKdHlRqsE9rBU7xRhuARykb1Ct4yRnJbroi3g+
aCLTh1USGgqa0md6u8aBdq/6oMjYcYvs9qLpulxivhSX5JOCJh6ZlgBGXTspG5nKZvDJOJIjqYHL
4THcWFq+7uOukma/u95mXDfhTcccYTU/US2riApdTStVYXiuzDMkxYVCLGMRlbVr2dZGVdWkvSqr
Q1KVtOHj/jr/6lwhEcwHBWAB7wEmjr4F1wP2OktzpT6duZV65MnyLVakP0gdb4mlF55wIMT+jzGn
f+lmsRV1aEabbQAjEbz0WC4TDUGSxARwVvPHBxRZ70e42/Q+1r0js5FQ5Iv9rSWWbL95uiE6o4x6
Ewmw1qbaoEZEmbW9SXCHqCJWzK/9Itl/lOIql1CGke70BVOWbEp0TIsqGyMhwnw2864KwTYirLP8
cMI6FyFinibEmSeYhlstWEIyag19lVFoYK+62FnqFrYzBwWVDhze+O4i9D6mD4l5JzAgttyxC+2Y
solmKt5HeVgkh5UEfWbu3zAiSdbaUk+XWKS1e5/b1RYGh1qWHHC41MoCNcsdxu/bfuJ4FC/jXiTU
QiD8FL7XMyvDiIiXTybts8X7sNxGd93lnXiNcpnadSYfQe+bGMXHnxrAN+vK8z1PWs/m2PuGLuMk
4lQ4xwIQDHPFXc1YSPfPqu47gCQmWiXnYcNEHQ5ZNT6lvvs9ZPFVfU2xxb3SACNntfK9RXWjcUx2
GjFAUYLhlyNYMFsRyGns/8fK7Ks1B5RCxWnYTtooJpE990Zn981f5Y3srEEDsQ9Y4pLDxrrbAYVr
LlZSnD+SgQOt57FuML7qNdbOiw7KB2pLZ/M2rYR3Z5WOhsjaKNrGG2bCC8ZHptKodI7UY4zw6yrK
OoCPl6z+9Pwap8LeSCK+eMd9dSN03+j8xNVaTK5VI0O7g+DmOhLq4yB/sc037ZIgjxbdHd9Y3Da5
8j80eT167eNoTzxhBfUZM3UknF5gG62FsbsxjMIsdszj0OgLhYYMPEyNP65yz0lpwFoMOZveqR5u
Gd4ShhUuL9hLkufOD3ngEweI8PNph6MZoTj7/AgjXuVMYnX/foldXr4S78RigLRISRfDphw4/dQ4
nreELU7Jl0GaNEGTrpcb8S2F7VdTFnrRw4yp0aI64ME2noDIqbhgXLMGOv7ME62zFRJo6+CKaVYR
Rp3exdVVvC18CZRTMsd3TJwkJx3lofM+2j8Ke212y0Su2lSV2LgSFdOGt5yyw3uFlKNtaDGcEeTP
wXu3WZ6cg4azBmFtJrB25sqxW48WifSiHmrmRCHRgFfReIruAppVzKDS+Bn/m+QaBRklZL7pPt15
uGTNrWKU6YNsa5T2lZKOlkhMkY42kibYUa0QP3AWC5zrUKg94Qu+LBk5cIHMbI9Hfpq9hBuWIpA0
D9PAAJhuLYCFG1ybimIDNqe3MysnV5uXRTnrdfHnt0ZhMFFCfC02W20OuEtWZcDtODMxwRRfLAhu
qkCyoUM/fUb/ElNV1FC080BieeFEUiyFJ1ikw92TVCclY/UVofFb8t6teKL2hPiClIsVUdu3pSrr
IfM5XC1fxu+CIP5hDsMTK1NNDpGgAD0YEDne8mehX4kE6m0wiVu717f+okeIsT+LCZI88RlDhfi6
0AF15wgAI170qGs5wWUXxaa3Bi50ngKXQshUgbTarnibKdPnsS+s2CcWIi6KafdS55viq8elXPZS
Wi3/SoiKDhTWnGFgncsiLx0Sd8LITYhvvY+w009lT95NM1b9pAB0U5niln3BIWDuUFhJKolICi+Q
QLFNfVf72gVzLTMZWuhPf5hHOaDumq46Nc/o5h6MDQCOaDqsbDU5txtqS9LZQ9gtBk9jyObpZdCW
LqPc3t0ZzbSFuKd63ZCYo/2iPVIucpoWkdIJGl9PrVx8JwyrHj9I/1Qt87bpCBG3xoyRYjx3MPo1
mzHaCqdXriencr6jUnaoedEM76wjO9i0XBJZ8kU9S/yra1Xcdn5WfB7rNL3ut9bfbk6IuCUMVsNb
0Hw2RlDGEULJm7FSvGknKjtQYXPD1l4jQK41FT212QmZSZvJ/qXvNpVb1GQvBDAMaJcKYbtluwrP
YvMPHMwapRmW9KD5bszwuYGywW/2TfsKuXnV26DffBzadPylykPSYZD3hoNe+/mbOAU2Yp5Chsu0
C5+AAVIgUu124xD22lJ3cYHIYqQOT5Lmb6T6wRlnjkRzY06rCUbhRFepctMeyiNSf2LArSY6LMFe
neXA1rexd6kAORB5Wm2joyUW/qqvzykpvUHAvc81cghW47idqn41cKbQaObvTmhfPKq9H3elC0H6
Qtmu9diQEaWN/VOzoud/H60h7sL/GKyu9kZ1gkZAjO6sM2l+CzefsjB5zb+cz51AIlVIHv2t+2xq
4izJoKqHvK3nz2bkaSAbDzes9OkJ58NbLAqPdGnX6A4fRNe9REtx1/7zRZg/4LX2otYWOTYOxOus
uk9li2rkalim2oXYw+3fWL//MrvsGzRLoC4YabTaEeRXB6Bbq+NQPbZ7KqVelBcVsTl0iKEtzddZ
hV1ZmqMuvBm/5+VGWrlvxWDK0vglQ9L4ZhhUAsv/0xB0Qz99vnMVceK3F4uG3Lgdo8khCmGh1IRP
6Duzelta85N8S9axsWk9jZGC2oZtX2eDy7urJMJvcXvV6ipPSMUG1ghdTEwRZpONX+v92Jb87vtJ
mriuERrYCbTZ7XmMd0hwY/ZWuNbiUPQr/xFrbNQWN+EDJ4LBha9yfcBbX8A6IkweTqX6zANrP7T4
mu+02P05q7YcTBIPCI1CjwNFUAz3FHqEUhaeXLhELGHoiOHN9upeoa+5fnYyYidJ8TlxZEu2yFTR
ADS0SGMfOanc5JMITa3T1/wJ/wvUvJd8U9rDs0MHBaGxIgMTKxW47+rcG3F0XvtlEKgn+3Qf/sb5
bxkpJ35iQg8KaUHDHrWbvbpouNtcXKOTBeQXfaCcunEiCxjUJw8vZ3SDAuNTcLH5nYw80hS4hTkz
OjMzf/YkUoSv/AoG3F1GWXVBLEpqBN7Vt+IVyNf/kUVikTh8433T8XNnPPCm2nOOkFcSP49wPvHL
nlHEbCY+7cUCPhs436yQRjznz/8lvMye75VlaQix4ewdH5YXpHLauDxUPpOyyu/3IVn/wYmAhzrp
2wFaxYRk9ho1GXilDKbkp9aP8KCa7/ZPYOUN96mr+hrDTl8252/SLjM+QHYEFDB8x5kB8XsQZnTK
fkHRPgEyAjlBSOwxjlNPE5zPe2eWbb6n19LClthdCCTR3xOPTxLrIm1Knv6FVR6bL2BmULoyZZxx
ePfj2hbUdGyEyPWs0px9Bi2tljGY1Tj3tJf08x0ZaXLdzZ7YHwaEODrUAug9t7NpSlz2lB1Q6sfF
JXdo8zcqQLbNgcVCPWKs4ZDUDsg8HA6AODWaePL2KdbK+X3D9LSB28J8Y2BtzoP6RBzE73rKnSMV
fUDwqFfIhMZlGIe9XC58hJZ5MJjlQ3Soi6gZa+UgjmA8vwEQ03zztuAYE8wwkhwhxFeeHZzy4e1V
SYdQtLRy27tdc/qVwSSJ39b3fjuSEXzSOKyCX0j4UkLig2E9eJ0fd27+ZEhwBWNkyPpGiX80a4fR
cC69I+dqE0sYannMXamax6ZF/caJ4SDvqsajgLTYB9JvsO3eY6gyBJ1v6hwT/lIOc0aHLh6pXOCs
3W+1noE4M3MpaZN12+53vAaiRiUTa1K2ebwEW/EBMjOfH/lz6lcn3u9504xu/o1rlQOadt+5y8kz
unhNk0ItdUVoXbVi4jGI4JMpy2Xgdoa0Qb/IdWeaHFCcHB5hR7PW3qqqzq/PXcisQFhyFifAM4I1
HFz7xxVILRe4rJGmiK0p7LDh6SyecPhLzja7XOlbkTgH1Tp7E6XStvsy0i/qOycSu93ybmqWTDDf
4PshVW+ZkcnOhh4//veXAM42mteKaGTAGHXCfCesq7o/Yy21mluSHrRY2C4H3w7HAt6h6ucAMZdD
wIlnQydPJcaHv5ZAHO72R5v77oSjFmbWl+YsK1+BTbyZ7Mq0yV3KdRTscSNSoiCh51ljjWNf9X3d
ehuO5b9vwoQZfwm6sc2X4xmYJCmajmbTLi5wrhI7z+q+Yhz3hUsbgcc4z+fmhuGBd6YqvvlR3tvv
cqu4Gp3qnj9/89eMyS2drkf+yIcHOc6MYu8apB9jMVEtSlx+9hevEilZ2znYkyf/SftOI1qzpSL8
9+xFNt0rdN3eSTEFI2FzLrqJMHE8Dw8Lqjpt2Mgxe5S1rDkqBCGKHmySw0GAroHBypBbxUNfrWhY
zol6mExCA46ZV9kBDFbxEJF6fNhQPYuAJVoenYWLy+u30jpc4t//irj1obXQy1j7gAhziKUkEELm
I6pCVk4lm5kOqp4Dpo4ulUBYH5Ocp5P0L3FGkpkljZl0agK8WTTat0mg3wYPDu3mkcYhDCr1sMLg
qjh1h3JIpKAdgfyHcVE7LnQQ6de3olSLFEDWKrCJWiLK1eKz1RbXhUHbSshkX3RiSzLO0BnF4RPq
38zL/p/Ay8tbxegjMBT5S6nKVUc7Tuflgs+znhKm7jt0Ce0TBe+7GeQOwZZNFL/aRjVfDbqUcZZS
JsK6ry+JBCqPsjHqSe6+8sDKFEpHWoCOND96fm9nrTlkK0sd/vSQMpL6fR2lBz4oN5tY/L3RdbRm
n90biwnucplDivT3U+PmoswmwQhMz88sGgLAWI/n2B1eLXJxbmbZeL9U17+6FLMdN3tAs7Npq+mB
7FL7/fRCEk5nFAsah43d8DRq/Gz+Pss+Ix4sB2oMdLn5jGH5T6VXwBsE4RjNN6gVfmRIGw7PgdBM
SA5EdPs0Lbc/EOV4BNU9ixLFgnb/F6umt2QJT9WBMwGbKSZrnvj65YN4RTdfeaZ/RFEb8IDggFwn
++/7YCbZnuiWmiolfKUKfx4/+VFU57nSuSwvtiJVTfR50zaZ+p2KFE7IVh4JIAN/p03I3ov7PzX+
/4WLCc+NtLS0yaAqeVojTR3BSKhuVndCxqBM8gC2Z9Y8RPFDWa65woYN2ATeCTpnWxU4UzFAzSTR
t1BN+xavTc7ioVbqg/HfWLjQlqZvumZX4HqLHZG+L3lXdgnySS0HSv2Y26bLNN/1s8SQ7qCOugda
Zr2B3pGQMpaAKZa+UkhXZ2ZYxjAXXVnlXCciRSsG6VakD0DTex/wJkUtXBsAprlWSyora7zssh8C
u1uz4ccTJIO7t51p0aMfjLTkahJCcYzUeS9RNIhBFKEt5bdciyTXjxSjeAqtyLfp2g/+9Mh4q/cd
w6QvrcVJF8tZgEaDu3gJxo8j3n0iLBMy0/8eLYzMRd0mydTF/nI2IAXCQGnee/zUbGWnlelshxzZ
xYiJmfBeDKB7sVrshfYA5JR1kVlgypbShNWMn5P5asm1QlUX8twNhd10/aXNmJjJF4KwI3tGONyl
IQCpTvVbFZhplbA1chgH51atLb6xUaVrIDuB0drFU33FFS4Tm7oqZXIJPaWbM2ZxwUuF/WJu+NZF
X4/lKIDf8xNzD2SOzpbaxaTF0ozfIjKKixTF9n5AEt+G8jDn4JbjpTug6pcqNN4En9SB+hgot72s
Jb4NA0Jd7WaLUAQgyyvw+LZKLvM1PghvwM8Fnb0Q2nU1J7Qz4ysT4rnlDM0fHGsqSlnLFfwRtI8a
GbrIB1zyP64ws/CYlQXe3fj6N5y4o+BvBKMAlqpNM9xTmNflykCHaakkQy96FAKwOiAIIV8Huc1Z
UMbnR9mroauK2rLZAtq7Vn5LnwOqopexfhbxvSfkCYGnNLJNLJB0ysQzFB634pRYeq7gc6AoDuOO
lvt45cPtFuuSsvg6AY9ew4nAPOlL2diWENdQzmHym2SDsbjWNi1aZyKvWgH7f3y/FR0uiUaq0ONI
oFuX7sO6oJjLgDf14h5axhIPdduCF8W5HtCSTuWvCrDnMgacAwT+y89Uog3kXaxjtDdvalpH8ZpZ
BjAnU9APUVuAFH8oUVCU+1FyGLX+N58Ge8XPhits+JxdWXM3CQuK2TKsWaGvPOLaTFmO1w+vCe3z
9kibBz3Kgr/SKGBXPDh2M/z2sygBhwsOGRRp+sVb06cbMeXwSXF/Ikp+cb2A1IKnJZ83nKOIHZ6E
SvUGmrRIY+NvgZf3dNn6CgaZH2UVt9ahBX/9XkhajqkK7ZujyFBNKt4dmu5pQaMSAtCat85/fXC4
+Dnp6Mjbo0iqjp9p0z0jDgpWBgitq4KtQzlac0auf99Sb72IPkzWYNRibmyxhxLNtRinPIGC7e+N
vS6riM/Zw/YawVW8IlrPXTmxnLkHG2XTSh31fBcJg+vE9HYIHNldjQjNPBq3INSuZuSKu1qsT1zQ
QZlCv2TOIoOPxwU1V42R812OfyPBRmqCsb98eQaueU7aO3MdRJ2bxr/csXYbhvUS4k+6I8yeIXAg
+CNEb7cn/8xdgFm/NNqVlOeeX5nJEWNdL7rrZAASyELqiqeBLJGn+w/CmU2hIvbVnR9p5ysH0b5s
+qPGR5fLtK3JzCitOdfnhLIeuOXKrP0SUdFXBpySKse8+WxPn8c48gkDkySnJnERR431VXHiDAuN
1PWG4cKS4lDddCU3Ag2LZRKb4lvDfA5DZ6qE6XtkL3N8CN0U1GUzkU9K8gnl+xAVmbFpOwRlR+Nd
zOZz9IETXsLJXcKHSWWgpksiSwFmv5kF5GBqmMOwsU4U8kXli4uHDHURdcDIjg89/2NeUjpST6cS
dYutIktgjZ3iKF8P+/3+suBmZH1MsM4bMZggnix7CMdgFGawZFKNRBmU9rC3+CuQKKMGSh3Eb16+
5oz97X+U/UwTYZGVsFTV3n3x+g3KxxlAyv3oJ5Dbuw+YhFzgWAhTqYaE2JhCayEdfY/Y7fQ6q9v5
/f9WfAKf0dRNaQrHTDCs5P8JxFzkM97ayJtdJFNDmizSaksnKkQCjgp+CaMinKXu/TfQxttadsWD
+0xftaEZfNmFWtGYjY9JgX34h01wxYBBLzfMzR4WwCgKBZJoExx9uvOgANX5dVBf3S5be1jPGmTH
IHQG9al/Wxsac/ZBpBEx0/EEG0Esq7Fd6m+iTTnz+7oSK9RdxNJvC1tTFls6GRhWHx3DHGYPngd0
dNGx9PBggAgbQ0AN6o9cqWLajNCMkl35ETwD+N3HvlQ2Jyx808DPq9OZt4JzqVx+DuH0fTxoWIiT
dgCe7GB8IGSLNUOyXwauvFjwaWhVX8K+SI7/RaPB/TrBZAhuUzx4cUAFT8vWwPnOR6Vba1exvIGe
Dr7WPUphlj1ZvtIquQZwo8qLCWTq+sKFyCbdExO+fEykYrKlMHai5+VjvfW1Xj/xgdJbsNQwGloU
PPISZzgpwwK3cVb2xZxYRNm7j7xeb1VsPneRg+PWTXmQqur9LKBX424d+T5ZmEj+GbPeu/5oqyoL
2kEEjfeXPDXPMYVF9PDFzymutwAAz69qTneAxAu+pxV4dGZuK3GBeIK/PXlixZPRKBmLR+j4GTFY
x+X0FuH/9tb0Q9ODy5hUQQc/8FehX73dQuTq2aFPXMfY+5BZOQG8zV59nk6+WKA3RfohJntxlu21
dQPMX0XwMZ9n30JASIk1XpDCHfEv9kogA/onVxIPstt+3AxA99LKf4lvjhzn3phU3h9jBF6Xo9gg
wT7nVVhAZYrCeWKwXqt9+KO9XQlsLBPVq6RIElnofjS8Z46K9joczOt7cQHHo6U0wEqtEh8W8ncB
fSzW5uPaqhZYGeqiTnsGuACCMc4rRCvTFp1nUWvZs58lIxgnswyeb6QRHDsg7zIlUhfs4H90ue4v
vdhf7HV2jVpUoFz06tQHIybAgOH8dDXn9Y27f+BHnnw7e8B/+M2oM2tyge8wFelWiTsjXD0ps7Vn
Ox2NO8cu0b18a2TGHAyolykkyCysnfg87E6vffH3EnU9/r26V75tg0VQFY6u0GUvt4lVKl9yqxaV
6O8h6VBTKO10F4f7Tw28I8Sf/NyDcGuU68dGU3GzG2SK8lpAY/Il/suV3oLqVCeZM/StOeHHzG4c
v3FnsrI+f1bM271WpTeUBG+xDTXZFsarefXMpZgcVN4s6emzecBf33wy2QP9N3yZXdF5DQDEQ0kJ
OTjORKwWzO4mZwUuDPARj5htyQBkwkR41exAenA0CcAXg+sJz1wTpeL9ebA3SN9mqLl9PvhtQcKh
2gqab9syuF2vr314ElPUC1vtIc8TO2ukWpIAhj7bSNj/h2U9WMEvAtrUFzW/zn4piT+v8OkjbtKp
1u1bKwfzsZtNVUTmvfpupumoCoAikY3pR5uSAOUpy415Q1xJ4r3dvc+ifrm4SHFPyYChn+1NdUgG
nc2GCzi8XQN4BBCwBBShTQXCdgackJNskTBCmEBCHdqZxwdvOltCC4cXTofxIO3dclbGXY9aMVg5
Xd3Pwx0s9WEXj+ANzn9wvlQ7xt/xTCIIuYY52lp7+U0Y+Bhcdn680uAGjszqZ0hZml/dAZJcp1YB
WIlgCXnMWm6c2vIyevrUMSuyXJqWsssmE6oAuHAE+Hh9A6+w+IPV7Wynt9ffm1C9JfT8Hwcjx8bk
W0GMi5gEzEeDRrpFSWuOZnhBzCsgQCPjA+4FtZ9vZGrbmXj/cP9PZ0Xf6jwXEmqBoPU1ORS0wAl7
oo7+6M+E4kavQzdPv28GPRiUman6ykr48W13xRQWkj6qP4fBRt6651snhYESj+v2BGQn3xb52K63
vaFaHPnrSHszQ3TC8STcxwHpSjW2eNJEr60HkAeTPsoNrRZEkBs7eHEeYgot7jycx58OztLyslum
GWB2Q01y5khNQTCZQY0zuuf12tcMIpC7j1xAtovQj2B46KtNO9nQvivXXWGrb8ZrGp70XvEo5ddR
voiYGq3ZXjplOtADNNfUKpHIPL/uOavt57PwyAgqLNZOX8upPgIa12JMHIyExC3S0H38lQnZIBnk
A5g2XU7g57NoV2N0K2KsEzdFrnlVf3PA+GCI+NDq34DJqBXyBxe4jDV+syTAYbee1fdYDEvTrj7D
QSZI/HzDgGz9XVrRGBEBznlUAOQhS0Lzmmddel3iBlFWJdsFGbNomqYd0gYZy8AjLEnXfWPXq1NB
aNu9bkOfd2P4uhX+z4He1Q42VfDxR//LfdqTq6GZj6JTZJqEPfkjkNHI+gh2fKjQEN/DUlB+pGPV
M4aJMClUlTgWe5v/j1PrZKjvDLdjRn4n0KzF4Ywednv5RfXb456HAXmTTS6QpZY5PcfGr967TUMe
d4yvG5GrW/nckcd34SCQp7lnaMsX69RvY8i9u5NPSZbVksg4oNz5CeQ6V1B96tNlg1nua+tXfwwe
rz0Fbo9btOHDhbOATUWbK1Ds7aMsaiTGErZoNwONUws9XJp+BS9wcMeXDHT9ydOZUXcS/5Z+OIeL
z2UdMUfqjxLqw6KP1+Hh4dWytFne4rS+uOEWmBh9T2bJ1ssE1M4HRl2ADejZu8RrvtwAsRiroIR9
WjGfLaOMboLrkkJeLQy+hSdhemEKeVicTTJPiDZ1rsilBtprmUGAQlo+u8Ctx1Ow3WrWXv6iT8Tw
XVMd1eSadBO5DhyAlQ6sbF9QOO3Gee0MRk49QWZ7lCiEOqYxOHAxA1q38EcJDLBCUlTQlBHzuT0B
s0c7Cr7R5AhTg3tDjTsWrKxMdf/KdNOC4Md5l7OPFmXxv6fas56ugYNRqVmkY16XI47nkOVJwQBd
RMNVXFxabgFjPfCM/GM+PWfiD7mkh7qiyF/Hd5GkQwTvESRLGBLE0yN4JNTb8c05m2YUz4NDfNiW
xbF7qy4GqI+v/6OWH95K023iEo6DKddhQptGxNMXaUj3g9ZQk5qNwihrm85Q1MAYlMOM6ZIfAjBh
AKAY8kE2y8lH82W2ekx800MYltS+O4k7VpwnBbW+7mGDT7aDtfJMaSbXddSGUtXJASu7vVFSq9k0
Rb0X9pUYoGQHlo2RFmkKbviUPvXyviKo52myQQX8eJIGHUOvVSJ6h7Zxpgf2Jn3aCBzWUQHEgVrc
0kUb+6NldgoKU3LmHgzUvI3QRxKkySfHdud9QDDLjOMraTLY5Prn164J6alj4vm+Auiu83fhZAZK
c/JbkIJPcLZlAF2SxpTXz+hMfp2/5kOc1CXPl3QkEzT6U9t5HushQILSphMoRj2wZmSE/GMYnpOI
3pUI//dyILbko7IbIuLqANGWB0ML967IaywV2F/Y03mTPduGz8z3RdI10AARToalqZrneYgvRIWM
KcY2o6MHyRM8vT8ajzklXrKoP6byC+ic+H6eiWIxKUNJ7RLMagE5cYvxt97btDYSq+eNirRFPK7q
bwJedY151UqBnseVIhlzkZ5Lp53QOXy4kbxjl4bGyA2rv2SfmTVlm2ZHV8TdKRdjoUWE+kALHXYX
nUXY9i+VGDJ8S3EizI13JfPbTAl+xUiLvsZ9YFBpYULY5R8LquMXDtP4TCKoXtkVVKe41c6jlx9s
XbpUpKDSYJwwoeYZXe/XvuIf4lxGlREE6TA1u3Im5clO/cPFCjTUqWnkAyZHfmr6g0KQpk7ml3Gf
nNDWPW4Ma7mVxtHhyK5ClhcryHlxqMemXuTcD3jVLU0+/r+Vockl0msCoOn4qHJwfPjTwNYPCICU
SqbeunSg0q4DliSbyp2lL7lfJRBhVl/+Xs9R6C5gEIKN/01x34LW/Ak8JJOy7HK93nGFqcdYjGz3
ID4QWy2HAIP9TOc+1dkwnJ9uhCnkRpeZXFNfVg8hRADOmsnQ3baFvjOp2poAR27nLGONknblLtNa
0rsfQ5g5bZtM4rioNT0i9crcHf7toMq5uUV4QgwecvN9PFdsAr2oVOT8GQNZHKHRn7IWbj/Kw12E
bcWn1XBpniWNHT1fkXjrISIdSA/cjNboGKVMucFtZcqFLXyayD++oEuNDl5op+EbxARCSl2xCnUv
VnRrPxo9UoQN4p4a1oNEeeuDJK96mpeB+JkVvnHWgdOGdS6p1cJKLkMkpDVi4PpN4b2qm5ppCo6p
tq9r1wiiTFm+XysotsLAell+K4DKnnVwYMRPfxnqxEmRLmIJaGOARjK5EPCR+nViaJ/OLrGIrZ/w
meP0rZKjlIu+cF4wd7XB+sM8MY6FsBYvB0k2M3hWjjDV5FOgAQgEnJAVpoTBI9v1abua31fyxgrp
/NjwUH2gnmD4WjpWb9gKFXI68VepPgvTJK/AmDJ82lQNG4quoWV7a8+FK4l+IiKnD9xbCxqy0z79
lv/+NkV130EPxk3vIjvGrvc4dkz1KRPOZycoQYwyOf5BijsU3cTXdTjFGsmh4pDjLGNGl6/cnewR
iiRN2JRniaIvsshnz/8dB/Caw3Qys5tB+93cDlVUF0b5BAN12xsvfn9sJnTNu/msMEgf+vE2HJiU
hsKtBZ0GmSQyOFcIhx+4L6XzIxfgDCLjSZZYoEnhL59ehkjaMWoksyYGEF9GJrFjfhnoLsYKwZb2
fL3dr6i6pI1+cr4GG7Va1WMP+FZOVN9UhYRB8tkor+JbqtDxzYRlvfymbPpfrYzCU5ZjOp7ewdHl
62TZbgiIRMBDu2yjjsIAmGNjh2xgE0lSr1/jg/II7CB0VN1sSnP92VEMjeWOl8shwdO/RY1H5Lgx
0HrOgl0I2YdkyIDzHr1XMR7WKgYapGs5iTmV2nC54V/tpdaqX3YVm14F2pCCcxiJurX4ToMp+gQB
tlf/Jyk8nvQ4ov1SZd1mBt9ELlxtOcP/LpWs5sSwBEin3FB4/U0/u8A9SdZued8Rm/CtgkpTyEuV
uAMvGLWEoWtkXuKuq4qUtw4PI+UhDZIauxOBuPg35qsDvPVMI1utlZGczOoEuQSWLEUm3SP2GtOo
wnx9QOFYJfhiqFtxJyki+oCCup47Gz1ZQcGPnhVAxjeEZ95iKZe5AsG5rtOfzt4gwat6lySzcT2o
s3RpZzFjdZR9l3gk7nQiPVN45uVHaVSE91yEoO2+IGvgM3da0d/nqJH0ckjmvasR97lq2TLNmm/1
aB3vXNaRmyagiUeCOBfnnaCylCo1x6/QfZKKdea2+Pa0sPCdwPM/LEr/bXzwEM1SGGAMRMMzFm/f
8TehQfRY4I1zAcTOEz3R5w3xNyArnJqOLxc2u3qQMjDVUzX/E/CeChu8h9aKSIu1ll0DJCtKTHZ0
sBHltieMMoKW02p6ytt5ZeHSYbtiLgnxovPDNNILjVx3vlksZoxAvPoI93dsXelfnoZ7dFIWMsoF
vklpoCns4KD6oVQ+56yarTyyjdzjSOEC2uJqeeetFh3CNyeTWqRW7zdNrUTMncvccz7qTzJ61WnM
foG8IZV0egd0KB37Ar7//Wk5evutpBrjz1qOI0873w7AacR9FrJLJKvA6xV+8swKEGtGmyB+Poo+
3j0NSReWP44wOyekoQZIP3Vk3VejHDokHzvOPknCjVskxgqWkFIemKFE499fyVvo1nKiDXOtRihv
uoDaYss72lcFCxSlbYEobdBMXIqrk5T5lnOfdaZCnPBd5BV5OanHFkBPlqTRWiWLWsSC0SN1pPKc
1n6J3H2CvVx2y++DNRg9eCldWsx9qQ51nqRFFhATa6ulNrswhRZ2tNi193f4VarthTBWpUMdcoUz
GDaPFihIhla3+cngr7XST2p3ppM9CECtH2oRjPm7A49jfQCouqFgmBNStYXHTdkM0CJ2FvPmsmdO
eFsGL4Bzn7PGXyg5ov4Sru8pqgrTgEzWmQZ7Utllw35CBi+sXDoAYzuwojOz/70sC+8W8R4U0QmI
RFfl/+tMZjz2Z2RpVGIbhT9fBvNnOH5VUiE9N8stIngtx5SWtXcTtYJ6eecOm0dAobeaRYepEF5g
JiXLChP6ETOKOvU2QdQ7b/rePWBFHK5yMJYrTENlqy0pvfrk/V0Y+b4WUohVL4H8Vtd6bnGQq8IL
oCBCa42PphDfH506HsX4xpsBQnxzBHLwQZbfwUo8K8XjZrP0dgEMdcGU1Rg5wGkmgOg+R8LNTZQP
BjX6efM4KiwbMYEC5411mf33yizu7v8rTaKKGnserpWb0MBLdLzr3oH3AbdLiqSe518SxVaS7bXK
B2d1gKSBOJWYLnpvtQolksZRHMIhMcO1lFmJclS5F1oq+AWjTCtCZ8rKPzN7eSqogNUn2umaLu+l
/TA+/kaW3alQzzoDtsfNGMoZmKKOo7wyoVIrH2MvbrGtp1NolpL9rFVA52/A8je5yXmXDxs+KM28
syG+pQPfStPu71W7950NXQYX2iSs6uhRM+ZdLul7LwPlgdRUl7UDHHC73Lcv4sX1MYs5eC5t53CA
N0Lu9RQEYYFxN8cCPB9FEY9cGiFV7YJ4DH7gDimSu9llmvlalkW9PzY6YJvEJjwUhW+HjmGZAZPz
6pETNzl+K+W+jPWh7Pc+nPRtzclNeQ2GrTSHdlr+MwbnzBfaWSrC9UdbaXMAXkKcHMPVP6qiULg9
25E+QdCGYF8A0iweWuCZ3H/x6MLINRPCFAATpCiO6j3bmIRm4CLzBVd7n9iXQ75fTBtm6uxaUNHX
6Emg8YrKaDkmA9sUMXx7V540Vcqi+rqGJLDr92hG+UODuMCkJ5qjSBKBLzw4mrD17EZXT0INctJ/
Kw5dHeT3QB7sEQZk0Q2Ur6rI6XN4GfRyg/ytMPIXEBaw9347uQONg5Z3NCUT3X1KJl2EN8kJXmCV
9Hft1cyJViShslO3J5+TBM3K6q9hv0MZ1HTMdkUaG0IQFMQlGGEFdobu7/BnA9TmS3nUfe4W3e1w
0v1Z6jhS1CRPcxPAqQBxW/iuEAcjSa2EwJpZH0gD8YUjM9FwWDszHU/Q5UB7JECMZM6o3ylhG3RR
7cwjWMiw9Dxh1Oc8ieB6YIkMig5avxzr4Vs9aYBdsQ5N3a3MXQT217EX9/c8ODEEYbHeJCe9es5X
gii9TIf+0MoWympP7onIollh6lWl8SDpKADNPv8zn7ge91BF03+xae8x3qEyJE955O0z7XiY3mUv
DXS53ssXQAokiWxPDvwAE74MqzbzdObiYHvrjePYi64TCTg0hKp3IgN2EIG7N9sXTfN3oKVB3A/q
Bl4FON0XSY+ADD1qzQnvJ0zWer00qeh5w4ru9QHa4DM9mAAuL2/0XwpChtCstYHklPBhtl+4hnMl
LULAJS9SDrZqkLDT4PDGJtLkpNxQTVo/EMA1Zloo/fltmT4REXYm53mihC4f6ZXl9djFm111SbCj
FrcPSe/+pAO7ti/R+7QCnyyuctTxTsmXSz22IhUZj5DapL5rZROpR8W8SOJXHl36bTQlplnJupcn
U5UhS8/6i72AhB4Pzyt9kZpjDlR4HUtPShKw3ECRL5ypgaeFwij5z1bNzOzTQBdmAD93u/KATXDU
GwzWDb9TowVbY3O8H8BJKhZU3t07TRaitB1W9rb6MdcD8++YkfXxUgDQtvvpNuwfGqD96NwVRuhW
zoEowmmcZR3deNIf6RTKUBi08Jr3b5oRmnG6PHT+QKOs76ZEMl0MButiDy3Lvthz5FvKxNogMnrB
DHV4SqWDPpZjBajmcvGbAtrwrex5ji4bvaoNzWUHbewqkxxTuZFG90s2u1aZSdScART7WcHUpVL9
3ksV/Qrq4ku7by/zJHOhMkvrPKR8o4Y4ddw5muBd4dc9IZMUiuzJ9tHQEcjd3R9Obqwj9UkHVS/7
BhZmYwKgNWHq8LLhuX8c3Tbn1CtrKxepG1YqTP3vSs3V2tZ6txPD+3tPXPDS7i3SI5Ggc3XStdn2
+ODFchXO2qJpvhITIUGSiHTrx7sgMptwkui8HWWuwE3nDFGwuAzLWCCzM8Gyi2u3/b4j7WXmr4ND
+nDntLfvf0w+z3dJgk66wwxiCe6YgBpJ2ErtxvE8WZqwUL9pdLCKPZ/lz/E0swtah7VpKK7RYL87
cmmc+wer/v19EZPnBJtvk49pcX7afcwzOXkxdjI1hgnxftk31eYD/Z/6gVa1yfI1iixlWqWST5Xm
wsbQXiSSEx1BODubM/e0h8oFhRT1cHL45LWlRvjGNmX53j+nTTK312LC3dcTDqqRHQOgOATL3Yaq
B1yMV3eED5Fe7HllFpxkOASTjsKJ8ydiS0J/iGnu9iA0cEG5+x5Po5mm72LWD7REGFIBrwjGXOQh
WTTyhxI9QNmGcfiJVofFGm/QakQRnRtUSAFQYCKdZbazsA4R71fKxST4wHYyd2g5xKCKQ9dOZPOx
aUbLXZw2EhbeFifLQRgNDJoe5VaedSEXGcCTcJUysLHQEIY0PVAXRROjqCi4xmzWKHMnwOY0P+IU
ADBRTyfHqoZQiYfJBcQVuE+wzF4W7LnZl9SXrPBcNTnFVYRg8ItVZk0f4/k1eatvuAxYcnNc6t0R
jZVvz4cda9c/XsdWvcLYsPlGppDJ1oeyrW5UkbzF7PppiAXfr3bHytZoLBfPPBT5XhIZ23/fyJoW
s01T3dS6eLIYnqAWcFOmK+FmmosYv2n/3NXVeMsdxub4sTno22Dz6xlH16ENIrpn85k9Ycve0RIw
AqDj20ZJ4p278pU+Ndymf6qeUuRzv9or0/7FOzIbeazYvj50/DSFgx+Rdx8xYiePpshwBiMJmR6V
Ov2S11m9R4PtfIclt2d8em+D62/iyvzg9y8M7blwPM45SwZfVN8tobTvm/jX906TJc6f8Stclgjk
rmmYCJO7/PGmEFWxiribmxHe21JFrmNy7TyBFXHQk300pVy3jhnhnnZmg1bAZydi289tJY9z1BpY
qTBHzmmgVxVyugl5hjBgaNbLLs4ckU9wRp/BFQ+8LINhUmLR8Qqvd1PqNuCoGS31OrBTyiBYnae8
n64kT9cVrkaiVGkil6s5MaCDyxT7Em20e/L4RYgIWyVm5E4hsvkZ3RMt4K1Hbe9KD7GBiaReTgs2
kuqpgEi+Orm9E/M9vUJ9+wkvhxq1IZz1Bwm3HLs02SZODZmHxlEqCmyaPZ9hKVIAfqDSs3qR0G/6
dnLJD76OLKOHJwAEs8IW3meIqbegF3KionQaNy7e0lCaiRDGaUDqkoEEh73UcLiJZlwNoerFA+Dk
XA+dx0Pvd+yYDm6nfT0gtpd5d09Cch5Sv9qhZOkQfK5O4A2O45mWtiubSef53nKdeThotkEcNHA1
WyQad/IJwi/Q6OktYTmE6NZihcSoTFQklflq90Qgg1V69qO3vukzla3/FmZLqcHxmiR0qKueUdbf
2jhm8A4vBh4kFurZpn8vIL2hLYgsHLZg4O4WH4Hgrq2qhRFdtdv/m+9wFqiazwTeYLN9TRMdTja5
FYrb3Tp8D38SRp1MGaPVUHTvdbHOjEAR/fLn05omglbKeiRbmkKZFTNYAL4uCn2M9jsn/491TcdA
TCHFdqs15cJ5MA0pDjZZ6XjFSXP+pvnNc1HIxOWmdonkWZbTEphSH2jJkjoLboRNOgu/oxpYMkgE
Cy7SYjuMScT1afjlKzTi0ssrWtU6Hbb1iCSKyfgYxfjcclZ2uKrQGqsiQ3TzDVtdqI1n/U7C4Z+X
yNBX9tRQI2Csj/mMnktNJphT1TOf5nGTIiXKF9hMj0yxGJZ1cyHMat2LxMYOlmX6lxpjUot3i+uW
SdIr4dKlafvnZARQv4364gXiLgszEJhrntQuhcccDVjRQwNyLP9HDAIbsQbeFGEaivrA6v0SuQbr
TunKoGeENkVY62xhoy1C+72qdqByE37eLHxeIFUVzn23/np2DTY37Nkb3uXCYCfKJw39GxIoh4/g
vKZISSy1DZpTf6zKdgVV0UnLT8oK4fkoH8I5+E/wOKwqMQU3LTbOs/KS2QxMIgUySPDNXm9V1Xk7
DcisxgWYUJzFr02lQAY6VueqdYpXzGhyKZm/8DcT+efR4RNL38x8D6MrAnS1irrIYfhaIsWtiPf/
BjbFGRM/FHNUp+c3RMEydAalXGtOTJ4YPWlAZBfeuFiIEXEgK0wnJZEJ2RgN0/XKwBmLiE+EwyJk
GrDwjZs74G1hCJkp+JtIG2ConowdOAa928gI2Dacjy5trnMD0OQRBEwIS1v3x5xCWMUK39/r5qLn
beO4W+lhlQfrBKiBGkDx2ilTF2y9cK415UT0uK3aqRaeTkzaB0U/k5YBpIRd3rbGQpyRp6DeKm24
uPzi2VKOumDQsSf/biIlm0Vo5BRMJDdkCAOe0Awz9JOH5WSal73rJTPQtJqXDxInTQI7g0WRtViL
bf9gD8AoU4yrFWynU686sNBpvXv2zXAa2tCxC4unvP5npo+acqnoBGXkFQxuU/KHLYiTYQKeQo6p
Whu+DWmtBMNh5fgWlqrfzxfsvKh33NLFojBtAxIDjxtcant3zNkC4+xbPR3yX3DxdfVf+w59DFMS
Tfwtw0qoG9kE3I36Rtp+8QBLjxIAtpB/44Ih6TK7w+aW8t+RltSf4T99YKCVRZbC/eQC3HnZcwpa
PXeV6XwZiy3ErHEn3pQpdn424SuzqEKWvB47nIOM0mgVn5MIpm2bxx/i+q4GTfjK28rYvsNp8E7O
ihY1xm5EvVdtE3zeeD3g3P3rT42UkLHkiwTN+I70NVL/G11fJ2f0C3IGz17A6EjEkG1KWOi11uya
dZ46wO9tSZ3rg6VHziL8S0SHpRtK18RZk0DLEErskAsmhiuUISeKqFu+/GYlrW1LELK+9K8ZzE7z
UNqrProNxvsoIv+iLCeM+7tIj1v19ZCUa9FRqXhvrFUXXyyBm2djghPqej5zFwgWf5e5t7fm5u0Z
YX3xFvySTHKYwRMagZ0x1cREO5nJvpUUcqrUcp4gp/EnvQneWkPrjydJevjoGY7YLbuUm9kpscF7
7kGZg+RP1jAFxGXbWS9yv1mJlRwwcWTFYMTW3nFJi0qyXX+8Vg8/KVk/3Z7E4bOGr//DcRqEtfZm
LJRmZmPW4Ibl/kn6eWQ/5RovL/R5XnjbtX5sC+tBXyLV4SjB1yD+8eULw+wWnbXoxnfDbtmT0I4d
2kmdOxxOXK+weUfN7ZGNXN2MMPg75ajGvVg/ncUDLq7F4pDbeOjEKIpvG36sozVvCGoxJxh2+PM4
6D9UioIaBJfbHoD69K9mjY0kkTFADbUJXUmYflNo4ei75EgfA03cBgwCJrg20TZhUku9KvoQxcho
UWSuwA1Fn5oLdHDTD4onsfXIfNPhcBgaz2vch/WgvcvTb+Ir54bkPOZshfFWQOhFdC+TgDPAdvwk
FlauE/kyv/YDwtZgPZZ3DDEDiXPan3nTZvnMmZPYOZCdvrWx+FS3YYHzpDAaGHrC7BAYH5IoBtbc
/NEQw9TqWNDOv9l/Z2VQ5N1I2BIy8GW4fljPApJXc67FoWvCeZ3Odjbr2LvnQ8mWrL5hozG/rB9Y
gepZFNxNW/KdxR7Lmnv1ldPgiQcTGf/lUp/jYqdQni8Oz0/dyxbiCuow9PcAqU05DTr5Tw1K6Jgs
uyVXaBfC7zOegGwU6CAD1EQxqG/+tZQYUxmZXJSGEQ3TN7XZJYFS+yzjDiglhfX3leVkgDo11LmR
yyvLV0rMqmAYP9sG4HyXobkZBxxUV/9HDHUvN+K3ocrm7/YLJietWTLsxzfiXS013arR+yTlT2zw
advDusGCmTUgLX8pLfYmKkHGT3KL+9aBlbz/lre3ozTLkqLNRHLDzzm/Txuh1bux09f3X5+2bYP4
JU18yMPyeHJbB3lmQeSboCKh4F8nAsu6WdHsKerjxNzQKeFa9H66WSEmUnZtym0ad0JfZgRFPaDZ
IuXftNa9IicMybtSLH44NfwRRw1at/+Kc+eyUNZ48HS6/T5P+nMcWD4GvRE61M85y7Fl8xK9QpME
aws4fsRGoh2YPgrslOaVnT1uzwhW8hiil4LRbo/1iQm8fR2/9P1Q05QlFPa8c6aEokc5KLjiIW8/
ASwaG+kkjsdRT/zMMQa/UY9yZQizPwSYAtiD4EoZoA5n1yoFfLcS0fBfklMJ1MiVFAuNlccUNy66
yBRn4pFayg8nQKvMmYjCt06cQkjpFueGAP6/cFFiNl+TaPoeRt6XA6lppEQH0rme2QMLlrR1Ny7D
Zm7Y2an/Ti76x8XCP9cgQB8D26acWAlu+GW8fgKN7En+PKIDOH/wgFZgJTD+baaEWYqJHixOxAWM
0Ps7lYrvctYZYIW5SW7vngMTq6Jb/TBAjKkdg5EPDtjxS5O0jwpzaMBD4VLarpFVD3tuOS+8TnHF
9FPpWYPA2wZalalRfsgyXD1245nrS/M2rZDJRYgzfhCW4Wk9ELvlebB6hxrVHnAElV7hUgiRxvAk
i1F0gmqTXJG96F9gBNriPf5RNwhFKrYkAnTkBaQwe9po8fumaSMNaSHMdsm4H8TanWGg1oL+k+Pd
5b8jMbk28Ngc3/uuxBvxJQNgYDuQIg1ET985FDPZ+GdQ6X2sIba9j+cR8fFnAYvimR90+nC4UL9F
CAK4QqNtxf8gseI4zMCbKJ64CDoXVcbtUU85DIqLJxaOet6alN9AUihdFHqbMXJjr/tvX60Nd4+M
rEgnJYTyWImivk+jNayN2o+l1yNSCijB2y0YHTBid+RD2ye9WfZhQ+OSMiJwS0SJBD9QjxnPTCYX
2pAN4JdU97eyCiTSez/O065rztWsqmrMb7R/pqUHNrfGv6VRKhRWnrs1hUTHC1kfVGmodVX2wfI9
OzIf2T63eOMz2WOJ1p+Yh7mx93wUSdDvx6v6jfxEkImnK/JqVbpHUDmKvX9725nDKrCFBoR2fN4Q
kfBz5oXF0j7Aw0TuvzO1sLeT7E2Hya7tC9QgrAwiz/wEs27WZNi5AW4uiBGQLzKBbfYkUxOOYoJ9
+S9JPtS7J+b9PT/q/JLtQth+yFDtx+Kaz+CjlwEHwNUljt93W+c+FqUHSsfq3sJM7aAyx+0+JOBS
W3EFvf7CQOd9zRN8mF2SJp1Qtc6WCnxYczo38twBpwqnL+lDDtDNSyXAzcBPEPkJVgIX9kvvDdiB
sgo6rzeGGBUpYITOvTh2Aoz+CpgNFsJQWi6Z/GTDR6hk0Z0b+vDqZZAa35duAkYJn3LulQ0gIywF
soDhLKZTRrzEYnTGlJErf8z4EdCSFDsO5P0aDJI8VY8P3xM4gzjRxEjylQScl/0/NA5/xED1rGKF
vLdzE8ztsZoY31LIv+PQHDxYKjiMMoMCaI387pYheNHmfQwyikuwmOgnJ2JXfnKZGQqaKuuvNg7B
VeXIkivMeaGOGt/eJb0zy/7wmaG7/W8Occ+1yNKvaUTHuhjgIg7LM2VEgbXdCVyJITZ37RYyf+et
/BL2hhEb76YmcTckmR8pJJC7C8Y1bqZWYP0+cbCZqf0euPp1OSnbfsy83jDjDhJfSqBvvhBgQrff
Y9cR0uJDOtW7WphtM8kN10HCxeJNsi4/cS51ExspjVavxsgR/bwoP3+c1DY3my4zvNGDPeb0N98n
ePCrMjhKTis5XoPKhk5nxOg1Ju8/+h9HA7l3C71rociydpV5GeHz6vXHpLl/dE1l/goyyOnLvaui
+FMotp6sr0RUF5z49+GxAVMrkfn5hL7oezy3odxwp9HjFO/3bnfVajXnha7cwfa5hJItGl9bfLnN
sCxusj7SAuBWifX4gW99MYbTANX0VgIono2ovrthkZr71VNP24cALUYtgejsuaWuFGeO0PQ6kAhh
+OLT1HVGHlT5kuzdRPx9dGL8ItlQdWL3I+WkcHA7/0qAxByuULp5wc2+bVqpof4cYifAK5B0dwwb
yLljoXBHCeHno2i8frMtnXEjBfIpXBRDlVAtRktwvwFf7evUEWkjQXGmlbTL1O5AsE0Yz3oQbq2W
DKRjGQu2Lg80MvLMmJPcfqmpEefwOUjNPurspiRN90K8MzcodjrAn3m4tqFhgG0sdDqiqiqhJxqE
jYetIYbLXdkgtujoUZhNM/XWXz8/KcvleFsJAuvgtAOoZmjhctfDRwm48pPm9m/JyTBSzMslCkEF
Lp23Q4k9MUym6k4UvkgzizZ4VxHFiCUigyM5WjFe7K70rkgnoHklbovfFXnGR0ir2GFSSplfIia/
mJc7ezZVMD5YatyuFosqN78GLW0r/Xt7gKlLwtwgEwsHRBO3d0BP0QdOUANcV80cKA/9iWt0bk3U
8XC3loAe8l5BVqqORvD/LZCJEFFxA33YMgIwrW+PMsHYvd14X5nXRE7FNCIUhGuhcwKHEBwers1V
kPFAInnSWbdIwW2PCAv/muW6UJsPRagr/Vhr0IMjoBOnycPh0X/7XCRWbgWtM0bGscy6jzZjmTbA
LoTK7AcTtD6PnM1UEkAiIeZVE1T9t9RzPDgXoXGKwaj4ltv6R4RfibLB9tlfCRAxUDQldcDSJP5M
AdrAzbVxCR28B50hSo8zz/Nbz9NsE5tTr0M+8IhhUmBDOfuWZZXmZhskyvYDewzyXhrIbMOTV9sU
0i01i+QKF9Ao6u+d7ryyj7IhrFGWc9kycOGA9TPtiC9v8DUyoM2RvB9Nmy5bh8DZmtUcm35sXYkk
8oFCWRvTwC0VkDqhykIkUY+79EVqXcPeftCTOnlNcdBjZZBTEj6vjqtAGxdVWKEZ1JPsTWANtNH5
wlTyOHeh3qJN9kqiKyW9wisyDCLtXn98cB4QsoNvVz8hTjXGmX/GM6l+GMAdTOEujictur3pivbq
mriXDnzRg2qUUE5J9h80sMrNJEoyM+FK0tazI/azcXyfXMwKZbRmAME9oP0GEGl7duBnmqFKNHfT
fk2+MGdd01hTyvcsrY11n/T4heGStZRMIhRoHYpX0kikxvrEhjoRX4naI54MgMlGIesx9JF5DZEH
wW1ugxFs8UPQw8SVNpxY8iH6Kp362dryka32TFeNRwP4M2g8PQT7K2Hu4YtUtYX4mUD+EOtant+F
PKcGri2J++bJKX9i/lngeP4c7LiGWZiSbDJYcWUlZzkqmrhARmOy9Bwr4iKqRWziQlOeOeaFdcui
nOL+tkC35L67HxRgKeUwl19v2phdO2iBv1Wmla3tdDm++KlfheyqFFDQvd3rtd7RlZCB0iDZLrLc
bTjTKyhMr4m2nzvBII54arUPxBE+w5lNUXGMHsw904/tMr+D+q48/mZEfW4+lGst1M168F4Yvzwe
kXeFZqDN6hWqEwHMWnLlAMnZq8E3rreARxCKP3gSa3CITz5fuUvHb6ZCJpTXYnBqZA3U4BiSBDG/
GIN1pRMNUuvgRi5VwhzyzfUVrVTi2KOyYAy4/9A2uK2bBIbdALfZGb7MibQFp14jRltD6h9s0EgM
R+0xboONGUxTCoxICvXsm3/F9TFHsUBDtvZaFDQ88Z1dUlr7j1kjEKHS4GuCcYiPtXu53sNeRBBS
zdN8B76ki9LvwbwaElTOVHjkDqUgmA1QH2R4T5eLzXveePJw0PUnbXspCpadBA62pabZeaUswfm8
Ik+3kBOPWDBhd+aWCAmtbsEgx0IIK9lhpWEBkNVU4cPRR/ZMl9jzlvj+LK1Envx8FvMsxrFnkrM0
ng0GpzbIIlR0wuQzEfvgp8dJBjG7KD65vHQS7mJ2NdCx5JXAWzBdqHbiRrVSKP8tq95evvsD5op8
60s6CgvgKi6YbpbfZFo66kIitCTsjSAR89245z4HgugA+tTUY8M5iMh+ro2yHh+foxu86TER05nL
b7dAZy3VDRnXhLzjShX1GgqA1pRZN9AyKtWfDmuDn9sDAChsuSBElGpjBZRDgB4L8Oh4cSY3WyW3
l5riIdHeaOwwP46L7Q96OelpUkn45h74kHhJijPuwJown3Qck+EG5HV4yMmAKHyR04LFDRSKw63R
c7mySB2Lb3CLfCZaVAvQGiPQaeZ8jUAqofIIVT/XruyUA3Mb+Yl3O8JhGh2hP6gOK3JJ993HYhyH
1A+Gb6rj0Ow9FmKOmiXwLDL+RvEM2SV6/YC+Nu3QZbpeoe3kulFcNfRTGU2YT7mNMsBwtxcPT86M
5mFOCLt0N2qpUWma7J0vjG9OexRpimZ2E40brvC7m47QQ4na7Z1nzRGphQbb8UU0FRBY8Gw8qU1z
ApkS7wOmZzBXGurjDHL3RvzSPjU/PnB+zuUm5wQT4JkMVaUuaW0oxPvOKVTKgGawN0bOUfHaQIFj
0krCMfBzGud1unn0Cmm6zRi1lCGAMd3rDjze/2ndEkUuvR8XfJR5svOXl4S3vkwn7swJbKLrobAG
HFq/EJSexzkt+EU4WXJohv/3oqmi/WOJIdL7xjhtzqxR23kGY3faQeWer9+kkbbKDoXOGvQxQTGn
Dtv5Fw1xjDfnxnUGpklZgiodMUkLtL/caKgswM6OpDlRLyKllIUA8HlVzUTn1xUPmsLcq89dwN4f
yy2V4DYODHsujyQibhL+dDsSQ2n8MPtSvB6eHyHBQ5NeSy87uDyfjCUk00XmEGhSas9aIoSqM8Eo
3Mxrckt+qR1rktmAOyu2/V6b+mkR9Q5dYLhyQXzWETv/Sgt6jfGOZ8X//HzGpKHCQvk8EXMRv0Al
S9RaxNiRL5AKWQ/0uJ0dULRylXHymktPadUBy7nDXxcJOOdtFfB12rz8CE3Jvc9jJ6BO5wvIOC0o
pEv6OkA2CBZ9JGbpgN9OAyGAVF8f9mbpAKzSp8kH2jzX/Pt94aVz9j32aFzRbmCRM+zNVVBhfKXj
JLJuuKkkCB/DGSnGMiZZbULamxwjTBlnru6WrWr37Tq4JDiAVkJRLTGG5RaLYiSbk6ZHU1me+oOx
bf9DI4Cc1P5AMwogNUu+IrUnaIMr7peZC9gJX2aSy60R+RSIy13lUe5DVzl6ynEqmOjJTEW2HKj+
+c6AGPIJxWJRAILgD+hjfwiZ/leKBjnrXR7dnb4yttYxHt63Y4dBr8hmGOZIMafrpl0MKkOiQACf
0tOkjaKaUfPE4kTahTSCYt1axbe1ZwH+J3VlLSzPK5ohR4IF9dFJs26ely19O0bOGuvg94YljlTw
7AcRtCuyo8phwUC+/L6cuxXWg5NTAr+duavoChRm4kygWpka/rtIuXuheFMc/qlsFK4w+B58f1So
S9icOQ8YkEiwA0d0EHjIslWOqAbVfj+Y5awwd0Gz4EyPLUMU9dcshIR3zU/+oN0FbZXH3RQXdxiT
elUlRdY9bUBDHQBkdqEjIGd1aGhqffwNSm7krzr05kOVAWKpTM0KKVNg5sv3/O8WPkYipRyUyxya
INtKEE2xW1SWlVX8oC0qej12R33/pZQKkotzuRuJ756dqNA4kNYNFEZMrl8KovqP8uYNdzk2UHe/
NCKgrdraevLCVkJPfaLFzMhYuRYvNE6stdJGLnKmvpWPedI/+plgVhhR2vMKpo4uGc1OljRNgm4H
7a87vX0B2bU0K8Ew0gHbpHZivUnBQnfUG0Dco35G8F8WGDPL221G5cFiVlZTLBdr1DgM8s6vxHzk
sKOf7IJ7HtXQa2yYePMO3uAoImYSm6lvRbUlvbmIJi1u/XfE3LJprxwQgj3skoERYffep7iBjKTo
lFgc1lyoCtlF+dWdIsPMglzgQI/OPQ+8oa3Q6OKoqR3yeLH4IL4u/ahqDsH5FaCOoEkJnciFCgtT
BLywi9NBh7FUqqixmLjy82OLNfjMrJOjbgoZml0+oWraKCwoszjnNm8QVfivil8eX0q1SqvDtPjn
xKga7n5Y3DzRSNmrmfEpfYb9zxtIp6hhcvsd7hrok5oT9t5aVOEys2ZbYHL0K9gJoy5cxdjjkO9W
TwW5VGFsqWbx/bplbrcVltKbzQV6FSdGfSPwCR3j9TRzcK0DzO89j6LT4UVJ3IMTLaT659h2A0mR
xb9AXyY1Azx1+tSNX1pqV+bYJUcSyygrNxqZ6VymSmMoYl05/7i2mvbiQCXqjEIXezZhkVouuPyz
Rp9c+NmzAyrkl94ueFTAdK6rd0t0zGaIUQGJrGEKDqxsW3pAokVLw+2VF9h0tV4ooOVlI0GCj1Ke
mIPlUzhYnJ9sh5F9Y4JvYOwA/Hxt3TluAlcm0jC8PWhhUX8caWzfa2Oy7rScVpCkayXaCsI1Ljf/
5/wk0/nyb5iuFaHG1tUNRx/hNwwSC/fShE5pGFkmS4SHn7QEgsOLzksqcYLqn5q0Xd3VtX58XvSh
SQH7fNn7uii3eKSsu6Q1dlIFZ8+ZS6g6Yy/eSMkeX82JoaE9BmA5t8DxVX6YU/wihjd+9UiMudsZ
8AzfhfNReq/XlH4g6QWUrClK5cfubps15ob9q/s4TWlpgC3aii8hxXv/nTkRtbdQaSDqdLo4WQJW
B/xTbMIK8lhWblTywOwF4PgBgEFOQr6FCj/XGitLCPjYERh0+UsmoJHlOt8OojgMfPPfJjnk1sCR
UK5V8YVCtZG+kmV3XTBEROHpAQNZSgwFfHfZcttBLbm6NMAdkXsQUbPHNjsPpbzoofMhBt44ipb6
AEmAg3cKYSd9GBHmictjzONh5luwEXGis/uVMbhce1RMW5yh/kFTfosHrtEaC5IZI9WuyPQoQ8/H
4L1jaiTPnkg+XfOYd73wsEebraab/8/eDXe4UZEoGEMR/XWDs86XDKzkESkkp8h6HJWjdGRVMTgv
1Tv9S5G512QCIsEiOfrbp4MGQEBArIhV6DkHyRlDIF/ta7FGlVYkMbC0BZyPX0ta4Vbwa0szlxzI
KJnxJFPwGEqu45XzHM7HOpRbzz3Kbknc/xopsy0OHr3iKTUsEVnGIsP5Ej0nzfoRKQ2CdgxiuoK1
V8/8IRIOgx0MM3yuE/xw9+wBxn87deq06Q/vQrHjArnEe61+zfUG7306lSInWxzU74F6N3L8MS2P
DRSFCdUj84AdUSlJSpWhxtsvJ9SUC6trhIJREXGU0OVaP7/xEi6dBBzrp9YfwdcrAJ+Amhjr1954
oIJU//4UecUh9+1etScJVMgLG7W2vH54g5/qimA8B9lwebfxOJ4A+Azpwz6USq7ISaw7cssFKhzq
ujBi7E3v47pmoMhd+aSf9Vhel44DhEe7GYKb1t/VbTM1KjmVbAoD8mleH/22lXg5IK50TBI/nZDt
z9T9SdGHOyYoRaKSKWk4b+WOArPnCnaPE+S8SyruvLXsjWCnqImuqBPCbmvBYXrJ9UA7awcrxec9
oHLPE9JyaLFG+Wf1l1RlEYEOArPEbHeM96RzZcuavxMy/T7Ua5h0MkNZ7DP5lH2KZwO7D6/rO7Lt
wkT/W+mgGuo/NARCGaDmiL7beufwBGsMwrTRsbS5JXibu8aASYfzuN0LHMh0hEDfDMBnZsmk3cLI
GVtGYElao5L+L6ynmvgjr/zcqGsJpMrinIPBiZ+kwOmF/mkR+awBuZFJsxL75ZpVyGoau+jdtp5b
NS73voGUGWcU8WbwAZf/bT0wZDFQ5z/HyLmiKAtO1HAQBeDovmfeNcgc2BlVZrP41u2MeJiW3iFs
sMGmaiQAa8iqu0xaHrP2SXxDF/TREjII9ObRwAnmqR09x9BIyUkvgsiBzgexaBbGt92FMj4SXSsJ
/+SZ8Jp7Ar/MUw8CzBB9gYZpgpaH4J314Os38eJyDQl7Q/V3XXkn2MKoAiAWrhH2Km8qlYDbyOVh
ryhSaoTIndE6uLiIsuQQnSNgpzFeaW3NM0pMYYkBfM3MLZoSoaZBqZ7toM3ru5Z1crrL9DHtNh7e
zNCvBH4d8WwI6DSfyOI5B7XZ9/Idkr1P/5q3VxIMNqlWeclhB7LDGqXX2xh06uTEFFBw3/BqKAJa
uQAIhWi7UzvbzYByZLSRnY8xjeihUTuEAjVzHkjL9Vfbj0HxfQFZwlB3+Uv9J6/TulxN466bXmmo
k/+RlbKLvVD24IS4uyb46mPO25Wc5RYHPgY3eUednhRNOIb/3yiK5k92Nj55tjSDMYpMYt1xFlp9
O6GVd29eF40JCnhBF8zvbUqDn3I2UyWXbYBRkQXgu/vVQeFCRfjgyCepndimhnZHFNmwgy7NxkYa
l4qX4hnianvNIs+1ok0slJ0nzVfBb5pmi1Pz55qnYEJ0fP5oxgKcHN9TubmoCjIVLeJVIITQMUbA
X7Nh3YXaYNzppBlqkCkoMrTrjAwLeyond0UQB9QSoGwW1ZggxrGGhYhshA1drSh4ZoHf7v+TmQui
YB0HLauWpaHq7k/GhlirDsLveiXs4soQHlTx43nZMZPWX8WMW8BsB9aIR9aGF9ogEEaKo1xBkC3F
8sPLOyL5+UFHD2x2VmXj8BTsBOt+zWrv7Raqq9Uj1Avj/Mu4bEEgsl/HN6Ns0tfDZX29GCj7adh8
sbO+8SGCMek0XRdkVTyld5t5DWzAjuZ1MwIfbh9JGOycoCv+Axqyi56PzzasNTtfb/Y9fxS2juvA
4VW+Rdq7Dqwl+rp5bUJKpuqHxbWOTXlZ875aYIpFxlZeBCmfMBNkoyb4ufetYPNswGqhgrNeRaIS
gOCVi8uJvvt4JG/+hwJ4GbFHkaZzy86qAkXFEMapa8sp0jLq9bmDxMoaUQ7/z9gS7alfhZvh6wb1
TgWMs3dxF2lxwf/O0CThtCrJm3ZKcMrVn4EHhThnnkCXXIhGj5ThA6Evv1Ccq7HbxlB3dSgbHMTE
oAsMHcHsG0rQN74P6yZE0vfAJgMLhpRHngC8kgo1RFftTKr+uX4TBDZ9dqM7HP2imIIKx8AJ0avU
eSSYqZDGjFH5WxBvNukhn6ADwgJBNpTMPfxb7gVwml4SwTNE8Pd2oil9PjW0h/ZjtujrNLa0CMv4
9Iu0MuPundOxjMhyHmXqL5r5GMT/dE/uFCAlse0kQG7wCmgLUp/+YAAmIaYq+IDWtV/bYTcd5ulk
nQhEk/ndlZT1VbpAveJt8KLc5nBOjGrjzpV+lh19buCB/HXPeaJPJGwPYFDfx+N02wpVra0gj2Do
3dwVSnEow/a/3FWKXpfiX7exTRiQNwaHYEkw6/xLE1ulz6E0ytd1bXIhR3pC+h/MsCGSEpknYp1c
6m527WC2rwS466SVxdytXNzQODzzS/4HqeJzJ3uprXIg2qpH4FkhQQZs5sb+3ZzvV9uKI4z/n01D
3MC91TmbN1MZz7U1uBsyE9W7xeEQgm4Sqpi3U/RAsvVEfaYMcc1xB0m2C1oKqQpf3ExbOrJ0huOa
00lcpPPRHC7VLOfutOGo86P07FPhnVkKffz1FQE9eU4qNDrElRfhcJ1eojGEvdDXefScJV275ICM
KoROAli36tnPvZCLmc/WEPMuypye+6U/FczoBLhURpCaCOXnpAvMUj42izBVDaETCHlq+uPyysgE
a5ZlukAOg9f/lPzNlK5ZBVZ1HLfrTM3oDRdURK0GDyFwltd2v8m4e+kRz2YhJeMSR1dHwfcAeyCO
gVYMaCd2hymdWOXmVG0g6GCRYsHPbtqFH5bcKx7RHfdXmgZLZbqqFS8/CVcnGJf3635BTjsULJfG
cMsVD4vbZAazRAe+qysH2Zu3g6XAvRsmHjN3x7Lyru4h/QR84p/2rLjfJuwGETVQ9QQizFr+WYYO
9zfB6WL/H3IeHpkftj4KB6H5O1QTJAmzE0bEawy3kxi0GTGhTVbP/6I98dBpZyTFhd1lViHMvClX
H57G0T+n5NIVfWw5DPdY+Wzek/UFDI9L7mJc4R5hMBtbNTQGjELsvgdJKAIIWegPr8aiPOePJMVK
BN5mAj9ZHbDPQoIIjZLaPCKANFBofd1HADXiL3vTL8tWnmtsSSH6VbBogP37I4ubaLriWpFfGUZU
FNg18W0VhXCQnX87+uKGv6lLB47sZw5N0nEOoNL0BU+c2HlRMAwgtjQnQeLYlan5Z4d1tBTGPNx5
3zb6ZV6pbP0/1dF3VSeay8hOa6PDIveGdxat85Wc0GAkaTGrgUkUtJjFvmmLCC++mAz2VQ9qS6IY
sRYEMsS2JkrRGG0WXLPMHfWFmqFz43+jYJBKyWVBJstEkbJ0h2PBwYGLIIvnK9pqf6K2tccfp+nC
2SAzZUb7Z9rReqYpbjL6KJe+rEbIWU71lxF+/w35IpfDbvU++Tgks4rSJd4rlq8XwHxJF6T42olZ
289XBOsCbp9XGwf18a+BTZudYbbgR1mQIlhIBIzlEpomcOhxy3oBbiCMj40z86pcv/IkvO6qM+c6
fz1uReeSwiCy+DA7vQ3lI+IJSVuUb2Z5pIX5tIGMozCjxxi9Y+e4sMGqR1Fv1Q4IBl7E1A+IjcYY
Sy8UvjUZzcawXPs/DyoMvqyIQerRq0bWLnLwxfJHu8skBf5K6BN6qF+pYLJxRICOqSHutqk7fh0v
oPj5a6gQVR13A/M0XBVQsSBczDf/EmgaQjDp7RHVnf90108dUK8VlCMqW60zf6tE+khePAJB1/ba
25iNSGVblYzEgw8QS75mSfsWoGqMraCS2TBR+l/lurut8jhtmoCzKOsJWoMi73OnOPl9DI0KB8Fl
C15mwNTPPxz94nQlHF0AQBddceJ++plZW6RXhkWL45hn/HSLyscNUSJsOL1BFMKOoVC53aytt1ln
f2PU6sy5vm1k/dZXaxK6j7NzPNk/k2zk+STak2TpyZ8q1ppB1UMPsCWU5xUH8STQPkm7y7gDVIOA
+ZZ4pfthBaKbJccKTXF5d6e2EgVPZtNkBGGBgnsTPfuuen4ntdjS3HEeSxj36jvy8n83qZHKm1Nv
9Y7Ae//2RfefBfCmqy1MDxVQ3aTm9PLcnSLj8hAyg/FZFsZEM230r6ukzbAufbhcRtFH9zdaKTug
CHuylWO8En7q486/c79ZKDbyWexn6Zfm+M57SLR2NHnjeej2Ib3IFiQzvi1B09p0PvNBm1bdHan6
gNSRk/8YdEwHFNfOxbT56X2AVtQJhOl+LwWnY9DLmV6rJxZB3+aVZUaz7dkqC3L8CfnF9Sy5TYF4
1RQ8XtLQfSr56pRB++7dcHCC2iHkobvYLCWUmB849gdnMmTNbcF1611h8VDKxeM53PJFVz6Eq2zQ
7QVYJGDzdutNjsjoxU1iz65BBomyjm8WpQgk9Ac3iirnnX44xkrNrCDARl1o5xJ3Ix+RA4u72nIL
06wTPNMqEuGUu1G52uL4Y49FCIkQp9+b2q3FUhA1O76XWZ4GpSwtMGywRiH1mXLZnOFkPADgkgq5
zWQkJXlvHftJS+pNp5hHZgrFPggHlFc4CGONx41uZRDGapFW2lJJfw5xc/2gWtwveOHRu9vkj9Av
xuPMLmypEX5lC6ayWvAkR0J1n2RVDNhAHN+03NtGbvbhe16JKigoEx9uiJCMKXt/Yk+2SWhHw9Ow
+u+vx/F1K2jwowoszOFOghffAcIisotFkSpc6w/Hy2RU9ChFgDEPh/2+s+DZF0ogg7LnBm1djOHO
zZuNdeSJxcZgByl2yDRbdkMIVX8b6ZfMRyatfwppoC/S1Zyy3HSt5vrLnD0htW3Du5QxHbhbzHQO
HAhaQ/0F8NN+ZxWKuefM4+s1qT1Iu4Gi3Wtun7v5Niovczj3ITRJnImuh/2mHU3pnvxJrEvhEIDb
XK29PkUTBcYIEjWBM0XHjZ6tITJ5gtHgrBWO8zOzt4XrQ1xkcN+B9AG4zeYl2YJhadE9T8vhdAc3
cGF7BwixOk2L+uL+cZ8lBTGuE6CEjt6yoJViwQgn8kH6RRjhS0aXD/TXyARgfNBOLkMua9CO7nL+
gMjrP3yWjC4N3FUtfkwcBIlEbV5WuWaV0ulClxsHF4CAjo53J7cCgdCkJpRk+yWttN05AlRw+1IU
I9vXqI9d7O59mB7463O8Zcm/7wXcJq2NwQ0M+vm09g8Uk47Ou3fKeYvob3uhheKWeV9PNgbo+XG0
TjlR5tDFnfvMbymZ8Wkr6DSd/GAw34kmFeaRDyt+urvWE82v4wYJlJMQPBPTzZqQo0J89wvBGnWL
rm6ogOSAYsYjH8MoSOd1s2drKUYD21TXkiG/DTOu8Yuleg/gHD8L9WyzLThxjK72ZKj8zz76lR/1
+Sxz1ML7HkqAPfiHM3tTUskvI6Qv5FHmi2ebrAkQ53AiyHr3gqx2AktdOSF8esDAnKEt9ZSFzf6+
dE5LuWN8yW9CLfJeBEmxVfUj7Pr2rZl6cyLaktaZz+7wuQS2tT2S0XbOr95epVp4IxN1PMWwWvbf
/GwFLJflDv44ctT1azwkSAUfN1uKITd/roN7YVtGEPVMF07vx/fJj+VIbKHx7zCGeqc7fpdHJh5/
RXIb2Wv/25mv6bykL8anCc+RhGRr1OkRO4Y50GwUfw2dtYqxp817Z7lQ3OipWItR7DAxaMZL/bNF
BUpKWN9ApMZIhJXjJB5MDqxU+QJxy2K7GXUD4x8X0Quv9bcRbnDuubF5ZklxF9qvMboLQ9hwEhBq
pPvEWjs7RqxZW83AcGR0lWAt7YfZXVd0RwSPPrRfuMIYRnBX6W1jOoQgc4EoZgnJDV/ruJAAfpxG
bxIM6Sowda/TdMD9C6zz8CyZYy3S3drcfByqWOc8Sls8YAg1WJzXKkHqwHEOI1/gNNtkJuHk5ZsS
yTEIB1FJLoa4AkDTyxL6w1HBX9b3Z8YGBk4uX4r8dZA0Lx5PS/FdXZRpU9RLXfRuPA/s6TDuC/yH
cRXru8gcy2/XsaGzqxIgo4KNSBGPizcz9/xbQ0ufpqdDGlXFgU0K6STiN7AarE7yT9s3S9Vafmyt
Oyle4FhlwX69Vo7sHX4mX1OZd9i2zfVEDF4fYR3hNpW+ZUzlCHlv0bVK4WJu817Vh9aACIuA+K+J
s34YQDBOSiHNu1s1CEHdQTVIxTVJ6eLPFY2AeGrQ+B+YrkGmJiKoJHpe+J5pDd9Pv/WNEte7nD5k
jXkM02qWgGe97NRJGpnUgFrz9YZYY173dEPUhkYv42zaU6EB5blt0UYfIRAMB4NGr6yyiwypPiMc
xux6WKCWf/0i+1eSdJ1AG7/Gpj7AhRG0/QnOoYtr4Ls1IWvcAjVeJPf2msrTiGCBheBiG9AijF/1
dO9c1qKCE1cTdNEm8HOAl0SqCdhZJ3M6M3SlpRHKs7VmAYhcSSm/sRSctceFT421Q9Y7C/mwYUkn
LdnP5GArF7+7PYLcCh5fJFy2vSpwtLnEvj2dTduXzXe01GysSkiGqVWGQ2egXAz/GSiaxrJVKGog
fF/GEsZ2bMhFkeady/x17gbe2UmEvdvfMzeXEF+W8oLBSDkly9bwr9ezHNyGqoRhX/19Cl4KbbfV
wXI/5xKdi/q95xXkzVc7p4hTl001TZdr/DEtOValPR1nByPQYJhLHsWFbLaPdnYgUHk5ePXaOTLT
OIqg5616gnEMIc3uPWqsyKuc0id68YHYDwfoFHpj840XMWdQ82bvdUQbqdZPbQBAr8x3Nwe0tYbx
8jR1ND4rsDnquca1telj8s9nkW4BhcI5qpEz+x8rsGQtvSGKPSloxrOt7e/iXkaRCvY33b6eK5oH
pqpcUjzfJE7t1kGYF99ymX9nGSwnbYp1e9H8XhBVjEErsItPOcNztVdN+/4ZDg/ZWcGIT5LpBN4S
m8P4jUGJfwXAbiOiJREzc9oBn88BXb6TZ1ZTfZMalRo30iiNuUh+DObt3RIGjXpc1ZDH3BgfDW2F
io0KbfVZty2a94rVbaRzJvrKnBKJWi3z76x/PeLhN63KwwTLwosHp8xwi4/CYu5GARtgdnPhIcEg
2/Ka0jbFB2xuqi2RvZRxcUunSr5nUHQTXvg5j+o42fZKO0NW0XFcCHgJ9WJqFTb5SoZXYSsgQtuD
1SE9V6T8re9IDwPRT4FxgCas+HuraBQuwMiyTr6Y8d0gnvozAGEUGZZyFcK84Jmxhg11UjsdQdkh
P/pkuVsKx7TWx5U26yZeC6HeaLPzohKZuGn3ZxwQY1L5zBQOLizmCWWi9YFM+mdtSnXO9s1tiAyt
HfDFwp8oE5ZsLVdZMcE2RxqQg5CvconGP4oXcUdyShzMslsmb52CpqLHiH055MgJLqt2+gw/vCJR
uFpzv8EnsFsuxM41W25RIUba41vtRLh46euDp3OpBSlGI8Tw5Ka5+CIa7oDAX0YRN9QVwS1RvqOV
4aAbza9KvBl7rEb/BsXMRaQ2wLCjajdvOUhGIvdYl7LkoEy/JBPclFPHVbNCFfb6RgnjTkYaeept
IFHCCOwYu294tJOew2Bv+P+Nm9lEQs7reLv4FamsLVYPj4KzIdD1mJZ5j8fA6jbBhKk89MkDiky+
v56fBBwBTtU9F2spXprVesVadFrCu6xM8IXwiFp31WM5AtqsDCXrnLAEHwdTzOC1HszarT3CegoM
ULI0gKp/FSbxppcPxWPVFqpww2PuMaHTOiK999A/SZ75c2zjtcDrtEwZ1k3scn4HG2SBmHr3Qm/w
23DNpVqdmP71BdYj98PYEPPnwZKBscwU+a5msjbRj/ttsx0seyJjTX5hT1tUOlLI77XPRXO7q8iw
BksTlw6YHaTGiHMO61LsqKVS3yT+jKzmWpgCnThnOo9RyX3ecjtug93jIIgwt3tyFb6Th8NogbbY
6xiUooXwzwDHshlQzGRCcCwOL699d8OPYM4RHs2VO717ML6oS7GDLC0OmaQ3ntvwOZyHC+YUusWl
mH5gvpVfjTSpABEkb4kpNcVa4VBxSQTMU/m3NXtF42V3BqiH2QSUOZV+t9HLxsTwiFk9Jhxw1ubn
bEkJ+YHrkzCSUw8VkeMxNE9vIwvsHnG4LXx66c7FStWDsvP/XFywpWQy+ehY2hQisHzEmfs66i+0
vuf5+ztoViRfe7Lr1PUPxsy8SiX/8DHipleNiD6Ud7zuYV5MSg1BbrnrSOtHUNVBUJpOQweEDxXA
h5MPr1E09+vbxv7VDdXx7dTnyUqoPJzSvX6YgUlm2T1fnF5Cg3o2dwNg8C7UUJFLvZo1bgYtyz31
lHMInQVsrGOoGt4xHfHD/JbFLbeJF1PM2d2+FIb2UWBKlpcwHacM7j2PBR2PL/zdbRz0xrfuZXcc
zRnqf6xdTgfX0I4PmSaw/5cBfb/1JG6RzFh5Ew8PueXaAsffFoOUGbCqNLPAaWDOc9VjILMz0oLN
kxQbqjfrAmKkTDN3UqFAnIufi3s7bL1lqpyqkPImWuYavEWdJwWg2Bhw/6Is8vQrGr6sl6TrJ6Df
qPemENhvW0T2r5DMhlYOMTmkG04jYnch5EEvV3KDGkfRu8pN+6HKpRg4BrYg3KmG/EcncD3HwDJv
EpnBQIdbHleOlik44lmtL5nRMcLRXXoSZI0ia+1dQhbzxIoOGjdQv+kuZSIGFJLi1pNPiSZ8gGBx
CS2EU3wk1gB6Tv2OKxCyxdhH3H5Ht6/UwcJWEfhB7SZEbl+VrFMo3sOGB7oL2R3bN+ZZEeitcnZS
uyawiDOUSOSOma7NUeLNPJOQzpbhles0nm83NgqGAcOOOv8eYNdVObkbHeA0meB3NTaFcVyKy4kY
IUC+adMvH/98PkWv44/yj6gTrM+UwbPD2hJpc55vjICsNltCaikaZhOsyZlUbfd4PgUxcBy6bHu5
hgmpLAHTEdp3E+KlCQydYOlA3B0Ts+sdvshYuTPyacp5XXW50HKfTHo3Dzv4RGeNniEtQwn8sbSA
aOM1/NRcO1xkAIbfTHaRUz783XrY7W666W58/yy0pkyIhtn1MU9yz2sbl9XNsNxg70khJnMmNsuH
3LxFsj55XmJe6WBykN+9kFgRCD1DaTo78WV29y3+SPxtpPteRdIRRaYvlmooSKy8YbmBLq7J7Hxg
Eo/UeYY7bX6UhDIckE2eeoWLLgiutGEKKSQzVFhPvAsryxsRBv9ljZoVlcOVqnQAO+gAMDux01/5
h4TrBq6ZMkzRjS/2FDyhKcTR5qUjDsFe2UCNCOCYUGlafnnQsXjp0mGbqYWem1waJMdrYN9R8M9C
dY981L3a7k0yPZ7gLKPM7BnKD6y26vLSSBydZPuh7EzKE2AOKdr7v/mBMab3EO2jIBaeoMlDc1N2
kxxkcewjMQ5oufGXlzaovm5/NcYohN/u3BZu+6yXdRFZKdCUmyhQUeAJeR7ml6IyU5Ga8Cqjk6ho
Dno1DqDtUO6cXkiM4A2KXcZ2xCIURbjHtm/CJnVkpAC5zSlQX4v2KGDf9iCHZW32BIM0rSSvgZw7
XVNzSZ4kCJSMwoi30XOJWCOBsMzyMSTmIuylRTFeGm1rrnR4Pa5NfjSnSUlsoQrmmONK1Wyfpwrz
3kmR30gUovbjNwZai4HA0GqB8efd9mQMID7NYp5Yb2bMr490qVoth3fo4LMSOTMsR0tWEtt07gX5
mxWqtYQjrtEY6dz4aWgt5Exaad2iFiE14AO07zu1qWOGaGdJNoUQOwhBVs1vftW3g2lBwE1Fc4fj
DTv6kvZIaaRQDMiA9Ucf4q72ycfn8Jz/yLZkT4jPmD98o1igvvD3RydIx51Bm1bR4JDJGJBtiSz/
Ed+rjRrvxC/otjamREBs6MWaCCbaqzKz5E1taJ94ybUNztzBetYCsEiEBXoJ918gRknwQUCRUUS+
VKoPW7GNl4l4yboXtG29mbeYQFYs9TaCsEOge7BZNU3Y+LbDhW+Grvl8fwrvGvdB5rA6M/rG3X/j
8sEfFSdPQztQHit+leIcf/KwCqEHyq/lMqM09pJ19WcNwWoASdqF+mlz1d0CGJZvCKZu9zXZaG/Z
opIKzPu0Jx+WiiMIwKNYl3Qcj6Mea5hZnjYz8JUcYThwGzHuhaiPk7VZ0j9BcogUXZ+WZVuonv3u
nLaptZ6zURaF3ul04zG+Yt3/oH1KH+5rXMy5NHQjnbu7yHWtwvW/zcmKcaVVGttoQFrWEjrvYSgx
r27R9rHBoNG0+wol096+iJekHmh25dQheq2+8cxQB1XBVzDKLCrUqA2WLZiW7FDS79EcZNbUNVRU
3x+6CHHUkuEU7D2nOKBrnVqhZDym2yz1lVXzcjngwDmkYXEoy40uCgphRql5GDzO5xBjYODnbfMy
+4fnLGsOxeCwtP11uLYWTnrKzbZbHiiYQW3mZei6o8Vp3eJhNc1cGxgBDucggeFT0MvlVPPpRAVX
AyIMXQ+XJcFnCRZHFShq9cqDpvVIZfUR3o3TI0mFK5SiNUzEtlkojM2heBnBQiJ4hj7iQucSo/6f
mNCzBzvEyGX84nyt65e+EXju706sMjdpJKvuR8EtZdKuAWp2PS5kQnv1I07R7ynJNVaNRPgq6mIS
kmO+xUsV06fDiEqRJ095kSNxeBrCrK8EMDlfo2BULYUQuPJWvtDK86/Bbk0hFE/UF8xA7jFXxzH4
XtTuGHiNAVZkXSleyk8Xwf4HsGQO667KCRKIXMp2Ghv2b85f1pqxCo0uQGKMLDj7LGc7UIV5dAuB
QO7kaKTEUN+hlsOwNnkREpLsuf0x2d2nvqO70/Sr0IpwV/KyiKexC9Go+kamCTXvD+aN5CEjb/lw
iabijzTxnE6jnSj27xhICRJiY6wenKoSPlodSACw/E/W7X0bYXUwDZfzoOd1fgIrBZgEDId816w+
/t1h5fXZCPsB+S67ydsfWkA1uiMF/iqBdLYplXAdVGp9I/VFg6qNei0/LgqCdrOWL5BzVRDlLzZF
11XHskOBa1bjG8AdFfWyPIPaKebtlbQjGU9MqCUprcpNYNL6F7euaxEoXcnrFzB5wNgEidmCBXib
2vaQDVdCrqeVnwMMC9N74lycs0a89elR3rpzsxXqtumtf44Nc7ww/QvMmC6da9qnsRj4azmIHsZb
PVie2Tdakwz9Eh97cY6UnuXgazzGL6WqsfQs7A0/FD6ZPQ+RALbXYl1SmKKzRouhh9MJYOk43xq1
YQBnr5dti4G7VXwqT+Xy8pATwiMLJzaSN/6SlhTpRDsFObJPPTHGsHN/zHCeTL+A76HjT3XnJUAx
Dy4cTzr82IqPMzdOEPY22hMEqBFk7OmH5jV/Za3cwkFvTHyQWeCR38kuYXE5jtFmebzOUDUMqUzj
NTUtWWCqjBYAEULNwd0KKwr9nsHiTJJWt431VUkpcTjonhmGcLtYPGI+rsvYh+X5ix/ZR+Ytl3uf
lgrlnBGaAf7NBoEu44R8ZSx5ePC2RFEqr+2Ne+2JaQFt/ovDvnqHQGnynhHDwnu1GtCTm6LiKEt4
FAoP8G5t4qwTkMO7XIvaEiQwIxRVxjnmJnjOiZNXvkPNDclkPQAcqGkAhnnhfgJSyGZnU8i2cR4u
5cD9VY+qUmmggtwhwNWKwQXzQteA4q5goj4jAvhCWjqJueT7mXU4Fr3lAP8JjeFTMuFpZIY2UHRS
20CYkV6MBPwSssFbBgIsGTnXA51pdRSFod2XM1YLQbjgot/de8QNrSwTvP2UFkzGz740L6SQmHGZ
OfQ0wyrsPKBaZxskhL5BsqTOWJmshdlHzSVsjVITE+yxFsk7IDut+97X9iOjeSbXnpMkzzPk1qNL
DOG3iSCWL21L0LkODsZ0v0yStrO8wE/ChNqH5mHDFSVr5oBUVUc+Y9aSHv1X4zQ++W8uAJ2UC+0E
hTqoZF5OIN3c+zKIUDPtLpSoQ6xmMOBrOXZ9Unna1nA9sSHTiyavrDfpr3r70lFaInhQ3Eqk97xw
h0FmSgFGxSyuq+mIYR4n/d5mQGOG4U9kbaLtgzE4H5MNZqaf1XLdc1koQzqd9SgWyoXtP3k1JFyK
pZEBdxc1FOdp2lxvSGNWRpzDGLSiremdUYET9nuWix4r1+9+/3lzwaELwQ6nhzoE/+ItDY0p4hxo
o/KKb0XxO6onpTDeWWb8EJpfjpzt6Cw6KEPxJYkmi7PXe5dW2xsFAZnZI21E8rLtLP1+SXk/rQem
9w9FLejjtUTvoKfW7LnnVO5iSU2Jn994SZjSGQLGV2H0bw2SRZGglpsvof9M9jHcj9bqjPoHXUAs
lOkuvSEsgEtCJTbE+1Gp/MGzKLLFJ7zgjFEHsaHBouJwXepI2G2ebb1kEMYp1BbAYVesG/gaKzxZ
Mc8/PLuW66TUK8LiztTRv5Kl4WKnjAj1SF8nt3OuMZmu6R7fPPfDbL/fR7fZuLVEUQrK5q1e55ps
hCliY2SOP9m5QbqE4Kc7Amjz0f7dy/aY241CqBG/VJuFhSdUusDugs4flW3RKOv5C8Lql2O9ow6o
ngJuFv87ShkVerRJ6+BANOA6N23fpQzPz6N8shKD305z48w0OjFQKbFEz8yH+eXtw3S9iz7UCmvQ
oi21KBKCpLXMQOIn3eHOus6Vdg+745x3H2XO62BmQlJeNb5Fui68qSCBQhsLhStZfytSyvzFvhoL
i/GkwTjhfhRU7QgSkc8un7lF/XQ72WLiVGeUFDIcW3/MVWo8D2rIIQTv1fR1JA12AJyo4/asBuBi
SVEB+rXc6U0wvHgr1s27Bvh1oj0OuS6gDqRgrK5VAJTf1RIU36MT0B0IqxZI2lrgoCSJmaZO4U4H
5GYTCj86FYzNGL+vADsR7KG7zxejQmo7aioixmnO5quwYQcTJlXxBqIc8HZxU0m4Fer/Lf2tqZt6
1vVTfd5ITwqwysFF3Y6xXnycybMYMFfNUIRgZsi5GSkFjJu5zoK3smEMyTqNeu35aaFLwezfPHNz
tptzm40juFeYCD9T/Jea3rOehMXSDHwG7F9Gy47Y+ZJBmCUh0prdo39Ers78veYcH3avH+lwBYWP
cI6zeDX3aw4Rh1yX2FmvPMGUCnHQ3yFi1plNjqpnoxPSZjK6sMY4GyQdaW43MLTjlvytkCQQLTQF
9zu6ETRf1BfcChPJTCVk+gO5/igPAx0k/8Y2jRuweazkI0uFOZQpHP5qIcnGxkD1P6BHrYy3IUar
K7WvbdjXaAFTFcT2Vfnb7NCkahMZapWQJGGkq3Sf4i2a4zEL3QZs8IUsfhTifoG6o4XqmF3Uruh6
l6p7tJ270nmZhssm72cpBVPDU/ZmHPKLMjCARcbU2WdsmoFLXKDDynzcOCUafC42WNJT/oXIWe3M
aDfo9p7ltB6QmwJeeDKsNMWcphhwZOEwnTCYIWO2TOpI+JeZntYviGxJ4pwJZllGKJU8T46osO5i
ZD/co7bnTTL6Mxzb688pbKI0Xoyc/tlnNwGykw8h9Q75BkohMYHHpK1QUzBcGd1GkHEhi6TgS7GP
bLaYakY4KZPcM9zPbvHUXBtLyB+6ceedjOrlLJuN5sF1e7kyajROSkJ55uPtZA2h4EZdhZdXlvZY
g5HpTlDQfViRgVhZrWvwupjOIUTLhKjwTGMBGuE6mHvT0c3MRN3C6DnF1iRXUHqKHTWq3jI2yEaZ
O1JAHYPMdzzbafkHCaSjhzQd/Jbwy5/vcCUTzkmYkeSK6omu/fUj+rgbnS8izMjcNbwmdipV+Hu8
eqOq03U456barA70sfmx1mmSaeXEYha9+ubqbHJHQBtWCgsJZIQnco7BxqM+4vnqnlig25dKV5o1
CVmRTTjGar6uGxo+GypSPFIzzJoYM/b6ayrx0fJ2oYpe4ovhn9hRqVt1/FV0sKPZyyCi9ef2u7TN
LVuETZd04LzpIPRiBJavrdUyFzm45ALtaXB0NEVAp/T4B/u5ZMEP0ExiL7X8jQMy3/DtRrPl+Dd7
/u2DUj3bT1Pt844l1AEeES97NPDCR7eQOxVn0nA9RkGBWeMwdbsp12h2BOInUoxokKayjFu+ni4p
dlOF0TH2hYh9O+thwFa++zV7RAz/pSu+7liEvwHB9fFLbFUWEdKXVKFGpmpc+InzcNiUOu6Jiaof
Dgk17WlvH050jKKKY++bZBCoSQjHajbHiPJMqNHzOwHfMUtn4LdIR7EdnGgu3hz51vQdnKgb92xI
3YUQ8/NxiT/HBz1AU/fPG2syU75y+kdo2mXyv2hKhZctX/qcOjmdnXUDl6dBEIL6rR0lBbVrtxor
W3UEXP2+/1Hkz7tpMaSKkbBNlmE0qwre3pX3IjXXcG9ql+MPtFn3llUk7pJGznPtY/q2TBIw5B99
quHJwHiH152ptxr6PwpsAK/amZys3BMCwb8U1E2BDlPhy4HlVzBeh/vIx9i3dyFDpfRFa9XkMDy9
OENK052tMaWcXsG1XCZcMJOvtPW8TAC4PGsL6ttgMGLLKj9TD24ne0g2KON3rEAUXNgUPwuRN8ao
88D4Wuiot3gdel8reIL5WBEIjMy54uHEyso3Jm3mxfZnw+uXTTNqXanmVdmrLPSK3pZtkGw7PKEF
S6jF4UCFJZFTUh+Udx/YM1qS6IcK/CqaK7tniPxl2LNJQCAPTZNe0+7VkiByh2tP++9ygXPPwNZb
4nfVNFafvri9eNia6379k7RECfjs2CDFcVMpeQdb7y+O8WXmg07S7L/dJd6KaUk5dXVGcN0D68Za
i/hWayxneN0/vSeIPNRbtvgr0GJFhdsubgnMfsWxqxKsTvp8ozCLjzxpFcNp+qqELxlxOc/OAz1n
XA5bSYgmMgTiWRi4OW533ddiExYIAHtufpI+eq3dOD0REjbL/sd/72+IzcqvNIJeQCaJInf831AE
MxLyz5o4yAF/hZD6GJ4GY/3mHLELYCRmqdKkQ0Bn5w5fGWgaPPe1p0ooxiw+NlP2d+82LtjVRBAj
pp1U0WZsCR27177F+VyQjnJjom0WYTQTJ7etTM8Ti4Avb+0ayFtKSfd58cxAlqMRDIOkw0ne7G9N
Ym1kGCdASR5qArh1qNvOYSoGkUHVJuSg1odnklPcCgk4nGk5IG4ge4Epser54+1Bz6BZ32guxW/M
28ZEblbn7zZRYDvUnER6SKhDTCRlYrCJMXAVZgSFFHqParHk7FyKGqN56wYpAyMhvzsqoaHpMAeE
+k+XSXUaQfJvqrvBrDJ5McpXnvua6UDSbpSEaUidIQlHoIEUQktztE0xDrtC+/N0IguIdZ7igDrw
ztPIYRKEyLtT3dh70RxZ+cWAAmk/5aX7KUAtG3Sjp0iVmS0MoknK4quXi9ThvVKzODeELvQQaeeS
+gbli/+Aec55R6Nu2U8RFSMzcwvrI9VkMW/8+mcH0qGAgLpZhRgcCN2FVXaCA54nUkxvRMnCsCUR
4idwH3/IARSr6Yy19FjP5txakx2Rlqn3zcoSagt0WTlcini1DgRtVSnu8oflOBs/vbcTcTOTP5BC
UYE7oMKyys9hqI81ujYhJx231jKw1sdIH0DLhnX4scGwAznuoiBPaYChVKIYQ847AuBhjRUdYgia
BNUE033fG56Le3nDPF6QyHQLq7GbE49sOo+CkQhJrUBrvrcjsqWqtLE5flLz0y4sZE/GNMzXjBQy
RBK2NZp+ceFhCCNi+S9Z293q4uhjI26FeYuKuIttWEiRZ8onrIVvWFeFQdD34spLrrR7fWzVXkdv
nV0LL9RMbBdc8+bM7iVKgrKoDk12/RzYdtHNLBfHcWfBKcpP1mABjZgJclavlx+CL9Y/PvywDJP+
YbrQ1brPSF0aHzY1uHhtF2lqyN0i/rLcnCRSVGY3AHbt9cWZzNDoBR88ZlvKfZZlleU11ez6y86/
E3/i/BT4x5XLta6jc9VOaiODVxN2BtJYxa+7NGbaKTO+1PPESHUpQicsBzMjiZsqG7fndjoBfhfE
16GFG5wKWditzMM2iZ7fCglRzRlF8e9ytN2t2Ew9S/W1PYwxyPSUFZ26WpY7UCo5rdaeRCT1aIaM
oIbILvatXfxxSy4u4/PQQLq0bb8aDwR9KYdN9mWMDqzhRGisF7TUXVYTdJJU0/IdPrmjf24xkbzt
L5skUQ7LteLMJr8uviRRei+Ldik8Uem7vxOiJ4XcaYJSBfV0lLwGzysOCuZxj/z9gKc9wABiMJNK
M3YXCifmN2WJbZWw3CsuH+QzWj/MQ0SMnAyKjHJ/tgiOBgZ8PGID0lMtN4LqpL0J0CU7ar3uO4VF
ZN/ugMl58MISv9ZcYCxcsGyygCAy9a+JKxT1uuvv/89UCFHnIf9u5Anc4rBJF3B29CCYTUjwh0EF
kkFoWGoTsrSM+E5D17kE3IVu5I1gMOqKR9tCR17Yq1MLQlvFYXW/JK9idMoiJtNnZOAp5CI762bT
Jd0zeF5uFI7pGu1eeY8S9dlPcU5795DRHV5gaPpGqShUjrp7/pK941C034cBePCOpv9fqCkH6Av5
YPiP8zicSkatvk17NUXkcVW4/F2S4mnA+dYnWd0ANdAKzqCqDT52SgxkzHI3drjo5bR2ROoV5LgX
ChOoqsLB8vMY7lTTsde/YIGTTJGZpkfNCNJ3rfW6AEoJelRmMDOgF2uF9myKB4CUvuWM9unTclnw
xFg8AQKQfaBgbGyTM3lSmc1iJ4kopOkBsGpSZnn5UrF7oCaTPFhZzOXSdDQuDUGDyDK2Q7DZPaHo
KpCA7/hchT7o3xsGKlXOMHcI9axAdcXMjWyCUDtHcjOcEyJly8Li9FfnaOYQW24g6yxMuQlGUGN7
nG0jiHAH6CrRQE+4EjQSZo6p3Ns/LGdwXgyoLCC3NARtH1Og/NckBSabpQomyCdAdm6NiieaXd8b
6Ovx9zQLHRLUVCCnYYxlZ0xAnYNooTRoxnL2i2jX22RC8SISRW2OZLRH9qhmIsbeGlYmSJo3t+1z
7SSBsIILLCRyhe132sP8gwVpI0tuuDlC+o8GVETn6zzPzjTag/m5x2s+vyPxL4xsmBSlxhC02ven
XrIgnsCAyt/VUq2gkSkMaUrW+ly6RiXpovbn/12Ty/JB2i4PUsIJo4g8g419PGMfQ0T/RZJeW3vn
QJb8x+8/hz/Sw8iLT/JczKoddThItkPjmww4pVPNXWJCVeo4JvOr+s1pgPQOY7hT2QGCCXlqd45N
GyzlwLJdb5uViX8ly277G1RiKtCFJI4QfuiBymInVVGZyi1ymu+dZdqgWEIF0zmOM0Vb0KwsWuA2
Btt+W8PIdGAIYEgWdmlcw9+Ahwn2HXz0VTwE59yvkRkEF8U8Opaf/ggudCSGRn+JuDFDjTMqdcKR
jzg6aNyn1n5Sph03uolOMyRs/+HcVmNbFRQVEHTr/F93kR5OjSsAR+ewVUARyTifMUCyYAjJLNvR
s8cxP/HeBmfyZ2siK34xKtFgHeWaAWO5SENA/sVC6wOdYLl76xx1ZVnQWa0mTVoh7XswhtE5fhyR
s96xBUbaclY7E5nxeXwBSaPVqHeis3U7ykjCmfyZc80QPqjl2iyJgJ82262N36aw7LDRPE5sR7b2
/5AJNInz3AazktXH+TcrGIdtcxQL1zGZAG4wfq2/lbxOdC0WobJ0RoaEB7jZ+fqlrUy9g9at7X0a
Y2KpsTejFSmp94Iiy64gx7wbFC0t//ceeadL/K3+yygiFTNOqoq7ERqXDDFE8QKSeynG4Y8dpVJx
t0o2s2c0WX/SdhinrQmXcJp9cAt640uf7RD4DaiL8tzDwWYkb/GyEDwzA3D+nVv58TuJ6k1fmy97
LDWjgGfQ4e+EqnRHB6/05pchQVyxXzn5n2ASHnDVeTjaWUHJHNhykv3CsCZN+XuSD3CusvQ6izel
DzaNUvgZwdjBsOdfHzLwmzt5nZe43z69rNi2FaNRXcTFTcc9fUXy3zGTyMCLSpnyLTgWQT79S2gX
bwG7n/Ba/lhyLp7hniGBscMSo29R0YigkpMyiX5jCUID56L16x/VCHypZTa92zVhYARVjCJb1vhn
/gUigtVKG+wLniR8+5Qg2ie5+DO9DuwJEGVf+iP+VVxuJlxTaXdfo4lNoAduCabSCsLbptEVx4Lp
LYIAlcdH+sdfb3CzyQhtHwKyxECUOP9wzma99RLimjKCqRJaJpVLgRtRkn2elabTi5U2W21VKLOm
VqzTrZqei6JIf/wa0stWG19JNzZ/yZv0quq04ucmWO1xPegGnpwvsupfBhUxpKUzXU+IieRxLlx1
m4SvNObDWB23MLWG9ID0hjXIgKWPrikJ3uFpimLU12WonwxJL5VOZd6pcz7BFLAvEkgO2tK50D3p
c0is8b4qjpST3hxU0wNOL1U3QQke5Wb16rHMOvUQQ6sCKJpPIm1cFQjBjz2YA2AbeGpD6d8gh3kR
PhJDfHV/O/FO9qKjUz6UK2GmaUbE6e8DwMhuxAk4dQAIms7XEX2size82bGljl5kPrBqJ/P7UNb3
VLqmoaicAMDa8r0FB1EdlInKGPsmC6cKmnbuVDWKIyfocptYl01Dv/Xd5tXaKmYr4BT1E49S7y47
QajLmhQ1OBKexl7oPl0yx5Cbxk8BueRVBhaAEArb81FDjhXbobMWqiaOYVbkOB4GRidAfYt4Dw+L
2VQ0B5U1fWJMh9VDm9P+PJmUMK/w5cBejhj0Hn3UHAehGajf7asE+QuDzAokLfAxoj5E/YulBCWj
XTFDTbCMoVvRWbDfDM3zqShp/51n2kVfZBOn4KOGcytOZ5vF82sXd9vf14jgUE45UsbuSNGugExn
VI9pNLvy//NrIUALtx2t9ckGUxhXhwIdi8Ft8TqpndeHrf3DCmg7s2RTYx3CNdTVtEzQgOo1Se9K
ZqISoyzUcY/ZfHors8Q1kJp5T7ViYm0zIxZjKTqy6uvjGyaUMp0M09nv/brMM/he1n3gZXl/muP9
XlqC1ntuiWwqp+Hcqi8K4wjffFo9wIGVuK0d9fJguzP+kWMZRkF6QoynidwKAYsMOwPainwNKBbc
dZmtpZ1pE2fyZnlzv5qp5lHYbbdeeFuv76pKqAHeQwZebjyc6bfs98oj6x2RrWf+5oMekjnVdnnM
2RkfJ4MMWBvZet1nqzGMFkv1wVVltT3tmQFWHfVfaz1f+U8LQ7a6hee92P+403s36T7WS7ykgyuC
HAWa4a8esWSw3+3OCc2upEZoIvVAC/WTB40dv8QKUjz7s0YqKO28lH45bcY+2MuGuB9dnx5KCS/8
vZa7h8PIjtaiv+sMKg+U4JfdhE2CsKskv7ZSkQStRBcaFois1n7VEKy9oEDh6LozMNXC3w0BCEMg
XH+WukYZqcVYU66Yv2vnOXXhZzT92iqUwQ9ER+uIuiwr9VYVYi16sVo7HTNCHfGdGRoLYN9kEBvk
12IBo3mR2ovBVxtKHixZD4KIrHVztmqOFztd75/QQq4aE7/ESxw3/AldjHYjd7s8gJroyOckZvwg
8CEpc3eJ2gz2lsFc1Ec0RWXR/H7DuyY7LUQEIjJXzC3J+jZiV/OvbhUuU8Cee/tCPBe6Y2K6Loei
uXnm+NedTZxeqS8yrX2UNruaF2SOUu+uxzf36fQ4QBf9E5+cjWfwzm04XAS1svNp8Iq2N4iHTA6p
fGSejN3UkEpO7QUo/PZ0qTBAuVYn597s4FpU6IzDZzxcSnXrCcExOyqz3dFwW7O5caWyYkcVVCAa
eR1wF/cfeR8+X7xbN8zZPyw1Oo8xZonE7nuC6r1jhrJI4RcVQz9CLQfUpLgaQcEk/CFo1qbJyVLi
mDBHYvIu59LdwPP89SV4+OM3qGmcbc9qxxzTKF2jKBSM711BTO4cGEPdVIAL+fIyR5kZif0neq4k
AAR607+QVWjo223GFKz1LYe9+AzlClRBF5MuHGPWJqv4Rxh56Xj19ygWz4VqQMLIkoFVkM+1ff71
hYWD3QOywark1kIDmGPzXOCamu4gSr7oRMyHTr8o9QnFa+vVgoQZaUDqNeQVJS9lGxggg1vUMkwW
NKnrkNJcWUIGwUJiReGP0yk1LviZxiVelocbt0PruVGwSMRW5rKOWZ4jc2LHIZ0HvjQgnCYHqqMt
X8f07okWYpzMDAsL5lFg9FKEVxehLovr6kw/nB5YDggy1zG8ChQW+dDWqjM82JRfHIQqMlJGIxrf
iP+IeY+RcfB86stMIDixXh4746vKmutrbCjZLYsKKu36wsJXlihaFEpcPb2nZPBZwiUTnlDidMyQ
uvVpJovnlUv2yJn+EzkgMEWJJoF0Qpp5p3XczWU+4UE+gL+/iJOz1KZA7ygDM7fGnpFF82rI5dtY
606xGFzkV13w1lzm0UdWD7Zqzjf5L8pcY6p0gnuWYbUDtMym8vs7nNFeK77+qnqHoaDBmDnXReg+
9ZcIuvYdltAuTE6/I/1hchIhr0dDed3DB8xfOHeLvhh1jATc0X2Ng3h8tuvMKFSvY+nAzov5tuju
kTiCjfTQna0Whih8Yt5/8jPTm/1dDCfXqjO0YdesRnJHXGvFeH6nUKtAxLX7+3x1/oA9ue8+3FIn
tu9fq+83XLRTz6RekQQ3JNyZeadlgMge+LPNM9dsOZnp/KgsCu5IMaxOp5WcV0RWDB2d7HSaxlE7
bgZ8O8eFLlBbAs1nYxmtAw8hfGNgc2UdXI4FcXnIZviC0JqZSrHxKv5XN2NLZ3HBvMHejxs+EEzI
DRPtJpF7IHOEpEBB2DzBZuxuUaZ5AT0d2ld9qhnvepG8qkPhByJfVHaMzbMHsUE3pTs+4d2gUkYq
B6P6FDMw0Ms9VB8AClMKlM1qiOH2pqRexxfmyIDO+v0v6f/CklhDKp8iXg2wiNCqFgxKI7uMroJG
fRP8RC/KvuPpV8c/3yDHas7T4lyRASZYGNTUULiDZnZT94LAme5oZlnggmHtmnEvB4ySTayAnmri
ABWNcnoGLZA4XBkFOVX5tH9YeggdO428fObab2vM5gqnEV1LxeaRgt+dnVisv2PTSTDj3swjWCCq
n5U/dSxv8jZ1APvA57/fdo325tCQLfrrCaeOEpmw/nNgWoPg/34vfcsZNFRg/uO7G6rkfFhjkhf1
V5AEGHafK86Vy4IxDJH5f9+YNOPWbedrcm6IBTJpIXaVn5UyR4D+7Tvi2dbt3v13oRFj1Vth7Jit
POhcE8VOjpMkXPD5P1V85tawM646gNIbbIrvHInAcV70EcpOXtS/kqgHs8DHSFOpaWS39oLH4Her
waDPeFnvCjnPFVKNu/3feeZRq9NoBG0h7GNpa1MPvhcA60E0rVsNTuzWqrCgxKszhQd8zOPJHX2H
PWCJwge5P4Jn8Ejct2FPVThHzJuOT4UX/pce9L2WTRvm6hsMDXys4NTYnot1pvgcsR4/BgNN0n7u
iEp2F/f9aZ08HL6gHgVZXMRjetd8OxmYtaaUAJlvacocrHL2EJO7+sOf6R16Sl5DLr+MCJ6GEJxu
yA86apZ1ohi29D1dY5Xfkk8k7qqpZAy1XeqxUvg6e1J6zijpibJo7f6ftwAT1XTPNI2vzDoMZmty
5P5zjaSMCNe/RPuCiXyB9YT86X0jKdATV0m0WHHNaLJeXNy/ea5rbgttc2dgV4di3hhB8DWORw4M
udCB/3ELe7oKUnGWNkyRyMYDl8k/0J74UYU5UExftLBIrDWG2rX2QBnZ7crkpxI97edMNf1/613x
Z8U6v5v7DTzsKd0Tmy9ta6qO9rl3PrwnYqVjaWLp9dXhWrdEdpEtWxxlQxnIdGs/dsTdhv2Zm1AO
ryze1PWtQmwid9Sr697aJukOyjQY1E0PdBSzzg0mcrLhyU2nPgcmExMT14x9pV5Gn8/MVFdiGqr8
/mO5bLX+LZSHzRroQgOcL14GBM98o+8q9oBlHfgIYC/dpq9pmsCXK0LGwXyObaBBMAK0hribK/8p
xgFgtilLgruh7vyLLXV9BzeJDPRgAzr/MYJ485ItEesI/ofbwXIpyBMnHFZpti8oX/xSdUAN0EN/
V3KX/9qHPKqUhX0pkxpo1a8+tJjLYIkkgTY5jTDgF4Wv1SL8EL1IRbqebyfztoACOjva0reYQUxY
tDin4Rh3dbC0YNQwD5wsyAa9j0apTvEhn66Eb0Y+zilmc3UuJAxd7QvNBZjkCaoCJ6k8ZguqcqID
Q1vpaYdX8nK/EMKiH8j9CmnoXbtH2Q1wKzTAxjd7VpTAVWnun/vWKhChDZEKNBph5GlXlgQ7JQle
vH4+rCuNLheGQnZ3eQ61CKVO4RzLDIloNNXi6eFgDW5PiqxN4a7c7BKBN+neNo9sVkCTJRVzTdIS
JNTb93WcgpToWka7s6jcPWkIP0Y/hOdWZMheBGyv4cGqdqebp2wYjSG6DvMHs4pFt5EeY99fNyWx
46/Ob1mCdRZa/cGm56dPw/77rjMqvfosAY5T1IgUXhRlJqo/50Xr/JTkIWgYbALTk5uSvBWjfKT+
k2QoQTYFMke1ymQyYSrEBxLUobMvaCRJcnLNu1ZNUTjkIfU0Ab4uLzMBwCy1IkMUKYHfjBdzr26Y
2Xhod85sPnck4sa0gV2vr/Pxy0rBH465ptq3I0Y8eavIZmnC7qkU3e0ln91XGBcMcwDrD9imsH8p
16YULO0siPmL0eUTW07LAzXMlivbV9mHJVHpHmNow6lpz9lUe3dyZJMLzzNSx27tIZAo1bW8ug7D
kod5mCjE/QEsCCf4bsxQdbDYkFikkYGKigrIFQiinkvetPBSr6Jn8MjOfp8BtMTfDTJ/kVfWfAfr
+IlBYAZ8jKmSNi9qbYmB3Swltsjdj9dy4OliCuYdVSUFkfbIFYwynef8b4x1Dedtecyw8pJ8oVhk
kp3CZqos6TQjgyg78NbbbLkMNss3CwHEAdn+6Gm6epLt7xvEUIWY1U3/tDHDhJn9pyUhktXhMAkC
ACwQ4nrVXkTdqsaBaB7fO2qg75GIop3qxkgx++kEes9sqwqaetStWq2aQXOP9JLKCdQD96iN2SGS
0cLPfq4Es98SCRDqmzAMFMPuDmA5db/Fn23srwS9WNqgG91bP683nYkv3nLoqYuGwT0PXgPfo37C
C3K3y/ht1McxU0gtNbummunFUothaj3klnxs9QmB1UQxa4VE4q1CFXWdwl3PUKiLvjQCwoHjvpjS
x/ENkFsizQsFIZRYU1xMpsWZLG7Q3OFTcJD5J/PINrgpDVCpsWHJD+cIhDpVGdDXnxQu8YnG8v2q
TwK2nbejDuCMb/DzTUTqKLr8YvB2feKmI1wEEbipf7LvntIq0m7nQ5dIh7Aqz2P6Q44k4AuLpnrW
9zPu+2sjdyGy594MM0Ph4/I6r7zbf/nT+DCzIzlOjpFgRLi0KFvPsVX8J7Dhl5R53WbsZMVfPoPx
RZo8AJhTmZBy3M85OlEzka3uCk4c7YUAxgzLNaVBz5HLyMHu5IURMul4Om3svghg7rK22/BmFt5H
KJ9HEo7lRSviGqobyNblVdBty4H+tOXyOx3e3qQKs3WCFVfzfrUHl7T79bMSGc8sF//brpbLJKL+
U13Es9hn62XcizSThIa07vAsYXNinnBZpX4LbDE+9kuTbjcJBrbkEYd77Lu97pDNmZ+CELsUSbVG
9EQxdzRvvLYQXJpQs6loJKLkXvvR99rgWkjsBN2fWa6NXq/Wbo0Y/sp2nJqDXDU6qEAoEhOFQi92
2/ToWLBnmjHgzb1uYC+dFoPRkguuTTcBa829bwLdl+R9ClR0VX18yagrWBn1yFiXBE+gMeqmkceQ
jcgmnmxnrb3zlNyxpjg+wyo5ubj6kwz9P/CNWB2uplu2wzULMUJyMLDm5y5ly6L5rAClh78GAmw1
P3KAM+FSrCFF/AJ9T94dZxnKiOWllPB5/0ntvZWn8FQ09Usv+RS7rlSke1T5e/l6ZPjAiIa0ePch
qX/UuskXcSERnPF9kGmEv+ElCAjz4LUaR4NzXpzjzY92n6UjiBkWR7QdNmEOCEtA/mhUwtMrqMFN
tekUSg5UK/r5bov+HPTJcLBc9cb6PVthSly3JyxVGm0IuzJ2TLEwbmpAMbF4GxDskpE4nkf5zFtS
RiWCoN8IyPCtO61nKF//bXMXUDXGQckXb5eBtZRjWk7DohTOwCfCYYygq/K0K/QogxISAmyrbpsH
HvZ6go6/Bad5hJenFaufBc/5xpka6ckgIdHejzzVNvy59NLIEVbMHwrYNgrfoTZCfDBrypaR24Ew
44r9aE4CwsciyKCIBzUSxNOGIjeUF8ZjWYsRVNGukDshgw99JDFcBNyZ5SLcDCSJkLgVyanY2OwQ
l45hpNVMEDwWutWo24DiF5xm3TCfvir+nCDXFqejxFcnbiZ+vM29puMPfprLZ2INNakHyMmgcgZe
rNx/CVooxpmlc1Lq+3bhIyORJd8DqUgeYVvK0MTBRUIv3/YzCgJpyotkxuTsCiYBMWAu4WYXJmpp
Z2N05MEallUK34/JS9E81S3uuFypOQAIVEpOwDF3UzI9zB+jGx3Hpu1Phbx9qOt7Kvxq2PgLuq+R
X4yc2FFfwMqjaZ/r7ZscAiqop5yV/bMFyH2zuzl2xqMO4lWMpd4Q+JtkBizvsJb2BOtENb1A5CzO
YCdG6/B6QOA4YE0s/e6bA+WbRUbbjsp487shMXcgL5xn2+BNVfI0Da0+4nc/bkK8rwE3sBxRywgy
hxOxCSIwKu4zcBKu4mGRZUYMXcqukoczTGCrkGTjlo5QdT9npKJpT9irFz3aVFuMgTKVQQXNX5Qg
YJZpOz9KoKpZG3BAYaNhNzmcz920a9MGd8lF5WF/T8E3rjQncHA8UPcZ9f/OKDS65mI+qYGMOGT2
d4g/uLtiNjW2wjx9G8TV/m6xeMjb1UuQ2gSu8ZCmagalP9Bc66CBCVyBZEGSbBb1G/pwR733MGAV
QEEhkHJHrqJwICoiGccWsEj4+A4gHC6wApRa/8YXkE0QkbXsQody09IeJ3OU8E/0B/hvGAbQgXFQ
9sZoIdXlvbchcUF9To1/wTRsQ3oMeiKGW9ZE7i17RBVs7Rqn019iDtFQLWDPWeEcamWEwvTPUxAK
VUQ4nX1U4iaRkHCd3kMir5AZfyiCLtXIOC7415NyMBynaHZOKOq6STDrPyvpVKZyMnoMab3FeP1Y
DRoEqAq1dn4MDpG5y9o8Fyl1kOS2BWqFuhI/Ggnrwdn9cskNkwiBu60WnmPmWUSh4NBxfKwJp5uK
LDM+V5yS2XTZhgFVcC4vViK5NWUTy/lcGCjK1rTBhX9NXE6gkH9HBNAjB5hYGZNvAVqrru/dspOv
K0s54A381L04h/o4l6qIV8ymOQvKxmkLFeNZvWiAUdIuJzPXXBKX8vtm8TQu55CkJWct1IimCWPy
KG7F29YugOD7olNxJUDnLPBK1TYVUHRUsoA81eKRo6QI0VemriQBp3sTZ+LqAWHwdNnP6FzVpNT7
OCseoBfQPYyoejKAqmE0rLuY4xtkGxtKjwa5IC8SF+YizOMwSAabyxlQe1e2Wkzlk2aTePuJ2l6S
6qR9ZA+/bG2GyQMaSpOrnhLgzvsbH8ZGBQKEpGxxQ3sb8bYkpqk91Bfw7M7j/pD2dLmTsATjPx/z
mtAHfdDbZ3VYs3zXmGpC3JCeJG59IxekQgdoYQEKNC43JbAIDPkGLEtMhMLdp0+kTrLtR1LrMhJi
cSogs5dsfml5ZODKEPEZAf0AGrBDKAZcBDeQGJ1myqwYRbw5sRvACXQNvbVQJPlml2iCNZYqisyE
ffhUJQrajVnbBZOhCOtFRZHbUWCFLrDfd1n79+s/TsvxWWJJIh3ldhQP8odP5/mUM1mqJ2TsGzEp
zH760r/zXCS1uf5Ql6Q9D651jpYsvIUirTgikqMiRK+8bOBTN/mdq0C93pUGMjJrzF0APVRQUuXJ
F6y1r00Qh1Wg0laKr09hqLAr5lgXseOlDDPTTQgDb/wK2TVkK12U+hv6pddV5YE6LKBlhTSxKn75
qol9Ev0vIwhfvAJaLMCOngv0QA2MAdjgdUFvEOamEPGPjpQNQl3SCMnaD9HvfYdeJgHYkebOhnN1
BlgHnIXW7vc88++lccqP1zSUDLGv2xLhLoVmum8laTbjTNr5U8hjgpC7chmNOfQtc6fs+Zzkv/R6
sM4QmUHkQqT5KujXaCmiT/+ZG2EThE6hr5+GiBojy7viVqi0zahVYl7FSOmGn8vXFU4pbY2h5U1W
zo7NN0cDimvy9FLZauHjVEf6HpmRsxA4jSGIhfVuG4JdczANp5wyxOkfaOxbSSJ3AKOgN9z9J55c
fJDYGLO4wA0NST4C3WN7WhqRwU0yhRdu1Wp2FEgZrIvl5ZLiBzjBKw0tgUuSFj+EJ7KrTfEQPen3
+/uqGrhLNuKJ11VXx/+IyDdmVsq9cBYUJrRtpWHA2pIkzweMkvfD6vzaBbKpwSILJZo0jwDjsP2S
brK9s1LE16R4IKYq1Rd1DdBCJCCF0+DzUeyAmWEBpZWm10wBQXllBdOJVGcrXDRSYjNVvKmooaZZ
CSQvcdh+hrwa6+k4DEn4jd6s+pZwJHRmT8T9r7k4MtrEmiR4upj17rbSaGKvChL/LcpEbdI1Mdgp
S+RT7WPP01ro2OkpzuGpwXuBlOkAoiVEsPnIqtBlU5tStlPmlRJE6QQTLkXrrTYoGtckXhRFL1qm
aHMks4/jX3qmVYMYmZCY9LuzAIep9XdlO6nPer6MV/zQNq2zCGxz1P2o+tHAHmLZH1JGECTjA2vu
fyZIUCvR0jNJRGjPfjXFEPWAssCXP+X6BTjj3uBJ+51Z/hlK/INjGSslU/DRvxSqgx5uB7Idj9Bm
9u4X+JqjSrkRx4MTiQqG3NtPHL+/eoutSS0gqWhi/9BsS3Ys9Ao8cmCikfRmUjoHpzqPZUeFvS9n
d181VgW4X14NBvEkLsG6nJ8CA+9RwIz1wB1L/vtxmGkZOQsrrsFQy9PC2irwB5kMP7m48nL9pDDp
YefVm8F3W8SZBPpCt8t8cQAnUehbuCxY0AOqhrge0uhuJ7rUKmLVfzxf02zwqkvJf8zoWHu/SFC9
GsM6ilShsbh4h8g5VzySpmfKxJRgSoebkUFDyTXCuh7AeJ92PHqQemPDoF5wyHqyO6/9aFkKn8BC
tfdLDedN9UxO+MvydZrczHEfr1fre/3rez2Nm0AQrg7QwJWm9JdMiAovOx13WbH3cHlxgvQFxu72
Xd7VIHjdZHuDag72vpiUgUOiq8tR1pKjbYv+NssKh7FtaRpX9aR4ZsQsiFZnz/UzQ5j4GFrXKRQ6
ib0uWXQAVeM7UCFzI5CwK+n/l4EvIoCb+HN6BRwTUnsZs69fGgZW45GJH01BgZY1nqTcLCNwldt9
Wsdxa0BgJ8rESNNOGWKs4+1yr1jfgfPppkl838hADjJGgDWt5TwvKSnh+5EzXDGsuoBpObvYU3yZ
cyyK6i/w+M3PS13ySDdvMlmWvvibRIOv3YgZn2nOh2YbYrDRQLpuUFLS0xhQ8MHqR+ylgSiuEx3o
4mFlXLkXX3tfbHtyLQbU9j82vYWufBp8XbuhX2xySNRt41DQchAVCuBw3ULmHDH/CHou70DqqlJi
M5yl7kq2XQjAILKJFw+OaFCr87WwA9VysrcXKX0vzGW+DbcpWFmshxJUELh495mUIFN/wiDJT18G
s97eZaTRVbDbG/077G9OsMmlnx26kgkjpZX7zTICOO+iISiGW49N+rqNkHJvt8p3UEaYeSUkx5hO
kA7YC9/aSCAOL1dl3S5/1rajaIiZmKM2vyBlBbmWQElAiBM73+pAw2aeR4oywBPtSvyYgO1oQZtG
mTItBbKffikTvKcsrP2PXyXrquYbR/Uu91Deizs5Gn5AHQbob+tAimb2hs6n9DYDv49bYrLNkxCa
u+VZuhUR67cOwyT9BJJcpu3oPw1uEG99/+SWq+X4fYpoyN//yL7h8nMOm7paMlMeg+Pi1Ca4J58a
QwoL5SvFEwKoXxOGedRAOZG4q/Hss8a8HmU58EIT6iXXX7vJam7MKRhy5KaWtwl0+NUUhQ+IGtb3
fKzYQW6R18H+5pp0VV0TyXGYqYnL1tR01dEfPlyPyinhMhi9oOhkTefEhq6Z/Tasmqroe32MVbvL
bUUh6EB/aqwxp6l2xjIsh7gzauFzc7ZjmydKXDcq/FSWLgCmTtZ/rOD77scjifQAkH4AzuULNqIX
OQ3ptchD/xCbkvvhf9mKIoTmmGA0JW9WX8dHDC3O4bv2WMKUp5oThl2ywSo8pUgBvq7YiWZNyKwV
/SxUv+jyMbFQEBpxIhiRQ6ums+W9ntaXPV4ZkJRcdnxc+nGn5Ew45yCyZDDTqYN8xPrAndHqYDQZ
X1XzTWeicRgGB/9hopVxKWrQ6bE0iT7IBC2d9rCeJ+6Kwxh9zMUWJPgMKtYdu5/ETuIJYuRElLUk
rAYmE2b3rIQpc211qXf9EltgNV1LCXkjJ6PhzzZ7YGbbdTozqeoUd2cJcewZOVA55QzeRyyDnuHB
pzSurgpkPxGoLkdHFVc3cyeHQHCpBQl8BMRfgiEhh/u4IpbLDAPmVD3IlRcHGDpT1yvNImulmz1U
h2T6ZdR69HOFxrQEIG5uO/zrtwslo/DrL8HJhOK++9P1uT+GtC8QBSbCOJBZEx14e3LvKOU1fopS
I1978qC4276GD2K3+dMUmyms7atqHvcqascoNWISRjNkt0AFCgU1VydbWs0cmX+3phLuk+4KTbxA
Z637XEIveVN3L+zn2e57jPzl15ENniKMdPkK9IhJ4ybtfswv04VpKcKELDyfT09f31MrzblxQS+l
nBvkO4qi2f4BbxlZRldvT836ZQ1DsaUKuHTYb2VlVnGMcrAXgytS/p7SPoQqadSJ5vaGLpAulQiZ
1sWoLMrTf5+kqq286DUcvKcj7Fj7O1HoOdkKFmwLx91t7kx57bm6KfFhN4H1x5x3nY78eITS2bRW
f3r+9QhJzfPPrJVaWuIlRs7vtviZioEL6LaVwThBDTR3k6nWnzFjnKJk9PWmTr1wOtzhyYrhzrD+
08rdoWm/tDnPWNgoU+R+aNKTDw97+fRXHS7f18in7gK5O/sGPYAlhVLhtFp1D5dwnqiu0wq6AU4+
/ooH5jnXkvOkyaW2bu9DKv1ZrPiyJ1cPuR35kFv2OEv7nAUPdfPkWrc4462YPzI2ZzXL19TQgupb
w2BTnyMmYAYZKLQ9/e52tJrSbvJFyTpPBlcadRTKRNLRUkt80HeIxpZO2zZaKFE+5s5AAn13bBhJ
WYfiemc78kxoU+CFaajy+cPG7UwozHrejCB6oC6Fjux9pT98R5Vg/2bchbMXZlXp0YlJISVJ017U
ifxF4frEDAhMHO1vVa7rEGa4Pr1WRL3nC78blzGkm6t7zOsLlucfUBseOz0/ADFRumKUB1ut+PrR
zAZiuhs76a9gJ+ARa1Tkt0JXpb4450jNiCV4uIbjGqVqBM0Cl4cg5RMeCVodNdM/zcVCrN5QJE8n
uxiVrf4PK6JwIsAvhavLR3GrzpB6cDOMOowDUjABlpP4QvjNfTspRNYRkG+6Huf8VjwtF/KVBspt
1MxG0YGUpjmvNkO2QK+h5NHJd4jcdsbFhylondw3lob8Priwm1K8rVJvrlRykTcDxX5yhrwlYYxR
KgcBScQpUNVPCaQPR1VoXNJjDXHxO71yEuWSgqeGvvzPGKf+gRFOmnb8mnSol9Gc3UgoJYcGoWSh
gO0WsB/OkAJkou3Nt7JWnlL0Hrikl0ezgOV1Tcdj3ZMed3qtl41mVPlecIg0M3DGl3+phlJ1Am29
AlHLCpIsazV2uYEmXRbv5OsvxHlIDWvHwWbBan3AmsAmZ0CYFe3zulW28fqi8UcHEG2wECDVJULv
Qyin/PA6TBxnQSfdtUTPExIDvXt2O6Iy6LDwCMUkUWsB8TTmliBYhKNG2fM55sVefy4nD76j6a9e
kgxAuEJhXvjkJqRO8GsyNo02jtS696SqiIxxp6WEHHF8nyOELuvD8JTvzj27y5HK1Polcd4edrCM
noQueAc5VusJK12IW6+E0jcxnQFVwZAPbZjeu5ifg+FHUTXmaiVT3Furpi4zfReShv6xL8qpJvQf
fjpQlFceZIZpcsuuWgOXAvemrye/l53yMqbmTLj42leOO62heQWWUvmDH5jcaXYYwHQ0SAMviuIP
MY2eDC/pw4tBP1CN1x0weD5XkmFNqcQS/wzdGOBZw5iufKiUQ+LK8mU9LYYoF1x6M/L4hiLfkffq
jxPLYRxgGbnLSYgN9qAHQGdTfz/QY0XZimAWALuUJegwnfIY4EaaImtg0z5Y0QwqkXya8A4AWapK
HbOYQrbCRYDXVQfeLmm+hgbcoNBIv4iKjGvsZ4cc+V5JhZo5yhMxUq0DEItXqxq3BjLOY9DcfKa+
UdDwaG4nB86POe3rpDKgebmQYc1vcY34dl2LBoyUepUXHMc3o73pVFGP6YHaiHxCFltZGWSs37/4
5j0OyXxVtFBdG3t6YbNWIFZC9aprXChp8ul62H9ddnaihbC47998oQ6fWkUqnvpysah8Mygu4Ml3
1X4pLVy0y1buxPMCyb7Lm9afwKPxrK5acTn6RDZeNWBD8UEVYHmRcj82DKZl63XuMGqTeoAx/Aao
wJA7G9z6/spo6SMzfxFK4npMphd5SChrcy5V3/uXbd0J6jKklANQ5F/5d0uTklGVNtoJa6He1cQp
UB5CF0NqEtlKrUr1c1OnOaFYENRat3Q1Q+FiRQxEHwNtNKGWaCrKn992dmDEPxPdojF0RYTfAJ7w
PWEWACvbCkVzgYqGvNyi/ZW8FmLwWDogtCEeGhvXErhqb47Npf81EIBkhTMmxOE8VmYrCmlbbHL5
2R2MywXydUXOrIZ7ucTSqRw56CkTxDMpqUZ7e6xe7E0TLUmugF5yZZrpwiaCjcjJxTTHcQBeXXPo
I+xkkL9wQbaE2rKdRaTi9yRT5x90DYpCo2vOZvTuLFS2w10dg3+GyH287fXWRJ6EIJMDKJ7uRMnJ
mTDpknBCkG5fTjKZyLzaRDIpxrc2YClYZdWE4eC8I3U5J+ORFb2txMrAwHCGfQ9hWFUVujLFrM1I
6JpoLIixbVlv5V7QBtVg6L7Hv+QhjaMW65XAH6reeOBKuGaDQHyVxYvaoviuGhAI2RpuiIqdhNL4
6yH35Xbb9wNdr6abTuVC3u8uiNM192FV05xKcSmZfpGmDaH2guBLYbijmxwSTOSkgARjv5PKDFQd
8Ffn29glR17J3O18kUXYiTVMNkTfoameuE4qm+8SQUvnVSEf+Q7tv3Q87IggKSKIQ3jzK5iyOQ5W
uzMADm/mRWhgccmmcwpN12n7aCfI8IzH0X/QR9dfyUgDUTPZ8HUHknkE5AZzor8Z7hPIQ87NY4Gt
El35VQ3PGTmHpyoUsWXkcBhHmpSROVLVzyIHpaYKCL4iQSKCwyrqu/CejKeDhUFTIkDabhJjRU1R
ZKETikd5LGbRuiIgqvGzRJ33CMUGVcv9EAWFWTY54XP40gkTWnjQ/iugXs+9lNohgblZ7l25bbAd
hCSxxo5hbim+Cu1U/ucBrr57YmaNb7ujJGnuA8Lrm8P4r+sDvSsIAE/rhDYee14EvZRTK6e7ADOb
if7xJKvNk8zXz+fkogqRePIPrj7vv5Zr8EWuMXKe7Z5XE4iVdLLPp8N4fnHSYskFQoehYsxEpFCf
mDDkOVSyb2uIC1hNE9U4UywyqbSsRx+dNiHM1Unx4uY/3Lg+rnDmHcbGPFJYflzDDkYNwBt7B4rD
ufxFGeE5jCD46RrPagNYhqCwbQm8K3/XzsYwgEgwFZDf/COIMdR/FMMXWFCKgLUmV7KxtaiA7sv0
8SnAAsJYDLWLI+VtVk2+wpAI+hRbV51QoWVLvXurQEKV70nG1Yx5TpiVb+SbGwSvZUdmJDonEJ0e
UVySthf5oPGR1HT9s6mXEnkNMBU76n7qfXFDPyXfWtTOlhiwDXinyIrfw/IPvn27Ii0oeikLC3ZH
wDHRvFaVnVTbcEiyPbp8qQus89ogbULN8lUny2so6arMbQngpZV2YfyMTUhNOsxQNV0Bomw19nCH
Qqt3MMh23YIlmoh6CXxcJq6sjIAl0zVY5hlIHKB5cp9yqzllOlwrVyrUj9NhKB0TaBD8RGgVRUD7
YZ5M2qietuIXJ/hZnzuk9NPQYEt1wqwn0ShePCi8H5JXztG7JlFs1SidSDLrdK2C4p8jrzDs5awY
x60/Ooe8HcCp2zffZot7+tnbGJ98Lu2bhEZvFdAshvxaNaWo6ukthzwf2GMtrWMZeg4A9C9B2wt1
TBGwYcuubp4TAFEg0Sn3MJkG+k+wf6WOghVsWuXJjInaRNTVCHpvsoHNMo8sj7YuS2YaFrdsiCtA
vpWKAo4LpNjyQwqDNOT6FgODjWZSjVamaWCIog/mCEC9CP7a6sZQ0189LR8nV8ypkgaHUDbdeufO
jG6fd0XblpG1445EkE7Sg6bqq/w8lUhOlM/Q18shjK7gxNnNh1zg2pN2+BfuPDKjkSMzV6H1Fp/D
WAiVJoX8tmws2PIhnmyHqbc6CTMM7i01qrTYUhgibRK70qpMOLwXnSMQBiVih5uFYey6Vu9yWdMV
0kuObOmdKNo05/RISH5nBuCeTTUMiV1h6qT/z59Uor3PCY8nzYwrbiOYTp8BQfejHFA/A4gCquni
vRf9lTJXJMdhy7d3svmENI5AhLT4RDt/taJtmDK1h/5aV1PzX4pHWSsEnXAmBtZ3j8rCqCurZKDu
XqKi4pIl07esIyCSryaDc41bP+Fn0RmzZHyvdE2ndsuGTyX3vKvesbt9fZ69t89+v8ZzHDUfCOs6
tg5rEK8RthfsMc29codkLRXmVNlF+fpxyevDnSX68WDiy6vqs4KiegO7+TuLg+LZ03BgWm4+G/e5
/FnvwTVuZrEEz/0MIa7y7QFGgFbdnqnT5GZ2JjCrdQMLe4drC5M6bjwwXMBzAsAtlRwlsc9K90Ug
niC5sfZPip0tS9Y4Z1xQwEhxZBkPsdqx6wGc0rpGKA39DfOFHu8L+tfdzMqkWYZUn/EfagGBeqwt
JyS5N6f9eu6BY7ghFbkFZJy6alCnDctpdqrETO0ioeIWMgNbalapfgctbRkoJtOgZ/owm4OZKbo2
auloch7oDVPf6mbrKi5aHVvQHZWmFTdBA405YxXh3TNpVYVA3MB2F2P1mnWidwsct0hZGOFivoIb
i5x4CTbXtH7WHh0DZ1N3PoX5iahaPAvnVV5YpbH8SCooXp651MWIAULcZOX7w11x+QsivEQfEte/
F2E9HI5NFTSzONvgPQxl5UP9jaQXi/daczXxO+WViBVejfERyzeOs5FJtgSrDUgXhwrrlZvcROsj
Ho5xo3VmeF3HiuU2gwLoZQfNjz7r/jKEFWjMWz68HJ2m867q8G94Klnjd7jjuWKE/OTJrkjhxdvZ
xJdWr7DvjB6nq5zOg67DBaeWbmmcoGGwxSi138XEY5Oa0huTYkw5bFdVfUZjdVIW9X4E1kvp8AMS
cHqSPBx07jBLwplmVnoRKCpF4OfUs95LSZ92vmEp2M6uMgVKL1ZDFbCjZExNh9Awj6B6FYQgtqn1
afIkYu45YmJWDM61kUhOj3t+b7YIBCONuijw0JaXej8Sv7krAp9nINErqW5FnTaAenNMPc3TvEpD
B9qHo2jkIMewMqXepvRP12sNGHIyEmM58vyJ4HR8bXa6jm4kQ4M+kwoET9KqGz+qbkLYXpJKpf2j
f4/Q7OF6vmC+/7UnvojQCGbR7sLdwhb4kvbc+TOibNBnaCYF9VB2+UbI+7g4QdPPLLe5HrbksiGu
LsOMg/eX9OxBq11T/lSn1V5pUTabMDpB7grWazlavCakck/rtsICTAg3KHLrLwAJJfN5ESGDE7kx
XyTTxzWUMgHubjB9fGC71jkvrK7mm04XBALtjxaP8AgKqHbr13zCyFDu3PVPntIdM6JC9F/fzSWA
MrakxOynIl93UfnDjg10Hhj+ERqQ8Eak0Ayj+I1Ibx+2IHvAL67mOD5TdRvZ6wr5QR74wZoCq6xn
kCvGJQVWVO5iQ8GoNxdZWhj5AoBASmwuzPq8ktLf/Yvjyxsjcd1W1c/6AHMmpY2U3mu3yTNjErv1
lI5skHxy4wLfMXS+fcyE+qcGGw/nKBd2Pz2EGif7Qd2Zs8fsVtGG8fy0agY1V5gqWzDCTb3ZuJFO
8OvUno7mhXohZeMvWs9mPbteb0j/X4/VEqfrcUBpzFsszO6rbFevRHfAFEJ0jtxHL1bJpp3vy6qJ
umzP1sO1VanxB6dIO2Gh+tPa5nHOIUEkm5AfZJmarfSMu1e+kjkDI5pzwBgSP4vuguf4cXWupqet
EBdM552mNQwJ8MehjrQSDZJNqEnccGVEiGuBy8LXGdohHQrqa3SenLuT8/GIPgUHX0Re1tv70OZU
atyCS3em37lUMr9hwZVndYTTlohHcOkisk0H/CWYrgRTrsb/PMlg49tZjFQE73D6sILzrxHikNR6
J77O0RvEDg2wuMfrnR/a6GD0IVBYDHCIsctjM8RGoccvAaZW88+putar84SaMIvwLHtPng6MPAVA
VWtlsX70wyD2NctVj9Zn+7NlY2uXPSgIFwt/PrMtFbc+xVHq+Ghj3S9z3kJjEtXJgXcTDj10w9w3
jXzh8w4lsJKrEy2xfSkZcVzynCeGDDcYCWfLhNYN8NzswZEw0T8pUsG+mnyI/v1/HN/TRJN7BVrD
U9aQtoov8GKp+ZCz+D51l7Ik7/w5vCXXPFvoPNQDDRVj1eYZQ6DB5p1p2aPHc3uPapXA1Oq3YMlm
y+F9emuzqjIteiip+KmEBHWwfRx1aKUhDsYqpdfo0TVwCpZV7t4buJ9umrMF/LHOnbUbWIFedR3x
0XaqR8csx8OLrAJdxtaqrko7c757PRU+lney+Qy7gXfYQL09GqeoikN3+X4lpWnr2Inhd5UOfMGl
wrPW//x+4nXYbkXG9be362q+A8Ahic7Hw7pXk/cUIl0Xs6MHcvx2va1tEwHnZvjno6m3iYm6ISLq
5CzbZ/Gmj8P6+shvKX61BwJi09QWmBCvkuFd9nDNUNXnd2EviB7pKmqPIqy6QbsH7JkMSNgMJ9ov
8w5dYBuLSaI2Gd3TeWomSP97lgTVFeOX94avqdJrmzwfF5b9SndSCP/CYPknNDj8CVZY8LCsLosz
+IFduSJgMntEWqBIzOCaTvEa8YF7TPTmMpwVJDN8s9B++vDZ0ir2WRg3+mI7cgiz44nWVoR7N+Bk
9hw70wlTkSkeyhLGFteMvNKYG+jRPO08/5QUvqFK/K915rArrZGKcgrNNkeebt5zhMwlndaOJc2T
lKOHf4CIonCVV9Kd301csAzCPVqemfPJmqNrRAuZa46AXeV0e9V1XP+o03HVoLDkdQ9KyYiopkn6
46uyoHCPXAhDL/V8JGIBLEhxIwkNFss/sPLy0nvUEi9+nfAvOqsJkuBtayU73vjOJwGaA/gB3hon
xsudopb9UvoJ/CtbKIEMDvtL7BF9HM0gsmp7iHUeipjU3WRiz1RMzRVUibkhQHWBw4IT4D9NKfyS
NSq7EXNhE0gQUpzlz7J5naOiL5Fqe367EgppJxBCRJu7py0wbwor8IudLqyIkTELSyCO+XJU8zR7
Vo5BvgzjVgcKw4ebbIBxw4sJiYkEfdFfw9/JBjSQzOZsKRZdivrRslFzwY73Wy+Y/eIh0y5kksdT
Ae/vOe4xM1Mtg9WbzpBxX1FrKSYFAoNZf4tvH+0kMfQCY/i5YzVe29NgrJX0hHnKKBciI0p7NlM3
5f1X/1o5ycpDRqdkdpN/mX92TS/IDXODQtwbKD/CzX1Y73HZ7oWxs6NYMEJRnWusHTncA8Q25SMJ
0iEZFzuTemyfKUbgjMYLbNWHmW9glBHvkcdu51Zam9D5ZCIPLr6UWh8DIeCbEKOe2Be+YGhJejEQ
4IGp5SZhcrb3X/4roOIgoq4hUqzhXnuNJTipEtDwMJ5r5w4cjsUaQNkoMHNmw532GElVtSsBm2sr
9JNEZQu+8tFOx7Z1HQaiQPNlcscYAZjD/npvgXRX++HQWqLzVvkEhflYW7QgE+hutQLV6jYb8eiC
shPraEhH1XE6M1rc0/ID8OFALSjQQU3nFnI3SgbSGULK0U2cgVDp8DBOArp7jZPp4+MKyyDGDABq
xSYMTlVylBZ4E5Q0LpmTBNS/r5K1tfyu7oHBnbiJ5L5R+GyYyhKqtmEecLFTtzacr7OS0Bueb0Iy
/HYB92o4LfGLV7/iuSGaIMatb8H/tQ90Np1rRt/UtiRRduxyEKZVcDZl8bJz7iEx9PDWTCq7BrPp
Zqv98i/G5yimQb8AMQyPTnUGG0+xHhNQEk6DHx0oZsKSeB/tRjhcBNxmC+v5I6GqkYb3QtPS4+Q4
eF4cFWCQb+tEZVNsvXUVlnS/59TGeSV9ersTZbAKwHXXpRnusbZyECNxedeT4zboOWgCbVfj/cE0
Qe8Jjp6688tQzfntaL9DOofLNl942Dx1eE/xdyzm/gxGnu4ZzcTWNqyo+0/QT+cq/qRqRaGuSvlr
GeGO1VQ6xcQ9xOFQoRjU3tRgRmYdAqw67iHZGKk2irGFlyT+SlReZwtj4l6rsqm40FWNNLu/E66I
Mb54IaKQH+X3Wn3tb76em3abnF5fom3Xr8LkBHS5hK3VUi4jfQZzIZc31ovsRRltwKjmAheTVBs1
hSg4sJKl5zDvbXO7MAhHauRSiDBW3yvrds5oXODYIEMv5lZMm4Px+QmZifbuyFIbpQ9EsT3R9tPZ
iKOgH9fepmUgU0wNNciBfjVy/o/E3TyGItUsGRJf4RtBTFohAwukzVqzarxaPa6oiDDCeT7KTcgN
Ans+nnnqMt5uZalGIHefLHg7yAqtUqnV+qedskP+BtxFnWqrhl6fDaeeGcRBVEeSXjqulm1KmYCg
UKi5XUsEYSxjXn89m77TLl4pPYaajb9rdciOK/bZB21HkUjD63dC1HrRn9ntRw2zjghm1sgxO/d6
KwqStP8hHPPAmGmsVjDtiktcmyrxDbTbTQ9pl+V5VFSUIxRUXi71oiMK2r4B6AkCrb2YvE2hRf1F
EqkbLLrdYqxagXJaBKPi9KEOtD++RRSR/pwVQzbz2hmjKGEZlimLHivwaA4tAEgA5q1QxRy6wqQK
W/UvD4NRYvAnkorZWiROBqwwYoC6o/Ya+X2nzA49pax/kahhsKyQt0iqaIFWtAnkhTcKRMJlrGle
WCdif8fTuVLEO9Ki8uGAGavapbMmoQIbkX3/XltvMwGnB9zzeARLLkRoUiMmrkQ9oqc3ptlTNDuR
j6dHkc1to6Gj/J+pye1mQS936XgvFNIZTFUeRVyWkNol6wTA/3g6R5dG3XU9rQK/SueNWQSDH6gJ
HFzFQHO6K2YlyPY+pj3rufRmfxjzLrpd4WMn9i4aRn779GEygtnW1Cv4svg6+c5/0foLVJetaakn
LsfUEY6CoxSg8cblbcryHLmy9Dtz1jK0ExjPyK5nOmh67i+Ev74VCmWImnPImpYCNMkXWcSwE/cf
kn2l/HLHkuVEJ4eg62n0Vc/KmJ7smiOFeEj/rnde+HjNJj0CdvI0oGRryqvfSiFwaEdBmI7H+lzZ
z3t5EL2wBD4AFK7sFQYkCKeSrDz4L7kmRQm3HzY8GGVycqiv8MFNq2YBVLL8jw/MqzkzxHEba252
nd/6YFufrB74eQyP7yrqgTO2DLU0I7jfbOo4EIT/2nlvxJBtFl4i/DrO3FWyfPuVCREl3/Wk4KNv
hbuw1ycnnIuu1YriUPoyxrQMulRTHaoKvG/Qmm6LEHGDf1SsfVa6X0c2GQ9FFD9SQDXTi/kaCoO2
0dxdCPVyVIq0Q9BlkTLZgjGY5cyqeztbDWJWrFtGvRQzTjLLt74c6hg2+xMLxUWFgxzzEEmfl8z1
TbStoO6ZZDXoYWKUtCGZxxntYe411wfeiuSKiFekF0szQB3jN12MPJUHy9mpu6UmOTCPlVFpmk1K
1AJxA3jleGkMMmvmEPr0lKbe1G6c+GKIj7rtGbsNXbE0Hikfpe7WcNn3i1uoRouVCukx9JBQb50G
2+tfF+KOVeRApTjek46Flf4OfPEmmZc4pa4Cnx07ULHWUcUEUjkipHrLHOpXjLJQ00wu6OPjvD9r
bOc032JIhUeea8miNzF9pLbWFzEXRxqclhBr5XiYxxQ4/NFbWlElwnwBdp0q2Tsp1uamiQv3hs/Y
288d+6ygGSnRsbpt3qTu8Xcv788TUy5k9Kc6pudmeR9Ka4zhPAw0pzjN/A2/3CexZcMmK6+5KaVO
5djTWqAAO7pCHGIi337jvSm6XruR9/FJeLK7JqhH/VheQwC+jwIjfnf8JDxc7TigVZ6Z0qVueNZ/
Gl/+b9kWO87qkoFHWCjwJ69Y7W4Hz2x+5d6xMZ57kQQaXrolNavu4UqZHR3qDWFzgqZBus9VDhVe
dSWLeZicfk9sCRsKLPSCMnNUUdmO62AOHdQkiv2wKWNF4jyA2uy4sy8dCTxefHx1MPrSM09LGTm1
fG01ut3+ezKSncgGkrbX24JwsXqJ7NmGyKjNd7iEi2Z3bb8YqAvgcEbhQSIWqR0Cc/3TpCxNYLo6
Ljt990OhuhiX2YnVxbXDqNfJOM6QxMyJvqWJHXs9sP/EcSbflWXDXlZhvpuiZSlmo8OEUL20IVaa
FDx9EAFGCB6cD+OBEVJNG4TaPgRooCHxqEUgWTv0RnZzR5cUpQPCG8Sv1cpD3B3DzBf2TyDmu1G8
3X54D9l1V7Eo3q6N+w9NcqVHAvwnEgPs9Nat9f+gthLbgwrq3H4C5Fou0H7UTDmECfkPdYR+q1W+
3nktFRIROgoAbs7S0hQioVcCPoN7xxhm5wbp8jpeKZdJROa/p5uA/QWBrIyqutYQVEU/joVS2FcJ
OW7mkPxSfKv32uYK1JjhmdV9W6/y5u68wbZjKN0DG3vcxSMoPJwDbaxaiPPS8Bex4I+DvK9ijbs5
7Hy8/Ep3TdY/TuYRzBAH7ZJ/Wn0/w+XLgKmaYMKGxnL0bwbiu3sVNalx/kELxY0atX+EkDYNtsiP
U8kyXHIuGCKl0EtxB3jMdfjyuT9GlksC/eCvVpoBvAQ4ufxsrFC4ia/W6fyHdLNux1irqZOfD7s+
Hs8qiDOGd3evDlq22PLtm5gn+xUE/uibI0O9HBpNqICn+EdwYvtAXOKYnYkiNZK4lMrPGXAE/wK0
+WqU45Qk6h/1YmKLj0a+Zr2swPxpcDOuxnxRaDxvT9PCJ0fELxoGZ/ON8ijX+xq+Sap9hCCGgYbn
/2JOKrWal7X9j7Jlk8qvUG0aTQgedxAILrTPVQWKcKeT6eY7CJf2tJkOV/BFU7Ez7bPFGsr8qEjE
TvXutJtGJ5LEI2cmEX/H7KlYBwG/lsMzq1vnrIEiQqzCCRubet8aJq6UaEuLvm/goUyD9s+Acfzb
uUQenABjX2xw4fPtvPw3XK6D8kNPz+IYN/QcI3DOtcLZoGK1bx+38HZvVAiyy5I6pIwBi+VkszYE
dPuxNHvfDDCsNBNXpeuw+LBjMI7TM77LO5y+ssh6fM8YVwA7MUBLaae6ToN3ie2u0qx/M3S335km
Mw+e+019yssnE3wb4JP+DiHCIiS0MvUYRCl+g7DxnVAidxpqbHy6mr9IZPrRWaqUJwwd0WNeCVUU
Wc9vE34bux5FoKfW15FHhLLEW34rgb8bAjbdp0cvoAbXZYb6/HVnxCWDCKRTQiV6uXG9u0YHxQte
APnIVPxmqPouDe9hqB5WmkgR0zkJwn8RaLCwq8VXUzYD6+9MEfkEk6CN+s18u0o23ayy13kfwRyZ
FequXWmFx3isaecJX7N8oA8DclR2xeYTmH8RqdblMfC0kgd/DVhfPnJtpeLdrAyOs5TJu0/bU0Aq
12URKfDUC1DSLjkrj1mVJuXhTyWWbp6oS0aLZ+AiBfEDcDIG0QHAZTK8oeMgEjHhhXBNM26lDpBE
x+0opKYIkpsfGOf1KMc99/QyEGSWyOx2EYoIWxELXo7wyr5VYhG49DLuQgJ8Ro9FQorZkNYrphGn
TCcBMhOi0A2psJFjx4kThOqkP7gmsZJZkBdaRGkBXF+RtVnSK3YaiYzpSyM4qWQ1e/bm5p0Edjta
Rk1Nhd/ORcdpe3mj8kGb8LoHaJi94qNzM/FxH1Ljxcx8WH0un+pSaFH287gI/v+mhhkVs6yu71Ld
F2wbINg0ZKtgmx7g2vKkQKU+Lbp4Hms/oDVibsbS561g9ALvuz/EuS9DYte6piHYD2nlIs/vVjEH
p4uAzFL9zZ/DJqPLXhSxTxGviVNS+FS2TtI7/MiuvH+zkLV4ecQlNzw4zTXGaxkc4RCpRi5X0AnF
eOMuqu5UNuWE9dQnO2z9x7vPIgy7oiICfs+2vnBVIMXs67SGxMmjO50JryC/zFPL3In/bHKB6INB
EHt9Hydi+g7sjEZ7YuP+k3mBgNFQqiDTwlj2h71PbHCLhwQCw64XjBgLpSI6tlH2u4ZAOLBtJxdA
XITca6NUSttbQ9Z8uYWBrAVeP7FRttS3FtBN+LW03vGHqI9pN6iY7TgqkFO14obbfmM3EclnkLaP
8+Rpkn+uyiXTt0y+jtTlafxpMxAkXdrqT7tkUc2I8JBsyhNdcUY5pN4ajmBxrajC+CmEqYO/inki
bb+bUzmJoJZSVUemsni+bdhyIodAzKZEdyVu5qjl11TK58PrCrYKYlCBYR/xBDl1aYMULxPQXdZT
RoHCvvwN1wrXUB9GqcHt5AiA/b52cfIl5+CZcUtQkwTvENHRR9tc98SnH6KmZjFKQggWZewSGvf8
75/zZvUgeqXFMfykUfgqDv8bRgUN7ghE+g8bOu6eIi0ZoShd5iz2QtVk7rD7XmY/dK7iUpuAtt+S
mLKkXblTXVd9LX/+dc8c5eDZDwwiUiUoMDbaYpGBnUdSI2FWuH/Iq4NyS/tS5kYiTpJ1n7gy6Evx
bEkdLO3WjxAat4hp/Bd9bgIjo4n+2VYpTnuRXpq6XNMo3eZCcuFgp76lrkvZHnlcmgquhH2ehe2z
aacxtbHt+FzM8lfVcfgRpmUsXvZHbRWzL6uVvMIyDxy6iWmLAPdBzwWh15WjxBzdHtLHW6riMiP3
Ih8V8+t5BAlWEKP57IjTWMEnLtAP/E7xv4qtpYgnCBx4WenRV3BxUGOinLVis3S1ND6Lv9Mkz5e5
CON1ibko6O+xzyG82xwigMbF88CP2SuyNDLPYfR+ag0syFix6XoR17H18VjaJqZmGO9fV3/9I9rC
zN2jyFHtvMqgMbo/bPyT9s4LuOsZ7cKRIqrIoDuu4oN+Uow0b9ZEK8D3XPTdZdALKkNc4Mcf82TY
2MB84MN8+p6bTezA636woiB4sQiSO7m9Djued6Wc3dzu/e3HYp4x1NEzjadEoVEF6diaCLxvWnm0
/H9lHj2yQH2xHx8w2wEITKQ2bSyQp2bl0j0k0ha5dxlacqNAgHpN1vHMChpkiF4el7M/JAUG4ZPU
SfOjyHxJctf7Gwo2Pf6/2FT/wjdOa3QpOMdEetNk4bG1Hc17BZLv0ZSZ3nJ+OGzPIgb3foT620SI
e8tA5Qp8UPwwh500IiaB1jpywhauiinP0HMeP6wkLg5DdqG8Nb4SgAXqu7pMTY+0W1hcSzMhuKPK
5v2tLFWPoZx3Iwd5mNJpW/OrTWteR1ez9olV2183suKz5hriCe+g/7mVh7Uxw2YlAcmp2ALihJd+
5RA24oOgF4jvR0geli12u7hDjLddAk43clgCHcTk8UTDmurctl4wXWZrruZgDSzZN7jF/LBx9MbF
CyTz/rpaW5W1Mg+Ydlp4U0v3ySEzgeWmb77dQ1S2ykkL9j4tAQS2jVEJ+hCMs8/TASdVc0EkE7OK
9IItMx7kONiSydpXOqC7pxmEbgMUBam9BXJvj2ciBeayKhjy7HjiZhn7HeLvhcGWfIX7yajshl5a
etsB6UaBH0s2O0VEkmMOfwteEvxBOxIR+faX0cEg8EHZVpAXTSMsSNJbEDec50KMpSHFqJMerqAp
kZ001Xr/ZPDUiIL60TeQU3ILHZoGesjjLqPofnxyimEbTe1pFC8DF6phNAwZA+5JFQO3mhCxwOr3
z8D0t2oAhxd10PG0cby9TKXO9rPFR7f7maElgpZ1ItUXmUQGVDOczhg+f4P4jnkj3MNDIPxeWzXA
M9gsljOQuTOsRyVWBkUytZ005ChoIDCQX5GLjhEjKwPQg6Ri2YYSW/rHZubhoMS8owyAZEjrylnO
NyzKrE/xGi3dTgerwfr1y6GNXZ1mz5Mo/eMMsXhJnfbHlkCZ7TllaUf3sjX6bkdCTQFjIXJnZ8TQ
V92yPks2CMxRuBhwBZeX5UBCNPEqO6wovSyVCvhDte+MFZgjbCEoOytcPQA3miB9A7bCLCxAKL/C
LSjAlTHv77sEdQsKMmWqW2b7wOCXE+wiVC8HC7Rx1O+DCJHRm1FV22zkS+JTSam86UQdNL2lutRt
ap8zXllNr0i8Knra3TpM/oL/wwj9RuVnLWJY2LEqxntmgb8W5zsvo8OnM0R7wPmNmNwrHJmqQCoE
wb3wlIouAC4pjicYS/Nkg5QOvqhvQLrJzCq7Sk8/yK1qz+T06AR2CQCv77aJ/6e8ptj0vg4qPWu5
0H7PaYsYp3hytoMzIi9dJJ2oNtYBeUWQbU8WW53W2TAxaVP/+FTPHi0Ik8QwV1HVwoYBMssqXVmj
mVWNRMbanJpdujtDB7DeuzRSb0PF6f1VKAFuLy6UrGVgHtykxKCYXIMPd6LmRCCK/vHoI2FKI+g1
ZQpYoGQ8kcDdbNUy8EbHA6NQ2iml4kiG/EccuPd20pwabdB/rHvMBWZu7QdeBQAy0eyH69uLIQA2
sZXCJjTe2o3YtbVFytcYsWHXjwy5bAlYIyuZ2OTmGAlj5p+DYI1lHOijSYTngt3EehSgH6orz+Ku
MG2MrMjrAsnkhaQ2ATVOlFE4sENEzfmWo6k+03ml+6HCm0OwvwmJqb3X6F6TGLBdX3cDbk+UYpZy
UjrkYjmXtPgtkcEu18B/7lxEYP84dIWjSpXY9wDRz/y9dshzuSrlFruDEwuM+j/4QqDCFgbOf8Qd
oURiT2gmTaUs6e3umO1Dkt8184P1tW+sJ4bF8vhIk/b+G0n0aPZcjSKdcdWoD/eztLacRQD4yk+/
ilpRticCExeHyvgtHXYTMymA4gyeI10q4DKcOUuuJ5+46JHpX8kyF4mbdge2aZTjysBGwZX32YnP
4GaBlP3YnjoHdygjgO/wpHi/u5C5p5jLKvu+H+N+/KypHkX28MofRGBiEsM6JA/yiXW0119zVUHt
xF+YwmdqMH3pQS3cmYrQUPtHqPwHuhTfrz365S75GeR8pG34Cd2piCNxcbJUPbsUFV8xMjBEXbOx
rQdzNCqH+GHLtKA/fU9W3UPplQXp3chGw/cDZgLpF+WeaghpQ8/Z3NKnYZnwZ0RjlT//DvuqqZYF
/6/vTFb+uy4O52d6toqp+MF+Ztuolwd98nunWU2qWcxymvw+DwysOZTokHpeLH+pbkjTl6b/pytc
W+iTXbJVPro5HhlhCiKdgHGDtVhMkFV3GdbkvwXqRwbIYZbnFvGwml8lq4UKljLATR+lcCPj9B0b
7NS3ylz0q1KkBVyd0OWHzd06GPSL0IOe00Ptf7CXHSA5u7TE8JO+QWH/b313BJseahsN6wlzuE+H
2/VcmfiR3Kc4c588fovkDVHIkjDYujidYxdYBTJBRjGPDetvkp2HrzlSiI94xusqYGK5ZleOdIlz
qGZwI9xa0JTC0FQkzELeTNo3FAFEcqVqTsoC+fkP/Ca7BgOii1Tov91i7cFnoXxKwrIZKF9P4zyY
T6E32VfbiD6DlFnMJ93XUTQZAmsy6ETn8sZKfom4hJljslW3M2gNdjTpJm9mqweJxXObisl5Snat
+r71PirEenzBZwX9gheb0afofWhMyFP95gMorCu6tjQDhm78dHqXpbABJj28lqC/koFdpa3ynI/V
UTWNMWmc5MNeJ7C15AzqaqGGOXX+oLyUW+cJDNbYABbXfMF8MmgBQ8a/RPtUMeEWweTrjo7fsyHb
+vW6Hn6tgQySmXR98hACuYzQSSR5GS3rVy3yJ+LQPFwQaCnZPLOgMQoPoU+WudMwEtdcaovqJGxS
F5yEpdsuNBUsBI/JuH1swRlGVVb823cd0MjOwyIgu/gIt8xHJLdHm0p+mju1nGxUl5YePjqxBC/h
DJKQHkFsNMts6Aw8NDrEvyu2/f8slYhpOkh9MCw1RcE74sTU10B6CjnupvTfx9QlNaF6HVMfFYEn
OP3T0tAReLeiVSLKrJdnxeX+Pr4tf7Z+Fv+OElnVi5tO/LPtQNzopTLv7FSmDmheCx3zlAVyoRQx
zhrrucbxDkHriXKOYqaugmOLKXf/fXGycSU4p9AFnNOeYcXdbFqDGruU88MkAAj7znT+uNZ9nYS/
C2rb2hdeYCgQC3hjXeriYv5IOr9bF5i30XujsKF21vdLM3h5rZPDmgq+/R7o6KPYdz1/CCoZVoB8
jMY8wiWcTkPkQ++WABz41IyDU7a4Q6kl3gTmT1t0FNszPi9/sEMpntwZXOqWBUwT5pKfg0s/+/7H
zzAxOAkhs/w/ZZg9XbgwDluLC3y829vAMMlI4oVem2IU4V13AYug5jGIHFtoRV8kAv4LiDuIfpnP
UUJTvwWA4Djw8WVQkkh+ayS/Kx1VLqTDCR087YofydU/Ab1xBdfXY8JzcFFO5z8LJQwFHIQYTy+N
HCJQBscRnWWw77IKcfODtHamHXX2dwMRHIoWBVkjCYiPi5FqLFbFFcg05JqcUZss5ixvNqBZrcV5
gNkc5ly4nH0ZG7QJTttDTNFcm3qchZopQI8UH09Ji0yppn7za7oWse3W4m/mDD9cRzbmSXeeBdn5
tYEIo7SjOTY1pIy9opYbLhUend4HMB4ZUWuVssUaTfsD/GYenkx+W71jZ/sldwD0En+GvXoMu7gF
W6O5MBqPJPUKjgqg69C7UkOjXgjNuvi/zTy08T0DlI2eHxpYUenTYgCcHd8caOy4Ap32pJSb+ZNg
zDWDfws6Vp0Nw1LmJs0Y4acmcgjO6SE2HznYNtkajVFU6TsiODxHTTzgaZC/5k84i8JV1DYhAXOw
zGBsrd479eUUobVceWRHZoZyc2QaYOZ9XBdjWy6vjarREVb4SIB0Bs2+rtdsktQ03hgAHpYtCVrI
npATbm0Bn6AmbIb58IaWz9he91q8o4mlQlBMX7QzPYLT1euNYCTwGGp6CY5IzBOsG77fdt5tbkKA
k84nKmn1L4Zv8wd4WqgCaPFIrFJQdO/s1/8+HqmgsOyr638Q0filCVFDJas9jpg3Y3XThxG8tNIO
gjfZTkdN9MkWLzLZH3HLpgrlNtQpP02nqP5Oly3fuRYo/YoEMOuHSKDNc+vbNX3/+KDI+IPX6zJW
eTJud6mYtLggg0gD0FSCH7IfwHVtO00LZGbcjePrZezIPZM4ze7ZZ/OIib3TjD83i4LhmekQ3Bhg
f163kibLQKoM2qev6fx1cVcAm/7s0JHLqmk9Yr7q98D64MLdwRjwGP/MmO01Xsr02jY6DnDkFfZy
/RyZ2XSwUJ9g9bXoB1rOSgw3VOIsb//id7ENAHGqtGs2H3OkTwR4lPN5oCK3J2Rgg0F5DYtikCCx
5utuQA2OWLkg86yOngbe0G84cmmE3wDWWfT+tlti7fSnQwdXNfN+Up/iJraIpspRRKyURiQQbA/t
TwEWeoNcP56ix7Kblf2ce+FY4ZQLIKABUtgzcg7Ys4dzjtEtxzWyxBxc0snEhfw2JtNPlltgyaNq
mbV/uP8Br0KFPNibLax8iV4hpOxO+lmvqJ7So09mwFZL5R14/hE6WwZU1+QmXbqc21iUxqYSxhG3
HJwnXsCIeOvdm8inXC+I7RUGDHaClgUuNAEPh63UJR08l/9jFBOqQcFirlQIqSow8q7LjlLqpLV4
RCdCsRy8PavkuKRjo5sntgKOaYqKidgw5vO01v9xpf+HrodxKLSL3gxYNK31ifTNRvBLDEesRYdj
uL4+aplw30WExes7Ee93ajbaMz9xRyyqE0nxBi6Zk62e/M7gJ3Har7uLtGlfIk1ffH0KU5LDSdyK
OAv4LKSOE5GhR5lUrH1ne9oOin8iB0tHnoSOJhUv8tBF1yzdsOYROqsgv8abZa+fuSQLo+vVB9EQ
vaJVEtkXBmaRLfU3j2ausY+B5EJ4PVR/h4zRyMuBJMBvNi7tvTIKvQhPcfoSI8V1U/l6zFqs66qb
VGVkQc6thso3uOYYSTh29kksAopSdBopS045RX/c+tF3Lo2KVkKH9/oEB1wO4lr4KXzOAqSPYU/U
cNsMGoeJ6MTEGjKABMxxe+MoA7QT0W+7KL4Hx2ZNZZIv1bOgcXSYL6lxMlos539FGgAHffzs2KlJ
uslSKwEbqZmg/ttGq3szqWjjYMY+eriBrNor0TLJbkhMksXRF/qvEYne25WZfzAR4Xmysot421sE
lQE2dDedemcbKfsELwmmhKBrd9g1MU8sjzdWNSJ2j7QJS13DSUtqJComV/NnKgPC93xO6B3cW1Hw
Uod6CFia11zMGoqbntfGNfPND3kjy9wky7+H4PHNaxAASM44NX3sEZ3QrWmfV6BXq9G2kti/SZWQ
aXfqvzMS5MFMqRXDwqXM4ljaEe6nPwNhlDIoh5lrBLgnfPx9UtduxGDm0R2RioGOf3/ybqD1anw2
LMxFBDirTFVOzK3GZVijQKrBp6JyDpbVKlIzb7h71Bjrp/gx14WsCWje/VhTPwoMn8mW37BTlvqk
8vV/n/XzhkE9cqPsdfK15tFc48mK6AnEcoO92vLfa23YhbGp3ZsH/RfW9FJb4CCa1IDSfQ+43mxo
RXJYKzdmIZ9N5dG/flG2uOAF50vyhE0CPs8B0Gzd9lzl0iNtkS+twdZq4ONJBG2u88MHZ2f03HzE
zpAuzzZsKLl8kOHxrNxudcmcIX3jR7Z9zbOjSUGMDqOdBmXkFY/ELojLk8PDa1zCwuLMS6EmAF6X
eQzJ2Y+ssHs17o1+stfsrMg7ve6SfhmSgAo6zBDXcaP6Tw9E1Y/WYMrwdoIFfhPxprQkpw+HoVlX
0hwaSA7J+rcD/eES0yBz8iNHpPYs/ufx0B6MC1Z/jDRPrgQAUBIqaaCFzU0KdQxYgoiaSCPWNKYn
M0Vd9HcOgZhfxwBXSt6lyp5HLI2jLvy2O9K1inbSI2DsKY54VTCmMQJVxVvmXUMa7UYdxI2w95yJ
gIXmc+goHdzenC4FU7eqFxJY5Ux9zg1/A5vzKCPBIl6Go1DeVRG2k12yQbgQ3VZMNW7PFty+1+Q5
esM7BX5Lj5WVfAcMaygoU9UWnRhgkfk2vi/yXKyBzsv9LjiIHbnvdauUUg3oPIE+eUE4/UMVNCGU
HS/zJ4y/ls4eDS4QZkCUEpAfY55gv8gNGwLw16FTRWgIPZrTQRUgpQdH+009rYh5d5oh8eDw2du3
BixY8eKgQ/GXkuap5gbM5nDb7imwHFNfsU70BDJn0+6mzTW2kZQrkJxKjeEtUE4nO5hA72fPRPV8
hmb2iPhg3z88doauhgQCXh1YSiyyaWx5gn7oUkXeAvfceISm/VPwywfmJSZncq7C+yhrmBWBJ9of
EEIYhRrdcwpdDa3bnk52gYgGy/cMp54Rivua3Xija3s76O0v+Y05LSfTZ7s6e5BrKPe8U6yjltAR
EE8p0yYbc7HK12nSg+jT49tmAu+QoGk3sp+Wp79VqS2292K+ZbzPYaKTQT374djDHSu7ywT8Lf82
jHtI7yBvLhG18TASc+0kU5wwNaBle4u9l0B2FZCGJ7xQnYp9blutG/1tk85NswuWvSwF3TmR8ip9
p5ps8hN2vdnyQWlqWrw4egCzy9rdywzuHI4pCBobitNrH393bjiZB6sAjO7bwiXJxiSkhqoGSz1o
sDWRHAbCTOYVlGVpqds96BUVu5IKz67piwWrkaLXSUD5cDlKc85NUaNdBIzEi2oje73L3x9X9Xw0
36bDSKc3/7yoaEnhbFml0pjFgmHaD3fdCQ8vbiSTTjB6+MrCGWHTW/ZvYwYKMZ5eeX2kvxagT+I/
iPoeF860JbJvnF1w7Jd22JwqhQqfLxXASi919cZuZgV/+HJlPqzed/eVscbMJBC4SSFXofPsEkQD
EvmJjBXerCpgY+pWBEEarjRF9120c2w9TdAG/JKZsx/jCalRQSzz18bvqlr7C0NoUscQJ1rnAWkG
6xI2VFl6UHgc0lKepWc8qAwXftVqaeilYKPpYzGGPwEKTRobPYN+JGW7dFpuc6wdtXd3iW/7MBnP
qPigftoo1d26YtB714FR/q3B7Ix7WKG46II26+RCsbGJvWVaa9YTDFmEdFTPcDgy3uybJgyJttBW
+FcMzegDCD+HNNhxhcNx7EtPWlReP76k3Lr7l3RxH+ZybTd0z6v+J8XCuiTCIsrCPlbxgujG+1xb
frd3B8H9jqXlJJJdhuybFdkIQrqd3776iavSEg3xFdCSeGgTALgWiAo9QTE9C9sDPe1urPsla8ni
PZrIUA+x68QwIRXYrJNFV4tv6MJPCKoq0u4sr7xOMT/9+nHtzhhQre7km+0wzcf4144UvoPefSdb
qkGkA34PVjjABvgPzYYnbGsPTs662U6c3kqbtKawOrALQiHdkYH+t90uaOE4SHZK3ZE7CDooyPfq
Z5oCdHENfec9Xbm1NIf5xuvsLmQ8rl712DrEyuJDcUBxHw2mNr8vuqZcyx4j0kJB7fdXEtfRdTIl
MVbb9GuF8bhmVgQ37Jp3thQnniU1fSpEXS+H/LsL13ai+WTdzBOuqj59PWy1fEU+drNI0RK5I9ac
CdDdre4bUCUHpRXISXBh64myvrdcPB2GJGpVWZc3H0YNDst54NmXPuuFvx2jShGP8N3Z5fXMKR1F
J5SJf+t4fT9SrwihBWS4mVS6fHeWH+/b3WUhmSRPHoN1ZG6RJxTir8jLuhZtP85n6wQZdHVj2Xky
GOxb037KgRdk2Uc2+hK4LMJ38kUxudFave6hhcXMlUhornOHNrERdMtdKB6qfpiIb9VV1pW1Yjl5
hdVGsmJxDasYzXR2sTzeGaBVw9lce7HNZOT5PQInnadQRm6OSyH8/OQyaZae39C015naao3Ge/PH
T+m3PVcWziLJWGItdT1GqJD9aKGj/VuxaNof2VFW4mbunL50YyMRZ0chrA/pmlw2EFCLF0xClI+5
wFJ9vnU2q+RbsBeubMTtmi2DhjXeViBy6EOQGLTe4UklHYG4GewAgUUDBPyAbfdocCcAjZ1u16x7
Q+OQ36w/puBSF00imUyETNiDxNtZZukl1e4BaOwzqiMWjd5AN+GjnNqq8WFO6i+SF9syOC5mwKvF
za2qzF5u08lF1/JeQAhEI4pdtJ8cLZS3VlDR8DNGL0MAv3n/AfUCMnbqM8Pb6VRxaT2CBT0+0Q9D
1UG9mVlU4lwz60MlKPxgQC7KC3RsOlg4/BpArMb0z7+jU49Y3L/v0GNxfvIdM/b1OIHI7ErufXuF
wOz/UGv8Ag4/btcvvJwVvH/gy5bJbOc20JIgnN98kTCpj8vWydKirberr4OqDgZSrj4VAueLAz1v
gz0lVWyrDEiSB1Hbj8cYq6JBCiamCjXC6J4r+eEGP/I89PeRuH5qoXE2szlsWATeRZMwGfByxsOP
g/AtfOw5stwO5XMC2gy8I8A4CyVzAzf50ImoS4NriK6wgC/NTMutYrOJUjONwphgoJ87Uq4DRUZX
FrWCkyV5KZz1BXQ/j0x8/UJxL7lcK/r/Jwx5TnsHN/YGN7m6qQyhcnJx57lq8IYi0XJNgYW4/EAJ
A/ekxJ4ml5fcWhCyMHu0SNwL3Eolo8NjPPOm8xI6bq95OqfNShfyRzzHCoq/kMjN0MSmwf3kK6IT
WFcxgvy6c0a7cUCKiDhHYj8v6ON3bnWjCmAHVEvM2s2p3Esu16xCnwpqCoZfZpimaIt4DIFfN3sK
JtEY2nsJkonvIAvi5GxAPbiBMiqzZAlSKLMqZgWUI0JWzZ3H9v/+iRiQRJoanPIWvY54I9lhlRKu
KQyUs8M1RTJrsczNzrEAttMFvoLoeFDrEEGZI1jwLjzk/zaPBBLtRBKVTtH+y8SzMPH/Jxm4Gtto
4SVs7BsKh8ylJOlRo0j7uflNVv8D47L5F2PsyKfEU/nozRhreOSSOzJS3CJDUo3zdxRNxfYFCJVF
VcfnD6dhbSIg2rk5e+bMy7/AUvedhQt+gZuX/Mq3/acUDH231E1PyaqY3QqVud5kZEBbRM+5ehsm
Zf3YrYw8x38kdxNwUJzi0D67cpzj4GTRb+zrxlimuUT1vNdai4V5PGhEh+mbADdj+YD0jL5HJOgW
8Y+30VcuO4Tx4SgqZOqZEkYWTO1vcOykMg9GoAD5H5P/ak59grFnyrspLDfDspTVFM8A+VctU+bl
WkB7FG2OnT0A66g53ztccVuQXAMk8hq7lwD1oEvKCFMjzofUSYbqqKo3ZhTsHSTXdeeGIUO4vbva
AclJFzbZs/CnQq0id0tg8AHOn8CLvoiumjX3737CHxxVAHXdCMHzsgBgSdHVBN05RGQou46GcEnl
MK9OYqeumNHjHd0BI/idz23wKIWy6fmJIVn9fYWe8KjtCr/SDDlYeaXvGgWZord2WzzBS7meuJP5
k/jWlAz2Rid3QX0oVGhZW3TdVL4hOAD+/A1bjOhopZjlDKKyryEXRIMsGPhgZbzzwLyUWmZ3mkbS
2wQ/U0F3+1+tPOMWiomxiSka2zOdFzVSjzeKu1G27WlD1C5dG49Gzueg6t+l/G9CMTPwRu2UypqW
A9JKtOjlEcN1RQ1EA6eZfM7+sR1egNU5ny65v317P5GbOtVc+veNxpZNQE2SJOrsigL0tJviEdH3
xFc0k3XJ6qA03AfEQ9NjfF/9SRQQEX6IolIwOYdI29iP/SOhNoRoGJWB2cIfsCVFXQhZoUxRhuGa
s+rABYlX8jG1NcWEkd9PamCJbOAP6YkYpU7U7Zzp4WL6I3MEpcvt43H7WQ0PXTQOwxMjmqTq8Gqv
iGzpRoYvKhvXWrGCfpxWUfBo13YjP//99bKGER4Exnpmbo8FMJKSy3iN2xRjFWhe8OSNQwSLDtKK
02H2D4R+RGVShe/E7QrzmqoUc/m/dkg2pm2pBecl93SmqsCYPmiYgC4pPFWn2otbnyA/lMthzCbf
4FUGoP8MRn1sNkbAPbHlHuNZMB5VlRHvu3KHpcGMwlVK9fdTXsr1TadhT98A8ZEcEDGPcA8SPidj
0hp36KzA0ohqNct6+8G+Hifx1BtP9xfGniOE3fqXEPjjn1Wl9jP0a/iu+1WfP6uk/B/r7sQ9j77W
I1HqKJdpWwSGxvwhSjwFEB/pr/tXrnQ52R0IEt1LI3UQG16DTti0EVjeIK/INvZYxofHCgPsSr2i
svHvlDEw/8HiYSYmyIjfhGCcnmW7eYcSdJERRIxNYADpYbWSQEgyR/sLcEPWR9GFzYilpQ7tH+uQ
VAO0pRL3pmU3YCT29WK4vKEKD2B4UV8sNcyDY7wDpRsgQedzlbvp7B0Rer9ISH+gIrFI2a2te/gp
QuV7Mm4+r2dGTHuwjYfS04B9H4Exwi/miSTw0Wye1ye3291bBlLPiu1s62NwidPo8g9YgmlpX7r0
vUg7KlFOwK9FbKH5+ZLiuWs2MVlrticgvZLZGrbAoPn9bmCWQ4MvInDHkSYEKliw5/eEzdgPJCZs
TIj5cJ1whkLYjUgyF7l8Xta0eVzkI2jys7/N6sM4gBgx0iGTiu7i2hdJQ17nrCp+noCGiHsecJq3
4ABox6WW5BWmbCS7Z/BTJYRHYAIJcUefdKh6k6dCMKucStDbKW4hsFTsX3BJ/AZiWedoDnPSpu+9
+3Vb6UykG9rgvYzkZXRZRuQKxnVjWGkb2uPR992uVXqCHSghYanHKd383K3pBgqYBGErUqGp2qvT
0HXj+o5VaPKJTV1OebN7n9XQ1jkUsOcK0dJ3C0stNbOO49tyrMOVsBj4vYTUojj1XEnQAyM4CpVg
KTKSjA+4vZvbUmyn8D2ALI1V/XyPgHNk5N/c9RJuP9x5ZDw9c8z6jmqas1KV6V7HV3jmkH3MParG
hPqgNjeTYy9WHjjdd+42hnqqsU3Nwkx91WGgj6Wqq9jCnt2AZuLydsxyPeGcFboV4VnmI3SKQJxZ
y3LanbJ0LjEGJEmc81QC55fbAsElWSGGJqQ7qnQI7QbYhxbzCWrmprFBDS5umK3wHIgzcziQOrES
pb8/D0ytZ2Qq+8u/xHfYTmU2F/Hdk61RPuFEfFibzINC8mMBH+ilDdPX/l4wXwh4KGhzHDJDN6Lc
8xSUBF7UqNeBsDI8VWh+7k5R2q/iTCnYekA9zX085ESvo5wrfuhDARGYNy+7mrZMlW7wo6mmCfNy
kRsd3GX+h8vu6+D4LO5VutstQ1GiN3u8UaRV5R0BUihpg7DWlLdXvzaXM1nT1Wb6YvKKdnILypy7
I7pgFqr3eqdotwv2rUY+aOVrHGBKi+PQrKOyQlWnUYSaqTAFqVlgP9fYvrPX38f1/9sabz5P9Klq
X/upGMBsPLB3c9GIkvRfrqStSXHArBp9C3BPyh6UmEUZfI7XIBzbHKUsI0qkDc2frOdj0TMYVSQX
/6Oa9VWihcMa67WUj4AI+BmPVvJh0CJiOTqWJUTY0uYrpB+bWFPYyPzTcnmvnR7jSfnPMBS0PKfS
noe0PcqixxVAbB33EQwNgOkOI0TRFxvJzKVkPlYmLvi1MJpueiE5XiF2UNiSLsA/omlQZNicteEZ
tOV+KqV+LTrN/VxEjp+wmWdoLExtEewAi+TupP1ClyY55lnLXa5OSgzcF07vNhMvNh9WrmFTyCj9
zs1+78b7xvBcqpjnow9ZLKL9+ZlbiZE0KcJTqflBjFApiJS/5enMgQQeveYdLcOIa+aSVHMzeP97
rEHjkghK6h5iDZQwtcqlMEXrbYxwrIPYHrzl1VSaPp1cxR+p1fiG2BG5YZdhpZcVax9C+X08mGhc
VIIgrIVrpeOPlcZCdVch9CIxVUzbVo8szG/uAf7DBon1LQ08I3iJOTqPSPlAZrpXJzNDoR7Z/x7V
l+NZs6J7cq7CrBFQQ91bS2owHOq/+/nP7Is9OZSvb7bPDpkkJYJtEwJIOjhUEGED7pAOPDaxgDjr
i9CSsQRZC5hKrZx4dm8nurs4/C6waZN7FKAWCAFJMAjCgk8bdgawsXTt0XG4tQgef21zLxVHooNQ
AmhxNA/Iw94dTUdTyA6ByvJPmuwdb8xxSL2aD+31JnfPRNu6dl3Z21/s4Ciz1Bkg9hzFxLVbVYVK
wTSz+krouxJkYMyqPSRB0o8LFooo46vEVdVu+bd0+Qsq5gAFn28wPzuDmvE+CA5FJxzCDTNKkX1e
DmDvyaKZMdL8aiZm8sYJs8OQIKw6oE4AuQOvdANziJvmhfFfVPpYsFryTivAF2FioBcKD0eKF0zj
UIkpLX6uNmmes0GI2YqEX0uq1kipysFN58aVlvJx+rYvWfJ+uyU6GmCik5ncc0v5BEmqchHWOHy7
Uy+enzOTvNWPMXCoF1t550SHael/xR8WeZy6b6RRUzTyLiLJiCpemq2Aybz1+dlor3NkshHWGZQb
Hl9EVq3FFKvSnxEYHPjKM88KBUhFXQf9lsb5yqQEMYY511+wSaOwks2cvOdEAl/ImCJ5z0OEfvBb
Kp5AqdBfQwmNrOsh7UuOT8JdLAn9jE/8o/Swwqr6u3BYyuRw6/3GlK0YO9bvQ2YTiZ7cYPMDfyck
j7YEksn4VUC+teGKtoYyWSgob7hbSYsPAp05rvCgvs2uasm7D2BUsKlXu/vVLCzEHYcICqTD0Z4c
0a+b+huCvqcUlcLtbF5tzNDW44xILul1Mu020Xkl5vEgWa/zFDraSe/a82t3I7aUk6+RDC84+One
KCL7YC2azhU8IrByaG/zb/Z91PwF4eXmhMht91mST5WOfTHF/lS44yqUf8FrJz0dhz+pRjKiepR+
+zJiqyXS2+IJH8MW1UhhtNI38S8NfuirJOpTmIloAPGR3LrAN+HfT/j32sH4n/NjrJd60LdaB2w5
KWbp3RA3FtypAPzWRNmHmN/GhfufeSD/RfWv+g/gWU28GHZKBL37x8OOgqiUVhF2z0HDuW66Wjr1
cOQ9hzkdzzRrdSwyV+/QgxUvIKekrO9xbnwRwTgf/mOk6q5Zb2Mxu7/+sctzx7YPXWdmzykIAr4p
CltIpa03e/IwmiY4tcXrAWP4N18bBD/HJvWx22jcpdJ9+GQ8ODvCXsNLBQOfqWD0JGbfGUvuWcNf
rMabhwg5Uw1F+phjyoJldjeD2BaZSZAD52iM3pT4ff0JOcdq/G7kD/yN9Kc3gTdeeOfNWhZ1fWXE
MjYkc4XT2FNSnvpZe83tW32erzenyavZqRMQ6IsDEH6PSMAj0lyxwCFBcnxuVm0oMCZ6O7jrsA9g
HyW0N2UoM60yAsrTVirhAoHcmXvN0mN+9WJ6deV5nhod9t2/xNEUXt/5lfj404Zwbq1+pI+15M2i
81EFgVUK+Ius6EEn/qnIiJUXXbHeHwgm0HblO7I5Uo5AMriCdqH1SK8Q43498TnoXD4zlcbimwJf
AO9yenPPSz9kSeb0fWfnFsrGIhyXXyyJx2OvVeAhrrXt0iUzWoCg7lA4UKroK2f7E/qokcCEwtxS
62mX0Mbiv+bdY1zYcs2dIN8Ujh8Pc0G4cjyjIrIK1OuDJPGyLpnsr3J5gCFo4+5/Te/omV2oxjVE
7j2QNE8KUy+iXFiCy055ns3Z7nstTGI1T5JlGkoul14eh+1oigD44CU8Kkyvak2Tou/NfhYXBurb
gE+pQHQJDuWMT7nJ+PHBGvDEs7UM4EG0b3vsduSMYM3rMx1bYM1vo6DO4fAMX0R1pTVlhWiKaEmX
DqvOymKrAFAeZfkTrts1N3ym0zr4Q2UPo/iTfAfjONXkRmsqa0qbPeaYIdavF+eHxttUdc+o/Hx9
bK4uiPXbohupphQdrpM0n7o7d9HgAAoCVlYZaSVPnEZC7+BBK5epzbRPz4zlxUIYui509kFhw45Q
4gMzayO6RrZDZEm8bujH1kTTnlRtDnPt5mp+bxv/T8+vstWZxUKrjHfUSBwvBxft3yp0vjMnuNcu
wOsatUr8IhyYUQHrU86goz+OxS9/J1/Ni6TS50UrO86331Mf4EzDZaQPOJNCnEL1JY6LC4c9DMoK
/9RywddoAnJA2iMLz/TOOuIU+WQYi5Z0jIVDlOfu3FfL9A65JkU0BpMqiXO+NxcMEmd7I0yw7aiu
VfOx+Jx10v9dDRC4E98jCLFqvAQdy9PvsDgXgVPnAYWZSDH3mzMC4/oEAT/IWuLONrB1rfMl63kW
oWPsSdYvcUiXPDTG66/UyPPqKDfCHwSBFR8l9ZeVh5vnxOW1dVKHPFgo1/TrQw/4fgfuIpUBl7Q9
7dp2w02AI+qAOj5Cov9l/ktXWkq/LO/pqWkl3NcbszOyHoDs4dRuBMwi9zPxhIOkjh+INXbSEUZA
HOG3JLrqRbLb5au+0m+0G0jAitGr5BzHT+607emcqt4hlXTHMadVrQcpb98EmNVHPAqzungrkwO6
Y9IeA9ZW8k0UZJGVmsyv+e+yhTtBhor46etGNmDkWTrdSGqEhcxrPaJe0vpW9g01yahpLTsijt9S
C+1jcHWR/G1ZcMZN3GnKWVxdFJk0QkMXyAlAbcqxnAvVHlvi1+v3U1+PG+4WndW2Pd7dUk9c+G72
ZmFH7odUTvgY3YiHhVXhtbrMPx5dTbsApfpTAzEP15bQIpF5k1LPHCV/V+5Ia3XzD8VCyT1aV3Q7
Koiihtv0FTl2fBYbOzJNBXTGx4NgQxF3jm/Bunt/rLyPwDadeHnre1LvS8fKNtE2x73+qeB+suOM
gnFd8XL67RdfJVvKYXei9R/Xtg6j9iHbuAGjnY5QddESqcFDrlJSehgrGcE9piDwutzjWvIoFXfb
JrkheaxdzUPxVDdOF9Zot83jWn4KuXV4MfSMma9jFKKimceJ56X92svSCj0AzcMgjvac+d0VJ/2m
/36k5dGxzhnYPQa7rC3/nx2MAXGlWft6QYnVhTug5nBwR0SpL3YMRp9rSPHe5/WT+qg/tHwKZpOi
454tu3rfOn25BWlRBxVYhw5eEsng6fqg4yOktXTlullubflcNp7cDAyJpa400xjB/YE6aLHJYvZh
E/8AUog7IeerKit4rOeXPI7+7rh1LK81QWUP2ABx5bxBtmiX5OnuyYE/0B3cIWlM5wikU6wTrzWX
zu0DpRnhJP+D40F+Z6lF5OYJDbCFu/EzVS2xWQCkWyN9kBxSPAmke0Y1KLYTFO0qGLyqNWM6xnBv
EeZlPnWMzSzh/ptUAgA3nRVTPnsWY2YyuiPCe7qbPqvZRbEAuI9EjFQy5troBibDBBLTT9VNHkRs
fjvuIsOqWJWW9ONp4d1lwSGQoD4GK2Wqm/uqZvA/3ChvPy0cjQVUN0JcxI2urDrVRvwyqeG5uIul
6pt90lMa9ho54O4hhLTZPEUOBWsmq+44m+n8V9cYIojpteDvriT2vuGmHlEdtw/3fLqj9DjyFQd6
aVgsOT0cIwEunNRD8PhMYDHVFNfUDVdKQEslhiT9P3bojCvrV1pmNn+tacsRTew8q4lZkpgMosDC
sP7eGgyYpW2G/Z95ZyyEbP1Ck6YWJt1zq6iOmp3QRD9BbDazBdEq1g5g8csjAWKyOTREreFFBbHA
evK1RKwCCxILHTaSes967Bo/ZpAhxO0WpiLvRnxpxhGv2LPbjZyC6w3Y19IplarZ4IdRjejnQXnv
ztpykSniolqlYSkmuNgjMC2c2qsaBrNUxmXNEIrgtiGpioGOanl/Zuikin2qWWQVcOiyE4rSKyhJ
L346OX8GtYAyCX0PaQuX4oa42lXHcAsIOXxVzKMIj0QUu4GfE6DoelqcEnPqxrqoFoJ0b4kLI8Bc
OFmp5haQRfgm3k4tdH+k+5fUZiNFDKPEYwUoNrNRYnu5FgYPgvte8dJ05J4AgOdd6wtYMJ8Pl1oX
BPGVbSLc8jDRPrzhu4A2rgSCYoxnwyGi9I4G+enuCKntpW6JZqaUNQDhQBoDs/ElmaeeWOe5mnzg
mnJhglxh6qaXUQNEIOj8tL7ldrK4s8wgXuh3t29X/DbRYbb7RUZwTMxDP1IewRC1jbqWjD2i0wAU
pr/jXPOE/qrfN2oBKntsWewcHlPxyK/LHEK6+19HxSZt/UWpMUHrY+CTAeLqCfYIL/vUX3K5czc3
2SRpRK/fHPqRz7z1y7AR/tvSWZTezRYhT1H/NLFhbEPp353CWcGF1OMA3ij+H98HrpS7WxliLSyP
YZQ+UxCEV/jmD0V5U2XJeXWgo0e/MseO0ulMlyJ5J7Dm8wpUoe8Za9UrC0P286AEnjS96APGjtw9
n9kL270LqkRib3w0rTPcdpo1js6UNbNsibtacC6YaqM6RrDDA2WJD0f5ywEOzxFuR6RaSLGhdMmJ
eqaqq7zPMj9qgfG7IoS9ciEJtS2rOdbNTFVEmpoIhmnbznRiSJ2Civ2WayTQLG1MlxnXl04gBOE+
Dv7lkiEj5qRgJeE8OB2ddVhRorA3A9zZpxhqC+5Er/zO2sgojwylQwcTTptfqcaIP698culVYD49
5qRyEHbTTndkW6hJ6RGxTpUq4FbaWWpYlM4fFjPvXPc+EH6P63DVw826VCZrTzIbUO9IZeef9w4O
yCb6PI1tpN0bihaxfFHA/sACCRaaQstZZkOT5zPwj+ybyZ5Hu54eHgn5FFuRL+Jc9FfHvA2f7oUr
cNeCR+V1jYZrXc5o7wSHrn9t/A7htDLOT1ji/jg7ZEfek+zT4+cu9I7thqJl/TkBZ548XRN22bLi
PQ57r+NHZDZlaokPiJf7GHEByGpAtuUFVuU7xuYpu+IgYp94S9JqtJbti8opLfJpXZAHbG04HFdY
+t5JK29tsylCnOuWVgNvCN4vn1r1nJ7+1tgF64yNyV6XHG51FbdszFDWN9qHf/qgEVPxzXpYC7fw
gBOT767qXQXAMYb824nr9tOSNQuD1qz0WcXsMwteOzLrkFg09aYGIEki8i1jPWLJNJ+suTTHV2F4
Bw1cyMisB20cFn1wrEmFI+zMg1Vl1J7SS3ePlzBbbJ3x2cb9x4Z0UjU78wvlxcqUJJvvBVpNP3/s
IZuKR9pZ1bDxvvXsIyHbb2A4u/4cRh0bw0q7nwTki5m9e9VS2Y89NkxxG3o6uw996j7SAdzAWf/D
DL/pZvro5jXz7ljEVv1BRVdrdjes+cUJKKbBLbmdXEsqg6vAGEzyns66HEbPoNm4ZCATqxKOANFy
WBvy4H3GssVlJUPfPURT2715H5k2NKPXkWIho5mvnP29CjSB3wuZPTDFT34DDCtxmJfzJ1RiSLgd
73ayMMgZwx/82pjo3b3SDbIsm5YhStFPXw3SSyBMCEeFkiAPxwR9nrmxq2asTca3xNQYLuZKMkdO
A+E8/eZvR6R0uOKzOFu47V3bWRoON1H57RWkr2P55u3iH05ZcZ1Ldl/SZ4h+HMAF9vAmSCKQ5W3d
EWPMHKnRdk8CVsHAXFRvdjt5vIuDb9IBi7V20zUr9SrHqmRaWpxUpCwVZoXlcJV5fJcwXZNEB8BD
4z1ECavuSXXyAeGMPU3mEcd7j63/7Dhu9kvtbJDpDi3wG8eH2uF8Oo6WjxsvzIIQzJjMyMmiQaZt
V7BiRe7qPoGyp9mqzRHozZDHxmYGkGrXTkT8q1TJEsTIyEiwhm7u0H+2L5atB6sVHMjgk7dpGNOM
R6uySYxoGvIFn3XHHHhvs+86xFR1mL0uYdb10w3TMEoMzbyp3G6xJY0f3xZNVrSvaw4zz87VY3fd
mTrFwq/wm8IJYWvOlMG9K+vOeXihWxdvpnxSJlVRgzGg9KSXkeedUsDNKanDE+U9B/shlNaX9Zqe
wDklsYSvmmNju3DfmtTUeYgEx8UXfMOStaq6QXdDPwEkwUOtNojUFQBzvnZ7MG+CfX3tuvdg36Ts
MLDgDgNb4fKmaH26+c1+69izD3SvHIFhJqyuWFiBezeNUitonsdeIIP0xLG+VE/SqrgCfFXEOoYr
DdNVGM9HTEl+/kEBVWcVeuVaNr7bHdzBP7xfShsrqb5J8ucE3x5kCy/XEy/SVTm8i9lDy9mVdRpQ
ClprefOEQkLT/kAiMavnmdkCaw1SttfzB6vxq7Z4EuGgO6dsQRyRVRfm9ie8m8HR0iFfrLbTNaY1
pQIOYkyUMb68xPPcHkIo5QKhmQt3EPNVGMwTE4cgtFVdNJ3rCEeTOgZdS44eapPxurgqgBUYqIom
Ff4tA68/rDwBXXCaAu5+m0GQ1E5oq7XHBca37wv3pqhyocm+7ceG8k6UoCvjq5uLewB82d+7dYKw
pol44x7BtAlosq0lxNB5SjHaxODRcBRUhUbuW0ROZZLd4W6Pk2YYWYwpJ1E0a7fEKjMufC38sNEy
+0ZsxlXXlLlgdVpOWKbYTJieHG6OjotjA6H5LMBbXpt9GVXr4v3N1xvbnZ0GwhFMmdfZgs5DYOAn
dmWCgl2LcJSk5385nIf+EW81e34SyJ4kLbCCl3zwEfy3v0MuO9AQzEMKgbvT0QOd9AFfhe2IUFJi
fQ4VJax38JO2NA2rRhM5BFTEo9TqIY1D55RioWvQ0w195pbsF5UDc8TbjFhIrsZKppZNGFs9Ad8H
9wQ4vlCFzsgRmvjICHFhD2JxLakLRXH8wsNMWNRyQyeeKMF+2sFkEKwqS9vC83I4HWRhtwZ+z2Bn
wNdogaT4UnkaPnF3y8S+ewdsgFRPGGLS58vHHe36IwcHyTX2uUI25sGao6et8Kny64jzl/vNxTN6
RGn0BXGsKagK+eT6ZFDJzMOBYiRwKC8NlV33RiRX8CHeZE+AQGmz7rXuWYUx1iPC7mP9LKoa80t4
tQdfIej/ySztDIVYoEqRTOUVWtbKM9UyDoPtXC9H/So7Y5p/oKgEqJvNwVTFvStlD/eS70dse+cs
ba3XZxzd1KQyk6Unm75AOocwGCFGdFawiWDPLm9kkR9mF9sEFyb89qldBMwZYDvA+Ozplhn7EEF3
dZlPxZHMXz9FLEUfpWUNK7D/s7ahZPcYWr+p4bHHoJk4YRd+eVeAGo6qfl9cimtIUqpIhEvg7WaT
uvkO/SrLe+wIdr4sJLPxugnv4zz0BPdJG8b2SnswH2F7f6Vs7lCnz7kxm7EaqB4GyiKRseyCe+W/
fmJibtWxun1CEUoGplZPVla35gFVdc0gbRjhIUHA6+RNovZliPXCFNWSTzwzFUQk+36krkr69lcD
XrNMJbecMcAIydcchyQXQ1w6aRKvHH6EHc69OupwTw7lI4cOqhKJpaKP27PTD+mD3DsXz5YadsE9
uzEG5X8KN4YWOd916AlGTzI7WkxTtia9MXJEDKbSBUhIoLH6IiQPHK5NS4pBfUL1VwcJ70F6bEJY
nIwjSvXZ4jAXYHnABUC1un0r6eY6rGvlgNI0luj/gyxONTsVu+uF7Cy1xlW+8y20YWXkt/f/oBal
tUIqAq1JYXcG5NXor1WabZprgi4TT0L/+qpXH2+qe+vwVHLMnu4TNSpvEEumrmLmDAjQm5+Bnn2u
ZHDQ+M2e40x6KSDq9wxqNcMPiYlFHqEjJCm8uGHIk0kv2zlnkkh8TXy8R1ZVlEZGMHRIP4lPEiVW
lvo84PvHAne2HqocC3ByvtBybvWosc1rvlJO8O70gmmrJVFdAvHtYVcZY9gvY+1m3/sbmKRY/m1r
ys2N9KM+aVLcFFQ9KmkqOTFU500ZiADOy2bAlSZLxq9guQDm3teizz6M6vc0+gkNRtBwrKlbtD1V
BEPTD3kBttpMFZzzb3J5to+qZSFU8tye1/qUwy6U5XsMGU4XjpsklGg5KG8z4RRuieOtqomF6anY
uvevPLr7E+q5tdxN4eqE6hBS76xxqY7OmVA/EQxvknVlyXA83fY601fPOCK5C9XvmTLGJp7fasmK
32iGfaUlt1ldFyG1oRSweZ96+67WZqlfiA3E5OvkNHOWttlDjip/uPqMBtwqBzduNPw66caItmWJ
3t25RFEWKlmZnm8UMWILA/0QndJMFZMqyzFov/8ucesaOGhHc4KHxBR/tl7Uj0ZFrKTDDT1ERwMo
YfdLA6wZZlZ0UD2k2vnjA3a4P5V50ZqvKDC1Owtf0LXRCW66lAk5Eoixc+PTIdsxu4WDQ/rir/CF
vfu7bDumAI8PWW4J3s2M/hQ+POxjKKX/DXnGrWNv676zkbxtgdhYC6Ks6QPKmlybnctVmmjhFZE/
KkZRYtAglQ8LIKkQkNaNq/yzifxzffmZNtaq10qPoz8PoO652inSzIP88ReKYGy+26HZbv+rmE/a
OrVHcAP9DoJyvQKPrq6KIjPCmLR9OqN7Z9HKXiDNhUUKkgSMEEYGnRXx7pY8izGrRdl6FGTyU57/
c1CSx+vHn3igUsZtRIaYR9QeVNgj5sWKA5AgxQovO3Ejl9FIp21OrShdxlqKtshfUWpEe0FMtwFB
da2kwRw9k4/guHhQpn8mCHAuI+p0TfYFKRswvtY2D9AvbsaKhtoKF2Gx1KD/UzimW5kympSeAUOU
YRlPPdT7QgTB14hlca9tI05GXqHX5KV7dTc2mshY2We90yLqdECyBHguh96w1iRpNC9GVPuoak6d
H+kLpQCYbovKaraT/0x01aEvl+III3AtvZoNy9n8mi9P+mvVJQ4o2wsRqlM0EKxH5AuVLzworScG
5pBMD3bvn3Rv6MProhPibWr+7PS+AuPvUiRye3rJz481RE5no6T0Ug+tPbPkrMZJ3KZOXsNpvmb8
9JoLK84QGySoHqpPJzyG5dvqP7N+STloz41FQRGV6WOaaWKssENuK8K43aMC0X2BshSYi3setSA1
/WiyTWyT+SI1SAzfczRuzwOndqN/KgTzl9qy7ws+YD7h5n0g2n0xYyb3NM1JC/r/XCT+SwslP1B7
6GzbMZeQ2NnfsVlq95Tbbb7UaIosFzrdlL05UDgu1MYN4apNA3Bgu1LFpLd13bRLJ9SWtAIQ0/M4
sL2Tg+Rb0Nc636lBO4DsbUkzBcfODzH0iH7XH8ZdzKtqBA+uMwMxPLrexgF1pBvTO9DPyeXGWgMB
Ncvrzl5dSf0e/fdyVF+J6tZa7jwjWuKTABkJnlRt/ANuVYQX1yMekDHGIwxHl6uK8wx7Ap1JAymg
eWbFxdPm6+WGmWMskfNGAgdJ3MgOq8IlMHnnbeKuI0+aWqC0lweMbfsx80cMalyMpTeHmNlNg6d6
PfGLW8oaxn1jU60LWp5pvH9OFhteNYZ8O4DiGP8nN21iNOQm0jsMQmmgu2r5P1McyvSuFkX8stbh
0vuEG47MRnFxW2wppOqWtRvvnJuH9dVWR4Y9S8/U6E1kIJ+maRiXxy5Ee4d5OelhFA1JHOuJ9cl8
Os9bBVvbYtDavVRxoMGcIvgDLK4edt8+t9A3Sl1opos6Sd/bdjKpCCeXBy4kbR4kJU/+EW5MR4vY
s59cZ0TctySwc0X10BFjY7odxV3Pnc6xk/eEWUmK88JQX3HAsCCVD0DlKjsK1cAuD8Q3WhW2nwCo
hd8Zv9UzqccL6oeAg+Yg6SIWk9/4zTyNM5MqjiWoqsMNY9XJ16iioxoWoUQuqGazBxj13ePguLGU
A3O/dkyxw9mo3xIMBCoG0mTZ4wWSbpQ818hUQ1P9Hs2AmyvjXXMh2PN4YfP5WSByYgzR+9+iUjE6
FNozlu6FllAWR8JodERlpb3Anonq0bEZKDS0M+Szbr7TYZ8J0SnD3oiosAqr1kvgIq9a8d+3FLNM
XGdgTAzHJCrgE7QC2YLloB9HUAKojkZQWOUoWdx+bvxs0+6a1q1JaSGkVz4HDTjfOdfpEGx0Gmlb
dL5F6b5lQdbGEKfSRh3u2BfqLwImUo/SmQhBF/98Hx5ljVPb1FkS00SawklFkCi+zzXVdx0Ndve+
W3q9GnyUcizfFtObDnT4V7zHME5yxIzP24nnr9um4MUKDE1RKrdSnN5S5eg1f3hjT8tpP7eWHEHC
ibikUJSFPv96HmgN7zBuGFchCwLJY0UABsBmQHPHdJi4UBlCSERyIEKakupYpQlUwLKkgbW7lwzO
e7RTNNGZaVNUwT6KNSap/E7i05hE5TV3GZ5ejewxXnOhzBwh2J5nnWzzjWudLFq7w/UojRG01p1N
wxHqjPSRw8mdoYviymVOGu1pbrWxbs8vykyoiareLMcrb6GXzfWZ4LLVQ/SOVF49hieZ5ZII2ozV
oRLuNsIuOlNtXNyZ3GgBfOqXbBIj0QjepThowzztWSmPgweDRZMKjiiUekpncCa6aC9spUcYj8sl
1raZ9aA//VznFmyIQ8AzgOOH4B1R7BtD1am2zEfd7SmfB7w8srVKjesftaJbR+CAAjog8udsz/+v
p+oA+MEIF/AVarTedqNl+pJXL8rgBGi0uTbwY7Z5Dxkp2v+sUtaFnceljdksWK70wTY63hXc1FGu
bZzI5MCIM/SgKG54JbbwP+StXO8zhYOY2JAf6CWgeoVJe6YKJT9UMRS5ceqI3KAX7uBCz8m+yQQQ
D5W4bQS1ikPFrab74K6MsHns9l4QVg5Wmtd6tJYRapC/PamqPPgxwUAwgzJbTry8k56ecBNWg8yP
elKNlRi5LEB5XmjzrXLsZr/txP6/onHO4yu8o2XzCHCwD3n1dZMMS/MlSxBVLNOA9KoF8vlWag9l
8j8eUuMIVo91LHERX3+gG0RZE5tJK3BZY+bqDgpg2csJdTgRwP0r8bQafgC6EIdFxfR4tU4A8DlS
P/z7fGkow4bNiZ9dPVCRHMjf3AXSKZ5/bR783NPkvicYKj8zk4nt9iaI8pIVK2kslf63WUEBRilC
9LkjuCe+kccu4qdkvPXk7+FDOQ6XiYdCUfr7vlkRBEMXvS66BRChqXlbuS02lthQFQxMPGHhZAIx
6jOubt3md32DvD1WyYCcwhWoAzfckFKcpiKblwa51WQAGuOolt1oYyXkq/25U7mF63ost7Hy8oGw
eJVKFeFNKTUratSLCwJhNTEA8TV6dM4iBf+stNeolefxf5SHBANICwUNj/duCxv78aiFlBDuwUk6
fufLxDqMz5zI09DLYGxzbrgQxx/Nxj/+uFQNEUW7VzuK1v1Zcxsr0+sMSXIk0ngvcWSOzd7J4vUi
14oFVXG6tP81cF5yJPz6oMFg2YnN0irMmUAqfJd2ddeKmM1TuTC75YPccKVnm1ikx65LX8d8hbGh
RSmDr+rRxchCntcsPyOa0OjWYKNw8lYXgZJz1lWr9ZKW4aIdyoIdlSbeTokpGzyIbgeeHKlJgG4a
aEaPCL5mXex6uA7/bBHW59ROFoUjOemuhQ535QExFzcHRF1ffPekYk6TBgfO5iL0mq+Hm9I2Z4T5
RXPYD7Mpj5vGsSL1FmpOLgDfPrQay2eczH2bagdhY70AyWEdtoU9TdCJLHetokm3wrjgxSm/CbpW
TzZjJ5e35CJ0XcNr/v9u2r+wgEkjLt8nL4nSGwt9BIFCNodDsg6dFMeZLOunA/8E4TKZfnq/8335
43zAtrx6ECKPLLR9QVFxpuNw2S2l6qgmvO/SRVb0SJ6YSLmhxUozGRCOP5vNTgmDWr/rxYfilYn+
60wqlH00B86x7uUpVUaR4CG5+awphXIqtgwMAwMQwlA6eqlZBwGoWMhacFH43PrfXgwW+onUrmUl
5PKJSvKt1zHqKl4yY411/RPI3bPGAcI+NcbGjp3scFXZ4PTOESKFZvBFavH+28dla8o/IEmnZ0zL
nyOY19hYVKWjcTcsQ55uou9by2gcKCWCyFcLFK+NIv/ctHMTV/stIpqh4oWckodBDTTeX6w1BuNc
PSqu5ZpiCx+AapmQsF+x0CETXB6GkC1aoN2p14tTHooM2ZmWecn1z44CG7U01Wvlk9/EgqZkBImb
ydk7irUZs3510jLiXAEwIP9UBHGK+WyFlQ1Af1op/rbTGXmd4CL7n03OGL5atlh/f5oSTWji9C8c
n1CYrFtQnRO584GJVJNC2+MzhmuA9USWADsBf0XEZNXOfAK47I6GOEwRtlN3VVigaa3X9fXn8QUG
TDYrxzhoP/liAVu07598tzS5ZSxhaNendydynyxrdbtFrbE1DrSqwfYlooPYRxUDsGXHT67dRLNQ
zHN09Itnhgte5dTY+AYk3rFhwCaiE2TBo1RRRLq7QQwM4oyT7w7TDj6Pif2wy0nmIc5bZKBJV0/r
QkyrLv+EMAQzMhNap5M5e9FE9PieCXUd7yzPvNtlH4SSMHbr4GUc9/QOtYCu5QFrrWHY4J1pL4IE
KEiMGvscdvqAGvNHKBW84v8vMIvDv/lukrzIqymuSw1M7XGUpmRkL1E7VDHJaXudVjVhrH8bTSAs
ozMXaEp4pJ0gcl3MF72qUp5JmqPfHV/2/dHL4CzdPQHIV6RczgoNZ0kYyRMppKhzuGg0LRLAjJmV
LbzlxHYAOVMimnTGWmA8qzMqZ2GH2I0wNEs0+nO56YWeS4qIv9WjA9+d8RdiWkr5LkeFxePCMLzs
WbFnDMM/63ayDwmxqiiVM0HCAN7jhx3edwsMCs4Oumned4HBgDtIIowm2BLMFu8iIyhEvKrvRafE
o2cEeYrgJ2HA/eqvH3h7EEnBb2TMlt4LfbTy+QkiWCjKsw/8gIQkq7mN9oAdTJjnbV7P4yXd4kbz
iac6OP+miIfK0+V4HdADbfJJYJhNJE7ubVBsFainMzWFuzV12caRp/JR3a3nhvTqPcuOhZIxcx0c
S2SaoN3cfnw5Gt5NhXCdatCyWNWQ4RIAAq4GowV2R/1jclS3j1qUsSf4L9gijKr7Ap0gvi6A2wWL
gdEM/Cs+f3pds6GKTMxk2CeSvZW8YeipI6uSweImtO7O+57awCqI9FrejtFFWU2VetQsnHdWY5B6
KK/tmcZzd6c/JUfgXaSb3k5GP+yGwQG+s42RlFbPte4AFk2J76KFh494JKdXaCzWMLtxaAElFzeW
P/KwYneedNDtJ3bJ9dP3rLzu0NC1k1cPaFggMpqViXFo+OItMhb79Yya8pqGxOsfI6vV+oxoq5Pl
YnPzYtSeHcNqOTF5OSsRMEjBCUYq6qz40Fqj7Z2ufZZDaQU5xgZuhUGKMVFGYENraGv6Omq1wGlq
qL3q+M1SEjt3+H+p1Lroe+FqfSGshFEQjI8rR/hN8of99HYnQWE1iNvTQl5/46ivKhUo5j9KeG/d
qBiGUWscEM0NhDOshHXgq3y31HhIAOud6yRj81s5S0tzl2LJczh/WS5w8Qb9LBxXeqcz1jN0JWRi
InGEplEo/GXFYQ3lViQ/RD27B8ifWOZAE+8k/1rrKx1KCzVQzK92wGnbf6wnSQ5KHykt9eM03NAX
Rvdw+WZ7P1puO4kjBU8MpmGTvsv/XUQQsboph/7djqfjUmMqiABx0vBah4h6RgbzcXDtRRQWVF8Y
o+wOWHnaniHiXQUZL0EJEzdDiLbEAmi2GlCQMh4LEVwgno7D1+LfX81RsTqAmhbboIa9Jv+q2Y5w
50JtetllO8k6bAAb7tzLlAAUsiE+nOfcfyUW/1r4Cba/VPN9qBXWAAptOrqIdHz/EhXDcaeJtL8I
PP6CE+VnMbDunfZMhXgLHm/+s52Bvnug8Fe1jB2v3zgoSeWaSUtqsIt3rKQPwnG5Vz2TAtaY7rnV
kurKU3yOR6siuMxkpGktbGC66l4GedC/fGxM9ISTY/IBumPEKyr6ZHTqgMbAiya1kzJxOuU7EzQV
i2QKCfE0aBqS2Q6qApR2QJKvtc7BopknZbVHYhb4lHyVuGNxTNftKQql3rPfnTyfWl7/zibJ7RLo
q38WLLppCf5+as5C/x4J7MedPVaK8Bgsx+oFm5w1pB0vbhQQo7qIRRa9IRIm6Qhf1pnZzShJTwHd
jezBaAHT80pWGWWjlUIWMkkKSJkpY6e80aTigPSoBnqalIfIPE5tlsQ1G/93LePMHZvv3EDZcNkj
1lwhUQfhaFmYZZYrEVzGJb9nPub6ykzV0iIKc4F5JJP2cOLehtx8WSt4NNMI6ZAoWV2K7fzKmswC
6CI4oWyldOIZ4+rz9KzFb+lWD/+Rci7omQWj7oP7HGLfbc/WwZDA+/kqC7VOBC9MtE7UeUGlmcSi
Ea9dMr/tQz2ihAqvvnwQeA+4DymbU2iqstEII1fUoUSFyHqx6dlvFNHpXHdDfbFSj5E9wIoaCmQ4
odNejxl9sFhxfts3735Wy1ZhONrBYSGiS/OxnrXrho8TmDhmA6uYWT83jAECPc7hhdnk16gtMgG0
qzVIbbhOs1uSdz8er0Gg+ZSw/DaZ3NTabz+z4/THIXhj/zX2D001uGIlsQ/som1walk+OuhJ6SoW
cy6c/virEFlTkqes/vNOMaq7zaMvxLdUmw+ojMtqO/1YxKq+xXmp9ZcOEH0EDlHULDPLGo+Joc3q
8rKjtxN6Lry9kN5gEoi8/mbg3lmbUoNdK7AvD4hwYKYS1OZ39mskRBi81wDWTsLKX1hHRtp+qAgK
FCQIbdzFIJLJMd8qbCWj4xQCMtUlsR6DGttOHB4oaRNBr+uxRyRZQe+JaWb0eY1jlZafwpWDLQdQ
ZP4LIzvw69LCtQooZI1UofJ31GCyMzjcA81YLPsl/4Y6AYScsUTXa599rFh7HQ46dQ6xU42ZWqjZ
oU8TZRmdM0hudruPaTDZlpnK+e7hqYCwkVe7KdbhoNylpoKH+5ZZeJa5QP164ihiSXqIZPbAUIr/
hBL28yKtCvCu1wDlfdUJe2awXdkA6xC6dVJPBDLOf0ETAWyCzQ5gpYp5G+ksYjkTX34Ch7zuZvHn
HiA6131jK+fb4y//gKDeCxNIOF/topGPl9LKWlj/qYCiZvEWHFoHJrhv+BBiiBBYuOehqHe5b+Rk
X24/FP3Zr+CR413vjI0+k+Vsa4ruLSonM+7xf2X7x8Gr20/Ft38F+zNFbcKX1mb/4uUZc+noCBFh
LdUtz2GthEmAFVr42uXxuzzx9pIWHklLOaV+dUrZh9Hn8cZzVVKGywQzizH8xgdtbeqObBe+FWFX
o8aIKezE1pJ3xu6Qu04th5ilvzqoEAH8qPsLQRYK345nle6E6si5Mdn3AVY4GSwG++UpmgsH4ZQ9
mBSggYfmpdoTje3bnXYoXmeSNaW0BcJj7pU1XM6qNnQVteT5cXWRMgi0lyzB+MknnykeoxjQFbDs
+lRf+3FtIIj2NDmgEiY2s1a4WeJ7469h8E5Gm+5ouXYqDKeJwQmObwA2ZQoOVrMP+yl13ZacLBhB
GXW5gQBfNeSj5l3zNWo0w+puSzzE6vcHR05yGoHFSyiXYTE6JV2iN0zPby6TMYeq3STHLYBTstaU
Kc1HZ7qCtmW0K2x47BMITUrmn8/SE9rXpdIvnLvzEH1XBtYqQi0hsVWf+R9DhmbErIy0f+JK3M6s
bkdilEcA4SRygMAt6YwEz1MQFMGkCaHUnDoZIwYZ1lebIR0dOdPVj4zORUSbjFHcZQFVj0l5xRlH
DYxOgpxf20+Xp+Ceo14Bm75B12kiXIRedHUgoTWaw6Wz9Lr2xD1xeW4XpxIW+wLY74LD8ZA8sJW8
kSEbx6zkEY/mxqxC8OqGZZ3Pe/vYAT1/4kCFnJjP2rhMmrSuR/vDq/FLJOJphHEuZ/nQ8l4v7ygR
F8vMaDNHeMy94C63xMwzM4FyNuPriW5zD1xiIxPpBGHcOH+KEg3htk9VeTxuVKUbDb9CKKifGU6b
jbbsp1sMqDqSIP+is73pBEcxsQRUjhKIe4RzXfUEL74hx2qoct+E+/SSlUTLgTN3dz7PGPVtWRbR
iCqDAjJzfirm4Dwv0h0UcFof/wqu8RuhjyyCe9kQigBuom+pgu6tqOXizny1Jv03UDskE0HeGEMc
S2jvy4/JO/B7VFlN5y03b6JZNStIepBbz9RlaMOMPAQnMZnAvjrZH2Swf9dUq8nCJhiFsKnf1hS+
gRBtO1aK/jd41PKaHOAbEEkI0HI1pS4M+0w2vMl5A0T1kxjM05goqAwxNSoStnM1v/9GLGLTAiBI
gJx/JQltSS1lECg6pCjqYOI+DIYvKYEnK2OY+2N72Ob4Xxp6g/SLBPxrhdHTRUcNGjYadazlc02k
f6LtDxqjlPSxGVpkoG65GQs6r530PPGHyJiAPgyfjXe/wycnko7aFvl9vt0dTpUXulo0I3PqyyNP
awS70eW/Z2KOOurA5Fg84DRad8/BviU864hQplx0zSMHk81Dr3gTUnmagJZ6GfiFhx6Rph7+uLEy
lL6tIE6dv8iuqBAMqlNPAXJLMP7ot+Ment7kG8AcRoIoIUCr4iRg+qnyoEM8PX0H1xej1pGI5fB2
EYjVo7VqU67/3azOmlogZgXJ16m+dQTdUIhKYVE2iZ2HCTVGxy43YYIM3KWQiZCpTUSpq4kmreF5
yeE3wbJqCApyAIquWTYA4Vl8C8HRX87NJGqV7hooxLgtgvmoprgxdZeGX12g9Tr3AT89jQ4fM7JV
kS80SmuPt0zFL/d0m9BkhStWr/G+a2YqxtT3cI9mj6ObhQfchVqy1zjLpnGZ77Wo+hlsbne8ydca
d1RZizcsZQHlhePH3Lir0fyTaJTGjFyPNtRPk4z7kVUUbK4RNUUsR4JZTgQ2ALWntVfMZbQhLKPz
xW3jY3EBudV+4OoyByNGBDz1VatElkow8gm0kLSDaT3hnWzqV1oU7kbYDr9V4IScGRuA2o2W/Wv6
ndS1EHOnngKpqJBo6/HoZZ0K0NHLIlDYyeNmsPqlLARKzyLLosZoI7TAAOl1fk1elDZDDtd5vDLb
tlsJo8mFdiaVcl3qt7eHj0f1l5NNr87/fmCKns7Wlhi6OSleDcTXcnrWtd2XDckMZjaksVqYpgh0
CtqmDBTUAZQ3QREgOzQBNh9x4q8PySB3AbHGubKgDNuQ81YSprXhLorohAwEU76k8XLpN54ayxJl
pxFcnRvSdKS4iKpbDEgihQRtADZHY0K8q68KPStWTtorVOEq/hxU+Rwk0Ui9ux/URCZ2wjOMAKsX
is88L6y0OK3fEnSCdUqmY8q+oqbUbR1tubuWd22IMVpT4AmqhZWLromYAJ2uS4RH0cy4cmTfsZgM
Y7n8DPjVya8Mm6fdW/HP3MrAecayGGr9QHrytXT0fmdlaF58S9Cf3Dex3EVpZHJo0hWlh+l2/+7k
wDCFr/bLB04bh+lUdLvX9h5RXhlMP4xpdBxozKJ9gEJG+gZ5fmKtDrJEGq7L08TUDfDq2rzf4NtL
C/z6Jmu+xb4t4eJ2bBcyyN0xy5lJZoVXG/dCpmXdMj5rGfxGFysY1Pm8U9/81aS6UlDWRfsSmdpF
9+lbo2qiKxJCG8U7WxvGhCVji+6tkvntGYJu5o6pOSp6AhsG3JnHR3DxmcPwjkKcKI3n5XPIhxYr
5bFWOYisrG80eA6qDSYW0tI9449NBVtp7K/J0mN1M9biPti87rA9/tNNmF1udXR+fOBr8PHp9uLa
zdtp3CnZHH8caJGwSHttK020yuIV2Z+0p2DwNe4NvybDfuDNDn4uDqLHCs7TCtZXRlTvkIoL3D15
Rt/nmVWBmaaMhVDle3iBTS6gOvA74ZSI+PFzo6+vpCiPt/FCXbsoULbYbQ8sjzrBDUj6zbvlnydr
3P0PgFZklfvEheCWNscH5Ta0Fozlrmp6gZcqXuA4BvHnB8Ixsa0ZXxliqV57I/GEkkLCBL9I9Nsy
asKEL87TBI3mZqvZNAPecEVCmXK+UBWwR92E44Ta69z9aEOYXIkRaaOmxM8ouFgR/G4u8DypEB1B
+OIwkNb22R3TuDl2gukqBOOvmJCANp7pU7h7GJRj6DkR6x5h1WoplZLiH88CSg2e2NfHqwvtv2N5
dtuadgLWV5Zw6LrKWmtNNLYRc1dZTi6l3Pl3rQp+8HCJh4dPr5RrYQa9V2ErVrYur6tYPuOfj6sP
Kbs4RuscYO4n1a5VLWylpLq4MDMlT3Xv5fhkFrr50hhMSzh0NK4wCwS+yLED+75z5VZB1z2AX61/
qhalteFPZPt3rW4D7M7DTgU9ZD0ZKrrV97xZusMMxWGdo2hKjatlRI1cXKTpHUs+U3u3g6VWhdQN
rExdgErjbhBGUQFoUVzhx9BtURDjCxbcdZ+zSKxT9c+j73Br/9kz9umi1aRjurPSrkzPW5zLOWDl
xIU42PH3IfjVvoyQlZfXq7WJVO4IFg9UkKKNTQjjpQzmEzTnDIEMihmnOkfTb2c6hV0YW9zds/hx
AQaiNNWVgT6u+5C30I8gnMkyFXLluG1nZOKmU0mmRGI0R2Eoj7qiPGsK7B+SIhhmhKXvk67jAaHn
0sOvci6zbd6hM9FN9ikNBaN+smSf7rQ3mWVNvow0Bo11GWddUHbIDb4K5sz2DVaqcHYRJQZtXMmj
DW86ev5614F9ddyb/MDaCUiOjXU4RP5PY1z0i54ZvfLGIaYc/Nc+F7OIltFIu9QE4Hwj0lYoOjJY
zM62ulPDtxPmlNB5e0lvmhlSr+rPw2969GmPbUfMGfyDDk3l4N9corXtwZYKRRdjT9eEYtYhyzgn
sWVcpykMVK/rHuG+9oBRHv01cKrTe9rgTaOQFNYcQ3CNVvaPWHPohiV7zCAXL/FSSFELnZcI3FUV
W/HCWya42aZN3nLriVmewNMGV947qmXODVAS9catXt7HNfCxx85CZLscbW8CHrE4p0zkfM5xoZEN
LyJoqOsOGe0SU2Sdh9v4c1GbZ1/iUCIqwx4lPZO1WmGKl/f/V2Mby80QvfbhpLlbSqWDeITcWvg4
INztpUmrLYxwjVFvl52pgU6yWwj64D8ICi0CEIQlCDylxGYLAzg1Y9M8qjIh5Ukdg7ydn3L+r7Om
bwt3N/K6wze0yoieacZCeLZw8QAC308UMV0NhUKIkgRMIP1RWzBBX1nGdezhn1TN4wxYJo6O0eib
68QT87UtWZNtzFkHXjr6sEiG4Q9s75PhyS/h2niOjgl3S+OJoND3c3K/W/XNdlkOVP1MaOneJnP5
MEQs50Ix1C7VAWI2k4+pnysOdbv+WWESSuyqwwhFp7KbRbxvDj4D3WBNvkcsFCyklVSStI0Qyjgs
VSrD8mlbcHpFCsh6w49ymYpMW3emkABsOQa9iVrnoIFbwpSNOuOhHfjNoVpVQiHNnbfuFoFhXkId
yNjG1eUB99Y0tXYH8pEnQPZ/8SOyPFePTH4rc5q4FpkmxFJ9LIql5j1aroeDVD/+FICjR00BaShg
zDgVr6zIvcg2irk9tvpT2xk6UUwV0Cxq6r5GWsZ8Te6ofGc4YdY8rrnkcIvOmHzDSiRIm1huC4FI
1qHvt7nTO4N4Nv8v4UaNQ7fz6rtxcp8C9pX+CVEQVTrjRbnp3SsO9cVtcESVOS3CyI5ZcQO+v77S
NPT4BP/lcWlrcPUhJGFSQ/vQjqlIPhks3aCOLwagaBlrUM+cyJ2a6KlP12GpQ5jzvaQi6lUkd8jP
n0BLfHXWHUU6o1U4GL37RtFLn4deW3YHBun/B646G/iz6rqplEIytHeaHWRGxUuJw8+ZL7+5ya2l
iHf2Kt10jpvsIaQ5h3IF0sNS4GZ9DeTrLlKWa5/ZYTnlup5mgTHwpTo3w4HC0yvVt1ZDJZjCOKxC
7VrtobmPdB1j867G9C/jnIwT+hlmmXp4SkiWlBEO6JWl5XPLrRspMQFQa79yPmap+K3+qUlT4Gga
S1DOA0uVV8ux1suXDdRG9gvD/doxzuD0rtEGzulbQEFxxt+KQ56dOofxYd2Ifk+BBpwJT/Ry8hnw
zZYdo4LUHCsKxgvN/flIBjqGj9SEVU9HlH2EeEkOEJkEKlh9xFZf4DBn2zbaVF9twK0DTaYE1M2Y
3To0ZUfWrzYxhrmSRxpoEf9bQF0dpwTE3sI3hF4p3HAIK9KXuCv10ZpoPUtecy+LYDDvnqLRBcNy
YIy2jvBU6fxGOXx5wz8pDRHF0GAPwFfhdV9eSOEE419PnIL/3UmVBUU7Lplt856tMzrZsXQhJalB
CH0d8rAbJzTjTXjAuRs7plP+eP+RmUIQityN0LbD/trOYZnDouPgRU8TwCCPiPPRYEJ62PYu7e2s
n6neaZUO1IfXpWDgoxHBQXsj7DPTp5zMda6ZE+v3VMWOfEtPJoBn3/yQzTFxyq0umqm15Tv5jLya
TcFrx5MoaB1COlxvuvONqgbcUzOYSD5ksgTIyZQz54pa4J1CrWQGjqKxWO0Xr8g6iIPaAWy69ZTI
mSMrM7dLJaA5OIUpkJkiD0IwwhzLGl1QfAXGeH0t253TXfttdR4m9rsPWaXlCN4wH86A9LRIJD3s
pnMws5semoBhCFlFrMoom8aJLndrKJSs35XFT2kAY8LWQMpHYOKlz3J8+xADT8ZceTiqukDFVZZk
YvtY3lQHPAy6bJP+NVEQk32CqzcsPPdnHKGrnxfGpSnVv2F/anITHjK2HtpGqBJnHnKu/7vGeXK4
5PyC6o5TZveMB40uAQ7/+4fymkyc9F1fE61yYjv3uyCRQuzD2DtfQksyZvvexEwW4DGbTrLKuYX2
E8MG0m4kWuN4AtOFDRFLBlnB8mrmt4dB7IOo5bSqUSHUgeSr7iyeODrmbZjwcVZ9vZSnh71gqtRZ
yKYWIz1bIM1uwYuDEHBVALsVAzi1+jBV24kJ8AGw2HYMtaNVKCXywns4PI6XZbKEpobS0jaKSMof
uK1siUGsoEQkL0/gqCorGbYI9DHPHwoMBNQowHAIpVb1INmdwnYJT6zbwHv6nBCBwSgoU/FledxV
Uv4tRqlbQe04aUPxr0wW9vEMYXvwaui07ZaCViI7BzfC2bFbCiKSrqKngf/jJjQqcG9yW/d3ia3z
r8mW1RPXC5o1Fe9ZbA9oyUZVnrMlQt4OAGmAB/VhQKevCJ9cwgEadrEQcRjBP6LWSgvL5w0Jf97v
ggRS2Uy8ZtmfzTQFfX/S+GWdJU8ieQ61aGw85c7VivNnXMENJt5I6zfan3Ibrs9sDtKvIvzqQSVt
3qwhf7lYC4yrfXykp2EhnoyJul5cce/q2598Zciv4Ykb3WQFZqS3sUylj0m+JfFubH0RxQxt71i0
0RmCm0DKUCSbUqi19+qRNXUOaLZuDCMIF6d53LiZA6tmQXvPQ5BnMrHZz1r8SA2ygngTINO0cysK
ivCBvvVpMoOrztHIo/f9stpBgwN5rCeg0ZNErXFxp85Ba3eN9BgrJ4ER7QJj1TU23ph4b6+zg1kG
EcgI+D/88lNll+ScObFyIWGF5Ur/UqLWTxM62O4GzSSng3ADu4QlkbgpfA1aJjT8Sw5VWiQw1m6h
WNF9/ZXrlIlYcPGOSm2sRuNO/OxYb47Kc4rqXdMH3s/g7GRxkPSBqRHPubfYPfi49JdN1yuzJWdR
sUDvOuclasFvcIH3A6nr5Qjbods6e8XqRLsDUx4tOyuW4l/5Fa/tRrg2PkXeNPySrzEulKIHf/bn
ADLUOwXaz7SeQZSS9f2FX7v6yQ9BzZT0BLFhytNo0bYRP4rhxIvWRoFrnub4gbXx8uSMrYljBYgw
UnrbmeDT1v4IupvGVEOYdxhnJ0Yu7CgVO8Ty14AufbCBFfFQz5V3Qo+7tp5ABwp+oT6jtHrHrBTd
O5Th351H95dxshrNK7qPFYavrH3mI+yylXr7Ag5CUNxWNLngdrwM/1nydNJXbEFm2bg/wXIvlgEW
hWXeU948HxXiz4C5ywhLdtS/uZxayj41zJmfwReYrIEqrFTulN8UM4HY2Hp3qnAGXNwG6jQ+X+uL
P+O39+hoB/XFPjVAtFzfuzxjI9ajdFxDbBVEYJJhi4hLD8aJP/aFYgQ50yeJSLUflxNszLP4QJAi
dmTLTGpiKKEBgh16JUh9y9R+CJBaWGfOOnf4KMVibITtyZ6DZdu+eNb70TO7JU9yoyuhAe7VIXq1
jicisFDsy/E3m+XKPJM99tFToM05XViwpwOZUdQJ1JiWUT1SbSVp2BcuFGrSaa1xrF/dxv6/zI2N
xxgXBDWZ8c/kdTwMmJvPCwXa4pUcJXep4uH0Tz4zDKWDUPHmd9hSicFgCfq1V2epxEJkmgfKYddq
TSpcfg7A7kM51yKdhc0+sp/WizSWtq2GD4qmhxSc6Bq2izgCZFZ/Pg9XtvxRLHFl1o/dXYs0Zke6
ah5/FXygzRYZx8YnSjFZhMFEu4GU3EjqT+MXB7QhkKw4O0hraBDOknm+pIgOZcMKd22vZ13ZUO5s
dHMDfuDsjVcbyTEx9nep55IxIN7PnRGWEJ2df7gazwFKRYs0LA364qe8qf+BzRLonoD/lv/F4+Mk
KljajCI+E8yuO5WYm/CqaYy820Vph2ilu27SED/4iBzyHaVSDqUfaHUY7MXOcS2SDbsVh5APHwJS
/a9XOZV4Za7H8h3X5QKrALEgP7ugO1HxNrsQs5Yl+I6vpa1XVWxzp6MPjlWjx85u1DuZtTBB5UbH
NWGHuSh43Gbw7esb/LUB0R01+1tj02/DZAouJiZRwvUJPAOkc2dUsTIHUQtCxrtJ66c3dUtDpPBG
7pgqI9QgTBY9gMGxK/I2lqwbtaMjRHnH8feU78HqO5nHSLE+JN8Sges4y8gzfSq5IM4CXv1P2WLD
Z7HL/O4DjLsq6kqfedjbD0v8Jxh9US6SiRjz94R15ECoL3P0b7PxY3FlBL/eoxcCnHwSaAoq5jTj
iTjGZ1Dl9MzftNQ8kwXvGU+oITrckRS7ByAYtOGZk2jZ2eMIwWRwn4mBB3Om5HIOJ0UYhrgNFmDS
fqY1QTUG1q0aX4JkKjwHY2veK/vGAhdBtnetH6+ogemm7urE38+xbHg0SoZ2P2Aun0xN55jOwcxv
Jdjlq/EniJwZSGPChG+91NZLZgV+ecoA2LRwSQ1ZS1SHv2jRgy/qKbPwrtsIH4kxAvxyi6f4yBm6
S4QqI7VDRX1owI95fjNQp7ZjhtWSbaR3btuVeg9nV0zCk3QgmLBWRZ0Pqz08QlkS6PxeDD6wG0SA
O9gTcRc+R79/efQnDDJ+3wFqqeu2u8TAuiGoLIxFLZBQZWVDDiyBKiW4ZUWQ0HfNwkJ3PADvuegp
OPOB/O53ePjmd/zuVN2cg+IRjfJKf4ruDfTL0DMXDZ6G/oCvz8lI32IQsmXCesCbkQAYqJ+9esuv
oZz24DNGkTjmulCTiggsNB+849FLWUnBJnnKIYZAsWMNvB56noYG27pt9wp/5jshLT2xh8tWVOSY
1Qn2luKUoP8spqFFWW2IA0QOIgT/JW7j6Mlc1DD9RHc5iAPr1/Zv4bh07pTm3DibuLp5tMhEoV61
btDzaNwQ8bEUSifu4SH3EzgQvT7Hu76qHPFm034rKB7e1pNS6+DqABfTBgkQVvwUuzFUsjWo9dS5
3RsORTVC/gW6LX1rou97h4ECFKWkIwLwMM38w7JtDXgtjOk3E3wL0skn+5MsMzMLbebXMGaCAZBi
0L7mcsktZtxtpv2cgKE+WQhgGC5vk/sU/g2LtRMvPOVm44u9VViKeHAvV6B0SZ5DKRq5XQtY4ZbJ
Yr109mavWoN8+8hzKjKIIs0I0OXoYn3WwqfO/Rfo/t+N9HLRl7h71BQVa8IqlKSX/nTG49Nq3vI3
yNydt9svxrhutHpBcd795UIjTLNSGyCPJm87jDmaWNNzbiPT2U0Vd+YOMSU1X5Tn5p0gB53IhKsp
k8yLtsg/p4kE57Mm0xs4/n5WHzOz0f+03Or+r1PZmVCScncayqDAYVVt9wOq4umVKO5WPi4fLMX+
wdg54aiYJp+r2YSRNzsicC8F73UP8vlibRVeNPzAxG0Oyt087IS4BtDTPJFbp6FEfvRORdWyxse6
x2q8wluaKkuDIuRnlEfgJGTj4NQbsSh0k3cQAzzPl95E+45ijEEIsu0kx9bv3VlFuxv8XQKcmGBJ
I/LJi1n/W50nYavD9i1Zv++6i9KHX/OvwiuS+9jByLZW473zgAqd7ZDl8lf9iUe0V3jxe8Toh9Ka
A14x2DoDRJ7VNJRcHzmCF8SMMhzN1TDA3vjd6Q3D0J1jy+lMmJXAAmPa8vg73Kr1gIA4w/L3qunx
fSBv9cOYPr1YKy5fnaZLKtjTyeBIK3ZidJXJhJgnopCHSX/riztayNf2YN8KYdqnPojsd+l6wFZU
BAdx/PGp3a7eVG4gcQ7qbyV+SSh92CB/ORlZfA5xgO0Xc2uvLesvAi0Wje3G4b/N8DGaOXXQXTes
FsGVhJDDTzuqkuoN/rpwnVutUDbQ/ZosTZnYktn48XewyRMrcWxEfpTL8cUS+kqWBAnt74/7KXZM
h9QRnSCfaXTsk0esps4LFO0se6aCsn2N3L+jxWxACeNmad6d4cComyFF17NB3QnM6vAWThEbOIgt
MLjvHkRybKJssqtrT/TZRa73DfK9FiYSyMtTZNk07qtNJBKW6GWSca0mUdT+ZLiOZrmgr8LTzucj
96sCWEp9vUY0hj/bcmT/xe3OXpbHAbhwei6054hAaVJSGtHK353AmT55IL4u752wbtAj38HJnA5F
1oPkP7PfcAscPBV9baC/Y/hra6/3sLEh28yieJD+h+XJX9kqpcPp0UH5GXZrJ53SP59s7hUM6ofe
6bq7dhjLVdunmkDijPC1zz4JlzoZFSWw2PjWWBM8wWKNhnuPji2WFKIvL5/QhsGVyQ7gFfWZhKKM
4A/byoHWefVit4RTdCNXV6z5g4yZ4ck6a9rqdsluKaYcgd2gphvzYG3xZPtuj8OBQasjkAP7xN+X
UuI9b4fIc11NpeRUun9EEtGNdtvKKDIx+7So3l4z58DgFxwiSjOz8bKWSMUrWYyTp2SUe/3235Kr
ztBiJnzAoK6KLinrmVIn0C2VRE1IOgihzTLcfzOB4jVd2oKBnnX7l+6uNSM5wim0E201SwZ+5syW
cORFC0wX1SPBfkmmyHp/Gp9YnF+Kd76hdZrwGDLjwMg4lgk4tfx10DToP5r4sOA8pSi+Mei9zPyP
5gvzsfb0myqLvbwQn432DFtWHY3C2Ah31rErFSPWUehCu6xJBBe4ob23mZszld6EzGJrTvaFHBlT
lCJdZ3xz5v9rUtJtyguavbpgHfKldS/QEcRzoqi3VgS+qcZRlTo6TWSGQOebgeiDgHTzkBjSXBpt
whpFsCvPEvhB6UiOYI794Ms4l7oW5+Ag9mGjmbFQEcwRtzwOCls9H6V/wQAdUkqW5opSc5Kayyxr
0Az3gJ3BlNVmTQFbM+djfzAmgA3BaZ0GLLE1wv7u93ub5itQsHctXpSKVHXJyv6MfhDfHbFu+oI4
cXOy9qVehWtCc4NN8tQOVOpzXpeSX7NeTYVmtwArBu2WenCYJpvDJfq4ELZVFqQeR0KFAXLDN64J
Cp7BmEz0CnGQNeVV1XsTZj3oCdBl4ShGoKKkYm3D+t5M0QOtGgmGLSrXRzloaZ5ZKmWF8lDXiP+q
tZcf/19+Dp3VI6n2D5aZFUkh0ZfglWuJTljSKuLBRRnBYg5M61n2nxWEVvCpUQGSAfjbD6UKd5ds
mZUPhKw3OHn1BBFcOw+OM4pbluCRygDN6LWhcJsW2zBbFtrbSPjwnnuyFCBr3SHHHiY464WAS89J
/++xNqvEPp+cUIIu0o+ME43IW3kr7RN9oDgcxWd7kTfC5kByX/Z0rpYxz72K37eOiQrvotoIaIh9
Ui6aLBYHYit6XMii5dy1e9PMuk+en+18ezxjb38kTit83pyj8d07+X8ILwvWBp8y3H67KTgiCC2+
KL04jbxF6TW2YejL6iEPrKvMNPFzuFGu88dDXv04nLrxZrdh5Uy3quTnw320mqzdYTpOWUaqk3bn
IxN2qpYuHRGgWCJz6x6JMeTcEvFbWkzKjfSMPtvWaAS00IZTZpDgHzeDxarOD50YOW1znphp4e6P
YABbvVseRiYc5flTukW6wz8OiLrDoUyquiC5lklOZce+1znbWGstrFHGALdn+EonHFc+4LvSgybC
JDc4Duj8twA+S6QE4zy97wCRItbxGMNNL4CZ8cIFKvqhxwXvBjPLhjqjwTZyLbwAsuDjUTEvNjlj
GlXJskRXxuqUBY6MfsmOI5crSiNB5hFJjASDQqlCNHuzEyR5WaTJx+njDzdyPXPQ1+7+SRLW6R7o
X88XaC2jSKEBD2xOZcn1NLE2bLai2ibn5A8IS399YLjY4Dnv3y4ulowbI6vVbZZPIDJIWhm9rHBU
3ISErNdF4hwM0yi89vQilxJBTmDjuPcSSTQGT8rTprL1T+IGirTgle9XFvY6iS2WqxUu/ZULjlZD
SZ7jHCGU2wzIReHYREM4KTvOcezAiOA/bTQPDzys3JZWtKs/NXUp5cSphqqIHaPlWBC0bYf12HYy
aEs3xeQKVRATpLPxywgyBlPxz/K36y17XEuYs1QuDJIM02OMa76VoyUVplJO6I6lnYcUZZ7vPYhR
AIZG39QmgtaTGaWJTqGCZUVT0/NJfe5ujChIE4dZj9IKGXWGcAJwgcB+AAZT5T8XmcZFLmgB8Zqd
RBXzSoEH2gxS49JN+yv623x+82W9PRzoUYTBufOujEYKlGoaVkdInPn5GzehAdUa46bXaqc/oXTv
chZ5cCox1pMSAozmgVjFUQ/IdoSZgFj4O5S2Ulbi+zOGUhnHskhgUN0cBLDClvD1IXO78nY7gSGR
9WIT5CtjK2r0Vyg03jxSN8dI5KYgAOz8O/PZ5f7qphh28ZjyLG5WqJAhwqddRc6D3VHvHATajNEi
dSHZDzKNwp8gfG/DGs+yMh0WNsXSziU6x6nDYTvJckaw/Yzm5e5IDnUoTQKaukG7Y/KrpwtdbQkt
x9pHzuKYV2oQBJsXMhKBdI/ktmeGo4/khb+vj/KmhUjmSJAVSvsnHSJyEYVQ9hcCg5TQ2nXH6dkg
yQs3hbdjFLnkZ0iY6Xp6cHKGr0X5JzkN/i0TB7P/mWAGEv10cCthy+4qltDyk2DQNoBAcP87UOGa
FmaCF58J+neFWPwyoNeLY9iM4cedQeUxRVuL9TmJzzb6O2oRt4Sjpr4OowJtOOOxXjoHF2hiioM8
xV5LAFqohITSEisz4dHNELtmJM4i2jSQt97xgrlI1fp2a4pUhrl5E+1wRwBaAK4LBFH4kJQuwMhc
92fvkyARgbJGMfA928C13j5rNLFXKGl8tY7IFX3Gm8FI5i38jA0TPk5S9r87/Mg+/kCy5yLDwOKi
drvtz+xhd0x6ihq75V01JM7Z0vJj/i29Qp3eb/Xu2q+CL2eqVUMq0iqxnbtgZOO4c8x4Pn4wfedh
a94XuRePARyopo4EBllIt7mXW/y+2ZR64cZ3erzzo4599MNPNI7TmJi7kcMJ8fDuGZqpsYr6uHf9
SAoSebRleBjAXmQp3lODeKHIuGbbdiWhTolWKWhKwYcY58q0gRS1xSUzm11N1N0tlzDab9QLoS0I
hJ7N2Ocd2sePWvVmqQaWIzD4dmD25B02Pu82nkBlFMriqdlGcmgmHTAa4eXKx2YY6b1tTkbxtRUu
R8mpC0ri44AUeuMaJ8bitDna4pwu3rUptcZDEek88oK4+oA7ibIZHIuTr4EtYwSuoy2Trv0YYCqz
117qsqXt2kPM8oqW28PrYVjm8IQZ9kFxLK5RYgdfqJX9f0tE+86T3D5FwsrGHdV9pVZWjmAXqehf
+lXnTj8chKHlUdOgGD3Wzbsj0n3fU3MAEMNPW2WDabOEUbkwpgic+QclJpDWUJ/8rkFDvQpgHlk6
2XY1y2fWVlLUSyul3j5nEvBmep9E8igR24PyYGMGAmp+k9YvfpdLH00BY+fNPt21WED+U9xmNah3
0ppUtlCf9Q+JjzXQeeb+q4j5/dnhcM7PSoeWqfDgK53X26+9iGh4jdoLyMQZ0QG1YLmPglBpTKXJ
g10D4J1waW+/wbgadM9ehH9Myu1Nh3rffPdEivZFZzqFUO5gneqi/TUY9g6G1qSUunhRQbape6Q5
DySkD2WA3UxpWOdNQOdwrOxkbfW7mLOt4jyAP81yDrJuQ2yPZ0naOjmggd+LWlHHxg4rqUoW6hY0
N5HyvmjChgfr5yz+2EJFR4J+sdu4DtdVCgLc1ohrUt+6cDYLAJusdLKQw+v0tPlQsp7neEoScRjr
h2jEijN39B3fzYdhp/aI2HQmTfU3thCo4/Y+YXc4qSiY2E5VoiwCmG8vMqagHeX5/lBPsCXwQfvP
C9LDyiqCyPFGIB0KuLP54TQavFsHywFVporH+WaqY0rzawbUfQy31A4NcgWp4pRifgG8hHtfwAQs
UKGkx3uBjjaa7qF6ZRh7HlC6cshcWIkbazKuMfKPgNto1X+wImzXabZjHMN5lqKOMzdksI+BS/q/
b0SfkdO1oxdvAZ+GlA+kq1Kh0g1M8Q66FHQ+WVZcn9t6QM5zhxJU5uaSPPkUF2Q4jx1PJnBnXpQd
S6eNvhF9rORvaOhvmHeTk8kqqYAFWdYLI4ewksavUV7ZrzlZeLkpXpiQeBmQJA+H3i8YjnV5XPBg
uhQZj/US7tX43bKjOcFh+309f9cZGHhgcig1n/yQuRIKn21uSiZQm5kMaqxTyrUSEUsbgngGvY6w
BkQrB9pJRhsZ6vC6kyZKK3UPWfBHEj9hwKZzIcZr7v5EU/CEKIwOAOwNSkNPw41mdXZLasgokzVF
7UXcdRjyVpb7ujn+KSD13W88YzHAeDIx0uAnAMHSJkME6vE1rupiuolIYDzx/DdeBSG9ICIFSDBN
HZ1X2Lkn2bZDhfnA5wvcwIUCtlD7soD90mT6p/oKh2X8du50GdncHta8YtFCTTEFLSd79mAvGY+S
XOgnCmAVjmV1l/TwUEsFSdnG/WyRS+LDQYVqhlG/A6iAxrRZWPrOLPSsdZuZ6n4Fh5EfGp4Y4B+R
4w5Bx/bO7/QteVFYhhQhViR/tk/I9U4C1Wse4saLnW22DFTuX8ZIJycpk+usWUToU6pGavmuqDF6
8l0vUn9gDvKGLJut7UnxRTO0plvXJ9ybNkmNZlgUc9biyTJNdhDxHPyryaPC3x4vz3q0wYEChC1J
vPBMU6S2d0H9hxsJ8TWM93QLuBS2N2MWE0RgG0IN21yKda085XPXEUX9my/TBfmKTJnRq/k4K3Tp
2nx67mFCUCGKUg5IPT7HwaAX8WJkComE4Vx0u/6vlYbrRxfPhr3IZIcSgIuebnPoYMEjGqrtwFhZ
4CIAKtbFsRpODvp+Hfz62HtZl67Ajw9G41l3sv6IuFpZYd7MER+cFXc6AfbuFomrDVyV6BZEjEo8
UPv47ptBG7S0SdcCtMrryN/HK/SWnUyMX2UnQ4vo4jJIgEUgOWkV5E0rgEEpqxHXsEV3UwOxQhlT
a04Pvp+QhjzLbiwIPj5X1HA7CSgueg6Htr27Ci4EZgRBDUZ2NXrU9AglUEn2fAmkQR79/jNtvWbm
NmryeczB/8ctktGNItXh3FJwALTcBq1VGmCbnXDfCBnBYDgTrWdI5R+Yk6r/93gagN16NaM50Cy1
t1fYJCoFfD2jnqYnMnGKvc3I/8s2nbPrWvklZ1DbdQkqgZx3Gyq1GsnN1For53XQ3HJlJjuSm50o
zei9ce8OgIwoBAFlqAZsIKlGxdPVWelrjPwEfMWQdcPQhKqHbLywCGi3tQv41bQlBsxM+RmPdeAb
6jKxFQnTuMa29UNd+OEpARAgRUzFfzVaLuelbYxmLk10k7JTFYwOBPocro69VpUecRGcVPbrKPSG
ifKibocudkYmP42yPtxY3DUm6wDp9jZIcNQWey0+gspv9dkT3acQfpxDKQ7d624X1jo1xLWITPBt
KaO//3+aIrLWSP2LNUAEGWo42ByATxCW21RB1ZE/vLkTGkb2QBpUbXcGxQR2toJqnDuOSfHiHpiy
SyesrZiiJRENA+fRj1WxNhu6FfJWTBs7cfTdSmNJg757POiOtC1P3IyG+WmDcx9xtnNLYC8cBkB8
jy3VUCgL0RZutEoBK0d1kqHKRsUfvuo47a1fNkrtsNAo8BlffhjDqXWVAI44PmPClts8tYDmUFvT
IuzzWb/ONaNnCt8o8fEnvMWv8/utYuhAGKnogDb6eAxQlxSWaRbe8GojPfXTE3F7x0RWpGIBFvpo
CF/AcFr4VyL/thLa1hcmLSO9rfpnedo3bcVGQ9fOzWEJ2mcJK606tXHCOT5G7YAjVT0pcDh2rXU1
T3Qt045SsBk/zKKtz0fNgohPPUQQlAdO5OpHRqmn/LK6Dysapaa3MzEpcrT/LQvvJHoq/LZ1n3zK
+JdfYMK3Rlq0cGWSWJqhwVlOPDKDM6iCOWVDGECLEzfQrEWByJjCocgDJBZVFiIaTPgweroI+0VP
D4O1RPwy3i1pEc7K82BcJi6f3FhHQ/PsD71QHAhV/pU8ALsIjXUK7zg3OFZ91/ioQYuric3iuQc0
Fgkn9GBJbhLPvXNCbhOGKPCgUITUMoQ6z01V2Ke4Xkd53oe5irYa2kd96GSPCJRgPCEAVQWZteff
tQx3kiaVqBxDhkFk6uLcDZMR4/9qjOmyzYepcKXa6ZhTAIwZY9xhFB6RVwQ/TSQ2LkxtNAvU3WGj
6da+Po2ELp8d804CzQ8ndjEop+rZmm7nqDH7kvW8qZD044ZvgewYfChPenzI1QAU+SkKoEsd6Hwr
1hS82tL3yqDeZEerSX2P0DPpuaUHZ/VxmojHsoMEas8T8iT8U0kF3mD2RLcb1UbcHi0nb65IEVIM
8uAYbqGxMKnjRx45WYS2EbHEEWyByTnKnkhq6tWICfRF1EESjBWEgTTxFcdjOpMIufgop9NKDPCm
+tpfOTgz3bQ6MqApw6T3zfifxYL46vKuss3r2l+AyTga4zaJ18Js0QU4ccpSook9JxclyvTBrgQp
+Nl+r1Wk6H/m/gHtzKrZhVEjGBnROLG2PBS9M5LXTSeNMTJddJNkILt+XTLC1pigkItD5PnxtcOt
+p6XfxiGQGU8bdyqgzebl8qcOHig32pQdlrvdaQWLh+um65x14FwJgik3xtKqDxiKJcEv2aHn7uS
2NHWFnCLC8aQgMLngQZ8KANttoFCleLLzClBEyy59TD3FQN+ZpCR51bXLCFxBFlmnz0v13sL5XOr
di2z5liAJGx2GT3lTxB0vlrUQ0g49fDzkk5/uQT/By0mvkp1MLBtHLx2EEc2g9WibiZ90xXoHkwQ
1bgf/v3PTxwg0RmiGNw62vPosSCKkO7At8HErsvYXGq4ADU3kMf/P4F1A2fLhqGcb8MLpbtBhNFU
cxwpvRBlV+CseqJvlsq095lgzyGDaEp1u78+nBCv4ZzGguadLjxoC9Q2nHEN0g4mYfjpA/Hs2ee/
L5xjHYH03uO7LTp95z28v/FiEPQlax95jZbkO/qL1bBtIjnbBAXtQ80zi1hra244R1d2J1B7hV+0
fQ6EVsVYtVXmelpeC47L+Kr3Lh9buAwj3APPGBxiFeeriZVICUGm4I08ccFp1D/p2lxnSA5tTEMy
gBFdvyrYRBnE+aIILnLlWpllaUvBKe19hZUnFvSL6QH/WBbrz0aedN2sWDDOWvP6KV+MN//tboPb
aaTWFi0ROxwfSLEHAs4BGuPUfAU4lNi0XYHUo95rRvBNqpaE/ECGf548QCujDI1GEeQzax46isYt
51Qt0rtncpwISazit87KyVPT9tJlnChooxjr8ZYxKSfxzbySQP9nH35MFE4k9UYXD0fYSW0VWmS2
NVEHCLEEZglH2ILFJPM/Vfr9m3XGZxrbleef/AWopMlHC8VSmzp1s905b9JPgp4ZluRiLRj/1rAs
d2oy6CMU26TQhUb44DSfv1fMHw2dPtl8+vGbw+9bRURTMEXQ0H9ZcMR4ow17VDWCwJUsG6XAfxt+
PVhERvFcjDUmFys4JxxaGhhMdXsFaqX510UHTGSAv0cbANW/QAQkieUJm+zQXU7v14RDE5YF8MRZ
JM8YW4JVBp//Bi2v34xR75DYJXdem4Sl91cQ7ZJL7rWhIj+SuxUwnvKX9VfSrynCgqe1hMWPjdFG
gX/5z8tgw+/+b4cj7MVy10yMKOR8KicTI8qBX2vHTnXkEmonZcy7QtjFQ7Px5KWmx4DcP3y7Jws4
ojzhKs9vti4u+xtL/kKyXR462sPyCC1J6+Gp3l8xs1GL2LYGSvO+4+XJq12XaEDy8L7D6F7P1Gyh
JMx44ugB1NRdZVij2tXkTIvA0oNDCDfWDwwcXdGlNltFtTkcrtnifAfNZCHW+4egUUDm66/d9hrN
xqTuVX8OgxmBM4N+FKGLKwcKOv6VB3bVy8jvpdmWcu3/LPskRNXMzR6Bx8r3sp9kHaiYAL3Fqz42
F2g2mah1PaFCyFCD9kSSWY9BH1hj/0Dcc9bF9G1Un18QwLUKXAFJBokiBRV8ImsmouWD+qObvgBY
DiX53n58OP2W0LXzW9GTwgXz8qus0xhdw1BimhVWyFkZ5KG4WwYGAkFfQavD7UEhcRGny0cVTZ8A
j8kdQFXsRJ/yufcaHyee14hreS1SpUHtBSvXRS/TtPHwwrpH2sTZdnaEtpv9qBzblqzBp+Gy+hEA
FY4BYIiey1RcU0zFLJDPJZ5YJarxPikqD9HU+vNFZzFF1Tl43lrr1IA2SSVoX+NRxS0WI3WQdee6
XPs5BxXQp9r3O9gctVnuGtfg8GJ4BT0w/tHFnjW9M4vNBTc7upo36XZl/WF/2zM1GlFZhswwfYeW
rZMNH0PpCYjDnyASe7L+L0jOEsPj1V9+5eDZJMLnW5Blb5DAPzRIkimBGSm1X8atuUH9duz7MrI8
hM/p54Hdnw/ztX69/CAX4r91/5qvfkTQekPtmnc5E2iI2HQzUXdVn11pnLlis83lcT7muul0eovU
7bnOOaOafBum157zjdEZdbK+2Nww9+iHkfBEXscDy+8Je9pRHhdN1hTHcTVYQwhOTq6amsb/0rUK
bwzBK6+s8hxXat3n+9Rc4CHQj/a1GBrtpvEOovfO85u/ZYKhd+faukovUKNr71DJ1hQnfe16a8R3
CN1yoQhqjBaUgJlAvBrwSkLVBzgRfovj5YMAoBj+Yk1ftaOqW64ibkDkK8HnxAlW5Qw9cYbG+Gaq
VMYGvCnTJ8+2sjXgqs85izafYj/0ei2tQbESHHc5YpZpkgfrd/irlp/zRay6R8hGmWxcOiBxoaGy
bmCtxhX4r5o7gxv+k9I//mWY7JSjxwzWQY+K2l4m4Tfy5EGyfHu92Lsigvj17fKSxT8ODCCj4q6U
DEAoas+GPFO4lKkxCBMFBTMSYiCa8PZ0DluQCzruMTFeL4H0uOzzZhxyXhgBWt/edNlTHOeJ5ziQ
ODVC0p9aUyE8dohW/qTWVjFmGdtXC0sMgsp5X/1ZetVbdBm3/3fn1CufAG0csmn7S1aol7o0LMGN
VbO2c0yu1aFMjtSYMZHUb+YyuBs5wzm88lBrbgXFU4eLozIKa83TIxeiDvp8hvaaUJE8dLAxpISu
Y2hajJXzmHXeoLqRFGHjBd5kKhCK3KyavMisn3v85YDsRTg959TlZaMAWW1sQD5SK0uhOWnUMFvQ
lbPGe02sfsVdNs7gRRzO0gZOVX2ZNhip4DIkem/Vh0eWAMKSRof5SkpRHICN8Rir/BJBk8QBQLkF
YEioMJ1bUJB/i/GPU5T2D8oBcha69TXLsMKf4Vz4mdtcxvVhTuNgKfdUGtnn5ubAyuE+L+Obvnbg
EE24hH6JQI7tmIpvAUgftN25WFfU3mUR+RGIuP5ZOZYYnvFTzLNcciPT7fyp/e2eZwRgloD9pS7Q
rvoV9oYK8DY5T8Dx/I4qZNJwCM5+URax/BibxVfgZ2qOHK2TABJIR7m9mvKE18NRspGrkelqkIGU
/QiarrFc8I696I5z/tgRmX/jp6B9sMahixSCrVSP4MuMMrlgSxNvxiQdEDivkmrpLq9JtMpCTK1H
RULhZlDziR7fUZTrBKjqdfOPrPPb673gmdvRggYmbsgeD0BVL8JptbawduomyaWQtTGMvK2Dj+bm
K6ZextShTABaV0y+k4/0QHiI9wERNIRza6pbVvQvlagM0Kd48tL2I9QaYc2j6WoTWMbFlgYTmque
ECmyOVmoEIZpf/5dnwEe7IazSDXTp/cXok/tRQy42nYafJ4+A22NeDdsnkpAHz8xzEqNGvA5thx3
2MYQtaMfN0ilDV1+yJs3wsabVLNsefJiYmaFoqNrCaAR5QV967Z62m8efg7GiMzrHvhTSy9PSkpq
lXFW6r++yIRKX7ezOaN855JFpTL01nRXq2oquzRHYP/LfcBtnpHGSueFREKqaYJs6QIg35+Cj4Z3
xdCNEhj9+dcdOGvMvrMic0KfsT9homjtR/v7DCnnJnwKkcLEt0NowYZULsTQzjaummtqO3WJoX/O
COyi5yxNDQOrTWCBrde9vwvhU76OSN3LFphhn/FoEiyd5CgTQAFJARliuG2lkeNts3bnZfyIMWqh
X50L3nyzN3m1ayjnBMZE6HgbJn3JOrfOuZUcHrdp2qjpvK0GFJG+T9VO0FTKaGXmHF6iE2hFuHlJ
b35FBqh703OXj3Xt4NtlgGXwdirzyy+l3+5C0jya40OyOIaIFR4w7xbOHYqli88uNzmr0oNKfhkX
VVLVm4PrbjgktWyRhHljziUQb58JeEo5GHqGNDWeyvgNApAKGYHU+8d0TQ8DfPIYiOPFWGB9F9k7
OdEDlh9m6AEfIfwK6ESZJqELbWNlMrdX+2LO48SkGW2uEpKLh76yfkfqxquBfRgErGY3GCf6wrSd
sI6mHErVyCFTpyeC3ns2UXslDpSdVWhoMgnWVlERBEuRH4/9neaTYe9ETuIk1v98ONQDV+RKZC3i
pRPThjkYA91lWkiA2yBxHdR3vGUCaS8ZnClIP24KkDVAKezL5sMlyvoMUhqneRjWDJQRYfTXRuiC
HVSHHWwShAIvO6c0u7+XjpjGPxTdWIhnFSAOELLxfybRy7vj7TuyKH/EPRBmSsML6JGl7rMWxezI
5yfgeldjtxRZcn3Zcsb+xRjVv3hIJaHXN6fNoD8VeSoQYoUmvFelN23rHr69KlmPMJAbrU183KeC
YUj15ZkXeI9WRf/DnVnuMJm/IMru3NvswawlDcLEwssggaXDCnrmdN+JLbGB7M8FIG0Hfg/xiu8p
EwMyXG+Z4aj7TdNFofDxxm0erGtMKN+APj8e2upmsxu10Re5XExvFYQ2sTAk2d7eJ6j4hxFEIfr8
/yGnWr5SvnWyMEWCd9aqzx9t8sLbzbyyMSFf0YqSfmyA3zSeSCOULiS3j3Klprpfj4gq9lBerAzw
mNlG9F3iew7czLporPLvaUtw6r+PnH/eyQMCmYbJwiN5d850gy1C0kmB132yCwQbIWPGDjuk/qUP
ltL9eGcoFu2PrX/MXJoTJ7jUvU0LHC2hfPpUAvpJfkx7Zqj11fd6tYeCxxzK1KNT6ioK9g3QIdm5
sYlCx7wPSCxbFZcD0vislIucwW/b16RdSkaxybBart0f/Cb1aIYfLsrRRl8xLC8ZXWNMYtv/oUBt
u/ZUEnd1SWjmRzMpmTj8WhD92FiP7/WU/+zMYJOn5F+iTolDWomoEebn+WksjEBYCLpuSm6fXL9R
ea7q1vroMwSyQS9KzISd/OBe+SzPn/er8Ipmu0BPX3lj45/3c6FMb1EDysnidtQFBJr37jmLb52O
LGljIHiEiIoSaH8va+mUHo+hexTEwP0EJcUWeYKzt3YXt8J9F82ZKeq098OtuC6/RUmvPtn538Gc
MMxWE4hsFeYoPctLxYfZ7CgyANcTt3NI8KmoFqkgjwHEEkPPthO/wpLW1AF+K65CgajAV8uy1tJe
c94ShxbnBPQrLokC+SN1sdlV0yt0rICjKB3ByU5ajNqo1dGe6KtaFFxysKB6ZxrKxcvF1ttEMLs1
2iU05J9UC3qIchhCnZoM4rHEb6nhwMGZeLSY/uBw4eWKyYzWiUfVJOpN6sqdxmWDl56iwd730G3b
I1lURqIUwoJIqG37L/zCk4g1EPUb5JMspedldD+s0oaeE9JBIp7GtIuOFMvgF2Cd73Q9BKgqVGcE
nRuEny1PCIDo51r8wXPWcFKp7+2UqX/4/P3VUHoFQ7y6mg/FOHpXFMJ1rSL4XE3ZvI9UdsK1bPmY
usTAw8IfeIiiYBFUFoMmQ+z57eI0ZFsvWxP9x3SI7ZaDPz8Z3c9dJt+qdOuPVgMy6LPA6lQ23snP
qSI+J9Hl1X7QcmRKDfW2nZ1KexRtCHBkbrcowhscscuHJYkHlBIqXJ3ihcWhgBGf2orWWxCXymCM
xWUUK2hH6EzdznbK4yI0qTfIwMJmFpWoV/qNvG+XHT3M4lVuK3aCwgN5IumABdc1gqeRHZR+PXeO
0wQhzT+CHoz4a04t+2X61ncycrxyJp0eDs0ntkRhgxOGWwyGxlBUIT/lY89FTD3cuSwBIkERARWZ
JkcQXn0IH7h8KH7CrZOTrg050qO+RknXYQ0P5lwbwVONY/AQjlVb3jfImXX738fi43ZbhvJjnUUW
jF4lKSX5NbBORPsnfmwgRZYTL752fLdFhLRXdVMQTwb4TeLXMdAFkGNGJAWecqQ4D8zvif+EYhna
HWne71ka3PqbgLsikyZMMPU6KMmHoK8E5MWj52z+pgXbhx1MVxmMmMztKYTPrg2NHTsfr50B7wW4
L46q3vfu4rsixsX5YJXvs0u1Lhi1vEIjeg/+/Sxb7LI8pygy7RHAg7zDpkxe6EFwoZOpkeyIDVx8
CPCk3lVZJoLMwOH/ZrZXdB6GM033IV9aytTm3Q1DWhTXLbZkqWomHaYTLVlqC6PDT+guXbit/tZE
umt5/LpZnn2ERevg8EEPWjEf2m3em47tH2TF2G/hw5oXI+IQ+IOPS/IPpMk2wSaStQhD7dpXogci
MnryKKSdsg1G7nLWElb9nJlyyGWvs0mgANoiS3xxYGhBEC5LTMdmUecWa3SxN8sRXOL+pcE13A2Y
FC/6V9FzXRaYo9MQhSF6MChZN+KHSXdlXEnffF3lUGzrwDCNDbSwEc1ce9yhLsSSeNq3bdalB2p/
fM9rSJetDrBp3e2tVOlvsHzMWT5wN8vumIUenIT5s8OJFxMGOK2K3ebvcTfFPfADz4zMyvva8VgM
fBoUg3Wng+Ux30c5PQXFJ5ymQACovz5/xXvGB3KPcmK+X777gYv83xEOxrssyIJRW8R3y9NltaKy
Hk+IaZKl3HprHBWONc73Xzw6Att+LVqsAGPxU+SQG3yYuWyD+1Bc0eLgoIDhKRLhGw3Cbwrpz++0
lSsL7juIG8M9+iyrbXKj8R66UgIWjK0MfhS9QYcJWR5kWyMbgOGgNIE3S8Qllr/woy9yZyQygPsj
MpU72I+x2Gml71EtJI/ZVmGGe2X2TBJp/xJpB+MZKnjvq3HQ5htV5JUqoF937tSnbLoWBoGVGa0a
hfnaITKXhcnANBrkcZLmTiANGS+OnCT0nYjNOEUT3ybgxSlw1CmEewYLtCSdDXR0MoNiehqo8FVG
kN56DB/Zy8BUjD04pXX7X6ZAkz+u9YaJcU7fZ6s7MnOWozCfaWIguc0X9u182XyFRS+0fCpxkWlm
QpIov+atG6bB3pbfL66wN0tSnjIlyCI9mIX2EIFiT3TX/DP7z8mtT3/DcYcvlpiROgK87EDKdU6U
M1eFT/lVOtIPtbTPLPJOoTkBFgIqOZEQunkvTVVRRHKXKvsFlje90YZ5cLYA7cmCKeeAcKCYsuWi
biv5B0wjuye0H0sokM4XvTOydmL2ut0furTm7UkHMU4OwYSc7X6wspt5NVOlqMSU84fnj16uT/lY
WDcscCT8rzReRiVcgTpTnVjPfBZKaOhH+UjYl2//PYv3nCYjcY5MpwrvgtNxnrIi3RrpWRkcgQJD
PA24B5GGQg/595dADHxXDd9dUohJlfRM4RcOyss2iRF+aA7hG77AWjO3LlNRmDZjYM9CsuZb/n1k
qeyrHR0y0xMCKEnqE38/+w9wd2deBemR8nbZLfMc3bPLO8Fuo/8qfaFt7SUt9jrcmQUx7Ql5XoQF
HED00qqbJQ0vl8Eqk+U63jEX6akRLjyfrQmbpOPf4Npz0RnEdxNFEmtacN04A0JkZeiNlV92PhBO
ab5i6Ss2A4YCi7Za56YpSIt2DsH7qmG2j7jpCmnhc9zY3zci2GM8q08Z/g/aHWal2hjcBfFNBjTG
BT//9NLLdlXu92AzqGwET9bfORVXtdbn99JpyS++6t/+ZCMnccRg+bgVcz2sr8AYGyZzjS5wjNO2
A6Gt5tJLvIQA/8f2uJJ+5OE/TW8zR/TjjWDn9DdnbLKc6p+QOHbr3EBZKBHyJ9fmf0B6hpZfgfPN
nqeUAR9tciKcVwAtwl52ikSAIIZAqXTKu8MJa2nHW4//XhRQiqydaLDw790HTTCalXuf7ZYxp7/D
lekyX3zTvkkLAIT9J7RbFPSenmVv9aH50rQsnFDsckoSPTeLwBDZc5fmJZGPEujn03dp2HPrq2D3
Aid6BHKAL5zlc4EcC8lTmRr/HI8N98LdPw9ilREtUQgCbyP4IWaHcLPY5NUQm9Cb/o0rNnGYnFev
3U5fqPI1e6yjbkECzwCApK+uOPTMDNPl01gUPmSKI3Gvcjw3G9E7ZbxhQ07fBUcFBw+lmknBKUGq
CiBewyI1KFd4fFMn4y5SsNaNHN266Cu/qFshZMXrmxa5TjWWp1gyUGrJlg8vdlFGqSf7S3CV2FmO
a8WJ2Fz15VXeCQMdm6CWY8/bY/z3fTknziv5PczbH4citJWEj0xeoNQ7eI7i5nlyMdqhyQLWm7Mj
oq9gC7IvYpjpyFXzz28MKBSxqGQOMIetsVF0WKcoFTmCBmUZTNveDDkmV3WWo9LGT31pFp6RICX1
Tgwp4Cr8l/z47KiGl757UpuephGQej0yOvjTtHc1yWnoIRrPWTyvvQzWII8bR5M5qBAl+dL/FYQz
rF/BUX6SwF9FYXM/K2BqZx/B+KQKjSxwLduAk26Kby97dKYDIunZtX/AQS9vYbW7nRI0gglePWMx
P/wG389qP8x1ddTftf2fFJlgkZ2s8AN5LeX0g2iu6Y6u/Q4h80wtrKzLi0gbsL2Rn5T0uA+PDMZW
r5P/HjhcE/eZTbHqWCkXm/capp+V5ShzNrsdjZ9v6Uam2LpU1zrwh9mDBhZVwkHZZbBx4/MqDG9R
j31s1ERxFwu0uCKneYou4m3zFuzvmmOV0/CDqJpIQ73KA8xL06mJejJqVMv9KaurQndUATLu6kQr
c4/6+wbfF9XJJAlRwPn4giqNYn4+dvsx2HxBfticKYPm+nr+wKOxQg1Yte6mLugherfsli6ek2ai
O7N4WmL4iwXZ/Av2GJSgLXFcf8u+cTRUJExfPNZvPydgK+6Ypura/VsvSyy1NqlkzMuxKo1BXOHv
QM3kw9Z8lkmCSpAEX/Kbso+3TrPkmTABmel5OSDppdak8AwQAWNDI/ok54883m58KCd8L1Ut5Fko
t9PLiEiylOtZl9tGxADASLGttdryr/p5Ga2CmdW9fIbFu2wnr3vb7qfPgyioBKMNsmSXNs1/tAK/
bUHK1PTFOGjE9wKICdVGOPDTLfxhPc5TyioPEymFgfT9OBcHQTgI3rzuv6j3DdSPGhRWmDNewjXG
RSgLTKjFqrA1FAPyB/5bY6y92GihZwnWveog5d2L83MeHfULh6oxtEsQGtjcg3N5dzTwwhj6L3TQ
z6tn5LK6HFYvDSuZofr18nBuU53PreoqsrZ9PfoQa7anYSfqVeXy8s1/dGV1dytRH6DlwK7Uttz7
kXV7wM9O9moI8UjwE92b4bnzSVy1OWIekCOk9WetfV1BGnWHiJPmB9A6HeikYnwRWxQnae/FpRiu
TpmuLPAjxBUdTt5njxNv8sNljcylWMjLydt4TWv9J1PS47h6y3vAaPKG7BjvgnzTMSn2tocLB6v+
IuFGeUSsG+dh7VpNf0fFHqi0XQ1EMyEgBjDIYPi7Ng3lpn7JYXUQgsCkpZMjqeuBn/ILZ4jJvZ9E
1LuXLr4/xopF7XP/tOfqb4KtamIXyvUefu70IeeWt0L3DxUwNb6f3csC3Ah063HrmyJiluZdZF4H
9vzomte8+NmX312iL45Sws8LITzYoYNSgPdThj0n0MuqzvEWPDnQKchzc9LDIsaNZmo3GWcpTh+O
lU5xRC2qR6kAuJLXy7Wco+djHy3JXbZ44YwNEfDLhSUng24C6pB3B1tVpy4aZyStVWiOCR575FOq
5EGP0F4qYCzTZgoxJdFvs+HG1Uxl3eno5Dgtt4eHtET5mi+YLHnx3pk8rs6xxrrL3+0dgG7C9uBA
zjNRJxkSqjdpN4VTy8M4QzP3jxCXq46NKL2/jFoRPoSDxfVEYk4OSjJZaCEMeobSiTqZ4TS4E740
9KQE0n6WmlzxLcZIK7kDu4foFaKJ7NVFLiCXqp6xPAmElKfLqfMHFEWZ2n3cfHWYewpEvYa4kRGl
im8Oug7tVZImliWYz7X0hlu9WmMQnzyq6anCk6yecEjeHyuUThwspS+prehvVYdIWWEq49tQwwjZ
Os/S8lY9FpwOPxAggxPaFtvEODKa6/7FiyUziNlRgPfQSIXeW41qYruzdVgiOHQxK+/N0v48lWLk
HT1F4L04ZtRNIJYZzaY67jnTCO3Vs7dbap6IjKuquqNh5LzVQ2c7rRbtZtIW8G5rK+MjXPruuKiW
6z8BPOj3aZ9cCHZcc50h5Z3omlR3B5ZU/dYbMovK5y2//LbK/06BNbM0qwQw8fUdYtrICK8DIUV0
uJOzZ4Xw/hUmfNPXunLWFudEejtfr0+G/VyxaS50ZL7Dg+BNxt3vpHK1ZkEqIfmczobmN0goW5Zs
ev/+K4vQ7JM7au+dODDuWOAADA/WpTfjhMEMDmdHUJTQKIMRpe5UxsytMBdTo8/jEYgJIuD7DR7h
KWJA/fC2wvCGj/W/jFE+0mw0twQAHQwyX46/uFKppqDJn0qUBo+FYzmSSaA8BO3dC0Hicltn8Yjj
1RqkG3/Xu1MkTk5BnTzheuzwOTkNEv34a2wVnkJ7KbscL2hZUNnItCjmH2a8TyO8hsT/J8yWSxJJ
saKiEhz0RSYgFr5+rlz4h01zLXpuuP0qKVPcVR92k2yRtxx7AySW9m3qvjiL60cQtJujyoEH5UhB
WXdGXaxd/5imvIl6dpRMFWejqN3oOoSZferFn5uX3SpNKrhJ0zzHHyeHHx3l5ewfX29S3hhYPCHL
q3vX4F/Dd7eZSPmfNqulD1smUYgd7khse08nxIKdD+k0IpV09Le3GKClHTlC6JTt9KqstG6fWXlA
p+frTnTG5HmjU4z4FwM41sjSeBqO3P57tmSGZlqh0AYHf6LbqCFKQMEaqq5iZ+DzmoZXWZHKTVSv
ap2m11Q5y/Lx5bDVPXOaiapacrd8/asbsr4UOlS9j07C0QthfCd4j7qjbXRPEOHCcb3/EAu2F0q1
MfvmD2izwAMvPrzKDKnQnDmLwz7nFrQDVgYGIrnuoplMF6LfwBM0ET4zNiYZGqw47cq6f3Y8ShEz
728gd9/yjvt8RLJT1rhxbiUhaujgn5plIK9n5fkW7UEcoXPDzTKwD2RjwKKRskrxKpFdTqIe85ni
Ifk/s2AsduZgkOTZLJ8VD5m0p/OINeX7DmgEPTYMvnBHqHRN1opmapdzZxE8IPHZn+CPeqvhWFx6
AfXXLSSp/bg6+Z3WsRyZyoqvVgw5JrDjLpuZdfc5bfC0N4fuXOVFSSkNBx3TCKLQP96VlAjU4iuK
olmF52Tn5fl9GvuGEJ5EYblvAO1YqBpjYKquOEWEYfCRb34IM3LoUwPs8Q4B5mZeWGeet6OZt5ks
rJgfrZnnOMr4yF83BeDFiYxwX2uRcG4DOnIQgdiRlMvBFk+g5Vqaf86gb3m2mWcGPFV84KTNqqGk
dufbjJtko1J3xpbO5o77uLSlSR3UM38p+CVb7JyBdKhcyGhyT9CpT5iZ5tCSbZ0V71ERLyr2VYzS
wA2PphVm2sfv0tBAvF12jWAMb44W52T4sN4wWXgqzz9CuQ9MqsUQyoaJty9smCbW28bxF0zwje/G
eA4XygqCFXfmM7L3kWlegrq0fZzUCfxitY4tGwB5t0yXsQL+s/wGpSZvoMAlW2O9xpp6R02vtF81
F+Fm6kCBA0s95yFtrQ8b3yvtxM6IBpoDRXUfEW9KgHPg2JJ1KPbPQdyTRQqSrMpviT15SQHVMOqw
CUYCsyR/mnu9Si/fhc5KXIQQPfoFIFdj9ZUB58NB07RtIksCsytqCi6T2eOAEBU8ezc2e2+8gTdo
EyhYd5EGUKWg7b8XK7b0nZYLOxKc1zwo/KjjzTa0juXtixZXZZytJIaZzulGYiBXyXpt3DWDdfRv
cf6SyK9ZhFetn5+h4A3uSJlEgJ26ZxKB4JWwd4zzxDmerf9S3OdHBZumG8hcRMoFt2Veo7syDX5x
4d/SfMttMSFyqtEU4wzdyq3OYaTnrEyCFFbHuwxo+t0nUAWjxGQewRNmXBIXQgtJeC7aO06LAhkh
RrqeHh1gRo7bBt6JpqE0qSvwNAhKzrTk9JD3CDnpI5Que1e0YyYe9v1hxIm5GSML8m2vdeBy+iPL
3/4MKcdF1PCHKY44MWqXurOtn+YKgG/4WaInLqKiomQTkNx1mf36ESJUKZ4m3kzxlEch0KwS8J6f
0n6MPnt9FuXWjLE+9nPHYKCyn5nWHP7Ovkv8pGJEys/cUbEk8Jbjptp/2LhIsIxSry4lcnmaVOzN
b+ExBtV0XnqEPMjw6DOMctscReyyUsK4odp/IDwCOt1Ye1N6Ff3xZ0TkFWHHzreZM9i18FT+LcoN
uPFmmjh/t6eNwt8K9KFgPwucLGaaAz+8oc6zRSjpWiBZmHcSLt7yHjzKCaZrhtoI703ZmcBM8mQ+
EQXsQdjykbISveI1y+a1XDf2U730VXeKwovkhumkC4lE/BBeXyYTSRMojZk185RB161pkPPogWVW
HWYtC/gt9TkHf8mQr21uymeIpe5VqTmBtuBelOKMQsP/ckUrcRRFr+Pgq65MRvLFfNd4QgUYaQmM
1EBnEmgbvmKJPpSB9spCDCiMXEZtN8H4wusJEr9Y3Vu6dshYjgSMVKzjcD/PFJqZVdAjAFmPeqLA
DsV1/2Xp1ldY9tRr6t0ykaKqCl+QfQ7uf5fE0rYgkmCp4QCCRWgMin6Ftm05GENsZAB/KV40bNQm
Yt/+pgCHwNTyQEhXgJEXdqFuFQ+Xzh4VXl7j1j8RCi4c4Ztkh1Bg0QC101QDskIGas6aSfZTkFQq
341bugrfKR+FMZisAyFqdfKO2wDLz1s9fPAPWIyazWWGp4GTq8A5WcP388W8wsnyVHsroqP83Oo6
Ng2i2GCmpsonZ5xzgj7/ttDs4VLXekAsK8ENJaWxRmRxMwtRiJPnparVd6w7T48USzvuQfwKYqVA
PuGSyWk9x6OX4YdkyVrwDXhgRrKBq/uhHnDb0BhFq9b73wcirNFjebvlcYzzKOX13AeVgOuStLFR
VeKIt2CU90i+HOQLNbYuqqFrEbl58hkC8jD/WeCarc+xDh10Z4/whDBWGXZiS5TnEWUaPYfib+5s
cqmWJsS1E+fz5chiX5mDoR7vZZjvG3msd6p+FXtjlis3Yw3i2oc6IYdXFw5clefGcF+qxWq6Ylaw
/g7w563JO+me1WlmSIrTI6JnrxJ/t+DABkehFDvoPgJqRknWauKWFxtdejmdVoGPXk+TmGUET6ia
ZdifQDToooAqRSOCPhYQ0Fl0IbpuaMPLC2cBNUw6Qa1J46UkiQbddesnfUCRvT1OGQZCh45IH28E
CEySdO2A61O6YreI9rR5De+j6LtiDK5F1NyQ03xpGCDSLY3w8IPYtjUz6raOAbYucssEK2rb7NSe
D9zAcfaZ8aplmzbts5kbv+n28r3Ay4VXRocaxKMJpnuIlgwSymhzdG/MI3MpjrE6ULQXO0euj7K3
QFJtF5zpJGmY3MS9UbJH+CgIffX3q2nADpCpTghCR8Z+F9Wl2AcRabfaEGLhIjfYSDJNeYVV84hW
De8kcd6QA3+3gLfwQjCkqO4e4j4f6DvgDRip4zZeOMxR7B5NvzIWnXnm4gccscKIL9BCr4QPm9eJ
XzRWpU07ciwkuI8CLqGkhLcAnv3zVJivdOn0QY6Mc0/WJyJh6CIcd8FoFUL0EnuACVVkhWQ22U+F
+L51gvEz2pvR2fELYR78GQleD518hdy//gYq9kg1nfxCTWlC89G1zGTL4IksI8e2hFDafP94jpIr
+Z1UjzwHl3PePWF/5H9tvHQOEJpJZ4i/uxAIa5vH/Lf2ZEXTpceBZbnC8iU/0mdOy9LT8/faEGRD
uFBPRPYwPkZrtKy7pLvOhkj06rLvq67iQsG7ln4tbCadWOrkZ+qaWmab2ahihDhyFuVD6df784d6
xzUjDjVKhSW8GjyYQ1vdGw2s9pIJZYQeN3CQvvldMYVhFN/9VaxugdE6B9jCVFOt5hXx90QuPIKY
YQJlB4oIGeYsIJoDU65LtqFM3OFOpuLziFtlQX/yNqs+H+Z+de5fPk7Ch52nz1K/TW/sn7qEMwXo
TClYuJQ5pa0NB/11YM92UF6V/GdARjlw3OmkvDJA/y+hfAAu1xltHABntA1ynZZUE8avcUuiVfwI
fHjvAzhN3fWB5xEEgDJWDaeZ+mV10U4Zz1F7VOFe18ha5GwMQvTYk1PhnSAqdgK5SpIqi3mAIccw
0f0vg9jhWuqUoA4OYaz6nCTfItBPqQZrm8uNIdabuoxjg5jlwTHGV+3XEw+wY3TScbLKxQmtt8+1
Tbl7sJvFUalTOYTHr1myAjBr7nUBmxvhY636vlctFt/yt8t7MNVdLwnpxkpqkrCx1z9JB4MosfOT
YDZRK3Oi4bWcdKS4myGfd4NsgyWyLDL3YMM/s/u7PssKmfmQuWhHh0MCsaEb0HyNa3g/S/cloM3X
zXiSfQN3Yq86xGGLwuP3+z7C1vBnG7ru1DVh5TanbhlxOCg4qCp6dkHU//xueDycESBpy13RvoeJ
cDYb0L+DVyBe0twsMSu48r++3nTYTi0q/tIeTKFkPqiTqfMVi5DF9qls63ZbWr3UNIRsIfzk7jFj
E7r4p2mO3BelIfUofJ0HR8ioU7q/xTE2tf8HylaDWsQtaZkB85F1c5SFjIGVbF87szYX9n1S8zVl
uwyYse9sWx3v1PDo5doBZ9pcUohVp4yjmmlsctQkD7z6e4gqxGYNWRc4/ym0PSnN8Y/y9Vvv2ii4
9vG/l5oPu0yhrhO2Fw8fcO89hPGYBJNpsM/PAFJAY3JUHtPz4MX89CkNYr8Eur5CGGm8IuKPF9od
Pz/AcCfEQaWYhNp60HOW6mr91f5dkjRHqj7ci3BpO8aTR7pqEGrRl8O+nJRnzYa6Nfqlu9KL451R
NKQT58jls4oxfhl3u5DEjyCon+KBaSTbEa9jrx6N0DeJbFxseR9ifwMurpOKrO99sS+dElaaF/2h
qS0IFMl3sJmM+oJOv4EQ5hW+jsf0GFKCQ2eS9NsMDwgIjQThhDHYZ0E5HHIJlaJdV+cuZVqnbAXK
nkaf50z1AQ4483V3ECnpvF8zW33y5Ytgmrd+5PASdLGwJExy8Ff2nDRljupWSg0fF35y8x5hl8y2
OU5GkXh9NHgtVRiLyEc0DtRQp0xqsZuzlmiqirySG7TRK4Bk8Ur20mhWZ+7fwrt21Mg7xj4b3ndg
vQyacxz4YkqHzRRLJb9DuViRPG1dbwmmgwUMwNMCFKTfKx4RAs8vyeuxVgacz1PpWRY4w84SYs5A
7LReUFBIxsO6PLuZ8RrxwXxGcshWMhG4yGR42S/Hi+O2dJdMBw40HyUiWglqS7uIn5Y1um+2CVbL
xRgBgwF3hWfgx/uMp50u2Puw63KU49boVK1joGf1L3ana6k28se0J0LAjo+P/WU66PNl6lFQFRNy
CjS6bi6KJdw96SnuJfCBnArs5uREb842MmeZmgXvVVYbAv1C+ZqOthmO8C+BDtbAH0hvU4O000PF
ibn7cbYVGE703Ni3JPNQYC/JdTs2ivrU0CE7U3rREXx0abjVPy6q3r0VNK/a2BHbQy++CsP96kLd
0+fbWVoOlJbtvvrcf/SD8d+Eq2z3yJiOT8eZu5KuQtEZ8Z9t6LfTkVsp59PKjfOOr/V1cm3qKwQv
RhhmgiBENBiL3xOk/J/FQHGhGrfGRiQKSMD1dAKFFW57bFQUHJIPHwHt5obxaPiPQHCsng+jehP9
UkYW9PNon0hDAjy6zxBkny45iyitTFXMl0s8J2d5P8cGnpUsEkGg9u3GFii2DAagXHzFEyuO5rS3
UpaQolgvg8wkWxhdXUX0pTWHiIwAI2MaCGuS+i6lJj9g05Lz91koyc5TIOhiNtjz8qfpMZCbU6S0
OkL+cRi+2y+yHJd2FzWAPepS/77FuUl/V/94GwjP07lroV+C2+APCBJvx8yVESf2TogNKwwxIIL/
qNaCfrW2/ol7LUTHz/stZsuDLuUtgHE/lf+iaCUiPK/8D8jIdQ1qLGuPLf+i+4cE0i07EfjNkodE
lSB1t33TMEF1NSCQqgxknL+7Pl4guC5Q8JTXy1QWYUcQ3yISLVug2UoSivi4Wka4vt5LFbN4iV24
KTlyFzMJwfB9AWH0RPngNna/WJH1zZjcH+p3Yde2BzdmOMlH5Im0Q7DaO5cWQ+eeCYPsk2fJjBLH
098dv/ClnEySBpuEyB5fjNccYFiwRBB/t8EkYk2Wz7J5EpVWPirBSeN3x1paAdVKFsDkc9QqtF45
+cCUrSxCC4LcHrw+gimvl3hTUYWCDzYI7NaRKxHEWKXITRj1xzl8VXodwAEz+LCyMyQpuA1Kqbxa
Owm7eHn6LsFNAV/1JLt0djrXHmQ8Zdo5D4p5HkrUd4VRsaF2yRK6WLJzsPEiquD+C+cicZ2FJl3N
jELy0E5fR98kG37nBTRgE7yzPGl+LOZw0t6N4UQdkr4zio0ZmEkiRNV/ycbpE4o2nKd2S+57EVM2
hnGDdVNuDcnkkIUSh4ALtv9IoBbVENeOW1bBy9akVmpsmBnuiA0gyu1YZGkM83lWkXfI7BUiJkLo
VoOWYP5T/sYnSl/fwGAxbzgEDfcgT4BZMFaRYd5jZAfBJknfkfVtQjOM2Dbad9Ptdbo+aXs/ISfl
4hEBHsfjbhK0at2QSAf/Vm1QKJXU2cgFGNLbFGooieV+0caZS2psKl+sFOAd4cALE6bB6Rf1N6fX
J/AF1cFU4ZE1CgtMvGTMijl7J2CGaHwKxHtbR5bv/asFiElykGdXWP1MWeOfGbj5Wcg5B3C9NobF
eCMVWi4YeJt6qJI5ObsYEbIr1SSdtOb4BxrSeRSs7WYImcoFsPnCaFtySALD6CBakaRxR+lEeV1L
Y61qyxitHZSj3YqERhUWoxmF5JFrCPfig0mx7j5XwgGt7tlbcKz4Bm8/murIjrKWd+0LEBZBIK80
xXJp4yEYCSw8xA/3jydS5kytGAibOSEzNpty7/J8lzjnsZ4VMiXTPXPs5wZuLD7M5upxgfhLR49n
iegjC+dAltBg6z2lycbqd47O5McS8xXL56uJCleF832A6MxjXIEl1HXcnCxfRfVmfmMBRgjLBvSU
Q/myuPF80EtzLPGQ1/N2RSbUzsrxzpLUCFAmr9V0B9JDbMhfyVTITKcRBi/G0Zu6iits6e78Aqc3
5jgHkddEnR01RcQnjNaV7DFij0eRlIdDCVrd7JmKIsgGOLCAnE8Up6GbBMeM9JjMwf+NGWznmPMU
vF6KXC1/cU43pQH1FhWyoTPFDdrNposYp05FJxxJ0YE78h5eKDdYi0DXLAhIVd3iMUoNH7pPMZJP
un4D1DOUSwyrXkJ8fyn25NAJ1gJY0EkG8umvDS2R5xYf9pK7rn3bKCeQH/UkWx/5k7NWBCd+Mw12
Z95JHiRxlNqn/jg33/+PF9IRTSx9NzyNO7R9pTZPfLGk2hd9KyMkhzcWXDsYo1otx5SzmuNj7Zbh
nG0vHvTqKb2KfemqvIeFmYm30HgNgZ/SRYWCWlAfPf/yDYm+0JQodwhWFNVJTLp/w4ZhzNVsI5dg
5pthc2tGQJHHmsVuYzQsdkfVcR8Ra8TLip2v5cqfs5RNnq9A9UvPZuBuzGFQ1YiiTdzJG6tevATF
vrkROPRKOSBS274BAuU6l/VRo+swpCn42qRlFe3E9dP7rtQ2R3o8fgq1IGCOYe5VSUkOIQbglYHt
s2FflGoE3RF+kwKjPcJBkWnhC258qWMJQDrY/tokQsdn5ewdoe/MvbABy1k7XuRXbOjPBcPxwTCq
jU3v7Y6Q1+1BTubrat53hZ0ebx0V2v98VVZaVj5lcoBuWVgESpEQgnZsqShz23+ODDXrLaz8iIZ2
/xQHyMfWDS1q0sDi+riWPSiav2elAcin18//F33b4NaXQIotYjE7pLs22z4grAhNxFYqvN/mR6yD
MW4YD26pXnlIZcS/4aPQNMn8l3FSV4SLQ2+CTOC0NNE7vJ/a4j40MvZfibmW0N9lTTbBk8TsG2rq
pJymrTybdqWmi/tWWeUXuhCuy4M8K0ltr5j/LiUK+I7f9Y+Pas8KKFLe5n/SIHN1zvm4smV07N3s
rnOvKQmgsyTZ/jB1ibkp60wjjOs2be5G1m9NJEyk+XAGwcKldizrOpioYJb+ESvpYSO4FnajSA7y
r3H97qYo0F49LCOBYdAmkcDyzDGM8A83UmpXW2GrHuD04KNmn731hlNv4WNXtZgZP4Fm393v3wxo
Vw8dmfU4NRRSz4fuJeO7NUgSsp+jwRUijMTfZHaF1aTJneKzIAB8m50RtczE9IesBXogyPVl/6r4
j3QRZ+jhyPxJWwZXil9z/p9MYF2bm/EDxiILuuGtVJjx0KKpdduAp3hTPJp+X/nvVzXvQ/rJtKi0
KtTWo8On9xIT94b59zGo8hde40wP3noA7ZRxSFz5rFicT4qPn15qc5HqfKG0+XMo3yQ3jN2u7Q9P
6ABIuLs9U1GfnFAWCDn5apep7Yn29GXSL8GPZHnn4biKviIIxatEjL3NhgIpUilCRBZ1VlwIvNm+
oiNqHYLm6/jILZq5SRwK5QPHbQHxeHpToWG9E4lNGT+TJyBr33GRtd8yuvJlZrFmdtK3HzCpJJ77
Ll7Cu/uhZQJ2iCmv876HyzC2lRaz3ArohEwbS15Daz1Wzvpit14l1wJNTWak6Y7LjsGkvFYv0GGG
WK5DHvxzVRzYOCE82KmUr8H+urzCeJPVz5AsuNgE3N0WS0s3M41db0xQFgaI9uYlhOTGJqxkNf4A
2qAV87PORGlSxBWmWjpvt7VaUo8e4Dq+AovPdp8De/t/KqSceI1QR7XD4XyWgn7+TaeGE+/Sj0D3
BGxiizcSJjKj76tUjJPZR71kmxOy2woTiU7KZBy8i/lhjxh2GdJuhFNehz4Yu6ZWsAJ/0QJ8M5e3
Fvu2sQklDv77ail6pqaWr8xLqt0LoL26xEkp74/TLXGoVRPDhpWDzgZuQYSzxDfvdLLoPMtqiHZW
OrCc4PywgwOiV6DryeN4MiXnE8fWOtc5MBo48CsqeC7jFyl2Twa9eSRWYYcAUts97wkF2MMkOaDz
QVAXccbNjZN6abS9NhXMZ/ivL3Ud/0PkSYNXW9FbRnw8opPMt9bBAlagAqvhRb3pwdRJRr4XrWPq
1q2GQY0qrlR0YOKKB2qhvHvIPHtxDIcOlSKzBz/no/dd2i0F57Zr2ECyJX0q6p7RhHkPBfyedS9l
P607aFwnmKL6PCnkIvi7S3d1lC9SjjdyF4ykaKZQllgntVP6Bnn7OVWcZfrKr1fO5So+VEVvCClS
CStZI91LYZvpYcbEDCJxB2VXDJJKF8eNOWAmm2NBW3ZwciNKljfLbDKdmlghLUTI4OV8g/hed2bv
W2Ou1eBcBWBGbLUilv7KYiaBN2QTCYJpolYMeAiI8QW09S9aODsGyxW7IQOl+rgJuqRW+XFfKvMH
ekNsqGmsOiyndAWwgw04702WixrL8haPEDKpt/3n8nnEWbZgQGcuB4/BJclSLjUs1AHxkcomeCZT
x7M+aGBxAEDZTj+O1K3pqYI+ufTAwneyP2ahR4W84B77bAZ98dBjoSvuTEEmonnrdCliH8l2fLSH
uI1LuU37+W1kzj0QtMhqK1UvBRRsGbTDYg+S4P9CTK2O8jsLW/GPDTiIJORYXqcZ3Ytf253TniJC
/S/AwQBTXsacna/wRXNWLd2dhqplZD9u9wuGmwmPB99abXP7aLH8hfy94ghQKKePLoQ92vssszh2
+jdVzYvonErXM+39faJHy8/xLNpxGTgMJQ3xUl0TCtnXvHgWQooSqvp+GHwm+eIWKDRaY++cBBRv
zYVlrwV69z2tW9NRXv2EuN87FcuPJFyeFWENG+46ZmcUqene2nvxZ0z7AD4N1/Ossi5nARvfVcQO
SfeSDr14XkVcuyxAa3joosp6pXEalfcYeagP/YwJNpquJczrAHX5OWpFct/Z3wvu/g0I662+nTXf
31fF8d1HxYlQjyrUspdlGlask8QzGOQlJyX7Tqu9Mwh5gH2/fHuVVu69kaB3Jr4lDw8O+O3qI7cQ
R/jCn4ofW7IPAhZMV52moLJbNvCO9KsMhEVMHfDRgPBIV9U8xNdq2g+XfzjlW40YK+v4LLFn0GAv
YpgwVKC3N7F2Ttlt6yzV2c7XeCJ946bSBqS20T3tNsh5SpQPl5YLB8XeEvE9gVeh/FKd2LQFCvd4
sRbIL2HnwnDVTVikcd6geVw2LbgTFuKrwPwREfwAor3TiH5TsDhcC/+sNoK2zqruIig+YxcgWL8A
0UUHuF8kJFE35mmtoTULp8oQbKpz61or7M+DKMjh/sKwz71RCOgwEh03WkYdhAA1Gwb8fd3BtORP
a/fOhRQtWJDxK7zBfYJfH+3jddweE8LRkyq5iVpGOrFKYOFbZFTIxpjmMG9NTkPGJ+Yhh2Xp4ZhU
vk+2yhobP/Ia7/34gpqcUwdmxksBow9FleKekW5OTSKK0NIksUfwJVjoXCMgViSOumA3bDvOFUxx
4iluc78hlrMyFDGQL+vW5IjyM8nVtEBhHzgdfQfFFJiW+GMs0AUkSLD6P/vB7o3JiKyfIkBTFcHF
7fOk/b8aAmUndVfiS5OiQryTfw2rvbXM696H/HVXYFmj7Xzie4u3ZH+gTjxyckQ2KtOQNk9ek1kk
JEg2M9OfitQsgkcKLBXtIpwichMyOE2GS35VfIS6F7dg0Vnkgr9WRqUCOcfkDm+G/O6zMnI1Lx1r
vrL1pPTOGTBTUqieeMNZdAcyGlZs0L3hSLGM2/44wlr/0GLjFp0NT5BxDl5GuhLEaogs/sgqbBJ0
OcCzFSASJAr2lpkuQpnCfwz8blbQPLj3ltH8TpSPms0HlYxd0KSSc9yn4tVePsJPTcBzxMC9613y
gRETwfeoKrtMwWJH6ssT+0ko+AFjO992dMWD+6pcC6fgwfn7tYz9AZCTb1XEwy5QZzCfcrbJ8AyM
KBJNqJIAvmiBWG7uc8n4DwFMMi0EO4JxwwvbFUarl8swjOPp+m0d8inBXiQmtlCILJ5gtqcWCabW
u6blBTW3P9khkj5wNVTpzuGu5SpbRFG1Q1wC2HTStQ/8KprBzUqclBkiFGTRAKf11Yjn76NmWqLo
x365dtYhca7iHCPqDBrIW2/YPub8URGhjqCrYZCJtou9NHQKc8VbiIdfNALQXv3N4UrUai8ixeoH
7KVE57aYy5ts1NByqpUVNj85CRYPizyIjmliIYXFVsifB/c209/ry1eaboRgh14tDsbPdTUYrIb4
GdX6xeuPnJWS0Gicng1xGr1O9lQ6b8WxBeblPcXqWlpsE0XwSJMJh7UvoDzWinbe+Xj+gslM9P2y
Tt5CLuWpyztn5VoiwuHiyQJYPunbCTGcSIs0KdgK/kMaI/0JSl6yI2xjTbWWPCb/CpwE23dZWRmy
U6Cb9lN5vpIYS0YHVSYFKLzVNFBBwvM9zQ37GtUATtzSavV3auHB76hglTLqFA4ydSx95ztUjYg6
qOcc/Sg+9lzb+WNnrYxXmbfSiNJ1irASIhM1pXMLiSweBjmz3f05AhcKPHMnfRui93SRwbX0Iw+/
C7Mtxp8qsTT06DYAGKNLgGaPwKKRa+OzBMbR0DyrF7LsjZ1UfOyS+azrDK+9gRFjUGA6XffSFKXD
PckUos5GaK4uNzuBSqtGgdGWFpARtFdso4ZgzTWpSqVkoOCFTXgpRQqoYexLe6fZvcw/GCACL/pl
X3wklOTlNYGivZy8PX0NMnnbs6CygZlRb0145mvwRSldw6agyPxNgmPFlaqdNkyiW7BecuMFQRn8
tj1dLdhHLDoVsclQglokJq9r6ZfNT1lgOPk+E4e3euhQGq2+D3/3vsvb5VTWoKA/jkSn6Wq0sQu8
YjYEWu6a2qMvaJmQZf/gDpbmv1WZ7fjRdCPNXwFLUkQgXVZWDJ+OiaGsfdDyw3c6ytMoH5GhoUqc
EnfzRA9KV+RmuRSi7fdwdKh6rGGPX5P8lt5fmEpzyq/lmTq2R718JnjIohe2b5P4Ec/IRMGKhIrf
7DYYkEGnSw3F8fU09csXfXBz+VUjbshNwtqUGWXhqSaQe+OYBSRsG7xrOvlk3DDzt4uzxgPUVYWM
RFAjMbzIbcff08yWaPYa87Cnr5Kzjo+i+HOipvf2qTwOIbVfPg2CLeQSg0WL3EavgkIaUuBXu6nq
pgRoUY8ztq07iiIEOl5gBre1H+h4jyXjXwGLV74mI2nMpsfqxP31u8WJQVVnhGgbrCCVriQLQDva
6TCwi7PAdOHM0zNyVznDVywyOj6bAX90Ku8R4ifw1mGAoBtOepg2Cb6lSHUJjm4LSn0yg++jqJiP
hL/aFLXQnXZ2ieHE4ZHBghF/Eo9ryv110WS9amNMc949P5oXfI6xbbcZ4DRyEZJzHUkt0olznO6F
xysQr//K0Py9FZZrmxI1hovM9TmgV5JUyWC9IcoW25t+rvoCgZtNUrv+Yp3i4cbANZpUGk2x7iDr
JyGh6995+LAP3pJDRRlMD7UudBxFQ7E/06rhHgxyWhng/kaJNCtXJw/HjjuyNynwuEiAQGEZu5i5
VekulGxoocKZ6ZuD+WG1wLsPjB81r/Rdg3UTFay2KnEkECQnlyGiaeF778Xpp7IdZeMCqqgY2p9m
L2MWP/l3JCHko5BO7m45BUC2msgCDD8QSowYpjHavqWA4vIe0iDyZ4+rXJiikx46pUEzUheUEMuq
JFY1imQLc558u4PKUxjs04HQkmBy4S7/uWNfvEGNvjQ3/ceG/h7UBYMRKPe81FONn+qecb9eeJIG
iMvwJ+ZUcp2LJcGhWhTVjihvHjd/gZJIQT+elfcmYZektYhTls0QVhPdTqaPXb56zsqNT+AUH5/k
duJhDUxcarndeEdD1UKK92waWm0hqaJAyZ8WiVxrQ66XF+YVfMWMLk/TvqbqJEA0UjoboFBdpZZj
3Ia16dKBlPN6TszzBsmTDiBa/854YezikJtM3TXCDYGT7zxhmLp2QmJVJlXY9mZt6eXGEQ6VEuPM
GqVFhaXrKgZ5Ud0xPOJLNLWiRSWkXtGalyOVh8v3V4vyjsvtVYo7CUQnjCItUKDVxwyUeB28VOzc
wWFixRCmpXkbJqiaEfqDyzKvHHWpareLRF4nM/y0Wo3i7scpshYsg/3MYrngruHQgaM0v+gTvn8N
DQde6kvKG9BVaNDmzqMEEP5/P+o6MSUyccxucN4h0qj0oi8AzgR0ojfgDI0FwCjDRg6+k+JsiCBa
F89CjOsTXQz8GiDapb2499nRWqvKxPGR0w0G4+J+AR1m0q0SUP7dowMLc2DoMl48RLU8LUyKxF/i
iVoizug59nDKZac48WEMj+yWr7q4TwSSi8yQoUSLGwAsdPoCcuVPp7XI9sg7EuZ9dpEeOIbvlQDr
645js7L4MH2cUbcuptoUt6sgNS/668GB7VIRvfVbQqsBG35Zo5SNmEGmD0x5dQTtg1qeKxa/dRpB
7sYZlIUd3ZGI9j+BNFIFCleJjNU5VxTiyt643i9Z2OBqI7604Gi+h1xZDO/NUPUbZhvHuHlzSPBE
VL07zEYO1RVEgt03Wt9dKVXJkymwwjp/25zyX3KN/5zkv1ZZKYt2N/HToyPVb0CWuH04I/UOicAE
Jlh8+ZmWQYJBUzF7c062EJJE3EaOhxNDRFWXZNCjmcvb3idps0iNnhmHOqiT73EWWnyfwsNJOJGH
9YVPe7nMFltHpZLujABJI1bjO17cLm6FTGkuojatDcFblWCJCYY6R+Ygy6ws/diD2n5/iF/e9lih
fKxzS9pVi+TFVB+J6DjVAPcvngvWrlP32MKC+4+MAiL7s2I/edUfJChJi3xH36Qps4oNf8DvtYwZ
2nV667g63xDkFveVl8AlwWbU2V8ngHpfQ9yjTkKyYZUDpaupbMRCBZOsC/NoATMY/8PDEuWrl31Q
DG92cL6IxBzqjUVI3Sa7QSr1p5UtDJlwxvAF7Md2U7o0HTmkvGSkt1O+65KbNZfaj9BwxGJGW4lf
NzbBKFG87duAlWQBf14wr36h/+3ECaAqHngo3AfJAZKDz8Fr+aIWjYBpibx2kD+IIX0y9U4YHktk
UhCVC/LIvHB6uBW/i1Mt2vqFhnoUCiV0jPutrNBmIx2j8hWl8pW/fFUa8J8Z1gbj0Bpt4lup2MEn
cfIid2lcj7i3pYR+EkPp1x2fQreenTwXEbIDumM4UqGqxSin8VaIM+g5qCHuNTlDAvJabsOTSocN
5m2wq3qClzqlcVnHkAJ4MSqr8j4lKH6aS2Dd/9hO8KSCAG/9cNPrwdEOPTKqvPTEC63ISRAPC2Er
T1NITasNTx46NF8dZnFxA2jmpQrPfgPIVIGCuiruNM/aPU0R+ergvG0/z5/lUNItStG/d2s7KMVM
jeuKqp0//qgPNU0RG8pQ167gXP91sGDh/WSC8o8cbBEiDkw+bqhENtHe+XsUopD33Pc1McXYGBSB
npslrFKw2sHR9flj8ldIcUuzFBhhUvoZGmTVTXizeslHThMoehQ0OuQkoLYxCp4quSaCNuDXpP8p
iIlP8YKOqQqOcZGCtV2bKO3SibxiRei2qywxkhN3n2qbI2FoLTvt2HfcVgcGI2ftFEFoGTbzEiaM
b5IZAC0P0ynvc0oGQq0iDBWH/PuIrmSvYLjrSw2rTwf439ji16+Blc1iSeJe5upSUH8mbd7y7V2V
8Uih3kEX/ISIY7wkyyGtRTZkaRrmWkJ8VoicoJkfLnXcbtZphMxE/boCkPesHlADS6AHS+0hqsLM
fu2DAOyNDZTnW0EXeJdkZNE+rE9Kf4oGpEvJUu6hhhr/SUwiX7xCxvrma6S3nQ9Hnre81Z0oqfu8
MFKHLiBPz21A1i6u8/5ZZlAuaGEux9Jthi7jF1c87xiGp2E1LtgdRG0sPQcFDlqX20+60hyzyOZd
gqOppr2+k3/byD1bCdLvcOgU0+p900Ka+Z4K7AMagcFOq/j/lrCavq2ar9Aszdyr50mkqowtHJxM
i7ySe4qj7HDGxxd839Jlp5RlzQ8dFB/BesknU2r9H2P2NfJoqzdwg83j9VRin6DB4fvgv9LUUrc3
Hzn6b3tcG8wYNb/SH7XV/F0lKtOhChTgyJfIKHI4/AMb6B/OB1DNXrD9qNoKMeaj5VPopFEXEF7R
1g+HqPyMgR8YhaHVmwIkMsYzO0IjGdy3VjAgpRFLI63oPMFt//NwksMNxwTJqw3Q9/i1RMLfMUXl
hHCPT9KuiPzoniwuFwV5p2OEr8oYxhtPataMaZWbMWSlf4rbV27TIQppuRYHmCuefkJTaw91dUgq
TuD+5e37S7GB2bnfg69H6sis6LL9WxWV1mhWvEcL7je11f2uPVj1Zbsu5842rXUOWqkbOHAaoE4K
ISulLSH8aeYP7utravDkbxSCMrl9xFngbRi6T2Rc6TPcpZOgNRR12wboqpy+QU7cahTRweLw92fm
cTzWHAcDx95xwkrn9+jNCqN5CeQWf5d9u1RTzG2noUxZZGKSprbD+e2BGKwIV4+LHyjj/EPQxyyk
SIXp5pUjNV5QbHU/6QUzgziDhbNcwFYEbMGwNccux4ERZqt8vve0PmFwOVDG3fficqjjKRfsxcW1
9djmzaUaX1iGIQqSAEF8wb7cAAoY2tPzcdym+Vt0/hvo9XbbazSMbHbd1bDahzk9Y8HVBCv6+imQ
Pp9j1iKhgqPBL01b353bOzM20hFZ6KhTgy9av1PmqbMVUR9AXR6HYMlFfOwBMhRC9r+U+Z4R5V4x
sKKYvujsRaayY498J4Hm/tkeG7QIglE4AmYi2+COpsHTkR2ZH9fbkgEE/3DE1yMCkF39MDMcoLM3
uYpTbtzUNrazLGjCB5l5flCV8PpBpp370KDZi7F76vJMBdEo+uTjbnPmMTiRcfLel9/gPENr5800
L0LDRKbzcE8+7IUFT1XiYQx4Qk3nvidOuLMIWDRYo1rVVa/jjzw96PjWqAedfpAix4P5uym7WjC2
MHisjJJjQcnTksOljBtolAqjMhZnSmCRGwr9Ccy8jZ9TiL2V0p2unxaQjS9EY2VJinkAe6K/dzlF
iLw+qiEV+ZcEEFKU9UYhrmVzDJkJ1QkylLKcGS77SCODEhtAId6iyhGlpjWcVGQhJDMGdCXVrnmo
k8fknn9kCh9Dpmc97zgExl7eAwBkRJJI10mhvjTIpgys/fee53GsBRXQBs6XKyptQqOUurz3LBrN
CjZ3uIgkun9lISjeT9Ms6OBXIcuMffsHNzndppU9JvwaBqrwiwOgZbxYjEew0uZ1u9b5V73PMKkl
QEFxRKTNTgwon1i+LyaUaymNbHR4UUN5xOW/lNNfgf3EZabWMxrHzTOy0Id4MSqwmTsIdEg/xJsE
+T3A7PpI7GXvWiuVW3IvRrcmsu8Sej8OsfWd70VpoZsP0tk/fgQ1NG+6cKenqAQQV9V5NuW+Blpm
lkAPIqChaUMXTzTSkVJrc8bP2MlTx/W6MxHgrAwf59RPoKIXz0gpq98e9EI8WfqAFTeAQUWfL1xh
14VJwvKHkAdaSW6KcAOuOOhAualDnorLCRV+1FnK8N3NkvW18q4xcAJHW8ld28OSiegV5Gt1THah
wXaCzOZuVa2DyvN4tvjvuXxDDKlQR7pwYY3DhcDYoELVrajTT7gz3RMysaVj094KkgdwH7xLHboe
Gg869xDWpQRM0SEj8APa4i2Y87K1HDhciPIsmACeQI3kJx1p3ylXv/32rxWWP+iyuSx2WhE3LD6Y
GD/Rh55b344C6/gbQidZXW+Dn++HLe0EWW0kUuR59xnljlhMb8Qq34bKi5sL+cx+P0VE8EAiP2iE
Relqioko7t/GwExwfuE/lAvxqXOoVwmVIJ5EJEzwy1LAkrVqcDt8tbhtRmZrFiIr9etz8BUGm2D8
jSnjEhwyIv8QaoNEH2+pG7/PVGp0rKQgfwIv0H3zkpHkDk4jsmstcjNx9A+8VGXBoeE7d92QvSY1
o7t/TsWQD4xqveYiOCIG6FhYFL+Z4fe/66QZ6vxyFb15vcQpe9jMqwB4h0B0umFz76KwN19Y+KBb
gZn8UHdihSyQ1k5cvxjPQeou5ecqcFl1X9EH7eXcbdsSNAXJQxfxQCA/eL/IHvYtxW0MSCLND3PM
utIf71ExLiccEQR8K15xZVQn0/AMPcLpIPXeAhCqedJhBrHijX/GslnnXPqF2UQwPSkpqHxGHfk9
k9DngINfGmAKkh8fGdcBtbp1vNs01Iw8xGTQwdGmHrt4/cWUItjZyO4G4UA8hLO7s4NxWhjLb1FX
05L7ltVNci4ZsNnqApDonrtzJ4s5YXDalE7BZ/bofUMpmZBEz+wJI9c45Zsn2Vtgis98qkAe037M
XvARCMoG6aWMyFyq7OFWI4I25NJ5AoztZly4bpYSK2JoPFCbbrc8k4s2SOUO1YRpTofZxDomVqTJ
SwwZmc0THWDY8dt+RH9BKFTZFZemT8/Vqck6ZJ8UjM6C8yc5OorZNAjCiHCqyzLeBTjOgOOtKF3U
4w7kAIx0okox2g8yaUTEDGeG0alC/ubxVxdC1mxyJvil4jnYNM8QiGEAKNxsN9lmxT+X3dDNZk51
Cxo8wn3y0xU+UT/iXBpMX5pO7+C9tX+zY4d5vGCljGEn+h0wfDXL8mUnRsrbqQkkexXu7uV1Z/QV
iE17iwc3PLzKX9vMeZhHItKLMp9oSd2oiMvZ+JKWcD7SZe4XOg9vGMsJ4Fa5rDyUGGaI9uKN+s78
wRU2WeCIMwDiBWeizvn7zSFml/VV9GRR+gTruEaYKGVMqXiQXPCKzGdHvWp9YfH3H/81lnt+qX2a
yze6B/j5HFJ5sLCQ+ueYkIvcPZTMSS3+cA4rNEJ4ZIiy2OWYHS9TcGJD9HQekOnuDFO7RujZfnPr
vJrgu/u75IpG8WS/3vJrt1c3grJGOR0T48LRd6yoYseLzf9yjpyuH01M5k3MH6Zehy8A7uMJmDq5
WQ19hH/54USyxWYfH0goWsWBbUocCuqFdoZUH245qS86Gz5hbqnF68H0HuLcOjD37SGN3MXxHDB7
zuGbgTrTNywcbjm26f7C/hjFuKsUpMTTTVYHEtCX+DGB1Ct22rjnbwYK3V3NlhV9G0tqNVzxRGWL
IlbeyvcVHNjZmuLDTfvkA5A7hth7Y4M81M+U8GRmmh6Nhc4Xc56z9wPtDYRbcp3VSg1EszhsKNni
W2Rs3BInMh5dYsZAkOS7XCdYFcBk1JbgBD8qYjNFGDuNZKRQfN3Foj0XDXhCoMLQ0CsLLZ6PMHxd
OBri5yNWzNudv0ojqHhhZM80+tmMMjYmAH8k/0ByK6oHX2GQL/btUDyztkYrh8l133sdCJksJB86
Cwewv189u29ZFtwspnoKF80siA3Gy19YfkVEY69bzX/fYz1MYGyGWmbHAAyQk9AfSGON0FYAByMb
hjAzxgNa7fMzskfAAkNH2AX/7PQnnZJgL5ZEyJtVFovIUELWPgZMaW30DfJEJCzW64u7FISIMq7K
2WYeeFFzsTeSNuFzO/W02a6qod7s4x5hzhuGj5Lox3w1sWZFnCRl8hEnarZAGjq9HkoENIuJ0aY9
BLswXQqss9NkyPIqGnPD0Zntck/wOfbwIHqCHn7FBiC7YYq/z2mlh6kNuOq8y8jRCRcdeYh1t2ML
E/d43ibKe6rGI5dVaVLlnuQYsFEm3cQC/NEsECyRpNn5j3991MCJK+9niNy8sNLTJVc4J2mRIB2j
INS7yklgiIudHW5DDaGH6uiF9E47n+FUazPA7t0uuOhCvj+Doefe6ik/tdFisypFYNDtOqYaedKT
5IUMERJLsn0AAEf8VfhKjWcR2DAFFhYQEQyWrbEXwyHgyj180E7IS9HWF9SjF68uYk7X6b2N2qQv
o0cDDrGq58cPt3VJLmLF/dT2n/p8F0xP/+KVl2nsiTECz1xWaUmRc/j7mMaMIXaReTrLPBsZNfdV
FUTbWypc05LpqYm0lgchbjux0XiPtsaKSEhPbu2lEXKYJUpk/r+Z53kbUfDi89F4KzntMlt94lyn
GJRLhdgTlkN4bKkGwIda1ErAsUbAm2xMCtvljHp8Lp8JBIre5PSwSjqND2/ziUagVmjvPWvflWxt
uYrXaoCoz0guJ5B73CO5ykfjgAnaztKQfiTX3+yjWNoS0Li13OdjPMTj2cLubt9p9Kw4TYVbkD/R
OLruRGyj1qY2cNbyhE42e7PTzhwtIZCUZUcpYHYUku3JiWnyQEXnHK6jnLK0jiIVDUGD4RuBsDwf
8oRKLfS6hPlN2wQU3F1PvAJOvaw6HJgdkR1LTJrRXd6L1am7PDcAsI3W63XWWI07LVbEOfaR1Yxf
UjIm+wh/OkMghjnd8Tmzvw+8HbFb+ej1iBoUKO4QA2SihPc5Np8aPjp/D+6cDQPFK/AT1rS9tTaW
Hdxl8oOBpHAwBuxNNpVZvmuJS59y71CTLv1lsxKuhZ8tH0nE0SGIJqqIW4EqoFyl1dfAjySDQvMD
njGn6TICmoPfwERzqc1hTJTUg3pRvfuXZTUIPjsTRsd9S8SY56nz50PqpCo0m4A0CQXZu+wtUTeS
4Qk6u5N3QbCDanGA9bQHLdVsSAPLK56vSNJ98a/TuudM+NKZ9eYnCjwOMXBgmjDqWJbqurwtZT3z
6vpCeVjfqC0Dnu9PJhtiUGehC8/Mvx67+4o43c5eGUkHQz8jXpZA9EI2Sxmh8pFKTQTZnP6CoQkA
PTKXb9Gbv9xURhvLAownvS6cW5F35Bivy+0n/zZaL3Ciaov2DXNaaq5ZSNNSQfEVPZLdPSAJejOt
7dpmvDXGrbY1fS8k3B4WD9YgTr6SBHvPiNhSFewcCo6/NvPqm/SZR/SmtHeU8Nyr9hI+bVWCwbht
i0Fw0P4AsXnCWBbr5BTvvw0AW8gHYRpt7HXc5GbkPWER4UKPT1scsWW8b0VsuT1NGlHa4lBwDb/O
2VJxaBhw3H5e6jTe5AbYDPPqzg2QFHl03r5jGqVpRwqcvWKV5kPBCuawQcW7+bGNzPgb6Jty8NNn
u5RtLISKdZNU63DBGdEFebXk0+qxOVO2fPnlepYuVP9EvGjnKsQWmGR6RwiEEOdvRSli61np/3T4
C6KO1ixBaFK/VwWv1l/M1dlC3JylJHVDrYs1gI4Aoh7JTF9cmQuLNZCoqxXh/c/NX+zi7khjIfu4
pUheHD6Q9FqQWs8Pit6YtQPtDcm5DKIlL1rbBAc0Mab19+26sFcDKXGjjYt6W8J+MrZO2JJmt3RM
/8gUGNIT1pwcIQOD72QQxTREHQoAcX9yn2i0vxp6ZjN7YJbEG/aS30hw8PeSW7bD/0nyid9PTTJD
22eOz4dSdQCWwFQr8RjlWnW+s9VIWK6sMK7EXEJgt2RW9qeIn64dzIXT+90W2VwB3FOFkK2yPmIv
cdl4qdeOSWLKSEH2nXbZl+l0PofRFx0nE1b/4JHNa6SvRyg+01bAPenTZWrSJ2Cz4t9/2/2aWwhC
Tqzj0CRVBCl6+LWBfrCOw4sK8rpNkiCv07JuIleuppY87oNGSa8WwD/mkW+lRHM00hoGYg/NwLuL
4A1hVbacjybGQGLdNs2h6P5Xcp2fUo0glaT+hjEbBUNgqb2S/wDXY4MghcB/7OJtA0A+x94AxlHf
BRPHU4hiW89hdlgIlhT1NItHpCS1/RuI1PmLFSqBQIwZDmgAhe8kRQuMaJW1Fiox+U5BUTEjYppx
fE1gK7P/Cvm76g48oRopD4ryouyFTt/IngSzmVYg185F7/5/xFau/ZOjwPHgFibZlpNc0YgTj3ov
wz5/YZSho2AVMOQgcJk9FaBF3xn197/k85ObeZlDygBNlLG63744u+INuSsKRg+eTgKiVT25w34A
88wmpYbxBN7N3OTVocZRn6rwtAK5I5uqmR1QJ0dRKcJx2FrdXxO9zcu3yc//RXekoOOdU7beQoOf
AnPYkk1HuOxzbaXqUDhsuWSViwVMQRjnGLnK5MeICjIwTFaY7yiTeqrLPAi2j2SFdn4BbUMSwAVz
aM7oU6l1XOpo85uNHP5cyNlRGCY85we8fzb02lt4GTWLY2MInkp/Oq2EKTjZxa5tAb/XdBRHCy/2
PP2EHYI/4TLt9++BuCh5lC/s6sVdw1XV48Rnz6SisVuPMioyYJmJiwJqEZP3coCGSTnpWl1rhcSa
6Fxbaf3ndHkn6O0rPjP7AeTZbCE42p/GcAZ7WHsY7+j3bLzeldxV3B/wT3unpMWKt3V5sKbzQWvr
cTcfyhPOUe4KglwSkt9XeXkYc5C+d/++L8Mlq6ovVAW5ZTTcFDOsW4rLx+dWtl0hMLFPdezMuxYo
n++ahrfcCMDzve7fPeVNERnQ06pxhIv6RSwAYwDdrJwHf2LmWkO1gLuh0KmKgO3I6WvRHPtXz6hP
fLzqy++/QDqigvOzM2LmzmfNFAVBdwSidd2Vok3XbSLN1RvWz/RcItExjz9HpajtEd29JsHNweop
Efms2t1JEPr8spzifPoDwmAekfSP/b+oJy5QMC4VrQVrrQAs3lRsdAclaNLTZPhYaIAxlwbGdce+
YbnJ0vXutu3emroqJQJ1xtrfXSV6o8n0Vyz3/mFJrXHvMcaEccL+ybY2gQLA48aOMaG9ah+acDya
CcV9IeRvz4MIrp4AEQAuQslRnTJac8AKQLbTt2LEI42HCcHbLrJwJf4y/7H75IOy+3bLOhYZtnOj
80nf72YXEI1t4HEh+wv7vmq6VqpbAfZejm2WSEtq7JP8ZnN9B0K95lQiTjMugEOwx3G6OKRzfp0b
L6u818jakfCC7jvC8xlPoGHWwYDTxoUbB7x4nWzKa8Rxn4nEcIs9DZoorcmpMNlfmdrMr0JXatF8
3wx2YYxBDUJgJG5KT97eifNMqk9TAE7M/W2DgKGINsLf4O/uwLYKrHBRuCzsFLxYe5Isps0daCyw
P0/AsXL1vWz28CuerruT+/QHKvdWbC5iWgl2TvaK/2yRchfFjmC42kRbbN5JIuEL5YRJc73kaDTw
fSC+KN50/d39n/GJQUmFisi7/hcLj7Qja62etILxkhDAh0rWROWhNVxDpxeWtdSpKNWgrKXlcY18
9jRE5/O732h2y5aY4F5Oj9s9uaR2w4zXd3U17zZpxoK65OTFLmDUIbEb9wtgCIj6ov3Jx/kypxdI
7CsOtIAid6XI5wMyainN3afIHxrR1rhJJwJTgrn7eMhrW9DwsS6LTuXbvOSq5/PoE3b3hPboWJIJ
SgIb6qyvkcPuSwhIX+C29cJ64VnWy6gMpdvO1aQT2vkPqNErymkYm4yGKihIOHN6PVVLv52LPj9O
MQPCS5dPhvL6erboc+9H/hLIm2WJWP3BP44w4rFp0dcaBzkL8dQa6zUUJ5/0dGqH3X7188T8v+0p
GDPj89QaDHPXdp/jOuOwkYv5sy+G9Sy65lvZPT2bHvLoOLPmJTPo++t0WxRydHl9z2W8XDFOV5Xz
B+j4BP3ZLfVnH9yt1uQuqF2oXZ91CnroI4d4xWKo3K0gs262rIaFNn41Mij3ERrIlq8vbXCaPoB9
Zs/WQ0q9/2yyVhaoY3pG0b15HV0X7hL220PQQ7+1cuIYC0GBZpcYJftZz4OaTvIh0moGNRJ2Qj5i
9AGoLSKjDUM3Cu6pBCH9C4OJi0RDcNy/LF7Ou5Fa+1W5tyRqGAOy2Bic0pt56zX4xM+PzG8VaLzG
YSX84mcJm7g2H2YuNgaHIZIhKl4L+saRjDrqtbjV3YhSa9zb1VtIx1TfDnZ0lLjH5gIpfB/RDAFo
wtEXLhkZI57/jTd4V3VEWCV9yZ9E7WbHVicw71wvXrlcispCf4DzqSA2J93tuZ4O2LnM4udxcSHx
NPsoJUWv76WTFPgkCprvgfpaXLG7lZyD4z6E1BQpBeLIRjcz+Y+AswvSW8zcfu4x1F2t4+/ihdHQ
xqhnY2j3vIyWgVC6y+P8lTdgAEg79V5gq1mWSH3qupjMmvmGC4rwoPilYByGlqOa5GKiD11xls3m
7hFv4a1TjRL2hVyJlOzVaHciivHqY476aVFzWXRwOwTard4I2D/XHRbVgcYSpDSWUvvZSbQL8lVJ
eFIWPPhFAY0d5OsGR1TnBpCdMnOfD/vvodK/LplV0p6vND1qwWyM1g8iE9pGfnTTGAnOqPyTNYn3
IIpUT73F+Veqx18Ps8eD0kT26CnfCOfpUhozlZ9qG90wbe21zmljeTa3FORLby1EmLV+8doP+ZNc
Z4HQQ77Maz3twhV2eL7cFDNr9f0tuR0vJLGWP+gm5rQZ7VF5m4LLfiVtN5Bj8m3n3jqKy5QJn80T
FJH6qPCCoUfhq2cxc+0culv0FHkxwxym3ho5/09MQNWE4Od5JjcXWCTPwAhrpx8nuTx/opImPw0i
f8KWAaPXz51GeFzIKyPumX8jJPsSXGrAJcA2fkfEZ9iMrvQi7pm7hY8OC+uWhL5OnKny1kY/6sSK
DqoIq6e0Zf9yZnNbbAl8v6ctw0gkdpsKuHmJhIysRlEHiYGfQ6kHYkaL4i+QEh5cmnY9lk1Oy6aU
LAm3NMBL2PCkITHKE45WVZr9Yas90o4VlZKsY+cZQigCf5ZkkVNTYE49meTGNiItpduh3Qfnm44g
aXKCwv0BJQ/ZDcLMTie8Ojs3XpFuZ9qrw2XmZnZ5gnl1hyNhOf3fZkUJ8dxxbWmhHY+iJdkemxQ4
f8qIGC+kRYJSAN5LXEENAUS7t+MnNhlnjSI4SUnJXcoTUZ7e2orSjPkoWiNunOA13XNewPgXHiJG
DgLhXu2naxYzz8k9QKXAwmiA+JZNwIlqTfHdL1S9tALWtmjtlIS1ke8YpwNyk2vmh/c2JB8QmoQE
lIfNcGqw2fC+LGZYPaz7exPXfNvQ2q8HkInr0wHiLJZhtHBGVfn9SARoh8zWQx5uZP2HGgNayWo0
KKbotuff3AFVKetj/FY245PqvoUtXZldBw9qtei8FWXQpJek6SbQ77YzgipUm314cmxTZLc4u3Sc
9gDPB/1ofJKb9vdqI4ffSBCHrGzSVpP5OHX3X5f0vJhHMXongGJnqhYOjh9jRIDodyLF39Hc90hh
wd2pJpcT+qgKPGMK5xP37lWk+aucWGAcV6EXNKKHDtRo+ezS42rgqgapiyMautAHdjSUOmB2uskd
KQDJjW24ZfKT8y2PRnijMFVR6fHZ2IotDvwC9+YHOGYpB1C1kIbsTDjnFme/k1Ybe4GU+S965rTT
jTE0QCjJw4KcAZwnkQZSyoTPouMVwuOpYqmYnHtW5GseTYcy9HFrl7cZrMFUimwLbABsGBmVFJlo
Xk2mvkIpKAIBcAVl83WqrF9agvGenoyMijtTADUhzonKQRgajPUQO3nCfylSRoK9yBFMLhwvH3OP
bTbPB3LCOhcX6kMLt4UAxoe12EejG+7a9gdzE0nRvnzT+Sk/nh1JohUxqiUvkNG1XsWfXp0/m0Rd
ecZdULPb2lIgmh09HRv6IbnxqMpmuoXd2B5f1RDNpoEoudrbfJcIQXj0CVSAW/bnubxVNhBZzlXe
rt5ByACjJydzos6pZWVrlA26MiPE1S0C7fs5ua/Z75julKopmrnsOltBlyhrv+MlSIC0Q92qIzZU
j9iUzdpy8qUKAOCkBZgTKkaFb2kHk7yickC69L9NuXwN03zyl7csk+PftpVRNly1/i6rpM+vG6Kn
8449Q+LAidnqZuMAJujyuLSY9KoYjsSbeeqFapkUNIDpMWoqqYlpBhfvUvhcuYXmyMhPQ8usbGpP
O9WpYOqt+WEejGKF5ZLNoBrnHa2h1gsnGB9FSK9jFP8g1MWAruJ8OgcF+aEW/hzQvejcOSZLoiPl
3oiYN9cNC+VHwGyHo1NrUthDqwvvsVQxXpJwM01zjskedHhSdpt9iqXw2y+4W9GhsJPirVPUNIS0
TSLDoWYwzcpK8b17UAP1yOSTBPzXCvyariJpR3oqCmUs1z5IBSC1lDvleRqO3EugavaXdnjbsXU6
wPhbBzba7ynyjnZBx8AKgrKYsOanwiTTVzQolbRvtWGYgcfeiaRPBMdWlDJP4a7QAbR6QdS7hAne
dAUTYeIAzq9UXX7bG6L/YMJQOmxoe4tXNbxMDZ8C79aqXp2kQjoB4WlM7tD73SkJ/q/LoSpYMKKp
sQ+eGy5cQImplDoc6hAQFRhXdOIrOTgdYRqfTi8txnGSnCAj1YyhzOX7pgya/j3T3uN3bROWpckq
ffbQdvX4H+YyPgWmhWQIxPJRmMCTcUc9Xu/x5y+cnWPmGBFWwzHxa+JCbpWGQZQ7sAoNzlWFNmnl
JbvltceBJtvK/bjGhuWQ9X+zgf+HH8lgyyf84CHszfN+YIn5fiOMryirzzQqqNPul/E85j3j7ftB
zIWUEqiswtMgjL9BbSF94kvkfV3bsaYroH3Zi8YEgeAQXeHAxxlsSA1be5Z+v+J1ahJlfTbar1dr
mUhJ24Mk1OMYFTfKwlZmeUqea4yUVrn/unMGL26dm1svNC4wHzu/UTPSRsg7rsCWqvF8z4Vp6f8X
lyY9RP2XUDcLQy/yuKvzeeCbpzYrQPgDD3fe86/nekyYxsQUgfMTUMdF59CljXluWCT4Jyd75zhP
3SUrtocCbK2Kb6R8aL3dtJILL/AyznILI3Jn4Yn/hOnWocWYlBP16SvoYmSpdozRt2QLIQ+RJCuv
dckRUFQh8pH6voHKI0AwOqdBngsJJivOaC21fawlCHsLQTyLNZPiWM2DgX6ur5oxdwyGrMhhS+LX
s6VUFI/83LAZ8wWTlUzwWUdhX0/ocyL3c6p1LR7AtX9ZPVRrXClfAh/9Oevl0mScVvm4zf2XP3gf
7AR5JfC6aJOFfKp2zJoZgpCnQXaEIivWluc8c6V9VcvxME0/gPvvIrl1sut8z7wzcxSN076ZPzZo
ysuyzwm0Trn6SaIgH96wzLejIWJd8p33Lxb+aKjDA4bDtb7EYeLH2NEgcNDFzxjKGNSL/A4kZ90r
b+eYm5g+9oxiDHlq9hc5jX/a1fz/gMDfaP/+qRmXqPYIleyvgIK5d8H5dUIVzVwOG9PWDBj2udBb
7t0j/uyo7EBDRKSSdgZYvvP2KH//3m3nc0bhOSooUnb6b6OdBDNwA4/A3J0rCaNoQApa56TBkpIS
7pIc8VAwCClGJJZtyyWjMFy5aFhWE9xVL6XOPiuu56YrfRcDlLtP0s3QVthQYTowRLxyn45II6MK
L307cnOwvZoJpMgbxeey3jWxYsCv9A3bl1jksUjeYSI6OaFBPsBwcRv5GZoNomY5CrMbcm0QinWz
HJrszoYzZlOKJutF7dIpKKOlGclD/DQRtwUtOkfD7SlCSp0a5A10fMPSaIIQ1h+2VGYELUdpnK4N
aHSQ8tlFBwGDwheYG0ko/mtQRPPC8Wok/A0xJ8FH/b6qR7U9MyovPfuMvCWqxCSpASNehjKpCJ8M
BNtpTezp5Vih4uYteqfY0AYBfXKv/meZ2JBoq2Y/rQ1QuY/yhAyHJpjD+D0nYIcLbluIkKFoPXvI
w2l15mcXMg/wGuAndCcghyonOV0JWJsO6ZwGMuWqs37pCqsOwzlmEirofpEi3mhUtTiCf+ixy/kt
e4DDf5vV/+wh8I3WwJPTJJxwPxOME2wOvwPuDxauhh1f6uwzngsqCbyNGEBr3mOe0UNUmCcvU8El
+EmvpX7bQd6xz7DyLtOikokJGPyyctlYwbZ2ae8eFyU1+UxvyQn0LAKMJrazCWDQ4yQUJicCYAU8
H4sVUh8k28+wqduK4Cxo6A8z2RkhtNKk/eZtSPFxdPJ6tBXe25hdWOX81QCRbyUVxCHTYdiqPfNM
OcAOyZKd83KafdyoL1U5YE5LX/AhTQiHTqnRV8DDGnWcctj2Nb10P4SWcRPpRKIRGN981tB+I8o3
5a3PDs7vi2LjalehZTTNsXLbC47tlrofdhdYHyvLekm7KK2yEIzkguICBNeN35wP+RLrDoFPglcl
vs+tIojKTD+2F6ePnJzIBiumsm+BHfMgmHL9UTaxkYxw5gXv8pn9oXUcN65Y22ToYREE2pLK3DF1
4Gj/COTiSQeDmtXA+nEBfJKwVfIDyNquIAlHz2ByocuL4gvJuBzgE0eoCqy8Pj/vwX8JXAnosGMU
FQB2kZKfeTde4BLdP+xB30eqZllm27Hp7ZaLs39V1qp4QW7MdniZvetW69urBCPTdXGzwxe/wSfG
xQWMS/0wma5rRY47HW+dt8wcZLjYc1sTXTIfnkKAKnRHA7PXL6LEXoMDmUSwLrcFcTx5cvLYwxRD
YYWduelFKbDruIGLkluHwR+aEy/6etI0FEtZ1BLcgDSm/Ht1MvWz0JH4YGbq52lDEGxL+ax8jTsJ
8T0L3yVf9HhMBEyoS7W3jEurdYHXfqN5wySQdpGKPpikmiu+n4mlf44FSLXDDQqTZWSCkBisky03
fwqcqlkBBNA1rM4//dWOHsw3O86d/TagBcMjjxJ9mCAFlcpzicyHm5R0/PJDllEEBGMvoUIA8AP9
idDwnUg4Sxpevq5ugyVjW+SB4+5EHpBdRgdrZDS4mjoUoaJkSBWrHBE0UL6t1g8+svxZR5CelM1d
ufTNQCmaegxJHhviyCky7CJfr/k1yaGNVr/n7/JmtfZMdAlO7H4u12GXkCXUcf9s4oTHiC6+moR/
SMmNl+4eMTQJCYY7Eod+c2BeVcK5LE6Yb+b+2Y/pQ8ziao2LjXG7TwYWGnfZRqD2lRJeSH/8F6Do
FCoXODZ7h2O8cdDT/COP7FpwFbKnqh0hVGsX5cPZz35L46uEDWpVwoErHQ1zmyCGMzsAiT6hlLhZ
fVmk2gkdmyMO3TRicSF8FZktwX4g2JXJ5yJL02NireRc8UUOqR011zhVH6NrdqBp6qQaF2ewaYcb
+hExA9IjAHE4sagWirqfKaItUxyFZKwUTF58FbO7P0tXGXq/SguIolzU5fiuMLHhG18+6N7uAFl9
ljPRBTPQfTF4XqHio8cZeZeJGd7ACcjjQd4jm8UdRl18RlkG5yaB+Va+UHP1y3vZyZVjbC/cJbGx
DJcx1MLqnGNrfRhhzB4iOqBWME5dTvnkxeR5qkn5tTuHO8uz21wQkP4uG9HDtqJ2pxBIuUuPi0M/
hfO59EbNqzF4EfD11CquSRVj00LMW0wN1KQRdJSY1t3KH4meU8+fza6zvfOJUrrN+5taWmtHyXTn
dGtpESruD1KmJoxKvRjdoKnNQJREZawO7iinTzQBk584lfqJHCjqwYUO4IHzGZrV7IzzZerhJH5d
J7VrV1lbM+1QkGRh+8+JAd3iuLvqwbbjwtmJhpgf9DjdNokvaBZCNaLG9Vy0vf49O7CMxuzran3E
z/UF1Pijwz45fAG9Ka5kcE+L0jHTu9IeELcYlbDto2MJhj1EbwkRXH9vdrOWIXkNgZbL5jD3/8Mb
DdT98C1MgSL9SEGc5PL7azZoYE0GaEWdD2mvrpergi1Ysi1g8qjJEbQYCYkvNqRVP0ts6cT9SRat
rWHpo2c7soej3VjdWn0T5VAnxIOvnCMc0hIrOZZMvR9NFbgXzzHa58cE/+J5tmJxhvtBSmEZ7ZSE
f5+n6j8P4jjvgUpN/od+TkiYxcUvDCK4bTId3HVCsziM93CCe2JTybe1Ch2D+AAWrYebrkhlBRL1
Y8XImsibXbgASBGgBqI3ogkGt7HCLB/gENrBTmAfnrdELz+AczECfdV6h6yccBGfsOfJMJSIIHi5
TsZZsloEBt3tRy/B0EWh71HIXDHHbzmDGgNVBB//SKaeghNwziq/IXAtw39ev5A33Ak7/2IHMjM6
19PsNVUEmQzlWFCYID2NxsWUUEz6eDFxbq2GGv22cBXOSuDXRfw3dLO7D2zSOplnJZ7tQA0jwy/2
Vh0rIwbW3xfAKIeOsca+vaPQ4ch24rH+ziwqkppiBkaBb8Q3LPAbQ1w8xWMyJoe39XYC27JJNh6Y
7V/+qVfNmR1H+XvHj9SiZz4H1/5rBZrBF80m+kofO22SIolywD1C/a1EKUjjpa1JSP14dDIH+Zn2
i9tM5QjLH2ZTfcRMNI90WZQin8jRv0SOi/omL3cHtWjKUrgShG3w2kaUILG1BJQAXZiLvX4mzW5P
3vQ+kxFDLnnMjz02hL714culSFBy3pwKb8imZf6euPAOoqEQs/7mXwmqFaBloYlrCmxqXGfuJd3B
puRl8Av4YKFUBYPnuD4YwUHanwtnFvUCcOle+LG+BUiOeH/m32HG9PXmv9rkAlRHZemAyn6l9zK7
6H/YpWdgDkLMdcaTzTRPpyo2/jxXQ9eZhWnDQSSomUPX+y90qnAaSbF1AuxQRw5Qzgprfx70fTyL
owarvEvwDPUQCXtSLecdekX9/Ej4zSRDYQ5MMUrS6plxuA1pOJTAl3yFo0mte4S/ETiyZfxue7/+
I9KHaLf8HPpoL2eUQ+CQdPIujjV5B/PrXJn9aJ2urBEdF8rpfeHsMsyTORp3uXfVUHOyRoQ559yS
ANJwDV84cG82+zV2L2psbAakwR7wxQL8nbSzxYlf1xEMt9hScp5xd8Ui+guetwO4ZTta0O9BbpPl
dgDKm4izpgcrsScLpFAK6fQVngL2ZlZjdu6i3BUnwIIJknJi6WmzJtSL5g5PgB+UZRQpTOjnwMXr
nkEH0k0QOiyskh9JkxtZegOlYF+F9OscJKpwkPusG3MqH1XvQdCXaW76VHV+LXz/8GuKEbrXtqV7
ARp9XYs2t/c5rg/FDvad3MZ4aumXr+a/mEq8aUnSFCV6FSTufJ3N799JQkk4XO7AinhMA7LC4rst
2fw9AE2Gz0ZnWgeDs22pWpT6sX5YnSU2cnAQBXS8QkFN0hgr1qrwBlTV83X1Q1mZ2ZUZ/AxRooWI
qgIB3VevHYfwiHeYdbvuuoIGvaPT7yNQ/nQ4inGwBMmIFZ+Y+TVYpnikg7cMPyDYIDIyU1W38hFl
KxD8kFl6/gSrDwnEfKB1XWLRp8ymjT5CVvHsB4rlazGh+aNoTX0LBzv6/K3xbk1206PkZ8qZmCmN
PZZmqo97uadxjzuZVpUSxr46MMGeJETwmwBMBt632p0x+0BTcvpq3NR2INIxM1FFgojG/3Xul6Hy
87QLgJwXI/dDtjf4SkuLyYzSA0l985jD+/VnWgbMELv37JY5A4JT153jBiovq+yuZG4OkIWiU6fM
K8cPohPikNG0PTlf2+hRaJMtwDeVUJc+ghCTqoHLM5MsCFRPofwO9H3o5x79034neDjbbzxzRRKH
yMfatme7pexSF5/r6JDpPQTnsi8E5iSzbeoKf7P45P/gy+0Nv3J2Eal3A8RfJbK5qlszhz9WZ0PD
wLXckeKKzlH2lT/DssVmTN59X9vuZ8rV50OFS7aGCJQz7KWtY5/E8dxIE3xWjekZ5b438yBhpE1h
Cuh/e2gwa8y9VYxnv2RswyrDgxJx/MPk4L0nbYvNESkF99Ka+4ADnjUMWC466/v29tu/icuc
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt is
  port (
    gtxe2_i : out STD_LOGIC;
    gt0_cpllrefclklost_i : out STD_LOGIC;
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    gtxe2_i_0 : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    gtxe2_i_1 : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_2 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_4 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_5 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_gttxreset_in0_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gt0_rxuserrdy_t : in STD_LOGIC;
    gtxe2_i_7 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_txuserrdy_t : in STD_LOGIC;
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_8 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_9 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_10 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_cpllreset_t : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt is
  signal cpll_pd0_i : STD_LOGIC;
  signal cpllreset_in : STD_LOGIC;
begin
cpll_railing0_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_cpll_railing
     port map (
      cpll_pd0_i => cpll_pd0_i,
      cpllreset_in => cpllreset_in,
      gt0_cpllreset_t => gt0_cpllreset_t,
      gtrefclk_bufg => gtrefclk_bufg
    );
gt0_GTWIZARD_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_GT
     port map (
      D(1 downto 0) => D(1 downto 0),
      Q(15 downto 0) => Q(15 downto 0),
      RXBUFSTATUS(0) => RXBUFSTATUS(0),
      RXPD(0) => RXPD(0),
      SR(0) => SR(0),
      TXBUFSTATUS(0) => TXBUFSTATUS(0),
      TXPD(0) => TXPD(0),
      cpll_pd0_i => cpll_pd0_i,
      cpllreset_in => cpllreset_in,
      gt0_cpllrefclklost_i => gt0_cpllrefclklost_i,
      gt0_gttxreset_in0_out => gt0_gttxreset_in0_out,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gt0_rxuserrdy_t => gt0_rxuserrdy_t,
      gt0_txuserrdy_t => gt0_txuserrdy_t,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i_0 => gtxe2_i,
      gtxe2_i_1 => gtxe2_i_0,
      gtxe2_i_10(1 downto 0) => gtxe2_i_9(1 downto 0),
      gtxe2_i_11(1 downto 0) => gtxe2_i_10(1 downto 0),
      gtxe2_i_2 => gtxe2_i_1,
      gtxe2_i_3(15 downto 0) => gtxe2_i_2(15 downto 0),
      gtxe2_i_4(1 downto 0) => gtxe2_i_3(1 downto 0),
      gtxe2_i_5(1 downto 0) => gtxe2_i_4(1 downto 0),
      gtxe2_i_6(1 downto 0) => gtxe2_i_5(1 downto 0),
      gtxe2_i_7(1 downto 0) => gtxe2_i_6(1 downto 0),
      gtxe2_i_8 => gtxe2_i_7,
      gtxe2_i_9(1 downto 0) => gtxe2_i_8(1 downto 0),
      independent_clock_bufg => independent_clock_bufg,
      reset => reset,
      reset_out => reset_out,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_RX_STARTUP_FSM is
  port (
    data_in : out STD_LOGIC;
    gt0_rxuserrdy_t : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    independent_clock_bufg : in STD_LOGIC;
    data_sync_reg6 : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_0\ : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    data_out : in STD_LOGIC;
    data_sync_reg1_1 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_RX_STARTUP_FSM;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_RX_STARTUP_FSM is
  signal \FSM_sequential_rx_state[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[1]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_10_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_9_n_0\ : STD_LOGIC;
  signal GTRXRESET : STD_LOGIC;
  signal RXUSERRDY_i_1_n_0 : STD_LOGIC;
  signal check_tlock_max_i_1_n_0 : STD_LOGIC;
  signal check_tlock_max_reg_n_0 : STD_LOGIC;
  signal \^data_in\ : STD_LOGIC;
  signal data_out_0 : STD_LOGIC;
  signal \^gt0_rxuserrdy_t\ : STD_LOGIC;
  signal gtrxreset_i_i_1_n_0 : STD_LOGIC;
  signal \init_wait_count[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_1__0_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_3__0_n_0\ : STD_LOGIC;
  signal init_wait_count_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \init_wait_done_i_1__0_n_0\ : STD_LOGIC;
  signal init_wait_done_reg_n_0 : STD_LOGIC;
  signal \mmcm_lock_count[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[6]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_2__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_3__0_n_0\ : STD_LOGIC;
  signal mmcm_lock_count_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal mmcm_lock_i : STD_LOGIC;
  signal mmcm_lock_reclocked : STD_LOGIC;
  signal mmcm_lock_reclocked_i_1_n_0 : STD_LOGIC;
  signal \mmcm_lock_reclocked_i_2__0_n_0\ : STD_LOGIC;
  signal \p_0_in__2\ : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \p_0_in__3\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal reset_time_out_i_3_n_0 : STD_LOGIC;
  signal reset_time_out_i_4_n_0 : STD_LOGIC;
  signal reset_time_out_reg_n_0 : STD_LOGIC;
  signal \run_phase_alignment_int_i_1__0_n_0\ : STD_LOGIC;
  signal run_phase_alignment_int_reg_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_s3_reg_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_5_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_6_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_s2 : STD_LOGIC;
  signal rx_fsm_reset_done_int_s3 : STD_LOGIC;
  signal rx_state : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \rx_state__0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rxresetdone_s2 : STD_LOGIC;
  signal rxresetdone_s3 : STD_LOGIC;
  signal sync_cplllock_n_0 : STD_LOGIC;
  signal sync_data_valid_n_0 : STD_LOGIC;
  signal sync_data_valid_n_1 : STD_LOGIC;
  signal sync_data_valid_n_5 : STD_LOGIC;
  signal sync_mmcm_lock_reclocked_n_0 : STD_LOGIC;
  signal time_out_100us_i_1_n_0 : STD_LOGIC;
  signal time_out_100us_i_2_n_0 : STD_LOGIC;
  signal time_out_100us_i_3_n_0 : STD_LOGIC;
  signal time_out_100us_reg_n_0 : STD_LOGIC;
  signal time_out_1us_i_1_n_0 : STD_LOGIC;
  signal time_out_1us_i_2_n_0 : STD_LOGIC;
  signal time_out_1us_i_3_n_0 : STD_LOGIC;
  signal time_out_1us_reg_n_0 : STD_LOGIC;
  signal time_out_2ms_i_1_n_0 : STD_LOGIC;
  signal time_out_2ms_i_2_n_0 : STD_LOGIC;
  signal \time_out_2ms_i_3__0_n_0\ : STD_LOGIC;
  signal time_out_2ms_i_4_n_0 : STD_LOGIC;
  signal time_out_2ms_reg_n_0 : STD_LOGIC;
  signal time_out_counter : STD_LOGIC;
  signal \time_out_counter[0]_i_3_n_0\ : STD_LOGIC;
  signal time_out_counter_reg : STD_LOGIC_VECTOR ( 18 downto 0 );
  signal \time_out_counter_reg[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_7\ : STD_LOGIC;
  signal time_out_wait_bypass_i_1_n_0 : STD_LOGIC;
  signal \time_out_wait_bypass_i_2__0_n_0\ : STD_LOGIC;
  signal \time_out_wait_bypass_i_3__0_n_0\ : STD_LOGIC;
  signal \time_out_wait_bypass_i_4__0_n_0\ : STD_LOGIC;
  signal time_out_wait_bypass_reg_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_s2 : STD_LOGIC;
  signal time_out_wait_bypass_s3 : STD_LOGIC;
  signal time_tlock_max : STD_LOGIC;
  signal time_tlock_max1 : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_1\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_2\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_3\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_n_3\ : STD_LOGIC;
  signal time_tlock_max1_carry_i_1_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_2_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_3_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_4_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_5_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_6_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_7_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_8_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_n_1 : STD_LOGIC;
  signal time_tlock_max1_carry_n_2 : STD_LOGIC;
  signal time_tlock_max1_carry_n_3 : STD_LOGIC;
  signal time_tlock_max_i_1_n_0 : STD_LOGIC;
  signal \wait_bypass_count[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_4_n_0\ : STD_LOGIC;
  signal wait_bypass_count_reg : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \wait_bypass_count_reg[0]_i_3__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_7\ : STD_LOGIC;
  signal \wait_time_cnt0__0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \wait_time_cnt[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_2__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_3__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_4__0_n_0\ : STD_LOGIC;
  signal wait_time_cnt_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_time_tlock_max1_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_time_tlock_max1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_time_tlock_max1_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_time_tlock_max1_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[2]_i_2\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[3]_i_10\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[3]_i_9\ : label is "soft_lutpair43";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[0]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[1]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[2]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[3]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute SOFT_HLUTNM of check_tlock_max_i_1 : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \init_wait_count[1]_i_1__0\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \init_wait_count[2]_i_1__0\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \init_wait_count[3]_i_1__0\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \init_wait_count[4]_i_1__0\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_2__0\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_3__0\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \mmcm_lock_count[1]_i_1__0\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \mmcm_lock_count[2]_i_1__0\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \mmcm_lock_count[3]_i_1__0\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \mmcm_lock_count[4]_i_1__0\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \mmcm_lock_count[6]_i_1__0\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \mmcm_lock_count[7]_i_3__0\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of reset_time_out_i_3 : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of reset_time_out_i_4 : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of rx_fsm_reset_done_int_i_5 : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of rx_fsm_reset_done_int_i_6 : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of time_out_1us_i_2 : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of time_out_2ms_i_1 : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \time_out_2ms_i_3__0\ : label is "soft_lutpair51";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[0]_i_2__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[12]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[16]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[4]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[8]_i_1__0\ : label is 11;
  attribute COMPARATOR_THRESHOLD : integer;
  attribute COMPARATOR_THRESHOLD of time_tlock_max1_carry : label is 11;
  attribute COMPARATOR_THRESHOLD of \time_tlock_max1_carry__0\ : label is 11;
  attribute COMPARATOR_THRESHOLD of \time_tlock_max1_carry__1\ : label is 11;
  attribute SOFT_HLUTNM of time_tlock_max_i_1 : label is "soft_lutpair52";
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[0]_i_3__0\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[12]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[4]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[8]_i_1__0\ : label is 11;
  attribute SOFT_HLUTNM of \wait_time_cnt[0]_i_1__0\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \wait_time_cnt[1]_i_1__0\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \wait_time_cnt[3]_i_1__0\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \wait_time_cnt[4]_i_1__0\ : label is "soft_lutpair45";
begin
  data_in <= \^data_in\;
  gt0_rxuserrdy_t <= \^gt0_rxuserrdy_t\;
\FSM_sequential_rx_state[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222AAAA00000C00"
    )
        port map (
      I0 => time_out_2ms_reg_n_0,
      I1 => rx_state(2),
      I2 => rx_state(3),
      I3 => time_tlock_max,
      I4 => reset_time_out_reg_n_0,
      I5 => rx_state(1),
      O => \FSM_sequential_rx_state[0]_i_2_n_0\
    );
\FSM_sequential_rx_state[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000AABF000F0000"
    )
        port map (
      I0 => reset_time_out_reg_n_0,
      I1 => time_tlock_max,
      I2 => rx_state(2),
      I3 => rx_state(3),
      I4 => rx_state(1),
      I5 => rx_state(0),
      O => \FSM_sequential_rx_state[1]_i_3_n_0\
    );
\FSM_sequential_rx_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000050FF2200"
    )
        port map (
      I0 => rx_state(1),
      I1 => time_out_2ms_reg_n_0,
      I2 => \FSM_sequential_rx_state[2]_i_2_n_0\,
      I3 => rx_state(0),
      I4 => rx_state(2),
      I5 => rx_state(3),
      O => \rx_state__0\(2)
    );
\FSM_sequential_rx_state[2]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => reset_time_out_reg_n_0,
      I1 => time_tlock_max,
      O => \FSM_sequential_rx_state[2]_i_2_n_0\
    );
\FSM_sequential_rx_state[3]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => reset_time_out_reg_n_0,
      I1 => time_out_2ms_reg_n_0,
      O => \FSM_sequential_rx_state[3]_i_10_n_0\
    );
\FSM_sequential_rx_state[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000050005300"
    )
        port map (
      I0 => \FSM_sequential_rx_state[3]_i_10_n_0\,
      I1 => \wait_time_cnt[6]_i_4__0_n_0\,
      I2 => rx_state(0),
      I3 => rx_state(1),
      I4 => wait_time_cnt_reg(6),
      I5 => rx_state(3),
      O => \FSM_sequential_rx_state[3]_i_3_n_0\
    );
\FSM_sequential_rx_state[3]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000023002F00"
    )
        port map (
      I0 => time_out_2ms_reg_n_0,
      I1 => rx_state(2),
      I2 => rx_state(1),
      I3 => rx_state(0),
      I4 => \FSM_sequential_rx_state[2]_i_2_n_0\,
      I5 => rx_state(3),
      O => \FSM_sequential_rx_state[3]_i_7_n_0\
    );
\FSM_sequential_rx_state[3]_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80800080"
    )
        port map (
      I0 => rx_state(0),
      I1 => rx_state(1),
      I2 => rx_state(2),
      I3 => time_out_2ms_reg_n_0,
      I4 => reset_time_out_reg_n_0,
      O => \FSM_sequential_rx_state[3]_i_9_n_0\
    );
\FSM_sequential_rx_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(0),
      Q => rx_state(0),
      R => \out\(0)
    );
\FSM_sequential_rx_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(1),
      Q => rx_state(1),
      R => \out\(0)
    );
\FSM_sequential_rx_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(2),
      Q => rx_state(2),
      R => \out\(0)
    );
\FSM_sequential_rx_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(3),
      Q => rx_state(3),
      R => \out\(0)
    );
RXUSERRDY_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB4000"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(0),
      I2 => rx_state(2),
      I3 => rx_state(1),
      I4 => \^gt0_rxuserrdy_t\,
      O => RXUSERRDY_i_1_n_0
    );
RXUSERRDY_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => RXUSERRDY_i_1_n_0,
      Q => \^gt0_rxuserrdy_t\,
      R => \out\(0)
    );
check_tlock_max_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB0008"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(0),
      I2 => rx_state(1),
      I3 => rx_state(3),
      I4 => check_tlock_max_reg_n_0,
      O => check_tlock_max_i_1_n_0
    );
check_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => check_tlock_max_i_1_n_0,
      Q => check_tlock_max_reg_n_0,
      R => \out\(0)
    );
gtrxreset_i_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0100"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(1),
      I2 => rx_state(2),
      I3 => rx_state(0),
      I4 => GTRXRESET,
      O => gtrxreset_i_i_1_n_0
    );
gtrxreset_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gtrxreset_i_i_1_n_0,
      Q => GTRXRESET,
      R => \out\(0)
    );
gtxe2_i_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => GTRXRESET,
      I1 => \^data_in\,
      I2 => gtxe2_i,
      O => SR(0)
    );
\init_wait_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => init_wait_count_reg(0),
      O => \init_wait_count[0]_i_1__0_n_0\
    );
\init_wait_count[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      O => \p_0_in__2\(1)
    );
\init_wait_count[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      O => \p_0_in__2\(2)
    );
\init_wait_count[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => init_wait_count_reg(1),
      I1 => init_wait_count_reg(2),
      I2 => init_wait_count_reg(0),
      I3 => init_wait_count_reg(3),
      O => \p_0_in__2\(3)
    );
\init_wait_count[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      O => \p_0_in__2\(4)
    );
\init_wait_count[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      I5 => init_wait_count_reg(5),
      O => \p_0_in__2\(5)
    );
\init_wait_count[6]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => \init_wait_count[6]_i_3__0_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \init_wait_count[6]_i_1__0_n_0\
    );
\init_wait_count[6]_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF40"
    )
        port map (
      I0 => \init_wait_count[6]_i_3__0_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \p_0_in__2\(6)
    );
\init_wait_count[6]_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => init_wait_count_reg(3),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      I3 => init_wait_count_reg(5),
      O => \init_wait_count[6]_i_3__0_n_0\
    );
\init_wait_count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \init_wait_count[0]_i_1__0_n_0\,
      Q => init_wait_count_reg(0)
    );
\init_wait_count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(1),
      Q => init_wait_count_reg(1)
    );
\init_wait_count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(2),
      Q => init_wait_count_reg(2)
    );
\init_wait_count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(3),
      Q => init_wait_count_reg(3)
    );
\init_wait_count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(4),
      Q => init_wait_count_reg(4)
    );
\init_wait_count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(5),
      Q => init_wait_count_reg(5)
    );
\init_wait_count_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(6),
      Q => init_wait_count_reg(6)
    );
\init_wait_done_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0010"
    )
        port map (
      I0 => \init_wait_count[6]_i_3__0_n_0\,
      I1 => init_wait_count_reg(4),
      I2 => init_wait_count_reg(6),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_done_reg_n_0,
      O => \init_wait_done_i_1__0_n_0\
    );
init_wait_done_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      CLR => \out\(0),
      D => \init_wait_done_i_1__0_n_0\,
      Q => init_wait_done_reg_n_0
    );
\mmcm_lock_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      O => \p_0_in__3\(0)
    );
\mmcm_lock_count[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      I1 => mmcm_lock_count_reg(1),
      O => \p_0_in__3\(1)
    );
\mmcm_lock_count[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => mmcm_lock_count_reg(1),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(2),
      O => \mmcm_lock_count[2]_i_1__0_n_0\
    );
\mmcm_lock_count[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => mmcm_lock_count_reg(2),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(1),
      I3 => mmcm_lock_count_reg(3),
      O => \mmcm_lock_count[3]_i_1__0_n_0\
    );
\mmcm_lock_count[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => mmcm_lock_count_reg(3),
      I1 => mmcm_lock_count_reg(1),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(2),
      I4 => mmcm_lock_count_reg(4),
      O => \mmcm_lock_count[4]_i_1__0_n_0\
    );
\mmcm_lock_count[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => \mmcm_lock_count[5]_i_1__0_n_0\
    );
\mmcm_lock_count[6]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I1 => mmcm_lock_count_reg(6),
      O => \mmcm_lock_count[6]_i_1__0_n_0\
    );
\mmcm_lock_count[7]_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I1 => mmcm_lock_count_reg(6),
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_2__0_n_0\
    );
\mmcm_lock_count[7]_i_3__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => mmcm_lock_count_reg(6),
      I1 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_3__0_n_0\
    );
\mmcm_lock_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \p_0_in__3\(0),
      Q => mmcm_lock_count_reg(0),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \p_0_in__3\(1),
      Q => mmcm_lock_count_reg(1),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[2]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(2),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[3]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(3),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[4]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(4),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[5]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(5),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[6]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(6),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[7]_i_3__0_n_0\,
      Q => mmcm_lock_count_reg(7),
      R => sync_mmcm_lock_reclocked_n_0
    );
mmcm_lock_reclocked_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEA0000"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => mmcm_lock_count_reg(7),
      I2 => mmcm_lock_count_reg(6),
      I3 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I4 => mmcm_lock_i,
      O => mmcm_lock_reclocked_i_1_n_0
    );
\mmcm_lock_reclocked_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => \mmcm_lock_reclocked_i_2__0_n_0\
    );
mmcm_lock_reclocked_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => mmcm_lock_reclocked_i_1_n_0,
      Q => mmcm_lock_reclocked,
      R => '0'
    );
reset_time_out_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(3),
      O => reset_time_out_i_3_n_0
    );
reset_time_out_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"34347674"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(3),
      I2 => rx_state(0),
      I3 => \FSM_sequential_rx_state_reg[0]_0\,
      I4 => rx_state(1),
      O => reset_time_out_i_4_n_0
    );
reset_time_out_reg: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => sync_data_valid_n_0,
      Q => reset_time_out_reg_n_0,
      S => \out\(0)
    );
\run_phase_alignment_int_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFF0010"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(1),
      I2 => rx_state(3),
      I3 => rx_state(0),
      I4 => run_phase_alignment_int_reg_n_0,
      O => \run_phase_alignment_int_i_1__0_n_0\
    );
run_phase_alignment_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => \run_phase_alignment_int_i_1__0_n_0\,
      Q => run_phase_alignment_int_reg_n_0,
      R => \out\(0)
    );
run_phase_alignment_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => '1',
      D => data_out_0,
      Q => run_phase_alignment_int_s3_reg_n_0,
      R => '0'
    );
rx_fsm_reset_done_int_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => rx_state(1),
      I1 => rx_state(0),
      O => rx_fsm_reset_done_int_i_5_n_0
    );
rx_fsm_reset_done_int_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(2),
      O => rx_fsm_reset_done_int_i_6_n_0
    );
rx_fsm_reset_done_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => sync_data_valid_n_1,
      Q => \^data_in\,
      R => \out\(0)
    );
rx_fsm_reset_done_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => '1',
      D => rx_fsm_reset_done_int_s2,
      Q => rx_fsm_reset_done_int_s3,
      R => '0'
    );
rxresetdone_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => rxresetdone_s2,
      Q => rxresetdone_s3,
      R => '0'
    );
sync_RXRESETDONE: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_10
     port map (
      data_out => rxresetdone_s2,
      data_sync_reg1_0 => data_sync_reg1,
      independent_clock_bufg => independent_clock_bufg
    );
sync_cplllock: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_11
     port map (
      \FSM_sequential_rx_state_reg[1]\ => sync_cplllock_n_0,
      Q(2 downto 0) => rx_state(3 downto 1),
      data_sync_reg1_0 => data_sync_reg1_1,
      independent_clock_bufg => independent_clock_bufg,
      rxresetdone_s3 => rxresetdone_s3
    );
sync_data_valid: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_12
     port map (
      D(2) => \rx_state__0\(3),
      D(1 downto 0) => \rx_state__0\(1 downto 0),
      E(0) => sync_data_valid_n_5,
      \FSM_sequential_rx_state_reg[0]\ => \FSM_sequential_rx_state[3]_i_3_n_0\,
      \FSM_sequential_rx_state_reg[0]_0\ => \FSM_sequential_rx_state[3]_i_7_n_0\,
      \FSM_sequential_rx_state_reg[0]_1\ => \FSM_sequential_rx_state_reg[0]_0\,
      \FSM_sequential_rx_state_reg[0]_2\ => \FSM_sequential_rx_state[0]_i_2_n_0\,
      \FSM_sequential_rx_state_reg[0]_3\ => init_wait_done_reg_n_0,
      \FSM_sequential_rx_state_reg[1]\ => sync_data_valid_n_0,
      \FSM_sequential_rx_state_reg[1]_0\ => \FSM_sequential_rx_state[1]_i_3_n_0\,
      \FSM_sequential_rx_state_reg[3]\ => \FSM_sequential_rx_state[3]_i_9_n_0\,
      Q(3 downto 0) => rx_state(3 downto 0),
      data_in => \^data_in\,
      data_out => data_out,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_lock_reclocked => mmcm_lock_reclocked,
      reset_time_out_reg => sync_cplllock_n_0,
      reset_time_out_reg_0 => reset_time_out_i_3_n_0,
      reset_time_out_reg_1 => reset_time_out_i_4_n_0,
      reset_time_out_reg_2 => reset_time_out_reg_n_0,
      rx_fsm_reset_done_int_reg => sync_data_valid_n_1,
      rx_fsm_reset_done_int_reg_0 => rx_fsm_reset_done_int_i_5_n_0,
      rx_fsm_reset_done_int_reg_1 => time_out_100us_reg_n_0,
      rx_fsm_reset_done_int_reg_2 => time_out_1us_reg_n_0,
      rx_fsm_reset_done_int_reg_3 => rx_fsm_reset_done_int_i_6_n_0,
      time_out_wait_bypass_s3 => time_out_wait_bypass_s3
    );
sync_mmcm_lock_reclocked: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_13
     port map (
      SR(0) => sync_mmcm_lock_reclocked_n_0,
      data_out => mmcm_lock_i,
      data_sync_reg1_0 => data_sync_reg1_0,
      independent_clock_bufg => independent_clock_bufg
    );
sync_run_phase_alignment_int: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_14
     port map (
      data_in => run_phase_alignment_int_reg_n_0,
      data_out => data_out_0,
      data_sync_reg1_0 => data_sync_reg6
    );
sync_time_out_wait_bypass: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_15
     port map (
      data_in => time_out_wait_bypass_reg_n_0,
      data_out => time_out_wait_bypass_s2,
      independent_clock_bufg => independent_clock_bufg
    );
sync_tx_fsm_reset_done_int: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_16
     port map (
      data_in => \^data_in\,
      data_out => rx_fsm_reset_done_int_s2,
      data_sync_reg6_0 => data_sync_reg6
    );
time_out_100us_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000100"
    )
        port map (
      I0 => time_out_2ms_i_4_n_0,
      I1 => time_out_counter_reg(17),
      I2 => time_out_counter_reg(16),
      I3 => time_out_100us_i_2_n_0,
      I4 => time_out_100us_i_3_n_0,
      I5 => time_out_100us_reg_n_0,
      O => time_out_100us_i_1_n_0
    );
time_out_100us_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000002000000000"
    )
        port map (
      I0 => time_out_counter_reg(10),
      I1 => time_out_counter_reg(12),
      I2 => time_out_counter_reg(5),
      I3 => time_out_counter_reg(7),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(14),
      O => time_out_100us_i_2_n_0
    );
time_out_100us_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => time_out_counter_reg(4),
      I1 => time_out_counter_reg(0),
      I2 => time_out_counter_reg(1),
      I3 => time_out_counter_reg(15),
      I4 => time_out_counter_reg(13),
      O => time_out_100us_i_3_n_0
    );
time_out_100us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_100us_i_1_n_0,
      Q => time_out_100us_reg_n_0,
      R => reset_time_out_reg_n_0
    );
time_out_1us_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00100000"
    )
        port map (
      I0 => time_out_2ms_i_2_n_0,
      I1 => time_out_1us_i_2_n_0,
      I2 => time_out_counter_reg(3),
      I3 => time_out_counter_reg(2),
      I4 => time_out_1us_i_3_n_0,
      I5 => time_out_1us_reg_n_0,
      O => time_out_1us_i_1_n_0
    );
time_out_1us_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(17),
      O => time_out_1us_i_2_n_0
    );
time_out_1us_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000010"
    )
        port map (
      I0 => time_out_counter_reg(9),
      I1 => time_out_counter_reg(11),
      I2 => time_out_counter_reg(6),
      I3 => time_out_counter_reg(8),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(12),
      O => time_out_1us_i_3_n_0
    );
time_out_1us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_1us_i_1_n_0,
      Q => time_out_1us_reg_n_0,
      R => reset_time_out_reg_n_0
    );
time_out_2ms_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF01"
    )
        port map (
      I0 => time_out_2ms_i_2_n_0,
      I1 => \time_out_2ms_i_3__0_n_0\,
      I2 => time_out_2ms_i_4_n_0,
      I3 => time_out_2ms_reg_n_0,
      O => time_out_2ms_i_1_n_0
    );
time_out_2ms_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFEFF"
    )
        port map (
      I0 => time_out_counter_reg(10),
      I1 => time_out_counter_reg(14),
      I2 => time_out_counter_reg(5),
      I3 => time_out_counter_reg(7),
      I4 => time_out_100us_i_3_n_0,
      O => time_out_2ms_i_2_n_0
    );
\time_out_2ms_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => time_out_counter_reg(12),
      I1 => time_out_counter_reg(16),
      I2 => time_out_counter_reg(18),
      I3 => time_out_counter_reg(17),
      O => \time_out_2ms_i_3__0_n_0\
    );
time_out_2ms_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFEFFFFFF"
    )
        port map (
      I0 => time_out_counter_reg(2),
      I1 => time_out_counter_reg(3),
      I2 => time_out_counter_reg(8),
      I3 => time_out_counter_reg(9),
      I4 => time_out_counter_reg(11),
      I5 => time_out_counter_reg(6),
      O => time_out_2ms_i_4_n_0
    );
time_out_2ms_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_2ms_i_1_n_0,
      Q => time_out_2ms_reg_n_0,
      R => reset_time_out_reg_n_0
    );
\time_out_counter[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFF7FF"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(18),
      I2 => time_out_counter_reg(16),
      I3 => time_out_counter_reg(12),
      I4 => time_out_2ms_i_2_n_0,
      I5 => time_out_2ms_i_4_n_0,
      O => time_out_counter
    );
\time_out_counter[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(0),
      O => \time_out_counter[0]_i_3_n_0\
    );
\time_out_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_7\,
      Q => time_out_counter_reg(0),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[0]_i_2__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \time_out_counter_reg[0]_i_2__0_n_0\,
      CO(2) => \time_out_counter_reg[0]_i_2__0_n_1\,
      CO(1) => \time_out_counter_reg[0]_i_2__0_n_2\,
      CO(0) => \time_out_counter_reg[0]_i_2__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \time_out_counter_reg[0]_i_2__0_n_4\,
      O(2) => \time_out_counter_reg[0]_i_2__0_n_5\,
      O(1) => \time_out_counter_reg[0]_i_2__0_n_6\,
      O(0) => \time_out_counter_reg[0]_i_2__0_n_7\,
      S(3 downto 1) => time_out_counter_reg(3 downto 1),
      S(0) => \time_out_counter[0]_i_3_n_0\
    );
\time_out_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_5\,
      Q => time_out_counter_reg(10),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_4\,
      Q => time_out_counter_reg(11),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_7\,
      Q => time_out_counter_reg(12),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[12]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[8]_i_1__0_n_0\,
      CO(3) => \time_out_counter_reg[12]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[12]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[12]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[12]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[12]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[12]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[12]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[12]_i_1__0_n_7\,
      S(3 downto 0) => time_out_counter_reg(15 downto 12)
    );
\time_out_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_6\,
      Q => time_out_counter_reg(13),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_5\,
      Q => time_out_counter_reg(14),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_4\,
      Q => time_out_counter_reg(15),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1__0_n_7\,
      Q => time_out_counter_reg(16),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[16]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[12]_i_1__0_n_0\,
      CO(3 downto 2) => \NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \time_out_counter_reg[16]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[16]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED\(3),
      O(2) => \time_out_counter_reg[16]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[16]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[16]_i_1__0_n_7\,
      S(3) => '0',
      S(2 downto 0) => time_out_counter_reg(18 downto 16)
    );
\time_out_counter_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1__0_n_6\,
      Q => time_out_counter_reg(17),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1__0_n_5\,
      Q => time_out_counter_reg(18),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_6\,
      Q => time_out_counter_reg(1),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_5\,
      Q => time_out_counter_reg(2),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_4\,
      Q => time_out_counter_reg(3),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_7\,
      Q => time_out_counter_reg(4),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[4]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[0]_i_2__0_n_0\,
      CO(3) => \time_out_counter_reg[4]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[4]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[4]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[4]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[4]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[4]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[4]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[4]_i_1__0_n_7\,
      S(3 downto 0) => time_out_counter_reg(7 downto 4)
    );
\time_out_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_6\,
      Q => time_out_counter_reg(5),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_5\,
      Q => time_out_counter_reg(6),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_4\,
      Q => time_out_counter_reg(7),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_7\,
      Q => time_out_counter_reg(8),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[8]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[4]_i_1__0_n_0\,
      CO(3) => \time_out_counter_reg[8]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[8]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[8]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[8]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[8]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[8]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[8]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[8]_i_1__0_n_7\,
      S(3 downto 0) => time_out_counter_reg(11 downto 8)
    );
\time_out_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_6\,
      Q => time_out_counter_reg(9),
      R => reset_time_out_reg_n_0
    );
time_out_wait_bypass_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AB00"
    )
        port map (
      I0 => time_out_wait_bypass_reg_n_0,
      I1 => rx_fsm_reset_done_int_s3,
      I2 => \time_out_wait_bypass_i_2__0_n_0\,
      I3 => run_phase_alignment_int_s3_reg_n_0,
      O => time_out_wait_bypass_i_1_n_0
    );
\time_out_wait_bypass_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FBFFFFFF"
    )
        port map (
      I0 => \time_out_wait_bypass_i_3__0_n_0\,
      I1 => wait_bypass_count_reg(1),
      I2 => wait_bypass_count_reg(11),
      I3 => wait_bypass_count_reg(0),
      I4 => \time_out_wait_bypass_i_4__0_n_0\,
      O => \time_out_wait_bypass_i_2__0_n_0\
    );
\time_out_wait_bypass_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => wait_bypass_count_reg(9),
      I1 => wait_bypass_count_reg(4),
      I2 => wait_bypass_count_reg(12),
      I3 => wait_bypass_count_reg(2),
      O => \time_out_wait_bypass_i_3__0_n_0\
    );
\time_out_wait_bypass_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000400000000"
    )
        port map (
      I0 => wait_bypass_count_reg(5),
      I1 => wait_bypass_count_reg(7),
      I2 => wait_bypass_count_reg(3),
      I3 => wait_bypass_count_reg(6),
      I4 => wait_bypass_count_reg(10),
      I5 => wait_bypass_count_reg(8),
      O => \time_out_wait_bypass_i_4__0_n_0\
    );
time_out_wait_bypass_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => '1',
      D => time_out_wait_bypass_i_1_n_0,
      Q => time_out_wait_bypass_reg_n_0,
      R => '0'
    );
time_out_wait_bypass_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_wait_bypass_s2,
      Q => time_out_wait_bypass_s3,
      R => '0'
    );
time_tlock_max1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => time_tlock_max1_carry_n_0,
      CO(2) => time_tlock_max1_carry_n_1,
      CO(1) => time_tlock_max1_carry_n_2,
      CO(0) => time_tlock_max1_carry_n_3,
      CYINIT => '0',
      DI(3) => time_tlock_max1_carry_i_1_n_0,
      DI(2) => time_tlock_max1_carry_i_2_n_0,
      DI(1) => time_tlock_max1_carry_i_3_n_0,
      DI(0) => time_tlock_max1_carry_i_4_n_0,
      O(3 downto 0) => NLW_time_tlock_max1_carry_O_UNCONNECTED(3 downto 0),
      S(3) => time_tlock_max1_carry_i_5_n_0,
      S(2) => time_tlock_max1_carry_i_6_n_0,
      S(1) => time_tlock_max1_carry_i_7_n_0,
      S(0) => time_tlock_max1_carry_i_8_n_0
    );
\time_tlock_max1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => time_tlock_max1_carry_n_0,
      CO(3) => \time_tlock_max1_carry__0_n_0\,
      CO(2) => \time_tlock_max1_carry__0_n_1\,
      CO(1) => \time_tlock_max1_carry__0_n_2\,
      CO(0) => \time_tlock_max1_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => time_out_counter_reg(15),
      DI(2) => \time_tlock_max1_carry__0_i_1_n_0\,
      DI(1) => '0',
      DI(0) => \time_tlock_max1_carry__0_i_2_n_0\,
      O(3 downto 0) => \NLW_time_tlock_max1_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \time_tlock_max1_carry__0_i_3_n_0\,
      S(2) => \time_tlock_max1_carry__0_i_4_n_0\,
      S(1) => \time_tlock_max1_carry__0_i_5_n_0\,
      S(0) => \time_tlock_max1_carry__0_i_6_n_0\
    );
\time_tlock_max1_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(12),
      I1 => time_out_counter_reg(13),
      O => \time_tlock_max1_carry__0_i_1_n_0\
    );
\time_tlock_max1_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(8),
      I1 => time_out_counter_reg(9),
      O => \time_tlock_max1_carry__0_i_2_n_0\
    );
\time_tlock_max1_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(14),
      I1 => time_out_counter_reg(15),
      O => \time_tlock_max1_carry__0_i_3_n_0\
    );
\time_tlock_max1_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(13),
      I1 => time_out_counter_reg(12),
      O => \time_tlock_max1_carry__0_i_4_n_0\
    );
\time_tlock_max1_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(10),
      I1 => time_out_counter_reg(11),
      O => \time_tlock_max1_carry__0_i_5_n_0\
    );
\time_tlock_max1_carry__0_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(9),
      I1 => time_out_counter_reg(8),
      O => \time_tlock_max1_carry__0_i_6_n_0\
    );
\time_tlock_max1_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_tlock_max1_carry__0_n_0\,
      CO(3 downto 2) => \NLW_time_tlock_max1_carry__1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => time_tlock_max1,
      CO(0) => \time_tlock_max1_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => time_out_counter_reg(18),
      DI(0) => \time_tlock_max1_carry__1_i_1_n_0\,
      O(3 downto 0) => \NLW_time_tlock_max1_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3 downto 2) => B"00",
      S(1) => \time_tlock_max1_carry__1_i_2_n_0\,
      S(0) => \time_tlock_max1_carry__1_i_3_n_0\
    );
\time_tlock_max1_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(17),
      O => \time_tlock_max1_carry__1_i_1_n_0\
    );
\time_tlock_max1_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(18),
      O => \time_tlock_max1_carry__1_i_2_n_0\
    );
\time_tlock_max1_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(16),
      O => \time_tlock_max1_carry__1_i_3_n_0\
    );
time_tlock_max1_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(6),
      I1 => time_out_counter_reg(7),
      O => time_tlock_max1_carry_i_1_n_0
    );
time_tlock_max1_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(4),
      I1 => time_out_counter_reg(5),
      O => time_tlock_max1_carry_i_2_n_0
    );
time_tlock_max1_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(2),
      I1 => time_out_counter_reg(3),
      O => time_tlock_max1_carry_i_3_n_0
    );
time_tlock_max1_carry_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(0),
      I1 => time_out_counter_reg(1),
      O => time_tlock_max1_carry_i_4_n_0
    );
time_tlock_max1_carry_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(7),
      I1 => time_out_counter_reg(6),
      O => time_tlock_max1_carry_i_5_n_0
    );
time_tlock_max1_carry_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(5),
      I1 => time_out_counter_reg(4),
      O => time_tlock_max1_carry_i_6_n_0
    );
time_tlock_max1_carry_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(3),
      I1 => time_out_counter_reg(2),
      O => time_tlock_max1_carry_i_7_n_0
    );
time_tlock_max1_carry_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(1),
      I1 => time_out_counter_reg(0),
      O => time_tlock_max1_carry_i_8_n_0
    );
time_tlock_max_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => check_tlock_max_reg_n_0,
      I1 => time_tlock_max1,
      I2 => time_tlock_max,
      O => time_tlock_max_i_1_n_0
    );
time_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_tlock_max_i_1_n_0,
      Q => time_tlock_max,
      R => reset_time_out_reg_n_0
    );
\wait_bypass_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => run_phase_alignment_int_s3_reg_n_0,
      O => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count[0]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \time_out_wait_bypass_i_2__0_n_0\,
      I1 => rx_fsm_reset_done_int_s3,
      O => \wait_bypass_count[0]_i_2__0_n_0\
    );
\wait_bypass_count[0]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_bypass_count_reg(0),
      O => \wait_bypass_count[0]_i_4_n_0\
    );
\wait_bypass_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_7\,
      Q => wait_bypass_count_reg(0),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[0]_i_3__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \wait_bypass_count_reg[0]_i_3__0_n_0\,
      CO(2) => \wait_bypass_count_reg[0]_i_3__0_n_1\,
      CO(1) => \wait_bypass_count_reg[0]_i_3__0_n_2\,
      CO(0) => \wait_bypass_count_reg[0]_i_3__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \wait_bypass_count_reg[0]_i_3__0_n_4\,
      O(2) => \wait_bypass_count_reg[0]_i_3__0_n_5\,
      O(1) => \wait_bypass_count_reg[0]_i_3__0_n_6\,
      O(0) => \wait_bypass_count_reg[0]_i_3__0_n_7\,
      S(3 downto 1) => wait_bypass_count_reg(3 downto 1),
      S(0) => \wait_bypass_count[0]_i_4_n_0\
    );
\wait_bypass_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_5\,
      Q => wait_bypass_count_reg(10),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_4\,
      Q => wait_bypass_count_reg(11),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[12]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(12),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[12]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[8]_i_1__0_n_0\,
      CO(3 downto 0) => \NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED\(3 downto 1),
      O(0) => \wait_bypass_count_reg[12]_i_1__0_n_7\,
      S(3 downto 1) => B"000",
      S(0) => wait_bypass_count_reg(12)
    );
\wait_bypass_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_6\,
      Q => wait_bypass_count_reg(1),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_5\,
      Q => wait_bypass_count_reg(2),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_4\,
      Q => wait_bypass_count_reg(3),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(4),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[4]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[0]_i_3__0_n_0\,
      CO(3) => \wait_bypass_count_reg[4]_i_1__0_n_0\,
      CO(2) => \wait_bypass_count_reg[4]_i_1__0_n_1\,
      CO(1) => \wait_bypass_count_reg[4]_i_1__0_n_2\,
      CO(0) => \wait_bypass_count_reg[4]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[4]_i_1__0_n_4\,
      O(2) => \wait_bypass_count_reg[4]_i_1__0_n_5\,
      O(1) => \wait_bypass_count_reg[4]_i_1__0_n_6\,
      O(0) => \wait_bypass_count_reg[4]_i_1__0_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(7 downto 4)
    );
\wait_bypass_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_6\,
      Q => wait_bypass_count_reg(5),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_5\,
      Q => wait_bypass_count_reg(6),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_4\,
      Q => wait_bypass_count_reg(7),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(8),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[8]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[4]_i_1__0_n_0\,
      CO(3) => \wait_bypass_count_reg[8]_i_1__0_n_0\,
      CO(2) => \wait_bypass_count_reg[8]_i_1__0_n_1\,
      CO(1) => \wait_bypass_count_reg[8]_i_1__0_n_2\,
      CO(0) => \wait_bypass_count_reg[8]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[8]_i_1__0_n_4\,
      O(2) => \wait_bypass_count_reg[8]_i_1__0_n_5\,
      O(1) => \wait_bypass_count_reg[8]_i_1__0_n_6\,
      O(0) => \wait_bypass_count_reg[8]_i_1__0_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(11 downto 8)
    );
\wait_bypass_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_6\,
      Q => wait_bypass_count_reg(9),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_time_cnt[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      O => \wait_time_cnt0__0\(0)
    );
\wait_time_cnt[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      I1 => wait_time_cnt_reg(1),
      O => \wait_time_cnt[1]_i_1__0_n_0\
    );
\wait_time_cnt[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => wait_time_cnt_reg(1),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(2),
      O => \wait_time_cnt[2]_i_1__0_n_0\
    );
\wait_time_cnt[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE01"
    )
        port map (
      I0 => wait_time_cnt_reg(2),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(1),
      I3 => wait_time_cnt_reg(3),
      O => \wait_time_cnt[3]_i_1__0_n_0\
    );
\wait_time_cnt[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0001"
    )
        port map (
      I0 => wait_time_cnt_reg(3),
      I1 => wait_time_cnt_reg(1),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(2),
      I4 => wait_time_cnt_reg(4),
      O => \wait_time_cnt[4]_i_1__0_n_0\
    );
\wait_time_cnt[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000001"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[5]_i_1__0_n_0\
    );
\wait_time_cnt[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => rx_state(0),
      I1 => rx_state(1),
      I2 => rx_state(3),
      O => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt[6]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4__0_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => \wait_time_cnt[6]_i_2__0_n_0\
    );
\wait_time_cnt[6]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4__0_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => \wait_time_cnt[6]_i_3__0_n_0\
    );
\wait_time_cnt[6]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[6]_i_4__0_n_0\
    );
\wait_time_cnt_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt0__0\(0),
      Q => wait_time_cnt_reg(0),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[1]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(1),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[2]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(2),
      S => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[3]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(3),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[4]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(4),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[5]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(5),
      S => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[6]_i_3__0_n_0\,
      Q => wait_time_cnt_reg(6),
      S => \wait_time_cnt[6]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_TX_STARTUP_FSM is
  port (
    mmcm_reset : out STD_LOGIC;
    gt0_cpllreset_t : out STD_LOGIC;
    data_in : out STD_LOGIC;
    gt0_txuserrdy_t : out STD_LOGIC;
    gt0_gttxreset_in0_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : in STD_LOGIC;
    gt0_cpllrefclklost_i : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    data_sync_reg1_1 : in STD_LOGIC;
    data_sync_reg1_2 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_TX_STARTUP_FSM;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_TX_STARTUP_FSM is
  signal CPLL_RESET_i_1_n_0 : STD_LOGIC;
  signal CPLL_RESET_i_2_n_0 : STD_LOGIC;
  signal \FSM_sequential_tx_state[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[0]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_8_n_0\ : STD_LOGIC;
  signal GTTXRESET : STD_LOGIC;
  signal MMCM_RESET_i_1_n_0 : STD_LOGIC;
  signal TXUSERRDY_i_1_n_0 : STD_LOGIC;
  signal clear : STD_LOGIC;
  signal \^data_in\ : STD_LOGIC;
  signal data_out : STD_LOGIC;
  signal \^gt0_cpllreset_t\ : STD_LOGIC;
  signal \^gt0_txuserrdy_t\ : STD_LOGIC;
  signal gttxreset_i_i_1_n_0 : STD_LOGIC;
  signal \init_wait_count[0]_i_1_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_1_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_3_n_0\ : STD_LOGIC;
  signal init_wait_count_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal init_wait_done_i_1_n_0 : STD_LOGIC;
  signal init_wait_done_reg_n_0 : STD_LOGIC;
  signal \mmcm_lock_count[2]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[3]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[4]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[5]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[6]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_2_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_3_n_0\ : STD_LOGIC;
  signal mmcm_lock_count_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal mmcm_lock_i : STD_LOGIC;
  signal mmcm_lock_reclocked : STD_LOGIC;
  signal mmcm_lock_reclocked_i_1_n_0 : STD_LOGIC;
  signal mmcm_lock_reclocked_i_2_n_0 : STD_LOGIC;
  signal \^mmcm_reset\ : STD_LOGIC;
  signal \p_0_in__0\ : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \p_0_in__1\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pll_reset_asserted_i_1_n_0 : STD_LOGIC;
  signal pll_reset_asserted_i_2_n_0 : STD_LOGIC;
  signal pll_reset_asserted_reg_n_0 : STD_LOGIC;
  signal refclk_stable_count : STD_LOGIC;
  signal \refclk_stable_count[0]_i_3_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_4_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_5_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_6_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_7_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_8_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_9_n_0\ : STD_LOGIC;
  signal refclk_stable_count_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \refclk_stable_count_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal refclk_stable_i_1_n_0 : STD_LOGIC;
  signal refclk_stable_i_2_n_0 : STD_LOGIC;
  signal refclk_stable_i_3_n_0 : STD_LOGIC;
  signal refclk_stable_i_4_n_0 : STD_LOGIC;
  signal refclk_stable_i_5_n_0 : STD_LOGIC;
  signal refclk_stable_i_6_n_0 : STD_LOGIC;
  signal refclk_stable_reg_n_0 : STD_LOGIC;
  signal reset_time_out : STD_LOGIC;
  signal \reset_time_out_i_2__0_n_0\ : STD_LOGIC;
  signal run_phase_alignment_int_i_1_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_reg_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_s3 : STD_LOGIC;
  signal sel : STD_LOGIC;
  signal sync_cplllock_n_0 : STD_LOGIC;
  signal sync_cplllock_n_1 : STD_LOGIC;
  signal sync_mmcm_lock_reclocked_n_0 : STD_LOGIC;
  signal time_out_2ms_i_1_n_0 : STD_LOGIC;
  signal \time_out_2ms_i_2__0_n_0\ : STD_LOGIC;
  signal time_out_2ms_i_3_n_0 : STD_LOGIC;
  signal \time_out_2ms_i_4__0_n_0\ : STD_LOGIC;
  signal time_out_2ms_i_5_n_0 : STD_LOGIC;
  signal time_out_2ms_reg_n_0 : STD_LOGIC;
  signal time_out_500us_i_1_n_0 : STD_LOGIC;
  signal time_out_500us_i_2_n_0 : STD_LOGIC;
  signal time_out_500us_reg_n_0 : STD_LOGIC;
  signal time_out_counter : STD_LOGIC;
  signal \time_out_counter[0]_i_3__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[0]_i_4_n_0\ : STD_LOGIC;
  signal time_out_counter_reg : STD_LOGIC_VECTOR ( 18 downto 0 );
  signal \time_out_counter_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal time_out_wait_bypass_i_1_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_2_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_3_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_4_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_5_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_reg_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_s2 : STD_LOGIC;
  signal time_out_wait_bypass_s3 : STD_LOGIC;
  signal time_tlock_max_i_1_n_0 : STD_LOGIC;
  signal time_tlock_max_i_2_n_0 : STD_LOGIC;
  signal time_tlock_max_i_3_n_0 : STD_LOGIC;
  signal time_tlock_max_i_4_n_0 : STD_LOGIC;
  signal time_tlock_max_reg_n_0 : STD_LOGIC;
  signal tx_fsm_reset_done_int_i_1_n_0 : STD_LOGIC;
  signal tx_fsm_reset_done_int_s2 : STD_LOGIC;
  signal tx_fsm_reset_done_int_s3 : STD_LOGIC;
  signal tx_state : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \tx_state__0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal txresetdone_s2 : STD_LOGIC;
  signal txresetdone_s3 : STD_LOGIC;
  signal \wait_bypass_count[0]_i_2_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_4__0_n_0\ : STD_LOGIC;
  signal wait_bypass_count_reg : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal \wait_bypass_count_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal wait_time_cnt0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal wait_time_cnt0_0 : STD_LOGIC;
  signal \wait_time_cnt[1]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[2]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[3]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[4]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[5]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_3_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_4_n_0\ : STD_LOGIC;
  signal wait_time_cnt_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of CPLL_RESET_i_2 : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[0]_i_2\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[0]_i_3\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[1]_i_1\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[3]_i_2\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[3]_i_7\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[3]_i_8\ : label is "soft_lutpair59";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[0]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[1]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[2]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[3]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute SOFT_HLUTNM of MMCM_RESET_i_1 : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of TXUSERRDY_i_1 : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of gttxreset_i_i_1 : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \init_wait_count[1]_i_1\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \init_wait_count[2]_i_1\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \init_wait_count[3]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \init_wait_count[4]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_2\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_3\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \mmcm_lock_count[1]_i_1\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \mmcm_lock_count[2]_i_1\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \mmcm_lock_count[3]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \mmcm_lock_count[4]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \mmcm_lock_count[6]_i_1\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \mmcm_lock_count[7]_i_3\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of pll_reset_asserted_i_2 : label is "soft_lutpair60";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[12]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[16]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[20]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[24]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[28]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[8]_i_1\ : label is 11;
  attribute SOFT_HLUTNM of run_phase_alignment_int_i_1 : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \time_out_2ms_i_4__0\ : label is "soft_lutpair56";
  attribute ADDER_THRESHOLD of \time_out_counter_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[12]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[16]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[8]_i_1\ : label is 11;
  attribute SOFT_HLUTNM of time_tlock_max_i_4 : label is "soft_lutpair56";
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[0]_i_3\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[12]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[16]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[8]_i_1\ : label is 11;
  attribute SOFT_HLUTNM of \wait_time_cnt[0]_i_1\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \wait_time_cnt[1]_i_1\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \wait_time_cnt[3]_i_1\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \wait_time_cnt[4]_i_1\ : label is "soft_lutpair63";
begin
  data_in <= \^data_in\;
  gt0_cpllreset_t <= \^gt0_cpllreset_t\;
  gt0_txuserrdy_t <= \^gt0_txuserrdy_t\;
  mmcm_reset <= \^mmcm_reset\;
CPLL_RESET_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF1F0000001F"
    )
        port map (
      I0 => pll_reset_asserted_reg_n_0,
      I1 => gt0_cpllrefclklost_i,
      I2 => refclk_stable_reg_n_0,
      I3 => CPLL_RESET_i_2_n_0,
      I4 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I5 => \^gt0_cpllreset_t\,
      O => CPLL_RESET_i_1_n_0
    );
CPLL_RESET_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      O => CPLL_RESET_i_2_n_0
    );
CPLL_RESET_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => CPLL_RESET_i_1_n_0,
      Q => \^gt0_cpllreset_t\,
      R => \out\(0)
    );
\FSM_sequential_tx_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F3FFF3F0F5F0F5F0"
    )
        port map (
      I0 => \FSM_sequential_tx_state[2]_i_2_n_0\,
      I1 => \FSM_sequential_tx_state[0]_i_2_n_0\,
      I2 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I3 => tx_state(2),
      I4 => time_out_2ms_reg_n_0,
      I5 => tx_state(1),
      O => \tx_state__0\(0)
    );
\FSM_sequential_tx_state[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => reset_time_out,
      I1 => time_out_500us_reg_n_0,
      O => \FSM_sequential_tx_state[0]_i_2_n_0\
    );
\FSM_sequential_tx_state[0]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      O => \FSM_sequential_tx_state[0]_i_3_n_0\
    );
\FSM_sequential_tx_state[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"005A001A"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(0),
      I3 => tx_state(3),
      I4 => \FSM_sequential_tx_state[2]_i_2_n_0\,
      O => \tx_state__0\(1)
    );
\FSM_sequential_tx_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"04000C0C06020C0C"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(3),
      I3 => \FSM_sequential_tx_state[2]_i_2_n_0\,
      I4 => tx_state(0),
      I5 => time_out_2ms_reg_n_0,
      O => \tx_state__0\(2)
    );
\FSM_sequential_tx_state[2]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => time_tlock_max_reg_n_0,
      I1 => reset_time_out,
      I2 => mmcm_lock_reclocked,
      O => \FSM_sequential_tx_state[2]_i_2_n_0\
    );
\FSM_sequential_tx_state[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F4FF4444"
    )
        port map (
      I0 => time_out_wait_bypass_s3,
      I1 => tx_state(3),
      I2 => reset_time_out,
      I3 => time_out_500us_reg_n_0,
      I4 => \FSM_sequential_tx_state[3]_i_8_n_0\,
      O => \tx_state__0\(3)
    );
\FSM_sequential_tx_state[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00BA000000000000"
    )
        port map (
      I0 => txresetdone_s3,
      I1 => reset_time_out,
      I2 => time_out_500us_reg_n_0,
      I3 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I4 => tx_state(2),
      I5 => tx_state(1),
      O => \FSM_sequential_tx_state[3]_i_3_n_0\
    );
\FSM_sequential_tx_state[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000300FF00AA"
    )
        port map (
      I0 => init_wait_done_reg_n_0,
      I1 => \wait_time_cnt[6]_i_4_n_0\,
      I2 => wait_time_cnt_reg(6),
      I3 => tx_state(0),
      I4 => tx_state(3),
      I5 => CPLL_RESET_i_2_n_0,
      O => \FSM_sequential_tx_state[3]_i_4_n_0\
    );
\FSM_sequential_tx_state[3]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0404040400040000"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I3 => reset_time_out,
      I4 => time_tlock_max_reg_n_0,
      I5 => mmcm_lock_reclocked,
      O => \FSM_sequential_tx_state[3]_i_6_n_0\
    );
\FSM_sequential_tx_state[3]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1000"
    )
        port map (
      I0 => tx_state(2),
      I1 => tx_state(3),
      I2 => tx_state(0),
      I3 => tx_state(1),
      O => \FSM_sequential_tx_state[3]_i_7_n_0\
    );
\FSM_sequential_tx_state[3]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      I2 => tx_state(2),
      I3 => tx_state(1),
      O => \FSM_sequential_tx_state[3]_i_8_n_0\
    );
\FSM_sequential_tx_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(0),
      Q => tx_state(0),
      R => \out\(0)
    );
\FSM_sequential_tx_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(1),
      Q => tx_state(1),
      R => \out\(0)
    );
\FSM_sequential_tx_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(2),
      Q => tx_state(2),
      R => \out\(0)
    );
\FSM_sequential_tx_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(3),
      Q => tx_state(3),
      R => \out\(0)
    );
MMCM_RESET_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70004"
    )
        port map (
      I0 => tx_state(2),
      I1 => tx_state(0),
      I2 => tx_state(3),
      I3 => tx_state(1),
      I4 => \^mmcm_reset\,
      O => MMCM_RESET_i_1_n_0
    );
MMCM_RESET_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => MMCM_RESET_i_1_n_0,
      Q => \^mmcm_reset\,
      R => \out\(0)
    );
TXUSERRDY_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFD2000"
    )
        port map (
      I0 => tx_state(0),
      I1 => tx_state(3),
      I2 => tx_state(2),
      I3 => tx_state(1),
      I4 => \^gt0_txuserrdy_t\,
      O => TXUSERRDY_i_1_n_0
    );
TXUSERRDY_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => TXUSERRDY_i_1_n_0,
      Q => \^gt0_txuserrdy_t\,
      R => \out\(0)
    );
gttxreset_i_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0100"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(1),
      I2 => tx_state(2),
      I3 => tx_state(0),
      I4 => GTTXRESET,
      O => gttxreset_i_i_1_n_0
    );
gttxreset_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gttxreset_i_i_1_n_0,
      Q => GTTXRESET,
      R => \out\(0)
    );
gtxe2_i_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => GTTXRESET,
      I1 => \^data_in\,
      I2 => gtxe2_i,
      O => gt0_gttxreset_in0_out
    );
\init_wait_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => init_wait_count_reg(0),
      O => \init_wait_count[0]_i_1_n_0\
    );
\init_wait_count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      O => \p_0_in__0\(1)
    );
\init_wait_count[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      O => \p_0_in__0\(2)
    );
\init_wait_count[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => init_wait_count_reg(1),
      I1 => init_wait_count_reg(2),
      I2 => init_wait_count_reg(0),
      I3 => init_wait_count_reg(3),
      O => \p_0_in__0\(3)
    );
\init_wait_count[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      O => \p_0_in__0\(4)
    );
\init_wait_count[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      I5 => init_wait_count_reg(5),
      O => \p_0_in__0\(5)
    );
\init_wait_count[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => \init_wait_count[6]_i_3_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \init_wait_count[6]_i_1_n_0\
    );
\init_wait_count[6]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF40"
    )
        port map (
      I0 => \init_wait_count[6]_i_3_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \p_0_in__0\(6)
    );
\init_wait_count[6]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => init_wait_count_reg(3),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      I3 => init_wait_count_reg(5),
      O => \init_wait_count[6]_i_3_n_0\
    );
\init_wait_count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \init_wait_count[0]_i_1_n_0\,
      Q => init_wait_count_reg(0)
    );
\init_wait_count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(1),
      Q => init_wait_count_reg(1)
    );
\init_wait_count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(2),
      Q => init_wait_count_reg(2)
    );
\init_wait_count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(3),
      Q => init_wait_count_reg(3)
    );
\init_wait_count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(4),
      Q => init_wait_count_reg(4)
    );
\init_wait_count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(5),
      Q => init_wait_count_reg(5)
    );
\init_wait_count_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(6),
      Q => init_wait_count_reg(6)
    );
init_wait_done_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0010"
    )
        port map (
      I0 => \init_wait_count[6]_i_3_n_0\,
      I1 => init_wait_count_reg(4),
      I2 => init_wait_count_reg(6),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_done_reg_n_0,
      O => init_wait_done_i_1_n_0
    );
init_wait_done_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      CLR => \out\(0),
      D => init_wait_done_i_1_n_0,
      Q => init_wait_done_reg_n_0
    );
\mmcm_lock_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      O => \p_0_in__1\(0)
    );
\mmcm_lock_count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      I1 => mmcm_lock_count_reg(1),
      O => \p_0_in__1\(1)
    );
\mmcm_lock_count[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => mmcm_lock_count_reg(1),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(2),
      O => \mmcm_lock_count[2]_i_1_n_0\
    );
\mmcm_lock_count[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => mmcm_lock_count_reg(2),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(1),
      I3 => mmcm_lock_count_reg(3),
      O => \mmcm_lock_count[3]_i_1_n_0\
    );
\mmcm_lock_count[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => mmcm_lock_count_reg(3),
      I1 => mmcm_lock_count_reg(1),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(2),
      I4 => mmcm_lock_count_reg(4),
      O => \mmcm_lock_count[4]_i_1_n_0\
    );
\mmcm_lock_count[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => \mmcm_lock_count[5]_i_1_n_0\
    );
\mmcm_lock_count[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => mmcm_lock_reclocked_i_2_n_0,
      I1 => mmcm_lock_count_reg(6),
      O => \mmcm_lock_count[6]_i_1_n_0\
    );
\mmcm_lock_count[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => mmcm_lock_reclocked_i_2_n_0,
      I1 => mmcm_lock_count_reg(6),
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_2_n_0\
    );
\mmcm_lock_count[7]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => mmcm_lock_count_reg(6),
      I1 => mmcm_lock_reclocked_i_2_n_0,
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_3_n_0\
    );
\mmcm_lock_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \p_0_in__1\(0),
      Q => mmcm_lock_count_reg(0),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \p_0_in__1\(1),
      Q => mmcm_lock_count_reg(1),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[2]_i_1_n_0\,
      Q => mmcm_lock_count_reg(2),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[3]_i_1_n_0\,
      Q => mmcm_lock_count_reg(3),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[4]_i_1_n_0\,
      Q => mmcm_lock_count_reg(4),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[5]_i_1_n_0\,
      Q => mmcm_lock_count_reg(5),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[6]_i_1_n_0\,
      Q => mmcm_lock_count_reg(6),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[7]_i_3_n_0\,
      Q => mmcm_lock_count_reg(7),
      R => sync_mmcm_lock_reclocked_n_0
    );
mmcm_lock_reclocked_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEA0000"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => mmcm_lock_count_reg(7),
      I2 => mmcm_lock_count_reg(6),
      I3 => mmcm_lock_reclocked_i_2_n_0,
      I4 => mmcm_lock_i,
      O => mmcm_lock_reclocked_i_1_n_0
    );
mmcm_lock_reclocked_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => mmcm_lock_reclocked_i_2_n_0
    );
mmcm_lock_reclocked_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => mmcm_lock_reclocked_i_1_n_0,
      Q => mmcm_lock_reclocked,
      R => '0'
    );
pll_reset_asserted_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000CD55CCCCCCCC"
    )
        port map (
      I0 => tx_state(3),
      I1 => pll_reset_asserted_reg_n_0,
      I2 => gt0_cpllrefclklost_i,
      I3 => refclk_stable_reg_n_0,
      I4 => tx_state(1),
      I5 => pll_reset_asserted_i_2_n_0,
      O => pll_reset_asserted_i_1_n_0
    );
pll_reset_asserted_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => tx_state(0),
      I1 => tx_state(3),
      I2 => tx_state(2),
      O => pll_reset_asserted_i_2_n_0
    );
pll_reset_asserted_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pll_reset_asserted_i_1_n_0,
      Q => pll_reset_asserted_reg_n_0,
      R => \out\(0)
    );
\refclk_stable_count[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFFFF"
    )
        port map (
      I0 => \refclk_stable_count[0]_i_3_n_0\,
      I1 => \refclk_stable_count[0]_i_4_n_0\,
      I2 => \refclk_stable_count[0]_i_5_n_0\,
      I3 => \refclk_stable_count[0]_i_6_n_0\,
      I4 => \refclk_stable_count[0]_i_7_n_0\,
      I5 => \refclk_stable_count[0]_i_8_n_0\,
      O => refclk_stable_count
    );
\refclk_stable_count[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFDFFFFFFFFFFFFF"
    )
        port map (
      I0 => refclk_stable_count_reg(13),
      I1 => refclk_stable_count_reg(12),
      I2 => refclk_stable_count_reg(10),
      I3 => refclk_stable_count_reg(11),
      I4 => refclk_stable_count_reg(9),
      I5 => refclk_stable_count_reg(8),
      O => \refclk_stable_count[0]_i_3_n_0\
    );
\refclk_stable_count[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFDF"
    )
        port map (
      I0 => refclk_stable_count_reg(19),
      I1 => refclk_stable_count_reg(18),
      I2 => refclk_stable_count_reg(16),
      I3 => refclk_stable_count_reg(17),
      I4 => refclk_stable_count_reg(15),
      I5 => refclk_stable_count_reg(14),
      O => \refclk_stable_count[0]_i_4_n_0\
    );
\refclk_stable_count[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => refclk_stable_count_reg(30),
      I1 => refclk_stable_count_reg(31),
      I2 => refclk_stable_count_reg(28),
      I3 => refclk_stable_count_reg(29),
      I4 => refclk_stable_count_reg(27),
      I5 => refclk_stable_count_reg(26),
      O => \refclk_stable_count[0]_i_5_n_0\
    );
\refclk_stable_count[0]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => refclk_stable_count_reg(24),
      I1 => refclk_stable_count_reg(25),
      I2 => refclk_stable_count_reg(22),
      I3 => refclk_stable_count_reg(23),
      I4 => refclk_stable_count_reg(21),
      I5 => refclk_stable_count_reg(20),
      O => \refclk_stable_count[0]_i_6_n_0\
    );
\refclk_stable_count[0]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => refclk_stable_count_reg(0),
      I1 => refclk_stable_count_reg(1),
      O => \refclk_stable_count[0]_i_7_n_0\
    );
\refclk_stable_count[0]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFF7"
    )
        port map (
      I0 => refclk_stable_count_reg(6),
      I1 => refclk_stable_count_reg(7),
      I2 => refclk_stable_count_reg(4),
      I3 => refclk_stable_count_reg(5),
      I4 => refclk_stable_count_reg(3),
      I5 => refclk_stable_count_reg(2),
      O => \refclk_stable_count[0]_i_8_n_0\
    );
\refclk_stable_count[0]_i_9\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => refclk_stable_count_reg(0),
      O => \refclk_stable_count[0]_i_9_n_0\
    );
\refclk_stable_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_7\,
      Q => refclk_stable_count_reg(0),
      R => '0'
    );
\refclk_stable_count_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \refclk_stable_count_reg[0]_i_2_n_0\,
      CO(2) => \refclk_stable_count_reg[0]_i_2_n_1\,
      CO(1) => \refclk_stable_count_reg[0]_i_2_n_2\,
      CO(0) => \refclk_stable_count_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \refclk_stable_count_reg[0]_i_2_n_4\,
      O(2) => \refclk_stable_count_reg[0]_i_2_n_5\,
      O(1) => \refclk_stable_count_reg[0]_i_2_n_6\,
      O(0) => \refclk_stable_count_reg[0]_i_2_n_7\,
      S(3 downto 1) => refclk_stable_count_reg(3 downto 1),
      S(0) => \refclk_stable_count[0]_i_9_n_0\
    );
\refclk_stable_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_5\,
      Q => refclk_stable_count_reg(10),
      R => '0'
    );
\refclk_stable_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_4\,
      Q => refclk_stable_count_reg(11),
      R => '0'
    );
\refclk_stable_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_7\,
      Q => refclk_stable_count_reg(12),
      R => '0'
    );
\refclk_stable_count_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[8]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[12]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[12]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[12]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[12]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[12]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[12]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[12]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(15 downto 12)
    );
\refclk_stable_count_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_6\,
      Q => refclk_stable_count_reg(13),
      R => '0'
    );
\refclk_stable_count_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_5\,
      Q => refclk_stable_count_reg(14),
      R => '0'
    );
\refclk_stable_count_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_4\,
      Q => refclk_stable_count_reg(15),
      R => '0'
    );
\refclk_stable_count_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_7\,
      Q => refclk_stable_count_reg(16),
      R => '0'
    );
\refclk_stable_count_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[12]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[16]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[16]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[16]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[16]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[16]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[16]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[16]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(19 downto 16)
    );
\refclk_stable_count_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_6\,
      Q => refclk_stable_count_reg(17),
      R => '0'
    );
\refclk_stable_count_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_5\,
      Q => refclk_stable_count_reg(18),
      R => '0'
    );
\refclk_stable_count_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_4\,
      Q => refclk_stable_count_reg(19),
      R => '0'
    );
\refclk_stable_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_6\,
      Q => refclk_stable_count_reg(1),
      R => '0'
    );
\refclk_stable_count_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_7\,
      Q => refclk_stable_count_reg(20),
      R => '0'
    );
\refclk_stable_count_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[16]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[20]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[20]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[20]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[20]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[20]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[20]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[20]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(23 downto 20)
    );
\refclk_stable_count_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_6\,
      Q => refclk_stable_count_reg(21),
      R => '0'
    );
\refclk_stable_count_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_5\,
      Q => refclk_stable_count_reg(22),
      R => '0'
    );
\refclk_stable_count_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_4\,
      Q => refclk_stable_count_reg(23),
      R => '0'
    );
\refclk_stable_count_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_7\,
      Q => refclk_stable_count_reg(24),
      R => '0'
    );
\refclk_stable_count_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[20]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[24]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[24]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[24]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[24]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[24]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[24]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[24]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(27 downto 24)
    );
\refclk_stable_count_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_6\,
      Q => refclk_stable_count_reg(25),
      R => '0'
    );
\refclk_stable_count_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_5\,
      Q => refclk_stable_count_reg(26),
      R => '0'
    );
\refclk_stable_count_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_4\,
      Q => refclk_stable_count_reg(27),
      R => '0'
    );
\refclk_stable_count_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_7\,
      Q => refclk_stable_count_reg(28),
      R => '0'
    );
\refclk_stable_count_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[24]_i_1_n_0\,
      CO(3) => \NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \refclk_stable_count_reg[28]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[28]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[28]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[28]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[28]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[28]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(31 downto 28)
    );
\refclk_stable_count_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_6\,
      Q => refclk_stable_count_reg(29),
      R => '0'
    );
\refclk_stable_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_5\,
      Q => refclk_stable_count_reg(2),
      R => '0'
    );
\refclk_stable_count_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_5\,
      Q => refclk_stable_count_reg(30),
      R => '0'
    );
\refclk_stable_count_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_4\,
      Q => refclk_stable_count_reg(31),
      R => '0'
    );
\refclk_stable_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_4\,
      Q => refclk_stable_count_reg(3),
      R => '0'
    );
\refclk_stable_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_7\,
      Q => refclk_stable_count_reg(4),
      R => '0'
    );
\refclk_stable_count_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[0]_i_2_n_0\,
      CO(3) => \refclk_stable_count_reg[4]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[4]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[4]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[4]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[4]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[4]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[4]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(7 downto 4)
    );
\refclk_stable_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_6\,
      Q => refclk_stable_count_reg(5),
      R => '0'
    );
\refclk_stable_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_5\,
      Q => refclk_stable_count_reg(6),
      R => '0'
    );
\refclk_stable_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_4\,
      Q => refclk_stable_count_reg(7),
      R => '0'
    );
\refclk_stable_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_7\,
      Q => refclk_stable_count_reg(8),
      R => '0'
    );
\refclk_stable_count_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[4]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[8]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[8]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[8]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[8]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[8]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[8]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[8]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(11 downto 8)
    );
\refclk_stable_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_6\,
      Q => refclk_stable_count_reg(9),
      R => '0'
    );
refclk_stable_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \refclk_stable_count[0]_i_7_n_0\,
      I1 => refclk_stable_i_2_n_0,
      I2 => refclk_stable_i_3_n_0,
      I3 => refclk_stable_i_4_n_0,
      I4 => refclk_stable_i_5_n_0,
      I5 => refclk_stable_i_6_n_0,
      O => refclk_stable_i_1_n_0
    );
refclk_stable_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001000000000000"
    )
        port map (
      I0 => refclk_stable_count_reg(4),
      I1 => refclk_stable_count_reg(5),
      I2 => refclk_stable_count_reg(2),
      I3 => refclk_stable_count_reg(3),
      I4 => refclk_stable_count_reg(7),
      I5 => refclk_stable_count_reg(6),
      O => refclk_stable_i_2_n_0
    );
refclk_stable_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000200000000000"
    )
        port map (
      I0 => refclk_stable_count_reg(10),
      I1 => refclk_stable_count_reg(11),
      I2 => refclk_stable_count_reg(8),
      I3 => refclk_stable_count_reg(9),
      I4 => refclk_stable_count_reg(12),
      I5 => refclk_stable_count_reg(13),
      O => refclk_stable_i_3_n_0
    );
refclk_stable_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => refclk_stable_count_reg(16),
      I1 => refclk_stable_count_reg(17),
      I2 => refclk_stable_count_reg(14),
      I3 => refclk_stable_count_reg(15),
      I4 => refclk_stable_count_reg(18),
      I5 => refclk_stable_count_reg(19),
      O => refclk_stable_i_4_n_0
    );
refclk_stable_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => refclk_stable_count_reg(22),
      I1 => refclk_stable_count_reg(23),
      I2 => refclk_stable_count_reg(20),
      I3 => refclk_stable_count_reg(21),
      I4 => refclk_stable_count_reg(25),
      I5 => refclk_stable_count_reg(24),
      O => refclk_stable_i_5_n_0
    );
refclk_stable_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => refclk_stable_count_reg(28),
      I1 => refclk_stable_count_reg(29),
      I2 => refclk_stable_count_reg(26),
      I3 => refclk_stable_count_reg(27),
      I4 => refclk_stable_count_reg(31),
      I5 => refclk_stable_count_reg(30),
      O => refclk_stable_i_6_n_0
    );
refclk_stable_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => refclk_stable_i_1_n_0,
      Q => refclk_stable_reg_n_0,
      R => '0'
    );
\reset_time_out_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"440000FF50505050"
    )
        port map (
      I0 => tx_state(3),
      I1 => txresetdone_s3,
      I2 => init_wait_done_reg_n_0,
      I3 => tx_state(1),
      I4 => tx_state(2),
      I5 => tx_state(0),
      O => \reset_time_out_i_2__0_n_0\
    );
reset_time_out_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => sync_cplllock_n_0,
      Q => reset_time_out,
      R => \out\(0)
    );
run_phase_alignment_int_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB0002"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      I2 => tx_state(2),
      I3 => tx_state(1),
      I4 => run_phase_alignment_int_reg_n_0,
      O => run_phase_alignment_int_i_1_n_0
    );
run_phase_alignment_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => run_phase_alignment_int_i_1_n_0,
      Q => run_phase_alignment_int_reg_n_0,
      R => \out\(0)
    );
run_phase_alignment_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => '1',
      D => data_out,
      Q => run_phase_alignment_int_s3,
      R => '0'
    );
sync_TXRESETDONE: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_4
     port map (
      data_out => txresetdone_s2,
      data_sync_reg1_0 => data_sync_reg1_0,
      independent_clock_bufg => independent_clock_bufg
    );
sync_cplllock: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_5
     port map (
      E(0) => sync_cplllock_n_1,
      \FSM_sequential_tx_state_reg[0]\ => \FSM_sequential_tx_state[3]_i_3_n_0\,
      \FSM_sequential_tx_state_reg[0]_0\ => \FSM_sequential_tx_state[3]_i_4_n_0\,
      \FSM_sequential_tx_state_reg[0]_1\ => \FSM_sequential_tx_state[3]_i_6_n_0\,
      \FSM_sequential_tx_state_reg[0]_2\ => time_out_2ms_reg_n_0,
      \FSM_sequential_tx_state_reg[0]_3\ => \FSM_sequential_tx_state[3]_i_7_n_0\,
      \FSM_sequential_tx_state_reg[0]_4\ => pll_reset_asserted_reg_n_0,
      \FSM_sequential_tx_state_reg[0]_5\ => refclk_stable_reg_n_0,
      \FSM_sequential_tx_state_reg[0]_6\ => \FSM_sequential_tx_state[0]_i_3_n_0\,
      Q(3 downto 0) => tx_state(3 downto 0),
      data_sync_reg1_0 => data_sync_reg1_2,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_lock_reclocked => mmcm_lock_reclocked,
      reset_time_out => reset_time_out,
      reset_time_out_reg => sync_cplllock_n_0,
      reset_time_out_reg_0 => \reset_time_out_i_2__0_n_0\,
      reset_time_out_reg_1 => init_wait_done_reg_n_0
    );
sync_mmcm_lock_reclocked: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_6
     port map (
      SR(0) => sync_mmcm_lock_reclocked_n_0,
      data_out => mmcm_lock_i,
      data_sync_reg1_0 => data_sync_reg1_1,
      independent_clock_bufg => independent_clock_bufg
    );
sync_run_phase_alignment_int: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_7
     port map (
      data_in => run_phase_alignment_int_reg_n_0,
      data_out => data_out,
      data_sync_reg6_0 => data_sync_reg1
    );
sync_time_out_wait_bypass: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_8
     port map (
      data_in => time_out_wait_bypass_reg_n_0,
      data_out => time_out_wait_bypass_s2,
      independent_clock_bufg => independent_clock_bufg
    );
sync_tx_fsm_reset_done_int: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_9
     port map (
      data_in => \^data_in\,
      data_out => tx_fsm_reset_done_int_s2,
      data_sync_reg1_0 => data_sync_reg1
    );
time_out_2ms_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AE"
    )
        port map (
      I0 => time_out_2ms_reg_n_0,
      I1 => \time_out_2ms_i_2__0_n_0\,
      I2 => time_out_2ms_i_3_n_0,
      I3 => reset_time_out,
      O => time_out_2ms_i_1_n_0
    );
\time_out_2ms_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000800"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(18),
      I2 => time_out_counter_reg(10),
      I3 => time_out_counter_reg(12),
      I4 => time_out_counter_reg(5),
      I5 => time_tlock_max_i_3_n_0,
      O => \time_out_2ms_i_2__0_n_0\
    );
time_out_2ms_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => time_out_counter_reg(7),
      I1 => time_out_counter_reg(14),
      I2 => \time_out_2ms_i_4__0_n_0\,
      I3 => time_out_2ms_i_5_n_0,
      O => time_out_2ms_i_3_n_0
    );
\time_out_2ms_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => time_out_counter_reg(4),
      I1 => time_out_counter_reg(3),
      I2 => time_out_counter_reg(8),
      I3 => time_out_counter_reg(6),
      O => \time_out_2ms_i_4__0_n_0\
    );
time_out_2ms_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEF"
    )
        port map (
      I0 => time_out_counter_reg(0),
      I1 => time_out_counter_reg(13),
      I2 => time_out_counter_reg(9),
      I3 => time_out_counter_reg(2),
      I4 => time_out_counter_reg(1),
      O => time_out_2ms_i_5_n_0
    );
time_out_2ms_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_2ms_i_1_n_0,
      Q => time_out_2ms_reg_n_0,
      R => '0'
    );
time_out_500us_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AAAAEAAA"
    )
        port map (
      I0 => time_out_500us_reg_n_0,
      I1 => time_out_500us_i_2_n_0,
      I2 => time_out_counter_reg(5),
      I3 => time_out_counter_reg(10),
      I4 => time_out_2ms_i_3_n_0,
      I5 => reset_time_out,
      O => time_out_500us_i_1_n_0
    );
time_out_500us_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000008"
    )
        port map (
      I0 => time_out_counter_reg(15),
      I1 => time_out_counter_reg(16),
      I2 => time_out_counter_reg(11),
      I3 => time_out_counter_reg(12),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(17),
      O => time_out_500us_i_2_n_0
    );
time_out_500us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_500us_i_1_n_0,
      Q => time_out_500us_reg_n_0,
      R => '0'
    );
\time_out_counter[0]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFBFFFF"
    )
        port map (
      I0 => time_tlock_max_i_3_n_0,
      I1 => \time_out_counter[0]_i_3__0_n_0\,
      I2 => time_out_2ms_i_3_n_0,
      I3 => time_out_counter_reg(10),
      I4 => time_out_counter_reg(12),
      I5 => time_out_counter_reg(5),
      O => time_out_counter
    );
\time_out_counter[0]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(18),
      O => \time_out_counter[0]_i_3__0_n_0\
    );
\time_out_counter[0]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(0),
      O => \time_out_counter[0]_i_4_n_0\
    );
\time_out_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_7\,
      Q => time_out_counter_reg(0),
      R => reset_time_out
    );
\time_out_counter_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \time_out_counter_reg[0]_i_2_n_0\,
      CO(2) => \time_out_counter_reg[0]_i_2_n_1\,
      CO(1) => \time_out_counter_reg[0]_i_2_n_2\,
      CO(0) => \time_out_counter_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \time_out_counter_reg[0]_i_2_n_4\,
      O(2) => \time_out_counter_reg[0]_i_2_n_5\,
      O(1) => \time_out_counter_reg[0]_i_2_n_6\,
      O(0) => \time_out_counter_reg[0]_i_2_n_7\,
      S(3 downto 1) => time_out_counter_reg(3 downto 1),
      S(0) => \time_out_counter[0]_i_4_n_0\
    );
\time_out_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_5\,
      Q => time_out_counter_reg(10),
      R => reset_time_out
    );
\time_out_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_4\,
      Q => time_out_counter_reg(11),
      R => reset_time_out
    );
\time_out_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_7\,
      Q => time_out_counter_reg(12),
      R => reset_time_out
    );
\time_out_counter_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[8]_i_1_n_0\,
      CO(3) => \time_out_counter_reg[12]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[12]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[12]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[12]_i_1_n_4\,
      O(2) => \time_out_counter_reg[12]_i_1_n_5\,
      O(1) => \time_out_counter_reg[12]_i_1_n_6\,
      O(0) => \time_out_counter_reg[12]_i_1_n_7\,
      S(3 downto 0) => time_out_counter_reg(15 downto 12)
    );
\time_out_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_6\,
      Q => time_out_counter_reg(13),
      R => reset_time_out
    );
\time_out_counter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_5\,
      Q => time_out_counter_reg(14),
      R => reset_time_out
    );
\time_out_counter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_4\,
      Q => time_out_counter_reg(15),
      R => reset_time_out
    );
\time_out_counter_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1_n_7\,
      Q => time_out_counter_reg(16),
      R => reset_time_out
    );
\time_out_counter_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[12]_i_1_n_0\,
      CO(3 downto 2) => \NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \time_out_counter_reg[16]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED\(3),
      O(2) => \time_out_counter_reg[16]_i_1_n_5\,
      O(1) => \time_out_counter_reg[16]_i_1_n_6\,
      O(0) => \time_out_counter_reg[16]_i_1_n_7\,
      S(3) => '0',
      S(2 downto 0) => time_out_counter_reg(18 downto 16)
    );
\time_out_counter_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1_n_6\,
      Q => time_out_counter_reg(17),
      R => reset_time_out
    );
\time_out_counter_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1_n_5\,
      Q => time_out_counter_reg(18),
      R => reset_time_out
    );
\time_out_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_6\,
      Q => time_out_counter_reg(1),
      R => reset_time_out
    );
\time_out_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_5\,
      Q => time_out_counter_reg(2),
      R => reset_time_out
    );
\time_out_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_4\,
      Q => time_out_counter_reg(3),
      R => reset_time_out
    );
\time_out_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_7\,
      Q => time_out_counter_reg(4),
      R => reset_time_out
    );
\time_out_counter_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[0]_i_2_n_0\,
      CO(3) => \time_out_counter_reg[4]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[4]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[4]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[4]_i_1_n_4\,
      O(2) => \time_out_counter_reg[4]_i_1_n_5\,
      O(1) => \time_out_counter_reg[4]_i_1_n_6\,
      O(0) => \time_out_counter_reg[4]_i_1_n_7\,
      S(3 downto 0) => time_out_counter_reg(7 downto 4)
    );
\time_out_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_6\,
      Q => time_out_counter_reg(5),
      R => reset_time_out
    );
\time_out_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_5\,
      Q => time_out_counter_reg(6),
      R => reset_time_out
    );
\time_out_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_4\,
      Q => time_out_counter_reg(7),
      R => reset_time_out
    );
\time_out_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_7\,
      Q => time_out_counter_reg(8),
      R => reset_time_out
    );
\time_out_counter_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[4]_i_1_n_0\,
      CO(3) => \time_out_counter_reg[8]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[8]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[8]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[8]_i_1_n_4\,
      O(2) => \time_out_counter_reg[8]_i_1_n_5\,
      O(1) => \time_out_counter_reg[8]_i_1_n_6\,
      O(0) => \time_out_counter_reg[8]_i_1_n_7\,
      S(3 downto 0) => time_out_counter_reg(11 downto 8)
    );
\time_out_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_6\,
      Q => time_out_counter_reg(9),
      R => reset_time_out
    );
time_out_wait_bypass_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AB00"
    )
        port map (
      I0 => time_out_wait_bypass_reg_n_0,
      I1 => tx_fsm_reset_done_int_s3,
      I2 => time_out_wait_bypass_i_2_n_0,
      I3 => run_phase_alignment_int_s3,
      O => time_out_wait_bypass_i_1_n_0
    );
time_out_wait_bypass_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEFFFFFFFFF"
    )
        port map (
      I0 => time_out_wait_bypass_i_3_n_0,
      I1 => time_out_wait_bypass_i_4_n_0,
      I2 => wait_bypass_count_reg(5),
      I3 => wait_bypass_count_reg(13),
      I4 => wait_bypass_count_reg(11),
      I5 => time_out_wait_bypass_i_5_n_0,
      O => time_out_wait_bypass_i_2_n_0
    );
time_out_wait_bypass_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => wait_bypass_count_reg(16),
      I1 => wait_bypass_count_reg(9),
      I2 => wait_bypass_count_reg(12),
      I3 => wait_bypass_count_reg(10),
      O => time_out_wait_bypass_i_3_n_0
    );
time_out_wait_bypass_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => wait_bypass_count_reg(4),
      I1 => wait_bypass_count_reg(15),
      I2 => wait_bypass_count_reg(6),
      I3 => wait_bypass_count_reg(0),
      O => time_out_wait_bypass_i_4_n_0
    );
time_out_wait_bypass_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4000000000000000"
    )
        port map (
      I0 => wait_bypass_count_reg(8),
      I1 => wait_bypass_count_reg(1),
      I2 => wait_bypass_count_reg(7),
      I3 => wait_bypass_count_reg(14),
      I4 => wait_bypass_count_reg(2),
      I5 => wait_bypass_count_reg(3),
      O => time_out_wait_bypass_i_5_n_0
    );
time_out_wait_bypass_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => '1',
      D => time_out_wait_bypass_i_1_n_0,
      Q => time_out_wait_bypass_reg_n_0,
      R => '0'
    );
time_out_wait_bypass_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_wait_bypass_s2,
      Q => time_out_wait_bypass_s3,
      R => '0'
    );
time_tlock_max_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AAAAAAEA"
    )
        port map (
      I0 => time_tlock_max_reg_n_0,
      I1 => time_tlock_max_i_2_n_0,
      I2 => time_out_counter_reg(5),
      I3 => time_tlock_max_i_3_n_0,
      I4 => time_tlock_max_i_4_n_0,
      I5 => reset_time_out,
      O => time_tlock_max_i_1_n_0
    );
time_tlock_max_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => time_out_counter_reg(14),
      I1 => time_out_counter_reg(12),
      I2 => time_out_counter_reg(10),
      I3 => time_out_counter_reg(7),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(17),
      O => time_tlock_max_i_2_n_0
    );
time_tlock_max_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(15),
      I2 => time_out_counter_reg(11),
      O => time_tlock_max_i_3_n_0
    );
time_tlock_max_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => time_out_2ms_i_5_n_0,
      I1 => time_out_counter_reg(6),
      I2 => time_out_counter_reg(8),
      I3 => time_out_counter_reg(3),
      I4 => time_out_counter_reg(4),
      O => time_tlock_max_i_4_n_0
    );
time_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_tlock_max_i_1_n_0,
      Q => time_tlock_max_reg_n_0,
      R => '0'
    );
tx_fsm_reset_done_int_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF1000"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(0),
      I3 => tx_state(3),
      I4 => \^data_in\,
      O => tx_fsm_reset_done_int_i_1_n_0
    );
tx_fsm_reset_done_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => tx_fsm_reset_done_int_i_1_n_0,
      Q => \^data_in\,
      R => \out\(0)
    );
tx_fsm_reset_done_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => '1',
      D => tx_fsm_reset_done_int_s2,
      Q => tx_fsm_reset_done_int_s3,
      R => '0'
    );
txresetdone_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => txresetdone_s2,
      Q => txresetdone_s3,
      R => '0'
    );
\wait_bypass_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => run_phase_alignment_int_s3,
      O => clear
    );
\wait_bypass_count[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_wait_bypass_i_2_n_0,
      I1 => tx_fsm_reset_done_int_s3,
      O => \wait_bypass_count[0]_i_2_n_0\
    );
\wait_bypass_count[0]_i_4__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_bypass_count_reg(0),
      O => \wait_bypass_count[0]_i_4__0_n_0\
    );
\wait_bypass_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_7\,
      Q => wait_bypass_count_reg(0),
      R => clear
    );
\wait_bypass_count_reg[0]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \wait_bypass_count_reg[0]_i_3_n_0\,
      CO(2) => \wait_bypass_count_reg[0]_i_3_n_1\,
      CO(1) => \wait_bypass_count_reg[0]_i_3_n_2\,
      CO(0) => \wait_bypass_count_reg[0]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \wait_bypass_count_reg[0]_i_3_n_4\,
      O(2) => \wait_bypass_count_reg[0]_i_3_n_5\,
      O(1) => \wait_bypass_count_reg[0]_i_3_n_6\,
      O(0) => \wait_bypass_count_reg[0]_i_3_n_7\,
      S(3 downto 1) => wait_bypass_count_reg(3 downto 1),
      S(0) => \wait_bypass_count[0]_i_4__0_n_0\
    );
\wait_bypass_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_5\,
      Q => wait_bypass_count_reg(10),
      R => clear
    );
\wait_bypass_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_4\,
      Q => wait_bypass_count_reg(11),
      R => clear
    );
\wait_bypass_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_7\,
      Q => wait_bypass_count_reg(12),
      R => clear
    );
\wait_bypass_count_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[8]_i_1_n_0\,
      CO(3) => \wait_bypass_count_reg[12]_i_1_n_0\,
      CO(2) => \wait_bypass_count_reg[12]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[12]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[12]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[12]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[12]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[12]_i_1_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(15 downto 12)
    );
\wait_bypass_count_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_6\,
      Q => wait_bypass_count_reg(13),
      R => clear
    );
\wait_bypass_count_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_5\,
      Q => wait_bypass_count_reg(14),
      R => clear
    );
\wait_bypass_count_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_4\,
      Q => wait_bypass_count_reg(15),
      R => clear
    );
\wait_bypass_count_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[16]_i_1_n_7\,
      Q => wait_bypass_count_reg(16),
      R => clear
    );
\wait_bypass_count_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[12]_i_1_n_0\,
      CO(3 downto 0) => \NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED\(3 downto 1),
      O(0) => \wait_bypass_count_reg[16]_i_1_n_7\,
      S(3 downto 1) => B"000",
      S(0) => wait_bypass_count_reg(16)
    );
\wait_bypass_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_6\,
      Q => wait_bypass_count_reg(1),
      R => clear
    );
\wait_bypass_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_5\,
      Q => wait_bypass_count_reg(2),
      R => clear
    );
\wait_bypass_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_4\,
      Q => wait_bypass_count_reg(3),
      R => clear
    );
\wait_bypass_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_7\,
      Q => wait_bypass_count_reg(4),
      R => clear
    );
\wait_bypass_count_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[0]_i_3_n_0\,
      CO(3) => \wait_bypass_count_reg[4]_i_1_n_0\,
      CO(2) => \wait_bypass_count_reg[4]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[4]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[4]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[4]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[4]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[4]_i_1_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(7 downto 4)
    );
\wait_bypass_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_6\,
      Q => wait_bypass_count_reg(5),
      R => clear
    );
\wait_bypass_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_5\,
      Q => wait_bypass_count_reg(6),
      R => clear
    );
\wait_bypass_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_4\,
      Q => wait_bypass_count_reg(7),
      R => clear
    );
\wait_bypass_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_7\,
      Q => wait_bypass_count_reg(8),
      R => clear
    );
\wait_bypass_count_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[4]_i_1_n_0\,
      CO(3) => \wait_bypass_count_reg[8]_i_1_n_0\,
      CO(2) => \wait_bypass_count_reg[8]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[8]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[8]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[8]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[8]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[8]_i_1_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(11 downto 8)
    );
\wait_bypass_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_6\,
      Q => wait_bypass_count_reg(9),
      R => clear
    );
\wait_time_cnt[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      O => wait_time_cnt0(0)
    );
\wait_time_cnt[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      I1 => wait_time_cnt_reg(1),
      O => \wait_time_cnt[1]_i_1_n_0\
    );
\wait_time_cnt[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => wait_time_cnt_reg(1),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(2),
      O => \wait_time_cnt[2]_i_1_n_0\
    );
\wait_time_cnt[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE01"
    )
        port map (
      I0 => wait_time_cnt_reg(2),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(1),
      I3 => wait_time_cnt_reg(3),
      O => \wait_time_cnt[3]_i_1_n_0\
    );
\wait_time_cnt[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0001"
    )
        port map (
      I0 => wait_time_cnt_reg(3),
      I1 => wait_time_cnt_reg(1),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(2),
      I4 => wait_time_cnt_reg(4),
      O => \wait_time_cnt[4]_i_1_n_0\
    );
\wait_time_cnt[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000001"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[5]_i_1_n_0\
    );
\wait_time_cnt[6]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0700"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(3),
      I3 => tx_state(0),
      O => wait_time_cnt0_0
    );
\wait_time_cnt[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => sel
    );
\wait_time_cnt[6]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => \wait_time_cnt[6]_i_3_n_0\
    );
\wait_time_cnt[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[6]_i_4_n_0\
    );
\wait_time_cnt_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => wait_time_cnt0(0),
      Q => wait_time_cnt_reg(0),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[1]_i_1_n_0\,
      Q => wait_time_cnt_reg(1),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[2]_i_1_n_0\,
      Q => wait_time_cnt_reg(2),
      S => wait_time_cnt0_0
    );
\wait_time_cnt_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[3]_i_1_n_0\,
      Q => wait_time_cnt_reg(3),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[4]_i_1_n_0\,
      Q => wait_time_cnt_reg(4),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[5]_i_1_n_0\,
      Q => wait_time_cnt_reg(5),
      S => wait_time_cnt0_0
    );
\wait_time_cnt_reg[6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[6]_i_3_n_0\,
      Q => wait_time_cnt_reg(6),
      S => wait_time_cnt0_0
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2023.2"
`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
TCSZ2Ye5IRfHavlF8Mno1PI9xixWuSiNh3ssU1FQtkjW1fmNtc2c3x12slL242UQayI0rzZTqe6S
edtecLHTOnzxXpCZjjU8NFmgLPerTSDZ1W5YhyIi9j0Ap4YBpvaA1ojM0+r0Cx+dMOXohQGeyljq
+fnTaFTUe2678DxpqHk=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
NIzZVKMB1/8UX2qb4BB+VXCafEKXsrn7axB1cJDPqDCzSyt/2KG1NEEZTDHZVzIr8Bf9501PyXmL
VowTAAXX/RopKyKOM1xJN/qLtqXxegH2a4dIkUxDIIclIcbv/smna9VCwI7m6JhrnKsNciTTilgR
27S/h6JPpZsZAEmsNxxTC70WQhQSM8TlHJjZg3KDc5KTnvC/mVTk6I05U6x0Bdd1YR9GBvhwRqhP
B1ukL/1JVOwR9Ce9p+EHFE/xyApypCjQPGwq+8IFQgS8wltVZHX6eSMw17Q0wGCY+LHduRTA+abV
LvAR0NPf7PKQUSCECe2mBbLPO7wD4BO5RAkJeA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
udw4XxxzcaDNM1qWnRgZ2JEM1MMqnKwKVqha/krU9EyUAsyATjQEMBqjlOHw5QXMU2jjizlL20Nl
h2pF7iKo1S+7TS54Y/UIJANp+Dl46V/qfy6/yBnE4YclHON1k0jRao4C6T951tgXuCAIQEmXbr87
aJfL2dNqORH+TDKUBdc=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
JFe89rPDaiIUicPDNoXCg8pJUoYRyVDoW/5yE4T0Cp9xDHtuIyKQVbC7jVb92OsgJ5GHDm7DH2D2
rYZKrdCIqPt2jo7DG6bcJuDFcisZb11HLlYWNsK2Vqs9DdsTPViykeE05CD5AgfDxb983x8F1meK
w8zjeGoD44djsaRA+lvP1zLhl24q5LWFJdPSyIT7uWZwhxHqlyJu85ToXLuwZQZO76Mp+1mitxDy
vleizC5rnk/4hqxfEFS21Qi1TwCz5hdU+H3nA3dTe1KRY+obbFP7sRWKfmr9Rcf9enRvbaEbLoJA
9ADkl72jc1Aqlnd+YCGq4EmbElbWLxblpamncA==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IV5qOrW4jXfiGt6hz/YcNm+/H3ij0v503CF3Kvut91tUWldzNzyLt4pIZEWEzSmn6RcpcLNN88po
1kt45UdSBz+mL5HDQaw4J+VGD/cCBmW1jnOclCf82kwju1MIDfa2EKicjqaykCUROxV7cwg07FFp
clLfIwd4kxgSWnGzeZi1IGezx7OpBsAkBTz9ha4WttEm0+D29DF9O4GaQl6q8IBeA0QIrO10EESt
slfRi2evxdOeTZBVFoXU91OszneH/prZqyCsHeyvTa8PABTZ+Y4CH6ICZCXRn7QTNJgoYSGABuPs
87saNJgzomjyaO6IzGl1fBgMIsIurKw90DE8Jw==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2022_10", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Eek/EeBa5kEiakvxzHC3fZ0NXCsvWnLN8FYKLyImepfBUv2jdVDY2j6Qs928DJaMO8pBuO9SGcu3
47rhcN1DAjZza5Ac83W24fngY1+YWblivVc8AoggXS1t2Y7Dy1vf9+ZxUdOvq63sje+fDJxapZwK
3HQGdtBX86RTaUS5K+HyI1FTmcIhUYmJWmxQjIxLla7FF1QZ4XpTCfqAG5i7ZKlYSoDFb8sjCRG4
XWFuk1dbL2UfZPxXZ7XHIm+03Ck/JsHtsjLCc8oTB/9MLom2HX9SjX8H6tFbEXR1NatCFWQ04JKL
kHSYD/xDlwjhN9CRvowRhNJaYSmKQT646hlNoA==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
gaID+cgqOdyoJPDKM9JAyimEjoxNakxuNjYf52N14HEyn4NQksF7thq/bXWc82vmfdc8aodx1+ky
i8uuKszW1WwV+apGSqk7YXBCxx3ACfMsPzNzeDQ2HVzGfznpQD80Eu7I7iwtz3k5Mr31iaeM1kQa
oddk6CkVESI8CD21PQHMVeu0LKLZJp8k8NHf3i0UOXsP5o768iecieYQh2VYXZ6HORDDyd+IpDB1
CAFBZctXco8C1w74wCB0LXUSYInc5ythxBURkPPTJ1GBuXpoQGZD2sNiI2Htl0y1toEdfgExWZ+0
+4Docnd9TgOGhAhZzUcj3c+6cQNbgCB847/G6w==

`protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`protect key_block
tEBTlBoXowC0cOrrCu9pEZ6t9FjMS6GWThbzsXMvR2xb9HBcccdvXT7EfASM09KkNzvlYoyNBWct
0TRl1BNzzlomu3G857U6kezS+CCRF/K2qOhhxHFxEfuM0qblRVdNHoCGGMM4PkE/rt9M7IqYoXQg
WOHI1ydpZZn08aVL9QYJgz6ZuVHNLwSpL6rjFFDXV1cB82gVFBkRP/0NxpGW2WH6YA/MJ0czV0ji
o0umOWluEwUObdytKX1lfuNYimI0ziWrovqq2osL8J7NBKDUl2R6gJ51DObsTBgC8uyUHVibyNHy
nhzTpwcBeeXdtAueCg1BlHDIwglcMUdy0sBZEyHM/CLzpxgr1A+uUcmzlWx1drrc8lRNwGMFDDJQ
9OzoHBABtNt8N3bbO8A+rE9HtsjMVr1TxHhUTxBhWcypwra+xzsGykln/IP3JBwwQR0+d+V8/Vec
5Bh03crJTvJZUbYidozNoaPOfnHi0NxFDNdL7L1i75T+H6bqeE1ADR/4

`protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GM0AtcDH85MjCjnA/pijf0ZCJap8X+OYUX7W1StOwznqG2XPd9DhtvTyqD0c8/7BTdeCzGUK5iqe
QiGwEcy1dCrSVZW0KtjFXllkYV2ai3/Qn7Bgg1YuzxifEFKe6ClTsByfgjqRdyZeNSAldwvx9ZtT
0ZhijV96K37zXwfXFeKDmxOZOV553ovWfXGekaS1EPmSluoDYBMQKc2XV+ZUXR7n5NI/6E3QdK7K
utsZyrFYyJdYW8Po28hQf1nWeQP6+PxQB6wi/P6sUzudntNcQ7uLRr4PTz6twPPqYwUF+7YW8baL
p/2EFPf8y6fBb+DOBCnzmGZvmq+M2qQot14r+g==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Ot2lizvbpt8emXxoZl10oi4H/aTQTzrHDg0mf7DDs3BS7iyFsRmaQCG/BRF/mwjlw+EbST4/x/DJ
d6Hf6LIa6mllnMD6G+uVHQ1z31eASHPw3m+WnPMr/zCNuebPcDitgiXWmq3SAS9byYvxiwcDvjn6
CMh89pvlx8xLiFUoo5j/lAPe4cPBJwSMleQLLB989s6rByi0lVW0QiLTzakaB8DHBMvhIYEfi74m
Lxby6+nYRGrAUKPOemP0Ag/LW83Eup/Wa0jVOtxzlj3foiYhg2mWCt2zyFhgQsDA+oEsDa/KZc0F
OUzOI8vFDrwPmYRwd2ejFI9Nz3/1mb05VQmDRA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
OamBwohUIPOIa5bsJvu3upqnGl4f3jNYS35bg4S059C9qVDgQJcw/it81deNA5zFvzX3Cq9CiO9K
zl02VsdpFgNPjSwEO8F47LJZ4fHx99EmESBogsNwUNitzkuYTLCb7F7ZF2WSJExQ0KsYt+TRp2UH
yQEvpM1lHQYUXxzjw08qUI5ssSnOsQFydvP8BwA/6aGrVJ+LuEgPVdMqLBn0EeAmRsynxJ7OhPGV
DlvHQwtVuBrkvjQHED/Ye43ZIeWPm/xOcjNfZjYeOvdEJqTbaviR0Fo1LFx8EX80uvdFeK55ywDN
wzoZ4cVH87f6VwR1xHdo0JpVxrajZpSZ0jPudw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 59392)
`protect data_block
Rz1ZuXu+08StTOUttvRdzFvV0kIzl8YJvYNWWsnt4fgBoB3S0R4Ma7zLz0sbk/FeiU2Morj9ZyFh
+LGXeTLGOyYFj/Mp0hr6UC3hU3SLKJyJOyBLLpHOJRt1aYfpdczoFFPy8JXBvyWSFC7JOndJ+Uk6
eEsiddzIENDh67uFqvlbBvLCpydBddDsc856whpwoRqq6tbjbwgHjzhU2uWB/0m4HVkGTqkh+tVb
A852bCVup9csBHPPyrD2SnkJfydeXZlOEGm4rGc//td9qz+ksHWgCi/6rUE0l7cmRxYBmANNzKZZ
CEWQDQHYcHBmzjzUhlC3W+ohOe310jp9QmGv7WclcORY0DZUvFkYlg0cl+8vC1rTkGL5gDNSWNEw
yN+aX9Nlf2fx41lw6QojEqiHCizrwgIZoYvaOi6B4ZqvZpzKqS/UUoFrGTDZJrUMrLRBd5QbYJ3P
8AQ6q0oLRhOIo9zW8zqQBNXzxL/4uAYqvCWmhZHtSBbrkYaAdEYtY41B6crK2ts+puWxuIdMtDpl
cXFoj4MRWCJnIOhqyu7z9A+l7qvkbLM8Ys3sNpXvHLxpKxVTH6NupU43lIi69mbB7BT+rUfBNnSE
+8eHaMEwG+12QOsQo5+tC8CJQRu7zZfCJydpwh0hQXhxgYthWDWtm6zytVgpsavjsvstvGZD8S5m
nDDOBLCI9xltc19HnhBclI4LyvTcHqOMjbWxWX+k5ICxh58oHSSXgU2LfYDJkwdFeKA0okrYBDMt
B5tmCd1UP5KzJ9tNWVTlYO2DsIf7WrE4EepNFPWTcxID2+aREGNdiGVGXBUnrzb9nr3u49QTNkQG
1A6G3Rt1aSMDd/hjubr/joijGQ91Dq6i5FbZJ2VDEv04c9WE7GvtjhpFQYFHxJa8oLcvgeHa5OWs
PUigo7xV7wfxhPVGjgOb1wJq87DndeaO/7jccs9GfRw1fgNbvz6Wqru3YIkejrknMed/k+6YhxsI
l55F8l1hOl6ycuQAg8iVc4H4pSNkmAUQjLTxeBBOCKUNr1XKVIbjEIewafIgwo3fV/nKyCo0wLUa
mRcqVKZqjc4/0kzaYRhzt8nsf/Tl43klQHSMddKOThIu1vEf2crFEr/mNdJQ8BFY8sP3wRE9tYWs
ros/2T1ziwFuiP2pWKltfgArotH4EbouEY1RSYpBoE2/kImgnN4JRqpaVPsnC1Hm75vvC46d98d2
UaXhIEgXWabk1jCnPyK37mpdoY1B+U9Vu0FAMiQa5fjyOs4olm2f8FL/7UFBJjoVhJoFpqQU+fiv
1S7/mQGk7iubWE9qRohBGFm3SMWqLGBsMJDElSNTnB/1+4Uxul9n63mq3kTnlcw3unJBNEFPSknr
0PveMr3VbEzRLDf50UpJpZRe32ezUTF1gVoplbYlPPVn1fTMGZtaG+tQCQbpsQErqjwOQj5cZdLf
MMjfOYcKBLCHLCRaTl1ddggcX2y2Vo3hzapC+8CRN7pBRLek6VPCJE8oiFBVZe8hWtpD8VJhZHsk
xbUPl8IznuHL9zl1EfL88SXZQcxqg4cpR30JTjhYHvpJtU8b2Y5fp8KXfer9rMu5GfUN7rJODJIF
1oqdCz6nls3jKFEaflACOob94GFwaUQ1fbtY+/0kB/MYJzGSROcdO1p4vWEpboQcYR8xil7mtYl7
NL4oF+Q6S7Cxbu/feOadC4AA5r4867gGC8mPgbJKJNeAfT1uEeDuDLSacOq2xs7PaxTk+2gOyK8G
lNILKYjukprdjCfoqV0I6s90qL/Rzb7vWRq9A2OBhKLuexGW1OG0dBtipfH22R8nTFEddxJtvMgI
AInbhuEAL1jD/aK1OCZPegoR4t6f/XNQcTvgjQ8dO/OOD77kU8YvYUR2bSXv0FRnBn+fJ4zoeAFx
P/PFGl5pylWfWgfrnzihUExxOpu7mi5Dm7rvSWasKTWszgd6k0nyducLSL13N0ya4CbU+Lb9GxyI
0jkzS3xOuU2pRcOpx8x0aI6xG3+p6AquRhk83n6aygLXSh68Bv8ZEyZlvGCAprXSzSGCqcL9RR5V
geLjyNDxWObjJhpnqdk9O1kGIhcMhpEoPGPJjGfRjsE8WxHZ5NCSi3kclbYMUcsgZdT4yDCpaOnQ
wfcULrTemqS9qzOOm9KnTvPH31rmX5rUySNS+o6dkP/gioz6LMRI2MMZKhqXDyv6gA1WFIezEU+v
7mBvKpAgfTgT9/AHzUPDuksx/egg3xqVWneOl6wNgtvA2lYoMo6SFDmUGwOj9AizZzeYzZBQLyjS
lA76AcnzTHBM8S26+xdKA+Y36+UuVtCqu2QW3yDoKmQWJVrJ1tTNp8WxYetEp8C3/E0Jr3G8GbN1
Oax6yza6r3JaATcsE9FYdr35pBJ6bFoC9ehUNSFRkAvo1TcAhaf3tIRN8cP/hhMFV1iwcws6RxUA
BGrby0122TOir6CZqt67iZyO7e2FQZTQSnEv/Ac20cB/y01pRqIrdIVlv4ReJ3EIw8NHDjTOx0+3
8g/scXdfQ5rM2AWY9K2gfKoy7Y7SKzF5jJ0pUfuqUpc0/1ynAGLYPjtdSGBQPyB46EpSLOuXUClK
6OFgipXKDrePu9nUHmWMbplKb3xnMFihdfzdkSyEDUSqwaVTP67MM3JqBKPBuCg/HYtTY9z4faHH
pGUQPR1jc8ue/jhKhCHUON8kHWvuajfaFPO2K/4BGwjOsC1X+UDvyaf1ba6pNsEzBtF5ur0PaQ/S
6w1nOM/f0mawU6Xs2taxnT+dWSpQL2Ozncf5xxz8kAInl6j1ACB50QAkX7MSSttyLT/+y47sJDOK
pCu2eBWgKJsEa8S5a3fcAHsxFjA2s538NhucvOAG8k8kb18sDq1JxXPL3foCCKXSoENZgrfetPKX
7hPsRs6gpns1aeJgNf9AxmKU3hwKsudAaAzb6zi3Q8Fkw7YzcM8eBotqwthwr8tv1dvqe5Z3q148
H5iUcjaPU10Qn/On73ZGtjlGvVrxXZzvQUiElGwvohi9HHpDlI2DOO9q1ViNb/w+LQuIDuwmzSOo
nTluD/gXKhNf2pVPU5xBylXp6WXJAbgVhrBJHXm0zoHLa0t2s9/pjA2RMYTnP5uAsa5/jX+vmkcw
jozFiYdaFE9kpFqLzCVzVv4GCjQOSfGC8NWrRnShKe3ztAZgKjj+G0agFs92eY02XKaYoJtoX2gE
78KfHR9pyrzyRkBPm5FnGoWV8V+hjrTV21KzjBSv8ji5uJAXaZgW7xyGxGxTFaOiQRRRUtSo5vxf
u33UzPLh7kcmd08qW/3ZHG97Zp56r/wFOXoYEmYY+fuAs9jlUANy/erGG7jzXTCmKyuzlp//b51/
TKIx6908k6x/yLpVpdyZSgqjiACOSsU0cKc0I++FtohV0Ea2at4BcRUKZIu+i3FvlyguvJSMVT8H
N3UiI3NUpsKRC7KNfvyu7IIun3RQYjYQpzpLodzwEwKSc1RPDmdSYxB/stlRMT7Si/jqLkZxNonH
X9KCRDq4DIn7TQwBWNSgOI8c4TrpeqSa7a4foBOPXZ2EYs8DJ1NL24LBL0JffmWY6ftrhTMnfpVk
XjD8b2KcTEXE8v3vEFJfXOG1lrjqWstoXy8VfDeKX1MjmUwIBouy9k0faytK88gSEhcSHS+7LmZW
eO1A0o3QElBHk8wj5eLEu27Dr4drLc8CqfeUznpMasDTDVtyPZhAYBSEk0lB6iy61mucagxkk28w
fXRAhA5J/MnTq1N1fsVj9SFzM4n5PmLkJwU93haFq0GCzMHT6Is6e1hwTbMWhj9NKdsCayuuFqAi
rb3fBWBBoYQ2jFV01jAh2lXIRBUoAWz67D5+HUEvdteiYaSmbWD+xEklyjCz/PUK835SyjJdadjT
mjiTCFPpqs1sZF6Mm1sN31+QqINq2p7f9SLBRLJ3W033X5CO/2iCCCm5hB2yxbfEQtBdWNMPJ/G7
Kfv6WpGe1cq+I43xTssiLJDM6dnpCFogeJXWFKk8HoxfGTKHjvnjhJZrgpPD7yRXX8P4gaICGUfD
w8i2JqtXBe9SufDQMWXVfz6MQP7rNImotaqZU7RoSuhLSjcFe1ibFBPtJjDoE2+Vyo1weYGlpgo4
L2kufhfMHBz6iCsxeEWu/3gmVNQlo5vXGMjlfraulN5MPNPSKxxfkzZKKY5tmH6c76TFtqIp39OV
iIqkOVqIh9RzMFOlnAIrS1guVq41T0ZdxcQD/m1MJZq8c6TD7kUZI7xiPevLhknxkJ7YN1KXjJL0
MlsDjqZ9WaNHpUcR/l3FEU5iLgILf16JDn4grkjWqW178PlyuuPEQJSkNCKHzvtn4Fn30+dbYSST
Ku7VQqpuIPi8hM8bIAMHtv5KV4YFW3GhRmrwIi6ESgkL3CN5bTfDUnHpt9ClBjqj9CsxRlUnl5mv
Cg6kNi9lxG4Hha2NqKnqn39HpFj1fHtgy2MMWJiSFOTU+L1xA17KFAWQFmFPr4vMgbGwmqx1wAc6
JqSCLjaDxmhMj87AMZuDr2bRMG90jSKOdMxLUJDLAs1ZxM71N7/obAvvUSq/69tOWlEFvUBtlbTk
uhpmiq6lk8GJ/kL+0prYZ8k6JzAGFyoetU0fJFno5HkDiHfrqmJ+7BeGrIlIp3GhXFqGBuMDYn8f
PFcWk6AuQW4VAbXllHDiwnfM66E9zLpQHjTMV/loOsPNLL9AicctZCX9c4IuvVj9q/4GkgEuUy2L
7aCOGfTt5CUT+P23BbtDs+aCTDeArp606ahK5TSVt9GMQ2c/N+DRxJL6jpXIhWju5JsUBjA3LfUy
6k5vMVqmRcph+3989yHsJP4/8dZ/FUwzp/QlBvUAMIquWggJokJILaG+OrRWJoCyo+Ka+pWex+xs
s/HrSNJ4AD9cqRQ4ir9ctpbxtFKv8gJDD+eSQDgTZLShvp0VyDwXJ8HHcif6pG/T7NPAW7zuID+c
DjUbddf9vccGgxQGeY6Bn41WhzW37rNZDE3sWN4qdILlqgNJuJX1Hi1GkeXy0M3OahW28hpVFTak
87IiLijkVbBEnzzeyUyrHRm581EqDgbKGzcr9ZOQAPnri5cuwAYNV1tCkvEIDqu+UL4vjD4W+0kM
T/cauPERUcS8lvcvkrTb0uYH2tMm7AWPBsIfGz0WafzV1anhOBDjUeZkd7kGGPvnuB5xyAKXFRgO
wsJPzStHgdtdUD2fvw2sIC0LZ9HiwmV/v8ALTUoRiocVtyJ9B+7Br2kQQYfX7rkjRRSby2ACj43R
eueuDaBhfxU7zPOIitW/sBD6qA6tqTaG8vdfciX5GdvX6HC9C30auDz9UHJfyefPui9KAwkF7pXf
2OANJ9OG6m8Y4Kbk3IuctVcbXXjKT0vAaL+py4AUuAwLHZgXbhmnaIHuSrqN3k8e7dcBvZLF+r/n
FFYVLsxSc4S1wYmy3EGs0ylz1r82NKCAk3xe+CemNtv8WCb/LGoyB+/6Frf0kt+Cf9pvAk0tZTyF
C3XI1h73Tq3Hn1anRM7bHXV1H8GEkIFBITi2PmgHgL7N/lpfokGB19Rmo9KyKQPhR142XWhq4stT
7IiHIXdiGnxqehDWRMcz0jZJ1HceOd/k3CEjoJsv+WS3xRC9bKqlrOG5Zysrl+6eeWwWrwYAsRFn
dG9C90xDYj9K3Mv1ddnSDGhOmtEPChXmsL8YeSYnUb8yXBZXtldUWrjfF9YXaYtAENmmPG6yWsZ+
Zfi+l3dyP2j2W2vounryL0lpsyvmb6xmv6eArktsvkhGpbku7LDgaDHs5QxIQDeGx9YQfSsA708F
w9x0QeekI9cACA2VanN+VLY0ZIzxRwkYaHI5VHcGLtjXDyS/rKuWuZHbQOSVoiVZ3sqvC2eF8Kga
lcIg3+rFM3n+WNNxPau/UTSkWumtIIYV6zGKWhCsBY6bkg/3qn/kuIaqNKKhcoVk4bWZlw8Qo1Rp
7OKCekporGHSruCoKoqrV6PGqz4RGrBeAe/SlG4WQJM29ArcCWmQPTrIvHEmE0DorjRwW2feZst8
418tht/rgQrcplQNrWBaFnB3I6cKc1n6S4SRyrKj736957npe96wUrpRdLXGF9SgMOpmuBOKVJ8Z
DnGTMcrJzf4Dng16ivHdvLcsvot8czRUmcvOzDXkm8WXQgyxWS6LGcLStgU4e8HiIan4og0IumX2
IWMnFTpcb0EjPAI1koFS06WtKlwfUHIFt1XawIBwspETFbDJkdKxPbvavgSVP7I8pCE+u4pmzhk/
XEXG6EDH3QUDnZNsbdkbB8lA1ZSc2b6ogoscnsHZ00o48tcH8eddpUyKkk3zelPRdDdurOgOZ2dK
fPSAxsX9ar32jxkzj3XSFwNXp0GPvFqx6/uBMqswSpuCweSwSBs+Cd2EcJMK9cR2aQthyf9Vtuq7
xAlNgZsvWjPfst6TAocr5W3gwwtARqFPVqx7RErGnznFqwZa1cx6Ndngv+qd3F6sTXlRUz3bGfgQ
r+CoK1feqAUbPcY0NLx+9d0zYApN5/4rt1a+zcGBGUq1NENDxef9Dcee0aHMTa4ueoYDXWjmLJop
ZOR/3F0xoFnokLHg46LtrIK1+nTeo6dQgIkqoxnQWB04lYam9WKNpO6uGlZdz1YpTysG9AuKe99+
ZLyxrnSjhL22j+uOI5sjulE6BPk5kY16XRn9iKjlS8ApNxUYIWsBjL+PoYcZ68NiEqhOF4KM20br
Cxd1Aea8To0hMn3Ifj1aqNUQdGqIH3iZPM8s6I3BFu/hvuz7GsdGocqUMqC1E/mFbYYW9YVGMpYQ
NfBMCBvqIy2ZMPk9tIJV68+6WdYjqY0DFV4dc1FtnxCjGMZQVGIA68S7PWdw1ZdpxSKIUVywOeNa
ASxfMiGMl/ch0JzWCymQanYucpWLUOWccEgSVnlc2+INwiENZc8/OZW8s83L3l2sphkyhjUGR/Q4
JK4S+J64BDK7YMcNBuh4I0hBUSxTXzXQ9v68BvGNNlP+DPAjYEYHqTagIOXEyLEeXARPj+nla+6L
Zz4JAo1VJs9XovIVnWCr997q/iHnr1JtmV23b031KfFQyKXbMExQnNRBLbDvdDq3S33zRF5EvVpZ
0c09UtqkzRd+8z8fdGWNrOWbXGUhaMBRA61r1z56bgmXX7LaeqD6AnejBd00ssdf/lfr7wvn1tu9
W0EVnK5VEqs58PCXJ2RyUa6Pexft/2jfXDM6zEYNHTdqJVnT9kX8P6+wru/92j6nYmKV+JdT8TbB
aiQySSgAQo14cOVs47jZ4yUjEini963pESqkpEK/BUTbMhyP8d2mcGkP5LunXN6wg4KRrvHMhZMY
Ja+43DnnXtYOqf5v3vKF592TmmZN3cnUiyfz1o4D8k2Ob9cAp+7/5xplkE+OHHmuebAAOe3e1aPW
LHkiVw7iomWU1Sq2eF6nKOoUtsjD337GIwRW5haOCLWmHj3l7IKtO+c2kiqkezNHNsHFZ6Wr/HyM
/31dINwgW3eOe7MgiUaHW0Vzd/YMn509eajslMdy2qJNRva5Pat6xuFbqTc8heItgX9eTzFLbYsP
LP2Fa39WCAKJQbo8Q4s2pJo9JiOt+KbDrybHSrQx1+nk2w/1bb5Sk9VbehDNUdbXNdg5CqnXjBVz
eOkaVXDJX75r3aq+ogrUWy0Cc0A2CcZfiRo1MfzitC8dk/AuHWiWmrSe2s8l0jLJpjSi2eXqRTtd
lpu1HBGUfAvbs0eSp5TRUaDsN2LoS8S1GG+3uogTjLAe93yiFnrW8fgeLlB18hI71WxyAuX2NlhC
7B65gOkBh0WRr+10QnL4SWXyam/QUPviAqmXd/khGZT+dUjmg/Z6zgyxbEx30/nPfRQ8TxXmxWds
W4LDDzTWgtyM0Igq6Q6+5ByxvXgh2sULasuR7e7+Cly1Y9S/bFcIS5tzN66lmCHnF+y2/KwrD5Df
EGrzGsuib6Zy1GyLSL0UkqBEUf1vqoNZ7aItkjOuzlKY85kgqypKyfOpuspta246tIfDNB0Lq8H+
WPbLb90ggPUfOyxSkRixC7ZWhRL6zOgb8Qamtz9IPuuXxQto2YilLiiaZP/qGCfFbKFmmc7d7eF3
TFf1D/pG1yr73RZOWD/tLzDaUfa9YMlZRpqMq5vAnuEvatXJrMtL4dGjAybxG/b9CXKrt8FMX1DI
kq72ukXoJPWr1vQD26hPtMzhgj9M7+IAyA4kXz5r6bDnLZg2Pb0RaXnBGJ/x0ILQmghESLnqn5oI
6WT4GTbxRR452yqABB197masBXZezRyX3YSCOhi6226bVMa/bLxaWcjMake6dLnilqbgVVw1NFw+
alw2APRmheXZmLU74YRBH+Sd3ca1Gz3KupkY1xaKK2bNwiyvvdI8wBokJakw/XZPEqVq5OUcYCDZ
WOmSE9DTTySmHiHvwK6mgUf61U0pBuOhoV/II/h7TZ87UBlBUhV1cx0nTg6Lc/eMqd53x+OD8tc3
XQTvybxCFN7TfxCRMee5nOSzqvcv8jzMyQGVmWv0wRvOrMcwCG5z3mcz3xHI/aTvCD6pmXNc5uZY
tL82LTAroKFMY5/mFgGufEhW5tObc3v7q5H3ohRpIkgFAbBmqs097c1tTF7PyHQwLMu6uHFuYxfd
rDYtEpW1PQpWJvEmZ3TWH/k1xVxmt74eLjNT8L0TOjfT4c5LaZKJDnbBbxjTGbuyvwwwbFvaHGXi
GD62E+oNQjrCicPEUQzRXz18ZXQ4MpJop4dq9NG0PDZibyrm9WrQNq5/cjLlLdAX/V4KNA9HbqGa
MiaAR6VWIwC43zEpESwLVcsjJgS9yU9mQxLEaR1EBCOL3vVn++4S6CMNCzTWaRNFHZFN9Xu/fCKw
LvenXbXqhOWJWN4O3XQesZH7oCUwEbsN/yNeDgJjoBYLwFo1+LmVUfp1IcFOltP7yI8l38HLVn6Y
3/X4VGN63W02SkGLBI7DwKJ0AzhZrOhIJRHsOUOMfd0IXd8wjexM4boCu4d+sFYm5/9NNgqzwK/m
SeGOIPfZSuMRYIPvRrDovUvXBaykpoblIRjjPE5MwrFl/9dPcCC4qLJjenqwz6mLylIXf5ev/Fp+
PFbnx2QpO4cRBrZ1pKY62l9xaKWgbyz35Z7jXFBiv3iqyFDBPFEhDV7LTp5G2pASS5BqB7zzrqnm
8ZIPFDdhk9pMq9+o2AkIaoAnaxFhiBTD9fD0Av+aZgjmzP1PunIe9V9MNjxHUl0HwizH9pfqNSSW
5F6eR4F3VmJKd7CvKcO+qEk/BDzpWToIxQO72xpj5LAfH3li2v2mzazCJBLC3l50d4n14ssEhwKd
LrbafqUAo/3EV0vPXepSnR2ggyC9RW4mSQycBjigxhA9d0eylUDjxdBBpc1FokUGTh8mO+wPjgbB
gpIOLDjSPmyr2MAnUwz388fPWnkmteqgLx7OxbIUQN/NDNqyTIXdJIVtFVhrDRfinTWQHSXEV/tq
k/o+TcwQQuADJGLEuw1iUkgjbzNemDBSYjHP2zNkiwXHyprRVFECWNXe1LC0FyQoLChVAZvKjydy
W6BC5fRk71j7A1m0jwAm+KzTmx/bhbnwNU8VEIkakM0ginwHCQpVsTYgW8PvSzAmRszwZRZDDiWZ
z4qE39frbErffI7P79cgagAhVWUnX2AulzN1bWZWSoTIxctaN1LqHMgsVXGsukpjhjS1aX8vDCHC
OBFq0BZ0EbsJ9BTsZTivJIs/I3iRqpY9gHs79uRi+4/mQnL4fVAd3BTq4VtKBUC8WEDOdaKU2ry5
EmjQvZcbEqTwJyUUGoXCCfQb0yayOp6Sq3GmKWCw4KC8sQf/21yEEM5o1jcqvNXSfsriNOPl6Szw
q3xqJ3DzZ2WGWKfYttdFEi8kKhFyCYNURLXC9ZxwAHW1atI9B5+Qrb49/wtd5nN1WGVGw8YyHkEJ
MJ7L6uHUPuoIFT9nHGjhul90WdmbFuNjmiL/sXiVpXvGXJWMCbCONquMDXpv4d6spBXHUZWx8i81
xNaPyIdHKQmQ1r+AhcD77CX2jC/jAtB2xE9l3R/SXW3D6BJ+BSfIIOCavoHM7fgtlYyOTk1+uGmB
TMxzIwk/S+anVi5DyUqt01YDr2Cn6/9h6AwzH18/7EGMMlQErN8QTHhNycMHU2DCC35gcaDYz08A
d6dvk4PtzsC5SqR0qtUpGkLEGC1aR1hEt53wWd2Z3wUoR3gEjq07NYiFkN+dOHZcJlEWtwfb2/DR
xxDoAzKCehtO+NoIMzJVsXNXR1sNsb0j9YQppKfiLHignnDKh6Cue/b8Zf9PrfJnKJYaF+hENPvW
TYXa7euFpA3vu/mJza+HoA7Bo1/aPo7h9d5WwPPHYmPeWzEfvdU1KIYPwjarkw+yT/9zY4scJoRd
oyoFaK11Gp17bPrv5BtZ3QCPetsZa4sF2XR7lVnGQ3XYdH/mqwrL4lDIz9vvL1xADdQ1ZtfglCtc
fTQPBd4WUoZRZPsbotKLntdxEbrjClbV75z3q7dDC/lSZ+gBGN9BgkbRgkVr7K/Ei6s1fBHScID7
9hhpBugyCA572rLEEmgn1J4ZIaSPjs/rnRPPvkWI3Nnzhljhh+PMGN40oo9hRcLU556fRhkYlIGj
RvbYOZJQ57GBDAqEqKWzrFjdlisejBIiTfFP5Ta6vHNooGxPqeSGeqBAa8piCJCiNVkuY2EfNM7N
bIyiBDQyeXdPDXfWJVMNfaxyz31VTEEwmjKhAR/gf05/Nl6DWZHkNw0KFR2JYtUwCTU3xMAi0MbV
Yr7oPkoxsJ0Yw2CtqR/Xq09ZzQVu5D0aJzbqEojr96eE6yWWXa29bGl6kzDiPqR6P9swOfLOJzJP
V/Z3lpHnWPJvUoxjytpaL7LmPOTfATqGs1dQ5iYkfd8GXTZ7eV+IYY0oHpxAgc0/ep/0FaogBdyh
iCBFqWUUgSVnWkVxQb7GeVBI8F7Zqpm/be2EQH8FIPbtaBCaFRUX24ti7Ww1Yb2NzTaJR4U2SyoO
VrWA9gyLEWQqpZFDyz0w11hInhAdfFBr15Ta46oWZOFV6dAPL0pwwl7b0ICi5V0pjBSFn9BQi0+N
BK6sVfmvRPQs6ZkGhMr6F8umZyRzQoObWzFysQhdbH47NCN1GiX3gyyps6vxE0kgsmAcrpEQ1ggr
BO3JxqWNroOd6c+iFzbFmhQcbBHo+pI8e5r1kSCnVVtnVUZ/4MqidQhusNFz0Na0ew3MQsEzWK4/
lNRksjYnTzAusE1e8PKM8SjPUyPJLSVepBSYfFaEjX5c9hR1cBMf3MJ1yVS6bIfyTFGj4tx1428a
TdJ56++T9eLWm2gN531scNK+3F4OY2d/ES9t1r6F5RzsuQoWzQdvWdcbgRtlcSQ4n/cKYkixY5Nf
JDxaekfQ18piG3WML4DELgddwnAAtDBphpYLWnXVzbhwl79byxw3xKRcdmECQMo+DGNNiZRE2Lnu
B2rM07liDUJGaMCxHWONOFHGDbMUaWi5to4t1TxQoOrfIHLXMEhbTr09LlDP+5A7z6ZXWu70x14y
1J8ymxhefQufhuNx0YkSn6lEk/97CBXzRa2CR0Ks+QE7EYSlvLvuWgWj6BfVhu25ngjupQ1LxaZT
7NBDul7wK+giuPDp3SWeDl9uWINYunJRYlgTkhidmtOJmlC1Kg1PN5k08WZC9b+3gACn8nFZFoDi
O8httVmQtx3AIpUoonOW1Cnms0my7UwkKe13zgWt+yCkxUqr19enh2mipOc8UkFqo89mBML44i2s
Mn415wM/UsIsXMymhtDxyA7y56U/g+Rci+wyj04ZVR0es6Gj9Lpv+cbKU5H8GChnLNQ28UTkNv1X
sO1F/VBWwY1z65TfNYlJQlGMP5dBmr4iGQw2aHYvl1yB5cMq3w80XEOICEDdX5yonzki0F11VHth
eT53nvKTpq4QSz4omtWhgT3G8keIZZBfCKbniNg0tJq+K3byta5dUM5uuROtS8ofxQW95LTn9Az3
Z/YHeXbyMYFGrVoFMhT1EQ8YcE6N9gxxZSJp8DYBC2vV8at4kS2hHG/XwYBET7WkT7IY4X9wJTsQ
8fkOtLMCgD1Si0MalMxj4MR/GBnoihFMoLGSl6gum6/Ih6xHJPFc948BlD+4omomlIDA0FFfexui
VDOdh4vF8WV8As2m+6DD4gjfRgG9au0qIlYkJMJ/YPI8Q8QsdQZ5E1nga6U/H2hi3Ej0Q8NLc6gw
zkP8+6xu9iz5mzgJ8Gdo11SmfQ2OmBxoxVNj3JFsoXLu1ULIb0NNEonmU0Ss6EZ4oKqCVyEOL+9D
jED4TxrO/Q8AyjlCw8nwoexAbWj+TF4WUDBtJ4DQnir00IOICPN0DQCMN6BpzzVNT+ER2IkpImaA
Sn1BcYe+q3IrxTiw4Rkx286B+nJz+eu+s84BsUDTDVE491wFWYsOtfjAW9/f+bQmLqEnuGvbj5x5
hg+OMAt41w7q2UtfrHldSVHebN3UFfOcMZta6o0ljCrCr5n4YOwkgJOWa0Bfv4r2uc3+howMq4qP
ru/2jGdxPJLr28RkaPpdHDaOSdpPt2LcneuX4TOYXNV792zKyZMX7zvqyVSUWptFFhtUzWZZlYHk
/keml1r2lbLLnSWxiY1SEJm4X7vwEd+3yP4/+mrPaX9fxrTJc4VrazD+Eoig2Z1WTQNjBzal6/5i
TEXsLMKKUjkfq7V9MEjAFhtbAeNPHDQ6kE2qPdL4lfZUDPE0/V3dyJsZ3kaa4JG1ge6p+sUS61Jm
vdhB0Kk+f3iS099fjRrNAC64qmsITPLbF4YgFm6WAIDPpPiZoMOJ1Oi44f2+6AGbZguk7UfTY6I+
kuZ6vZt/YlNRXyV/4nrZzMvwKgXjasVHgAtwaaocTzDujpAw6jWNXrzpdgBhkkhK7AjGMzITge0c
BU5s9qn+NvfDC++6RbfyRBl0+RkSD4KeDIjN37Ha92nkTbtsUFwtiP5dlPuJFMtYbCg9HIL48zdn
GP2ARm5CJORMRH2Not/f+KMM9gzyOgV38Mh3H6u3pybApYfi6gPV+yC3DaIauZtCnMlWg/Zb5ksj
W0C8SjgAQ5badsy/cGqf2rHGnLaNFA86sOI2PQzXi+G6M00uE6rp/LWnTkyjkrRJgeG6PSobDcTn
bLopDficvPK9vHakvnRo2HhdJ9Ktylh9jfzdm+pWmbEnqxBZ1F87Uxiks9s2bMHDs6APCVFjgKZK
SccIHDmfhMBYaaSGY8aPUw/4VDRTEZAQU9/vNzxAAn67WFIKcCa1BGfZY1t/Gn43azMv4rMe4J5Z
3fztQKRNCTixwcpOLOwbr/NWD22KaPl5ncBzvPN5Vbs/Kt9DnXo3AHZUXx+sTje0GANo9gPNaasN
/JmW3ZZZs5UFO6Z7nQ7IP9FAGsYOKO6kYA7vTd5C982xXAnRf20XEJWUorILXx2yXhBXHP7tCEwE
CuTrOvXVRF6/QUCIeSvgUtTdA24GZavUOrXxfjBf4oVcA2igGZjgybtJrtZ+GHERtLyQDjov012J
TtbRm4urXCOgRxrshUcgkOuymom2QYgAc+yxo7pccULNWMLmzxmJgVSxryEwGukvLeMsPubhA7I/
WNE5yazf+uF6rvpI9HwOUOPjIxBMlz9qWilgdFvvSOrjqomcbxNvj7NVgWuwUAyr9oXEkMtdgx+n
cY7b+JHpsYgNm/0t22LWOTDmf+9wV53lroAGKNRlz49tnJhkd/cedm28AzfVDaDA9vV9RKgkaavJ
vSlM+jeFxKf0d7oy+qvauWnkQ5vhbnCo1ES7NsUcgYPA7kyOOd8/4gokqvfKMclOIvgSHUAiRpjd
T2UTFQ3paLaP8llC2UClSGGNw85FsscIa/mtvvNTPKJaXCNxLyMvQf2wfkSwYMyYjI92BYt+Dn4e
iwjagmASla5gqEqTLbXCZT3NfMDiAczuQy2cjhCGLw4g86H1+kzYbm41vL3dclF3oOAHlZ3regpl
mOk414ltEpdhiq/gH5F5RQpizWrCX0aBF6flGRbkUIZOAP160sCsVVqCYT2FpQVx0eaaewcnFIy/
v0YScIoyrY6KNMGjnEzQYrEYYXJHVvd1YwN/wTzJfpDh6gnRP5KenIkY4rKiFEIeKLYrmgv5ew/1
VtPH1XgiC686chzctMFP2IaDwYYL6GigtxxP9ArqrWxgJXUECPHvQ4KdGcu/rLHwtACXnOK27TDO
lOecBxIrWTDfQi7jMtrDPEbH0bUnRM1S67rJaUz+XQTKKGvjQr9VZOJ6eY7RoxLtOYTxAW8AXVKW
IeH+D20vvdUCxmh8LLqD5kETVNb0xTMMmRfxkhq3Y6mcT06TdI4G/FV/fXuK+BApL5vaBDAX9Jf6
U5cADNsaYf9Vftv2gG+jS+PJyCyxc/M/zGepSRzNFlh0u0vbw+lpKcUCE2XBH6OO3lHHThtmlVJM
hTikAAfc0B79XDpggwb2TYHb2B/W9QB0bloFpHVdwoKWvcSY8WXJG6JooaKPeJ527hzTBnxcgD2K
+9bt3o8PdcQfvJ6mNlA7qMNvEY2fZxQPY8klTOVbuo1Ad0oDdvo4DXXv9XuPOkniGfRL77Kpedej
6DW+HFx8dxs8WidOEwqG/MYnbpmjgVMFNobX+X5zjpbFa+otizN4/WkoGiUzqHI3LZoC5DNPScHE
JQVg0mDMW6L6oakUkowRcMHFKp03WWpT4gkmUld/oPrxS3xs98+kQAYG5mlBn6IoHgOAxpS1G/tC
zlxQlG2vSm2dTNKhUWUiRBm5o+Ng+EPMv2Q58jM5jOtksUUOtMRa9Ow1ySu3IdscmYuZwp8wQmrI
bAx1OeymJVUDVYMKySXpnkn692eMfc0amELKfh0W41pYBN8s3mHbxKgM4HJSYVYNiTP61DOaEceY
P4tH48qO/JKTJWDwuozbr7qQorP4wW+Fffsa1BYl42nkbfgwbRi3azvyBUPDywBLuRVRFAv9HnAZ
93wvRD1/WjQ5McPa9LHqKCYHxodKtHfi1V5ReSCVbtwjgjv8gI3XdI5BcODVJ5WiPWi78elXiu1n
26kMtaGBHdCrEX8oU3tiSU/kXdN1Muam/vGJ5g8uWWFuvLqZeb82Fqb2MBKUWv1qegaFY6pL5wdt
rhBlcbjDj0PU/aYYSm1eSO6HXSbbeLhMlcEbkL6BUopl2UzAcSPCMgIwBsy2E9uEP3TQmIkXiJE6
yJ3WqRVdk3uue1QB9ikpob3ekGexWc2SHGR4VIFdnipR151IA5uslYDbFmEqUYY5/ZYy/XwDy3aZ
HTWhjZkUS1sRKFd5B0Dfcg8/jSdBe4rkUJXbrdEIBt3tpQZxAEFG3CG3UO3ZunjUw0nzQxhCo+Hi
IyyFTEIHhb51tszSBgzU7CJ/JrG1f2lC452rP3djIjfxm6WV4ArRwq0fTBFdzUKjMcA8cEEBQH7M
2BDc+QrTU/RZeWER4ha9IaCpXmnk9d3AQkLSuvZFZOYA5MYlrriQ7pzokR4SRw9iELLU5TziFnf8
0hZ/RDDSIKuBR+AUVRjGhhmPce7j2n9FpKcS396VmFTvgQjiOeWGMDHKoDuhJ2o8162nqbOMDnLP
lrduKucaqFpuxNv5fX0Wh3KpygHLa1Oxbj46UTQ3/lOyk9gpgdks7+a7AKGVblIFmqCt2Ev5Txmm
343f8fHxZwTnnlRmdwLlqXoRXJUyEvXAt0ObMJQrO0fLo3kWs5Y2aSHjcSCbFwxYec8gI/9j72UF
IKnZtoNXEsN/vzY+tdP+8jo8Yibcs7XVUma0KOdhWUW5Lqu4PN6QEyutq31Y7HquGtAOjx/Hw0/g
czyhBJB1fShB8L/alyTC57eL/u6zsFcDt9WfrrZFwqNQ6T62mfi8npBM+QpG9RSoKvIhuRuyvKQM
Enu3oCgtD/DPaXvHTXnJVt/LbZbPDawoHHr2m6SZfOPw5D9Q+JE9a5xeD8fwMaXUlL+z7A2XikWy
RYKezPKqVCCI5Z17HtN1wKGYzGqOVScrPgt3AZ7+OA8kDqtaeOeR5Zi73YkBmOV4t9N/SmJZ1vaq
j9+MGgjZNYRauvxfMBZXGS5OrMWVBFuLCd3nDpKL6vnDQZ8HZV2aSM8cwp0LvgPDAPwMPZGHn6Bc
c92jAR62DnMnTV0aZNWgfRP+Ex5PPc68+7Ve9L7o9KnD9aCGCZyNm4oFupE2isIZBU837RDzKda9
gLECeFby0fNJKD8IVxniYRngMr0xYKhJTJTIpB8bCuQVvdqrF4XB8CMtcgsiZXhOQnhiLxPuvIuu
F0ajRdrZa2jw1gLYoq5v4naBWjoDmC2fbMDIzMogAMhb9Ko8OLFsaEgfE+1BG2G8zXmZT/0rP+0s
cbyvxNQQcNs2XOerbQDt6s1+DxmBolOFYkdHyTAdu5yegqId3+dhvDN/61uyPMV2A3E8Jm3+ZTgk
IA7D7wLPLcKRLp+nm70pO4h5WS5xIrhrzxUKuqCJnzTcftT/oaXTeV0DDco2g6SsNuz66A0/EOLl
rdLsolI/PDEuANpuXxFIWj98OeuUgWejg0Xq2zvJOO8o7joAcndT8xOwykttgExkXn0Hlzt1tUF/
6W7gc1tWUB9MWzw7msdWYoLnrqEZsGE0OF3AArRoxWUDflui7eFosnroDM37W5lkURmwYd/MWMi5
iGcAXAHIs1vlyJA39RLdFhn66mPQLEigR6PVSBICH5JBurOM9UGXgDPxUGHm1SbqWhivFDq7qqXq
eEd0e9pg23UHFHhgHq+BYyQSd0oP/iAFxRovcurKUw7Rgxgt2h4XPs7j4DUmMm+EPoiKqO4z37lN
dWb1YEMznQ+mWOpS6+frxPdsBx11yBpVUzX7RFsb/TpZROhadiUM9onqJayFFQ9vnt+PU2y7asqq
DuhcUHs5HXx9TXx1W2+n5yuS0NO+50rwXBNm97MmcKz30G8jVdYFTyuo51qAvwnoc554HYFaHVl1
dXKqRrT/YKVwxBfQ3HQ665EwEqI0dxT1aaOxIdDPBHhokWguElAXbhV8GKM3D5IB6Iax0YHvADU7
9FPZE0HGidsOEpsvjyQbmPvdSANGLZoaxIQjLn2IjmES7XvC/rz90CkC1DEtdrD2M9g/23cSDEZ2
hDIQIsdI18Jd4yQEpRf7DHEXQxbw1dxUdL//dLJ69VbPho/n5cnshp8uaxdM7J8BGZb+vq3zhBNa
6D7B3QvEs7v9IyPVus36kQ8PeWWFcyA1mp+zMQKClhqtPS2x9YwAVGYwNRWj0EtrvuAmbQQvdJrr
yJETZekCG7p/ibifsFAGSpXArbra4JTu6myFSff8tQmi7SsFEx+lnvXWyenkAdgx3WvZIyPs49VQ
iRNrrl7CLlQoKwPZi87dJDZRklObEUcIm74reHFSk0vJloBLtBJ4ZhOOEJVhXEPLLP8FBxW4YfLQ
uzcqjSvvvTJNcv3cVaS5CyZM3AFaLXdJF7a3qp9GlQ1+YsOBjiBNZNFglbV76tdQ0rUsSYyhaSqH
6cKmA/sdjgetEenvDiyd0siqlzRGdFBQ8leRlNAS/OXobwyDTbvE7WctaIDZRPgl07uAZLojxW3Q
+XRwsCu9rjswbnEQNxp6avf3LAj8ITvBK+t4QR4T6bemYTvx+cpy3D5XmhNF2CbjyCuT2limSz9o
3t4+kYtuAldAASBCwhzhx4pZIwaUZZW6APFbjOJPkX8LRq8QAlbut9e5Rwncibg6YhbjAko7MUhZ
E8ka9aEvCWaZhjeDlM3qaAXxvoLrfRPUA94UmLFXg2eBbypTTHKO8NtJtuqwNIETpUWOXcWG2oUp
Gr5zDjSboszcQfe2aTdOj1Ta1n+z3VMJOSRG/iSZYnW9IpBUE4Yg4VWf+85O0G67QEcsulTlnG7O
MGc2HF1IjTEdkyNsnFBHtPX/iXjUcpC7bjByWDjwH3ucpmkNVzc2z1jiDW3bqpwyLyyCBeJVZtft
6ibLgqUk87hwTHQZs/PTyQ6qy5ECsaqxXMgFrVq3NKEzFEHqgKJZi+hyQ/Y7e48fXK4arQTw+kZn
3Ln3ZQ+sXnFHFex1K7mDVlWKQZnYbJTjTndp0JFqdk4JYFhkjk7fy39scsfV6cZvBC16lm+gLHB1
xNRXIkFlwEHvoiOpAJ5gtRRK10aS3e/rzkhU3lgGhd96YaslfTVkmFgtElzvLSWY9NyCp1BzzZOe
B9cInTE8ngxV1p2/1vAEUlKpkKh1paTEehhKrEv+4xGMQbAkNJxqCbZJgg+9VV3T1fPtfTkVwUiX
T7kbyCfw2WAC5V6XKMdFIqmotiesE1yZkGTt9K4NRoik39HD6QrWyb5bkj4q2ux3IhKq+5bXtOlS
iGKRBzwMvcvfGppb575pZ+TWCVWeeiDuZ4417lkQgr8cJEiFWSK2ARmkJHZ04n4IXlAZ9wbpxJw1
cDKEDRf17n5YT28PbcIojWZXeRKan0vbplN0XllxgVwcM4hq1hnWEuoDQpbpjrnqMiNbVrjl/THy
dSkKmizvjr5CHrLHvZgdJFhh5Tj4xYeoNsaBxo0GINeGDIrAnGQDKvxYkOcyLOl28VFklLLokQCw
kmLxPIUHsKS/Y8RkvA6qeBMF7qrDVZjwbiFeBWuDpfWu1+mqz2d4vnHmC3/+sJ362BTD8GW2X8Of
Fdm2b7KxKeOUWjaQSDvMloJlOMg9uHpic+Cv74aSy0aGCFuwYl2VRDlgumGJ04VFnGHFWQ8hkh4C
Z6eLo8U4/PMoFFeI509jndD0GwHtmGtNvCUPRdCcM9B3o/BlZ1mdrOdhdz8ehRHew7zmSNsUUGR2
VmHHjB0P4dDDJF0T4OI+TwXFjUeXDPUoYFT8Pzoii5pLQAJosvV+E1a+ML8UliPBjtftCQFjXijw
0NsmWf1c+UINWyISuv6KRCIDVEZdn/F9xHoGDxcN/ngqfECSYUsHic5nnt0H8OYdB2Rmjl1F8Z1e
fKWtsBbS5ksZkrED+fryCTINtAOK0lLv4LaJlwUlYRwbNZaQLDiYhf3EBLSpWCROupUq+BE+5TPn
ImMdMRC5Wt6heFS9LcNaM+cAAUmiGKaNj+FJcD4oWyxLEsflOWqYyeICgBEd3n+fx12vXYBpGFGv
I/5+HvOLd06PYNP7438vn6MgDDGq1NkeUfVYv0dexf8MSsZjxa33qGq3wE02dpEjVJ2l/XmtRCG3
MteOFo3pGnvjzOaVB2EPf5KnrCoK1JXJztuVl1LIczYQPsLFPJLIMOceHnrSQ0cH85RY103LKVLY
+qYsD2uzkazddom/0ZYLu5fmw343FaSvCWhSZag/IHWUs+bhrZrdWsO8Hjp9Upa13wjZxU/Yyox4
Y8y7WOfvXTlx6XqaVCJjlvTnuEgW9FAKxGfoq8pkc0vBuMH5PKQ1sbwOfLm33PvZtJYaXlDCdjWT
KtjzYMNfqDirjxlGFqzoUM6sKqPYb6DzrswPoNb6Vk1iJNkPIW94HHCh5UYyS4Ck+h7YT6wlXJVH
EK70oPeLxCDYVkUNNTs4WKF4oacPQNcJBsr0jroeW/KIieIpFHsjUXZhM/Y+PCHYMD/nbH0/U7ST
iZnVz9SkBlPEmYbbVycyekDQQDo3+VXGVZaoGFVNA92941mO0Ualg7CInD5FEbO8i4oMsBFM94cm
bRZjLctZg3QEuL8RUZXoO2dqpaWQSiZLpYwE4rO6esHnLxQtX6RIrNZ9oyHOPxLH7CmFOmPvRs0p
hYR3IZ89y1oaxUitVMsYR2o1o6YQIl6lgn8MXrvcO4fkV9dT4ZR/O6KRa8RPfruccGtuC0R1aQvo
d8NS26/2SaZ0IF3bCh4qFy3HENhRYPHautQqmzLPBD5Ps6lAaJwPrtH2Ozb96luB27CtrAVRHAaY
n2lmKlcNhQ9e+KAYRM+5NJnsTdNz4cqGoxbOszNo4XTv5m8Wiho3cE74qgLGLlVi2Nzd0Eyz9+St
HO2dQTA7Tci0tfI4kwca4HAD3ROYCeIoAKFV7I1rEiRD0rv5kO1m3Eu27ieCnSEzmnBa+JGj6bj4
XKpnaSDITHtBxqedbkHaktbNIo4uwOuBSmakvc7A1ChU/8LcaLu4wWgsCLPhwayutP+5AqswkUKf
Qpp9bpPKGnM1al6r+YYW1m51XcnkuDUMZIDXAvACCY7ADWvI4K1srF9nfIjfNGLIEreTNYG7VP90
psaCBq15ptpzMH3TZMhIFUg2qCll7RKBnid8yFZ8JrcW3B9cBDGJgy56Og3ctD7R0qnI+1rtUgkn
+AjyN/5wp7xVqF9saBflBJm48jK0R4PqhafjqoOn5ngVTKWNj8GNW2KLHknm5ZIC61wj1S0fbhcT
OwAoDvOhuLy2BlhLNo5Ajzy0Tizm5jJ5tDvkB1gxz8jipidmGE4wQn/Nm5yyNVXlHLytJonC+sYA
bqXWg6pVpkwaeZfbz1ObwXclr314xo6JfQCVM/4kfwdAj+OBu79PzpNFjiJfN7A37mm9UH3u5hs0
n9iw08GH97y1nUCWArUvqBrLnv+DvO5JKPhewpKUJTMUW+Tu+NjG0bBt9eiTAUe+EsaFU+EMLqL5
L7mrlfEIdRFi5J0f/zNkSsbYiGGJsyN/G1Nf1kBs6cIs78e967M9xcc8bOx2YmdYDX5V7tDvKIGv
tMzT+IyGcs0FcAEqlks+VS5ePkUH97SbYhRCm4IrRZPe9+PXYWG1hxx1oc6HIMMy0tLhoJ/lAmgm
ZpTGm9qLnrl0fmpcEnSYNN/zKfYfERbPK5t/TiHX9O8d9qP1HYlLFBnpuAo23Ce+EsOXWDEcrVhH
RfT1F/rBQeMuH30Vb01PtlkIOiSLfpqNf9f6me7PUREKlZV1RywizlRhABr0p4ImSJIy5o5X+zlD
dLbtRJDiipCyoJem6KCNWRJReuQmD1iCj5Wayvjk6WKzNEyoW+S3T57TwyhA0XYYDuUA580bkm45
1lU+BByBtGWRuWvqsB8XEqnoNXMWlYc895mp74Uh/9ntP+xdR979IuLmC//a2UjzuxRmmFqGqsIw
l/INAwvTqT1urUCU7iiag0kH6iGyTR3mR7Qvz/1j7bUTxmKXgohJxnou8d8J0eH+ZSdNvXF+yd4t
RONloKdrJojO56Ud77cYS7yqEueqdzdPvq7zTTO70vdrwnOVzQ4RbLBPQoYXdP/HCIjdZ5MP++uC
Tid+hiAZDJ4WpstZWvQeV8FyppPAXXWc6fXBUFv5Am1AY3VXgznuBAZFiVSs3xi8ZmZZRDI2hEuw
ZB5oHwdNguZj8Et1309QfDm9CM3MNaFWfktKQUxvHyl8VkYYE4ibA01zJ5zT+ZTNEyCYTLgxEXja
ISsxikGebZJ8S+CQj5KDrRwEkZgo08v3hAGeEEP+bkAArY5LmSbZaF6K/VpVwN5R5np8b3zEoHJr
WqNS9rpRnH5qgv6kgyeJZCqNrz+L/xusHP6F5Vxk2ME2ciN2W6ZHRqId3SE5mv0MlO5yUJsb1S/6
Q51XZoBvm56Gd8NXI99zYbYgIejki5ZtTQ/Tqri8gvS6CNwsu3R8jbpZqMP4gDIZvnJlm4YcRXzu
CadYID/IcsbjQ6ezhncOs4QK+qI0pkAwUBX2XUPJb0HcifDYHKfxa/SZgJv5YKl+1YrOE2jCUx0U
FaVpWt/Wu/GmzqhRslexQc9jlWJI+9++cGQYac4aPRS20ev9vo3Kyp/VjkyZclu/Ixiv/O4EqkIq
TgAk8/AG9iItP3RlF5SIPAaoVNd1/Es0ODIMHKMPrbfNfDEPCLqEzLpcBcJoaiMFF0stmqHH/Yl3
bG/Mfy5ot7QjlQKk50g7t8tp7ACBEpVawrp7i3sTuVkOgXrbSqpud1bi2EKlrzJB28BDR8n3oYxT
UiwT2LMW/+hhNhDtIK8lZ1iIcug0xSoLmHdK/ksMGD/BI8rVJ3rjrRIemI3XvjOYnPorZTKCDIxx
TxVELhen0kPtT2/Q5w7KlTi5Lg3tP7e/uxEtWBcX84IwTfBO/fZdwCilZwHZoe3hxXkZvataP26s
YwOStY5KPJhzaPFmJb5yJHfQ0+dgkeWfMz02ji0kknWxxQCXeRX6ddk8jsjdo18Z5KDuT9LUYOqr
j0Gc8BpMLpsLI+bJhuU70S8Z0j0xEWiUXrNhpj9KVbZLokfVl18/P77/1j3otC7Cfd+HNAD7kKWM
g3MK/eb7nvf6JbSGcCnuvSwggLL5xnPQ8RUrTFv+WcaDSuz3ugkt7pWx1Buu2MV+OQl1VREflfoP
rfT9gHJ0HuZ8rXqbDYscM3D4WaTopmUulkZnXfLp46pVNcnqghysNJYR/BaE819PHfmuJPuSDLVS
qsXHiw+/9tAdpzYyM5fNyCauxpBF6iAO6Ot8nFvG91iQNlK37bqla/qs1rUmv6zTTEVoaqMyDigm
HH8N0ZzzxcdCf0EO7Z3VAUQmXqJpKda3BH56yt7QUZ0omEUeejXfRGCvK9Tdtlu0cfX1oOHG3luJ
1dOFNbSMhrSSz7c5OlXvjNuiOkvPhZu7KZBqJR90UgdxBeVg52FFY9WfJ4HgLVhZVXsuX/Z7g22g
Va+fLWbVo2yOjCeIiBuxvtxLwMPRxSMVNUs3+VUN0dR3dJxkksL8DMz7oiOee616RLWeWE1pveaj
9FVFJr8dXxOuGJ/vnhFEfJ0sIRsAfYvaGOgMYD0p3es/3H2OmXPQcaiEh2XmhBQSDGX1sxEVoaaV
MB3naqVkkXPHz2Iad4HOEaZyQz7Vu2ugdQty2/KLZlUp/tHYhbDLZ/vgnwL0SmkFa0bMwHVrFp47
UuMAKWSVDvZcL2WIl0cnEWlmbLe76Q7excZhp0mItyJkCZS9ICDCpCcwC8ecVPIiKyu7gwy1Swpi
f6QF4aMG2FjkQvqaYsvzZVApC59T1BF6dtEOgzoyikwnxVcpwSeoOvSj7vnXAvKkHFJdedhyirBc
8/D36qP/xdgMDpuXyvKU379EuURc0oXCE2AuomcAlNULtoHGRZTMl5gpKR+kBocwHuGfu7SL+4sf
5hN+P4QINapECfeBHVNF1rU6Cged83q1d3s2LaQNTM5VpyYb4IE0jQgy/HzBw/NyBYOMPBjjx/vb
CXwhUBaKp4Y8qLMMfZY8cLN8LQH8QgWgjS4GSgbLtNZFOQ7MlVOX2vNJznU27/DE5TQwpjw0nSZk
lVURENwLYBkpi2PrNXof9UpCNZd34XLBdg0mvkw/Q9fHjtO3gEygh030NTwJXQSK7s1GVmVFlGvY
XepXkGOZ19+IText5KZakOR1gRd/f5MOZBdnTE7xsytllvGBExEc1cZgCAn7Zhy1zVJF6Aqc4ePx
HOjEp62rqFv3UoWg5DG3F1yJg0aBWIvHaqe39ToB2GZjbBS6uk0Mab0ik7RP0BlKkYR6b5iRCtBy
lbKfst6oDTB6f736AozooWgbmv/SF6VipKSfASoAxkROFD4sb2yabdDcbkjEZxv8aOkPKkkgSmn1
u2IBIjDQWk8X/5MBfMIXTGo/98E0AiwwFXofD6so4DhtcqSirZI5ijf/exs/cdIf+UNvG7Tfz1J8
74NsQkCO1Tlaop6uYtoCFhnSWAjAeoBpZeApewtPdILXociwpb9hD7ZQtGrIqTsPwr5KmTLj1sWy
MZX6bcylGqJFtvtPHRPx/v71TwbztCvqiaD1ekp80j20zklvn/+L6JQ8RrZWYJYkiv7QM+oQ7Gw8
Cobwip9uxfeJf7ws/dNAXO2G997O1JgdEhWRYx6s2SruhNW7J4JEVYuyoOb5pNLV7C9zzk1P4wa2
Bf0JKuQs4k3F1crEtdiJXXkDP2vZKKV7R4ZlQkvVXU0Rqw3IeEVXeYKGNhHJnV4x9QChbkIYduNI
c8jg9PqeoIKg48RCepZGVDRX095FMsrCB6prjTP2P1Xy7RTt7IA6UkZPYtG1WhkRa/DpCKbyVa1o
ZAB5t2qwoOPxkDcTD38ZH65zI8C6YpjdVD5Cv5J6nfML50jQiHJ36OD2qDuyIoLIKemyFSGo/iAG
e28B6Dzh3IhD2PRtxvJcZ+d33T+GfIB4yYWB59e7nFs/DIQPob51I1xQrIfGvy93Bha4fuRuxLa9
2bm8uVGdYBQbGTFhpTSTgc/ImaYWW6KsAO/CbdmGjkO+7OPmBZ3IxC0R1V/3XZf2hfe0iOAZeaJT
lVO4pFbpLl/xDFmmA9nuDB2zmvC1IB8uau7oi0bGJn69XJcfAfyav+D1fF6qFkdtf+26UhD63aMN
LfdyjnpIt3hYm1T4H2jEYG2MFKIoPNW0UjFkcvT8mx+qD/BGFQtZT0cSZWVe8CQzPiHXl9YgQYSr
AAQ8z2+tIBnMRdLHmMgovpHthDQ26KGTxbLtNhBkzrs+iBvBhqXtjYMQxth3o/Z+Paa5vBnimYLz
kIo5VEHiwJfdgQskYNiAO0qC4twC8LGPNRJPe6iknm+wrttjcDOfbmi9vPqBNrD+ep//Y5QcD8fa
vpWuJbeaZNoLra5ko+smHC3NkfhMwhKuDbpdwmcF8GTId+bwr5bK+OgFyLSHcdvpzuYalUxoi/A2
AKafaUGOONqW8n3GfB1UH/oPW1Np0QCkRFzgCGQxVhbPlWy8H1J8WbJEhQLGLaMi9N1cn4tmiZBX
V7mc0R0Nz4Iy5a3ptx5vlKX93bwHpCtUeeQBmOMSW9OcqzhtJLvx4uM/Uw5zoenfxQNwN+CGgp/E
6cBo3NGW2+dw8h2Xn4iId63hJRCFvpds4VQ4bkMTUbb2Y2XoTGdQf5aGjhajwD6CNsqOVxjQW0nr
TnA4UJK1n5Nhw1WhtyC6WKRMjy1ivpY9f7dN+9fQpvyD/P6XQ/fqUKeG0lGiqImLPWma4LlIeyGP
xlvh+eB4WVMlYHREpFzVZNWtQyRG9tiAt8mPseBAdh8AsB0Qw9KHeaJpv6ThWcWcMw92DVKrv1gp
RDhEKP9wPJOV7/7W7kCnv3o5nFzj3u9Itgh1ziRYhGrvPJ/ySVpY2jzsJcYZ1oxsbmnKQ2sE3cT4
bCmtoDxshAqPN5FlSEjY2ofbYcENjPIE3hJEQpSiRt1Mp37bTvzgeZiYuW+heaxUD1hMcdLlPm9h
g/ujXKXbeBY4JTBq1sv2cOzLOIXACw1HOFSo0uG47kYrVdK46aAWjlLDiLAnPnn8aacGiJ57Yjir
I1d1AeY9nmJ28RfRPrwq1oRdabZ5UZPKughtNeCXTwjd8kgPOlsqhKI1/BV2+NKw3npvp2a02T0h
+yYxDmoA0ZFZqJISEfc2Bj1SlJMVf1So9Iz5wmI8xzggvhdOsNGVosTDqLpfirIm+2BoQOvPOITt
Lzgd7pyuHGUnoNv+fSNMAjyOnxTydMWu/WphexC65zg3hHpJGjvEqSxoJQelXXtPBfYCeimAGytr
1Kr8wywbxuDzkN1b9/z/ZMso1BhPIQMDLozwAKFYxmqOtO8POagdpFWe57W/WP35diu4MSEtkpOE
qC71OyAWHYhMkKvcf2N3ldf25pelthZkEXuSG0vHZWBT82wGotaP7K7wVjB2LdsqQ5M3yice+wXl
lgo+hOclyfpNR7TVNnhQlUtWFpXzZzAurSw2fMH6LjtmIe7ICTCjli9+kT7vIzMaTRQnup+kfhYG
OOewIJCSNJ1DvyukV9BaoHf2j3HawPJWx7GTxJX1cFyMD8K1XSofTTLYbHNEJIHJrEjzNw4C5sOI
xWiZBWGeBBIOWjZ6fxuF2iyuTB2tzo6wFgornpM+X5QbFi0K+D9BhD18ELOeuqTPKdBhgGTekBwz
rG9miGipJ3EPz3zSbZZ7vNHuZ6KdGW5kPuEHxFg2tW7z5EXCm0GObETZxYEvr5oG7UDuN4ovqeRP
sUe/AycIIG7Zw3rAebBOdcEkor5XFNf/hLKhOYVFVK5xNheP45kIC7ea6WX211a3UDKLdbOVZexO
z/G1wG1kVCGH8Gdt/UjD1Qx2fQhLxDqK3pxFtXMwdLis3SQRHepIHob5GxsNctrh+yIFkM+BA4Bc
Q68mBFA3l7f5VsLwg5RN7nBHiLU2EMW2DF3jttnpR8fvpHS7+QdX3DEVUTtScr6Jg+PiqMCBGCd8
+UXhrcyYwAY5WYa9Lu3NzNU2M1vF5av7DjDr7gGsJ5FgFE+Plh+UO4dnwlRd0VPOpW+sx2OAq5nQ
XOelvaQHYXcNnxQT9DILHkD6Oz8kPAmbOHXrYa1YWIm83wEitoeq9hgZADNEoZ9ut6DFhw6EZ6TQ
EAbol6uUlkckUTDHS5GUVD0iz5V744xR9aWH/GK4ycnNc9WEnKi8U25dtVIS9Tgi3Gv+sXeDVdRv
pzUcFVCFI0WqHHQP3lxYnrpXclwaaw7fforzQmCO89xURefGHkiPW/9yuwYAzB7UGV89bDKXqtEs
wMeBMNO+Af5uMbcDvAEQnnW5ksfu1L/+gu/Lx10GqNZnfjn526GMsmy+58q0UR0/UQM0cdQIr+vO
kXvR6XoGD14ep0ucDEbOTEXgeW4NDy1YjA3qvJ0CnhA5AQuJ76AsSkXP8lcPMWpoSMnED8cpM/TR
2mDBlfS4vUKLhMnU3vSZWhAShtAVtI/qBjZDS90MJEBNfsnvQq8+LOnUPP4n7cmoBB73QdSe/+LQ
2r0DycjF1f5lF+2NAty9Ygnr3Rj4scTX6rUssEYLxhsuLgnmoOqq8Daj3uFrfgC9zIHwMmJJBT4u
Q6QKG84UtT9B+0y9/P56yTOiIGxjEgl0vTC4hTDJTGd7oMlpHsqxLFhnaveOwrokxcx8nSSwDjtF
C625CMNwyqY+FPYGJjInUxBWAOhCPcm7jQ3ziA3H3NHhCgg8Ei0ZqrW2zXxDkyYa+YcFj0To3zjb
zf5YbmPI0vgef0cs49FmS40f6+cnIuwuiI/37mZqV9Y9KH67eACpROyewIGVEaOtsbGuX/L+sXvq
6GAOMgFlpvPz2dZ5W++FGJM3ZazeqbAZo+gSsUw4N+CDFBa3JDDb1dx5anFwJb+JIesjxBQyu1Ne
hnti5Maj1+/yLxgAZbq4qzYyyha+wvlcjDRGe6iLkL9bt1yTtbRXzEINYLxdLWId5TxbdGK4VcoC
/YdW0eLC2MG8N7VvSP6g3MVKyTpAlDnViebruOoGB8HNzL4tGOWQU9CkDsRDFYjxLr89k2zFPnlU
eMMvtdXyMC8jnvUauvxUPxE0j/xjs1rJ76Q6T7/RxS1AmKsLnuW0owkyE2ukcusyeeJ3VI2CS/WY
6uZaiRavWSZ9GGq1lj4BIoYVq5IieO1oegeHL4wuoicHrWnhVxz4VkJABR+6Xy4DyKm7WScIsw7g
pV8FHbPfed57IYjHIQMuCSWbHSMIh5JDMvB8WN1LK0YoSM4lJTYvZHgra06xBWFEua33l//Q0XUq
dyA4/cMQsik70Y6gQi5g4hfK0BNxzrNsNLxA5byDvTPUpr3YlQaMG/Bq5PHWSWrmBPn4iXBVF1az
CwdXYtVcouQ0f3Mpn7foyXaknTYlgNsqbz/s/0UqRGvRhK8zBQ5yYMlerWf4OUst7qvRptWkr3TC
37o93EQ7p2Ooz5SwuY94OIqUkW3G4JtZntZ+2It1DIvJk87fp10O7GegCRQQ/IRfDBq3Scze9Jjm
IMJ5v1tK10Ut5CfZqV0jjZ8XbY6kAS2JU0EhEhRNSfRUxp7xVr9jBAmWMq9T8WKzyo2MJRzXhVUI
dVLJvnDgAJpY5y/PQasHoJ45ArNcEmF49wA6kUTUcMNaCVuxZ8/lpjsdSYgrAXblWZ6yw7r2kqUD
HfsYqoRtqxZlauLgj3Sxe+GhEkHIvK9mqgTXfCv3F3KVD0DXI34IHEx/O0vBEtuAUoHMWtfrGoz9
UM73uNRD1fOKiEE37d2pmTlfmZBmiS71lfXH9XDMpcRXdrcTGh/2qG7s9GPQBIVOA4+aExz6XtZX
E//ocWvsdXZ3vp4Lfzse8Bk5iWILHz7yPQQOsUy/wZVoFbwqreSlFa4QLhdNBBq8vtV4/NaiP0nP
achgmSIfRg6/RcTAIf4sxSlAcsfS4ipl71cT1jNirXrVWi+hMeerO8ogPqT/eRp3N8LmE7pE5cAU
17RR6RB/hhiXprpiJxSEoOO1ZXWrbB5jRLBnVNLIdPasVFUv7gyKZfifh/BWD3Ny6n4549TKZaRZ
/gyV18KhiJJxpIZqBTb1XPGvUdATjEf6kYQHtEk1raVp4rTcRmapQGCRAY0ZnDgInyOiK8qxZ1xU
S1N3HAT+HHze3GQ8nywhUemE3ZyytfJVp3FLwJBx9ZlqxoJQVOiCvYEMcZBDpWkYtIuoSSU5ttEC
kZN6i7S/Z15o1AjpKAyl3YIdDgvd0EP9wiRkA5/BtjpS2R3U87s4HqDRThmQDCAvb88raZ+ABnuI
g3AqU0f8UJH82+71h9ff///9t24fWFSMwkWLq94ZaaUf5uCume/dKkIfa0+wgOhBfZrJnfuzBuJ2
UiNV5TICSWjRYWLul9X2Jg5tq06hCqSFs1oA1KHAHpXHG7Vdad5xRWSfyITxlQw4gYIBj/obf11s
A6BYMzamXElSmcMwqTfihDroR+UUu2KLAZNt5eJbCYEj8Lhy87iyVlDNLrdUpBTg2BmWpMd8X+ZI
pUB1+x41Gv2D+wSd4X6S5NPNE5qzOSyRz5JrqNo/LeQQT3rIX0un4+rhGFsO+rDhkE2oIVaA69k1
TwVK8mo/XGMkdgj3CJPpx9JH2neZlLF+YwY6gOD7iQ+PqFE+H1OEmPzyBniEj9FdGO0SwtsH7o21
AB591W6h7Nm5ug8021+amHLsqzJuDyW2C84IBYksEtg+1kG3cJJwz+3ioI4yIifpR1n4U7t9ES7+
+iN1HSXJP2ZlAF5BzNgGTcRCSypJJhiOjtdP/GKsiZ6BxwJUXbv7xP8od9MWajLcd4aqCC9I5cKi
iZXgAIhAFGmKiHz3+aBfDGpz8B6ShjGYFF3zIaloNmQKPaD4GSxTVQyK9tGorZaCvkpk1u7yOhmm
u/SVFZYA3hAE9kFJPq1A7QuS+zaKYzj2k6Wi+ml8AsUWVeuyXTeTK5OhnDRFB61lEpxvPC2SWdhX
WDKB/MBmu4Xy+hISpAWehb3o+AiE5E7tyP0nnL85fLomS0wuXuqPZf753InIUUj5mohG4eyhg/kn
1DtgfQoFjjVQIOUPcMZ6XhsQdq3lQ4y3cVDxu1OAhWzkYH4ZNMpTpPSG0UJT8JJlPVVEJOR9MXtk
JpaQeOSDvoibko4zh4bZqIRwppmMuI+ZJkxszER3llBZZaPaJO6YXjVgkakFGznh0z+fUUK07E0Q
sJbfhh+83zRmtt0X/LBweSOe5+IgOHDNw3V3mO6dFReMY09YQ/OQRjU3lnUh+k3ND0Dgh0AyGAuj
qqaZEQYc8Kwr7b6mNmjnjpmsQrjMwMrLtsDss6dtvgRZzfTmdTzpLWZEYc4EFrMBo/OojRhDmNdw
lBzy+Pks1ja+sv0kQjXhHwZW9YI+ozwnsuQbPH5iRiNWBWukFjox2YSUdvd84NNQykWjuvWqUoN1
XyX8QM6HxMCv7Bh/PjVDizo8AxJ6UDNrVsb5tvWhyQK1SzYAmaqYWPc1Qx09u9wiGCUJaGibo/ra
eeP7JIIEq8ChdrjodSTQX0Ky0xLw1MUodCt+b8H10YCB07Bc78AM1S+8JCaZv3iURBR1CdVB94Bk
Uql381m+HNT2cVC3b2pb7JyA/Xi8jLQofC6NXcVqjRukQ4nnNRJX0JitrHEAtIffJ0/nXFSg3gxj
hFcECHdf0KxC/LVoPCOOGKKwbCoL12NKQcPFp5EBeGVqQlk4+woAQsnpnCh6brdZdOy/Jx5qowOu
Hgcs2iPU2JAfxsLDE5zKRKAmbePBp27+cVZ3V46k1jnxHSyVds5WTxNcq7z5Fc9qmMDMhtl9xby8
ZlIyT2hFNZD7s2cd4m+HM/CdkA+sTMc9fzBkFv8uPtC/+yM+eVj9Z2OND5lDSLKB4tpnhzAmTFjA
zFaIhXQEiZonbUuy6yIIeeisrRIZlPorMver9k7jR2XzkxndUc25nlLrmPrmD6Fev6tEOfp7ie7P
WbuchrIQRGgpQDNibifVIVVlxkH/zcbQI2N+ZKWNYXBxLqEpPR0M/PFk2DcdHSEWbt2EVObQbEM5
pjkX8nuPEBpg/STJLnP5U9z1gpXePrC8bLZfewWZLGmE1vghvL/Ci28mx0FhrikuFQMK+TkriAgj
nd0qI/uyCmD/BX8x73AY5Rp7vrb/+iR1NOxzFqZznNxKPw2pxN/4QuHB++Inpjb2Gdq5ONsy0j+L
kl6/VdzgcpuCdYE8q+WVNj3QDkovkFT6rnRX5uMDEmqUfs9grvivRF18JfGCPat2YX45ZEZU+T0J
j+kJS/TsffKLlD7wSie0PpMSE11l4mEf2O+50cXByeamFl4xVzRgD4dyWGIWNDgpBb8or5cd4nJy
RPWtUSyvpjxmY2Jx4nrNFPlgJgjqzNpQ9wcCAPB3RR2oWNPEmD0f92qsxSWu/ibyv34WLpk/ZYx4
r5vMRo8Z3GN1ZixhJPoecKrKfONEYAqc/FGMfQzS5i+Tnc3aKGa4NpKNUDeDTnHOMY+AFo83xI2i
dnDwnDiaPMhYNSMWRK2vpN8EkG6lOeaaYOxc68J95F+XFgzo/UUusZIyvG8joUBMcNCtT751Z9wI
o6ef7GBZrDE8SeqOCpHZTTLm3bXNcba8zXuzwj6z4gNuywhzePxmuseNJ6WXzyOqxxHfz1k0XH7X
hpsfHJeaKpyfSf3DgN8NBwaYn5V91idfWdvRth0Gj+qd6mactUKEAGUhh25VBbwbPbputsQknmu1
Z3O9GMiH7KBs4tsSOxRQcdYDkS/LKwxULNb3MQbrlgUOMSkxtaNaNl5sLWQ4wnzSR8qFvqTeoFCM
Gvs/zbsjaLwZWQMOotCoapHgo9aVuhOveUaVcN2KPUk3ZcSFWo653puczkW/5aqloVDjP9At+bdd
GXdmGs9p+rBszuhRVEXacF2DJfuHOcuRt2CSu2vpRPwVbQ9kht3LArBmOu1BcAbUgyTvOCup7ozn
phcOyjgGlap5hfUt/lk1LEXfAPeVm2rz9ZPHGhmoMGv9MyLUyNITlRYJhS4x6uviXtPwmeGtOSU6
qWmjLp/B0KAPRExGtX5jOW3inq3LJUxvOVdL7otLpGEwTqSCcd8diAdQ/3Wk1ColEMou8P3CNQHK
DwS601FH9yqmwHeLDAJWK0iiF7FVTiarm0eMeklBI8+7NWUamaU1FpHgIW/m2dKdIzFyuGqe0fL+
y/LwPtGCciV19nOrA4SYBbtaxRb0JpF7o8m+JVTs/6i5vacnroUKJ7AJ0xRroPcnFtwft+bzzTKX
e5a3i0XKgHft902DijMw9xZ94j/9EnZmihP6oDu0gAF4OVCIaM+oPibkOSK66Aj6n1iQY0oXOWW5
2GQthXBjWomtD9kAVSb/n36cfcyhCbImcrzjClMHbdNmQSiiXWqXvff25+cPNStvCMlv5xm5hkdx
XnXHOzQo8X1LIEuxVvyfV0i6ooqvbMkLEwgFbKdDUtPSOPhI8uqfLi1XEKf5rvotyVV5PjnP5qnh
OoArv9CHloLXZj6Sz0VjmO8UE+3QD/ATqpqYIxMPunCmkZa5cefJO7CacpIqNZ7fnwQYkhLhqMG7
xJ1QSRYeNw5hzBL/wEbNAfN8hFxA3qeJ1jX+PXk61qHR1oM7D7Lrr/MjAnq6NOqGLpC7uCQZsI3d
qec7LtdFuq3QXw8W1ScA2PaPYOeZoxztwB27hCmr4V9QeHPPr21Qu7jksU+ctXDD2tjALG/VM10I
IqIcQk7J999+xK0FAo0tbejNMxM8jK24XOj68JhfkSxniyVqZ2xxG127Cy6pJCOg9LNrzxCIYXZP
lfjnleJzZEXkKW1D2v68eh8/Uj7gFhVqE4NKKs/i1wkqNxRXTVViPVIGAStUlYIl5XS+A0f07QE2
QG7QfwX2rkXZ39swwmiTpWTtZ7jgV5G+05gT8kg0QHYhWllwc2UKCIvTgn4ysqDTnM1KRS+JATGn
iImrstlxG+hVDoycgKjYqclOqY7fH5RRmoYs+THq8/WjtkY7lP4EY0VVlkwOOxB03aWxQkuanMTP
YnGF5+BDZSn+1c9npgICbPJqa4vOT9ZQSVKRTvk5by2GLfJVOr0IMOHEstxaOAdF7cA2NnL7ySKR
msjpW3PAsZ2w0eUbizWw41zW5AAS0bGKK2qy5IijGQSaeQ3TzXcMfUXbN32Rs3Qaf3nHM3/HEWm0
HLCVjyY+A6iJvaPQHniKz3aDrpWgAahmKiGTBHpq9tJ2c9i+sI4xVVL+uFqO6hneAQr9Txyo4Kg9
VYAIZkXbIzefgNir9UscRiiL61azw+dwllHW1ug0NxEY1nZx5IZCwF9kRtkLEktPlRb6e75bjiCm
AWxczvbstxU8fkbSIa2t0J2Halrx9gRzqPMSFzyR/prJSsBJAdhYt7M0V+Z9r+zZYUtTdoKVOvOr
/Y13il2+s5xu8H7dwfxxfU88B3Zoxjww0K2ika5BVKuq8Nk7M9yFoYCji2NegMPxAaqkH9TMKgab
cyV8ZWC5mCmdpF3Eov9MOZfBG3g/L3VUk2Xh6Wtu3lPRhDc0PU5cmhSHd8B3IlsLFjfHa0Hrm81K
DyB1OQrB65O578Dw1J0u0FphjuapRWaCTmylpmDBfql61dAsBOhVDnlsOsknmFocUQLLnhDmkCar
Tpdf9F8b9cmS1w0+dKpcnz5zJjf4gWE1IQxGYtY2/646bWOwfUDqMTanAPPolQUZB24rLmwV8XOY
fGVbdDcMDmwl9vmLtb1kRg2UWSMXmPri5to64IfW7S/V20XA+2jNWHeiK1I77UtBef0a2kIAiYZb
U20phGdbzaPGwyIIORKe7/nEuhj0NCdI+dWxUHLOcL5WoAw7O9VxlrkGKL7SQQadI5cZ0rsoxaFj
p0kOI+W8wcv/F85CRYZVJL/u+Xpa9xoPmSm3OewCU7CVrA/heXtm5oB8CDo+WuKbvE6sBXnlsA3R
EWGtzZMWxJQVJNxWLLk9Zsuhr3IFydVc8xG1+zY60AHMNROBboCz8KEqN5hfXQu71HnP6tvnIWLr
8sD/RzyaaU3Z6AogQTzNXpbB5MxLo1bcEwFw8AbNJbbHoBCwWOLvBiQx7QQEp2K7+Z4LeDSs/bPg
d1sG8JQzgdUOMN38w7oKrAYikueYG4FpPiFhozrsqC99XVlzYrR+Bpoebu5i8yQqCVp8Gkd2HedQ
shKmNQ2EC3adLIksyq0eVdr+3uBodBt10HAHUUhtDIuPYaOe3Ej40LBCDKXt32+INDFuaD+lxaro
/ReCp8TJCgAWbFrHQlldbQd1r07g65mo3Hi5NRi6+MFdY1ViDDbHxW42JwQZW/BYRpzcgeG0ULd3
JO3h5vWGjVEZYfNo+7w1WYiJWoWr4qXl35BNwfb/mQX1NnJpqJgOLdlYSNfnbgGKyGApmgyuj2C+
JDxEvmszaYG4Is4zZ08iYJPH97hbofEjKyU/iPptRj6FFg7s1RhBcPmFBRqRGx8VcQhppQHylyAM
3Djyq3OqTN+4ONwwHoAVzqllDoSNfr+CRhJfIdkJBsve1r6I1GeCdkqHjcTRdBipg1WyDIG9HtS1
ifi3m1KKecb7fg2MjE1vgxOApITopjJ90dEm4+smMXi1pxOpBKQkz/XxqcyJM1LBnqjHxGsvwQ6M
NaHPWvIHGb3pJdFBB3Jml8nm8OgFmx3RdbiajjNrp24fWmU3FJG7ykTfJOyDseH4wQIzt1e79tP1
0f0CWLQ7L4eYwIC2IeIIMHR1eEr5QXcE/YVduz5FBr+PAK0z5I3eYhoYGtGrIb5XyypPV0S23wRL
P/+WfHFMlpmGips7WUV6OuNHPYYIqTuTDG439B+b3TvFGQuymLs9CmT6X/6ZCWl2p9iU5TB9Wxu/
eWOGlrZnZwnuouYyLSDG4RmxzDkcl143S1plzyKRuqeQBE4LyhWqQOAbt+IxH6qfyrS+U8/hEJoZ
kAhG1gW3fYXlV5mtFuX3uqP0QzktdpzL44H0+SnuDnLSPWUf/UPY887vPfzIFTU+OoFQEn5Y4ylh
cAWklgEkgrwmMZMc3feSByRVUrA1LGDSZk4JlgIkObvbqlu7Zkgj8Eupt0MAH0UzPI+KdRso9WdY
VEDuq3G78TFXYRAUyuuu9QuT6zowang/nt+8WMjPruROiK3aX3mSE2gZDD4i6LeGyeSFa8NZRZi/
SqwZJzDEhS0XwUiO3r5se3NVjtI5b+m+GyvPyhZYB/8YiMqDukOmNaQwYCoiZVgLKYqPzxY0p3SJ
faPYxZ7NAiufdRaDQbohHn1YyrHLGoSaLBvYvx9XPJlRNq/iyE68/PrKJrvEdvvWTcWC8h5ABA9E
ugROEsGn2NxfaS+ZuDqZsPTSAdttKm7sXJGAhISwOXJTx6Dvlqu3fQH+kbCfenkDbFrZ38ebK74S
cHqUYjdpcID5fWKol22GnynajZiOIWyuD7lpaTawVyFAxx5v02CMy6SOFq3/onlNyOa0ToC1CDiX
DVwNrv2Hanbl6kw487Eo4U9WysM45oSIR38n6Hh28rgdQmpLcs4B0Rt5TZFAWr9V5xqo3pzgWmhF
26a5ZIJtOPzcKSF4cWhvvdOYe8XAZWe4sevDDzRMm4aSdsCngf/PvHr9v+hKSCpO8MsCAVP81OU0
yK5BODK/cvG1YukAZ/worxc4C4yP8Kgomz1a+Ug/xhxrPPqFIGt+RAQdbF5WDWmvbMtG7g57sUSr
nUudgV2VKIBHzVZAmCZHILJJAeolRwAkkDQphpvrK8QTtArPqIQUhCNnzqAAq2e8bykkhbaa3ZUh
6kg/3aexiN6IAqD01G1eb9mGIWbn2NCNAhSs5zYqsS6NYF+1kamQMXQlPcfR6ZsT0pqUCwvTFQLN
Rz+crqkglVHxdBxspvctrjbK2NihuZc1VgVCCHbdDv5VQz9SVSzdFXbRDfzk6M/pJcq+VWSL8KzZ
HSv5SQOyGIczuD+fvnvIYu115aY8jHCzjTXqAZxk9oPk3vL2xmLiLMBVjO9/wi6xPIBzezfKoGFK
wsMNHVuegLOFjL2F/9yEImi2STEKZGUYaREzEHH/2Pq9d1tv3fLCM6L1dIOXYyzphR3Ms4jZveT2
C0jqXIZepKGYrwcglkcoyvlc3qWXezSUAaz5mj/iweEuS6A+TQ66YJqvYhV0DlwSweckxC8qM6B0
0b3L7zErmvtolZ3ao5mkKsenWUgVNEfCRHOSoaleWTDh2CoB6hkyjNFfODPVo2rjGKD99/So/1S6
+mhJcGcp96GwPl8j70eLtQXwGMBYPf6abonYa0CtHFiUGS84wW/pYeLBHsKcNuL91NiIvczYW5om
f21HfYD0YP1eMRMPl7OFLxPSdkNwF6C1gCVoZXKaQz49xZXj96tls3h6uHMh+1NN6j/HBZgjFWQU
QZT1CEbfZzMCulZvi+rkciAa8Lufm6Fi9ejudDKzcZFVPqr/hCsoN1BNvIBTGSnlNaYk10z/6Hek
iP6SmUEa9rgxcB14fn7EqMqThtOUkXTn6k7myjrHyuUbrY5eJlLj8yz7MntDg+EaH4gMoO//TkyE
/JWHR/zaRu3xZbLAtz9sxQSS1XVVuRPM+fzd8QHPRXoJeOF8QUj82QVTmiLX0z7YotCX8RNch+t+
tQJHW8INbQKY7yvD6wv3BcMvmteDEkP/chsXRUhRFiFZHtO7jgMjGdDk4RU7mzHLKC/6chVAf2te
RScZwzx6p4epMotydxjAQ5J+ULWBhaIi4ZGr6f4pGLztOJKVwFepdIl1z1GZA0NWqHilhqR3rh1W
msGF+IMcD8cqa6hvSevoAWSFMX/1FFgjox73iGXHxf9vqZqJKQUE5uNZ467n2zJu1pQRITixY81T
G8+lj9hB9YbxAW2LS5jv/pHLm98jk+5SeQU+D78Ej7nZHbMLm7TigMRZ+zIxoa0AbZUiXSIeqE13
tJlMaQZEClDMY+RXaSuD1G08MA2ot1pE54LczUKg2tGCg6TcdmjHC1I+r047/yBmcJpPjYWi5RsJ
HGyiZ+5DY4vhJ1FYjNYUp98AzRhRQnax/xtgEsPSQZsKO7mDGllbNdf3o9h2BRd+h0bBdSqQeshC
/p9t3rPPjmbmc9eXIdCjnLygoeTwdC/ftqWg17Y4LOeND/LD13QI5RCQ85yHxBzWFfR7IlgXXMsL
QWOlcaNFJ2BwT141+bgdpdO8IlIqBBT4ynVjqwXZr3O8XN3A/NmV7yjvzKym+QD4UKiDomMRm5QK
RV4+DDEFdrH1VTsa0YeyFMGbFw+ms0Pn6rBQLFn2QZ/I0ZHMIzQLOasTvleWYoJq9l1ozgL0Spj7
5Dx4qFtCejK+z5eIzl4GT9wQQ5N5+diGtAnSPxbZF2XMXktorF5pqkOytiZhh7GphGVFXGT4ey6i
Bud+gLnvOvbqzvE9jqhRZG32DvZtWt5b608gCSpHAcDeL7EMk4VFwqV/LDfVzohgQgBP2vdbRdqP
vn7TbPkWrzVsg1/WVva1W/VR2lgFs71eFlVLw5qPY1pCbOFb+cwVy1gSWje/MCnd8PBTh4soQPo1
9rkXHYG30LQ97REcV/xUaISK0p7jCCfa605JvmVBUXBph2IP/KoHsmI93VCKcBpHxg6LIA243Pp/
wWFjZ8tbS8d3KIiy2OYUgGiNYdNyNXja1v5a2sHj8NVZ5oz1PkTiLHYAcm1KIpJhtADB4fmrQdlf
75qRako4nR/xVer48bMMqZkYus9mk66+NHzRIwPKStNboguiXCxaN77f+NijW4AEqMa+gLeVop5z
+YQZB9lOGtxICK4Dp999gRylvTzNaNOOUi9OxybGuzOiEjC1UXwyus3BH2aAcKjq0wVSVKRRqlks
M8FWZezw+EVfXmknkfzBHxerNqpFHF57AKa6YE31V09AmXVyAwDHCM84k8GG+08soDhmPEmppVk8
U7lSUiSIuO+SuwGCKhgA42E/xM53VaSfyV70bZF8WC8jC1tFWdMhs1KNxixcQ9zsFcAUvvC+p/vL
CwWZwVPZKehq8X722TbKuzwJTy137lcnEZ4D6uvRoyrDimUHtY4TegAUm6D0LaPyelmFNqXyss5G
1ZVHRJRYO+R/aIq4st4zQsJkzYZXhdAZVsZNIjAslVk+PZDCV9Bix4Zmt1njx6wemzTfHTDD609e
VRU1IxCQBkDBz5OB3UWZVzIq55EmZhkUck9zlD8ENfr1KQKVqb2ouIGrKwDnjGqDlDmT/3gVJXSx
R9szPfxihTMK/UGBDVoQlwB7Rv2cpKEl4AOgxUOGz+PGZKW4UX/KyK3/cPNXW8fXo9vCHYcC0+FR
PzeAhDP3iiZkrDoMPHPPp5ZdkT30rPbmFhfBrukjmITGhmZcDRH1jxgfauyciYWiF6wggnb+Qa15
iPjawUsdXjI8aFIJw/ChJRAQBolMeoHIdF/mzTj8Rh6oGPNWv921Fndg64FnnUgpJJxx5ZsHLFuD
Sbk9mwYX5Engeug/W+C+MCi7llEjU78M9SXMME0N7HuvQYpVFrjRQrkS2DP/13Xnn/eoTlkNVRDu
vdEC4OgMBIzCQguGYwa+zYedb8tyILeVsz/H+36Vv5KnjOuQVkwf5ggPqzxamJDhsisD/YgAqNvM
e/3TB34fqmk1C5F0Kj9o/1SCwAFXToQgDNeD3O090cYTjSjDqJ6hKULGJuH+POzn9losdjbnTtvl
LrcxuYI4+EACODSv81ljroIDjuKmNwvlDtHzfvVWAFQ++aJ7d4HuRhsv+IiRwbUp4oj8LlTs7cZT
ykH9fYHPV1IoEW046Ush8UeBrxHV1eYlQ1E0jN/uhT9z+jP78WMWjdTRLMZWYWT+g8BUO7gPXDZG
3KQ3LQAdNlevjQjdqjHWWV2PneyokWzjRwfvrf4nBOCKGZaNBGCUntm/U7mHX6sS9ecxCSeU3mRK
NWpMcY18OcU6YBwRhZWXYeY60Ji0Hj6Yh7ycnZjqcAg64h/et56pofKCGocXPFiipiuLvQjjBwjP
O337jbsXkhQq0uX1qQXIJBvqKexA3Mlx1aYG3pxAGtKS2zqskevocXRLyOs7wFPILRJfS7vtMupF
MthBGiQdijQ7pIXjapy+OCIxOo7zzmSaV4xUxW4pR4KbGBZVInOkydOnzx2EkpajhRK68Z+6ktzZ
j973n6P5kGg/QKOGgUjtNIXp8Nt/cDP5Wx/0wbrS1HDax7e60Y1rQkFDf7btxZlHEIrmQ+Lahhg1
cSpf3vhrBRD8vnmyU8ILQFyxp5hvfLES5PI7Fpkkx/mgjjspTcj5TsoaOZoam4qABS7+c37pqHcR
2NxONUZrSo7SRzPUcN6kW4IDWBuwgCJ7DH7x2AdhSUoTjw4yIeKLa7YYNSlba532eQe8S6ETbIja
G+hEsvOsWCBPqEoFvWJbyij0gBaaoCAoIBcn737CsIJDZPS9GFQ2tTx3e1QniVb+Ljn7vYcaONT6
Tf650/s2YY/hvf8vcOSowaGY+1L1j1z54E8U9mBkjW9sSW8+Aiy0kgFu+VAijpre8GcMyuRjeO5f
BiM5GHnKPqE9bfsBLg+PR8wNuLqJsq+b7DGo2BYd1tuTBwuW58zHmxv5qobxgqP2kLp+JCpGqznC
m22JsMNP7Q9EnYSGHeDEKDeXBaThL9No4Wd8jRsOeWRFkGZQEIBJMW0JtXK8ZOeXGkXWyAoLNlX4
4vDQOv4+lJLPZSWCBTsRqnptUJ2VfgbOJRRKKE1w2OJ1/liQf3O9kqzRKuNvmdTJ5z7bqMib5vDL
ONPiFpTC40IovV/LBwVjvNvL7WPMJo5GZrnw/FIbnr65ADLelxv7k31BXtXWiMMV4+AjgDA1Ueiz
aZAEUj11qt8WzNGOhukFIVzeirt4Si4YNagmiBAmLh97xtvs4Mb/fTDa4Fnu79CRzXOBonSx1TKe
j2RFc1C8JvM1bS8sFZSpubnFHDh85OPRzE7jcH+CQvWKDI5Y/MSswNfc4N2B6/ceikDBqitjQxT4
yFm1fAK7djGQc8JynP+KQ3UvuH8xpgfIisCNzd2fvTokvLQNk22KrHnI/1OsOAknEoJGCQz2IRlv
Rua1ZHWcGo4RcsH/KnNPbR6x+Il+3O695ZrDi8PDM3/5FwMtL4yUkiKKQKR+QIqxBsjADaReUvM3
da3eel/vGD+Sq58muQc2FPh7pQUzwaYZCz8ef7LQVpRdgmLf9TsqP12Sc0Peg1H0nb6YnMjie3+8
Hl2IC7BTp4Zhcj3SL8hDEMESgDM/YzF+7i5YH3ucBVbPShEgBYCGBQ7C/jvVB4J4xV5wYYu/7I0o
yfdUohMqAByunXFKI9DkoMBXL5ooayvfcMHU4NMyv/9B9i43AHRO7Umi1+x7dCuZpJDC3c/wvPYa
A14qUv0NC8ztYwjA9jXWQFj6KzdpTMNXAfvzRiB4itY/5UDAWjjQ6axlIzzLJ2WU+4KgMQ6246be
hpuQINFPCeCDjOf89Cv3iBjC8WVyR7qzglM48FNaMIGUbRBCIQIcSmATH5A7vUBFtEajKnySsMvi
GoGHVeMLBY5/PxaOhqpPl3I6q82I3cVVyR/bOIkfyYK/2B14jlU4ehbnXsKqye4EHPmPv04+MoIK
3Vg5EJnaEfJjZM1pne5zqo6V22un25zJp817wbj/EdZNfZD+D2zWPxYux8YVvkwzfMfuKahyA20y
+mOLYSV9HHUcBSwJkiI1n2BrR8kU8VcnEWZ1W26JW8f23UKRgLBsOLk0cdWjFL6ZWMweQRbC/x1b
e/aJHXN9dZR2hrp/F50vtUB78UMpqOiY1lf7tg3kBLT+vgH/FBCix31RDK2aJdFd0oHjgPlbNjmJ
JscvzrKIbbm2RFASb1FumaV6Z34pgFj7Tdyu5SMnwFtGZI4M+zuf+vjAzRhX0l01Jkwq0MOjwTei
4f4YFDszSnpC3dYAnRqvgjWbtDAKM3vbl2JHZeA6vuQ6mGbVOlRP/XWE0gnn8xf9b8WIDpN8hr3v
HwIQAL2qnSZQKuZp6MqMUk5qO5TAh4mKXa1B8OQLKFF1aT2kbPlZuEfRWJ0HwtzCVysetz7SaI5M
4dbJHpZvBdPTOcOfwHwKfawHbmm8JMcxkoOloBwSUl0gA/LqqQZZ2Cr6n21nrZnbW/9NlDXGj4DJ
qgmWkb4umEfAb/1ZJqqAA5zp12Yy4d3vPjPysXzk9Nl6y/E2JEfk1ErYoh6AKyb4Sj5KPyhr+tyP
PhJwZX1mQdpD5jaB/nGFM+6nItZKf7BA+LQYGSdUmc7f0nKE/Q6UgwFb6fkbmetjhHDOTCfj6hz/
oB/ovncYKdGj3Yi9LiFeIlJCza6cGFEd1uvvWFjGFxEjKW7kTdXjGY3SS3pTKR2fdPM5Yd2gxbkr
jyco8EoJphqK5oGk8n7/9mvdOhGySQ9j5+LPKLAEudFp8BVuhDZLRfllQEoqyaNJ5SUzlLzv84rh
1V+cmmg6GF4Je/Yqe+JqrFhS0+/7KtzSx/YN8tR4V4zZqs7YWhxh2lA5Z37y4GZxiXbTSetkhZQC
6c2e38SXn1wLLu/bk/SAwVTCeWgVE9RiMTkaNeoHIWAFAeBOfq0NmiSYAm9LzTqAp581vdAN6izN
N+ik2wZukVwoiAjV32DYdT8LIMcGAajy6HGNKlQNWDjFzUVhn6Q738uzUIDoSxTrjQwjaMVPvkG9
iOeJoIycpeoC5LmvSWxgJejz7fpUYjPnXXB08TRUCjia0TmkiGu/7cWE4jGXkD4l+H1SZH/r54oB
Hu9tk7DfpuVfdbT27b9mD0Nd2MDIfjACPEuzWOIAkmtv725Uf2q52Uz+meTcjLPbh4R9MSyuOUie
wL2gTBujYxIGpXgtDrE8/7NBEuvFxtzIPQ3q2O1s5UQoaROcxxn+nMknBw6wDCK4GJ5HI//xlSPM
pKFyjTHqZCJJ7t6mrq4POPv42p8JfiO6Vnl7GTvpYQUg/fstkjDnvH3OZ1Q6dAlvIexRqHrB7Mjx
Sgy35Bvy65lkbGeUiR9lSn1DTyeR2QrO9M4MlIVexo/QDQjJ0NBK3PfKsE+85Awzpbdd6dFKz5g9
0o7+iXt2n8P7IIAH05aOR7mJAOUXtTvN/jtGVc9jqMB5vbXbal9NMtm2IseT55uOSj2xwtbFfZnH
VAtl9NJ3qqL2/wWDm9nJJ9jS185Janbf8qdpRhPTe2ftsauUKI2WSxCT0RauIu8smKmFw5Xo/GQF
g3J4MxYYRn8DNAEA+DhGBZ0go4iSBG96a8UW0OdTDJ83VFkWB3wTo6uYUdfPXhajupgcdv4y/nOP
T4PPVFCnUnjDrcAmYM0qoZQCSGQsZcicUKDoTy76k55Hy7DF1vtTfaLqMg6OU/Bt0JBkL5iHD5Sc
On055u8Oyro6qDOokPLJLHjPUR4hpSnxhMF33ioCIL+MdOlplXL12iFAPzVZUQ1ciTMt3bYOwD0P
CZRJjE/CPE+wx6au1ee1IThifmA7423mSccyTmgagxXu+e9DkaHvs8m+FRsueUmZx0mwHkOkFynU
KvQ/3ovqgx2u5S9Yb2vPXD20RL1Tpl0EUboGhl/K7KvTXMBJpK8VJlU2qTQ0RW46gBziRYTl40fC
PF4JLW/Sa+qyM4iURIJocHFIue9H5feKl5p6olFtW7FLSHh6y6M2Z4JUAuY25+FrXk1ayegUKeM0
cxyajwk6bHrmH4TJc7oO67xKzpWyDirIVcpEAnDreEKa9zwMsLo0yly0ZowVs8zVYPa3mjq14BYB
h+Tw2PDeD7dvgTPHtHIr42Zk47T1qEHpAq0/1grpNifqrtQ6N178AcRUQhEH5G8RqNO2i7LZK5f5
zy9sqi+L1d+31v3Vw3B7klizmt4Sss/FBQGMSPzj2c6fmH46Vo2Dy4FnKXsIMB/dspzr8r1Dx1Ws
cU5m6gYhyyk3rTpZ/5iOiIy8z5XDCvjV/yla8wPiezvaGdNSPCV+sO8Jr6A8oHt8qJP1NtSPF/PO
EhEbHj38i3cMXZnWtAp/0xEz0rmXpQpoDzjswzgWHbDUcD8yhHrQIvXFSQjvIWpsaQhcPwxjBL2b
i8OyzvUOn4pF5dtFD52pmEjfXlxx7fekwJmnJIMA+kLDr4hNj4C6ubtSqMahcw+rVPWXKhjzbXWW
HXNlH/EesZX3vkgVWjbnSLYvA1Qk8Qq5GI5FEesWVt3pUlDaQoEitNRhxTr6VRW1w0rh7XCwXh5p
eXNmMUWs1vnSKFQQQQi/Fjg94QWKd/8YUP4fLqwWoPwsE28iKDKk6F99/3OKh3XY038DZ6XTDdA5
NqBdlfQM2EKM9WPAqx7NLil4QYoD1AXAwxjwT9E4oqeoehDjJn5Ml6O/we7iJGA4ouTLhyNk5rR9
NmoLh8sf3wh0sqhiIGsrFYqfIA9S9TSGv1eHMCbBL6GSoeaRLU0Hv6leptLAWIrp1vRGZE9n8cD+
r3wfNDDxFzH7TuDzZFhpGKNEG0hTv0GI9qEiMzCQcwR9RU20KhyVw193wch9kwgrxzcFbTDiiL9m
IPCDdWkONawHDyR/n4ODRr99cKOIM4G+tfXcOgIEtfJL5V7OftEpE+LhF8cLRtBP3tSCFTIupwWQ
KhjJ9NgKhjEctdljZZbOLo0Tc6wPa8AstxR+EL9iLkNs61bLhtNFWhFZnJdd/NDEKH+E89INzdbm
0dsw+Dy1vdy3g1dm3H6gvWywU/zAfLFS2IMlG2edwg2Y0XA1jWv2OJtKMFOkpmo4A+2kXgoZlypc
mmnEFXhtA+yXDaOVzRAhS2hd6ik5PHEOnzzAmfRZkvWUeap1uXlTeZIXIIO8s/zSr2MD9n4xdiL5
gNccuID7l2qxeYsw65Ig01/eUGMVwY3yfZljySVVdBlzeVIf4W3s6Cc7xrMCPPVMuC7F1pEtflOC
Kr6kDYT49mgIBTsLihLgPJEt278ybKd2Dr9CH7cnW+4Ki64VLh263ZIpZMzHBY+uzwMGGtlyji17
1JxKQF60bOqmriO5yRLeK1OE3w/52cgOhVXhSzaRJxJKE+T/d3TMki1tztDCtjpYCL/FgEwyqbo+
9UH0k3LNJKQgPe2FAoVZghhJq4k2cT86Bm0vkibRaYXeDXk1XZm8HhS/7up0RUtylpGtOQxW5A8V
ocbYUFdKj0WHK5HGWAAjzBsc4NuEsCrjKRNQwo9eTT5mclDjKMUHmky91v24Afh+PtOAkjEisQ7j
eIMa1kth8k+4Q9mvxBQWmbhsIllfn32UoPASe8l5NSKzT/kb3X0HxpTEM/W91uHPs1XFSXa6wufE
HLnVpKvckgaDxUOHW2GDXLv6+C2zJN80Soe/KaHAwxFgseuxPIX9qAXJ+8uCjV4Ngw7rPjrWVDo9
sZFXBttl5sFywLRiiE8zBvT8VC+OotX4wT8eLUoQnI9aYyVKnEq987BpGINH5zJMvA3taaCTP80u
Iub2jQEGp2DClX25Pyo5YiJlSycpKUU7Ur4JWttrTnKwSgcX8+YmM2kQ+RkoKo26BLkdva7IkDdH
Q/P94umwss42uOYxbGZno4h4vP2JwGN9Nv2mjKeGTismCCSSD6dlz8FRDKlw9tZJny/onaJeV0Z6
O31jq8rTQsaYmsINXOtvrliktwBk+PSkVxi5uHMCxoYweSgOnYo7uA49qyrovHrl9GKBAhoXP47p
oObnel+bUl/KY7VI3D5+sNxwbhQd/s+Wg1kbkiucMz5utQoMrK8NL1LARcOC+Hbn+JKq5oVInIcy
81uhfP3NIJxvt4KpqZu8BlmkmAaEzglS17RY6r/SGuHsU1akCMDcLGuhlASkVeszR1s9ZYfssXtC
grz7kcdVGSx0KDlExFVvHJJHbAnGvY4foI354i4Xls3ZXRv7lPJxbXycXKE4T0aRuQtdA+/6o0R8
DOKnkaMFvtG12d1lqLa+iEC/zYaJMa0TJpG8AE+DApcq5L6BSa4I0Z+mhF6LOgT4qDoOVxxxw12+
bekFQuZ3Flcl5NYK6b41ShYglDdIEXjaKkhRZEjT1npQsYmEiKbfgV3600efKUMfkw1I3fzjoDGe
5QIL5rRaYK6sJJ48p57kZqe+gDlvY+XVB+Q6AtDgf3Pf/xVnluCxGLngrUN6WFD8fJ5P0ehzczCW
EOJeaiKWCfFguUmqBfbzwxkjPzq1KFwrYjD8F5sINQi+Kft40IMes9EaRlkrn/rWVZghfJsY6wCs
r05NCnUMT28Reekhq9L/T4GvI1w+bJ1Limu+UF5mEPoH8S9rehOUCvpw3286EuRAZtpK9qoX1NeR
OLV9drXGQ4/gv61sv9l6E+sF97aZECg0nc/ZwyY/Ptohptfae76WmTuI+LN6bwxV+wVDcUmnUcjT
ikQFAIg8PiKhUoPQcpKhVyPTyYGVMXn8rvfKD10GWCfrkD4iIA18YTkCcRDvYl+iwet2YW1xQMGt
rwZDQSnco1ALdRuZVe1NvUWGRhbvviXrDuP5bY/f/DESpRQEszCupxLmb8iJtae0Ngfrny5fd9kb
O8zsBBo8CrphlmUrp5u+UytFADgpUvI0kb//u1OFxr8Pco8KYOtpcHYlZjoG5V4DrJVndMV2dMKK
lZ0+DPLXGugGpcIHOfD9qd7+LCRdhkkACNQWa1lvOZfJYpBR2sz9r1JT3kAMucQhOCp8jKYRNLIK
vIwXE5Xepsd45/46SaXWrioFG0c+9BwAJxplvNnbxsgmBdBasBYZ86VgyjQca+E24VoIc9bfnnem
hrEMqSRtwG2LGwqQCszpaYr+nmKGrtEzsf5WppQ+O2lLHe3ur7pdtG4F9nxcxRbJA/cC7f7bFVed
VcnvD9/41w9JWGMfwo4K8zUitBfa3Vpkf9y64QYXSg14zJC+w7B4LTO4ZZ+29NZ2fmht6tMLHqaE
s9HwZ4ysSRGw53wmSkQ+wa3b9uoqAJiUlFmLrSrbC2upJacuID+G68SaRV0HxdiWXW/HF7eNMSok
Av7Bk6f+qJ1xZihmDSiQPseHBAbAmgRzkyEe+RGqQuQFz6Rmywpil21BzzKFsunR8UVNAEMNyUr6
J9g6tvDv/ABeM6u/J7UkOZZuqWAeRnqIp7rGN5+FWThoMZGtQ+umyXA8/k8UL+o/b1YloSafMtNF
9q+mBcInUU2uHxnDxGKeLrfHmEkOl9JRoOdpvE7XRBGb+LKzE9I+NnV3HTTMXJGa6YI2aNaerwmV
CT5+bGeMX95vLIs9EGX2MuNkSAJE/Ow6+Xk+utQJJ7z1Od4r3bI0YmskhIZmA38pAjooo9ie+CCF
ICWFaicmVTfrJ04lSjnRkvzHRAWO6fVlyK0kZgVdgI6SuRx1KtZHi6XQ+/uc+fibVPdqf4A6o33b
0SNbn1MLyWAFLoP2Qn4jDlUaO9amPLXwgpsRmqwLIoTDH2PO+NTiDelPVKRd8gGJbmpOMhpz5T6I
QTTodX2ROqNodhzi2CICjFCEMxbyYNgn2CEigXDMmhLj9NGW0uZ5RbjYhLjW8YBzGug3nlFq53Z1
rpQVjjP+q3pzgtwrOxlU3tNJZTpkexglX4Ccgti10SFFcUO2erKxo6sfmEXgbp+tUmG9Y7fvljm/
loCc/hQq2aL1oGBIR1m4ey2jPYfe6j4ltxi/p/58GWh3ypWkunVT/cX9tUVvjMQC+x15feA53bCT
NFJqE96DCKa6yA6JyFOpRYQe8OuOANd77lEjPsQas6HTkETjk0B3IPF8/EaEagsdr+P5bEU37irT
+7Dgb9kGy+AxhiY5JdblPB2x/yQ2goaK0qFO7z7c3+fsTPJahcPflGyMjI4VpdnPmXLYGpLTZDJ6
ZuD58BK740yi6F7KkxP0vOxapF0admrscEpcZ3PFjycPrP4Ytv2Rc1Q2H4tZRyEF/HKxTn7rCdHP
Z9sJlsBaCxCtbfZyIapS4fRAsQOE2uOL/pWc7QaAhsqAIv426Uwc+MgAPP1WCY4m/Ok7faN2KMCs
UE4LsOBsmLOXKREKFNzGNzidtGdVvPrrRyqU3NZU3EF/6mMXGOjnSYlJwP3VkVw+FwOy4iaXZCbr
fskItcvy9k4t284d+ovcIlKLX6Y6Cg2rRL2pDprEmF6nd6W4zjXUB0u4cQ/1HuZTeNIpJxV6iNBo
nOIK3WW4+eajhcrBDza+77LgnxKJTXOKnOnWp9n+sPvUltOtEHsEv5ZZen0QVyUsjS8VzfM6HmXD
SwhgNKtHZQM1RLCoYg++9VZ6ZqZv2431XZsSuZ7R7NhkpBbeyQauYNxrIG2ab9OeGWLoRoNC3Fpa
O/TCKhBnrHyRyVOru5XFQRIoYxu3w496gyVNsKHRKjGz9Vpqwe7E3B8yvHwdLMd87Xm0uhnNiyZs
Rvew8MQyQliAqXNmjzdXSB5SDSBoDuRlMCDHzCIdzBigoNQRK+Uqu/F8Y7dNbNr7dH5mZaF5nWWp
W2E5F0Pmi2PmcmhuC/br/QliXvs9r3d0N6A/F2s6/Igr5IGPzZSk0nj0bZOxR/oUn9aiEJVBJmIK
7hf6Hz+gj0kEWn/1D8XZTFQeAUTC5sekEPmu21RAeK+N5Zf2jd9fauKzElqRoU99MdlmJWG5nzn1
r2voWBRhNbsvczPO3djNFPb1uMXrpVI54u1Lkt7uKydSUrb7HS/IkXULxxtx65vVulrPsBkk4hGU
pUIdY24ovGp58EU1/+QpJTIpytMOFoIYyU+p/hF1/TzUbMCtC4vre35+UFGDCO6+D5xmqTbqgm/b
6YlxZgCIrq6af0UjPFjtEEPRbkcBvvmKclFsQ5UddCySg2v8GY8NzR6JL4gjuHFhmsim9qOjVEb0
dp3TbBz7pPLPtXdZB66sFxyYrekjW2lY6UCBOrobnNB7GRLhqkdpyLcwiheWWANuvYiEwZ4zmZiZ
0RUF+0ykLfpzlLIzX6O6HIzskjkmqz585ROK4NtOHUwyiHBlYB7i4s+1l7vjLkFn3J2nqRY2BbCQ
1mdoPhT2FwoiOSrEvAnPcYhLbjun2B9eP2pAQfR8lLfMwsEfGFVc9YSv7g6gvHOsfRLOCkEu4EBH
ADItDgz4W4O/TRxEC+xe4NeGzJ+PPg4SMujm66IS4H+4Kr4zZxYFGkrTPpHw7RaJP31pc0D9gSp9
feQeIXdb49hXDY2n/g5ywPAqzAuLWlvFDynuF7VonU2GoqV/cZ21POH4cjKQcG6kgeggcSe1rMvi
hI1aO+JgXGe1ZnwdeBHefZEOooTMW2QxgbSEs1zu94smlNOltkzZ0KAmNgrrOwapnn72J+ZydoDW
hRVQtzCG5oMoyq4qpqrE6JD4D8W41VFXSYpTGQpHWpLfxbqHzkDDcliJL4URlZBRL01Ni4i/yC+H
3RSPDM0at1mXjV0arBFgDEyNe+tFGjiE6CmbQfSdCq/QTMfseYMW7m4oG/RXKNhFAR8/QRzbNERq
mI26ZrIlBarTi6YbG3PgKgFysFXPTt6li1Sg/cxRpU/IzynBqTVBk3AoebfTAsqrNCneSiQuJ1EF
Ykg3XGSLgQ6+/uZOg0fCA5TZXj7Le374c7Tf+DfxMNvNq8/HY2g3oNNBVnAV3uohFk1/SIAgVctd
GHOCerWJsc0S/W2Q/r5BHWr5QbVBM7AO4byxetnkoYtHPibL+xUHNFNx1ySK348RO1LPS8Vo0MDK
WNiPkkcOFIy/p8lIPhWzo95t2YkZI7sitjNEdaCef9UhCffB6bBmtjj/QoOEJ406ckeB9r0+CGfp
bas4C779YxRQiY58p3Hkn16yX40ov3R/r6JjHoAvqKwkl0VpVEMQLy/1H/WBVnDix8Gg7xMam3x8
U5Bspk/wWTepkVjlKpeQ7Be4rQGI0QaZs1z7F6Svq9/qhmiTIn4A47nIdJCYiskv2VXI0oe+fDOe
6Zt9xWm2LT0GA1xz6fQNfm/tQRPn/WCy91SDFczq83MtPzg7uOefJlqHGIL6sbcCQPIhj9fLto7y
rhJNp6pnXdttFksoFh++Bnhoj/GDEU/JMQSgBZy56/IC9TKsAqOiatesG+UXPLCxxst2K7MOJLZm
eeWomRiZk7lWpo5P7y7jRDE31SPumUfPvdMtiRrhffpCm235OTo+jCchZOYNnCBZlF4AEqjXURb+
jCZIm8bTg+7kB23RfLGUV25K0hLPkj0LRgE3WE7igsJN4+tsSlSlsgsfcMPRCnluzszcI99uk9cO
pnyI6nUDE/AtUGCCNXYlVBTqwbf3jz+fvrku25T/gf45YeEWZkEVgKxmBSNq85Z1ZHrdIDW9M2Z9
t2qmAMWLU0X5hAdFzeBxC8U0P8slKihvj11glhd3Z8rKyP57/1keNGfQH4DUixA/WCibo4m5y9Mw
UKXAiDPKD2G/6BH0YYb291SbCJYQ8GYYL/H7mh99h4yFVQrjv3NrXG9HV0yZi3Rnbdl9NoCRIL5Z
leWE7ALiKpUKSbcseIVkXZ3AgpczJNTs4ScQGztyIERQuJZ3hvWi6Py24L8Kv30C9ZXBh4a2CbMR
F5pLNf/iBu2xljnO3gkIAt6YTcUEwiWdL0NtjXHJ4/ozY5jHeM01G1HI/MHwWiHNkKBJNudqiM6U
H2IH358gdE0WDIOoXb6aG0O+9L6glFDV8WaMzFI05S5+C5aadVCXPS3cWFoR4OkZBt1thJ3vd8EZ
YXf2lxDw0STAe83ZCP19Ed1X8zbmD8/1x8moxvj2UkhYe8XyD1dlLBnb+URfkvCCagQi8eOoGJ4/
JFZv2Wdeyj3tIF2Svdn3SjyKXnKuimQd5HDFLqwNiy5/haBwYoZUMtVbKDfs46bY/KEzlwlzpsZc
0Rsi7Ayabn1Hpanfq5uyZRfogGlBwtF3Z/WSV/z/seVBd/ZNLVWPwEFGmzZajuY2/mF+W6cu2HDS
AKHdAd+GRSMZj7g+bldrqb5YAOD2DzVhjyJbaWY6zzNJpK9K8CjMRGB2uStpYnB39YOykdHBX36B
RyO5/IhDqFRTUVIF2ujkW5n3oaEgEOzXnW/4LL8OBZ8M9haxSfRKhrmsu5I2RKbBPnH3okF3CJ3n
Jto8V+2lLxNRIcZtmF6Ayf05iV7ma3xjux6m1FZ6lEz2CkdoRL1oeZXPj5oXs3Sc6F47cjkRMcwB
LlLRmSZkGjlQ+naMb/pYdD7aqvE6EPj85kiJxiDCnH5u1nfBm+AQzGtfB4wSjve++xYSBnU25aUy
Xu0nQSz+ZODTUJO7FX+HYcvfChGnZFfgYH9xF/lH2EAfdR1+1fiFRHsnSAf43GA5KTVzzIvzY1QQ
Ic6wfTtPJ32K/ESlwoB0yoMe10KV8+Jl1Z1zFVXciYCZbBxFDDUi5Md2GYiTMZoYpZ+DOhYubGsy
txpsusuEGHG2fNr3EYZQGH8Frzdp8N8zDvDkMAbJxZ32unxRPvLk7yRQpWp0mC9zyRxt6d/sj626
IJoIZezeL3bG24QCMIe7OzmTO+eVAd+osVvDuEJZ/a6AKOZDMsmWl/WS1p6VEzW+SXm32TLylo88
n/cIvM+QlMouaBnuhqn8SgdzSsuPLNUFZNQX0OKWZXG/+Wr/jdw7UaCl/4+t7/sdvXrbFM+yrp+7
yywLAWwc5l84ahlMK62FZcq9OOamWZwhdCQ77QjkgGph/cL/sawoVxGjVwaIqjRNPlnXDzBmZAbe
9GfQVWvVrYGTjevuy+NCguCT8i8hq8/v5ldLzL1DGlIgttvo5MtF9ClndWgpadYeTe85CaS0Fq+o
txFAva7iOAhStYtYXdQvmAd473FerkgOSVdGd7jeUPq2CU4Sq70PJR/PnLE9ZFC5mveU7hOtlJE/
or+ixoBpWt2pyjMCa4BhKSDrAwm3WXT9UNCkrbfoIoey43GHA763u7XHmg4KiJnri3W8hETSI3FJ
4ZiQH6I5ksSZ2si+SgYJryUxsMD7IHuoNhcVdZTKfu5Va7V1Y0Rz/8fYGzkK6p2umYNJ/apzdQLf
VqPSZFf+iH6XHnfP9x/hJMUDdAnsIsfzGtlvriSgwku2Kd4u6smHMYwZaHlICtRqMc6df//ZOm6g
0PKa3IcRC/GRPowosPFIuSxA47oY9FmVukmhRwYPWsQeFnWTSmyl1al4r7dCh+nvDO+Sn3KdvGdf
8bfE/8+hg9eO0ZUeRgQNj2pau5hQ/dZeDI4S3Oipq/7JkRepCVWzrPzKCxPpDRoi8tQAHq9EIu6E
4Cw2/EBr2ummiiIlHX9julVuWuIluJfLnUsJTUj8FjP0SCVjOpgsBM/bsNQx7C59GzYyEPEZalKD
4TBpg0zJfQfMB5sq6mvi5qu83F81l9nBBdyn+B1aDwEAoZKKIhWr3GflYTcK0T/qjutNDlmLGT2h
ZBys5007GxkX8i2h57f18QpqcHksVwN/OgClaAM3uhvpd24mdOc4D6Sh9MhM5FYxVYShBT9Y4vAA
mrlos5dvwG6gvQWnKZOALPjU41Ku+TGBpFFr8Nv37X3y9Ac3CSucXeNWa80SMBvj4MOvew0KDwpa
DNFoRG1iMXUIb41zreZjdGiNg5xVxttMFFH5R1Id2b8MEHvSEW24fKBhOKse6IiVWxqcuIamcvC1
gs/JUSIcy/ta2bgbDyiiMPnNhYU8BZ7VGTzlqpBHCUUEXpT62wh0qdJBykUGmHWJe+NAZL9Cl2Sm
u/FKq1mZUpbGMDUZdWofr/C/2hK1DJJlfvuIRgF3qEjsBZm0TGiKj07E2jrJS6cEKiQnCUFaQkC6
5n+BL/kba2CTI3ZW9Hjnrdet/RvyTEjyybWbx5S/VbHyqZNn+OoZJ6Y+dVXYST3PO/x0PGUjaUFT
PHEUfu+JE4iJOxvyWcgG/ZZnUFC+lWmuVP5fJVPHZPk7vgGhcil9TpQV7ugWzqhGZqNg4Dd2wIau
FLjLayp/rRKzqpao8R2BdKjDNLzSppNKT3yjeujxXPBfcGKSrpecsOD1PxPTe4KvtdgpichvI4TV
f9dZsguL35nfRPR4NmKVMpzuXQcVLKB3A/6F7JlQwRLF4ZlzKgsyi23gj0K7Dmb/zIpuTf1KkAcO
1CFv9Ucldzah/AHX+yLfJRrLSYH2LEF/JmFVVWL+N5RdCQ6keZGXZgMpB5Xh07UvWHltEWAC/tdq
z+e6YZLcUaOAW8E4hf02yvVmiaZ3sktxPGXDsC/PICD0UZo9C/albom6MD1L5J9ikj/JvvrtMpH0
kSYTapE+LN37an07XlH+kXfYfj95jfktXx8GWyQorjNecSsrMTeHojPoMxa+qi0sR54MW0DtrYeN
mBGX26kwODU+kFfJ+iS+PZtKl4h7RfyqXEfJ4VrupczRrq1ORPFstZzYbg7i2YIKr18l1qby87sf
e14H7I8gQPvTnMiy79KQycuBd2VRPfmi5Cc7Hb9I9k4KPrq31VLjF1o6024JvEdY8wSGRvynMOUT
j7V15x6h5oHY+4SC2Ky/J6v6l4530g+mZr9lkumhxXdBH08jkf5QwJ9LMkSziwSeGdoL11pyz5OB
aqRXYMHiJbhby5RJttVwWlK3PfAfA38x3+NZQe5bahV3cv2Wdf9jvISShjGs17OCR0cS1nrTLYj8
Iip+ogwfkrf6gK3gLBvnEd5JIBhzsu/VCicFnwfDQjzaNUnOPW+DYUlX2VWSOW1YtzfH/4muSCpb
/qIWjtY7gezfLpYEseS2vsf/Zv/1C4lWWRW+42+rfkQaKgPON3aj4U+LSaTF0GEasw6gDspCe5Yb
qIa/FXTNUXTeIX7E3xXxKQyxvdnkG1gjlTsaEV/0jP4lzDfBImiKb7Z5Y9Xju9bSgsOS2jyM2GaJ
+BJhuf7AXj9OVlfXJ0FnRf4muvmgnZF+oQnlk2PmVk+eRY2Ld2wdAUzNBX8x8bNg8vGvRjsglxpw
xkuQBda6YdQw4M3e/n2tWl+tI9XG2+F1OxJ+HRpfDmySLiAhFh14LmweLxs4ZtOkTXmFUxSRKiK5
Yh9V81FK21uEcreOikoHW7izqJ2FQ3kDbiQfWpNwrNMwf+oi1ghGWTIiJENKGKWO63/ZjIUT9wB2
vfyglBzmru42AEq+stMxUmFpAHFKWGlbxPELXHl0a7SnzWegD00lllZUgwEaSBIQOy8PFULGhXTM
R/ofxjaUlNvdxWdV15JcDTwVHmgWoVi1RRyLOgfwqxizh962BWMz3jYov1wRKCJVk+uAYGZZ3HuA
2MlhFf5WB2+JdWAvPkIRBIRNADqNf1+0DICOXFqahMAb3jkXCoR2tM48vfjnFFxQtY/kTRTMOXHz
oPlxGqyDm3KwFJg6tKjhj2iG1PU8irHnRI+3Y7tFOD+Pz3XjBgsViPnKPBRnPGCUrPhRSbPS50IE
O9sQz78gqDnTbv1lNhvK4Dub/9JrCfdTUxM5fDK3he6nqjs8nE84TR1XW3meCWmv7uXkvQdPmPKF
KBxYyUanYO/GIyEPokqf2AE3xNehQ3fFOGNsocQ9VgJF5Oi3rQtPW/w8ePncqYN9XBE+qh23yo3W
/ZkXKbdOGgpyPcO4bdA1VmCbVa8usFkkz3oCchNXvnaRlnREfDHAdpYHT3yeIp23fnQYh/CVUab8
96bmELWmti8WrdE8H8nwi2gTAo/kNZpBEuXTsGxh8DFTLw4gMOMA0bfCOF5DzpNTKXRi3wwQLeoB
nIjobkv5hmHjItcwjFkZnN1jfMNOLXDip36o7zVeFXfgCObUunP9B3vxsEPIjUbtQyep7kgP2UVn
gWC1Y5JYsmgd0GZRhkP+mpyal2wFzQdUWh6ALjouV43Xaeh9r43GMt+EWdeAIUJ/GvW+7bugSuZY
lnH7cfch6fq3oqxtD+TXDa8QbIjR3vrjO4BHuCel5SNS2TLxiNHaXYxHPjrTu9flqzR8QZS5tCO/
D6eQfG3j4JUndQSdUzEO733wV8P6vW6+o0h4F8jXsittFGKzREHkQNwyu4EBxE7wqYfSSls1lXbZ
x+pWgHzl/8+jHg5AoR3mI2o/6NCihNJyQgGIP2uHVIRPOQrADdKNDZwqxsSJRKbXwaBaEbmQM5/I
m+qmLteW/j7k6zBrIumXWFjw1iKdj9OC5iO2fvsxi/CVLjZ40v+39ZAk3Z8FZCb7t+Ln8qRJtiPr
iJ0ML+DsXHDekIP1TiAug0eg6MpdUMuXpxVYXU6Zpinx15mXskMRbfevt8JMDv9g3FopCw5n8xZu
qsflZpFQscpg3wROqVTbKC4PnqPXBt3RcVMRCowIMY6o8HeUw+nfxyYvUvOtGcvg8MA/2zO7GKsM
f5oVdmCIpav+YoIuqItf4rgQ29PLclU8EH8QaQpSTIaof9G2uV7gfOn2igFJ6IEscPwO48qXfYGx
NSc5M5TbDiZUZ8PCEDKpVfgKs6BHTpAjkI7N4/KsseI1z7uMVGJLcHLirJuj9T85YiEmJjhG4kLw
dtvT16gmWD/T8mfT8jpnxAmj4AR1KGIh1TD+EFncO+c3FKicunvPg6EJxsOPnrWIIbbez0CLSi3V
Tsg2BXra0lPIaMP+09hCzMAFPOvvV0/ZjncLv0gM2QyBixJ4F3RGuGNOHpuzV3ZR7wUiYg/YoUL+
ny2uIaYm8n3EWDptsnkTHDLAfOhX/wVZRipphzMtb/dIEkfcLTl4isIxh1gAZYudm/Q6m3pBJurp
vaAGuYZ/T5/mq3m2k2qOcYkgA/DKot9a4ruN/GVli0CpolAVEod2niTPCYaFhAahiqiuzwWTceTf
xx4LbxCs1HVfrV9YsvnUf7ixNHPT1V+lTeonbkcuXJ91h/Si4sXV9l3XcfZBc4oRx8zNAO8jXSCd
a/gnbnFnS/lcvE1Ip6FatfM7w1UfKuFn/TY8rjmfBcmXgeGAq1MbeG5I0RuysXOyCSabjb8UEu2R
wLVOk/EkM+Fr76X/h4+2EXVFwIXQ83dCdqKGWo2SLePFIFAbsmUA4IFE66az2Uix1Qs9V4T+vnMr
RJ+CSPKIxWT7KQh0OoVOO9Mwk7e/gcPU2CdFpxDaT8qLqVrGT9YJauicAsiqpHT4nJJtMA1Oo3oP
VcCidvVlLg8r2Lwte4kwmp7RF+PXd+sOnRDgv1JvCys3iWyJW/dr1x66iCTPRkGyT6/IWm+3oXR/
zrphP9QtJy63u15GzEz/+77zAGZkW0B4z0UVjhg+Zfi2nWXpyIEjFAGSGMenHK/MGsklo4DPfv+w
iii2X9Dl1uXrU48P38q23P9RlVwROIxn5pcHk/6FwHoJMWxFnf1zivk8j4ZawgWNLzkLAnc+AuhX
VzpzV02adAYZ6myhE1Rv69DcI04vhYzDvqbfT781LKWpj+JRBGfWx1Uh1rNK+uZhLGKZf4mB+ciD
3UCrfM3OABiA8+oCej573Sfljc3EsqI5yL2YJUWw4EtHsFx8OdOmHxE8kXpgfJZ+tdA7ITxa/lnZ
EfX+c6xHSdfvuZB7rELNt8HdwxVsJfpVprGw2oeU6157Ft1Q5uWqwGvXyhcT7g1YaQJpLwowSChk
45qIPrGv8NuTmSfgPvkjLkEBddV4bQvYYLBV3rm4LpLLk8Txs8Qz/pmR+7Us0shDmzDkhcqky/as
8NWL1aEOTEXs4I51+OfdGWfSQ8SUq8unq8l/bWPNw6hohLGFwulifAU4P62Cnjsl1mY+hK6COCgr
YXkMo6/0h/oa0TcJ/afVleaCMkYKlGt0Y5q90zbNUlqsRgiCb+pVs4shGXL6pwcHA4yx3jzKljZO
b6/QNmvFsnaMENJU3ZgpxaKDsxy12wU80HwC/LGP61nFpIDXXVMBYOD1FJLKNnMgnvwAVTS/ogh2
Iwr741om7MK/BtZ4KqF2TWshnCFCgpDp8Bnhxykndp6B9FgAu4EtXQY3WmrE54ku0TOwhF/FcOUu
UIhSg48+j8GVeS0O0GGrS1S4de369xXvN35GcbDl445KBXlT0Y8B+9d+zx29VbjTGYc3c6qcCHnt
IQAWc5S1T776PJGiicAssvh3M5c3gP6mjn/XB0s+6yd1p0XqVVQ4aIJvvSo9fUFI0gkTOdIcSFu4
2/KtgoupGCChjctZVzAngwomN8eo+2WzC49ezhr2LhnCtCbjx3RwatRmnUmM043CYo4EqrJSdozx
zX0no60I+dT4N/inZQpe/VHTyQWuoIOWjarhFfX21L+mCfKPeU2Xok2FQ/U/siDjfIh+o6B/xnuE
Zla58pl5+Bw3or/TizvN/Cicf6j3tyL79UIVIEFlZlqR1opcwWnnIaUYaMPDg0NASiCK0lE+st9n
t855P/NDqtfnOBfQkQ+hG1fw7c6h3fTmUb84L15lq5szA8wRfqLDscVVfu4FfKSS9nl1cwUmDfTF
LQcP3oHLT1f6qedxuzSh6p0kSc5LQx+3dbb5Oz4hWRHecFwuIcyB8lnPeiPq18JKwd72IH0cTJUV
RwoeUlpgu98gesKbfsh6+JaVo/zPW3dUK4W2xrT33Cewv1jQcmbDYEBSMM3v2nJM8LZp0yIua3DU
YLKK1TlW6eex6otZGganyWqYZLia/jXtTIcsvtAOv5iwIezil/TARuZbiQsbACE4OlyNgHW4Zlv1
flYH9iZ+dxefQi6kuJ5oqCzVuicgayNnskpSuHIu4naufmADXZf/fDjGLfmLrxND/8mCRRPrxSbh
vLYnWYnzHDm5BcAflwqVIM4evOsO4DEB6gQgZLfH+A+w39lsM5vTIWqtssu7Z+KPgvihgdgKW4Wf
j7+667b983jDy01IQMwMql7Oo4R1svMMOJ0kDx6Ikb4Iw6x9bFqwC2hGpM6S/FFBbaokxgT49Mt2
f9uAXeQp95Vts4AJmdTc/dbhNtAVMHhCIyhs2E7vLGh4dLxHAzb1IRobde2H4ClPPkuW1x5EQy5d
MvvPjdsMBKlkN9erMCn8b2hJm+ZSUdOVeA1IBKU5XR356K2NhAto3P2PBHw3UDuWDUPgZh4JYTe/
XIgM8GAXesvNl1SlKoGfNAJfrv9y45rH+YqARklPnVUJkVOC2AZq2J17ioDyh4dK/8mYlfhWSvH/
p57mMkoMHQgx7RM/JAIjL+aKAdtlx+mK4wRMj2punLyQAXwEpAG6+5bLGcHEW2IrQE+C3tC5SDqV
+7XoBFXU0aimxBggeIDSPdoZ7MZ2mYI7a+DwYCZTtDrthoMpWHIdExc6oBjYFEOmKqpFsKEcNI/o
b5kAuSlsKAMp7iz6aky6c/da84vGuN5V+vV8A7Gxt0FFV20lQQ/oeVcpM2Zw8+FipensOgl8LCMY
K/QGBsX1oKJ5B9BtVxbh8VQzRuz04On+4maiuMBdZANg6OvCsEil7ot12NN8Mpa0eGM3AvTTxFH7
6mcj/9h/hoiOJMzffL7vZh+iGv70vvWvcG2D9/YdGUjdJSlR6vOZ8EQ/uHbuDWKNFdxfACjPE/i8
a5UF2EdFRh9Or2OfgplDnnQjwnOJxgGOyVJZaymrVRtJOnQSX4Vat8aW7WxOGWRvHrwn976kPI2u
J20UWLaIWH43v16CFd9jBXnzWbT4H95+QCiABh0uX8yjSrAkKvXr/Dy+MTvySmYObUx/OsPZ3aib
lg+Etc/5HqIopLZ1m9Xw1JNv+f8VeE3Gm3JahWsVNT6q6jjeCH8E5YuXprBK0hzJ1MRl5LXisFXA
sOo/6VlqJDGepei50U7GYONU2PGdkpctvYQUlyS0yJBffDI8p/inX0nl/LaFqwipnQlyWzK2mw+z
NNvhtLXTfoqLsUQtB3iaZPKpV+WO2lhQgergjhBIbFK8241JMgmCpf4Mm9EaJ+PplzWWj2ZmmNPa
fKYfOSyKA9jvuPFpPjgTtF05hmWSAXbVxNct/+1CXVfUWExx6r6JVljRKSWWWTpDsu3oA1idyUl9
LC0qZq0IFv0F+2b/sduQ+wMgyA/WXeflp7fxRnxjPhR7Whdgt/LQgYUpTkryRFtj/AzY8mq7Wj6z
xC9Fx2eXdAov6GuNjAZGnVNMUVIsjOCp3mDD7Sq/VFQG/tSI1eajPPSrkkAARtmWTrPnCG3MLKGr
VONZrTKAt83qyhWPslK2drErXxPOgexrDVMp1eRadE5mbxJ4t+BHaOIA9hleXqZkbx1Ig/GhfF/c
rYkj+QETAt6W2iXyq3F5gsga+8vjPxFcHY4QGiva1bEb7l4n6L0JW00rUb7HTl/phksyL9vMks9s
Y3ZiRNP8MRBEzeiQoyWV2eTnwNj8sHFoJcBd0XWG+Rl+ee5ep78Cr6OznNhnP1B7BPj0mraXN1UD
2nfzywf2nEj1amBo8yvBP8RcorfPDJEjED3j4pJL0/imHaOALP8o9KBk5M42ErjXiN35cAOubVSd
UA1a0xD9fmFd+n4ZwSMMoPVfqhs0IIuEvJ6lbVQsO3vL8EoIsRiC1IvQ3DJdpRZOkEIRXeJaiVXn
dTzJG5C2Ig8yL+WLj4Gficq0PKUXLQvGiimEs55zSr56zJuZ4o5HhOc9WfKjOHgF1TdRj5ZE7Rle
eCmQgHnXJoPutaYU0DgE+0isZISlTTbVrvcE6N0pMYOHxiyzrPBoAaX+zro6+EkJk80SJLZeX13d
PPECIeTR9nqA2624IdF97dHHJsNhVvP6XOZNsKQJniUgehBNRuEGj/ySggGSu/KFLXdzNS/oPzff
NYTBLb756FslpINoUxOLSvh8P9Ewp5LSmrwee80rDFy6qewPJpYX6e/RWHMMfS7LEU60Fj2cdkKC
seJgG/1bqNCIVIiaxiemnrs6Sbvgm9vNvkfQl6qa2ksJNAS0jaEV97EDQQTwvIcfZL0xzUlm4cHX
uXH+vdgSA0uAA16q31alOm3rNmUcD0mruX/bbZVtcDnDfkRunKcz4IlUIg4QMBxHmK8ACsJauQjk
o0stdEd98reLyr9OBcc0hRIbz5yh67eXVpJdWf7M0PiGrZmrTEy+KlVqjDGTnbR/TrzIGjMl0Ul3
n+zTKjEboO8kOFn1s3aEeJyoWUcvcmevNa1d0nzuARop7C1L8X6uyzwKOLJh83fH3wZKbsAM9L8a
jkVnnUMDXNpQYndvMdQogxsKUEJLyWZMcjbIcshp2hEsh401/hNM7DLAQF+i7IufscoixkBl2xrM
3k0wivdbUK9/c6brdr1dPGc6Ej3fkc/32R/Ajv+e95b4tpo3JmvjZT3niIIk2OtP8o+uXIQLT36O
KjauQECpMKrfMaBtsgEdHy+TBmEV1sEUMmI5KfPenJ2wtkqRKDglXjuBdULJukkzUU3/VrhYZygF
D4UgDg/5uSWyHYOsfdxvI3vVBLkEddkG6Vn3x/z6VGZjRirwLUY6B35ATdaD5oDt6XZq5NrG36Zb
J/WdjixDvfuSAjVqUeahdNnHwyPGAtXNN798I9EPhSy4EUM9E7ZUx6ENfWsP1lVtCMyiaGYb9SJL
HgXk7jORoxsXfwliA8f2XwVwjHtkiuePSzcDbdODpXkfmQunb9YifRTCCs8Mh+qqDtUDrBZfSR4Y
v8k7CRD1ysMEdDpw+LS6KLM32eptPBtR0wu8aHHdorQ6FAhPZiClsT9Gky9yKCfJV2tz5oVKJWsZ
Ba7ZMeN8+e0cXKEb7onIoQqSPyG3a/Xfzc6lMZyS241P4edJd2K5yQpw672MPOU/beUkgq+26jy6
tV9h1J7Haz6Gv+oAXldS81nSQl570IbkylyYPd1wQdNZpJbvubiO1odH3o1TuAIyvil8oJZuTTW9
eVs3PLRSVagmuSKGqSMCgcK6dc5YGTWBWDEMW9PgErnXaZPlBe7dPtxW1P13eddlCHj4WWdt4rsP
TnsQAc781Xbc5RJD5UCEp230ZH0nezaud1GzmlFbwUlx7SqEfYECmVglfpyW7H5FqwCJs2JKxpZm
uXjtUQZPdvyIpF3uR6n6B2bWKeCjDslkLl0WkkOje2aK8xoVSvaTQNXdfD8wxVxP3HyX5jTEsApd
3Y+0DzvHuGoDi0N7cKW/4JxoIgEY1t3ydb/fATTKufnNJpYEboxEzQRUTUa2IMWONPPqFnJ0kfjG
jXv2yuDRCBzqYxl1oYNGe+zyTfhVKLpnVZ0dy0bnZRt+KKq0f/KVKGnEZLvu3EAA7PSkPpX8yTWT
3UDMBNmUOsUNQVMiodEUQ9Q1FcfHfIYiiqAyBjcvcsxJQTS9xTLtqkgER7fC3KBNZzZ5OP2gON0M
E55LRvP/l9u9BPmlgVSLr8SIlH7VdBkHknvpqhWPuMqurBoF0yrRvbPMWZ0amJpcmARSGhOE+ONK
/nlE5svHNBXV8a4yXuFEHG6M7WHOSa0yspqTJdM69t1NIVhwR4kh++OUwu8+7KWgz1GvpQYcnI/9
8sYhaCSSgiGk1vqFnhP2erbgmm0N4a+JmD2wV+rBa2Ka8VJUNaQzIJUrjZ2oZ21ni0mzX8MhPIUI
+bEiBsH5ZLsiP5PVwXhBNAXtEEX/p3+qlik1cYi6D/ijHcB1pJmKcoeS8M5VAFDL/7pz7kFjUzmX
gYGk4N8FGhJnOZxKaNIX+pop3S0o3cEeSlm278P57ZCT4xR/NbGsoo+jIkjSZWwNfnUvXT0fRdCj
fBRfyAO4OSbyQBysa5awCY+9hQtEB9GtE5sTJAt5ACqSDOTRfqS8jb1yMsEsch9fGsHywWehDkEJ
xUkrF2C5n8uKvFDH+m80EXe22pvLmx3Ek83AC6l6BhB+FJSzLsOwadmPnBHX7ZEQAQJC0vPDXamD
xkaIk0Cj9N+3dCgFpP9no7nBQKeADPqfQMn0SP/wQ+usGkt2Js7ZKMghel8pocsB4AByXzPtJyTs
uOSrhFfdM4pPDJgC4XL96KDfOgumcz0S1mQR28MIqlw/CDRUhLlrGEE+Q9VVsgHpY8EGIgY77tdm
wWEns2P/dhZAINL8kRxHze2BYKHHujwQSyIOtrfkjrOz0StSejUoJmhbh8UUpI9yQCHbCLhRftD5
N96DDUOSF2+diRlu1Tg1ztstg+E3YfG7TFFbEWhxJKjL4VHeBh54t5pboOCH+C/iTR4lH3fbTfxy
ef0dT82DdD2nJNrBUbW09+NmhuRJHRt0XRrA5UQdEeUGvyUeSP50BTx3r1MD2NLjc830NNmKmb0r
fzyWzfK2vqmuqYZm4vmpQV4qt9s2iXgiR0ay6PJg23Y4XswOq79mcJJgBelM5WDRC77KWioXIwv/
TzTkM+pLNqNfnx124/rxd5L1M25IXu5iBguwpDbjv5pf+BybfeR+f2hp3Psv2+Xo/idoSc98Qpyb
f3bU6GRUUKVpWuCqWbgw7pp+waDnJfqk4X17QZXAUpPGw1lniC79W3DZZqr545AAfIpqnOt7IQBZ
mueO/8AFXTM1zvYh29AKnicz3gGUuXAjL47rYxjTcW94HeHUPYaj+xYuoh4hPtXaSZ0hVUlgycYy
vuSY5bP3G4YgwCGXlzHz7nR65J6xB9QMqjD2TVai7X6iVXaZbttWkS4UBPPPexPdO8smsbbY90kl
1R9Fp0OtuzHYPX1cHjQZx1sgd1MxOGJIkYpWkTZYwrhM+IAK8+7vP7r4VtOrFOsBcwm1jjb+2ZyT
KfykEughbZaGffw4G2d8/P5+7lnAka4GsIv6V9Fsa5XJOYMcoVes5oLpvB8bEheJQLK7wUa0G9Cq
Sy5gA7NMA8uMHZbD9VQtpDd6Fxr9/1+E0Aa0C7zdsZm9qSx39c22IxKukcfKy/9Pmo08L4VS4lwf
YfPd4X3tBD6YC9WvPucW2e1eIvIWRWgi5PduBcCfdwAgGVohMsvNDztc4aKlTsyfMZS1J9HVntB7
tV8txhY3k9EJKCSEJuPZ9rg+uXTBSCMsZcF1gAVaVgbfONGUQ6C0X5PBmgCfTBr79kAdZGGqhZNM
CzNkE0SQcLoNkjjq4h9sUVgZwBLDQFotSIfEVRG+NMhXjRVnQgD1L9ka2G2lvDxDHq9UdO4vu8GC
ANEKMVHnVxuJlDW4NUVGedn6UjrFco/x3Tf6XUz15j6FgFSylm+JQWoD1aeKYpce4q5pEDSB2Jje
BStb5eT8nPd0/LHYrSwSrV2xQQbQifEVNg9jKVW9Rjba9WnhYISP44cc1H2WnCKROKYPEtEu5c8n
meeoxP8TkIu5K0jfdLvi0Ke4PhgRqE8c3imyW7H6IPPYlaZaKg2Ki59i77hY1/onKtmSiZKcGmbB
izzHSdJpdqZjt2pRFgOYyZ18jeDdBGm3/lXbkpum7lmJBadTENr6RqdM1t7yxeg9n4gXn9DVCdGs
4IFu8rWNcUogqsWrYylLalluazOmNw1bRZiDHbFMZTfLrDPk8PplhNYm5Fd279Udy6OctioYa23O
3ohQ/QHXrjrwFFzIcTERG6Ecm7t1R02PSMb2Too7c7kg5A0q0IV8izDt7TYCDoFPgPIrLRJ9w5z6
pP18AVqc4QCIVgd8PDOSq4e6F5eMtymHi0uybzIbRhCWDTbqhAh4tG2R1jvnGb5nhQFpCMXWbRAf
k9lj9dfA0AYkH/6EMiXKfizpSjNLHlbtJW59WfGnVnahfH9jO7a/ikWhnuiVw+U18imbowzuV50e
mSD9LYhLmKGayLq4QqIe+vLA08FufjA3DE1bgPUxbdRprYYUQ0/1mfOKevKFc5HY+mun488ZDoPi
0cxVNi6VDW3fSFExQHul4W/Y+iajvhyyV49Sn4/Ppf864+RvUTQP7uUZpiKZI5fY+fy3J5dYfzNo
bHoJiA3NtOiqtaU4GW7frBKljCruK6QRt2dql1om9lZYYXdjzDUySCXuCNJsFaU1a49fgP5R9K3+
rkNkGX04bDQqmbrUcmqv12s0cGlEY2ZctmLhGBfdpsQDOx6ii62es3l6N4gx8LtWCrJUXSMg5JXj
q4BXiuskt2ti1bsSC8AVJ2+tcTD/Ow5Mn9gUBvo53HPI8RCSl33pXGvwLPncYuupDy1WuV+RzQEe
T+Zh2ImgH5UEl9HjIdOw2jqGp7GzOrSkSr/5K3L6aR5s3oYOY5IStBoCWhMfqAgHCUJuin4V8zIl
6usL/pc02FHZm3z+NIzMSdgb3FBN72KJswJ3axxqVXiYojVkxMaLfmdZV/IArOmckdttbr/3ZnoV
6OZDkIgt1/CeOjBoUtgVoAcHfAYzxNQHKsdbCmC4nUpcADsuVfNA6P00kpmc5qQIU0GT79ZSCXCZ
9REunCftkSwB19WkaQfrY3WqPJQ8U6908ThTFZYZ0xLDqZlsU9yCaUu1Mo4qPtUl0yYInkyacohw
0avuOTxKWNPMpyKFkiiB3glUvPwVawgWZfewpeoGeyHmctl9vpf1aE4BRQaLn7BOMivSNqszyJKL
vJnBWeoz3EUugSdTWG7AhhICqUlXvFnTKRKW9kjCnHwZOg3ZTwRbFwxQGbLFlA1P59x+GI3nGlQh
dMabzAI+oDR72ob+3W3HnxhQvhX3tRb/53T5S0f/dk7RQQJ3I7Z24tVEjixwe143aZCtZcM+gl0m
Ik09gUNyWbWOSlNgfV8trCXrf8XxwK2rGpUXmGuxV4OLlep8kxdWYCCXPpZuHvW4NuXHZ2EDa8BC
hDYx8r9GEjEMEMwZntfUMsqp8hYRxB6+aJ8LtkE8n3BJ1G8O9r9GvCsgigsaMRBsnYpGYkeJC2xt
N35fGvaxsL8LhBllXoYcmsw4QEUEuCKnAFBHuz8yxsaMGKm/PJg16Ttqrz0QY2igyF1/LUKbx01S
hnmOcSaGG9WYIMqQscC2rvpdlWTkrNTrEPkAG2+ATOOYcMhaCMGuDAMpW3DKggxzfVL6sI1Etzqu
TPZz/cWafarlK/42C8Ajn0keHRFB1sx59Dy7P7my9cPz8XsaweWAEdlT8hmCFIqxTopMF5k7ou8A
oNx2BmasCYNSo90rD/QzDMIbE470XnNEcfcjW/WzHa+LDcGwsZxgnl/EDXZommH0GkPw9m7OVwGb
qVIjBobegJP4Jw+/1qruCJgUrs10Trf4dnUtHLPvVVx7NXpJrAPNW+C9ugEJ9b+cOL2vtxmyIaTS
zDYFv5x5y3Om2JcVEPlh/Vrh+eWI8BySlg5unJwNeVRgZiHhkokVExEJnga7DM93DyMxcAzVJYsS
1z2fPsJIaMm/l8oWq3o2R3GyVQN2o6o2UAkphLcVhLUhFJ6CDguaRORkt6out77lYPxXBUh8omZ7
ybs6sgLBlDMaeh29NupbmzZtdX3pfMgNCpnd+L0a8i3VjvZIFxmIcjxZcUQX13iqUZBKrdMsjxus
86O2M2EPbsYMNz1MNUbpT4PENY6TYX7SmCp/S/EtVsNAWSUyAazCes54RkJg2eMR0+FwhqrICZar
WndN7CpqnOwyujAMmJP/aDJjYoVlsJ5gRTqS7uRJUQma1Zy2ziD7SxKHBGWH22im+LchvXfieFQR
rqow6bZeUyUL2Yr8CPGN7UfnxE7qGoWMclhJ6XGlzpHT9BQ5li5gsvGKkyEogjq/lP2an3Gohuzt
g6d+uXz7a6CWxBnLY0aIb1AUWW+HS5JpM7DC2U4McFbpt4IHmzWWP02BjfkOS36wwqiy553luo2n
rfbFUK/1ZMzOoRLV2g6BzdOsgs9kR+Cv3al+8+bhEQZRNANktvM4vTYifFxJyvSBMVC+07vLNlqa
x0rrqJbx1ZoQ/+eceGXx1jDI6j06ZO90MyYO1OggLz9lYQFf/DzF+aRx+wYSWs8p7Sg2WApkRFcB
s93p5av20XULKyEXJH6ZEY9sUBgKPOcYh8vZuvdsr3U1GLjI+HCpXz0MXu8IfNgrdXIaY+pZor9k
HE6ksW5XNNmKPk9MLXOCodqlDZR/Hs8t08VhFRWPqGwm/04Pub61HUE+wdy7trd3tl0EKISwXNmF
UDMISuKmuE+WBpFuW7b5Ixn533bFxt71wfJu4YHJf211ktmnWi5oGX8cYu9nKmgvdJ2hoXrLreiM
iD+vG2kfDf+kAJNBNii2CKkgxVkOr9bfU8kwh3+eGYrjND85pqWk5K1yCgI9rxKhZUhnq9Zq3uqJ
MUfyiHpryDvcq5Sm1YlIP8eSa+nnrUIJ626XUCP4O/CEG3f5XFoqKmNEU0KAcMrXeIuRkwHfFStL
9m22zhhAjhJSBZqhieOV10HKBq6Onxv6ytrFQcdZuC1+97KNZ7R4y5uVfRp6yUk6Rqyc7bbCvFmD
UZbyC4m3X4nqSM5G65ULwe+xf1NW1d3x9GdsM+2ycjVfRYqqg9s5G52v1YIigTq3X55ayxnEjyHF
sUYNMxGayn7Pks/3ry82u0QLtAuXBYGMIqR/3EbUZElvUev2C41NSRaDeCJtEuyEHYwDmZFyKcW2
vvp9brBHkm9++ZmilPgMkhYMDz7oGP4oIIHNvzKrmiT6liQwCG6R2fjdXufLv/PdBpfYw9FYFMV0
GwAP2vdK5vQ7CZoKbW0NxFF3Cg1as3VMOASzhuzUcXAMvN3427ZjDmBDaW2WqQa3sSSHodQxKemd
/Y0xD+t3t1bDlU3VQThvnkJ7Vi/Bo4ZKVw3wD4Bx1e5WKu+4vX5BBHtgDjEUTTDRDtUmoqWDCv4g
YlC0tbNq5Yl3t1ShXAh+A5iB0z7DEnLBmHn4g3nFEfJondLwFIX6s0bgT8+XB7ulXrQTx9a4ekP1
nH9n5aGxpNAEuUTW3uusgH+s1FtD7BK3J+mF545v1paTldEmMEkiTeL3k4Smt5hgb4ZWWq+eKg2l
vFTvBtGsyd1xPt9bv3n+/XTw747OgwYJeYa/VjFOsgiby5/NgcpHrzrIzDsB+3j9whG/CYvC/5V/
+fVe0iipp35N/zCgxIq+jPGiuiQYwH+t3s+loyAen7bczoUBWq4ve4vgyS2U6WGk1Gjnb3iyJbhN
M+k5n7DKgIUTH7lAyJIMYiQQeNyLy7m4l4SRQ/nsi7V3P8NWro4F0dA/HXSDjSve2Jc6639yia2N
l/F3dsmjjF4IU7Tdy75e6IzMmX8g2cZhUGC5aXa0X8vCIQEVUrrfUW9xfkK7HmJTQBvH8monYUDL
dVGZhGlVoi66muIXp5HuvceMRWW38WKUwfZx4TUW/Jzw7AGB7NZqfJeQ29jWyOJvWfjbCUKsyfmd
80IklBIBXeOy/9KIXtACrPMnqiI6Bltz01dbYeIQq5Q6xNUG5zJk/l6bmpJJBUvs8yZ9eQnTxIps
goM+mj+Q76H5h0N41I3GGdJwT7JchkiLp1dt/dVzLWOSSKRXajdBjXCUvO1dCqKOk4r9bkaLA0kp
FCET1BAHziHlxo05u74ZmqiYKbpSrBSYzAuwMdtNz18FkwQfmuU//uJVVlm1rX75Wi9X1GuEwP13
V/rXfudLQKG+jUan+Qw4xVeeQtDIYC3G/oBTeOJzC5FOCdz7qyGssbopSKPUIwc+Fwk1pwXvICPp
XiOdRBQsdjnzlelXk7dqA4jUksHfs+HbGaXgl2594KbNMIh9LL6d2RfwFOi0TZyG1iu9rMTDFvIZ
rYppyDQb8hLMKyeSpxHc52hqTAykyqJXHJ1T7Zj7zIHpYbk0eH8Vf4EPkPV0ASo6t6pcMeyoF1gu
MVEYYU8360YTuWhM1jkHUXTVlRuRu9ZwADmVRm0WFUHLr0nFC++0JNMk8k/m3Cj05xsbtBwZBtMr
BWdjJQ6LyeJwMZAKofxxLv+rJlF0tGeTPTfZz15/fLiwbrlYO6A1r95lsRqumnOD0m43cBl23mWs
EMuiJ09WSlatXEh/O0xxKgkfvuAvqA98T6+RzZOCnP4KPRWkLrBjvMTLtHHKp77q9Wh0uDjK0Xh2
vig1SQzBnAmtRCL5IR8LY0Hb7M9gqHyfxx7WIJNn+7b2vLsZM+Y9r4v6ZTLQuaunji90ojuiKerZ
wRG4XKdoBMR+uK2YuuB7SR55N9YROyjNJ/cKicXPreTxiJljx/KNwV6D8+PHoxRYm0PMcVbwMSNp
7/b2bmo5smjv4NAQ6mOmG+bRsE+Zn0qjLis4aj0gNZZjmMK5oKW6/ScmRbES/NjTZyGsQAFxxatX
raPB5PKl3Gr5GBZcfD4+93Sz0HVbP/EjHfI1b0aRuC2Mmajs+DaGEX9btt/3nDVSXKAxnaROTb8w
4vQ7tqwg3jdoJRq6FpOK5rbga2EsJ0OAMRhEqm5FP3ABTOZ7knQmjQ2GD3hmkMJ3gQ8ElOu/8qJa
/8w7VkqSSm7Xjnk923u0jrfABE6O/aPvSa0SYG/irP6dfYVPCOVZP60bgrxGkZ7+SRvwgmHXMBsX
iNcjQprjlwD6uIQGPrcCFN6nsGKeRKcQB4lOFduke1+8lLukPHVjcITNia4f7Sk/LBU+zICfKcpg
nzkffsZXVjihbEJqOJBKh10yrMzS+d7rkEG8mu9W+1WyMpkJ8VhkSEWHJFjKKaxVtXzVCzeZaWgb
wjQFvoM7QIQMnqtvBDhZNwiRqT1qjiQ8KTK7YwgBqr73Ag2urzIREhCjREUNQaOOzs6KVWNH4Kvq
D/2q29b8rEkFe3bWBpO7TVovX1qNJlnRzFR3TvhJUCCE5oUSOfmdI6h1+8kfS8vuSPwPXaRXFhNd
IJkG9zajSHIN4oQ47USxc+Cvf0TF3Tc+ycNtYNokztD1qPW6TmpoHnK8yxYmarTUAvZw5EkyYlmz
ugvr+LSl04Di+t4hTzIpLVA06vE6HYcGRmZjGfX9GQwkOIU3eJd5wtS81StnIaYM2gfv2SlvHObF
YSNIZVPhAX3H+bxPP05bqCeneN0QtvCxU9Zmm8NTmzGDANpB040enTkHvai0O0gDVun2J6wBMN6e
wgU+ds5OQtHEOThY8LvAM6GSio/x+OShEIbFZ1/rFEuX1ygJN6p5OR8D9gAwYPcIcSpsmCblAEKi
6v6NVCGh9QXWjpuwdy+281GjhJMM/5YAzKuVbqrFv/OEbHr7dFqO2zIPa1y1r755zxoFw1aKZE7x
WLWa19f9yKw+tQPPhazATTUvjBf2REfhyxXyRKn2dwt5GrIOZemi56FscqM7bGfSVfuPF+XYoTM5
p/Kml8i1xxOVgH/Eed+5GP7bdBRxEmbzSDmEHbpHqVQCclnaLMojOBB7Qnf7nDe75Yju0nfLBu+b
15HlojLIg5N64kG38SH88LBQHWEh7sK1k9ip20NAAomMwNNVrdVILKE3atkEAPS2cilJsLhaWmq0
TqHO5G8YwWkcrrwo0dnj7RtfwBAhutoOozM4zkeaO5fqeflGgg6LEek4XXXPnqRQLZJP3a0wafhN
xTWF5jkbw8iVaOhr3V4DMu23aqx+jpTH9PvOF99zXR75VsF8/Ji4rMO2+XihbdfeeOFQVAbZEU5d
ezmBaRgQ60YtitUofmXUNSTK7Y8KkVR98pRabyK8XfIWUWUAxPBTLVnxvoHI7Mg/VSPCPg6nlt39
IVZ8+124uQ41h+6gL9zjtQi07J4a71EDZAqs82bzbz2rQ6SQofWATbNsfceqV5Fv/0lVlSO38jUZ
QGW3kYoqkYxsdEoJBRYqCbvGDmGslNBFwyjQDHqVLGLh/oD+801A8IZiXxqEgy+rKfLwX1WAMJfi
BVdUornda9ndMfd3J5iUmZe9i9Xc5IapW5MGHapTtDYMW+P83MvvKwXiPumwkDZCpbLraiXGDPZ2
IvjVflXhIZco5R9UYnXOYpIZSbzrgGEEF5t1tqKp1qiKL4hs/mMBXWbLMatmFTQDG1KRmw79iGHa
+/5CTjx4anmrzsGBq4e2Tcgl049xYiixAWHsQdDjdINRYheFwGagydwy3LnBYm8pZk6cNrX1UE4F
8pjejnNTHS9VikZCI1FFQ8xcUYdVoYiAU14Dj876EPJmxSSEzOt220f48Hym7Zou/aaTeS7lLtx7
fsL9O2O2kul8XazaQAL79+IxMQYrg4ophhKyChuIhVS/c4BAry4pFcn4cb6nDdGTEGw8zGrMOE4+
TDMoTNkIWgicGYVOPR/vOvbrju66DJbBr1aoGmIGSjB37L2cFBudzFshi40XNYVc/U2WwIiT0MsN
4T18LUAssT/VFsBw+R6s88EY0ixTwe1kIWTA6ulL3Hz2Txx7k1D9KV7f55CjoO33OteYmKRbp4qt
lRFR4Kz4G/0empg20BFJov8TPdOsNSDXfBZFgxMJZIqsy4To5SYFWd6TBWupoHt7KurKsx4lKVXd
zSktiwCMlik1UtuLyf1+AmXDeSj6Q7wCQI4RP4MG4MDQzSZYrB3s47tNOgcX/CB5ugbCp00umdd/
gaEJdxbbKHcP+3jOJzM6TC9wBRKt6G4YW8xxRczeKatfd/e0OGzob1A7xo7WoDBS2wrp7J16q/Px
5l8lOLIpHKYmui9dydnLMlXZhoCiZXwgOegFsSjast1I8JA3C5mV+BPf7W9HzGkDlj1WKzD33o/1
tSTgMDqVhdbaUth5X3C+DygIcxvS58sMFM/XUfTUHYQXVF8i7VSYK2aRP//OSONIGxnXR7glpxJ2
Dk+yCmwghDpa4C33GOljnemxsF3q8iWlw89JFHa4lFVfCA2264v7Hr49leXLDA1O/gpF3ig6fOVW
WSN2cz3yvJiiSUDvga+P7S4329tmLitiQ7RTIDeOYVKFaCCnrtCPPkSBpHl8n3q9Kp3wbfWSeGhM
7AKgXLzCtdaTHzZ++jiYVu6hJUwyrr4B3O8MEt4EBSUgmbtmjRSg3AvQLjxK45ZUUZcFGfClW6RB
wipuzoQH4ocKZ2A41QP/yFUpMcSOAfbkx7FrMLlXCCCzHph7oVU5wPRs+yd3ynmLon4WyXCfZ/5+
MSR8TAWbDGogUhy9nlb1WCBFXF6omfm9NwKBBZhwj7RzPIVPt7nzZHx7QAwVevMO8qAOI1x+8Xpa
3ib4Vo2CenR6v+kOzBsZqbzdxoFvBt5meJbFCk0slvoFkHF4l4hZo8oVR7kr7jhFp3KeVeYCm1Ab
V1XTisik6KuUaFdQUM737ThNFU19x5OFuKU0B8v35r+31PHhGGCFyvN4Vhgfg5Z4zNzVIrfXDcoA
J3CyPnqXP6z+Y/qHd5D77hl1Er+r10+fCftK8P0SPnkV+F/ZRnYfVp/t7zg80GHCwmqSl1jTl8+N
yQf00AbmWP6Kp1Q0o+hXzyhCXRz1h+sPEgGg0AtPU72rQTg4/pG6c5VA4NpuRzKUwDVVUP12EQWz
kaa237humS4SAww7h66IS9kW3/jPTa0TaV/plChnWdD4Bnn66D7ZUW0VBjJgzO8RJmh5UE2A+zHi
h968h5m0eO+tPk/sNXLLVUIwAtEd6nafu8td/EME6tbbPuxr6YLx/E+rREou2mNF238U7VJCxWJ7
g1YHHokPRnONxVtC6/aYLqpUThlqAxekZWhdWkFV4GQuyBk48peF16mgcDB9d/48yWgOG6LtdLB5
806lnFR26T6bqPlTF8RqFW+1HUDrsxS2SLFc3yO8r6mF8IPz41XDHrniVBXYFf8p9ia5EdKcu7Id
/iYDTFS3g3C902LvkdEZu6q7v4FeO9ZaubfbsdSoVoLhixh3N86sZs30SCk9zLmW5LDC8gknQmal
lUh29/KXXsMGiY7eB2v1elslQqnfDizMWZMXJqOp/Tfujy2piuDWrl5E9Njkb+Vna71T6lQIgL8/
MPTKuy7Cp9QQpmlHANJktrkKjwIl8U7sDnGdCiy3cP26KH5UgvRMv3B4iUa7TndnXSbvETrSLJiz
OMgdVp3XoBpAkj29YEztrHNZSlGwI5lX+OhO826ii0eHEtmg9mmfkgWPuEAZsAUlIKwUTw4mh6Q/
bsl6wdOWAT+3zwJsIavfspYRAl1QF65l7A5taiCKJaKQRYsak0Kl3N0ouXKxiIqmXxxQrFUdn5aJ
juqoIG0/WX+mqixp4nhj6ltYO5BDTF8RU6NniKxvS8um/YlaSQLSKW0xI5S/GtVjseTOAx14YOrX
Ki/Pctg6S4Rz/r4btPdQXlJfU1nS+4MonkwKHfR+RLcG2tdHkCe5F6bw1c6Kq3SzCkd90OlTui7O
ggNY3GOHbq1bJCa7k/s8nNIbU/uwDkLQzEVOUF4dhSyQyiZ7qwaUtc26ObqKl3PBOHxLj0tPihOD
p6Q/aABfpX1Nh8khg5cZPPCEp5vg7ZxfCLbYk6yExL1GjqPYrR2ZnMbSnSO9kysteBnEPX9SntHq
VUtopP8WCrVbWRyO76sM88CslZFl++dw0bX48inMl7loThdHLsFdkcBXRgBHe9Tw8E5f5VGaK8qo
Ww0jj8t8R/1cORrP2lUfEXQ51TBH03jwq5OllHN4ATscho5jPxK/P2XUu4j8dNAOo4fBEcXU8FEa
NziRZe5CeXJ0P+Ce2jhiPahJ08AP5lE9+GCREsU+GNYFrcYc3WmHPpqrqJtoQ2sJuN/wvVsaFeRc
Pjonx7u6IBPYpr87MM+uM8OWtNLs7YtNNBDceghyKDXzoNbdN/ercVn84TF5WDW460CM7sCL6Rcp
jz8Rh+AeX8FX0BzlYDCqi50reBK4uLCj/CqN7Vbb4djN56fGQQcifb0F7OAOq/R65sqUzWHkpI4y
nRA4A3fhyq9WhT1gE5gz4pHDir3O7oLr81YhVPwxXmzqTCq4S3htg7z2sMpMrGl/UCGFkEG0fn42
cndz7MemHK6oYpmUILcmtA8/S5ZuUQOQI+DwA3LKVkGsYCSGKcwUZ/aK7PYe8rSFn/TevnDo1+5m
ynIMzIUNyBPzY+Sgm6YIXwU1R2KH4WWsKZoB0Qq1h4l6o3NsiVB65LgO6WLx7Aw8q7nErhdmq+ZV
A8/RFv35FM8aXeYedPr98am6v6PCcMQfXKetXNAxdZgIByuo2QNKQ3+eXc/LApA7WzPtN1OfRO8t
NhI4ER4+NQf5NM0HCiHZ1VSkzGgisKJwgvj4pSB7exJWx+LofL6cJoM+QLTZ3LH8rnkHe3ZfOlSG
lKn+JDfQ7jzj3My/wF6Hp8WZhHUfR7Zeh+YgCSXgp+q0BqSp8qMBQOu9ePWqjnapxxTe0tOV3rU/
BIY9GZ0vieBPUqyHTD5pg8i8XhMHjHWgWorT9i/KlfSY4jRgge7JfjYDAVJcAWjctMubgbn2FiNZ
dbAUE+8yLPZCdxV/Hyq52tzRUleQXBXTDWWrg7ApIZ8/l3e/3a30/97SD9DH3oK53BnJ0/arwIQ1
0K1Hjlt890cSZR+oPrP69v1WNpyoUh4pJkO/LJaxtx5d5QQHXOGlfN+FFJn0+Hn9vsv3Xx5uWWLZ
veatntJ+0QYDmrdL3XDOfQAVgLABTcZe2lsDqfr/MC4x7/5Dt5vxX15AvqyQ/amiEUUt7AE2raDx
DwO1RHV59AKfl35wG6fHc5cLScpctcR5l/PgKEYIYm5/cOFZoQzqWpxtKAau+nMnvSC1bw3kf0sb
BEFkqxURvq+UUH5oMivrGPOq2l7fZtv/ejs+jZamrN9n4Ocm5Go7X0wmnlZfPPd6E1wAlYFVXtlV
jy8THToSzjethD+DvhuBKqXc7gHx+d2Q0jtNpZvpJAfVaghSXDe30shUPQ9mO+xx8D5v9TJLFGaT
q440Pa5JNt6pY9KdP06n3lu52N74uKzK9cjIllIBCVjaKi+TMIwXeFLsgYsBSFckFz/8i+o/YNq+
ca6BHQDl9Tbr6g3Ab5N8mAzb4ypL29HUkMEfnVR0MzX/+UsfmAQuMzTzTKqDY2eGe/EnPPZYBN9u
oAKi4e2/qxjh6l6zLWAADmYtTYGy7PR+T2jpDysW/vK+6oh77W4AkjYp5cu12oq0pYgAccH2Qe9P
pE8BZbhI7nzBXJad5mBoIZ38g9RWJhN8Q2JgPvMR0IeaqBENyzQDNh9VKO43ivbhbeQG1C7ihzFl
DJxiwQE4lydgH1vmacL4sQhFHI4sSU9aa+X/0fn0pLs8nvvycE7aKIHZ/6LU/UHsBNS4/OJlmK21
VCcsCBumXCQjZxEpMFsuj7Z6r+ETjG/gRfwsvp2GHCN3uwV3SvrViheDu3/pjfIO4mq0oL+ILWwc
z+DbsV+/EVLDMdA8ZfvrsH6K41a1s5V7Gsx9ATvARNndn9dvLEOnwlCjEx8521u3HWZ5kMY3X6kn
m6xN3qGgA6VpV11uKLnWRgjJ5++ke3FKtSsCKsFUtKVnw9GynktvSsO7ex31cARCqC4AtsTxjsRj
ucKrLHWytlMSrcKnQdmL2x9O4JLhybb5Ssf/LKy2TAtmnZKcHHqb0QISWv/qwdkuefsqPBaqHYUv
/Q5F4zADKFdc/KmVu8Nqc5GqOX+cn/KMpCJWTvvSMqaLQ+o0urUQTYpOyqafOgpDsBPtIhrik1YK
gF1PiQJHd0RGbz49i5Fi+FF10uDQQl+lJaHc4hYM6pA7jQbvx7gG61loWthycdb/0313XPLbZujs
02smWjBKqJTwV9gyo5GoqEfa4LwtTLkzhyyZbul+I3/Y9RTXog7iDBan/uJgjGQLFVQE2mAd6b34
jj8L1W0zi/oIkRl1Ibn4IqDlsM0IhKs+T6EFVn+zHFLP3BHpG5YjADNBDONHP3urz7jqtkwBYmor
dmFjMbHZK4PK1q+TKmtXPMBEh9dm6y7m5NxZXeqbXZ54IgQhczj+F9sYalx5SGeCxsnY5XlUo800
tFzHwgQXFOSHFMBUAe8UGh3tajR+RFqiDzhdD2z9zGhzNtCGeAXNkwQxF27BQGUZTx6xOY84TTA/
luWgjCOb+v420rZVR+vh02XWIZsPnOYOHYkdT/sPaqiC5myolQGnzOiDPAOg/WkfOzyg1koZGUaj
agY0b4mOEHWIxzkz7ucDjkMB5iUPnGWZ0BMU8DDbMGvuxOlZl3rkvlSe5vTksk2tkcoTuQ5HplSA
jH2ylU3zIycuUDOSiDjepoLjBlfy41dCtuQuyvnGf4qqz2yshjcywAK0fz+8NOcC82yrUHA5gtTV
gRl/SBb26SzDX5dCBeWUahXYDFOcxmdE4462lzCS7OGHZY/A8JIMp8/7dnTKgawVM2Knt6BB05JU
PArfYzT2e3JxXLANDGo6DaQB/zI9u91JWLrPid9PVrZyqCEImxw1QF0fD0RaBIEflnGwzhkL7Z1D
OGA6/lse5cf/iCZo757f20Rd6uQ+ZSpAG3HwQLhGWUsBVkjEecXw348KNXcj5DJZbJzMTp7Nt9EL
vymcKBzG6WcqP7LlXVrNbcDYYyHkFJ6wqkvVTExlLMQm7mfrBxPiH8zQXN390gG7xRrwmkdxCWm/
uN3C9Q3d1HnTrmR/WlUnRLcdwkwyRb6dyPArkHtWreNNNMz87qnZB8WSNJmfc+1ps5COK9JLIc51
rVlH6p7UnLbQ44OaomYWZMh8IfufIt/aNs2GmEi2ao4wQhUfdopkGIt2GqGuOoCXU7JM0HxRWIBu
Mn1sAljUhosQlEEdhnnhvAga2sLgiKG/V8h1Jx9vTK9/TurjX/6vGapHIIvYfasxeyAXwvHsYsvV
JPkYFk1voWcinNV2vVXjpFHBy9OndVwHWxGhGNlXViHK5mfvVDow+5ECCqL/OTIncNI4e2kY9Hd5
Io2XEce1jBJInQmsLoSeuLfYSVC5qg24sV7K5ETsBVlpKEPpA4UdHqEVGglnLo90rckdGYXuvCDV
u6mH31owBewFjqr0AgT2UiGsgYIxURfEd5vtBypHlJMZCRWCyhxHxMoZ6yQkOadVzsJOwDSdhqcY
HJXwrqf6eQN7UZeGmopICKsfOYE20f7zDUgvswMfi0lv/uBQuvMZGMTqZn3TAacAOLQSX+1gi2Mm
jMz7pXcN4GEhkp+Wq9axLY5jpM+7ZhRrbDY8CP4EhujEj6so8CFF3T6AxMGnWxpTqNn48MMHDqmE
8ddv47klCwYoxhenxuQFmcP+hPd5hUgJZZ+z4UdtkJW0U1GeigZLsVKNb5Xxrl3hoRhG8KpOdrXK
9pr6ZHs/IBgQ+BEVArTdZ4QK5/rUgDCGUHPbF3x/unFPsjI05dkLzhY+rdzZjF+kMmMlcHCClvtm
gBrcFYbebVqDTwr52fPExo3xCjDCFJzHsoTTtfqrwc30LcAu2icos4HclcBXts78FuvYZmRWfGGJ
h9xorWFYeA+swjsH64sWEwAs4svAtVyg1/tzACG1SBcD2bXyTyh9mHud++mgQ4LkY4MTENqNdzz4
iElXyljvNOA9SHAD21yad8MCaclQynzwbLJnNK/P1R3gZsr/8sTxeJZTZL6As9p3l71p8MAspDcD
Sot49miFaa3r6SKUNOWjMuCurvc225XZYBenaPiH+aaU4uiOnfIPagso3p4CvqJicYfENSXp5xi+
tFRmGQmFl7etF4Ye6r+9oXFHKxLfsKFB65eZGw2B6tiT9iW4Pi8k0Ox0HVw6cDijF3WWrWSwdPh3
dPr9buWoGyvhkFRXuT1iaiEvs/GKQNG67kd96FPnYlg1HiwmJku1n4h504cROMOdqGETSyWwdu4Y
UJd/9ZidtSfzOo6ziVN9tecWJ53QVoYI2cgFRrX4DEQZPH+ajN9ZosdWaVeHFLBj35InOcKqeVGc
USiu0rmAmX+TmUOp8WlIkcR6RXHQhtkkfBy1Y+kELKGfNATWh6PQ/Xbz6b2821IdLaFiidYwgOSG
adb9j++Oz8isvSg4STU3PZXNXCbDNNAHRlTRCJbKe8vIVglW5szBTQZ0ZZg97mRj9S1OnVROCKWd
FK2zhqyQyXnFHXFtwB4xhndJMJHmeSnvEgU1n44MGzMo1R0KxmLHEQ3FbWwA8JT/oXaRivGI6KU/
EFcx8rbJ4mIrNfetYdfQmXk4CFp7LgNEWJi2gh3umtKNsVGbg7GUr3LnLen+2DlkA9LTYBdRgOWp
mpRu/X0myaI9SvlCiD2G/f41q0rQO5hoj1H3HTi1TGlfWUdkfOwxnnchr+fL/foqpbjPWa0nql+3
5zWrdam74iOoApJu2puruVCWVVi7Ebi15nLKUZ4UWfcxRp0msnSjDJhEGcQJLPIjjObUY5dIiSQb
gjBih+g+Ax47EWKbiCE+yR4eXotTHf978jkHgFmC3h9UALCnMZsy2KOwwhgQisCgd3ilOG6cmkBs
GihCTAf7VfzDiCbJnuApSszll27cKWl7jL5S3j0oA/OG0iKez2VZrfd2u0JCRt0tg3fSLEIuce8y
4pNzlaeq/6z+u6Dpo90PYx1WD1mmtRE4db53YnmUA0MEhGq+Cm+PX2Qi6xjJfcVK7cBw5aUwySkV
bLEsYo7nW0qVY6LFsnG25araMo/hMDB1BHbOIlD0b6nkftySyNh/L9cCuMHFhs1zVRtCUZYAcTFf
AYz4Xs2c9ZUi9dJtzBN/s9lrtJMD0mSWQkiCuSWesVZcaSsFvp75pucZgoBEPhQaPOBoIfF/ZJVd
wJfenXC1rf/eIYrd7HJtV4tNo4w12ja7lkQJqZjBhW/Nlc+5StiAnyaOatJ0XVHPFjTl05L0jnvl
Kk38qECB0QS/9GgpYez7i477r1IUG1j+5k30o6bug13fcuCxKjrL8ogkxLPxzDeX691/h7IzhsVZ
SvnmM1gm314s10+Xd0wQmRZpue9j0kPNlMxQHeLMRh2ln2cjaoT1T/UCPMAWW6TxUncnm1u3JrAB
7CripGQjZXfTjqG0Ce9CM1Sp/jKmZLdwSh3qVg8nwparRMceD2ItFsCGfm+0pooWYAtSo+x7F7bF
mCs1WzlOtmdJAOegRhBGGfwv5b/1D9qnyb/FNr21FrWmrTCYdqaKYTRL9t5ZobxTDCG+eTo8YYg1
1US3YfU+9SxtaPO32cetE+2gWoo5Ve9toGUwIK0uLBk4aJSIhYWdNbZzy8naOhnFdcikCTF4o+BK
4nA/oXqpg8SDS1q2+VuG8NniDxz4XTqq4GNGAWgVAKH4C+is34lLFngnbgrwpuLiG06ZCGposg8E
8g1vbh3yOPr3LjJ1NM9xKiZ8GHlbDqzb/N0AR7yjaHT1TeGzCtHRl8Da+MoWKJFoZqdzHv9Efrk9
9t3ZDf/01eiHOwtsUQVZ+7vISXoqenW3bo6BtzWuvBERzIZVcGc14jTQJUJ4a9U8onJS1psGxix/
M8vpGjxfFOkROPYdYT4ROZKV6EPmUO99eGuSODTyqQCNt7dHDJmoH7KqbLT9dRUJgV0ENdIt4SXb
5fq1BQTsmr5rr5jFou+Z7O8gg3WhhGWtfUwk6ncbajo0uUOER2wd2rhLX5kdGlzOutyQ38N7D+x6
asuAo8SPWJFTsjBuOek/E8DQVtBeOo95c5W9rBMp1tl4u1OmLORzPren1FDGs8wo61AbqgW+60u0
WEQAkAF3hvcH9kIfmJH20h55IHIRYXIoibObW5sfl5Om7/ym0CmdUneCITwgqBXVxOZUgzghw7bw
eUwfJnpgmxOgwOTLKOcLw/wgyW1FjXJS/pvyUiYF5e7KFzYvM7VCrXOGl236MdUoOMPHZ38wmhpY
sbiCiZfXzhKIfTnz/GX74V1p56oQOTYyZvNYt1rovylfCxhjO8XlOTgzhjoPHrrIjDgGZLzmv9XU
sePRGUOglLKT2XtVmFSH0sLV0e825rvFMPUc9NF0xKS5NUAfi8tf1UQQytRkGDuF+I+p7Htnx+67
gibdXNNiKYUIdsjnZUGEXFL+OAylheC+UsrKgMDiRrK7ifdF+DpzNkKiyrMAT8jVSBr9hjjq0ChK
IdlaBN0MQBpMry3CnjLY7hb+5f2EQQNgD+EzqxIEhlHKGIFt7Fyc7X3QZNzJLUS/0kMflN+AHQPu
DBRCWuJLqv7guk/8XudgoWBAb0PbEln+SbRhBLthBRSOOOr4IwKpjLBKDHkAFJPCIuxA17DkfDzQ
ig1cYL7b3aZREPldRxMMVceAl23FdW3kJOSkvlySmFHTh6A1LUBf6N6UCg29/qK/UWX1DrWcOW6+
B1kWM+gSILCvaBZr6eGQgijhMbasVoU7BU+GIXSPZnh+ikZLdHKoo5TT9FXvrZ4p/A1Lb1TzWb/I
ByjKUrfMhVqOPBJ88hEAKEnY8G90uO+kCe1zBMTi1UIkuhItbE5MD9xwwzBZYbga52VW/piDBjJ/
/kyt82HvBsbBm1ca0Hlz4wYj3fyLHcXLODEa3HydLyLWEh13gl5zIs2dMOs3yN6Xe4eWyiFJ+Sd7
RPQLuIRN+HoOPb1y8SJktHsU2iy5kWQ48KhYJNJ+puqfD8uyC5hozqHX0BMo2QXPdU5Oj/a5peM/
lffqHy0RbG6f8ZpDHZ/OU2JMOJ7hcJ+u3WfITioqP83NdTcWiiFMZo+LyGABCx8uzYKMgpZsF3fs
yNmUUqujC5JGnBxYLto+r1bzM61riZrIWlequh5DEhrpPGvxGbshDgr+5A0Y5t1sdPFEPjlyUyFi
wZb8MJTi9UdoDdBAqo8KZ4DUIXp9fi5xcJk3nI2mKSXjk1ykNflcvjteNgUUzWGSlPWgsQO5K2Bc
Twz3CrBQcHGhRFk+3y+gB2kYr/e45/S6zkNrJaj7tWR08zFWpt3a5OprNJ+4HEyNkLfZ2GqTq2DW
AeFuAilyEVgmhhr+QZYaBYMnGdKhsX/QdzjAbduz7XHv0vLd15m/y64/zNyinKLRSLR2AcDMF34Y
H1FSD5mNBFyZqIcWSNZ+SkoLaxf12zgA/H+po365OclUHntNSLbZbatwTrjLsk7XX1z0FJpR1rZr
GvChefmCLEVGhat9q/8MM6KX/aGMoGV/UFjDon8O1gYz/SbD5ca9/oVkg8vZyyeSywjH6mCTjKHT
lt3v6B9fmu8qavfFV18w51ZtsnZ7fgnr/5mKUybrYbxGlaVeUIOIqHfuedCskItsO7VxY9MaU3TU
kLmCZ41l6yVQF9TgnD28A4Ii0X78I6xaCVDVPidEfKiqw3diMilkDo2JHsmKIaqwWzAcJyOEAx3o
T+cPNuNEHHT/vQe62b6RAUEk7XVSL0Mb266DmGsQE36O0s/lpx24wtcePae5ZUYVgoDHXbzWMKnG
m67pfkPDasW6aJbdb4h+HJGo6OTrrYODuMCEFS8SbT1Anh6a7h3hA3Rvv4kF0co3fZ0SKMxPeyHF
YecpATvm1pk+vJ4jhs3sNvlhSOYYsAre0eWJrYnrvs5BglEkSI0s6mlUzp5T3ofKupbMRfecTUxV
cxZMZpdyVzJOWtuoyigYEYiR8Hq4iQXHshAcAjOisXUnIyKurCu5yLsxx7unoTsmFvGYW4LAaEIZ
EtYBKkDFR5rxX/1qplNv6W1Ma4fBbk2rkKM6haSd6z3N2aDVn9TRHEAjzGTbF2fcTlSzBQ4Clpzz
bJEGk1060pVOx0JNEz1RMJPaE4te5z5i2Xl7QLwa8zPJYFmBgLakQDEX1aIucl5tl39s8TAlf8/u
E+11bIUu/XRqhcBzx2LMq0jcJ2Sq+xUdyaRNG1/ohTmGt6weksfNcEk4nJth7oBzq8Cn5GKCufwZ
96+FDTKuu4qlQ8oZFIx0PEpqi0U8Xj/rx1OwZdY3Zx85WQDWv+flOR8vbsg7mwBVzzaE6/v36dRX
v4eqja1j0tN5nPgRSX9M1vgcVEVyiFzL4ufHsaMA5REB6bYo/gTqeqctpEl1bpgccnPtx0Sxms8e
rNU2bYH11JA6MvnyrIQCN1yO/N/D7p5GA7ebdv+LLzrGmUwdgB1vR8IxHQ117784UY3V5m4TcBHf
PRrlbf8uJW1pqBdLlvUryQwJo/UI7mlp/qcT1x/qWBHSx92zy3WHtahcxEjjfcOhKEEQ/XtbDSKp
PLms/GzdQMNdybNkgiNRZQ0l2C8A0/C0YM3B9tqH5Rwws8DabhO0CLNyViUFoIpQje9HSkIM9MNN
krwXLMXz22Yy6Z3PzTwL/GyLSP10Lv61Mua/+//w6uxTL0uVnplp9pW3vWiiHdkuz1YAy+FFfodx
DhQIFHRZEjuv6r/xShF7A+ibABEoXYKKGtG0w9GxH22Qw3X7VvLjcd19qO/e9HMn2QtzFOEobJcP
aiDx+1bDBfjeULFfWu9JaZIowPoZXDR9HsqRon+Qp9FdYAPk29jaBILyDkAPzH2e060yCyD78G2O
S5BoMwYQkcnOtFZ0Cgc1BU3tdIYVUESuBW6ERgLHDv0d6t0RpPhr5exE51YmIWRqDXoDdrZikWjT
I5GhYFDgWxlgdVppn3Fytp+FrhsIx1lmvu67bWN4SsuYUJuXX0rMSAUhPVxwYY9knklbI/dr3BQu
yAQx4yLFGAkLa9A9YxNWQ6/LAFq54fwnAGKXjLbJhzplRDzlFaI4KbXaA9NSStf14Qlienw09Wh8
UlbySMlGI+91D4bvkLFpz03xWmMQQmd4za1N1twCdkdaTZ7BY4t1B5q4tTRYQsBQ45xSrr+1LdIJ
tZpBRnjmCTMeWL5Aa7JxHjc8eSoGbw8eKwihLiJXokfT1Q6itjTlQNQmmOfQ5khucefWRmF1zx8y
i3v9bvSdD0GMZlRHE2sLFdFTq9D2Kl8QK75ZORKc0Dxe6FJ4ZIW8A3snSR1n34OiyhfhJMs6Jyga
gid2SJ/g7VZIFtx9nSGhRjq6PlJBSN3wnHkbENLq015d5l7y/geYNgK3KlYquiTi4eYbV7dPMdiv
nKfc9KLU0VRKHaD6f2r4xP6WitfNltNgJ0uWn8Z4iD3pSa0nBLYrP9OmAt4ZzMEt5KXvuLpFPU8J
HD3BLpkj4tkB3PZTuCvEHPu+IDLIREpe+jn+NdFEG1sQqppepajyH7zzf86XhKZD+lGit5Z+Ug==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_init is
  port (
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_1 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_2 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    mmcm_reset : out STD_LOGIC;
    data_in : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gtxe2_i_4 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_5 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_7 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_8 : in STD_LOGIC;
    gtxe2_i_9 : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    data_out : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_init;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_init is
  signal data0 : STD_LOGIC_VECTOR ( 13 downto 1 );
  signal gt0_cpllrefclklost_i : STD_LOGIC;
  signal gt0_cpllreset_t : STD_LOGIC;
  signal gt0_gtrxreset_in1_out : STD_LOGIC;
  signal gt0_gttxreset_in0_out : STD_LOGIC;
  signal gt0_rx_cdrlock_counter : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal \gt0_rx_cdrlock_counter0_carry__0_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__0_n_1\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__0_n_2\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__0_n_3\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_1\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_2\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_3\ : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_0 : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_1 : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_2 : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_3 : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[0]_i_2_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[13]_i_2_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[13]_i_3_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[13]_i_4_n_0\ : STD_LOGIC;
  signal gt0_rx_cdrlock_counter_0 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal gt0_rx_cdrlocked_i_1_n_0 : STD_LOGIC;
  signal gt0_rx_cdrlocked_reg_n_0 : STD_LOGIC;
  signal gt0_rxuserrdy_t : STD_LOGIC;
  signal gt0_txuserrdy_t : STD_LOGIC;
  signal gtwizard_i_n_0 : STD_LOGIC;
  signal gtwizard_i_n_5 : STD_LOGIC;
  signal gtwizard_i_n_7 : STD_LOGIC;
  signal \NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of gt0_rx_cdrlock_counter0_carry : label is 35;
  attribute ADDER_THRESHOLD of \gt0_rx_cdrlock_counter0_carry__0\ : label is 35;
  attribute ADDER_THRESHOLD of \gt0_rx_cdrlock_counter0_carry__1\ : label is 35;
  attribute ADDER_THRESHOLD of \gt0_rx_cdrlock_counter0_carry__2\ : label is 35;
begin
gt0_rx_cdrlock_counter0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => gt0_rx_cdrlock_counter0_carry_n_0,
      CO(2) => gt0_rx_cdrlock_counter0_carry_n_1,
      CO(1) => gt0_rx_cdrlock_counter0_carry_n_2,
      CO(0) => gt0_rx_cdrlock_counter0_carry_n_3,
      CYINIT => gt0_rx_cdrlock_counter(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(4 downto 1),
      S(3 downto 0) => gt0_rx_cdrlock_counter(4 downto 1)
    );
\gt0_rx_cdrlock_counter0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => gt0_rx_cdrlock_counter0_carry_n_0,
      CO(3) => \gt0_rx_cdrlock_counter0_carry__0_n_0\,
      CO(2) => \gt0_rx_cdrlock_counter0_carry__0_n_1\,
      CO(1) => \gt0_rx_cdrlock_counter0_carry__0_n_2\,
      CO(0) => \gt0_rx_cdrlock_counter0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(8 downto 5),
      S(3 downto 0) => gt0_rx_cdrlock_counter(8 downto 5)
    );
\gt0_rx_cdrlock_counter0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \gt0_rx_cdrlock_counter0_carry__0_n_0\,
      CO(3) => \gt0_rx_cdrlock_counter0_carry__1_n_0\,
      CO(2) => \gt0_rx_cdrlock_counter0_carry__1_n_1\,
      CO(1) => \gt0_rx_cdrlock_counter0_carry__1_n_2\,
      CO(0) => \gt0_rx_cdrlock_counter0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(12 downto 9),
      S(3 downto 0) => gt0_rx_cdrlock_counter(12 downto 9)
    );
\gt0_rx_cdrlock_counter0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \gt0_rx_cdrlock_counter0_carry__1_n_0\,
      CO(3 downto 0) => \NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED\(3 downto 1),
      O(0) => data0(13),
      S(3 downto 1) => B"000",
      S(0) => gt0_rx_cdrlock_counter(13)
    );
\gt0_rx_cdrlock_counter[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[0]_i_2_n_0\,
      I1 => gt0_rx_cdrlock_counter(0),
      O => gt0_rx_cdrlock_counter_0(0)
    );
\gt0_rx_cdrlock_counter[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFB"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I1 => gt0_rx_cdrlock_counter(4),
      I2 => gt0_rx_cdrlock_counter(5),
      I3 => gt0_rx_cdrlock_counter(7),
      I4 => gt0_rx_cdrlock_counter(6),
      I5 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      O => \gt0_rx_cdrlock_counter[0]_i_2_n_0\
    );
\gt0_rx_cdrlock_counter[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(10),
      O => gt0_rx_cdrlock_counter_0(10)
    );
\gt0_rx_cdrlock_counter[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(11),
      O => gt0_rx_cdrlock_counter_0(11)
    );
\gt0_rx_cdrlock_counter[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(12),
      O => gt0_rx_cdrlock_counter_0(12)
    );
\gt0_rx_cdrlock_counter[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(13),
      O => gt0_rx_cdrlock_counter_0(13)
    );
\gt0_rx_cdrlock_counter[13]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEF"
    )
        port map (
      I0 => gt0_rx_cdrlock_counter(1),
      I1 => gt0_rx_cdrlock_counter(12),
      I2 => gt0_rx_cdrlock_counter(13),
      I3 => gt0_rx_cdrlock_counter(3),
      I4 => gt0_rx_cdrlock_counter(2),
      O => \gt0_rx_cdrlock_counter[13]_i_2_n_0\
    );
\gt0_rx_cdrlock_counter[13]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => gt0_rx_cdrlock_counter(4),
      I1 => gt0_rx_cdrlock_counter(5),
      I2 => gt0_rx_cdrlock_counter(7),
      I3 => gt0_rx_cdrlock_counter(6),
      O => \gt0_rx_cdrlock_counter[13]_i_3_n_0\
    );
\gt0_rx_cdrlock_counter[13]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => gt0_rx_cdrlock_counter(9),
      I1 => gt0_rx_cdrlock_counter(8),
      I2 => gt0_rx_cdrlock_counter(10),
      I3 => gt0_rx_cdrlock_counter(11),
      O => \gt0_rx_cdrlock_counter[13]_i_4_n_0\
    );
\gt0_rx_cdrlock_counter[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(1),
      O => gt0_rx_cdrlock_counter_0(1)
    );
\gt0_rx_cdrlock_counter[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(2),
      O => gt0_rx_cdrlock_counter_0(2)
    );
\gt0_rx_cdrlock_counter[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(3),
      O => gt0_rx_cdrlock_counter_0(3)
    );
\gt0_rx_cdrlock_counter[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(4),
      O => gt0_rx_cdrlock_counter_0(4)
    );
\gt0_rx_cdrlock_counter[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(5),
      O => gt0_rx_cdrlock_counter_0(5)
    );
\gt0_rx_cdrlock_counter[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(6),
      O => gt0_rx_cdrlock_counter_0(6)
    );
\gt0_rx_cdrlock_counter[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(7),
      O => gt0_rx_cdrlock_counter_0(7)
    );
\gt0_rx_cdrlock_counter[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(8),
      O => gt0_rx_cdrlock_counter_0(8)
    );
\gt0_rx_cdrlock_counter[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(9),
      O => gt0_rx_cdrlock_counter_0(9)
    );
\gt0_rx_cdrlock_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(0),
      Q => gt0_rx_cdrlock_counter(0),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(10),
      Q => gt0_rx_cdrlock_counter(10),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(11),
      Q => gt0_rx_cdrlock_counter(11),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(12),
      Q => gt0_rx_cdrlock_counter(12),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(13),
      Q => gt0_rx_cdrlock_counter(13),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(1),
      Q => gt0_rx_cdrlock_counter(1),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(2),
      Q => gt0_rx_cdrlock_counter(2),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(3),
      Q => gt0_rx_cdrlock_counter(3),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(4),
      Q => gt0_rx_cdrlock_counter(4),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(5),
      Q => gt0_rx_cdrlock_counter(5),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(6),
      Q => gt0_rx_cdrlock_counter(6),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(7),
      Q => gt0_rx_cdrlock_counter(7),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(8),
      Q => gt0_rx_cdrlock_counter(8),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(9),
      Q => gt0_rx_cdrlock_counter(9),
      R => gt0_gtrxreset_in1_out
    );
gt0_rx_cdrlocked_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => gt0_rx_cdrlocked_reg_n_0,
      O => gt0_rx_cdrlocked_i_1_n_0
    );
gt0_rx_cdrlocked_reg: unisim.vcomponents.FDRE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlocked_i_1_n_0,
      Q => gt0_rx_cdrlocked_reg_n_0,
      R => gt0_gtrxreset_in1_out
    );
gt0_rxresetfsm_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_RX_STARTUP_FSM
     port map (
      \FSM_sequential_rx_state_reg[0]_0\ => gt0_rx_cdrlocked_reg_n_0,
      SR(0) => gt0_gtrxreset_in1_out,
      data_in => rx_fsm_reset_done_int_reg,
      data_out => data_out,
      data_sync_reg1 => gtwizard_i_n_5,
      data_sync_reg1_0 => data_sync_reg1,
      data_sync_reg1_1 => gtwizard_i_n_0,
      data_sync_reg6 => gtxe2_i_4,
      gt0_rxuserrdy_t => gt0_rxuserrdy_t,
      gtxe2_i => gtxe2_i_8,
      independent_clock_bufg => independent_clock_bufg,
      \out\(0) => \out\(0)
    );
gt0_txresetfsm_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_TX_STARTUP_FSM
     port map (
      data_in => data_in,
      data_sync_reg1 => gtxe2_i_4,
      data_sync_reg1_0 => gtwizard_i_n_7,
      data_sync_reg1_1 => data_sync_reg1,
      data_sync_reg1_2 => gtwizard_i_n_0,
      gt0_cpllrefclklost_i => gt0_cpllrefclklost_i,
      gt0_cpllreset_t => gt0_cpllreset_t,
      gt0_gttxreset_in0_out => gt0_gttxreset_in0_out,
      gt0_txuserrdy_t => gt0_txuserrdy_t,
      gtxe2_i => gtxe2_i_9,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0)
    );
gtwizard_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt
     port map (
      D(1 downto 0) => D(1 downto 0),
      Q(15 downto 0) => Q(15 downto 0),
      RXBUFSTATUS(0) => RXBUFSTATUS(0),
      RXPD(0) => RXPD(0),
      SR(0) => gt0_gtrxreset_in1_out,
      TXBUFSTATUS(0) => TXBUFSTATUS(0),
      TXPD(0) => TXPD(0),
      gt0_cpllrefclklost_i => gt0_cpllrefclklost_i,
      gt0_cpllreset_t => gt0_cpllreset_t,
      gt0_gttxreset_in0_out => gt0_gttxreset_in0_out,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gt0_rxuserrdy_t => gt0_rxuserrdy_t,
      gt0_txuserrdy_t => gt0_txuserrdy_t,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i => gtwizard_i_n_0,
      gtxe2_i_0 => gtwizard_i_n_5,
      gtxe2_i_1 => gtwizard_i_n_7,
      gtxe2_i_10(1 downto 0) => gtxe2_i_7(1 downto 0),
      gtxe2_i_2(15 downto 0) => gtxe2_i(15 downto 0),
      gtxe2_i_3(1 downto 0) => gtxe2_i_0(1 downto 0),
      gtxe2_i_4(1 downto 0) => gtxe2_i_1(1 downto 0),
      gtxe2_i_5(1 downto 0) => gtxe2_i_2(1 downto 0),
      gtxe2_i_6(1 downto 0) => gtxe2_i_3(1 downto 0),
      gtxe2_i_7 => gtxe2_i_4,
      gtxe2_i_8(1 downto 0) => gtxe2_i_5(1 downto 0),
      gtxe2_i_9(1 downto 0) => gtxe2_i_6(1 downto 0),
      independent_clock_bufg => independent_clock_bufg,
      reset => reset,
      reset_out => reset_out,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2023.2"
`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
TCSZ2Ye5IRfHavlF8Mno1PI9xixWuSiNh3ssU1FQtkjW1fmNtc2c3x12slL242UQayI0rzZTqe6S
edtecLHTOnzxXpCZjjU8NFmgLPerTSDZ1W5YhyIi9j0Ap4YBpvaA1ojM0+r0Cx+dMOXohQGeyljq
+fnTaFTUe2678DxpqHk=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
NIzZVKMB1/8UX2qb4BB+VXCafEKXsrn7axB1cJDPqDCzSyt/2KG1NEEZTDHZVzIr8Bf9501PyXmL
VowTAAXX/RopKyKOM1xJN/qLtqXxegH2a4dIkUxDIIclIcbv/smna9VCwI7m6JhrnKsNciTTilgR
27S/h6JPpZsZAEmsNxxTC70WQhQSM8TlHJjZg3KDc5KTnvC/mVTk6I05U6x0Bdd1YR9GBvhwRqhP
B1ukL/1JVOwR9Ce9p+EHFE/xyApypCjQPGwq+8IFQgS8wltVZHX6eSMw17Q0wGCY+LHduRTA+abV
LvAR0NPf7PKQUSCECe2mBbLPO7wD4BO5RAkJeA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
udw4XxxzcaDNM1qWnRgZ2JEM1MMqnKwKVqha/krU9EyUAsyATjQEMBqjlOHw5QXMU2jjizlL20Nl
h2pF7iKo1S+7TS54Y/UIJANp+Dl46V/qfy6/yBnE4YclHON1k0jRao4C6T951tgXuCAIQEmXbr87
aJfL2dNqORH+TDKUBdc=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
JFe89rPDaiIUicPDNoXCg8pJUoYRyVDoW/5yE4T0Cp9xDHtuIyKQVbC7jVb92OsgJ5GHDm7DH2D2
rYZKrdCIqPt2jo7DG6bcJuDFcisZb11HLlYWNsK2Vqs9DdsTPViykeE05CD5AgfDxb983x8F1meK
w8zjeGoD44djsaRA+lvP1zLhl24q5LWFJdPSyIT7uWZwhxHqlyJu85ToXLuwZQZO76Mp+1mitxDy
vleizC5rnk/4hqxfEFS21Qi1TwCz5hdU+H3nA3dTe1KRY+obbFP7sRWKfmr9Rcf9enRvbaEbLoJA
9ADkl72jc1Aqlnd+YCGq4EmbElbWLxblpamncA==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IV5qOrW4jXfiGt6hz/YcNm+/H3ij0v503CF3Kvut91tUWldzNzyLt4pIZEWEzSmn6RcpcLNN88po
1kt45UdSBz+mL5HDQaw4J+VGD/cCBmW1jnOclCf82kwju1MIDfa2EKicjqaykCUROxV7cwg07FFp
clLfIwd4kxgSWnGzeZi1IGezx7OpBsAkBTz9ha4WttEm0+D29DF9O4GaQl6q8IBeA0QIrO10EESt
slfRi2evxdOeTZBVFoXU91OszneH/prZqyCsHeyvTa8PABTZ+Y4CH6ICZCXRn7QTNJgoYSGABuPs
87saNJgzomjyaO6IzGl1fBgMIsIurKw90DE8Jw==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2022_10", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Eek/EeBa5kEiakvxzHC3fZ0NXCsvWnLN8FYKLyImepfBUv2jdVDY2j6Qs928DJaMO8pBuO9SGcu3
47rhcN1DAjZza5Ac83W24fngY1+YWblivVc8AoggXS1t2Y7Dy1vf9+ZxUdOvq63sje+fDJxapZwK
3HQGdtBX86RTaUS5K+HyI1FTmcIhUYmJWmxQjIxLla7FF1QZ4XpTCfqAG5i7ZKlYSoDFb8sjCRG4
XWFuk1dbL2UfZPxXZ7XHIm+03Ck/JsHtsjLCc8oTB/9MLom2HX9SjX8H6tFbEXR1NatCFWQ04JKL
kHSYD/xDlwjhN9CRvowRhNJaYSmKQT646hlNoA==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
gaID+cgqOdyoJPDKM9JAyimEjoxNakxuNjYf52N14HEyn4NQksF7thq/bXWc82vmfdc8aodx1+ky
i8uuKszW1WwV+apGSqk7YXBCxx3ACfMsPzNzeDQ2HVzGfznpQD80Eu7I7iwtz3k5Mr31iaeM1kQa
oddk6CkVESI8CD21PQHMVeu0LKLZJp8k8NHf3i0UOXsP5o768iecieYQh2VYXZ6HORDDyd+IpDB1
CAFBZctXco8C1w74wCB0LXUSYInc5ythxBURkPPTJ1GBuXpoQGZD2sNiI2Htl0y1toEdfgExWZ+0
+4Docnd9TgOGhAhZzUcj3c+6cQNbgCB847/G6w==

`protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`protect key_block
tEBTlBoXowC0cOrrCu9pEZ6t9FjMS6GWThbzsXMvR2xb9HBcccdvXT7EfASM09KkNzvlYoyNBWct
0TRl1BNzzlomu3G857U6kezS+CCRF/K2qOhhxHFxEfuM0qblRVdNHoCGGMM4PkE/rt9M7IqYoXQg
WOHI1ydpZZn08aVL9QYJgz6ZuVHNLwSpL6rjFFDXV1cB82gVFBkRP/0NxpGW2WH6YA/MJ0czV0ji
o0umOWluEwUObdytKX1lfuNYimI0ziWrovqq2osL8J7NBKDUl2R6gJ51DObsTBgC8uyUHVibyNHy
nhzTpwcBeeXdtAueCg1BlHDIwglcMUdy0sBZEyHM/CLzpxgr1A+uUcmzlWx1drrc8lRNwGMFDDJQ
9OzoHBABtNt8N3bbO8A+rE9HtsjMVr1TxHhUTxBhWcypwra+xzsGykln/IP3JBwwQR0+d+V8/Vec
5Bh03crJTvJZUbYidozNoaPOfnHi0NxFDNdL7L1i75T+H6bqeE1ADR/4

`protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GM0AtcDH85MjCjnA/pijf0ZCJap8X+OYUX7W1StOwznqG2XPd9DhtvTyqD0c8/7BTdeCzGUK5iqe
QiGwEcy1dCrSVZW0KtjFXllkYV2ai3/Qn7Bgg1YuzxifEFKe6ClTsByfgjqRdyZeNSAldwvx9ZtT
0ZhijV96K37zXwfXFeKDmxOZOV553ovWfXGekaS1EPmSluoDYBMQKc2XV+ZUXR7n5NI/6E3QdK7K
utsZyrFYyJdYW8Po28hQf1nWeQP6+PxQB6wi/P6sUzudntNcQ7uLRr4PTz6twPPqYwUF+7YW8baL
p/2EFPf8y6fBb+DOBCnzmGZvmq+M2qQot14r+g==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Ot2lizvbpt8emXxoZl10oi4H/aTQTzrHDg0mf7DDs3BS7iyFsRmaQCG/BRF/mwjlw+EbST4/x/DJ
d6Hf6LIa6mllnMD6G+uVHQ1z31eASHPw3m+WnPMr/zCNuebPcDitgiXWmq3SAS9byYvxiwcDvjn6
CMh89pvlx8xLiFUoo5j/lAPe4cPBJwSMleQLLB989s6rByi0lVW0QiLTzakaB8DHBMvhIYEfi74m
Lxby6+nYRGrAUKPOemP0Ag/LW83Eup/Wa0jVOtxzlj3foiYhg2mWCt2zyFhgQsDA+oEsDa/KZc0F
OUzOI8vFDrwPmYRwd2ejFI9Nz3/1mb05VQmDRA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
OamBwohUIPOIa5bsJvu3upqnGl4f3jNYS35bg4S059C9qVDgQJcw/it81deNA5zFvzX3Cq9CiO9K
zl02VsdpFgNPjSwEO8F47LJZ4fHx99EmESBogsNwUNitzkuYTLCb7F7ZF2WSJExQ0KsYt+TRp2UH
yQEvpM1lHQYUXxzjw08qUI5ssSnOsQFydvP8BwA/6aGrVJ+LuEgPVdMqLBn0EeAmRsynxJ7OhPGV
DlvHQwtVuBrkvjQHED/Ye43ZIeWPm/xOcjNfZjYeOvdEJqTbaviR0Fo1LFx8EX80uvdFeK55ywDN
wzoZ4cVH87f6VwR1xHdo0JpVxrajZpSZ0jPudw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 18112)
`protect data_block
Rz1ZuXu+08StTOUttvRdzFvV0kIzl8YJvYNWWsnt4fgBoB3S0R4Ma7zLz0sbk/FeiU2Morj9ZyFh
+LGXeTLGOyYFj/Mp0hr6UC3hU3SLKJyJOyBLLpHOJRt1aYfpdczoFFPy8JXBvyWSFC7JOndJ+Uk6
eEsiddzIENDh67uFqvlbBvLCpydBddDsc856whpwB+EGu8sXg9QCYrykZIna2WohF98yhgexobC1
MwljxkbQry+q8n5oqkt8sQj2Ay11RnFugZhjcXTBQTBLQ3Q+yCUOJoUQ0rTJtp2n1JyxvsfX1vGc
M02rdHkLy1Y1vfJs68gn6lgwHp2Z3Y0dPsu1A8P7WiQ0n86zCyDMB1epJABcPDc7EoM+XxK7AfN/
CD6tAzBlSLjYtoHxWDCk8cMolRh6g4xjCaKZr/KWRDMJKDy0OZdAh+AKNwYiVZ4Lg1yqIlw1xx/4
Ujf/cCIMqkHEz0KH9npQ/Oa7pjPPMS9p1jzTPLGmGYvH7kjb5Dukbp6YNY+WeSRIIMSe5aK3aB7f
O9+AyXW6FyqKFQ5KrrPiZtbBYxZxEwL/IEcwixPDa8b3tNRqQoW9HJRzTUGCq7JOJ0+j2B7EVzeD
I09dC48xhN0QYuRrYGdwwMggbFwFvhYBwexuIq7Zlo4JHA9gwZvqFf3DKd1qH+RB0TGuvR7lojxe
Q1VDxIt4cPX56R/9LXXI43k1uR79ll9MsWc6OQlTYwRj+JY9aLctt/G6ARTEQtOQt2mCS9KYuNKE
k5EvAy+o0nNFMwjVTjjONAkgIIPdGhErUEXNQGi1XyH+waWStbt1gEoaoFku+Y5BYt4YFGo7TpxC
O0WtNqDOzCn9vA6tGnxR2boNwGwvP4KXjvTpnuqdIS7MpD3IsbztzxZ9cviy2f3wkfWLvSIht+W2
5oDgQp2ExIbrLczqWga5sH7ZUZ/SsqzpX4At8ye93duEU7cqRlruvlkzu6BosM0yQIrWeHiQRftQ
oQiAq0zjL4oRkAT+rfEILUOQ8KZAu2lZ5B/+s1y1JVwelIDEbNTYUCDIRlnkbYkNid/xoRQd+0mW
Ge3uuOc0seJ+aoqzdfiMY5qTXX+TMArFo/jGlZQzosLkq+E3zLA/ULlQtZjo7I3G5fRo8yh+6GqZ
m9oiolmsYJWaRS7q627YkLMyPF8G8d0xwmQioC6j1wuFzgecMcs/gkgt6q2x0ZgYQcfWT/qDyO8i
RBHiqzdd8S+j6s2sZS0FMxqitbFt/bSkkb+lSA15kSZ8RMtdvdEd5rqCLCThiKo5kS8eWJPQ/3Kk
wwfewa9IbgRE9ebFYa4D0VXB1oSZX7A0qHGihZWa8Zb1Rxf/+CJfiOnm9gN7+5plx7hkFGZi4dtw
g3K5EMfqSuLn242j7e6f7dFDds5dvOsRYWwlq5UF0RPJfZp3SXErD9KQR7PNZoohJ89YVN6g2Ccr
u0ga3RrdirdLRernoYwkwf6VpAVooIY8V+ZKSmxkxTz15ZNVXJuJuPkmjn76jiHgcNTHSKqd5yn5
K5wTqf7ZB/PzKMs5OoIQYJcmfen1qsV3J6YyTbRyvDGaiQdD8ssrW5Fxcr+2WzAg7YC2spOOkgEf
KEN3nmG60L0OUSs5NEMRA42hr1J3E8SryxbNtMyy3xLy2pHeu60ARi7plg4SL2bK5idJOJOlr0dE
WY5f8I8tASGVbY7JeyTFmabrBichs7Dm4SLM09040RhJV8Bt+6PYovuXGHbhmAx58S0kErVIQrbe
WZ6rIt3hMaWUkFhqqOcMb1kYnxnPqRsmV2bvLARAC9wio+IFExvrbqowfrtOKL7KNRdzRR4tN23w
xUaEykIYcn6r14AGOWmdbR5BPjIZtP5+vOWHMM7zT8AC6gi82azzUUuya/fMqwxlllvyXxXHXuY7
xdCqWnmIDOpc7/ZPerrX6y82hn8xijsw579WYzuxj5cpJ5U0x8WSHRATpE7VBiBJgLaEhCxWYzEl
Z8aWVwUM0yXRQDDS/ZEujp6JykgklLB9fjzTtG+tbqu/pnS5cuTJbtF26JU6iOHZ1OzxtC2Xnnem
2BuTgvqk4BSL9L3VJfzpSO319SB6SJDOpFqj8mWkx0NXIUPqpWMkTRoNoyon7qmyx6c7XrAS+Ppz
edzwExSAVgunqxqrLyaRJUGfp5PF6Q8KQKwBq4GyPRguH7CZZyI5PpvZYKBdnQyqXSFcWYaubvtx
6+/os//jdvAHrp6epy6AOaE3L7Mo+EAogq2ayt6uhWwy/QpBdTvI7i2R9efmem43Bo0EVM6zUQXR
Ul93mKXB88nY+BTM/hXbciq7nmrWLWQ0AbJ8hAJeyidxKitFC5xYFgIwkryf+A0fqq5Wv06+zB23
jamYTDPpkn4u4y1Pw8CCJR52h/h/QGftg0ukd2bl5bpcG/n24qzbBljyzALvH2SezuD0WMOK4+9B
LN9MK0vsBd48T94kSj6FT+iAbnjcMH2uLJVAn4FJvlPQ5Txd8e53qvx5Gbv7xtaSSuxytiLsRWH/
J6BoYaCLj6M/XezAsiwYIVtcBizEE0Bur8scR43Nqy8zGCQQ2sDrHP18o3DSaf/IqMMCahOmnBMQ
EHjVwVe9rvzB9QK9wEZCOL1Mpn/FMcyQF+7LAp2YGun6olLDg9h9hqIBgZAxVWPThSxMMROTiUqG
bX4tHL/tFsanBnDpW8P1VILsdMoZ1S3CVzhX6C4cCugqs2fIYQqQ97xy2/sOG45v7zTpQFsrI+9u
kIkwv6EEivxHYJAGD99xQrR03X44ftstdg6gb/NVfnXaDsrU/J0Pp1X136E+5LO8zIT+p8qdIu8y
4OtTG9fd2GqLJBOBRUb4g9PzJWXm9n2fJsMBcdMS2l81FFXy/CMOB/Zt3e7g59GU7DelGUfuiX0v
lsWDbIJS1G96TeTtlINhp/eFLdgDIxTGuZ4fDVgYQNS7uZbLvnmmLIqYWNgcL64dmzXfS6z+9Dk7
9lX5rVfyI0GFeHBVvmHba0rTlax/bc7bwUFKN/LxtLGsz5fAKuis7xL/8f1s3D2oIrueAV6VzL0o
8Wf1u/gpLSopejio5DGJOmmAc0oJBWQZMeuwXhdtoxuHVooJtUoJAe225KiBGoFbZvjSowu+i1K3
BNZNjYHGZUKO68LNXuVCUSuY926CyE9981+1mRD7BslrFctZmw5pXp+tcdlENIY20jBkvkcTj579
DNbiVS8jlVtPpuCHykUNgj3DeOIUyrwNkDnhUagI5LvcSmizhsp7VpS/l4iVRnVdvfqAkQAlpwPQ
V8DAxDgdyIwl9gTuotlL9WmW0DoUOFgs4SX3gsNs0QjAOy5YGwsGwsB3ipdXxws4hEy309KuHJxO
Uqb1ekpax7KZ1NxvgC+PiEA1ONjTxD3KWutd3d9a3b8aQXAEV6k2uH8+6d+ke6QMld1oUpughREr
blzDRc0WjogAZuwX0Cdf6+Y8Do84MShSQc0vTFzUtfd+ja9JglZoszpydbjxuibmyyxEMLbV1WTs
2cnyxzUFGVNWU8VCX2pRJn44b4nnHAVhw8VTqq7g61+c4aYtvTSYPx4QFfYofT0F5fvO6zF4B453
NpHBBwYt1+KBz/mdPaUNyHZF9Oj+jEesCBvz4q0/DKrRZgcxjen51JC6jycKsz+UoVIexwLy09pd
wqR9JwA5vcmNaKtZDknSIvAbUreRz1ND06A3pQjNicBuSoOkmnBCGB0T444bpHMWaaL7J2eRPkfL
Fdu5QAUFtFvcfXZREPGQchgh0e2B4XtIMYQHP+fA2wsajuTj62Ljrbozc7SAYrUOCQo6gXzk4yao
9y5QNlc165ksCHUteq00Wo3HjcAcgQ6/5Nn7tG5W9dJkJT0fjy6FQWMliQuXFAsbRT7ldqBiMUfy
C/Dl4u0zbHi5D0pgD137JHP2BhN8Lhn8AvUnq1c2IBkGYaHK0Q+bqALtKQtwxAucyGpI0ukoktx4
k83nV7gzZY0LMo0gCxQjltNejfM0WHCREtXUCW4eeHWu4mzfljCXJwRS43KvpyAxZMvIHZzU2gEE
M46BX0KpyThXAZL3S09zos/sQ30pWTvTUvz9paKEnsTyrQV/4Ksp3TrjvKoQL4N9+0fssrkfUt5E
oC4BeG89Bhsf5psFTsaeBo8Ys3zdNt8hr0T8zOzyRxNzJ9Klotu15O9V+WdiCHZymjnktwvIO4wQ
xixC2OB5OPBJTU97qVDLLkwiPV2Ilqec5q+k+gHLwloi2VLUVDlRoJ+ukSnHNlGPP6mobRPbi/VV
b6pyihsch4G7HKlZZm5qYYskqCnrWC7cWj3zvLA9HHX0NJj+SzIZXTPGn/6zxyCMTSpkC0pNL6Ml
k10YDflSLI5CUihaGMPyucxtvOvmqVjkRHojszmgRQebMaHbJ1stmsLWdlg5dgdy8SESt1JUw+79
Tve3jC0qWAWVFOLpkaP0rQvRxauQJEzVWYNeuUcquxr15LcWhtep6psxLLmsrswSNBrzvGhA3NcH
F/g2ocBm2STtp910+jU8D9dhwKE6bu0G+LfAKklU19axN92MrWHsJzfz82msU+pdbq8WaypJ8Mwn
NV1JcucIZMZFeLGjpeSSjHqbjjrxlw1slIY5ZMsse8xCeld1sgMZwzzHylxZ2gYUAhvmMr4pPcL7
9VhyptMK8M58S41snS1grKh4fo5z5Mo6bD3tg0OCV6hW28BuCWXWVZ6c9oAsonQiMkBKETbjQAU6
/LagvVtXkmASMCtz5MBKf04/Ac/5/OHe5dFXtgsYmW4D7Hd5OTAK0CpWkYrsMiAqoDHCyCF0GEg7
Hw+/YS9UoMby/bEFzqi4dhuomLjKlosxn2Ye9P7QIJz4MLGLkzco1pjrPshd3Xdi7aUO1ITMB9sG
sBUAXFE7dzvWKLt7WWYvSkiGyZpWw/s8u+BmgOrdnIkt/dG+V5nJYROXJYLmf0cQWIjCocSH00A5
+BzZ0NSSvydGnXlWWT86IXkuCbdiOi+ed4R1ItiexCJYRFSO2RRlXjfUCVRqRMhHaaPPNhGDVDHj
/kRIYZCsR6xHhQq/rGKwLuF1oQFncWmJLSBSn7OR5WofVfz/+wyirU0q/dHH/JwK+1SvmdabItbJ
dPN8Hvdwr5OVg45z6b+VmUCug1NecdXRgwjn8OsMTIuc1dLLoG4GYyDk3yrnmyoK9pLcq8G75O5E
+w3gCIF0TBE5luxJ4FEdp5eGAuKgj1kIVkRiDrroBQZPleN2UNgkFC5Ap9TNk0RkAXSmvfpp5BtY
Z9caCg7NRwcqITlD/K9eWuipDhVg4Dc7Af4CMSa2II/mPFdvar7ygtuGJ63p9fYlkDGdJcdmY75c
pZBN4PV3kgtFKMBDSs+nbB5pWzVFObLjrXRXous6gSCMFoJraAI7CJrW71hQDSyOr3hQZRws4Sjf
L3YEtN9jOKymNy2ymRsDh3vCbqZPMONbJDn4HdYMJHwTMvL8zHEynV6CRMyRlMpkLqXfImC6R4f4
VRd0R+/MsSTjKgyDiqTrIXe1nCb0dEYw/YuP/LcfXHRmktL6NZqQoKaoQCAEJco4hB7HDuF3Ctoz
3nZc8NTr7VA1RcOKA2T6m3k9B9ZsDPfyJjPFfsJ7SEJSKKPGSbA3A3iPWoetv+CWdWDo2Hz8C/TF
eohgEDjJEonaEnDSfSDKKWW/xouthqEKeA66x8VTYtKe/OEyFOmt3EdWzExMFZdN2vP4IEzG0Kwa
VCIb5J4c7XS1JSWiLmmbrbt9j+fSWORJOZQG5f7fanH6nidzhJC2XJ7mHwoRYTmVWHLKAjgQ89H2
SxX9ghXU0Paapu18hFEvSpd03Xpc62gaAzETvHG79H9ikYi9X8sQuytS4UZo9JWv7IabddvJ7mG9
1bGBrgnlbUGXWMK3HLqa9Q9Rq6Kt51XBbR/SLVP7uP0TucXby8WBVR0oqlBO1dRKG78+5hWQ02RY
g1f1iD8/mqMi2tNdNkrN0eHimwDGO7HoNkzjT7FenDaU5giZgVGjuTY24/HBlHxBl512qRu/BtYw
06Y1qgz1JgRh1yZFoY/NcDGG5gGer5ZlBPpgPKebpw7zSN7szWUWDzKs8vNTLK2/C13a2UcIpUdj
psToq4F5e+updvEm/TZ0C37pa+ds+XnyVBhwLscu6RObUw1EbzI3fQomeVFg3eYsLX2/8FVec+Ok
O/FtKcfuuiT8SkF5giIY9jAZPoCPavhDE/BaSJgiDRUFQG5mvv9E/QR34i8kIN4s7j2uBD0bAbux
qWCzxthf69+kA9MYqMc3mmD/lwm+SrmhI8XlE0C/Xqk5zJdQSasP01sQ693fQHWa4kPXZofkCi1F
Czgby54XF1TRPUH0+45rynGSdkxV2RIuQsjJEZWn/JMPYOi/c2gdbumL9Ot1gZakm9RNNx0NhKU7
khsNfblFlRLTzZ4XhvKwlht14nvrLPa9Q8FZ9At5zIi+nWCBxZ2e638Oet2C/N31HIDrMc4qMMog
jjV8kMMFYI9ndZvZWcSAzUVOdXQK1y4Dbjh/g2cJ3uoHnT/Lip0Z1ZzBDxzHOnWFKPIXGG48n0dg
qadV7lwe4BZ5Mam4mH76BymukMlfCFoM+Ybbu5fD1tHthXl/Zi3OmU2buh5JXU/X7IFZN/6BvL4s
rHV7/Xvvp9PjvQMNU63txWdJAbbwh2yl25q/u53hEm74dVV1cMHFDQ6YCOH6BOO0Cc5lfownouVB
VuJ3p4zUcw2ZY0UF820fWP47XsQH1Q6OOeycCaD2K0z73AQKfON6lybUbMuB2v/9SGaEYm1oAkVl
9HtFt/x35ko90PJEDTmdOOnMEx1JkC1m3rag4bbmO5OQU/BgANMcfKNmW+hfYoht4UC5iwnF59Eb
kPSXTk86SBPoBvuZH7+CQTMKLuTKxh3NxTgWJL+iS9kR2iKZ3wBxuW46aNuomxGnISDQ6YYTWTN4
NKGajRQS2/hasRXIl1sVjlSPbZayFTub9OdzO5tKoyPqPgpDGDfIZQicxIqc4DMsOYFWYI5RFUVL
XBJr95c70M+Jliijayn5Frp5gZu6t7Plbmw2aMnAsvQpNKkNgpnU2YaG8wTRac47JaWAW5+LT8L9
AwLYaGk27Lpt4aD8B1zylSXvmx9jGbSxXdugqZOo7yROUPyJR9VBC1YZo9p9Q4J+kHU7+k/dH6Wi
dX4CF+CYc7M/HSk+Smjs6pNgyzm9zAJV1x4slm1hha8fJxrRI4vz75NpumHrD3eAXXbHd7SPYE72
1/HTFKQO7cMYv4VAgu4w0wlHaKU3r7sWoDzHVkA/+yv9vPHIqEQYocTJiQo5KFR2nGDee79/Bhx4
w2mOpfvnPwAkoKxunS2KgqalnS123dASGl6LmEMEjiLB4bMXrJaG+XNYbkoXOQlk6pD1ibV8LpEn
PbbJ7XZ0cM5xIjXIZxwWd3Lwz4MY0YG2Osa8VSpIbl2PldUDM7vptRnsSf2e2qXap+8f0mT/q4/h
lqD0XpxoEOxedy6sajGhO9u17A3ePBe9wex1zxh+VdAYsTs7INrV8iVBImLU6mzPrN7E2NGeiA5G
IaBD8yP2G8u8Og2SQ3Aoenfn7wFBOwJuDnuqzddoS0F8gFb23fGdWyzo3LhtzUohxWYrWti3CYGu
zY2Cu9cYMjijaRqdbrZrWUi1gPhPW5iQB+wQ6kmCh/9NwEoCPTzoJa7OOP4x7XyOah9TS8aJSNnR
YVQ+w1Ola1M8lxoJSCeo8mppzSsumJUEtLIwAbbne2DLph9Q7nyKO8J5yedE+DTL9yZ+MnpJUUcw
YQsKj53ysuxaglB7IU3nQLFLSF8bqUuCSwjqV/1A6qTBAFT3XW7cheYohj/iC2vdA4haA6bXTgui
uejcD/CPlm0iN3Kp5/DwdzH4DFwXPtezZ8d9cRx9tbnzQvmIOgy/S4c8AgU04Uu8NMzguuY6X8UK
RJGWELrdCfm8IWmSLRR+JV+uTxBRPQ/o6wg5z7dt43phJhJFBqp0XmBad2frQYJ4rjU/owvwjew4
vt0z42PsU7WDeSwAGA3oTO+9nx8qu2ntVlxN8m8ZPjB6j137gFLZlaWAMPUl6FQPeQoC8PofCSMx
6E0aHpaY+NHMo7Sg/jm7wF1YHOjmmTob3jCEElD4or2d8JQAIvpAwj6u5M0q6kBHGsmRneA9KAO8
E3/MLzVoOk2hlnp9acLwMQdteCT18tjN8oSO7qPrLZ7flxhQIAVYBjvSmjn4mYaeK2lec1WadPT1
wtupYFOT+u+EAG+Sn4InCCjE51WszL9VsfkubMaDznD+RwWI2vfM6k9oDJ543vWyhRqCqEDl9WzW
Yrf1etWTNEh0zBGJVu9nP1wm1h2PY3NtPo9EdcjNkAbEYwc2oba7fQKH4zlH0UX1O05mYrJrleZn
RL86kraGBWp5cN0UD5R4OEDyScadAaQFXJwxPRfzakllWG6OUE9WKMDPWxsZwRtoVtm4dxtdUDc6
bWkXrBZEVR76yh2HSVpObRZyVOdQFqvwlAkaz417Dx/Rkw8O59ow7e2M0jndBH73X9roZaDMKq27
wU3iIuQvq4XhbwvaeiaAHdsfvjfbwZ9AuyJTrJ+k7y1h/YcgxtjPaETMXfVEGFIhUGJXsjd1NOoN
f6HoA5tUyJ4/Q/BFa/NpvCtC6ro7lhCHXMGnF7OCCw8K2K3h1Jad6Cih6GlkpGBOhnffqVy5sA3D
cGTXvxe+dso396apRB4oAnkvHGJaJuq4PATiOLAXQE6ecgWqiwOedD5lnoG7QP9p3nt7uRbKX52b
n/0GLmg5vfulI1k00YZUwmyH+oBxG+xgJC1H8kTfoKUR6Zp4iIUsz8qM3NsLVJh7vF+pHHPJ9SUU
G44XbayOzsVBLXSqSX/SamMXqgdsXqMA/OITHZpKwX4TuwZPTLsB2UWenWPjclnk+RS62LtT55qw
suK3yRKiTltitGKfH+yvGxt8LcIHFEdd9RGPRqCppQg5QkXw/4qEj24FsMlrvWwQ1C9Cssqj9+f4
JH2BfukbiQdvnSt3yFIAg//4Xt7P9cqOg3UvqplqgXlgHn8ZgHbA3kgLpV42JHpMwyWj/q3qK2kf
UKxxKJXZLD5rb7qheS34JYIQNGAyRooJghFXtQTgYsDks89iLZIm4aytQ7LpRqulgt9v01o31Zda
GgX7zQOq1NyjyDT3v+TGyTZNE6l9FzfUNAhU1FY9vPIVWcLXTEsig/KPMm7kE7sEvdW9XQhfMkAW
vqpZRwGB+rSRjwBjfwtvAlL/d7fD3WBf1QvOuLBEL+5RR9CjJzhdd62Jg/KMSFd5dKINtBarge/4
ALxZLu/p9Z/DdKq3PKnZuAe6Xsx64aq6GNkPwVmSl83ByYU8R5JCocTtli7SgmxsErq2Qlr0D/9L
xe4xNx0uxOQgcAseJuGyTHf/Z2ewJMcZhUrsx054em5azgNpGLTC0+GStPkDmXZnBhwLCBw/1Fel
qeZHRl7tie7u7/evDfWGyHRteJvaeJmsB4cK+//pAzK5xS2lJkLWwRL6rzFFcyTL8ZwXnAzIINsk
m1DTqrYq4ktj1n+tprnhTXyuGiw4Oc9MnQo+xG6dNdCeWPoX4aKi91c7v0JA85BIpcPrkSXzyh64
lF1Rq3ZXkSygesoEsffNyp317l0RoDbkM6IJYFftv0xSMXeDupqdPEr16i8WxIyrnyyGuwERjrVz
qdbXyQ89MM9AM4Oh12RED/rPJ+/9z216FCUW+BTqezWhljlij4RstSCeA5NRoyKd8Ur25aiGi1r2
kbP9gTAqHYf5S6h6kZgDN2f054likPwv/iFB52NrhIgHx6bTmjVUAZMyWTj9HLqLfafIkKjHjrMw
av0zCh2ZgzWNiF5xPPsTsU3YGx3I38/ZWxGfjwsEXrFgfZgvSmG8htBUfNbFLvr3Cu3RrnoVQECI
TM2A4NXmqVuiBJsaRFQHjFvgXD0zy3MQONzUyfP5e3Wz7ArndQVLXb8mS73NVCI1fGnmc6es6Ndd
QsjV0+oy0HOVD9XBn5hhLTXuCeNZ2oOsT8/47ZZ/2lQLkfjNeoeJXjLTeA0UMMRCwm18c3WpjTAS
SYQL7qTnQBYPaCSL4Hl8CtRaQ72K40iWpORTJwkhS6AEnWpCGOnbzJYEGIqfcwoEjkwhPEA8ZAJv
5/v3ecs8V36G8HukLsx6alJ8DEfNvFucpQMIwMLUYSky+H7wu/GpHA8NcCjj9zLR/EORo/h87ItY
0RXzCIZ3Hw99P+MfkNtqkNKWFx3f7E20lwgvXjzlJuCGj2r31EMjIB6lq777MmQ9y30ScEjGlBnS
xnT8k+KxfKVUUqe7Uck1VsVBYuVzmxmh2WCzkIIX1YSbNt2Xh6+I1n1gHKxX0jZq6KPA6CUN1J1T
3ISX6bIHaxScu/fqSCodHLaAVwT5ZqPSEcAzixbYMeBnXgYcC0LwM3BrwnJVWkmapqVdjKlqkPQ3
2AGe1DV4dUQL0rlMux2wwKnrN1F28SHRS584ei/ze4NL97MfL6IqUdCCzzileKw/hY3wHJamHwBd
Lt3FYySuVNESci0losjIFVoDvam3FA+iQ6HTX7Mi6eO4sAOIRveDgY9WILkT83L7X6B2yNVoXjnj
tF7+liWyLwpl3WmKSyJJMCnQA3eCEoD3ziuzpk1QpqqCE+iiS28UgIT+W9dFr3N/xH9oEipnxk7w
UEG8sCKfbedxJ3fo5vpeQwvrVVbThTVTIj5dcQhglkif4NGZ8skFkTi113YEAB1DKVAmhh0MeR6B
yFv0pKG6bbBZUEhVShLz39vnsFz1OLEjqoWR+gODvocawmPWpXjnvc40FQH3Ehk3Dm+6NmoMPVEz
8YTP0X7HwZ7LwucIACE8pwUCcesyauWyh3mHjJaSXO2G34IWJC642pN0hkgGoD7hqISURqg9gQ+K
RzHIIDGdbiUds8csInFU/D8vwuWSLz3KaGqAcoVRnWSfF6dm1VN7YYdP+YUESJuCIJzkkGNOLMMK
pNQA4IGsn41LDecuj59+UIQfYzAqhJn/1/l4/HjKxJjYo6FZav0cE+/bHeP73qK8+yCHH4Wu75Uu
wYzLbXhIJzw6Xowl/OMIE8Lvqv08lnDLDpXJlvlDxfp/WKWjis9D0h+FsrOrXQsfq/97t2o6J93Y
2u03aQpxm+FXCIKLw11nPjz+s/qh+HLix2sq9jhd4cVmC68Xn42JAnDNA6dHw+QXujq/ugqnNOlq
ns8mIC33rfebAk7ewgbqSCnoKMu6aldbhy6LcLaRlbrp78brP1LeJwS1YWxyadttF4gMUcA2o6Wf
SRttFzu0LRqOkqv8s+Q8aCAszEEMuB3+jGruQ3c6XNbl2EjYGxd0QN/MmgYTKQVwIPn0nH90bDpw
vAn9UmHql+9ZO54R13lCND1jZCw+F3CuMF7TnrNRlD1T7We0vlCGQF7ZMAIaufhWhP5OsCAkxi5Z
MBH6gikxV6vtYKRhjKZJ/88y4HNUu6XEszN62bzeCfcMxr7TlC2hdgNl42lhfAwgl2e1p/5TQQZc
QZ6q05QE3notb21XQiMlOiEBacc/pK8Nd3DFyOpLC5kpPXzQCA2O6joK7OduloUX+f/OwQsLevwW
Wa+Nx1asVn8aD1FKIep7+qi1M2zgJUMfA8U0RonSoeaHvehy/YPZT1v3lbriRkYhE70wqjkvRpUN
unVSIoxahmEAGKR69a5zumVGfDss9wjMj/AWY3xugCrftOmh9fgKA9Oxzh0t5PbXiP8yTHJBCMeN
4TeXwPLaG63L35bBLc/qnlZpLx07kv55GhAkc4Rc1p98zGcbKnWtBD2C1Gpm1+q3nTHQM4lmM203
X4/jhGo4M3+3d1PCeDL+pr5aa81vLKU5JuJQJmy9whZQ+pjsx0p2ZBib2JE0RRcyCWq3YoI4XH18
nbY364tV56hCoXKl2VFevJ8Ww4HTAS+516Clg7QeA8LhQKlhYbc/w2Rof2bO613dBeXViBVfNV5j
Tibb9p2nZqXJ9+LYRT46uq9wWonGKsJRNZyil8Uy/DL4PGONtsIycSW/fLdzfm5fesH/a3pNH0nL
tAuM2qREJwe8aJ+hKkIYbU9vjhrl2I0h6Oov73z3gzZgQIxUM0SFDGofthTu9FdXeei+HMMGOtM6
n6V4L+fm4Csa3fy1ftcWENuOZHHO6UuSqqVhgNJ3JnLBmsduCRuzZiyK9X/ykq08sEombMuNU8ks
Xrbd48xFo7SPbziJAb0o2B7LQvRsQ73ZPO0hJWhSk61YmHGavCGa3vAuWPNmi/nZ8JAtAUPwmaLA
go2k9aFu3rtJPZ7LS+JOY08JA1Ph1Zjqp++IvVF4Sum9WL5Fsu7beAhKIFZfSeQD+Z3zZOV3joy6
wdxhymxkVvBb7u4/d3AWSjer6EvPVBaEbKpcnqOyzr23sQVcfC5CTfR6nggAdgrO8gkacWriZyN+
ej/eNOYXteTqNHjMggnyZ4HZpzo5GHUPykHnqX788idqHA2s1LLkL6e6eTQ1pEBEFyfojrbexzc3
6Ltfmv4/KX6ztj9oSbl7W/u3dEnKEryeMVUzUQPOjy/nvKP9tjMbW09nFWAj4HdS8rn39Y7qjF8V
4Ztvm11w2P7/8RVCVgXJTXIbi7RrKeBCbjwGZt5LUUp5LIaPM8+ZoEm83wo6aL6mG8Av6vMvHo5y
61QNqR5nhmHb3ObBEzrwOdAO8Oh8oxOJ5dIwCSJ5npuzaoHf6DJBLJ6zmG3GTQHhkYDyo6Ff/EDD
sVUZW0TPQU5IHxiet8bBITgxINKybRgnGiuwVvRc8CAJKjxhh8qJhda4ifo+lgjFpBqiVTJnpBol
7T2ykBHPi+fKRt9M5+615Z6iUySDmavfqkDw7jjiYHlhlyOWcYZAxz/oHjL5ZgcuV8Y2/YG5mLb7
YVYXWBJgvR1V4O/t9SLT46dcygCBkpZ2PBS9j2+6NJFkH/wQsxWiKFjwOSr/tnoEFyUHepoNwlEt
G0M1tf1QKuVZfOpu7dS+g36D6jtFZ1TR/QjXSZIIvU/7py5XMzfo21ItlSLK+NvBvm3fhw5Hve4V
3KCmDD6YI3C4ckY03Rs0njKde1yUMo2u99oBtBJHEpF6EwxT939WqhQng1zwKM3+9FA1/AzbuQV7
vwDtGliyfEGFy4qAIJDW0c3p0Yq7nC0Y4oW0LVt+gGlNP4Q/E8dAxpEtgGQfrkipNs3mjmX56mZP
wNKreMkwGCI7n9PP6XmykmnOZpTmYrba1TRQWTdKShV79tB4au/ozMpsxChsmOXRDzfBvutuBihv
Q4oeJqFYr3ckoviAGue3Y2QAKfJdbC8X/GGEkJNdtGIpFCOOihQHZQ5Ju/v5Vw6L4TywvICJrLvt
q7WU1JlpGc6iREkEJVKvhQhy2t1h5cPTUyjIxRuzDLOAZ7kkWQzIX5W+KCU8WFpjLgqpo97Gdskb
+G6CL7I3+7cjQvYhUxqsjjKWmk0kYy4wycr1ReuMPTKI2noEpLqypG6SpkREqqxIv7Rs3r4gHaZr
vhl+IJawRqejZm08k8u0IXOULQ5aFgqg9RftPWg13PVBfeWx+PpLkNdCGCu6ELydArwkCa+eTkLD
v0RegKNreS6S42Koipn43H0yn3mb3FSt1XFQnS3eCoAZbQY8K1M/7nQSIGR+dMOYzkm1gGZb4GPV
/nHQ3RyFonColBvsGwdUR+P6ASoU6kkHzii2DQLjDuU0GwND/IadhPFPNhk/DcfYEz6mXy0KAL41
s1W9i7rO6jaqMAwEg7J1NghFTRYsfRCgT8ncvt2pUVe/PU5FRqkTdEoftNQLYJHb+W46jyWUHtcc
QuF83KxF3mkRZZz2zZPkqEHxeYSIyGGtHY6SlpiWb0T5sZHCB9OJu0lqeZEmlj1luCuX+ZjTGCR8
FM5LykV1cgtBLff94i0nG3G6jDz0qugFeOPzbT2hcfAS+/1sjNYTS36oaVYzfi/qxmO6y5MtfMQa
NQ/QsPfeKIcDT/gZol5FiTEdNmz/FhOBKBq36f8owYafUPArEGvr7gUvCsAWQ12wDAjTq5wqQKeS
UInmbO8KrQEEunz8sQ0y9F5tjUKECSMEkFcgwr+OIy9ZLKqySdbMAdEUNnGcTOh7/8nuxNTr4Vu5
o+WfRaWFsUx1LEeMbX1XQxAiw5fp1r3KY8dfmT/lV8CVZwx6vU2ZOKGdkHIvU9fw/eE6bTV60wJi
8EzyU+J+4ebHwm7NELdrl0xRlcrkZvo76pAMnrq8gl+PoO+jipA2GUNQUHwz1//Gm2HxZkAy5fJo
T9kNjIydfBhSGLNWxS4dolKWajlPAJpCEYTgKlbg6ylJVtT6VqrAdSR7sf9xNh0ejyWF8iZcZWw7
dEQxhE26sgbYuhM0gRk7dcTV144c6v57oYUtGyqNkGbtQxKwCa+qin9QUIvN7b0A6ZjWeP7EZRo5
6LpQfTEYDuyAAyrN2bCZBwykqxsOen50Jihw3mtNAnsi5ZOCU5KiiZgJwe45iSER60306a7Oyiti
7mDryK3Lh4RPdtXphsTVHBUoC75eRpPrk3Y11mqctM0SKDAvBoK7h6nLKtY1QU6cmSDjXd026LBV
ZzpELVE4JaLKKjuEaQSkwwnFy0FZDoLcPp0PRaOyrNjxLCyLP9w+rxZnofv/rvvzbzaHUrbDJvGX
tj2536fA05NpuzulkMLCyfaFgyAlaZuqCGO5gHS2cCIYDUckuROdYpEi++vi6z0r7T7qhFczDbf4
BsvTWWjQcM9mswdugFcljTEhRQtIuvBThQs3M8vMfuLEzwmRzj3Zxauwrq8F7A00whtgncJ8rcqO
dOIhGegk1lJwsKyJ9AICGXOmL2gu4/cLs9sWyU2c1LwpGGMNFP3zrcsVL11d3OIMIUfW6ETElHzq
Iryg89vi5RFD8oyM9MhD96E0PSrwQ/rjRr3tXwUP5xAvx+CQ/XnqiuZS8S5bp00t1L9CoPudBJaO
ofHHihj+dnvzpYua6Gl/5GhJRiESvqNyRYSA5f3m/FDWYAp7e4SqL7eY1GWRlQU+jtmWfGvS8xhZ
Xi4Q7K+yMUY5qrxhcMhQ+b09Qd40dE9GApNuKRtadSc99RyCQwMjGlnBuLgmdnkRfCr3wb2cINrt
D7ByxLzBSrExQRRiPr3hR2Z3apIW9gHju1noG8BHjE1FcL4VKPstxsLsM+0Ys0zJa0S3rvoKGpqS
i56SJuxrckWBcGKyttlsu4I8cRzmt5TffG8j8vEZihYdZvrGgj/8AKJHdRu3DHcC8vgTu8Jt7Rio
r5f8pl3ZF44n+MXcxuULuOHCBWpqzjUMmEUAbLi1or2MuY1QKY+XqyPYlWTK1T5TxMjtG+juqkZV
tOuzrYfGRVmbLrf70LkIbIEqjqICRpjXjjdhedbos9eoYIwdKTTm6MB8rkOq+kEza5Fd2xBVFdsB
UP1eU8c7ZkwHoVlrdglI9rTz+DSPElGsQ+dQgdLNssWM3cn5irOF/nYUhSSj+PWouI1hIf14mBkr
LqutdoNaegkJuUQjhmaaGi55m7qB1apUwnZqLO33RTmEjZYrD1bd6zzVsXYYYnW/wW+1JCsDaZYS
Qfd3cfrfJWTcq10j9z96FFbLXDLg05qK44SJcl1ZgqgTNVZmHdp/PMdqZfJZJostStG8dZKVGVlB
7/QKVceFX0Vb/MrUK8eUHAR8EY7ALPnAtAGoJWgUr2n0PSrBbbhfWofTKv9PRXw8oDbxlkNIEvhV
xKODvEyvtjVPzLue3L47NyM3bc2mm7kpsbBjsC4xadft+JLxEg8tM9VtrjWwiefHWg7zHp+rYuho
HCYIMj6Fqhr3/wJz1CVHdgRfhP+1fTG0Q7xVv/3UkxkYdx5SGcCsqEkyV6xmuJDJxIpZSjgxDiKB
C6LS5f8GtylJgmqjl4FpEIunAbBQjN70a2gY4Md/chOn2bID1cVaShYuhofCMUMz2/KGVGx3fmIL
adA+WLmjllO0qin+d8W6VC94whR9eFDSOC2rRgoIyqAVwYv95+mDyjIe6kDBppaRjvM2UA3uJaPR
7uPZ8+pPymRrj9VVNLJJpTiD2ynDF4K1VrBzFDZL5vsgu6L+KskoH1uOFaTxuVro4sR7w+/epMoP
vh6VP5OiDTSq+Ih6Lf9GRjckYPvWzodzEAlS4ul6JpsF1IzgsgnZcqRQJm+tiFS6iuxpUNG2f1IK
NUAJgC6OtBw5/iZZT12tc0xcxe17GCrHS19X1s47zgn4/hM+1C59OGf+3zEJQYlVHnlw+H4GPM6O
7zvNp55XO+EcYr546xeAcHJKvclTY6F/Cnal5x4yn7xXlt0PBXBGqhZJTDnrgWRAfimNfY4rOPjN
C6vOwpPi/Eo8shSc/MrLvMxVrMoDY88iA0RzXV4UdjdhUxje3jA2OfYEXbCwvVWymIDOai8ZqNcF
dwd5ASEFvCanaLnTQ4c4LweGZk3dsFqFU2no/92EXEs4Q57E/c2k7h5RfT/Mr1zhtnbLEjfHs0cK
VkUP/556NJ39nVnylY45dGqw/+KWl66kqNEGrYOxtLZsROzevDnWJ+SoBCHPA3YUQZ/jxn0Hr74R
IEc0a1JT3wO6Vp8nuwqHrQPlT1Mdl3HZ4BV/DCSssyBAO3e3Dpej+LJCrLs0AVvrCHX6C0VCVnjc
BwUOT2enMtTFfHTgOdP27SGi+3knGMu2XiKIaV8tSrvn/DwhaOgfG++tSpd9oxxBOju8hE4siQTG
toxkXOw76FVXN8xJBDSjXu3J/Hjc05n+bJmvma+7c3SS9gAKziuF4K4HP3vj2axgveZKkVVSt/td
GtfZyIYfujY5OTfXOHJKm3EibrLuWXqgljQGp+9ts9AGsndQMreWkGhmi06UtCnUQ0rnuCcicZch
5Z/SyqYYkkhcshHd5Hk84TWxkNlbrVPDmR5lwvcfvpM7u4XZMPPNqM+WcysvDl8Iua7yC6E6Rl0U
fmi4cMGnd6ii87S4HSnaY1vkLskerakLokhcRHfXfjSnGjA4DnXR6/MRtbWad4HnyR1Eo1O4muz8
yXUCQj7FLYYA8sxEpTptIx7/3loba5mWx3xTJSjNK55XPTHNYwGSusASuNWX20jPmXPqusPEqzE3
3Uus6KAfc8yiuTY0o7/Urq0eYpK8HcR2uv3ICvbtJszN8SB0+QUoK8CT+U5IqPdsWHoyu7UPcJWp
ihJCVLRZ5uOFmvFbsa39eirzlPKo2OkNh2DN4c5+HUDeWAkT1h+ukN6QXVTbb81PflN8TXtWjK4X
DgPHk8BDA1FDobsyqfI8G4fApUsz3XsJaeUCR9+EiP7SXNhSvUIWuWQYX4rKeGY+gE0i95EhaV89
5nNl7OYNrmScLnfDlqc18ctBYFzSdJ/oVCvE65+Z/j5tKeCw7OttAvhbyx9ZagVVUsDwo4084cFu
q6YyLfoE51RzCRoJnNNgMT3d31iz197aq/jkfNMwSaujXJMvp5y3AnzEyDFBFx69xY20UWyZVHQ2
PhiuKKTcVAHF8jwDbdTxM9jwGdmjvjFjAnB29XsipWB3Pe+GMCgN86mGsbkffSm46l9cJY7RvKbc
mW7EUS6Q5pN80nh9TwOwcMT0/bBacBfCsJZd0DvoJqe8HUCOPm/Cg2ANd32SEIzXJHDoGHQmbvu/
xoG56jtZUH0qeoeZD/PcY98ZemfRHeEQxfeFPaUL20SR32Ag1wqMJLd7BumVK7thZ3RDhiyjjX29
agA3QZn9Nh2R+NmPDOZzF3TeLVzPJOtpuBAiOhB/hOQ5fg4ftIF9nFqWm6LGer6Yepu0qF5ufF+G
rH4fE+T3cPNVq7Dq2Njl7P4Duu3Pi02E0anTHPERNWGWsYEZiRwk7KUoTfNqRgPgdRafDZkGkSrP
bKYNEGTEkXDP/BtnCFl1fjGFR3UgEHszc9UUZXaXFzJYkZ8GQjfigehaWKPEGeB/ieQbGnDhTZdv
pG+FmrZ2R6qNhtZkYOrCQJ9S9p+zUq6zKvgkUDAWsf2mauyhHD1FVNS5UCrVG1W6CW3j1d9bWwj3
A8nNUDyP7QemZjqBEPyhpMzCoKmH2uGM57Z8N6UJA2C8BK83P2Nsy0O4+6vcKny4klwMYcNwNhTg
VVfZMu3mT8yNKXYdl73GHsCkwPpfOZt0lQZgpq4FWGpeRrNfpd/yJGVRyUK7vglq7lzn9o+rWg18
l9jiPfOkApo12GYbDtpTgi5BNkzGDhsyvHv1jYZFUHvBHyy2j+ZkxtMUWyuKoVE6Isf8O1tYR/JJ
SH/vybZERsgYTsUXpdM+zs9oKKlvxHUxmS4IBHCFWI+J/W2hIAJYMUccsIT71Ojd//9VHAR+zfqI
c/g72kHt6esJnRZtdnmwiAZbuc4Zh6CD0pPp4Fv9Wx7/vPp/P3sTt+SLBJ1+ejlPWX6z3hIoM+Fs
Evchet/DtRTSm4mEdd/SONNO0Wtk2qIZpiQhTEat6+DCmGkihFOgEQGfy4hg2IibppyNq7DTweeI
NiyRYMdOG1MClFv21s8NCbZ3OYrSJfqhUV7s2irvni23Osjdq+pvVT1FA2kl7uWR+OB8rd2ymW1B
+/66e0ZoQqvPgyeMn8wSrLNPO9AzTFHPCeSukWnfDPXTaHghw7ZcbEkcyEKL0mxcwy8jExpexpE1
cMswQmGCODC0MeBfg+JkI07yB76N5oJODt3CfnYTfh6QXXadxBP5Iv7EogknOXC7p6eQHNPRBkJd
BwspCTCEYxzWiEJd7lhbU+0klz54sMXHx1x84BSXgsX95TIFEJFkid8eTdZ+Y06R44DGHmBKCfzx
PC/f6T+cKrpUka2XEwHAaTAFk2IqRrR4Ue2ywwsz/NL/rbfgxJIV4kNszWayTOS/cm147PipAwel
/1BMhJV6JuGFoYymdaWA1hyaw+kLyCp5LsxLrq/NcjjyHTjTLZpYYY+gqV/B69L95ZUOcAKYLMHB
Q/SVNzzKrZcGPhmz/nsqU7TN90K2FHKhgO7KTK/1f0lwSkwp8J9WHKlpdzwWa9pAqsALrfDnttWc
zei0xXJUdT41367e2Uhq7CuVIEfrdaFDU4bhXvktS0vnZNcCLkrSpD8YwaNFGwlc45TTygnWZ7eY
wIQfI3iEHbp9yXYWOjNOLfdq8/dTyjhZY4947NIkdgXEpyP9U4r7cOZOQGnIYojIvZ2K/FQO/yQJ
xKQLb2KGkenfLUUvVHzexVEw90gtlqhg1HawJ+TDtpXWWVqQ8EXDvNb0Xc6PZExT7rZAjvJ9/Wb5
+L0YstEFQjT4dSYs0mj5e6Ii2z3/4DyIq+IGFnzl+hIy0Pml0l0oWoddNq3/oSApikPkbc+0uMlZ
FVwmk7AXV4W4af3X1eVHbBAsoihfpjvbB2jLG+hq+nJu5xWEzMJRIHNu3pcIs17qnSHEx0hIr/0x
7LlQ+feEzBRydPWwb/v+xiNpmAm4onVNLLVs5AtbzNG7eBXp30Pa2d+bfEX3BNlL4qigkaZotaTm
DmBvfqgIEuaDH5u7COo8uf1JvPJEd77kDZnakpf8QU/IdNWB8F13TK4QUhhRZHmuqfpfWqeB7UPa
hpLhcjDS+/qXE9YuS4KcOsoNWM9qaSepwnOTxx1Rpsk4YDiyLbOdBQFomEfh4aNYIw5T6kfy843E
aKDPh9f/dQ/yvJQVtuyQKzxzXGaJxWD4EPROL2OhKqrZWxSZefvOGweCU8dew/qOQfRyW78i6yub
CBmRyBtnrPCAglBK0CMDfEcuO3lDSIf4jddWiOPRll5jGy6w0Isgqr+mY/MOYNX6Oo9XEPVxA6zs
qIZT25+w7L5pTo1Q0Qer7JvEHk9SJuNt2GRwMeHpvX34kiKONX7MrAa0n8jqpfsXlft9QZ4+4SzK
x2BSW62pPBJbcmv5ndDPeuDWpm5LKqMT7aoBl3U5aZQWBnZtPUKanHM37mX9AqXWH6x+lTmTFQSP
YM+ST+hi9Sfyym8tMSQLIOwG7rDyirW6HIG13C8cHe7vDZ9+ixKDQmGtnvHb5mAXkEgRwKSyWvVR
kRU715T6pppqDIciypKWUB1DDSz7aUW1JqbkrgYMoNouFBRjJNFhqoi1rpz4RkxS2dJeUR3T4e0+
F176Zbs8w4chxVf1sEGpRKw8/T/jb5xodjyai0ynq8ya6PlPxgZluEN4WkJZRhkeCV1Ispd9ecq4
I99qaapqqwJl1VAdhekSN1vzW5TSr/BbjURlfR0NoCM6IVyByaGwKCBG+ezknsRFYLCRj/G4gSzE
N3pNeaVTHFgUQOi/2U62UPEIt/8T0PqqM16Q67hwoi2ztAFkMzo27AL5fO3lsccbHVi2qo8fEiEr
C6JcM1LovO6x0ww+W0f5kVB1c+WUfF6VDjSTLYSyASwWsL424mevGzrid1brR3Je6Y8n2LaZnXHp
06cbCVT89sfRsHHJQ7wgI/+2ZzhGLaP08SL2MZDePsT2C2zHdv0XETblvJuO89smilSbIh1xXpDA
BKn4F4xJT0c3i/Ag+dPj8oZFF3AQvEtKi12ktt9bFhfoiLnt0jDHUSg7e+NcgRUD3es55D5l+6hW
98U5Wo/pXWCPeh0U+J/dzFZr/LFAHmkBaQhw74SruunIY+RGTQmNuk9yLodgJuL5nbUtM9Ix03L5
jUxinsvDiO3bc6KZqy1mujaPKCKo5EFAccSNXvs2jR7BZHFBfY6yExBSO0hVokEMG6LGQ2g2FbHG
9iPsAw5OI6GcduBlcjxtKOpnFyfM8owUq6Zi3vuOKLITFsu/7lLK6Wshn9ITCccQSrxzrdUbNGMw
FDa9HEMfuWC/E2Cq92JYWEmIo0PhwAAVR7ETco9Zq6Q8SPcL1IezVlibnM4okSHaJ58ETe2SkWOW
XzeIJ3nsafXzAqQaYMtmlfK3okyNZ7ZY8AZb4ISljnWQeHSNWxwBRhvcIcSrZWExr5YHN/H2kd3q
Cv8TQ7tP7yETQp3ebBb+wm3/VDdGdWgec+8R2bJF/tDEPE+uj0EYvajJa9Bw0AIdDEmUUxMPfzAS
3HUlnOOvwynRCJqC7uV/az9rWPbCsLC+IL3cPVvU5QAtME2cRFDxhfcKdTlaELM5GZn3e7Or6fbG
5xkQSv0qi9Mp4pCN+PZhwIJPLBAXpiWdS4I0jEkgkPJnc+kZMpHTdbrAGECWZsAfLwP3DjAdcK7M
E53iYLjNpGUAQKTFfTX8xAviqkpl74B0iZNCesV+pbDB2bAoRG7pnBhNmjBi+745CXBv/RsaRU4S
AGV2yw9SXpInOE73vse6DNTLh0VFXRwF9lwRmG62ao0SneSc9PcZEdrecVIIqKm3hhSAxPPOgaBC
ZFCGiCJhGFhlRWqDAVxDbSY4leA56GuSUBEbVLeWJx+CWs1zFmBrLYwEfCBa8R6eCMGs11w5ZrKH
Au7HYNa6gI2VJ/nJC/XMClr6I9xsv5Tntfqp3S27d/NoTR176QQihdw36Jb1HLoe1tvKdd/QqtQA
5dw6SF+YvFwrITyT0JcOUX5OeT26+plO9Fa+fDsTlGw7fWb4QGe4RAkXfYMpegvNVCE1w9aoefxY
/yw6q9BPZmPWqSsdKgCTtuoZWgVkHAO3o21yET0yCqYRyYgsaxQ7Tvtuyx7XCQ6SAAsAM0YtOFES
7htnnfqkTUNA0bkkv4ml5qYtnhB2Pffwxr3irVakLc43MDkwZM7SCU1M6FN/VOFywc9JgT7SCGb0
JArdsdw/4y5zEdfTrW4Dh71VfaYjiXCotvFEUw8TDuO9CTAaySf1b1gpIcDvuPrtxn5DeHPl5UmY
cnNm5qRAs1VkME8qe+3LRN4OEdaU78R04yZbzYrLXjc0gAHxKeQbwXPCLIn2SGc/nHrQRqE3KCv9
0j8WG+F2BzLi6jhc4/fvyTWM6E0kKqZMv+dzgmGOWW8H+UBwXokihauFhmH7000krmJEa8yCrqsv
7eYVCCXZlmesIVrPYAzjYVSLN4YT+DL3o/xvbGZJz1bCOWgqrQ54AuQzxcN26wg30We+7PEF5OAm
56kALXuq8qiM3gx3Zx+npbwilGNOTUhdyDpFJ/c9MaQI3Js3ZERWzeRnRJOH/ZKmvww1ooBUUa7O
RiN5M34LjQvxewuzmrPaEOf4FvkCN6Ap0kh8m8o1wze6sz/XSK9if07MU7MBOCLGi+3DmTbdJZWy
oJjjEi6+t7qjByzwnb7Sy8oGovofeCADLtfiu25886yDMkHG7x/bT8u7hsXWiT5UPtQEahteh8KO
KgLXxt1ciIa5D2t8sPoqj0xheiHjBYBLXQ/fkBFWqSJPbOKmyMzz/TV2yl4MffM4WulYOMmDPCpb
Okf0wZhYh/7sO6Eclut5ApBQSYD1qNL/jt8/v0Q3TdquVOGhOei9WL+js6MlBQGH1gaytTNEr+c2
TrtTGgG8m3dZEPJP9m3WVCV4A+RJKUIbxsN9CpzU7cKMdQlgfV+OVifdXq/eqkMHXypUvQSX4ZEG
skq///WQm1jlq/LfFfhvAzi6dIwS2ZWb8B8Qo9/ElXWUEXIbYZwnrD7fMNBxw5IMozTMeRvg3hq5
0+2++45Cg9Fkkf5SxtHptV3M6qe0wXpWggq2hRxDF2I6Xf/G+h/1Ul9c19Lud7xSMCxR1nnAsjl1
uwwn1YdZaU3WIUIXZGf53qMG1mQHH+F2tXIwvYY/lQzQrXYPuxZBuiy8KxHS0BuXDEJeXbrzjBKS
vyRH4y3khwfV4fKCl+qVhm2ltr+c56RIawj4GCgY3jzuNvT7mAZVx1ufrLo1qwDU+Cp/QoPHUBnu
5rvf8byPl1FceCj6AyeoXMAo+G1hCBg7myScnPURCQvak0IEQd59fspgVjTDpNDOBC28a9GezECq
9E8ahGZAsKuI2a5WjXn975vppUbWojLXZrF8gM6EpPIWwfTx6cMkE+eVF4aYTKiDD4BVeTdcKR35
CHIQZCo+OkxtQWJzRi/E008pIMriPGvFE0Jn8YE7X1nOqOKzB4WS9ZY/8pWDImbB2KRel5raSXo8
9XIO8NEikBP8UeVotHxhVxy93Uof9mWcIN73ABfOiZwDkU2RL8pKWq1Y2W2dP39RQy14B2g799+B
jb6FHpe/6B6TARmRRi4QXEC88S810zqDJQtRe7nsjxdxbDDwl/va/pQ9PaNWreCRak7NQa3rWjOz
JhCpJwc7gf5s/kHwgIKiSzokFsZjoD1jMsm1BYU0KEA+Pq+pQDDBc6M9qg7fw4JcID0R0WrpnV88
0/YlcFZKADZv7oGOnVDbW7XwPWdYIZw62dCQ4BsbKisx+V+z3ElGBieHqkn3nfDC+HLsSxJk8NDO
ppwFLUUuDu+dcW/pU8xsToAo9JUrThKrbv0HxjsyBr6iWqQmlxcSvADHgpytw3spaavRXjPLK4No
Gm3rK0Wskphi8JZcMEU8JrVjt+Fk3JjbmmrbzavYL2eBA/qRvV4YtzVW0VWHymbOAmvVvz8bYmc8
rkrQ+1oFXz9rguAB9K/IYA7KdfAYsW+zJoSACBBLv66xB78JPeAxiKmQGCrj+9fZorQyez3UU9O6
qM5WFIHINEM6usQrHjZuCgHERIrdER/1lXnexKuYGGC7TNgtD7Ni55VBmMbrOuC+iOiq1XXlyWzk
ak1xTIYw7jkiPRG5WaYWy4RCfwtF/0nLSkr4QDSvU1NLPm+evSgq4G5FDVItTDBl8R4SrTpDeZNs
vQDKVTe0ZvZoCaJvDOWGI5zAk9qYJQ/sqNDYAJGop6255vlkEyNTHnvHpmBynUgmTHiOdpI+U8FD
Ilup/bCl9dGEkBT/HytY4UfpQEx31pR9qi9zH+fi2Roe8Z1LRbudOVIWVnIWdol/T1XxXgwP17qC
x2qVsUK8fC8SZxnAvwo9X8AOhl/YSGGjSQMxIzeflueftCHySNJsOfeqmmVThdY/bN6eTfvF7oY0
MPYETAqF6W7IH4gJGMjLhJDiYX3PvsbPozfkHlkpGFLZqTINyWduce2DeLsfIunmKyUlujvt+TwH
7E3YbqhK6f4OtvhhTFggtmMVHlM+NS0lmzoKpjb3f9EMUl+9F823lWp6gKFirCnxsw6EM1ijWZxB
2oPYTSl76x7etqN0tR0SULwdRGy3IZedBG/dYiVqx4GHakjAhyKwNCBgDxwJzF/OuaGEMRmLY26H
EFTsCJpyZWt8SF7rk0Wq8qBUjUnqI0SiBoC4Upd+Tytn4K6PHoChiXsSYCR42MyEYYdRGcJPKne7
5Ye4au2htZNx7vRwgrc9ja+nChIjyIoXbjFlkyELeIHZguoIaCswq/QOcQ==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD is
  port (
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_1 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_2 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    mmcm_reset : out STD_LOGIC;
    data_in : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gtxe2_i_4 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_5 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_7 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_8 : in STD_LOGIC;
    gtxe2_i_9 : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    data_out : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD is
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_init
     port map (
      D(1 downto 0) => D(1 downto 0),
      Q(15 downto 0) => Q(15 downto 0),
      RXBUFSTATUS(0) => RXBUFSTATUS(0),
      RXPD(0) => RXPD(0),
      TXBUFSTATUS(0) => TXBUFSTATUS(0),
      TXPD(0) => TXPD(0),
      data_in => data_in,
      data_out => data_out,
      data_sync_reg1 => data_sync_reg1,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i(15 downto 0) => gtxe2_i(15 downto 0),
      gtxe2_i_0(1 downto 0) => gtxe2_i_0(1 downto 0),
      gtxe2_i_1(1 downto 0) => gtxe2_i_1(1 downto 0),
      gtxe2_i_2(1 downto 0) => gtxe2_i_2(1 downto 0),
      gtxe2_i_3(1 downto 0) => gtxe2_i_3(1 downto 0),
      gtxe2_i_4 => gtxe2_i_4,
      gtxe2_i_5(1 downto 0) => gtxe2_i_5(1 downto 0),
      gtxe2_i_6(1 downto 0) => gtxe2_i_6(1 downto 0),
      gtxe2_i_7(1 downto 0) => gtxe2_i_7(1 downto 0),
      gtxe2_i_8 => gtxe2_i_8,
      gtxe2_i_9 => gtxe2_i_9,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0),
      reset => reset,
      reset_out => reset_out,
      rx_fsm_reset_done_int_reg => rx_fsm_reset_done_int_reg,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_transceiver is
  port (
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    rxchariscomma : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxcharisk : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxdisperr : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxnotintable : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxbufstatus : out STD_LOGIC_VECTOR ( 0 to 0 );
    txbuferr : out STD_LOGIC;
    mmcm_reset : out STD_LOGIC;
    data_in : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \rxdata_reg[7]_0\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    gtxe2_i : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    powerdown : in STD_LOGIC;
    reset_sync5 : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    txchardispval_reg_reg_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    txcharisk_reg_reg_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    status_vector : in STD_LOGIC_VECTOR ( 0 to 0 );
    enablealign : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    \txdata_reg_reg[7]_0\ : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_transceiver;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_transceiver is
  signal data_valid_reg2 : STD_LOGIC;
  signal encommaalign_int : STD_LOGIC;
  signal gtwizard_inst_n_6 : STD_LOGIC;
  signal gtwizard_inst_n_7 : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal reset : STD_LOGIC;
  signal rxchariscomma_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxchariscomma_i_1_n_0 : STD_LOGIC;
  signal rxchariscomma_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxchariscomma_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxcharisk_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxcharisk_i_1_n_0 : STD_LOGIC;
  signal rxcharisk_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxcharisk_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxclkcorcnt_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxclkcorcnt_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxclkcorcnt_reg : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxdata[0]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[1]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[2]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[3]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[4]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[5]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[6]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[7]_i_1_n_0\ : STD_LOGIC;
  signal rxdata_double : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rxdata_int : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rxdata_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rxdisperr_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxdisperr_i_1_n_0 : STD_LOGIC;
  signal rxdisperr_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxdisperr_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxnotintable_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxnotintable_i_1_n_0 : STD_LOGIC;
  signal rxnotintable_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxnotintable_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxpowerdown : STD_LOGIC;
  signal rxpowerdown_double : STD_LOGIC;
  signal \rxpowerdown_reg__0\ : STD_LOGIC;
  signal rxreset_int : STD_LOGIC;
  signal toggle : STD_LOGIC;
  signal toggle_i_1_n_0 : STD_LOGIC;
  signal toggle_rx : STD_LOGIC;
  signal toggle_rx_i_1_n_0 : STD_LOGIC;
  signal txbufstatus_reg : STD_LOGIC_VECTOR ( 1 to 1 );
  signal txchardispmode_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispmode_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispmode_reg : STD_LOGIC;
  signal txchardispval_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispval_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispval_reg : STD_LOGIC;
  signal txcharisk_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txcharisk_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txcharisk_reg : STD_LOGIC;
  signal txdata_double : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal txdata_int : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal txdata_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal txpowerdown : STD_LOGIC;
  signal txpowerdown_double : STD_LOGIC;
  signal \txpowerdown_reg__0\ : STD_LOGIC;
  signal txreset_int : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of rxchariscomma_i_1 : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of rxcharisk_i_1 : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \rxdata[0]_i_1\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \rxdata[1]_i_1\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \rxdata[2]_i_1\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \rxdata[3]_i_1\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \rxdata[4]_i_1\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \rxdata[5]_i_1\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \rxdata[6]_i_1\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \rxdata[7]_i_1\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of rxdisperr_i_1 : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of rxnotintable_i_1 : label is "soft_lutpair79";
begin
gtwizard_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD
     port map (
      D(1 downto 0) => rxclkcorcnt_int(1 downto 0),
      Q(15 downto 0) => txdata_int(15 downto 0),
      RXBUFSTATUS(0) => gtwizard_inst_n_7,
      RXPD(0) => rxpowerdown,
      TXBUFSTATUS(0) => gtwizard_inst_n_6,
      TXPD(0) => txpowerdown,
      data_in => data_in,
      data_out => data_valid_reg2,
      data_sync_reg1 => data_sync_reg1,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i(15 downto 0) => rxdata_int(15 downto 0),
      gtxe2_i_0(1 downto 0) => rxchariscomma_int(1 downto 0),
      gtxe2_i_1(1 downto 0) => rxcharisk_int(1 downto 0),
      gtxe2_i_2(1 downto 0) => rxdisperr_int(1 downto 0),
      gtxe2_i_3(1 downto 0) => rxnotintable_int(1 downto 0),
      gtxe2_i_4 => gtxe2_i,
      gtxe2_i_5(1 downto 0) => txchardispmode_int(1 downto 0),
      gtxe2_i_6(1 downto 0) => txchardispval_int(1 downto 0),
      gtxe2_i_7(1 downto 0) => txcharisk_int(1 downto 0),
      gtxe2_i_8 => rxreset_int,
      gtxe2_i_9 => txreset_int,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0),
      reset => reset,
      reset_out => encommaalign_int,
      rx_fsm_reset_done_int_reg => rx_fsm_reset_done_int_reg,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
reclock_encommaalign: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync
     port map (
      CLK => CLK,
      enablealign => enablealign,
      reset_out => encommaalign_int
    );
reclock_rxreset: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_1
     port map (
      SR(0) => SR(0),
      independent_clock_bufg => independent_clock_bufg,
      reset_out => rxreset_int
    );
reclock_txreset: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_2
     port map (
      independent_clock_bufg => independent_clock_bufg,
      reset_out => txreset_int,
      reset_sync5_0(0) => reset_sync5(0)
    );
reset_wtd_timer: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_wtd_timer
     port map (
      data_out => data_valid_reg2,
      independent_clock_bufg => independent_clock_bufg,
      reset => reset
    );
rxbuferr_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => p_0_in,
      Q => rxbufstatus(0),
      R => '0'
    );
\rxbufstatus_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => gtwizard_inst_n_7,
      Q => p_0_in,
      R => '0'
    );
\rxchariscomma_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxchariscomma_reg__0\(0),
      Q => rxchariscomma_double(0),
      R => SR(0)
    );
\rxchariscomma_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxchariscomma_reg__0\(1),
      Q => rxchariscomma_double(1),
      R => SR(0)
    );
rxchariscomma_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxchariscomma_double(1),
      I1 => toggle_rx,
      I2 => rxchariscomma_double(0),
      O => rxchariscomma_i_1_n_0
    );
rxchariscomma_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxchariscomma_i_1_n_0,
      Q => rxchariscomma(0),
      R => SR(0)
    );
\rxchariscomma_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxchariscomma_int(0),
      Q => \rxchariscomma_reg__0\(0),
      R => '0'
    );
\rxchariscomma_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxchariscomma_int(1),
      Q => \rxchariscomma_reg__0\(1),
      R => '0'
    );
\rxcharisk_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxcharisk_reg__0\(0),
      Q => rxcharisk_double(0),
      R => SR(0)
    );
\rxcharisk_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxcharisk_reg__0\(1),
      Q => rxcharisk_double(1),
      R => SR(0)
    );
rxcharisk_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxcharisk_double(1),
      I1 => toggle_rx,
      I2 => rxcharisk_double(0),
      O => rxcharisk_i_1_n_0
    );
rxcharisk_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxcharisk_i_1_n_0,
      Q => rxcharisk(0),
      R => SR(0)
    );
\rxcharisk_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxcharisk_int(0),
      Q => \rxcharisk_reg__0\(0),
      R => '0'
    );
\rxcharisk_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxcharisk_int(1),
      Q => \rxcharisk_reg__0\(1),
      R => '0'
    );
\rxclkcorcnt_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxclkcorcnt_reg(0),
      Q => rxclkcorcnt_double(0),
      R => SR(0)
    );
\rxclkcorcnt_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxclkcorcnt_reg(1),
      Q => rxclkcorcnt_double(1),
      R => SR(0)
    );
\rxclkcorcnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxclkcorcnt_double(0),
      Q => Q(0),
      R => SR(0)
    );
\rxclkcorcnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxclkcorcnt_double(1),
      Q => Q(1),
      R => SR(0)
    );
\rxclkcorcnt_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxclkcorcnt_int(0),
      Q => rxclkcorcnt_reg(0),
      R => '0'
    );
\rxclkcorcnt_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxclkcorcnt_int(1),
      Q => rxclkcorcnt_reg(1),
      R => '0'
    );
\rxdata[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(8),
      I1 => toggle_rx,
      I2 => rxdata_double(0),
      O => \rxdata[0]_i_1_n_0\
    );
\rxdata[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(9),
      I1 => toggle_rx,
      I2 => rxdata_double(1),
      O => \rxdata[1]_i_1_n_0\
    );
\rxdata[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(10),
      I1 => toggle_rx,
      I2 => rxdata_double(2),
      O => \rxdata[2]_i_1_n_0\
    );
\rxdata[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(11),
      I1 => toggle_rx,
      I2 => rxdata_double(3),
      O => \rxdata[3]_i_1_n_0\
    );
\rxdata[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(12),
      I1 => toggle_rx,
      I2 => rxdata_double(4),
      O => \rxdata[4]_i_1_n_0\
    );
\rxdata[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(13),
      I1 => toggle_rx,
      I2 => rxdata_double(5),
      O => \rxdata[5]_i_1_n_0\
    );
\rxdata[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(14),
      I1 => toggle_rx,
      I2 => rxdata_double(6),
      O => \rxdata[6]_i_1_n_0\
    );
\rxdata[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(15),
      I1 => toggle_rx,
      I2 => rxdata_double(7),
      O => \rxdata[7]_i_1_n_0\
    );
\rxdata_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(0),
      Q => rxdata_double(0),
      R => SR(0)
    );
\rxdata_double_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(10),
      Q => rxdata_double(10),
      R => SR(0)
    );
\rxdata_double_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(11),
      Q => rxdata_double(11),
      R => SR(0)
    );
\rxdata_double_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(12),
      Q => rxdata_double(12),
      R => SR(0)
    );
\rxdata_double_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(13),
      Q => rxdata_double(13),
      R => SR(0)
    );
\rxdata_double_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(14),
      Q => rxdata_double(14),
      R => SR(0)
    );
\rxdata_double_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(15),
      Q => rxdata_double(15),
      R => SR(0)
    );
\rxdata_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(1),
      Q => rxdata_double(1),
      R => SR(0)
    );
\rxdata_double_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(2),
      Q => rxdata_double(2),
      R => SR(0)
    );
\rxdata_double_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(3),
      Q => rxdata_double(3),
      R => SR(0)
    );
\rxdata_double_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(4),
      Q => rxdata_double(4),
      R => SR(0)
    );
\rxdata_double_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(5),
      Q => rxdata_double(5),
      R => SR(0)
    );
\rxdata_double_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(6),
      Q => rxdata_double(6),
      R => SR(0)
    );
\rxdata_double_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(7),
      Q => rxdata_double(7),
      R => SR(0)
    );
\rxdata_double_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(8),
      Q => rxdata_double(8),
      R => SR(0)
    );
\rxdata_double_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(9),
      Q => rxdata_double(9),
      R => SR(0)
    );
\rxdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[0]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(0),
      R => SR(0)
    );
\rxdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[1]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(1),
      R => SR(0)
    );
\rxdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[2]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(2),
      R => SR(0)
    );
\rxdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[3]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(3),
      R => SR(0)
    );
\rxdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[4]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(4),
      R => SR(0)
    );
\rxdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[5]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(5),
      R => SR(0)
    );
\rxdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[6]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(6),
      R => SR(0)
    );
\rxdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[7]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(7),
      R => SR(0)
    );
\rxdata_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(0),
      Q => rxdata_reg(0),
      R => '0'
    );
\rxdata_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(10),
      Q => rxdata_reg(10),
      R => '0'
    );
\rxdata_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(11),
      Q => rxdata_reg(11),
      R => '0'
    );
\rxdata_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(12),
      Q => rxdata_reg(12),
      R => '0'
    );
\rxdata_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(13),
      Q => rxdata_reg(13),
      R => '0'
    );
\rxdata_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(14),
      Q => rxdata_reg(14),
      R => '0'
    );
\rxdata_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(15),
      Q => rxdata_reg(15),
      R => '0'
    );
\rxdata_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(1),
      Q => rxdata_reg(1),
      R => '0'
    );
\rxdata_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(2),
      Q => rxdata_reg(2),
      R => '0'
    );
\rxdata_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(3),
      Q => rxdata_reg(3),
      R => '0'
    );
\rxdata_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(4),
      Q => rxdata_reg(4),
      R => '0'
    );
\rxdata_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(5),
      Q => rxdata_reg(5),
      R => '0'
    );
\rxdata_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(6),
      Q => rxdata_reg(6),
      R => '0'
    );
\rxdata_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(7),
      Q => rxdata_reg(7),
      R => '0'
    );
\rxdata_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(8),
      Q => rxdata_reg(8),
      R => '0'
    );
\rxdata_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(9),
      Q => rxdata_reg(9),
      R => '0'
    );
\rxdisperr_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxdisperr_reg__0\(0),
      Q => rxdisperr_double(0),
      R => SR(0)
    );
\rxdisperr_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxdisperr_reg__0\(1),
      Q => rxdisperr_double(1),
      R => SR(0)
    );
rxdisperr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdisperr_double(1),
      I1 => toggle_rx,
      I2 => rxdisperr_double(0),
      O => rxdisperr_i_1_n_0
    );
rxdisperr_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxdisperr_i_1_n_0,
      Q => rxdisperr(0),
      R => SR(0)
    );
\rxdisperr_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdisperr_int(0),
      Q => \rxdisperr_reg__0\(0),
      R => '0'
    );
\rxdisperr_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdisperr_int(1),
      Q => \rxdisperr_reg__0\(1),
      R => '0'
    );
\rxnotintable_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxnotintable_reg__0\(0),
      Q => rxnotintable_double(0),
      R => SR(0)
    );
\rxnotintable_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxnotintable_reg__0\(1),
      Q => rxnotintable_double(1),
      R => SR(0)
    );
rxnotintable_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxnotintable_double(1),
      I1 => toggle_rx,
      I2 => rxnotintable_double(0),
      O => rxnotintable_i_1_n_0
    );
rxnotintable_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxnotintable_i_1_n_0,
      Q => rxnotintable(0),
      R => SR(0)
    );
\rxnotintable_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxnotintable_int(0),
      Q => \rxnotintable_reg__0\(0),
      R => '0'
    );
\rxnotintable_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxnotintable_int(1),
      Q => \rxnotintable_reg__0\(1),
      R => '0'
    );
rxpowerdown_double_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxpowerdown_reg__0\,
      Q => rxpowerdown_double,
      R => SR(0)
    );
rxpowerdown_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gtxe2_i,
      CE => '1',
      D => rxpowerdown_double,
      Q => rxpowerdown,
      R => '0'
    );
rxpowerdown_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => powerdown,
      Q => \rxpowerdown_reg__0\,
      R => SR(0)
    );
sync_block_data_valid: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_3
     port map (
      data_out => data_valid_reg2,
      independent_clock_bufg => independent_clock_bufg,
      status_vector(0) => status_vector(0)
    );
toggle_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => toggle,
      O => toggle_i_1_n_0
    );
toggle_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => toggle_i_1_n_0,
      Q => toggle,
      R => reset_sync5(0)
    );
toggle_rx_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => toggle_rx,
      O => toggle_rx_i_1_n_0
    );
toggle_rx_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => toggle_rx_i_1_n_0,
      Q => toggle_rx,
      R => SR(0)
    );
txbuferr_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => txbufstatus_reg(1),
      Q => txbuferr,
      R => '0'
    );
\txbufstatus_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => gtwizard_inst_n_6,
      Q => txbufstatus_reg(1),
      R => '0'
    );
\txchardispmode_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txchardispmode_reg,
      Q => txchardispmode_double(0),
      R => reset_sync5(0)
    );
\txchardispmode_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => D(0),
      Q => txchardispmode_double(1),
      R => reset_sync5(0)
    );
\txchardispmode_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispmode_double(0),
      Q => txchardispmode_int(0),
      R => '0'
    );
\txchardispmode_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispmode_double(1),
      Q => txchardispmode_int(1),
      R => '0'
    );
txchardispmode_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => txchardispmode_reg,
      R => reset_sync5(0)
    );
\txchardispval_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txchardispval_reg,
      Q => txchardispval_double(0),
      R => reset_sync5(0)
    );
\txchardispval_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txchardispval_reg_reg_0(0),
      Q => txchardispval_double(1),
      R => reset_sync5(0)
    );
\txchardispval_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispval_double(0),
      Q => txchardispval_int(0),
      R => '0'
    );
\txchardispval_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispval_double(1),
      Q => txchardispval_int(1),
      R => '0'
    );
txchardispval_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => txchardispval_reg_reg_0(0),
      Q => txchardispval_reg,
      R => reset_sync5(0)
    );
\txcharisk_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txcharisk_reg,
      Q => txcharisk_double(0),
      R => reset_sync5(0)
    );
\txcharisk_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txcharisk_reg_reg_0(0),
      Q => txcharisk_double(1),
      R => reset_sync5(0)
    );
\txcharisk_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txcharisk_double(0),
      Q => txcharisk_int(0),
      R => '0'
    );
\txcharisk_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txcharisk_double(1),
      Q => txcharisk_int(1),
      R => '0'
    );
txcharisk_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => txcharisk_reg_reg_0(0),
      Q => txcharisk_reg,
      R => reset_sync5(0)
    );
\txdata_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(0),
      Q => txdata_double(0),
      R => reset_sync5(0)
    );
\txdata_double_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(2),
      Q => txdata_double(10),
      R => reset_sync5(0)
    );
\txdata_double_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(3),
      Q => txdata_double(11),
      R => reset_sync5(0)
    );
\txdata_double_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(4),
      Q => txdata_double(12),
      R => reset_sync5(0)
    );
\txdata_double_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(5),
      Q => txdata_double(13),
      R => reset_sync5(0)
    );
\txdata_double_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(6),
      Q => txdata_double(14),
      R => reset_sync5(0)
    );
\txdata_double_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(7),
      Q => txdata_double(15),
      R => reset_sync5(0)
    );
\txdata_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(1),
      Q => txdata_double(1),
      R => reset_sync5(0)
    );
\txdata_double_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(2),
      Q => txdata_double(2),
      R => reset_sync5(0)
    );
\txdata_double_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(3),
      Q => txdata_double(3),
      R => reset_sync5(0)
    );
\txdata_double_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(4),
      Q => txdata_double(4),
      R => reset_sync5(0)
    );
\txdata_double_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(5),
      Q => txdata_double(5),
      R => reset_sync5(0)
    );
\txdata_double_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(6),
      Q => txdata_double(6),
      R => reset_sync5(0)
    );
\txdata_double_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(7),
      Q => txdata_double(7),
      R => reset_sync5(0)
    );
\txdata_double_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(0),
      Q => txdata_double(8),
      R => reset_sync5(0)
    );
\txdata_double_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(1),
      Q => txdata_double(9),
      R => reset_sync5(0)
    );
\txdata_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(0),
      Q => txdata_int(0),
      R => '0'
    );
\txdata_int_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(10),
      Q => txdata_int(10),
      R => '0'
    );
\txdata_int_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(11),
      Q => txdata_int(11),
      R => '0'
    );
\txdata_int_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(12),
      Q => txdata_int(12),
      R => '0'
    );
\txdata_int_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(13),
      Q => txdata_int(13),
      R => '0'
    );
\txdata_int_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(14),
      Q => txdata_int(14),
      R => '0'
    );
\txdata_int_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(15),
      Q => txdata_int(15),
      R => '0'
    );
\txdata_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(1),
      Q => txdata_int(1),
      R => '0'
    );
\txdata_int_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(2),
      Q => txdata_int(2),
      R => '0'
    );
\txdata_int_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(3),
      Q => txdata_int(3),
      R => '0'
    );
\txdata_int_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(4),
      Q => txdata_int(4),
      R => '0'
    );
\txdata_int_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(5),
      Q => txdata_int(5),
      R => '0'
    );
\txdata_int_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(6),
      Q => txdata_int(6),
      R => '0'
    );
\txdata_int_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(7),
      Q => txdata_int(7),
      R => '0'
    );
\txdata_int_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(8),
      Q => txdata_int(8),
      R => '0'
    );
\txdata_int_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(9),
      Q => txdata_int(9),
      R => '0'
    );
\txdata_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(0),
      Q => txdata_reg(0),
      R => reset_sync5(0)
    );
\txdata_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(1),
      Q => txdata_reg(1),
      R => reset_sync5(0)
    );
\txdata_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(2),
      Q => txdata_reg(2),
      R => reset_sync5(0)
    );
\txdata_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(3),
      Q => txdata_reg(3),
      R => reset_sync5(0)
    );
\txdata_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(4),
      Q => txdata_reg(4),
      R => reset_sync5(0)
    );
\txdata_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(5),
      Q => txdata_reg(5),
      R => reset_sync5(0)
    );
\txdata_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(6),
      Q => txdata_reg(6),
      R => reset_sync5(0)
    );
\txdata_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(7),
      Q => txdata_reg(7),
      R => reset_sync5(0)
    );
txpowerdown_double_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \txpowerdown_reg__0\,
      Q => txpowerdown_double,
      R => reset_sync5(0)
    );
txpowerdown_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gtxe2_i,
      CE => '1',
      D => txpowerdown_double,
      Q => txpowerdown,
      R => '0'
    );
txpowerdown_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => powerdown,
      Q => \txpowerdown_reg__0\,
      R => reset_sync5(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_block is
  port (
    gmii_rxd : out STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_rx_dv : out STD_LOGIC;
    gmii_rx_er : out STD_LOGIC;
    gmii_isolate : out STD_LOGIC;
    status_vector : out STD_LOGIC_VECTOR ( 6 downto 0 );
    resetdone : out STD_LOGIC;
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    mmcm_reset : out STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    signal_detect : in STD_LOGIC;
    CLK : in STD_LOGIC;
    data_in : in STD_LOGIC;
    gmii_txd : in STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_tx_en : in STD_LOGIC;
    gmii_tx_er : in STD_LOGIC;
    configuration_vector : in STD_LOGIC_VECTOR ( 2 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    gtxe2_i : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_block;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_block is
  signal enablealign : STD_LOGIC;
  signal mgt_rx_reset : STD_LOGIC;
  signal mgt_tx_reset : STD_LOGIC;
  signal powerdown : STD_LOGIC;
  signal \^resetdone\ : STD_LOGIC;
  signal rx_reset_done_i : STD_LOGIC;
  signal rxbuferr : STD_LOGIC;
  signal rxchariscomma : STD_LOGIC;
  signal rxcharisk : STD_LOGIC;
  signal rxclkcorcnt : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxdata : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal rxdisperr : STD_LOGIC;
  signal rxnotintable : STD_LOGIC;
  signal \^status_vector\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal transceiver_inst_n_11 : STD_LOGIC;
  signal transceiver_inst_n_12 : STD_LOGIC;
  signal txbuferr : STD_LOGIC;
  signal txchardispmode : STD_LOGIC;
  signal txchardispval : STD_LOGIC;
  signal txcharisk : STD_LOGIC;
  signal txdata : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 7 );
  signal NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  attribute B_SHIFTER_ADDR : string;
  attribute B_SHIFTER_ADDR of gig_ethernet_pcs_pma_0_core : label is "10'b0101001110";
  attribute C_1588 : integer;
  attribute C_1588 of gig_ethernet_pcs_pma_0_core : label is 0;
  attribute C_2_5G : string;
  attribute C_2_5G of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_COMPONENT_NAME : string;
  attribute C_COMPONENT_NAME of gig_ethernet_pcs_pma_0_core : label is "gig_ethernet_pcs_pma_0";
  attribute C_DYNAMIC_SWITCHING : string;
  attribute C_DYNAMIC_SWITCHING of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_ELABORATION_TRANSIENT_DIR : string;
  attribute C_ELABORATION_TRANSIENT_DIR of gig_ethernet_pcs_pma_0_core : label is "BlankString";
  attribute C_FAMILY : string;
  attribute C_FAMILY of gig_ethernet_pcs_pma_0_core : label is "kintex7";
  attribute C_HAS_AN : string;
  attribute C_HAS_AN of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_HAS_AXIL : string;
  attribute C_HAS_AXIL of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_HAS_MDIO : string;
  attribute C_HAS_MDIO of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_HAS_TEMAC : string;
  attribute C_HAS_TEMAC of gig_ethernet_pcs_pma_0_core : label is "TRUE";
  attribute C_IS_SGMII : string;
  attribute C_IS_SGMII of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_RX_GMII_CLK : string;
  attribute C_RX_GMII_CLK of gig_ethernet_pcs_pma_0_core : label is "TXOUTCLK";
  attribute C_SGMII_FABRIC_BUFFER : string;
  attribute C_SGMII_FABRIC_BUFFER of gig_ethernet_pcs_pma_0_core : label is "TRUE";
  attribute C_SGMII_PHY_MODE : string;
  attribute C_SGMII_PHY_MODE of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_USE_LVDS : string;
  attribute C_USE_LVDS of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_USE_TBI : string;
  attribute C_USE_TBI of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_USE_TRANSCEIVER : string;
  attribute C_USE_TRANSCEIVER of gig_ethernet_pcs_pma_0_core : label is "TRUE";
  attribute GT_RX_BYTE_WIDTH : integer;
  attribute GT_RX_BYTE_WIDTH of gig_ethernet_pcs_pma_0_core : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of gig_ethernet_pcs_pma_0_core : label is "soft";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of gig_ethernet_pcs_pma_0_core : label is "yes";
  attribute is_du_within_envelope : string;
  attribute is_du_within_envelope of gig_ethernet_pcs_pma_0_core : label is "true";
begin
  resetdone <= \^resetdone\;
  status_vector(6 downto 0) <= \^status_vector\(6 downto 0);
gig_ethernet_pcs_pma_0_core: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_v16_2_15
     port map (
      an_adv_config_val => '0',
      an_adv_config_vector(15 downto 0) => B"0000000000000000",
      an_enable => NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED,
      an_interrupt => NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED,
      an_restart_config => '0',
      basex_or_sgmii => '0',
      configuration_valid => '0',
      configuration_vector(4) => '0',
      configuration_vector(3 downto 1) => configuration_vector(2 downto 0),
      configuration_vector(0) => '0',
      correction_timer(63 downto 0) => B"0000000000000000000000000000000000000000000000000000000000000000",
      dcm_locked => data_in,
      drp_daddr(9 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED(9 downto 0),
      drp_dclk => '0',
      drp_den => NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED,
      drp_di(15 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED(15 downto 0),
      drp_do(15 downto 0) => B"0000000000000000",
      drp_drdy => '0',
      drp_dwe => NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED,
      drp_gnt => '0',
      drp_req => NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED,
      en_cdet => NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED,
      enablealign => enablealign,
      ewrap => NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED,
      gmii_isolate => gmii_isolate,
      gmii_rx_dv => gmii_rx_dv,
      gmii_rx_er => gmii_rx_er,
      gmii_rxd(7 downto 0) => gmii_rxd(7 downto 0),
      gmii_tx_en => gmii_tx_en,
      gmii_tx_er => gmii_tx_er,
      gmii_txd(7 downto 0) => gmii_txd(7 downto 0),
      gtx_clk => '0',
      link_timer_basex(9 downto 0) => B"0000000000",
      link_timer_sgmii(9 downto 0) => B"0000000000",
      link_timer_value(9 downto 0) => B"0000000000",
      loc_ref => NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED,
      mdc => '0',
      mdio_in => '0',
      mdio_out => NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED,
      mdio_tri => NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED,
      mgt_rx_reset => mgt_rx_reset,
      mgt_tx_reset => mgt_tx_reset,
      phyad(4 downto 0) => B"00000",
      pma_rx_clk0 => '0',
      pma_rx_clk1 => '0',
      powerdown => powerdown,
      reset => \out\(0),
      reset_done => \^resetdone\,
      rx_code_group0(9 downto 0) => B"0000000000",
      rx_code_group1(9 downto 0) => B"0000000000",
      rx_gt_nominal_latency(15 downto 0) => B"0000000011111000",
      rxbufstatus(1) => rxbuferr,
      rxbufstatus(0) => '0',
      rxchariscomma(0) => rxchariscomma,
      rxcharisk(0) => rxcharisk,
      rxclkcorcnt(2) => '0',
      rxclkcorcnt(1 downto 0) => rxclkcorcnt(1 downto 0),
      rxdata(7 downto 0) => rxdata(7 downto 0),
      rxdisperr(0) => rxdisperr,
      rxnotintable(0) => rxnotintable,
      rxphy_correction_timer(63 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED(63 downto 0),
      rxphy_ns_field(31 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED(31 downto 0),
      rxphy_s_field(47 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED(47 downto 0),
      rxrecclk => '0',
      rxrundisp(0) => '0',
      s_axi_aclk => '0',
      s_axi_araddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_arready => NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED,
      s_axi_arvalid => '0',
      s_axi_awaddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_awready => NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED,
      s_axi_awvalid => '0',
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_bvalid => NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED,
      s_axi_rdata(31 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED(31 downto 0),
      s_axi_resetn => '0',
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_rvalid => NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED,
      s_axi_wdata(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_wready => NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED,
      s_axi_wvalid => '0',
      signal_detect => signal_detect,
      speed_is_100 => '0',
      speed_is_10_100 => '0',
      speed_selection(1 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED(1 downto 0),
      status_vector(15 downto 7) => NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED(15 downto 7),
      status_vector(6 downto 0) => \^status_vector\(6 downto 0),
      systemtimer_ns_field(31 downto 0) => B"00000000000000000000000000000000",
      systemtimer_s_field(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      tx_code_group(9 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED(9 downto 0),
      txbuferr => txbuferr,
      txchardispmode => txchardispmode,
      txchardispval => txchardispval,
      txcharisk => txcharisk,
      txdata(7 downto 0) => txdata(7 downto 0),
      userclk => '0',
      userclk2 => CLK
    );
sync_block_rx_reset_done: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block
     port map (
      CLK => CLK,
      data_in => transceiver_inst_n_12,
      data_out => rx_reset_done_i
    );
sync_block_tx_reset_done: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_0
     port map (
      CLK => CLK,
      data_in => transceiver_inst_n_11,
      resetdone => \^resetdone\,
      resetdone_0 => rx_reset_done_i
    );
transceiver_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_transceiver
     port map (
      CLK => CLK,
      D(0) => txchardispmode,
      Q(1 downto 0) => rxclkcorcnt(1 downto 0),
      SR(0) => mgt_rx_reset,
      data_in => transceiver_inst_n_11,
      data_sync_reg1 => data_in,
      enablealign => enablealign,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i => gtxe2_i,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0),
      powerdown => powerdown,
      reset_sync5(0) => mgt_tx_reset,
      rx_fsm_reset_done_int_reg => transceiver_inst_n_12,
      rxbufstatus(0) => rxbuferr,
      rxchariscomma(0) => rxchariscomma,
      rxcharisk(0) => rxcharisk,
      \rxdata_reg[7]_0\(7 downto 0) => rxdata(7 downto 0),
      rxdisperr(0) => rxdisperr,
      rxn => rxn,
      rxnotintable(0) => rxnotintable,
      rxoutclk => rxoutclk,
      rxp => rxp,
      status_vector(0) => \^status_vector\(1),
      txbuferr => txbuferr,
      txchardispval_reg_reg_0(0) => txchardispval,
      txcharisk_reg_reg_0(0) => txcharisk,
      \txdata_reg_reg[7]_0\(7 downto 0) => txdata(7 downto 0),
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support is
  port (
    gtrefclk_p : in STD_LOGIC;
    gtrefclk_n : in STD_LOGIC;
    gtrefclk_out : out STD_LOGIC;
    gtrefclk_bufg_out : out STD_LOGIC;
    txp : out STD_LOGIC;
    txn : out STD_LOGIC;
    rxp : in STD_LOGIC;
    rxn : in STD_LOGIC;
    userclk_out : out STD_LOGIC;
    userclk2_out : out STD_LOGIC;
    rxuserclk_out : out STD_LOGIC;
    rxuserclk2_out : out STD_LOGIC;
    pma_reset_out : out STD_LOGIC;
    mmcm_locked_out : out STD_LOGIC;
    resetdone : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gmii_txd : in STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_tx_en : in STD_LOGIC;
    gmii_tx_er : in STD_LOGIC;
    gmii_rxd : out STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_rx_dv : out STD_LOGIC;
    gmii_rx_er : out STD_LOGIC;
    gmii_isolate : out STD_LOGIC;
    configuration_vector : in STD_LOGIC_VECTOR ( 4 downto 0 );
    status_vector : out STD_LOGIC_VECTOR ( 15 downto 0 );
    reset : in STD_LOGIC;
    signal_detect : in STD_LOGIC;
    gt0_qplloutclk_out : out STD_LOGIC;
    gt0_qplloutrefclk_out : out STD_LOGIC
  );
  attribute EXAMPLE_SIMULATION : integer;
  attribute EXAMPLE_SIMULATION of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support : entity is 0;
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support : entity is "yes";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support is
  signal \<const0>\ : STD_LOGIC;
  signal \^gt0_qplloutclk_out\ : STD_LOGIC;
  signal \^gt0_qplloutrefclk_out\ : STD_LOGIC;
  signal \^gtrefclk_bufg_out\ : STD_LOGIC;
  signal \^gtrefclk_out\ : STD_LOGIC;
  signal \^mmcm_locked_out\ : STD_LOGIC;
  signal mmcm_reset : STD_LOGIC;
  signal \^pma_reset_out\ : STD_LOGIC;
  signal rxoutclk : STD_LOGIC;
  signal \^rxuserclk2_out\ : STD_LOGIC;
  signal \^status_vector\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal txoutclk : STD_LOGIC;
  signal \^userclk2_out\ : STD_LOGIC;
  signal \^userclk_out\ : STD_LOGIC;
begin
  gt0_qplloutclk_out <= \^gt0_qplloutclk_out\;
  gt0_qplloutrefclk_out <= \^gt0_qplloutrefclk_out\;
  gtrefclk_bufg_out <= \^gtrefclk_bufg_out\;
  gtrefclk_out <= \^gtrefclk_out\;
  mmcm_locked_out <= \^mmcm_locked_out\;
  pma_reset_out <= \^pma_reset_out\;
  rxuserclk2_out <= \^rxuserclk2_out\;
  rxuserclk_out <= \^rxuserclk2_out\;
  status_vector(15) <= \<const0>\;
  status_vector(14) <= \<const0>\;
  status_vector(13) <= \<const0>\;
  status_vector(12) <= \<const0>\;
  status_vector(11) <= \<const0>\;
  status_vector(10) <= \<const0>\;
  status_vector(9) <= \<const0>\;
  status_vector(8) <= \<const0>\;
  status_vector(7) <= \<const0>\;
  status_vector(6 downto 0) <= \^status_vector\(6 downto 0);
  userclk2_out <= \^userclk2_out\;
  userclk_out <= \^userclk_out\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
core_clocking_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_clocking
     port map (
      gtrefclk_bufg => \^gtrefclk_bufg_out\,
      gtrefclk_n => gtrefclk_n,
      gtrefclk_out => \^gtrefclk_out\,
      gtrefclk_p => gtrefclk_p,
      mmcm_locked => \^mmcm_locked_out\,
      mmcm_reset => mmcm_reset,
      rxoutclk => rxoutclk,
      rxuserclk2_out => \^rxuserclk2_out\,
      txoutclk => txoutclk,
      userclk => \^userclk_out\,
      userclk2 => \^userclk2_out\
    );
core_gt_common_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_gt_common
     port map (
      gt0_qplloutclk_out => \^gt0_qplloutclk_out\,
      gt0_qplloutrefclk_out => \^gt0_qplloutrefclk_out\,
      gtrefclk_out => \^gtrefclk_out\,
      independent_clock_bufg => independent_clock_bufg,
      \out\(0) => \^pma_reset_out\
    );
core_resets_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_resets
     port map (
      independent_clock_bufg => independent_clock_bufg,
      \out\(0) => \^pma_reset_out\,
      reset => reset
    );
pcs_pma_block_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_block
     port map (
      CLK => \^userclk2_out\,
      configuration_vector(2 downto 0) => configuration_vector(3 downto 1),
      data_in => \^mmcm_locked_out\,
      gmii_isolate => gmii_isolate,
      gmii_rx_dv => gmii_rx_dv,
      gmii_rx_er => gmii_rx_er,
      gmii_rxd(7 downto 0) => gmii_rxd(7 downto 0),
      gmii_tx_en => gmii_tx_en,
      gmii_tx_er => gmii_tx_er,
      gmii_txd(7 downto 0) => gmii_txd(7 downto 0),
      gt0_qplloutclk_out => \^gt0_qplloutclk_out\,
      gt0_qplloutrefclk_out => \^gt0_qplloutrefclk_out\,
      gtrefclk_bufg => \^gtrefclk_bufg_out\,
      gtrefclk_out => \^gtrefclk_out\,
      gtxe2_i => \^userclk_out\,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \^pma_reset_out\,
      resetdone => resetdone,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      signal_detect => signal_detect,
      status_vector(6 downto 0) => \^status_vector\(6 downto 0),
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    gtrefclk_p : in STD_LOGIC;
    gtrefclk_n : in STD_LOGIC;
    gtrefclk_out : out STD_LOGIC;
    gtrefclk_bufg_out : out STD_LOGIC;
    txp : out STD_LOGIC;
    txn : out STD_LOGIC;
    rxp : in STD_LOGIC;
    rxn : in STD_LOGIC;
    resetdone : out STD_LOGIC;
    userclk_out : out STD_LOGIC;
    userclk2_out : out STD_LOGIC;
    rxuserclk_out : out STD_LOGIC;
    rxuserclk2_out : out STD_LOGIC;
    pma_reset_out : out STD_LOGIC;
    mmcm_locked_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gmii_txd : in STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_tx_en : in STD_LOGIC;
    gmii_tx_er : in STD_LOGIC;
    gmii_rxd : out STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_rx_dv : out STD_LOGIC;
    gmii_rx_er : out STD_LOGIC;
    gmii_isolate : out STD_LOGIC;
    configuration_vector : in STD_LOGIC_VECTOR ( 4 downto 0 );
    status_vector : out STD_LOGIC_VECTOR ( 15 downto 0 );
    reset : in STD_LOGIC;
    signal_detect : in STD_LOGIC;
    gt0_qplloutclk_out : out STD_LOGIC;
    gt0_qplloutrefclk_out : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute EXAMPLE_SIMULATION : integer;
  attribute EXAMPLE_SIMULATION of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is 0;
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "gig_ethernet_pcs_pma_v16_2_15,Vivado 2023.2";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \^status_vector\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal NLW_U0_status_vector_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 7 );
  attribute EXAMPLE_SIMULATION of U0 : label is 0;
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
begin
  status_vector(15) <= \<const0>\;
  status_vector(14) <= \<const0>\;
  status_vector(13) <= \<const0>\;
  status_vector(12) <= \<const0>\;
  status_vector(11) <= \<const0>\;
  status_vector(10) <= \<const0>\;
  status_vector(9) <= \<const0>\;
  status_vector(8) <= \<const0>\;
  status_vector(7) <= \<const0>\;
  status_vector(6 downto 0) <= \^status_vector\(6 downto 0);
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support
     port map (
      configuration_vector(4) => '0',
      configuration_vector(3 downto 1) => configuration_vector(3 downto 1),
      configuration_vector(0) => '0',
      gmii_isolate => gmii_isolate,
      gmii_rx_dv => gmii_rx_dv,
      gmii_rx_er => gmii_rx_er,
      gmii_rxd(7 downto 0) => gmii_rxd(7 downto 0),
      gmii_tx_en => gmii_tx_en,
      gmii_tx_er => gmii_tx_er,
      gmii_txd(7 downto 0) => gmii_txd(7 downto 0),
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg_out => gtrefclk_bufg_out,
      gtrefclk_n => gtrefclk_n,
      gtrefclk_out => gtrefclk_out,
      gtrefclk_p => gtrefclk_p,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_locked_out => mmcm_locked_out,
      pma_reset_out => pma_reset_out,
      reset => reset,
      resetdone => resetdone,
      rxn => rxn,
      rxp => rxp,
      rxuserclk2_out => rxuserclk2_out,
      rxuserclk_out => rxuserclk_out,
      signal_detect => signal_detect,
      status_vector(15 downto 7) => NLW_U0_status_vector_UNCONNECTED(15 downto 7),
      status_vector(6 downto 0) => \^status_vector\(6 downto 0),
      txn => txn,
      txp => txp,
      userclk2_out => userclk2_out,
      userclk_out => userclk_out
    );
end STRUCTURE;
