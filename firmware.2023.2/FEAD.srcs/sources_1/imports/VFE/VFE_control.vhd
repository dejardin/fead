library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.FEAD_IO.all;

library UNISIM;
use UNISIM.VComponents.all;

entity VFE_ctrl is
  port (
    I2C_Access        : in     std_logic;
    I2C_R_Wb          : in     std_logic;
    I2C_Device_number : in     std_logic_vector(6 downto 0);
    I2C_Reg_number    : in     std_logic_vector(6 downto 0);
    I2C_long_transfer : in     std_logic;
    I2C_Bulky         : in     std_logic;
    I2C_Bulk_length   : in     natural range 0 to 19;
    I2C_Bulk_data_in  : in     Byte_t(18 downto 0);
    I2C_lpGBT_mode    : in     std_logic;

    I2C_busy_out      : out    std_logic;
    I2C_error         : out    std_logic;
    I2C_Reg_data_out  : out    std_logic_vector(15 downto 0);
    I2C_n_ack         : out    unsigned(7 downto 0);
    I2C_Bulk_data_out : out    Byte_t(18 downto 0);
    
    ADC_start_Resync  : in     std_logic;
    ADC_invert_Resync : in     std_logic;
    ADC_ReSync_idle   : in     std_logic_vector(7 downto 0);
    ADC_ReSync_data   : in     std_logic_vector(31 downto 0);
    ReSync_busy_out   : out    std_logic;

    reset             : in     std_logic;
    BC0               : in     std_logic;
    BC0_out           : out    std_logic;
    TE                : in     std_logic;
    TE_command        : in     std_logic_vector(3 downto 0);
    force_I2C_low     : in     std_logic;
    I2C_scl           : inout  std_logic;
    I2C_sda           : inout  std_logic;
    I2C_clk           : in     std_logic;
    I2C_ack_spy       : out    std_logic_vector(N_I2C_spy_bits-1 downto 0);

    sequence_clk      : in     std_logic;
    ReSync_DTU        : out    std_logic
  );
end entity VFE_ctrl;

architecture rtl of VFE_ctrl is

  signal I2C_last_transfer      : STD_LOGIC := '1';             -- last transfer (1) or not (0) of data (1 or 2 byte transfers)
  signal I2C_reset_n            : STD_LOGIC;                    -- active low reset
  signal I2C_scl_int            : STD_LOGIC;                    -- 
  signal I2C_scl_int_del        : STD_LOGIC;
  signal I2C_sda_int            : STD_LOGIC;                    -- 
  signal I2C_sda_int_del        : STD_LOGIC;
  signal I2C_ena                : STD_LOGIC;                    -- 
  signal I2C_ena_del            : STD_LOGIC;
  signal I2C_addr               : STD_LOGIC_VECTOR(6 DOWNTO 0); -- address of target slave
  signal I2C_R_Wb_loc           : STD_LOGIC;                    -- '0' is write, '1' is read
  signal I2C_data_w             : STD_LOGIC_VECTOR(7 DOWNTO 0); -- data to write to slave
  signal I2C_data_r             : STD_LOGIC_VECTOR(15 DOWNTO 0); -- data read from slave
  signal I2C_int_busy           : STD_LOGIC;                    -- indicates transaction in progress
  signal I2C_int_busy_prev      : STD_LOGIC;                    -- mandatory to detect busy transiton
  signal I2C_loc_data_r         : STD_LOGIC_VECTOR(7 DOWNTO 0); -- data read from slave
  signal I2C_ack_error          : STD_LOGIC;                    -- flag if improper acknowledge from slave
  signal I2C_slave_ack          : STD_LOGIC;                    -- flag if improper acknowledge from slave
  signal loc_I2C_ack_spy        : std_logic_vector(N_I2C_spy_bits-1 downto 0);
  signal loc_I2C_n_ack          : unsigned(7 downto 0)          := (others => '0');
  signal VFE_reset              : std_logic;
  signal BC0_del                : std_logic := '0';
  signal TE_del                 : std_logic := '0';
  signal DTU_flush              : std_logic := '0';
  signal DTU_flush_del          : std_logic := '0';
  signal I2C_max_Bytes          : natural range 0 to 19 := 1;  -- Number of bytes to transfer (1 I2C_long=0, 2 I2Clong=1,or 17 I2C_bulky_DTU)
  signal I2C_n_bytes            : natural range 0 to 19 := 0;  -- Running counter of transfered bytes for short/long/bulky tranfer

  type   I2C_state_type is (idle,
                            write_reg_address, wait_for_busy_reg,
                            read_reg_data,     wait_for_busy_read,
                            write_reg_data,    wait_for_busy_write);
  signal I2C_state              : I2C_state_type := idle;
  signal I2C_busy_del           : std_logic := '0';
  signal I2C_busy_loc           : std_logic := '0';

  signal ADC_Start_ReSync_del   : std_logic := '0';
  signal ADC_ReSync_transfer    : unsigned(7 downto 0)          := (others => '0');
  signal ADC_ReSync_stream      : std_logic_vector(79 downto 0) := (others => '0');
  signal Idle_ReSync_data       : std_logic_vector(7 downto 0)  := x"00";
  signal ReSync_DTU_loc         : std_logic := '0';
  signal ReSync_DTU_del         : std_logic := '0';
  type   ReSync_state_type is (ReSync_idle, ReSync_start);
  signal ReSync_state           : ReSync_state_type := ReSync_idle;
  signal ADC_ReSync_data_loc    : std_logic_vector(31 downto 0);

begin  -- architecture behavioral

  VFE_reset                      <= reset;
  I2C_reset_n                    <= not VFE_reset;
  I2C_error                      <= I2C_ack_error;
  I2C_Reg_data_out               <= x"00"&I2C_data_r(7 downto 0) when I2C_long_transfer='0' else
                                    I2C_data_r(7 downto 0)&I2C_data_r(15 downto 8); -- With long transfer, read msB first then lsB
  ReSync_DTU                     <= ReSync_DTU_loc;
  I2C_busy_out                   <= I2C_busy_loc;


  Inst_I2C_master : entity work.I2C_master
  port map(
    clk             => I2C_clk,             --system clock
    reset_n         => I2C_reset_n,         --active low reset
    ena             => I2C_ena,             --latch in command
    addr            => I2C_addr,            --address of target slave
    R_Wb            => I2C_R_Wb_loc,        --'0' is write, '1' is read
    data_w          => I2C_data_w,          --data to write to slave
    busy            => I2C_int_busy,        --indicates transaction in progress
    data_r          => I2C_loc_data_r,      --data read from slave
    ack_error       => I2C_ack_error,       --flag if improper acknowledge from slave
    slave_ack       => I2C_slave_ack,       -- acknowledge from slave
    force_I2C_low   => force_I2C_low,
    sda             => I2C_sda,             --serial data output of i2c bus
    scl             => I2C_scl,             --serial clock output of i2c bus
    scl_int         => I2C_scl_int,         --internal serial clock output of i2c bus
    sda_int         => I2C_sda_int          --internal serial data output of i2c bus
  );

  program_I2c : process(VFE_reset, I2C_Access, I2C_clk, I2C_int_busy)
  begin
    if VFE_reset = '1' then
      I2C_state                      <= idle;
      I2C_ena                        <= '0';
      I2C_busy_loc                   <= '0';
      I2C_int_busy_prev              <= '0';
      I2C_n_bytes                    <= 0;
      I2C_max_bytes                  <= 1;
    elsif Rising_Edge(I2C_clk) then
      case I2C_state is
      when idle =>
        I2C_ack_spy                  <= loc_I2C_ack_spy;
        I2C_n_ack                    <= loc_I2C_n_ack;
        I2C_busy_loc                 <= '0';
        I2C_ena                      <= '0';
        if I2C_Access = '1' then
          I2C_R_Wb_loc               <= '0';                                 -- first write register address
          I2C_addr                   <= I2C_Device_number;                   -- Device number
          I2C_data_w                 <= "0"&I2C_Reg_number;                  -- Register address
          I2C_state                  <= wait_for_busy_reg;                   -- Synchronize with I2C clock (slower)
          I2C_data_r                 <= (others => '0');
          I2C_ena                    <= '1';                                 -- Initiate the transaction (I2C master latch address and data)
          I2C_n_bytes                <=  0;
          if I2C_Bulky = '1' then
            I2C_max_bytes            <= I2C_Bulk_length;
          elsif I2C_long_transfer = '1' then
            I2C_max_bytes            <= 2;
          else
            I2C_max_bytes            <= 1;
          end if;
        end if;
      when wait_for_busy_reg =>                                              -- Wait for I2C master to become busy since its clock is slower
        I2C_busy_loc                 <= '1';                                 -- Stay busy during all the transaction
        if I2C_int_busy = '1' then
          I2C_state                  <= write_reg_address;
        end if;
      when write_reg_address =>                                              -- Chip/reg addresses have been latched. Prepare next transaction during first write
        I2C_addr                     <= I2C_Device_number;                   -- Put back Device address and add transaction type (read/write)
        I2C_R_Wb_loc                 <= I2C_R_Wb;
        I2C_data_w                   <= I2C_Bulk_data_in(I2C_n_bytes);
        if I2C_R_Wb = '1' and I2C_lpGBT_mode = '1' then -- lpGBT BUG
          I2C_ena <= '0';                          -- lpGBT BUG
        end if;                                    -- lpGBT BUG : Stop transaction in the middle to mimick lpGBT
        if I2C_int_busy = '0' then                                           -- Chip/reg adress write is finished
          if I2C_R_Wb = '0' then
            I2C_state                <= wait_for_busy_write;
          else
            I2C_state                <= wait_for_busy_read;
            I2C_ena                  <= '1';       -- lpGBT BUG : restart transaction
          end if;
        end if;
      when wait_for_busy_read =>                                             -- Wait for I2C master to become busy since its clock is slower
        if I2C_int_busy = '1' then
          I2C_state                  <= read_reg_data;
        end if;
      when read_reg_data =>
        if I2C_n_bytes = I2C_max_bytes-1 then
          I2C_ena                                  <= '0';                   -- Deassert enable to stop transaction after this read
        else
          I2C_ena                                  <= '1';                   -- Continue transaction for bulky read
        end if;
        if I2C_int_busy = '0' and I2C_int_busy_prev = '1' then               -- byte read finished
          I2C_Bulk_data_out(I2C_n_bytes)                 <= I2C_loc_data_r;
          if I2C_n_bytes = 0 then
            I2C_data_r(7 downto 0)                       <= I2C_loc_data_r;
          elsif I2C_n_bytes = 1 then
            I2C_data_r(15 downto 8)                      <= I2C_loc_data_r;
          end if;
          if I2C_n_bytes = I2C_max_bytes-1 then
            I2C_state                                    <= idle;                 -- End of transaction : 1 or 2 bytes read
          end if;
          I2C_n_bytes                                    <= I2C_n_bytes+1;
        end if;
      when wait_for_busy_write =>                                            -- Wait for I2C master to become busy since its clock is slower
        if I2C_int_busy = '1' then
          I2C_state                               <= write_reg_data;
        end if;
      when write_reg_data =>
        if I2C_n_bytes = I2C_max_bytes-1 then
          I2C_ena                                  <= '0';                            -- Deassert enable to stop transaction after this write
        else
          I2C_ena                                  <= '1';                            -- Continue transaction for bulky write
          I2C_data_w                               <= I2C_Bulk_data_in(I2C_n_bytes+1);-- prepare data for I2C master for next write at same address
        end if;
        if I2C_int_busy = '0' and I2C_int_busy_prev = '1' then               -- byte write finished
          if I2C_n_bytes = I2C_max_bytes-1 then                              -- We have finished the bulk transaction
            I2C_state                              <= idle;                  -- End of transaction : 1 or 2 bytes written         
          end if;
          I2C_n_bytes                              <= I2C_n_bytes+1;
        end if;
      end case;
      I2C_int_busy_prev <= I2C_int_busy;
    end if;
  end process program_I2C;
  
  proc_I2C_spy : process (reset, I2C_clk) is
  begin
    if reset = '1' then
      loc_I2C_ack_spy   <= (others => '1');
      loc_I2C_n_ack     <= (others => '0');
      I2C_SCL_int_del   <= '0';
      I2C_busy_del      <= '0';
    elsif rising_edge(I2C_clk) then
      I2C_busy_del      <= I2C_busy_loc;
      I2C_scl_int_del   <= I2C_scl_int;
      if I2C_busy_loc='1' and I2C_busy_del='0' then
--        loc_I2C_ack_spy   <= x"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0F0F";
        loc_I2C_ack_spy   <= (others => '1');
        loc_I2C_n_ack     <= (others => '0');
      end if;
      if I2C_SCL_int='0' and I2C_SCL_int_del='1' then
--        loc_I2C_ack_spy <= loc_I2C_ack_spy(n_I2C_spy_bits-2 downto 0)  & I2C_sda_int;
        if I2C_slave_ack = '1' then
          loc_I2C_n_ack   <= loc_I2C_n_ack+1;
          loc_I2C_ack_spy <= loc_I2C_ack_spy(n_I2C_spy_bits-2 downto 0)  & '1';
        else
          loc_I2C_ack_spy <= loc_I2C_ack_spy(n_I2C_spy_bits-2 downto 0)  & '0';
        end if;
      end if;

    end if; 
  end process proc_I2C_spy;

-- LiTE-DTU ReSync codes :
-- Code / Hamming encoding / Action
-- 0x0    0x00               stop 
-- 0x1    0x07               start 
-- 0x2    0x19               DTU and PLL_init reset 
-- 0x3    0x1E               I2C interface reset 
-- 0x4    0x2A               ADC TestUnit reset
-- 0x5    0x2D               DTU sync mode
-- 0x6    0x33               DTU normal mode
-- 0x7    0x34               DTU flush
-- 0x8    0x4B               ADCH reset 
-- 0x9    0x4C               ADCH calibration 
-- 0xA    0x52               ADCL reset 
-- 0xB    0x55               ADCL calibration
-- 0xC    0x61               Laser trigger
-- 0xD    0x66               CATIA test pulse
-- 0xE    0x78               BC0 marker
-- 0xF    0x7F               DTU PLL reset
  proc_ReSync : process (VFE_reset, ADC_Start_ReSync, sequence_clk) is
  begin
    if VFE_reset='1' then
      ReSync_DTU_loc              <= '0';
      ADC_ReSync_stream           <= (others => '0');
      Idle_ReSync_data            <= x"00";
      ReSync_state                <= ReSync_Idle; -- Send stop code after reset
      ADC_ReSync_transfer         <= x"50"; -- 48-bit transfer
      DTU_flush                   <= '1';
--    elsif rising_edge(clk_160) then
    elsif rising_edge(sequence_clk) then
      TE_del                      <= TE;
      BC0_del                     <= BC0;
      DTU_flush                   <= '0';
      DTU_flush_del               <= DTU_flush;
      ADC_Start_ReSync_del        <= ADC_Start_ReSync;
      ADC_ReSync_data_loc         <= ADC_ReSync_data;
      case ReSync_state is
        when ReSync_idle =>
          ReSync_DTU_loc          <= Idle_ReSync_data(7);
          Idle_ReSync_data        <= Idle_ReSync_data(6 downto 0) & Idle_Resync_data(7);
          ReSync_busy_out         <= '0';
          BC0_out                 <= '0';
          if ADC_Start_ReSync='1' and ADC_Start_ReSync_del='0' then
            ReSync_busy_out       <= '1';
            ReSync_state          <= ReSync_start;
            Idle_ReSync_data      <= ADC_ReSync_idle;
            ADC_ReSync_transfer   <= x"50";
            ADC_ReSync_stream     <= x"0007"&DTU_ReSync_code(to_integer(unsigned('0'&ADC_ReSync_data_loc( 3 downto  0))))&
                                             DTU_ReSync_code(to_integer(unsigned('0'&ADC_ReSync_data_loc( 7 downto  4))))&
                                             DTU_ReSync_code(to_integer(unsigned('0'&ADC_ReSync_data_loc(11 downto  8))))&
                                             DTU_ReSync_code(to_integer(unsigned('0'&ADC_ReSync_data_loc(15 downto 12))))&
                                             DTU_ReSync_code(to_integer(unsigned('0'&ADC_ReSync_data_loc(19 downto 16))))&
                                             DTU_ReSync_code(to_integer(unsigned('0'&ADC_ReSync_data_loc(23 downto 20))))&
                                             DTU_ReSync_code(to_integer(unsigned('0'&ADC_ReSync_data_loc(27 downto 24))))&
                                             DTU_ReSync_code(to_integer(unsigned('0'&ADC_ReSync_data_loc(31 downto 28))));
          end if;
          if DTU_flush='1' and DTU_flush_del='0' then
            ReSync_busy_out       <= '1';
            ReSync_state          <= ReSync_start;
            Idle_ReSync_data      <= ADC_ReSync_idle;
            ADC_ReSync_transfer   <= x"18";
            ADC_ReSync_stream     <= x"00073400000000000000";
          end if;
          if TE = '1' and TE_del = '0' then
            ReSync_busy_out       <= '1';
            ReSync_state          <= ReSync_start;
            Idle_ReSync_data      <= ADC_ReSync_idle;
            ADC_ReSync_transfer   <= x"18";
            ADC_ReSync_stream     <= x"0007"&DTU_ReSync_code(to_integer(unsigned('0'&TE_command)))&x"00000000000000";
          end if;
          if BC0='1' and BC0_del='0' then
            ReSync_busy_out       <= '1';
            ReSync_state          <= ReSync_start;
            Idle_ReSync_data      <= ADC_ReSync_idle;
            ADC_ReSync_transfer   <= x"18";
            ADC_ReSync_stream     <= x"00077800000000000000";
            BC0_out               <= '1';
          end if;
        when ReSync_start =>
          ReSync_DTU_loc          <= ADC_ReSync_stream(79);
          ADC_ReSync_stream       <= ADC_ReSync_stream(78 downto 0) & Idle_ReSync_data(7);
          Idle_ReSync_data        <= Idle_ReSync_data(6 downto 0) & Idle_Resync_data(7);
          if ADC_ReSync_transfer = 0 then
            ReSync_state          <= ReSync_idle;
          else
            ADC_ReSync_transfer   <= ADC_ReSync_transfer - 1;
          end if;
      end case;
    end if;
  end process proc_ReSync;

end architecture rtl;
