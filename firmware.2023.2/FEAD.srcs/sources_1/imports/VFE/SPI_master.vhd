--===========================================================================--
-- Serial Port Interface master adapted for CATIA_v1
-- Adapted from SPI_master.vhd
--===========================================================================--
-- Version  Author        Date               Description
--
-- 0.1      Marc Dejardin 2018/09/14
--

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_unsigned.all;
library UNISIM;
  use UNISIM.vcomponents.all;

entity spi_master is
  port (
    --+ CPU Interface Signals
    SPI_clk            : in  std_logic;
    reset              : in  std_logic;
    transmit           : in  std_logic_vector(1 downto 0);
    r_wb               : in  std_logic;
    SPI_user_data      : in  std_logic_vector(23 downto 0);
    wait_for_transfer  : out std_logic;
    --+ Hardware SPI Interface Signals
    SPI_csb            : out std_logic;
    SPI_data           : out std_logic;
    SPI_strobe         : out std_logic := '1'
  );
end;

architecture rtl of spi_master is

  --* State type of the SPI transfer state machine
  type   state_type is (idle, prepare, clk_goes_up, clk_goes_down, finished);
  signal state : state_type := idle;

  signal SPI_data_out          : std_logic;
  signal SPI_strobe_out        : std_logic :='0';
  signal SPI_csb_out           : std_logic :='1';
begin

-- Map hardware signals with internal ones
  SPI_data_obuf  : OBUF port map (O => SPI_data,  I => SPI_data_out);
  SPI_clk_obuf   : OBUF port map (O => SPI_strobe,I => SPI_strobe_out);
  SPI_csb_obuf   : OBUF port map (O => SPI_csb,   I => SPI_csb_out);

--* SPI transfer state machine
  spi_transfer : process(SPI_clk, reset, transmit)
  variable bit_count :  natural range 0 to 24 := 0;
  begin
    if reset = '1' then
      bit_count             := 0;
      wait_for_transfer     <= '0';
      SPI_strobe_out        <= '0';
      SPI_csb_out           <= '1';
      state                 <= idle;
    elsif falling_edge(SPI_clk) then
      case state is
        when idle =>
          SPI_csb_out            <= '1';
          SPI_strobe_out         <= '0';
          wait_for_transfer      <= '0';
          if transmit(0) = '1' then
            state                <= prepare;
            wait_for_transfer    <= '1';
-- Start with clock high or down depending on the edge on which the data is latched in the electronics
            SPI_strobe_out       <= transmit(1); 
          end if;
        when prepare =>
          SPI_strobe_out              <= not SPI_strobe_out;
          bit_count                := 23;
          spi_data_out             <= r_wb;
          SPI_csb_out              <= '0';
          state                    <= clk_goes_down;
        when clk_goes_down =>     -- clk rising edge : FPGA data is strobed by ADC or ADC data strobe by FPGA
          SPI_strobe_out           <= not SPI_strobe_out;
          state                    <= clk_goes_up;
        when clk_goes_up =>      -- clk falling edge : prepare data on the bus on both side
          SPI_strobe_out           <= not SPI_strobe_out;
          state                    <= clk_goes_down;
          if bit_count > 0 then
            bit_count              := bit_count -1;
            spi_data_out           <= SPI_user_data(bit_count);
          else
            state                  <= finished;
          end if;
        when finished =>      -- Let time to the caller to aknowledge the transfer end 
          wait_for_transfer        <= '0';
          state                    <= idle;
          SPI_csb_out              <= '1';
        when others =>
          state                    <= idle;
          SPI_csb_out              <= '1';
      end case;
    end if;
  end process spi_transfer;
end rtl;