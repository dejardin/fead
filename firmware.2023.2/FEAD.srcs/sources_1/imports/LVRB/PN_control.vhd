library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.FEAD_IO.all;

library UNISIM;
use UNISIM.VComponents.all;

entity PN_Ctrl is
  port (
    FEAD_control             : in     FEAD_control_t;
    VFE_control              : in     VFE_control_t;
    PN_Busy                  : out    std_logic;
    I2C_Reg_data_PN          : out    std_logic_vector(15 downto 0);
    reset                    : in     std_logic;
    I2C_scl                  : inout  std_logic;
    I2C_sda                  : inout  std_logic;
    I2C_clk                  : in     std_logic
  );
end entity PN_Ctrl;

architecture rtl of PN_Ctrl is

  signal I2C_long             : STD_LOGIC := '0';             -- short (8 bits = 0) or long (16 bits = 1) transfer
  signal I2C_last_transfer    : STD_LOGIC := '1';             -- last transfer (1) or not (0) of data (1 or 2 byte transfers)
  signal I2C_reset_n          : STD_LOGIC;                    -- active low reset
  signal I2C_ena              : STD_LOGIC;                    -- latch in command
  signal I2C_ena_del          : STD_LOGIC;
  signal I2C_addr             : STD_LOGIC_VECTOR(6 DOWNTO 0); -- address of target slave
  signal I2C_R_Wb             : STD_LOGIC;                    -- '0' is write, '1' is read
  signal I2C_data_w           : STD_LOGIC_VECTOR(7 DOWNTO 0); -- data to write to slave
  signal I2C_loc_busy         : STD_LOGIC;                    -- indicates transaction in progress
  signal I2C_loc_busy_prev    : STD_LOGIC;                    -- mandatory to detect busy transiton
  signal I2C_lsb_data_r       : STD_LOGIC_VECTOR(7 DOWNTO 0); -- data read from slave (lsb)
  signal I2C_msb_data_r       : STD_LOGIC_VECTOR(7 DOWNTO 0); -- data read from slave (msb)
  signal I2C_ack_error        : STD_LOGIC;                    -- flag if improper acknowledge from slave
  signal I2C_slave_ack        : STD_LOGIC;                    -- flag if improper acknowledge from slave
  signal PN_reset             : std_logic;
  signal I2C_busy             : std_logic;
  signal I2C_Access_PN_prev   : STD_LOGIC := '0';

  type   I2C_state_type is (idle,
                            write_reg_address, wait_for_busy_reg,
                            read_reg_data,     wait_for_busy_read,
                            write_reg_data,    wait_for_busy_write);
  signal I2C_state      : I2C_state_type := idle;

begin  -- architecture behavioral

  PN_reset               <= reset or VFE_Control.I2C_reset;
  I2C_reset_n            <= not PN_reset;
  PN_busy                <= I2C_busy;
  Inst_I2C_master_100kHz : entity work.I2C_master_100kHz
  port map(
    clk             => I2C_clk,        --system clock
    reset_n         => I2C_reset_n,    --active low reset
    ena             => I2C_ena,        --latch in command
    addr            => I2C_addr,       --address of target slave
    R_Wb            => I2C_R_Wb,       --'0' is write, '1' is read
    data_w          => I2C_data_w,     --data to write to slave
    busy            => I2C_loc_busy,   --indicates transaction in progress
    data_r          => I2C_lsb_data_r, --data read from slave
    ack_error       => I2C_ack_error,  --flag if improper acknowledge from slave
    slave_ack       => I2C_slave_ack,  -- acknowledge from slave
    sda             => I2C_sda,        --serial data output of i2c bus
    scl             => I2C_scl         --serial clock output of i2c bus
  );

  program_I2c : process(PN_reset, VFE_Control.I2C_Access_PN, I2C_clk)
  variable next_I2C_long_transfer : std_logic := '0';
  variable next_I2C_Device_number : std_logic_vector(6 downto 0)  := (others => '0');
  variable next_I2C_Reg_number    : std_logic_vector(6 downto 0)  := (others => '0');
  begin
    if PN_reset = '1' then
      I2C_state                          <= idle;
      I2C_long                           <= '0';
      I2C_busy                           <= '0';
      I2C_ena                            <= '0';
      I2C_loc_busy_prev                  <= '0';
      I2C_Access_PN_prev                 <= '0';
    elsif Rising_Edge(I2C_clk) then
      I2C_Access_PN_prev                 <= VFE_Control.I2C_Access_PN;
      case I2C_state is                                                         -- I2C prog state machine
      when idle =>
        I2C_busy                         <= '0';
        I2C_ena                          <= '0';
        I2C_msb_data_r                   <= (others => '0');       
        if VFE_Control.I2C_Access_PN = '1' and I2C_Access_PN_prev = '0' then
          I2C_long                       <= VFE_control.I2C_long_transfer;
          I2C_addr                       <= VFE_control.I2C_Device_number;
          I2C_data_w                     <= "0"&VFE_control.I2C_Reg_number;
          I2C_R_Wb                       <= '0';                                 -- first write device address
          I2C_Reg_data_PN                <= (others => '0');
          I2C_ena                        <= '1';                                 -- Initiate the transaction (I2C master latch address and data)
          I2C_state                      <= wait_for_busy_reg;                   -- Synchronize with I2C clock (slower)
          I2C_busy                       <= '1';                                 -- Stay busy during all the user resquested transaction
        end if;
      when wait_for_busy_reg =>                                                 -- Wait for I2C master to become busy since its clock is slower
        if I2C_loc_busy = '1' then
          I2C_state                      <= write_reg_address;
        end if;
      when write_reg_address =>                                                 -- Chip/reg addresses have been latched. Prepare next transaction during first write
        I2C_R_Wb                         <= VFE_control.I2C_R_Wb;
        if I2C_long = '1' then                                                  -- Prepare data on bus in case of write (by he way, we do it always...)
          I2C_last_transfer              <= '0';                                 -- In case of long transfer, start with MSB
          I2C_data_w                     <= VFE_control.I2C_Reg_data(15 downto 8);      
        else
          I2C_last_transfer              <= '1';
          I2C_data_w                     <= VFE_control.I2C_Reg_data(7 downto 0);       -- In case of short ransfer, start with LSB
        end if;
        if I2C_loc_busy = '0' then                                              -- Chip/reg adress write is finished
          if VFE_control.I2C_R_Wb = '1' then
            I2C_state                    <= wait_for_busy_read;
          else
            I2C_state                    <= wait_for_busy_write;
          end if;
        end if;
      when wait_for_busy_read =>                                                -- Wait for I2C master to become busy since its clock is slower
        if I2C_loc_busy = '1' then
          I2C_state                      <= read_reg_data;
        end if;
      when read_reg_data =>
        if I2C_last_transfer = '1' then
           I2C_ena                       <= '0';                                 -- Deassert enable to stop transaction after this read
        end if;
        if I2C_loc_busy = '0' and I2C_loc_busy_prev = '1' then                  -- byte read finished
          if I2C_last_transfer = '0' then
            I2C_msb_data_r               <= I2C_lsb_data_r;
            I2C_last_transfer            <= '1';
          else
            I2C_state                    <= idle;                                -- End of transaction : 1 or 2 bytes written
            I2C_Reg_data_PN              <= I2C_msb_data_r&I2C_lsb_data_r;
          end if;
        end if;
      when wait_for_busy_write =>                                            -- Wait for I2C master to become busy since its clock is slower
        if I2C_loc_busy = '1' then
          I2C_state                      <= write_reg_data;
        end if;
      when write_reg_data =>
        if I2C_last_transfer = '1' then
          I2C_ena                        <= '0';                                 -- Deassert enable to stop transaction after this last write
        else
          I2C_data_w                     <= VFE_control.I2C_Reg_data(7 downto 0);       -- prepare data for I2C master for second write during first write (if needed)
        end if;
        if I2C_loc_busy = '0' and I2C_loc_busy_prev = '1' then               -- byte write finished
          if I2C_last_transfer = '0' then
            I2C_last_transfer            <= '1';
          else
            I2C_state                    <= idle;                                -- End of transaction : 1 or 2 bytes written         
          end if;
        end if;
      end case;
      I2C_loc_busy_prev                  <= I2C_loc_busy;
    end if;
  end process program_I2C;
  
end architecture rtl;
