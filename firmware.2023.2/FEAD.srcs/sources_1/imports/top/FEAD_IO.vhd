----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
--
-- package for FEAD control intefrace to IPBUS
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

package FEAD_IO is

-- General setup for different FEAD flavors :
  constant USE_VICEPP       : boolean := false;
  constant USE_LVRB         : boolean := true;
  constant USE_DAC          : boolean := false;
  constant USE_PED_MUX      : boolean := false;
  constant CATIA_TEST_BOARD : boolean := true;
  constant USE_GPIO_I2C     : boolean := false;
  constant USE_EXTRA_I2C    : boolean := true;
  constant FOR_SEU          : boolean := false;
  
  constant N_I2C_spy_bits : Natural := 192;
-- FEAD : GPIO 2 and GPIO 0 connected to ADC inputs,Can measure V1P2 AND V2P5 from uLVRB
-- LVRB with GPIO_I2C, No Mux/APD temp, CATIA temp :
  --Vaux(9) : GPIO2, Vaux(3) : GPIO0, Vaux(2) : APD_temp_out(Vref_reg), Vaux(1) : CATIA_Temp, vaux(0) : APD_Temp (Vdac_buf)
  --constant XADC_VAUX_PATTERN  : bit_vector(15 downto 0) := x"020F";
-- LVRB with EXTRA_I2C, No Mux/APD temp, CATIA temp :
  --Vaux(1) : CATIA_Temp, vaux(0) : APD_Temp (Vdac_buf)
  constant XADC_VAUX_PATTERN  : bit_vector(15 downto 0) := x"0003";
-- LVRB, Mux/no APD temp, CATIA temp  :
  --//constant XADC_VAUX_PATTERN  : bit_vector(15 downto 0) := x"020A"; -- Vaux(9) : GPIO2, Vaux(3) : GPIO0, Vaux(1) : CATIA_Temp
-- DAC, Mux :
  --//constant XADC_VAUX_PATTERN  : bit_vector(15 downto 0) := x"0002"; -- Vaux(1) : CATIA_Temp with FEAD
  --//constant XADC_VAUX_PATTERN  : bit_vector(15 downto 0) := x"0100"; -- Vaux(8) : CATIA_Temp with VICE++

-- VICEPP : GPIO not connected to ADC inputs, Can't measure 1.2V and 2.5V from uLVRB
-- No Mux/APD_temp, CATIA_temp :
  --Vaux(0) : APD_temp, Vaux(8) : CATIA_Temp
  --constant XADC_VAUX_PATTERN  : bit_vector(15 downto 0) := x"0101";
-- Mux/no APD temp, CATIA_temp :
  --Vaux(8) : CATIA_Temp

  type Int16_t  is array (integer range <>) of signed(15 downto 0);
  type UInt32_t is array (integer range <>) of unsigned(31 downto 0);
  type UInt16_t is array (integer range <>) of unsigned(15 downto 0);
  type UInt8_t  is array (integer range <>) of unsigned(7 downto 0);
  type Byte_t   is array (integer range <>) of std_logic_vector(7 downto 0);
  type Word_t   is array (integer range <>) of std_logic_vector(31 downto 0);
  type length_t is array (integer range <>) of natural range 0 to 16000;

  type Idelay_pos_t is array (integer range <>) of std_logic_vector(4 downto 0);
  type FEAD_Monitor_t is record
    seq_clock_phase      : std_logic_vector(2 downto 0);
    IO_clock_phase       : std_logic_vector(2 downto 0);
    reg_clock_phase      : std_logic_vector(2 downto 0);
    mem_clock_phase      : std_logic_vector(2 downto 0);
    resync_clock_phase   : std_logic_vector(2 downto 0);
    reset                : std_logic;
    FIFO_mode            : std_logic;                    -- 0: single event mode, 1: FIFO mode
    trig_loop            : std_logic;                    -- 0: external trigger, 1: internal trigger loop
    clock_reset          : std_logic;
    clock_select         : std_logic;
    clock_locked         : std_logic;
    LED_on               : std_logic;
    I2C_low              : std_logic;                    -- Force I2C SDA and SCL to 0
    firmware_ver         : std_logic_vector(31 downto 0);
    board_SN             : std_logic_vector(7 downto 0);
    trig_self_mode       : std_logic;                    -- Absolute trigger level (0) or delta between 2 samples  (1)
    trig_self            : std_logic;
    trig_self_mask       : std_logic_vector(5 downto 1); -- signal threshold to generate self trigger
    trig_self_thres      : std_logic_vector(11 downto 0); -- signal threshold to generate self trigger
    trigger_HW_delay     : std_logic_vector(15 downto 0); -- delay between received HW trigger and the capture start (x 160 MHz)
    trigger_SW_delay     : std_logic_vector(15 downto 0); -- delay between generated SW trigger and the capture start (x 160 MHz)
    calib_pulse_enabled  : std_logic;
    calib_mux_enabled    : std_logic;
    TP_mode              : std_logic;
    TP_duration          : std_logic_vector(15 downto 0); -- duration of the calibration trigger pulse
    TP_delay             : std_logic_vector(15 downto 0); -- delay between icalibration trigger and DAQ start
    TP_dummyb            : std_logic;                     -- Always send Iinj to TIA, not TIA dummy
    delay_locked         : std_logic;                     -- DELAYCTRL lock signals
    FE_synchronized      : std_logic;
    VFE_synchronized     : std_logic;
    link_idelay_sync     : std_logic_vector(5 downto 1);   -- IDELAY setting in the middle of the eye has been done 
    link_idelay_pos      : idelay_pos_t(5 downto 1);       -- Final Idelay tap position found during autoconfig
    link_byte_sync       : std_logic_vector(5 downto 1);   -- byte alignement to get header sync with 40 MHz clock is done
    link_bit_sync        : std_logic_vector(5 downto 1);   -- bit alignement to get header at byte start is done
    link_error_sync      : std_logic_vector(5 downto 1);   -- Failed to synchronize links
    sync_duration        : std_logic_vector(31 downto 0);
    VICEPP_Clk_Config    : std_logic_vector(31 downto 0);  -- Clock config for VICEPP board
    start_idelay_sync    : std_logic_vector(5 downto 1);  -- Launch Idelay tuning to sample in the eyes of ADC link 
    DCI_locked           : std_logic;
    CRC_error            : UInt32_t(5 downto 1);           -- CRC error counter for each channel
    debug1               : word_t(31 downto 0);            -- Debug1 words
    debug2               : word_t(31 downto 0);            -- Debug2 words
    TE_pos               : unsigned(13 downto 0);          -- BC0 signal output pos w.r.t. BC) sent to LiTE-DTU
    TE_command           : std_logic_vector(3 downto 0);   -- What to send on ReSYn upon TE recepion
    TE_enable            : std_logic;                      -- Disable/enable TE generation
  end record FEAD_Monitor_t;

  type FEAD_Control_t is record
    seq_clock_phase      : std_logic_vector(2 downto 0);
    IO_clock_phase       : std_logic_vector(2 downto 0);
    reg_clock_phase      : std_logic_vector(2 downto 0);
    mem_clock_phase      : std_logic_vector(2 downto 0);
    resync_clock_phase   : std_logic_vector(2 downto 0);
    clock_reset          : std_logic;
    clock_select         : std_logic;
    CRC_reset            : std_logic;
    reset                : std_logic;
    FIFO_mode            : std_logic;
    trig_loop            : std_logic;
    trigger              : std_logic;
    gen_100Hz            : std_logic;
    LED_on               : std_logic;
    I2C_low              : std_logic;
    trig_self            : std_logic;
    trig_self_mode       : std_logic;                    -- Absolute trigger level (0) or delta between 2 sampes (1)
    trig_self_mask       : std_logic_vector(5 downto 1);
    trig_self_thres      : std_logic_vector(11 downto 0);
    trigger_HW_delay     : std_logic_vector(15 downto 0); -- delay between received HW trigger and the capture start (x 160 MHz)
    trigger_SW_delay     : std_logic_vector(15 downto 0); -- delay between generated SW trigger and the capture start (x 160 MHz)
    calib_pulse_enabled  : std_logic;
    calib_mux_enabled    : std_logic;
    G10_calib_trigger    : std_logic;
    G1_calib_trigger     : std_logic;
    AWG_trigger          : std_logic;
    TP_trigger           : std_logic;
    TP_mode              : std_logic;
    TP_duration          : std_logic_vector(15 downto 0); -- duration of the calibration trigger pulse
    TP_delay             : std_logic_vector(15 downto 0); -- delay between icalibration trigger and DAQ start
    TP_dummyb            : std_logic;                     -- Always send Inj current to TIA and not TIA_dummy
    delay_tap_dir        : std_logic;
    delay_ADC_number     : std_logic_vector(5 downto 1);  -- ADC number on which we want to tune the delay
    IO_reset             : std_logic;                     -- Reset delay to minimal value on ADC_number and reset iserdes
    delay_reset          : std_logic;                     -- Reset delay to minimal value on ADC_number and reset iserdes
    bitslip_ADC_number   : std_logic_vector(5 downto 1);  -- ADC number on which we want to slip input bits
    byteslip_ADC_number  : std_logic_vector(5 downto 1);  -- ADC number on which we want to slip bytes
    start_idelay_sync    : std_logic_vector(5 downto 1);  -- Launch Idelay tuning to sample in the eyes of ADC link 
    VICEPP_Clk_Config    : std_logic_vector(31 downto 0);  -- Clock config for VICEPP board
    TE_pos               : unsigned(13 downto 0);          -- BC0 signal output pos w.r.t. BC) sent to LiTE-DTU
    TE_command           : std_logic_vector(3 downto 0);   -- What to send on ReSYn upon TE recepion
    TE_enable            : std_logic;                      -- Disable/enable TE generation
  end record FEAD_Control_t;
  constant DEFAULT_FEAD_Control : FEAD_Control_t := (seq_clock_phase     => "000",
                                                     IO_clock_phase      => "000",
                                                     reg_clock_phase     => "000",
                                                     mem_clock_phase     => "000",   
                                                     resync_clock_phase  => "000",   
                                                     clock_reset         => '0',
                                                     clock_select        => '0',
                                                     CRC_reset           => '0',
                                                     reset               => '0',
                                                     FIFO_mode           => '1',
                                                     trig_self_mode      => '0',
                                                     trig_self           => '1',
                                                     trig_loop           => '0',
                                                     trigger             => '0',
                                                     gen_100Hz           => '0',
                                                     LED_on              => '1',
                                                     I2C_low             => '1',
                                                     trig_self_mask      => "11111",
                                                     trig_self_thres     => x"FFF",
                                                     trigger_HW_delay    => x"0000",
                                                     trigger_SW_delay    => x"0000",
                                                     calib_mux_enabled   => '0',
                                                     calib_pulse_enabled => '0',
                                                     G10_calib_trigger   => '0',
                                                     G1_calib_trigger    => '0',
                                                     AWG_trigger         => '0',
                                                     TP_trigger          => '0',
                                                     TP_mode             => '0',
                                                     TP_duration         => x"00FF",
                                                     TP_delay            => x"0000",
                                                     TP_dummyb           => '1',
                                                     delay_tap_dir       => '1',
                                                     delay_ADC_number    => "00000",
                                                     IO_reset            => '0',
                                                     delay_reset         => '0',
                                                     bitslip_ADC_number  => "00000",
                                                     byteslip_ADC_number => "00000",
                                                     start_idelay_sync   => "00000",
                                                     TE_pos              => "11"&x"000",
                                                     TE_command          => x"D", -- TP by default
                                                     TE_enable           => '0',  -- Disable automatic TE generation by default
                                                     VICEPP_CLk_Config   => (others => '0')
                                                     );

  type VFE_Monitor_t is record
    I2C_Scan_Fault_LVRB_2V5  : std_logic_vector(7 downto 0);   -- Fault register_read from LVRB register in auto-scan mode
    I2C_Scan_ISense_LVRB_2V5 : std_logic_vector(15 downto 0);  -- Data read from LVRB register in auto-scan mode
    I2C_Scan_VSense_LVRB_2V5 : std_logic_vector(15 downto 0);  -- Data read from LVRB register in auto-scan mode
    I2C_Scan_Fault_LVRB_1V2  : std_logic_vector(7 downto 0);   -- Fault register_read from LVRB register in auto-scan mode
    I2C_Scan_ISense_LVRB_1V2 : std_logic_vector(15 downto 0);  -- Data read from LVRB register in auto-scan mode
    I2C_Scan_VSense_LVRB_1V2 : std_logic_vector(15 downto 0);  -- Data read from LVRB register in auto-scan mode
    I2C_Reg_data_PN          : std_logic_vector(15 downto 0);  -- Data read from register
    I2C_Reg_data_LVRB        : std_logic_vector(15 downto 0);  -- Data read from register
    I2C_Reg_data_VFE         : std_logic_vector(15 downto 0);  -- Data read from register
    I2C_Reg_number           : std_logic_vector(6 downto 0);   -- Register number accessed
    I2C_Device_number        : std_logic_vector(6 downto 0);   -- Register number accessed
    I2C_access_PN            : std_logic;                        -- Access PN_test board registers through I2C bus
    I2C_access_LVRB          : std_logic;                        -- Access uLVRB registers through I2C bus
    I2C_access_VFE           : std_logic;                        -- Access VFE chips registers through I2C bus
    I2C_PN_error             : std_logic;                        -- Error during I2C access to PN_test board (missing ack)
    I2C_LVRB_error           : std_logic;                        -- Error during I2C access to LVRB (missing ack)
    I2C_VFE_error            : std_logic;                        -- Error during I2C access to CATIA (missing ack)
    I2C_Bulky_DTU            : std_logic;                        -- Access to DTU registers through I2C bus in bulky mode : all registers at once
    I2C_lpGBT_mode           : std_logic;                        -- Mimick lpGBT bug in READ transactions
    I2C_bulk_length          : natural range 0 to 19;            -- Length of the bulk transfer (17 for LiTE-DTU.v1.0, 19 for LiTE-DTU.v1.2)
    I2C_n_ack_PN             : unsigned(7 downto 0);             -- Number of I2C acknowledge received during last transaction
    I2C_n_ack_LVRB           : unsigned(7 downto 0);             -- Number of I2C acknowledge received during last transaction
    I2C_n_ack_VFE            : unsigned(7 downto 0);             -- Number of I2C acknowledge received during last transaction
    I2C_PN_busy              : std_logic;                        -- PN_2C is running
    I2C_LVRB_busy            : std_logic;                        -- LVRB I2C is running
    I2C_VFE_busy             : std_logic;                        -- VFE I2C is running
    ReSync_busy              : std_logic;                        -- ReSync broadcast in progress
    DTU_auto_sync            : std_logic;                        -- eLinks synchronization durng ADC calibration with idle patterns
    I2C_R_Wb                 : std_logic;                        -- Write (0) to VFE register or Read (1) VFE registers
    I2C_long_transfer        : std_logic;                        -- 1 byte (0) or 2 bytes (1) I2C/SPI transaction
    I2C_ack_spy              : std_logic_vector(n_I2C_spy_bits-1 downto 0); -- Spy register of I2C protocol for CATIA1
    SPI_DAC_data             : std_logic_vector(15 downto 0);  -- Data read from register
    SPI_DAC_number           : std_logic_vector(3 downto 0);   -- DAC number accessed
    SPI_DAC_action           : std_logic_vector(3 downto 0);   -- DAC action performed
    SPI_access_DAC           : std_logic;                      -- Access DAC registers through SPI bus
    Vref_mux                 : std_logic_vector(5 downto 1);   -- extra analog mux with CATIA_v2.0 (use SEUD_in lines)
    ADC_Ped_Mux              : std_logic;                      -- Set Pedestal (1) or CATIA signals (0) on ADC inputs
    ADC_calib_mode           : std_logic;                      -- Put CATIA in ADC_calib mode (output signals near VCM)
    ADC_test_mode            : std_logic;                      -- Put ADC in test_mode : all data of both ADC are read out on 4 elinks
    ADC_MEM_mode             : std_logic;                      -- Put ADC in MEM_mode : Read only even samples of test mode (80 MHz). 2 ADCs on 4 eLinks (1,2,4,5)
    ADC_Cal_Busy             : std_logic_vector(5 downto 1);   -- ADC is in calibration procedure use it to synchronize incoming stream with idle patterns
    ADC_PLL_lock             : std_logic_vector(5 downto 1);   -- DTU PLL is locked (1) or not (0)
    DTU_Sync_pattern         : std_logic_vector(31 downto 0);  -- DTU word transmitted in sync mode (V2.0)
    ADC_ReSync_data          : std_logic_vector(31 downto 0);   -- Latest resync code used
    ADC_ReSync_idle          : std_logic_vector(7 downto 0);   -- resync idle patttern
    eLink_Active             : std_logic_vector(5 downto 1);   -- Bit pattern for elinks actually connected to FEAD board
    I2C_reset                : std_logic;
    VFE_reset                : std_logic;
    I2C_Bulk_data            : Byte_t(18 downto 0);             -- I2C registers read content
    I2C_Bulk_data_ref        : Byte_t(18 downto 0);             -- LiTE-DTU registers read content
    Nsample_orbit            : UInt16_t(5 downto 1);            -- Number of decoded samples between 2 BC0;
  end record VFE_Monitor_t;
  type VFE_Control_t is record
    LVRB_auto_scan           : std_logic;                      -- Allow LVRB autoscan mode (stop it during cataia calibration)
    I2C_Reg_data             : std_logic_vector(15 downto 0);  -- Data to read/write in register
    I2C_Reg_number           : std_logic_vector(6 downto 0);   -- Register number to access
    I2C_Device_number        : std_logic_vector(6 downto 0);   -- Register number to access
    I2C_access_PN            : std_logic;                      -- Access to uLVRB registers through I2C bus
    I2C_access_LVRB          : std_logic;                      -- Access to uLVRB registers through I2C bus
    I2C_access_VFE           : std_logic;                      -- Access to VFE chips (CATIA, LiTE-DTU) registers through I2C bus
    I2C_bulky_DTU            : std_logic;                      -- Access to DTU registers through I2C bus in bulky mode : all registers at once
    I2C_bulky                : std_logic;                      -- Use bulky transfer for next I2C transaction
    I2C_lpGBT_mode           : std_logic;                      -- Mimick lpGBT bug in READ transactions
    I2C_R_Wb                 : std_logic;                      -- Write (0) to VFE register or Read (1) VFE registers
    I2C_long_transfer        : std_logic;                      -- 1 byte (0) or 2 bytes (1) I2C/SPI transaction
    DTU_auto_sync            : std_logic;                      -- Do eLinks synchronization during ADC calibration with idle patterns
    Vref_mux                 : std_logic_vector(5 downto 1);   -- extra analog mux with CATIA_v2.0 (use SEUD_in lines)
    ADC_Ped_Mux              : std_logic;                      -- Set Pedestal (1) or CATIA signals (0) on ADC inputs
    ADC_calib_mode           : std_logic;                      -- Put CATIA in ADC_calib mode (output signals near VCM)
    ADC_test_mode            : std_logic;                      -- Put ADC in test_mode : all data of both ADC are read out on 4 elinks
    ADC_MEM_mode             : std_logic;                      -- Put ADC in MEM_mode : Read only even samples of test mode (80 MHz). 2 ADCs on 4 eLinks (1,2,4,5)
    ADC_ReSync_data          : std_logic_vector(31 downto 0);  -- Resync code to be sent to LiTE-DTU after Hamming encoding
    DTU_Sync_pattern         : std_logic_vector(31 downto 0);  -- DTU word transmitted in sync mode (V2.0)
    ADC_ReSync_idle          : std_logic_vector(7 downto 0);   -- Resync idle patterm
    ADC_start_ReSync         : std_logic;                      -- Start ReSync transaction
    ADC_invert_ReSync        : std_logic;                      -- Invert ReSync data for transaction
    eLink_Active             : std_logic_vector(5 downto 1);   -- Bit pattern for elinks actually connected to FEAD board
    I2C_reset                : std_logic;
    VFE_reset                : std_logic;
    I2C_Bulk_data            : Byte_t(18 downto 0);            -- LiTE-DTU registers content to write
    I2C_Bulk_data_ref        : Byte_t(18 downto 0);            -- LiTE-DTU registers content to write
    I2C_bulk_length          : natural range 0 to 19;          -- Length of the bulk transfer (17 for LiTE-DTU.v1.0, 19 for LiTE-DTU.v1.2)
    SPI_access_DAC           : std_logic;                      -- Access to DAC registers through SPI bus
    SPI_DAC_data             : std_logic_vector(15 downto 0);  -- Data to read/write in register
    SPI_DAC_number           : std_logic_vector(3 downto 0);   -- DAC number accessed
    SPI_DAC_action           : std_logic_vector(3 downto 0);   -- DAC action performed
  end record VFE_Control_t;
  constant DEFAULT_VFE_Control : VFE_Control_t := (LVRB_auto_scan      => '0',
                                                   I2C_Reg_data        => (others => '0'),
                                                   I2C_Reg_Number      => (others => '0'),
                                                   I2C_Device_Number   => (others => '0'),
                                                   I2C_access_PN       => '0',
                                                   I2C_access_LVRB     => '0',
                                                   I2C_access_VFE      => '0',
                                                   I2C_bulky_DTU       => '0',
                                                   I2C_bulky           => '0',
                                                   I2C_bulk_length     => 19,
                                                   I2C_long_transfer   => '0',
                                                   I2C_R_Wb            => '0',
                                                   I2C_lpGBT_mode      => '0',
                                                   DTU_auto_sync       => '0',
                                                   Vref_MUX            => (others => '0'),
                                                   ADC_Ped_Mux         => '0',
                                                   ADC_calib_mode      => '0',
                                                   ADC_test_mode       => '1',
                                                   ADC_MEM_mode        => '0',
                                                   ADC_ReSync_data     => (others => '0'),
                                                   ADC_ReSync_idle     => x"66",
--                                                   DTU_Sync_pattern    => x"0ccccccf",
                                                   DTU_Sync_pattern    => x"eaaaaaaa",
                                                   ADC_start_ReSync    => '0',
                                                   ADC_invert_ReSync   => '0',
                                                   eLink_Active        => (others => '0'),
                                                   I2C_reset           => '0',
                                                   VFE_reset           => '0',
--                                                   I2C_Bulk_data       => (x"2C",
--                                                                           x"65",x"04",x"00",x"00",
--                                                                           x"00",x"55",x"1B",x"40",
--                                                                           x"88",x"00",x"00",x"00",
--                                                                           x"00",x"07",x"03",x"1F"),
--                                                   I2C_Bulk_data_ref   => (x"2C",
--                                                                           x"65",x"04",x"00",x"00",
--                                                                           x"00",x"55",x"30",x"40",
--                                                                           x"88",x"00",x"00",x"00",
--                                                                           x"00",x"07",x"03",x"1F"), 
                                                   I2C_Bulk_data       => (x"0F",x"FF",x"2C",
                                                                           x"65",x"04",x"00",x"00",
                                                                           x"00",x"55",x"1B",x"40",
                                                                           x"88",x"00",x"00",x"00",
                                                                           x"00",x"07",x"03",x"1F"),
                                                   I2C_Bulk_data_ref   => (x"0F",x"FF",x"3C",
                                                                           x"65",x"04",x"00",x"00",
                                                                           x"00",x"55",x"3C",x"40",
                                                                           x"88",x"00",x"00",x"00",
                                                                           x"20",x"04",x"03",x"8F"), 
                                                   SPI_access_DAC      => '0',
                                                   SPI_DAC_data        => (others => '0'),
                                                   SPI_DAC_number      => (others => '0'),
                                                   SPI_DAC_action      => (others => '0')
                                                   );
  type XADC_Monitor_t is record
    XADC_ready           : std_logic;
    XADC_addr            : std_logic_vector(6 downto 0);
    XADC_data            : std_logic_vector(15 downto 0);
  end record XADC_Monitor_t;
  type XADC_Control_t is record
    XADC_access          : std_logic;
    XADC_WRb             : std_logic;
    XADC_addr            : std_logic_vector(6 downto 0);
    XADC_data            : std_logic_vector(15 downto 0);
  end record XADC_Control_t;
  constant DEFAULT_XADC_Control : XADC_Control_t := (
                                                     XADC_access         => '0',
                                                     XADC_WRb            => '0',
                                                     XADC_addr           => (others => '0'),
                                                     XADC_data           => (others => '0'));
  constant DEFAULT_XADC_Monitor : XADC_Monitor_t := (
                                                     XADC_ready          => '0',
                                                     XADC_addr           => (others => '0'),
                                                     XADC_data           => (others => '0'));

  constant DTU_ReSync_code : Byte_t(15 downto 0) := (x"7F",x"78",x"66",x"61",x"55",x"52",x"4C",x"4B",
                                                     x"34",x"33",x"2D",x"2A",x"1E",x"19",x"07",x"00");

end FEAD_IO;
