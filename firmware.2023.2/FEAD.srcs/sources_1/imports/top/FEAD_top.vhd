-- Copyright 1986-2015 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2015.4 (lin64) Build 1412921 Wed Nov 18 09:44:32 MST 2015
-- Date        : Mon Apr 25 14:31:29 2016
-- Host        : volta running 64-bit Debian GNU/Linux testing/unstable
-- Command     : write_vhdl -mode pin_planning -force -port_diff_buffers
--               /home/dan/work/CMS/cms-ecal-vfe-adapter/FEAD/testing/option2_484/io_1.vhd
-- Design      : ios
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a50tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
---- The following library declaration should be present if 
---- instantiating any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

use work.ipbus.all;
use work.ipbus_trans_decl.all;
use work.FEAD_IO.all;

entity FEAD is
  generic (
    -- Temporary trick to generate weird clock from FPGA (need to change also xdc file to get LVDS_25)
    USE_GPIO_WITH_DBG : boolean := (not USE_DAC) and (not USE_GPIO_I2C) and (not USE_EXTRA_I2C);
    USE_GPIO_WITH_DAC : boolean := USE_DAC
  );

  port (
  ch1_in_P          : in    std_logic;
  ch1_in_N          : in    std_logic;
  ch1_out_P         : out   std_logic;
  ch1_out_N         : out   std_logic;
  ch2_in_P          : in    std_logic;
  ch2_in_N          : in    std_logic;
  ch2_out_P         : out   std_logic;
  ch2_out_N         : out   std_logic;
  ch3_in_P          : in    std_logic;
  ch3_in_N          : in    std_logic;
  ch3_out_P         : out   std_logic;
  ch3_out_N         : out   std_logic;
  ch4_in_P          : in    std_logic;
  ch4_in_N          : in    std_logic;
  ch4_out_P         : out   std_logic;
  ch4_out_N         : out   std_logic;
  ch5_in_P          : in    std_logic;
  ch5_in_N          : in    std_logic;
  ch5_out_P         : out   std_logic;
  ch5_out_N         : out   std_logic;
  SEUA_in           : inout std_logic_vector(5 downto 1);
  SEUD_in           : out   std_logic_vector(5 downto 1);
  PllLock_in        : inout std_logic_vector(5 downto 1);
  CalBusy_in        : in    std_logic_vector(5 downto 1);
  OVF_in            : in    std_logic_vector(5 downto 1);
  Pwup_reset_out    : out   std_logic;
  ReSync_out_P      : out   std_logic;
  ReSync_out_N      : out   std_logic;
  VFE_IO            : inout std_logic;
  APD_temp_ref      : in    std_logic;
  CATIA_temp_in     : in    std_logic;
  CATIA_temp_ref    : in    std_logic;
  I2C_sda_in        : inout std_logic;
  I2C_scl_in        : inout std_logic;
  TP_trigger_out    : inout std_logic; -- Used as I2C_SCL with prod test board
  Calib_mode_out    : inout std_logic; -- Used as I2C_SDA with prod test board
  Test_mode_out     : out   std_logic;

  SEUA_out          : out   std_logic_vector(5 downto 1);
  SEUD_out          : out   std_logic_vector(5 downto 1);
  PllLock_out       : out   std_logic_vector(5 downto 1);
  CalBusy_out       : out   std_logic_vector(5 downto 1);
  OVF_out           : out   std_logic_vector(5 downto 1);
  ReSync_in_P       : in    std_logic;
  ReSync_in_N       : in    std_logic;

--  Test_mode_in      : in    std_logic;
--  Pwup_reset_in     : in    std_logic;
--  Calib_trigger_in  : in    std_logic;
--  APD_temp_out      : out   std_logic;
--  CATIA_temp_out    : out   std_logic;
--  Calib_mode_in     : in    std_logic;
  Ctrl_in           : inout std_logic_vector(3 downto 1);
  Ctrl_ref          : in    std_logic_vector(3 downto 1);
  Ctrl_out          : out   std_logic_vector(4 downto 1);

  I2C_sda_VFE       : inout std_logic;
  I2C_scl_VFE       : inout std_logic;
--  I2C_sda_LVRB      : inout std_logic;
--  I2C_scl_LVRB      : inout std_logic;
--  SPI_data_DAC      : out   std_logic;
--  SPI_strobe_DAC    : out   std_logic;
--  SPI_csb_DAC       : out   std_logic;

  HP_CLK_160_P      : in    std_logic;
  HP_CLK_160_N      : in    std_logic;
  CLK_160_P         : in    std_logic;
  CLK_160_N         : in    std_logic;
  CLK_select        : out   std_logic;

  GbE_refclk_N      : in  std_logic;
  GbE_refclk_P      : in  std_logic;
  GbE_RXN           : in  std_logic;
  GbE_RXP           : in  std_logic;
  GbE_TXN           : out std_logic;
  GbE_TXP           : out std_logic;

  SFP_Present       : in    std_logic;
  SFP_LOS           : in    std_logic;
  SFP_Tx_Fault      : in    std_logic;
  SFP_SCL           : out   std_logic;
  SFP_SDA           : inout std_logic;
  SFP_Rate          : out   std_logic;
  EQEN_25           : out   std_logic;

  LED               : out   std_logic_vector(3 downto 0);
  GPIO              : inout std_logic_vector(5 downto 0);
--  GPIO              : out   std_logic_vector(5 downto 0);
  ADDR              : in    std_logic_vector(7 downto 0);   -- Pins opened on FPGA. Pulled down = "0000"
  Switch            : in    std_logic_vector(3 downto 0);
--  V2P5_in           : in    std_logic;
--  V2P5_ref          : in    std_logic;
--  V1P2_in           : in    std_logic;
--  V1P2_ref          : in    std_logic
  XPS_Sout          : out   std_logic_vector(1 downto 0);
  XPS_Sin           : out   std_logic_vector(1 downto 0);
  XPS_Config        : out   std_logic_vector(1 downto 0);
  XPS_Load          : out   std_logic_vector(1 downto 0)
  );
end FEAD;

architecture rtl of FEAD is

component clk_generator_iserdes is
  port (
    clk_160_in      : in  std_logic;
    clk_20          : out std_logic;
    clk_40          : out std_logic;
    clk_160         : out std_logic;
    clk_640         : out std_logic;
    reset           : in  std_logic;
    locked          : out std_logic);
end component clk_generator_iserdes;

component clk_generator_delay_ctrl is
  port (
    clk_160_in      : in  std_logic;
    clk_delay_ctrl  : out std_logic;
    reset           : in  std_logic;
    locked          : out std_logic);
end component clk_generator_delay_ctrl;

component gig_ethernet_pcs_pma_0 is
  port (
    gtrefclk_p             : in  std_logic;
    gtrefclk_n             : in  std_logic;
    gtrefclk_out           : out std_logic;
    gtrefclk_bufg_out      : out std_logic;
    txp                    : out std_logic;
    txn                    : out std_logic;
    rxp                    : in  std_logic;
    rxn                    : in  std_logic;
    resetdone              : out std_logic;
    userclk_out            : out std_logic;
    userclk2_out           : out std_logic;
    rxuserclk_out          : out std_logic;
    rxuserclk2_out         : out std_logic;
    pma_reset_out          : out std_logic;
    mmcm_locked_out        : out std_logic;
    independent_clock_bufg : in  std_logic;
    gmii_txd               : in  std_logic_vector(7 downto 0);
    gmii_tx_en             : in  std_logic;
    gmii_tx_er             : in  std_logic;
    gmii_rxd               : out std_logic_vector(7 downto 0);
    gmii_rx_dv             : out std_logic;
    gmii_rx_er             : out std_logic;
    gmii_isolate           : out std_logic;
    configuration_vector   : in  std_logic_vector(4 downto 0);
    status_vector          : out std_logic_vector(15 downto 0);
    reset                  : in  std_logic;
    signal_detect          : in  std_logic;
    gt0_qplloutclk_out     : out std_logic;
    gt0_qplloutrefclk_out  : out std_logic
);
end component gig_ethernet_pcs_pma_0;

component tri_mode_ethernet_mac_0 is
  port (
    gtx_clk                 : in  std_logic;
      -- asynchronous reset
    glbl_rstn               : in  std_logic;
    rx_axi_rstn             : in  std_logic;
    tx_axi_rstn             : in  std_logic;
    rx_statistics_vector    : out std_logic_vector(27 downto 0);
    rx_statistics_valid     : out std_logic;
    rx_mac_aclk             : out std_logic;
    rx_reset                : out std_logic;
    rx_axis_mac_tdata       : out std_logic_vector(7 downto 0);
    rx_axis_mac_tvalid      : out std_logic;
    rx_axis_mac_tlast       : out std_logic;
    rx_axis_mac_tuser       : out std_logic;
    tx_ifg_delay            : in  std_logic_vector(7 downto 0);

    tx_statistics_vector    : out std_logic_vector(31 downto 0);
    tx_statistics_valid     : out std_logic;
    tx_mac_aclk             : out std_logic;
    tx_reset                : out std_logic;
    tx_axis_mac_tdata       : in  std_logic_vector(7 downto 0);
    tx_axis_mac_tvalid      : in  std_logic;
    tx_axis_mac_tlast       : in  std_logic;
    tx_axis_mac_tuser       : in  std_logic_vector(0 downto 0);
    tx_axis_mac_tready      : out std_logic;
    pause_req               : in  std_logic;
    pause_val               : in  std_logic_vector(15 downto 0);
    speedis100              : out std_logic;
    speedis10100            : out std_logic;
--    rx_enable                  => rx_enable,
--    tx_enable                  => tx_enable,
--    tx_ifg_delay               => tx_ifg_delay,
--    gmii_tx_clk                => gmii_tx_clk,
--    gmii_rx_clk                => gmii_rx_clk,
--    mii_tx_clk                 => mii_tx_clk,

    gmii_txd                : out std_logic_vector(7 downto 0);
    gmii_tx_en              : out std_logic;
    gmii_tx_er              : out std_logic;
    gmii_rxd                : in  std_logic_vector(7 downto 0);
    gmii_rx_dv              : in  std_logic;
    gmii_rx_er              : in  std_logic;
    rx_configuration_vector : in  std_logic_vector(79 downto 0);
    tx_configuration_vector : in  std_logic_vector(79 downto 0));
end component tri_mode_ethernet_mac_0;

component ipbus_ctrl is
  generic(
    MAC_CFG       : ipb_mac_cfg                   := EXTERNAL;
    IP_CFG        : ipb_ip_cfg                    := EXTERNAL;
    BUFWIDTH      : natural                       := 4;
    INTERNALWIDTH : natural                       := 1;
    ADDRWIDTH     : natural                       := 11;
    IPBUSPORT     : std_logic_vector(15 downto 0) := x"C351";
    SECONDARYPORT : std_logic                     := '0';
    N_OOB         : natural                       := 0
  );
  port (
    mac_clk      : in  std_logic;
    rst_macclk   : in  std_logic;
    ipb_clk      : in  std_logic;
    rst_ipb      : in  std_logic;
    mac_rx_data  : in  std_logic_vector(7 downto 0);
    mac_rx_valid : in  std_logic;
    mac_rx_last  : in  std_logic;
    mac_rx_error : in  std_logic;
    mac_tx_data  : out std_logic_vector(7 downto 0);
    mac_tx_valid : out std_logic;
    mac_tx_last  : out std_logic;
    mac_tx_error : out std_logic;
    mac_tx_ready : in  std_logic;
    ipb_out      : out ipb_wbus;
    ipb_in       : in  ipb_rbus;
    ipb_req      : out std_logic;
    ipb_grant    : in  std_logic                                := '1';
    mac_addr     : in  std_logic_vector(47 downto 0)            := X"000000000000";
    ip_addr      : in  std_logic_vector(31 downto 0)            := X"00000000";
    enable       : in  std_logic                                := '1';
    RARP_select  : in  std_logic                                := '0';
    pkt          : out std_logic;
    pkt_oob      : out std_logic;
    oob_in       : in  ipbus_trans_in_array(N_OOB - 1 downto 0) := (others => ('0', X"00000000", '0'));
    oob_out      : out ipbus_trans_out_array(N_OOB - 1 downto 0)
	);
end component ipbus_ctrl;

signal gmii_txd            : std_logic_vector(7 downto 0);
signal gmii_tx_en          : std_logic;
signal gmii_tx_er          : std_logic;
signal gmii_rxd            : std_logic_vector(7 downto 0);
signal gmii_rx_dv          : std_logic;
signal gmii_rx_er          : std_logic;
signal GbE_user_clk        : std_logic;
signal fake_packet_counter : integer range 0 to 1536 := 0;

signal mac_tx_data, mac_rx_data                                                         : std_logic_vector(7 downto 0);
signal mac_tx_valid, mac_tx_last, mac_tx_ready, mac_rx_valid, mac_rx_last, mac_rx_error : std_logic;
signal mac_tx_error                                                                     : std_logic_vector(0 downto 0);
signal ipb_master_out                                                                   : ipb_wbus;
signal ipb_master_in                                                                    : ipb_rbus;
signal mac_addr                                                                         : std_logic_vector(47 downto 0);
signal ip_addr                                                                          : std_logic_vector(31 downto 0);
signal pkt, pkt_oob                                                                     : std_logic;
signal frame_size              : std_logic_vector(15 downto 0);
signal rx_configuration_vector : std_logic_vector(79 downto 0);
signal tx_configuration_vector : std_logic_vector(79 downto 0);

component VICEPP_Clk_Ctrl is
  port (
    reset               : in    std_logic;
    clk                 : in    std_logic;
    Xpoint_ready        : out   std_logic;
    XPS_default         : in    std_logic;
    XPS_Define_in       : in    std_logic_vector(31 downto 0);
    XPS_Define_out      : out   std_logic_vector(31 downto 0);
    XPS_Sout            : out   std_logic_vector(1 downto 0);
    XPS_Sin             : out   std_logic_vector(1 downto 0);
    XPS_Config          : out   std_logic_vector(1 downto 0);
    XPS_Load            : out   std_logic_vector(1 downto 0)
  );
end component VICEPP_Clk_Ctrl;

component DAC_Ctrl is
  port (
    SPI_Access_DAC      : in    std_logic;
    SPI_DAC_data        : in    std_logic_vector(15 downto 0);
    SPI_DAC_number      : in    std_logic_vector(3 downto 0);
    SPI_DAC_action      : in    std_logic_vector(3 downto 0);
    reset               : in    std_logic;
    SPI_clk             : in    std_logic;
    SPI_csb             : out   std_logic;
    SPI_data            : out   std_logic;
    SPI_strobe          : out   std_logic
  );
end component DAC_ctrl;

--Array mappings to simplify instances
signal captured_stream : word_t(5 downto 1);
signal output_stream   : word_t(5 downto 1);

signal loc_clk_in          : std_logic := '0';
signal Clk_ipbus           : std_logic := '0';
signal Clk_Gb_eth          : std_logic := '0';
signal Clk_I2C             : std_logic := '0';
signal System_clk          : std_logic := '0';
signal Sequence_clk        : std_logic := '0';
signal shift_reg_clk       : std_logic := '0';
signal memory_clk          : std_logic := '0';
signal Resync_clk          : std_logic := '0';
signal Clk_delay_ctrl      : std_logic := '0';
signal Clk_delay_ctrl_loc  : std_logic := '0';
signal iserdes_clock_locked: std_logic := '0';
signal delay_clock_locked  : std_logic := '0';
signal MMCM_locked         : std_logic := '0';
signal delay_locked_debug  : std_logic := '0';

signal HP_Clk_in           : std_logic := '0';
signal HP_Clk_160          : std_logic := '0';
signal HP_Clk_20           : std_logic := '0';
signal HP_Clk_40           : std_logic := '0';
signal HP_Clk_640          : std_logic := '0';

signal I2C_data_LVRB       : std_logic_vector(15 downto 0) := (others => '0');
signal I2C_data_VFE        : std_logic_vector(15 downto 0) := (others => '0');
signal dbg_i2c_clk         : std_logic;
signal dbg_sda             : std_logic;
signal dbg_scl             : std_logic;

--Monitoring
signal FEAD_Monitor           : FEAD_Monitor_t;
signal FEAD_Control           : FEAD_Control_t  := DEFAULT_FEAD_Control;
signal VFE_Monitor            : VFE_Monitor_t;
signal VFE_Control            : VFE_Control_t   := DEFAULT_VFE_Control;
signal led_counter            : unsigned(31 downto 0) := (others => '0');
signal pwup_reset             : std_logic := '1';
signal VFE_reset              : std_logic := '0'; -- Pwup_resetb sent to VFE
signal FEAD_reset             : std_logic := '0';
signal CLK_reset              : std_logic := '0';
signal Xpoint_ready           : std_logic := '0';
signal trig_signal            : std_logic := '0'; -- Single event trigger
signal trig_software          : std_logic := '0'; -- single event trigger received on ipbus
signal trig_sw_width          : unsigned(3 downto 0) := (others => '0');
signal trig_out               : std_logic := '0'; -- output signal generated from trig_software after some delay
signal trig_in                : std_logic := '0'; -- single event  trigger receives on GPIO pin
signal trig_hardware          : std_logic := '0'; -- input signal generated from trig_hardware after some delay
signal trig_hw_width          : unsigned(3 downto 0) := (others => '0');
signal trig_local             : std_logic := '0'; -- trigger signal generated looking at input data (required trig_self=1)
signal trig_local_outb        : std_logic := 'Z'; -- trigger signal generated looking at input data (required trig_self=1) sent to FE
signal trig_in_prev1          : std_logic := '0';
signal trig_in_prev2          : std_logic := '0';
signal trigger_prev1          : std_logic := '0';
signal trigger_prev2          : std_logic := '0';
signal delay_out_counter      : unsigned(15 downto 0) := x"0000";
signal start_out_counter      : std_logic := '0';
signal delay_in_counter       : unsigned(15 downto 0) := x"0000";
signal start_in_counter       : std_logic := '0';
signal DAQ_busy               : std_logic := '0';
signal BC0                    : std_logic := '0';
signal BC0_out                : std_logic := '0';
signal Test_enable            : std_logic := '0';
signal ipbus_counter          : std_logic_vector(2 downto 0) := "000";
signal init_XPS               : std_logic := '1';
signal VICEPP_Clk_Config_in   : std_logic_vector(31 downto 0);
signal VICEPP_Clk_Config_out  : std_logic_vector(31 downto 0);
signal clk_counter            : unsigned(13 downto 0) := (others => '0');
signal orbit_counter          : unsigned(31 downto 0) := (others => '0');

signal SEUA_loc               : std_logic_vector(5 downto 1) := (others => '0');
signal SEUD_loc               : std_logic_vector(5 downto 1) := (others => '0');
signal G10_calib_pulse        : std_logic := '0';
signal G10_trigger_prev1      : std_logic := '0';
signal G10_trigger_prev2      : std_logic := '0';
signal start_G10_delay        : std_logic := '0';
signal G10_duration_counter   : unsigned(15 downto 0) := x"0000";
signal G10_delay_counter      : unsigned(15 downto 0) := x"0000";
signal G10_DAQ_start          : std_logic := '0';
signal G10_DAQ_start_width    : unsigned(1 downto 0) := "00";
signal G1_calib_pulse         : std_logic := '0';
signal G1_trigger_prev1       : std_logic := '0';
signal G1_trigger_prev2       : std_logic := '0';
signal start_G1_delay         : std_logic := '0';
signal G1_duration_counter    : unsigned(15 downto 0) := x"0000";
signal G1_delay_counter       : unsigned(15 downto 0) := x"0000";
signal G1_DAQ_start           : std_logic := '0';
signal G1_DAQ_start_width     : unsigned(1 downto 0) := "00";

signal ADC_start_Resync       : std_logic := '0';
signal ADC_start_Resync_del   : std_logic := '0';
signal WADC_start_Resync       : std_logic := '0';
signal WTP_trigger            : std_logic := '0';
signal TP_trigger             : std_logic := '0';
signal TP_trigger_merged      : std_logic := '0';
signal TP_trigger_prev        : std_logic := '0';
signal start_TP_delay         : std_logic := '0';
signal TP_duration_counter    : unsigned(15 downto 0) := x"0000";
signal TP_delay_counter       : unsigned(15 downto 0) := x"0000";
signal TP_DAQ_start           : std_logic := '0';
signal TP_DAQ_start_width     : unsigned(1 downto 0) := "00";
signal AWG_trigger_out        : std_logic :='0';
signal AWG_trigger_del        : std_logic :='0';
signal AWG_trigger_short      : std_logic :='0';
signal AWG_trigger_width      : unsigned(3 downto 0) := (others => '0');

signal do_IDELAY_sync         : std_logic := '0';
signal start_idelay_sync_or   : std_logic := '0';
signal idelay_sync_counter    : unsigned(15 downto 0) := (others => '0');
signal CalBusy_delayed        : std_logic := '0';
signal CalBusy_or             : std_logic := '0';
signal CalBusy_delay          : unsigned(5 downto 0) := (others => '1');
signal CalBusy_delay_counter  : unsigned(5 downto 0) := (others => '0');

signal sig_monitor            : std_logic_vector(3 downto 0) := "0000";
signal FE_trigger             : std_logic := '0';
signal FE_TP_trigger          : std_logic := '0';
signal FE_veto                : std_logic := '0';

signal ReSync_out             : std_logic := '0';
signal ReSync_out_R           : std_logic := '0';
signal ReSync_out_F           : std_logic := '0';
signal ReSync_out_loc         : std_logic := '0';

signal Vdummy_in              : std_logic;
signal Vdummy_ref             : std_logic;
signal APD_Temp_in            : std_logic;
signal V2P5_in                : std_logic;
signal V2P5_ref               : std_logic;
signal V1P2_in                : std_logic;
signal V1P2_ref               : std_logic;
signal I2C_sda_LVRB           : std_logic;
signal I2C_scl_LVRB           : std_logic;
signal I2C_sda_PN_test        : std_logic;
signal I2C_scl_PN_test        : std_logic;
signal SPI_data_DAC           : std_logic;
signal SPI_strobe_DAC         : std_logic;
signal SPI_csb_DAC            : std_logic;

attribute mark_debug : string;
attribute keep : string;
attribute mark_debug of XPoint_ready : signal is "true";
attribute mark_debug of MMCM_locked  : signal is "true";

begin

inst_APD_Temp : if not USE_PED_MUX generate
  APD_Temp_in <= VFE_IO;
end generate;
inst_Mux : if USE_PED_MUX generate
  VFE_IO      <= VFE_control.ADC_Ped_Mux;
  APD_temp_in <= '0';
end generate;

--Mux_Calib_iobuf_g    : IOBUF  port map (IO => VFE_IO,     I => VFE_monitor.ADC_Ped_Mux, O=> APD_Temp_in, T => not FEAD_control.calib_mux_enabled);     -- Drive extenal MUX for ADC calibration
Mux_Calib_iobuf_1    : IOBUF  port map (IO => SEUD_in(1), I => VFE_monitor.Vref_MUX(1), O=> SEUD_loc(1), T => not FEAD_control.calib_mux_enabled); -- Drive extenal MUX for Vref measurement
Mux_Calib_iobuf_2    : IOBUF  port map (IO => SEUD_in(2), I => VFE_monitor.Vref_MUX(2), O=> SEUD_loc(2), T => not FEAD_control.calib_mux_enabled);
Mux_Calib_iobuf_3    : IOBUF  port map (IO => SEUD_in(3), I => VFE_monitor.Vref_MUX(3), O=> SEUD_loc(3), T => not FEAD_control.calib_mux_enabled);
Mux_Calib_iobuf_4    : IOBUF  port map (IO => SEUD_in(4), I => VFE_monitor.Vref_MUX(4), O=> SEUD_loc(4), T => not FEAD_control.calib_mux_enabled);
Mux_Calib_iobuf_5    : IOBUF  port map (IO => SEUD_in(5), I => VFE_monitor.Vref_MUX(5), O=> SEUD_loc(5), T => not FEAD_control.calib_mux_enabled);

inst_GENERATE_CLK_MUX: if USE_VICEPP generate
  Inst_CLK_Ctrl : VICEPP_Clk_Ctrl
  port map (
    reset               => init_XPS,
    clk                 => loc_clk_in,
    Xpoint_ready        => Xpoint_ready,
    XPS_default         => Switch(0),
--    XPS_define_in       => FEAD_control.VICEPP_Clk_config,
    XPS_define_in       => VICEPP_Clk_config_in,
    XPS_define_out      => VICEPP_Clk_config_out,
    XPS_SOUT            => XPS_Sout,
    XPS_SIN             => XPS_Sin,
    XPS_Config          => XPS_Config,
    XPS_Load            => XPS_Load
  );

  gen_init_XPS : process (loc_clk_in)
  variable counter : unsigned(7 downto 0) := (others => '0');
  begin  -- process test_counter
    if rising_edge(loc_clk_in) then
      VICEPP_Clk_config_in <= FEAD_control.VICEPP_Clk_config;
      init_XPS   <= '1';
      if counter =  x"FF" then
        init_XPS   <= '0';
      else
        counter := counter + 1;
      end if;
    end if;
  end process gen_init_XPS;

-- Change clock_domain to make Vivado happy
  XPS_to_main_clock : process (MMCM_locked, pwup_reset, system_clk)
  variable counter : unsigned(7 downto 0) := (others => '0');
  begin  -- process test_counter
    if MMCM_locked = '0' or pwup_reset='1' then
      FEAD_monitor.VICEPP_Clk_config <= (others => '0');
    elsif rising_edge(system_clk) then
      FEAD_monitor.VICEPP_Clk_config <= VICEPP_Clk_config_out;
    end if;
  end process XPS_to_main_clock;
end generate;

Inst_GENERATE_Xpoint_reset: if not USE_VICEPP generate
  Xpoint_ready   <= '1';
end generate;

Inst_CLK_DEBUG: if USE_GPIO_WITH_DBG generate
  --clk_test_OBUF : OBUFDS port map (O => GPIO(0), OB => GPIO(1), I => clk_160);
  GPIO(0) <= 'Z';
  GPIO(1) <= 'Z';
  GPIO(2) <= trig_hardware;
--  GPIO(3) <= trig_software;
  GPIO(3) <= MMCM_locked;
  GPIO(4) <= 'Z';
  GPIO(5) <= 'Z';
end generate;

Inst_GPIO_LVR: if USE_GPIO_I2C and USE_LVRB generate
--  GPIO_0_obuf   : OBUF  generic map (DRIVE => 8, SLEW => "SLOW")
--                        port map (O => GPIO(0), I => I2C_sda_LVRB);
--  GPIO_1_obuf   : OBUF  generic map (DRIVE => 8, SLEW => "SLOW")
--                        port map (O => GPIO(1), I => I2C_scl_LVRB);
--  GPIO_2_ibuf   : IBUF  port map (I => GPIO(2), O => V1P2_in);
--  GPIO_2R_ibuf  : IBUF  port map (I => GPIO(4), O => V1P2_ref);
--  GPIO_3_ibuf   : IBUF  port map (I => GPIO(3), O => V2P5_in);
--  GPIO_3R_ibuf  : IBUF  port map (I => GPIO(5), O => V2P5_ref);
  GPIO(0)                              <= I2C_sda_LVRB;
  GPIO(1)                              <= I2C_scl_LVRB;
  GPIO(2)                              <= V1P2_in;
  GPIO(4)                              <= V1P2_ref;
  GPIO(3)                              <= V2P5_in;
  GPIO(5)                              <= V2P5_ref;
end generate;

Inst_EXTRA_LVR: if USE_EXTRA_I2C and USE_LVRB generate
  TP_trigger_out                       <= I2C_SCL_LVRB;
  Calib_Mode_out                       <= I2C_SDA_LVRB;
  V2P5_in                              <= '0';
  V2P5_ref                             <= '0';
  V1P2_in                              <= '0';
  V1P2_ref                             <= '0';
  GPIO(0)                              <= I2C_SDA_PN_test;
  GPIO(1)                              <= I2C_SCL_PN_test;
  GPIO(2)                              <= not FEAD_control.CRC_reset;
--  GPIO(2)                              <= 'Z';
  GPIO(3)                              <= AWG_trigger_out;
  GPIO(4)                              <= 'Z';
  GPIO(5)                              <= 'Z';
end generate;

Inst_TP_out : if not USE_EXTRA_I2C generate
  TP_obuf    : OBUF  port map (O => TP_trigger_out, I => TP_trigger); -- Calib trigger for current injection
  Calib_mode_out                       <= '0';
end generate;

Inst_LVRB : if USE_LVRB generate
  Inst_LVRB_Ctrl : entity work.LVRB_Ctrl
  port map (
    FEAD_control                       => FEAD_control,
    VFE_control                        => VFE_control,
    LVRB_Busy                          => VFE_monitor.I2C_LVRB_Busy,
    I2C_Reg_data_LVRB                  => VFE_monitor.I2C_Reg_data_LVRB,
    I2C_Scan_Fault_LVRB_2V5            => VFE_monitor.I2C_Scan_Fault_LVRB_2V5,
    I2C_Scan_ISense_LVRB_2V5           => VFE_monitor.I2C_Scan_ISense_LVRB_2V5,
    I2C_Scan_VSense_LVRB_2V5           => VFE_monitor.I2C_Scan_VSense_LVRB_2V5,
    I2C_Scan_Fault_LVRB_1V2            => VFE_monitor.I2C_Scan_Fault_LVRB_1V2,
    I2C_Scan_ISense_LVRB_1V2           => VFE_monitor.I2C_Scan_ISense_LVRB_1V2,
    I2C_Scan_VSense_LVRB_1V2           => VFE_monitor.I2C_Scan_VSense_LVRB_1V2,
    reset                              => FEAD_reset,
    I2C_clk                            => clk_I2C,
    I2C_scl                            => I2C_scl_LVRB,
    I2C_sda                            => I2C_sda_LVRB
  );
  VFE_monitor.I2C_n_ack_LVRB           <= (others => '0');
  VFE_monitor.I2C_LVRB_error           <= '0';
end generate;

Inst_PN_test : if USE_extra_I2C generate -- We use the FEAD board for I2C reading of PN test board
  Inst_PN_Ctrl : entity work.PN_Ctrl
  port map (
    FEAD_control                       => FEAD_control,
    VFE_control                        => VFE_control,
    PN_Busy                            => VFE_monitor.I2C_PN_Busy,
    I2C_Reg_data_PN                    => VFE_monitor.I2C_Reg_data_PN,
    reset                              => FEAD_reset,
    I2C_clk                            => clk_I2C,
    I2C_scl                            => I2C_scl_PN_test,
    I2C_sda                            => I2C_sda_PN_test
  );
  VFE_monitor.I2C_n_ack_PN             <= (others => '0');
  VFE_monitor.I2C_PN_error             <= '0';
end generate;

Inst_noPN_test : if not USE_extra_I2C generate
  VFE_monitor.I2C_Reg_data_PN          <= (others => '0');
  VFE_monitor.I2C_n_ack_PN             <= (others => '0');
  VFE_monitor.I2C_PN_Busy              <= '0';
  VFE_monitor.I2C_PN_error             <= '0';
end generate;

inst_noLVR: if not USE_LVRB generate
  VFE_monitor.I2C_LVRB_Busy            <= '0';
  VFE_monitor.I2C_Reg_data_LVRB        <= (others => '0');
  VFE_monitor.I2C_Scan_Fault_LVRB_2V5  <= (others => '0');
  VFE_monitor.I2C_Scan_ISense_LVRB_2V5 <= (others => '0');
  VFE_monitor.I2C_Scan_VSense_LVRB_2V5 <= (others => '0');
  VFE_monitor.I2C_Scan_Fault_LVRB_1V2  <= (others => '0');
  VFE_monitor.I2C_Scan_ISense_LVRB_1V2 <= (others => '0');
  VFE_monitor.I2C_Scan_VSense_LVRB_1V2 <= (others => '0');
  VFE_monitor.I2C_n_ack_LVRB           <= (others => '0');
  VFE_monitor.I2C_LVRB_error           <= '0';
  V2P5_in                              <= '0';
  V2P5_ref                             <= '0';
  V1P2_in                              <= '0';
  V1P2_ref                             <= '0';
end generate;

inst_DAC: if USE_GPIO_WITH_DAC generate
--  GPIO_0_obuf   : OBUF  generic map (DRIVE => 8, SLEW => "SLOW")
--                        port map (O => GPIO(0), I => SPI_data_DAC);
--  GPIO_1_obuf   : OBUF  generic map (DRIVE => 8, SLEW => "SLOW")
--                        port map (O => GPIO(1), I => SPI_strobe_DAC);
--  GPIO_2_obuf   : OBUF  generic map (DRIVE => 8, SLEW => "SLOW")
--                        port map (O => GPIO(2), I => SPI_csb_DAC);
  GPIO(0) <= SPI_data_DAC;
  GPIO(1) <= SPI_strobe_DAC;
  GPIO(2) <= SPI_csb_DAC;
  GPIO(3) <= 'Z';
  GPIO(4) <= 'Z';
  GPIO(5) <= 'Z';

  Inst_DAC_Ctrl : DAC_Ctrl
  port map (
    SPI_Access_DAC      => VFE_control.SPI_Access_DAC,
    SPI_DAC_data        => VFE_control.SPI_DAC_data,
    SPI_DAC_number      => VFE_control.SPI_DAC_number,
    SPI_DAC_action      => VFE_control.SPI_DAC_action,
    reset               => FEAD_reset,
    SPI_clk             => clk_I2C,
    SPI_csb             => SPI_csb_DAC,
    SPI_data            => SPI_data_DAC,
    SPI_strobe          => SPI_strobe_DAC
  );
end generate;
--  Calib_ADC       <= FEAD_monitor.ADC_calib_mode;
--  I2C_sRst        <= '0';
--  I2C_SM_aRstb    <= '1';
  Test_mode_out                    <= VFE_Monitor.ADC_test_mode;
  CLK_select                       <= not FEAD_Monitor.clock_select;
  SEUA_out                         <= (others => '0');
  SEUD_out                         <= (others => '0');
  PllLock_out                      <= (others => '0');
  CalBusy_out                      <= (others => '0');
  OVF_out                          <= (others => '0');

--  reset                            <= pwup_reset or FEAD_Monitor.reset;
  VFE_reset                        <= Pwup_reset or VFE_monitor.VFE_reset;
  Pwup_reset_out                   <= not VFE_reset;
  CLK_reset                        <= not Xpoint_ready;
  FEAD_reset                       <= Pwup_reset or FEAD_Monitor.reset;
  
  gen_pwup_reset : process (system_clk)
  variable counter : unsigned(15 downto 0) := (others => '0');
  begin  -- process test_counter
--    if system_clock_locked = '0' then
--      counter    := (others => '0');
    if rising_edge(system_clk) then
      pwup_reset <= '1';
      if counter =  x"FFFF" then
        pwup_reset <= '0';
      else
        counter := counter + 1;
      end if;
    end if;
  end process gen_pwup_reset;

--  DCIRESET_inst : DCIRESET
--  port map (
--    LOCKED => FEAD_monitor.DCI_locked, -- 1-bit output: LOCK status output
--    RST    => pwup_reset               -- 1-bit input: Active-high asynchronous reset input
--  );
  FEAD_monitor.DCI_locked         <= '1';

-- FEAD monitoring :
  FEAD_Monitor.firmware_ver        <= x"24062003";
  FEAD_Monitor.board_SN            <= ADDR;
--  FEAD_Monitor.clock_select        <= FEAD_Control.clock_select;
  FEAD_Monitor.clock_select        <= FEAD_Control.clock_select;
  FEAD_Monitor.seq_clock_phase     <= FEAD_Control.seq_clock_phase;
  FEAD_Monitor.IO_clock_phase      <= FEAD_Control.IO_clock_phase;
  FEAD_Monitor.reg_clock_phase     <= FEAD_Control.reg_clock_phase;
  FEAD_Monitor.mem_clock_phase     <= FEAD_Control.mem_clock_phase;
  FEAD_Monitor.resync_clock_phase  <= FEAD_Control.resync_clock_phase;
  FEAD_Monitor.clock_reset         <= FEAD_Control.clock_reset;
  FEAD_Monitor.reset               <= FEAD_Control.reset;
  FEAD_Monitor.LED_on              <= FEAD_Control.LED_on;
  FEAD_Monitor.FIFO_mode           <= FEAD_Control.FIFO_mode;
  FEAD_Monitor.trig_loop           <= FEAD_Control.trig_loop;
  FEAD_Monitor.trig_self           <= FEAD_Control.trig_self;
  FEAD_Monitor.trig_self_mode      <= FEAD_Control.trig_self_mode;
  FEAD_Monitor.trig_self_thres     <= FEAD_Control.trig_self_thres;
  FEAD_Monitor.trig_self_mask      <= FEAD_Control.trig_self_mask;
  FEAD_Monitor.trigger_SW_delay    <= FEAD_Control.trigger_SW_delay;
  FEAD_Monitor.trigger_HW_delay    <= FEAD_Control.trigger_HW_delay;
  FEAD_Monitor.TP_mode             <= FEAD_Control.TP_mode;
  FEAD_Monitor.TP_duration         <= FEAD_Control.TP_duration;
  FEAD_Monitor.TP_delay            <= FEAD_Control.TP_delay;
  FEAD_Monitor.TP_dummyb           <= FEAD_Control.TP_dummyb;
  FEAD_Monitor.start_IDELAY_sync   <= FEAD_Control.start_IDELAY_sync;
  FEAD_Monitor.clock_locked        <= MMCM_locked;
  FEAD_Monitor.calib_pulse_enabled <= FEAD_Control.calib_pulse_enabled;
  FEAD_Monitor.calib_mux_enabled   <= FEAD_Control.calib_mux_enabled;
  FEAD_Monitor.TE_pos              <= FEAD_Control.TE_pos;
  FEAD_Monitor.TE_command          <= FEAD_Control.TE_command;
  FEAD_Monitor.TE_enable           <= FEAD_Control.TE_enable;
  FEAD_Monitor.I2C_low             <= FEAD_Control.I2C_low;

-- VFE monitoring
  VFE_monitor.I2C_access_LVRB      <= VFE_control.I2C_access_LVRB;
  VFE_monitor.I2C_access_VFE       <= VFE_control.I2C_access_VFE;
  VFE_monitor.I2C_bulky_DTU        <= VFE_control.I2C_bulky_DTU;
  VFE_monitor.I2C_lpGBT_mode       <= VFE_control.I2C_lpGBT_mode;
  VFE_monitor.I2C_bulk_length      <= VFE_control.I2C_bulk_length;
  VFE_monitor.I2C_bulk_data_ref    <= VFE_control.I2C_bulk_data_ref;
  VFE_monitor.I2C_R_Wb             <= VFE_control.I2C_R_Wb;
  VFE_monitor.I2C_Reset            <= VFE_control.I2C_Reset;
  VFE_monitor.I2C_Reg_number       <= VFE_Control.I2C_Reg_number;  
  VFE_monitor.I2C_Device_number    <= VFE_Control.I2C_Device_number;
  VFE_Monitor.I2C_long_transfer    <= VFE_control.I2C_long_transfer;
  VFE_Monitor.DTU_auto_sync        <= VFE_control.DTU_auto_sync;
  VFE_Monitor.ADC_Resync_idle      <= VFE_control.ADC_Resync_idle;
  VFE_Monitor.ADC_Resync_data      <= VFE_control.ADC_Resync_data;
  VFE_Monitor.DTU_Sync_pattern     <= VFE_control.DTU_Sync_pattern;
  VFE_Monitor.ADC_test_mode        <= VFE_control.ADC_test_mode;
  VFE_Monitor.ADC_calib_mode       <= VFE_control.ADC_calib_mode;
  VFE_Monitor.ADC_MEM_mode         <= VFE_control.ADC_MEM_mode;
  VFE_Monitor.ADC_Ped_mux          <= VFE_control.ADC_Ped_mux;
  VFE_Monitor.Vref_Mux             <= VFE_control.Vref_Mux;
--  VFE_monitor.I2C_ack_spy          <= I2C_ack_spy_CATIA or I2C_ack_spy_DTU;
--  VFE_monitor.I2C_ack_spy          <= I2C_ack_spy_CATIA;
--  VFE_monitor.I2C_ack_spy          <= I2C_ack_spy_DTU;
--  VFE_monitor.I2C_ack_spy          <= I2C_ack_spy_CATIA   when VFE_control.I2C_buggy_DTU   = '0' else
--                                      I2C_ack_spy_DTU;
  VFE_monitor.eLink_active         <= VFE_control.eLink_active;
  VFE_monitor.VFE_reset            <= VFE_control.VFE_reset;
  --VFE_monitor.I2C_n_ack_LVRB       <= x"FF";
  VFE_monitor.SPI_access_DAC       <= VFE_control.SPI_access_DAC;
  VFE_monitor.SPI_DAC_data         <= VFE_control.SPI_DAC_data;
  VFE_monitor.SPI_DAC_number       <= VFE_control.SPI_DAC_number;
  VFE_monitor.SPI_DAC_action       <= VFE_control.SPI_DAC_action;

  inst_G10_calib_pulse: if CATIA_TEST_BOARD generate
    G10_Calib_obuf    : OBUFT  port map (O => SEUA_in(1),    I => G10_calib_pulse, T => not  FEAD_control.calib_pulse_enabled); -- Calib pulse for G10 on calibration board
  end generate;
  inst_SEU_A: if not CATIA_TEST_BOARD generate
    G10_Calib_obuf    : IBUF   port map (I => SEUA_in(1),    O => SEUA_loc(1));
  end generate;
  --G10_Calib_iobuf    : IOBUF  port map (IO => SEUA_in(1),    I => G10_calib_pulse, O=> SEUA_loc(1), T => FEAD_control.calib_pulse_enabled); -- Calib pulse for G10 on calibration board
  G1_Calib_iobuf     : IOBUF  port map (IO => PLLlock_in(2), I => G1_calib_pulse,  O=> VFE_Monitor.ADC_PLL_lock(2), T => FEAD_control.calib_pulse_enabled); -- Calib pulse for G10 on calibration board
  VFE_Monitor.ADC_PLL_lock(1)      <= PllLock_in(1);
  VFE_Monitor.ADC_PLL_lock(3)      <= PllLock_in(3);
  VFE_Monitor.ADC_PLL_lock(4)      <= PllLock_in(4);
  VFE_Monitor.ADC_PLL_lock(5)      <= PllLock_in(5);

-- Trick for VFE board with 1 LiTE-DTU. Only Cal_busy(4) is connected to FEAD
-- Recopy this bit on all channels to be able to use LiTE-DTU in test mode or in normal mode.
  CalBusy_or    <=  CalBusy_in(1) or CalBusy_in(2) or CalBusy_in(3) or CalBusy_in(4) or CalBusy_in(5);
  delay_CalBusy: process(CalBusy_or, sequence_clk)
  begin
  if CalBusy_or='0' then
    CalBusy_delayed         <= '0';
    CalBusy_delay_counter   <= (others =>'0');
  elsif rising_edge(sequence_clk) then
    if CalBusy_delay_counter < CalBusy_delay then
      CalBusy_delay_counter <= CalBusy_delay_counter+1;
    else
      CalBusy_delayed <= '1';
    end if;
  end if;
  end process delay_CalBusy;

  start_idelay_sync_or <= FEAD_monitor.start_idelay_sync(1) or FEAD_monitor.start_idelay_sync(2) or FEAD_monitor.start_idelay_sync(3) or
                          FEAD_monitor.start_idelay_sync(4) or FEAD_monitor.start_idelay_sync(5); 
  gen_do_idelay_sync : process(start_idelay_sync_or, sequence_clk, FEAD_Control.trigger)
  begin
    if start_idelay_sync_or = '0' then
      do_IDELAY_sync        <= '0';
      idelay_sync_counter   <= (others=> '0');
    elsif rising_edge(sequence_clk) then
      if idelay_sync_counter < x"5000" then
        do_IDELAY_sync      <= '1';
        idelay_sync_counter <= idelay_sync_counter + 1;
      else
        do_IDELAY_sync      <= '0';
      end if;
    end if;
  end process gen_do_idelay_sync;


  VFE_Monitor.ADC_cal_busy(1)      <= CalBusy_delayed;
  VFE_Monitor.ADC_cal_busy(2)      <= CalBusy_delayed; 
  VFE_Monitor.ADC_cal_busy(3)      <= CalBusy_delayed; 
  VFE_Monitor.ADC_cal_busy(4)      <= CalBusy_delayed; 
  VFE_Monitor.ADC_cal_busy(5)      <= CalBusy_delayed; 
--  VFE_Monitor.ADC_cal_busy(1)      <= CalBusy_in(1);
--  VFE_Monitor.ADC_cal_busy(2)      <= CalBusy_in(2); 
--  VFE_Monitor.ADC_cal_busy(3)      <= CalBusy_in(3); 
--  VFE_Monitor.ADC_cal_busy(4)      <= CalBusy_in(4); 
--  VFE_Monitor.ADC_cal_busy(5)      <= CalBusy_in(5); 
  FEAD_Monitor.debug1(0)(15 downto 0) <= std_logic_vector(vfe_monitor.nsample_orbit(1)); 
  FEAD_Monitor.debug1(1)(15 downto 0) <= std_logic_vector(vfe_monitor.nsample_orbit(2)); 
  FEAD_Monitor.debug1(2)(15 downto 0) <= std_logic_vector(vfe_monitor.nsample_orbit(3)); 
  FEAD_Monitor.debug1(3)(15 downto 0) <= std_logic_vector(vfe_monitor.nsample_orbit(4)); 
  FEAD_Monitor.debug1(4)(15 downto 0) <= std_logic_vector(vfe_monitor.nsample_orbit(5)); 


  --GPIO          <= sig_monitor;
--  GPIO_0_obuf   : OBUF  port map (O => GPIO_O(0), I => sig_monitor(0)); -- GPIO0 on schematics
--  GPIO_1_obuf   : OBUF  port map (O => GPIO_O(1), I => sig_monitor(3)); -- GPIO2 on schematics
-- With LVRB_noMB_Isense, GPIO2 and GPIO3 are connected to XADC
  --GPIO_2_obuf   : OBUF  port map (O => GPIO_O(2), I => sig_monitor(1)); -- GPIO1 on schematics
  --GPIO_3_obuf   : OBUF  port map (O => GPIO_O(3), I => sig_monitor(2)); -- GPIO3 on schematics

-- Signals coming/going from/to fake FE board : 
-- Ctrl_1 and Ctrl_2 are 2.5V compatible
-- Ctrl_0, 3, 4 and 5 are 1.2V compatible
  ctrl_out(4) <= '0' when trig_local='1' and FE_veto='0' else 'Z';
  ctrl_out(3) <= '0' when DAQ_busy='1' else 'Z';
  --CTRL_4_obuf   : OBUF  generic map (DRIVE => 8, IOSTANDARD => "LVCMOS12", SLEW => "FAST")
  --                      port map (O => Ctrl_out(4), I => trig_local_outb); -- trig_local active low
  --CTRL_3_obuf   : OBUF  generic map (DRIVE => 8, IOSTANDARD => "LVCMOS12", SLEW => "SLOW")
  --                      port map (O => Ctrl_out(3), I => DAQ_busy_b); -- DAQ_busy_b active low
  CTRL_2_obuf   : OBUF  port map (O => Ctrl_out(2), I => '0');
  CTRL_1_obuf   : OBUF  port map (O => Ctrl_out(1), I => '0');

  inst_From_FE : if USE_VICEPP generate
    CTRL_1_ibuf   : IBUF  port map (I => Ctrl_in(1), O => FE_trigger);
    CTRL_2_ibuf   : IBUF  port map (I => Ctrl_in(2), O => FE_veto);
    CTRL_3_ibuf   : IBUF  port map (I => Ctrl_in(3), O => FE_TP_trigger);
    Vdummy_in     <= '0';
    Vdummy_ref    <= '0';
  end generate;
  inst_Vdummy : if not USE_VICEPP generate
    CTRL_1_obuf   : OBUF  port map (I => BC0_out, O => Ctrl_in(1));
    Vdummy_in     <= Ctrl_in(2);
    Vdummy_ref    <= Ctrl_ref(2);
  end generate;
  
  ReSync_out_obuf : OBUFTDS port map (O => ReSync_out_P, OB => ReSync_out_N, I => ReSync_out, T => FEAD_monitor.I2C_low);
  
  sig_monitor(0)       <= sequence_clk;
  sig_monitor(2)       <= FEAD_control.delay_ADC_number(1);
  sig_monitor(3)       <= FEAD_control.IO_reset;
--  trig_signal          <= calib_DAQ_start or (trig_software and FEAD_Monitor.trig_loop) or (trig_hardware and not FEAD_Monitor.trig_loop);
  trig_signal          <= G10_DAQ_start or G1_DAQ_start or TP_DAQ_start or trig_software or trig_hardware;
  trig_in              <= (FE_trigger or (trig_local and FEAD_monitor.trig_loop)) and (not FE_veto);

--  calib_trigger_merged <= FEAD_Control.calib_trigger or (FEAD_Monitor.calib_mode and FE_trigger(4));
  TP_trigger_merged    <= FEAD_Control.TP_trigger or FE_TP_trigger;
  
-- Hold ADC_start_Resync signal if we are near BC0. Hold TP trigger as well
  hold_ReSync : process(MMCM_locked, pwup_reset, sequence_clk, trig_in)
  begin
    if MMCM_locked='0' or pwup_reset = '1' then
      ADC_start_Resync      <= '0';
      WADC_start_ReSync     <= '0';
    elsif rising_edge(sequence_clk) then
      ADC_start_ReSync_del  <= VFE_control.ADC_Start_ReSync;
      if VFE_control.ADC_start_ReSync='1' and ADC_Start_ReSync_del='0' then
        WADC_Start_ReSync   <= '1';
      end if;
      if WADC_Start_Resync = '1' then
--        if clk_counter>1 and clk_counter<14174 and VFE_monitor.ReSync_busy='0' then
-- Send ReSync command at fixed position in the orbit : let's say pos 96 (160 MHz) = BC24
        if clk_counter=96 and VFE_monitor.ReSync_busy='0' then
          ADC_start_ReSync  <= '1';
          WADC_Start_ReSync <= '0';
        end if;
      else
        ADC_start_ReSync      <= '0';
      end if;
    end if;
  end process hold_resync;

  gen_AWG_trigger : process(MMCM_locked, pwup_reset, sequence_clk)
  begin
    if MMCM_locked = '0' or pwup_reset='1' then
      AWG_trigger_out       <= '0';
      AWG_trigger_width     <= (others => '0');
    elsif rising_edge(sequence_clk) then
      AWG_trigger_del       <= FEAD_control.AWG_trigger;
      if FEAD_control.AWG_trigger='1' and AWG_trigger_del='0' then
        AWG_trigger_out     <= '1';
        AWG_trigger_width   <= (others => '0');
      end if;
      if AWG_trigger_out = '1' then
        if AWG_trigger_width /= x"F" then
          AWG_trigger_width <= AWG_trigger_width+'1';
        else
          AWG_trigger_out   <= '0';
        end if;
      end if;        
    end if;
  end process gen_AWG_trigger;

  gen_calib_trigger : process(MMCM_locked, pwup_reset, sequence_clk, TP_trigger_merged)
  begin
    if MMCM_locked = '0' or pwup_reset='1' then
      WTP_trigger           <= '0';
      TP_trigger            <= '0';
      start_TP_delay        <= '0';
      TP_DAQ_start          <= '0';
      TP_duration_counter   <= x"0000";
      TP_delay_counter      <= x"0000";
      TP_DAQ_start_width    <= "00";
    elsif rising_edge(sequence_clk) then
      TP_trigger_prev       <= TP_trigger_merged;
      if TP_trigger_merged='1' and TP_trigger_prev='0' then
        WTP_trigger          <= '1';
        TP_DAQ_start         <= '0';
        TP_duration_counter  <= x"0000";
        TP_delay_counter     <= x"0000";
        TP_DAQ_start_width   <= "00";
      end if;
      if WTP_trigger = '1' then
--        if clk_counter>1 and clk_counter<14174 and VFE_monitor.ReSync_busy='0' then  -- Don't send TP trigger around BC0 
        if clk_counter=96 and VFE_monitor.ReSync_busy='0' then  -- Don't send TP trigger around BC0 
          TP_trigger         <= '1';
          start_TP_delay     <= '1';
          WTP_trigger        <= '0';
        end if;
      end if;
      if TP_trigger = '1' then
        if TP_duration_counter < unsigned(FEAD_monitor.TP_duration) then        
          TP_duration_counter <= TP_duration_counter+1;
        else
          TP_trigger         <= '0';
        end if;
      end if;
      if start_TP_delay = '1' then
        if TP_delay_counter < unsigned(FEAD_monitor.TP_delay) then
          TP_delay_counter    <= TP_delay_counter+1;
        else
          TP_DAQ_start        <= '1';
          start_TP_delay      <= '0';
        end if;
      end if;
      if TP_DAQ_start = '1' then
        if TP_DAQ_start_width < "11" then
          TP_DAQ_start_width  <= TP_DAQ_start_width+1;
        else
          TP_DAQ_start        <= '0';
        end if;
      end if;
    end if;
  end process gen_calib_trigger;

  gen_G10_calib_pulse : process(MMCM_locked, pwup_reset, sequence_clk, FEAD_control.G10_calib_trigger)
  begin
    if MMCM_locked = '0' or pwup_reset='1' then
      G10_calib_pulse          <= '1';
      start_G10_delay          <= '0';
      G10_DAQ_start            <= '0';
      G10_duration_counter     <= x"0000";
      G10_delay_counter        <= x"0000";
      G10_DAQ_start_width      <= "00";
    elsif rising_edge(sequence_clk) then
      G10_trigger_prev1        <= FEAD_control.G10_calib_trigger;
      G10_trigger_prev2        <= G10_trigger_prev1;
      if G10_trigger_prev1='1' and G10_trigger_prev2='0' then
        G10_calib_pulse        <= '0';
        start_G10_delay        <= '1';
        G10_DAQ_start          <= '0';
        G10_duration_counter   <= x"0000";
        G10_delay_counter      <= x"0000";
        G10_DAQ_start_width    <= "00";
      end if;
      if G10_calib_pulse = '0' then
        if G10_duration_counter < unsigned(FEAD_monitor.TP_duration) then        
          G10_duration_counter <= G10_duration_counter+1;
        else
          G10_calib_pulse      <= '1';
        end if;
      end if;
      if start_G10_delay = '1' then
        if G10_delay_counter < unsigned(FEAD_monitor.TP_delay) then
          G10_delay_counter    <= G10_delay_counter+1;
        else
          G10_DAQ_start        <= '1';
          start_G10_delay      <= '0';
        end if;
      end if;
      if G10_DAQ_start = '1' then
        if G10_DAQ_start_width < "11" then
          G10_DAQ_start_width  <= G10_DAQ_start_width+1;
        else
          G10_DAQ_start        <= '0';
        end if;
      end if;
    end if;
  end process gen_G10_calib_pulse;

  gen_G1_calib_pulse : process(MMCM_locked, pwup_reset, sequence_clk, FEAD_control.G1_calib_trigger)
  begin
    if MMCM_locked = '0' or pwup_reset='1' then
      G1_calib_pulse           <= '1';
      start_G1_delay           <= '0';
      G1_DAQ_start             <= '0';
      G1_duration_counter      <= x"0000";
      G1_delay_counter         <= x"0000";
      G1_DAQ_start_width       <= "00";
    elsif rising_edge(sequence_clk) then
      G1_trigger_prev1         <= FEAD_control.G1_calib_trigger;
      G1_trigger_prev2         <= G1_trigger_prev1;
      if G1_trigger_prev1='1' and G1_trigger_prev2='0' then
        G1_calib_pulse         <= '0';
        start_G1_delay         <= '1';
        G1_DAQ_start           <= '0';
        G1_duration_counter    <= x"0000";
        G1_delay_counter       <= x"0000";
        G1_DAQ_start_width     <= "00";
      end if;
      if G1_calib_pulse = '1' then
        if G1_duration_counter < unsigned(FEAD_monitor.TP_duration) then        
          G1_duration_counter  <= G1_duration_counter+1;
        else
          G1_calib_pulse       <= '1';
        end if;
      end if;
      if start_G1_delay = '1' then
        if G1_delay_counter < unsigned(FEAD_monitor.TP_delay) then
          G1_delay_counter     <= G1_delay_counter+1;
        else
          G1_DAQ_start         <= '1';
          start_G1_delay       <= '0';
        end if;
      end if;
      if G1_DAQ_start = '1' then
        if G1_DAQ_start_width < "11" then
          G1_DAQ_start_width   <= G1_DAQ_start_width+1;
        else
          G1_DAQ_start         <= '0';
        end if;
      end if;
    end if;
  end process gen_G1_calib_pulse;

  gen_trig_out : process(MMCM_locked, pwup_reset, sequence_clk, FEAD_Control.trigger)
  begin
    if MMCM_locked = '0' or pwup_reset='1' then
      delay_out_counter     <= x"0000";
      start_out_counter     <= '0';
      trig_software         <= '0';
      trig_sw_width         <= (others => '0');
    elsif rising_edge(sequence_clk) then
      trigger_prev1         <= FEAD_Control.trigger;
      trigger_prev2         <= trigger_prev1;
      if trigger_prev1='1' and trigger_prev2='0' then
        start_out_counter   <= '1';
        delay_out_counter   <= x"0000";
      end if;
      if start_out_counter = '1' then
        if delay_out_counter < unsigned(FEAD_control.trigger_SW_delay) then
          delay_out_counter <= delay_out_counter+1;
        else
          trig_software     <= '1';
          trig_sw_width     <= (others => '0');
          start_out_counter <= '0';
        end if;
      end if;
      if trig_software = '1' then
        if trig_sw_width < x"4" then
          trig_sw_width     <= trig_sw_width+1;
        else
          trig_software     <= '0';
        end if;
      end if;
    end if;
  end process gen_trig_out;

  gen_BC0 : process(MMCM_locked, pwup_reset, sequence_clk)
  begin
    if MMCM_locked='0'or pwup_reset='1' then
      clk_counter           <= (others => '0');
      orbit_counter         <= (others => '0');
      BC0                   <= '0';
      Test_enable           <= '0';
    elsif rising_edge(sequence_clk) then
      if clk_counter > 20 then
        BC0                   <= '0';
      end if;
      if clk_counter < 14255 then -- LHC orbit = 3564 clock at 40 MHz
        clk_counter         <= clk_counter+1;
      else
        BC0                 <= '1';
        clk_counter         <= (others => '0');
        orbit_counter       <= orbit_counter+1;
      end if; 
      if clk_counter > FEAD_monitor.TE_pos+20 then   -- 125 ns wide TE
        Test_enable         <= '0';
      end if;
      if clk_counter = FEAD_monitor.TE_pos and orbit_counter(0) = '1' and FEAD_monitor.TE_enable = '1' then -- TE every other orbit
        Test_enable         <= '1';
      end if;
    end if;
  end process gen_BC0;
  
  gen_trig_in : process(MMCM_locked, pwup_reset, sequence_clk, trig_in)
  begin
    if MMCM_locked='0' or pwup_reset = '1' then
      delay_in_counter      <= x"0000";
      start_in_counter      <= '0';
      trig_hardware         <= '0';
      trig_in_Prev1         <= '0';
      trig_in_Prev2         <= '0';
      trig_hw_width         <= (others => '0');
    elsif rising_edge(sequence_clk) then
      trig_in_prev1         <= trig_in;
      trig_in_prev2         <= trig_in_prev1;
      if trig_in_prev1='1' and trig_in_prev2='0' then
        start_in_counter    <= '1';
        delay_in_counter    <= x"0000";
      end if;
      if start_in_counter = '1' then
        if delay_in_counter < unsigned(FEAD_monitor.trigger_HW_delay) then
          delay_in_counter    <= delay_in_counter+1;
        else
          trig_hardware       <= '1';
          trig_hw_width       <= (others => '0');
          start_in_counter    <= '0';
        end if;
      end if;
      if trig_hardware = '1' then
        if trig_hw_width < x"4" then
          trig_hw_width         <= trig_hw_width+1;
        else
          trig_hardware       <= '0';
        end if;
      end if;
    end if;
  end process gen_trig_in;

  -------------------------------------------------------------------------------
  -- Clocking
---- Clk port from local oscillator
  --clk_IBUFGDS : unisim.vcomponents.IBUFGDS generic map(DIFF_TERM => TRUE) port map (O => CLK_160_in, I  => CLK_160_P, IB => CLK_160_N);
  inst_GENERATE_IBUFGDS_VIVEPP: if USE_VICEPP generate
    clk_IBUFGDS    : unisim.vcomponents.IBUFGDS generic map(DIFF_TERM => FALSE) port map (O => loc_clk_in, I  => CLK_160_P, IB => CLK_160_N);
    HP_clk_IBUFGDS : unisim.vcomponents.IBUFGDS generic map(DIFF_TERM => FALSE) port map (O => HP_CLK_in, I  => HP_CLK_160_P, IB => HP_CLK_160_N);
  end generate;
  inst_GENERATE_IBUFGDS_FEAD: if not USE_VICEPP generate
    clk_IBUFGDS :    unisim.vcomponents.IBUFGDS generic map(DIFF_TERM => TRUE) port map (O => loc_clk_in, I  => CLK_160_P, IB => CLK_160_N);
    HP_CLK_in <= loc_clk_in;
  end generate;

  Inst_clk_delay_ctrl : clk_generator_delay_ctrl
  port map
  (
    clk_160_in      => HP_clk_in,
    clk_delay_ctrl  => clk_delay_ctrl,
    reset           => CLK_reset,
    locked          => delay_clock_locked
  );
  clk_Gb_eth        <= Clk_delay_ctrl;

  Inst_clk_generator_iserdes : clk_generator_iserdes
  port map
  (
    clk_160_in      => HP_Clk_in,
    clk_20          => HP_clk_20,
    clk_40          => HP_Clk_40,
    clk_160         => HP_Clk_160,
    clk_640         => HP_Clk_640,
    reset           => CLK_reset,
    locked          => iserdes_clock_locked
  );

  MMCM_locked       <= iserdes_clock_locked and delay_clock_locked;

--clock for ADC and VFE capture
  Clk_IPbus         <= HP_CLK_20;
  system_clk        <= HP_CLK_160;
  sequence_clk      <= HP_CLK_160;
  shift_reg_clk     <= HP_CLK_160;
  memory_clk        <= HP_CLK_160;
  resync_clk        <= HP_CLK_160 when FEAD_monitor.resync_clock_phase="000" else not HP_CLK_160;
  --resync_clk        <= HP_CLK_160 xor FEAD_monitor.resync_clock_phase(0);
  clk_I2C           <= HP_CLK_160;

--  LED(0) <= reset and FEAD_monitor.LED_on;
--  LED(0) <= reset;
--  LED(2) <= FEAD_monitor.delay_locked;
--  LED(0) <= trig_local;

  LED(1) <= CalBusy_delayed;
  LED(0) <= do_idelay_sync;
  LED(3) <= VFE_monitor.I2C_PN_Busy;
  --LED(1) <= MMCM_locked;
  --LED(3) <= Pwup_reset;

--            VFE_monitor.ADC_test_mode or CalBusy_delayed;
--  LED(1) <= FEAD_monitor.Link_idelay_sync(1) or VFE_monitor.LVRB_busy;
--  LED(2) <= FEAD_monitor.Link_byte_sync(1) or VFE_monitor.CATIA_busy;
--  LED(3) <= FEAD_monitor.Link_bit_sync(1) or VFE_monitor.DTU_busy;
--  LED(1) <= FEAD_monitor.Link_idelay_sync(1);
--  LED(2) <= FEAD_monitor.Link_byte_sync(1);
--  LED(3) <= FEAD_monitor.Link_bit_sync(1);
--  LED(3) <= VFE_monitor.I2C_n_ack_DTU(0);

  -------------------------------------------------------------------------------
  -- VFE and FE interfaces
  -------------------------------------------------------------------------------  
  Inst_FE_VFE_IO : if not FOR_SEU generate
    Inst_VFE_Input : entity work.VFE_Input
    port map (
      IO_reset            => FEAD_control.IO_reset,
      delay_reset         => FEAD_control.delay_reset,
      bitslip_ADC_number  => FEAD_control.bitslip_ADC_number,
      byteslip_ADC_number => FEAD_control.byteslip_ADC_number,
      delay_ADC_number    => FEAD_control.delay_ADC_number,
      delay_tap_dir       => FEAD_control.delay_tap_dir,

      delay_locked        => FEAD_monitor.delay_locked,
      VFE_synchronized    => FEAD_monitor.VFE_synchronized,
      Link_idelay_sync    => FEAD_monitor.Link_idelay_sync,
      Link_byte_sync      => FEAD_monitor.Link_byte_sync,
      Link_bit_sync       => FEAD_monitor.Link_bit_sync,
      Link_idelay_pos     => FEAD_monitor.Link_idelay_pos,
      Sync_duration       => FEAD_monitor.Sync_duration,
      Link_error_sync     => FEAD_monitor.Link_error_sync,
      do_IDELAY_sync      => do_IDELAY_sync,

      ADC_cal_busy        => VFE_monitor.ADC_cal_busy,
      eLink_active        => VFE_monitor.eLink_active,
      DTU_auto_sync       => VFE_monitor.DTU_auto_sync,
      ADC_MEM_mode        => VFE_monitor.ADC_MEM_mode,
      ADC_test_mode       => VFE_monitor.ADC_test_mode,
      DTU_Sync_pattern    => VFE_monitor.DTU_Sync_pattern,

      MMCM_locked         => MMCM_locked,
      clk_delay_ctrl      => Clk_delay_ctrl,
      Clk_160             => HP_Clk_160,
      Clk_640             => HP_Clk_640,

      ibuf_disable        => FEAD_monitor.I2C_low,
      ch1_in_P            => ch1_in_P,
      ch1_in_N            => ch1_in_N,
      ch2_in_P            => ch2_in_P,
      ch2_in_N            => ch2_in_N,
      ch3_in_P            => ch3_in_P,
      ch3_in_N            => ch3_in_N,
      ch4_in_P            => ch4_in_P,
      ch4_in_N            => ch4_in_N,
      ch5_in_P            => ch5_in_P,
      ch5_in_N            => ch5_in_N,
      ch1_captured_stream => captured_stream(1),
      ch2_captured_stream => captured_stream(2),
      ch3_captured_stream => captured_stream(3),
      ch4_captured_stream => captured_stream(4),
      ch5_captured_stream => captured_stream(5)
    );
  
    Inst_FE_Output : entity work.FE_Output
    port map (
      synchronized        => FEAD_monitor.FE_synchronized,
      clk_160             => HP_Clk_160,
      clk_640             => HP_Clk_640,
      reset               => FEAD_reset,
      ch1_out_P           => ch1_out_P,
      ch1_out_N           => ch1_out_N,
      ch2_out_P           => ch2_out_P,
      ch2_out_N           => ch2_out_N,
      ch3_out_P           => ch3_out_P,
      ch3_out_N           => ch3_out_N,
      ch4_out_P           => ch4_out_P,
      ch4_out_N           => ch4_out_N,
      ch5_out_P           => ch5_out_P,
      ch5_out_N           => ch5_out_N,
      ch1_output_data     => output_stream(1),
      ch2_output_data     => output_stream(2),
      ch3_output_data     => output_stream(3),
      ch4_output_data     => output_stream(4),
      ch5_output_data     => output_stream(5)
    );
  end generate;

  Inst_VFE_Ctrl : entity work.VFE_Ctrl
  port map (
    I2C_access          => VFE_control.I2C_Access_VFE,
    I2C_R_Wb            => VFE_control.I2C_R_Wb,
    I2C_Device_number   => VFE_control.I2C_Device_number,
    I2C_Reg_number      => VFE_control.I2C_Reg_number,
    I2C_long_transfer   => VFE_control.I2C_long_transfer,
    I2C_Bulky           => VFE_control.I2C_Bulky,
    I2C_Bulk_length     => VFE_control.I2C_Bulk_length,
    I2C_Bulk_data_in    => VFE_control.I2C_Bulk_data,
    I2C_lpGBT_mode      => VFE_control.I2C_lpGBT_mode,

    I2C_busy_out        => VFE_monitor.I2C_VFE_busy,
    I2C_error           => VFE_monitor.I2C_VFE_error,
    I2C_reg_data_out    => VFE_monitor.I2C_Reg_data_VFE,
    I2C_n_ack           => VFE_monitor.I2C_n_ack_VFE,
    I2C_Bulk_data_out   => VFE_monitor.I2C_Bulk_data,

    ADC_start_Resync    => ADC_start_Resync,
    ADC_invert_Resync   => VFE_control.ADC_invert_Resync,
    ADC_ReSync_idle     => VFE_control.ADC_ReSync_idle,
    ADC_ReSync_data     => VFE_control.ADC_ReSync_data,
    ReSync_busy_out     => VFE_monitor.ReSync_busy,

    reset               => FEAD_reset,
    BC0                 => BC0,
    BC0_out             => BC0_out,
    TE                  => Test_enable,
    TE_command          => FEAD_monitor.TE_command,
    force_I2C_low       => FEAD_monitor.I2C_low,
    I2C_clk             => clk_I2C,
    I2C_scl             => I2C_scl_VFE,
    I2C_sda             => I2C_sda_VFE,
    I2C_ack_spy         => VFE_monitor.I2C_ack_spy,

    sequence_clk        => sequence_clk,
    ReSync_DTU          => resync_out_loc
  );
  ReSync_out_R          <= ReSync_out_loc when rising_edge(sequence_clk) else ReSync_out_R;
  ReSync_out_F          <= ReSync_out_loc when falling_edge(sequence_clk) else ReSync_out_F;
  ReSync_out            <= ReSync_out_R when FEAD_monitor.resync_clock_phase/="000" else
                           ReSync_out_F;

  -------------------------------------------------------------------------------
  -- Networking and IPBUS interface
  -------------------------------------------------------------------------------
  inst_VICEPP_IP_address: if use_VICEPP generate
    ip_addr  <= x"0A0000"&"1"&ADDR(6 downto 0) when switch(3) ='0'          -- 10.0.0.(128..255)
           else x"C0A828"&"1"&ADDR(6 downto 0);                             -- 192.168.40.(128..255)
  end generate;
  inst_FEAD_IP_address: if not use_VICEPP generate
    ip_addr  <= x"0A0000"&"1"&ADDR(6 downto 0);                             -- 10.0.0.(128..255)
  end generate;
  mac_addr <= x"060264BEEF"&not(ADDR); -- 06:02:64:BE:EF:XX


  SFP_Rate <= '0';
  EQEN_25  <= '1';
  Inst_gig_ethernet_pcs_pma_0 : gig_ethernet_pcs_pma_0
  port map (
    gtrefclk_p             => GbE_refclk_P,
    gtrefclk_n             => GbE_refclk_N,
    gtrefclk_out           => open,
    gtrefclk_bufg_out      => open,
    txp                    => Gbe_TxP,
    txn                    => Gbe_TxN,
    rxp                    => Gbe_RxP,
    rxn                    => Gbe_RxN,
    resetdone              => open,     --GbE_ready
    userclk_out            => open,
    userclk2_out           => GbE_user_clk,
    rxuserclk_out          => open,
    rxuserclk2_out         => open,
    pma_reset_out          => open,
--    mmcm_locked_out        => LED(0),
--    independent_clock_bufg => sequence_clk,--CLK_LHC,
    independent_clock_bufg => CLK_Gb_eth,--CLK_LHC,
    gmii_txd               => gmii_txd,
    gmii_tx_en             => gmii_tx_en,
    gmii_tx_er             => gmii_tx_er,
    gmii_rxd               => gmii_rxd,
    gmii_rx_dv             => gmii_rx_dv,
    gmii_rx_er             => gmii_rx_er,
    gmii_isolate           => open,
    configuration_vector   => "00000",  --"00001",
    status_vector          => open,
    reset                  => '0',
    signal_detect          => '1',
    gt0_qplloutclk_out     => open,
    gt0_qplloutrefclk_out  => open
  );

  frame_size              <= x"05EE";
--  rx_configuration_vector <= mac_addr & frame_size & 'X' & '0' & "10" & "0X00X0000010";
--  tx_configuration_vector <= mac_addr & frame_size & 'X' & '0' & "10" & "XXX0X0000010";
  rx_configuration_vector <= X"0000_0000_0000_0000_0812";
  tx_configuration_vector <= X"0000_0000_0000_0000_0012";
  Inst_tri_mode_ethernet_mac : tri_mode_ethernet_mac_0
  port map (
    gtx_clk                 => GbE_user_clk,
    glbl_rstn               => '1',
    rx_axi_rstn             => '1',
    tx_axi_rstn             => '1',
    rx_statistics_vector    => open,
    rx_statistics_valid     => open,
    rx_mac_aclk             => open,
    rx_reset                => open,
    rx_axis_mac_tdata       => mac_rx_data,
    rx_axis_mac_tvalid      => mac_rx_valid,
    rx_axis_mac_tlast       => mac_rx_last,
    rx_axis_mac_tuser       => mac_rx_error,
    tx_ifg_delay            => x"00",  --x"04",
    tx_statistics_vector    => open,
    tx_statistics_valid     => open,
    tx_mac_aclk             => open,
    tx_reset                => open,
    tx_axis_mac_tdata       => mac_tx_data,
    tx_axis_mac_tvalid      => mac_tx_valid,
    tx_axis_mac_tlast       => mac_tx_last,
    tx_axis_mac_tuser       => mac_tx_error,
    tx_axis_mac_tready      => mac_tx_ready,
    pause_req               => '0',
    pause_val               => (others => '0'),
    speedis100              => open,
    speedis10100            => open,
    gmii_txd                => gmii_txd,
    gmii_tx_en              => gmii_tx_en,
    gmii_tx_er              => gmii_tx_er,
    gmii_rxd                => gmii_rxd,
    gmii_rx_dv              => gmii_rx_dv,
    gmii_rx_er              => gmii_rx_er,
    rx_configuration_vector => rx_configuration_vector,
    tx_configuration_vector => tx_configuration_vector
  );

  inst_ipbus : ipbus_ctrl
  port map(
    mac_clk      => GbE_user_clk,
    rst_macclk   => '0',
    ipb_clk      => clk_ipbus,
    rst_ipb      => '0',
    mac_rx_data  => mac_rx_data,
    mac_rx_valid => mac_rx_valid,
    mac_rx_last  => mac_rx_last,
    mac_rx_error => mac_rx_error,
    mac_tx_data  => mac_tx_data,
    mac_tx_valid => mac_tx_valid,
    mac_tx_last  => mac_tx_last,
    mac_tx_error => mac_tx_error(0),
    mac_tx_ready => mac_tx_ready,
    ipb_out      => ipb_master_out,
    ipb_in       => ipb_master_in,
    mac_addr     => mac_addr,
    ip_addr      => ip_addr,
    pkt_oob      => pkt_oob,
    pkt          => pkt
  );

  -------------------------------------------------------------------------------
  -- Translation from VFE to FE
  -------------------------------------------------------------------------------  

  inst_slaves : entity work.slaves
  port map(
    pwup_rst      => pwup_reset,
    DAQ_busy      => DAQ_busy,
    ipb_clk       => clk_ipbus,
    ipb_in        => ipb_master_out,
    ipb_out       => ipb_master_in,
    FEAD_Monitor  => FEAD_Monitor,
    FEAD_Control  => FEAD_Control,
    VFE_Monitor   => VFE_Monitor,
    VFE_Control   => VFE_Control,
    clk_40        => HP_Clk_40,
    clk_160       => HP_Clk_160,
    clk_shift_reg => shift_reg_clk,
    clk_memory    => memory_clk,
    BC0           => BC0,
    clk_counter   => clk_counter,
    orbit_counter => orbit_counter,
    ch_in         => captured_stream,
    ch_out        => output_stream,
    trig_local    => trig_local,
    trig_signal   => trig_signal,
    CRC_reset     => FEAD_Control.CRC_reset,
    CRC_error     => FEAD_Monitor.CRC_error,
    APD_temp_in   => APD_temp_in,
    APD_temp_ref  => APD_temp_ref,
    Vdummy_in     => Vdummy_in,
    Vdummy_ref    => Vdummy_ref,
    CATIA_temp_in => CATIA_temp_in,
    CATIA_temp_ref=> CATIA_temp_ref,
    V2P5_in       => V2P5_in,
    V2P5_ref      => V2P5_ref,
    V1P2_in       => V1P2_in,
    V1P2_ref      => V1P2_ref,
    Switch        => Switch,
    nsample_orbit => vfe_monitor.nsample_orbit
  );

--  LED      <= ADDR;
  test_counter : process (sequence_clk)
  begin  -- process test_counter
    if rising_edge(sequence_clk) then
      led_counter <= led_counter + 1;
--      LED(0) <= led_counter(0) and FEAD_Monitor.LED_on;
--      LED(1) <= led_counter(8) and FEAD_Monitor.LED_on;
--      LED(2) <= led_counter(16) and FEAD_Monitor.LED_on;
--      LED(3) <= led_counter(24) and FEAD_Monitor.LED_on;
    end if;
  end process test_counter;

--  GPIO     <= "ZZZZ";--i2c_clk_in&SFP_LOS & SFP_TX_Fault & SFP_Present;
  --JTAG_OUT <= JTAG_IN;

  --i2c bus for interaction between VFE AND FE
  i2c_sda_in  <= 'Z';

  -- SFP i2c bus
  SFP_SCL     <= 'Z';
  SFP_SDA     <= 'Z';

end rtl;
