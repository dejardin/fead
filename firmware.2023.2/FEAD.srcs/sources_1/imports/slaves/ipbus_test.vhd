-- Generic ipbus ram block for testing
--
-- generic addr_width defines number of significant address bits
--
-- In order to allow Xilinx block RAM to be inferred:
-- 	Reset does not clear the RAM contents (not implementable in Xilinx)
--		There is one cycle of latency on the read / write
--
-- Note that the synthesis tool should automatically infer block or distributed RAM
-- according to the size requested. It is likely that it will NOT choose
-- an efficient implementation in terms of area / speed / power, so don't use this
-- method to infer large RAMs (noting also that reads are enabled at all times).
-- It's best to use the block ram core generator explicitly.
--
-- Occupies addr_width bits of ipbus address space
-- This RAM cannot be used with 100% bus utilisation due to the wait state
--
-- Dave Newbold, March 2011
--
-- $Id: ipbus_ram.vhd 1201 2012-09-28 08:49:12Z phdmn $

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.ipbus.all;

entity ipbus_test is
	generic(addr_width : positive);
	port(
		clk: in STD_LOGIC;
		reset: in STD_LOGIC;
		ipbus_in: in ipb_wbus;
		ipbus_out: out ipb_rbus;

                clk_LHC_4x : in std_logic;
                ch1_in : in std_logic_vector(13 downto 0);
                ch2_in : in std_logic_vector(13 downto 0);
                ch3_in : in std_logic_vector(13 downto 0);
                ch4_in : in std_logic_vector(13 downto 0);
                ch5_in : in std_logic_vector(13 downto 0)
                );
	
end ipbus_test;

architecture rtl of ipbus_test is

  component pacd is
    port (
      iPulseA : IN  std_logic;
      iClkA   : IN  std_logic;
      iRSTAn  : IN  std_logic;
      iClkB   : IN  std_logic;
      iRSTBn  : IN  std_logic;
      oPulseB : OUT std_logic);
  end component pacd;
  
  component blk_mem_gen_0 is
    port (
      clka  : in  STD_LOGIC;
      ena   : in  STD_LOGIC;
      wea   : in  STD_LOGIC_VECTOR (0 to 0);
      addra : in  STD_LOGIC_VECTOR (6 downto 0);
      dina  : in  STD_LOGIC_VECTOR (13 downto 0);
      douta : out STD_LOGIC_VECTOR (13 downto 0);
      clkb  : in  STD_LOGIC;
      enb   : in  STD_LOGIC;
      web   : in  STD_LOGIC_VECTOR (0 to 0);
      addrb : in  STD_LOGIC_VECTOR (6 downto 0);
      dinb  : in  STD_LOGIC_VECTOR (13 downto 0);
      doutb : out STD_LOGIC_VECTOR (13 downto 0));
  end component blk_mem_gen_0;

  
  signal sel: integer;
  signal ack: std_logic;
  signal capture_address : unsigned(6 downto 0) := "000"&x"0";
  type mem_data_t is array (0 to 4) of std_logic_vector(13 downto 0);
  signal mem_data : mem_data_t;
  signal ack_running : std_logic := '1';
  signal ack_start : std_logic := '0';
  signal ack_stop : std_logic := '0';
  signal ack_start_LHC : std_logic := '0';
  signal ack_stop_LHC : std_logic := '0';

  signal addrb : std_logic_vector(6 downto 0);
  
begin

  blk_mem_gen_0_1: blk_mem_gen_0
      port map (
        clka  => CLK_LHC_4x,
        ena   => ack_running,
        wea   => "1",
        addra => std_logic_vector(capture_address),
        dina  => ch1_in,
        douta => open,
        clkb  => clk,
        enb   => '1',
        web   => "0",
        addrb => addrb,
        dinb  => "00"&x"000",
        doutb => mem_data(0));    
  blk_mem_gen_0_2: blk_mem_gen_0
      port map (
        clka  => CLK_LHC_4x,
        ena   => ack_running,
        wea   => "1",
        addra => std_logic_vector(capture_address),
        dina  => ch2_in,
        douta => open,
        clkb  => clk,
        enb   => '1',
        web   => "0",
        addrb => addrb,
        dinb  => "00"&x"000",
        doutb => mem_data(1));    
  blk_mem_gen_0_3: blk_mem_gen_0
      port map (
        clka  => CLK_LHC_4x,
        ena   => ack_running,
        wea   => "1",
        addra => std_logic_vector(capture_address),
        dina  => ch3_in,
        douta => open,
        clkb  => clk,
        enb   => '1',
        web   => "0",
        addrb => addrb,
        dinb  => "00"&x"000",
        doutb => mem_data(2));    
  blk_mem_gen_0_4: blk_mem_gen_0
      port map (
        clka  => CLK_LHC_4x,
        ena   => ack_running,
        wea   => "1",
        addra => std_logic_vector(capture_address),
        dina  => ch4_in,
        douta => open,
        clkb  => clk,
        enb   => '1',
        web   => "0",
        addrb => addrb,
        dinb  => "00"&x"000",
        doutb => mem_data(3));    
  blk_mem_gen_0_5: blk_mem_gen_0
      port map (
        clka  => CLK_LHC_4x,
        ena   => ack_running,
        wea   => "1",
        addra => std_logic_vector(capture_address),
        dina  => ch5_in,
        douta => open,
        clkb  => clk,
        enb   => '1',
        web   => "0",
        addrb => addrb,
        dinb  => "00"&x"000",
        doutb => mem_data(4));    



  
  capture_counter: process (clk_LHC_4x) is
  begin  -- process capture_counter
    if clk_LHC_4x'event and clk_LHC_4x = '1' then  -- rising clock edge
      if ack_start_LHC = '1' then
        ack_running <= '1';
      elsif ack_stop_LHC = '1' then
        ack_running <= '0';
      end if;

      if ack_running = '1' then
        capture_address <= capture_address + 1;
      else
        capture_address <= "000"&x"0";
      end if;
    end if;
  end process capture_counter;
  
  
  addrb <= ipbus_in.ipb_addr(6 downto 0);

  
  
  mem_write: process (clk_LHC_4x) is
  begin  -- process mem_write
    if clk_LHC_4x'event and clk_LHC_4x = '1' then  -- rising clock edge
    end if;
  end process mem_write;
  
  process(clk)
  begin
    if rising_edge(clk) then
      ack_start <= '0';
      case ipbus_in.ipb_addr(9 downto 7) is
        when  "000" => ipbus_out.ipb_rdata <= x"0000"&"00"&mem_data(0);
        when  "001" => ipbus_out.ipb_rdata <= x"0000"&"00"&mem_data(1);
        when  "010" => ipbus_out.ipb_rdata <= x"0000"&"00"&mem_data(2);
        when  "011" => ipbus_out.ipb_rdata <= x"0000"&"00"&mem_data(3);
        when  "100" => ipbus_out.ipb_rdata <= x"0000"&"00"&mem_data(4);
        when  "111" => ack_start <= ipbus_in.ipb_wdata(0);
                       ack_stop  <= ipbus_in.ipb_wdata(1);
        when others => ipbus_out.ipb_rdata <= x"DEADBEEF";                       
      end case;

      ack <= ipbus_in.ipb_strobe and not ack;
      
    end if;
  end process;

  pacd_1: pacd
    port map (
      iPulseA => ack_start,
      iClkA   => clk,
      iRSTAn  => '1',
      iClkB   => clk_LHC_4x,
      iRSTBn  => '1',
      oPulseB => ack_start_LHC);
  pacd_2: pacd
    port map (
      iPulseA => ack_stop,
      iClkA   => clk,
      iRSTAn  => '1',
      iClkB   => clk_LHC_4x,
      iRSTBn  => '1',
      oPulseB => ack_stop_LHC);

  
  
  ipbus_out.ipb_ack <= ack;
  ipbus_out.ipb_err <= '0';
  
end rtl;
