library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
---- The following library declaration should be present if 
---- instantiating any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

--use work.ipbus.all;
--use work.ipbus_trans_decl.all;
--use work.FEAD_IO.all;

entity test_trigger is
end test_trigger;

architecture simulate of test_trigger is

signal hard_reset            : std_logic := '1';
signal sequence_clk          : std_logic := '0';
signal VICEPP_Clk_Config     : std_logic_vector(31 downto 0);
signal XPS_Sout              : std_logic_vector(1 downto 0);
signal XPS_Sin               : std_logic_vector(1 downto 0);
signal XPS_Config            : std_logic_vector(1 downto 0);
signal XPS_Load              : std_logic_vector(1 downto 0);
signal MMCM_locked           : std_logic;
signal pwup_reset            : std_logic;
signal trig_signal            : std_logic := '0'; -- Single event trigger
signal trig_software          : std_logic := '0'; -- single event trigger received on ipbus
signal trig_sw_width          : unsigned(7 downto 0) := (others => '0');
signal trig_out               : std_logic := '0'; -- output signal generated from trig_software after some delay
signal trig_in                : std_logic := '0'; -- single event  trigger receives on GPIO pin
signal trig_hardware          : std_logic := '0'; -- input signal generated from trig_hardware after some delay
signal trig_hw_width          : unsigned(7 downto 0) := (others => '0');
signal trig_local             : std_logic := '0'; -- trigger signal generated looking at input data (required trig_self=1)
signal trig_local_outb        : std_logic := 'Z'; -- trigger signal generated looking at input data (required trig_self=1) sent to FE
signal trig_in_prev1          : std_logic := '0';
signal trig_in_prev2          : std_logic := '0';
signal trigger_prev1          : std_logic := '0';
signal trigger_prev2          : std_logic := '0';
signal delay_out_counter      : unsigned(15 downto 0) := x"0000";
signal start_out_counter      : std_logic := '0';
signal delay_in_counter       : unsigned(15 downto 0) := x"0000";
signal start_in_counter       : std_logic := '0';
signal gen_trigger            : std_logic := '0';
signal trigger_HW_delay       : unsigned(15 downto 0) := x"0030";

begin

gen_trig_in : process(MMCM_locked, pwup_reset, sequence_clk, trig_in)
  begin
    if MMCM_locked='0' or pwup_reset = '1' then
      delay_in_counter      <= x"0000";
      start_in_counter      <= '0';
      trig_hardware         <= '0';
      trig_in_Prev1         <= '0';
      trig_in_Prev2         <= '0';
      trig_hw_width         <= (others => '0');
    elsif rising_edge(sequence_clk) then
      trig_in_prev1         <= trig_in;
      trig_in_prev2         <= trig_in_prev1;
      if trig_in_prev1='1' and trig_in_prev2='0' then
        start_in_counter    <= '1';
        delay_in_counter    <= x"0000";
      end if;
      if start_in_counter = '1' then
        if delay_in_counter < unsigned(trigger_HW_delay) then
          delay_in_counter  <= delay_in_counter+1;
        else
          trig_hw_width     <= (others => '0');
          trig_hardware     <= '1';
          start_in_counter  <= '0';
        end if;
      end if;
      if trig_hardware = '1'then
        if trig_hw_width < x"04" then
          trig_hw_width     <= trig_hw_width+1;
        else
          trig_hardware     <= '0';
        end if;
      end if;
    end if;
  end process gen_trig_in;


hard_reset <= '0' after 25 ns;
sequence_clk <= not sequence_clk after  3125 ps;
MMCM_locked            <= '0', '1' after 100 ns; 
pwup_reset             <= '1', '0' after 50 ns; 
trig_in                <= '0', '1' after 145 ns, '0' after 245 ns; 
end;