-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- Copyright 2022-2023 Advanced Micro Devices, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2023.2 (lin64) Build 4029153 Fri Oct 13 20:13:54 MDT 2023
-- Date        : Wed Jan 17 16:49:39 2024
-- Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 38 (Thirty Eight)
-- Command     : write_vhdl -force -mode funcsim
--               /data/cms/ecal/fe/fead_v2/firmware.2023.2/FEAD.gen/sources_1/ip/gig_ethernet_pcs_pma_0/gig_ethernet_pcs_pma_0_sim_netlist.vhdl
-- Design      : gig_ethernet_pcs_pma_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7k70tfbg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_GTWIZARD_GT is
  port (
    gtxe2_i_0 : out STD_LOGIC;
    gt0_cpllrefclklost_i : out STD_LOGIC;
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    gtxe2_i_1 : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    gtxe2_i_2 : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_4 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_5 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_7 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    cpll_pd0_i : in STD_LOGIC;
    cpllreset_in : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_gttxreset_in0_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gt0_rxuserrdy_t : in STD_LOGIC;
    gtxe2_i_8 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_txuserrdy_t : in STD_LOGIC;
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_9 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_10 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_11 : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end gig_ethernet_pcs_pma_0_GTWIZARD_GT;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_GTWIZARD_GT is
  signal gtxe2_i_n_0 : STD_LOGIC;
  signal gtxe2_i_n_10 : STD_LOGIC;
  signal gtxe2_i_n_16 : STD_LOGIC;
  signal gtxe2_i_n_170 : STD_LOGIC;
  signal gtxe2_i_n_171 : STD_LOGIC;
  signal gtxe2_i_n_172 : STD_LOGIC;
  signal gtxe2_i_n_173 : STD_LOGIC;
  signal gtxe2_i_n_174 : STD_LOGIC;
  signal gtxe2_i_n_175 : STD_LOGIC;
  signal gtxe2_i_n_176 : STD_LOGIC;
  signal gtxe2_i_n_177 : STD_LOGIC;
  signal gtxe2_i_n_178 : STD_LOGIC;
  signal gtxe2_i_n_179 : STD_LOGIC;
  signal gtxe2_i_n_180 : STD_LOGIC;
  signal gtxe2_i_n_181 : STD_LOGIC;
  signal gtxe2_i_n_182 : STD_LOGIC;
  signal gtxe2_i_n_183 : STD_LOGIC;
  signal gtxe2_i_n_184 : STD_LOGIC;
  signal gtxe2_i_n_27 : STD_LOGIC;
  signal gtxe2_i_n_3 : STD_LOGIC;
  signal gtxe2_i_n_38 : STD_LOGIC;
  signal gtxe2_i_n_39 : STD_LOGIC;
  signal gtxe2_i_n_4 : STD_LOGIC;
  signal gtxe2_i_n_46 : STD_LOGIC;
  signal gtxe2_i_n_47 : STD_LOGIC;
  signal gtxe2_i_n_48 : STD_LOGIC;
  signal gtxe2_i_n_49 : STD_LOGIC;
  signal gtxe2_i_n_50 : STD_LOGIC;
  signal gtxe2_i_n_51 : STD_LOGIC;
  signal gtxe2_i_n_52 : STD_LOGIC;
  signal gtxe2_i_n_53 : STD_LOGIC;
  signal gtxe2_i_n_54 : STD_LOGIC;
  signal gtxe2_i_n_55 : STD_LOGIC;
  signal gtxe2_i_n_56 : STD_LOGIC;
  signal gtxe2_i_n_57 : STD_LOGIC;
  signal gtxe2_i_n_58 : STD_LOGIC;
  signal gtxe2_i_n_59 : STD_LOGIC;
  signal gtxe2_i_n_60 : STD_LOGIC;
  signal gtxe2_i_n_61 : STD_LOGIC;
  signal gtxe2_i_n_81 : STD_LOGIC;
  signal gtxe2_i_n_83 : STD_LOGIC;
  signal gtxe2_i_n_84 : STD_LOGIC;
  signal gtxe2_i_n_9 : STD_LOGIC;
  signal NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_PHYSTATUS_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXDATAVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXELECIDLE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXQPISENN_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXQPISENP_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXRATEDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXQPISENN_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXQPISENP_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXRATEDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXCHARISK_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXCHBONDO_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gtxe2_i_RXDATA_UNCONNECTED : STD_LOGIC_VECTOR ( 63 downto 16 );
  signal NLW_gtxe2_i_RXDISPERR_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXHEADER_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gtxe2_i_RXSTATUS_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_gtxe2_i_TSTOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  attribute box_type : string;
  attribute box_type of gtxe2_i : label is "PRIMITIVE";
begin
gtxe2_i: unisim.vcomponents.GTXE2_CHANNEL
    generic map(
      ALIGN_COMMA_DOUBLE => "FALSE",
      ALIGN_COMMA_ENABLE => B"0001111111",
      ALIGN_COMMA_WORD => 2,
      ALIGN_MCOMMA_DET => "TRUE",
      ALIGN_MCOMMA_VALUE => B"1010000011",
      ALIGN_PCOMMA_DET => "TRUE",
      ALIGN_PCOMMA_VALUE => B"0101111100",
      CBCC_DATA_SOURCE_SEL => "DECODED",
      CHAN_BOND_KEEP_ALIGN => "FALSE",
      CHAN_BOND_MAX_SKEW => 1,
      CHAN_BOND_SEQ_1_1 => B"0000000000",
      CHAN_BOND_SEQ_1_2 => B"0000000000",
      CHAN_BOND_SEQ_1_3 => B"0000000000",
      CHAN_BOND_SEQ_1_4 => B"0000000000",
      CHAN_BOND_SEQ_1_ENABLE => B"1111",
      CHAN_BOND_SEQ_2_1 => B"0000000000",
      CHAN_BOND_SEQ_2_2 => B"0000000000",
      CHAN_BOND_SEQ_2_3 => B"0000000000",
      CHAN_BOND_SEQ_2_4 => B"0000000000",
      CHAN_BOND_SEQ_2_ENABLE => B"1111",
      CHAN_BOND_SEQ_2_USE => "FALSE",
      CHAN_BOND_SEQ_LEN => 1,
      CLK_CORRECT_USE => "TRUE",
      CLK_COR_KEEP_IDLE => "FALSE",
      CLK_COR_MAX_LAT => 36,
      CLK_COR_MIN_LAT => 33,
      CLK_COR_PRECEDENCE => "TRUE",
      CLK_COR_REPEAT_WAIT => 0,
      CLK_COR_SEQ_1_1 => B"0110111100",
      CLK_COR_SEQ_1_2 => B"0001010000",
      CLK_COR_SEQ_1_3 => B"0000000000",
      CLK_COR_SEQ_1_4 => B"0000000000",
      CLK_COR_SEQ_1_ENABLE => B"1111",
      CLK_COR_SEQ_2_1 => B"0110111100",
      CLK_COR_SEQ_2_2 => B"0010110101",
      CLK_COR_SEQ_2_3 => B"0000000000",
      CLK_COR_SEQ_2_4 => B"0000000000",
      CLK_COR_SEQ_2_ENABLE => B"1111",
      CLK_COR_SEQ_2_USE => "TRUE",
      CLK_COR_SEQ_LEN => 2,
      CPLL_CFG => X"BC07DC",
      CPLL_FBDIV => 4,
      CPLL_FBDIV_45 => 5,
      CPLL_INIT_CFG => X"00001E",
      CPLL_LOCK_CFG => X"01E8",
      CPLL_REFCLK_DIV => 1,
      DEC_MCOMMA_DETECT => "TRUE",
      DEC_PCOMMA_DETECT => "TRUE",
      DEC_VALID_COMMA_ONLY => "FALSE",
      DMONITOR_CFG => X"000A00",
      ES_CONTROL => B"000000",
      ES_ERRDET_EN => "FALSE",
      ES_EYE_SCAN_EN => "TRUE",
      ES_HORZ_OFFSET => X"000",
      ES_PMA_CFG => B"0000000000",
      ES_PRESCALE => B"00000",
      ES_QUALIFIER => X"00000000000000000000",
      ES_QUAL_MASK => X"00000000000000000000",
      ES_SDATA_MASK => X"00000000000000000000",
      ES_VERT_OFFSET => B"000000000",
      FTS_DESKEW_SEQ_ENABLE => B"1111",
      FTS_LANE_DESKEW_CFG => B"1111",
      FTS_LANE_DESKEW_EN => "FALSE",
      GEARBOX_MODE => B"000",
      IS_CPLLLOCKDETCLK_INVERTED => '0',
      IS_DRPCLK_INVERTED => '0',
      IS_GTGREFCLK_INVERTED => '0',
      IS_RXUSRCLK2_INVERTED => '0',
      IS_RXUSRCLK_INVERTED => '0',
      IS_TXPHDLYTSTCLK_INVERTED => '0',
      IS_TXUSRCLK2_INVERTED => '0',
      IS_TXUSRCLK_INVERTED => '0',
      OUTREFCLK_SEL_INV => B"11",
      PCS_PCIE_EN => "FALSE",
      PCS_RSVD_ATTR => X"000000000000",
      PD_TRANS_TIME_FROM_P2 => X"03C",
      PD_TRANS_TIME_NONE_P2 => X"19",
      PD_TRANS_TIME_TO_P2 => X"64",
      PMA_RSV => X"00018480",
      PMA_RSV2 => X"2050",
      PMA_RSV3 => B"00",
      PMA_RSV4 => X"00000000",
      RXBUFRESET_TIME => B"00001",
      RXBUF_ADDR_MODE => "FULL",
      RXBUF_EIDLE_HI_CNT => B"1000",
      RXBUF_EIDLE_LO_CNT => B"0000",
      RXBUF_EN => "TRUE",
      RXBUF_RESET_ON_CB_CHANGE => "TRUE",
      RXBUF_RESET_ON_COMMAALIGN => "FALSE",
      RXBUF_RESET_ON_EIDLE => "FALSE",
      RXBUF_RESET_ON_RATE_CHANGE => "TRUE",
      RXBUF_THRESH_OVFLW => 61,
      RXBUF_THRESH_OVRD => "FALSE",
      RXBUF_THRESH_UNDFLW => 8,
      RXCDRFREQRESET_TIME => B"00001",
      RXCDRPHRESET_TIME => B"00001",
      RXCDR_CFG => X"03000023FF10100020",
      RXCDR_FR_RESET_ON_EIDLE => '0',
      RXCDR_HOLD_DURING_EIDLE => '0',
      RXCDR_LOCK_CFG => B"010101",
      RXCDR_PH_RESET_ON_EIDLE => '0',
      RXDFELPMRESET_TIME => B"0001111",
      RXDLY_CFG => X"001F",
      RXDLY_LCFG => X"030",
      RXDLY_TAP_CFG => X"0000",
      RXGEARBOX_EN => "FALSE",
      RXISCANRESET_TIME => B"00001",
      RXLPM_HF_CFG => B"00000011110000",
      RXLPM_LF_CFG => B"00000011110000",
      RXOOB_CFG => B"0000110",
      RXOUT_DIV => 4,
      RXPCSRESET_TIME => B"00001",
      RXPHDLY_CFG => X"084020",
      RXPH_CFG => X"000000",
      RXPH_MONITOR_SEL => B"00000",
      RXPMARESET_TIME => B"00011",
      RXPRBS_ERR_LOOPBACK => '0',
      RXSLIDE_AUTO_WAIT => 7,
      RXSLIDE_MODE => "OFF",
      RX_BIAS_CFG => B"000000000100",
      RX_BUFFER_CFG => B"000000",
      RX_CLK25_DIV => 5,
      RX_CLKMUX_PD => '1',
      RX_CM_SEL => B"11",
      RX_CM_TRIM => B"010",
      RX_DATA_WIDTH => 20,
      RX_DDI_SEL => B"000000",
      RX_DEBUG_CFG => B"000000000000",
      RX_DEFER_RESET_BUF_EN => "TRUE",
      RX_DFE_GAIN_CFG => X"020FEA",
      RX_DFE_H2_CFG => B"000000000000",
      RX_DFE_H3_CFG => B"000001000000",
      RX_DFE_H4_CFG => B"00011110000",
      RX_DFE_H5_CFG => B"00011100000",
      RX_DFE_KL_CFG => B"0000011111110",
      RX_DFE_KL_CFG2 => X"301148AC",
      RX_DFE_LPM_CFG => X"0904",
      RX_DFE_LPM_HOLD_DURING_EIDLE => '0',
      RX_DFE_UT_CFG => B"10001111000000000",
      RX_DFE_VP_CFG => B"00011111100000011",
      RX_DFE_XYD_CFG => B"0000000000000",
      RX_DISPERR_SEQ_MATCH => "TRUE",
      RX_INT_DATAWIDTH => 0,
      RX_OS_CFG => B"0000010000000",
      RX_SIG_VALID_DLY => 10,
      RX_XCLK_SEL => "RXREC",
      SAS_MAX_COM => 64,
      SAS_MIN_COM => 36,
      SATA_BURST_SEQ_LEN => B"0101",
      SATA_BURST_VAL => B"100",
      SATA_CPLL_CFG => "VCO_3000MHZ",
      SATA_EIDLE_VAL => B"100",
      SATA_MAX_BURST => 8,
      SATA_MAX_INIT => 21,
      SATA_MAX_WAKE => 7,
      SATA_MIN_BURST => 4,
      SATA_MIN_INIT => 12,
      SATA_MIN_WAKE => 4,
      SHOW_REALIGN_COMMA => "TRUE",
      SIM_CPLLREFCLK_SEL => B"001",
      SIM_RECEIVER_DETECT_PASS => "TRUE",
      SIM_RESET_SPEEDUP => "TRUE",
      SIM_TX_EIDLE_DRIVE_LEVEL => "X",
      SIM_VERSION => "4.0",
      TERM_RCAL_CFG => B"10000",
      TERM_RCAL_OVRD => '0',
      TRANS_TIME_RATE => X"0E",
      TST_RSV => X"00000000",
      TXBUF_EN => "TRUE",
      TXBUF_RESET_ON_RATE_CHANGE => "TRUE",
      TXDLY_CFG => X"001F",
      TXDLY_LCFG => X"030",
      TXDLY_TAP_CFG => X"0000",
      TXGEARBOX_EN => "FALSE",
      TXOUT_DIV => 4,
      TXPCSRESET_TIME => B"00001",
      TXPHDLY_CFG => X"084020",
      TXPH_CFG => X"0780",
      TXPH_MONITOR_SEL => B"00000",
      TXPMARESET_TIME => B"00001",
      TX_CLK25_DIV => 5,
      TX_CLKMUX_PD => '1',
      TX_DATA_WIDTH => 20,
      TX_DEEMPH0 => B"00000",
      TX_DEEMPH1 => B"00000",
      TX_DRIVE_MODE => "DIRECT",
      TX_EIDLE_ASSERT_DELAY => B"110",
      TX_EIDLE_DEASSERT_DELAY => B"100",
      TX_INT_DATAWIDTH => 0,
      TX_LOOPBACK_DRIVE_HIZ => "FALSE",
      TX_MAINCURSOR_SEL => '0',
      TX_MARGIN_FULL_0 => B"1001110",
      TX_MARGIN_FULL_1 => B"1001001",
      TX_MARGIN_FULL_2 => B"1000101",
      TX_MARGIN_FULL_3 => B"1000010",
      TX_MARGIN_FULL_4 => B"1000000",
      TX_MARGIN_LOW_0 => B"1000110",
      TX_MARGIN_LOW_1 => B"1000100",
      TX_MARGIN_LOW_2 => B"1000010",
      TX_MARGIN_LOW_3 => B"1000000",
      TX_MARGIN_LOW_4 => B"1000000",
      TX_PREDRIVER_MODE => '0',
      TX_QPI_STATUS_EN => '0',
      TX_RXDETECT_CFG => X"1832",
      TX_RXDETECT_REF => B"100",
      TX_XCLK_SEL => "TXOUT",
      UCODEER_CLR => '0'
    )
        port map (
      CFGRESET => '0',
      CLKRSVD(3 downto 0) => B"0000",
      CPLLFBCLKLOST => gtxe2_i_n_0,
      CPLLLOCK => gtxe2_i_0,
      CPLLLOCKDETCLK => independent_clock_bufg,
      CPLLLOCKEN => '1',
      CPLLPD => cpll_pd0_i,
      CPLLREFCLKLOST => gt0_cpllrefclklost_i,
      CPLLREFCLKSEL(2 downto 0) => B"001",
      CPLLRESET => cpllreset_in,
      DMONITOROUT(7) => gtxe2_i_n_177,
      DMONITOROUT(6) => gtxe2_i_n_178,
      DMONITOROUT(5) => gtxe2_i_n_179,
      DMONITOROUT(4) => gtxe2_i_n_180,
      DMONITOROUT(3) => gtxe2_i_n_181,
      DMONITOROUT(2) => gtxe2_i_n_182,
      DMONITOROUT(1) => gtxe2_i_n_183,
      DMONITOROUT(0) => gtxe2_i_n_184,
      DRPADDR(8 downto 0) => B"000000000",
      DRPCLK => gtrefclk_bufg,
      DRPDI(15 downto 0) => B"0000000000000000",
      DRPDO(15) => gtxe2_i_n_46,
      DRPDO(14) => gtxe2_i_n_47,
      DRPDO(13) => gtxe2_i_n_48,
      DRPDO(12) => gtxe2_i_n_49,
      DRPDO(11) => gtxe2_i_n_50,
      DRPDO(10) => gtxe2_i_n_51,
      DRPDO(9) => gtxe2_i_n_52,
      DRPDO(8) => gtxe2_i_n_53,
      DRPDO(7) => gtxe2_i_n_54,
      DRPDO(6) => gtxe2_i_n_55,
      DRPDO(5) => gtxe2_i_n_56,
      DRPDO(4) => gtxe2_i_n_57,
      DRPDO(3) => gtxe2_i_n_58,
      DRPDO(2) => gtxe2_i_n_59,
      DRPDO(1) => gtxe2_i_n_60,
      DRPDO(0) => gtxe2_i_n_61,
      DRPEN => '0',
      DRPRDY => gtxe2_i_n_3,
      DRPWE => '0',
      EYESCANDATAERROR => gtxe2_i_n_4,
      EYESCANMODE => '0',
      EYESCANRESET => '0',
      EYESCANTRIGGER => '0',
      GTGREFCLK => '0',
      GTNORTHREFCLK0 => '0',
      GTNORTHREFCLK1 => '0',
      GTREFCLK0 => gtrefclk_out,
      GTREFCLK1 => '0',
      GTREFCLKMONITOR => NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED,
      GTRESETSEL => '0',
      GTRSVD(15 downto 0) => B"0000000000000000",
      GTRXRESET => SR(0),
      GTSOUTHREFCLK0 => '0',
      GTSOUTHREFCLK1 => '0',
      GTTXRESET => gt0_gttxreset_in0_out,
      GTXRXN => rxn,
      GTXRXP => rxp,
      GTXTXN => txn,
      GTXTXP => txp,
      LOOPBACK(2 downto 0) => B"000",
      PCSRSVDIN(15 downto 0) => B"0000000000000000",
      PCSRSVDIN2(4 downto 0) => B"00000",
      PCSRSVDOUT(15 downto 0) => NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED(15 downto 0),
      PHYSTATUS => NLW_gtxe2_i_PHYSTATUS_UNCONNECTED,
      PMARSVDIN(4 downto 0) => B"00000",
      PMARSVDIN2(4 downto 0) => B"00000",
      QPLLCLK => gt0_qplloutclk_out,
      QPLLREFCLK => gt0_qplloutrefclk_out,
      RESETOVRD => '0',
      RX8B10BEN => '1',
      RXBUFRESET => '0',
      RXBUFSTATUS(2) => RXBUFSTATUS(0),
      RXBUFSTATUS(1) => gtxe2_i_n_83,
      RXBUFSTATUS(0) => gtxe2_i_n_84,
      RXBYTEISALIGNED => gtxe2_i_n_9,
      RXBYTEREALIGN => gtxe2_i_n_10,
      RXCDRFREQRESET => '0',
      RXCDRHOLD => '0',
      RXCDRLOCK => NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED,
      RXCDROVRDEN => '0',
      RXCDRRESET => '0',
      RXCDRRESETRSV => '0',
      RXCHANBONDSEQ => NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED,
      RXCHANISALIGNED => NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED,
      RXCHANREALIGN => NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED,
      RXCHARISCOMMA(7 downto 2) => NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED(7 downto 2),
      RXCHARISCOMMA(1 downto 0) => gtxe2_i_4(1 downto 0),
      RXCHARISK(7 downto 2) => NLW_gtxe2_i_RXCHARISK_UNCONNECTED(7 downto 2),
      RXCHARISK(1 downto 0) => gtxe2_i_5(1 downto 0),
      RXCHBONDEN => '0',
      RXCHBONDI(4 downto 0) => B"00000",
      RXCHBONDLEVEL(2 downto 0) => B"000",
      RXCHBONDMASTER => '0',
      RXCHBONDO(4 downto 0) => NLW_gtxe2_i_RXCHBONDO_UNCONNECTED(4 downto 0),
      RXCHBONDSLAVE => '0',
      RXCLKCORCNT(1 downto 0) => D(1 downto 0),
      RXCOMINITDET => NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED,
      RXCOMMADET => gtxe2_i_n_16,
      RXCOMMADETEN => '1',
      RXCOMSASDET => NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED,
      RXCOMWAKEDET => NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED,
      RXDATA(63 downto 16) => NLW_gtxe2_i_RXDATA_UNCONNECTED(63 downto 16),
      RXDATA(15 downto 0) => gtxe2_i_3(15 downto 0),
      RXDATAVALID => NLW_gtxe2_i_RXDATAVALID_UNCONNECTED,
      RXDDIEN => '0',
      RXDFEAGCHOLD => '0',
      RXDFEAGCOVRDEN => '0',
      RXDFECM1EN => '0',
      RXDFELFHOLD => '0',
      RXDFELFOVRDEN => '0',
      RXDFELPMRESET => '0',
      RXDFETAP2HOLD => '0',
      RXDFETAP2OVRDEN => '0',
      RXDFETAP3HOLD => '0',
      RXDFETAP3OVRDEN => '0',
      RXDFETAP4HOLD => '0',
      RXDFETAP4OVRDEN => '0',
      RXDFETAP5HOLD => '0',
      RXDFETAP5OVRDEN => '0',
      RXDFEUTHOLD => '0',
      RXDFEUTOVRDEN => '0',
      RXDFEVPHOLD => '0',
      RXDFEVPOVRDEN => '0',
      RXDFEVSEN => '0',
      RXDFEXYDEN => '1',
      RXDFEXYDHOLD => '0',
      RXDFEXYDOVRDEN => '0',
      RXDISPERR(7 downto 2) => NLW_gtxe2_i_RXDISPERR_UNCONNECTED(7 downto 2),
      RXDISPERR(1 downto 0) => gtxe2_i_6(1 downto 0),
      RXDLYBYPASS => '1',
      RXDLYEN => '0',
      RXDLYOVRDEN => '0',
      RXDLYSRESET => '0',
      RXDLYSRESETDONE => NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED,
      RXELECIDLE => NLW_gtxe2_i_RXELECIDLE_UNCONNECTED,
      RXELECIDLEMODE(1 downto 0) => B"11",
      RXGEARBOXSLIP => '0',
      RXHEADER(2 downto 0) => NLW_gtxe2_i_RXHEADER_UNCONNECTED(2 downto 0),
      RXHEADERVALID => NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED,
      RXLPMEN => '1',
      RXLPMHFHOLD => '0',
      RXLPMHFOVRDEN => '0',
      RXLPMLFHOLD => '0',
      RXLPMLFKLOVRDEN => '0',
      RXMCOMMAALIGNEN => reset_out,
      RXMONITOROUT(6) => gtxe2_i_n_170,
      RXMONITOROUT(5) => gtxe2_i_n_171,
      RXMONITOROUT(4) => gtxe2_i_n_172,
      RXMONITOROUT(3) => gtxe2_i_n_173,
      RXMONITOROUT(2) => gtxe2_i_n_174,
      RXMONITOROUT(1) => gtxe2_i_n_175,
      RXMONITOROUT(0) => gtxe2_i_n_176,
      RXMONITORSEL(1 downto 0) => B"00",
      RXNOTINTABLE(7 downto 2) => NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED(7 downto 2),
      RXNOTINTABLE(1 downto 0) => gtxe2_i_7(1 downto 0),
      RXOOBRESET => '0',
      RXOSHOLD => '0',
      RXOSOVRDEN => '0',
      RXOUTCLK => rxoutclk,
      RXOUTCLKFABRIC => NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED,
      RXOUTCLKPCS => NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED,
      RXOUTCLKSEL(2 downto 0) => B"010",
      RXPCOMMAALIGNEN => reset_out,
      RXPCSRESET => reset,
      RXPD(1) => RXPD(0),
      RXPD(0) => RXPD(0),
      RXPHALIGN => '0',
      RXPHALIGNDONE => NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED,
      RXPHALIGNEN => '0',
      RXPHDLYPD => '0',
      RXPHDLYRESET => '0',
      RXPHMONITOR(4 downto 0) => NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED(4 downto 0),
      RXPHOVRDEN => '0',
      RXPHSLIPMONITOR(4 downto 0) => NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED(4 downto 0),
      RXPMARESET => '0',
      RXPOLARITY => '0',
      RXPRBSCNTRESET => '0',
      RXPRBSERR => gtxe2_i_n_27,
      RXPRBSSEL(2 downto 0) => B"000",
      RXQPIEN => '0',
      RXQPISENN => NLW_gtxe2_i_RXQPISENN_UNCONNECTED,
      RXQPISENP => NLW_gtxe2_i_RXQPISENP_UNCONNECTED,
      RXRATE(2 downto 0) => B"000",
      RXRATEDONE => NLW_gtxe2_i_RXRATEDONE_UNCONNECTED,
      RXRESETDONE => gtxe2_i_1,
      RXSLIDE => '0',
      RXSTARTOFSEQ => NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED,
      RXSTATUS(2 downto 0) => NLW_gtxe2_i_RXSTATUS_UNCONNECTED(2 downto 0),
      RXSYSCLKSEL(1 downto 0) => B"00",
      RXUSERRDY => gt0_rxuserrdy_t,
      RXUSRCLK => gtxe2_i_8,
      RXUSRCLK2 => gtxe2_i_8,
      RXVALID => NLW_gtxe2_i_RXVALID_UNCONNECTED,
      SETERRSTATUS => '0',
      TSTIN(19 downto 0) => B"11111111111111111111",
      TSTOUT(9 downto 0) => NLW_gtxe2_i_TSTOUT_UNCONNECTED(9 downto 0),
      TX8B10BBYPASS(7 downto 0) => B"00000000",
      TX8B10BEN => '1',
      TXBUFDIFFCTRL(2 downto 0) => B"100",
      TXBUFSTATUS(1) => TXBUFSTATUS(0),
      TXBUFSTATUS(0) => gtxe2_i_n_81,
      TXCHARDISPMODE(7 downto 2) => B"000000",
      TXCHARDISPMODE(1 downto 0) => gtxe2_i_9(1 downto 0),
      TXCHARDISPVAL(7 downto 2) => B"000000",
      TXCHARDISPVAL(1 downto 0) => gtxe2_i_10(1 downto 0),
      TXCHARISK(7 downto 2) => B"000000",
      TXCHARISK(1 downto 0) => gtxe2_i_11(1 downto 0),
      TXCOMFINISH => NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED,
      TXCOMINIT => '0',
      TXCOMSAS => '0',
      TXCOMWAKE => '0',
      TXDATA(63 downto 16) => B"000000000000000000000000000000000000000000000000",
      TXDATA(15 downto 0) => Q(15 downto 0),
      TXDEEMPH => '0',
      TXDETECTRX => '0',
      TXDIFFCTRL(3 downto 0) => B"1000",
      TXDIFFPD => '0',
      TXDLYBYPASS => '1',
      TXDLYEN => '0',
      TXDLYHOLD => '0',
      TXDLYOVRDEN => '0',
      TXDLYSRESET => '0',
      TXDLYSRESETDONE => NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED,
      TXDLYUPDOWN => '0',
      TXELECIDLE => TXPD(0),
      TXGEARBOXREADY => NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED,
      TXHEADER(2 downto 0) => B"000",
      TXINHIBIT => '0',
      TXMAINCURSOR(6 downto 0) => B"0000000",
      TXMARGIN(2 downto 0) => B"000",
      TXOUTCLK => txoutclk,
      TXOUTCLKFABRIC => gtxe2_i_n_38,
      TXOUTCLKPCS => gtxe2_i_n_39,
      TXOUTCLKSEL(2 downto 0) => B"100",
      TXPCSRESET => '0',
      TXPD(1) => TXPD(0),
      TXPD(0) => TXPD(0),
      TXPDELECIDLEMODE => '0',
      TXPHALIGN => '0',
      TXPHALIGNDONE => NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED,
      TXPHALIGNEN => '0',
      TXPHDLYPD => '0',
      TXPHDLYRESET => '0',
      TXPHDLYTSTCLK => '0',
      TXPHINIT => '0',
      TXPHINITDONE => NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED,
      TXPHOVRDEN => '0',
      TXPISOPD => '0',
      TXPMARESET => '0',
      TXPOLARITY => '0',
      TXPOSTCURSOR(4 downto 0) => B"00000",
      TXPOSTCURSORINV => '0',
      TXPRBSFORCEERR => '0',
      TXPRBSSEL(2 downto 0) => B"000",
      TXPRECURSOR(4 downto 0) => B"00000",
      TXPRECURSORINV => '0',
      TXQPIBIASEN => '0',
      TXQPISENN => NLW_gtxe2_i_TXQPISENN_UNCONNECTED,
      TXQPISENP => NLW_gtxe2_i_TXQPISENP_UNCONNECTED,
      TXQPISTRONGPDOWN => '0',
      TXQPIWEAKPUP => '0',
      TXRATE(2 downto 0) => B"000",
      TXRATEDONE => NLW_gtxe2_i_TXRATEDONE_UNCONNECTED,
      TXRESETDONE => gtxe2_i_2,
      TXSEQUENCE(6 downto 0) => B"0000000",
      TXSTARTSEQ => '0',
      TXSWING => '0',
      TXSYSCLKSEL(1 downto 0) => B"00",
      TXUSERRDY => gt0_txuserrdy_t,
      TXUSRCLK => gtxe2_i_8,
      TXUSRCLK2 => gtxe2_i_8
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_clocking is
  port (
    gtrefclk_out : out STD_LOGIC;
    gtrefclk_bufg : out STD_LOGIC;
    mmcm_locked : out STD_LOGIC;
    userclk : out STD_LOGIC;
    userclk2 : out STD_LOGIC;
    rxuserclk2_out : out STD_LOGIC;
    gtrefclk_p : in STD_LOGIC;
    gtrefclk_n : in STD_LOGIC;
    txoutclk : in STD_LOGIC;
    mmcm_reset : in STD_LOGIC;
    rxoutclk : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_clocking;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_clocking is
  signal clkfbout : STD_LOGIC;
  signal clkout0 : STD_LOGIC;
  signal clkout1 : STD_LOGIC;
  signal \^gtrefclk_out\ : STD_LOGIC;
  signal txoutclk_bufg : STD_LOGIC;
  signal NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_PSDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute box_type : string;
  attribute box_type of bufg_gtrefclk : label is "PRIMITIVE";
  attribute box_type of bufg_txoutclk : label is "PRIMITIVE";
  attribute box_type of bufg_userclk : label is "PRIMITIVE";
  attribute box_type of bufg_userclk2 : label is "PRIMITIVE";
  attribute box_type of ibufds_gtrefclk : label is "PRIMITIVE";
  attribute box_type of mmcm_adv_inst : label is "PRIMITIVE";
  attribute box_type of rxrecclkbufg : label is "PRIMITIVE";
begin
  gtrefclk_out <= \^gtrefclk_out\;
bufg_gtrefclk: unisim.vcomponents.BUFG
     port map (
      I => \^gtrefclk_out\,
      O => gtrefclk_bufg
    );
bufg_txoutclk: unisim.vcomponents.BUFG
     port map (
      I => txoutclk,
      O => txoutclk_bufg
    );
bufg_userclk: unisim.vcomponents.BUFG
     port map (
      I => clkout1,
      O => userclk
    );
bufg_userclk2: unisim.vcomponents.BUFG
     port map (
      I => clkout0,
      O => userclk2
    );
ibufds_gtrefclk: unisim.vcomponents.IBUFDS_GTE2
    generic map(
      CLKCM_CFG => true,
      CLKRCV_TRST => true,
      CLKSWING_CFG => B"11"
    )
        port map (
      CEB => '0',
      I => gtrefclk_p,
      IB => gtrefclk_n,
      O => \^gtrefclk_out\,
      ODIV2 => NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED
    );
mmcm_adv_inst: unisim.vcomponents.MMCME2_ADV
    generic map(
      BANDWIDTH => "OPTIMIZED",
      CLKFBOUT_MULT_F => 16.000000,
      CLKFBOUT_PHASE => 0.000000,
      CLKFBOUT_USE_FINE_PS => false,
      CLKIN1_PERIOD => 16.000000,
      CLKIN2_PERIOD => 0.000000,
      CLKOUT0_DIVIDE_F => 8.000000,
      CLKOUT0_DUTY_CYCLE => 0.500000,
      CLKOUT0_PHASE => 0.000000,
      CLKOUT0_USE_FINE_PS => false,
      CLKOUT1_DIVIDE => 16,
      CLKOUT1_DUTY_CYCLE => 0.500000,
      CLKOUT1_PHASE => 0.000000,
      CLKOUT1_USE_FINE_PS => false,
      CLKOUT2_DIVIDE => 1,
      CLKOUT2_DUTY_CYCLE => 0.500000,
      CLKOUT2_PHASE => 0.000000,
      CLKOUT2_USE_FINE_PS => false,
      CLKOUT3_DIVIDE => 1,
      CLKOUT3_DUTY_CYCLE => 0.500000,
      CLKOUT3_PHASE => 0.000000,
      CLKOUT3_USE_FINE_PS => false,
      CLKOUT4_CASCADE => false,
      CLKOUT4_DIVIDE => 1,
      CLKOUT4_DUTY_CYCLE => 0.500000,
      CLKOUT4_PHASE => 0.000000,
      CLKOUT4_USE_FINE_PS => false,
      CLKOUT5_DIVIDE => 1,
      CLKOUT5_DUTY_CYCLE => 0.500000,
      CLKOUT5_PHASE => 0.000000,
      CLKOUT5_USE_FINE_PS => false,
      CLKOUT6_DIVIDE => 1,
      CLKOUT6_DUTY_CYCLE => 0.500000,
      CLKOUT6_PHASE => 0.000000,
      CLKOUT6_USE_FINE_PS => false,
      COMPENSATION => "INTERNAL",
      DIVCLK_DIVIDE => 1,
      IS_CLKINSEL_INVERTED => '0',
      IS_PSEN_INVERTED => '0',
      IS_PSINCDEC_INVERTED => '0',
      IS_PWRDWN_INVERTED => '0',
      IS_RST_INVERTED => '0',
      REF_JITTER1 => 0.010000,
      REF_JITTER2 => 0.000000,
      SS_EN => "FALSE",
      SS_MODE => "CENTER_HIGH",
      SS_MOD_PERIOD => 10000,
      STARTUP_WAIT => false
    )
        port map (
      CLKFBIN => clkfbout,
      CLKFBOUT => clkfbout,
      CLKFBOUTB => NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED,
      CLKFBSTOPPED => NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED,
      CLKIN1 => txoutclk_bufg,
      CLKIN2 => '0',
      CLKINSEL => '1',
      CLKINSTOPPED => NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED,
      CLKOUT0 => clkout0,
      CLKOUT0B => NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED,
      CLKOUT1 => clkout1,
      CLKOUT1B => NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED,
      CLKOUT2 => NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED,
      CLKOUT2B => NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED,
      CLKOUT3 => NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED,
      CLKOUT3B => NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED,
      CLKOUT4 => NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED,
      CLKOUT5 => NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED,
      CLKOUT6 => NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED,
      DADDR(6 downto 0) => B"0000000",
      DCLK => '0',
      DEN => '0',
      DI(15 downto 0) => B"0000000000000000",
      DO(15 downto 0) => NLW_mmcm_adv_inst_DO_UNCONNECTED(15 downto 0),
      DRDY => NLW_mmcm_adv_inst_DRDY_UNCONNECTED,
      DWE => '0',
      LOCKED => mmcm_locked,
      PSCLK => '0',
      PSDONE => NLW_mmcm_adv_inst_PSDONE_UNCONNECTED,
      PSEN => '0',
      PSINCDEC => '0',
      PWRDWN => '0',
      RST => mmcm_reset
    );
rxrecclkbufg: unisim.vcomponents.BUFG
     port map (
      I => rxoutclk,
      O => rxuserclk2_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_cpll_railing is
  port (
    cpll_pd0_i : out STD_LOGIC;
    cpllreset_in : out STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gt0_cpllreset_t : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_cpll_railing;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_cpll_railing is
  signal cpll_reset_out : STD_LOGIC;
  signal \cpllpd_wait_reg[31]_srl32_n_1\ : STD_LOGIC;
  signal \cpllpd_wait_reg[63]_srl32_n_1\ : STD_LOGIC;
  signal \cpllpd_wait_reg[94]_srl31_n_0\ : STD_LOGIC;
  signal \cpllreset_wait_reg[126]_srl31_n_0\ : STD_LOGIC;
  signal \cpllreset_wait_reg[31]_srl32_n_1\ : STD_LOGIC;
  signal \cpllreset_wait_reg[63]_srl32_n_1\ : STD_LOGIC;
  signal \cpllreset_wait_reg[95]_srl32_n_1\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  attribute srl_bus_name : string;
  attribute srl_bus_name of \cpllpd_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name : string;
  attribute srl_name of \cpllpd_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[31]_srl32 ";
  attribute srl_bus_name of \cpllpd_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name of \cpllpd_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[63]_srl32 ";
  attribute srl_bus_name of \cpllpd_wait_reg[94]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name of \cpllpd_wait_reg[94]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[94]_srl31 ";
  attribute equivalent_register_removal : string;
  attribute equivalent_register_removal of \cpllpd_wait_reg[95]\ : label is "no";
  attribute srl_bus_name of \cpllreset_wait_reg[126]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[126]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[126]_srl31 ";
  attribute equivalent_register_removal of \cpllreset_wait_reg[127]\ : label is "no";
  attribute srl_bus_name of \cpllreset_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[31]_srl32 ";
  attribute srl_bus_name of \cpllreset_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[63]_srl32 ";
  attribute srl_bus_name of \cpllreset_wait_reg[95]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[95]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[95]_srl32 ";
begin
\cpllpd_wait_reg[31]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"FFFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => '0',
      Q => \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllpd_wait_reg[31]_srl32_n_1\
    );
\cpllpd_wait_reg[63]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"FFFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllpd_wait_reg[31]_srl32_n_1\,
      Q => \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllpd_wait_reg[63]_srl32_n_1\
    );
\cpllpd_wait_reg[94]_srl31\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11110",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllpd_wait_reg[63]_srl32_n_1\,
      Q => \cpllpd_wait_reg[94]_srl31_n_0\,
      Q31 => \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED\
    );
\cpllpd_wait_reg[95]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => gtrefclk_bufg,
      CE => '1',
      D => \cpllpd_wait_reg[94]_srl31_n_0\,
      Q => cpll_pd0_i,
      R => '0'
    );
\cpllreset_wait_reg[126]_srl31\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11110",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllreset_wait_reg[95]_srl32_n_1\,
      Q => \cpllreset_wait_reg[126]_srl31_n_0\,
      Q31 => \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED\
    );
\cpllreset_wait_reg[127]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gtrefclk_bufg,
      CE => '1',
      D => \cpllreset_wait_reg[126]_srl31_n_0\,
      Q => cpll_reset_out,
      R => '0'
    );
\cpllreset_wait_reg[31]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"000000FF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => '0',
      Q => \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[31]_srl32_n_1\
    );
\cpllreset_wait_reg[63]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllreset_wait_reg[31]_srl32_n_1\,
      Q => \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[63]_srl32_n_1\
    );
\cpllreset_wait_reg[95]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllreset_wait_reg[63]_srl32_n_1\,
      Q => \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[95]_srl32_n_1\
    );
gtxe2_i_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => cpll_reset_out,
      I1 => gt0_cpllreset_t,
      O => cpllreset_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_gt_common is
  port (
    gt0_qplloutclk_out : out STD_LOGIC;
    gt0_qplloutrefclk_out : out STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end gig_ethernet_pcs_pma_0_gt_common;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_gt_common is
  signal gtxe2_common_i_n_2 : STD_LOGIC;
  signal gtxe2_common_i_n_5 : STD_LOGIC;
  signal NLW_gtxe2_common_i_DRPRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_common_i_DRPDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute box_type : string;
  attribute box_type of gtxe2_common_i : label is "PRIMITIVE";
begin
gtxe2_common_i: unisim.vcomponents.GTXE2_COMMON
    generic map(
      BIAS_CFG => X"0000040000001000",
      COMMON_CFG => X"00000000",
      IS_DRPCLK_INVERTED => '0',
      IS_GTGREFCLK_INVERTED => '0',
      IS_QPLLLOCKDETCLK_INVERTED => '0',
      QPLL_CFG => X"06801C1",
      QPLL_CLKOUT_CFG => B"0000",
      QPLL_COARSE_FREQ_OVRD => B"010000",
      QPLL_COARSE_FREQ_OVRD_EN => '0',
      QPLL_CP => B"0000011111",
      QPLL_CP_MONITOR_EN => '0',
      QPLL_DMONITOR_SEL => '0',
      QPLL_FBDIV => B"0000100000",
      QPLL_FBDIV_MONITOR_EN => '0',
      QPLL_FBDIV_RATIO => '1',
      QPLL_INIT_CFG => X"000006",
      QPLL_LOCK_CFG => X"21E8",
      QPLL_LPF => B"1111",
      QPLL_REFCLK_DIV => 1,
      SIM_QPLLREFCLK_SEL => B"001",
      SIM_RESET_SPEEDUP => "FALSE",
      SIM_VERSION => "4.0"
    )
        port map (
      BGBYPASSB => '1',
      BGMONITORENB => '1',
      BGPDB => '1',
      BGRCALOVRD(4 downto 0) => B"11111",
      DRPADDR(7 downto 0) => B"00000000",
      DRPCLK => '0',
      DRPDI(15 downto 0) => B"0000000000000000",
      DRPDO(15 downto 0) => NLW_gtxe2_common_i_DRPDO_UNCONNECTED(15 downto 0),
      DRPEN => '0',
      DRPRDY => NLW_gtxe2_common_i_DRPRDY_UNCONNECTED,
      DRPWE => '0',
      GTGREFCLK => '0',
      GTNORTHREFCLK0 => '0',
      GTNORTHREFCLK1 => '0',
      GTREFCLK0 => gtrefclk_out,
      GTREFCLK1 => '0',
      GTSOUTHREFCLK0 => '0',
      GTSOUTHREFCLK1 => '0',
      PMARSVD(7 downto 0) => B"00000000",
      QPLLDMONITOR(7 downto 0) => NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED(7 downto 0),
      QPLLFBCLKLOST => NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED,
      QPLLLOCK => gtxe2_common_i_n_2,
      QPLLLOCKDETCLK => independent_clock_bufg,
      QPLLLOCKEN => '1',
      QPLLOUTCLK => gt0_qplloutclk_out,
      QPLLOUTREFCLK => gt0_qplloutrefclk_out,
      QPLLOUTRESET => '0',
      QPLLPD => '1',
      QPLLREFCLKLOST => gtxe2_common_i_n_5,
      QPLLREFCLKSEL(2 downto 0) => B"001",
      QPLLRESET => \out\(0),
      QPLLRSVD1(15 downto 0) => B"0000000000000000",
      QPLLRSVD2(4 downto 0) => B"11111",
      RCALENB => '1',
      REFCLKOUTMONITOR => NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_reset_sync is
  port (
    reset_out : out STD_LOGIC;
    CLK : in STD_LOGIC;
    enablealign : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_reset_sync;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_reset_sync is
  signal reset_sync_reg1 : STD_LOGIC;
  signal reset_sync_reg2 : STD_LOGIC;
  signal reset_sync_reg3 : STD_LOGIC;
  signal reset_sync_reg4 : STD_LOGIC;
  signal reset_sync_reg5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of reset_sync1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of reset_sync1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of reset_sync1 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of reset_sync1 : label is "VCC:CE";
  attribute box_type : string;
  attribute box_type of reset_sync1 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync2 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync2 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync2 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync2 : label is "VCC:CE";
  attribute box_type of reset_sync2 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync3 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync3 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync3 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync3 : label is "VCC:CE";
  attribute box_type of reset_sync3 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync4 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync4 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync4 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync4 : label is "VCC:CE";
  attribute box_type of reset_sync4 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync5 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync5 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync5 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync5 : label is "VCC:CE";
  attribute box_type of reset_sync5 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync6 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync6 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync6 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync6 : label is "VCC:CE";
  attribute box_type of reset_sync6 : label is "PRIMITIVE";
begin
reset_sync1: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => '0',
      PRE => enablealign,
      Q => reset_sync_reg1
    );
reset_sync2: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg1,
      PRE => enablealign,
      Q => reset_sync_reg2
    );
reset_sync3: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg2,
      PRE => enablealign,
      Q => reset_sync_reg3
    );
reset_sync4: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg3,
      PRE => enablealign,
      Q => reset_sync_reg4
    );
reset_sync5: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg4,
      PRE => enablealign,
      Q => reset_sync_reg5
    );
reset_sync6: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg5,
      PRE => '0',
      Q => reset_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_reset_sync_1 is
  port (
    reset_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_reset_sync_1 : entity is "gig_ethernet_pcs_pma_0_reset_sync";
end gig_ethernet_pcs_pma_0_reset_sync_1;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_reset_sync_1 is
  signal reset_sync_reg1 : STD_LOGIC;
  signal reset_sync_reg2 : STD_LOGIC;
  signal reset_sync_reg3 : STD_LOGIC;
  signal reset_sync_reg4 : STD_LOGIC;
  signal reset_sync_reg5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of reset_sync1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of reset_sync1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of reset_sync1 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of reset_sync1 : label is "VCC:CE";
  attribute box_type : string;
  attribute box_type of reset_sync1 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync2 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync2 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync2 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync2 : label is "VCC:CE";
  attribute box_type of reset_sync2 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync3 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync3 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync3 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync3 : label is "VCC:CE";
  attribute box_type of reset_sync3 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync4 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync4 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync4 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync4 : label is "VCC:CE";
  attribute box_type of reset_sync4 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync5 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync5 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync5 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync5 : label is "VCC:CE";
  attribute box_type of reset_sync5 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync6 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync6 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync6 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync6 : label is "VCC:CE";
  attribute box_type of reset_sync6 : label is "PRIMITIVE";
begin
reset_sync1: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => '0',
      PRE => SR(0),
      Q => reset_sync_reg1
    );
reset_sync2: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg1,
      PRE => SR(0),
      Q => reset_sync_reg2
    );
reset_sync3: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg2,
      PRE => SR(0),
      Q => reset_sync_reg3
    );
reset_sync4: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg3,
      PRE => SR(0),
      Q => reset_sync_reg4
    );
reset_sync5: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg4,
      PRE => SR(0),
      Q => reset_sync_reg5
    );
reset_sync6: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg5,
      PRE => '0',
      Q => reset_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_reset_sync_2 is
  port (
    reset_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    reset_sync5_0 : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_reset_sync_2 : entity is "gig_ethernet_pcs_pma_0_reset_sync";
end gig_ethernet_pcs_pma_0_reset_sync_2;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_reset_sync_2 is
  signal reset_sync_reg1 : STD_LOGIC;
  signal reset_sync_reg2 : STD_LOGIC;
  signal reset_sync_reg3 : STD_LOGIC;
  signal reset_sync_reg4 : STD_LOGIC;
  signal reset_sync_reg5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of reset_sync1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of reset_sync1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of reset_sync1 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of reset_sync1 : label is "VCC:CE";
  attribute box_type : string;
  attribute box_type of reset_sync1 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync2 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync2 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync2 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync2 : label is "VCC:CE";
  attribute box_type of reset_sync2 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync3 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync3 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync3 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync3 : label is "VCC:CE";
  attribute box_type of reset_sync3 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync4 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync4 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync4 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync4 : label is "VCC:CE";
  attribute box_type of reset_sync4 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync5 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync5 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync5 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync5 : label is "VCC:CE";
  attribute box_type of reset_sync5 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync6 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync6 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync6 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync6 : label is "VCC:CE";
  attribute box_type of reset_sync6 : label is "PRIMITIVE";
begin
reset_sync1: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => '0',
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg1
    );
reset_sync2: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg1,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg2
    );
reset_sync3: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg2,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg3
    );
reset_sync4: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg3,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg4
    );
reset_sync5: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg4,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg5
    );
reset_sync6: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg5,
      PRE => '0',
      Q => reset_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_reset_wtd_timer is
  port (
    reset : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    data_out : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_reset_wtd_timer;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_reset_wtd_timer is
  signal \counter_stg1[5]_i_1_n_0\ : STD_LOGIC;
  signal \counter_stg1[5]_i_3_n_0\ : STD_LOGIC;
  signal counter_stg1_reg : STD_LOGIC_VECTOR ( 5 to 5 );
  signal \counter_stg1_reg__0\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \counter_stg2[0]_i_3_n_0\ : STD_LOGIC;
  signal counter_stg2_reg : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal \counter_stg2_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal counter_stg30 : STD_LOGIC;
  signal \counter_stg3[0]_i_3_n_0\ : STD_LOGIC;
  signal \counter_stg3[0]_i_4_n_0\ : STD_LOGIC;
  signal \counter_stg3[0]_i_5_n_0\ : STD_LOGIC;
  signal counter_stg3_reg : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal \counter_stg3_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal eqOp : STD_LOGIC;
  signal plusOp : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal reset0 : STD_LOGIC;
  signal reset_i_2_n_0 : STD_LOGIC;
  signal reset_i_3_n_0 : STD_LOGIC;
  signal reset_i_4_n_0 : STD_LOGIC;
  signal reset_i_5_n_0 : STD_LOGIC;
  signal reset_i_6_n_0 : STD_LOGIC;
  signal \NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \counter_stg1[0]_i_1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \counter_stg1[1]_i_1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \counter_stg1[2]_i_1\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \counter_stg1[3]_i_1\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \counter_stg1[4]_i_1\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \counter_stg1[5]_i_3\ : label is "soft_lutpair71";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \counter_stg2_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg2_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg2_reg[8]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg3_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg3_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg3_reg[8]_i_1\ : label is 11;
begin
\counter_stg1[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \counter_stg1_reg__0\(0),
      O => plusOp(0)
    );
\counter_stg1[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \counter_stg1_reg__0\(0),
      I1 => \counter_stg1_reg__0\(1),
      O => plusOp(1)
    );
\counter_stg1[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \counter_stg1_reg__0\(1),
      I1 => \counter_stg1_reg__0\(0),
      I2 => \counter_stg1_reg__0\(2),
      O => plusOp(2)
    );
\counter_stg1[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \counter_stg1_reg__0\(2),
      I1 => \counter_stg1_reg__0\(0),
      I2 => \counter_stg1_reg__0\(1),
      I3 => \counter_stg1_reg__0\(3),
      O => plusOp(3)
    );
\counter_stg1[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(3),
      I1 => \counter_stg1_reg__0\(1),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(2),
      I4 => \counter_stg1_reg__0\(4),
      O => plusOp(4)
    );
\counter_stg1[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF2000"
    )
        port map (
      I0 => reset_i_2_n_0,
      I1 => counter_stg3_reg(0),
      I2 => reset_i_3_n_0,
      I3 => \counter_stg1[5]_i_3_n_0\,
      I4 => data_out,
      O => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(4),
      I1 => \counter_stg1_reg__0\(2),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(1),
      I4 => \counter_stg1_reg__0\(3),
      I5 => counter_stg1_reg(5),
      O => plusOp(5)
    );
\counter_stg1[5]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(3),
      I1 => \counter_stg1_reg__0\(1),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(2),
      I4 => \counter_stg1_reg__0\(4),
      O => \counter_stg1[5]_i_3_n_0\
    );
\counter_stg1_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(0),
      Q => \counter_stg1_reg__0\(0),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(1),
      Q => \counter_stg1_reg__0\(1),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(2),
      Q => \counter_stg1_reg__0\(2),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(3),
      Q => \counter_stg1_reg__0\(3),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(4),
      Q => \counter_stg1_reg__0\(4),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(5),
      Q => counter_stg1_reg(5),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(4),
      I1 => \counter_stg1_reg__0\(2),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(1),
      I4 => \counter_stg1_reg__0\(3),
      I5 => counter_stg1_reg(5),
      O => eqOp
    );
\counter_stg2[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => counter_stg2_reg(0),
      O => \counter_stg2[0]_i_3_n_0\
    );
\counter_stg2_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_7\,
      Q => counter_stg2_reg(0),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \counter_stg2_reg[0]_i_2_n_0\,
      CO(2) => \counter_stg2_reg[0]_i_2_n_1\,
      CO(1) => \counter_stg2_reg[0]_i_2_n_2\,
      CO(0) => \counter_stg2_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \counter_stg2_reg[0]_i_2_n_4\,
      O(2) => \counter_stg2_reg[0]_i_2_n_5\,
      O(1) => \counter_stg2_reg[0]_i_2_n_6\,
      O(0) => \counter_stg2_reg[0]_i_2_n_7\,
      S(3 downto 1) => counter_stg2_reg(3 downto 1),
      S(0) => \counter_stg2[0]_i_3_n_0\
    );
\counter_stg2_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_5\,
      Q => counter_stg2_reg(10),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_4\,
      Q => counter_stg2_reg(11),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_6\,
      Q => counter_stg2_reg(1),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_5\,
      Q => counter_stg2_reg(2),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_4\,
      Q => counter_stg2_reg(3),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_7\,
      Q => counter_stg2_reg(4),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg2_reg[0]_i_2_n_0\,
      CO(3) => \counter_stg2_reg[4]_i_1_n_0\,
      CO(2) => \counter_stg2_reg[4]_i_1_n_1\,
      CO(1) => \counter_stg2_reg[4]_i_1_n_2\,
      CO(0) => \counter_stg2_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg2_reg[4]_i_1_n_4\,
      O(2) => \counter_stg2_reg[4]_i_1_n_5\,
      O(1) => \counter_stg2_reg[4]_i_1_n_6\,
      O(0) => \counter_stg2_reg[4]_i_1_n_7\,
      S(3 downto 0) => counter_stg2_reg(7 downto 4)
    );
\counter_stg2_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_6\,
      Q => counter_stg2_reg(5),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_5\,
      Q => counter_stg2_reg(6),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_4\,
      Q => counter_stg2_reg(7),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_7\,
      Q => counter_stg2_reg(8),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg2_reg[4]_i_1_n_0\,
      CO(3) => \NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \counter_stg2_reg[8]_i_1_n_1\,
      CO(1) => \counter_stg2_reg[8]_i_1_n_2\,
      CO(0) => \counter_stg2_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg2_reg[8]_i_1_n_4\,
      O(2) => \counter_stg2_reg[8]_i_1_n_5\,
      O(1) => \counter_stg2_reg[8]_i_1_n_6\,
      O(0) => \counter_stg2_reg[8]_i_1_n_7\,
      S(3 downto 0) => counter_stg2_reg(11 downto 8)
    );
\counter_stg2_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_6\,
      Q => counter_stg2_reg(9),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \counter_stg3[0]_i_3_n_0\,
      I1 => \counter_stg3[0]_i_4_n_0\,
      I2 => counter_stg2_reg(0),
      I3 => \counter_stg1[5]_i_3_n_0\,
      O => counter_stg30
    );
\counter_stg3[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => counter_stg2_reg(3),
      I1 => counter_stg2_reg(4),
      I2 => counter_stg2_reg(1),
      I3 => counter_stg2_reg(2),
      I4 => counter_stg2_reg(6),
      I5 => counter_stg2_reg(5),
      O => \counter_stg3[0]_i_3_n_0\
    );
\counter_stg3[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => counter_stg2_reg(9),
      I1 => counter_stg2_reg(10),
      I2 => counter_stg2_reg(7),
      I3 => counter_stg2_reg(8),
      I4 => counter_stg1_reg(5),
      I5 => counter_stg2_reg(11),
      O => \counter_stg3[0]_i_4_n_0\
    );
\counter_stg3[0]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => counter_stg3_reg(0),
      O => \counter_stg3[0]_i_5_n_0\
    );
\counter_stg3_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_7\,
      Q => counter_stg3_reg(0),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \counter_stg3_reg[0]_i_2_n_0\,
      CO(2) => \counter_stg3_reg[0]_i_2_n_1\,
      CO(1) => \counter_stg3_reg[0]_i_2_n_2\,
      CO(0) => \counter_stg3_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \counter_stg3_reg[0]_i_2_n_4\,
      O(2) => \counter_stg3_reg[0]_i_2_n_5\,
      O(1) => \counter_stg3_reg[0]_i_2_n_6\,
      O(0) => \counter_stg3_reg[0]_i_2_n_7\,
      S(3 downto 1) => counter_stg3_reg(3 downto 1),
      S(0) => \counter_stg3[0]_i_5_n_0\
    );
\counter_stg3_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_5\,
      Q => counter_stg3_reg(10),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_4\,
      Q => counter_stg3_reg(11),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_6\,
      Q => counter_stg3_reg(1),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_5\,
      Q => counter_stg3_reg(2),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_4\,
      Q => counter_stg3_reg(3),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_7\,
      Q => counter_stg3_reg(4),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg3_reg[0]_i_2_n_0\,
      CO(3) => \counter_stg3_reg[4]_i_1_n_0\,
      CO(2) => \counter_stg3_reg[4]_i_1_n_1\,
      CO(1) => \counter_stg3_reg[4]_i_1_n_2\,
      CO(0) => \counter_stg3_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg3_reg[4]_i_1_n_4\,
      O(2) => \counter_stg3_reg[4]_i_1_n_5\,
      O(1) => \counter_stg3_reg[4]_i_1_n_6\,
      O(0) => \counter_stg3_reg[4]_i_1_n_7\,
      S(3 downto 0) => counter_stg3_reg(7 downto 4)
    );
\counter_stg3_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_6\,
      Q => counter_stg3_reg(5),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_5\,
      Q => counter_stg3_reg(6),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_4\,
      Q => counter_stg3_reg(7),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_7\,
      Q => counter_stg3_reg(8),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg3_reg[4]_i_1_n_0\,
      CO(3) => \NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \counter_stg3_reg[8]_i_1_n_1\,
      CO(1) => \counter_stg3_reg[8]_i_1_n_2\,
      CO(0) => \counter_stg3_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg3_reg[8]_i_1_n_4\,
      O(2) => \counter_stg3_reg[8]_i_1_n_5\,
      O(1) => \counter_stg3_reg[8]_i_1_n_6\,
      O(0) => \counter_stg3_reg[8]_i_1_n_7\,
      S(3 downto 0) => counter_stg3_reg(11 downto 8)
    );
\counter_stg3_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_6\,
      Q => counter_stg3_reg(9),
      R => \counter_stg1[5]_i_1_n_0\
    );
reset_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => reset_i_2_n_0,
      I1 => counter_stg3_reg(0),
      I2 => reset_i_3_n_0,
      O => reset0
    );
reset_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000001000000000"
    )
        port map (
      I0 => counter_stg3_reg(9),
      I1 => counter_stg3_reg(10),
      I2 => counter_stg3_reg(7),
      I3 => counter_stg3_reg(8),
      I4 => counter_stg2_reg(0),
      I5 => counter_stg3_reg(11),
      O => reset_i_2_n_0
    );
reset_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => reset_i_4_n_0,
      I1 => reset_i_5_n_0,
      I2 => reset_i_6_n_0,
      O => reset_i_3_n_0
    );
reset_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000008"
    )
        port map (
      I0 => counter_stg2_reg(3),
      I1 => counter_stg2_reg(4),
      I2 => counter_stg2_reg(1),
      I3 => counter_stg2_reg(2),
      I4 => counter_stg2_reg(6),
      I5 => counter_stg2_reg(5),
      O => reset_i_4_n_0
    );
reset_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0020000000000000"
    )
        port map (
      I0 => counter_stg2_reg(10),
      I1 => counter_stg2_reg(9),
      I2 => counter_stg2_reg(8),
      I3 => counter_stg2_reg(7),
      I4 => counter_stg1_reg(5),
      I5 => counter_stg2_reg(11),
      O => reset_i_5_n_0
    );
reset_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0002000000000000"
    )
        port map (
      I0 => counter_stg3_reg(4),
      I1 => counter_stg3_reg(3),
      I2 => counter_stg3_reg(1),
      I3 => counter_stg3_reg(2),
      I4 => counter_stg3_reg(6),
      I5 => counter_stg3_reg(5),
      O => reset_i_6_n_0
    );
reset_reg: unisim.vcomponents.FDRE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset0,
      Q => reset,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_resets is
  port (
    \out\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    independent_clock_bufg : in STD_LOGIC;
    reset : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_resets;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_resets is
  signal pma_reset_pipe : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg : string;
  attribute async_reg of pma_reset_pipe : signal is "true";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \pma_reset_pipe_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[1]\ : label is std.standard.true;
  attribute KEEP of \pma_reset_pipe_reg[1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[2]\ : label is std.standard.true;
  attribute KEEP of \pma_reset_pipe_reg[2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[3]\ : label is std.standard.true;
  attribute KEEP of \pma_reset_pipe_reg[3]\ : label is "yes";
begin
  \out\(0) <= pma_reset_pipe(3);
\pma_reset_pipe_reg[0]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => '0',
      PRE => reset,
      Q => pma_reset_pipe(0)
    );
\pma_reset_pipe_reg[1]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pma_reset_pipe(0),
      PRE => reset,
      Q => pma_reset_pipe(1)
    );
\pma_reset_pipe_reg[2]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pma_reset_pipe(1),
      PRE => reset,
      Q => pma_reset_pipe(2)
    );
\pma_reset_pipe_reg[3]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pma_reset_pipe(2),
      PRE => reset,
      Q => pma_reset_pipe(3)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    CLK : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_sync_block;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_0 is
  port (
    resetdone : out STD_LOGIC;
    resetdone_0 : in STD_LOGIC;
    data_in : in STD_LOGIC;
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_0 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_0;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_0 is
  signal data_out : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
resetdone_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => data_out,
      I1 => resetdone_0,
      O => resetdone
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_10 is
  port (
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_10 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_10;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_10 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_11 is
  port (
    \FSM_sequential_rx_state_reg[1]\ : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 2 downto 0 );
    rxresetdone_s3 : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_11 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_11;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_11 is
  signal cplllock_sync : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
\FSM_sequential_rx_state[3]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"008F0080"
    )
        port map (
      I0 => Q(0),
      I1 => rxresetdone_s3,
      I2 => Q(1),
      I3 => Q(2),
      I4 => cplllock_sync,
      O => \FSM_sequential_rx_state_reg[1]\
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => cplllock_sync,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_12 is
  port (
    \FSM_sequential_rx_state_reg[1]\ : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 2 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    reset_time_out_reg : in STD_LOGIC;
    reset_time_out_reg_0 : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    reset_time_out_reg_1 : in STD_LOGIC;
    reset_time_out_reg_2 : in STD_LOGIC;
    data_in : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[1]_0\ : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_0 : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_1 : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]\ : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_0\ : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_1\ : in STD_LOGIC;
    mmcm_lock_reclocked : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_2\ : in STD_LOGIC;
    time_out_wait_bypass_s3 : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[3]\ : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_3\ : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_2 : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_3 : in STD_LOGIC;
    data_out : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_12 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_12;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_12 is
  signal \FSM_sequential_rx_state[0]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[1]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_8_n_0\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  signal data_valid_sync : STD_LOGIC;
  signal reset_time_out_i_2_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_3_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_4_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[0]_i_3\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[3]_i_8\ : label is "soft_lutpair41";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
\FSM_sequential_rx_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEFEFEF"
    )
        port map (
      I0 => \FSM_sequential_rx_state_reg[0]_2\,
      I1 => \FSM_sequential_rx_state[0]_i_3_n_0\,
      I2 => Q(0),
      I3 => Q(1),
      I4 => Q(3),
      O => D(0)
    );
\FSM_sequential_rx_state[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => Q(3),
      I1 => reset_time_out_reg_2,
      I2 => data_valid_sync,
      I3 => rx_fsm_reset_done_int_reg_1,
      O => \FSM_sequential_rx_state[0]_i_3_n_0\
    );
\FSM_sequential_rx_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF24200400"
    )
        port map (
      I0 => Q(0),
      I1 => Q(1),
      I2 => Q(3),
      I3 => Q(2),
      I4 => \FSM_sequential_rx_state[1]_i_2_n_0\,
      I5 => \FSM_sequential_rx_state_reg[1]_0\,
      O => D(1)
    );
\FSM_sequential_rx_state[1]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => data_valid_sync,
      I1 => rx_fsm_reset_done_int_reg_1,
      O => \FSM_sequential_rx_state[1]_i_2_n_0\
    );
\FSM_sequential_rx_state[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFEEE"
    )
        port map (
      I0 => \FSM_sequential_rx_state_reg[0]\,
      I1 => \FSM_sequential_rx_state[3]_i_4_n_0\,
      I2 => Q(0),
      I3 => reset_time_out_reg,
      I4 => \FSM_sequential_rx_state[3]_i_6_n_0\,
      I5 => \FSM_sequential_rx_state_reg[0]_0\,
      O => E(0)
    );
\FSM_sequential_rx_state[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFCCC0C4C4"
    )
        port map (
      I0 => time_out_wait_bypass_s3,
      I1 => Q(3),
      I2 => Q(1),
      I3 => \FSM_sequential_rx_state[3]_i_8_n_0\,
      I4 => Q(0),
      I5 => \FSM_sequential_rx_state_reg[3]\,
      O => D(2)
    );
\FSM_sequential_rx_state[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAEFEA"
    )
        port map (
      I0 => \FSM_sequential_rx_state[0]_i_3_n_0\,
      I1 => \FSM_sequential_rx_state_reg[0]_1\,
      I2 => Q(2),
      I3 => \FSM_sequential_rx_state_reg[0]_3\,
      I4 => Q(0),
      I5 => Q(1),
      O => \FSM_sequential_rx_state[3]_i_4_n_0\
    );
\FSM_sequential_rx_state[3]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0CE20CCC"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => Q(3),
      I2 => data_valid_sync,
      I3 => Q(1),
      I4 => Q(0),
      O => \FSM_sequential_rx_state[3]_i_6_n_0\
    );
\FSM_sequential_rx_state[3]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => rx_fsm_reset_done_int_reg_1,
      I1 => data_valid_sync,
      I2 => reset_time_out_reg_2,
      O => \FSM_sequential_rx_state[3]_i_8_n_0\
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_out,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_valid_sync,
      R => '0'
    );
\reset_time_out_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEFFFFFEEEF0000"
    )
        port map (
      I0 => reset_time_out_i_2_n_0,
      I1 => reset_time_out_reg,
      I2 => reset_time_out_reg_0,
      I3 => Q(1),
      I4 => reset_time_out_reg_1,
      I5 => reset_time_out_reg_2,
      O => \FSM_sequential_rx_state_reg[1]\
    );
reset_time_out_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0FF30E0E0FF30202"
    )
        port map (
      I0 => \FSM_sequential_rx_state_reg[0]_1\,
      I1 => Q(0),
      I2 => Q(1),
      I3 => data_valid_sync,
      I4 => Q(3),
      I5 => mmcm_lock_reclocked,
      O => reset_time_out_i_2_n_0
    );
rx_fsm_reset_done_int_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"ABA8"
    )
        port map (
      I0 => rx_fsm_reset_done_int,
      I1 => rx_fsm_reset_done_int_i_3_n_0,
      I2 => rx_fsm_reset_done_int_i_4_n_0,
      I3 => data_in,
      O => rx_fsm_reset_done_int_reg
    );
rx_fsm_reset_done_int_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00040000"
    )
        port map (
      I0 => Q(0),
      I1 => data_valid_sync,
      I2 => Q(2),
      I3 => reset_time_out_reg_2,
      I4 => rx_fsm_reset_done_int_reg_2,
      O => rx_fsm_reset_done_int
    );
rx_fsm_reset_done_int_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0400040004040400"
    )
        port map (
      I0 => rx_fsm_reset_done_int_reg_0,
      I1 => Q(3),
      I2 => Q(2),
      I3 => data_valid_sync,
      I4 => rx_fsm_reset_done_int_reg_1,
      I5 => reset_time_out_reg_2,
      O => rx_fsm_reset_done_int_i_3_n_0
    );
rx_fsm_reset_done_int_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0008000808080008"
    )
        port map (
      I0 => rx_fsm_reset_done_int_reg_3,
      I1 => Q(1),
      I2 => Q(0),
      I3 => data_valid_sync,
      I4 => rx_fsm_reset_done_int_reg_2,
      I5 => reset_time_out_reg_2,
      O => rx_fsm_reset_done_int_i_4_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_13 is
  port (
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_13 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_13;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_13 is
  signal \^data_out\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
  data_out <= \^data_out\;
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => \^data_out\,
      R => '0'
    );
\mmcm_lock_count[7]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^data_out\,
      O => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_14 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_14 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_14;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_14 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_15 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_15 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_15;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_15 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_16 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg6_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_16 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_16;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_16 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_3 is
  port (
    data_out : out STD_LOGIC;
    status_vector : in STD_LOGIC_VECTOR ( 0 to 0 );
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_3 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_3;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_3 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => status_vector(0),
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_4 is
  port (
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_4 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_4;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_4 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_5 is
  port (
    reset_time_out_reg : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    reset_time_out_reg_0 : in STD_LOGIC;
    reset_time_out : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_0\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_1\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_2\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_3\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    reset_time_out_reg_1 : in STD_LOGIC;
    mmcm_lock_reclocked : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_4\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_5\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_6\ : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_5 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_5;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_5 is
  signal \FSM_sequential_tx_state[3]_i_5_n_0\ : STD_LOGIC;
  signal cplllock_sync : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  signal \reset_time_out_i_3__0_n_0\ : STD_LOGIC;
  signal \reset_time_out_i_4__0_n_0\ : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
\FSM_sequential_tx_state[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFEFFFE"
    )
        port map (
      I0 => \FSM_sequential_tx_state_reg[0]\,
      I1 => \FSM_sequential_tx_state_reg[0]_0\,
      I2 => \FSM_sequential_tx_state[3]_i_5_n_0\,
      I3 => \FSM_sequential_tx_state_reg[0]_1\,
      I4 => \FSM_sequential_tx_state_reg[0]_2\,
      I5 => \FSM_sequential_tx_state_reg[0]_3\,
      O => E(0)
    );
\FSM_sequential_tx_state[3]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000F00008"
    )
        port map (
      I0 => \FSM_sequential_tx_state_reg[0]_4\,
      I1 => \FSM_sequential_tx_state_reg[0]_5\,
      I2 => cplllock_sync,
      I3 => Q(2),
      I4 => Q(1),
      I5 => \FSM_sequential_tx_state_reg[0]_6\,
      O => \FSM_sequential_tx_state[3]_i_5_n_0\
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => cplllock_sync,
      R => '0'
    );
reset_time_out_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => reset_time_out_reg_0,
      I1 => \reset_time_out_i_3__0_n_0\,
      I2 => \reset_time_out_i_4__0_n_0\,
      I3 => reset_time_out,
      O => reset_time_out_reg
    );
\reset_time_out_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"020002000F000200"
    )
        port map (
      I0 => cplllock_sync,
      I1 => Q(2),
      I2 => Q(3),
      I3 => Q(0),
      I4 => mmcm_lock_reclocked,
      I5 => Q(1),
      O => \reset_time_out_i_3__0_n_0\
    );
\reset_time_out_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0505FF040505F504"
    )
        port map (
      I0 => Q(1),
      I1 => reset_time_out_reg_1,
      I2 => Q(2),
      I3 => Q(0),
      I4 => Q(3),
      I5 => cplllock_sync,
      O => \reset_time_out_i_4__0_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_6 is
  port (
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_6 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_6;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_6 is
  signal \^data_out\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
  data_out <= \^data_out\;
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => \^data_out\,
      R => '0'
    );
\mmcm_lock_count[7]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^data_out\,
      O => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_7 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg6_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_7 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_7;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_7 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_8 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_8 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_8;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_8 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_9 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_9 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_9;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_9 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2023.2"
`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
TCSZ2Ye5IRfHavlF8Mno1PI9xixWuSiNh3ssU1FQtkjW1fmNtc2c3x12slL242UQayI0rzZTqe6S
edtecLHTOnzxXpCZjjU8NFmgLPerTSDZ1W5YhyIi9j0Ap4YBpvaA1ojM0+r0Cx+dMOXohQGeyljq
+fnTaFTUe2678DxpqHk=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
NIzZVKMB1/8UX2qb4BB+VXCafEKXsrn7axB1cJDPqDCzSyt/2KG1NEEZTDHZVzIr8Bf9501PyXmL
VowTAAXX/RopKyKOM1xJN/qLtqXxegH2a4dIkUxDIIclIcbv/smna9VCwI7m6JhrnKsNciTTilgR
27S/h6JPpZsZAEmsNxxTC70WQhQSM8TlHJjZg3KDc5KTnvC/mVTk6I05U6x0Bdd1YR9GBvhwRqhP
B1ukL/1JVOwR9Ce9p+EHFE/xyApypCjQPGwq+8IFQgS8wltVZHX6eSMw17Q0wGCY+LHduRTA+abV
LvAR0NPf7PKQUSCECe2mBbLPO7wD4BO5RAkJeA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
udw4XxxzcaDNM1qWnRgZ2JEM1MMqnKwKVqha/krU9EyUAsyATjQEMBqjlOHw5QXMU2jjizlL20Nl
h2pF7iKo1S+7TS54Y/UIJANp+Dl46V/qfy6/yBnE4YclHON1k0jRao4C6T951tgXuCAIQEmXbr87
aJfL2dNqORH+TDKUBdc=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
JFe89rPDaiIUicPDNoXCg8pJUoYRyVDoW/5yE4T0Cp9xDHtuIyKQVbC7jVb92OsgJ5GHDm7DH2D2
rYZKrdCIqPt2jo7DG6bcJuDFcisZb11HLlYWNsK2Vqs9DdsTPViykeE05CD5AgfDxb983x8F1meK
w8zjeGoD44djsaRA+lvP1zLhl24q5LWFJdPSyIT7uWZwhxHqlyJu85ToXLuwZQZO76Mp+1mitxDy
vleizC5rnk/4hqxfEFS21Qi1TwCz5hdU+H3nA3dTe1KRY+obbFP7sRWKfmr9Rcf9enRvbaEbLoJA
9ADkl72jc1Aqlnd+YCGq4EmbElbWLxblpamncA==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IV5qOrW4jXfiGt6hz/YcNm+/H3ij0v503CF3Kvut91tUWldzNzyLt4pIZEWEzSmn6RcpcLNN88po
1kt45UdSBz+mL5HDQaw4J+VGD/cCBmW1jnOclCf82kwju1MIDfa2EKicjqaykCUROxV7cwg07FFp
clLfIwd4kxgSWnGzeZi1IGezx7OpBsAkBTz9ha4WttEm0+D29DF9O4GaQl6q8IBeA0QIrO10EESt
slfRi2evxdOeTZBVFoXU91OszneH/prZqyCsHeyvTa8PABTZ+Y4CH6ICZCXRn7QTNJgoYSGABuPs
87saNJgzomjyaO6IzGl1fBgMIsIurKw90DE8Jw==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2022_10", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Eek/EeBa5kEiakvxzHC3fZ0NXCsvWnLN8FYKLyImepfBUv2jdVDY2j6Qs928DJaMO8pBuO9SGcu3
47rhcN1DAjZza5Ac83W24fngY1+YWblivVc8AoggXS1t2Y7Dy1vf9+ZxUdOvq63sje+fDJxapZwK
3HQGdtBX86RTaUS5K+HyI1FTmcIhUYmJWmxQjIxLla7FF1QZ4XpTCfqAG5i7ZKlYSoDFb8sjCRG4
XWFuk1dbL2UfZPxXZ7XHIm+03Ck/JsHtsjLCc8oTB/9MLom2HX9SjX8H6tFbEXR1NatCFWQ04JKL
kHSYD/xDlwjhN9CRvowRhNJaYSmKQT646hlNoA==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
gaID+cgqOdyoJPDKM9JAyimEjoxNakxuNjYf52N14HEyn4NQksF7thq/bXWc82vmfdc8aodx1+ky
i8uuKszW1WwV+apGSqk7YXBCxx3ACfMsPzNzeDQ2HVzGfznpQD80Eu7I7iwtz3k5Mr31iaeM1kQa
oddk6CkVESI8CD21PQHMVeu0LKLZJp8k8NHf3i0UOXsP5o768iecieYQh2VYXZ6HORDDyd+IpDB1
CAFBZctXco8C1w74wCB0LXUSYInc5ythxBURkPPTJ1GBuXpoQGZD2sNiI2Htl0y1toEdfgExWZ+0
+4Docnd9TgOGhAhZzUcj3c+6cQNbgCB847/G6w==

`protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`protect key_block
tEBTlBoXowC0cOrrCu9pEZ6t9FjMS6GWThbzsXMvR2xb9HBcccdvXT7EfASM09KkNzvlYoyNBWct
0TRl1BNzzlomu3G857U6kezS+CCRF/K2qOhhxHFxEfuM0qblRVdNHoCGGMM4PkE/rt9M7IqYoXQg
WOHI1ydpZZn08aVL9QYJgz6ZuVHNLwSpL6rjFFDXV1cB82gVFBkRP/0NxpGW2WH6YA/MJ0czV0ji
o0umOWluEwUObdytKX1lfuNYimI0ziWrovqq2osL8J7NBKDUl2R6gJ51DObsTBgC8uyUHVibyNHy
nhzTpwcBeeXdtAueCg1BlHDIwglcMUdy0sBZEyHM/CLzpxgr1A+uUcmzlWx1drrc8lRNwGMFDDJQ
9OzoHBABtNt8N3bbO8A+rE9HtsjMVr1TxHhUTxBhWcypwra+xzsGykln/IP3JBwwQR0+d+V8/Vec
5Bh03crJTvJZUbYidozNoaPOfnHi0NxFDNdL7L1i75T+H6bqeE1ADR/4

`protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GM0AtcDH85MjCjnA/pijf0ZCJap8X+OYUX7W1StOwznqG2XPd9DhtvTyqD0c8/7BTdeCzGUK5iqe
QiGwEcy1dCrSVZW0KtjFXllkYV2ai3/Qn7Bgg1YuzxifEFKe6ClTsByfgjqRdyZeNSAldwvx9ZtT
0ZhijV96K37zXwfXFeKDmxOZOV553ovWfXGekaS1EPmSluoDYBMQKc2XV+ZUXR7n5NI/6E3QdK7K
utsZyrFYyJdYW8Po28hQf1nWeQP6+PxQB6wi/P6sUzudntNcQ7uLRr4PTz6twPPqYwUF+7YW8baL
p/2EFPf8y6fBb+DOBCnzmGZvmq+M2qQot14r+g==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Ot2lizvbpt8emXxoZl10oi4H/aTQTzrHDg0mf7DDs3BS7iyFsRmaQCG/BRF/mwjlw+EbST4/x/DJ
d6Hf6LIa6mllnMD6G+uVHQ1z31eASHPw3m+WnPMr/zCNuebPcDitgiXWmq3SAS9byYvxiwcDvjn6
CMh89pvlx8xLiFUoo5j/lAPe4cPBJwSMleQLLB989s6rByi0lVW0QiLTzakaB8DHBMvhIYEfi74m
Lxby6+nYRGrAUKPOemP0Ag/LW83Eup/Wa0jVOtxzlj3foiYhg2mWCt2zyFhgQsDA+oEsDa/KZc0F
OUzOI8vFDrwPmYRwd2ejFI9Nz3/1mb05VQmDRA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
OamBwohUIPOIa5bsJvu3upqnGl4f3jNYS35bg4S059C9qVDgQJcw/it81deNA5zFvzX3Cq9CiO9K
zl02VsdpFgNPjSwEO8F47LJZ4fHx99EmESBogsNwUNitzkuYTLCb7F7ZF2WSJExQ0KsYt+TRp2UH
yQEvpM1lHQYUXxzjw08qUI5ssSnOsQFydvP8BwA/6aGrVJ+LuEgPVdMqLBn0EeAmRsynxJ7OhPGV
DlvHQwtVuBrkvjQHED/Ye43ZIeWPm/xOcjNfZjYeOvdEJqTbaviR0Fo1LFx8EX80uvdFeK55ywDN
wzoZ4cVH87f6VwR1xHdo0JpVxrajZpSZ0jPudw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 119328)
`protect data_block
zGVL3ji0I3fTqDJdPT1aiblwA9LMgjxCGUEAwxMonK14Dw64f9ZaoRvZpfZmX70tS4Y9KXuHN53B
I8EKQs8vxPRkDKvoLhQZtS7uvDKP/1vsv1MgUmrOQ53r/lRv/rpdCpaEKPCgF2dB7NIB9bc3emkj
zrm6z4KSFDPlZWhJeaDgwG3h/BuWu85C695r99Wcm61Z++U7hbf4pEecBjZHDDzxYrXbeuld/laY
knBeAS8Rf2bb3INyKL4zl0SgwSur32GV8MPhyWsxgxPYFy6zOnSFsp/DwtDucD/REeKiymKaWCCm
c/H9Ab2sUtNzOM0AZGF/CzcN+CPJ/EGMhbE6u1T/otBt+WJxYdW+UF8dGKgpz62B68hpd8yRkeHB
XBVyjs93rKHMQes0wPS6f6+BXnyPkvkNpAfC4BOdVodnJK8L1v/oAz3gM8CLn2akgquOE4OBAQiX
ib3gHbO27g6CGlPD5BM4JKG2cxhL1FnIE5FcMe4U5wKqEWj3bshdJQe5uLMQLpyvRs0dqZYwkl81
e6Rb+CWhu8BVat00kLzWITYe14d/jl9HDKtBqRaV/Fox2qT0bZK32JYdfp+Huj02CRYIK7yz/m27
yu6hQOKwEFJ4OOce+7fIOSkKlzoNjCj6igEoo7/zKteErEs82dpVeCByRPYJ3lQKmiTFC3rU8dYk
XwZRBbwiKqaDYpGzu4ewKbMHFTRXLFWpGmqU5W76QhfEp9dpzX/NDlICVqbDoKTkhrFfV5y2pHFc
+nWS5JoB2odh26cNN4U6qHzuRWKFCcxGjuCWkFlQMk+gA5wJYAFCHGWeqh7l0bSjyfHyufiAUxvy
jGM9woLK6pElkkW6jZHe4B8UIG9SlwG4LzXRBEOkRjvkH/5grxiW6M7rMTz1Z9iZYlCTUmzh6HFT
ZHP8PxPVYbSwpelnc6rPvJ0MGju8L7z3mIV8DOl/JmeCjnxdTDb5tVLDF2vjzRuUkOaE9Dh0hLLp
Y2pqeeAVCkTEEcHdrMgnQSUBlrH48W3Q98FBa4rPEgCIqEZWcPiVRWETJ41B9DjBu0RU+39HYLEA
/5YDnfP3Ydbh6xZEBnorST1kIRbEhJ8vZQGe7uirP7ZSSgYce81SIPnmrd9VF/6K4xaK68zAwlti
HGEP12r+niM0LQ2nhcPCvsCFoPIpI7MlY81RfUX7Mt0Kp5Q+EO9eZYPcuqTnkRNm+CFFUO1+R91F
sPOYknRv6vqlhcdpihoWX3Hqc9t6Jk2FBNCJbQzxaw/Iu8fnpOJ6wvtWTaGHrMMvuWyUYqDaTy3x
BnfJsVij+++p/RkqQ6IO4k6OOiPUJPeZe9qHKYUEmd9XMBZ5SMX85EzPFZ8tjT8PanAIrNG5sG7V
/zDz7J3EmgHOHexdWCEQX2IXSYfIbv6ItpAgUsFFTi4KKpf1zOkdCDLVl1FhqPb/3hPpZXVR+0MC
OlfECD1hjJgp6jclWAqXsfBMGpOr6uwS3GxGDrGORBZVwDmUE5OD/Zws4UDdUzzZ1LY2iTrz/oud
M8OvwTI9+NgXbnsOuFk4KGVJmB4B719WINmQu0e4XPWC2WvKJQdQwYdXEydDAOapB+RKrF1PC3je
7Pv2Jl208leNdCaTJcVFzFZlbtATwK+jzoRmJTOXdHJKIsGrx5g96OmGjIOy6xXtHsKNZrPvn5Wq
AKmIb9Z261EMF9QsCcf6vSTquSx88q3VFsV476AKMmMoLznkJEznIENBG8kRn9w1wD+LBseywdRa
ac9aUwBa2HqgSvIm9p0GlXnmDgF5OgnZp7f2VtpdFG7tev4x9T9rpaButAzjgba1IgdgHb/LvaQ3
oXigimtBRhXs+fnkqJ5qZV9Xw1RR/eOjJCbbBWL99deVVolxJuitIoTRVt3MYPzyy+IAmbRraxS0
yTsMRUdkBSON7Yt00XocAioiqIS8iGahfjGpcGfSp7eNJqCVmGdWQqWK2pk5uLsjj5hxZmMEaDg9
fzDY2ieeU8cn0/L1SStk2ETf+QTXz716ImKpowi9QfnHftjsmgGVxWi+mc+Cgf7fnn1z+w15xbhq
PgeCx/d3IgolIeebG2d2YghZ+I7jkrx4Qa4BNceYyWNwplhivhraww3eWSv8aCUr/0PVej3pdTUv
VbuYVH2L1Ncxtk6gKbn7qjS59oLUZ9t+g9IxrsvfO4oZHo8HoXKvJ33P73m5Qu0y/xjvkx/xQmZD
A9zPoFrqXA2Z/LUFJHxDIzpatJAW9U8RgsTNGu3bzS/e2GNO6TmDzZzBTb9D/x/6rY5Dqx2DPJwS
nA+L/HKz/MqNIw/hGjlpZfD91Oh0V8f81dQH8g2JFc7dH/flTtsrKtqlCWIFZBtpWgnDsXMytsdv
eq7dZvylVSA9eiJsGcvNCzDgCogygSLEDfTcWVr2RIzWehPzt9/vKwGOCXsll5y/PhVLRbp5H6Q/
n+GHO+kWTtMU+Z21yLU7E9J3uU34q28VTTbYoZsdtd2hNbsW9MELfij4rFKteAhYfc3HJtaRMziz
TLw8UpfYQEVrtocqyL7iE2VS4TXztFWGq6aOoMvgQM1fAPy8iEVjZ3cZcYG+LAYNkkc+S6sDcJqz
c+HHcDgoks2Wt+v9uQ0GiOEdbMeFPp52Wd/jFfe03Rn2TiRqXmfPAAcU3dMETtydCGzN43U7kLvY
y4IF+irRa09okrGR5VV3zphrna0Uvvt7ir/EQfqpNxE/PyDtaropxZr7jPmW6NX7PscZiFnKbKHc
HJsKZ7UBU0aEkjJJhXh1BHb4sBPlQJUgEkbl0TabXy1A14+rSW/nZIzGWMQokl9of424dEggoiwA
SgV2OnMD8htN+1egkH/0Z8O4OrGfuGn+v6pLInT2tnYf/Se3SdlS9Eh2atLlBw90jiteNSPOmLts
cYBX8Cob0ZucjrIKOVRKjjT0FMFeur0CVPUM9n73nCt+ABtZY0EXsXdCF6YrkP2D3pfNP5hrVbKt
JwC9aVjBO244HI33KRdAoC+c6SCp22Ol/0sQ5/KVeKgJGNu0j6H9uxPQ3iyyoSYLeMw9TxDkHxrw
PpoSH9i+DrEqKp77UPIzWzzdrz7wySmvB4RsNQCNvR7HSwRk4h7yW2IuOe1bW+1Trh4i6F27k3dJ
ea99zCNOIf4jn3pRoWSXMOlmI5NMVDp82qK8Tyknq+0tc94ps9gpYyo0AJ6ReVF16GfJS4y7BQVI
l5EUfGGK1dwVv7j0FWtyOGulkkznGIn8D30IDPuvjHjW5km6p1poR8cVzIJbRl48EeNhrZbvAUut
fq/0eBs1RspCQ6ilh/WvhjZ9U1BZFpk0DkydoCQvQLn4TvuCQMi5naXzICBuFz7PPjD2vyu+lKe+
EWeoi21RKrpBsZSVn4m/FObNps5BapLdgTVvQmO+SUuih8YALD+HRFgTVHaKCbRsSeXFX/ZpMoOS
+s4eOQbomDjwdeLLmnbjlrrupIRx73C3QwaTiJHd+qytG5BT3a7Qlt7/N1iuYoq432u8+LeWmBP4
xSPKe8V7NzgfxJgvSu7JAm2LQ/0FepvKJDFmZhwgQ99x76x1LQ8Gr/BjGLhl0Q4sXrEfq5BxMV8K
79FrO/9l/UtzBy+Me2vovxPXasjtyT/lzXN90s15fngxlmhUkjhpnCk8dfOwQZ31lErmzCf4l8Ol
mbsoRhiZZdUPSQZmyVIxv1/IaTSoqzkE7pxSGoLvpb8ipsJzHlOLXEibn0vHyaZ3MtRVHn/m6yKe
L2Oa1WjHV5mrMVOTqEgz6vojAPNRgOT2jFl1zc8Vbs02bCzImohaxvhv8QOBrOb4sczZumWlW82f
qGRjAP+7Vwzxy7WW//NdnomrQy8wjhSdmDhYFJPdUbPCpIk8OqszeelHqMjLl2yopL5BMejceMfe
iZUG11JjZiNRNvJ0WlmXL71ORbdRJwvBg54XKDcuJyQ8tlSyupznWtJjfB36OlCxIXhtfpgbh3bL
J6M1+4QvrKCmJH1bezT2s2p+ZnZfKDjRDPBWxLwedUyCjH/79TS4fT2jAub9tGuEiqeLEBWljZOK
Uk80EXQAgO/+oI1QkRFn0Ej34R3al5IQ2GOlPAkVQKBhLdccevE0c91x2lkGTiTd4A3XMCtFwAqM
tp8Vp7aCyuiRPU7aQca5AgcMjrBIZBrFzwEKtvUZgeo2mJH6UpJwUH6rts1fP1rcb001vmkOep63
yPDMjSGSuWjjkD2shZhqOFelfMXOD72w5cT3a1T5TIoevCBH3TcbEQDKpvQJ4HQ8xA3cvaS6CZOB
qsaZxeMDK0Uq8buM7hoTfTDLt5YVdznR9AbToJvCmRzyxhyWHjkx2vX8ntUUYqr2tUr60MWCmqu2
9kyXeX/kxZDSGVPw5WOc/WxfcSBZjs3rR7PJ0+3wn07iADPjkfnOSyCxbwdiEFw1vwyqbbEUfTHM
dq//OO11cFRKZVx7ZL24IghB8wTjGDHTVm1ZSk7u/UcwXC+clWa0mKOuhWfJSRHzxa+KnVyNl7q6
LBXcRXycSj0XUui9SuLB/mN8/GxIJeQXIDKZ0T+vmp5jbSrvHJrkBrJmOJ4TrgQ16WvhdwgSkOHO
xY6FnT9b7y2oJtJFc52XEXQwq2HTQy52TsX1+iNG2dIoCOWnNOiBzaRFgoYenmVhtwbkd8ullZGy
IfGPdt2zbVBZlHID6yse5pwYWk5UgExYHXCgslDi+xV9vOamVOfPqob1y0GqWBSofkoHn/Nouww4
C2nKPtpuLOvvQ2XW/b61YXB9bwzkjlaviS+tInmKLVLBOaQiRlb+SkTo+JlQY+61ySRzcQonr1vF
5s9kv+X0xbAEZOubgwZPiuRZpROpsZYsndEx9lQVBoRvD9/LEczfw+r7S+pyROfjGAGUAm+mG5zn
pQU+/iF9EKdpHsMG/jOrGf3QBa07yjbPSYGPi+lDFvzaPsrNsxxw/LSR/gnLtKxEeqkBDU68OFrZ
dbfmyi4koCzAGHiiNvzpCT6zmUay7LKQ0Q55GbhqVtOHrGA5MGsdLMbiAcvEOaM5Y1EfwxiQKN2i
k5TeLoa09EJ4ltwQZwM6QEl2ZFJQKtJQw/O6PCElOAkjyIapef2ChdoRjawozkQlx9wxsQwGJ/rB
vbJ6K1asuFvRKkRchZlqTvZ+/z+fuQ+uJiOv1L2p+G/u1wSuxb9iyTc0qMmAhVYa5SBnPTbXa9Si
PBIQXfieEcYU83D7528YUA/nmpadOAAbvJXZtgpw6YtGWxX98ZHqhM1JZfCbOeWv1F7Sc/oZK4rl
uWOBQ0xmV+X9B0Uy6BmH4/wWp8mExUYuQqWciXUaUm6caDW0DBe5dhDoHOu2e7cWUJWu849E7rjd
XQ1msFW6EExFHbIEyjbEJXUN97ZWz4NKBUQ0uLHzHrDDZDQgDrUUcAft0vqJI4QLh4HNYxdo2tGJ
yBmPyqsGkj93c9137RvPXlLbaffgXq3OrBGeh5UMyjwewrXnAAcB/o/D7SYNaEo9DEuDj6SwbD1L
XL8y1wigkvhF/VaZrHJD3Fk8C6LrYfwveSYhuV5Bc1hPlr9fXc58YO/0cjrYsXO1gWZDMdBEbnDt
AyKktvi09bqQEraBfidWdvUNPRRCtmtj6u1eItCQLJcgq8UP+jNHTC0/VMIBeu53HUr0Q4uTX7RY
36x8nrea4dPp8sI+SYj5pi1jbLdsHfLw4Pnj6TRQGmgpEU5x9EucNBSJ2MHXPEJQ3jYXoRNa2Zjv
OyOi4GE4mFyFG7sZu4R9KuEj/HhIJezfvyBIfoYpwcbQq7AO3ytnYi3vOgSzNfv91QailZWV1Wmg
Mq//Pw5ZpXb9GwmekK7BlkHXUGMFkUms6aSgyOJkCjMbBSwwlT8yHONzmVakkrudsWtT1BxNi35p
/XN16cDL+x95esxSnaJZ5LFY9ZMawNuiMEWyjDEpOPUMGeFz1mwB2unQQrE4xMTvEqEEEz2UlSI5
HvMH2Z4X4NLQHRxcGXpnMT6fU5o7upl1TimKmzWTv9cp5yYHG7bF4ZNDceqK+HnOEPuRGolit1YT
84odUVG3GCFnkyQgTW+3vri20IfFjZzUxFn0bj7/eLSOYxN9gWNJFQg68fXeRZy5rVoFnu1DEnm1
Lfl/mfv64eRcjHAL2wvm13F6mW2x3Xtbm+jHMgifAzc7/bGaoc7e5QPuUZ3Hxx61t07ht0pG7m4F
EJtlxsjQ2n52qbowNxTOloFeBjwR7VYLg0RW1NgozjdAFp44W2+NOSH/AdqAdXCglyxPwWZCjc4U
mOVWOh7yeveRSgmpbrnOW35Pa81mXt2e8FQZzF6WR9wdjvYQNWfKVjK6/5/+s/YUaSFhg/e0TuYq
1ubzBp7D8s5ZuLbvyepyPIhq9c5MZUWKMxSKiRNcfACvYzOif72qEOSQ6dBSh2ncv/MfRTuyj+oe
PAxDEcWTq9PQzjbsk29qb4wfHSoyDArydYY+KDFfIzhRkJD0+bOYjlgmLOfyka4PcYJUCpFAlSOZ
HI9dINGtTxTaBLHWQNfsN2bRvjlRL+jr9oZVLhl07otlTa54Pg0xhSiTmLOav6S0m8y0r+fMtOmA
wLijud0gL7rALou876812cjTtIjTN4JDV97qe/+R/Saoa9o7G0ALVMyvQFoMHAMcHitP+Gw8RaBT
PHlIg/mpdb8Wd9BlYcCKUnmYqI+f+hu2cooPkB5Rl/EpACdeF2b7lxqG+KpLulIKsL0yYlCQsIMg
WEz6oYOR03XaxD6IqPQal3CV5cO+9vdj20ATzzDNlQsabNgorH+qPYDtQVD66XYasdDoSAlB22BA
zCeSxoTGWklOLJ8IDe0B5UHlMNc8xN1LJz5gWuaWQwFMLBspmo7cVvnMts4QURbw0BhVeVW/eKKj
QdhLqGvi4rJyTzC+mYj7YwOMVfRucDPd3B1Vkov3C5ti9OLJUU8heb6jYFACKL2HvkcBIP4i4Fjz
qud5O9FdA1VecwkGfAvMSe1VooGUerldSVQquTXDw9iwb3wCzKqS2e8p7TtO3XO+R+Yop9XAi7xD
fj8PdIfSD6rnVHtTQr2DN2MaC/rGJqa+TRLLb9w/XIgdMfmGgVj2ukf9ePFE7BqvFoEtt1LRVwu1
GieSgjBNPKSdet31i/sY+BHqSIZ1NgDXcXkLRH72/RrGywWNM58sLJGWw4hD3Yqfg1Rwz2IbXr1p
qQLxW8+U72+62pcyzHVltFtiDVqtEmzXJQGSkwKaOD8/HXUSI3yAmBGJiuIXDSDmEgh3vaTSlDK+
4JAqRwOPka1EdCaG7Ot7326liTjlqZ+TYzp7zi5mb0b5qjCWQzs/WS0xINMBRignn9Ucv90grh6d
5wLrnRq0x45PW5vCX0QTMAtEjw+V4oIkW7VH8CPVrtMIkaefcyerCqDjNPmCFuTeBj8E6kqNAqCz
icuxEpCkXroEgpcGqjBPXYPl59LmVChI1oBN6uy/nZDsqzNryUuaUjT1VY/hjknqVkFClmHRkzBW
djVBWHZhPfL+xZL/fcTg6zjIcGIRmAt5iOSQUUzaF3cz92et8aeRCA6kBZ/m2mXRI2529Zqfcvpu
tmR3LO8iFazZM0/IitSaRAULV3rMEUvi5fTFpmuYSgIgd1EiOb1xYjKvys5wnFh3qeum7GxbJ3sU
QCK59KnYX9aXjedJRqBjHn0ctIY3sSCkFWulhrdEum9/iAnko1kNIdp48IMVNHTNnqSf2vKtARAy
qznsHHomaZrcQeyNbWwsSKXR6y9xt+QkBiCcshvC5XppZ5knquPeWOcOEmxloes57Ptc89iH9vmg
2v87GnxVydyBgfDBhsgJNbK9bbyZjNKeQZ4arkwdLv5y6pmaet7SjHBQ3xesPAcFPLHRz8tEkkV1
poytdUlmNsmR9dbKfGPSBGGXD27OiUM3ksyhIMfyBFAy95ooUu4MaQoAXqfS7Stt7pK1XJO4lt0T
IsM+/8IY1JzJW7SJ0n0xtdBX6uW/l3Bs9pegMG2B+azFfKzjvkTwDR4eD4aI3nxgotHDYvG1EtdI
3luJvHNgseiY8K4+VySaXt5nR5mHUVbfesFq7uO/NldGi4r19cO+Nx5LWVeij/GoK9vW9YWCXTjI
LkjvmSa/Wgs3pLIIp8guZjmgqXNrsWEuYtaBtFdqsAwwWOrQSjikqFVWsEFNURYKYK3VXQZ2SCU1
UDGn8pNSirdOBoMSetaB6Ppmcz9s2LjHicGptDpbDSk/eo9/Z/sd20hLZsDWJcmrVrJjDxRs4lIE
SbLGIIdan55bSdyqi5WhDupjxrJGkwQC2dnJXUKuQtCQGplrsRuF7XUE5zn+G616MvEbvIIQUEj1
/NTxQXUA6Gkzzxv/F+WIV0bKW/Ef2DouFLqM4/WM+Pt/qQ+240bEDIOFXnS45aXVeyfNkoPKDKaP
VsE/v2mfG05IPGntCSQ8hFYRaPNzCjva4YmSFVQ/CP0VjJEk2DJ20nRIXvyUNDVvAYfZsDkazz/a
2WSwqgJsvGsUdDl4CwEyVb/eJmLCpWMO+tBEgPHrrC8ysvlNez6wYISiUTic85XJwrG/jDnbIQBP
q42yJrfmD9cX38Tufr+MPutr9hrUX6kAKxWvBeRHh0r9R4j1Y75VRvew/OeEphoiBngKnU8pZs4p
IZglDlPD9OXEps8vNSCjd+fztlbKxMiStFtS/G/VmytdxTtzpVLe53G4KkoW5h9vYKaMxMJed0l4
Z8qbIhiHaa5qDksvSBSUB5flUNwCe3BFpIuG7Hj2gwlTuweNfO8U8YJd/YlVpV6J3MPvn6ziZfkj
qYDFsWjwuRfrapHM56vCT7US15PZeKsXKrdn05oZtDu3Z/TteTyzX1rribv9sAU3/7+ibxKBF8fC
8E9W9amFEaJYarQX/+4mfSsK4w3Pz1jO3Q6tER54HBDe945OyMCig5L2wI8TTvR/kowBIuSbn5xn
aYKLMi/3KnCljdLEvB8hRz7QvySMPa84geWsPnvdGpeNJcdAhjRckRSW/gF2ixUGYaBbyRdMQm71
F6kGaHA/NfYGcN72D7hXhgZOZThxjHakDGH9TRlkCQLb3X9qIWmjpnWbKPaUq4LApUri/NfqPJJg
8IVat9ZmlnYkPd6FFwECstgD0812lXIZ4iuuamZ8QCByApAnxhPKxQH+GEJLbgM9nvjF8Dc+3zFB
XpOPy3/4ekSee+fhVkyc/uP1iRg8Jdf1fkHURvDBlyF+LW/2j+2mO0T1zT2i7ObsLlvU0MZVbebo
T+GWCFLmFcPhjXtAueHodLnOUbfgcZbNCtnhF4uI1Lzqm81950e83/Bwl4kT8VLVQA/w2gbPMZve
DMViHRX+2frMK+7C9Wjej5boJItyeo/XhPqh3mCfe4nw2ASPN/gZOAxOm2bf3A2JrUdvttHlxjnx
BB/8Ch9dNcEpmEe/PQq0XyAiRFO+4pcC7ZfNsE6Dr4Xxtyngw2VI2ajcWWTNpKYC6r0IkHh/srXn
WSCdhTwrTK/rrAbgNdoaHryrtX9/G/30tP0UyI9LC5fFIbdMXTYkgPwPH+wPAZJ6rWAthOtZczvr
/x7jzfJgHcaTr3E5Fc3Ti/Z99+wq4Xc57oLYM+/j30zV96/ed/8Xx+eq1M94g+pR6gbd2woTXTY4
XTlmJ6xIiX4JwHwnHEYI5OU3Bgrlwz/s9zNd/YuCnPR9Nx58wSOa5d7jmDpOQuxFYQ/57SJZxQpf
9Ffd4hxNl9SvKC7M99ycA4clhbdhdeUDR4rs3TT+696snaxrFAsEz48y1c4cJjajcAN2ywm76Bmq
eszkhWFf+IjEkW7EmzNqsVj4OrBeMm5iGN3HhMm8/GldWh1R4bU5mZPE+5t77eW3/El7Sk+ifzr6
3bDqoJz5HZToO30dafkXoauLeQTmt02qqFJcEC/mBdIpPo7KsvQu8vOxYbTcUS+hp0+G+GaNyWbg
Pj+qqX+1u8NpE0NhybGfjuj/6bSQ6e7fbLtMlwjZ7RXMJx8e9iKVGNBWQc1Cvx3ZJbB9Z5/rWwLA
vClkYE1SiSYT3zmI7UqDS+R/nFrxKKZygqaYKZDBcIcBb50hDOVNspRgqdciyFe86oNYZp/8PxGy
rG8X47Rw9SCkzFfC5MQFWFR1Ng3ps+hARI6FRQylmE/k/uf74ovmBVIL7KIQJ31DfDw3UJmPSKVx
fMeLWfNzlkUB3J5Lb9FPL4oDfsRbPzRcdIhPvWF3QL4GWL60+ivJ6aCLrYo3OdkZndlLhF/8X0zv
2F1EKBQLVXb02yCN6+5Ygtms0PyRaVL+bRm8ShttJ9cvkNp0r3O30z3+STY7+u9PwTwFzfG+I0lS
L7YfyDpr1NIm/qfuKhL/R82t92eY9XbWsXKUhK2he9cpRU3it1hJVk+vdZ0p2C6opzBCVZErUnyI
eVDG5rGrqjj74AJoo+bju41kawBTwhepLOWQairB1kAB9JEh5KjNMNA0VJoohCv0AAzDmuZFFra0
6vn65QK8ZcGGLUFuP6FQ2yp0+1CcKcbdI0LL0Oi2dCgWccvJ3PSZEHXiaN3GhYoPUopbhXMJi1Lx
TRoo2eQWNGevwdOzDSf6Y61Vt406ATYJGd1ibYv7f+1TOEb1S2WhFGj3hoBwTOHGlPpTjKcMX800
DFm5Qn32KmgDKv+b3KgaieV4IXE0c5XVc82s9baJaQb6+13t58tzjpiiBkGmfaOSmJKTBGAQqpPJ
pYNskf2+rzyEoJk7QeXtTeCfbgvJs4rY5KzR4/89KOCy1gU7sAJWrPtPDKK+RIvy6upYY2DPRJl5
zU1tcA6XCgQ0zTsuxShzFc0KbNVr9tmkw0aC3vF29sk7jHHjt223BVMntzQntBTuDg5SNs7Ce7Wv
4buf8ikAX+X/qUxlxMW4hHiA8MoZDBdlEADZrOXWow63RENsmnh7R+kwfVNrY8Q2bmJxb6u2uwi3
o5gY+/YpTQvziDmFC7QzuVIYw0VRmgA+DOIg6q6YysnX0DHpTnQHpK5exZVp6egKz1XJJQtH7N4t
+6Psk27SGKHsKK93f9vsuttAG3JdttYnosL+9b2TsjEPy30Qar13mUMkd39vRSTliGa7Aalrf7zo
IgUYc2h0qTwcA2A5jM4ZFGg+KxFU/x5+ymRPRjT1BRnKg4UMDpOw+IBtVd2M5N197J7j6AowDvlm
7xccwWqEfKVQla28g+ymj8DE2S1IOI0fLdwXvhUNkdQntWrCxFQz7bapMUW9vJ+M5K8/4l1b10x3
5FQmEtJIQdV1qjBCt8UNIVRJCxIivj6jz/8R9ce6lCdAlj8CabZmHSXtHVhlTa39YJW/AIja3J8E
4FQbMdnJTnusEMPRBHUMuySA9d3cLNdwy2Z8FuYT4xziYxcO/QxzJjkw46ogBq3AYZxQNO2d2XLy
4uI1Mcr9Jf6xTLqQAWpqEgBt1osUOzUsABXcShkYGxaJY6wr9cpLkBg3QMBiT+NQLd9K+/QqZO+z
wTHE5rM8IJ0sJAEJhcnUoYsmkjyqJspLZ+nSGkyi+5cme17EVAX2RQ1IKXLZ42E6Y3fShveqyja3
p2m9+UnXmF5Vnv66ZVDGNYfNjIwnuysU2dIniPLKChbCN9lsbp+lP7LEzzhZS8rgs1aR1Eo/jbmm
SB6PsENQmVwjyL/autxTVzeHmaQ3E1OtBbh+HPjZcPlYVfrPZr45cQEEj2XpCTAL45sYqCpYrysb
Z2BbwpNnGVNeUKi6d3oAnj8C3kgxOQD81sjzr+waZukaR7g4qUkHiaOcfD68rpfjSRZ9oABf9Ofv
LQERt0MLSJ0cNpRaA6V9GPN5NFdbVR0tsCB6dCbK0eJ4n/Cqy2w9VAaGDpVXc0R8eXAhly/o4wKr
HWUfLKS8Rx1bOd8zik/J2kgSDtT/2pzX5/4YdJhlqiRnKz6jXjDhoAJxbAu4k89F/0DR182ctf7K
vva5GzdVQk4KBpyovaIyxQ1rSoFAiGahMlVL6WzVMYPiCfiShUFRXyiAR6PususZ6ndEtMmHqROu
MvtZvkds5GOX+XYtUuF8u7nW46CF7Ha94p4GFA441ZdD5TY9RnBoecZ8l5U24RbuMuiw3AEMW1iU
CM2HsuCmJGz9Fo388EIWonk1qoshdBwAY3LsD3P9n3p/TvdqWg+ntq7GkD3mXp3P1KVxBPTbbAcU
guiwHVkDEy5NDrDZ5r102vh0zdOBQ2K8GcP+iJFBUvOXT/P64jcaCCu45r2LllfEz22TWcJs62RO
YRBb2Pl7oJNWKYEmvGclhfgXAAutXIBGAsMVfUY8+MEcz03BxBS3q2RMbJrdUCM5UWHDHmIPsO6t
QFLQxrHIgCZdJphD6V1dhdtG35iBkQLEojO+PYexPWtrIgPehvRD7tp4INlIYHlscQDe4pdrREkN
4P0GsCiiGMZUoH8wkslYm7k4gusLQXlfeRT38+RRPVuy/J2VU4J0as9MEpnep44vTpZj1OvXyqa4
ZUOtjdzr7e1cf9WO+ov9ctfjuWEJKfjac50Ao1bzp3C2gWlnXawpZI0/sgxJU8ZAKlqWB2VV72kq
Hwce9r5dEF+B58JDdQK9WbV30b7iu6ChwEGQAgi/qrBoHMTrJfIGutOqEzfVR4wwfpmyz7+dyXxL
uSzRDlo3uOPGbeY2goyJ/qpfHGKVLCZrxk8NxP1iObO4P2gFHQAB9H3G4BMFGFqupFiJQOvpjPvh
Co/sybMdTV4VlcaFjlt0jn5UfiWiclSKpxjBBJ+rhHtjt33p/xzdk9nw7I7AdjCiLiwYz6FtAyTS
Xf2VfoyydX7fdDlGeN0hhArr6red0o7QjrI+SUsyO1Yij4Zc+oLm43dJ+zeInqZnTqq6gvY8BTub
HR7GNKBLW3Dy1sEGrIc2MYrG0bZ7y5M1TgCDRCG42U+J0LRB0AqTf99iySnJZvIAu4PFBIu6YXsP
n4hVdaOlKiL5Svuyk851L5VmyhQ77LjtNS1ae044H1QBvY0f6YsVgeffjk1mJmH/zP7GBr/48J13
pbw3ow6zuNCLsVHYo2nlDJPnP4Hppgi5Ib3Oz/BinuKeLGgNhP8lz6pCwwB3qSwhpHARJJ62u+86
zWJA/ooOwcIYJF4aBKSPH9Ua8bLP4O/Ck5O5J8TFMt8Ylzd2pwzBMrdMDTXLXUtDfp4bSFyijtFT
qPhm+TS67q8M0146JrYvNwokPBQEP32qWL1pN5jxHVJvR9XJMEIWMiZpKdAB9KCbhn051S9654Rk
9oxS9am7+EzX9KytCUnt4tNmG0LQzJ8AB4xg5Bf3K5hq+mz4Fd+HW4Ffl/XZFvrCGRMGsZi1gZqL
xy+6aEz3bbHcDa89VvJhU2LmmwxBEE5SwUqMsVxC3FwiaAI29zELdtXl4KM9FsHZWZ6KUsf1oVCr
c55/z9HuY9P/LrEflUHnIHjq/PhG3XP4nOg8/VJP+T0pDbkHYQVcXksCO09sdncwVhch/eGXR8yy
vsabk6g9iSG37ZJiYwNzrPWfT/P5/G9/bpb91AXYfsP+zd1nTO2PUWdJc0Ffw8yKsZKc35jvBB9N
2gn+6UGhaMOcd4c4wgKNdoo2MIhY41Aa27u04WTFiaDotc9n0+RIgsP1KQmGlPVU60SZfrLq/GkK
PBWamYFuypDolVunjgHzjEuAqLsth0i4UKAHYBpMSk/Eqd24TDrE4fNb4v/7sE0VwFwJWpmJXFli
zhKAmV2MTU/WKRB9NVutrhFkFA47d09BRFZz5bl/aS0e5uwOy4PEcKPlH594od9TWfGWDKdBcJDW
Onarq+HbKwCcEoitv3oT1nl6Ee7MuwO963qk2qZJqIlEwrVNpWlWi3bbL6G6GfbZlK5cKsJoFxW5
NVXEWfb2CS+6Vs/lIhGh7TzEKWMfdjdADrSYFMAK1LhR1znvm3/dYueeIzB9JwpWMsUcnWRX3tSe
dsZmgehW1G9959g+Sm1v1Bduovwd/KxFeV8PvwyNebB19eH+kG5UQAioyY2usvmqap7P+3aD1p7j
pckIGf9KixmBFBO/6Ji1FJwdyJIvGolDo3NMgoxOy5uaBMVe+sj0N0txZaVJbZyzUyNbvXfV3yB4
Yzle4KVfQIhZ3DqqS3eKsnA9fTbSwsNl+BPuUqaA0qM499LUWC+WTH5UqDHMlGIeybFPOozshfpG
GOwai9Fy4KbGIGyLGO41EeOI4pjXtZkFKIQ/s/zW97CFRf5P0PufO/mepyl0O+F7uSnX+tuYiPou
eddJnbU82XaJi2Bxt23mRDfTtrL+vwGuGeELsCoAD1ZBSJ7hbZcggZtRj5Ssv1jMe2Ff2PJcHehq
MMtNEMWU4vb1tzxM1sac209/HMrA3HXeSkE3JGD/ycL7H9eWQzS28crX50NPycGm7eHz38RpiysY
VOma4ww6U+K/NbeSX14SZNpI4ZnUw4xtQfUI95bzNyOn3nAJsJ+Zs8BfWZGa7RNnCTCfGFeu0vZI
3CpkePmxmhg4RcInfmdV3BBBY+uWXMCX98cY4NleLl20sr0jwq3RMjpIAJjiP6u1Ef8oYVkYTJt5
Te4syyqdHQWk2Peqlr/DfsJhFaWR7VmgbYl5orTztJjQGq+pDc5sMkzxdLQOovGJrzKyG90Wss37
Yf0eoXTWbC/PJ2SxEJT3T6UsXZ9EXwPh2DL113C4Ckcpnq7IwAqGi/MAe+c56bebofxNEqkYYy99
OKyUpRyESdmx8UtQ91npKefz37F7q2AJTQj3WhnQbtnTOOETNxEVwdHS5dyrnueBUGL5mg0y/uuR
2mKPSO56qwchr5HMM8J0Ad/2dQSGo1oEjXBvfQ+oEis8vAfTiKtJEd4/I4RpFt8UjIsHsMJCeJQl
3cSbEFeIa71113UAU0B6o4kxzlnB1DyIed0eq2PuWLk7pnKmvsXLzfyoCsaAtMrlTx38kBeXzUb6
2MGanSQy3/l5fssPqzgwUznNWumf2E8T39WsMyCItVpnzeJmcVSPAPqoaeHbZu9jM83Ia5XUP73W
yxVTQN7wc0y7bEKK+xDxARPXuVj/9DStFlaaFXinlqEJpkwkBTzWqfur9oz1bPUR1paiH0qDq800
45Di6HupqeNd/CNP7VGfhEg4uwz4P0Y+LeUQX+2aXYgI/v9LgYiFzNVnmScTCzkv6YOuOG30x6Ph
4bWcnGoUx079JVxyAp4jICWXjALBTBPfk2RlX+mjR3dZ4616ePfnReYLf5j9IgIE+F5/INJEyXKU
ifSVl53UBrlyyFJHVQTlMk5579oBwOZZOZtalXa7YK7YODOjzc/48DN6uWfWLR2bORZv41/D8Tja
BAyto1okBNS/hwdN4l2ylRvaGHYmyZkWRTtoLtsf1f+J5s+NLsUsUQo2SXnF8KI3GW3fsVGRT9pv
bOB+N+DOZ4RTc5k2RaiwDjeE84ao7tSZpio0O084dAnlaHAOaZnr49hdGa85oMYVpeUGizV4Ejx/
bhKi05kT1C2af/q09kGwuzSSzmHsJLcANxJhEJJYoc4Nk2t3y1XQiTfSrVhYWe80y1JltQ5g7YQ8
fRlNexrEaosonHH/H8tSM0g1pYR0d+WWVBgWBEjQ0IBY9fXpmTUmZ8JkKkLJe+tR6IcfdE53cz4y
zbkLZpU1nnkRDODhcLSaoJyIR69g2heWOIZk2/DTXP3zHc13Kr+0hQOCU2OFKb4EkafGiEaApsEA
zm204XfB+ThwI9Afjubne8JtobHcsKurb5vaEARjjxHnR5AGc9r3Wq8ABwTCbpX9h/aZbh2Ro/u8
gvhM/ks4ThTf0sSufhXIQvV2xo/9PyFWKFCePnVd4X1OiBmt2J138E6yd9xYjzgyb2x1XZ1UVM14
84Pv7sKJ7MyVtceydfDRpluj+LVy4R4qasSwURlFMo5LVbgOJ1GEzYE8muiwnAVS06epoglP0mou
GEG6yjJdUBqolRD4kY+ZMWF0eLsltQN5cDu6vnNF9/mhuFULA2ta1AcOm6NolcNBkItxPALVa17n
UC3v5Uih2DgCEAwR1ueMJiTnFd7lRvKprXgjliWjRBz5QrxQ75/SrmDuHtL4sfga2T2TXK5fRDnl
zAvj2eIQjidfeoWbqXvOcN5CUYBBabJXRcBiizl5XQmvyFiA05j48xOwZcHoACqPOHw5oL2OXGzP
3DjNT2jK4XKpizG8FIxAIolVTtK1D4v/stMUlQmdY2joWcsewOIq+0yiEbqS3/9UlJSGI0uxlgMJ
mzrJ/kXahbsIY15cFqaKhPs4vEWAws7S1clGPC4qlCi2B3wNh7inpNnb+qmz44dXO44XH/M0/Ctp
vLxEA9GYPx0eIDAUuQwcCn8sxX7NtdmPz7sDTrcMRwAP4q2tGy4GfYzadQOlgH/OrgHDFcnZ2GSS
ct2+OT9A4zSE5Y2nHgHtRz+wyKb/QXn3rlABPXFd1ywyExR3y3mu1KP/FK8qShazXqZZEYpzhUgh
xeJ1G64VkhdJSc9PjlLuIUBcJ0OSBH08kxk/y1KREg44ay5IN7IycNjlKp+/tpn705AoGNVM/oPP
mrwUUUEDuVrNQ/4Y8POmNHIYVgl0XFvUtNc/d1m4i5HmKrtas4Lz91OttBrXWbf53Ti1Vkq7Rek+
BgRvjJip8IkwX3HZNi98aPplidFQ19BqXSg3W2UX5hiyXdB3gQ1I/7t3p65v3lg35DFGW5P0qnQQ
bozpFerkcKWLa4Wn2/wS0/UebOz7SlmMNhj1jW5TwopNhmRZfkmA56nmEyTyktuvRSoocnXW8PEe
Sk2+BLGzGkWuOC6glWRwBH0oTt+psnlj7Yo/tL5wE0v3RrQdDiJ3eND02/uSkJ33GADHGElDi/eE
ACDgnSBSwjh+wPR3Udkm9O4Ft0e9fMF2qiixl54hdw5jQyuu5A6Ipt7dyLIt/SANrQL9UHnUlZP7
K8Ql0FiCfdxJG12/+ZRsDwyMRKDHwCfvxs3Haqyc4i987yYocZF2FkcIZwIW5TdDXj8lYYapUS+/
sw2G9i9W8nDiALLok2ybuebEsUqfBMrH8rHGdbWR1o2Ogl9LzGN3FK9DWO0UqAEeievxeF4jsVn8
kaBpI2n84MBRDfFOrCENlS8kGer2cyruqGponmEpLKRJ4d7i0V4g0hBK5obvWiBv5d7Fmq5ytbBC
dtkWN1TsZYGBzwgGfrTMhX3yIzi5Y4QAAZZ41CxkIswUmGUuvsuj+mefnFUT0cAgDlDVAO44YioZ
llh8TxdZK/a0yv236o6mxda+NbIiXNoHw4GfpdpsK9AdTJOfcREnA09FbrH2li45Frb6ikavo6xY
I8006gwTl/+5b2I4pSoIqPXgxCVLggdOWf8LFrBpH9yUbPT0TqOeVDqRdG0446O1W+Jg9hsnvHnW
QtDaWmSHrmdSQUzGisKUaKk0zf6ucPSjcTTXEsSxcD/ZCxNrT0yKFKBmFrPk7uZLmbCPwE1pb7jr
rCawE4o3QXig4l+l/Qydak//V1TUrcIxwOR2uJN522VX8MusmaD3Rn2uJqETIH+Ciaw+cETZXPbC
RQCVAsF136C4VZd3cqJBFZISrblq2YXxdjvknxXkNLFZCllfgS0cqsv/SsJYVK9vmgRrT5f1YQX0
QABN5uMdLm+YVXCZUZQjMxPifYXwkF1xj+oB1XqdjZKivLvT5F1+9EWUJYy3kiSW2JjR54zrCMNf
44Nb6Azn1iGB0toZ0j6a6trEiK9i2U++JTVOrgi3GEc9B2+j2kye6AULzzr5Zwf9Y85WPePTSkhY
jSno9ebsaz3/8YOyXaG3rWd/Z7AV/Sy0GDJK3VrqqgrzZkN1yotP18uUpV5w+rxQZPJ+IWaaSF3/
rq+SswR0iVyKxzhH3P4gTRshoNo5m39gYnhEK3eNf/56i1u+QIqoWSYhp5GOHaS+WAQVACn4ck+a
D61srBTi/FwJdbwL80r+DuWhGJ3pmO9GGy0VgnyVpirJQmeLaoEOvkEkUQInYhQMP9Jm/kMdhwDP
KK6ostiSAKuWTP9YAMTcNsUnPiynl5bmPszNkxFddZclNf+cxd+8JGaM00E1w99e3XtDn1CPewUT
K6r6P2OInGk9r/YcDXlnh8VbpDn9vR2z08nzRdyDfU7XA2ZE9sR9aDYmIsz/9ch1heseT4ysuGwQ
BqIDJLxwzjo+doD6Yn/yLA2BwIXGXQdAbcarHXrr5IXBktIrVFHFVggvqGLbZFc1CfdOnHiMGC8p
GBCTq5rcHaKJl3HeSck+fwzA9ud8gTQiq1oolo3IaiGb5duA5qgRgA3BtbV2AyRJqt+88ukoDaL1
Pg5mss6Tl05Ps5QlNLiTOSI6r/bDmIyrfWw8AfT/sBbrXsunpWr/64wbjkgwNwdTb0lT/4DHHH6A
418HONwAmQ6v7zYhp4EMtusiq+ecFJddi8m9g9iTMj4k9Xdw20IXYSBIssGU7NcYbFtD2emjKIoo
9OGU/Gib06+lcFmEKipjf9K7gWT0D0P3MIhkG/pFoznAdSWKQv/QZJAneJsQbyj4VHknHFnizn29
A/ST1oiRREEPHLEsv27NQ2EIxeVtlg+xt3hCFbjlMCrnUA2Jm20oODeGZEzX3U7SxQb7iGdFg/+J
y0+GvpWnhy/VKYPaGkzyuku9QWONw28vllUjUshD6nnRWX1/MBKMTd/r9E/dRaumU7zlMdLu/NXO
7qwc0hMsIU3OtNk06ynCAxeWEoIV2Saq0U9szwlMvpYT2rOco1fKam92gyy12EBJUUunxOei1qDX
NAGV4qmH6eTmn/eQ+Q/U+cJ71z21awg0vH95zErHBq9pHhkOF/T8Ov/TCfQ+k7yZC1gtwamkkZXH
xGJWmnuoQqNv6mJc8eYgOoBf3etXpSGJ/nQ6ZegoqwYTWjlOX8rBNI1Gk5WdAUYFu6ibDDCONpC8
Ly1PUhYceBYuB+XJnzer66VqdCnZQ3rTMS3ON7FQNAhnUyk9aCaVmEmiYAdpxQiEUZ1JITLIqec0
OHT4VdnDhYPpQou87RGU5WvMbXfYVq58Gf6z+vsptwxQ+2eNp1QNfhU6mytXQhRIK62VthLXIW1s
KDxaKIqzrNIV/Vb4yHMfjkL19geF/YQZF9xgxOhZiwq1s6kdwZqZgSMfkW9QVBGRhph66tvPt5H2
gkHABSMXB/XqPh4NZqWaumrMUvE9TBGl5w93uzWzJ3JJhRacz3Q8Z9jr4VvW43mUzF7imdcOGmap
i0BN2pm0Z/hYtizt4nRv8Rw4/t3q+S+prBQcLqvALUJkzXLMO8/foqjrz0i/Pjukoi5Jc6qGsPll
9JpEEx8iPgZPmsg2ZX0nVn7tyhbas1pQ4spbvA7sfZ6QauxZ1mJRLGY+pz3d6yt7IdE/dnenZIKY
dJqCdytvjqEyCTFpczZm3ZS5LupoOC2i8o1ODNKPiZG4PQ+er92q2ktmrV3EvFhRLSef8HTyJ5R0
k/X6t5y/Bzo3MZAgVHlkCRsmx8pl03MpujSp+ScsCPegUGhA4PKXOfwGRS0Cqacefl3VUMugq5AN
95G4/t9CX5zIDQUxWdXJ4mWsp6NrdbP6Uk2XvTNgQqA/rGpEW4a1dG1UMzovRpOo+CDWUIxrGyVI
wcnZm8FFJOBRKv1HtlDHsYkctKrH9egsoPkN+S/mZPDhzBfYJ71X3PdpxyIFCdozxSbukz+eHl07
ItcyZFAVB2/Bbw8zOExEVRf2Cage0a6J8oFgjaRcgFjn4SGb6iOLppKDfkh1aDpKdo6lh5nyY8Vf
f/l2zsIIObHBjjLJSfKjFbLPS02DWM4wCPBZk+KaxUXkQrikcAuGy0+FdVYNKzHEZtjM49DSqh0M
rgExnVHRZUXiOxe9vC1k8tNGwV4eV0QAuYcZs3gZh+ezREjZjri6KQef+SQ2MxTpB6BFDniHNP1W
yG1JK4tRsCRD/IiwPUsah6kRbqmu9im4NJQ6oEUffw1RXPvBjiv4LhJ6dBjrgA0zV1rbqiKaEAxg
dLf2FPiwCBLh/NhceIUMLbMy3/ewIfTMUlcVE0AMSq0IN45RyekdObpTMNE3t1wVQwwoc7qkuAMJ
cnIjmmHDS9b/XVS6jPp05rZAoBwEA+Y04zEyY1tOxBZ2gszdEtJQbWxWvVXgQSF5zA15AoPvLXmw
oDVX5GK8v3uyzlNTUh+8CSNXXIs/3jgl8MWouvdBn465CzkfiN9TqbKRkC6gYRuSQB0H/TnQxUO8
SksgbpoFXaKPOjgNiB+2cCfu/XzrINatMrzZIjf0DTVu3McWK+pwzzJj9hnSsqul/SbX+PwxM4vP
R7a3YcBB5KrCUXmmIrPK1vWXct0of2B++NGMYH75kfprls5I3hp0LwggqYJj5DU7p9vhq4Ig6tmR
m4J7rUX2hlx/cid+e+XLiZsuwlKUYgYreXRKLSbSNXD3MAfwM4v4iLYcc26NR63D0ZJsJZToqQV/
ZhRCCUJAWVb2LvcPpDp2bdGLYJqYDXcjhaxNPjoupXvfer1pUMPY/0m6BMM20nRCsab3JlTNAfXG
8oZpXyV4PQLoYa8o4ZzbXUo0yrzUKhipAgooQRQeuwdbydc5+KX8lGj7MA0mJESP+p7UyuFSuBAQ
BZcluxS5HjPiXMJCTQfb3HtWJap6OzxoQVFnOtVfTfxjm41PZ3xcmMC+B1nBuP7wa+S4IP2a1Z8B
SiDSNQDv65QgT4Vziu3+0pX2wk97cGZVDxg3mlRLJZ25SP1oWKvhokfeHm4z5DCc64y21D5fDfYM
r79rrkGWCSBEUBI55ovZzmDJ84DL0HHNpwFT2JLFGB100ewao8BA5dN05y/NwGQ/rZJsRHQAyzK3
r8332PhIA7nf3DQ/CAgjwYJYRJMIUmnZvRCyIiQYGoAbIsJur2pRX92nG5wvTJdPzagaJValh51P
S/lJRdwc+Hn83PvJ9NcSgPT24a/Nbk8M541hLK0jIzFublniICXJXtYYazcHFpuucqM/QvddekvJ
d8gU8n5miEZDjk3DlxOCI1aLHhsgFYO7saYLAzy7Wmkm6p+PaJBzPBmrhVZV+5x7qHU0ltJrpwmS
yJSyGZLWHhUFhEYTbaLJUKkbZ/sbvTx6Mfyqc0C2wEDCK84Jsx8iIhB12jyRBSAafsNg0Yq25XM8
GdDsUQ7HOjbTkrY5GhiHe2i1n+JtNNapIFFYC20T9O5zErMpmgDP+gkLFPkvrA2d77vz+U0UXlHj
Ck+cNmr9wY5ekHWGjU90lQPr6SVckSa2MOIHegTIavYxLqg7ON6bBcok/yKGOdkzDpMCL9tGw8JD
z5yxIOZrCT9Z108LuAYpFQy9rHBHIqGVW6gBFBWRSnddAMpr8wNvBa5vICZ61P5G+wZlZ4NNFRim
JziiJbMELvqmJlUijdFD9IolV8YQyE39xEQCspwwFHXi0fsxWxp3KuDJHWV9O1uLxG1mRZWzOBr+
yrn4zaaowzuW32p7xdOUfLcCHGKjnUvmxZkNvdXV2gIh0sbx2NNy4gC5AAtpv4mHbHzyrAAZJoHE
zxVNpxtGPmcsZQ1ctdDZRVAVtuYu0XG+5y3DPZocZ0pxnMm6BwxAxBniLVW3m5ygsovcscjhCmZf
HOJPuZ3yGzDLzyZxJWIzt7hUM02SG/hT/wijomFhU1sJChMmLX/pGlnhOSqyKTC3c/OwS4k0Lz+y
o4gok+JP0c1LdMEXC4sn+QAm5eIDzhLWDkTqzTb2WmZB8y9eRY5cuPse4pCwgYOtI++tyS7fRiNI
Z7No4fM3I+WyHIkTSyMigQM6xOm7eDljYCp/w2zMdKS9OSdvgAak2qAlJGho/WD/6a15B7Y2fcqp
TXXCOBTHNjhEWSN13rOD6EuVXgKjiYzlYUARFSvV8NzgoF1f3K0Z15OVHNBSpUb05JacmTXVFebt
hCYdUX7dd74Gic/3jn/D6o0AHcDvyUEZp4UldmHanvQqmCOdAqhNgo6M3EUQLYs5J/ym9Mj/XwgJ
WBqFcprlnmFB8MK98H/81QNyssd+Spu/ZMlSNlQ8LU4jGM9OWuL2q6kOnaK+L1AEP1p2eMm4HMmN
nKfzaWv/zG0uQldAbGNeDG3W8wI+/iEgfa61jyrfx8iU5XhPGXZXna1aSKoFPJ45/+5jRehY7ULg
x49C2P/8iD65BTQC+ZNIW/7v8MBuVB8Ae4ONR8xim4kKwg+0gkwwddESHSSNmf+oHid2g3hebycs
zVkSVUaY456EP1cRj/2MlMK0Lc50X9xRT99a6TFPmVFIruGxLfkl1OsTI0qIwn4ncNGTKBuk9l1S
C7GhGlxBN99OKvbE0yTyXet1hmUDg82LPnxROPzhoIQuGmMuiw25weAZMkCy9M6uM5PrqCUq/QrZ
Bz4c5+gxaB7Wdb/4ShcWPLqRmiolwTk6pTArZVkpG/UEUol7VrzBeCc3fhK0yw4fETSqgrSELcbe
dKxNC/P/s6BFDxxw3t25weyZhoXznHpmdUaP/ahtpHyXsCKHwNcI1q0rE2MNWYUQl+ah8K8ITP5o
H89JiHq3uMgx/eSbVbe+QOb0SYH3pwD09BDnZdsVreGMm/OI6/h7rr3oFfVDWFBxebGkHr/7fKcn
FdPi5VzVQuuUmWhQ/J/yUH9KWoGHqGcpfQn2M+ieYeijDS8hrICRvHU+DeGv5uczKSLpyFEsn+G/
H5j2iCBjI4LYxfDF+67qiowpxHk+TcKhpYy5Qa+HG+ZomkjSnx5c36ius4SuniS/k2kNTpTAM0bq
CIxUFPr9PMfQNEJ2poaPNVHvNwWNMS2YSAM3JAF7Q3W0fmfJhnEo+U6VQK2+SNPVoTy7TDmjM2Kc
h8gNsdtrp2obq45sRwVSjFenxRTwCVaM3e6STyDdbHTgISooayI9l8Ys1sNJDvmrOKNietYWUpKc
QH6qXR4fvKSBdntEKCQUmSntS7j0XzypryJgxsKtOqWt8bBhxz+SwBV8VWX9lkKn3WSdEDg7SqUX
fJUX3XcmvVL1q1lK2+SRblfuNHa4c/dgmiUNpBg6nlMsT9YsNZ1NUO/4wOe0dctWr4VO73vy7scq
XObgWpLhM95fRXot/2NKMBac5ml+nzRuOm164bPEQeBs2dSR2djfvjSU7k+oia+yshKqhBhe5fsk
zpMseo6Vghx889lW9XoO6tR4L5z00AuGHVG193Kd0MDGvODnqxMNjEAhkC4fG/BDh/XXwYoRnmSQ
5eTL+F9XESMdidtpWQQJe29UK21k0+aAvRdeWR06DyOwhilGREC9di3IfAsKXicUw5+cc8Qkqjka
3O21b+Z04g/TkOyfpI6Th7M8bzgA2KKU0BQ6Sw9Akzmd4/sfZhcuWX80bybsaz36KyqmH/3bmSLh
ULzLwbN0cPvKXVc5gwkBJizYgZEe4s+UJ6zlKWGkp4RKqQSkE1hdNX4P27vFt/qpmrBDv01WzJ7W
6VMAM4kpY/u2pE/k+xRTEi4sa2XofwPcoFfMWWArIiwwNaprRkC39dL0t0S69hsPGDhW9AeNH2QX
lpcL91OzXFJrVasEpWAftblLYFnKbAgiqpUuWUFYRzSvA74SMrxpYsitf6qgAJ6Go/ttOkej1xOX
yvHCFZugKjHUYnI6ZCl7IKuInoPTJ4031HHGSO/Uu5VG5Dobx9hW27b1zMCkeghidU82kdk3D7oB
10J/BSDf93eN2IXu4jhswAnuseJ03VU8dUDfIfompt0Bo3Bt3fC2hy05eJKNtWgYfCht2FVDGw0x
TYSpLMPpk3yfqalyzRVNQRcZKmJ959OZPhl5xTBQvEMVzz34nVBseNhvC9o/qeooXyB8PsKZ/Ju8
WZBMqPUd+CHBrwbbvb2kI6/xXtfsANaC3Wh/3W3jn3h1+HT4tguRdUU6v/K6LN1tP9sD/5WiZ1/f
8v6W1i/p9ZfaJfRgAZ1LBI4mTiEsHYAcEYPJzopnRM5iEtRzJW4n4waT9ezYtpaTlPhWIFzEm0oB
pfS/MrjkHzOwkpA0Nfm8RiSMOoWNZAQJODvQQyF3IcXeMx7RHs1briqpKkkCkNaoonpHDVlr9vEW
/sQ/WVpPqSNjLN73mI2aEdzYHApvG+X8jSRSpKMcofGQTIhSb8I2i3hSmJQMjnx4CVHkbfNRzfwg
hKC3qWSoGiXQOj6RowpLTkL/JmvQbGPwnUK9m5sX7ARR3FW2/pWz4fZjAuaNWbslXqYmor5yzuiS
tDB8hoIVaEtgZZ6kc+f7yIyvMjZpUFZleg4OZyjqOYtyGLxQ7oJ7zjIK4HupYHkSae6mjNXtPwp9
0kMO/PLXaZDhF4+WBSYUtqnHoKW8gcMliTKti5lJ1aWm8EHU+vlpH5A4+IMkwZ7idIOLFUlWoh9g
tv5XZpnT8QEYTvkJ0g7arToB1LrUfqhMqDl23Jn9RC70u9Jb//eyqcH5Kk/LO+vOzoFQzaDYI93M
UxmtyI2b7PFGPvGKpJpmsCtU52OUSh515tm3GU+yMVRttnpqtUMVmxas9REznFGl2ujLgkyG6cM1
jy7E1ap6wMLQIkhp8VAFM0aqQNjD7eVL2vblp2APImUcTP+DJWlDaZCXzCzWnFIQkWxSeZhjlLu3
wxfV+eev6/z8/4N6o5WSW2AE6GZpsIBYbBOMR7pbbp/d0GThosNN6jfH8hLNFRxGbPxl6ejSpvQK
yKtdaxzG74hGTs0cqtBADpPXDRR8w2nOKbzCZ5czOKT8YCJOS5BG0+2kARCrHxwWZ/e8th7HxAq5
vM7W1CLiS2Tj3bsJa1SlrgnCFSEEoLu0PV4M0l8eiwy5ebiPnzH9Py+MCdjM6x4OjX/BUzoUWBZv
WJX7O5CDy31mSwx8sLwabY7iMyf9k6dMF8cKJHXf+nEA3LWamKTZuLBkRWr0/7RTtvGN9Kzd2YDv
8VyLIwECLHOnTkVDDJqEKI5KQ4sVHe9HZI4pqw3RG1ufKicPrSQKcqlFQ937eY6aXbAKfjLe5WNU
TsqH5xueSwBBMvaiIrPuRMcRkbBBClNb64JnY3fTxFUhqxr2g5lZJcpAfa1/1dzFrRJ9dPW4xORk
ZEG/FX54wXBUe1JG2oH+UtPozq6URMgN0R65kFDjV1ScZ2d3q68i9zzL9W897mM4rOg4/Mphq2AI
jtt/92WrZbi2UOQ+jbbXkM8MN1/nfHNUM7nm2ywPqAnWHxgwmLI3MeRY48ulLetA3XA2joBZQiMX
HcIr8rhOotQL0PWENGaQWfxZkiopyqITA3kaZyrtsAsW0C5pEZvhTkenn1JRuOc92RxaDZrCcNWo
u9ZMdZpzTi/LoyggZZDCzL+I9W/Z7eimXZO0oPXRdFVVKvq2OFI7qZmmsLcV3WMQNlH5yqcqrv2v
ASMfHDpCJeatwws9p3SqhtDlgVzEgRv3iWkR2qHMnQBBC5bK5mDLYPrD8zLQvJnZjJ806PbpdOrs
LMJj/Z7y4uyxIi+QE6LWQy2OJpI1BrKX9BxfpR5PfNCaqKP4CZ3GXIrf6pGdI2A6Z4Yt2MJU9VnN
LEVk16n0NNeng0iuCOtsfjQx4UOwxiFtrYwE75ZfAmecgJ4eizojnjyKA5KmiW6zg1ueLcc2NVjO
yO+4MjTBxrdxdktRqxfhogVKzD71SekeD64sWaPYflrxozSK5WjKBxO8f9edtPg5kO8YVFRVXymR
0V9esOMlhSEIf2wwbTcrqUGiI4Hwj2ohwVKD/Pzg8r+nAYlte1/SzN/bocxPCA5dEvjqxVXlNg1K
0aV7CzuW4kgy7FDRWr8uxrERS+4Ey857YZgz/oiyxAEAiri1azxkI7rqmfJq0P6tA1NuM43sHMyt
/W0REl6SabDPDhznpqovHzzWJEMtGWQ5BfaPo9MAOmBYlq3V0HfpZoBjlsWPPHuagHDnBkro5XvO
YYeOHizcWk92exs0haUwAJKq1Fjo2aWzi4FO7Ecm+o2z1J9Ib8+RDJ0Srrd7yv4CmuwX2bUk79u9
/VQjw/8bPh5sBMqKEuJcePRQwxAv9dex/WayfwHuvQhFsTCtukaQdtCO292W4NS9Yg2B+qwwaj0i
Sx9pCLt2WPZybRXoucLyNUaktfxRsJB5oNJW2XBtm4PUYhhzKM+hIYfafYNv2SO7I/VVRGCI34RF
MYr0+gLwjjjZjLzSkyBm/iyOU+pHBbVjQMEGvwAUZXkLHBTSTmJYSCw0mUjdZGON+qRLz1/zT4G+
fPIE63lik47zmGog48b1d9A4gfWqFy+kQ5XVGr4udePWDQminSDi+UvNBkNgNEmEJZCmiQvhoNL2
g6+2v5FgX2eT7Mb8dO+n54Sq4SthacX+VqDC79eEgvyixVGt1GMoPOojot1i4rZhLkINLjeMUgky
wopR11Zyab6TBwnYmBas5sN9L/ajC/Q2OVB3yUoihp87RlS6F400dKXvUWp3kiWuiIwY0RG5JBpO
BYDc7atNLXvfVXY47RFEIslNnD0YzCUEvqDN+TABMIHyNA824xxMEAPk2WJXGoIHWTsz9sBW8OsT
PFD92DACVy0Az+6UtbbP8DgCtoHjZ0mBrTkN1/ytjZRgx2ScJ0tx4lOolR9zBQAljcL/rMrVEVWI
7An9+skViSvpBZBMmVjAFYDW+DG65vVZS62qwmNhlTKG5fwjGRqEE7tgp3ycoRYQLRCgJeESb1Vb
2s2d4WS7hNFGiw/uS5RshIInSyIn6M2TiM8Tbq0nsEDALHJAxPs2bP5YLMVdeZl69L1f3c4RUKQv
7eIIB/vxsP+Xg6+VMfQrN2ijmA8X/I1Z7OkqrPy3BIGQH/1CUQ8VPoMXNkzL2wb4PAMUukRshaf5
DeaWLPsV7rz06L1wVjFBPfBK4TxfKw9ZsG0WW4iFNLiotQiS9Xpt9ebORZhZlew16I153JfvZ+5e
0YcESpLEmKlcTQLpg2V8/V1bjtgDTAIEIBLRVjvw9C9BqZtWM7Kyf4SrSIs9bSbCAO7UI9lnC4jg
7BiF66yBGtPhit3GViAkjrtzZXVuSkYpAxl04goevLUbewfNkcgaPlzvRlMWfX7dxHh0JVVuwMT9
WDp4VgYiUblr51eMHUTqyU7Jt4RZfK5divwkul5XZ2m1Kn0wbvc/bBc2cIoS6f7n3cf51ehV5rxp
GVdcaOPmxlliM25/x5l7vuV7nkJsxOnWKiLNtM2SCu/+GMaH0uC+vYy4pAzNi2+5pjI1oW4GIOGo
JUroRXmaqJlYjAng3fEBUpQ4zvFL2NA2R/WCbXKYiU0kMqtNgfyKYAaVMJGS+2Y9ybVKJ2vvDRQX
1MsLn8YhCcdLrEivv1cvYCY/+VFbWZnWyov11oKiF0tRfyKotHjw2bz27uTecv98FOBGvrSbSIMO
aXTKma30i+okDlPD367pzzgekj2tt0sBhQQ4br8DSjrPCuFLFMo6y7glMIOWb+RMPsQpLccKuiWh
llgTLtjuv4W5zfdsLPOAYgLI3zQRpF/avUJ6Fzfn4UVM4QtzqKCo5Pc664qio74M3vSvYgNDACWo
QlF2lOEOfBHJR3MwmxxjU2MKX8zexgqvNPstd6Q6VqtpL1R8/+7dwPIGpC6wbpeGso/OG+39Mag4
dSz67uJQmZDyTcKOMpz4MDF4t9iG5HSz3lEk2c5SAmp0kb+SfqSIQjDAekI3LHEhZCMNJm1gaasm
E7pnBRytsLYJAJ80LnoA6ozA1/d8nnfE9/c8IRAbxbGQUmxDT2W2nJa98BevfxJ05aK9PGTTZiH7
MuycFv2kKhmoU61dajtl+0rEbV+e4mTKnago7BNw3aYG8MS0RKpo4eY/+AZhx7uOsSzHDxrDrnGU
Znh/QSRuVxiBeqqQ2O8rqdY/QFXiBtSzDmMrVD4TBiXqq6gKBdNBX62yt13ovtddsLSwxqAepPHH
t/CgXpADrD/nLIM2VBztrNggeRF5v0ZYkokPoATWb2Bls4W9loGjlp0NIIu83IW/DlxWixJQJA10
663WRYrNUEBUTAv9e4+jIG44ifsx5JdiVTT8Jc2O7eydcJ3Y1cmxPhuqOSeTr9T/saBq7f2tasLh
7a+FpqHid2e0Ead6O5FPCwDyjO0znJ5MoFvPYQCr1aKvCXZrN8WfJ2YL1jz7L8GGpN/oheTBystw
1ANSZG+Hc9UOWr6vVgS5ZMIqohYTX7ohxkxsvL8j1uv2PeDzu/z/HpmnPfMIDnhZnwi7C/qt2sr+
U6OfeDOU2m6O21vP+Q7TKkuLHmT3MXLRWmQe556/Mj5z87OL8xOQTTOOcAC5p5Wb4rBcuPQNpQAD
mQawHKTZyrP7F8A9CpUcJyjOsSmW++2J7qf6xAi29W4UmY7J0tSR0Ap6TswM8CbVZ2ovCaWoAE8M
Wu9nVEoMccFfkKSO+iDJotgOAh80oOIBHM7Pv3Xq41j+zpFdjvalU8Aer5Z2r+8LRphcf1NZifQg
n7YOBWYkXbpAfVJu/UdXXQx0YlcP4iHTVC/3/ASlQZ4esney9YuhXhnVdYypOo9b7Xrk029QuuD4
5qPt5mMfSN886EPLP5cKe0v80czML/ZPaDpLTSaGr1nPKzme+1HTv2mFzR91CuMRb0ZrjffhHaEv
pow8aJa4VT/XuKuMLV6DuH7C3Zk+NDGuUvFculJaZKQSFTz/DmucKhh4iI5NObUmv+MNhT3eflag
1xJP5fzMIz1/D2eQppW/uAHOYwQ1Cwe53jJICm9V2jP6Advdi25LQ9F/MLZIbXq/i47vKO898Iqw
66B+fNhm/LKPeA+EB3EgbsbJX5cNbB/7uXWKEntpZ37qOmOs78jSLkFS+3T9ue7lU8Vgx/za2DeO
e5d5fZ7htZ77CyRS7u3T7ycIg9bRKkAgu9FXY/4T91xs27yLOd/0OUmdVnKo25ldBrC8J5cajcUX
Q0qFPb5CUWupVF68TYBe5uwsOQHMnkMay6pMAlEpGaSj9h3uI5Dp9eD1bwwyM3xskUigj0Q0qGSO
AamYDnJpR7983O3R2bnrilXElTSPlDY3ML0oibnAYtvFP0+TzIuOHvgOpTX7WG5WZ0QhjzEPEBBG
LZV+TO3da5MJQc0VJ7B2ADY/M8w4BezQandonBty8Vu1fD5PwsjNnyabIzpK5R+zBdRxDsK5AV5G
iKu2LFphNpLNdbGKRYADIFwBa8wJT+mJixRg9P/BQ5EKx1DeUhKczi3kF0RvvbZwKB8o5sB25f+v
zxlQUYZNQJVqwT5we/WwOPHThkINTmm/oDuYxh1sWuNQm3NZkLj8MDhYj7Mxj7FOXyuAHrjcQm4T
JMVISf2V5ZEZeyVSnUZlDx4ZcEViBSg9zHtJRsiwXrJQO0TqHxu/0dcxEesYkSVngxxKf6DbQqPs
UX35zPOJI+SK/fb/nyBF0ItqYmxnBbm5cxIX+aaOuKvcyO60AxrFJ7kPm6zxjnPz0Ah4D0vvhQAD
Mu+3rmgTmE5Um1FgEinAAFcho4mSq9dvaVDrgv6iVBrHoW2tBWirUovS7R+j2X9XrD+XTUH54RpN
I8RnvtwCoXH1xCKZlyJ0M4t8XLwwwgnU8eezmK/4vhrbCSyw6gZxjjzh87Tm8wI4BU+RinalUAJL
ijwKiGTrpL4L4Zt6JDdFcXj1gItPRq8kOeWRhg8p5o+j/rZpEz0YFo1ACJHmBJ+CTxrzNL05qJEb
hYdjLk+/vhm1NJS/1LpKqm4bxuL5Ll2ubNnLqQ4IbXcw4z13oVH0mhc6r3nwnaPr2tEn8tcnv+4H
vVfEFN98jTwiqdiIUcRLvHlN3i+t4jVgzrJF7k9LokgxsiiQfzNGGd1d5r5ahm4cIvnPxbtybOJR
F+sz2Jl5BIOaga/ZFO9+K3U8SWZZbntwTHBtnJt6D5nwpQ3wamdJCft4OLAHLftZX13IBGmrjVzs
9otO7rnOltPtrtAMn/l27gs6s3Y36oKXMCn1M8gaKCG7jGJQWcAjqj5D7Xw4Png6eZnTU+w25gO5
6pdmmKh9q/9vReuZfc4FMOAY5e4Zd0rltWmAn10PKiA9/9zY+q2V75KkfnpPkWyi197xI7nyxlsK
e1VkoM+o0Kiw5tV/2p5DSnx/BCeS43LtrSDU45LJpH8RGOMVZBGVJAsGMyAgXrtHpkhHQ06dwf+A
eEJPK9ytgIIlY+LZhCQl0cXTOhrfkYUDyCZfzmkpx6OihCqDKsrU8YIFBsa8xN1MGeAA7Ylkei7R
8DHi4pk2kRH/Op2P68ABnX7CZoTmBTk7Y5C9rPcYUEsq6WfPvbgjC4lLglTI6n9ahTR4VtcToY9e
PTf7lSwmr2zlMy73flZjpQmwzz1azA5QoDcs1tA2baJcRjyhliGdDqKcsGY56edC8yQmfR68hEID
x71ae7iJE2Mb017Z2YkxVC4JD2W16UGJEO7DnvVDGC1NNWd+q3r/jDRpmdX8OePDYL2VVtcB3/3n
9YwpOZeCd3IicnCBHDPuN9BBUoLcBOCpjJsxmRgVkZWwAywIT0YU29yrNiBFubYtayCSs49Yp8xG
cSLRvzgGy8eEYoq6LLKWOrx9FVq18x3cSw86UBovjhkmazD5iMbAk1M6IAzre76zyNBoCTSkCaXh
59+jADtN9v9LxZbgAbZVY2ZdZyx0EXYJUh0jKahMU4KgwB1iRmAXc7M9ZkJbjEY6wogJKXSe8dze
un/TDL04NqWHeZSCeGwmsuGRQtV7oIR/fCidsAhFjKz5VrBJrLK4QsaLZS3sSFfdpJ/1AbC2DWDu
cUuQH2eTX+SOuBY83e0jQVADG3EQuVu0Z1W6Ua/ZPpm8rWnLg46YsKD+hYJhKFMeo+P4qfJLWlfc
GVbuYgcsWvPi+vo7UxqQJwAr2scjd48TTsjb8iG6OhIKZN4jWYUt4bCjW5gHvoQ7lK9M62fCBrwb
xCcLCYzVYvvSYdvni/RTp4r5PQ83hCgX6CFB/0mfERfg38yP9I40LFy/0vWhdIna/INQRPs+pOd7
lQNKUqHAaplNbYnmuShA7wpnEIgcy7okO/GTHykwogP5/aJ/9HD4YpRVvgfh72beeOo8FDacVA30
TMiwS1h62uxFgZ6KdndvndCSHM4wj+77+250tgy59+gNns55Y/tw8xUydNWZVybenLJQeOGdM9+t
zpA+f882s6X7Bok4SFMOsOsWHFDjqERC6WOm7oTn5VVBYnjQHxcT4Nvv07ILCcFRynQkfyOSo1fU
cp5+n1QmCqJ+HySl6iqTSIj4jeud0ffoiWikjQwTnRTsRnifEs9axOTLhMjvoL7U9LGyUekP659F
rDLWaMvaoDdgjl5mJusSTTit5ShFXdoKpYI+DM/AhFlQoeS0E8ayp/Mv7yOl89dBZBmvDz6E0HNB
/aN/7msasM/koBwEXM0N4369qr1JA+WmAjmS3kVU7V1zRKtLDZnh0PfO7S+Q8rr9bVaKi4Gdmw00
FatfBhymiaL0DctJG0oo5HW3G+iFuhdECvwm08P6A+LpomUALeGbU6ewdsYnGOlYQeoniDGhZkeg
SfkgCHwn5erM3AURJDj+ovpVjm8p17061DubBLAC7JzcVuoRd8utYowx+uriNKMZBlmCHIw3pgQk
WNzvifUt8MfFDdTQgPyVpekHdpLEsjIcmajBSDLUskYgp6BbzWNXwao7cT9kmm/vP9NPgn6P+STs
iFM6HK9CQ9JUg0tApZVMG291stjQ70MNoJmY3ldCi9Sz7ZHBTxBrBVpTrEgfmpLnKwJr66MQKMYT
DBxBE9HgONyhxPhozXl2BIUVMb6lQkwawcvmfD/Cco7Ppz96aSeRRyNZd0GPgs+DbJkeVHSJg8rS
yWPI5ugtNfehpkPs6cB1ly20r8CyW6o+tX76nxaN4UincdDXtGypaJUCi55vvFz0EfJjFGrpwvc6
ff4D2x2Mr7AcuYl515VyXYdR3x03cCbGRGOd5v31c56g1RY+MNsnfyocUaTqvchd09sdAXfa140M
KmlLsTQmF8czhi5+E6HWqIXr1RHoSL57eSlJi1HH/ztW6N4BXewM4npKvqYYvkD+cuHHVBLTy0V/
WwBgMIoIuV8vejiTKLXD8wHsfJXvIZZuAfNDHZXZiT6Iw/OSv4tRxGuWWX6xCVy3Covqmokd3o4d
B6MwGGuHIz3qfgAg89gTzip+kdnjT74heRFLLBhVJmjROjWr6CjBqxtIpgaeCA6F+Tu77ZDUI9RZ
SqEe/DEHMEPJ6XUUumkgI6ez9PcIKkJ3Nox2gYAgykKcQ36Bd7nFw4mfUeItB8s3sLEhM48c6dO5
Ws4Z87kvXNEG+xVSpH4mVq6ucSv2q/sUzGgt1EcDCBxL2+rrtnwvLp7GDY/fSBR/jI3YLff5Vkpy
fbcSA43iapws+Eqo4oQeJm+p+oJa2O5G29oNSm2T5KGG6O9B0uoba4sxhJFoHe2l7JwZWhA2y1uP
zpWT/RldC1qkvr9cbURlRLOfHiRmaUFUzTYsBB5JDg+vsC5FBS+MIJ/RamHeJ2rQZrwXPYX0vqdL
1zh2p+EbzlyGf61pW7YseDX3g0NCV10qv4hmYDBOjq4dLlv2JdhEVCKo0iM2syxKQq2TM0akqjRk
9TPCrDf4wbi4zojS+cLP3KR9t3YeJHipPsLU7iJVMihMS3ymkiHHV9SmAuHLeo2rDjc00eMJkSlq
/r9g8MFumPJHklNRrWqr0UmiVtdlXpkjYos5BoLo3bnkmEOp0QTdrk7VDxIGwsQBWLZSNOSVGkdd
iXsJoSxdOr7GgH4f0h0a2kZGG2fBbA7oXZNaLiTUCVQpuiz6YUdZFNYgBvulrjdhpK9CGMFtMH7c
L1CZLX1GfptM0rw7z4acvgzWvgnIGk8RBqThuFa42M7tVWOQdwRXdlThhz46BwTdbqz9P0ev/v70
w3COQuNWy67/kZHJsM3YBi/mmUJd/YZiWSghDVJfr068x6YMmDCVwUDO5Gp1MW5G0YV8RwLakEf0
yxdqmGqs0M4Qhvh1XTh1iqLV+F5S9Q88ymCDFpilYHo1sW/Q9DSXmKNSHGCXf93lKAavNXsa/5eF
CKmF5UEjIpXlcyfZDKSh+mMilRDcS6NDw7oOB0jWx5rwlvT04K7IWfqf6wqMGhYA9I/js0jYjeTV
JnGu04QSlsnSbPSoBko7fOC+75JEDZbFo+Sl9foo+SirfDfVf65q0G9v/86ySs4nrrsDvXZASuaQ
pcS0ymuv53yiAA5ilAwtWk0OiKdf9MaMBUq0Xer1As0XeVYjp8pUvMxGxqlDzc9gw2EfxkabenfY
pyesNHSoYr/C7RweCcCsQRzlK1rNqhzaf6ah48Ll9pAwktG/PJVeV2fE3m2jXgow3gT+q6rnYVMK
zDi6LKEYjzd1QtesLMG9/lmPu6uoqrR3CbXtM49y4t0VRREy33bP5ifRnyQ1QZlMpKj/5kGqPRa3
bVbPYzDfNfnaSAutut4+CjqZq64kp+cCky6h0dMpmkJDdDqiUOHTDLFXtHg+lUTUUciyBTK1JHQV
2wC/HhxR6rogrH910jt0/NCpbIolzKJsbQcoKhIxFyE9RuAoK8yjerUY6PUJr53HhQU/XHJLfNIS
u2nqpAZBf+fb28Iu8UJubDeNWicwDGryr8z6+gvgt3waKixx8uXGHkzetvaOkEvFn48C7yCb01BI
ybiEyidMwgB2X8DPLNH3l4drA74lLANuI1exUdkkkzLJuJAsrUJoxxZKo5/084nPZwrZQHJG1LTJ
u4hfZI6hds1OPvJF/mXstnnN26gXdthZssWWisamEL9DSyJAYOLWC7U3wA5xeVLDNORJWXvs/0q6
G/FW9hYrhu02i1fNkgRXJzLd5+nbtoWFHe+dNPqMJ1O1FyXDiJHddH89dnXVJfunD/e81LqMxfP0
OC6k8/nmizYt7mQmXe9T1fZrEyevsiWWwIhY94IUxLgAcDHE4jUE8U6w1pstgSgCMdmvmzxHks59
kijNQqXzEZ05/zRSNcmJR1IgQh+2QMidFodZTYrjJqTUABCMU2QRYD824u8dWGSQPXbv6nPvOlYM
RsPG4zqg6LydHJ4xzNWDV2oiGPNKw2hxvDn8GSzX4EXyAP9eKXI/tcUAMGvM+nejJn0ODITtyo18
nnNsd2nWapy/Niqpp7PBmEDrILvMpnlsifNRg/YFYw7SUDbpv/+0vpR0xwkFN8MmxHBAczlJe2MQ
2Ix/9zfgnlMDCdgC8gWJYLngs/H1ZSuhn6dCbgYvIxG66SdU+NLZ7nG901oO+sIZL6iSDbmrsyZS
ZedDxZiAiRSnNNwr1E4CjSldrorKv4FtaADaOoJsoPTbqdTzvUf/G+dk/F1nZkWlDavIOygvYED1
i/YpaMUkJxeoh7torr2WrLD0wA/bCPZ2BJODXmyBXbdSWe9YiFxw4b4Wab/k6pr4rRCfU7GRwQA1
XLo/wnx3Z8YDrA2p2b8t3KFATP1tLQQ5D/RXPe/xmr2LLPWjsoS5brUvppnDcczlgwSO7IbV30tw
DdZRdJc4ZhH4glJqxh8Bjo3MbkGGwQYa0aXCh5ssId9oW7hnbRi3hA0r4mEqRpp4P6zn6aRIXwnl
lBW4nb85LV9qNmbICrdXyH3rlljq92lbJnXG5JZHd3H8kYY79DTdIDl7n3R7mc4isABhkWf4sEEw
hh9Cd6jV4Evqj3o5l7CdhLn1tcn1h4jkqCO8FH1RDlgp2fuvCjEn1n5WDkcJ615r0ajSyLcYkwrb
oTRPmWFxdRaxxEjVUKP+Yf4x7WmSABjqLI2qEwjdeJLWPnyzQ5qvHBjQ1dJqidK+oCo6lCWoP6NB
v/LIhv1BNiHZzEj7EQl1HkANyYf0WqCWYu1+5JVRveRY8FSNrb9zyZLBOZaCG3Rm/9BmQX+QoCD6
xxASe9Sc3MCxEc75S+36hVI9flSJ/gMXyiGtWlUxQ51hwqwWmj/gGOjccmEQsnpXsEHt4gtC7EaV
a5WqDd9SXtXd1XwNu66GTT9SDpZ2XXVN3lH3NkUA6IyLj9/3qUb8CGw+U98nbl+E86n4ghkWxWNw
RyE2iiH/XXaK0z002w6CvKRD4NoZdet6uZHrF526hzvB5mrAgMQt7CZkVrIK9vdD4tphLDznHvkQ
v66eEFgG04+g7xia7b7Hrf7NpA/iNdMPVzZVe0jdLBSzWxcTfNGGHJU1kGreXizL14lT09ExiBX/
ONLbjFje6o/5TAf0Mup8X9arpcIR0p6w+q+Yqi0RKODPnGhwdE12SahXyeKLRMEg7EgLuIi937q1
Y101878sNVftePTRPdFD+mtk3Ban52faf3rJLruCwEk58lVvvMW2wk8CR/Hi0rc/pna9u7kbG5ug
9Q89Oew9HDwr63FVH0OZBowjETu+06QKKS14mXigEtq8mSarrBJO0hJTPOqIm46tLzKWBk9T0aCI
txQtblL+boJa5eLiI1DOjdb03Fhv92qHytbOW41dPywvGWOSt3gbzrnuw4hh4exQMAIvgTDD4bYE
5w/I4MnjiwmOMl9xxHFZcKahUCrt5WRLxzoq6hdDC5hd7cr1AyttBODuXKbW1VmSwhwbM05qEnM+
DiGimkwG81j2Givgz4/hAvgW6u/aP1EVrQSp5Bn/WMm7VvElFDICKcxxhYGbi9Xne9ALHt+ZLcfy
JgoI2MhBkkLp429UPSssL+5tbDE1LicIGLENn7dzMKrIzJSv92nhDWARfAKeBj2cBYdV1I5YCpEa
IQTm2enfl7CL7VTkcwbWNzwipYNRwTYjhR9bfOZRJKEG6oZjHlpv6xAEeR4CuYvFtdizlERoPI6B
bYsfRf2E+QjkxuBi5m9WldLQkU8SZZ62bI6SRXMVeunA5dXMdPHW54fdrL/r9sIkpLQf4bxbjLB1
zSPRK+/1eCjW0z6kZPGCnJWJKChdYqVdiT11PGnLIgeCSSWHJYzZbE80bNx5zHXU/tRyhEelW2C5
6NZbqIQiHs9sCMXNldgGV1nw1I2kNgYSTQ+203JBDteMbzy0mlIG2N6XU8KV1flDqz3mkWK7DpJr
1h+Lr7AsqY6GzDaXWjlIEJwensX8f0H17wiedfSQxQpjgQD/8Jn5OU0H7hriUNet9U6oXOOqFbbv
1tzFH7k5Q981qxTnDlBJY1Mgk90gt2vy0J9k9zaA+sdlWalYH+yJfGDpq+DgGEIcPnXBl52GSJ+v
04qzDyrWtmW2d/tQGZFseMarmqD0SZ8gyJ4UJOMN2cpGt7FHz3pWVhUaD6Tekt85/oUMxAIbrolN
cAAAHcPc4NCm+64fBau6DFxLWrjVRXdbpI6kS5/7OHoB2YsNQqYFB574iN3BG1qQSJisQsTCL3KI
f6P9owEeO5FRGtADa14jj5LSHSitmK89iV2k5bdMMt+xI92STrVZwhkAqyiH535fXZ6wnXigvhTp
PXzWkx75hRufwCMNeInKhXwaP3rKwjL2zAbAXd+J6rgj6JedPOb2K2or76TYX1d8yQbK2Q2zHTXN
HSUqo3WudqOe+fFh+/5DhKT+FT8Xh6VT404wX8zKflbchEp+0Gehzad7aOYKECZQmjxTWXXxfhIk
fEoGTXHhZOUf1Rr5AdrsdeooxsNu+iPj62LuMBzuOxXiUW8jLSLhQP041JERTLfiGjdPK6JvwbNd
DEYScT5RYEbtDIfmYSHtnFmx+o0NfpWVBaNAfXjCUToghbLEHLZXARyjZ9Sw2pNVU0BP6Q4pHP7h
U90yJ8/4RmdJRUT6ICr4yojLpQdPvQfUuJUruko61h7g12rmCLMValIMDq/klXHk4s+XUZ9g9tfA
g/oGmWMrSg7sn6nrwnQ3EIZlxWCGIDg3/LC8OJsn4RMSGcqDadpZhe2ccmDySD0INoueCzAHMwZt
Ls2zpbDoEW6eVkYjD7rH05S/k73OsrZzgRkkMKgB9ceWfAUFXuoKVZpjWt9uRbOQt188nxBsiZpY
LKzYLz4j2g3Lpfbryw+U6meLL54FfTjII2sy7uu3A6xA4lNkUyzKMqv7RyJdSiPsg96tpFsKlUwp
A91fqVjKJFYLYPA7ULqUM0j3KxsgxFSzITvOxZ9LJFM3mQAAsFcVxAHulF7WW0NhsCRrK0etwCTU
eIixznkwZJrfMweYQyKQuuO1I8JYbZN2y+tzdY0E5jXVTNymMx8fpmafEnpgmAf9stGtp13m8sKV
2yR+UdyjRMIV0D7CERn10wz7q/wS6GWc1koSCdxGCiEbwcJwbiztZxumQbcmh3tM6ikZD8Nwbh0C
0oHezKHJFwwQa6NBR6UwxGeJu8oEDBc8MQ9iHGmlVZPAAZpYW3B16dtraAtzswBmC+7aNh6hdlwV
WB/EA7APuxKqcBs7z0eKGHJMsf/PyYNk+cg6wJiLa5Kv3GNbJCv3Gs2fhUqQQqh/RVMhFKghOmOe
W5N5JWAc2xE1wMsj/7tUYZsUL591N98ClN9LrEQbMool4e1r83uCxshN/xv/+9J/gFo9eNNGg4eH
BY/aS3kgfLXvu85Ld474kpkaJAGJzsgg6JVgBZgvqTib6UqqNqpzJqrfmN6k/MIvf0XHi1qE7mdI
4DuOMKYUMdZSD2eSCm/Jxb0iRfZmKgz5M3S+lFi6p4uO+7zwcGBdvYmXW7cJ6kvhCsMPyw494/78
TC/ViET/M7unIukIR3V4QS2yartBtYYxD6Posg6bxK7ougnaYGNWFNbxNnMsjY1JL8q25JwuMgzA
tDXp+kU6zpxK4mOHv+URZ63Eb3Bqfwq/FLx/FyzmDc2qmG7xTLAfBEeGM+L3rpo2x8g6mmO3EiWY
C+uR1vJVwPIYLGKutN/RVRk0aERpkpOYgwSMgvJMR3W0d3qSxLW6FJTPwDQo0gzE4XPwo/Mq2xlc
agQ/4GcYAMUlgbT+dKYBnEpbAIxJsawJswZSsa5NaTbNrBQcE14LZIffUH28FB2ZR8a1z82VzPkj
z1oMvjpfb/IY33+2rS/ZFO0m4lXePETvn7nLMbWKuaSpV6734+WhrPW8bOB1m68ENCN4St4ZcrLf
bb/1eH2znWJI5SQxXtk0CAdfB87al9ttkxJBIl/lt63C8CrNe5hTGQe91Z9tUxAeaCECgTbzSmbi
E62fXyJp4F//dRgKYgtb7JvY/VuH6/12Lj8RPKCvh97Z/kGy4W95mbtAgzRo7pQk/N6pRVRC4S05
zg/IlH5FMrPueUcaPCd1RWHovwxbWS7g3MeTCRmqN7vluhNN5fc1N1x8oaoREDeN7PunMDQb9tCz
3HLyEpO4UvhK91O2IZIGEKcS0Bir6InK8kHXGs4kQTyRcPZgNHFM4yb+2vEAdKOI9++8YXTUkno/
y/2ukbaDwTsZFXhiRWny25UxHdzmh87OV8nRK69brdEWCFZvL03a4HyiYLya9fDk2P0+0nKND+jb
rIVMPmI6n8nDRudZrnIH7gNfwAqzfH1uPRzu1JQ884JpRizvC6zxWo+fyLKaibL0iFF4ZmIvVAcp
WsgCBFGLoz2zvXLDgSeUl+79TPpKt1x3Dh1C5CIoy6Dkv7NWqWB1zH3pH/ezk+Th1jGjyA00TteD
nZESifARB2gxID+9KQQJn9NxadWj9mxdKlGWgRw/peut77vI/IpmXq2zc7v4qzk3VEIrS6znn8MD
95elEuuvaFtDkDyeNWXd1OIn+AfExaWgxqc+iLiXFrVHqQp7Fr7M+wUMb1RcPJN1OJ73E3PVlsHB
PQEOaVJkEzz4wGgXRlXhDMjqEWuxtj0sqAZtUxNQrwy/AHtMquDUHs2/eVhxQhsiQcGonJIiS0Eb
kXcLXxhEqa3dTXW7QYiJ6sE1DoWPn56ezACc+O9VLLtoImCTtKkIUUmhgx6U2uWnWZBG4WvtUO4a
FMtXlqQdSsabFQmzXP5cIPZ/xe2ji5anfvhQ8zf1kdHmJCG8w+qpv4ki68kJy79KHenvszzV+kY0
5AAmKBwaxrgcoHB9ABSOGgYgCtqodRJ2qD6E0q8I+598g/XWoFnHJzl6YAVB6PmudYQhcLWkr9Fs
ov8J7/60bFg4Nnba5qVioJaSCAuXR5vonxXXYVU9AXlNgesSZtv/It26CbAZ7Ioq6ml0/oVRba+U
/dkwVXEFVbQl7bcEJEO/PEE20r7vcUwWQwqgBBOAEdaYxd/4Tw2Y1VTagiWI44NdCK2MipKEfOwC
oF/cYqHPgLnpA7DKcnPa9s6u36k2rqxYnqoWppVdrezWrm5IcbUI8xCvZ+SwD8Wi6BFXwIxHyOyj
co6ty5HsP9ddVfNZPqhsmtZ7iQeKti3P/a3koX3Sz1AGUiWp/u3iyqXXJ7sJewtJnR/S6oO/Ryb7
mRfMKIwKZXhRQDjumSLuxXrS1zti0y+YD37gx+E6E2DLnHb9aCxbvya3nFRvpsxWjP+XdSVHsGtO
a8ot0Gyhfhdas9k+GVj43NChpd/TLV5ATit3YwIBayB/PeU5t7D0i/1DcUeBbDlv8peWpvBNlGuZ
PIb4TmP2rLZ1Tg3bnr70rddJy4Hpl+f/B50k1ZAQqK9kzPMhIPhCi2rMrTP4XhNd+Tz8r5DRKakR
GmlNyFE7Y/BTFLvCEdkV4QCLiOdkWrdtKST7jT0/EjPOa2Ak49TGCm+JVJX4tBd+Yk0F8c796yue
a0iyJ7ACTjPw4+8IHOaDi3LktirCQG1dX6JqTqa20lUdL6tqmtX8p9Vx68ISz/zZ3ejJP4HPw98K
G3EcodUoE2E4eHKbqVE6mhbgV0GhRIQ/zikODGYLep2Fj0ylOdDZGHJ1P9Dsb/20e7Y3HiFA/mIk
dM3vY0z7i+5RjvTXb4g03/WjaEfAXYOX8zF7wDD4Z8K3/vvvJb0sdNbRXpUysKv1LJi3N98LCcYR
j6wYKoLQc6L5v2fLo/t9Uz6rrN1ayjiZ19IWns0dnO+xn3OR1w5ZKGumF72fInrsOnUKQu7y4v25
Q0K2B+Y2Hqxqu9wjzYjHaVEBKFQPQBgbNQulYmD3WkchWhIyPkqQ9oHIbmnLCI6+BL3to20u09xu
NmjBbUIEIJVz5CjMjBJzp5+KMYBH++RE6q7ZytwaJnq2HBGMxJwuzyfSOsMPIKg6t/0Mh5HJWfG5
uY2z6Bd9DSfGDLHGKE1kXcNzaFw/Z65EtXPBOoqReidNh6pnewLk1DfbMAgBrkbkRxyqR0GMtRAg
u4lz5HIHveKVqBLhcw/G5cfwpzgsia4OC7gcXGtbTDH/sABt8/RZna9X8vONjnarul3St+eGU86v
NDre4ebACyS2D7eOL22p0n2iQB80WdvmlBtcZY9x2qEJ4aoS/X30XVv8YhwlvhQVuXVWiOOs3sDa
A0DR3FKBdVtVk75QICCbKnFAc1BBgI9JfcrsFLc2F6h4ipsjB8T9b86jjoGtgOqMkH44TCHXQaLV
VAP9k1lQ+7hFKoPrWb9dxB9CeNrqzIQxYEIwKuHLGpXFTNklo4NH7i94wInpL/xePWBHLtzLGW26
REuELcxM+/ExGIu7hpZ2EnY3CxiOiMpw4BoUMvPU8hoasV2/BcDai8CvpmFkrzxj3fyhZ8TPjfMe
ea98H89tlOKqjwII9zK1fI6Fu6etAYavWugTZNrK+Bn4/fBpF3xSoM0GOr67Rielg7XFPBzXz9x6
nvOpGyDyI+yDfIglVkF9ua02LkePBafyNUd7/F0p5O6ASh/OdZx0Shh5z5L5gkH/dD3P/5+tbMeG
fr4ZIFb2YwfSDNmF2GyWuPqDZKdgfJRsv6bfeUCZdTIM0fChuHxz1UbPa3sEFgPLVEi6YxI8jbn8
mV7ggiIS9QpQFMUwC4SYP5Se8R1iids5p1Kp4BnuxuLNoJLbwB4XC+b3lYr9DjXO4Os+jlJHDrl6
4DUuuoEkYY41XRGIVvzg6Cl+ILrwIc0HcN7Xtsnt/8T++4VV9REhgAFJ26CDie7umU0me/Lvty+B
WbSZ6iIqf/QtqjPtoMrgmvu7vhpocymMNw0+3E1bx5w2r560cCdf/iCOFP69G9HxYes32K/Iym4F
8xOrdRB2oaYEtkhvIvwRWXx8dlJomVPaK0lNHzOn4uQucvEM1AKMYERs4Wyesec8Jut0cIEQqPyx
s48+W7h69cFaSk9ev2zL2/YHsXXSNZCGW43k+Q52WPitwWeAjvRDUTDpxjnzYLgfHphYS4njnuZY
fXWDxblL1FPKOAycDHfJLJbnvM5lrHPp1F19tsKRo+Pk4ee5XjbP+sLlENyfo8Nmv87RWBz/b1R8
lHorBFWEN4Y2QPtn/9qo9dsgGm/s9UUOhvcW50EEmSXa85xYZ1jwaZso8bcUNeIUdE8Pv4FPESPl
MGYUIqXT9dqQaXwbAMSaUDgG9ADIk7+gz7/TwI4mwOoNbw6TORVGpTDJMu8npbiiAVyavjYj0iaM
IkCXXfIMhvRiWHpNsxLRXLK83cuD8MGaNZ4tsVpt+Hg40QISGBCMkCmdofvboyw7DZYwYbhztz9q
y4bzJeixqW7rFxMgXVRB8ZE05pUkGtHjxPbqZrA21bAH17pnqnNz1FmXfqPEKM/QnkRLC3VSH+wu
aw59IkIrdocCWxxWS4JhCCHvxnUk6u/FqjxHd1aq4ap2lKIC9nBd8UBREU0D97kjW1harRgVSxLK
jI9gLP99jRs9nQ+xub2mfO6gk94hb954SDVuummZxUeVCmyHUDtPqT/ZkIvGXIUHa2vG33YqvVBd
/KM7vCuT6P+t14IYNVZgVxADQe8vwlkYaiUblt5np08qkNEllLkJGFH27EI0pZMK19FCMEkhsz3U
wmUDMRsTuJefFQ5d2aQn1061AnroDm5hjFnmPpOZ0HfCEYmV8vbTiK2kc3TwldPL7BB3LDwv+6sC
Lc3Rg2OzJFWoreuQR2ukzAyQYIPZ/wj9+Hl0eX6vQZ6zm5de2FiPoC3cY+wt5GmqRzcSIAwFHuHB
FcQiXsvOo3g0kfbZtpiCZJQHgMaEMeTJ/Xaas5MXDAM7+1xF+LS2Yn0ekDWkdzft9s3/Ded2sRqd
kANkBpuRZ98Em9mazGQVnRow+J/8c123PqVLqWRbQPBaHfpwfcfPdGgaf9zfL7dh/VGrpum1kNEX
WKmN/AME/uvj2UvZq04FeFqj2DprtpDZILNb3+OUCz0KKM4WmqZQCroe4c/eAmxkD4EJR81R+Cwt
gJil6tayVHDjeGn4+7jf2OYwwI9njuv1RZ7KbjMFD1G/b2bDlC/LLxcg0NWeDvrEKGSktCJULUlJ
RfBF4TF5rIm/ft4GEWmL9XvKnXq81XPuhsVO6XI7o+sVEyJA/eCTSSM5cr9iaL6Lu98yq5BTBFjk
ISrkncfHNyOPm23tsoIxC56ZamylB4o62Rag/19iHDbTzORbhjhk4A2ZWWl4xpIO/Xh4bVKntzZx
oTeTa8av78r9R2gd/IffbIFoSeUJEo+IfVDD5JJ6YuH/r0XBFc88TpqRFNcpPvxWvLFORBCD/VIt
gk0CuK9lyWgyGVLh9o2M1GGQUBtdjQI8GlVTmw67zWVFV/a/wpRkxUi4WVdc3+Q+6Ogjq3TMAxhS
oPwZ+Eto4gaO/kYI5TcdhpOM1vh/Npl8K594iWxJI+q9ceWAQdTu/mksbfmjANljuL5GX2ZRWF7T
gqnRkfvgnbBlX5a5jXtbXcV102i1dby+ZYQql7zENCnqM0rqr7SDEQgBF+5ILDiKFQDlFjrnC1Qd
fAgc0wnHzpPWKBmg4O8O3aWyz2qqUlvCdF8ovE+eDzmfLfdJ5Fm9ErINETeEBP/0LholIGVCp041
87XCZAkRapkCzFHR3VMdE9VjmEGpxlvg18pKcEBBcfg3hOOOZ96ZQe2cqw1lFwSBtIaHK6b822A1
2iNB312AFB/3GSWewDEge/t9GkleqyNUJxEZE8CBG4KVrbQQvpL6pSil1bEO83LwZz5WrWn2adac
GddjMp6TN9ZN4Xk0lbCIJvoG3CvkCj/L2Dry3xM7uB0V2/N6bx8UCyKlGRfKcwl4993vvphfVLuZ
z4iJucv4UNJB3l4sLDIz+ltjjKUCRU405ucrk0TouRWOYFw/KeVEMp+W3iyfmUL1LDjBF0WKobth
1+QU3FoBpnGbLKuxuBwGHDnXVe4L7XWckpXsWbxFxv//2xu4oT1yo8oVA4Mnp9v92xv9VN17yCQO
wDnWpdxw/ADZgyVbyBG3K1woOhseq/27L3hAl64lBX3ittFbC0KnlnPuvKoPAVV3ZKlNgFKeXwLG
OEMfbN+Y9muT5PpTM8FbkRTfvX9XSdCxStgGfp/4fbEJ2Kgs/BXuY3CIDzaHJMiQxhfjfHqwns5W
2JP7l+VkYCj/jm8vzWn685BkdJNGdanZTVEF3hp/xOQAEiRIFre2vkfPzyR5zts4Sd3mPAw9i7/y
VcYYIATNzAWK3XKWz96aGlzASYfIa6ojlNmmEO2M8n8cP5foa8ItnzhQegibjoKnRHR9WX+iEHqI
xAXO2r48T4uouA2H86yqX+Dj7x+0t7ZjBLFie7stmeMYk72fmpMGTcrsEbJCVhxqmdwTnsKfuaJY
+KHcMgsI49opIXetP5Eer05lb7N1OfaX/tgTjLU2lXYRkDHRqMHfQrlhDASVujMrrahxX7lydBj/
NbLXBXyKYKWhJ0A6NkVbQsbv4DYNy11roP+LLm6SYRRclAbQS/KV30CYQmVN3yVyTEgb+v1bk+RZ
QD//sl9gZxr8imRv5NEIQrQJIY5+Szm/NZxcQ08R+/VMlmRvlM4OO2/dlLmUt83/mg5DpREQjxXP
kpKqtxBGBLV2pmcGOuRHqd4b1L/xOF5A3qIg8kRuAkFstjC9uAvow7EIQU85nbU6jVs/BU31XCpt
9dRQQq13XPXbTlKxe80HfoKlOiOF0nvDnLej/VMY9zV8MRHtIQr5O/jNy2PrY1AXdeqmCBKgLOpn
2DzhHktzwVHhgIeeecFie/y+iiAcNpQ2E+hPd6FnYxBE6Y+L6/lso8OpTPuugZit5O71rouAJyvW
MDePURuYLvRSGJ9zqaNl+1aAegBJcZfwKgEYppDFC/bTW0TVdAmKQSordpZwrJixNqgC+aZEKSoy
TFbkD0DYofrqB2k+D4oDOexKRGbytalW4giGzdqdFr2lp0zPcJh3/CbE3OUp/qWP1zLy14VaLV/D
efwysr28/LEbh6UJv1u/gsauQ97K5iQUrcNEzB4gs1G6JV+50edx6OLOlu1NxvNCU4+c7Qh1Jaoc
n8Houq3ETHzxDG06GZFJevuadjKbsS19h+s5+2VkdBIeQfz5KoXp/622QlfypTyYH6HqjZ/DFVup
iy5J/rCE8Yc/GaCAfMh0Ebg2LwsgPxdi6ppcOp4IYn3EunqNfGBoXe+/hBBzG9ZEiTLp+N3D7EwL
w7iEZHFVm+qgfKmW/86zEffXlOj/pbe5GT60ZATCdammgoRtgkfMRfeR7Vt4UtVl8hmEma6gUcS4
4GCvmIRLVOFT0A5966ddSUrSWw7zr8f9r1MIaon+8gWLn+ibIovKvzvvpfwngMUcZSh2HIlBDu6k
HQEHgFtcS+hjV6DtYvIVM1w7I/ULOcJVHxftKpWWDhxVZBzIGgef4ZCZMRNFjZbXqaRV/Ec7CdR2
d9Sr6pq5eutOfCRBr/Q/mJE7Twda9cYO5T0+LqLwUq3H6Je+E61dsXPx0l6pPFc3JqTYSUeIbpd2
PseQpVBZRhnHBNk/2YJYDxCxlqUWv5rYu71cmsR0CJe2gwIDammnTIUAgm+iHE7emhqgB2aQWfNr
5VCNTXT9SsMXJfPbOGCiPXkCPObAJK413vs8ScEryrWByJl4TTKPYFRoQxiwEha7SBF6YRw1ENPm
nzeEHAqLE/qMAUOpdVjv1pgRt67YkFLSZbhZGY7HWj3nwc+lAvO6HpyDj8LVjQefwGq9uMBW1Gy+
uutIDJhgP/AtjRMvNT/y3ub9Oa91YRjj5iqrj+P6e8aiFld66jgDEuqbFkSWJQsQ5G7+0HDx1m2w
ByUOlsqmWQqJg9PDYibtGG6cWkPlR0nNTEo29xHxACslfE5GmJ3VL5v/OpRx6IEtQ6U5NQ7F8DPz
Utk69xOalfrKOXYZxrkZk79Z/C2F3L9VF/u6F1oVi+O0QZlpp+rgBqKatJaf30ALJ3Oe3tGaaF6Y
ok8JcZPhvYbLtDK5bHZRdmdaHF5CMVURofsmBQc5PYH65GiFVD8lV4LsOzFwxyiS33dps3FSarzn
alCaK5/ufJmRM0Gr6WtHPsvByw+9FB2LfJIMqhYRnR6mRRCkfUjr2BUwJ7fTtpO6SJvwoNVJcFrg
MRUur2PBvn/VKKU3QPA24g+hqg11KbhiunQ5V7fCGVtElFpI/FJwJtHbXmDyXFEcgEgzCuUuCp3Q
/GZOFfVEimZC+7BCvlKq4FqsFOyqw8j4FsJBg1yuxi/IIXtYWv58Zc9On+YsxHHd+/tkFpscfJka
hi6ZuvwnljOOz7DObbRO4IPiEe0P3XSGtj3Fks61SAZNK3DEzhfuVOprrvU9MscZNyBSzeF41+fU
EbUKPQoYzo0DFfsmF7RffnqGHTyfC3XvLDwOTJ20+/6Tc1vbiSuC3N4bg/BFB8GYpwW97d6Qatgr
JYex+/oKfIRHWgfdz8OVLG7LgGM2c2d9pxeNTDM2Mk7O0zc4gE9BRS2Y9Xb/B8QRzdt8ZV8mHenC
FukIo+0rQ90wHJuBLojvAVig9O/KAxlJpOsaeRBMbazRwVh6HYxwBzJhktOWlPxGmezjdGd5Ev+L
DzaI9pLvorx7tnylx+btrqQYDp5+71SgyqfkY8TxX2AGk4sngcQD7rsMUwfIZ/CHkrBCubhTxagz
WnYO0o0vunWB7rJ+TpWAkfJeFRDfMMljk3SEPBX+iBvX55cMEcPIq7kVkNw2YhK6IG9LCnFBg0vu
aiYL9zR6t20Y6YFEzd5EIbltaycn5O2oslI2NhyIgCzUqq9OPSkYrLzcMeH39WPHwIqCuf7ZzCme
zUG4kPzkal4pa9frP1RzGhFyY6Fi+4NEJIABD9w9rUKHU/T8hhNV/qb1Oy5zpOjOezkB6D41QFad
Q8r0mWuD2J7HBeBbS6/YyLarlptbbdTxfNGLJTXTas1i/pyz3+YJRcuJyWXmiBY5nvSYkBOwmerl
1YngR/aURrkka5dRbntkBF6JnA0c0OKWQTR7e/1W8UvrqdumDlLnmr7xWRzqmRvEQD7ylxznaiGx
7F2/Xnd2rEg72a09bboeXRgAwUeb9F9G+cOmgCBc6NEqAywC3OfR969DBMp9OyILW1FdQWb1Zd4Y
b3NVpNvZxo9PN5tCuobf0Ay1QpCk9IiBsoL6WLRybifHtGjPCOi3IXTSMm+9Ls2KnxLW6PBlXdML
RUwuIbXuihMRcKz0X7Y4PhhZ75YcITZje4/OWuGZrfxhfXja1Pxs54Y8HKMnI2GrLOYolWF9Oap3
1MgAtkJS3eZDW+33bnVKyraQZ2IvXwS1S8O6STfd5OuFOUEORT3FsddiLSCxVryFNFiEZnNo780m
+okJ9OtlvjFP27Wltfdr21FRK69JEQMSKsglwEEsJBqHoRaQMKa86ODf9+TfrA25fiaSogrbPrny
IoZw/+vrbjRWB9OSN3uK92Mhv67+c+v0KjCFHSXJvV7a5Q82xmZh0QpU2TFlL+03m8gemfFc1OiR
eCTDSxbVjcAnL98hHdQc2US45hpbE2xDnBDCvpOXEhHPaD1Y311l5CF9WoOo7A4HSOZeSxArA7eq
n105K+RDVCpDV8H54zekhXAsNF6mpjaOIt7P3kOOxTZzQPPAAHhzGmYJjFXbpcbwOgEZkZN7zO2T
CVwz+HJqoAoVmIZkG7+km+c4Lm0CgAxdnDrk0SaYi4AVG//7TBfVk/an889YcjmVOtp4/LTnm2sO
i3/dAkSnOLA1MBf2IguCQ/P3eKKh1QR/z2jiwoHiCM6txOFBBJjSwYTwIMh9IMlJJ0rpM0rgwWSZ
pCtl79NvybKJOK5IlwXAhenv//iq+AymSkxlZLpuauFfdVtUjrRoQOZmvvqmBpW3XLmhFZ1XNTHB
whiQQzrFEqKpJ7C+GLcat0Qk5xBbq58Z3LIN1YCru+HOe7MIf0um2ZGJfCw3itds18iVfBKIfrCz
m66jkP+9YIme3wErW8/5P2q+SnTml7XhIG1jCEFkMYLhWJlcbGn97ZSvy+nS4bg1vdKufJ7tSLl+
1UATnO6x/M9LUFeuxpEGBU5c/CkGRw8p32yms/NIBvo3etyCQpM1lyg2oYh4TDK5U1CXLR2jQkaX
g18ZyhAVCztHj2FutqDmepmm4rWruHhQx7tJGAm9uxnd553Pr+fTtcNwEZ4o0lfgQM4LqV0RiM0W
TEzhr+XZTGZSSLn7v/rfhGufk6oIzeUzro+JjLWLolGLBLbTpEPMWnLYFdSMuLLJGP8p79+QXNoi
xFFTH7JXC6tgfzeemmaz0dB7nijSWAjg56R4i9n9gis/u101MVIsWl7wkp4KP7BkmCUyPc764Maf
bdgKo4IYBfN48nvFnjrhEhhL5aF2DzulUXNg4VpqQt0sXOGPaEBZlhn62Pw1viMrzO6vxODcNY6v
Qn0bP5cw8PuARDudMIIyJpcLiwB+mbwDvxfhn4helyTe6kOPkScgFxkkvOJiMFPys4WeqwMc7Wim
AzttbePVNOZxcM4caDNpgkvGuptXiTRVJGMgGnFalOFORunD6381PB55jewBxkbmdApiDUBkXRy1
QxgODEm/SLpcIdtocxkJ7q6T7diTyQn1s788h2Og2HxVjpXRPUAMebZO60o3qz+pcuxBAaVMzThF
sHHE4OS2EtJNwFLcVME+KU4OE8G3HJVyu21sCvU+Mm3iLet1C80xGCNJ03K2Us3LjsQWht8LhON6
NZiyA3egYOFFHuenzSvFTZmzyv0w+q6mw0WykYytmG1rM1HS/ThLYpAacin1vSrcWUtf1Or+V1U1
ssuuAOZcYHNa5c4DiTIJpvhwEliJwBmjFInKVK30HxqUd4vanTJ0uVtjbSLt9Ac4ZIZl/0qVs8F7
Dr/mvvz5bQvbY6WoEHXBbuWVsKzBfxQSYRur+XpLcXEDpY3VYVhDMjeLYDifu9U9qKSVLMgt0GZO
5zUm+E6Ev5l7JFqdsWDa5Pv6tg54T9IVIzvRPtkR6lzpeN796w+hf1PGxoYRzTN1E5i69Ibl1Ex1
DQfpswueAzvM789xmhR4ag+BkOCRxGtu/nNNcrKmHZTj/+tLQ8Wk3fDrNbiOFNbsNQECc0eABKc+
935WmyHXRzbkylZDa/ZCFdrLwhj0+DvRk1xgVzZE/C7KiiD5G3m/bh8+RNG7pjBdo4fx8xvjFF5m
IsTDRAjy2IrA1Ba/27WAULYfo8wAJp9Oz7ljLWknqrd6siWgEvcnnxg1aMVmrMKnYY4KrNDPAZp8
5TN3nqrHQqS70ZOPEB/aMLmlsAsxZUryCaJIbRB7F5JrFnUHB8qJYpaKDwuEVb427APFxm4KrXnz
mayi2ZDJk+HgCNOZuifElm9jG4E51cqqqUUlZkNHx8PnbcVvB69GBec0tZFtLwlHpnTFupw5PNVB
ihFllHmo+gU+t/EuCwhGWxg2D2C9f8D6PTV7odxkfdlb/cEoiIIUs4GultIJPrb6eaA9nHzrYo3G
inKnXSALwTCRZhB4mKjh+UKf78AZ9lJ58FnIXpcjflAKaEpnCJ0jqax4AljqJSXcypPY3oP1+LL6
BPsaAi/e8teSRF+EGJlLziqMZFG2JJMzfFrA3jXjDTZa1Y34cvAezlXnKFIpb+FDfVfPPkJEbEmL
Lu/EC1SQQb4/ophse730zMMzJloo1KIG6AnLMy242/IAlxRU7oup0uWMUQGlRjtwzax6F2IXxSP/
nasXzETuHn15iyWsBfFDQ0f60eEJhUsMJ3EJfghjI3H3sBGdFpFes5lvAItcxS6O0U1h5lmCaKnX
6X495SF6IEkQp/XcBvGhIqY92jq+wm0RDX75UzP1tQ/K0Avkxiw1WljH0rzkEVknzHySwJpxfu9P
3xposMPeA/JytmbwTS5DGzBssQWtnUcf4bO+m6DnFgCqAb3YqLiG12Fmu1k5GhR8WWi/MZfi7oln
hgOr3Q37xyArIip1fGm/QmUHO72DTd1eMAAvZGEkkLOOg0qeZv9LRkKnRCF3s2w+SmS8n1WpIrd9
NHWHJiHALLiZbcPekRn9SUNhtKn1VUixr38I9IgmG3ArBpvSx2XqMHbM6bJntQGtRv1gtbVb9oKx
05ueijRcsT/iNYnHTu/X1QkfEbeTnuo5G9EMbJJ47Kas7bXW8cnqkResazUqF67zqpbmf0bcHAsX
gwl4Kwb8EWpAIgiyxWee3FevQi2XDYXWaBdw2UJZ3fQLUs7RJxrh14AkXAgMGMe3dJKDBVjqlLgm
3/EkYQPvdAUQiK7MtkZOy/aV8nXSokJ3BY84xIgDnIj9tHrKzJyeS5210Iq+mBS3Lxx3Ub7/nVJ1
GSX7fNREtvoU5SWgzrmOXcIwbcwTVg0jgDQ8/oepkukYyNefBgVxRPB8jXdOGGII0HfnmxAcLGqR
EkaRgRtHSXsa4fMTIXw3eesuziF/MctLefcvPlLQVK4ClbT6xH6gCWb9ynK5nVGBmjBk3x6GPbCe
/MMBVIleBX5hnv9mmWzdbD+8y9Z9L8fhfIcWA9fmfWllXBodCew93OKijMx7WU47n0h89G8b5/ne
+gqQ/YwAo/WIiyoxkPnFJMOruaafN1+1O85iguzWi60neXXcAgmpGWowVewD+R0MXpJvDAjTIteI
qAKJcGEdqjER9eelWNvwegexxin+QbZ/1WlLtRDn7AjKwW3nycMNUEUaWhsnMStbppHZM3ogFO7w
YwbpP9DZ672B9Jx3jwPzVdurq+M8hU9Jz7qmItpqJfLUfbc5tun0CLZx3mtT0PiMxuw1PPg+tQuU
2oFvu9L65axyxXaeIoKBRqiqJs22czOvHKPwq+q791eYtbhU8uoBDEstkzq65BML3G19j+tYkCqv
FWkMyW+4V303lE29hnvNx13D73mEp3D2GzoYyg16fld6R3dKOOuyIMeNcbrGS1qJQZ9fgUd/+HXe
EtlRYDORIJDfQCVoCD5qI3hg1y8ymupVb7A466fUYmSoUDkb1SrWNEJJgKLnuOukRttL+kkjY8wk
oMEVt3sNXhdfH+wS78d10ezPTwRK2l/lqa4OPvccB0quSNTv4tDI640LKTptwxNBIEhmwOe+7t/Q
luDdFtFVwUp7fznw79WxDuXS8tRVZw69qII0RGmpt7vjKWfrDZQy0fncrH/5L4E1PiE7zehFIWCz
mIwF/sy4SsluV3Yj2tdA4/2shJZy9O/ajalCDF6rIeackZStkPH5QghQMeON1d4/ercQc38dyiEo
DhgHvKbAO1o1RZ7WSVsqey5WAn4jQkBotnbqDBZQSU2fd8ixvL8LVflmIw7PYQtfzCVyQ2E8AeVj
8bP3tjY2aSkNpYW6+s8R6sl3Kk1mbRHX+2lC7RnHs1hZHJopZ0Y+/9T8zEp+B6U/2TmCCrLXGdgh
531zUjyZ4DKvzTq3vJ2yKooClfEHpSeN7VSR+wnhR0JNM8Y/fHyzdkvELHa8BJa6X5xyHsyRXeQK
fQ4J9NXc62hOap3pj87lg38RRVjLqdQclvPbEuU4/DQ1pv1IrBndtc7IHIa24ZgN/q0qZviJpHH1
HK0f+rQm7mE/vqzTbDK0yQOV1D0wEHHSKfhuezkSnEEjmXmv9bXx5dB1177bxSm/nM0PnL2dAvys
baukyKKqoUjE6og0ayG4SlcLfuQGXjRluKnWIAdekEtOvPv3jWH+B5iCmxfM/HICCmjmKNV/FQp3
wgDfvidH5qPUsa5sV8c+ov5Ey1RS0sXZ8xfuS3lnledhsCEkD1a7LoLikwrIu4xeMRvS+l5aSEzy
Q5fERoKSzngokMQgUGk83O+UAsT6tPRqdS8QEWMvHV9zd1tVW6ywUp6otAWde5p9nVvNcZnLOg76
MaRWjBqgvpzqEnjC4y1kT1vOSJpqBi6IChFrHKOqed9Rt69VTxNWG1+uz7yBzNuPUHx2keX2dOaR
e0OOakYwVG213dtB5uTsF6kYF/VxVbHZe+NwO3uS3c+ipsJN0+T1xgQhe83muJ596sCkywtVZpTq
/dRv3lUVn6N6XzaS7EHF6cUOMzsV/i5BkC63n9njPjIcL8UmAsyPhiiYLpakQ7XzatIUiHvC46ZH
XaVmbI0TLjJ3C6hq1R1tHn0c2nh+aVq04yWBUtvfgdrSidZLSP8yEPQ+BrfB/9omApWSK/8go9Gk
uQhkoSiFk7r8zkBa59MvDOx9BnYEYR7OdJwnEqPZP9cphR2g7EqZZii2iDXuDjMR+y0XilXxj6r9
/HhhnDJo2Shc2ddhxq2cV8dt+IQh6gzSudw4qk+1BhX+HTpL0RR4HsVTLc7Ka6hQhbyCsfkgqgvG
YVulrekt/CRg6MHy9SumTWbeiLUte9i+Z6cOwkWDmH3e8n6rSFPOIJCvnTlcO3kw2n3qr91yUgZ4
EEDBigWN5VIQA9ghPGSv9mT/W57vY3Hnb6w8OnKS5sphZX1vfylakZVZRbTe8BBt6HFOYlPQf0RL
SJFejpyYStePp1hgsSNNxXIvC6DoLpwlQkeblNRpeKV/jrF3AjTLGXMNjZYi6hniuR7OslGJN9bk
Bt/B2339EruTBykLagZvrSpKLNbd7d1w7al6t1ICMsjEInIBksVal2NSY71L5MfFLvIh460dO4e5
IOwsbwMZou1rdZdNJi/KAJ/MDRYrx52/h79IDAM9DjUKpdWRdZQCd5SPizGDvo3tFB8CxvcsKdbv
+8j1iYRCLuay0rUVxM2K+KpYDQJU8XJ4fsab3E/P+knijw6FUPOPaVNlG7zDpaDakAPl1ExBWvUY
kN1eorMzgGgtbyInAaEpp1T4CBCOhl7rq/f8sWEddQbHzt2mz36I3VtgJri+1oPj6XXp8yqoplAd
TAcMj2bzwLJKvV2JOT1ZsmR2dCvuSYHWo0XWgDyjgP8YPmj9DP9zJLavtkNuR3Yo9a673pyLqCKN
18dwv0moSuGnuPceEXOtmVCBkyqN9tA0QnpaWRx+qfdgnDK/hp3PeZ/Hy+y+FB0yA6Sps3gnE51c
qnNOCh8EpXEeZLVWLYBRSAKfVcqiBA3LAkRZ+m4f+coP24YSxl0+GV1AILU/HgmnhLnnLNxM3U3p
DXmn6uwRFxkAtYNIJwfCav2XktHorfximz6rVatt4f9pONvZ2loHAfT2pWqggqC8ioiAu2AUKLIV
qYRCqNFMTGUM3+NZwaDK8JZ09izcpxY1paCG0OWzPyDuiSTHnXLcm8M4gDghI6SO4e2keN17IZI3
tOZ2JmiB+Buyobc3JAMVZVtrxMI4XrVna281bfYqPLogfGnIGubrqA96yTjjdTokqEBO2+CNy3d/
RN0DshJg5Vc/PQmeMW8BX9H2pHldfmlcmeuhygcUnirPTDx4/A+I0tBh4h3B/BHV5/o8Bbjqegu4
BtdqhSZla580lejTiuIKt1uqb99zvOfso37UfzCM3yt9YFKI5VOcOm28M7oXgLdvpFF7rgNeSph+
hMCMXs1CNe2e7i9A8yZyfA18A5lgctgvBYlRhrPoC+falxMy1PF2MAvQsulcIOGqeNdYMVtgccO3
0iQ7g3Np+kUcvQIOCMLTCinEKEiLOgEmqMGE8qfFyeEGULBCPDVpjqICgcpln8vClzcgOg3CRt5j
5qOigIxhS+M6bvZqfBslFJrjvgjAv36lZf4JJaMPaZ5O6WKRReaISE2Pz48k3NpT5rKFwcNcz0N1
T5ShcXHZd797inzoP/MT0f1y1NO9UaZ3T/hB/Mp8yzmF9m1fcnhHq8vgpTTSYXpVIEZSWbI3UFdQ
y9n/2ciwJuhtbVvxfFMjMdOLTXMFgZB98k3iXS1gOLq15j0XazvwOCoWdNVRnBmpFIeuL+EiAKfY
T86PIBUWNdN/OnIEGzujEctLiSDGFn95WjDnMt5ltZFn5veZcTVtRTJgwU6hCZ3eMC0RrGrIwRJf
jsD3u8lP8pfi64TRzWDL+vc+MSzkg4jBUrR5yqB/YoH/9KqF0X1zMzROm60FVmIR1I5RFVQ8XvfU
2HNx6i66RUeKx5iZw277LEiIe0uj//mywRyDZ1MuoGLsbzGF4d4cN6Z2nSApcDg1VwCR3RYuVmrV
bDAkioO6IK7JSH5FIU5ufnMAzkP1JjXqDdHC+D9BRSR074z5PcQjYL+3Pk+mhngcAaP+ciEtPtbR
D0YmUWdxxdY7b9UBJP6KtsB2jPKuOsL9Xoji1FAytGEWbCr6J3ElcCD8p+iLOsmzt2Cj6Obd5IXA
BmC/M1B94HU1X6a/uyqg90MJ5ZpKMwFkHurTW33kFbQ8BYUFYshGjR/cLqce1nlxyfrs8Rgw2KXq
2ARpA2orUsaAF+mM3dMV5DOvuLtGI/Ogtk55jtV4LsPqcEEXTc8KZ1g0hzHoQWB0VBlHknw/OMjW
UQCoP2MuidFUtpzRLhvs3pdc67Ok2+iRI0wsiLuSsjQqXzhaNii8mk/SmFJKNnZ2U2PCs7y14d7M
TasmVwUZuDHkvtgTr8hU+FUJnvktjtjiEkEaSSO+icot25GJvcUJezPWEjRWk6YejBPPqr0cKRly
6uuiXCHWUy5Dq33Nl8A2L52K8LfgHxX1u7sxwfMVXd4l1tNH05y+rs06BxnefrTWcnGXiIGhTz/A
gt1Z/ltKrKEh+uzFmbvyu4Am/CY+vp3PlYjwR/F61q0QMluUbg7N5v7LU0bEvRM0EvaQFBmXUypy
Ctcn6rwHHgs2XG62tBmvIO0v1UC75fS/pNFPjin+RslqsLExFC3ZEZ1P5mqEuCV0MzSjZNtCsepX
tux9oqIKZEUlOfcNeUbB/nPCiVPMjmrB/qljiYOm0OqXlMzMaaN/j/sToxrwIBUlmFyTPY8yzNmI
V2UQW/Lc9gS/bNM8F1TISaocl6krsWOpp9cwgUQ+2tVYISScgLxMhW1QcqH3RzBnwQx13YD/jbcF
A3p1evMgh5DS7DQ5JbZ9eGhyqvcQHFADGy1KuWV26mmt/rGdrHuZip7xxH54g8u019EeRDGfbPx4
yx5qGzpyALKOA5hCzhv88mfcQdmE6alUAAUbXlshreKoVl/LmWaLQPlLKxxZ5G0vy8VoxDUItZem
jAXdXoN0i9My25ZNFeBqam0DV7CHBRFaUK8kPzJkvTwjXAEjzyu7GTrLI8hkhuEVS6FZKnWokENz
1G+xkpRQHLsC7/cMTi6XgHoQ4c53DHWH8Hd140+DakIy1RJOpNu5hTAzBsnhuVkhP7Sv5HZ18jJi
QN9P6aRmfWxuMMPejQ8wQ1bnaJOOylhCFWdlERF3hSJwutVIngWHeb8ovF3WzKxurP1XglDoweD4
Ddzf7jO56xZ/US96CL10aCIdRMUpJVP8nTmRh8ENSszyPTcLxH5UmfMS7ZxXAWzxHjLdyVub1knP
3rL1X6Yw9iyYDie/9kBDdkLeHolyH9QP0netpnD8odq5AmWzF4P6zNozhZFjw6YMEqg9fG1Xtk34
EkIEPeyQBe8QLXx+sEgzD4mnITp1/WA+zZfoCbufN0ovVaVRzoSprnDJ02/DJ5x6r3kMw++Vey41
fjcoJdQsMwn6AwuBiPV4ELJoxtSLO1U+Z68ANy8EOOvu8QxoQUYPbk0KoSgGfhQwlVnFY+Su7/va
f6+Z+Om3pqVCc1uuLKsjc+fbMyrc2btCTMBDxbkQZMKQqdeFNJyhDEQd5f62m17Td9qRuZTsA4YB
I+PCVVKrvFYsZ0mh5S4KH/wSuPYZL4gzPAopCDF+4LOV+3iLKqjXD0E3LCzBHOk2dHZ20cpB1MHi
T1R6azUIW/uriC3Ef5df1RY7agSt6D0QIP05BZAYIF5JvdJEi2zdAXuWE3IRAHCYgT/N7VUyPhu3
+48jPgiKEwwlSZEkiLM/tb8QkAgqxKngF5ptiQeHVAP/V8O9+8H9VCBh2HBDY/qYB6L5zTgp0/Cv
QhFnuuQd2XrNQAuQBECnM1CsUdBWm1Mzl1QDhXJH61jBjwOrzXvIK0odD9yje8YkvUjCqUEfV1Xt
cWSsNUXS8sa0ehTWTxj4L8fbp6JqEmZI1A9w/fGFBheXUK6wtn7Kz5z5a68ASXStYRF5plIy+wA8
/7BQrwQL2HzMQL5ZRiV4mJrTitLJ+3o07Y8ABlikQ4pvMUxePSNCBca3+bAoG8S2ICijbL4Bm2AV
fEzRgTo8YLzGEBMHE9kWVJ2q2an6TKlsxZz+YshUHPe9Mf/+/hHekyqaU92gOx9a4vmLYyo/v421
4fbzXCA0pjrpZS9dws7DzTyweYp9x2UntjZTF+gTNi9bMK/pxBnPrYygcfe/GXB5NfHDo4ZLcn6L
xHKHlA66hDdAfappji0PpQpQadfilUDsA1Xjni8JdzRhnp4DSACCk4/ycDVlJSBt75ugehVsGoaa
Z1HcvjmEOXxzAbAFFhaIo1GorMhDICw/eo6oG4hNP+k5dkb4U/G8dw7YclNtswoY31Y8ppaS0W1U
uQj4PbW107Z41SOCmhdb9++S2FX5SQ+YtfgPFgnoOwzFgAiX71rFjlu1Mh5pazorsUziLjjHYDAR
YtTwgcUIcUo5iAFQR6d7ItgPfaAGeXKaQSK9D1TXuYqjgvC8ILDpOPYD70Wat9uL3qmfOaQvurSb
rqW/8fEIIQvSSpvRJpQTuUELhezcOn36U938MPgWvvF+15ZDlHzZvjhUwhN/sNrRRiRM4mlavAZI
2FUx0UYms2z5T1WOa+GgsnjoC1gj/WxN1tEDeb5mGFWRvABxzd2s11lz8T49o4G7xBKFiH+bvhzs
L5ar3Hyp4FrgoXCgy/yiJcmP7ncLFAIwG1h3ypYLoOuSnG/HC3lMFG9foPePV7WIQtE2E9T5WVQT
BhTtmL3WuGVeeFlwB0pqEPUKihsMnlCoxWInVukEKKOZKhdfpC1QWavZL0g643AhqS/IMzyFYSao
mgGr+7+D1zfW81XfyLPC5JenruShNYw5XbupUSsSFvYlx2SbXwE0+Ncmom1ufUJdQ4C/2/GWg6+F
pIMuG2J/ABunR5GuUwsOJOpbcsb+wz893HqLEqSv7A6MyxOLmaTILE2sZbOgeap0RlwPNidGwBVg
L2PxxCD+Rj9vpUP6jiW41xiIXXWssIbG8O31Nz1SBTHBSj2lxTPWSU/YVlBOxb9kwCZ18AmVyFfV
d+R+KArb74eICjdg8mfrJKGf7EyJ80qZXFWPk19f7Snd61vtS1Mj8js45Y8Y9Npv6Dh0G7xm6Hxs
06D2qc08hGpOGg84CSHFltP8ZNniZ0L6qK6uLZ1Jh70ZSJcJ05USlm9UeOtjnuQGD/gRDO8ELnrq
gz+NWnNtp6xBisvV65QOmzhU8lIUGHP/UbpSLvtpJ+qS56yt3mygNnT2v693ViVsJCAQSn1ha6Xu
MNszXwQruPH05zkx9meMZqJ9n1AUkhRQhtL320DafnYBRqm22FeN9N0jabZ6Wuwi6jX35QmZGW1R
tlX387X/Qy6uLfM1UKQGdCi1UCE6p8sw5Mu0KzhXBor3FHYUcE99VZkF5jvHg/rAUKI7jRC8He+Z
srwUu0DxxtwUEXNuZwaLL7+LV5+NwaNVbONvCBBi7Du/jSLn2pdT/agQajUWjpepcg67J2XYtTyT
2GyeyfyYTff2U6RxVbXDRH4UNdf92NRHsk+lWiLy0BGSPKVuMvQwO4mRWZxqciO73qTs+pi1SC2S
9jOokguoCt0Pk59GxpdkoFTmGZEpn/hnIqhC8scfr3QJVvwjBwplpyPZd6OdQnTjkGWPywg21IvU
D4gDdTfqAxPlHzplL7y/NqNXEAWynDpvdMfS/Q5Oo+/FRWPNq4tSt9hAMNDxObngu9I0aAw4Je8g
ILTPLqTprHV71BrqLRVJaqe5qAvrWKhpPOvXIoHlOwwVV5nA0K+j/KLQHyawrS8av9CK+r11Hf8f
Asq8P0ARU80FerdoJUP1YBM9TzT//CevW1HCvzkwsUpeWNhLnPBtTSEQeOgiJe6hmLbmURwhpqBy
zXdHflC5Yi/en8TSbOsReyTk/o5waCHL1U8y0ClXQCA61L1zCmUZMIer8YKqEpAkG8ycnsGgF6zQ
5hoNV6BNLQTPpwjQ1OqcPBlM5uOGXe5jT/r1mkSTRIv7WtABk3ri+hqknXMk44+X/9aQIz1xx7ER
JS4gmYoKdHi3afdhMdyAOcOmotbZ3U6/62Lc1MFWX+M59D7UTOzxOTPZphI84vYxkM2W8wHciOgr
EMIqO3g8Xzi1E/UT6vXmD3/XW2bxJCMOunqX7NvMYNSFZVBEvsmpsmvFmS64vrFiCs6lP6yvPjEX
nk5zK/eQLd9xWdj6LYIW1z688/j7j1z9mXINKq1Bi+S2e6a+VxDT4XnphuKbfMEZEx3iiidW33ra
Dn+LODaMRMPfYTJn2fM1XaT4m7OsPVyrBa0jcV9cR0zrRl4QhmZMD+5+BurzFkU+UZ0AceUy+JT6
n+jPGT8BxzwdumfK3JBDi0Ci0uXLF5ITu+pNpgfQ4U69f/uCJ782dIO7Xp43nvSlOg5kZQzBrPz0
1UE7xM6nTzHx0jCOl/WleIbNBsMrOjx9YQwUrEWBsk80SGNx4vinfMZmIrq0RtwmR7yqvt7fmni8
r454XbMBlcEEAM8gfClOMcegFt6iSpv4eIizUU0SJ3cwcpPUOfO1Wxv1fineVI0Ye0zGcZXt+WKN
CmNojKidE5WGwVblzlRS7qdaFd+Z2kSTpIIoqvW0H/0WmNrEIpLfn+Bvg6L9/+k/JqUV3QRzhnBu
n2iQI6txBkhaG0lo2aP6NXURPuawN2roLFyq/dHNkJgK/61Vo1ayIirvB1keyu586u+A2U45C67l
asEM6GwClfHbB2B0eQXFvIql68KTSK0A+R/5i5Nwe4pP9A7MQMM1S8UnPb2imtHdWYbepOkZbIus
bDqtmnCoiwZdldaMB5kjxe4vAXGVdc+2UaFKCTl9MlU10IUhaKp2n0uYARuakFlv8eVz63kQaarV
4yZIIxfzrNmyH8wpm4OnYmlITRgiowDTs2wSB2nJWutruWJ2VrXOfa/IJhacYtlM+BF5rJShd8bA
8omxQ+AsjwYaE7wfw1eimqdAL9SCgP88T/0Y46l6h8ve5JYZvhVh2MCmyXuw70yOxJr4KwZzqzFG
peYVY7oSUXKnhXivxzJEi58m5VMvUkA+mEt1sffwl9n3xb2NQLTp0e8t+hQNxOAwJwHkyqMOXHhu
V5nBIEKPKAEli6MgRRGQ4oSglP90j9kBPDxJ6nNl/a7zXaeqSOGJTRMPem4zDDWmok7S2lbVVhjx
ys0qRRVwm+brAlWUUb/T9zLwDagoQcKS6OI611B2yfOunbdKK+mjk0iYjxZvjwDuPCJj+jP4vC4G
Ci5py0UcekGDZ7fcKh+32vUAan2ynk2LZ5fD6b3y2TN8G6f49mKyt33by9D5FdVMtrNJtAgx/OgV
f1qWwEOt1N/6IOsdwxz9KPMxVAUoIYEdzZDIr/85H9DVv79gIBekcxFeZDw/0P0RhsgnroSW4wkZ
y9AnwiBAgMRE/ADKORQ1GyH8iBrNmvGYi8rBZqftvAt+sHXxiiA8ldDVroIHuH43iotP28Ak0x6C
F1Sh5qNU6LaBPQ5pUHYs5dlFwu2SVQDQFoc4LGGgsS4MmICOK9998Ils8OVozj2qwG10uzRD95c6
U27+AMqdvlpSLPGcYDIIu5vtFR6AB6wAiseADbcPyQukVXsCvafTkKMYUnkukXHtbXEfvsIllZHZ
zoMXTJww6gSwYtXwEp7130nF5YSeCcVAaUau33vQO9qrjxEsi2YQvpQ5QRePPDUo4/U3IjboVav0
4RkGLzs2AfazTgFOD8QWQw3MPe87P02qFfydIkBf4an/8j2YhxLA6Y5AjoNc1FgA3N6JrWIV1M1K
y/JBSCwlyow19I1X1nufzMst6toTHPV//SpDXJ1wQOSlnwrRLxb+fPLip5ucTvrOOUoM3kx2CJRy
CK+QWhvkKbZMoZu0Tg4Am3gNohKNo3nDobo6q0ZuaXZ1dyxyPeDhJGcdk/Ztqq1z8tx8xVD6Em/m
7obzumOpr0ANkE1roOV2yWN2y+m+1OLsUOhtSQzprU+wlUpc1uQLyq9ekruuRU1cAwyxmpr+fao4
QnY2DbTQgLZ8Gu2VFp7jpAykwAPOH/H75x6KswigYmXNBeab2R+FyX5nQv0kbbHAYKzjigRz10Fv
Xs2kGXIC8OF0sGbTtlhuZ7KB+mwc7aaMvz2UuHfh8TAPEhYNt9ogGBG4oEwn7D7DQmkyU5ZLpEQu
0NK5YkdvP+yKLj/3AaJrl8YtnmjNSXgWVrn2inyiewYkjJcQzUDHQmms+ceganhtCmM9e3LqTwuT
qP1Q0nQI8wCBcn19nWBvFzue1IVXDbvNQFJXGcAj2s3sWp+PJCUtTvqaitjBv+Rr32ZV4tTZjfpk
nOd51MknDQTH/A4O5f6wju9crNSC+VleZ4NmjqNMaT7/nJ22e64sH0+Q5bzmdBl5qWSw8vael8IW
iB9Kwi+UBYVzXFxBcrfTm5NT59Atgsx2ddEMfigsuPC/qIlHVpGJgzvFLHon4Pq/w+ZAfSWnurRx
c+e3vX1ogcZItvaeq79paOiDWRkuzFP6/H5pjvoaSHBIub3e873DizlqPEwgim83Pk62xNMAYtYo
Ea3mWShaKijY5ups6DmG0hjgenrprXX+GK004PoXC2ZFvAlru+aA34K6Wv1T7VQnxc0t5At8TXL8
mRCNHUB69ecZ8h0Rmobb0idSrAj4MTQKkbxy2zIGzy0kWrQFqTA+JVLvqnE1bx3JeYdLTOcbaMKo
JwXzNa2Sp5nimB2XNhvktXIUzNiQo0/Vs7NlStf39Qx2nhwr4y5J41az3Xnbb6wqJfxZsH6pK5hr
R4kFM9cAHTUrYqHnuTp419qj1NI3cK6V44M54mAJAGPyZdmXB1L+2OtMgn1Ll5XqA+RuvQ63HMiA
PuJbs1aIo3AxtH6Grm7Aiu1i6jse20MltAjohJDHqo2gWYFDZvy3H6uD2/gD+AXNcE1HjZGMbQrY
d/T2v76EoEVz0VKG5/YAhjPBPN4xMAD2zzcrE9LFeNK7m19zFdhGeipV5C8/KsEowwlsNDWY96SQ
pBAsh2Z90hCQ0r4gSSAnrYgC5K2pDBAxF6KynUMbcuoLBKl7wBfY69z2/rTOiaVRZ/PpZ9M/gJp7
Qb6dO0LB/svAllPwkSdjPCzHSik4ZRgxCMvG1WKEfbFqdlgCy26be5YfrRfzu+KGIJxtohGyZug1
PSRr3rDRcVao0ma0syyXH8Pbgrlnz1YSDm5I6nI+Jksw9yTY2IcqNsp+92lNEfCvHl5uw6/Zhswx
5uuBW7L4EQ20Q6W0hGguj7AGkJeq6vIC374mFwbH1os9d4BVwcyu7iS///cAD6pkRzYAQfS1qBqN
lx6NouGvgCYwAtbO2IREY1GLwxCOezGpTtF7hNddQrcm0rVCcOkJZQnfXMKzK+iy+dy/6d344eg0
kTrUOFiMDnuxL4cMgU/k6yXSYbpsS9nzROXd8QJsGxV8s9A1XXHvHVQc0fKx4Eqt93KiCsv9ZFrl
0R7Me2zLZlyl1qZFnT6vLce6zkCna10UpgllY7zBaf5haQfWg696X124ZCrqyCDR1NcQSq+5bqlu
fbVzKsgAzXk3s2M6+j+voO0QFN+hV7wdCboRFWIOGMJPh84NE9BfTiuaAL5aFawPUgHzEHW8jAXe
FQydWEYj7bwQiemfomM6OFg4Fy0N3Z9fzzWbbMut0BTWF6L9m5UkAO4/m2kMfPVnknK3f3W2sTO3
TB0usRLh3J0CLsrMaUgu0xO4uE7zE4w6X8XIkmwsIFy2Agoj/Et1KZXgLFcUtTI8+0QOZvYVUu7N
c/UOP1dC/CcwzK5OT6Et2lhpmdSPkfl/S3i8NUY1pv4y7jGUS/pY7R5J3qv1J2Pz05KWAxkbV9Lg
XVuK18dYI4WxxsoOuHwFkncJ2+V3NnfZbxxbvO0xjy3GUBNQ174UU8PbhFdPvRadkPivho3WyNMr
Iroy88D+o6/+Qz7rGuS16MZnU6x2qBV6XwTKMr4YYtCVGDfZRMj+CjiyhoAFAoVbPSyF4gvO94dO
NefH137EJTjC5t9jkmpY5z3EJ67nNBmqXyCmbvKZsFUtUv+TUboF20pmk0l/pmrWjKrnMqDWi1+2
GKuNMjqeoym6eSlswT8oe80ox7T/7fq2TeyoL6P1C01hCjnUvzGjS1ZRRfd7WwFzOEf5zs/vom2R
9lqjbouHUizJDKWTQ5ZoBPc0onwjRz1VcCAVXg9/d27LHm7RUIckYy9aZmQvDuFuVUB6Wo7mVkq9
iD046sSQLM8LzKxV/pWCPx/J8x3caNHwTT4A3ql5o8ktQiNjBsJ1fgjfMmkwy3wWuaqU6Gm1xFH5
z/MJ9OVQ02AtAcwfAAheCyykiBDFkgvNzvDZsc9EFjzuNm7sIhxxrc9xHebBhyS5YQVmobPfOxBJ
zoOaRwSSu4cxWaDrD9MZFBXHc5Uz3qiswDvQkRqjERXieXaL77wmWjokFm4o6eKD0DlOlPvhgD/L
W/8n+iBfOAnpkr2Nz5D9lJfow9hjrEk3Qhq2zXVYgGJ+4zN+jjNohYMDw3oEb9aZU8xMxY/chxUm
OmFpwBJYZ+qFUoZ40P2wh0XgRHMP+YYsf9FqYFST/4DZnNzFN7ZmrliayQw1sYHGwSDlevWRmQ8X
pjxK58jqYLg8Pig9ypAGPxGmWcwSHQ+MMbSxG2/ouU9+HJdrqIG+KSvatt2VoKD7uUzUqeoIT8xZ
K6UmvOf/imyyKHRbz15KCobBLRpKh8Vs9ZLj51a9+czBHmT5RmQf7gTjYtKgFhoiMFYqpAUJbJlc
KaNHsCNxD27g3KbDm+EEWxFi4R5HSq7wNLLi4Fq0GkTP8XpqDKdEC1L+BJ3eZhA36Zcukc4BPZsO
NQQq0PSgXfohSaeqzOjsp3nVa/ooqmamYUWFk1TbmXaPPqGBh2rbazNKSrp5zxxcUR3NW9c9xuOZ
WKop4rzOC1/msW3oUAfrtfpzD99LlZ6dxKtd9w59WTzo3M7eIwhNggBvvtiybOItECwcPrrP2yw7
6C4EO1gEOcUF7NCF1nXqDZbzpe9hLo6qz1lRWHcyw94tq6hG6cz2waRnA0hkc/63cnR9lTieVNhM
L8fpCdMWmvF1MDNKi7fbVPZcSZYu5f0520RsCzgwg+InuzQ/+We7asF0HhnQ1kCAZZi40gzneaI0
rs4Nx3tOuvnXnnzk+yKX5PQjFmuib8jemS8YzKIez3Wpnr8j4at5GeTpxTy2YZ+c8+izqFhw4qCh
yy52pSh0B4RgWl9b9gO2kU+NZ82wk78XulkLi9TyN5iCxdUfJ77bBmectpeE1ZyNsgIGu0w605t7
POykuQi8kL9/FvdwtpUk/bxC9QSqhLmXWazCOdnvYI7TWCZDtR1bWz4UpbzaApIwuxk6dI0vc7SX
YwLVn+zp3k+woN9w7v/m0wpj056UEWtWqRmIGVySk+asbn+N54kSDTzjQhHdGz3RlrEi6f3NbJ+E
YgVRQblisKBLmiNxgwCd7Xn7ojih6gFzwMTREBXVUeX2gynxsmchKOUuQs5OO2Wzvc7mJV7omxA0
ZiRWpWARWaNXAEWk2RtJXda8p/ZRQCH+6pzwNHYxAdPZ6hFdlZDVBgB1Rcpo0M2zLiWV0yUD88Yx
Q7e0Q/22xsrvtHomI4T+6NKyclcM8ap20MebcYlfeqeKyLPrunsWbDLjhxUHnElBe3gLsMamxPAz
tfiXhWgzPDR8YL7Nc2wWFF3npiPMXgQjBacCwNufKJL7ypX8u6TLwAMpoGywYr859cFyBGhwl5EO
hhxWq+4YgtLQKBom8XKCJrcEmVipaJmy7klVU9mNCybR2xiE3MI498r7qYKQ2dZScsyx0KqZIN9l
exyhjH4DF60kprf2IgJuZ4VC7qR2utQeV7eLC/QVXyeBhQpJuZc0tqmufQ9rvpkEEl/NAVcrXwgK
sI27ESIXG8+RhzQ2dehmOGn2xwoVnsGmKT6NcMeB8/x/Zv+XjLLHNO8TFs15UzCH1EgqQiF3MaWV
rNezc0jY+Q8Jbbivef+egcdtt6Jg31Uybk76uL/d76lZLgTtAZ7NzPAc9hW7bKpIFvwabmSUBrz4
BI/nSp64SOy+9OL7AOg/1dsIHBfkdcMhXYyCiFjsssVSLTl87QZ6WXGaX0v6qy+jKHnoB2YuzTWk
yo3/xok0qH+ibJ/sW3UCJcyruyvBLWYh+A2EWpv8NLN8Vcf0jwelSS8TFf+fm8RlDbYh9SPOXKDD
OFD6ZHDsR85tvErkJAwT5ikmxcxAMM8VKRp9eGy9nh0OLEZ2D/mHbaoMPMvkHET28FYDh5UR37G7
OImHD/w1zWa/oXxd1Ejf9t1zbt8f+mCSrj55FU/WKAjKe74eMJ09+O/mlUhY1muBH6KfwGaNeQKC
LDDWkfT4iRqcFO8Tl/ZUf9opUkeK3QqD9QLmi92ofGGlaeZ/wE/Z0NXeqmLftA5sYhVlFPh6b+Hu
FfGc5hDH3YDtmlI0lpc9eNyjEO9WkUE4bYYbVAlzt3rRRzhHnyye8JneGYVtAzt+zvwwqSJMGpzS
YtvgNe5nXP33QfojaMyTS7pqKolrmq3IjiIh6SDhzhMzgI7B6fLcJJWlMCtapvML56dw8n0yhzJj
fURINJ8sktbjN8BEht9ZKPY8kqTv991hgUICixVALcKBQRm5iPEDduOqTW4knr5qs5d+K/SoNwyL
WhWEAZGJ6duyFabFf5tveb6Sl+6WFYc/5NGdTGiZU1cyhezFUrsP/PKuuRkYCcnBn2LqHMYIwmZ9
GfSdsPjrzUHfMaoF/ixbvqs/AA6z13acUQ1kwKpfRzdUAACobdeugTAGgtnQIGqw5Vl8XSdSfhu3
6bCRUJPzsbSqgsjojRYJ95eMP+aavgP55QMu/0TnbHs6dX4c7R7/UDVwr3As6jwVrLdAJyQxqDWz
5gEAtl9wWlmJP4C9HLSWaVYx2r9ywjWV0cWO8X8S3kynSrVdli002BURfwI0ljKyFjglJBzXbPTq
QZIyjzs6xAEX2HZ9yI8JTHr6j5ZhH7T7MgK+2MhdfKmEeys0MNuOXnhxAPxtaBYdHBb7YaItrbR3
Bh8LYLCHFgsLT2onxaipezleeCA5B+rPc0nsfAJyVBkXvLw/AHGjuFAQKDtBvdHhx8FdBbSS59ZQ
hYkdXi5wN2pAGx7NqXA1RfnVXuDFsQOK099UfFw655f6/TmtbFmc06oxkTwo+4s1SALwoZqUVfG6
skOH13SpuG4sH0YbaWgj81uSxGr0e+WYCxplcxMI3Gnp5NE45ou8QgTwsNfHDkTn+BSQIzLQ+35r
gRBeuLeHQZ0jPAcWVbFRSk3mXjiy63FD6IHfhfLh2Sv1rtvcRmcMFWut9InNQiYqEp6kgKJFAxOO
ikO+CAkj/IXJAns0jobpCknMS4j5VVQirFguKNtAGPqd8u4ErhbW7gIGrc4qAKINbTb+ZvmLMEJ6
bqKO2HGk9p7YZIr3x7qK+TEwJDLCb1VK8j/VP6MPaBsHAwvh2xwXq6YcXQi5Lmchc0RIA3anXjOQ
v6OjWXGIFCnyXEU6IdgZApD4Ezq14bXzB5ubLNkvbRZwFsOEG5EDxrGDb/JS8wOmX9r7NX+pLbIR
qcQEJ69XWS4b/iWpwvmH0cLTZzLxFE65NTcjwO2YBhlUlEG6tWKQK+d4rQJNVf5zcgRC0j66r6Hd
FnTHAHMkW3aXcWarHGl7B79cI4o6f/BcQG0QrrM5npaO6cJ71jIibBGAKaS7Zx9/uN0zuaBnwedT
9NB8NS3bs5Ar+e2ehkDEQAzhew9bVLxmaGmML8jYrjaZ1TfQUDXwIWi4LwejJ1XFSQB4V+0nppjo
qspxNsAYfTZFsnbmECTKZTtR3MsfO6MhaQNkPa3ol1VDrdP4cAt9iMjde44ycuab6gYcwHn33LB9
2V871y7dJn3zO8HRxwp2aIDrrVVOo83QyFmFPdsgkW9DmPKTSeKYnjeAuYHDhqKqM3eTtYDRimRZ
nDCM0V5GYaULEq+ke6FH6cu2O31UFoUfNhHlCU9/V827lJIFxjkBNLphmjwwzIpgJswRFNS+CA00
m87GgqiSY11NIxId2NJa6NCTtnqybgKpRG6BdLibhvSJlX0+yRdtO8QDt6wa+5/+3RIereE8oDDc
auNaJi19ZbgNHiuSLFNRP2QI1TVmiRwQdl4i737xELyS+t3bt5awLOlAXW8kZKNHsWFxVK1V3N4i
ZIOksWK4DLFRbt7jLVvCi7u1g/K43QVLqLTr5bpC5Wj1fIDrtAuzDJXGIk5oK5MLtaK8j6/lg0Bc
Up2dsjftr/hwC0SJhqytUk4n0xAhz/otvkb4s+SYTt/h3DmwXip21PCIvvUUXLW/Key200NZYc7f
nboqF5Clgo6/iiXx0thYm/PouQIz9/OiaU5vKwJImiffT6thguzb5I4nXkDjrEBj7yT/aoIxlVFp
OCR4vD6mb0tH0Z86VZ0l56DHzlCjd8RKgTCz6MKZpY1hLNOE73iBeu5KO8LgFhOWp30mLLXzvsCy
hP17j+XMzuBR3by9QUWuarFWJmSEp/ZA28jfYAQwTVJhcbDS+fVIB9Qo/xKitUa8zfyatANP3hgR
HchmgE+epiKW7f+pl4wVwaSG8dLh+oZRXMGGaJcxGs7QlLjxcnryc+WrqJdbz+9qiKoNNTV4ipgO
p3UmpKFol/sDaBbeOVDEz/6iPiht4W8mfTuZFAJXvCW3V9b/9bjUhYJDmPfOYBYMrooco2sIjbz4
KWDT0NvJwXKX0CjLh9IUhlLdOeZ6WgUkG9+VnKfuGaqr1UKULDrTRlyXMJbcYJy1faFi5asqgbw2
1+fp6QsXaTckQdw53SjTIje1iVw9FHfC/O8mCQuVQbML68POk6k3D4hWNXGcKnn0NfZtI9TN0fX/
+lY32UdTcYC6T+xNJWJdQpQrCCcyr2GGySZDBCbpj6bJJoMNn7XKLVm0O6KUBW68HIzMHBDfVoB8
ooenDhd5VyN0wbKZVYBqDGRfDeQharc54G+lr1C+pynbf6ZjZSC5M/0C6vB+zJGjUNtuCdsTcqGL
CY2PVgodKQryvRED4YMaVZXMEOKf2l9EaOVaXW+PxuGDBqEl9cmJ+x8EWQX2ka1CbUbyuGek+2a2
RalmsyGtymmW0p/wgKpP7xG5/J/pMjOIEh/B8jjHI9ixP3LpcVdLQP0ZIB+UUU1KueZ1c47ZCA+2
tk23XqTUwelSCQuav3IRYFzGtHjZSk+Y68P7TSOX7ssUyV9mnYr79bU7y0fE29TlOAFuseglCPbO
HkEDQbDEA9hL1NmvX+g/VsDeR5ZzEQEf0fkmJd+2lWOt4VA0imKDnaC0huoPj3ZlL97NVkWCMAWE
bCf28uuIQk9v2V6Jj1EIp8NszBU0505G13O2ysKp6NHKguH8PC7CDT9kQqKmTp7fQDpBb6W2Jx+U
tJA1PUcqMWY08xXTaqJygcxPzmlDepWGtN1AQ4+4nZZ1p+ebt4qsUX4UmUGYz7wks1ffGXRFbvVR
QRoQ0IndcnzlPm+OQgoTE+CFBnajpgbahrTOOWETavTEfCKO4uKjM5URveU7s1NtWTss+Gwso3ln
TO+wkHj/vwpnAF6UG9hXM1YP/zAW+d73f8I5zHnp/PSE1VzyoJ8V8nuas3sfwFvoxajtdjWlwgVs
pJElCOt20Tajp46Bb7JSev/wmJrs1kEq2cy7cErzN+wRRpv439p5xpSLmRX2CSM/cIqnRyQRyXMV
GwBC3uwWJFMSyEhkNAos/rAeeI4j7W/PRIKZx79UogEXi98SSMWkPkpX6V/xSU6J0SFMHHYvnkQG
7AdnPFQtD13CQ1Mq0zZqSZnOSQ8jYa3yN6WibwkXohbZU7m4bZParnEswurO7ilmdCrXJKSsaKM8
H+Av0yk9x/Cezh7u4TzPngKLxB/edafsd0P+tJcvEnP1fC8HbSjeRPjviYnALJzwV+eJpLnlngs2
1rtc7FIVC2cCcknBZ/+pDFvRZc1wEmBLNWg22qef+am+uNBd/107tOreg/su/k+deOgqjV3n0qEk
UC78+AE4BUPZGik4vSCuQDZfdv/Z9Ej6z9rteLsgj9tOntihlJUKw+eskoi6NebFykJMDOxIvfE0
WucVNvBky7jg+dahwPqs30mjaZ8cJtghk8TPGigyPA8IzB1SThbioYZcyIXVViGvhHGIbtX5TQxk
3QNu7oEwuEQWEyVxk28FPDBvNhnlR12LcBtqylGMUxBMP7Oqd0T4z9wHe0RWPsKVcOJrrMQlO/KF
et7VXw/yCveES9pr1ijrtpquXQXSKw0HPBpHcM9LHkMGiqGCSV4E2I3dpLRNaeowWJ4+992U/P3P
z6htHlWSUNJoJXchIaBdLo2/NH9GNi92wo6qKN9SoiJtrDGZx5iGRq60fra+dniCFtrTgQ3XBDH5
QzB8H8XUQMgfmWUQaWDJlh/v3I8smGZqaJW/puXuaHHYwCJcQh4dST2Y0aKKxh6/twD9AXROpMy5
DHQtFYY7j9o6ZaH9kvGKMS4MEbg+6Fl1NR2HeYs8EnT8WI0yUO4N0B+zOOx6oNMsuL8z9Q66GMkP
w3G604SrF+A/LX0BbP9tp27mLVtDWP2FyvfhO5QXzpCaue1ZBxP9uSTg6YsWE33ZqKIx1mavShB9
f+eHPBUD/9LwKwXmJ3zQ3w8lnAT+6TsuY5DIPi06mVnPZEhHnDlemf/hoSxfHgEBFg01sAdaQ5e+
eREJKIh4FnCber8ZaJE4gkCZ8VJbqZ43ad080OXhXuQa+1bXioWd+g//P+VHLAMcb3E4mm4uGiT8
ALfjg8TXoZTpiYGUWlv5GEbFe7fGAS3fCTl/Gw+yjzfRfpET9BAiF+L6ZVUe2ngETn63VJbz+qIH
njqzNPEoynwwet7bfAjvMq/n8Cgmgvqb11sj67qleZX+hzIbcFQG+054whFWuPqeRI9YOWYbQHwD
L/paJ2LdHUEZqWswBLJ2Dujl1+gGQ9KYyzjbEiM3UULGYeGJXU7TtdkTQYaFewgyIe0W8S9nXLy8
TKHULXiH251MxX2dCN67natZrWtxAeyxCLHnDIPf/1c2a3a5KHAceiHsFV9OQaZGMwzve0r606xA
mS5CDVI6NTHFsbhDilDqn7IRTqxudjwDAURvUZl0slOL0pga3Hpx/7PhkPdp0sD8L0WHCT8vCLQ5
W+T+raEA7Di0+fEhsN0n09CQmskJJazgkG5Mr6KvbeMQ0Wp2y/XoMUPV9qvzTFrhKy5xbpo0ibEH
rrYwvP/NAQZrXCR+m6+dnX84FwaBlI6zC81ei5XtfemPe04bYWsrq3nGydFARfij04KlXHNMcoTd
WGAvVeOi1uzmi1MP4q3Ys30gmy5e8HHJvx6Na9E2btYGIJ5HmYDGExELAQUiHXd2X7PbaJ9j7uoD
boKRUfF+IXWP00LWJhV4BkLtSwLBUsyoKfSpV1xHQKvq6bbOEqCePFyUOboRKcxvotLAk8eE652H
musM1NS5viEem3uEnfNIU04cn2GCy0LalAiJTP/tTSkzLd1ta9secI0S9IzVTwQkwL2l9YOmok9f
WwaGOHCmiFjJYNmy95RXRXe+WyK2YecjyPh5sd5588E+U/+O60qbAHSX1E2N841E3cJ3XBwn8jUs
x2CsAw8UaHdEd/GZcLJh6OcXZp9HE2hqaABawQqm0PEqVdRmQnRY5MuAEWwvoRQ4efDOc+MOj04Z
7jJxdt+Ja66iCFqugYISUHh2dvKd2739aSdq/K+nZe+wJoq52y8+KKCFu73tJsQ0eKQpS+xcxptj
/QQ6hst0mmIH8pjZHs1hJjTFYs9DZmRM22MZ0r9zEZAzN7FSSzyPwryXhph+yDWLmRD6rTLCGlJn
WckEZ5tK54wG1GJfxzBP3MGsljONIS+bz5VJNcZg3wXDkApr6X+8WlKMVFwcOVlVJa8Fz5YY/WdV
2iqUNRLc76TKJqqbc0cgROAtYi/1SB9NZ88+ObP/OBGpiWpfy8Fol3J45YdlyoKI+5iEqWjHUnQd
XBdhDoHhXctI/mx6iR6Wpe/1WlK1gVQmIyvVOkVZkh4gvC5hWFbu7rrHF/xCzw421s/pRjSbX9BN
tjrt0okbJKjbx6daFuIS/bKM/oFz2S+nQgbzUIig8KGkDTIBvMtVM/fFbHPfOet3NFqnXsbZvUgC
+Pto7vlUh11sBcqs8d2HSR4uPtDhiNvFwVmehqBDvNmqI13HUNvqVHg6TMMd69CTYvz+QSS72EKA
n2TifTNhG6SQjJuZ/lyevTrTvOBM5T2h5i4v49O9VmPvNluAd3Yqi3qVMs3TlFWqf0tfwNcgpyg0
VFmVkfj1mErREro8ttNuEuRgVN6dnAkOzYxI9XXGPDtJ6fVRU2F9TsDCKKlIqYC1Vc56NBH71giM
asMK+FcI/8hNQxegHqjQDALcilNyU8L8/m45mz/wuu9LN0DnXeqnlS92XA9yAd7eKaYtTvAHOi99
3bGMy9O99rKLdzL/zxTqAb1RgByVqE40kCHtr8cKjLlRGH4ErUHfH/+E3SzTQgN1BxVmgFw+EQ93
iwSoKRiU+TL4LytO5dCkJQrwoR1lW6cK33SUcBrzzJN4MxDqEmoxW+dPxwSGf8qVhQl+qq2pxdRz
ddxLPqnSyIDkBOm6L7yoazy45s0/4Qf4Q3KRTa02jjyGytKGNjWOz8BMIRcfd2JWNptb9hZ7EZzi
IS3FOGnN/AeO4i0VVRhldwz/OhkFmn35biXCWkaqNKPADkV01qydXf4Qi6EsuyPP/dCJ8GDfxmIs
dUtGlOuhTfNlnaC1WlWzpcTjRngVRX0DDKORm3krPT/uCwYthDjBdySZLZNPDcGd5UxuiBSJJQLq
t9D3UmYlvviS0dDMTlp+moehJiUQ/cdTJU2HZKs58Jt2EkP43cZaJ4GOqYPMyB3eTCMiZp5fceww
rXV85kxVG0Q+G0spjkaYUDTMqkfy3op51plFvrGY6mFY0w3Wh4Ok6aA0ic0DzLVVXZXS6+q4qzQA
CPry5HVqAAh9JA5RMUJvK0f6GeU4LzKNXteEm1wd/BQolTGzVSjPPcOuKdX8f+Ks9tAxwJneWiav
wEGbs4tCX8gNbNbOGFrQjRDc6JZt1YXAGUYsSl/mW70cYyJJSmgeMr4bgjxY7qnu73qTBm5AyZrV
dsxx9sFDbGwOMQSAzXMrMhZb8a8uJgdvPd9jgbvz/9UWVJGzluIgNwJfWG+Osv4aCIwXcQtWiDWg
ZQh0Rdhw/+4LBVoKIKALWzekFqlrjyHanDtTjJHXOuroPVsUuSuYkdGzytH3fjs83xSTyphNuA/6
enUFg6VzJ6kGunGgzttCj7tISG3JOoaPx5+/hCkE7epX4OdXy1D5cpDnItSTjIEltqMsisrX0E4e
Rfgc0BrWzn3QaunZn7hI+w0l97p0ZbYE/w3yAVdsAUMlozYxJKSleuz/7l0voM+JZdQN1pZ72AVt
pmsZNduNhlVYDyHCD+VpCed+JQb3W5wt/lhiAY037xDPxkJLBRGhJ7yUCbqCj3v2HsDV9byy5QAH
dMmSjytONII5p5wLJzW6lWUHvGdJ1LG8//3NHK/H1UeWmtOERY43syxbOgLdEuGrnwOAZ+Q5pTIo
WcwECVn1IJCo/Hf2hJ+zWVGGI3NFUNbl7ofwiA//yQi0cIV4tZhc+4HU3mEJrZtOt6eNi9BADrco
mx4BB3KTg1cVRPqvq5ZmEJQEWtVh2JSsKICTKb59/NgzD/JBCwgOgArbjM0tz9iPtcN3Bv0SwP2R
21aGUR5p4xg4PG/Mi9YKekjfhlnAMck9RFJ8JhMNLHr1llVG/FqrTMYVE5HMPbBlQow2QDm8KGtq
Rh6I4KZzWy8SazKmilu+v3zUN3mRxti7Yg5snxGD7HARr9hsn5ATIu12yWxC0DiBu5pDdetw5ujn
RXXQVtwLjibovFsZmB0FpUhhFSP+ewVAEVXDbsDiiyym4sSO9W+8q+LI2+ZyMNJYnXcEVCENvpAS
v7Q3HNg71JBrJXEo6nkmYI8J19E0psZe4mMxt54O2hnw3O5nDeWsYiSQ4la1N+HtgJezPbP9jdrm
oCMSqExVWy+LqCHzOnSh05Pni+xoYf3NcVtfUcv4ZZaic94RAcl/karlJlTug9qooXbu/L581CWj
DdNf8cdYbZkItYpXvgI0YZCEUMVf50ccXlg2EbiiCRVytU3fTQbFrm03/dnsFTUiesPjc89hPS0f
7fPZgYzOyqMYgeP0kEk5r60EKMYj28fWDV9BU0ug7NLBWDifCmePKU9lfL72YSnQwk7+nJVHEyPz
ROQM1sgBp5ZLVahdkycrH/R5EZsqidYFQPGYftMqtzFpVQhl1IsXH3StG521VSFVvyE+l2r40ROG
AxjmGqA+VfKBf9YGYkxiepM5yxcNWNZ5RWx6Rtybr4FHEA5QUMxh4yj8Pvfy/OQRmzFQQ7CrltVE
H6T4IIBTzWKP3fO+vPmSi4ZWa/rKSJG5jEMg5U863GchL4ydrkMaxzJ82OITZ0r/gr1W9kjxV7RB
loHGDwOjkyof5M03nvf4kirnSJJVn6vP3y3LNIIcjq7EYE9KE0tzyFHREZY/tv8PgyZVzL7TcIKb
0kHoEKiomAaKUJMwq+v6KqfJRPF/eZ3VM74ps+2yk36KtfLvduQy74CCyhtgacAlnM6oqkjwwxpa
YDJG74HNVjagCK2hjlWcsW2nKx+XD4NTben2+2W/cG4TLeA2032KCC5pSJQRCZcv2ZKvzJySAHV9
t6AA6HufVE099D6J2QUhwhoWpxGxCt5ywsNtggb+21iBizpSKPfBoxq6CGAeCc23rr0UaFlrlB4h
FudM6tUOteh3Xh4kHKx1CNFAW6btsrC39V/LfBM1pozyJLKjiw80PvDZ/PimhIkbDeplT/dZmwd9
rYdForJrJVCfEoNE30h0lecBvQkEgjUMSytS+b4XYsSEHu6gIJaL+wED7HW6DGl9RRKkxYck0Ylo
F68fy3f/AmROUYOG+9z3OMyfn3SWk2VN611yRmMVKa9pabyfjP+PqtR5FtSb8uGqK412TkBTVNek
yQepavXooBKdH6iDRkymN/eUsG1V0zEEC/AiS695XW5VFL5QJA8h6gcaCToy1mvMET4Xn7jY7XS8
GL9iRsvVGxJrTKi3Txeisejvo2kz0UpXiWRyHNlVbcWpX6Slk7x55+r0wlsfl9/obeLlS4JKxbsb
Xbqbd/fW/RIkIR8NNzJEOROvZDKZoznHDSJOSdaUUhi0swfvk7jpXgNdDgyQ6mIAhxnbGvsXWVPv
mUo/MW+1Ddo+rfUDd6/01bk3J3aFfylZPIzzFsRfIY7nNe9kGZ/S5x/hUfVJZm783YokfGGzB385
UQXzopyhDjWLthvW34VzErcgXFL7PXA3Wg3L8Ccvu6jYu+Gk5liKn0vesVhwODJ6kYewRxteUVDa
9dPmdn4+JJVPgPZkl1Uoo5kvyNnYoYMhDKA8/ml3VocmphVYpT/CNZoeodIpZWDlZrJkI4+9PWs1
dhCDsxQkIXQGnaxI1FNpE96q79CFOLVpj7rmI/aMbK37kMmFZk5nflOnUUsBqX5w2Pj9r4DUTy3a
EyLM9WlYosymPS0knMKEMu6u3DaAP/m8/Yb7KfkFTvkB9GeOiv8dO3udqkh+k+zGrwcXuGJ7lac3
Euff32TkXcUnl66Yw9M8g0n6+bdAvcEDXBMB90GvXRBo17wUFNr/ZTI3Bg27Niv0MHf6M6M0bTgh
2pFb111yqTyHvgAjXBu8WdI8z7zQ4Q9/I79h+OlY4Fnp1H8seBLVEon9iT2tJdKolVyynV8vra5x
3k+y4AVtFbwm1Zf2xBKaIxweQTT2uD/YLiA7MRTwLkNko9vxo1XjbQQ56XInnGHe6P1/OLRMN4bR
zkMqY2X5Oe1zkeFw7V5F1ll5H60dUnO/0uBPaAj1nWben0FZj8g3DEYnVpJ6C3vOryTR8HJZgDvI
LfiTCqMONvOBs/N6SLISiDo+r6q32716mbgyjgPJTF20YuLxjhISJkLNSKMKAtoAPtLzvlHmyZm+
0ZphgiMPUYFsDaLCwfc3pOr6qFNKY4hY+MFzuMQYlRl/jUxhKABRvvj093Nz7uXwfsPuZ+/qaLnp
Rlj6Ra8BE29zWdbhO+ihopJEszwWF8k5MuLLzYnQU+MfLOYLak1WlXcxC1X9X3cnoJg01mAIYjrp
NcKJTg9KSeGvH9ddLgngB3slBXbMetbIovITKYANGSvgDeWXAY6B9Rj7vDeEQXPSSl0fIvvVqKw0
CRhoWE5Hi8In5DBJzJYPrbyyvgZpqmg+2qz9sf17fKPiHPky2dVihk+caDjCE1xzT5GOIKHIGtTP
dTZZVl+R/EXJwJSCVAwJVtJRZgeJwFAXKZ5oH3uihkIWHjscrvrbKvYgkeEQr9SMcL2ar85smeXK
bUHcHZwurfq/+cLqivny4bRTS5aSkV9Iff+l0jxJWeIfG4OPbuibnISsJUpRIYeHMga91cvSpW0v
DmC9iQ2fYs+BTz1YArHph0CEVUE1hGnXM6fy6S+BZIF7Ge92iejvIXrubcwQxagm/Y/93tduiWjz
j7lStPNiYmNw8FLOM3xd6+JvgP8cfXWyKM6PZDb0XA5jzDQs+TX8+zxHF5iOynEcvrV5o2kJ18Qq
TqshcGfZjAxjYlPeNnfB2Y8FAqw9a969PdC9xV1mPox1047uH7OJ+Wvezv94uBGac+dNNcatFX5E
mMtOGDTZi8LaMm2rtU82x1aIcEBHaxDUkPYmZiZtDYw1y0wQ0IFtREAGaFayfe1PWa7tskcupVkf
Dzba9Zwo8viO9OFJMpTGslkovExwDn24x1U1WQ6KMywO/PuqhNkJo4VDQLzYzT2pwrJ7HdUFeYMf
FXAwZbzzG/jajKQ13F+biT6wAtzvzk8caMIWkwS/f7aNRSTPWuMShQ8wUyzLnK74h/jks+04wmGv
85qZ1t6MAL8XZ8v7up6j7AG3iGYsKTIiFEtzA+KvgdfI/9L959sDg0ssvU9AmkcCzjNYhDB0/mJt
dN4dWfdbp2kBrLXFOp4CdeMNu9M6Tlt8HQ2n2oKD/L93nEioXB+HSzOU/6oUaWiqJ0OpvzUnnzJP
Ck8qoQOUpWVgae5XFFMJ8BKCdy2yHceZLu46T1KRQk5fdYPGOGjZeiupx3k/kQMp/IgRZvtuFjJU
qwxnjvCHxfsXn8qiV8tPdZRUQj2ffIN1k31+8lmojLpf9rFl+NwHlIxM9bUEcr+g+JSVd7nWNRyt
z4WnWGy4jea8DWjF1bjXkDQ5qQthcrcKba1Tk/MSR4u+gHMUha+SVKkdVHqUTxRmm2+zw5gFyZbx
mO79o5YF1rSaoxLVeCD+Zu8xsRLANsnTH0BopVGzLrMruvtJ50A0xl6r5SUe1MB4TFMfeTVuIETA
v74Ktlqpz08cos+SI4gOw0j1bYtDVs2YzuxM6nYhBLjA85+yOBVQSVTMyCI8WkRqdSzmV2mEa9be
CMUAldYEAfQkmuzvvu4kefFnDlSdD50g+tB87GYdOTMkBvU28j1EG5je4HGUJrb4P8L2PFKRyUMM
jQpVjqtrUnDWQV22Ry7AI6IKtNRoISKWyHNBryHCvP05t1+jPJwHLklcYU8WgpiqDzmoM9Q8jTHP
hKb64eOVJaWz7DSFCyDLfoBkTJ6GTVAL75Pm4xqzDdBDraUpb7lZ3araUGC1YpvB+FASY5AFqaDl
h0fZGwhjysjQYu83YO2qUOtdseU21LEvjR4s+xsRXMIor6IZRWZIxcC51EQaY4gaUJgF+aZhVhQ/
zNwVKLTQVkHti6edMSDwmAfaynhD8/NGCMgqY770ksTVtlRfFCYFEfwMCFEDrXK26Xzvz1+HaFrD
RP17EQOQbsJJqQt5qctE1raLbPZMACEEYHO0w9Ao45UbNu14b0eNHJj41nKxaBGVqgyNiuFnfqio
tiLo5lhbqDer+i4YNeh9nLMCtDoStPZ6I2qznZcJrzj/9apVt4In0ExpHeWYkJyuF0FzE0Dh6nqc
UfurwA93FzCm+btlkxcDfobcy8idmgcUdWE8sqhnqqouZybzEvTJeZDtxvYH8bKcVF/u8aFfmGuw
S5xQnmoihd1oqVz9esV+3NjpBKoXixs1kXligaob0TGStLfk238DskSwlfYIwenWtfMBeG7ZkYee
fRGEyM2zjTiKbXMzD0ATpKHrFd7ixCozb+Wd9XAcCK8sgSS3Kxb1UiFnotgwhbLh0GHL6teFnxqJ
mZVoABuymfFfdQQroHayqpHOb2DAozMFUXamYBM9qCGmlXjckJfZBBRQs+tLkWYqkIuG80MYwAqL
y84ZrY4xQwv9xeYWgzDGbjMcIQtPmK84sQUQkcTmFCM4p3EIYwQPJspYD+70UAKwtaXQJVhyhPYk
a+owrhC5M9xMybM1p/9m0b2rE/0ntgODfifq6DHZWYok1MdjP6/YQj59oqxlA0k7zn0BCdpEzGxI
+Ex8GCXC6ExmTl1kLs3GtCY6As6kD7f3WsK4Az5JeLUiy6qEdDrV5/PBMt+iM+zYEphiVSO84kY1
Wac1/GA+Z2FccGcDezAJSZ5uzdnNLtH2YffSIeHM+lxU6Bd70OReG2TV+LKy/kvkb33c/yVgIXLc
i07hcHuvWeYSLvRgWpIAPae92A++8t6WUs5/AB5YM4vT6zGMC7/x2NQB0UbwCVkGMUCo3FAbui5F
uq6MBb7dPO5LQVDFsBjPLrrGUY/Z9RQRCM4E52zT0jZXK8+VJ0zA7Z+2ulC1lDpa/e0p5P/chXZu
s0EzOjMWhQ3XhY9gKV3+z6Bu9XhSwSz56+HvHyk5uZYdVQM4ArieOmBS4ylJU5wgMpDKeGS6HBdw
CtR6EqNlKjmJWG+X3CglpeOo0c5F8vf+B5kquRx5bCSjd9CeB3jC7Vvi7iZV+mWPVJ3aJXsYaRbj
tEqC+sbyV1Ph4yL0XbyNgpWd/BQWAvDDxDsdBKuLFUL55Grvl/VHnPaFs0Rnpo0ZiPENW3lp7bJ2
LZOb+1w5ckUjoiJKlbx8lBR7oUQedpzLYY6OechtB5T3z5+wCOXjPPQO7i/57n/YDOZhLH5kBYDe
s+8INnEneDL0PnEuazmaG6G+jxx92ygAtWg6ZLVgYl/Za5ZJRSxiOO32VkZCGuLAAFFAACy6Lamw
XWkbX7UDV7RcTOqT6hNhQMa8TfA7b3PvedBHM1HlN0sYM/i1YTAmuh6SN58uyHhOJewsixH3TjIA
XrxchyDr5s1v/CkoGANXVHt13R2mM6dWqDqjBuTmBa/RzABM5ZU+ZB8mjtJCwlBN6/Cmd7z1NDXR
2BTn5jwC32VYAs8ocJcccNPPxjcP5WsVcfMm9pLap9PdcLr8SU+jNjZBISe8XjjlRBwfoQUH0Ciy
cu/wPyQNrroad7buwfPG2e1E/8COQzsuluOMHFwALeadBC+BNWLTDr1cgm78PbuWZLsIJPtyEqIA
rSUGMCqatT8f4vMu7DSLNmsDs5upRbRPgHV5SrlRlpvWHPl3Z6dgMSZteWhQTFQtLV7T6vH0jhpJ
vnFb9GG5S3Q8om4eHv8lbYPilhp+LM5SBIQUZ7ifOyB1mojO3GBq7+JHDVvSHa5WZrjTUtZjM5r+
39s/d41S+ug8WacS1wmh+ezWvIjSAj3RABmJ1dONs/gYYl4CSdAZx2LEwIxdNL/nYmPdIfKlBExT
clZFSNrE61RA9FvhV13PQ1Bjz5lsmLbFCOjc/h9OijAyQXmhTpAMP1Xm6jyNh61dYi4Am4HE/VeX
fGBi7LtGY88gW5Ps1THe9MPCiqYNiRWvV78m3VrYNwXMISOlt54bFTUOzUqggISVq3GYASHNSGkk
ZDE6EKJnMg3QeF0Ph026hmBepk1V3+HzKELaqCmVTkS0W1hHEzhRFBxHvbqdpJrKDjeFnvw+Z+M8
NVpoXTCpC9kvZIUV7fjOKT3i/7UxuNMrD+TcSZYONgDq3pUyUdes5jXaGL3piMEdvjWHIQLx/w5M
GkfINlLCuM880mgL7b9JMY9TYXcxa8hDl3g6hKb5AIrDA1kFMUshpOvUHJqBwQl5+ngpUET7rDqm
OHT3o9PrExLIyzuX8aZQpQx5RBVv1kgfxVZv2LjXzQTBw1e5Xndhev1429kZSk4wLI4D6XRciY0N
zAh8CfiBbbgrsj9wCAvDcCbusSc6sTSUeHzCoOMSVb1qlN57xSELDGAyRycWVSXmgE9tCKxOa/Tw
ODtayFVYTWoPS8jDddu+zrCIQzsylTFA3UyHPUopZF8HMAYHl84/o25kjLsYbcicPGk1mctKQJ/+
0RGLhuS+CtDzx9JaQYZW5S3HGLOZNTYJpFQgnEnZGGi3VcVEbI3zFXYajDALHNtiX3fCvDt+mEAU
lH5v8N8qxyN0Gq2caOreWp0gmPCJmMSrc0hY3hp3yObAOnFTw+2pRMhsUdxT3JD/nAXXR/gq9yfw
aGgkqqBrMKBBi2x1l3C/Vk7/fA0JDrbEqO9q8te/GdEHoakLXOIQOdDABuOCP5ECPHmh+rH3vP23
Avg0QECXHOp4c/hkTpBWwmbO6WfMG5wv6OX6w1LaO6ceQSV9+BYR5oNu5V9+aNP/QYNdX+DDywns
n4hsSveYHhrYVNS1L9mYGZy1QXCy+OPJZMFBga/kPjxDCOM2YzpTDJKLCpfZXbjP7Y2j9nkxzX+R
VuM5UBIOiFysuGhlF555fNMz6NzAd9O/g4nvhCvSevz+qLDy5UNdAAq5ceB1CNjZ5fC1wQ6upSIm
VbnYmcPgpX8WayvSkKl6HhEd03CBmcZV/ehl7NzBPJ57VmexcTGLlptNWM9HS0ANPcxkh6UHtHQK
c/OsCa+JvMwDeIHCzfm3xeGj+4JfG7KM/6NqUXUx1nL65i+mI95gFibSS2FboaQHq8p9ux/Uhsei
4rWqTufbHkhjCeqytxdWXQFvt4xIKIGhCKinBHVaXre6FVlqKSTdXYbuR3I0WJWTqGRVe21ARCQP
6gT84jJXo4uiG6BO+jgE32SOPQulxd0RM57pznGbyho8BbPQrxf3eFTHFCO610+XeWDufCrLhkFX
l/apuHIKUjAI8Y3jK6Lq1mA0WD8Xk6FfN2vViZjTziRjTbVsC1D01kYqq8BPeHYBtJZGbTFxsa/c
2CK/AOgAqAEeRqo15B1/cCpbuikXJonYZf6bY2JTyCi392UllbRUM2GPMqge7ZBO70jrAAF5zV+Y
EEPkyDMIOod08xZfK6BdgBlkU1JDy9uO3r12dg9LznnJ/HUFm/boBBe6wmkxgDGimCeGniVa6Sst
XUB+/a4cYLSDc0m2FwmAgLB7BVcfuQR2s3HeKUvVTyPDZ+HIB7cwGgYH7wc3ld/ENbsjVknu/5zN
lRDOHsHEttJViDd29lgwQv4zv1qnGAkb2FFuoh6dEkj9gNDG9XUoNPWKtLSU7KDlz4vh8CuRCeUP
ffav71IPfd7Oaj6dDbD53G8anM+F9ynEGTaaCF2UmOJ8KB3AIFL2XZKUKMTBlT0OHg6UrqwBckkN
aljsFD8z1KVwlrFkMRjk434BjrsjwfYEyiT1Lr/ttY/82aseu+pPQ3mwleoot8NqrnJkcqU/c3ve
WD6Azwa4VAfeFyF0CrrlFwX5FxZ4cA6IPqaVNc2aH/h0vJI3zgLLfYA9EMdX3AR6vYuU3KxmqiDo
EVPxpGSAjErOhw0ajJNpaKv0cwv1NDLzs48ChGsvS047CDWUrbOZzObhwMJNLrQ+CHoMYJ90yfZ+
jfa0H/a1S6m21VG6pa00Vw70aN9mS6zD2UmUEOLFbwsIbrmA69gPyKHhEG5cOJDWUxI7lpRju9MA
UrXEbXSV2nDQbDNyUaOght1l7zZ1ldVH25GIyTBUsGuGFDc7m5DieENH9wjPuIVnGXhbH0jBve8L
+qIqzoN1He7DEzM7wme9MzD/KJmlEhj7y1bs34GVXxt7oO88IYgpMS6/6qfujVtZnaf5ZXkR6o2B
chD/d+W2OJSivg27PhzEnQp4z/HrC2mBJyQXGYXi5PRzUeP7SEBPDrFutq7QQLxXMsFHt+TKXR7X
Xs0ymSI/t5J2P8xJal7bhcz3uV5VmqZx6AJlkUEpw7Cy3aeX2axLs51Bk2732YtqDID4QzY659ov
YP4qtLtYjKSk+AriMYYww2VikNpViSUyqGXmgdBlwSB+6KoSvkWvFGcZJ2Kp0HZkoHO1KS9BRe9o
UfhQSozrntIH2CSUd/aEb5eS97fNTT5LyVSyIrQbocxcoezgQgP7lnNUgnvvxbKYEkNBDVRyZc1k
PtxnJ4lk4FTgKtn+8k/UsLa6OjLa9y5BPttzY1gYa/13iQc4JuIqW26xMwHNaIMPCIBo6Sh9ziOk
zJC7xUhPOsDZ463Fu7F2k09ceubO+uUqDIP1hMH3N2VW1cxiQO+dk5tF0OjP0nSNkaBFxWCN8a3A
m3fWa8HZc2Op6YburXj229fl6FuhkV+rirGFAvawdD/P0Ub8SFesEGo1pu9nsQQnylF6wGD9/FuC
+hctuWTxlT5TJckh1pyitzTurWLALSOpEdSDzTjSsE18zWfKx6vgx6t1E4AQFqGLLRz4ert3qFfl
1flu1q4U3fDBEi7z7gtCieAFt5Bjhn6WNbU5ZQCvqmUny1kVBKG6/0lS6dNsBqYn4END01CLg0Hw
Hb3g5Wge/bZWa5mU3jPzCnOSShRx5vWNIXRPy9nZUy0OKWQwN8RmW2yB5ZdwhrzeRpP/XYtS8vYP
vjwjasGp/xb7ynXO6X18JAKWBcPESf/j/H7oX0U+Neq+iWA7Dp5vIgPURrwL5178BjrfZBPkDCwS
GvEBZirGi80h1nW+ZNUfVn4ab9AgFVrYcKGwQOJe2+lQnxfLOV5I1ufric63b4zeexSM45S3Ho0Y
s58MlaX32Mcy7WZovbnWNF8rZCBx0bmKQDQBV19ccxPX/yrKyw40FySyeqMd4qiLXdC53Rnqz3UT
ybfqSIvL5zQMdgR/fFCLNw12u6W+6htWmzs1ZKJn6DQOgcKQPXG+OKDz/m/UWLjp1s+alBpUg27e
tP4PX4cB7Ako3ygYEJGFrnAM+dx9/V9R0zRXYGxjkRFS9uLJnrUJif/LyP796nFb6xXY+PUVGYmw
wgL6TEvGiLsRwspc9fH1XBYpeq30lY1N2GG0WbH05JuQwRH2Q3YMlXXkaqLu/LRSMv618nnUqK+j
vMm0YsJRRV0LTVmmqVk+nf7d5IDrlyyhx4EYp9vtVd2UpIZXwsh6/rjmjww7aTj8j3xEBIJlDPln
AoECplur899hQv/3tt3NFCHUvoGV7KtRQ6FsFlRTc3eXzDRT1yFyJe5beboS9C0Za/TqiBJ6+qYU
Jf/HXCJtEOSuEs23S9k8PRmpvdUkrKMQTp30inmPcB1Ko1UP7Iwof1TdoUrURqZNgQbbFVS7bKDq
uGgcvaadWP7tY0WN4DrF6VYb3wwBsIMJegeYoBb4c9J/tySB7GRIa5mVQH3xVRbI9vyfSsk6V1R9
KtlQudXNMIm9KCgvjQMy4f8jhT69lBIROglWMd7L8bRTlCP6J8jC/c9EzqJumzzREfLmM8CfTOAf
E2V6MHC/OLAbzT5xsLhngd2MWONkgL/fkf4aVZm2kbMOb2a/Z6nfhj2cudWdKgfbtwaCkOkBxy5k
DYw27ym23QMlxc+QAyz6JFNvtrqGoULJ9d9NKGPJQjR1D2KDH52qJsAjaTH8vf9REbK4VZfTg9Id
jyCmPO6VddD3Of1oG2CxIm/+G7t4C/oYmzmxHPERsDiA5sgqPvKed/i0ls0LKRbVUAlITfbgF/Tj
xIaQX1ryG1vEtpcyjtw0xCscNIqFnf233Lzin/cSnJUy/1dhjVrvaGwhTqR3HLw6T0Ga3A+3iuLz
HypkNHX5A2imXuGldsR2kxYlBu0AVkZ+f2Nu06iCKhlyfvSLPatvCnVUSOtGAoPndfy4+YdYzuPq
wb8n4fEvcHTTJYbCJzbH8vyowVmrtsL0gAm+zsWTcVQQ8Nlc0EfsnoM2llB7oxyrJzSFDJW7bRrr
CMl6lmF2G9O1LJIQw4rRe6jItAcq/K+VDYg8iJaCHYflXn+pXT22ing2vOGeA+4sANQB+1wXfcNX
jiVPvJGtNGVojpHIZctFwSbzWk2SgozSE0Hd3r4iLgX+j+RNDZi5+9zBweQI4obbM2r05kq1KkCJ
0u3KkO5nI61dq7l2Mu9ovZfhnd0RXczidwxiSlUjn84mOHWVH7Tapmk7XZaOb26l25q5Ksqi3BVs
IsKqmQUceXscDskkvPyC6TyHVcB7SI5O/J1rEwiQX6iHHb6R8Uu+1RkM3HXJV9JtDpqfh2LFWs+B
aymgSY7JF3F6o1c65oLjiIGn9JPx7/Owlcd20iNPPOhYfgaMpC7k1oeX+XVWhqGhn7W8VbA/YBMO
5ez4twJhXFYOHMm72awY5rbIZw25n4dtOAmBreD6SISgs9orvOnLtzIGIBM9ejiBnlLR8wbfNy9G
Bn2mdow08U9+ozgpplJrJO/o1zM9i0SkW1R4/3pVRG9u1pOEODeKO3OmU2rsC7mfSGzHsfRQiVHI
IV2M/EK8J3viWnhl6r/Ydcyp5QL/RHqPMRcJZELI6/oIprLKEwZ7EVOxBLE0QTvuEmfyhzYIUjHb
ggtB0BkIdJwrHVb+U0OdqlUHTgP4k2DtsX25Gy/3oCZ+HFxSg9ijRou3bHR97J/srA8pm5E/2Hhy
e5i2d84hy1WE6uZODjEX7voiIXDlRu/Bf6ahrkGHyTqzAXM4eZ65AC+Pr99WLGNhC/+MGdLh4ir7
sawsIpnq4MPEMdK2ujCJ7bVrrCN3v4j/tfsXaa7a3hGNq8UPQRHbD6XRX81q8uv3bKyX7+X/dT4i
6V6/qabnmsahQn9/Qbex1NHW4peOXiFiOhl3bu8Q3tuG2BOIQQzOyFaE5+GMNIpztLfVa+nzJaCV
ExvKY1Kac/HOQyQ6CET1Eie0rWPJroL7AOL2AXPo7trxwGhHqvglhEiYR1LUTTXUBgQ6GmrfwgoU
MU/a8+9IcTTXmzNYLcSRk2L0k/SXKTVxZevmEmzt15Dkz5kVW+F2CWN+bRk1O8XDnXO37QpJpmC9
5thVZXA3lcBLLDPJraJRab3U4Oon6n35+TottBhrnZgjxI8BPh0Xh5KuyHnH+JwP5cwF9Q5Nxams
tYIc1FlvSUkr/FGkI64DSwj6VKyQORklyuW/4qwutUpBBbWIkWElc3O3Uyo6KZsFV/gWR9c1UpxG
HYLTfxR8y51lodRxvIEO4i6LKNS44Z0e0p3tArRnxYZB25AvUpaJIdRo/ZGO2zCXlWy//Gh8Nl41
6ismbzZXV2uwvqzOp2yXFGnhXi4Gec5sTrRlzMN260qBqwb/XFADkWhCJJ4FB1wZZkkiP3AVuVlT
gVlwEWWgdFWn8dN2Vh09CV6sDkrri5xqBAb4gIRdel57UI/jIxwHjhJNywrMngdPOcdwdXrCKrOx
yHP/Qagkq6JAZgtQHHZ65rUSyYVjRhUeQ5C6PnDZ0zcn2KxJSJNwplmnhfP7hAjqG6hFyGpDEypD
8NA1IY9WqoWEoMSbi8otjb2mh+5rGWbICSf9nSGgpB6c8djOjtY+/07IfD85OKJnfsKxXNmuANXY
5vJEYOqU3lMIOES23Nebw3MC9WLK8vTELCdszQPd2aAkKT2UDEWn1lq70RHkkzYjnW1hDhSbvHth
8VzFnRzVEkJFW8AgCfGE1WqXebDhIYINiZ8+4XiZGK3O2ZijzaS/0InyTHiTEBT84Ngd7B8XVwuY
b82e9kS3Gkfq6eYhOLNSIPifWcyShZZpGMAh+t8VBLK9NPg7o+aYfaXH0sqQH6oBkcj61O+Wgu6b
nn3i0UfZu0qJ3JIavtTEDzCZXOrpWZe6YOxG2/UoTswv0eR4WDL8yp0n0P1MkX6ZuyrORZnfahp3
2b5NzkpwoJFbH9n4UMDaMtl2EKEgH5EvILDAZ9X3yLlkYA5QzaZDAtA6JGDBHrML1hfUsjjvc078
GxjXS95GcRySpYhdZFLOCi50Gm2zfpgI5OGOLp3Z+rV+nvQRXrEsu90T1zNnOmv1ZHGqwEzcyTs1
xCLP1gg9KfOjGyzoP8DDTFGsTyoa1DdnDnrGAJV9S+hU34eaVIXJfsNih5ehksCenh/XYHT5uhpd
0KgZnQUfkgpUpovqjnKsV6UshWKdNYXUIjlM1RuPzjvU+yKZj6+bK0QVH/xQ8Eo7BLo46E4YOMZr
kz7nlVpkMizLfbFQYh/u+YQ7p16NngrVrFU6Hq6ef+lufCYEAyxYi8+lV8jc66M28ZpBBUbMcCXx
4J04gPaAHR9ddct8y5DzdUfoW3h2qjHECyIMyX7gtqC0CBrEcFFvkiiX1SlwR5UNB3jOEBId+Nnl
hl/kxgQTtf3mMUiQcGLsvNFGBPgTjzNbaCBMsy6i3+w7a21Kc0x7qKE6EUoV14KcUxd35F/dnTaG
nEnSVMVTg0LPQab1yM/CqaqNX9/M3gGTeW3xtvO/oYUYtIjjkMKpsltpClPVtL5K335KLQzIEOl1
maSpq55naM/1mnAHP2yD+z9SO5SRoWpNLXkrB+PZVT+tYVczdAiZHn51hdnopbHP/sesTrTszgt4
NyNF6FILOnOKk6WTpgTzDl9GXt6kAGI1e9J9vjYadqY4yJp+Wddpse/xJxko00RPcXajgVMkZzcc
u/Wby4thD1TuaL/2MS0UtdVjTU4sn7RsOtdhFohdNYoZXA5LFBKR+sO3YIQrWjNhqLCvHBfGajvU
LZ5G7CA4gSUY0NANBo5tYVyQjIV+cENo1TYdypkxWLX0ciWHXCTVnJHrtD0qpmEwhCbZIuvGFysH
vGF0ZLp983RDpexhDieAOaZLaE3tsFnFztfpzPemq5sYtW/TEJ1N90Wb8X9HQcusefStwaADAQNa
pnhnwFo7D5dbnST5I5vYwwh+8Vml7SseTpVxKjGBIZJr2QfViaH3WNp4t/P5ZexvhKSpuE2V/OTd
58hHLd1RAnVf9ndRSVxz6yuCvOWNvGpkt6yND5xUPV3nzNfcmkonXCZ15S85yjQAbVm+xRyMH0AD
AxI+U4CcQAMpg/uQ5BdgXB7pFQfT7VbjnMKnoNOnKKCJtMajYOQznrHeb+pBqKiddKJUY07A65d3
NbROsTbaugWza2zsl4qEDgCaBVcNZOAdjxXYiBmyDVhf7jdAP9eoTL/ch1BTiCUrQx5EcLa+KQLd
5JwhgwF1cb/YXb0GwdtFfOYPIfkJjwDAjipLFrkhOzzSxHt/yvamNiQvdXtNbY/tU77GqvewoVnF
iRKJlmxtq6JrvOsZzapbQJvO09+9YO680yKKTgBSc+kyYA7DlbVY3XklhlhvzfEqin5CrV4NRDAb
P+522PuxBI9UwKW1eucgZabu0gZyRS7i6xV8fRSXII8OJKdauTOj2f9FMHUNuJsQuv57V9Ot/7c9
aJ9v1J0awwAQ35rB1Rz8HijbH16ocAwz4y/SKpSLYb4lQ1ed/Scm3mZxqLAUwE7baqL+Eg0cj4H4
16qfd5zQZIXyVnSESTMSnXs71TC1taiNEGvsa7Y/x2tDDXw9Z2XM3icSymYPEVbjv12KFtR75NZA
ft4ofC6gcJtoKtP6ypZh2dD2cL5zsJP1iGObVv3pVmWyc3JID2VE97nMuv46nnnlOHIqt5dR4+8c
gljbSYZDdNf4EpsW+vZ0/D8i54nTxkm7b5fsGMZn9Mi1fdXNdjY2UtHF4w4CkxMJ8QXF2eG94FnP
B5KK8V82RdnBzp52DbZWN+5775XFWN9E+qgC5rfjYxpnvc2NaGEYJOc68rktMMq7lvUHt48qFDBL
5lejbGMaT7IYdU0DPds4w88UUeprHLpdoWfowJyGHFqnNA4UXExsL8HgRmdmEsepQYlAKXHfhj9u
5VyMLiPHmBvsDGC2/tMHaYjCLPppd7kmx+wd9Q47bnz+KfiBvd6FERTplnvmYW8yrHCeQlvoGXXX
U1EuebFWS8pu4eDYshBAp5mt7m+RfUbbPu9dwB1IvKSvwsqVztbtOhWGIis3y0tZGp5qC0fmPmr/
EgWj6bBnQMEr4+I59E4XJIepCb5+fIKR7AE4yICxB54rZC0PysyWWQIgzeCa2d/5HMr9W1YeyIlw
chKp8JhJyXg7J8unvfFt+fvcFwTcx+uI/cZytkSxoqvNlRszjuY6Izhti1sntsZzYu0gAVoqW42E
vqGCSkIp5BBV4sKHBdw6fLOK/9XoCux1PiLplQOLWUBTcnbCx4pFeTgbrRERW3ZIwUs4Br46E0qL
dj55190vEwtQSvlqDqiulZWVMVL11vcl2cgvWSFwNN1JKPPdc8CQNoTi1oXsVfSukIpaEc/nPXIw
3EmIOBgwVbpzASXVLEFDd4pGtRhDHbj5Y7PMMoaIRjkb47vvsqrBdxSOPYC2g0kVXosZigCYfCHm
u4jmXbL83w/l3N/5EMwUKBjqAOGHYm+MeRLbIvqC5OhoGefkEttMHxHi7JbK7oH4MIF9YAqTa5Iv
BNdnn3JmMj69BPo4aXZtPQZIhlUQjOLpvvED/qwstQW9QjEnQgCXgJXi+khDBtQ/RR68s/OUfMjQ
tQSjNoHuJhnFAtXLaYg/L/yUTo86nudPo/0ToPOFNT671UE4FxMvcIQW3UHRERl1NYtoVi1rorKX
zOR1b0oNgUUE9gyvZno0mgvjZKUWa2IGmD6OHqZu44IziHXDE2yjVbTBdzNYFI4ahrCeNBbOwxrj
fADbwZFvBSlmIlmTwVjGtoYd6I/KUgJvF86Lz8eHX66VyGf0pymRcAJWNDXMIgmMXYIrvWy8uCSG
UwOFucWoxf4mm0u2hlo+AXg84TEqAPEFcTbcI8f8cUvWz3PmfXlhv+AmV7qWFfRgyACEHY5jIAWZ
9eF9wU/LlnhC5NmnQ0OWSm0BLbsF0OmeZDoOjapsj0UbYnLY6EZCFUbXDF8QG/q2KyxZ/RhRKBVl
3Gdku9ZHTbq7/N9sNOmVd9yr46rR2yyfarKa+prCnCdE4VFlY1jLxDvZzeFqsPU74+ChjXBqynH7
6SDi7UEw3n+LXfqQJpENb3vccLGazPNyODnt4q/DdGYVN77E+Y0AENytkKPKsTm3rOxoTARpNNc5
vMOcrixQ5i45RbSZcwKYsuSyMt1YBkGRuoBkzfZtpS9zVpKc5mkazjVbT7i8dKnm/tJCPhDAKNLq
HSUwSALZMaoBhGZy9Ro0viWNRGhT4L9+59B0tSW24EdYzWUbTFEAPMHgZFDDropnv6arVMGP3fPl
4NJr5t6mvjehz9NU74K+3Rt7VO0QxZveKKwP1Buqx/jvbgqrTLuA7uDttaDeeQjlc1DpWZboiWRI
Y37BGlARcZQUFYB8cd7vtzaUskDV4GePdwo0qxkXTY3c4ssIKNyOtk3YhzU30bbyfQ5XVQfQCFdS
XwadwX6t9HCiKzeP0JxSjaT8C1JEvdJLjGE2396qUV8A7MzXkr0XWxb+MGOAgExPOPPXKhkx25Xg
ZLH/r4cfiHj5lMyO2zwLFTUH/hc0tPC6T94kQBs7RmR8xDQEJD9Nrw4gdhGbnq4sQwh6XYcpqj0S
SQmnEBCrBa1/IiRtf/D1OTShU18hNC4Em8jenQOQGzklKAM5561Or/b6Q6OIV2eUphZWdyJTTRoQ
qUxfHfjh6ABnB2amL6KmnRE9qagNMx8+XyWT7gNF+JeOwZHjsQoJL9yv3I5eI6df1VTuZpE3g3tc
FUVdp4O37DsWnYaa5xCDw/i8gkXvaBBo7gSxfJp8YR5v/Hod5oH0wP0cEpB+xQ3yG8+CfBECkofg
aRCElrF5RsAhCMuDc+hju9pafwtIvxL7fMmrixExr4/gQefXda36y6ynQEr2P3kYJNvgOyRTqYf4
IrQvfUN1ic4CVoHF8VndlMCF2j/aiXuyDimyyn6shKr1z8o480Of/NPqkm6lZiIH7oPQ17Y8adjX
AJ0we/l8DUDEf0jpZFm7JC8SPMQhEGuWRI/A0KvJB5J9C2nYZAaEqy+a7wkpU55sZOMmZWNLAGvF
bTMARfZt1mEpSoh134ux9br5Z2qiloD5hD+EdJPfkvzMRVOaUYYPU7pvMRcfVmFqAEYUPSOq5x/D
Z4X101H5Ws6hKVtGa5yvg9rSGEIThq3BEiInaODyWX+bGQfJFqjPyxZX19Z6++Rt3c0LswkAngIG
5mbfEJtfdavT4ZrLdkoGpJlBcVXZpzuEuFYTsx1Y2imHXttJPh4IiGIebtBWl4WpBtqCVuVr0mIN
uJZRXNs+ucmJmkBly0ea88aqKt13OVBNz6CDR0uk0400Zl8qiNguOonLjhyDnty3OikqTcNucOEn
X6EsVXaUvJRpro72ESfIuWMgLQdKVFyF61im1M1rD8wqjkJVbWYD2vycNjjBlyAj7afyFmibTXM2
6gQ4rj5lQVp5Bghyrg4RiISU+svLfoSii/t2BR2GZbuQmS9Umf8yh8o94S9uHY0v0eexBHnFymDQ
Dq6ayh2Wnrciyb9QXEYJ3Vxh9oF2LB4Kv6iUe/9V/sYOWKU+cijhkFz/jCkCpOPetrC7thc5UmsF
ul5ounx8210naxKMXHfMevqGPFrpKk+ZHBnTviPP6C0ygDXwUlpL+ymtsJiWuxXEokedsedm5S1a
ykxjrRTLkfJJK7PH1+adyBB5oA2cMsJWKUIUzQC67tpUn6KxrIXjyhSRDgYHNi3su7iEtt5E5Xq6
m/aELjLZr5sF0W7Xr5Uk1Bijt0YGpoX73OYFB7aPwP19SlupC4LzUw9VX1qYH3BqYS+IxMtf/c7P
ecPhVSb2XqqpgGq/rfja1Tb0wNEfx/MSzRndaMYYcc2WR3y2FzMl/YatMu+nY1mnjzqscUes/1Fy
tCKit4EgcTj8uTsdPdprnZpplgs5ro7TR4h7Fh3ri5Dbp3+0WO/glCAoyggE0Aqjw6DYbetmp1Ab
40mLheiOUI71+iSXMoIX2lJ2Mvp3Nk/3h0gSFK1c1tGLWtht/9vIRWKDIxFWAoBn/cwd4+1zt8n8
8K2MwRvm9lY2eYT8Jtk5g82qGFREJNQs01AaqUx1DdbVBT3Qyr9Ad8UUdQLhm4D9JAGl3AQ0UsTC
qB4A9J6I0eXFFoswNG0qhgfChEZJ4aG7Bz8N9h1uN8HQlBlOqvtkRsNxeV6DQzYtQgcp+Ex6aG9R
krk0NKF4XOuNU6aKhbZ62adhzs4CHan6qpqdcUilVNk1kg+Co3tST2hmxYjAF6/p8YFNeYqB3gSM
OtT2r3d+7GH6OxUqAS4lhkXOFPGAAKGaSBmSr2z+Qc5vasxgKdKRDNL5qEzXCeK1upJa7Y1Ngu+C
wD3a9MAxVU7uFWtTrz40T0iUjUguBB/NdltiUZg3KPWGVnLghzgSAiEtm0gAv490L6ljFinlJsX6
0Yvh5IBhJ3QxS3nP6C6sDkFpIoGMrNPlr4l9m//EHVNg560HNOmyU6tSFK0gZjtWvS0+uv3HP5Q4
JFVCcCrVz9SxpQTOGmjpMBncbkyzozWCyuDWJgYp3armczPAec0u7C3gnZtcNppHNTu4ToVQr6Rp
QT2h+rEFvWeHAPO96mJUPAcIJZjIReKWGsgysAqk/TlemIx5p4HjkI0f+TEBqpo2ZADfP7jNFn3M
DZH43EuEo42XmYxbtoRvp30t8Z/tqoycmRx3HdfNuIehhE2K6z1XaK9Z8AidU/dEZG9YRq/ZaAsp
gUcq9y9viANBM4lNjkFhPCA0ffuoUF24HNCthDRpwI7XebNGtil/R/bZnDWm2nA6OaORnuGv1vlI
7t6x3q5mNv77YWmoYWdQiVK3Qwl6D/nysc1Eo7VzrnuvBk4XOhtg/zNkPp1ViLW5rP/YuH+Qae37
RwDcMwa2+dLj9reLnvDV83HQ2YurDedWSp7UhNHOxeDsVinFpvy8s/mRFdNmrLtSFqGECatD3mAE
jYA5B2no7N+wnhGkJgCjCRnZRf9WWAUUYaZuxSHcYJqwUT6RxyPnmipqCVGwLRI9knhSYWJ7iwhO
vIXcN8nlgsB7NnqWeRGi+EsW3lMnkQ+kwCqH4VMUl9oeegLvFrL0506bzsRQmYxUEV17VAkM/eZs
UMc4UuSy2NKDb+yaNmnnnDZ/GVM+9fMG+9ShmMfpRET8Rh9oo2mRaK1ltT5Dn3tisbUhXcna+tNu
TdpuB5u7OxzAs7X5IkIiOqVXstEx5+N9UBHYvnwkHQyQyK7GWov1qtrLkkJ7iqClJDUlyRoHCrwq
Du+ydBjG6ksLznprvc7iy3zMVXD9OB+EFpd+amzlo1YfkQIGUo065wZsaIUIRRCOBcmZZdcY8+Cw
9z25Fuijwzax1vt5BX2jIscmxTeNXBp+UvzXIMzjC0abCoF2+7PuMVTBtKxSQt1O4RgSJxAAd+uj
OeFiykJmy37AzTqaj2Pai8+BLBp5hQbG55CbTgd2XU7w0LnvgK8/H5IcjYQeQzqmuOpnbJdZt7F4
RFqVHB6+raXB84IABVK9+Fget2b4z+qlMtgwrgCbvDDd8BUcFP0teC37WOOrOVYmvnJyNaWXzLmF
V+L7SQGotgItSg5S+g1O5cNM4MY7U2GPpPVCQ7mfbybmEDcgaGVW+z7FjlxRLoAMgdoApEUlKS55
fSnsV2rJ0w6Ycal9IDyoMb0DYnnSGqrxOAEUH8x1fnNRc1mgq+bZg0ZQTtaGXBSUo6Q3PpEE6EEz
0uAA2PROmxfjPhfFkuBQT0MM2tOray9p4n9aA9ETgE4KIcJ0SdilnF5ECc+qc+yUQ5bI0XkaiWpH
rYJjUhpRXlpx5fIvsoTMZ/OGGIe2ujXkuEwNbGoynl7D5UYuCJHucmThO4sRwJDGPwI4VmxeBGOs
EJc7kzHySZV8yl6ycwegFvkOHv6V+hQ8DsBnXQ3sIRnP0Bxd+yc6MOSVweXskqm1vtuLVcRfxLel
Oq1qKJk63BPBtk0kgFN1YsVK3dErC+9+u6m8S1G8Sz2zg2yOGvfEaiPvxSVC6O6xCMhGvm3f9ykQ
ILUi6pqLH0VpEKNoybQRNaJaRT8Ef6BsYZmRd4dKXmoX5f8f47oGdYqVTkel24iXvqiQr3GdAqOt
zd25zgKSbWex8XWydk9JWnb/SGonMX2KZG4ZxB2J8eMxg/8VopDm4jzqIg6mbCB00BLOZBSQ2MVE
kz7fCcGOxICSYAaMDwjc6p/SXFpIarr0jUtIWPen5nuCuTByr0hIpt/gN+z/kPZeHla4CbyYtUwf
PCiBhg2C3RuIviC5H5SnHobSniDqwfczw9M7DrpEQXqlTll3BeVXB7QprkfqTUl9Wpk+kjr7A/Md
nl+rPF+MDFU8xcWWhmakI1dEzPudQsX2tBchctLTsZsn2k+/7mTfiXbuVUrIA2mNlXYykjsltFaA
bcHXOy5pGjvCt6Kpk419NFzSWeV7EsoxI3ou14PMtmb7PZSPub3G7KXm+wpTiWLwMP9ilHln46zT
MboBIxAInG9BNDf2NKoUJJYx+EHvI0NG9f2FvLb1v79+0CtcUcg6mnxbEkIeJ4HSkX1Yx1PBO5fi
D5VpOFRsa6JTI2sks+X0DeRwrpNRldcppHiUYTuFurueIcZT18tS2B7BdttXVZFCBqNg94nUVrGN
nejURi37yT9aoJ4EGL+xg5NoclpXk7RHrECnUXM54X7eVQ1sha2bZDK9T+dcUZS+REd4D9ztqpIF
kWdGsniaFGQQgj6Bk7SFAKJXMxX3IPTumE/LpvYiC6RvuqTMQ9CeILyilo4TIq9QNQrwxcNIwhNj
MBOlMW3MwbfVMKeVZZemVRbNUNcupNxKIF5jMS/8kjRqq2LmmH5kZZd48I1F7g2834vPAxVtiyYx
LIUTOgOgFXOBTXs13Tevq4LbC6dVdWuFSEnjUqHuUvpPXs6ljkOESMt+EJ9GB65r8knuXKaH0Eav
YXvOhkmedahxXIMCLfrkW/a2vOg5Ni+kKYtqg5vx7XGnxB48joFbt+qWXBO6Ti7YKoFvAdEzu+Zx
etr1X85gYxq/s/R2E2DggmIPskvJ1Tvkwp4JGQezlbJFbYnnKFDImWc0R8My0WZX+r9dNV/c6F5E
BR9qiJtOy/J9KnQ9GFHaHnweSKX47dZ4gxxThKNLetpLGr7l2nwifGGpLVepb8KRDYSHxBq916iu
yFWN+iuoqSWEOLMY1fR9lU/sGXtqIIxNf5/hnQaADF5yid2xERz1QFIqbfVhnxGBsI0Cp5EcmphS
yPJwjdH1OF0PF/6vowfakAIGWuDT969eMaD98zqf9ZNZrr+ixFwXK5f1hnhOFbwRjTYcpj7viavI
st03JDsHtbyrt1jbaDVcuqcZrivP85OVy9KcVoRlrXD0j0+4amk8IOmwegE16vpFEJp0LqDxZbmL
EdzrOsi0qxtMunqa8sTAK4IiZ+gzXmhTjj6YLVAqgPSzGqn5QbiatRlSDdofGC9NEhvyYttG32ch
yoivVqJWG7j1/zco6Qtdg/thQflaZ92D2X12jzClrzJKt+YAWyENAtkvIKFK0pmSBwgwMhXBMqmZ
dLc1S2ZbEvnGKFkTffXaFHe1fF16MlRTc+WzXgrlqe2aUGx6h4dASTKkhzMMYTaCRl3aDNvvgLKL
9oWzi/Zmg0m5mywPk1cV643j22zzYHY67M13R3JGs/+e/U2BQbxEKq9duATuSelF13mmD1NN5GYi
Uj+wJtaDN4llzay7ec45LD7QE6RIWEQ3YrEPDml/fciXy51zX/eSjNDGbPcWqiLp6JcWp0mbiL5F
UVWADFLfcAOuxfNqRlrsc7++gifZOSnng2jfYnz6J/BLWfUMgO9YyHZ8HQiA6xC+lHqemW5PfWq4
osU/ylfbBxQS8noLLF9QIpb9IfowETRuC6YTv7HJ2W4ZDcArTs+tmzAseYynzpPF6wT03mwWPdY/
W2CPcnv0cMvzqDVnWZ90FY6bfhs4UQp2m3m0vwZMRPE64kSjXsVCFxorBmsWYjniyaP9cNhBnV9t
jmrBAY58nCEbjG3UYICOkNDbPu0bU946p/3pwFsS4PCTvTNHlGGZsYVHj6mzwPE0JcAlXEETVNVb
K/Z8BCHsZKAyViCuZ/hke67PfdWG6oPzZI5onm3BALUj/KkecV7y/JGy8WMnnPF5IUuEYHqjWZ1E
SIZztoCuDbW7nNzv7uepBlh8S34f9P3wakk8k5KzfOua6ih4jlaFHDxlQafioQvfGckSbjaZKzF0
Q5wT0fVtIFJe7D0P7Z6Lyf0l+zvu+EN/p17rvU+bbhEcYo8E5B65kSao7i8FLSp8uvmx+1XoUpFb
XLYt99Eu06V9xnv6tdINAHVc320/74if2BDK+6Fik4ji6JvoGi9BUnDQUsgVr4GJ9y2OEVOM5O+7
vlDKMlkDnvlSsmxYqrZdKdvQw6flpwbzSmjuxclV1j7KUl9l89Ydx8gk8prmuVhukkHnPuZv4dTd
Bqra+yG7MxPKz+9CKwgU1046gzu9RT2nAnPMVHuISALKjD6TCvJvqq1nfHCAwNVhmz+TVEgd3NML
eoL3P6/WjlTeRxDTVtBVj/e+x1wlVmO8OND1Rm9VdalrlhnjE/nGtVEII0mPEpToxJbbjeV4aNKv
4/J0YbRIcJTbFD5Jd1frXtnng1M8Of1Jy4Zh9msw52lO2aYaUXvMe2qct5br3rI0WiJrh+9ANa9s
N6/9tLAT2o+emdYdaDW94c33JqOOQxEjDRQ9AEt90OttnIm1Tcma8HnjkBDQTiiOTksUCFeNUwHB
W3wAomHvxLZfkcXrkl1dh7MlR4UsavMEFL9zvPdGDffH7+oIBMMHjMYanogztm5Das9uhtWEM7Bd
CmKsp6g2lBdyXAAG/Q5ivdsKjCDEz6swZQqwT0lyk2jGKteE0sXWGYucz5twgUGLUwvV4kqfLJXJ
40Y6du75XnKwhqAA+G5qQUXzL6XHENthp8D6Fw3czS5mShpWdFCXQ2VXlqXYX/I+ffUnmjkFmxDA
cV40GwHO9cao0+VBkqgilRKNy7/0yPASZP7ABdoRui4IxXTi89CCz62nIlWpOujcgbgymGa96wXn
npTWBfUr+sPUt3CxEUm02yBvPLfizm3NDn9eyUEPVerjaYDefP3V9krp7WAwRW37md6VPbnxDjtu
nk394q5QRQDsvzxcHb59tqAX7CDinzCu9/o52718CgHWJN3G/or5S8iXdLORwXdB/0XOGXKwur4K
0K/wti4kLhcu2OeHJCEpz8xf4ElCbiErtHd2g3IL1sHU1leeDSxWUWQx2ReziyJMUXLf1gw0Dzi2
COKbdPl2QhYyGOC0Znzus23QIrKtiyzwiHhzs2VaMq9eippjNgHgv5U9JGDQssU4MYD0ZX6IM+Th
L/WaVNdOZZTetqecv/pmSQw73ZXWA4HpRzXsZgam/+QBSHPMkhy4PWgBkjQQCiML6jqzk7vnnqmi
YBsQIPaA77E/2dGoXbP0L6MMgtanD01BAAgXreN+8kDm+i5TQi+7kVt0kzAqQUaoiTq44pBY6/3j
ZaGcjieXxjcb91B3sqSRRxTIUI7RomYWwypwYiszhN9zRysaq/1oI0JwyiEpxmGQqT2SP5kbXOVR
yT5PWuD5uZFC6GFt5m4Yhb11j2u8JYBquDH0iQWWffiAnK8vC3JNDUwSaPLWqf20Cda6PNtckgJ6
9JwE9yJ6sdshKfXNFKX2E8b/FfdY58i3MHk43ThLXV3eJg4ieA0VHt2rQbLaOcNiwtsFE3SmpeyR
Ry8blkVJ/KGHfsR42l0F6G9ozDxf7g5luLB769LrQfPXxhEAp61AU+9hpYXH8A0ikUAtdozl6hue
FCWOwafnp4eGzieYLgIzkV5OBsj35EZuzdW45vqH8d+n96fx4dFuCCPIk84PP0zgPI3HMoCZ8HOY
615e4bzYwNr5beWO2iuh6B0Lo4aGFnBJyHJzLmVcFRLsnlKkJ/adwu7qrKPLcPn8Yv3VzUHibaWl
TfZmvXuy80ya83HmHfR1Ods9C6XPfZE+pYorXd976XV5LOUMpBpMIS6G8PSnsgp6Sz6KYFuHdjaq
XhMG78iwr+CromD4uxT4c8HsA2XWIaHUzx/XnOpdA3c+TxMSNqpu2CLgn48ng5X6j/IuZ8BW3QhD
7Mi/KtjuHUagG41Aa8fszimnLQ62HOMlwJMLnxx/HrRXn/KAGaMhyslzCEKQz+h+11kl07tKqh25
KbMOm0NrUHMtjljOzKDdH+1QK3tLTZSJQinf/iUIoSMP4Wy1tPARtLph8yQmxEH4Ghui6uzB2icg
vIa29AsgkKTBmvHlf6GNYkXR8RB2xSZcR24PsC9Xu2E8a15l8Bi9fNCijLY+MiRyKCrTBQHpQ9IK
T3bUYLSS82kSX+onM2TIvT5IzBbuwe0r7UzuQYfE8fRC0A+fr6jMIoF5OL/IWHbar6aQAulito50
AYpUKLLkNybSiCAQxz+1j21PtoROV6rzwWMLTqF31KckH7Y2vijYMcB4Mk0z54qmXnhFjP4TJI5Q
QbMpRaSLFFxqiTtrVY/Oyz9RVQH0NYlCCsA/R6zjaVb/sxVn1pFuHlZmp36kwHsHtkZpPssE6WkL
MTZU43u3j/qkpQI1Z195cedccF2pURVeWvwKE3ffKS+yFI7WukHoqkw1j/dUn5Wz/C/vTKPj4PsY
eSQ9iatHVLrOMGRrvvj2TuH2TOh+5asnRVEb1lXRLPdeXfl+HUI0VPSR3g+hUFIyROsObvUTsKea
rfofRHGTDnkLQ3uykmWEjPCtqgV+pVQ3OPoJNaYeOPnz9Xs8wB6XXy3o77yeDMruKnm/nUpAqvx7
foGGG9iUWsxgaaFJmKn26SYxhrevE2HhpPillLP+JaX3LkBR1cuIJGngx7Q1szclvSpSM1Jjnq62
guIV4zNaY+eW80mpywbvBvexozGqzTZ3l9vC+eAZwGnsiAOoYHCD99rnVE3gqF+nAfzqVaCQv36Q
OXAx3PxHAryqacG8ls+wtFAtXUPMDHCfXrSyXfgGW9A6UC9lxVdu+5qjgiMaVcYgbRifRL2EyRWJ
3oXNZMbO+EYxoEFmfCyEe/yqGQzIoH+McjDm35bAh9GbjNsl7lwtitYPB0VWn/mPq5NEQGYBIFoQ
9SbXsznzZiieT3rKtLZpu3r3mQ6NYcgEgaz9j9CJRarjIksyVunqCcpbxVLWKaaRc1/MXPnWU5BP
q0fAgiC7Z4Hz8JuzQ7pEg0tWvxVJhNAa6l9MBAtGBwcUnABzGhX0KrZHa9CqXBX0yvaxeVNt7e7x
PZ/EmCgFAjZ006f+15fKZ/VpMdYZvERBcLG/7ivt3n/HQeGYffKgbJou+V9Vs7nrdiHNKwuJhreC
VROTCYNzP9wme+Dg2WPdQKqvSKi0NS5YUyS2b3NYA5WDu69bqh+mgXNqGdwRBZmyPI1ivaAaJqPx
Jj+ZdYtRguFp5RHY+4uPgrbwB6XQ4DCeA1fdOib/rpTfmdTJUWHSVce4DSwdBTSICH4aK2gdq53a
DgQEhEHazTWyEziNfPWmWRegkUqeh3u6p/M6S/se2PfuRGo2HFtgJ07OKWXd1X9X+VU4ZYZJjTvn
NrUns09FlDfbtNWh+6Mx+EW8BEY2vPA3g7hcFdyqJGeykUSDEASnBOz5BdzY/KzAcLvT5Qwv359+
exV1poB2gfPwmFdst1J/tt/BKvif2y3Ww62ZNQS07VJ79sAPZXb2g+pi3B+QEOv4Jibm452SmYY3
92odJvhZWZaztm5CddO5Qb5bk2l/6nBgNr0Erp8R9AN47WKR1c0p5GhbfdLfHNbfm/jaBI/IJeLp
wOfZ5BNWaBRzzAyQIgOs/gbegIt41MACRMZiCap8KF5lhU7jaU14bvIW/8FU+HQiFORckwQZ0ams
M/rVZJhMe4+Q9scNLW8BJNvo9uq1gmgyxRDmjnQVk3D+chaP7MeScfgGko9AbD+UcJmBu25/vOIX
/zXYdcmcRY+ntzDEeF/pEqWCDMkzRDvHmOTkVxJ+elh9f4NVwZaJRVtX3msfd23nBHnvle8jSMtZ
LNyTge8Ifi2RIP3R3jy/tNyvX5sS+itNM0/qbFW2XVPJIe6w2OqeLhFglcsRJKaXeSDOtCEyTnHJ
FfRddI72OVnRAbBedNlid96y7ZRMpSh2s86iCYYE00zAvCguoewhkfAPO2+avYBx52Iy3m1Zfc/7
lh9WzYtQ8zUmL8oCECK4y29wNsn6zkN4ikT4HtN5UeNQm5wsQ/q2y0hbwFBjsWoA84yCzadlq4GS
HxIlQhRfyaL8f/8zWMnvUdA/B2t62j4o3fIyXg28XMWfaX0azHU2Fj3qTK7DMlKA3pZTlxA7xre9
dPc8hRh5O8a99XGB+zpVSY9UFdwyeFse73Nc+OB86909haslz2RTBEr5rTce5B14HYv50vxcH/+2
SPMNNz0XQmDqKhcLtZFLI9TEX2Wj2nttIgWIsomF87n5n1yIHQm4o9e9WtnTsfc77MdB+pgIAj/p
1NevC+EfFwUzlfv7zuPeGHmcz717x07H683CwdTxvkkRlOu8YZdxseoCEsXJfDZxITF4ngDt8Yam
AJGtElKyTYTcgsseAot/42jazzDylgCAdJh26OU2b/SsOfsLlg9cKvbZlhI/bgLPupvBUGa03u8G
SgzjaIHlQY6VtW6lADc3cP1esa08+LoO4VKqw7G4o3wYWdidK87Lvz/64r4rcY2jvbICDbV7x67z
aPHGB20Ll1YIRoocjWMhl6vCmkNe10J8pUP0uqRKKzDrYVqFy6FftH3R1CYVfg72zBCCHc/2EXdy
+A98dciW/iNy5H17sMRfY89CLalMB3/4E48AnksKO0jOL9zRxRNjsLHUb8xxOTw/8HxNt1qkoqWf
6oOcktN9PhbfbyWzxzr7k2iO8BZLZ424ZXTZ+/RafljL2WJhveC3/zSA3FWnHMpE+SQumqjOuR3W
62PZ61CV3B5v4Sb0AOXDeB0JBhv1FmSe+BdaYDamso6IZVaSfZmo6i/6vnS0nOaw0EhBtaBV0nSp
NTSuyNUzVUhxxmfxhTHYzXJ4a9xIbP76XpXmlWfddsRwbEW0AqnZwF6iVrS1Khfxb3EszNJqa8jl
cE061Uk8duUaIaqWX19vjpbUlKW3SWsgT6YF7zkkaaQo6Nb8fPjDx0SidK45QPJcDmmT1weEzmQs
diD43gyNDMGHaisCjJToc4wHMbGBCUqd7ZHW5TU3mkfnr7SLx9MyRuVsCkPtx+wPKUYpHqKbO0Oj
zHm3puHQP/SUHVubbKm0CZducgOA+8Bqbjag8i6HEsQepbzsErzrHhEGyVd7DVrQ7hBZvaSobVry
+9AZJ+ZP9fws1kvihlmEFWJsi/FzcStUH4CemPqFuaU2QuxNXIWSGoDcspUer0QLXkvToQRowb0p
pDM0sy0hVZivif0IHr7qMjytYQBC1i/RbWCLxE6AO0yGFDUNoM6tm3JZdoNe+5PG0JaWrfmpvUO2
RQf1Gmq7VKEG7eEemhrOL8kFTnHywekLLlHkR87wnUUSN1937T/nS/Bgl550s7zr8nRSEAqeTafF
qnnmGv3N7TvHvejBPOGSPGmVJVBg8brQzmmWeaIn0CYV1AVZhn+MmO7u3s6Tle7ver2FkuIcYISV
0+0uwgc3ixyGEx0fddLAIRGS5pFeYs3NBRGBN+DMSuZNMBqxpcH9Lwo64hVBRbzOjwgyWEOkOVTr
X+0fGeq1+Nyte9u2heeOx9Vjz8iXkfQ0RX1HCvhWi9BpiawbcWf9G3EgArV7wi+PyUIlOCU+3lVh
i/HBvxPaclR4OKH1vVRkYNkA9HSx4cWBhEbWZzmrqo1hD6SF7mOyUbgxI8qAIdbN+odqQuWkBuWQ
JLx3SQT25ORE2zoFuPGF2PkVGbmKxpg8BmIPg87quXlNuDCPjq7W4Eox2mD5EmHCQDU9YiQOHw7P
RAKShdGGegea/1dOQlsI9KME7vDsVXicPVeVcCIKzehaJ9hTz8c+avMUKWJVkrHmLs6VuXVtVkka
m2q4ww3M7FYojltjIRE5mjuIJcbNlSSulraJkwmck3icQAQpn5/vVCSrCzzEjUaUkoLDzOOwITrL
yJOtnyYlBNKXR7JXTfHKasx5Loqz8gpSCiRciliBPkPYwexVb7l2sj697XIEq/CvcBIECVihVeL6
rq1ZHTRQRoB5C2IRHCePJZ69Iv9+zYoIOhZr8drJy6z19TB0UwycLQW/OvtNeYAQwseChIDjmLMA
DqRTqbUT1IzrscBkyDfS0TvW2HUqODeyK1BiHrvJR1Wfc5AEscsktbuxQT+XQlKPoLH9pObMnBib
QWacqyOaHJcMQCrwoSyHrLW7Nref6LmAa4rTJSkLWqGUjWPTukuSKVG0/b+7xk+AhAAD9HUZJZBm
2yCN3Vd4dFF/WHVEWHQOlPDjFFBDRjdJHU/wZowiTjrhvHLB4azU5iGuUUqUFJv5mz+lyynnmLQM
gD7xgc4BAHw/1Z8OH0p/N3wIbWg/D8URrADIj/hpf4cIYi8PytcD7AgDLpOWfN6bmBL9gk+QPbVI
tfRJWgG11Of4iloCMYSm8GD6yCAUngE0RkOpfSq6Ozxvx3g9QCQepwqxEPjnHB63bwye8IqI+vkF
4su6RqvSxLVm1zjk1+9vLLW0DU6aCdNTUeiUp2/SverN7jtPRSoiMZ4R8rQIhea/lzcIMNlI7z1B
Ulrs1mjQcx7D1U2XQvk7KIDNX0kBU3ZSrECJlkgKjmrsEbtR3EeDXoILbZaBEILMRK0q7rRdPBwT
jMtUaB5H83qq02+zQw8rTXRC8brbMf6bqC6ROZ4ERwl3EQfbSL4XmjFjF1B5wGViyi5bMpsZRu2j
Xbk2XXMN6yWs5ZepIXN7JDvSi9CCOUDLrG9xl7UeQ5DElfvvzlL1zmj7FXslC5VdXMtuo4fKfnqH
lsQIMQfo0GUvAAM9qvxzlq0Cj2UJMJg9d2MQgJY0vbhAJlY9+nEF85CLStnWplp4yAO9u/O6h6Vv
vTM52+nlee7ShV2T3dJ9ojQLYQanQ3RbG6MV7yslpnq3rHiqAfRPzOpmadM8O8IbKmo8Opf4GbQu
JTVseBj0xQaqZ+ujRmm1vbNKb9lXuW37Zs7XpsXk5a3bl38DXhLN8DWVfP3D4CK3E4yWhVBZEJxU
47szRs4DR+IosHrSMLOqjp9tl4EGg0zWt3sIAYgim4iXfNHixJIsQi6ZIZemSVzP0jweygMFE2f0
iSm3/j54KyziQ0G82DqfZYtA1JIKUlAHEVXPAf5OZMS9s4wc5jR5xN3Y/G/BisY8DLcqlY3fO1US
Kip67pAPwOgL1GB86uyL9rTPKN6TiKkl0mgnpju0/b+H1Tgw4IRv9gsm2GbaYxMFdJt2YnbZsPYJ
FZE7rqjslZTvn+3s018Y44IhTNr5piTAqngHvpwLfZ02kZiqp/CtmXxIp1T1zI6P5ZdM10qsLwGz
ZLRivtD6lKeIxIgZty/r0VK6TOu8R9UErWdEnDI0JhkxtIJnzF+ZS6fBWnlGRjYq81ukX5A35IgW
gxipNa9JFJvokaBpH6JdOE7FWxm04bkzinzQMIcUeRNrrDAGf7xyeACVw/S/9WuAN6Znc/1oh5Bz
0Saq/V0eKvhGfpiQ/TwEJWBh82BLEzCzkYfC9I1CeL7G+EhovbfgxW3dRPUbRihY59hOldQqG7kM
kluxmSb1pSeff/NRwCpJrcv8yesLgN1hu/lxyZbRKw8raAHbwlO0HlgwMZB0LK9EM9PyjjAxCUqY
bqa19N/nBsql0P8a5HF54S7kg+SOvEfkiv1oCqzLfGfS44sgGNs7tsQEOpg2ByHbw/inaajMfhAe
H/Q3e/g6jCudtagna+zwEcwDtL1aYMR7Fdul6BWSseooPXveXRn8YtytRp/um+SpC5GE1HePMCWO
5TY58+jQ6Lx353ielD3+4VsPxfsm42TB1rwRUmMrJmu0WHRjOJJTuJNlpYhHIHVVEifYnDmOjbd5
+D02BDT+jfC2kAhyGO1ePIbLpKqXJxEr1wo2NvaZxlGBlK50Gwi/soS1sNymJdiNRKmjCdx4tCDQ
4A+nRzFgnhp68dDcZHBHy0PB3dtmS0/6SaALnzGGaGOmEIXHp6GYJTmYmPUYO7/60WeUW3tXt+RT
nSF7sluu0Bc41EuKY+WbrM52X+C9FnWNNXnfylvwSeiocW/uQTR/IvW1SU8V3hQ09DG/K6T3gECd
6tblZPYZqawqY/iRS6Z6Xt3w3bYGJpU8nyVsugd7xCz8HfM6wM9Lw8A2f/YU1kG9GY2VBa5JfjjA
/oZkt1ShxgRC3YGynR4AJUPwq8hEitceOWThQYmNG2YD3NSaz2EwD1YwPanmHTjQbdpA+I0ZhZeR
jAhp3aNV4zIf7gYHOiGpjxbjvZ0FCWEkqE0OVPPM+NYUeKFBC6836injQSLclDoXdTRO9h6m1Lu8
ncY5tRv6S+vML2hRBR+Hnk7Dkx/OFOIK9BiooE5FlWLLIewGtekEAi6zTda7shVKldzAAGeJmD0i
qgpa0ar/kF9M627ds6EMWxzJZD09zXNU+nnJhNm4RvcY6CheDa5ZIVrFYmyhR0uiw/gIWsKzrr/H
uRmlJ40dqO0NxiNnq8Vm/JnHz6lcVuKxPmSJ6RLeZdsdAYnnXRm8V61O/JMWEbTIjCOEICuNQh5o
rnYzlLfNXQjL3q/f1sSZmJavMGezmtQ8SAtCv4/W5Xo0sEC8GorDGH2cldPZOB65lcPhAZ2TGrxG
dV8ICFsK/+7kpR5kfS+mqr+cIeV3+O3hnrCISastF6e8O1d8ehBiZofsW36QVG/n2JRYh5binZgx
jSr9ECcrCKnJBwyHjSyMQpa3HUqzqF9jAEhiof0G0tjmtjn18CbGM1Kydvds2N5ZUubhRwGSR0IY
KL2mqshxVbNz1CcqjwtU3xuVDhu8M1kzc+5JYuAoht+Trd1pePq/fCDdxkVQZZ3c3xcC4TLdB826
uy3phNYsONUu/cP9gP79NadPYGlru/h3CxY1L8rxSUyOhAAuTHj71Tw7Jnpn8j/vlH6klPeqKWGp
jLboJzkwxU+sMo+YuvaDOyOQfgrPgMRhdsMxXYWeKQNTsB8XEKUBceP3dT2KzsaPXcTibPxx/SKp
XmIAOZM7V50DBmjfaqvqcLg4Sw7BczJGCBEXpLIFDit/Ll5pDBVNGRlmJp+F/7FrCTtn9PzpXOy8
wMHxk1YdD/A/YhkmbiLnQWOSVK2aP+gCwwDKR4K9lRf88l6am1EwVZWyBdx6R4z1kLh5KAycLwmz
zG8KFW1MXeZasbJWDLyFlRxKQ+he1SG2SIJV5qZ9to5bcR+l776i5MFMwY1KGUYrH9rd+WCJaLOP
QmlJfG/Oj5YDOvUXG78GG90mCajna0e4Ukcjkwoh2rvqbukrMfEOsCHOCs/xUvjqnzheREGnqfVP
virfmZS3oh+gX9UrF5FePVQSVZCLJkSFYvFBJZgtmILDTRecV/YYzj6mgoF6gV/Jp+LlcO14Xy5N
eNxf/YOUh0O8m5VAIzF9AbSYJ0UwfYs1rOVrwZXNv67MKkJuMd0zRoONyUw0LXuc1hrmP3avpo6n
/uxWHDDLmQ6GT50FoX0FFUF1uJiQrz/wgRNmXUn6sTIK2BOxaDv7LYI7I4z96OJ42Q2j8HdHA6sX
B7odFaNiN+4SMRexrIX/19xtb+TlKdxNbGxlzgr1MM5+q0Nxl2jOCh+5pIiKJ1SGw61bpwgs/JTB
fqQE7PWs5bc6ZB+g8MCdMMqYiHGoFATkWIeRq92KmaOwHCJ2TZsMX6f6h4JWNVPNY1TPG0Em3sqV
aPg8u1V4FcAYJolTzCSAkZ4MK02Lr1oE+/sPT/IOE8nqbsAaXPYlal/NreBlXVK4uVzwKI/pSI8X
OnIiiklNHhP51/1GLqtUBn+W5rgSIqLiT2JcXYydXL7bZ+CiwHJiDlfByxcvPhDIZrx3LWT4/b3L
3+FJlViTA9JB+8s+YWvAHOJ3gdIPFvVihOMS/ovPEfAaEPR1t9xik07669M7WOQJ4L+IUjle+4bZ
sozvFsJccX1B4CjgRwvyFChvSXdTjUBmyoi7a3ASkOQ45/lkByT8xxy2WnSBwPc0oZTwMhZEZ9Zk
B/OR9Bcmh69vgKy3qMbrHyPE8hRaoupPeeDonIQYNf3lDsTgklZZ3GZkOWSC9orcaIyJidoSdoxb
IA6gKV+NzZlSxH/RRlR4o9m2oEnlH2JMx/QfSmlJiCbsbIMXp8pg0uoZ9aOHQ2BvERj+VJY41Hq8
DrIDDprDjpK9d1XsHOEXR+dY+WQMLwEo//PNlYEbZO3dBKWvM0uDNg1Mq1B3/D60u3a8uAv5HadR
6WHUxh05STNI+OFuTt85kBYRFS/zELn4bl97wRMAmSYnnwXNZ0Jb3rFs3UDrWzTwZ5470x5mkCxr
oceT4w2k6Cal8sTjunFtlAtJaACzFq781uAQaC8nZ14NFm2Y/IaJGH0wWfzoSnLI4eMsfU7Rm7PN
syKs2cWtoDVuEIBv4526GvlVWIFmShnz8VZjTXL3AlDtDXNWfvv00kcaCno8eCrAOjMyk8PM98Tm
DmE4bYLa7EMPvlxIzSV8zybYmqVsfOF2Njesm72v3fk4lSzyLbYnSk/ULWR/1WkMnnEi96QSearI
wFHSwLQCno7ScIiTzCt4v3LMMkg59PBA7ACxZDS0y1AUQp3rRxFeXqm+gISQw1ZalKeQcLDzZtCD
wyCoA39QfPeE5KCEawv3Xl5PqoJMuZxgjk5etzPowj2xV+E1Zy+B6ENB7uWZQbyVyLFJJ5zKIaOD
eMXhM7CjnolXlqEMZ9MASxZy8AJyaGpIPGb+cqO6DjpDhq7REXOcq2Sil5+Tgh/FB58Npnqrhbvg
gwRPzn4Wsb68Ezd1sJD1dkAHm5PK77lIfB62PshyTQWPlCdV1daz/x2KCHA9SulhogVbr996sO5d
BQbpB/AgOa+06w4dQqruUZVIeXNm+kmsbzKor3W0E/Ogy0u2nIWPOZkPrkAxTibLOdr8Upb7Nb5q
CV8op+pbmIWPyu89JeD7m2af0URn91xbwYGJ+/FicgVbKGnHAhGK4jHJAEBapKJqZoV3Fg1VKi5x
mOqAJyR0mT3Ndrb6cKgKuKDfTY7nUp0OIyxY3vjduHr/HH1vJoBiqD3+txhqUITXvf5csbH84AeV
42rU5SV6ihYOZ0KbApGdBezYew6a7u1GbBZpGUTUheXAlUdGklfBlyEWL8suQ53dNup4o0g0BuVC
yw/D4NMcK3eLO22s6wk4KrkJTjmNLD/8GITkhyD2VsqGE7wwUABwELtBOgb3iBUXTEpvob1vPgg0
A1hhLQ/qGa5g48Wtmw93gl79khxKTnost6pg2A6RFQ6j4IOqv8ii2svp8XRAZCczHLiCOqcmLvGx
vC1FDUz2jt0aWc7gEzsf9DJY3FoW4byvFYTXnNPJWsV9pN6yv9PkIdR7EKVV0HwNmQTtc/3bL00i
tk7/zJIwgs7wAgj5763UXcD46h3yBd1MJsqmAns5AzrwzcN0TMV0+o0XxpTzDWmM6U57b7IkIfgn
0nSPLEymEKS77u/KfjcDEkDuu4+ZYc6havtwNXnWaNSQE2KoEEpn7vjkS5ljpQv7cU6g3flbD2Ib
xe/bCGWH4qLmDPwvGyNAfbGHh6uoBr9oI+JrTwuY/BoOpMCyjz2DsIHPtRV+wUJ74ero2CHEyI4j
LB6O2fQZYDHUBIVrAmrljcw4AqYK/DL2yH73bEhUYYefUnEzozpTR6Y4vD7+n+Omt23DGjXYLQPq
rtcojPhpbwHbwP8ReS6Vy6+2nr+6fy0GAP+TdwUM7rE793QAov3zj50t7r2fvx+imHM6zoGr+MYn
SzVFGIdvZvnMzqNSfN0N5+Qur+Z7MFjf/Y5oqtlgF8qH/Az7wKpRoI57+ta3JAVV4xlJeAWDftpZ
5hpAdHYUqmWBaTQD1HVXbLGrlHYbbjs5bKycR9OUSwk9EJsUAIPOM+kwHIhuoqMbAOpsy7xVkZVQ
TPEReUpYckAqJimFIyB6NBWv1Sd4UyiauOzqVWHJyhJn6tadbGUOQdNSVVuD6k8M5q3JhBFp+nTk
PACKjAN+Rl9FPGKK+fOLt+isuTjfedI4fqoG5t+4G97tT1OgksFZpF2luUWl/gYNShXXM6Odt6Oz
+sWFU/Ooe/K3XJcMlytp7MPRnO+6NYdzoaKwWIntvE+P6moZ8wmuJYlZUTrcgVjDhmehxuNoFex8
13tmeE/CToeXy+PPC1sTyRfXTqt4xacbZZn8BpGTLSi92a8voFx76uYUDjsrEXql5Ln1NF4stXxQ
O9Yh2UlN5sKEPoJ9bxZaB3novRFKmBhVb3uU/xJ0/BGYaMYLOJFG3MNZ49QmsYBzD1xQN5IGSZBb
f3VCqNiB405wq6BXRHuYJSE1qZTtmizjmvMXoDaF2QdeE1HJRVDoFPuJJti3XjLORpF6LLGSUgOX
U+zCz+IGZD396YWdbW1bOZIiHuFiy5KzVzCyDF6/A8oPl8aA0th1SURs4j8M+HrGFoBb+1g5Bq3T
iC6THlqL+x36cLfTZAlGyCltS/gYkokQgvLw+t+Wz/T7gGZZE8NI95cYQ+cQC/FmuABe6Y5pP/9I
xOQHtkPeJBHkWzy0ntHXnvMHPg7IXtDT0SylgTM2ypb1ZiCUHzajB9WHkIvMT2vfjluz+vX8yC9C
R5dWbp5w5M2NlYH5xb3/Gra/lAAv41Azl1dtN1AVco0bZgj4ENPYyCs+LR964f6Aki4mGdOA8136
Skyyf2mfJ0vezQt8xmZjZUKezbsE7tT7NNHewJgC0TWCjMAXqwZ5nwIUKFJtuythYOhqCptR21P+
kVK7inO89ZX3+wS0nQKb1p9lQVh/m7w6bsOPZBYj7nHOcl/PqGgY9k5haN8d02LhKoGhiFJA8H2N
YXSla7Re6YRw5gbXNqL/T5jrnSppCEcz4A89el4S3Cd7G6PENrhm0oNoj/YDFj2Yr4I+/90yIgsx
qatpAPhP/972BOMft9mORpib3QA9s9QKZKm+8qThiHbhuNQTkDZ9JixBZbyh0N4rlYvtHWmZi/up
SlHBYvWqtK7FrGCJp0N4o4nwGtITjBOArbXaE3QZfTx79CuHjs6naMCat2h6Ufa86D5eo5Qqi177
24Lo6xAz5gQPK7MxwCegoXm6S9BVvTEnd0MiQws3c2aZFoSat8IX9nbPHaLUOjWcvRWokNOQdGu7
7ajGIU7pXRevRyQbywHeUhV3bDbpsKt3cUYRg/rumejSMYsz6tj4K1YNGS0rD0WlSfonxcVtP9ML
WcF9usteepMxWAWh1bdGPus9JRv6T4wBBLrddsFCDyt1xLJu3LAdVdDHa9Pay3aPHpxvVL1TfX5K
0XfUsNT8cGdwgADpWVm45LnlvOIiT2ZbTVPAaoQvcVscYg4gRWRCv1BpYMWMms+P8SxcWRIrzsfz
i/Xn18EzSTaV7HkW3/LcbTvUZDelbAXvfEFw+dhS4FJK8iAdRZVeKvWMO7Ow3qfnchJo0gLuUd4v
mcbALxEBmxygkFnMbQDtamI8kIwA6TXcWsTNdB5IPasFCtVarvy+U0o337JI8eD0z9Y3wy426/mE
Eb7AI4ZKqidHl4bXeVwr+mx+1NghiJgk22CHvo3LUyihDZea/aTbqXYsfwGjBl9CBQd+PS27vM2D
6oPxfGbChwSUYC44j2ftvxE0x6MdSxI3s5Mjw0Juu+bfnpsGYAVzK2kXO3cu6kR6HNvHNB80yivC
9EQeeNlNrSZUMR0gnR9GxY3AfJ3w+3naWXIh0wLf5SCddNzesgeA3eOTD1bhM7Wr8ZNDlwhraHip
hwMjOfl5AGbCz5Pj2os5/EHceLLvVxWoXgJkiBdwp8cJLXeGpH/Syaziyo3bbR8vEAE4QOion9WR
ew4btP9p5mddBwx8cG7uoYRWepf5SiodntSFdgHog/zUp618GV/k23tbaZUf7kU93iuzTUA+lAEY
lDVdrO2fh4ucohsW26Oeac/o1WcP+2sLmQQwxs0WusZdO9ITWL7LsaNQKU7bvlVdhLLubcZ/zKsU
FgfUkhegDdxEhPNJ0h5Et0X1m0LEvG4xyz7K+38OoDMvnu0GdeSHLMT7xon6PbxK1A21sImswf/a
Wzsx6ikHbBk55HHtBfDtj4nzliwaQxvpbmWYqfhq9/Qg5wz82CJUBXQuRcZP3dF2xm8OmeIOqRH8
Maoge2r2wJd7H6HKJnUhEJnTjKFmWyUmro3dlnbgHej6Hk7pjjdfR4Z9TMeB2KtQfVzAb0nXJx6D
KFWgHuNmxFY88pUVXCFdyCYc25NrtnuB9CxaXI/xRFnW+SlvD6NjegczNysQL7rcRnslbTYSMyO+
LW2Sw7MpakpzkDKgiQx1Wgb4QccAscU/8OKcb35uWFnCfFkJk1h+0fnfH5kWbUCRb06ZfpMKlmEG
b2CeYcDrgnuNPQpVS3rUgAzHT7M+2CcN3/DD+1Crvh9KZINkuTPoSSVEJINPIkfToa0K0QtOkQSF
UTeohqVlRLyKnC2DqkRlb1AYByjbiyIUjJZbmF6j9wt41XtmA1pIrZ6SF2yg16rkNR9fJybUt6tw
m9nZXd0KxO8HTvF6SZF59Sx973Vl/WeILRxIEwlhaT2x/wMS827EmtJkSBg4EPVtQ8I56ZB18hPf
zzzEgi3FbxUWbPDKlBc9v43RmVQ6RXIlN7PPTuLNbVdnHsIabTOAb7FBr0anKjsccXr8lhR0wRW0
yKJTQhNQZLvZZZBfsF17HUlSNsmS46yYZXIKi8tziSSRKHQT5MvNz93cuFtI7AW2HRTAkemrT360
J6DgtOqhx6UkBrz8NrBQOaSo7TW/dP+BBD2+ouxPPbU9krLDP1wZc6eYu4KspQlYXItF20gE6uOU
DwWPdb7Eipquj97+wKsywh14EmoH/euxKuwjCujl8pL48smSuzVfWsa0Sw/BI0+r0o6raUwySleI
0Ruz47VFi5D4qLRZdHY1kqg2oC0Jh8+qZdmUFgE3mSgdB+auvePhmzXSdnMppTrPJOckWi8rhDy/
0Lm3B33JjlqIar4BNbBCLukU1RqtNVZ4gU8ENXsPGF7/6VDxgABOWefh7CVJCyx1lJHcpLFfn6s+
KS9I0Z7FSafja6HErjfzs5cNSsy36W/XnHXd0t9q8FZ37rtw0QqwUwnCFCf5fXhTJL4Z8lWcRk4b
jB5ximfwny6tg85SQYvImQIBXrNlUnrgU6zOQkDAKYPvng+cI6FV9bVZbHzA0CKJfGzs5mFn0l2u
F/dfA+m+VSzez+I96YkTQnUO5t1qBWtDWUG6cV20PLKqgOKVE5fgakbBLTPRBVzCQiu+gvx7MWb8
nw2bbm/Juw8nKDXGpuBN0WoLwTeZnJx1zL/E7n+OS6lo04/2At5QlAXY6AMQaowxqQdc04FGYzwK
oLqG+/5OBuChZ1Mc+bFY47pFgP83fknNXGjPuDFDssqIfP/x5isMGl1p4yh9OfT8CheJkYTxUiI6
Xbt3gZAhx3b0nV0j9UapRsy18dWGL4A0qiX3ecoFd3gbE9GtbbZp8a/GUZT5M8l6+MbdAhKvFQZa
wxEP+a/gNNdIxNcw2q6A7W77/c/C52UvUnxXibCZUwLB+Jdj5+lk3iIUqjnwy/SqWFwN3Gw7NBzl
nA6DE1f72bmcRotIPDzcDEf95BUVpYyX7Vq/XFZm1LegKeiHgAsdcvSazEk7b9mO9zjCs9ZkjRxJ
OdA7zDuKDiQwdVzTjwLg41JxBkSiu5iID+5T4SuSgItrEgIWW4nQWJy9KKtKQBOhbIwV75m1J1xr
MMf6E6XqTg2PqgMc1BR0NLgtlE+VUmJNZOTo04apGD6AaxWUt/VIvOdMiUGfzmqzgRAMowOzADxC
pxWqzbWBrEKERXrKQz7UXxM+uVIjZUmtXFZmch/lQOlEdkueyi1G3RiFf+NVf6W9Vg4WR5uvME95
3g9ptRCIYmNFvoVbD7c8kcoXvFzYmzzk9yZJoqymrfPq3Q6LrzF5eDoPHArNLkpBSIYDgmN2mmOY
U6Idg8dEwwVLJ2QU1cpS9m/6jmMtSz6bnzF2IGVGtMh+5FmTtnLXMZahEI/l7hODm6esPmaUbsb3
sQUi3Ul7a9ZIq/dkgC+0OP9SePsAMmEGuIFO9xPX7GMuARj4EtMYatW5MFH9i59aUNQILHYIqp4p
nM9yZLYtSl98G7K8nxKHbWECmth1jl71dhNAJ/ehl8w8wcj4CE9zQaXnfMKJpCQmuz9J+1LCMpuX
Y0D98aQRztE6Y5Bfys3pyP3UOZBaZ+LqzEviBdVOp6jwxUM9OCMmkDW8BS4ZHoYuyDBz4jGIgFSb
R9vdZzPOLt8f517qgI7el+57Zbp1U6XTe7oQVtDM8v/ZyjzMtHfVG7Jl2By8ttYeH6Uw0lsCaMhR
qJ5b2nuQIt0cj/917ZrxWEU7fz+6Kf63VgZG0RAAEefs1hIu7am4/G7IZxuIBmrWoN5EYaYMXdk7
PeeXdE+rykDqYlaW2QqucNkJTwvRXQLxLfC4cnTy60iDZ2+psPF2yugQkpWKgy7TStEfYN9r//Yr
g9XXfXR9FXN2QigHbWV0LAL87XPtwX+ygrJc0OOaq1uPSJ+kUpQ0BPyXaimFOl75aX/vJ0bh7kiR
YBfEdKjylLv2IWqtqR8QTCDlF2JZ9IuAUjPdZ08NDx9DH1ybNNMQ6aO17AR5MXI6oeYo9BfyTdry
Ig4pyBTG/SZ2dDF3YTkBVPPNrKJkgLYGOj5/UWGQVhjTqeEdu8WybbKwQr4jmc2kJALlHgU8ECvC
vJ4irdfU5hNL+Y5BFfWceDtaQGv/ub5a4VkHD4eZXmInfPxZwGuTwgyibcLWJMzBor50pqVU5clf
tuH2n5nsk5GRZ9fGLXIIxcrd+TfCumcExLxqhtXnnUj8jXqeqgKRRK1nGIcoywFYUEoRWwl0Xxem
9YsFofW+D+mNxY9NUuIGS+6hLXRahqefnKRts2oBGrZ6gPR30jI9hBUpypjAFDs8xfS9Q5dpxA8d
Yr8lau39AmTMnFqjy4/N6c7pQr6TcvfpX+RiyS8qNLl5wn8mP4LfRTmvKMn1ZG2ut6Gu4Kqxe1fv
yZ6iJ57OZN8sSSQ//gOFeYlPsP8nw9cK7RzKsL0i+RI1snYILAtu44z8HVEt23UCjL91U9ac6hAP
IVjs++5cKNg8n4nCvuiWS8/XDuku81biA5StYr3QVt3+1Wm3f8YhJRzd/4AqayIkY2UFJ5ih5lhw
sPUWqPDT0HAoBgTgYkzzHydur1s1W5vBbgaIqAILnjauMg+kGjk0nueyMVr7lJBu/90b9ZNqZP3i
9FB2qsfDX5XKEhH3a3mUQ6GjVif5UpFNKnyUNKtq9QtPKdCwQqLOpPzy182QIqd5ADgmGb7xE2un
Uz4N4S59TO+ua+PfucljBQLMWJ8LGJVo4vz2RTc+FZtTiqtSWEqJUsYmgEqwPCeW6yCFwu7iT16e
uxyMTs+ExnJwdJpvVaXsdYuTMMdZ+glXMaIGBQdzzCyFx7gKHkGQN+6wG8vtJv1xUOFLGm32+621
Ah1mLoD3UhHtTmJl8nsNiHn2CEaHZOQi/votkveNIHni4806RAinT2arCxa07qRQsSG22wQ4P736
u1qlp5f7AccMW/2nb6DhIH6V4Ir9d6xAU3qWtZ3+7n3dl9M0ntd12w/LNk+UqW8WgsFcPCXTAghK
VIOlKzm1cRrSRzaF+qNmX75daDo2t92kbf1yQls0nU29qSxOY+25aLHcgAmn7nWtPrehVIJig77U
15LkWuXmSSn9FddGQFBrRQjQUJBOwYJuvj9spHTyhfhTHHHwpt9Zbya3OkFpVOIu0VEDA8kZ6XJO
fmq3GeZkgBKFKAiGVYgGeRsPZJt3EtXq0D+MeZemrZwUn9IhcK0VyUQ5+MLwoSM7z2CK/bPJDCqO
zicxfSJDkngj2vIsIUOjSXIZ5e2H2S83XjVIa/o+ALTFegkahf3WptC7m+v0jBmsY4XvmFRPaS5n
rjEMTmzt8UmKDKRdTMSznhOF2iKNkhiZVadNvhSEkDDftcvfiePttQr+oloSiLj6ZFPSKDdfYe/T
F9SdiCKNTQI4b9YV6GUPO8fYLYLSzeRzKACHiRg5Wng6YQZK32NVa54PSWHQJjXLZT07bMTyDTze
p1Z0t6CQ192CcRlciBBhOyBpxmc2M+OhMpT5sVs6KmGfV+Np12ocSCZpmcHtcvyrnFxuKLNSp/mG
osVM8GszT3DKaaHneqf1jFR0EOu6vwqbYXrBYYX6G1sFu2pqxnQ1RQ9TpbAX8drbyeYlEa+THvjA
TUb2Q00uQBJFR3+6/F75pIcp/90OoebTs7NOk7M3Stxt4Od38aQtzeDln6lj68MhdD5FAwh7pwb+
OJVGEvcQxzZV30Ho0KM6ac2sqpjdapd5n+qSn3nVZNLo2ToA/bBH/3Xdm++x9tOViJBTkJswM5Xp
bLU1eSHvACry7xMNrAce14PKpBbISQreF6z++uSH5mvHupWqqCer1wuDlf40rMGXzKc2MhCz+oha
WkOris7124NbV/9riWlqmKCUonM7pd6sXgbQdraUM9kT18J0IfJogo744b0z1YC3h0L6Om6uYLSM
IcrxqAeMIge/fG485BIuQK6/5apmvn86v/EqidjCif3pF+3dlP7Nb43PxjgkEavOpVpNOeSgX7TB
HfqnfK6jqrveBVYjpa/feR73TLrErlRo/ATdGKWHpXLc0ditCPxR/uDw9n8fOERAR+UNmgP00wQ/
S1n0hX3SfqyBIcE+NuF3kaWjh3J5C63ucms/PfBUn73/wXBd4d1ppQp/lfZC8BjWOvb/CSm0nhl6
LsEuFGgetehbVbnhzZlarbevcFOSuPUXQPSKzj1O24lc706mYE74CQvzLjPv5ybQ97uPz0NyoIkk
1nNhMDQOh/wZEyysQF6+GyYYMgoEAc9J/bboO+vwInt3coCD+yo4k9F1nUNkWgZ9kDY0Qc5J57Lr
xlZHyiGyI32LEdT7cnj7NX3FUswLAj295mOlah83Jd6hblerzSXRWm1d3PL12epnvM9uHEVvBh6i
rLowbWybSge9oYnoblfU3Y+XFILSyV7MnWglWroC8HV3MKfTw/pen7ULGy6XhQhN+Y8xcAwAItWK
A4KKEPUtKfBmNXSo9/Z9wjcGQZGV5KyM/24wWnhmwV48CyO4sjdIeWfDJhKbQlGsP4EfsbRWcL4d
UFGMkAg9tgbdidIlXRJqmOYseueaPxweOYs0LpVeSYqixVV+6COrwS39c+i8tY6p5j4fqVgBh8rP
PUOjcTVoZrdCXtByMfqRa1AUColsTdId0PcZHbZM0bB93EA79KnPaZlemnFgTELgzKWV5HJYO7Ye
mxwGQDYmRFvPn4bPEwYgMjoJNA6DiSejIb8Mmw3Efy5ELTCYdNpiGLPIWmTRkyPffq7isJoyrc2j
uEayHzrBsoJXSlDnCf1FlMgJZeyBgtTCKQuY8ZAanevXXJPNBKKira20ndnC2OiaRQhV1Y56yIeB
2r7uzMUfzR1obGdjkh68FNkevV6+8dpNd1JVG+ehX51uGCVj4yw2VdupEsETB4YscV7IQV95yInF
0NLrG0h0mDp1ggHDpd5Mkumk4oanlT/BflMYaQx/ZcghplkQONio78r3zUNTQLvv1i2tcvcQ4RPu
3GVlt61rcXwjA0SK7YQo4DipF6Y2yN4Lis9lttP+5t1MxlLqy89qk0OoP2d4yxZi896KLH883P6I
+1aTqEXyhMpKAC07VTffXor4k+iKJykAn8dIZkaZZROc8S04HS5S9AZuy2L0H/wGBx52nBXpn0Cq
b8WHnDB6M3lxiptmcw6yGuqHgIUI1Kdj0RZFnTq5if0Dlp5DF5cJ+hY/XX5yq5M3RIKwG9XudHBF
fgBo0a0lZ2ELGuHfbeZMac0N3xGafKeioP2Qbz4u8DAKGXVIdxfCMOlDIIUCtT+bEv7tO+Z9mRXQ
f71mbRPDpuIypgSC9dmOOlQAtvuKORyLadlvrhDND+Cg9mTPxktAJW2551hHyFFzNMiXbzgo9YhM
1yN0SSjz6ksecqYamfBhVLrTU0EjiJ2owGnW0LPJOdU9+17mOK2AizX6qi7Dc6Zz1TTAcxhRI6WL
pgNqJrmDVeZy7uz8upKOnQjqfExqfAcyhQgWQKADS4x6zpo0mc5ZNC0iAwHFM2b9IBMBxvugPh2n
a8PuU5YWoPTUsjnQ14ge6n2WJrR8VsUJdT/qybP+nRvMqHd7VaGZJQ2WV0rXfboLNa0kim+vYD9M
4yLKFjUGmXIVnZL4ah+g19mgOQ1c8+ezT34nJFFbMD9fX6gIbsAsPHN728A0slBsU+32CS/aY035
ddkxpcBTjR/7rm/LBvatB4N7nlHfCyEu44ehc4PUqc1TuPWESCATt32dEHYY4KF23TL5slAxhlHP
oCy6ZAax/3K7SmOa9q6RX0Y29C2fxTP/PPtg23Qgx5gXuvTTzYCWB16n1d8/dGvdoO7K1ESHjNs2
lGqe51GgN8Xy3dIzALZ5dxhxmbx6PLK+jWEjHrO4CF5TDM/mZWaO2VtHGqFILYHcH4amQjHu19Fh
POEVBBhU2S1c/s/jAi71V7gDgR7iIMIC+F3D2T1r1Lh1rw2AitqKadNlGDsQlBpkUXuPWvuGgNeT
qrtg51gkRdVyT+l0lW1pc4uxVzGzXMYYTUZPs0vQvxQK2T63iUrqyGvX0kvCcQNX0LUHVJ2/T26F
aUvbM9U9BbrZCtbtdBKVr7ctaTndZqU9iyw2sYK6sC4Tzkn862MOsaaqaabNA6Z5c2p2FD6k4XU/
gFXWvlsUXYRNFl7fVBH9lNT84SfP4WtuA/U2JGQ3MrfYd8KzfPPxdNK0t7EzLRQejpWHyXNI9s+4
LShdFY0VNfWEwjQR85lfMvCju5V7bL8yp59hYSKSrHWByN01Y8ZBesBGlOnKaC76mTNq7uqHaNdI
Y7MgGTg2+ZhI7ILUHsiZCd90IbFIYmMT8auhBS6OS03YzuDFya1pR922d+w6tA2tyJrWbhJhNMg6
nJIrmAOAX3XigvGgfR5Hn/Ax8dVzvK8PDu+zPyhAgCXfO5TKg3rStKOXzALQe87uyUKLLLnAv5ZO
ZrX/ZgPvlsi7/eGLJbL4PQScgKyGsXAJ8TCzE9LBIPEiaXQXLUPCt/XnOSZREUSaZaYcgJFMpI22
PxiGvgS3c34nPiparNnAh3oLdFnXd1NHzvOE/cd2kcO4nShNUAPNkAOW/72D2Rl36zoL3IyTJrAe
Uel6uEVlE6QAQ8q770ew0WKaNGSx6Q5Y5k2G1d72Ot1jLByfnC4u17Sn/aIXSBULEEvfwJhUXkGb
VmsPE+YftMLofr6TcHjWuSMMDDbQ/Y37GfqU1rx/bPVRjocg27Aq1NOlt0zK39gf++IpdAIb51OT
PuAMxc40Rz/gZRzsY3ixk8r5Vli47t1qgOoG9QThKcKelnmq6L57Oh54xrWwaXsa0KJBPGrQOjG/
bCLO4yRde9EboZspniNsUIKxlUScNOt1Jja1BbUe3PSK9sNQIu9CydczRCPhyV8qGs7LKbZKOCkd
161r9/1lWfFLqaehbSdYkqRaMvlzPd3PZOqOhGPYOWMme3WnduHVAERhV14OBTwS1Kra4JJY7PR7
6mgiAh+xS8IWiYHEJRtpOLx91f5oqHga//HMbRft99uk0AhpmDSDe6N0lEZHiy+oXObhrWXFocuY
RX6knfCAqlJnY9+jDH5HsfXhmcmzkLW5oICBDAvbeKKHt3vK7DoGGGcewijGPAcog0wWomIFnn2I
GTNQ34P7tSNz04uVFkh3xUjqjOI5WZCWY6HuJNSlr55WNrZ7Lv/zR15tddN6qo4iQrP+6btAbHuE
KQuRKZ0Lk6hJamJaH4X7KCoKqgOSSsXNYFiAqwLvga4AG24CY6Su/L0nLHbjjN4zGG/xcekhcUGk
iHR7eHJGGK7FvjMN/qI4pBjf7+WDUQWftZtPSPVBNOOtt6ZUwsMhvkljwzb9FzEHiK2E2FN2/wUn
Slh9GobtqzC1A99gwt4CkfV6FSKvCFyTtR8CeKulLTNfT5HjG9Vz9ZgqTSA7oorGskFBNWvEYSgn
yLRNB9J5MX8Cb2P6ONwybSTpHtaYobi5LeMzx24xNr/qDyWIyAsiRX+Njfev19uDiMSM6aMEQhZV
fm7Ly2sF7TABp88nAhnK5u9KOfZAcrDOdSQ9w8i5yb3dZADGPPA1NTLMjRUQZyCiMgqB4ljACD+B
JmHewMQF3GXCTjK8JnaSGH+EsX+b28GUmspA0LN4XbUvmK9K1fgyr+aQAqMw1X1Zh3ZraWhtAn78
XWd17E0/k0MboMgsgPskagz7LQjmBf1YXZRElsgCJhfqjwfMT1vNnWiaS7v0RtpPOZr5DLMiO/2u
DO2iUK9QgZv3cottscSrXOf8eXLyL+6WxZvf6cwfS21SqpTFcprtbQGaB/2ZMwAxSlFWLXKxav+P
ELAW6LHg6W7kb6FeqqXFKWotT5PFvBtO40vteARty0moH5Z5kO3rJnrlIeKCsTshPav8T9qSqesS
cfujmtCU4uOctkWQ4CFfaO07tz5C3lY7nPGzb0Y92qI+mEdwTSX2mhB29rUHDWkuMPA/08ygJ5ot
8oEY7aRj/R8tX3IfJ+PaxuIxBhB3MM/8b2T8B5IseN1abW7OjUQsSunyyMqNWAd3gbamuDyD8ANZ
S9MgU3uYFsYsG5RKxNksKeXeU80EeFtlqOLzoWmJ1wz/H2GkFzhqPsOdnre3mvGUYAEgHDTdlLJM
+IZDJYkzft1vbMGsKKIoB4pvoVxdLqp3+5DQ3bQKXt6X5A3pBJKHUm0lZ4P7OiUwUcbyQugDhr4m
GDWvtTMCbfw4l5qcVJqDFLQaqEPlQzeGkFSthBgt2aE24IMlvm9jZSJWngaKEUwA35IYoIBG8aSU
yXGlPgI3j+spj7a3Vd+JEu8Mz+76zp2YbGOXySaCNHiwiCD+/u24UJBhYd1AFHxDmVKUwlpFBylC
ABumhopjadnUs2RTeumvzkTnQ+T0ujHZaClenkAnU+ZFeavHe1nYoPggOV8iYqMfM4sgs511++mV
NJvTVEfnlNg+dQ2mIsAAIgzmPSw642Qt3InKNUjin4sokhudI5HkpncgQbAdO6wgZSEPnz5B0PEi
7MlhPZKkWANo2tuRKY8mOcQiAtksV6rr1fwI8UqoAd74cv4r17B23mmHHalFnbc5QpcAxWUZk4dg
bEcujACR4YQKPAmhWL/q8wFtAxWbeQ1G4rx0i4akGVqi2FnRjj0UKeorFBqDSxeyW0YtVi0Z2eZi
rzYdRTtSJwySgWSofH/2bk3UPKU0zUM5CwX8jFYr1edEgfp/yn4qAcCSdvRLXLyAYMc5OdHV/Iaj
wOch1OC5pPfy/+lKCcUnZDg/AdZ2SfOZ9zgxFdja/jfWDPZwDkQAZtuJd9geV/aIwCbrX5GDfhMY
LvZet963EInnNk1x3Y0asMxwh4KQMHMiB9T6RGyipnYk8SDkPsppJBDUvIjtevH8HuIoGU30XrWN
uVTVd3b6VB1gFFn5L+F2vECk4CqSUtmsXwWtLH1FFse22uLTPggPpkaWpLb4g9aHjLqUg1NIp1jk
9wsc4DP0ayOJZ5dL0FHxs4J+72hx3OEGRj2gv4A2OPVuc+Ds5bZFn8ODAw64eZuDO8QSQc+YlddG
FsG2OiuZRyYq4u1ptlDaR0/UJ6wnxowgsHIjNGWyy4ymwCiOnuQh9OuKffuY6g/MW/RutPUOdd4z
GQaLrswMkWnXSqeizx2ipZUwQL/+MysdQOdAXC3UnsuEsZK5ubgRJ9a3DKzRVP/IGKozvKitaLPI
Vc2N7vsi/MQtKIDV6r+bVh43Bdl6HkPyi96HZv03v5ec/TlMWYc6lb59SFPilqFZEMsY46g3fEbn
1/3CliuDF8NZv2NK+yldAVpVCab0ixenScnjp4vKPOHimKHGYfln+SFrFkydmt2rEKfkgkNsZrfN
wo0xSeEBUV6SvDsKQbhKvX/Gl20NurglWr46hVGOkiekLQCcE/qfgeXLQmzPe7Woi1gqhOt4TaYh
U8s0NId+1M6hIT0BAk8XUzAqBqKHkrlgDNRQnOcNOcOPWPyWtoc8bA+N8yxMW0CPcBfY9c0oCD0e
0KCfpHUqEjwwmt4ideH6pgjkkbNAKemeKuVjiqOk0kCMOeLMZobU8gdfy7coFO98eHkXbirSAtGZ
UaLeGvjYMWXR6+hnAbdXa+8cOTi/gPbrTu1a5L5CLJv19kw1nPXdAcsEyKQMgIuLZMYsVpvQRNkJ
xhi7b/KJk7eDWMb3v/7g8ClqHCbayvxmXFxpDz9tkYase+ojMNxEUs3uRtrO2IgEN8p1IuW7LFuB
lyIdBZXS+o6wfMtadx5Gpxl4w7XNHn2W/dEA5csNCkpKzks3mYGFoY/FwYObxj98POd2a9OFyNTZ
zaiCIlDufk3/OIk8TtuNv1c1jCOf2gF5CmA+ScP8rKtHPvjzdEdoLdxhS+SFi57uxehJoC8IzspB
Zz14tv4/kgJZrNGOT+IBFZ/VMn/4whc3PTbDgHzabM4YtV0q5mmLoeLUcI7/zFdkbz1o49p3k6gF
8fDlzFMnCjlrFX+6F1tuCExT6DFhe2W81tXEgEldo1c2S+0g63XGeavKKHz0s7EVOOXQRn/Sj9EC
mYNcW9Ur3z58AAKU35J4lNDOXzLTqy65rmbvwlCw2fl14Zbcu0VlLfVy87DOK3CyZFByKaMfcD6U
JI5NVs2DQVK97LJwjdJbaT0ZkXO0qWjGprBOdnfz4R+L99LVqqemeiQPyVijq4PzckWoAGiycvJC
JtGMXZ/F13DL/3btHKN+34TUV+pcuuvE9Sc5iGTda6o9AbYdbSyVLHR/wOzbK8KB7SB5/2IxFAN1
rIoBqmTOq06XpYfBOv/6sPTef5dkDsebDYz6agNTM2zIIVa88haL7+u4dKXtC2yCxZDQhdFn9gvG
Hpp/i0K7BaOgJQv3IYLWmJnFFi+1SQ+b1VPod7UZoOT3xIcfBytb7Wdvlg3gVERsMp2HuFKC/LxK
OhmZzzZXMQmpGTLl1oTQjRH7/dAxc0Efdxi09KepKntWtq+xVPJOiKAFSFqWeBlEW2ADJWWSh1qy
ACafYwNAy+pWksuW/ymuqFsT5KJEXv3JC58GPxEBHjFif9M2yI9XSw/VSUQAd4Bjoi9lHR3TPyl6
qUkzH4SsdCGe/9GCR4v7XbvP8LmAgVHurehS8/Lvr4fZg4UgkRvO2F9YEYcjn0CMeSOMHIA4prrI
+TKiPJkTQr9ZGigKvvGirZPK6KhLa88WScbSqvBUIKC2oNqkhoz/b+0Xm+9IPCOap0epl4hWKmG2
pXTvJeizIR/R8gTLPaY0HQWIvWynKWRjc9ZftU94b93RehVWF7xFPRxY8Y8PVFpI5HxwmNkBzmkM
dun91TXnT/8Oz9s8K4oiKsOKNPXyqbImwprkDlJ6gmU8ds4HZeFvLeur3crf9K0Ro3KygX8oFdlM
N+u+a569NaPwSOIjrq/Km5FCAg5jbm9iVB/4+moaWLDgPQ6uAeznwvWsBsLXw6978ncNDjTAj4IX
J+q4KNPUVWQYQghWRvErd0IY/K+p+0W/hQ0IGEglT60YKepoyLYDsKloaKQgYHPrJGtNX/FTEy5m
pB8z3r+jdGUgwFMIY30ltdI5v9Sl+MhsyD1A8ePQtijiMLklj9H7h1C7O6BGJ757Dk5fDSwkCWhk
8WtWrQZsoo4XBnMBntaidGvj8zDknQV+wVr5w8LVv/5YC9Yl6F4At/Fy4YI/G5F27QeK/IU4L0zc
a73cZDyrRBAEJb1jMfU8Qorg6nJBl2AJ3BojUZ7vSWlwD2KAwYLRdx9/e6OkhRQFEbc2YyxaOLAv
rMorBaH4MjXwvaq4DkhOMSpnaIL7SUe6/wfSigK9Nh9lSvsEK1Hj4Vrqa7nuiFh4TYe2UnWVOFKv
GZiPiuCOjyI6QwYG+zgjNyoFtccainEPKagFvIhukefb+naSZypReym/rTJF4UwY4izG9ZEII0ru
93keGx77a2++thonSwZHmFlilgA1lHK5szizvs+YJHCs53hPyHwlRZBq4XQZb9qn5DYq3oW5/TPt
450cDaldR6GeTvVrMVpyKd2zLbWrukNPcOveUukQ6wb4wbi41jupNhPKhrRxgRHHMMdyXLeMNX6c
olfuXaaRuxX/vXAaxPqORJFaLZD66V6aQRM2Cn/hE6JgEEDqlbHz3DL0yolQquJH6SKnzoPUz9Ni
lyyynZzoOjujOAlWst/Xo+vwYmQIIxrjPRqgVnFgOZgZQjorYl7vRv7YCWzJtnCEjr9Tl4PQIIy1
tNr1QTGqRXRG3/Dz7dbjKk29azejOva/zhYoTFZ0yLoITs0ShDQrOciydkm7NyJbDXr7d09sSTmH
NjamitUKii4c7LP4Y/vnQjXOkyL1H3psaNYU5ks0NjzICjhyZteiji2NiD8r7FZm0N35VV9DK/eQ
ukvdH/NtaOzpRAfSbNMKNuTM7oaZ23NizTO0x0MShAyfM7AW6/IfJRN3So4QoOiYbJY2dLpsFZHQ
Rps5RnDUjPPUlQ7xB8fohkvYR/ng/NmoMUaEtYLH+xqHlBful9i+50h7+xRPF4dNiBYxMuZy9y8c
+LsH6aGnKL3qN5CzBqq1YVu+ouXKOYxhNcPAoVa+pd9vOEStWmwyz45Ur60IBVxP30aX1HuH2l7w
lfLXu2ZBMAMy3Lb9Pea4lZFLfSAIX51iIxXbkxBDA389oLb2ZqeD1vIr6CEp4HeDTznU6Kh7Zt90
uTgtyPWJvxUYzC2z+gVPq3g2dOiHRaKCOVtfFzqvkGqu3Exw6fF/cgdgiqpe+/lEatbUd2d5p6BV
+Eg3L0CAMsbsBfis75hMDy7WPkLO/bykh8VwOiUTo/9sffEgFGVcmZBVPWBn3XJUudxVjbusOvIm
nqNM2URBNwpbGTKjmcWK7F3h9CQW+VdqLgahZ3tnKluVQb73wqJqP1NF8Ok8gBzud6v2KlpmfdRm
WjBnC9suhKg1YywlXAlHW2nugpL7gd6PMqHeqxr5t7VYpy/7I8ZySHcuaQKr6O6LcKLiDQ2mxU2w
3S4ExgOPO6NjnkxOVE2IqgJC1J6Fjms/UcJoHdLaxwMIyjGF47+V7t2YNxU2XmZrRzRTJuj1DkGB
sv44jg9h8Mx+44pr5Z0PM1J3Pg2WXyL2BevHNZktu/0ejiCfYbRTbtGT4OzWzGHXK0iYZpfUac1q
ybFfXlr3qo/hmUWNPzEdjUwuauMvQtVKun3lJTUFM77ePwz22TTNBciwiUfbLFt3ga7NaW+Q2a+N
lKB6oQvHkWWYMU7w5Cr7dngYxJQQWYZtTHzdYJpVevqdmX32lSchiika90jVBnbI2S95PmlNVIZy
uES/3apfRakvbssUANi/kbTuJJfhoJUswt+aKHX34ej/gBXbjlChe3u8nJPpyKFejo+BBI+afFqc
vLuQri4xbmT8HRLOrCyog1mct0aDHFapkz1oJKA4gFykVkv5/o6q27Y5O/v/GGeA6SIQ3iTGLJ6U
pfppug7SC7V5jz5iNm1S/W1Cgln1MtxCG5eqYX2qbDHmPx300WmNsuSOAXLrhhzvVVSNvlPCBpcu
W6mC6fOEVx4NqUXl/xns1/SGl0ajxKpY9AvMmzh7PwkNqusdZhlTUWAmAAuPOPWIcPKdFSpr92mq
LzICE6zWiYn7Gj04hPi+QxQhxPyOpCs3IzTh+vm8ezMIBpaEJuG96iMezCv/qRHD0nBD+DoVgWxI
VSMC6S9atJeVKS40qeBedoZiY2gme3tvFB6eUuL20PAk9KzSaoc0/YiTnEerSkx+7ru81Eg6kb/F
VYZ/hSXuUSCTbb4AGDg49AWJ2wyHovibQkx4Efg9DgTT07XV3S2TGc6XAfXZ5PJUJrFqktxX7HJY
f/PcstflPC3poC6V/+NwFzQ6MYJ677mPe2lYjJowHHkpqsgsPvtyDTmrmSSccTJglkwqrIPYoQNQ
zUs0JCa+DkiaEcoe3qaEUmnAiNUIOb4llyFw6rKJGhe788CKAsKUSXDqFXJUNmh3s9Z+E7Emo/LG
8snw26XCKxG+Fr4WXpQPhjpfYPHqWqSLSqYph/p7G9q78lMxWDjSPprEaA6eI4FoGdBc8CzZcdoS
1RXTZ+VVOwguo2HBSJ2Rfzb6Ic1WGJbbEegYJlHVllnMnO234RCJp1Rcr5PgfRNjxFD1AcFXgCUj
a/MEiENLs7bDrYb0d04GJZJtD/731EKjjwcDSNfLohppCwkSvpiikdFr6gppwL8Oef0cg50V6pDS
jiqFKDmJnKwE2e/U0VILoKWVrD6pZ6AmEEpiLcDjRRzEdZnQ12+rP2sAaVqqoUVg+G9akYtghH7V
wjwMxMdveRL6icc76wc5+T4bEIYvMNAITt/HxYzyp3izPMKtpXqusqHsO2taah29WKoL+J9vqMFZ
W2NJ9RsRY3owxS3QbxFd696TcS2J/waJ/0Zc+2ywdMqAP42tf+OFMOv1YVyfz5hhWeYUAc0mUwqk
f/vGiXlfSmiwHcGhyP9ergedYVUQuP0y3+ehmGUA3xos71JwoImKjWaAWrHRsPEJtHA5pducwZ0O
jjmXCllOv7WKN6MDNjRaVG99yS9pzqzHlxz7/8p1aJvXwaFka0BIonNuX7v/HjIPSosTps+Mk2dZ
BwfkUKoaYa/HA/qND8TFMZxoJgd4Usl8hVzjlazUdaYbERPQnKU5LnuYA8JmD4kVBU5uiCETftEj
rHKyuO+m4zk1qp8wBIULuaHAirVzho7sfErAAiVHI8Q0YIKIb+7PKRc0sVy4K41l7kq4HikKbpYh
edWUmzdLPpuYc6AZOYUpvj8jImuoQJu+foQR43jNNrbHY6acSQjt+Wv3NLPuHvSUzL7fN6TAZn0j
+QWBh7YtxMNnAbeQGCW+rBMMiza7d+zJrPh88+FZOA8M2CgZOofJAswmlkPiT4e408OimldYBvjk
TtEOKReRExV7BiHh64RxXNoFLu6H1TDVQLaKBJHWmY9ZCCE8eOo2iT/GSINselRJ48OhH1HGjrbr
cYHf6t1A8Phf0hBSHDU216V0TSVSLU7wvnsb6Anj4hd/CoE0PZESGbk5HQMg+tSdCRRxT/PGzDG4
MOD+Ba3C/HGGsQwBtIiGWAm/A++9jdXy6zCxlQdKm+ib3uxGdbc87dQBEjra6thbwQwEM8DiQunj
3uSHE8YbIiY/cXwymr/ClJYGxwlK1810YZGkR/w8Hbov01XTAMZEt8UjBEnLOdxWZAOaPDnRjYKv
BM4Cf8U3CzcSi9mbOvosY6rc2o8/PnESpPeW1f5I8qtJoUgenmjSWgV9rWc3mihiSp4Ld3Wkum8b
PUWuZNB9M6QsAyHQTWJNDm3+q2BBNfJlJEqQtIcr2Pkf2Y0cSKVP9BSiIwoZa4l8VHiGAHN2PM73
970abZ5H7pOoOxSzcunWUUE+04AI/sawuvv/Eizz5zeD/5P5L/gI19jieZSgbwvReLvr6S2B7tlJ
HMpEpbdPwqOD1A/g6v2GXNo/EvkNjE9CUxAhvErFYODFX1fy60j2ap3/xg6SuIMSaCs5LkBE5dPY
7e+If6q00o/j81o0YJtO/D2Qb3SkfX+XeMcCeIrkB1XdLSesZy9Qa0//rCeRkLXy8WpL/WvkQIPO
ULjpUiwc6+VIr6PXDqAwnryOjLbWT5rNTfZV3Zzl3XEM5m+38vRxM/iN9Wo9uuKulMip5gzNxi4L
S70bzvnB4cy0tXDEYQn9BQEQE3ebGWxtRofU7axaTJHt2yEOn3t/AxbbvebRmmy+zdZARJ3FHaAR
+6yZknO76CinQcFcA7LD6kYqtu538NCi+DJHP4rdnjEs62nq4Zsk7tOlIUMrvFUtq1RGjMeWIb0y
rfMziY2ugpaVzryS7dVzJ4ZOCTg9Oh4xvFpjFdBATCVJIVEd6D4FPS/NADQFJ2QM63xixP8X1ipA
UyzHzJOxZ8osJWtqpfPszAkqCeiejZxxLMHki2/i8MUSCAq6xcslMDM14c0eIS3kLGHOqZZrNnMW
OpRboInuoYEBWcGaSR/2IvQs8CnNwgr0qSM1X67HI+kwgwKRSDboVaxeBcUw5TLs1uPUFUsDu4Wp
oBi/lISycKv0Trp104MhzXXtOUgO1zpkhTS+obTMtxRowLEyC0+5ynTdyNJ4CqRRN7XkKfFSuv+9
taYyeHwjDJz5LoI5e4LtZVeAG/o+P1zZA8gURSbc5TpRE3wuGeqM5LpZU0CBfQ/kJ5QdEYmccLDl
VIpOgSPiGB9/carDnDAKi/KkC9yihyKd7dSgluPnZr9KQFB/xPruKwvxsRXAxyAQa7nEFxk/KxML
jwarUxZKE4QrwFBt2m7lZ3zbtvrzYLHQmWgM32SoPgRWgrd0ZT8Lan7pEnsysHMMCmK0jUydgmKT
V6k3J2NuAvn225uDoNyaYOORBASexEKACxe7D+n9Jbr1KjhnrGgZNZ/hzW/byfsr36uUKsY2DsaF
JE+M9o3yEPlGICDvTQsX5l9qS03paSA9i+8HEzy2nfemRpzzIYPL3ROPzSY6xuzBcYZ+p488kFqw
Wpqv+AO1U2sIVHflhvdYsF2rpi2n42nXrIguFoohqgB2e8bVx9tvN5Kd2v+IC6k9+Gsai3HmlgK7
JmD17zHldAl9WUucUpMfYRzAreClCOYtLW0bcqsSazrwzOqv0zNdITyDPT9yrjxUnnODYxqQYMMw
n2P2MBpDUsOv1gJUfQ3JY79UK2LHRUEP4WDEVc518QJDPo4fSH+vAknO4surRKl8Xr1v6A1CpywI
GdVJw0kNPEh65hwytFXWdsjH0jUT3HLKQ0JL74YUHsU1ZPm96CAokxBj+Sp7BKlP4zZ82707OQpz
efLxas7/eBpxHVpYWeTjlEgGX3SBB8B8X6EHYYcoCzHmaFok9jpNUNvaFFvJDvBdemDa5m1hmkwT
YcGHYTA3AbUmrau4u6hLPKNUhE8rtVVOxr66i879/i0zI+zCMID7SrC3MxuFnJvo5cP9lVliH2PK
6rxJEzBIZidmYUN1xpg/HHnCAi4wD32zERkZf/2+U3wE6+E5zp0fajRhynSa4lsbhImVnpgh74UA
TaxD0eu0s1M82VX9O4ztCfrOracloCmumjTHx7La6cQ964y236fOcEgcbg+QrfF9E7VsZ3MZ/qtU
tzgKVbFrOpzZZUVV0kFLWeGrCgKi6iskn9xCnzRsT9jJCgq10yl2Nxj98aJ7R+JIwAh+WHCv3gGV
P0n8U0AzXn9rrHoQXEMNrNvk+jJjH6PdsUCoPUYJ/62u07EjEnLBTQAL9x7YzO3aD22ZvpWBSokY
hPmPFJVAM7sdmJx7cJ0+f1qlPVNSvQPd3w8gnVnip39yHEuLNYkVADzoIwxENLwugUTzK8SGw4PV
6IC7iyQgs1HPz9D+euWmBXbfjQgHF91pegm6QtbSMYyf7O1wPPatRMbc3s+RnvQwezmgDz0o1PHg
qRixnyZsbMejqsN7yk0v4EsXmDtq4ay5ErpuHA/D2Y0qURikTqIdCr/vv9JllJ/Y5mw8VaOaYyZS
N7bwIx0Um8Lsshk5B03/Q4IeXRzcY5XPIuPf7ctKRsozccXRsdFB4kpQKo2ChmPZD6mmgAB1SE16
lsqzvNbwzO+3J3dfz7UYo9EWEmRQMFrB3uwF7cKD/TQyFxmRrltcwiHgAG3n+3vzIxVWsoDbvtuu
hjAV5R9vtQ/a7B/YBcIsEzPBTVi4rCIhH2rz6ZphU/ADgKtPuz9Oi0c8UG9joN5b3jMwSM/jQwu0
aGYJLEb5YMglQIq0mpjsFpeclcVcAmEoZBm5NsGbfNyQxOxol/RAC0e/iCCfhzRVZ/L8jHS5qh02
55zeWWZBh7FLAjeLlCZgutLXpsX378yqnSqoY/RIiogiKu5Rk4jGvYejHQoVRB9BbY+wImnUewBj
5Aqg7chbftVZbe3ueJtFCarv3JlBpgeKsd4YaxZ9ZAjvbVoq+T8qoYue39qzgSUel8UQR2SKdYZ1
i3LjMctBd7/iG8vGnKBZAAhdzFAI1gSojhe6KnKrXr6QfC0VLjXoqdFF1qLH2D/okajQrs3/zKRp
pNeyqd4ZmQkcVvbIMP2O6UIuogFlSpnU+ytX1smJUJ0YeqFbcNONTnqlnyJbFstUD5gSv0Nl9NP5
IhpfWG12k7l98gXailjZZyMqv+8EvsrtRTWGwc8bYi1HovCk56V7GzLRh6yIYWPgdRfwc0i5nRXR
tE03y5dc2u4SyeFKaMHLlbeFJnn4zrIGB5TMqWLqzYNHYyC0JZTCxKFSzvGAeOQSPhNZGJ0HMSvl
yRQs101vvwAxRpO8QQs8RvwxYbLVBrCd9g9beqJNBuDxx8a1oQcEykNJygVusw31gpCiDZhNPgyV
nCb4jFkZgbmbxHQNkDSQI3jrTW6OU257n86fXeys/ztsrPPrXIgaHqAOF3LLTD0hxpyKSBNfh7Gm
cq6tPpchxlfwHytuJFmvY+IAdOkRcJ5zBOT08dXbi1B36jclhn4Z21/KPhuvb2wf08qO6HRp63ry
hzuHaPTCTEBbac6x9imqJvRUFY0nUPYdplMgob3CmYuwHqzqu439V63ydr2sxGN28lwlzsWzPIPv
F9pkTxUsurogOYbSxZtwNiF6NayI7K50Z1lpAowIXKCblFxGKrRbs90snPsOBxvRs8UABloPqLHI
joX58xUSZ6YZ+JniZ6aBkCvXc3Ya/Gba5JhcsPjZnvCnojQkEy64o8ojzbOMtYqOVEdAwZI9cmyH
9EkLDiq+/qPgItOQwnfpjnQjny2uPB1KVXyXSbkGmDYB0x31zPUfnUDFNAdENfqjDRaJQ3LeQy+P
qxz6OKhtcPKnrVxrJhS9u86ToDXMEcVO+F1HyqcpxcfuF1KJRTOAJ4fusMrdxByH0hfS8qLKaedG
3ad5lIMh1Fqj9DmIPLF00fQmLFfBBBB0gRzefNEJEd67g3uGJYXzXV81ybG2wxou2gqYb0r/0CHE
oMUv3rjvomDBEbDOIWKMNQ12ycAKCbUhLOropD2MzdYnq6sc5+vQqYCitn5jcXT/gg42v1njJ6zj
qBZ0nSRq5LOlLMwC27GZjAdrsMzHONfb2pU2rye4r3L8itoueGf5G2gEJbKFHtRpRVfWBnZGSRyi
LBc+VGGX+QUedBdZPxD5y3uhDeR+SqLh/wuRUgUXEtvcxEfuz+gGWUUESoX8WDCSzlgXj5xSy4Mb
GQhT28LF8PUfXO2XqCmtVHYQ7viTT3bXqscfoZNvWJdt0bLKcaQsGz7RnsOkwTTf4DmKWXed2a4H
yVGJ3N5sExC5EcLGlohFEteL2n+EnDgIKELT/sBcT1X/wuvfiEdAS5afFXKomtUt3GHhr8lIiAV8
B76SjG82nKQ4beptBOaws3bMZQW0tl0W9EdOae5d79ITEqVFSwusfHC01fnZc1CZJOw0Rd1VSnjY
xocvmUp6vpqhcGDm0sr7uxdIvAz8ZiUKb3Kl9sslBbiQMIpYlZRkJiD6MN23El005A+jBoBBW+rH
KXJGVTqnyLYdlplgfoFEhgLBg7Ey0MACbxvugHBGE2bCHsVtJ4cj24Lf/NpuSjKvIpcJm5n7CgT6
RD6SxTqQp+e9Ur/g/y/ikTt7uPvgmVUv3vZcFXXy75ENm/5JsDg95c2lhjdi0MUFcKxEeXjoxUpD
e1mRU0Gz7/wriK4j4ceKKRWlb0KI16WpGuL/PYjDTyXXhHmKVd8BLbbt77ZsewMv57KWZoI3n/vB
a7VVRanWOuq28Ry2JsHopkEOkoDJluxZj1cpVEotoU2go6w2CbI5SNZ1N1Z5JOAxMVyceYxHgaee
UUxikuDBYQG7xCk/ZDaH6cplIU301llxnP2+rPMVPqrluq4Maijvfo7/XGGy5zmz0IGyDFzE1hU5
Fpbz3UPEc95ZtrWe5p1znZs490HILWihr2UBpJyI6Fa1JYIDkXwX4LrLuXjtrQxdznAsktRaIupY
TtdC+ntU3ROSTieGa/aLlWLGm5JeQKKAbYwcvaf4jvgSN/3r5QLPh6k1EHV2RIYCbgaZCbXh11Qp
5f/2UPSJU1tEa2yR9VswTAMwk0Jzaz9qtfRtu4FcXGcjhzVLrUQUNO3PugeB0Y4/RCK9C1iHRSUI
LOxicgJeRqI3Nlrvvag+ZVLzoL8n5MOCPvfGLBQpkVSfn4ou2+4QaLZpcNWj1WW3/pE8VznLag6S
uS/QH9R9HqzPOXZ54nzQcqWnhx73RJzClVmsyqkqT+dcainmubTuIxSQBlpLKjoLhU2hiFq4KXUq
30u6TueveAk4PZZ9v5JnVCHVinv0O3EaMHTLvNUoGH6OtTuKMnoOQJCszh5h8Bv8CBHB4cBPyrcs
/4WfHqBeSt2gXax60DySURwe1lubk1qiLyAA9pXg3CioWf5YWVoALMt9FMTUta8q61PzY+o6S0Qq
aPsi2uGdXVpjqgWciGZcuqMEZ53UHK1Uvd8UXXeLE9uNUxrCY6FcU1upPg6QeuAcGMsY56qrDp0e
YiPmaHRBRI3d53em+pMEUBP4cYvEICzr2Wp47TrxR971SegOaYNNxpwk1QWI7pr4R++cO3ntKaiT
h3RJdvboVYvq2B/c3EO2GXrBpS7nYzcXy5aGvYqQwpsqEW1b9tTHZRHq9iHgX17tlkaNh1NyQ6es
li4pNQxHerVqUCVs5XZIJoNQYE8cUtjVy4eR8EeJs2amXlZJUjHGq3iVll62Nxre1ZYfm0UJoaB3
yk/kxSkpCOnIuJFx6fgpYVn9fO2+sGnWc0Ym0Exh/WANMcbK0dJYKhSWpJkV0TusHrc2Ia3hRbg8
aARgnBM13qzGGwy0d1OzFII5Hj8OjiVFn5tG0o7iwXtTOscyuSVOAcS0kbh7nlTsaOr37bH3cHKQ
DE1+nMpoZni/ANsdplLhMwgZZEfvKf8ED1y5X7Q+unzvennU9vhFHzhSB16za3Sl3qQMtHeztEu6
Y0W9Lni98PixYfS85tm2NxZUlI2RH7dl0/Nh4tgbz5eiDckhJPdfdmpmr9+I4/q6SSLoaCJ6YuvQ
lsusastDSNbX5twAGJAvMpNKSgPvj2zEAzcBlC1PMtov9L6llIR+03F/sAdw0viwZmkJgCUV/9/d
bL/eTa+P/X+cuzYJhkTEAifso64Md2l9TItnq/QSdRMDPi3z0UNlzwMydnjOOct1ab6xjlWPw9nD
rBUD5wYlHis1l0wu9tuc7wdh0V/Ml7cudutSc2tzauMFITVsoTR+aLuP4oBoIRejmT+ZiVQwpKG4
vsTvr5TyZ4JQLLQ8oaTUi311kc+7BhRs9jgNOtS4caEvBAyegnN10S2eD1GXD3nJiTEMhU1y5TEu
4pt9l5uRU7xFyDsxyw4qMdd3iuEIBdILblpY3QUX7DNeQQZigqSn791bkrOPd6tpv52/2enZ4Xzp
glnh7Wcn6to99KeO1Y1XT+1tcKiwocwIBsxVEjnRxOQA4De6VMoWTGeMs5DGcYiTeQ1+DBI6bdh5
gQXEDUWl10tDQNlKg9rHB0aB3boxQ8MtnkRJFFcHpq8MOk2ToXMlIwMkVH04c6zeiebQ4BYS7dEm
tASCrZVD5aIp8zbOlubWZ/p4XBtrtqOY9M8448b5xTIaQh5SHF6+2daJdQlrY26StdVoqKP47vyO
1c95/egD3T2O55GQZkgqafDzm/aXEselD5oE0ng7lpaJdNMvituJC11fzG1cSag+whA/LQIBuQGy
/n6a34soTKNQwkCWWIoKz7pq1OQAmpBdIpVjU87Uip0osFUGVLBXkXP9W7+S6RfEMShEegAoRnPQ
evTnyxMjD8FV67w6rjAF1T6fvjLhDhYtUGy+puVHAB4lPjCYeXYfzAgxUSkpk70pqcUGkvslIUzd
MYMw7PU5twf9SmMwVqkKRH1yfjU0YzNVWlZlZ10MVQGiGWyEM0RhilTWUYXYgbEZvMXGO1KE4Rw8
6KsFN45KOce8EkUV/ToOdEt9zPaWgJ7D/lOv8yMZ5zbljANRVXbzi7zRmHv6cRTw6ho/SV9SfmhO
Ud1+fghDI5bRoJGwkPu96OMLbdN6eoR4YC5dFaDM10VZxFrOG20jtYiMQqjXMK1b1r3OiiH/D6CO
FccMynrXp/iH+wSQVVCYiZWoVRtxUpXoj1amPxdBXOLanpiGu6dXAqOlLo1U4gC5I2XmKSpzEcHN
7aQiDNEqXP2hxj2Y1OeCdEMgVnzk9KAogEaSw3JjGyD4DB5JnzxY7GlT2z/Ntv2h9XoBkPwC/hSi
g0bPCkLRXX1PzF0Rd23jCDT9ZZxmCyccjEO2EyERVXZBcjvoa3/KVt/Mvc34axYV3+DHitzNdGKj
SBaJEKrov1t0T0RXldrx50eFZ9YapSLOWO4955R1x0HoMT4vHwBnbiQVivpiKv9tgNcgnlX4GmSs
IsZ0ajB/pa7zIHiVlYf5UypxmgSsD2TVlFSd9VKjJly/5vr5M11Was1nHF8Xp+y38pgDAKj6D0OS
VsXMBr1p2ajSuXpNYdneu0x/RiVIFo9paTfXeRsDKn8bcKbgKyjk6oYVrkkgirStsHPmpLMFWnvt
5DtxQIepwispFSHwC4Ckz9XtoJHD0Qc9XUcHV4B+wBjJ/a4fMSI994+caZdn8t2a/Y7WXmcQSQAX
tE/PHz8cAlZDXt6IQFWrKCCfWsKC6nXFGPg0ntT9Z+P8omWI9V71UnuOOBTCd3RM+P68BxYJfgrZ
mUlKQzvOLWiIn0Caj/4I1Red/5OxPNf35mIl8x6GSwKYa5GlmdEhIma8vOD6xYvjsYy447fbD1qh
+Yk+/KAhCTw7njnAxSXqwMN3faDXzhPRtGTD67OntExi0d3ZaX/h63YjRWTrg7a5ST2b3aPT8M89
BR0jJ4vHH37ECLVF2I2uIYWyQu/8WLCpZQHtKKdVEU34mL+n9HNYwUhvu+8bo5xA7VSxsVuloQDI
pwsHUNpwiAkYbPqQnCNYb6qLYFLt2La/iX7LEnXZQUjrXfVXyUtY1lOVqsxR3smkc3gmGPOE3vUt
Zl+oq1NiRRkzEKN6teNqo8xpwQVACcqO24DhufJxc4l2EiKUXaZ3N1rgfkNpZdI/oFaoSok8IrEI
DtVyLw52giQtR3B9uIq+j1fBv3IyCW35IJck842vcfMb9V33hwopCMV/zXPi9rFhn+60MhdHnvKo
5C9Cc0y9M8bkkl0FIupTiNkqm/fACbAbMRUn94m+oblHf4c6V7ULeMZfDkkw9RaMOiNokllP4H34
/SYnL4vDvlp/DOVvWG3OKHRy6qVBrnUAV1EoRHALLOGsuWA4DESjYnjXd1CES3IdLMzxnuCdVaXT
iERU+269fzAfIZ1mjVCewVzYmFAXcP66OSmMyfc9ehvotbRrjLelMsEd8CvQlXQ9BG1fUI12vJhS
TK0n+N1CiDqRjDx6sEcRmf9M2EB370DlOPdcry8WQVGykZt2kLV/fNdSPeqprgpE5H3DFMYraMlx
dCbhe8c2SjBwFSsfEWcsQju70Gp3S2ZF1vbpOl/VovLvRY33ypR7YYUNvP4D/K413fie2b7gCQKA
23I6XqE7SlRH97UeSazzuM2f+9mdkpKBiRUqfVRoXhdHCha0cSLLGYD41DMbfel9VXGZxtJ/NOQf
CuVjvNv2uU1pVv3qjPEa8et2cqwST+GTvv3RXh4utUE+vSmJq3GoaRb/Bk6/DXoC9Evk4/XnY6I+
AKzoCgn7fAzno77gWmbDYtdHVW8uv9N5+JtE903ZpGc/5Su9mBftwJAsroOYCvPAm/gDePlBIdSX
81DWbmUqI4+U5voQUOkbfpHrllUxoY/KEH64XIEbRWu24wk2hKAy92Wlg0ySc8naz8nQ/UjeP0tS
yYYRU8U6JExTRx8glmcSeinvNp84lMIXk+lkbMZwvI4x5YhSxONZsQoNkb3Ujj2VyAyswW93bun2
0hew//gtiPMaR/63w6r7IDMSCC4zQyLoekGpa0PXaHcWKn7GOpgpYKn2YeGlg5vTOFS7o748oQ6d
Q5jpUXGdOhqdUT/dVPPPY/P1ANabEZjiX9i4iLeCZqnovcmJKqwNauYjq2HQNpLmPaMPgZ4QYeiZ
zlAGoVW1zxOi8lElKg6++cXhO7hUP0j53rtCwjyNPccCoLUsqRfd2CzLQe32414ac533rZvmfqxw
QDQ6C2wf5HrEy9140Vf9rWnedFMwZ/9UBicf7tUaIGi5sTgoOo3aVhE9M4XMzkHjzRelEcWjxych
HlyTzlB2KH/OgCrhGqpuKzLNB1zxkxPOjFzRTv+qxg2h8AY4qusxMPtTDXlSEgDq71BEgv52XRZd
gtlCuN0vFR4QVsTe0GpTjbQKJhKXUgAylwqyiwnyODInHIzhA/dCCep5SJRhjuh/pfKFPs34P2fQ
7J13bDqlRUMBZ12Xl6ufBZfknzG7k3eBBmj0Yhig2Qcend8bLcQ2/J0HhQ1PoYduxB1dKa5SuxWA
xW/8IzyOuuDTvOQITO1vf++y77LDzgVzPtCavV1bvHfq0bSsvuDM3+B1EPNRuPBJW7FVA/X8ZoPz
xYECUxA37Nm0mN7klvrtb30S7cRxsLvdnwNzqL47AFqD1Z86uX9uabowEDlw0EeYibMQekqtFsST
uuLbm/h7MaGi7WnJplMZl/7dOW9gfCcsxs/s7ODouWLa8A5v5x5sJuIHmwcwzdsfBu6JpAQ+0TfB
aaIRWZxyCJA9xv6ch+Apl/DRnrpRoSpeKmfExe4AYq18Pdq5kvRcEBhQ+fBHyjls0vB3yhWE3Uxw
OPysALXoVOB4luQdCBIByKNeD7uH2Xc/cR3iO0d+yeIGC9lhs2Z63pdwyAm5BseiTDEkfAAxSSGH
bqGtdplISk1uh39H5pJ6/oUL53/zN7VW/7nq3tVJCvJ+2hbpq4CWhv+GCNkuj9Pl5xmj6ogEZV/A
jJRAHBiDRLTenwcQzrLMRckeVL/NQzqk95KrPkQotlXhSxNqhORl9fZMPVuyn98j2ePcsop6lSzo
Snxw3w/b92/ZSMH6CTzl+3IafVr69dk2yopzod0oHskiFFibEiJS/1QzI3buR+TADmRWDBVTJ97q
B7CUcyy4CsE30jr66Dir/mEXB48EXQ7CbBQ1JX7pjNuNvoPcICdKDsldZzBEQgaI/KbSRqxL+J8R
WZ+kkNhwDVdnB9xwUkFdlkVZ9e7JmBOOO7i4PfU9Yn3bbLoLLbNHs/GWRmNwrFjhl/rfNXpM95/z
KJju0SetC8cD7L40E7IOkS5b07v8SC/9NNCwMqdJMWgbTnmfpAOrHdRpCtUe9pfKMs5DtZQjdWeY
BXng+KTf9R40krUkUhPpYETTnG7+vKJebtkVK1iciArfMXJVc65wZxo2m7xcfm/pzr6Yk72KkZhU
o6k88jWxfhvmJhKP6VymoPIWl1BTMqEG/J8pbR3dOZVgiuo8DK9jUATJXwxSAJhsimp2d949eGIW
+9Ye0OvvAht9osht+t2RMabRf/FVne5met36S66EHgrpem8X5bAVbON8jCVtjAiXz/tfYVo8Ojed
zziw56tDVhk6b7RO34xx2WATZ7xWRzAmHR7YwH5lUFDJhdb+v+QqNnD69Xbrj4CLBuMjsYcqpnwY
csuxmhPaSVU1+wBvQaBpnT7wvpQLh++/ueGqh+F4Jfbh77PGzB1rjQ6gYKFzXn5svFsAapp1m5te
/tgHOuf4t8+Ns/EXWArxQes9jyhotTXiPkFCHiZ7oepREMiuexGPFlNzfXu9ydtLZAmPQ5wyDTyP
/Z9KXkOq1yel09dFLCPx7qo0zmgAYeGVZz9Z0FMnSOdaobCgmpnW1HJaTM+iKvwL9yNzmfYXvTbK
+ZAPgUlY6SaU/Gv1GwqxkJukFrBd7X5eX6SgeYbDDlMFO888ZosaPxSGLd+XQ2Te9eZjfmujihtD
mRlzbJr2dHNtdBfXpA3VnetLAvUrb9wQ/Y72LZb8U9wDTGav9mr2WefhfW5ylNk72J7upZkptlHd
FlQrmWw99iIes+f7qHathJQqWMBAWTibYacJdVpVsyZnRzNpELghQMrdPUCr441LYFKW2O82yDzy
huiE8Cuh2g9z+uVbkpfTwY4fvRQZxzTs1skOj1EQO+PTmgtcTn/6aPA1Iokxq7LLrpOcLvpGjfTt
E6WTQJbsdSGQnQy4rbzNu18bxmSpkjmui6JnquOHguuBhNMDZ1nrWFvSa6ce7spPXw3Kp+rTbvvz
Te11Ro5/gpyztYrWTgqWr7/ut5huApEtHUIFOvyM9OxxUHBpEWocvFlhXmh2B8wvN8v0ly7ttys5
C9cbMVKWhNUReM2WSSAzj4Do4S6bum/NT+IE8h5CBvIZE1DfZrjVBMBN99xJ3LxZfmU63KQcacrv
DKJuDIGYt/7+2vO6BmmIPOTllJrZGq1rsJP9FRVi5lb6RxF7lQVl1wHzy6Ln8MbTV84rT6WJXkwj
ofMChzlJpSrfYxCoA9nCo9FTUXX/5TU5DbT81pFupNGAlomtmwh6lqzSWRmaNBKrU0eEmh2SYctT
oE/Hw2yw2PJngfIRBeS4SVnik2f3ryXqHSd1d3Gtd04BZTN+clzaoih2WZil7NBx+SYpzE2WnrXk
OJSNlq2xgNKZeawrgrsTPvSTRDgbmy0OsQcOnJgxR6cBBYcYGt8+b29g+XzX1F2uoR2dqhxl7pbN
MPxS2nUf7B/v1SRFkQZqmKGyLGnGHwWANsm4zGKTdd1ZSfOnDPr4wuAsYohHL3vEXNqJCIgCoYe1
ju7/pYgzxY2Dqm1tKqt4N+qgOfAZ4OHI81mnUJjmqrUncHHTyrHHxIomhB9HSlq6h7TwQilu/E2J
aO8BryWE5PnDUj23Sa3M5t9f6N3UHCr2h2GmRqa4oFbfCmSPCxB8hTaqoyxF8B9dWqIP15Ykrk47
2Ogu9U+rIKTONSkbOnAmw+BaUk2fgJbnuZbdGqxa0q+vWkBbDOhR/+znNJg5gBtMl+wcHeaM0bem
pRbNszbEK8zytqATeisz2PWLHZXqJ16nBIiXAXjJP7CublCGXGbWcozhrQkpfb7NVAcuW8K8bfml
4DacwmQiP5sYq48pCws2xaFDxNMkwc23JUfCwJQvFlDDfi5aGbMqPphvddjkcqSSGxtH9nvthoCV
FF2lJneB7HVF5WB3X4FA2E1CyNFgJafwEk0yafXS6Q74EuftxlrdBcIwlyrAgKizE2Eotc7+uK2v
QSe48EMWhh4F0ruZsUjJRbHPSTchYjOdznWhj6hL7oZebDxEL98cQgYrkOCclAHAlbUB/w7iHzEQ
0zBqne3ZA78/Kwla8ZEzHvY5S4tx5h55cXNrd6erRN9AjWbjg6fhdc7Fw/h/PoV0HUOGTfskPo0n
sgj8he62eNA2nZmiS+VXG2/NLPId4F0lJSdpyn8OoHO81A2mL0a4lJR2tAmBodNDDYTa9WyBQdHw
3oFFfusTJDPYhKVi0Es2pF9vlcJsDMbLFrRsqSdXxIa3vxtqU4GILVxZfs2fdwzMAVV7Xu27gtWm
jUWwlbS/RH0SI7GUVOWtjVn3vHYDlHXbYZDuwHIhejF3TIHE6OwZdHEN/6w6WD0fWka7IKVvlP+l
ztHEQPT5u7cWMLyoDefCsM4n/NHKlCv4BOqSPLDDgs+//AtM6ja4976+5nO3sEEx3dqjhFLgKeNa
sCsN7oYhll7DPtrmgJeQUNJrkfdqBo/B740buorCxZmMje8KOnDggAfkh8ZPGg33CdJ2NfVPvCTP
9h6elGECHywezaa0Fg0LXWgFCWzbziiZnI1Kz8wa6gmhF8dmfSW20cs1Ae3t2HIXqyU/pszbcO86
WQoaDD6yeA3iMO6eQXsb/34g8Bg5G6X5JovKBLrlByjuXIqF1nXpMlkg6lgIdy+HResoCixsrA2R
LYu2hul5LcpvaSxFXVCbcj8Q3HlUs9qmqvQ2JSjo+SGou0JoCOz7sJxUpa061w00ANbCDQatcGuK
dMEal7T19Ruf2ameWzWeRUDS3vkCxVzbbaZSEZ4etGdlypgHjJzL5zlbxlSBe7tmBlS+e7obI5Ou
CG0bIwwdLBP/2tgruRMOH2HFKrARE/MUjNIbfW46vwO6anyJynX0aHR3mlBHpAyOAbFbDT4y+B6L
VWynRifKMdAbfVKxAjVmo8oKtph5d5rBzrRzJ+JTFKozSMi+weZgi3isu/mh9Y2UvkD1o+VrqXWy
hQ6HT/jRbZDvmMy/0wgkheFPzeVQ/KQMRRNzBx6rKztUxaojpXQn4Yv38NKIod0nw/XHjm29zrep
ZoDoh3Z6ZBuHqg1Hr2Rb+GtVEjxAaElxAYiX348iq0Vcyx/XBtyVUMUdZ23FRItfzOlDNJoriJHO
ZLGVgCdhDZHcoE6vOzx0y7ifCnM3TWD5YP5oQ922ljsFesANWow4ddEMjLPpf2dIkGzVHsf2z9EX
3GRujsTPSbdIYyJ1NEVGYTOgmzW33+JZV+XmLkw8mpb3KQ0928/lHkLngNhq0V5tfmRB75glQVoi
2DM77rRNFxUm9B5jJ0EdCoYRyuRaZfnsUCXAbCvWAc9vJ0RYeqHfRJC0PpnngCUfzJXrCzxVNeVG
JB7KR4hV9Rd8V+EiBr+Kv6uNjXJpL1AVF/jw2RbJf33RdSGRw+lMbDb9mhGOnh6HYyo0bfVrtDdo
cVMDG9juWucULwUP75WXgoyHbTHPysA3ztsUuZVHXWF4aU+f2HLOuUN7ajv66gKXwPneVFReFPGV
9SQfHzKwSA1GgtO2wLkt5OOLz1MuVGx+On8goJ6G75avmNiWSnEcahxnjaEAtkfgi1PduKWqcLuR
8ke0/zmIJIh8LY8dw60hK+2vajSYg+b8m+Vcfpy1btC8AclZSIUdqnEqOQ14WRzGdHpSLot1/ihC
9Hep+Jt/mHFz4bsvUtUWGd7JGwDMBiu4k5c40q6/3uOtCrvCCH4T50s0xDL4B8lLWZgZnE/KYSwZ
426Kp3YgqCDW/GT2tghhDNAPctMXz5oF673TswsK/i0shPP4TM/YfrzvC4iE0NNgsFYZkxRF7WQz
yBGneY+ZbwCDjKW96ytLiHLSG3VeLWex+nMEqXqGXw6c4uyfJfd24o6GdccnNQpUe8A0Q+TEIcQG
s1HnpPtFkMdXhpGlOc0p1vT5+GQRVGTV/854/yBhYmHxUdTqmOi3QGWPXzw2wo+VgLtguXs4gzk/
+IO+2j3v2qq1y9Xws0LJAtcaStA6MqtziZ2Zgf2yuUCiSITDvYwsOwW2PE54hceiN8ZpjEg5dKuf
12X4YlBXH8K6T2L9WSCpCz3ZQDS3su2F+ese3PLOaZcVkpQVk2R/Q8bACofkeu0ihnuptXe0NcVz
EnXj3wHt6V2LNx6bpTnj2Y82LgzkuQZduxKXB3LeianWCVrhxaYxISmcRPYHOaDqam2dZGDtALZ/
wN7CL4uHFF99tJYoY7018G66cW/S5osEOtJa5oM+k73aQCHp4ZCLJr+zgvn8SDOI39+FgNkY1J31
RL9s8niFBBX5jhB/ypviGiPmEdcZ92QyjI0G1f9hNhiloCMrXVxoNOppnHsTsu49hqqeKchsJSpm
Lf5lLxGdHjl1t3pvI/XE2yWFq5kOOd0FZQDcgCvbKzmqF7+0/gbEGbFUaZIpTGI5AH9YaebnzJFi
t0HlX+PFlToR0EEc9u+YcmTpQKXqU2nkp+PbqyVZ67GMdeV7cBq6ZxFwCPUeKV88yjSsPFSoJjPX
X15gu3v2qcmqE4dCHKJBzU5Rttw/V2xH8H1UvL6I78vKPnfBmRMFi8bcir17/dYI1FXqmVWRHDU/
uf+f1RvUmakZQJV//4AV5eJeYsriyMsRwHvnIKOWpmH2h9a4BK+RWTsqzxj70pC32BYFp9yhgSrt
yCc+n8BUQMIwBOqhTWM7SquM5oYbd4hzVVDNtcM7owLwIk44qDRPk4HcESmbeVjOZorVHDH6Leut
CxC3/Qmdgx17OpVwBiOz9Zmi92UEYp5mU3AGCF5en5RGeCezMba8F6efw6ohf0nVoTdrNZl+WLxQ
991HWROiej5ahlloh8lUgHz6M19VFLjpTvOymEtpIfAwBio/Xj4MZ6iX6joSdMq1G3eWcRGj/4kf
GeUXPe0aHPVVjKRPTR0My9PWFw6CauJXB4kNNfRB746oCM5ScV3CAT4yAftxiC5P3c3dAxdLxt6a
oPcQHwqxqRonkMRllUMYeg6SI28SN2yQ1sFovO/qgoTlXl2pwFnhco7ri4TjtKHQGU2IW9dHS1cd
kGhkkzUfVdiSVDR17HLaOp2LUfTiIU/1QMnMlxw7DrOVhjUEenex/paEZexHgK8HWoozYb9r5cDE
5pOmDtjxXWHOj0Xnw8FL/GPiAyamqp7JrlpvN6ctmp8A4cNcOP4SHtN79GpjpNq4kNXtGwvZbAgO
au13qe+biC766ZPnTnE4OauG2VSQv8qq+BwkmbC7D1nUSIf0gyjnFwE00putPOBHpdRhR1hipxeT
19mtXRvYk00ngtA9GaOixDTqlbXxUv/eqTKtc3eoNrVZzyVvXzxFGhaAcB2C21Ovm9o76Xp+JzFL
ilOeeYSIGsBa7XPaeyX4Gv4s+WqL+4vvSxFWm/WS5yU1CzzUBfXiVVA75/ePBgmbkCtivqBK4V9/
W2VyXyKltmA5iJ1uSDMa9Kf4t88x18zPKIYQPhhIEOZ9Y1iUC3zr/jt8+NExpWHxYVwoDTrutKxy
caMz6Bm9jZgDUwKQfYOHiE0J8PWzfWi5NkP4jGaw3o7P1wCnZRsXfccKXwYbT342UnZjo8GAdYX8
dBkBzKhJyOq8b65lAwW/JQWQMQEO64+WzEeJW/HmAUpVpdfieRn+RL/FJMBS6370iq/8B/eWZ8a0
+FEblEl38NwO1jgn0Au/hhgdJWbatZb70S1AoWke5qSN1nseKNuaD2LOyVDf2xlLgy8Rmb7CpvFz
ulpzwW6OMfxg4hUh979+L3FE2SwaSQusvigrxX5Kilm8n0IBdaAsGpkfCVC+vU3u0D/DADdZSK8l
DahQ7qAd+SvhD1Toi9ZvEL1mocJ8RhCVdWj497TDezOf8Ssa0PfRiSaWQVQs9Q4bepqQmsIdoopL
ovQ7XCfsJ2jddd2d9i7sePyeZDdKO1Lz90TRj388oKyGFotedC3xAmAp0s7tD01fIeLIgDMNQB6v
xBy2epLDFzSp92MPVPV1PcGz61iQHPMusDhFoagsiW+rnMZ6Fc+JCLURX3c5c9tXPmdTjMC/Nmz9
esmBtjZ+BVG6LaoCL3RtHYPsx/ro30aWa5Z1Z3va7XQbWjHB+P3dx6o8pZ5v21uyF7GyJCH5X7Uz
+s/BtLmUKTvNBX0Ks8A03rZdg5G7QKz1PsHmKj2j2LxK/wuMQ5uMFiWoI1tG1Pwb+FCAwFfvyDeG
75utMn0AzJAAcVjxUQ8CER36R59mmqTz7R0AqTGzyfvV7K323R+Xf+3Gb8KZlaFzlYWR2sFhzo6g
eQRj9ruieym7AeDLyimCqrxX7izMMhgdMLnxqCfbBiJ8t6Wc6MzQV4Ccbfma1n9J31yV6tAEPLKe
WTp33l7KxHqAx6bJn2NeqEuRM1GwvPv+FgRJTb9mcZfJ0hIlDTQLujFtlF0GRlJ4Th5RK56E7eNu
RwhCsKU/05rEaGrrzL18G2aCYwkrmf4vVSpQZrnJhbrSwIuFOjzRfEARITR1eKZ3RYfuIkgvzPLg
jFP4NS6XR8sa1VsXbCi0lElHTrsAgv51jVP/8s1Cc+xNuznwRH4cW2IbAT+WTR2NcjOis9/WT/Ju
cd1E0v/Qcsktq9Te3xB6SEut96shuliIjI1RzZjjo933yMzs7HbG1KhBu7XCCrEYL6wpGUQcfMiV
VWi3v3ThMLJ113NIAKaFMv7z+il9SR87YUrWgKB0JtW+cTKKvIUqDnHgaeOQw8Am0q+vJeaOZf2o
bFmwzQ2zRFDa0ZoApwEymjZ4TXL7strVc7eUu7D9nhL3TWM8rUN+CABUacpC+iCoxRz3idctZ0vt
jMVOavQrTV5QMWbZiPsab5UHvbaGyCa6TMYSPw7rMxlzAPcxgzN0IF8YVzS6Xm2m5WO+dANnUX1u
ukEOxcM6+xxE3kyHMQtR3l5cMDYTwBEIyx8HhxjdMspRhVGW9IX3t3CzBHPNLZSvW9CY7Bx+REfV
IgZp1+MbKsDvm1INieplInRPY3YnsAXpV0vJ5mxgdLz3V+kHAh6dXmq0jHqUfzEbRbKKCc0xxWwp
Vh7JwP9GTXr3YG5nBy/98FjtRqIXRIcg8D9ypFBddZBU98EOmBCVjuXW4BT+CmeovO42+y+4G5TZ
GwjDoPhQQaMbu0O57b30Tf2R0pNhPqvwgS52pgZdaTrMV2d3cDcdN5SnmQFSn2G8WhAs3uLxHM6r
xUwBvpaKYrO/CanXJRp8YAtIBxP27VgyfrILRYuEq2qy8l3qUZoY9VmJZNfYx+wqpQw6BLxs+cn9
/BBqThxEw74u5/kuNzc+/UHXKBRjy9AZlieitnADZa5jljR4ri3GHyM89tfEIfxy3OEsVjDjSgjt
t7RvVY+WeYyrMwE/ab18x4xDuioACZWtSoNgNB8V088gKEEjNmoOUrcYKIaGTDZJZTCawVMjVUCz
Vky8NzCztBsjIXGOiPMEKNUQdJVa8y5n/IgR+q52ixbqPPpXMU5KsoIOH8Ce1fEICTzjxaP8vFW+
hs9jFKOSjAHdx0mHiQJOvA7preu99AeI4X42cTjUC0X2BEGVBNfB+DqQPs7GdGPl86tEqIKRNFkd
KJ7Gu3gpZTbHQbuIUVDJ6+QQgd+PK7pRDrb2MoArWqrWxM1+YZuhbnRVNVMi8R1jDSB0sdIhKchj
MJ2dVpciMD09eLodCNcM/iNG1IjJHILY2IiMQQbocDzhxtfiBZ70vMEIBZJDo+y0gxMDnV6bYid8
KR0ztiIs18Gr/JNIZIfx45c/UCtMH/q5DNtZG70qXFQQB+pXP4ijY526G8nyI0BT3Mk0nnojMXSN
lz18JS1fz9KpnI1jDCP8JK06tq87Q/Xi9kF6ByK/IcgrC4UClKa9IPCvZsS8Zxae2wI1JkqDSkOb
eSLtykVWWBvjrQB6wK8m0sdx9Scaj27J8UZp2hsHyu2cUjuY6m/IvtRS7NpDjrSwr0/KZAWYmHZM
q+qn8P3RsHOrw+RAETtFMVNL+kdtaKC5aJ9v9tbWB+D4LOeFH/yMf3vKoZMVIB7cPQvW2q/m4H0A
uJZgoNibnIxUSYXRuDGZfppIkk990/XDHOyPnKMZR6ZiWup1j9/emu2xloDT+qcPZqSf+/TAQ4g3
RlgNU5V0YYlXpoOX3VT0vyGrU5YgzkNt8gfvB3kwUD2ULgv78uqDZPJPUkSVcfmUU1XZfFDxErPQ
kFyVn6C2wZS9T8ZyZHqIh3KPGjI2XQfpxDs8S4WnOssnVBUeFLqIL306rNDdetoNFLBb+zaTnZJf
vGV7u0MbwBLP6139trHVo9aXd+hNVmLyhsj1BZPTJT8CBv8UvERgKCLsinJC9rgP9KM/+wu3zUkx
3oRIcOVITut8dJI9m1IVBKL1/L0OEUrif4nVgvlN42BS7qbZn8Xe0uSXj5R6AS6587g5+CK5rr0+
rKzdCoauCB/TL7Tq7ydSSOfoP9r3e8xBh1V/qsr+uXDHDOjRE3rQCFjAbSKh73BtXDq/Lq+QEvR6
HEnTuIRo0ilxS47tt7fjNhv4tEgfutTXAoFVJNPQD1UtWb2/TZeEbiM/797ya93SS7yrrX2KPCcc
n0Lr7V6EGh8sjZa6U7QMLgjmxVYyB9KJgYb5EKMUxBhDD0VBevE/Wp5BJmICJdZQFWj7NC/L2Az3
b9gZ3Xd+qikNgQDpFeQgACA6siTKFYTZ8XCcvj1wTK+oAlZNox58nGTANT4EhcPZiEycIrKEm5Ep
2NDaM/Maa589dHqAwWI6ifWC4UbanjaQ7Sz2+9G0w/Kf9+d4q8w6D62DMuh56oGDxg1VWX30wRv7
DDPO3xHXMx8MLsuNqyaDwkaPMscPaGcDRiVI93QUI2u1iaxh2oceckYPhxxcYiJ597MPE597/3TX
SYJaKVz6rHR9NUr2IennOLE2XHzeQIA+8EyUNS3EBfx0GcCTURVsVjntbIcJdCg2JhVYwGI683Ha
KchAgNgbtVGQi/27VojDSFWkL3TRdw72QqZW9uiVP96b0sHO+n8ucZ6uMbDVF+xzI1xfYU/J5bui
ll6HFGZ/00yAN1bP3xRAzfE85AJ6jUBteyvoodXQVc9iYjicZrN4+qLQCjcY+xScnb2TBXnWQFnl
WfR4DVXse/RrPN7c7WabpZlrfCEeBNzWnJasCDARQpVCzY7if8n8pknuLL7h038UL3pthFlyCrYV
rMVFv4cNH3C1M26kSFw1MFtrvjDfK3RUMznjQ2wRcnWs3Wnyeg6yhdmZzAmu8sbf8BubgBZwLxoE
cElx1IbcA7vihfWZPcDwWYkTb7APw9iNFH+WIhqpDmyrSVP7ZlxyypXRtQkRCP9jcGYjal7xMMZ8
i/L07ckh+43anCqBFK9eDa2408TjVeo2rQ0U2nADouT7M8QkkhGWYmn5ZrQ5k0h3VVqvQhnMTrpj
DoWfUOwxxx7Lpuo6klyGULxUx8VbDEEwEzFt0W2+JJ7+Y/npK16dzAIC2K5cw/1uAlLUiugEmb3L
UryVK+V12a2/pebfHsA4+mHEX4M2MQm43rCXaG3Rgk1+9LCE3UCMF3/dlSyUX5sIUX2ErWGmEWF1
WrCJ8M/AvyVtKe2tZA9wzE9+bygumabpOWsFFfrmERPEBZN3f88hQasSraZPLSOzbCG5Zd4pm1yC
2PsOKVOgGl5DHL2UrnFZH0jgLxzZEjWi5ihWHVOW0+wAVpD45yIiZ58dma/sqUKwPqRltcw+Nl4W
uT6olOzdDkH/wBKraKMqUvhysu/6c30CYkS2S8CnykJRYejpmr0JtByYhHiVBmqF8iVzvXdhVmSR
HmiJ/IGYMxgPXkhASHq/1eunla4DynEEBp0P24lE1UeYv1vOo9qhs/xFwlfRrMmDFBVH0E9H/kKC
rB286NgQ3gRVryOcYA8vQyafVg325p6WArfVrdfRXE/xM7yDNCkNZiapvpUbbbifC+43hfo6x1ZE
LSEsJID1xbfkf4IVVtybYWaTRi31AwZTToaLPvjgRCfB1GZbwPjhcFXXIy5lOB4PdDtVUY882ktL
1W3I3f95FKZN9493IRaVg+fa3DN4OPGif45F5YtRDSDWL4pfcx1ri7RDZpaoaz3+EGGPpPL01V7p
S82JPNSLSWwBctuIOeL9VyR1peX2Q3swopl0BC7MY5Kc78FKb3UktQjNcj/D5eCL6KoXDBkAsG2P
9OnzL//glRr1VPH4jTDEgD3L9J+BL0d5bQcsgbbs8qC8svfT/KXLqhQrVj+R7OVN8dXj21s09s8R
ffkafOSId+6QGh2QaCSjtPwAmKtIU+LfDrZTO59+YxtgMPuKHUrrDmEsNc7a01lH6T0kGoPHQ5GQ
etwzQP94IAWcFkwK/PFfOBbToGicVRYuvWQ08egYjsbnTGHQYUn78YT0qjP3TQp1PVQKLVb6+4Bf
qDMW8FutIwA9wysYMZqNy7qewm+SJgGIRPDj4n2AHLz9bmFiP/nvC6g9x8U0bCRHnS3hQraBiin6
/junpvIXk7t1Ct7gSweDiHBuekvFJsAhsuA6pVU+pT293+qdbsYTb/eVhDSsJhHpBP8VGtJ0dXVO
9l8uphOvlzzU2QFARbTZBhNd3qH3AGntsJrQI2yfY+NPCilvi38KQE/lU83wW+8BP3ftsp0dMZKx
wOd+ziYMttUkP1J1s1L+/oZFniHmbLVcSkTedLQIUtlosk3DUcbeXrHxP7pmeDsDWK8esMLVo10e
lTTX2qdDAH+tZlj6vMgJAG0U1DQVTX7MaaE8pVCEiyehGDIVGb/g2dFcyVs/aXw9z2WmYxHlgK2y
uobtCo6twyEQXhZxnp0sVLjkmr9U+Qt8pGQ+d/GrP3eklfIO3YrNIIjJoauXBKCX5M9+cD5up03J
lEko2CV0Ws3V3rQBN/ce5OQCnpTnGf+WjoixOq51VBmaz2AD8kPB70kydOW9jzlft3jdZaL+Z4E7
UQVu1dHIOUjJpY8JzH84EAufPcsQF50+j6r645kdx2m9YKjQ6oQ3vqdFRVEkme8ANth/KkHbIxUL
+HTc+CTgIqyKm5l2nimbqoRinGobpx7KZXrR81TZGkqZBQ4hG9C2b+XiqbJK5mo2jRzL3TQA23Nj
zR9IufNV2jIWx2clRR3HeqVPfQeUcgVoIzCaWA/yMKyB1LvCMVKvGyMWy96DlfCbOJICqF7GzMVI
PM6BtgItLAJuUMPk6ChOKWPiLgDDKQGnkWosf+k5xNV82lGiILtuA1xKWuJ9gjJpt/XqMI+PseWI
/MLlCKdyQsDMK4V9Rt2Jd+TU3dt24/CUkdfVWTmN5U8KrfxTaRDb+xg87KfOCVYyvgEpC7U+B+zE
A9Zh+x1ugzPBMYJDiIxVP8yqOClvWmrK1BkgV9tsnEPQ1kPIkMbo8JioN7Ip5953X3CCPcuzl+kL
vgoV60A/ACtGFXvcwkztMOXzjYjpxdOAiquP6OZnDN5g4imYuY8NzBGISe6IjsJdEl9vo2zFoeJT
l2kqwyMhci7i1p1Qyl8cXzesSSpY208u1dsl2+DXvETJhvKNlMZcJLMjkwRYY/icvIN6HRNlRUQ+
vRrJpGBX3eyg49aBp+rzh72hXfJ/QRZs0KoCw4zir8+isTS9YTTdkHKAFSnhkcwJL1LqjG7yuO1w
y2gLHUa5Iq1ILWr1eQGazA0cLOIKHHnoJJQd5MOs7kxxRdH45NhCU6ZAOfALcFDAPsvaxYCLsCKB
7amZSkDP0v2Nl/yo+u8PUqVA1ZFeFvxFYrlbGsric+zp92QRWKOtdzCYqycuQiBUuaArj4/zYS9u
q4niSq31VDFARqNyhw5P6C+Liv1D2mR/tIc/TTWKaoy8wgRqNHuCtg7tRymhYXTT1HgDzl0Qd7jQ
RD8lUBqYRKsrB1qNysW9EnRaiH2eVCHnE/XVtU6a+HdSIDsBVBCbLNcojzch22joW5biss3YNYsJ
T2HqCUgDyCF16bfinDUwguPtF5AUDDBCj1XWWEd2gWMu+2lMTB4WB6VEHk/phXd2XYNwQq20U1Ca
slHcpbkW1VGnnxUWEIFi1J5XM4sFE+4tvHVkV/TYHr25yQvIWopDZDuRaCnAFh+MjpEBRzvdBBiZ
405iLcunatU84q/tK7NjkUikyUC123JafWxxAc1utJGHinspWq2PzscDWK2mxkDsRXtBkYMPmmb9
5kAfxmz/ROkFXZiRDMfVfYWElMtQvU036FtQLoxIGSm2EATzsWFWMmUInGNuRZHJHLAE5FgtUi5A
tB6uAmGNRucBo9UxV6qVKaAmkggjgFSuXUioQXPqzbBJFWftd2xTEG5iA0wDDD5BIiE1wRVlAv/L
cih0hSBs2TZDqzUfDkORt6EL+fkgtqwyBOZ9YBy2vJAf3CUJPHvlpt7E4egSGHKQpMGvEsR94ihQ
utfLW4kS6xVIn6zoF5nh3MXSjLDhFPVDBgW5+Zi8kvswzZmrIHL5U+Bk+MbUKY37vdUWqRHrzGLC
RG/96Ne15OTggPzS6jwkAsvyX2Lj9A61XEzjnL4v47TTK5+On/2C9ZzgfeVHxgv0i8M8PS5V33QZ
hZl6sarDB0UeAjRSxe47Zpme6LKgPMpA1Ix1moZWVVbHF9q9tX/sBQmTuYdJyjtDUMbttRKUEONf
yad1zJk+TfkUsuaIjPEMIdDgYNfJ1gUfTqO6euDqLBGwXaQ3IzJLeFEmPz4XVlM8pYbajMsx+yHX
Gi+fYROUzhuajsgxczMxyXfxiqEHI1XoVChGJLI18yLfP/al4guSmeQvJHOsnSPUEyMppyO07smw
0zI3YR0pBLPLKW+7qE4jxYV/cvewjvKzI/0CKMUNH4wxa0O/Ts6k1Ft1idHgewUZ7h7eHfzT+9Ll
Q20SAnCbbqPvu+jzC6xO5lGLosexrhI0gOdk8koi5r4emEfR1J+AiKacGYMLbDUxWICFSD0OWgBY
3CK2d26VgQ95khp6C0aeb2z7jHig3AvBkuQWjD8I2ahDMcQO6nhUxHb2g+P/HNIrLrVgnL7lXaf9
a07cWGK0nG8wjUy84BJ4mhdo1wWAe95OcaE5Sc+GZGmHID/7TImK9Xkqz7okAvOuJUvYa3jX4WDR
QwGe5hMjm0g+bMMjABRq5KmuDwZbfbobVzkPjM6XBZSemPL1unBVC94MqnTfWWl5fMcYwyL9ZyUS
vrGTlt7DBX1OxZaABZzLghSDV+4AFEQMzbqMNmDsxcYG9Z/kw6lj7xzCIxvmtZjXy+jlRnp0L+VM
If33bo38aNT1E+e8V2CDfz6qw8s4wKaQiwr7C8UucGOa8MXwxwwVdi11l02aLu3/hfxNmLsjHb5Q
shwJlBNIt+WAm+C/sUAhaIKUIDIX+BsFfMsHP4TPt3se16sUWbxFen1yWu5auqvuYpPQq4LgOpQV
3n9CsWj3H66Yb5D7ivnmIS8+U+qXA4yYAd6l+W4HJUZlahfV26V8S7f/TYN6Bp0a/FfII171/+/q
rH4wb1f20Ujs9bO64eOqsf8yaOAA8BtZiE3jIMZXx8bPYkURPHkcyZnkXTHldBzpu5vty080O/RI
tqfxoi5VWwnt3gAMlQYpt57MOvyMaDC3ZJDqvyT7fxlAf6CjD8y/S2WovYEAbvC1CtABJzHRTfSP
rQ8vbWSC4GPAKEYGukdAjzQ6oNaNiNrHByPntN1KIseAGBLsn3jbJydKiK2Vd7IyyuF9Wxy3EHi1
BlJNA3UtwR6bpMqZdMdgVIcnjbd1XBrUqD4Jnq3sCjZTmqMVrox6iZ4bKp2WEDugg8iP+o5QsA8L
CH4JnWUcxPl3zGQ6PHUv62PgqSJKqVMTkNOmE8h5XRwJ90SfsitnYWoiwuwo3ppMJI940Hn5+Psp
5NL29AsZC2O6mgUtBlStBnPkBYsyquicUw22OslEBMbg92QmRlH++2RbBkLzhYOrWdePAQcYKX+d
aBsd6nt17vjMnOpuV8yBy4a1I48C1oHJ4JW51Yt2rUe6jYMnn6WYAd7nVWy0OO+iJaC0BkfYB1Zi
lNJ/XEDd/9PSEqBVthZG3UH8wNSW8WWZqQGXOBP+s4tRNjSGAilfX1pLH3c+fZ/E7VmDdjyHoClv
0Aj5dhQm0f6/FyB/yadFvQ9G/9uiJtsvdVCDKDPW/KTEx1HF1DTxdPPODzO9//MT6gyyFUwxUwRG
1oX8GzPHd9JKdwhUVvvJIxjnKwJ6KbCp9MHgXJN9XDuW08oDdal4lISZ0Qav4xaFKoHiEOLUnvm2
HhsNr3vaeq4yIhn4CdvmUBM7JSrj47YTz+1/V9a0d5Y+AgqlO9gsw9dyfhyQ8ZiGtTLVLHolId5v
INzYxyB95bw4VylUtb/Lxq1xv5LnjHK64ZjulO1V+mSyPZpARB4Adnk9bIYJY+Cz7WWfOq5h3/4R
sDvMLxAF1KKn5nNTsa50FuzTkZQWX4zRM5URDt5pCgc0Mk53bSCsJGMiiOHf5fU5BOB4wEoCTPMa
05Er1jVY+v9kLwsr7bwgQ96ODF10+SaWobvl/B6gC+afNw8o3H9xJsk29eUCJQgZKnZMCFs42qRr
MSi1ngofVrLnSHexnKnK8hVQ+gtQz8/EeZMIlrJrE65zRhIkQAwmpUeuxWCS/rAof042/0QIYBdn
GN/OR1SlrP/u0ycVumUnX4EchApPosRKhKjwBQB9IhOXBqnRd6mu32c2wgrvrv1zyrD1LueGC6aZ
uZ9SKb4kb+RlgChCM4WMBEiEpkcljl/87V4+TzPTc59jvZ3weIUgXUDcppl4tZfOhERrkEWg29FG
zmAqknk4ftJ7lJ7iCOZgkFnbTAKwth5rHgj/0OpbVZUX5ILCOB9DBe3OnxrVngbrpO5Vxrx/lQJ/
cXOkG2Oij7sLK4fX49i5D+Fii0qyYCzzo4uzZKdx3d/Ds/wtB141Dp5alwdA2anfkSoQk/XMFf2e
Yw8ozFb9MDeoBtxg6xcEZML3xbKNeEOtbzh3nh+k5MdLmnCq6xtH1v3jklgaaQlcMy4ofx8D03hi
BOqJQjIIn4GqqXDzpfFUxOR/EHC5TVtLGXU8jmWZSJxrm1g3W/17UrDcOh8kmKhbIsme3O099Z5w
MzmlVg3Rrl1cfbwYcMqTZndhzzva66Iyx8opyXVzxBgdOulMC2D7olgwXisIdVD9wZ+gHQtuTY8b
SzqEo9YlZypt3zoVT8aFEInv30yyVf+jlUw15vD9sWmDu8kMIL0wsSirZXFaGiwIByk9CtewFW7i
L78NDh0cbWaEoMs5o4xp4/QcSzqFJRlpcGbVCJY+uhi5HKEvIyfB+bpzdYA0rnh+ZraZISkG5kso
etr1T5izcU4363xk1n0aRLEcYPO72jkOeGolq1cXQYmWQZD1HovO3a9zI2m0xoGmKgm9dDy6dJOw
AePAFlZUAQM4OJp8CEqrN8U5vR17fzvXrB0O8yleJXs74KbdbCjS9oFXvDhuBAkpfT6EAEWOJlm/
w+6/8m0AOhwJNRmz2p5oYdqviiZq7aBxxw5WdemSjpLwkoNRB+b2aLlbqKiUULBUkMe+irbZ4xfg
AcAl26Zt+pL36A3uU7ZHCf7LGe3vio3KQlYWO/i99mfF6xxX2RSB64fTP0i2pNTU2vUuObGhXWKN
OSc/zoq/LKOHEb1u+Te7ffDUHdzo0iahErHelmcG+KxoqZcQmPxlgikqNlf2MQVetd+ge3fgWN60
BFX5rwSxHzycjh76vsX23011TTjVUiceADQ8eOm+hinIY5ypWxg68cdmiN+xTJOijlMio8sBcuKw
FpGm0euriRHej7ubHvClEZLe+JAgl4tOETcJ6a0/Mli2j2gpyaie5QOZF4xTgVA//rFRzeTNY0HV
lCk7246jucik3ICULdWyrupvd0rsLjhALtjoH2myAH7otsKSG+KTUPd2VmModHfpUItidG2g6b39
inPKHM4QXA9EQhT4CJFU1VPP6FL52h2dLX/sYc1EuJnblGCiQRvrG1rnlEU4TL5PPOP5eT6SYMZ9
EXneluQw2g5Xqd/GcTX0S0hauE7cVVOYSAcUacf5n+xOz5fikl8oKoVkTcMcVti78W8H6gYrVTXE
5e9VG2nAyTU5slt2K+pEmWLYRAmzrZf/eZvuORciCtCjtlVIxxh4lGuiaZaEQVd5+tHiIEIfa3cb
ghl2nOIV7nl8QjZHUFE6/mWv+h9xLwDIruzZ0zKEEbWSlcJdtX7UBgbW0YrqBF+ARuxjo6a8WkGV
EnAMIbXx5kA1z5Kjput/pSvl0ZnS/iXVwFYRDB6LTW8Aysms68pVDVdz/0JKtq4QDstS6fq+//7f
Ln9csX0hekpF24eBFBlPCZ7+CH7n6HDUfB91EPDm/ZMbp12++0oFsPwV7bQTZ1nyud4jp7taLl3B
/F5ZVcQS+dMyJ8Xr15pfFrwYReiJ3HdBD9PF6bAsRbmzhhm2KOlz+qDIVy3YmX0kpy6B2I9eYK0X
hmzbnc/ZfdfmLzZveJSrH0BjPtaGNwzV59mItF9COQrOlKUZii4hR+V1FPEOFKT+cSkIB721KlaM
/knbFS1IcU/McVCUmqy+yi9w14+ixZqBojO9vfLvbC2XVCokjYyzoaoOj19rgv3+hjSsyP7OwoeW
qsVXbTPzCZSn6SjgKCPzv0r7U7CCNHQAyQuUM1eAtF0sV85n0t2lZlnrk/w1htLu4eMztrgM3hBh
RRfssDJkpdkEUvdnPfYFf7Ct6WLEzyYTysuZKgLipBghTfj6mVCeFhniPrdaKs1XJIEpkmJfmCo/
GFfFoBvUqYnRYUmXcG5n5vYfUz+vi7V865REfetzVGPlGVnR5g+v/kQW6F6QZ7u8JrCPmYbuMQ8r
sG/9KG3h+cCSOLUIoVYx5hN+9/yRoWIXyRDjDgX55I0W8x+vnp9j1sqsyV6X+r1vethe5MxVHGCN
Sw42hACbVQTg83MVdA/R2NrukgifiIHLF8MeRMHq8TT/KRZM0ENOEWB181rLkIhvZBFejKo4p3RO
w02fS0Zh1wnjgll33ps+g4HCn5UV1dKIib/4/suTNMX9oj/BYSa2HR+SJdsucexRh/BlHADaBe1W
T/JHpoYPo39Cv+wOrgSNzZxDzoHXWoB73HA+bh7Ni2D5CTKHyA+xTvcjdRXlFTYodLwx5LX7WPxg
IwFo2ViNlwfVVFhESyjdPZFvraUImh60uC42Sg6T0UYfibHPfAuzWr7p6OkxjJRmRMGpgL3xFftt
iZS/iQgOMaabAiHqxfCJxCs2hcUPtKvF4ppz0UFb5BOg/T2DSL2l2WX98Opqmj5PVJBEMNOrPWga
LF1NU/dnBNodipPc466TUt8kYC/3PyFD2HhWB4ECYvvhgQud8fpg2J3ATctcPyDRuFwho2UG/Crt
Z5cipJlDMzQ6p15UjqEIDY9kZBQ99hB3xsuQOTGtaV3cHb9z3GYWonhW6uIN4C5aFEpAK8pa4qXk
5fUiWpepyjhEGmZwOUvEkX+O0NC3CG7VzIKaI41htnTGjpI7RhGmNszJyiRCfrCk+eN1TXjnrPLH
ApeYRmfsHePCLrbij25lUE8F/IcJg2bmLAlfjT9+J8xUFGcSplyfa9bhyeDwiTEJCZPk1oBObJN8
YfU+9Bjpbg7OUbk1MzpPqy31g1Ivt7B9KPgdtNb3jyE9EypHhrS+H6V24VvC02vJyp/jUS6fjce5
fwSn2hZRIxNf+AC1Vcq1+4X9q6FemZnHezcILbbF96XzwuCgzNX1Wbs8hfyGLazC92XYbsr8DQ5F
fPHZ30OXhuIkLkECVFUgoJbU/GJsLXVTXf9XRbKgOvw9rSnRLDTbHH3I3Yj+YTyYdD8sr0sV6Tp2
OxEPqo/TJDXEuVYtlYtK2VczqyPcujX/cmeBN/wGcAwDx6OaGmC4DMKcyhKSlhYfnxZtuEVtUz70
qcMsUwTPTxYvCDl2Kq97OHz/P2Ff5Yg05/10WO3AIgb8cSmu2axSW9I9Uo7VmSl9fljZyX7EnAF8
Jhuw/EL8UzTqPuHJ/I1k3r9EprZRWVaErKa6rtr/VTV4wUe0sbteuadI/RBpwpxEmLPTwgdZKXYY
DxnUO0GT4LK5eEGki39IqJPacIM3HjP1gPPMQaN4CGeLyCnnHD+u4+uGm3gjMg04qWbP4ppImT0C
9k3p77KmOVJlODvBNvopV9+bAeBxO1Fog+9XRi+bItz37qznE9k0Z4b79NCm7JdFWwKMOXuIz0kj
dqa2QeppZ/dKrBNPSDJgZTIyhYKQXJgS37UqSgz59fjJ1nTewfgnoFAyyqYofKOw5+lUSnno9yf+
cnkyCecNQEdk8doKMLduTmJH8fckMOfQbGIDCQKCU6gMjmRFUlC/Yq4fkpEILdXUSUMuPG02k9gs
UxTjonTo65/YqKt/K7BgO+/vNHuswKjMPpKyi3hD6iYtQKPXNB6/dZnutYEabW3Jj9tAMqyT0gx6
452Orsx4Njnuual/xoLROz7nw2klcbR3DTu9y2Vf/017dBVkOakg5LWlAe3+ClWEPtzrY93HVsBo
PoTpNhiE/4AQ48ZB7mChPiNOGI3z99nsu0LrFRExh4yH3sKYdKEP7txXI4hiBeIOwPVNZPsaeCeY
ENs4N+ljZ0XDXs/ZruurS4XJHX46XJ/bGJ+CT4TGn0g1SiaLp9DIozY95BAaO43es71UdATNKV+L
do4yiXQcyFooasEwTY5Pffgi3zc4KodqSkF/uN/emNaNAT8OQi3M12ehiZRmciS73nVDqTPvadtg
yf6/b9kbflQ3gGBqVdsubMLzm5LjSYpfJ6umGhazjPxpOqqZ+RpQecZ8o0raJmXPoGbDmCXOYnf0
tmua2Xr1aZkVQFPmrPkV0sj0U/I34/s4T74XDSJvtEw+YqTyCuozb10EiVqWHepEk3hS1DIFpxzJ
v8jNBpP+MasMBFc1ZrI/fAMBvRJ6fcZr5tA9BttVu+hhel5tR/GsWmh0TrFpk3yfNI4f/097coGK
vLPN9NWJeiA+00RXr7E5QWbeyAmcmlm0CZj5ZCHdlKmp+a0P3xzlQrJLh/qq43WNnKACxPE2NPCY
MoqCcQRJsWKgab0TnSU/DCKN9ZU6RNy0Vy2kwEMdIy3XIkToNSqS8dgrDdQf98bm7FJGODHPE2K4
qdwFNlQ1xZPw1TfvkQQjtn+PZhFwqhrPTOuXtYa+AcYTwmd9WlwGGssgcZKb98moMMZJl1+AXF3k
HOtQDt8OAjI/utDOWmK0gZ2quqQrYfx8qCJ2rLWbBbBgz9W1FX41CTrBeOcVDYytB97MbUWWyBWs
MwN0ht20ZwohizJKfIDwcNrEZiPy0sG+UcBQO1MqXaINnvwsk116wDnLE9jgz1F7KjZtgyind43n
H8PAP+m8t6OhTdo2TUi35tHoW11bT4O66y0IX45VkQXVwNJzxP4CxWoRYZi4fMicdq5nt5KsLJxr
G6TM4DuAKZurhJG+RQNUyqooQgK+DXHFyRWn5CK5gPdh+FDPJzGOeubGtqSr8EuQPZ0RYvI4z1tM
MA2DFqnjUzzyvfVLr6MzI9AjOBRcOinmWe4gFJ0DD3B5KA7DJKfyDe17Ni6s+3m9bmBqP1J2gPJl
Vt3MdqXH7GLcN3Aw2e6X8GUp1/4jj2eLjF9JhVYOesvf7hA2EEBxhpJeoUBr0cmI9Axkt9uqMn2W
+CDSl12A3wU3JfOzML/AGWz8dMBcmpQoTuH5dsk55/Ys66D9vV1G295ncmsOoYCOmsvppLIxHy0G
JdGtM6ufXdjKtSCEFdoUTT0QNcSayJ3psoVsTr2LtN7sl1Vbhx+5aP9hjPwXJ0aanuJTSDPI7znw
DNYACsP8aq9ajpBjm4XvNkA5BvcGwmkJ92bPZ1yKR/Xk3zCiJLdrciKGJlGBtBlC7Pdv0W0IYHVb
q58kB14Ai7YP6efM53W7dOE9l+muV3TM8Sc7Lj+YHzZOzWN/uk9Y9yfTFcBHjydD+csSMIBkvcjm
QkDtYmgIZP3JaCMAfuUC5HTGyyU8b7GfpxjOoYy/PGn80WtAQLYXQ82NKyVB9A3zv5NKfZNruT0V
b8uYyv0oQjfwfnmkuu8e6njpuQIjcrdSZ0JdndqBM13zw91eemGismAldgyo2bbdVVzkinJ38FSY
FL/ZMkztdweWPiXRt+S2kQw1bat9ZENi5e82AnTXZVs1NUWOzlPtbUwfCEFerJIA9mDRGdLghgMg
gkSp7yylrIMo38lkrPjGDNR5y8yq+UuvcHBQELPCAgsL/jbtX1d2FZjzeIpoiaXVRYDSmRWLrAY2
1l+zjE21sgPw4F4n//as2dA+ufyA0XyUsC7b0kpj89VZsFwa2eoTkaMJp7YBrVquBCJ6UDvxMCAR
UfNpPDoqLwVRVgR/RUqdHMOd3AqCvPu0aVRDx/3QFj+XPzq0p3v31Nvjdlvmu0iyQiI+lm0W3SGU
SfegPg4MihDTtQCUEeQXlv7MS1e7i/Tk9NkBc3mnmCMOJB0zDnscbO/DOdI7oSUOu+iiT+ueYyLd
Dq4p1Jt3Jw6prX6jFQW+1GD8OvZ37wUrK9LPjpgSTTT/7+IiYTtnfEcyI/A02a8kqTJAhfdCLSrZ
NGc+8kWzU26vs0govZ83wyLVzOw2pshOsGccWqcbwC2joELv7HLBy1mgVVevEVqpLAqGMMpAtgLE
o/nuEp5fRv/6kHy1rn5uMsStB2tZboyvr3XDpPdHtSJfQNQTPelv4fyInSQJs8BYRbGXEDw7jvw4
CNdrZS04KjyRSgefZDhaeTI+MbjESIjI/plNyU0rTlcW2dPJNbvtIQWk+G5LcgoF3k5rR1JrrtkC
EXZ+TK3eoDtN9LNRDd02Jk5CLl7Lb+4BHXTVcKHPCtDTnUc3jB9UyDGQrNMpH1gy9voiAe9AkIFZ
1QgKK8Ln4WP/e6Gc/TPXbFCaj7ybvji+/hv+ZhLTbvc5d9vEec5QRjwiZSnLexzND4JeKc0aTqIX
kVQwl6K4TRWBvoKorS6YOfphX/0lGm1bn5ARPCvpyxK/QkNc40+fjFFrKqDzx1+Ep7J611TQQQLL
rayBAPIoFEYIm77aNwqgl9FRaxZfUda2FxAY0h+Pu5z7gAJtPcM/sl+7QVXZSfv+aiVzwACOY7hV
IOQq3t7Dw6BWdfinnaRULPa1iaSRhfSzSs8AIeawm53SQ8yMIW4ZPh/HwAbzoLkYfYV49113dacf
8gMREzr4r4Ex5XtJingBEuO6urc0RN+xFK74XY42ROdC96JMN93biOLHiiiilQqdb49cLWu3oXtk
Nv2kMB6mEBhDvNAvAV+7Bkoufp4uQ4BXatXLa7moyD0Ubu40WW5U8xghpndAygpP8RvduMfiaYUT
Q29Oin+gEJeG4NpWFBM0zgaBZ4yVhKMkIZlmpq2m5p7uiO6IRhKqpNPTOreYTJDG1nKtPIcHu88n
P+QcB4FJn7S5SFqVCLEAsYNp7cXnDXrwdo5C+G6hC/AaVDyBj4m9ZpaWSQVpsw4DKe3VJiP+rYzp
i0bm2GUrDbidWws2QGUj3EEAHBb+O/0i+xHtGp7T5aNlZOGjz8d0nke+XxoKTLZ8TO7kDs3yREQv
qaq/tDd7QbvRvxbqtZxUymOWSYGBzoZ7eoVa7RpRz1hrBRTGshQC/yAEsa9HMbQ1BhrsVkeX1Yf6
IooJ1a35UpJmm+PdoPl26m5wl/iksdB/Bgs5LauvP/eYVdHmDFV5ZXodLLMXnQz2DELLvHVx6BfK
XHIYYm8YoTdrMFTtZ4sSp+YM0ckw/ogLP7pxwMh1HI7MQFjRO0Wu7lC5xiych8dcsQWSwLVaqgnV
hUEMJbgDgl/Kt6GZsU2AI9f7AnY6Z5n/Az/wrGXmeztDuCjrGvzqylY8o1/sfEcW32sOFOgFFg+D
qMHi2wfM8CfxxaUBzgxdWdzqz+sL3Ub2IrFnhV3YE6d3RW2aTtJlOvNzXHvi98x+20WnxyPjaA5g
UsTdBka74OgjIQm93FZAfcn+UgPpBIf/7hTmhkBHkJVV33SqxKgiqHBsUZnMddqzlXfHztsxNRh3
hqTNeh1jlx4jidbbarss80X6fAK43zXAcGiFokgBHjHOJK9PnEbYl1P1RldYzM6FgaroLNbGkG1Z
YLH4GTf+mOLAvSuIxA2aSNQC7NKv9p8yDlQHsH6QVPtBD9PipbcVlnm4A3p4NRrjrFW+Fy5/qbkj
VgDYLHHKMek/Nk3ordCDzT195wjfTC4wgw/aGWNtV+YbR3QQcDVPvg9uwqRS379qMvEmDlsznrKb
bpuQhg3pMlEZSwTSy6HhsiOYwQJ0UYgz1Ga31vCLkNrsqIVkhOJQcpAtR6XHFV77JWPjGHTmbPKG
CqnsWWNN6WH9VFFv2t2GjMWgL78qbI9dego1wfh/JZSC87Kx32uG/GWJIptfQavNV99YxSAlprqU
mnNM9e9pAB2CR0sGtWcFsWibpXqv0KNEFK+lbXa22s64YIpbpmU2/z0m0lKSS8/XcWg/7koJgh3A
ifpmIsz5g2/7xHNkFtCPiPJNwll+5muPybEt21aN0jIuqdhIAfywL9gI9qVIcIckJitPHqmZu9nN
mAUod3dT1Lv69Fy2hdIzVHq4sQVrDddmW216/K0NCsvUSe97wOa7sTXTrzDc/yeZKiOQDkBz2Mq7
NLmvMGzb5h7fS0KuYhcU8022HgWobssEZTBjrCfxlwl3ovVtbB7CvSs8nOG7W8RFNo+CP/2A3n2i
gnuFZnzXWc0rownU6Db2CjmuFs7Qwvy+h+nmn1vcC9IwXHyt/+JWaWhynIdsEu/uFNK8TjCM7MVZ
1qiU1JZVy2LfMaExYV1v6tdzBb4I+oii0XNDZ6tA4R2H/s5hMqQR8l3KTMkIRo+t4m2jdY5usStA
3n2RAQTmL6cnHZpqDZ1pLUviAdnBnpW2RT3Eg9Mp/L37y4PvymxcDvWOXALwLYPIxHNyFDq7Av+B
UBtUd0vEyXax+E3YFqdfLNcD1irLbScqE9L3KDI2YVW/Wwh5WDCA9CtpQ1COtm5dsbnLVvZic1sl
HLHgwsaDeHQXJ9mX0gv2GivdYVAjiOqQTD5pHr11Ex2tm0uan/0u8F50LP2Vck7czNVK6dSby1hB
Rue5bXgUg1ktOpbfvT7tOMZtVJlB9hyvN09Dxct9h+pXeyEti16tuQyqIaXhLIYPgOosTLTySy6p
t1iUH7zQYrnQ1J0jNgbzyPJpneifIDyVF02sANg6/PYwNUT3a+LIYcmcJ9+1rtZq7Nhh6Pk6vzzY
dR8jFbOY7QhSjpQJFfxyUNx7CfcyEs7C5I1BeG7bxUHpbYT9SMltDkoqq7yFSaI0KlMc4BJ1sa0a
gKjz+WfdV4DPDu5w3OU5LnLgjkzmoVlNEphIU7BAItr+jgKKQi2w9kzvwSkIqjXZ/4rXtY2Rqv0m
rAPC3cxDXlohInN5vpQFx8FxPlur2MXUWGChh2ZBiHObS0RMWbC6XCiVKIR8hgQW58t6vF2HKG5T
QOXCeqWkcwo9xT6lrxJqm25Yf1YRgeJWBfsuJxgQYmtSvs3uBgYTqgT6GeVNsLHXKIsNHj3s5W5c
NSk0lAR4sQ6dnEkzAua0S8X8mJuovuWyWp6kd0PhXVDUTuONXjdlfi2vSLuc3gAex2NZl0gAvQz8
V+TO4JQje13GXjRnvkOufHQYr4+PLNWx/PLD6SE3iNbDByChrN2TxZJsXPzCAiPwnPMg1TBHKxg0
+4q81l77b4hLbhjfWDZ3msNtqHAXvSWXC5Ll3V3HsjbEqG97TSgIyAp6HaMteEle7F87lGZlomta
K8VrNDXJ7nbik2WzUSiYgKk5ShXrrDbk5bS3r7uUuMEYGD7ybliko4DdPWLOYn98/DZMzkLnEdqZ
EI5rF/OjzLVKKv5Hl70M9YH5mqmnsJq16DFj7QSTb85cepZstDT7pjmB9AzCEfdKoBdXgbIfuR7V
dXWUbdI8yzK0wOqITBdnZaFsnsoq2qesDHyrIEriYDc0kQqqn5q2AoiGDpGWv1Q0uzDO93d5n4H5
FX+GuXrJ4vBIZOwFmsvCKHZfW0hAnu+r08MouZmrCxvZaRm98ePX/PzOGy2cho7NmT7yhfF8AK1r
6SSbkzb7o5gS37n9cVN6zoZJnX5FqUkclCdrNATTyjrhdHd8qPwmIDj5tuuypuy1qAdMDAj4Lz/s
S1Yi9OUo7zq5GCemG78R6LIlYYmYZZ3sf9sJmIWyJxBmbx2a22asCRE0ofzheFK0+8xGnucIvdHw
zm330Zjjo4FICDm/yULXweOdo5iL3j9qfMthLsOyrq1hQMqCYGgZtpAAC7tB1BHBO4kxGoBqwPct
3u/d54GEVLwqAda2J/U4ABvirPQYTUZc+LNUmEYJ3Ntxji0hStjqQ1fhei14B/YufzwSkDPLu46W
VG9q2qJNGtMcbt14MqADsMBLeJXleBqa0gIgU5vqe8uUG6ILJcuMrSQmNieDigb+vjoyJb/ctylb
GfcDvzeZV5idNYkXZEV89d7ctDDqvz9GSkQMItM1by8x8iTPE2PV7OdRFW1s3dnjZmGxIfOFF1f7
3aSb2O1Sxs/VMg6IEhjaEHYmacunGa9oFt0ma4AyC6EzLjUELUqZCDs+24GRiOUbZNHp78+10Zg4
HcCsGiR6CBY8lIGrIYRBireubviZAmXaP68F5UIWai6LTlPd0Bmwog35/L2xcuUd6OahBzMdmfBT
EzrOf1MMTMql9Vbv/83UYL3zvwsIGbM/7m0q7QyUQ+B7c1jXBUH+6aYV0NprowOkm0kiH+ss1SoS
0V6ILApCKbZ021EhxtNHANKZxsVRbHlowXLo9hBeoXQyfDZ/Bm+6PCIq6AGFEBjHjnXxFPEKcuCG
yLo+v6l2uhegyjE9wpKGUlDtS/f4qdujr8kzlkq34bo+E51E8tfsuot7rEMbUmNG5/VwLhGRkIrc
x7Ew7a/kW8b1tQdVCcoLEEyrbDYUvtU6vBvjJ5keRsmFYojy0I8FZvCflkFFmz7tMS4QHclHxynl
2Gq/V8iBN7PGwIz/8uyMIC5bnQCnSRHel8GJ1L7NBJrzsiCKDoh7ojOhNRjiyisGQLqe/3oLJgls
SELL0cGICY8tRw5FV0TnrEjP8VOO6oAkPp7t6lrNaYthwkRkkSX4fEcs6EI1qlPHI0x1RgiW6nYI
inNsHflXd0PtPo8eQcm8ICXq2w9/qpIJWaO9y+h0jhWnGlurzjBbZeWpeTXwJXhR5L6h7MFe7lsw
/LfSvkKDP9LgY8wSPdfkBDaYaGkgSjC4SyX0NeUUSEqxc+0jXEFE7GD/GwuYE+juXIdTPbHSvvgR
SBUfCk5i4zJGsp1EnJyBFLlCutkbsrCsTEHcwXQfXm9Qf0PyA/mFps9gveJlWkrm2qvC9FjUkbij
iPmwUWEDwyuMdV3TKsnZs8OAfqqcIpttTsv4Udvugyf1C/b+Q9IL1Lw/2bOz2vfRyuyeWC0zKyY8
pgvnKnq6iptbF9AvDwvQsrh+P1ALJhtKqNDUU+MQX0oZQW02XA0CrryDVAibyQzxS/AFkcayYElr
Ev83tnt8vh6759UZ4ItpIJ43cbiEIwaY8VqUUVxYFaN1UNIAXjfn4jIB1uHdiQ+TovgrH+Ftk9PL
K03o2f1bMg7XSADhDrlq2/Ct/+Wv6a94BVbKPMlbh2IoCVdV9ZWQhci/4bR1fCp9nZfNzDU1m/lt
XoXHsj6G0K/lD7b2cSWVXhM+U2kfZX167C4aCRLPbFA6nQGoCMQsRmDA4csjl3bwWryiiyFboJRB
7nNEQZL5KRJC2ekjrHaS9hQ3FBh0qmcxOdAw23QERv6HfaZWUKZof3tl3mraLgJ6V4dWLKgRPPQR
g5TkcLYsgySk/5slMsZu34bLImp0mdDnQ9E9BqI+mutwmtXlUbbi1T6YGIPezYiv8U62ThLMp1F9
OMFXlsjOvKNsVn4Zl8rqJ71pcjM6QIcLIaX2jeT2jXGwLhko0xJ97jkW0vbC3iv4JOSALXI/p9Yb
AoSvXJK/JP/eIXWzn134TSC5LqpeHRlUx+xQIXZvoaekp0FYZsoAAhtVSckcsK8qhHToSD3FFE5h
tw6cSuv0fERK6wu4wNv9U8pQc0LcfN8PcMhLS4gZLl0htvh42p2SznHjdUtbamdKam+g21pEu8Pq
akByZIc9e6IVbUgmuLqajt1eNFpeAOy8zuXqXJwFZXUs3+BBpbmds4Fj5CqLpXL9xRkEXC4bfCm7
HxcUEZv8CDeKF9vFLyMxfShAKPDoUZRFt5Rh1b2gXFT5yN0Wm9u/tOi/EoSYzMtuC1Sxu30crw4r
TC3JJ9HlMvOJkvZytydyiWwJi9/fyJKgI4cQ+N89Ozb6qNBg+PTdqouJlAIj7PuRqp16BgtJyfcL
bH28xQDpd1wGGGLTqIkqZ9aJkvNL7uaNCjaNyESXpW4nCGY+MJOWPuDaxTkHEADdCm0DYmISPp7G
nVVLgg4LyymdjWHnkotKlRS3CjwBSGKa7Fi8aQrXrubm39OEXgnEo6ObXcsdGQSKGQRapv3Z74xo
MunjaHlLKZ4SMJnuVyERq0UaJuKnQTi1EXoTbpcW0KHvlEFH4U5moLSZzVczWCqZMszwnL/TIU3b
ACs8+Ux4QDmIoT/w9c4ArKx35V3Aa25IyYxwmubQKIqJOUv4docWlA8TMziP73cHTDQhMJiifQzc
41TYu/0o2e0C07Ud4QOcDtr17itBoRUXC7cg+YQV2aO8f+ImnaiXMlUkix/jaWUi/ehALKSLxW3c
Ai8292NNC17tXGGZAyyOYZC7K0EefprEXsgmdJiIpiWgDi+eWlEPdbKX1Y5/JTbg8rfSZL16/0Jo
KeGAjNyu7XnuEuDW03Scb14OpKJLiS8HunELDN+bjTypmR7ySsLSEtTBTcCfXifi0Hcv7TdwwJLh
e0nD5vaONUcxwTyM5b8pRps+ZjbS9/frb2Ly+2dVEy4yFSgFOHU2V0e5pYmqguKSODL+xd0FORLS
4b02Epbk2HbrJLq7WIImyiyHDhbg97oSM243eshPMJ9Kdx5XsX86Eaf5uqgF+zcd0xTqni0de/lA
xVKj9ofWw1bIUATF21sEVmYAarpSjbdhuE4sDjibZrdyAHwW4yUsj6+/pn5fG86960DaHANKqQCp
1RBScVdmzHrH6iQlKZH6rwDkgmX38vb3w+MqTfLl2h8V7hoWSeSZJAZAKokwwXlM8/6UQPqGjiHd
EGmdOx0B6AC/1P4mnKZC7v3gIeZwGBhRDCgrndtGL8YnZnuqlIUjFec/PXYIDvl7GHUSWPyEXe9Q
qnoazQKskg0zUT/iBc2uFsK8Q1B9SzfZsckSYcjmDDZ7KGpnxflzFVZBBk0fk3blg0GplZZBd+eP
7Q9GSHQ5lOntKK41UwuKFbfFNjPfe+woaYsWqWSm5B8RETBEuSyRfXsNdNucm62CXT4cBCj0kqZI
+8Cfqm0ZQRSx7MpukbF2Xk/2Iyy6HOOmocRBa7qAHzxgzJ9zUcUoeggptbq3ZBk8olW97FxGPKfN
IoGMrVY58/jHOTWlXmTg5Kk+3TdGdVuergqgvUEhZNHT9Sd1JGrDfOc1+hmx4A9uo3+M5qVFjQl6
3TLfuerQ/lyULMq/bhBFW5VDD+4lqkYGdvh6fyaNpA3nQN6nZPaZauuiGhuue54rsGxZkLPdUISh
GDuRMYvPfRpoKKR2wIFLjIyOGJWosD7VLaQ55g2DezI+8kbhsgrg8npIMxWb8ASxqt/rmQ5+Qg9W
QTe+VDu40qqi7L4TkLL+9P8TqE3UNjsy6Zy/8pUWWKHLXt7FR9Smk5wTid1L1g9HqZ4EHLzT+bsy
qXm7DzoFKV6oq00sLAAYQpUotZG6A73r5xwK99w5qydXzmcuP9YPJqXrM3ZDosONWnHjF1gSpzMg
pUs5BDR8q7Eh+AnwYxZCebX/uxKrvT2oYZ+zw7x5Giaw5Z2YJDA47UKpasqbsDIaToSpr2pi+a0k
UeG1sqHVXFay4O7+7PEQ7GPdZJdwaFXhu9BT47j6v2OEZizS6MATLrDnP7g9A0NGVkMYqOzvaFak
fKDWf2M9/ScimZzYcV2EaLtIzSBuTMi0Orz1rJXVCfEgkkZNcGO7u1wWJvEtlH/Y1GOFe9WUVFZU
c8pJTG5YWOd28u0lS3h5DX9jFM7sBkImfq5Ikuoedtw0XOnkMJsOg7cd6P6dGBiSONFwGoIURvEh
8lNKG+lABiC4+iGAT0fYrteFisnKFRsq9BNn
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt is
  port (
    gtxe2_i : out STD_LOGIC;
    gt0_cpllrefclklost_i : out STD_LOGIC;
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    gtxe2_i_0 : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    gtxe2_i_1 : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_2 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_4 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_5 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_gttxreset_in0_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gt0_rxuserrdy_t : in STD_LOGIC;
    gtxe2_i_7 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_txuserrdy_t : in STD_LOGIC;
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_8 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_9 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_10 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_cpllreset_t : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt is
  signal cpll_pd0_i : STD_LOGIC;
  signal cpllreset_in : STD_LOGIC;
begin
cpll_railing0_i: entity work.gig_ethernet_pcs_pma_0_cpll_railing
     port map (
      cpll_pd0_i => cpll_pd0_i,
      cpllreset_in => cpllreset_in,
      gt0_cpllreset_t => gt0_cpllreset_t,
      gtrefclk_bufg => gtrefclk_bufg
    );
gt0_GTWIZARD_i: entity work.gig_ethernet_pcs_pma_0_GTWIZARD_GT
     port map (
      D(1 downto 0) => D(1 downto 0),
      Q(15 downto 0) => Q(15 downto 0),
      RXBUFSTATUS(0) => RXBUFSTATUS(0),
      RXPD(0) => RXPD(0),
      SR(0) => SR(0),
      TXBUFSTATUS(0) => TXBUFSTATUS(0),
      TXPD(0) => TXPD(0),
      cpll_pd0_i => cpll_pd0_i,
      cpllreset_in => cpllreset_in,
      gt0_cpllrefclklost_i => gt0_cpllrefclklost_i,
      gt0_gttxreset_in0_out => gt0_gttxreset_in0_out,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gt0_rxuserrdy_t => gt0_rxuserrdy_t,
      gt0_txuserrdy_t => gt0_txuserrdy_t,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i_0 => gtxe2_i,
      gtxe2_i_1 => gtxe2_i_0,
      gtxe2_i_10(1 downto 0) => gtxe2_i_9(1 downto 0),
      gtxe2_i_11(1 downto 0) => gtxe2_i_10(1 downto 0),
      gtxe2_i_2 => gtxe2_i_1,
      gtxe2_i_3(15 downto 0) => gtxe2_i_2(15 downto 0),
      gtxe2_i_4(1 downto 0) => gtxe2_i_3(1 downto 0),
      gtxe2_i_5(1 downto 0) => gtxe2_i_4(1 downto 0),
      gtxe2_i_6(1 downto 0) => gtxe2_i_5(1 downto 0),
      gtxe2_i_7(1 downto 0) => gtxe2_i_6(1 downto 0),
      gtxe2_i_8 => gtxe2_i_7,
      gtxe2_i_9(1 downto 0) => gtxe2_i_8(1 downto 0),
      independent_clock_bufg => independent_clock_bufg,
      reset => reset,
      reset_out => reset_out,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_RX_STARTUP_FSM is
  port (
    data_in : out STD_LOGIC;
    gt0_rxuserrdy_t : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    independent_clock_bufg : in STD_LOGIC;
    data_sync_reg6 : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_0\ : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    data_out : in STD_LOGIC;
    data_sync_reg1_1 : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_RX_STARTUP_FSM;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_RX_STARTUP_FSM is
  signal \FSM_sequential_rx_state[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[1]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_10_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_9_n_0\ : STD_LOGIC;
  signal GTRXRESET : STD_LOGIC;
  signal RXUSERRDY_i_1_n_0 : STD_LOGIC;
  signal check_tlock_max_i_1_n_0 : STD_LOGIC;
  signal check_tlock_max_reg_n_0 : STD_LOGIC;
  signal \^data_in\ : STD_LOGIC;
  signal data_out_0 : STD_LOGIC;
  signal \^gt0_rxuserrdy_t\ : STD_LOGIC;
  signal gtrxreset_i_i_1_n_0 : STD_LOGIC;
  signal \init_wait_count[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_1__0_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_3__0_n_0\ : STD_LOGIC;
  signal init_wait_count_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \init_wait_done_i_1__0_n_0\ : STD_LOGIC;
  signal init_wait_done_reg_n_0 : STD_LOGIC;
  signal \mmcm_lock_count[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[6]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_2__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_3__0_n_0\ : STD_LOGIC;
  signal mmcm_lock_count_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal mmcm_lock_i : STD_LOGIC;
  signal mmcm_lock_reclocked : STD_LOGIC;
  signal mmcm_lock_reclocked_i_1_n_0 : STD_LOGIC;
  signal \mmcm_lock_reclocked_i_2__0_n_0\ : STD_LOGIC;
  signal \p_0_in__2\ : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \p_0_in__3\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal reset_time_out_i_3_n_0 : STD_LOGIC;
  signal reset_time_out_i_4_n_0 : STD_LOGIC;
  signal reset_time_out_reg_n_0 : STD_LOGIC;
  signal \run_phase_alignment_int_i_1__0_n_0\ : STD_LOGIC;
  signal run_phase_alignment_int_reg_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_s3_reg_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_5_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_6_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_s2 : STD_LOGIC;
  signal rx_fsm_reset_done_int_s3 : STD_LOGIC;
  signal rx_state : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \rx_state__0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rxresetdone_s2 : STD_LOGIC;
  signal rxresetdone_s3 : STD_LOGIC;
  signal sync_cplllock_n_0 : STD_LOGIC;
  signal sync_data_valid_n_0 : STD_LOGIC;
  signal sync_data_valid_n_1 : STD_LOGIC;
  signal sync_data_valid_n_5 : STD_LOGIC;
  signal sync_mmcm_lock_reclocked_n_0 : STD_LOGIC;
  signal time_out_100us_i_1_n_0 : STD_LOGIC;
  signal time_out_100us_i_2_n_0 : STD_LOGIC;
  signal time_out_100us_i_3_n_0 : STD_LOGIC;
  signal time_out_100us_reg_n_0 : STD_LOGIC;
  signal time_out_1us_i_1_n_0 : STD_LOGIC;
  signal time_out_1us_i_2_n_0 : STD_LOGIC;
  signal time_out_1us_i_3_n_0 : STD_LOGIC;
  signal time_out_1us_reg_n_0 : STD_LOGIC;
  signal time_out_2ms_i_1_n_0 : STD_LOGIC;
  signal time_out_2ms_i_2_n_0 : STD_LOGIC;
  signal \time_out_2ms_i_3__0_n_0\ : STD_LOGIC;
  signal time_out_2ms_i_4_n_0 : STD_LOGIC;
  signal time_out_2ms_reg_n_0 : STD_LOGIC;
  signal time_out_counter : STD_LOGIC;
  signal \time_out_counter[0]_i_3_n_0\ : STD_LOGIC;
  signal time_out_counter_reg : STD_LOGIC_VECTOR ( 18 downto 0 );
  signal \time_out_counter_reg[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_7\ : STD_LOGIC;
  signal time_out_wait_bypass_i_1_n_0 : STD_LOGIC;
  signal \time_out_wait_bypass_i_2__0_n_0\ : STD_LOGIC;
  signal \time_out_wait_bypass_i_3__0_n_0\ : STD_LOGIC;
  signal \time_out_wait_bypass_i_4__0_n_0\ : STD_LOGIC;
  signal time_out_wait_bypass_reg_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_s2 : STD_LOGIC;
  signal time_out_wait_bypass_s3 : STD_LOGIC;
  signal time_tlock_max : STD_LOGIC;
  signal time_tlock_max1 : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_1\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_2\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_3\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_n_3\ : STD_LOGIC;
  signal time_tlock_max1_carry_i_1_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_2_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_3_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_4_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_5_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_6_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_7_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_8_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_n_1 : STD_LOGIC;
  signal time_tlock_max1_carry_n_2 : STD_LOGIC;
  signal time_tlock_max1_carry_n_3 : STD_LOGIC;
  signal time_tlock_max_i_1_n_0 : STD_LOGIC;
  signal \wait_bypass_count[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_4_n_0\ : STD_LOGIC;
  signal wait_bypass_count_reg : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \wait_bypass_count_reg[0]_i_3__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_7\ : STD_LOGIC;
  signal \wait_time_cnt0__0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \wait_time_cnt[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_2__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_3__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_4__0_n_0\ : STD_LOGIC;
  signal wait_time_cnt_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_time_tlock_max1_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_time_tlock_max1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_time_tlock_max1_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_time_tlock_max1_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[2]_i_2\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[3]_i_10\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[3]_i_9\ : label is "soft_lutpair43";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[0]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[1]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[2]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[3]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute SOFT_HLUTNM of check_tlock_max_i_1 : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \init_wait_count[1]_i_1__0\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \init_wait_count[2]_i_1__0\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \init_wait_count[3]_i_1__0\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \init_wait_count[4]_i_1__0\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_2__0\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_3__0\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \mmcm_lock_count[1]_i_1__0\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \mmcm_lock_count[2]_i_1__0\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \mmcm_lock_count[3]_i_1__0\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \mmcm_lock_count[4]_i_1__0\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \mmcm_lock_count[6]_i_1__0\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \mmcm_lock_count[7]_i_3__0\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of reset_time_out_i_3 : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of reset_time_out_i_4 : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of rx_fsm_reset_done_int_i_5 : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of rx_fsm_reset_done_int_i_6 : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of time_out_1us_i_2 : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of time_out_2ms_i_1 : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \time_out_2ms_i_3__0\ : label is "soft_lutpair51";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[0]_i_2__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[12]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[16]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[4]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[8]_i_1__0\ : label is 11;
  attribute COMPARATOR_THRESHOLD : integer;
  attribute COMPARATOR_THRESHOLD of time_tlock_max1_carry : label is 11;
  attribute COMPARATOR_THRESHOLD of \time_tlock_max1_carry__0\ : label is 11;
  attribute COMPARATOR_THRESHOLD of \time_tlock_max1_carry__1\ : label is 11;
  attribute SOFT_HLUTNM of time_tlock_max_i_1 : label is "soft_lutpair52";
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[0]_i_3__0\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[12]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[4]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[8]_i_1__0\ : label is 11;
  attribute SOFT_HLUTNM of \wait_time_cnt[0]_i_1__0\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \wait_time_cnt[1]_i_1__0\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \wait_time_cnt[3]_i_1__0\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \wait_time_cnt[4]_i_1__0\ : label is "soft_lutpair45";
begin
  data_in <= \^data_in\;
  gt0_rxuserrdy_t <= \^gt0_rxuserrdy_t\;
\FSM_sequential_rx_state[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222AAAA00000C00"
    )
        port map (
      I0 => time_out_2ms_reg_n_0,
      I1 => rx_state(2),
      I2 => rx_state(3),
      I3 => time_tlock_max,
      I4 => reset_time_out_reg_n_0,
      I5 => rx_state(1),
      O => \FSM_sequential_rx_state[0]_i_2_n_0\
    );
\FSM_sequential_rx_state[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000AABF000F0000"
    )
        port map (
      I0 => reset_time_out_reg_n_0,
      I1 => time_tlock_max,
      I2 => rx_state(2),
      I3 => rx_state(3),
      I4 => rx_state(1),
      I5 => rx_state(0),
      O => \FSM_sequential_rx_state[1]_i_3_n_0\
    );
\FSM_sequential_rx_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000050FF2200"
    )
        port map (
      I0 => rx_state(1),
      I1 => time_out_2ms_reg_n_0,
      I2 => \FSM_sequential_rx_state[2]_i_2_n_0\,
      I3 => rx_state(0),
      I4 => rx_state(2),
      I5 => rx_state(3),
      O => \rx_state__0\(2)
    );
\FSM_sequential_rx_state[2]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => reset_time_out_reg_n_0,
      I1 => time_tlock_max,
      O => \FSM_sequential_rx_state[2]_i_2_n_0\
    );
\FSM_sequential_rx_state[3]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => reset_time_out_reg_n_0,
      I1 => time_out_2ms_reg_n_0,
      O => \FSM_sequential_rx_state[3]_i_10_n_0\
    );
\FSM_sequential_rx_state[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000050005300"
    )
        port map (
      I0 => \FSM_sequential_rx_state[3]_i_10_n_0\,
      I1 => \wait_time_cnt[6]_i_4__0_n_0\,
      I2 => rx_state(0),
      I3 => rx_state(1),
      I4 => wait_time_cnt_reg(6),
      I5 => rx_state(3),
      O => \FSM_sequential_rx_state[3]_i_3_n_0\
    );
\FSM_sequential_rx_state[3]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000023002F00"
    )
        port map (
      I0 => time_out_2ms_reg_n_0,
      I1 => rx_state(2),
      I2 => rx_state(1),
      I3 => rx_state(0),
      I4 => \FSM_sequential_rx_state[2]_i_2_n_0\,
      I5 => rx_state(3),
      O => \FSM_sequential_rx_state[3]_i_7_n_0\
    );
\FSM_sequential_rx_state[3]_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80800080"
    )
        port map (
      I0 => rx_state(0),
      I1 => rx_state(1),
      I2 => rx_state(2),
      I3 => time_out_2ms_reg_n_0,
      I4 => reset_time_out_reg_n_0,
      O => \FSM_sequential_rx_state[3]_i_9_n_0\
    );
\FSM_sequential_rx_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(0),
      Q => rx_state(0),
      R => \out\(0)
    );
\FSM_sequential_rx_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(1),
      Q => rx_state(1),
      R => \out\(0)
    );
\FSM_sequential_rx_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(2),
      Q => rx_state(2),
      R => \out\(0)
    );
\FSM_sequential_rx_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(3),
      Q => rx_state(3),
      R => \out\(0)
    );
RXUSERRDY_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB4000"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(0),
      I2 => rx_state(2),
      I3 => rx_state(1),
      I4 => \^gt0_rxuserrdy_t\,
      O => RXUSERRDY_i_1_n_0
    );
RXUSERRDY_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => RXUSERRDY_i_1_n_0,
      Q => \^gt0_rxuserrdy_t\,
      R => \out\(0)
    );
check_tlock_max_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB0008"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(0),
      I2 => rx_state(1),
      I3 => rx_state(3),
      I4 => check_tlock_max_reg_n_0,
      O => check_tlock_max_i_1_n_0
    );
check_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => check_tlock_max_i_1_n_0,
      Q => check_tlock_max_reg_n_0,
      R => \out\(0)
    );
gtrxreset_i_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0100"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(1),
      I2 => rx_state(2),
      I3 => rx_state(0),
      I4 => GTRXRESET,
      O => gtrxreset_i_i_1_n_0
    );
gtrxreset_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gtrxreset_i_i_1_n_0,
      Q => GTRXRESET,
      R => \out\(0)
    );
gtxe2_i_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => GTRXRESET,
      I1 => \^data_in\,
      I2 => gtxe2_i,
      O => SR(0)
    );
\init_wait_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => init_wait_count_reg(0),
      O => \init_wait_count[0]_i_1__0_n_0\
    );
\init_wait_count[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      O => \p_0_in__2\(1)
    );
\init_wait_count[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      O => \p_0_in__2\(2)
    );
\init_wait_count[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => init_wait_count_reg(1),
      I1 => init_wait_count_reg(2),
      I2 => init_wait_count_reg(0),
      I3 => init_wait_count_reg(3),
      O => \p_0_in__2\(3)
    );
\init_wait_count[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      O => \p_0_in__2\(4)
    );
\init_wait_count[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      I5 => init_wait_count_reg(5),
      O => \p_0_in__2\(5)
    );
\init_wait_count[6]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => \init_wait_count[6]_i_3__0_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \init_wait_count[6]_i_1__0_n_0\
    );
\init_wait_count[6]_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF40"
    )
        port map (
      I0 => \init_wait_count[6]_i_3__0_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \p_0_in__2\(6)
    );
\init_wait_count[6]_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => init_wait_count_reg(3),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      I3 => init_wait_count_reg(5),
      O => \init_wait_count[6]_i_3__0_n_0\
    );
\init_wait_count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \init_wait_count[0]_i_1__0_n_0\,
      Q => init_wait_count_reg(0)
    );
\init_wait_count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(1),
      Q => init_wait_count_reg(1)
    );
\init_wait_count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(2),
      Q => init_wait_count_reg(2)
    );
\init_wait_count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(3),
      Q => init_wait_count_reg(3)
    );
\init_wait_count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(4),
      Q => init_wait_count_reg(4)
    );
\init_wait_count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(5),
      Q => init_wait_count_reg(5)
    );
\init_wait_count_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(6),
      Q => init_wait_count_reg(6)
    );
\init_wait_done_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0010"
    )
        port map (
      I0 => \init_wait_count[6]_i_3__0_n_0\,
      I1 => init_wait_count_reg(4),
      I2 => init_wait_count_reg(6),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_done_reg_n_0,
      O => \init_wait_done_i_1__0_n_0\
    );
init_wait_done_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      CLR => \out\(0),
      D => \init_wait_done_i_1__0_n_0\,
      Q => init_wait_done_reg_n_0
    );
\mmcm_lock_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      O => \p_0_in__3\(0)
    );
\mmcm_lock_count[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      I1 => mmcm_lock_count_reg(1),
      O => \p_0_in__3\(1)
    );
\mmcm_lock_count[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => mmcm_lock_count_reg(1),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(2),
      O => \mmcm_lock_count[2]_i_1__0_n_0\
    );
\mmcm_lock_count[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => mmcm_lock_count_reg(2),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(1),
      I3 => mmcm_lock_count_reg(3),
      O => \mmcm_lock_count[3]_i_1__0_n_0\
    );
\mmcm_lock_count[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => mmcm_lock_count_reg(3),
      I1 => mmcm_lock_count_reg(1),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(2),
      I4 => mmcm_lock_count_reg(4),
      O => \mmcm_lock_count[4]_i_1__0_n_0\
    );
\mmcm_lock_count[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => \mmcm_lock_count[5]_i_1__0_n_0\
    );
\mmcm_lock_count[6]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I1 => mmcm_lock_count_reg(6),
      O => \mmcm_lock_count[6]_i_1__0_n_0\
    );
\mmcm_lock_count[7]_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I1 => mmcm_lock_count_reg(6),
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_2__0_n_0\
    );
\mmcm_lock_count[7]_i_3__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => mmcm_lock_count_reg(6),
      I1 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_3__0_n_0\
    );
\mmcm_lock_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \p_0_in__3\(0),
      Q => mmcm_lock_count_reg(0),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \p_0_in__3\(1),
      Q => mmcm_lock_count_reg(1),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[2]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(2),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[3]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(3),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[4]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(4),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[5]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(5),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[6]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(6),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[7]_i_3__0_n_0\,
      Q => mmcm_lock_count_reg(7),
      R => sync_mmcm_lock_reclocked_n_0
    );
mmcm_lock_reclocked_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEA0000"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => mmcm_lock_count_reg(7),
      I2 => mmcm_lock_count_reg(6),
      I3 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I4 => mmcm_lock_i,
      O => mmcm_lock_reclocked_i_1_n_0
    );
\mmcm_lock_reclocked_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => \mmcm_lock_reclocked_i_2__0_n_0\
    );
mmcm_lock_reclocked_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => mmcm_lock_reclocked_i_1_n_0,
      Q => mmcm_lock_reclocked,
      R => '0'
    );
reset_time_out_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(3),
      O => reset_time_out_i_3_n_0
    );
reset_time_out_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"34347674"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(3),
      I2 => rx_state(0),
      I3 => \FSM_sequential_rx_state_reg[0]_0\,
      I4 => rx_state(1),
      O => reset_time_out_i_4_n_0
    );
reset_time_out_reg: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => sync_data_valid_n_0,
      Q => reset_time_out_reg_n_0,
      S => \out\(0)
    );
\run_phase_alignment_int_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFF0010"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(1),
      I2 => rx_state(3),
      I3 => rx_state(0),
      I4 => run_phase_alignment_int_reg_n_0,
      O => \run_phase_alignment_int_i_1__0_n_0\
    );
run_phase_alignment_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => \run_phase_alignment_int_i_1__0_n_0\,
      Q => run_phase_alignment_int_reg_n_0,
      R => \out\(0)
    );
run_phase_alignment_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => '1',
      D => data_out_0,
      Q => run_phase_alignment_int_s3_reg_n_0,
      R => '0'
    );
rx_fsm_reset_done_int_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => rx_state(1),
      I1 => rx_state(0),
      O => rx_fsm_reset_done_int_i_5_n_0
    );
rx_fsm_reset_done_int_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(2),
      O => rx_fsm_reset_done_int_i_6_n_0
    );
rx_fsm_reset_done_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => sync_data_valid_n_1,
      Q => \^data_in\,
      R => \out\(0)
    );
rx_fsm_reset_done_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => '1',
      D => rx_fsm_reset_done_int_s2,
      Q => rx_fsm_reset_done_int_s3,
      R => '0'
    );
rxresetdone_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => rxresetdone_s2,
      Q => rxresetdone_s3,
      R => '0'
    );
sync_RXRESETDONE: entity work.gig_ethernet_pcs_pma_0_sync_block_10
     port map (
      data_out => rxresetdone_s2,
      data_sync_reg1_0 => data_sync_reg1,
      independent_clock_bufg => independent_clock_bufg
    );
sync_cplllock: entity work.gig_ethernet_pcs_pma_0_sync_block_11
     port map (
      \FSM_sequential_rx_state_reg[1]\ => sync_cplllock_n_0,
      Q(2 downto 0) => rx_state(3 downto 1),
      data_sync_reg1_0 => data_sync_reg1_1,
      independent_clock_bufg => independent_clock_bufg,
      rxresetdone_s3 => rxresetdone_s3
    );
sync_data_valid: entity work.gig_ethernet_pcs_pma_0_sync_block_12
     port map (
      D(2) => \rx_state__0\(3),
      D(1 downto 0) => \rx_state__0\(1 downto 0),
      E(0) => sync_data_valid_n_5,
      \FSM_sequential_rx_state_reg[0]\ => \FSM_sequential_rx_state[3]_i_3_n_0\,
      \FSM_sequential_rx_state_reg[0]_0\ => \FSM_sequential_rx_state[3]_i_7_n_0\,
      \FSM_sequential_rx_state_reg[0]_1\ => \FSM_sequential_rx_state_reg[0]_0\,
      \FSM_sequential_rx_state_reg[0]_2\ => \FSM_sequential_rx_state[0]_i_2_n_0\,
      \FSM_sequential_rx_state_reg[0]_3\ => init_wait_done_reg_n_0,
      \FSM_sequential_rx_state_reg[1]\ => sync_data_valid_n_0,
      \FSM_sequential_rx_state_reg[1]_0\ => \FSM_sequential_rx_state[1]_i_3_n_0\,
      \FSM_sequential_rx_state_reg[3]\ => \FSM_sequential_rx_state[3]_i_9_n_0\,
      Q(3 downto 0) => rx_state(3 downto 0),
      data_in => \^data_in\,
      data_out => data_out,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_lock_reclocked => mmcm_lock_reclocked,
      reset_time_out_reg => sync_cplllock_n_0,
      reset_time_out_reg_0 => reset_time_out_i_3_n_0,
      reset_time_out_reg_1 => reset_time_out_i_4_n_0,
      reset_time_out_reg_2 => reset_time_out_reg_n_0,
      rx_fsm_reset_done_int_reg => sync_data_valid_n_1,
      rx_fsm_reset_done_int_reg_0 => rx_fsm_reset_done_int_i_5_n_0,
      rx_fsm_reset_done_int_reg_1 => time_out_100us_reg_n_0,
      rx_fsm_reset_done_int_reg_2 => time_out_1us_reg_n_0,
      rx_fsm_reset_done_int_reg_3 => rx_fsm_reset_done_int_i_6_n_0,
      time_out_wait_bypass_s3 => time_out_wait_bypass_s3
    );
sync_mmcm_lock_reclocked: entity work.gig_ethernet_pcs_pma_0_sync_block_13
     port map (
      SR(0) => sync_mmcm_lock_reclocked_n_0,
      data_out => mmcm_lock_i,
      data_sync_reg1_0 => data_sync_reg1_0,
      independent_clock_bufg => independent_clock_bufg
    );
sync_run_phase_alignment_int: entity work.gig_ethernet_pcs_pma_0_sync_block_14
     port map (
      data_in => run_phase_alignment_int_reg_n_0,
      data_out => data_out_0,
      data_sync_reg1_0 => data_sync_reg6
    );
sync_time_out_wait_bypass: entity work.gig_ethernet_pcs_pma_0_sync_block_15
     port map (
      data_in => time_out_wait_bypass_reg_n_0,
      data_out => time_out_wait_bypass_s2,
      independent_clock_bufg => independent_clock_bufg
    );
sync_tx_fsm_reset_done_int: entity work.gig_ethernet_pcs_pma_0_sync_block_16
     port map (
      data_in => \^data_in\,
      data_out => rx_fsm_reset_done_int_s2,
      data_sync_reg6_0 => data_sync_reg6
    );
time_out_100us_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000100"
    )
        port map (
      I0 => time_out_2ms_i_4_n_0,
      I1 => time_out_counter_reg(17),
      I2 => time_out_counter_reg(16),
      I3 => time_out_100us_i_2_n_0,
      I4 => time_out_100us_i_3_n_0,
      I5 => time_out_100us_reg_n_0,
      O => time_out_100us_i_1_n_0
    );
time_out_100us_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000002000000000"
    )
        port map (
      I0 => time_out_counter_reg(10),
      I1 => time_out_counter_reg(12),
      I2 => time_out_counter_reg(5),
      I3 => time_out_counter_reg(7),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(14),
      O => time_out_100us_i_2_n_0
    );
time_out_100us_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => time_out_counter_reg(4),
      I1 => time_out_counter_reg(0),
      I2 => time_out_counter_reg(1),
      I3 => time_out_counter_reg(15),
      I4 => time_out_counter_reg(13),
      O => time_out_100us_i_3_n_0
    );
time_out_100us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_100us_i_1_n_0,
      Q => time_out_100us_reg_n_0,
      R => reset_time_out_reg_n_0
    );
time_out_1us_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00100000"
    )
        port map (
      I0 => time_out_2ms_i_2_n_0,
      I1 => time_out_1us_i_2_n_0,
      I2 => time_out_counter_reg(3),
      I3 => time_out_counter_reg(2),
      I4 => time_out_1us_i_3_n_0,
      I5 => time_out_1us_reg_n_0,
      O => time_out_1us_i_1_n_0
    );
time_out_1us_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(17),
      O => time_out_1us_i_2_n_0
    );
time_out_1us_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000010"
    )
        port map (
      I0 => time_out_counter_reg(9),
      I1 => time_out_counter_reg(11),
      I2 => time_out_counter_reg(6),
      I3 => time_out_counter_reg(8),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(12),
      O => time_out_1us_i_3_n_0
    );
time_out_1us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_1us_i_1_n_0,
      Q => time_out_1us_reg_n_0,
      R => reset_time_out_reg_n_0
    );
time_out_2ms_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF01"
    )
        port map (
      I0 => time_out_2ms_i_2_n_0,
      I1 => \time_out_2ms_i_3__0_n_0\,
      I2 => time_out_2ms_i_4_n_0,
      I3 => time_out_2ms_reg_n_0,
      O => time_out_2ms_i_1_n_0
    );
time_out_2ms_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFEFF"
    )
        port map (
      I0 => time_out_counter_reg(10),
      I1 => time_out_counter_reg(14),
      I2 => time_out_counter_reg(5),
      I3 => time_out_counter_reg(7),
      I4 => time_out_100us_i_3_n_0,
      O => time_out_2ms_i_2_n_0
    );
\time_out_2ms_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => time_out_counter_reg(12),
      I1 => time_out_counter_reg(16),
      I2 => time_out_counter_reg(18),
      I3 => time_out_counter_reg(17),
      O => \time_out_2ms_i_3__0_n_0\
    );
time_out_2ms_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFEFFFFFF"
    )
        port map (
      I0 => time_out_counter_reg(2),
      I1 => time_out_counter_reg(3),
      I2 => time_out_counter_reg(8),
      I3 => time_out_counter_reg(9),
      I4 => time_out_counter_reg(11),
      I5 => time_out_counter_reg(6),
      O => time_out_2ms_i_4_n_0
    );
time_out_2ms_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_2ms_i_1_n_0,
      Q => time_out_2ms_reg_n_0,
      R => reset_time_out_reg_n_0
    );
\time_out_counter[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFF7FF"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(18),
      I2 => time_out_counter_reg(16),
      I3 => time_out_counter_reg(12),
      I4 => time_out_2ms_i_2_n_0,
      I5 => time_out_2ms_i_4_n_0,
      O => time_out_counter
    );
\time_out_counter[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(0),
      O => \time_out_counter[0]_i_3_n_0\
    );
\time_out_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_7\,
      Q => time_out_counter_reg(0),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[0]_i_2__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \time_out_counter_reg[0]_i_2__0_n_0\,
      CO(2) => \time_out_counter_reg[0]_i_2__0_n_1\,
      CO(1) => \time_out_counter_reg[0]_i_2__0_n_2\,
      CO(0) => \time_out_counter_reg[0]_i_2__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \time_out_counter_reg[0]_i_2__0_n_4\,
      O(2) => \time_out_counter_reg[0]_i_2__0_n_5\,
      O(1) => \time_out_counter_reg[0]_i_2__0_n_6\,
      O(0) => \time_out_counter_reg[0]_i_2__0_n_7\,
      S(3 downto 1) => time_out_counter_reg(3 downto 1),
      S(0) => \time_out_counter[0]_i_3_n_0\
    );
\time_out_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_5\,
      Q => time_out_counter_reg(10),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_4\,
      Q => time_out_counter_reg(11),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_7\,
      Q => time_out_counter_reg(12),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[12]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[8]_i_1__0_n_0\,
      CO(3) => \time_out_counter_reg[12]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[12]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[12]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[12]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[12]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[12]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[12]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[12]_i_1__0_n_7\,
      S(3 downto 0) => time_out_counter_reg(15 downto 12)
    );
\time_out_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_6\,
      Q => time_out_counter_reg(13),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_5\,
      Q => time_out_counter_reg(14),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_4\,
      Q => time_out_counter_reg(15),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1__0_n_7\,
      Q => time_out_counter_reg(16),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[16]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[12]_i_1__0_n_0\,
      CO(3 downto 2) => \NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \time_out_counter_reg[16]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[16]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED\(3),
      O(2) => \time_out_counter_reg[16]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[16]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[16]_i_1__0_n_7\,
      S(3) => '0',
      S(2 downto 0) => time_out_counter_reg(18 downto 16)
    );
\time_out_counter_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1__0_n_6\,
      Q => time_out_counter_reg(17),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1__0_n_5\,
      Q => time_out_counter_reg(18),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_6\,
      Q => time_out_counter_reg(1),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_5\,
      Q => time_out_counter_reg(2),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_4\,
      Q => time_out_counter_reg(3),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_7\,
      Q => time_out_counter_reg(4),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[4]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[0]_i_2__0_n_0\,
      CO(3) => \time_out_counter_reg[4]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[4]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[4]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[4]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[4]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[4]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[4]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[4]_i_1__0_n_7\,
      S(3 downto 0) => time_out_counter_reg(7 downto 4)
    );
\time_out_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_6\,
      Q => time_out_counter_reg(5),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_5\,
      Q => time_out_counter_reg(6),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_4\,
      Q => time_out_counter_reg(7),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_7\,
      Q => time_out_counter_reg(8),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[8]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[4]_i_1__0_n_0\,
      CO(3) => \time_out_counter_reg[8]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[8]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[8]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[8]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[8]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[8]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[8]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[8]_i_1__0_n_7\,
      S(3 downto 0) => time_out_counter_reg(11 downto 8)
    );
\time_out_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_6\,
      Q => time_out_counter_reg(9),
      R => reset_time_out_reg_n_0
    );
time_out_wait_bypass_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AB00"
    )
        port map (
      I0 => time_out_wait_bypass_reg_n_0,
      I1 => rx_fsm_reset_done_int_s3,
      I2 => \time_out_wait_bypass_i_2__0_n_0\,
      I3 => run_phase_alignment_int_s3_reg_n_0,
      O => time_out_wait_bypass_i_1_n_0
    );
\time_out_wait_bypass_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FBFFFFFF"
    )
        port map (
      I0 => \time_out_wait_bypass_i_3__0_n_0\,
      I1 => wait_bypass_count_reg(1),
      I2 => wait_bypass_count_reg(11),
      I3 => wait_bypass_count_reg(0),
      I4 => \time_out_wait_bypass_i_4__0_n_0\,
      O => \time_out_wait_bypass_i_2__0_n_0\
    );
\time_out_wait_bypass_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => wait_bypass_count_reg(9),
      I1 => wait_bypass_count_reg(4),
      I2 => wait_bypass_count_reg(12),
      I3 => wait_bypass_count_reg(2),
      O => \time_out_wait_bypass_i_3__0_n_0\
    );
\time_out_wait_bypass_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000400000000"
    )
        port map (
      I0 => wait_bypass_count_reg(5),
      I1 => wait_bypass_count_reg(7),
      I2 => wait_bypass_count_reg(3),
      I3 => wait_bypass_count_reg(6),
      I4 => wait_bypass_count_reg(10),
      I5 => wait_bypass_count_reg(8),
      O => \time_out_wait_bypass_i_4__0_n_0\
    );
time_out_wait_bypass_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => '1',
      D => time_out_wait_bypass_i_1_n_0,
      Q => time_out_wait_bypass_reg_n_0,
      R => '0'
    );
time_out_wait_bypass_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_wait_bypass_s2,
      Q => time_out_wait_bypass_s3,
      R => '0'
    );
time_tlock_max1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => time_tlock_max1_carry_n_0,
      CO(2) => time_tlock_max1_carry_n_1,
      CO(1) => time_tlock_max1_carry_n_2,
      CO(0) => time_tlock_max1_carry_n_3,
      CYINIT => '0',
      DI(3) => time_tlock_max1_carry_i_1_n_0,
      DI(2) => time_tlock_max1_carry_i_2_n_0,
      DI(1) => time_tlock_max1_carry_i_3_n_0,
      DI(0) => time_tlock_max1_carry_i_4_n_0,
      O(3 downto 0) => NLW_time_tlock_max1_carry_O_UNCONNECTED(3 downto 0),
      S(3) => time_tlock_max1_carry_i_5_n_0,
      S(2) => time_tlock_max1_carry_i_6_n_0,
      S(1) => time_tlock_max1_carry_i_7_n_0,
      S(0) => time_tlock_max1_carry_i_8_n_0
    );
\time_tlock_max1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => time_tlock_max1_carry_n_0,
      CO(3) => \time_tlock_max1_carry__0_n_0\,
      CO(2) => \time_tlock_max1_carry__0_n_1\,
      CO(1) => \time_tlock_max1_carry__0_n_2\,
      CO(0) => \time_tlock_max1_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => time_out_counter_reg(15),
      DI(2) => \time_tlock_max1_carry__0_i_1_n_0\,
      DI(1) => '0',
      DI(0) => \time_tlock_max1_carry__0_i_2_n_0\,
      O(3 downto 0) => \NLW_time_tlock_max1_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \time_tlock_max1_carry__0_i_3_n_0\,
      S(2) => \time_tlock_max1_carry__0_i_4_n_0\,
      S(1) => \time_tlock_max1_carry__0_i_5_n_0\,
      S(0) => \time_tlock_max1_carry__0_i_6_n_0\
    );
\time_tlock_max1_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(12),
      I1 => time_out_counter_reg(13),
      O => \time_tlock_max1_carry__0_i_1_n_0\
    );
\time_tlock_max1_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(8),
      I1 => time_out_counter_reg(9),
      O => \time_tlock_max1_carry__0_i_2_n_0\
    );
\time_tlock_max1_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(14),
      I1 => time_out_counter_reg(15),
      O => \time_tlock_max1_carry__0_i_3_n_0\
    );
\time_tlock_max1_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(13),
      I1 => time_out_counter_reg(12),
      O => \time_tlock_max1_carry__0_i_4_n_0\
    );
\time_tlock_max1_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(10),
      I1 => time_out_counter_reg(11),
      O => \time_tlock_max1_carry__0_i_5_n_0\
    );
\time_tlock_max1_carry__0_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(9),
      I1 => time_out_counter_reg(8),
      O => \time_tlock_max1_carry__0_i_6_n_0\
    );
\time_tlock_max1_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_tlock_max1_carry__0_n_0\,
      CO(3 downto 2) => \NLW_time_tlock_max1_carry__1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => time_tlock_max1,
      CO(0) => \time_tlock_max1_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => time_out_counter_reg(18),
      DI(0) => \time_tlock_max1_carry__1_i_1_n_0\,
      O(3 downto 0) => \NLW_time_tlock_max1_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3 downto 2) => B"00",
      S(1) => \time_tlock_max1_carry__1_i_2_n_0\,
      S(0) => \time_tlock_max1_carry__1_i_3_n_0\
    );
\time_tlock_max1_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(17),
      O => \time_tlock_max1_carry__1_i_1_n_0\
    );
\time_tlock_max1_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(18),
      O => \time_tlock_max1_carry__1_i_2_n_0\
    );
\time_tlock_max1_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(16),
      O => \time_tlock_max1_carry__1_i_3_n_0\
    );
time_tlock_max1_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(6),
      I1 => time_out_counter_reg(7),
      O => time_tlock_max1_carry_i_1_n_0
    );
time_tlock_max1_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(4),
      I1 => time_out_counter_reg(5),
      O => time_tlock_max1_carry_i_2_n_0
    );
time_tlock_max1_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(2),
      I1 => time_out_counter_reg(3),
      O => time_tlock_max1_carry_i_3_n_0
    );
time_tlock_max1_carry_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(0),
      I1 => time_out_counter_reg(1),
      O => time_tlock_max1_carry_i_4_n_0
    );
time_tlock_max1_carry_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(7),
      I1 => time_out_counter_reg(6),
      O => time_tlock_max1_carry_i_5_n_0
    );
time_tlock_max1_carry_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(5),
      I1 => time_out_counter_reg(4),
      O => time_tlock_max1_carry_i_6_n_0
    );
time_tlock_max1_carry_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(3),
      I1 => time_out_counter_reg(2),
      O => time_tlock_max1_carry_i_7_n_0
    );
time_tlock_max1_carry_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(1),
      I1 => time_out_counter_reg(0),
      O => time_tlock_max1_carry_i_8_n_0
    );
time_tlock_max_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => check_tlock_max_reg_n_0,
      I1 => time_tlock_max1,
      I2 => time_tlock_max,
      O => time_tlock_max_i_1_n_0
    );
time_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_tlock_max_i_1_n_0,
      Q => time_tlock_max,
      R => reset_time_out_reg_n_0
    );
\wait_bypass_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => run_phase_alignment_int_s3_reg_n_0,
      O => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count[0]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \time_out_wait_bypass_i_2__0_n_0\,
      I1 => rx_fsm_reset_done_int_s3,
      O => \wait_bypass_count[0]_i_2__0_n_0\
    );
\wait_bypass_count[0]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_bypass_count_reg(0),
      O => \wait_bypass_count[0]_i_4_n_0\
    );
\wait_bypass_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_7\,
      Q => wait_bypass_count_reg(0),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[0]_i_3__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \wait_bypass_count_reg[0]_i_3__0_n_0\,
      CO(2) => \wait_bypass_count_reg[0]_i_3__0_n_1\,
      CO(1) => \wait_bypass_count_reg[0]_i_3__0_n_2\,
      CO(0) => \wait_bypass_count_reg[0]_i_3__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \wait_bypass_count_reg[0]_i_3__0_n_4\,
      O(2) => \wait_bypass_count_reg[0]_i_3__0_n_5\,
      O(1) => \wait_bypass_count_reg[0]_i_3__0_n_6\,
      O(0) => \wait_bypass_count_reg[0]_i_3__0_n_7\,
      S(3 downto 1) => wait_bypass_count_reg(3 downto 1),
      S(0) => \wait_bypass_count[0]_i_4_n_0\
    );
\wait_bypass_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_5\,
      Q => wait_bypass_count_reg(10),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_4\,
      Q => wait_bypass_count_reg(11),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[12]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(12),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[12]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[8]_i_1__0_n_0\,
      CO(3 downto 0) => \NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED\(3 downto 1),
      O(0) => \wait_bypass_count_reg[12]_i_1__0_n_7\,
      S(3 downto 1) => B"000",
      S(0) => wait_bypass_count_reg(12)
    );
\wait_bypass_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_6\,
      Q => wait_bypass_count_reg(1),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_5\,
      Q => wait_bypass_count_reg(2),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_4\,
      Q => wait_bypass_count_reg(3),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(4),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[4]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[0]_i_3__0_n_0\,
      CO(3) => \wait_bypass_count_reg[4]_i_1__0_n_0\,
      CO(2) => \wait_bypass_count_reg[4]_i_1__0_n_1\,
      CO(1) => \wait_bypass_count_reg[4]_i_1__0_n_2\,
      CO(0) => \wait_bypass_count_reg[4]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[4]_i_1__0_n_4\,
      O(2) => \wait_bypass_count_reg[4]_i_1__0_n_5\,
      O(1) => \wait_bypass_count_reg[4]_i_1__0_n_6\,
      O(0) => \wait_bypass_count_reg[4]_i_1__0_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(7 downto 4)
    );
\wait_bypass_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_6\,
      Q => wait_bypass_count_reg(5),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_5\,
      Q => wait_bypass_count_reg(6),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_4\,
      Q => wait_bypass_count_reg(7),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(8),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[8]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[4]_i_1__0_n_0\,
      CO(3) => \wait_bypass_count_reg[8]_i_1__0_n_0\,
      CO(2) => \wait_bypass_count_reg[8]_i_1__0_n_1\,
      CO(1) => \wait_bypass_count_reg[8]_i_1__0_n_2\,
      CO(0) => \wait_bypass_count_reg[8]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[8]_i_1__0_n_4\,
      O(2) => \wait_bypass_count_reg[8]_i_1__0_n_5\,
      O(1) => \wait_bypass_count_reg[8]_i_1__0_n_6\,
      O(0) => \wait_bypass_count_reg[8]_i_1__0_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(11 downto 8)
    );
\wait_bypass_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_6\,
      Q => wait_bypass_count_reg(9),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_time_cnt[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      O => \wait_time_cnt0__0\(0)
    );
\wait_time_cnt[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      I1 => wait_time_cnt_reg(1),
      O => \wait_time_cnt[1]_i_1__0_n_0\
    );
\wait_time_cnt[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => wait_time_cnt_reg(1),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(2),
      O => \wait_time_cnt[2]_i_1__0_n_0\
    );
\wait_time_cnt[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE01"
    )
        port map (
      I0 => wait_time_cnt_reg(2),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(1),
      I3 => wait_time_cnt_reg(3),
      O => \wait_time_cnt[3]_i_1__0_n_0\
    );
\wait_time_cnt[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0001"
    )
        port map (
      I0 => wait_time_cnt_reg(3),
      I1 => wait_time_cnt_reg(1),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(2),
      I4 => wait_time_cnt_reg(4),
      O => \wait_time_cnt[4]_i_1__0_n_0\
    );
\wait_time_cnt[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000001"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[5]_i_1__0_n_0\
    );
\wait_time_cnt[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => rx_state(0),
      I1 => rx_state(1),
      I2 => rx_state(3),
      O => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt[6]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4__0_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => \wait_time_cnt[6]_i_2__0_n_0\
    );
\wait_time_cnt[6]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4__0_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => \wait_time_cnt[6]_i_3__0_n_0\
    );
\wait_time_cnt[6]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[6]_i_4__0_n_0\
    );
\wait_time_cnt_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt0__0\(0),
      Q => wait_time_cnt_reg(0),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[1]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(1),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[2]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(2),
      S => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[3]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(3),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[4]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(4),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[5]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(5),
      S => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[6]_i_3__0_n_0\,
      Q => wait_time_cnt_reg(6),
      S => \wait_time_cnt[6]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_TX_STARTUP_FSM is
  port (
    mmcm_reset : out STD_LOGIC;
    gt0_cpllreset_t : out STD_LOGIC;
    data_in : out STD_LOGIC;
    gt0_txuserrdy_t : out STD_LOGIC;
    gt0_gttxreset_in0_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : in STD_LOGIC;
    gt0_cpllrefclklost_i : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    data_sync_reg1_1 : in STD_LOGIC;
    data_sync_reg1_2 : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_TX_STARTUP_FSM;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_TX_STARTUP_FSM is
  signal CPLL_RESET_i_1_n_0 : STD_LOGIC;
  signal CPLL_RESET_i_2_n_0 : STD_LOGIC;
  signal \FSM_sequential_tx_state[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[0]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_8_n_0\ : STD_LOGIC;
  signal GTTXRESET : STD_LOGIC;
  signal MMCM_RESET_i_1_n_0 : STD_LOGIC;
  signal TXUSERRDY_i_1_n_0 : STD_LOGIC;
  signal clear : STD_LOGIC;
  signal \^data_in\ : STD_LOGIC;
  signal data_out : STD_LOGIC;
  signal \^gt0_cpllreset_t\ : STD_LOGIC;
  signal \^gt0_txuserrdy_t\ : STD_LOGIC;
  signal gttxreset_i_i_1_n_0 : STD_LOGIC;
  signal \init_wait_count[0]_i_1_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_1_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_3_n_0\ : STD_LOGIC;
  signal init_wait_count_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal init_wait_done_i_1_n_0 : STD_LOGIC;
  signal init_wait_done_reg_n_0 : STD_LOGIC;
  signal \mmcm_lock_count[2]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[3]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[4]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[5]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[6]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_2_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_3_n_0\ : STD_LOGIC;
  signal mmcm_lock_count_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal mmcm_lock_i : STD_LOGIC;
  signal mmcm_lock_reclocked : STD_LOGIC;
  signal mmcm_lock_reclocked_i_1_n_0 : STD_LOGIC;
  signal mmcm_lock_reclocked_i_2_n_0 : STD_LOGIC;
  signal \^mmcm_reset\ : STD_LOGIC;
  signal \p_0_in__0\ : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \p_0_in__1\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pll_reset_asserted_i_1_n_0 : STD_LOGIC;
  signal pll_reset_asserted_i_2_n_0 : STD_LOGIC;
  signal pll_reset_asserted_reg_n_0 : STD_LOGIC;
  signal refclk_stable_count : STD_LOGIC;
  signal \refclk_stable_count[0]_i_3_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_4_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_5_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_6_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_7_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_8_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_9_n_0\ : STD_LOGIC;
  signal refclk_stable_count_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \refclk_stable_count_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal refclk_stable_i_1_n_0 : STD_LOGIC;
  signal refclk_stable_i_2_n_0 : STD_LOGIC;
  signal refclk_stable_i_3_n_0 : STD_LOGIC;
  signal refclk_stable_i_4_n_0 : STD_LOGIC;
  signal refclk_stable_i_5_n_0 : STD_LOGIC;
  signal refclk_stable_i_6_n_0 : STD_LOGIC;
  signal refclk_stable_reg_n_0 : STD_LOGIC;
  signal reset_time_out : STD_LOGIC;
  signal \reset_time_out_i_2__0_n_0\ : STD_LOGIC;
  signal run_phase_alignment_int_i_1_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_reg_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_s3 : STD_LOGIC;
  signal sel : STD_LOGIC;
  signal sync_cplllock_n_0 : STD_LOGIC;
  signal sync_cplllock_n_1 : STD_LOGIC;
  signal sync_mmcm_lock_reclocked_n_0 : STD_LOGIC;
  signal time_out_2ms_i_1_n_0 : STD_LOGIC;
  signal \time_out_2ms_i_2__0_n_0\ : STD_LOGIC;
  signal time_out_2ms_i_3_n_0 : STD_LOGIC;
  signal \time_out_2ms_i_4__0_n_0\ : STD_LOGIC;
  signal time_out_2ms_i_5_n_0 : STD_LOGIC;
  signal time_out_2ms_reg_n_0 : STD_LOGIC;
  signal time_out_500us_i_1_n_0 : STD_LOGIC;
  signal time_out_500us_i_2_n_0 : STD_LOGIC;
  signal time_out_500us_reg_n_0 : STD_LOGIC;
  signal time_out_counter : STD_LOGIC;
  signal \time_out_counter[0]_i_3__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[0]_i_4_n_0\ : STD_LOGIC;
  signal time_out_counter_reg : STD_LOGIC_VECTOR ( 18 downto 0 );
  signal \time_out_counter_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal time_out_wait_bypass_i_1_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_2_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_3_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_4_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_5_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_reg_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_s2 : STD_LOGIC;
  signal time_out_wait_bypass_s3 : STD_LOGIC;
  signal time_tlock_max_i_1_n_0 : STD_LOGIC;
  signal time_tlock_max_i_2_n_0 : STD_LOGIC;
  signal time_tlock_max_i_3_n_0 : STD_LOGIC;
  signal time_tlock_max_i_4_n_0 : STD_LOGIC;
  signal time_tlock_max_reg_n_0 : STD_LOGIC;
  signal tx_fsm_reset_done_int_i_1_n_0 : STD_LOGIC;
  signal tx_fsm_reset_done_int_s2 : STD_LOGIC;
  signal tx_fsm_reset_done_int_s3 : STD_LOGIC;
  signal tx_state : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \tx_state__0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal txresetdone_s2 : STD_LOGIC;
  signal txresetdone_s3 : STD_LOGIC;
  signal \wait_bypass_count[0]_i_2_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_4__0_n_0\ : STD_LOGIC;
  signal wait_bypass_count_reg : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal \wait_bypass_count_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal wait_time_cnt0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal wait_time_cnt0_0 : STD_LOGIC;
  signal \wait_time_cnt[1]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[2]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[3]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[4]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[5]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_3_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_4_n_0\ : STD_LOGIC;
  signal wait_time_cnt_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of CPLL_RESET_i_2 : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[0]_i_2\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[0]_i_3\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[1]_i_1\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[3]_i_2\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[3]_i_7\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[3]_i_8\ : label is "soft_lutpair59";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[0]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[1]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[2]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[3]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute SOFT_HLUTNM of MMCM_RESET_i_1 : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of TXUSERRDY_i_1 : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of gttxreset_i_i_1 : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \init_wait_count[1]_i_1\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \init_wait_count[2]_i_1\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \init_wait_count[3]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \init_wait_count[4]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_2\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_3\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \mmcm_lock_count[1]_i_1\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \mmcm_lock_count[2]_i_1\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \mmcm_lock_count[3]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \mmcm_lock_count[4]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \mmcm_lock_count[6]_i_1\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \mmcm_lock_count[7]_i_3\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of pll_reset_asserted_i_2 : label is "soft_lutpair60";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[12]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[16]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[20]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[24]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[28]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[8]_i_1\ : label is 11;
  attribute SOFT_HLUTNM of run_phase_alignment_int_i_1 : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \time_out_2ms_i_4__0\ : label is "soft_lutpair56";
  attribute ADDER_THRESHOLD of \time_out_counter_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[12]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[16]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[8]_i_1\ : label is 11;
  attribute SOFT_HLUTNM of time_tlock_max_i_4 : label is "soft_lutpair56";
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[0]_i_3\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[12]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[16]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[8]_i_1\ : label is 11;
  attribute SOFT_HLUTNM of \wait_time_cnt[0]_i_1\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \wait_time_cnt[1]_i_1\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \wait_time_cnt[3]_i_1\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \wait_time_cnt[4]_i_1\ : label is "soft_lutpair63";
begin
  data_in <= \^data_in\;
  gt0_cpllreset_t <= \^gt0_cpllreset_t\;
  gt0_txuserrdy_t <= \^gt0_txuserrdy_t\;
  mmcm_reset <= \^mmcm_reset\;
CPLL_RESET_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF1F0000001F"
    )
        port map (
      I0 => pll_reset_asserted_reg_n_0,
      I1 => gt0_cpllrefclklost_i,
      I2 => refclk_stable_reg_n_0,
      I3 => CPLL_RESET_i_2_n_0,
      I4 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I5 => \^gt0_cpllreset_t\,
      O => CPLL_RESET_i_1_n_0
    );
CPLL_RESET_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      O => CPLL_RESET_i_2_n_0
    );
CPLL_RESET_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => CPLL_RESET_i_1_n_0,
      Q => \^gt0_cpllreset_t\,
      R => \out\(0)
    );
\FSM_sequential_tx_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F3FFF3F0F5F0F5F0"
    )
        port map (
      I0 => \FSM_sequential_tx_state[2]_i_2_n_0\,
      I1 => \FSM_sequential_tx_state[0]_i_2_n_0\,
      I2 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I3 => tx_state(2),
      I4 => time_out_2ms_reg_n_0,
      I5 => tx_state(1),
      O => \tx_state__0\(0)
    );
\FSM_sequential_tx_state[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => reset_time_out,
      I1 => time_out_500us_reg_n_0,
      O => \FSM_sequential_tx_state[0]_i_2_n_0\
    );
\FSM_sequential_tx_state[0]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      O => \FSM_sequential_tx_state[0]_i_3_n_0\
    );
\FSM_sequential_tx_state[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"005A001A"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(0),
      I3 => tx_state(3),
      I4 => \FSM_sequential_tx_state[2]_i_2_n_0\,
      O => \tx_state__0\(1)
    );
\FSM_sequential_tx_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"04000C0C06020C0C"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(3),
      I3 => \FSM_sequential_tx_state[2]_i_2_n_0\,
      I4 => tx_state(0),
      I5 => time_out_2ms_reg_n_0,
      O => \tx_state__0\(2)
    );
\FSM_sequential_tx_state[2]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => time_tlock_max_reg_n_0,
      I1 => reset_time_out,
      I2 => mmcm_lock_reclocked,
      O => \FSM_sequential_tx_state[2]_i_2_n_0\
    );
\FSM_sequential_tx_state[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F4FF4444"
    )
        port map (
      I0 => time_out_wait_bypass_s3,
      I1 => tx_state(3),
      I2 => reset_time_out,
      I3 => time_out_500us_reg_n_0,
      I4 => \FSM_sequential_tx_state[3]_i_8_n_0\,
      O => \tx_state__0\(3)
    );
\FSM_sequential_tx_state[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00BA000000000000"
    )
        port map (
      I0 => txresetdone_s3,
      I1 => reset_time_out,
      I2 => time_out_500us_reg_n_0,
      I3 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I4 => tx_state(2),
      I5 => tx_state(1),
      O => \FSM_sequential_tx_state[3]_i_3_n_0\
    );
\FSM_sequential_tx_state[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000300FF00AA"
    )
        port map (
      I0 => init_wait_done_reg_n_0,
      I1 => \wait_time_cnt[6]_i_4_n_0\,
      I2 => wait_time_cnt_reg(6),
      I3 => tx_state(0),
      I4 => tx_state(3),
      I5 => CPLL_RESET_i_2_n_0,
      O => \FSM_sequential_tx_state[3]_i_4_n_0\
    );
\FSM_sequential_tx_state[3]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0404040400040000"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I3 => reset_time_out,
      I4 => time_tlock_max_reg_n_0,
      I5 => mmcm_lock_reclocked,
      O => \FSM_sequential_tx_state[3]_i_6_n_0\
    );
\FSM_sequential_tx_state[3]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1000"
    )
        port map (
      I0 => tx_state(2),
      I1 => tx_state(3),
      I2 => tx_state(0),
      I3 => tx_state(1),
      O => \FSM_sequential_tx_state[3]_i_7_n_0\
    );
\FSM_sequential_tx_state[3]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      I2 => tx_state(2),
      I3 => tx_state(1),
      O => \FSM_sequential_tx_state[3]_i_8_n_0\
    );
\FSM_sequential_tx_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(0),
      Q => tx_state(0),
      R => \out\(0)
    );
\FSM_sequential_tx_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(1),
      Q => tx_state(1),
      R => \out\(0)
    );
\FSM_sequential_tx_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(2),
      Q => tx_state(2),
      R => \out\(0)
    );
\FSM_sequential_tx_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(3),
      Q => tx_state(3),
      R => \out\(0)
    );
MMCM_RESET_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70004"
    )
        port map (
      I0 => tx_state(2),
      I1 => tx_state(0),
      I2 => tx_state(3),
      I3 => tx_state(1),
      I4 => \^mmcm_reset\,
      O => MMCM_RESET_i_1_n_0
    );
MMCM_RESET_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => MMCM_RESET_i_1_n_0,
      Q => \^mmcm_reset\,
      R => \out\(0)
    );
TXUSERRDY_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFD2000"
    )
        port map (
      I0 => tx_state(0),
      I1 => tx_state(3),
      I2 => tx_state(2),
      I3 => tx_state(1),
      I4 => \^gt0_txuserrdy_t\,
      O => TXUSERRDY_i_1_n_0
    );
TXUSERRDY_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => TXUSERRDY_i_1_n_0,
      Q => \^gt0_txuserrdy_t\,
      R => \out\(0)
    );
gttxreset_i_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0100"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(1),
      I2 => tx_state(2),
      I3 => tx_state(0),
      I4 => GTTXRESET,
      O => gttxreset_i_i_1_n_0
    );
gttxreset_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gttxreset_i_i_1_n_0,
      Q => GTTXRESET,
      R => \out\(0)
    );
gtxe2_i_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => GTTXRESET,
      I1 => \^data_in\,
      I2 => gtxe2_i,
      O => gt0_gttxreset_in0_out
    );
\init_wait_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => init_wait_count_reg(0),
      O => \init_wait_count[0]_i_1_n_0\
    );
\init_wait_count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      O => \p_0_in__0\(1)
    );
\init_wait_count[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      O => \p_0_in__0\(2)
    );
\init_wait_count[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => init_wait_count_reg(1),
      I1 => init_wait_count_reg(2),
      I2 => init_wait_count_reg(0),
      I3 => init_wait_count_reg(3),
      O => \p_0_in__0\(3)
    );
\init_wait_count[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      O => \p_0_in__0\(4)
    );
\init_wait_count[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      I5 => init_wait_count_reg(5),
      O => \p_0_in__0\(5)
    );
\init_wait_count[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => \init_wait_count[6]_i_3_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \init_wait_count[6]_i_1_n_0\
    );
\init_wait_count[6]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF40"
    )
        port map (
      I0 => \init_wait_count[6]_i_3_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \p_0_in__0\(6)
    );
\init_wait_count[6]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => init_wait_count_reg(3),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      I3 => init_wait_count_reg(5),
      O => \init_wait_count[6]_i_3_n_0\
    );
\init_wait_count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \init_wait_count[0]_i_1_n_0\,
      Q => init_wait_count_reg(0)
    );
\init_wait_count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(1),
      Q => init_wait_count_reg(1)
    );
\init_wait_count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(2),
      Q => init_wait_count_reg(2)
    );
\init_wait_count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(3),
      Q => init_wait_count_reg(3)
    );
\init_wait_count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(4),
      Q => init_wait_count_reg(4)
    );
\init_wait_count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(5),
      Q => init_wait_count_reg(5)
    );
\init_wait_count_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(6),
      Q => init_wait_count_reg(6)
    );
init_wait_done_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0010"
    )
        port map (
      I0 => \init_wait_count[6]_i_3_n_0\,
      I1 => init_wait_count_reg(4),
      I2 => init_wait_count_reg(6),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_done_reg_n_0,
      O => init_wait_done_i_1_n_0
    );
init_wait_done_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      CLR => \out\(0),
      D => init_wait_done_i_1_n_0,
      Q => init_wait_done_reg_n_0
    );
\mmcm_lock_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      O => \p_0_in__1\(0)
    );
\mmcm_lock_count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      I1 => mmcm_lock_count_reg(1),
      O => \p_0_in__1\(1)
    );
\mmcm_lock_count[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => mmcm_lock_count_reg(1),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(2),
      O => \mmcm_lock_count[2]_i_1_n_0\
    );
\mmcm_lock_count[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => mmcm_lock_count_reg(2),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(1),
      I3 => mmcm_lock_count_reg(3),
      O => \mmcm_lock_count[3]_i_1_n_0\
    );
\mmcm_lock_count[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => mmcm_lock_count_reg(3),
      I1 => mmcm_lock_count_reg(1),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(2),
      I4 => mmcm_lock_count_reg(4),
      O => \mmcm_lock_count[4]_i_1_n_0\
    );
\mmcm_lock_count[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => \mmcm_lock_count[5]_i_1_n_0\
    );
\mmcm_lock_count[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => mmcm_lock_reclocked_i_2_n_0,
      I1 => mmcm_lock_count_reg(6),
      O => \mmcm_lock_count[6]_i_1_n_0\
    );
\mmcm_lock_count[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => mmcm_lock_reclocked_i_2_n_0,
      I1 => mmcm_lock_count_reg(6),
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_2_n_0\
    );
\mmcm_lock_count[7]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => mmcm_lock_count_reg(6),
      I1 => mmcm_lock_reclocked_i_2_n_0,
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_3_n_0\
    );
\mmcm_lock_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \p_0_in__1\(0),
      Q => mmcm_lock_count_reg(0),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \p_0_in__1\(1),
      Q => mmcm_lock_count_reg(1),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[2]_i_1_n_0\,
      Q => mmcm_lock_count_reg(2),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[3]_i_1_n_0\,
      Q => mmcm_lock_count_reg(3),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[4]_i_1_n_0\,
      Q => mmcm_lock_count_reg(4),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[5]_i_1_n_0\,
      Q => mmcm_lock_count_reg(5),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[6]_i_1_n_0\,
      Q => mmcm_lock_count_reg(6),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[7]_i_3_n_0\,
      Q => mmcm_lock_count_reg(7),
      R => sync_mmcm_lock_reclocked_n_0
    );
mmcm_lock_reclocked_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEA0000"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => mmcm_lock_count_reg(7),
      I2 => mmcm_lock_count_reg(6),
      I3 => mmcm_lock_reclocked_i_2_n_0,
      I4 => mmcm_lock_i,
      O => mmcm_lock_reclocked_i_1_n_0
    );
mmcm_lock_reclocked_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => mmcm_lock_reclocked_i_2_n_0
    );
mmcm_lock_reclocked_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => mmcm_lock_reclocked_i_1_n_0,
      Q => mmcm_lock_reclocked,
      R => '0'
    );
pll_reset_asserted_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000CD55CCCCCCCC"
    )
        port map (
      I0 => tx_state(3),
      I1 => pll_reset_asserted_reg_n_0,
      I2 => gt0_cpllrefclklost_i,
      I3 => refclk_stable_reg_n_0,
      I4 => tx_state(1),
      I5 => pll_reset_asserted_i_2_n_0,
      O => pll_reset_asserted_i_1_n_0
    );
pll_reset_asserted_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => tx_state(0),
      I1 => tx_state(3),
      I2 => tx_state(2),
      O => pll_reset_asserted_i_2_n_0
    );
pll_reset_asserted_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pll_reset_asserted_i_1_n_0,
      Q => pll_reset_asserted_reg_n_0,
      R => \out\(0)
    );
\refclk_stable_count[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFFFF"
    )
        port map (
      I0 => \refclk_stable_count[0]_i_3_n_0\,
      I1 => \refclk_stable_count[0]_i_4_n_0\,
      I2 => \refclk_stable_count[0]_i_5_n_0\,
      I3 => \refclk_stable_count[0]_i_6_n_0\,
      I4 => \refclk_stable_count[0]_i_7_n_0\,
      I5 => \refclk_stable_count[0]_i_8_n_0\,
      O => refclk_stable_count
    );
\refclk_stable_count[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFDFFFFFFFFFFFFF"
    )
        port map (
      I0 => refclk_stable_count_reg(13),
      I1 => refclk_stable_count_reg(12),
      I2 => refclk_stable_count_reg(10),
      I3 => refclk_stable_count_reg(11),
      I4 => refclk_stable_count_reg(9),
      I5 => refclk_stable_count_reg(8),
      O => \refclk_stable_count[0]_i_3_n_0\
    );
\refclk_stable_count[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFDF"
    )
        port map (
      I0 => refclk_stable_count_reg(19),
      I1 => refclk_stable_count_reg(18),
      I2 => refclk_stable_count_reg(16),
      I3 => refclk_stable_count_reg(17),
      I4 => refclk_stable_count_reg(15),
      I5 => refclk_stable_count_reg(14),
      O => \refclk_stable_count[0]_i_4_n_0\
    );
\refclk_stable_count[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => refclk_stable_count_reg(30),
      I1 => refclk_stable_count_reg(31),
      I2 => refclk_stable_count_reg(28),
      I3 => refclk_stable_count_reg(29),
      I4 => refclk_stable_count_reg(27),
      I5 => refclk_stable_count_reg(26),
      O => \refclk_stable_count[0]_i_5_n_0\
    );
\refclk_stable_count[0]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => refclk_stable_count_reg(24),
      I1 => refclk_stable_count_reg(25),
      I2 => refclk_stable_count_reg(22),
      I3 => refclk_stable_count_reg(23),
      I4 => refclk_stable_count_reg(21),
      I5 => refclk_stable_count_reg(20),
      O => \refclk_stable_count[0]_i_6_n_0\
    );
\refclk_stable_count[0]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => refclk_stable_count_reg(0),
      I1 => refclk_stable_count_reg(1),
      O => \refclk_stable_count[0]_i_7_n_0\
    );
\refclk_stable_count[0]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFF7"
    )
        port map (
      I0 => refclk_stable_count_reg(6),
      I1 => refclk_stable_count_reg(7),
      I2 => refclk_stable_count_reg(4),
      I3 => refclk_stable_count_reg(5),
      I4 => refclk_stable_count_reg(3),
      I5 => refclk_stable_count_reg(2),
      O => \refclk_stable_count[0]_i_8_n_0\
    );
\refclk_stable_count[0]_i_9\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => refclk_stable_count_reg(0),
      O => \refclk_stable_count[0]_i_9_n_0\
    );
\refclk_stable_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_7\,
      Q => refclk_stable_count_reg(0),
      R => '0'
    );
\refclk_stable_count_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \refclk_stable_count_reg[0]_i_2_n_0\,
      CO(2) => \refclk_stable_count_reg[0]_i_2_n_1\,
      CO(1) => \refclk_stable_count_reg[0]_i_2_n_2\,
      CO(0) => \refclk_stable_count_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \refclk_stable_count_reg[0]_i_2_n_4\,
      O(2) => \refclk_stable_count_reg[0]_i_2_n_5\,
      O(1) => \refclk_stable_count_reg[0]_i_2_n_6\,
      O(0) => \refclk_stable_count_reg[0]_i_2_n_7\,
      S(3 downto 1) => refclk_stable_count_reg(3 downto 1),
      S(0) => \refclk_stable_count[0]_i_9_n_0\
    );
\refclk_stable_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_5\,
      Q => refclk_stable_count_reg(10),
      R => '0'
    );
\refclk_stable_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_4\,
      Q => refclk_stable_count_reg(11),
      R => '0'
    );
\refclk_stable_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_7\,
      Q => refclk_stable_count_reg(12),
      R => '0'
    );
\refclk_stable_count_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[8]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[12]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[12]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[12]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[12]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[12]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[12]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[12]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(15 downto 12)
    );
\refclk_stable_count_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_6\,
      Q => refclk_stable_count_reg(13),
      R => '0'
    );
\refclk_stable_count_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_5\,
      Q => refclk_stable_count_reg(14),
      R => '0'
    );
\refclk_stable_count_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_4\,
      Q => refclk_stable_count_reg(15),
      R => '0'
    );
\refclk_stable_count_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_7\,
      Q => refclk_stable_count_reg(16),
      R => '0'
    );
\refclk_stable_count_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[12]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[16]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[16]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[16]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[16]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[16]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[16]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[16]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(19 downto 16)
    );
\refclk_stable_count_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_6\,
      Q => refclk_stable_count_reg(17),
      R => '0'
    );
\refclk_stable_count_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_5\,
      Q => refclk_stable_count_reg(18),
      R => '0'
    );
\refclk_stable_count_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_4\,
      Q => refclk_stable_count_reg(19),
      R => '0'
    );
\refclk_stable_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_6\,
      Q => refclk_stable_count_reg(1),
      R => '0'
    );
\refclk_stable_count_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_7\,
      Q => refclk_stable_count_reg(20),
      R => '0'
    );
\refclk_stable_count_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[16]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[20]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[20]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[20]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[20]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[20]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[20]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[20]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(23 downto 20)
    );
\refclk_stable_count_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_6\,
      Q => refclk_stable_count_reg(21),
      R => '0'
    );
\refclk_stable_count_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_5\,
      Q => refclk_stable_count_reg(22),
      R => '0'
    );
\refclk_stable_count_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_4\,
      Q => refclk_stable_count_reg(23),
      R => '0'
    );
\refclk_stable_count_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_7\,
      Q => refclk_stable_count_reg(24),
      R => '0'
    );
\refclk_stable_count_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[20]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[24]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[24]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[24]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[24]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[24]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[24]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[24]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(27 downto 24)
    );
\refclk_stable_count_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_6\,
      Q => refclk_stable_count_reg(25),
      R => '0'
    );
\refclk_stable_count_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_5\,
      Q => refclk_stable_count_reg(26),
      R => '0'
    );
\refclk_stable_count_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_4\,
      Q => refclk_stable_count_reg(27),
      R => '0'
    );
\refclk_stable_count_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_7\,
      Q => refclk_stable_count_reg(28),
      R => '0'
    );
\refclk_stable_count_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[24]_i_1_n_0\,
      CO(3) => \NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \refclk_stable_count_reg[28]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[28]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[28]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[28]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[28]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[28]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(31 downto 28)
    );
\refclk_stable_count_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_6\,
      Q => refclk_stable_count_reg(29),
      R => '0'
    );
\refclk_stable_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_5\,
      Q => refclk_stable_count_reg(2),
      R => '0'
    );
\refclk_stable_count_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_5\,
      Q => refclk_stable_count_reg(30),
      R => '0'
    );
\refclk_stable_count_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_4\,
      Q => refclk_stable_count_reg(31),
      R => '0'
    );
\refclk_stable_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_4\,
      Q => refclk_stable_count_reg(3),
      R => '0'
    );
\refclk_stable_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_7\,
      Q => refclk_stable_count_reg(4),
      R => '0'
    );
\refclk_stable_count_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[0]_i_2_n_0\,
      CO(3) => \refclk_stable_count_reg[4]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[4]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[4]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[4]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[4]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[4]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[4]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(7 downto 4)
    );
\refclk_stable_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_6\,
      Q => refclk_stable_count_reg(5),
      R => '0'
    );
\refclk_stable_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_5\,
      Q => refclk_stable_count_reg(6),
      R => '0'
    );
\refclk_stable_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_4\,
      Q => refclk_stable_count_reg(7),
      R => '0'
    );
\refclk_stable_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_7\,
      Q => refclk_stable_count_reg(8),
      R => '0'
    );
\refclk_stable_count_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[4]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[8]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[8]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[8]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[8]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[8]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[8]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[8]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(11 downto 8)
    );
\refclk_stable_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_6\,
      Q => refclk_stable_count_reg(9),
      R => '0'
    );
refclk_stable_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \refclk_stable_count[0]_i_7_n_0\,
      I1 => refclk_stable_i_2_n_0,
      I2 => refclk_stable_i_3_n_0,
      I3 => refclk_stable_i_4_n_0,
      I4 => refclk_stable_i_5_n_0,
      I5 => refclk_stable_i_6_n_0,
      O => refclk_stable_i_1_n_0
    );
refclk_stable_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001000000000000"
    )
        port map (
      I0 => refclk_stable_count_reg(4),
      I1 => refclk_stable_count_reg(5),
      I2 => refclk_stable_count_reg(2),
      I3 => refclk_stable_count_reg(3),
      I4 => refclk_stable_count_reg(7),
      I5 => refclk_stable_count_reg(6),
      O => refclk_stable_i_2_n_0
    );
refclk_stable_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000200000000000"
    )
        port map (
      I0 => refclk_stable_count_reg(10),
      I1 => refclk_stable_count_reg(11),
      I2 => refclk_stable_count_reg(8),
      I3 => refclk_stable_count_reg(9),
      I4 => refclk_stable_count_reg(12),
      I5 => refclk_stable_count_reg(13),
      O => refclk_stable_i_3_n_0
    );
refclk_stable_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => refclk_stable_count_reg(16),
      I1 => refclk_stable_count_reg(17),
      I2 => refclk_stable_count_reg(14),
      I3 => refclk_stable_count_reg(15),
      I4 => refclk_stable_count_reg(18),
      I5 => refclk_stable_count_reg(19),
      O => refclk_stable_i_4_n_0
    );
refclk_stable_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => refclk_stable_count_reg(22),
      I1 => refclk_stable_count_reg(23),
      I2 => refclk_stable_count_reg(20),
      I3 => refclk_stable_count_reg(21),
      I4 => refclk_stable_count_reg(25),
      I5 => refclk_stable_count_reg(24),
      O => refclk_stable_i_5_n_0
    );
refclk_stable_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => refclk_stable_count_reg(28),
      I1 => refclk_stable_count_reg(29),
      I2 => refclk_stable_count_reg(26),
      I3 => refclk_stable_count_reg(27),
      I4 => refclk_stable_count_reg(31),
      I5 => refclk_stable_count_reg(30),
      O => refclk_stable_i_6_n_0
    );
refclk_stable_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => refclk_stable_i_1_n_0,
      Q => refclk_stable_reg_n_0,
      R => '0'
    );
\reset_time_out_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"440000FF50505050"
    )
        port map (
      I0 => tx_state(3),
      I1 => txresetdone_s3,
      I2 => init_wait_done_reg_n_0,
      I3 => tx_state(1),
      I4 => tx_state(2),
      I5 => tx_state(0),
      O => \reset_time_out_i_2__0_n_0\
    );
reset_time_out_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => sync_cplllock_n_0,
      Q => reset_time_out,
      R => \out\(0)
    );
run_phase_alignment_int_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB0002"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      I2 => tx_state(2),
      I3 => tx_state(1),
      I4 => run_phase_alignment_int_reg_n_0,
      O => run_phase_alignment_int_i_1_n_0
    );
run_phase_alignment_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => run_phase_alignment_int_i_1_n_0,
      Q => run_phase_alignment_int_reg_n_0,
      R => \out\(0)
    );
run_phase_alignment_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => '1',
      D => data_out,
      Q => run_phase_alignment_int_s3,
      R => '0'
    );
sync_TXRESETDONE: entity work.gig_ethernet_pcs_pma_0_sync_block_4
     port map (
      data_out => txresetdone_s2,
      data_sync_reg1_0 => data_sync_reg1_0,
      independent_clock_bufg => independent_clock_bufg
    );
sync_cplllock: entity work.gig_ethernet_pcs_pma_0_sync_block_5
     port map (
      E(0) => sync_cplllock_n_1,
      \FSM_sequential_tx_state_reg[0]\ => \FSM_sequential_tx_state[3]_i_3_n_0\,
      \FSM_sequential_tx_state_reg[0]_0\ => \FSM_sequential_tx_state[3]_i_4_n_0\,
      \FSM_sequential_tx_state_reg[0]_1\ => \FSM_sequential_tx_state[3]_i_6_n_0\,
      \FSM_sequential_tx_state_reg[0]_2\ => time_out_2ms_reg_n_0,
      \FSM_sequential_tx_state_reg[0]_3\ => \FSM_sequential_tx_state[3]_i_7_n_0\,
      \FSM_sequential_tx_state_reg[0]_4\ => pll_reset_asserted_reg_n_0,
      \FSM_sequential_tx_state_reg[0]_5\ => refclk_stable_reg_n_0,
      \FSM_sequential_tx_state_reg[0]_6\ => \FSM_sequential_tx_state[0]_i_3_n_0\,
      Q(3 downto 0) => tx_state(3 downto 0),
      data_sync_reg1_0 => data_sync_reg1_2,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_lock_reclocked => mmcm_lock_reclocked,
      reset_time_out => reset_time_out,
      reset_time_out_reg => sync_cplllock_n_0,
      reset_time_out_reg_0 => \reset_time_out_i_2__0_n_0\,
      reset_time_out_reg_1 => init_wait_done_reg_n_0
    );
sync_mmcm_lock_reclocked: entity work.gig_ethernet_pcs_pma_0_sync_block_6
     port map (
      SR(0) => sync_mmcm_lock_reclocked_n_0,
      data_out => mmcm_lock_i,
      data_sync_reg1_0 => data_sync_reg1_1,
      independent_clock_bufg => independent_clock_bufg
    );
sync_run_phase_alignment_int: entity work.gig_ethernet_pcs_pma_0_sync_block_7
     port map (
      data_in => run_phase_alignment_int_reg_n_0,
      data_out => data_out,
      data_sync_reg6_0 => data_sync_reg1
    );
sync_time_out_wait_bypass: entity work.gig_ethernet_pcs_pma_0_sync_block_8
     port map (
      data_in => time_out_wait_bypass_reg_n_0,
      data_out => time_out_wait_bypass_s2,
      independent_clock_bufg => independent_clock_bufg
    );
sync_tx_fsm_reset_done_int: entity work.gig_ethernet_pcs_pma_0_sync_block_9
     port map (
      data_in => \^data_in\,
      data_out => tx_fsm_reset_done_int_s2,
      data_sync_reg1_0 => data_sync_reg1
    );
time_out_2ms_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AE"
    )
        port map (
      I0 => time_out_2ms_reg_n_0,
      I1 => \time_out_2ms_i_2__0_n_0\,
      I2 => time_out_2ms_i_3_n_0,
      I3 => reset_time_out,
      O => time_out_2ms_i_1_n_0
    );
\time_out_2ms_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000800"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(18),
      I2 => time_out_counter_reg(10),
      I3 => time_out_counter_reg(12),
      I4 => time_out_counter_reg(5),
      I5 => time_tlock_max_i_3_n_0,
      O => \time_out_2ms_i_2__0_n_0\
    );
time_out_2ms_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => time_out_counter_reg(7),
      I1 => time_out_counter_reg(14),
      I2 => \time_out_2ms_i_4__0_n_0\,
      I3 => time_out_2ms_i_5_n_0,
      O => time_out_2ms_i_3_n_0
    );
\time_out_2ms_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => time_out_counter_reg(4),
      I1 => time_out_counter_reg(3),
      I2 => time_out_counter_reg(8),
      I3 => time_out_counter_reg(6),
      O => \time_out_2ms_i_4__0_n_0\
    );
time_out_2ms_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEF"
    )
        port map (
      I0 => time_out_counter_reg(0),
      I1 => time_out_counter_reg(13),
      I2 => time_out_counter_reg(9),
      I3 => time_out_counter_reg(2),
      I4 => time_out_counter_reg(1),
      O => time_out_2ms_i_5_n_0
    );
time_out_2ms_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_2ms_i_1_n_0,
      Q => time_out_2ms_reg_n_0,
      R => '0'
    );
time_out_500us_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AAAAEAAA"
    )
        port map (
      I0 => time_out_500us_reg_n_0,
      I1 => time_out_500us_i_2_n_0,
      I2 => time_out_counter_reg(5),
      I3 => time_out_counter_reg(10),
      I4 => time_out_2ms_i_3_n_0,
      I5 => reset_time_out,
      O => time_out_500us_i_1_n_0
    );
time_out_500us_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000008"
    )
        port map (
      I0 => time_out_counter_reg(15),
      I1 => time_out_counter_reg(16),
      I2 => time_out_counter_reg(11),
      I3 => time_out_counter_reg(12),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(17),
      O => time_out_500us_i_2_n_0
    );
time_out_500us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_500us_i_1_n_0,
      Q => time_out_500us_reg_n_0,
      R => '0'
    );
\time_out_counter[0]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFBFFFF"
    )
        port map (
      I0 => time_tlock_max_i_3_n_0,
      I1 => \time_out_counter[0]_i_3__0_n_0\,
      I2 => time_out_2ms_i_3_n_0,
      I3 => time_out_counter_reg(10),
      I4 => time_out_counter_reg(12),
      I5 => time_out_counter_reg(5),
      O => time_out_counter
    );
\time_out_counter[0]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(18),
      O => \time_out_counter[0]_i_3__0_n_0\
    );
\time_out_counter[0]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(0),
      O => \time_out_counter[0]_i_4_n_0\
    );
\time_out_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_7\,
      Q => time_out_counter_reg(0),
      R => reset_time_out
    );
\time_out_counter_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \time_out_counter_reg[0]_i_2_n_0\,
      CO(2) => \time_out_counter_reg[0]_i_2_n_1\,
      CO(1) => \time_out_counter_reg[0]_i_2_n_2\,
      CO(0) => \time_out_counter_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \time_out_counter_reg[0]_i_2_n_4\,
      O(2) => \time_out_counter_reg[0]_i_2_n_5\,
      O(1) => \time_out_counter_reg[0]_i_2_n_6\,
      O(0) => \time_out_counter_reg[0]_i_2_n_7\,
      S(3 downto 1) => time_out_counter_reg(3 downto 1),
      S(0) => \time_out_counter[0]_i_4_n_0\
    );
\time_out_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_5\,
      Q => time_out_counter_reg(10),
      R => reset_time_out
    );
\time_out_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_4\,
      Q => time_out_counter_reg(11),
      R => reset_time_out
    );
\time_out_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_7\,
      Q => time_out_counter_reg(12),
      R => reset_time_out
    );
\time_out_counter_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[8]_i_1_n_0\,
      CO(3) => \time_out_counter_reg[12]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[12]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[12]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[12]_i_1_n_4\,
      O(2) => \time_out_counter_reg[12]_i_1_n_5\,
      O(1) => \time_out_counter_reg[12]_i_1_n_6\,
      O(0) => \time_out_counter_reg[12]_i_1_n_7\,
      S(3 downto 0) => time_out_counter_reg(15 downto 12)
    );
\time_out_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_6\,
      Q => time_out_counter_reg(13),
      R => reset_time_out
    );
\time_out_counter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_5\,
      Q => time_out_counter_reg(14),
      R => reset_time_out
    );
\time_out_counter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_4\,
      Q => time_out_counter_reg(15),
      R => reset_time_out
    );
\time_out_counter_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1_n_7\,
      Q => time_out_counter_reg(16),
      R => reset_time_out
    );
\time_out_counter_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[12]_i_1_n_0\,
      CO(3 downto 2) => \NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \time_out_counter_reg[16]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED\(3),
      O(2) => \time_out_counter_reg[16]_i_1_n_5\,
      O(1) => \time_out_counter_reg[16]_i_1_n_6\,
      O(0) => \time_out_counter_reg[16]_i_1_n_7\,
      S(3) => '0',
      S(2 downto 0) => time_out_counter_reg(18 downto 16)
    );
\time_out_counter_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1_n_6\,
      Q => time_out_counter_reg(17),
      R => reset_time_out
    );
\time_out_counter_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1_n_5\,
      Q => time_out_counter_reg(18),
      R => reset_time_out
    );
\time_out_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_6\,
      Q => time_out_counter_reg(1),
      R => reset_time_out
    );
\time_out_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_5\,
      Q => time_out_counter_reg(2),
      R => reset_time_out
    );
\time_out_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_4\,
      Q => time_out_counter_reg(3),
      R => reset_time_out
    );
\time_out_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_7\,
      Q => time_out_counter_reg(4),
      R => reset_time_out
    );
\time_out_counter_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[0]_i_2_n_0\,
      CO(3) => \time_out_counter_reg[4]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[4]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[4]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[4]_i_1_n_4\,
      O(2) => \time_out_counter_reg[4]_i_1_n_5\,
      O(1) => \time_out_counter_reg[4]_i_1_n_6\,
      O(0) => \time_out_counter_reg[4]_i_1_n_7\,
      S(3 downto 0) => time_out_counter_reg(7 downto 4)
    );
\time_out_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_6\,
      Q => time_out_counter_reg(5),
      R => reset_time_out
    );
\time_out_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_5\,
      Q => time_out_counter_reg(6),
      R => reset_time_out
    );
\time_out_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_4\,
      Q => time_out_counter_reg(7),
      R => reset_time_out
    );
\time_out_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_7\,
      Q => time_out_counter_reg(8),
      R => reset_time_out
    );
\time_out_counter_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[4]_i_1_n_0\,
      CO(3) => \time_out_counter_reg[8]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[8]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[8]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[8]_i_1_n_4\,
      O(2) => \time_out_counter_reg[8]_i_1_n_5\,
      O(1) => \time_out_counter_reg[8]_i_1_n_6\,
      O(0) => \time_out_counter_reg[8]_i_1_n_7\,
      S(3 downto 0) => time_out_counter_reg(11 downto 8)
    );
\time_out_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_6\,
      Q => time_out_counter_reg(9),
      R => reset_time_out
    );
time_out_wait_bypass_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AB00"
    )
        port map (
      I0 => time_out_wait_bypass_reg_n_0,
      I1 => tx_fsm_reset_done_int_s3,
      I2 => time_out_wait_bypass_i_2_n_0,
      I3 => run_phase_alignment_int_s3,
      O => time_out_wait_bypass_i_1_n_0
    );
time_out_wait_bypass_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEFFFFFFFFF"
    )
        port map (
      I0 => time_out_wait_bypass_i_3_n_0,
      I1 => time_out_wait_bypass_i_4_n_0,
      I2 => wait_bypass_count_reg(5),
      I3 => wait_bypass_count_reg(13),
      I4 => wait_bypass_count_reg(11),
      I5 => time_out_wait_bypass_i_5_n_0,
      O => time_out_wait_bypass_i_2_n_0
    );
time_out_wait_bypass_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => wait_bypass_count_reg(16),
      I1 => wait_bypass_count_reg(9),
      I2 => wait_bypass_count_reg(12),
      I3 => wait_bypass_count_reg(10),
      O => time_out_wait_bypass_i_3_n_0
    );
time_out_wait_bypass_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => wait_bypass_count_reg(4),
      I1 => wait_bypass_count_reg(15),
      I2 => wait_bypass_count_reg(6),
      I3 => wait_bypass_count_reg(0),
      O => time_out_wait_bypass_i_4_n_0
    );
time_out_wait_bypass_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4000000000000000"
    )
        port map (
      I0 => wait_bypass_count_reg(8),
      I1 => wait_bypass_count_reg(1),
      I2 => wait_bypass_count_reg(7),
      I3 => wait_bypass_count_reg(14),
      I4 => wait_bypass_count_reg(2),
      I5 => wait_bypass_count_reg(3),
      O => time_out_wait_bypass_i_5_n_0
    );
time_out_wait_bypass_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => '1',
      D => time_out_wait_bypass_i_1_n_0,
      Q => time_out_wait_bypass_reg_n_0,
      R => '0'
    );
time_out_wait_bypass_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_wait_bypass_s2,
      Q => time_out_wait_bypass_s3,
      R => '0'
    );
time_tlock_max_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AAAAAAEA"
    )
        port map (
      I0 => time_tlock_max_reg_n_0,
      I1 => time_tlock_max_i_2_n_0,
      I2 => time_out_counter_reg(5),
      I3 => time_tlock_max_i_3_n_0,
      I4 => time_tlock_max_i_4_n_0,
      I5 => reset_time_out,
      O => time_tlock_max_i_1_n_0
    );
time_tlock_max_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => time_out_counter_reg(14),
      I1 => time_out_counter_reg(12),
      I2 => time_out_counter_reg(10),
      I3 => time_out_counter_reg(7),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(17),
      O => time_tlock_max_i_2_n_0
    );
time_tlock_max_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(15),
      I2 => time_out_counter_reg(11),
      O => time_tlock_max_i_3_n_0
    );
time_tlock_max_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => time_out_2ms_i_5_n_0,
      I1 => time_out_counter_reg(6),
      I2 => time_out_counter_reg(8),
      I3 => time_out_counter_reg(3),
      I4 => time_out_counter_reg(4),
      O => time_tlock_max_i_4_n_0
    );
time_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_tlock_max_i_1_n_0,
      Q => time_tlock_max_reg_n_0,
      R => '0'
    );
tx_fsm_reset_done_int_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF1000"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(0),
      I3 => tx_state(3),
      I4 => \^data_in\,
      O => tx_fsm_reset_done_int_i_1_n_0
    );
tx_fsm_reset_done_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => tx_fsm_reset_done_int_i_1_n_0,
      Q => \^data_in\,
      R => \out\(0)
    );
tx_fsm_reset_done_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => '1',
      D => tx_fsm_reset_done_int_s2,
      Q => tx_fsm_reset_done_int_s3,
      R => '0'
    );
txresetdone_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => txresetdone_s2,
      Q => txresetdone_s3,
      R => '0'
    );
\wait_bypass_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => run_phase_alignment_int_s3,
      O => clear
    );
\wait_bypass_count[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_wait_bypass_i_2_n_0,
      I1 => tx_fsm_reset_done_int_s3,
      O => \wait_bypass_count[0]_i_2_n_0\
    );
\wait_bypass_count[0]_i_4__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_bypass_count_reg(0),
      O => \wait_bypass_count[0]_i_4__0_n_0\
    );
\wait_bypass_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_7\,
      Q => wait_bypass_count_reg(0),
      R => clear
    );
\wait_bypass_count_reg[0]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \wait_bypass_count_reg[0]_i_3_n_0\,
      CO(2) => \wait_bypass_count_reg[0]_i_3_n_1\,
      CO(1) => \wait_bypass_count_reg[0]_i_3_n_2\,
      CO(0) => \wait_bypass_count_reg[0]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \wait_bypass_count_reg[0]_i_3_n_4\,
      O(2) => \wait_bypass_count_reg[0]_i_3_n_5\,
      O(1) => \wait_bypass_count_reg[0]_i_3_n_6\,
      O(0) => \wait_bypass_count_reg[0]_i_3_n_7\,
      S(3 downto 1) => wait_bypass_count_reg(3 downto 1),
      S(0) => \wait_bypass_count[0]_i_4__0_n_0\
    );
\wait_bypass_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_5\,
      Q => wait_bypass_count_reg(10),
      R => clear
    );
\wait_bypass_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_4\,
      Q => wait_bypass_count_reg(11),
      R => clear
    );
\wait_bypass_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_7\,
      Q => wait_bypass_count_reg(12),
      R => clear
    );
\wait_bypass_count_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[8]_i_1_n_0\,
      CO(3) => \wait_bypass_count_reg[12]_i_1_n_0\,
      CO(2) => \wait_bypass_count_reg[12]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[12]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[12]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[12]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[12]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[12]_i_1_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(15 downto 12)
    );
\wait_bypass_count_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_6\,
      Q => wait_bypass_count_reg(13),
      R => clear
    );
\wait_bypass_count_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_5\,
      Q => wait_bypass_count_reg(14),
      R => clear
    );
\wait_bypass_count_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_4\,
      Q => wait_bypass_count_reg(15),
      R => clear
    );
\wait_bypass_count_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[16]_i_1_n_7\,
      Q => wait_bypass_count_reg(16),
      R => clear
    );
\wait_bypass_count_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[12]_i_1_n_0\,
      CO(3 downto 0) => \NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED\(3 downto 1),
      O(0) => \wait_bypass_count_reg[16]_i_1_n_7\,
      S(3 downto 1) => B"000",
      S(0) => wait_bypass_count_reg(16)
    );
\wait_bypass_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_6\,
      Q => wait_bypass_count_reg(1),
      R => clear
    );
\wait_bypass_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_5\,
      Q => wait_bypass_count_reg(2),
      R => clear
    );
\wait_bypass_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_4\,
      Q => wait_bypass_count_reg(3),
      R => clear
    );
\wait_bypass_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_7\,
      Q => wait_bypass_count_reg(4),
      R => clear
    );
\wait_bypass_count_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[0]_i_3_n_0\,
      CO(3) => \wait_bypass_count_reg[4]_i_1_n_0\,
      CO(2) => \wait_bypass_count_reg[4]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[4]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[4]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[4]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[4]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[4]_i_1_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(7 downto 4)
    );
\wait_bypass_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_6\,
      Q => wait_bypass_count_reg(5),
      R => clear
    );
\wait_bypass_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_5\,
      Q => wait_bypass_count_reg(6),
      R => clear
    );
\wait_bypass_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_4\,
      Q => wait_bypass_count_reg(7),
      R => clear
    );
\wait_bypass_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_7\,
      Q => wait_bypass_count_reg(8),
      R => clear
    );
\wait_bypass_count_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[4]_i_1_n_0\,
      CO(3) => \wait_bypass_count_reg[8]_i_1_n_0\,
      CO(2) => \wait_bypass_count_reg[8]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[8]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[8]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[8]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[8]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[8]_i_1_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(11 downto 8)
    );
\wait_bypass_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_6\,
      Q => wait_bypass_count_reg(9),
      R => clear
    );
\wait_time_cnt[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      O => wait_time_cnt0(0)
    );
\wait_time_cnt[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      I1 => wait_time_cnt_reg(1),
      O => \wait_time_cnt[1]_i_1_n_0\
    );
\wait_time_cnt[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => wait_time_cnt_reg(1),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(2),
      O => \wait_time_cnt[2]_i_1_n_0\
    );
\wait_time_cnt[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE01"
    )
        port map (
      I0 => wait_time_cnt_reg(2),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(1),
      I3 => wait_time_cnt_reg(3),
      O => \wait_time_cnt[3]_i_1_n_0\
    );
\wait_time_cnt[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0001"
    )
        port map (
      I0 => wait_time_cnt_reg(3),
      I1 => wait_time_cnt_reg(1),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(2),
      I4 => wait_time_cnt_reg(4),
      O => \wait_time_cnt[4]_i_1_n_0\
    );
\wait_time_cnt[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000001"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[5]_i_1_n_0\
    );
\wait_time_cnt[6]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0700"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(3),
      I3 => tx_state(0),
      O => wait_time_cnt0_0
    );
\wait_time_cnt[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => sel
    );
\wait_time_cnt[6]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => \wait_time_cnt[6]_i_3_n_0\
    );
\wait_time_cnt[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[6]_i_4_n_0\
    );
\wait_time_cnt_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => wait_time_cnt0(0),
      Q => wait_time_cnt_reg(0),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[1]_i_1_n_0\,
      Q => wait_time_cnt_reg(1),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[2]_i_1_n_0\,
      Q => wait_time_cnt_reg(2),
      S => wait_time_cnt0_0
    );
\wait_time_cnt_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[3]_i_1_n_0\,
      Q => wait_time_cnt_reg(3),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[4]_i_1_n_0\,
      Q => wait_time_cnt_reg(4),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[5]_i_1_n_0\,
      Q => wait_time_cnt_reg(5),
      S => wait_time_cnt0_0
    );
\wait_time_cnt_reg[6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[6]_i_3_n_0\,
      Q => wait_time_cnt_reg(6),
      S => wait_time_cnt0_0
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2023.2"
`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
TCSZ2Ye5IRfHavlF8Mno1PI9xixWuSiNh3ssU1FQtkjW1fmNtc2c3x12slL242UQayI0rzZTqe6S
edtecLHTOnzxXpCZjjU8NFmgLPerTSDZ1W5YhyIi9j0Ap4YBpvaA1ojM0+r0Cx+dMOXohQGeyljq
+fnTaFTUe2678DxpqHk=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
NIzZVKMB1/8UX2qb4BB+VXCafEKXsrn7axB1cJDPqDCzSyt/2KG1NEEZTDHZVzIr8Bf9501PyXmL
VowTAAXX/RopKyKOM1xJN/qLtqXxegH2a4dIkUxDIIclIcbv/smna9VCwI7m6JhrnKsNciTTilgR
27S/h6JPpZsZAEmsNxxTC70WQhQSM8TlHJjZg3KDc5KTnvC/mVTk6I05U6x0Bdd1YR9GBvhwRqhP
B1ukL/1JVOwR9Ce9p+EHFE/xyApypCjQPGwq+8IFQgS8wltVZHX6eSMw17Q0wGCY+LHduRTA+abV
LvAR0NPf7PKQUSCECe2mBbLPO7wD4BO5RAkJeA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
udw4XxxzcaDNM1qWnRgZ2JEM1MMqnKwKVqha/krU9EyUAsyATjQEMBqjlOHw5QXMU2jjizlL20Nl
h2pF7iKo1S+7TS54Y/UIJANp+Dl46V/qfy6/yBnE4YclHON1k0jRao4C6T951tgXuCAIQEmXbr87
aJfL2dNqORH+TDKUBdc=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
JFe89rPDaiIUicPDNoXCg8pJUoYRyVDoW/5yE4T0Cp9xDHtuIyKQVbC7jVb92OsgJ5GHDm7DH2D2
rYZKrdCIqPt2jo7DG6bcJuDFcisZb11HLlYWNsK2Vqs9DdsTPViykeE05CD5AgfDxb983x8F1meK
w8zjeGoD44djsaRA+lvP1zLhl24q5LWFJdPSyIT7uWZwhxHqlyJu85ToXLuwZQZO76Mp+1mitxDy
vleizC5rnk/4hqxfEFS21Qi1TwCz5hdU+H3nA3dTe1KRY+obbFP7sRWKfmr9Rcf9enRvbaEbLoJA
9ADkl72jc1Aqlnd+YCGq4EmbElbWLxblpamncA==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IV5qOrW4jXfiGt6hz/YcNm+/H3ij0v503CF3Kvut91tUWldzNzyLt4pIZEWEzSmn6RcpcLNN88po
1kt45UdSBz+mL5HDQaw4J+VGD/cCBmW1jnOclCf82kwju1MIDfa2EKicjqaykCUROxV7cwg07FFp
clLfIwd4kxgSWnGzeZi1IGezx7OpBsAkBTz9ha4WttEm0+D29DF9O4GaQl6q8IBeA0QIrO10EESt
slfRi2evxdOeTZBVFoXU91OszneH/prZqyCsHeyvTa8PABTZ+Y4CH6ICZCXRn7QTNJgoYSGABuPs
87saNJgzomjyaO6IzGl1fBgMIsIurKw90DE8Jw==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2022_10", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Eek/EeBa5kEiakvxzHC3fZ0NXCsvWnLN8FYKLyImepfBUv2jdVDY2j6Qs928DJaMO8pBuO9SGcu3
47rhcN1DAjZza5Ac83W24fngY1+YWblivVc8AoggXS1t2Y7Dy1vf9+ZxUdOvq63sje+fDJxapZwK
3HQGdtBX86RTaUS5K+HyI1FTmcIhUYmJWmxQjIxLla7FF1QZ4XpTCfqAG5i7ZKlYSoDFb8sjCRG4
XWFuk1dbL2UfZPxXZ7XHIm+03Ck/JsHtsjLCc8oTB/9MLom2HX9SjX8H6tFbEXR1NatCFWQ04JKL
kHSYD/xDlwjhN9CRvowRhNJaYSmKQT646hlNoA==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
gaID+cgqOdyoJPDKM9JAyimEjoxNakxuNjYf52N14HEyn4NQksF7thq/bXWc82vmfdc8aodx1+ky
i8uuKszW1WwV+apGSqk7YXBCxx3ACfMsPzNzeDQ2HVzGfznpQD80Eu7I7iwtz3k5Mr31iaeM1kQa
oddk6CkVESI8CD21PQHMVeu0LKLZJp8k8NHf3i0UOXsP5o768iecieYQh2VYXZ6HORDDyd+IpDB1
CAFBZctXco8C1w74wCB0LXUSYInc5ythxBURkPPTJ1GBuXpoQGZD2sNiI2Htl0y1toEdfgExWZ+0
+4Docnd9TgOGhAhZzUcj3c+6cQNbgCB847/G6w==

`protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`protect key_block
tEBTlBoXowC0cOrrCu9pEZ6t9FjMS6GWThbzsXMvR2xb9HBcccdvXT7EfASM09KkNzvlYoyNBWct
0TRl1BNzzlomu3G857U6kezS+CCRF/K2qOhhxHFxEfuM0qblRVdNHoCGGMM4PkE/rt9M7IqYoXQg
WOHI1ydpZZn08aVL9QYJgz6ZuVHNLwSpL6rjFFDXV1cB82gVFBkRP/0NxpGW2WH6YA/MJ0czV0ji
o0umOWluEwUObdytKX1lfuNYimI0ziWrovqq2osL8J7NBKDUl2R6gJ51DObsTBgC8uyUHVibyNHy
nhzTpwcBeeXdtAueCg1BlHDIwglcMUdy0sBZEyHM/CLzpxgr1A+uUcmzlWx1drrc8lRNwGMFDDJQ
9OzoHBABtNt8N3bbO8A+rE9HtsjMVr1TxHhUTxBhWcypwra+xzsGykln/IP3JBwwQR0+d+V8/Vec
5Bh03crJTvJZUbYidozNoaPOfnHi0NxFDNdL7L1i75T+H6bqeE1ADR/4

`protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GM0AtcDH85MjCjnA/pijf0ZCJap8X+OYUX7W1StOwznqG2XPd9DhtvTyqD0c8/7BTdeCzGUK5iqe
QiGwEcy1dCrSVZW0KtjFXllkYV2ai3/Qn7Bgg1YuzxifEFKe6ClTsByfgjqRdyZeNSAldwvx9ZtT
0ZhijV96K37zXwfXFeKDmxOZOV553ovWfXGekaS1EPmSluoDYBMQKc2XV+ZUXR7n5NI/6E3QdK7K
utsZyrFYyJdYW8Po28hQf1nWeQP6+PxQB6wi/P6sUzudntNcQ7uLRr4PTz6twPPqYwUF+7YW8baL
p/2EFPf8y6fBb+DOBCnzmGZvmq+M2qQot14r+g==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Ot2lizvbpt8emXxoZl10oi4H/aTQTzrHDg0mf7DDs3BS7iyFsRmaQCG/BRF/mwjlw+EbST4/x/DJ
d6Hf6LIa6mllnMD6G+uVHQ1z31eASHPw3m+WnPMr/zCNuebPcDitgiXWmq3SAS9byYvxiwcDvjn6
CMh89pvlx8xLiFUoo5j/lAPe4cPBJwSMleQLLB989s6rByi0lVW0QiLTzakaB8DHBMvhIYEfi74m
Lxby6+nYRGrAUKPOemP0Ag/LW83Eup/Wa0jVOtxzlj3foiYhg2mWCt2zyFhgQsDA+oEsDa/KZc0F
OUzOI8vFDrwPmYRwd2ejFI9Nz3/1mb05VQmDRA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
OamBwohUIPOIa5bsJvu3upqnGl4f3jNYS35bg4S059C9qVDgQJcw/it81deNA5zFvzX3Cq9CiO9K
zl02VsdpFgNPjSwEO8F47LJZ4fHx99EmESBogsNwUNitzkuYTLCb7F7ZF2WSJExQ0KsYt+TRp2UH
yQEvpM1lHQYUXxzjw08qUI5ssSnOsQFydvP8BwA/6aGrVJ+LuEgPVdMqLBn0EeAmRsynxJ7OhPGV
DlvHQwtVuBrkvjQHED/Ye43ZIeWPm/xOcjNfZjYeOvdEJqTbaviR0Fo1LFx8EX80uvdFeK55ywDN
wzoZ4cVH87f6VwR1xHdo0JpVxrajZpSZ0jPudw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 59328)
`protect data_block
zGVL3ji0I3fTqDJdPT1aiblwA9LMgjxCGUEAwxMonK14Dw64f9ZaoRvZpfZmX70tS4Y9KXuHN53B
I8EKQs8vxPRkDKvoLhQZtS7uvDKP/1vsv1MgUmrOQ53r/lRv/rpdCpaEKPCgF2dB7NIB9bc3emkj
zrm6z4KSFDPlZWhJeaDLXGRLyHNir2IkpHc4/yc52rs9590FkXGVKPs023/1EA6XTUJLGEmTvfT7
KtC2GHXoOQcscvsSDrilW1aiHQl2T0H6mZr3UR+U8Zd37HQMVtzqG3MwRRcTXv1DKeV3yaZCqxXy
cCqso0rGnveheHn/dx/pUXG3eLqMGVfM8WdixGJgaqenRlUZk3sG/AyMiC0+VWbcEreG7VUSxVhC
kL43qsharX7EIJWokDRJxebQwHrZYAe+RuUP2PtE3lObXCRSaHndS6fhH/YFfx3vQpkUU7Iz6DL8
/mAb0tbGOdcapAlw/bVG4xcu8p2+moQJn71UqDKdYoAXgmScOdGWRnMpGqVgj9XMG9yFVjn7r7ot
LBaTm4FUw4Iz7HcAnrBlt4XEjOeQHscU4f1zp56s8yEAcmmUryP9UUEqUsufe/QNz1MssWBCbBxd
qRuNRrv4uFhVXx4h9sS9fU0hzOXsninJE25baq07EQp3HhYeLcqvmSDvpu8BeZ0PPkrke1PYwG14
Crov76UnH/s+iWpgoUYBnqNd9F9AQC4dAOjeH+09B+Dz7U1DyjypkJhW+5a2AzInMb5hj4fVhkfm
pgEhF9kIME++9BmQjICa8CoKRg18gVU0rA1p8ShbKOp6SJYIc3pHtFzxUOqd+euMSBQ3kYwkKVHB
cwKNc3fL+IK74/Xy1rSkfD2hSM4rndPGkYKHEiQVM1Yzs8YKe0RMQ+hzBnjnvfbH1ITE1FqoBFKv
JJ67J7ba0IiEMr7CEw68koSr13jQxOZMkLR8OhPyWg6LDc3zDuc971qfdUeiJyETogAOnnDuHHzZ
JPt+ZB8xyUJRiNlWl1RA7bT+D71XwzHTfKvIkY+yByISXE9dVKGMweqInOrLe/jeHDOb2WBiJjc7
kDlImZo1vdi8oRIA4Oo+I4dhLlbJFxWupsapxLhNENooxtpVmmBoT6deLSFyOMs/bu4qHjn8AVjd
AesXIUmMfy07nCaIT+vreVpOdYTdz3vVdBqqqnj4F/Zp7jdL5zhZEztYtUh2XsZsVNsU2/jSsYe9
32IXd73VRRt5Qqcy9OWH5rJBznxSGxIdKk7REoH3jCizoLuLP7lQz5T7x+8/Gz+6Mc8x7tzJbaJP
5/vHXfErho3Z/Bzw1bvteCNr0oQcTfrjwgzyxq/gDvZpeZUL/9iBedPj3V7fz/2JxMNnzU4smd/y
p58ci6ce/c4XWrajhEaBRZUXHYuv21NVxuKPdyQPsQ8giUTf4tyL1ZFCE5LWiSF094ZuKzrd2d9j
NiHOTHczCH3QhK9EwSKIlUmLEuoO3+0gTCMQIZuFOIxEWf7W2PQd3ZPt7HJ7O8lJ8JcFRDTKt9ES
z6TsVP8oyVbrDSB3hwkAN8Qoe+khGcJ/yyWulGymFxBTQNuL5aKlvfPIqITsISWxbSh+7EDs1uEd
V8RCWBRS039f5xLWN7Ah7VVzhtDTngoGUL8WutTjxbmHepep83JuvLYh18BZlU7uEiG5/JsX6XJ6
bou2m0TKBIPCcoY4WMJvAwcqOLQqLECk7W4UuSgqjDkBfrsClEelF4WSFQS3/Xzthy0NKjXWXW2w
gWZv5JY68avDQ63FZYo7tQS7npUe6poJ1gm5UUlqzS2c/m/Ng8+MZ0t2IVo4RqCxRQR5Izlpm1+C
DehQ+7i8DdJAhHfP3nhg28YGwOjTbBV28uI57Go/4bCM7VvxvkA1ciMxZ5wlxCzp8DH56h4FCD+f
Mifr0lfJpjquD3cZ9UTMaQeCFU4A7wS/pmouCyaZYqIEw5m8bxYHphzzX8SCHbX/c16a4uUKqrsd
uJL/KRje1J5AtvspbmdSC7ShX1P3JKOMs8BMamwnc+JKJZYLsmmtwncO7+ZCqUWH9wnwXnHoDebK
R9T19AkEy6AbQ/19kGrnTszKvfhL6VCtRQds5y5zww3r5bTFtbO76qBEkV6js3oZc5XfQAf0r8TB
5M6C+INY+3qxYXu9eJHqliYvUQcL2orxg/gTFFUT3MoQUOrSLDOHMBJwRkJX+Fclc3peMzqKnstx
gE1zJmbJZjwhc1ZkRVnCvyhdreAhUd2WtxBiOj0CsZ//9j+yfRM3nOaYOKBMUNFbrjqAxKpjI/zM
sYfDUKU8FTfci2s1cNQD/lxpLO+lc1B8uJ3FnZIs0MBWMT2lvQT4pdQLtiz1YFhDljz9pdEkVqyX
k2f3ry8FsKDmHV3yM16dUBA0oBzfVTdwLNf1/U9VCommDBnIysJRYyTGvz9WwucrvC3cLJVZFCz6
cTRyZjh812pq0xWdGB8VkTY3xp0LOd7F1nPrtD5UPEPJg4udmBZsdcRUtNOIeG2c3ZybCzclwZ/K
y2KnJF3Ek0OQ67YXDz8x2MG5+ePGaIHO3E8sp1ZI+Ocmkq2z2zRvyUskXmFduYhu1BgIep2UdeAA
qdS87rC3zWm+7hLeOkJJ8r3UEjoH8DM6wBrQpc4nVwb7SuZhBrN2RY3waYWGu81OLUYjqn4OpuCN
eTHQ9Tro/pGxDkMPbXL3byclyAW+tWMzkJewG6brSZSZ7Mualdg62h5Rvyy/LMrSUF49Ehwn1Rst
OQ0l607yVh1VPZA+i0AuksE4Zq1Bvu9etTgwEOdJ1Mt/zNTz/kfAngL3jzx4+wPUWtgRIvjFCrnO
+G8R+5Qh34Mi+1+IyJd/A7GHKWsEmnHrcYRfHrIboi3lBntNljVRXOFozyAlSHDBuJ0aBkMIF8Xj
xpwuG/ztwfyTl2EPJUJpR4//nTv59UNU+WiVGHQoQy2cGM/XprZiAluv+DOjVXbfHNJ1DlQgE8Z5
4wiqlvjTidFpbu6ycQmqCZxk+J2RVLlXqv1VQzyrfIZt4BSTqNVdNWn4YtoF9LGRbT0x3lcdGqw4
7WhK/pJzzgwQPEn9o7lozYOOI94MWYoPCjwb/fuAlKYl7Z2RU8Dn5j+dz04wnuJ2Et0IDRQYFSVD
S/41E327W2gbJuNrJNGDQLwb8oYDBBeuAu+REZ0EcGrimtRopuW7yQX4d+vUZWuV7G4tE9QgmqXI
1rJoL0h504j4Dkv8nWIwsh7rhr08q/HIHcA0xZ6SnUiweqrW9s01/k80BSFoEzto7tkO1jkztoXK
sGohND/yQ/FxV15DEAyd6aPZswzah8JwY39jA56yvfpGJ4eKQ46Vru3eXZd67f0WVQFQaxMgebso
LCesoU1TSLt4zAUGe0e3ZtWVKMwIkZdgU2954ZLIrcRaTRkkmqnehRDvA9TDulGyQ0AK8EHPx9FL
LDRcTTOwtmdLlywPiVr3L4M3Nfm5RX1RUdK1ltcJWUNIx1uF40O9daqN1+3e8klGj6mR/1UNbDuf
6hAEEuQj4QlAEDJ1P9nx7WZEcjXui4AtyljRAWV4iziivxIOus6xdc1GmPBE8zoTUwpuZmb9saUj
Nwi7uVtO+gQRayadKWxKUIAEHUpSL5cYA1d9g4KWfc0+TQO4TsSocXD1HDyIU+4LeTUc/WC/8wXB
J26VJ1/N4Q1sCCtuKspvLiceHbdou9pUxMuWEx6an2icYLSCYbN1RbMZZkyjRvAs6u9qamRFbF4p
ke921CNiniEzT8an0K8PlEfphkj0ga2IyPkAbvOphpplXLi4IE/LkNgx5m8JXI/YCSy/7vHsoJBN
RMfA0X8SypUXMPrut4oV1uq2RSV/OohkfkefxFF+cHlnYakqO7aGEbb8kMopCWguT+9eAyF0TC9e
wUT7cut/2oG/PLQBNj8keTMPzgVO0xOhDSmDIOV2hS1Fnh/2MkMZ1v8l6zYhWZv3hEuBKm9dxZG6
7BtzkaIXX2JPw3lPm1e83x4uJNBlQkrw3rmeWqrUE/wG6t17GEiyRbngukkye8DVz/hFEYFWUbp0
sOjOEmVr6PM38QjTHXM1BShfczrDqdKZ7bXS0M/Dys6Ev915sYbvJlyKYD9bTKz8ezFqFJbNFKvq
0Gf7onTEyDJt/A33zzfBeH5tUX1ZtR08wubWlO/bvtH1CaIeboJhb1WJzRvmNv/8OT9KkxXRQzHK
l5IgRgKK/fiv9Z0gnBI2bHoou8YbpdK2TrkUgVBxDkBkgoy7VWhKwf9BXysN5j757EjQBcKmtBe0
RnWk5aoEM/YCs9IPgHgDa/B3sJJL2/J1rwKe/o7Xnj7Tkxdabsbnod75kYe5dDc8UCXt9heEc42w
QrWAUHViqB74L3LODntann8BHrjNPIPxVGOi7z42I9+BNTuyIRZX4+a235UQ8Vad3e4NfeBfm6Jc
N8yiD8Wa9BUX1rZSUCupKiH/g27eKCjftgsJFkCHOcQZscDO9jhByc0k+k/LuHFemH5CpFMH6W21
Lngj/aJMmaccYNYutoVadQStMZXWiXuSt428R+c3bqUhgCW4ZkK5oyx7sovcmIyMK25h1A2Th23b
wiSqjMEk0fj6MC2a2LvUroWm5Cxhvjkhbmw1ZeSS46g68tX/w9+vDkuRi1/PTG6YzvtQZneWbLWF
PfZb5mk4IslxPz68z/NctBY3nkDWTTUHhqPEUgyoGdm1tjmVGYulFWLfCo/cP2p2xO16oy1bQqTU
9cTNDSJbPk1iN36AjeRF1Ae7WSqpM6M4L0Hcxo4Z9Mb8GlyCzPL7yXhn6qckmQNtC6deGndRvI35
FsIPdTN9j70/Hh198FJT1OJ/H5MchakUCga3KQr30qjS09MKHTzIEKi6yUbbUzQv99q4uMO+9f4a
f0leO1oKsdt/ik4g+2YbgEFvWXNrR0VtbGh8kx3COw9/Dd4mZdkZlV66/a3sgBSv2XjHcvCtQfXo
Hep0L48QgHns/mxuinduNw4W95ILfGeQqP8HHp9IFpz3UVYuRTg8hgg/vx1PjSClAAqg0CW6ZPb7
0PAhuMfQjlgFfcsXk33mePu66++ULCkui5JQeoMNAaHag8+WdXj5wfNvFzKMAdYVORvUcfxW1uvU
mkSzNRsDKT1kf7mVfzfQXWVuiWvE0c2VtZzc5MDFRVkip9N78psSKcKKFah586orrtLQjWnvRnYw
lZhifANYkGUEQkehcawtERMxE3P4WBy1Suf+OWzQWc7VvQxYyOOVtnUxTGolrWV5QmojAnk3ULWo
rWl5IJ3kya0LEz9xCW6cUNIzzB9VDOTMyQe19eCMWPvadA9vQPxB449ND245mjJbHDcG0Q8ZiZ4Z
i0EbdCTOOWNfdGh2gkbbWkiqEgtBznFr97ajNQvzk/SaedNydipBX5zbMrMm4q6pwNFFno2xrtVx
Op2YFDieoXeEoLX4ueb71bc3GwFWVlo50RuH5hMzi+QNpxEazVtCGxkmRCmYSpZYpZdoDJG/hEIe
bllhqbrfjonbu+BsaydINXhOTEsmGivld8X9w+PKxVumpNshFfZRo7XI+8WazJwoI+XIDHtlUcCj
MKtDTDylj2/HJtdUmRH1ch0nJLX6A2wxvVCgpKJdcA56M5CBE+RIjvy8OidIhJ5nOCGnNlQLtqs/
EMdEB5Aoe8KD0TnRLnNZJhUxtdfnaxed5yvsG0jfwA6hZxs4xBss5kg5svP+ngngut9xiQcsFWv5
fsD2YCWM+rg51MN8nZYa+e6Wajc1k+vB/Ru3C7oxImQRb8uncwCzQdr1DWMtZL/XMtQKtLfTXnND
fclh4YvcRYZPAxb3Gq+fD+gxUCPxWK72J9qzz4xrz/k1dlTiSKUSCNBjmxS48mJKHtxHwykbT0ku
74TPGGKcxQPu5+YPdrioOfqf3vjQWvJ860m7yXVErXVel3S3YQcI4SqpVILK/Iv4PenjVV9nkrPH
ugoCksU6WClnjh12ePwXtAXw9SqMAUCWsIJFSJBSJ1nMpl3gCcBR36rfxQ5xQqPgZ5wd/qeyNNef
P8YqdLXfPMeIy7VCLiGiU8rLulJpstCu5INwicduyBg1VRzPaXx1cB79nrFKnHQFz/JMDFTFKKIA
fwF79J7RxpI88LO9wK3RLlfVUlcOvuDT4B/B7uzAkpO4MiKDcRWgrGK/IdgwqDui81JItFu9Es07
VA0obypNUuY7nNeckikqNnfIFu3SGQdu1BReukwOGphhl4V01BSzjYvUoaCGhZ5VeH+8ix7+L6ti
+bJ4v2uxW0iUXO9h7PA+Hnoa6McYnqlDZ0fgGf0HRnnNqYGqeHI650iPAFreTAyusxEjl4Jfxtgn
rY0+xO01R1wak9P0LhVPF5+S7mv33QUhKPigkdEvaNkTeZwslsyPLHcVg15kp+yt0WvHYHAIbS+X
Vkj6n+aNe80IZeJcSkZmDJqS8w0ev8bZQpAo/ijvp/4fOE7KG6tzxeoRUB/vtzEXhJ9qy0NTpSTF
eGZ6+8kwU6aIsT3gSnRLXVQq5uuRdpCe4rJAXFJutdsJUlQB8NIVMOVXXkmlHCLF24N9p6qNQJ/N
UqDOfMfXxyxKFHsetnyA6Uxo4Xqlqwbm9ApQwn6VAYQ6R/FpD2BlNWEyZSgmVMJI5wkvSZGH9/py
H/KXkp281WqP6CAaCbmL4Ayzz/cbRmUnSJowHb1IMGIZe6mHDKoJ0nrANP1uhSX18l+UWAoluQGK
pweVhiwSN//w8sz309No//SC/qGgD1LezGGWboj8eKdOZoc77lduk7rK7v+bDsVxWr0hC+79I2BK
zkra+q2cAHiH13prEyceoOSojkaneSRak6oqZtXOjVif8wbB/Lp5NeD+d1vXBF9jT6S/lxaJF1+t
cRxgU6WBcFyUOQ12Lx4RoYSW/BMy6wse6xh46VEbfZeipVMAHBXHiAqSKM/tJlQzygHt05M9L9t4
rMQNKIuIfhQqoNiGLgaCuYvoGgALLKh1iyG/TKGEpxLTGleHDiaxckssiiF0syx2rlxdAGqjBOMS
W79/qNFBDTzG24IJsag4lIiRH0HlH68CfSLcrsbtuA2cCgnJRkQ3m/n5Rzbyrp8YH+mS/R9Pw5Sj
+wIVPmXDXQPWc6Kr0sBmcgwzYQ+LCvNNC6pwcTIDLEtoGp5K3NgpSZ2t018CpaFN5TVeLm20cSuY
Xq9lJtE/DbNfHjiUqf6ce5v8s9IScAoJtdtMWH8lsyHrJ8phopf+hyg5nGMp1QOD6S3sGAKTB7hc
+Q3zo2aP6pRsY3/fS5fFBRhcudxHpmlIIo6xMVhAuXYp01P/cCSUFvYwLlTF8FXnijREMbXP5Krv
MUWYq11J3BH0u9kVDBx++vxDBjlV5HtEastgVuv8SwzPHy5ZaxTDuDLJCnHpfOx39zBxtWrngOv3
fmP5qrrclwDwWSEPyAwoOIWgmeIhD/L0UF1IetYCUGi04Ivdd1dihGazXgwSeB2e9+0UNkDvKmuQ
kaXQhzTFgqepqjicfRV5eYOrUFty8CtDCkich4FMt2GBaZRO/WH9NG8bC8+uVIufLHWQjg0F8Ngq
rycHYQKp40rSIAmkslk3HD2tup+lM3VDbOC8EEhpCN4DRsLfh+oTEQOi+8ig5CBoTztoJsTgRuQm
HDVhw+ZOa7TNcjKSQmGX7vkZIM2QVB5Ba0MkVbxdmdRIG+MYMRz7v2xBKieKkC7IF9to1O+v8lD/
jMEvegk+YB1byDhpqlLxnBsEUwXwJDbkT/o9Y3P5XZ/1TmTeICqG8Hk8vtC1WqdVMZwmt9F3H8E6
U87CuCQAp7lt5LRtdLtZMtxAeIa2wFsmOS38gkMn2DhA9KNIWGRD1WMFJIppyFYhoz86ZW96rLx1
gYUhlEVkkHnmADkBC7SLRbXEwozk+WRmS6OL2/leLlxFXqq54hqPXJhRqcMZtdwqb2yXB8XZpP97
LNiqMpDYtRboWOqLpkdcNUduxetD5RUakd4kLrREduXcstwiPZjF6Z2OBmcH4WZih86mVgHID+hF
hG1GYLfPQfcuPyxtE8VgrRXqa6ZjMKsEkvX2wIPi0Aycre2yPB0JOyFjhwk4I7WVPZmMWgWVfmW5
bkarYVV0JmxNFCoGctaVDSViHedHxKlpI6RwhD9hRxSoqTMiUK99hEnbRW7GS+90eYrBhTey8HfF
CdCDdiGtUss7YCU2o8irtsfo3dzZPPzsc3T1FEMVTkSHslfD8jNG0/b2W55Y3+brY5lnpwf5ZMMv
H3O9RO6XBgVuh25wNtSkZj7ocqxgYb8dJ9aV4J8LWUc6hYK4hvjJnpJ8C5U5b3udFAxcjGxQ3JCu
tPhm++sdgYeQfSAZhIVR5OjRtd7x+DTXVzzj4eWUPnzii+4jTBpnkZCy62Rv++//V/UCGlxPeGBu
oeYm1ywNXVMjdrYQU0xwMCePD09WMzmDmBXI0TVXClEINqET8WXnb0RxMG7lGCmmelJz5CakG0j9
rZ6RLSynPJGT1cx2eXZO5PapxJWQMI0lntOlZ30lC9prMr/WHxNotbYWEltqR6cYS7tTEUSCA1g3
924ngSM5qaUBNt4skVw8a1CoSVzoUI4KWLMmbMvWuCZ+JIsSouFhWNwrMEsftfvA2BdfTE4H/6bT
mn7Wtl7pC/dpDJ0urWktcg+SyFj1TZOL0O2i+QBhuLH8U1pHjb4cSoll+TcHAxlPN784I/VBnr5k
dE18f83KsQhrNYzLhowmC9faixtnwHYg0NQDzWwJq4d1ut+hlg4lySn0FqtxZ/QmaIgGrWxlYAvw
occu40vRa7vVMjyrF90t3pjn9Jck2xYSGNPxrooB6tmGksSMBheD9KM6GHIWtH++7+NyIMtD+AHC
BqajrA94U9UTyx19eTFubaRfqq5DU4pKtrFE17ueqplivFXQZgtdUR9YvM5fSmNycT10w9fe6FrD
ExnudaXLVuwAxGwaX1Zex671RjB6q6l7c30S4/1hcn20dUVimpYstacryVexMKE7JNb0x5lk1kU6
Ppc+7XjCykjbwyimVpQXDdTHnx9iCDS7kXjutkifkKNbiQYZlxkUdnQNZktXAtHQLSBYmVhjsyX6
09gTsadAwmeydhlCcV88ddfRGLuXJmq0GnuK3W6FpXhW2bdZ7rMkzacRb6hgo+4Z1Alr3lHx6Z56
arX7riAYiaCMJ2kBtHcz3wmneYXXEieU1NfkXczVz11a6auW/HRXaQMj/hvTiTDolcyU5MsheEli
AtgvRd1+nicpRp9G390aRzh7VgC+PIgyv0YGpKZ0WkKaxCIcjdZ/wkGinlVwMDPynztnv9LHnrpm
+vrTLsrQ6qI4sqXEBLVEwT/BO5WJII9hb4IChrEiEOSwsHyLycwEAdRVCWbhTBSuPrPLbFQSyuP1
i2CXuhgyIZ0FY74EZ6hiIMPe6dLKm1cTGt2ocHOog6C009rr6Mzo5QcYPVFY4AXzA16Qviounh6z
Xvu7NKGA/odTnfWndUNwyKvrvw54XqJlrVhrzIJKPH8ms8xVNVmD/ncf8LFEFbZtjzjEZn62Q6Ic
rdU23yxZo/aPDtXbFOChG2J1nW3RBM6snStoDvhOIG6RrzQvw9Wt3Ru5aMWCjZKUR0x2y0FeIvzY
YdGsY6Sjus9g3QB25UKA0TNhuX0o5WqBmO6TuCnNUgWrsg0VlI3w42tJVr2M3iDX31aO0kiDYrbd
MO0Xrx7YLfe72uXVQH2JEIpr2O850aM7b5/yvoxiDW/t1izduxYS2gHHk3pg0sIi1YS+MeVy2J+w
r1nVFkEiNVrHiyBCcieds+HtlaoVX9xaHtjcexJvOdalQeEIIfdqxk5RzBPsayTFPA1fq+xQIdA0
C/u5y0KiCwo2xZWftZaxWIkrfJ7Fh7SRKc5Y7FL6EKx4GQazi50/lZqakmZAOgUIjd3jtCLsM8KB
xsYvln1bewYrS37hqWFpQt/LUfsq+Lep8UJ+GN8Q77TSGsElVggrDUdvzxOXFIk5jJVGMLrreOxp
18mnfpimpjMWTMYhveXQjFMlg7ZbWELHoqJyzCf6wThjT/AjgrbeKsUakR87JApuPNwnKhhIg9D+
Boo/e+QzBCn5rSUhAM6ziu+ZKlReeJRYL6tkl9nlKdRAx1L1ZnsGph1mCLFvV+AYg09dOryyrc4k
NV7Vnki8ycJ47klygvvCS26F3JWKE58+wxX3mTcFrM8Ghso2TmugP36oTOmobSu4QBrcqRAKUTOU
9vrrsrJjBGPgnU+XQ4i/waEO4Rjk0w5cyefWUkDVrnDINgDU9oG8vSIQpyy2JYBEbGDkPfaiATPQ
nmnGSmk2cUr2TX7dWAD23bH/e0Ys6T+R7Jqgr/eoXKvyZo/UtOs49dnAWSIU9Lpgja4iqe1S3TSJ
2+/+17AVPV3kAFpFCe28mud8+ViOObnhePPrxwYrADUW4uGD8Gu5s1aYjqXZ/OQoa9nP8myehqcL
RZ/KPHieAhpmh+bCAhFAm8WdPC+M+ahNOlyDLZLtV2LnvCKMTqGLFEgsvz6OgH4ehEZPeL4O4/cJ
SqyQza/6xW+r+ZrpfTUW143ewz+MXGOFNMFIooOeIErh8fvoTAzEVhfQ9MNBxXJhuopzE0/UPY50
VcOZwJgwtmEYIvON5bCHsbzt4ZyQm1tiMMuZoUGUiRFcdoaoXehHcXmqP+DZubcepnli9zOpezHr
T+Vo32hu9JlPPzxTgRR0wFjCD4fjcId5y9Z9Wu5cWGAZzgT7cpDZ4r0MDN1kY2zEAf+G/r9t177O
grIshf2M1Ekqm0/QZndKumR+fOrVkALUl7vCBKKJH2F6TlLARazI4INXB2wdGJoVELla+hmm4JRS
qmDpEtlBLAwTpwVZQJZvD1GxVsTqsfgPrb7AXcJB1DEiDSI4lIKJq4VCojh2oUn+Jvp+v2pceyCf
9xV8xcJYV+PeCVXe55z6NmO7qNkqD24HIxqvmrzO+6syHVq1DW0Jj/B9N3Q79GXclyJYLbHYzvIf
V1DRmUaU2d/p+KdT6ve59CEUMz8IPoh2bEw/OFcVlViTxUI4A7IJgli3U9Cgc5hBHAf5RMou38XJ
clLsOZHpaJtEyno77HWq+79VQZrwjv5god3lwo/VZj04u4ezwUxFIhIYwoJ/aW5Bw/mxyhsCIHn9
DiXAj88GkW33o2A0RcVPWNIBTZcL+7C1y5jNvzuHcjdnhcI52x3PYGXAF53u4nsNKbM6by8U2Iui
+xtjRl3lr+sBTHshKKW356fxWYg4DPC5UYISm2cC5tUwG8hxGReK91ZCwmB+6ojIBBYiFbLXY1lM
OPc5iMDx3dPuZUvxIOIizjggoeUBvocsCIYuriD92WsWJesGYjzzh0nSt4GBLzuAS3DZzIJto//W
S6/wG8kbLr+Z2dQ8eEEwnUsj/ulqdnd0+RM7eS09lft+8kLNtSzpMW6r4Rb1FhUfJIV7onTWrDOp
Scsy33l11j4NEgzVudADFsl6ptlS2JFAl2bDjkuoPIXTvMoDYps6TOr+9I/ey2pWx3LQjcaOkXkS
2moBLt4YfendHnnhrMJAnuXY/8lxq9XuijrWxEjGW6SXZ8aZGV0DDq3X/Tc6s/3tIj4zurur7OhC
cWmvZG1ORU/RtVbCBj3o6LpVukXB6AG1lRSw23cAd2l9Hx7oNdUtfzzZarqfSEeRbQmW3Bs0wsr/
rhtdl5q0mvcEMOxqJslaahmUtGSpsddYVkWa08xhjMKoh/M2V+S5EGc6G5eWlnYKcdNjiLun3WNS
1V4NPjNYbOObdYJOXqSNweJUwijzWt7BDjUijKqwg98iU5zewXIINgz/sDj83lY9V4MIoufiFhWL
Vl4dFYKK5l9XxH+mJNemuJxVbRP4HjD7sCnlBDc1xAo11YZX16OeEhC36ncAHNGBu/y/2rV28iA2
7n9QUT9guIcoO+IdsLVU/6PTdkmlvfDEAEP0w+3djFk7+tFal01gakI2ib2psqj+us0NBjXDRuX6
5P5BV6DR46aAMSwMVm6VWh5tafSid+2L9E4jNbDYdO8pMoVp8Ni4G35KDDO87+0Ai8CubuBa67hP
a/Rt50RSMLCzmmnnKBteEH3S+JQh7pKi5pzFcYx/qF61Nyudc7yGRKCAV1CMxNEQ1gAKdSks98Ou
V8ETh0vjws/5XoNNbhlfa22/5fAV0nRNghZeMfMA67SUdG0pU4e9q+7jGzneQtAsXCJt5F8FgQzV
MxJ7cpMxYZCDbiTwhWJ+5RFM2gaUU7sFxdnl5GVcUDL8OpoVyA7HQbQn+lpvb1+zEp65rzG7oXwF
HiFJ+XBp/2lOH33Q5vfPx6+sDuFHMcR0uArtkEWLjbT2kns08mDMeeLiZ5xvtuFXGj1WI0NCtBYU
ptn4hZStC5wfnH2wPT6nAnUXNKkcPh1b9Zg5G8EisRyKrl1tZXHsrUA9DXoeHLjlMin/QP84Cqgy
Bl/+8Ae2ET4A09Ae8x1+ttB6xpvcxuFey975mUvPrwz051HcIHU8q9caH78NNUqbx3nWuylIsj1w
GWWFSDXaVpW894Tcf0C8RnkP0LlLweX8zL5DOpk0qn8/J8qESW1YUymZ2oHtMKowdYabMi3cCfZc
P93hiXPXYfD1TJJQYFV7wmRu4d57A9DZWPJitsQi7L3/mV6MlDkgV1FP9ZqrJPbHQ+06LVfP0UAa
3wl316zpr7CSrwmwjcywE6y8/NfLgNXyhcle/+bvfiZNLddAIgyGm3uciaWQzho+VgRbIKr2VkDh
nv3pS3dnC73eQh/t3mHTieuYyeMDH9IAA56l8RhaORVQ5nekGIMCCOO9vBKQ24nvA40/peJY9l+Z
CeLTc325neoPBTiEIFYhkXRFOgSBFBRWlN21tG037w8WLj1v/oxkTGnKTT2RbwPqYxTlq6zZw8Rz
Cq8m3Mq3G+mlhChXjvyMfiAtcnvD1g2EJ6fWa4w0/K4TJ3NycGMqcphdMOwuttDBWOQtiMuRtucl
LOzGukBrc8YEO5lh/HyuWtJclpw3TOevu3NBgPEp6gdizt5pdohlYllxWvonCcYLayyFHCS8y4HG
iRctgJhEMvSs7OobVKnGRbPgRLv+j7VjETncOhzvrzWWGz0oYSIUwwNmlOboPi4MYzTDa9ajUPjV
I5f+nXTupMtL+LOVz/iBo9kmlkv3w9IAwW6jtUHkWnwak7nq+XST176liDqrVRGrjUpUqzUe3QkJ
MKddf8Hjmp1pqo0zbJjvKeM/bAPGGgOCjrxGvJpaNBcajZ2DHxUQIf1V0kCfXrKjUVUORBOVMutM
dJKGJxs6WnfAcgYeAlJGRmKZHzsQV7FEtbwvYIcg3q4rt4LLfwlcOhhAb3d2HJkKebiCaIlvvPXN
8LHUWL7d8HuQ3/ANgYb0OBm3gKOjevwxmkLKjCMSRFTjY51h2LuesIWuocmAtZ/KNWJd8M/ZVn6Q
p+Hph+ZJB643Lo5D++LKJovhDPS8j4XGB2LxtYBRVPA+B93jIpjNtH0kEXw9UnvhBdc2mwAv1xhR
w4VhijU9axh48UuQZsnCjlnuAyTlkg9QnXAkXsx5I3Wvx7Row7aVlDu2dlLKgBElYRQnJpVwtbDQ
MBgcHi16BMU2sJvvLC2lcU2rRVEOSBmDR5LGVWCsDI53aD5RB/ow39oEursL8tyfi+Yr9WNLf/cx
69uJur4//DZNxvixoavJkrc70OGIZW6mLTw4hT5mdnWxSSW/WM+O393QQ9fCBK+hccn9yfZXK7NX
wqiMRlI1R8ncFNm135X2Tp72deC6FkQnok20s+926x7E9xUF7Cfzi3sjVxXhfFO5pILKAl25ydPg
UfllMGZc37BDGrTnb5JTZ6SgkEzXXNAyuJhqpCQRIF3x/Oxo6lsaLjxCEQqIX5D8OG18o6M8cztg
1W8eZrwyL5aLNOuhrdBG4jxCIBodt+YUSsFgzD7Ba+M7Jh/KsksrKiNbbW6qS/G0KcHafs91UC2+
F9VZ5SVmeAJQBOwtEO8yXqrchZHsiirVPSQz/bn7Ln47/4gdcmSCnydzq26ca1nd2gti1N4cCMz0
1V9ZI2/tvTyWJ/c+GaMH76C0Tx0XySktZiUWHTIie1bWGFBAuJFiPYdJA2U8conc8/eSIKY748jC
B+eJW9qGW4ulyB7FaHTADLoe2HsoKBifxRtFOMfVCb5ZKZCANqd06yd16CoDLR10V8DL2eaIPHVM
ilz2qdUIbBSDlk29E73eZVwVTjWAO2Rltv479i4brerTaCsMH0V5Ajm5X8kDpdlnkwfnog4MY5cm
XUL17XtwTNHoyX+WwgPK5j0qTHnOQdO+fjiimyITZoC72/5ZG+yX/k7CMuazApMs1fIYiXW3H9Wh
xARl9Mqb7f81PyvoKtqOiH0fwvlKbt47ibk/PZ0VQaTTpiExjBereYx3+KBFnodBygtS6Whb9ejM
q5SaGi8bfhyfDQHC5vDv1hHd1hWyc7KfZYB5kD79AiUKuj5Oe19MPW3MHlohHgbsDP7NEME27wk5
NU1k1L7dsb6nWn25CyZKHXOcEA0o3fUhWeIkAXg8lQ6RW/84walfRasebj9wCFv0d8npm7oceJpH
SvmBeKtpSRKuqgIxQYL7VyUnw8mYmK1vO4gJKqasRKUdEwsiu3Ik9lejcZs5Jbwsz09ruM2gpYXt
UuGUccJBiq+d+enf6rZ0OLCQlnnOtaOtKSYKB6uWV4V0hIG430lLnGLOw91b5H/BYMHQP4DM8nIK
iejWTnEsD17wVdR8sD6u3Ed9iEE4CM50bK2GbDeVjKARNrXfamhYVaIR3LQkGV86RiRrqjyJY/U5
6DZ9Wgq6meSodjirWWHs02AxLLgOsMqmUEqC8/SGkZqvitgmlwre76pQeg/sj14V2KMCe/m7vC2y
oWuWao2bi7NNX4fvWiJ0OQ2kJeP30GvnfID/l+EE2uDODhJJzFTAg/6eBdKs3L2zGoUFFtu+egn0
KziSLKpQgovN7RxpY4z0qgI3CpEcjRhl7F30DbyC9Yb85LBGGGv/OKMpwyO145aDscrZoICtTysm
7YsIhk/HZlG/z/CTtvdPm3nRg+Uur2SlosVLFEwMsUHAr+/nTiBcQttZCOGmbB2O4aAecLrgwQwq
uZlwULS+7XisbyGpgNCpQjgXzofKLX9HxizLdNEkFKEO5f2jETYMgLtX2LGnAPNE2S9uExpQJmXx
HP+kwvlddmFpqkkhIFtixkzFcUiHQQiELwssdcq3KHIQ560kYYgHk8MwiLt/VZoewvZtr1qviWYK
LycMCWE5B+KdOzgWjqlsjbcRr9Z7mdUhVu+pu9w7roLHkfiuVCSt3sCfSln0KJdzPYI4c6rQtUQJ
Oe57c2XdUzsn2rbQCdvm7qWkTMo/sx2dpPxmAKyUzHQ7liViECj6dy4gZ/NzcgKyktgFHoV+2FID
S7lkCmrfXkYAilv7Wcd5F+axl4HrtahQs88IzMfYQqlkMUUkV4DNypZIWaklfrxlPlLv8jZ7tnf7
vAKabmNDGKl7DNmmXpDo7FG4e5jYHG2s8tmEAxMfK7NUc7eO+MTUwxXQZvdtUOnFlZTrz/IRDmg7
IB3MaYlbXxVrapW0Z+o/yD+SJKKruCUeuCydT6gmTqPV/x5utk8twgCl8Lr87U0F47ylwznCWlEP
pjLeJgqwNH52JuFKmyWjmr37S0kl368MklZiBU0iEeAVbPU7jKc43ycOoJlBFbG8j6yNZvSnMBMU
oTmgLkjLdCh4vtaattiup8ISXkkT++tCio8I+We7g7IsqcwhksQ9SR/tM7vQxSYRBdoUcaYQdmgb
btLGNMvWFKnQA/noC3ccp+U9gzbj8Rsf54KfX5bQjKrAxMbBqN9T0J+drQAg2pgRxsPIiIBapCDG
EhvV/H7Q7YvifzhcXrC+hBhMfRCwVLYq4QYAT1Qfs8RcVqQCPTg83eGJ51lxDirSqD3jHvGOhXt6
umrfXN5IV1W40nyQAwglXeWbC/Xh1E+dYiaOYClRzhZ+aHrrPQL3RW4gkgyWAMjxgS8HeGsU6eaW
pPKPPxKq+HMc5nPJo0vGgF4ZBsQNp8tX6lLDui9R4pZfvey+MrxrikiD+W1w+xxtBD1EOi8HfVXp
D3/CmP9uCWiazMqvvOSdh7XAcCvqX6PFiPBvMg05TNDsgy9uMbXIzNGhcDwbx+VrojPW3wb3BkvO
X8D8phJPL+t4VzuD68yxNlksrAfdyXDbGhoNZRdgCOkQd+auh2nIK6WtSEA0pTbHqFpDBasHNsDJ
CMYShdjnF4DUJzy+oNv64BPxkCdgH00lkDTrFEVAqDX/gVS2swDh3mj2+sGFlE+nV2MG7fh28g3f
kUyaaxVs1ljk/7jKcTgGX4Elu+C6ifE3WxJiG12p2l9EAny2dJBNrGNIiIocYsKFz2OvFcIEjcGO
cE09eKP1TxFEQJS7ruBsmLdP4921CBCPZBiJMmWnWcfpQh/fiXWuEl7B9xe1l2HwxDh/acAXGCAU
2P1IRYfhxOD53YPA0YM/H6zou56xY0NgUEIncZ6Yrq0g/WPIE1JehxmJ4HGkjQbiRnoeM7IlcREp
yb2UAd4JEhkolnDPXrKbTOTJwX4gsliCPFE2cwxy2r04YWl0VYw354n5AIBwIOlYt0yf7dzBPOoB
++SlH/2BvIVKcfBwuKsmCBIJ/mrTq87IjfHTW5kTIz/8Fw6pnpA8EdlvTuGlDHeFbRfsO0EInUiT
H1hpvk0ZL9NfcNJjGn6IGzcvLw8/d49xUNFI/Z3pQRXzZTX1wTFtcDXxAV6SHu9iwlwMIcJ687A0
phyBZPhMnMcSwiuDorY0jqudYGHfQr/uat3HVctA3W2JRkfT52+NwCemdOM87Pv0QXVvWOSEGTN8
GinM9cwoJXZ2yHJ8tfI+GwT6F0BJ1lMhqJZRcYbZxot15jOrwGrw+WBwei1lTI9y5H5m4Faz7Pj7
bPzdcJ9F2YyY4CYZGMiIyQIjoa0NJi/r/xIPlctwZe8utBt9dKxpF/hiw6e3E4SSU4YBgXjpmjU6
gvRnHrCUnm0tqNozz79c8Z4K9zC0vEy3m9DHABxMZgCu1WLjYK8fw5ZytuC2F9Hn50ZvEl9tg37L
oRQvnPr9l7cjY72Ep8Cq0K4l0L1hNpcUqud0Ig0vbGNoVdjAqN3PuiB3Dg07P1pL6y7USEhZEq/k
GxzAH4Z7hBvEr03BSFyYNUty2GwyyOggbom/fNI9X77hFCVNJQjhWzXLzWfW0bwqqBOVXdXN/4Lm
I2IL6I82iWdsK6/BnWXkWXUpRMpJKCussshpGDxwap4mTiRO1RD29pXKgXnlDG1zabvyUUDrS3Dk
GROm9PIdZl5D7gbVOKwCE9LH8+MupYuTAKLmWeYcQxYkeCjp1SdHP0W+QLR28bsXHeU3p7TNv3Uc
nxR5rfaKo/ZOLF5xRl9uuuFqTlYr54bRWiV3vgCxjHiQ5gBYx4LGdlKWSGF4suZSE+i59nsRy3XC
QU3SnSTEPOv1mfZbH60UK6ryMtRqHaeHOBEwK+fUwXOrq+p0Tvavs2WkHeAUJAFJTH+AnWEAMlku
zKzwGkqZaeZ1lla3OLbeHT0puFrq8wnSNJUmJohn2iOD0z/nIM8PCElcH9HYlhEC6Su68q+vXvXW
w9v02XbiHJQU6+eaefyM8rO1B6R9BC7bkgcwkbw9/uaSR0RmcpoV/eKIZ3V4NhiSCuv88IUV9+lp
3ThvO9pNaM5nABRMk5K4wF3CXQJC2tXw294zgLzTF3WyE+51nxV33xtmhj0iF913MZli8JZp6F8s
NH82YXxHpOYq7zWWfvPFUOUdnr3yHqSUdJ5jPcxziZUTOmarJhUgHrgbcxvRtFI7cojnYOWdmtXo
bGfOQZjmpG/B6X4xREArDtQKwEiiCN03tAhvx7//Y+ndPntltA6h4sFpJYKnBywqfzfdOAlM8jS+
zaU1jY3R01y24Wtl9CNRdMJiXCZn7RiQxgM1PlDoKIOczLsNmff6YLsQr/Gn7DV8VroK7ZTPqvXK
zEuQPFcBa8vb5gXS4DmemlrV8sYCOVB7EEgbcuLLB3xzl64eiSuhmIu4eAXlWLnBze9918Zkiz0k
qnCFShnV+vKq5WIzV3cK86e3y3tb8nZKgPkGErqqRvJ7hOCFlEm/n+ivEcjjgjQsHHNy1xtzIkRR
+X34fU0yZMw8s8ZMXRiA8YqkAxgCgCyTwnILHVX1sYgiifUWlZE3O68yuHdTfOOBv0U8S8judFhq
Rn5hfftU3lp2Aiu7R54msknBHhS3yZ9LA3mGOLCOM5KjCT2R2gfXQqQDndffkFnDuRmrOEjswl4l
N7oOmjXcOhKocfKYlP0D3srAyFO8Dauorl5Upc3uwEBSl3Y1O60CRuGDuoIiBEd4QAWIl36DW37e
SLoCo0L5fxRU/SAcvb3zex0JSktdDx7dPKr+NDBnFwt12L9+CaFvzZf9ZX0xucOOoROcN+IPvwXu
8TyqUA8gfhi6QgYXAeQ6vCFXkxFDp+FxdJ5L8vcA4dgUQGMWDJa/T5ZQxXxUPnh6ajjmv47ZNRNx
RTMSoV+uDkyPdsZXWr2i7owOOgRPjGlu7wrXi+u40v4hklqAC4Rd2IUBpcB9Xmhuz/KPKRlyGecq
0/k+fEsxo4yYeArg7aG6i65j3Kee3HLW4jdHexHjUuczQSImgVe6wkBpsFRXZO5fFWNnqLipxHI9
pC0uFpWPLwVZl4s4dFGXTfNTJpR1D7K9he0lZBhQGrPRf75+k19aH3Zs99kI3dow/EDpFUC6Gv17
QE+IlxAcFVKKGQg4xxwtAXZcBBz2daLwCPnVBA7xEYqIk5sgwxtV7A7Da5OZ8YoRLk/KPES7Np+w
7U0gHHCCqzIwghLKzhOpUoasNVzxbopMyK/mUrUzA2bmrEhlU39YPa1TIxq1uq9mXHYNjECgQ0OG
DDMqtfNEjx92VWseUBJ0/dd6a7I98e1L5HNjKjhYdP7E7Cqq3CMTgtf9S4sbptEwdOo0OjSFgrTw
4r5a9HyyWczxOWC+BaaWwTk0gC6HusaOJCtwUqiwBFxascyeCRtcx4CakEYdQSvFKIvDQ7HdKfcn
A/LwhBe1XlgGT6Er9WDdX7V/utw40Tc+qzVYQ24ilR8218NeQS5zsBe8oqZ0dwks/L0QOO/bgcJc
AGMBYtW39XVZiiUQwKTuuH8RbLjBwsxEW4VuXQnWv81ScQteRyzhYKCQn61g40Xz+lsy9+YG8P7K
MdBKdFnP+YkIWSLw2V1NdeL0tc/e/UObA3V7tI4YRksPIdh5vnpFNASHRDebiFhTX1b5TwiYLWCq
3bgqbLIEJPUrLR22lLMkdqmAIt8Ky5ZKQjDVCkdg3kXNRgYzthluxoGafEvpSwvqsFaRTu3nBOPi
MQSwbGGk1sK/YOyx4LfR1xJUi4DFBHUWUbXhyP6Wz5WzjLxrYPBMqZmplBH05NPsCIJ5CgkC26ze
kFfGBB8WzVfeGmx/BMIx40qJUN0E9BoCt1q3MjYDMYQUVX9sKgqcpcd9zXdXPxcvxescQOAaAkEf
VY6d2Sia1dGWKzZ9PBmAppWXOOHbPuRvNv/rraWkNFed8X12IDDHCDZwxBrbZDVLP6SrzJEhkdzV
TZ3ulb00afTr7ooqnPFD1hxhaZ8C7SEP/O6VlbDoWrPrBwqx3O/5XkXFQ19R5OcK69G+ra4dzhKo
6UeCpvP0+IjFQKBfEDWt1qtoDB5dgWD9U2VcnE8Ptx6nwXEYp5QDEjL2+a4u1R4g7BolBA3XBxdw
uuReyb8VXINF6YJ7D9ZfkMc810xwplIzwBXhxfAfoWuO97SeVMiRzEJpzikgzXeEtvXVikMbLwik
uWDxgYliENdgfcAbLqgWm4WaIsWKabnWV5KHI9ZsCF8Kff+wjznfnErUYdym3Ei7lxhJJpQ2YOUu
R2x5OqQWduzDTUK/I+CnXNIK4RswCcE9VnuRt5AcoXmuipvtFNeGfpWf+E4L6ITONl9Vk+qamycW
9jJ7CIsP2vgPYyg3BaJLZ6/XJz1oYt9Im9GzBAdDw3IfjqlJebP/oINSSdsy2SfVz6XBMmpI25/r
fsX+Vi9ECFI4VjssRTlQ2f1LBhjUcSAzEDE1tZKqsDFWkeKXZSNrCK43M+p5y/tiDGfZoyzKIqne
evOuhb/fWRvbD8eG0V+N9So0e2P98wvPBR6r9amfnhYP13WT3hfiyZnjAYV7Z/cXgJDhrpiuKZAZ
Zu6AdxQdUPhgt9JeI6QhvOzuzrmLAwS5dZD0VUi1S4jiCMyghp4KzHMaF8/8+pMk2KMnsyd43e2+
y/xrToc1kUhuv3Vr5wGqWUt52JnPE2UHPqQ861R+DEMmzbrbiGNO14kPL4mribV70LhxucO+F5wl
yJFCSV3A1AwSIq2KrPgPboP1ZU/Arh1pSNGeS+qmze84gzmlxTfFUUGTea2oIct7osUFuND8FkUm
6M+9QClpreA3Vxt5+OlK50X9BvEiHrWntmbI520cgHPfP9gR/hOccg6PiOfPgjs2BaV2mgSy+cS4
LoEpJvhEI41wan/KAchHH8yL4BKA4SIKYJP1O1I5GAAMkICT7Jb//2jFEyTEOaPmzNQ1kXST+hEo
FlCjTaqyIh8Gr2MV91Mgkht+k0BDGh6f4cVDsCgYUyVSBOCrxoPjOAQUCQSJ6ktUi1uXcROG1wHv
4iNCiiNK9bUmOATQS3NiZRmQ8NLxTFXZVVAQQPn/y9QU1tr/C3No4DaidIe4BOW9Zyk4sJRfC4yf
UDq7Bzl5jDEZ07Mle52DUkNXgtMCou8slVvfL57GOtTO+HonNbICqEyCBMiabu7dMl1RiXnxwB5u
MEAh+8zir5v9DLPq+DbEMdv19nPhICY5HC81dFMzCt2ypC3xzt0ygd/304e7MhVOTCAOpQCqQZQj
2z/sz/pE3bPsmkqhEO0DdjrZWUiJov8L2fJFhssFvFw8u0twSUjT0my0SbHQry9+z2DD+3837q1e
8E/OEt5I1n1Dicp4zxSF/9qfcnUfHHKYcwPVy1tkRLrf3FdzQgZqqPfi5TXib9eVXRbs3GAvBBzM
0nTZHTBERmFoSLrUfqdJA7q5w7ESJ73FrM4ucXoD+HQC2MJweE6PZoPYBR6nHlLIQoILaCYZLH8r
aUamiU55eZHAX1qAq+KLufutsX/Obgj4QqXD9EAbSVedmsBAgG9cR1gPh4MK1KiGR6j10rR5xb4c
eoqkTK5PgbQM5uxEe8RgSWJ4ViEnNy5aGtbJNBuwrJmOC/ziYEgr8f3zMqQQJqwPTLjYx+9XlGwf
cZknveO0ZOmCGVBPC2QqvJ+xdzbPuD7hWJa6/hFIbGwGkAyH732qLzh9lg1cY8++R2ZNoYducFA3
nbOOG0PIDR2TRRD/mpk9rN50uk26pozbAULVPQixyiquiKpOtc1OC1KGOaB1La1RWcK/zZUgLSxX
3Qvf5QeaY63uJIlAiwLrUUy5SWuxmeDsVipySWa2DAhYGl/q+iFnqC8clzVKlgd6QzPCdkOxDLcV
b+64mARjtIm7e1s9nPf3jbiXIfPVSgcD8kiQXkUX3P22SGxuJ6HhudT0oP5mT/O/xQOSohh979f/
QQq4jyy88/5qTOFPGWJOTQMuJlhZmMHvmhbMimY+VfbDXbm3oUWNxM74EpvQ/d4Dg8IwgM8wBzB0
CKKz3AqyTLWHUo78v6Z2HBjSAvqP58fbUHkLtJJjUtSNBu/zsHxA3F4dhju7RWUoYE3wP3C1galT
7XEJM6kBokFrTQJvyixJ1Vquqw3SOLWR013G5gfqg8JJnVkgnuxARoivrtaGWBCy2jBtHntaHO4S
rB7aqKQG8difEtikMUpBHvlrDm3QMtr6Ygt+iXQVXZTiT2xdxKfgdfxZ/M/Jvc7h/35+DG53hCKE
o/wkOEZLysz8mNaJGD3CnlWtS5S+nDHVDlOq0qO+jGQ3MF95Icj7Xw26jRY2NooywtJrAvB2l+E8
IURvCxCMzxol8VaMZg9tHl4h0uza2PJIXIw+QP/1eUAFdYNaWOp73NdUMo8MNOzfVJB5H1BoOoZl
WYB47HDx/FuBnv20Aq4+Zf0ePVT2VP3iFC/JMQ6/xUJEzltvpg0JKeiyATXJmU5XhlF9GPCQyj69
ug0aSIAVVFBR0BDvoQeEx7CZRG3H16Ika3WtFOPxeG48/B5hpVj/tlCXOIGnwZZZEnUQTSVaTkJ3
uS/dkuPnXhnCcAPGMDSQvOpRn7KXI35kyd3Poq/W4297hWquNr5EDinVlSEXOVh/30lLO90A+3S6
FQ7IxwNP2+92HaOUtvpVl32V5cMdczTM1SpnUrgoulMwm1ffl2hbS4Stk6MIfvDXnBN4SEk4AOIN
C559eCQizgSMv2ra95qTqVVaCVVtT4YLWrmCmc3b7EeDADtCypmonKRoedMs08fmzzVknQTPnzKe
3Xonn0vuaDz1nzQ+zXZvBi2FIi8zMzOdGmGIr9oNq2HwZDyRxx595TFMO0r63YGvwAy3CA5KTaNN
p7z3VCBCpuIId5perasHbAqP46C3bBt0AXwzSDNL5LYrrB4FBsfJzapTfdtbKRO08ZRTG0LzOu8J
BCN/FANqCjc6byTHW7BUynOoWQO4BYsBnGnfHsCYNrp11sosqL3bVMFGIOgD28NaeuDp1ZVSd2Lc
W9gniniTIkGZhd+FliLY1zV5ZyxpYvjKprNeMa2WuOMWBJWGDmP+uiz4BQhEsAjhWZ3ZAAyD/dG0
te3JcQ5+NOq9JmOYNhbT8HBu+tIQUi1oGo2LCWjFORA2LqM44R2HefOWBGKUrlLNakwm5gHetcZO
dU7CaBJW7wwJtipqab25I778fdZNEprsLgOyVvVH52eMT3Rp4g3VfnU2fKM2yK5CnHf5V3joooDq
yskSieM+wIDZcoYBCCB/HF5h9VlrZEF9I66FCfT/4r/x4brDCltPi3jQRLcY6BdUgIob+CNjIGFa
wWIw2oNLJ8hWFg3Vm5FmzSnmX01rnhs69essPOYR/q0lPrV2vGR7M0psfh4F8LWp3BoVu2oS/pK6
e/HNBIOgC+U+CuxZu+5rmO25pAEp+xxPUIlyA5GfVj7T0lkJZpcfV/pLilMkZ1LBcMmlplC+SsnT
H7vy/O3+rRqhzZ/vFuWNYuYm8WZAk4G53BgPi/N6g9N/wDGkOkydL1i15Q6AsWSQuxeyXC2ujqiJ
El4U10kbWenZqhK1qAKAWsv7mcufMtgSbD0dcArPdLl6f9TMe9a1aoIidV2N7mmQQHWeWk30ce7B
xD/JjtlVGJWqOAlgHFy8BzI8QZSErSMAYdiRo6pyHQQIihR5QlllElb5P2DR5cS3a4W2FhJZCH9J
s79F29vDbpQ5glAy4Rc+7fMO029CQgqHiNmjFLfYbgqSuwvaID4JIPZRGKEnR6kT09lpaJotK20D
qeSWKaUwVjZM7hVJ9pjmHwzj5iCtQKbVgTRIdf6J2NGos01tkwskO2G8XDTfI513AraIyXTZO9W+
1p9VUojfibzwdZ7AR4dhpEmFDMDvqJVH37kRhsuzTmHYTJT9HkJ/CeAj7TVggExz2/XDANVgbhZf
GI/bXRB9kqKzJPe2qBlFh3pyyU0hFkj7dC6Lb/YGnfWsYurmbauLuosuho83XIuWol2uWV/zrpot
nhtTRP2m5OJ5pWV0tommnIl4zYxspfXCVIHMVCdQa3IEAPaiECkNvZoPwJFdesLEQhudmZBpmWXk
jOoUODkvwyBLBHnFKaurwhu60ot91LTisV8trw5WxYJiOHhkRtPvJQwZhtc218cmOZDXvm83aa+s
9EgmSUeaRuz2H/RbHqYUmUErCyvG7rnOB1pVKcKtw8pERwDyemNK8Os3tVbSyRH5wT8MicYMIONt
gBXsgSZJb7mOZ0SRL58CAZ3hSshhjHRrd1pG9CzeRllxhJnxiaAY853NUVWG0ZXg8gA/DvzsWf/j
PbmhnHTpwheQyQ8HX7VffhqnQUNWcjzhivZNAAVUfn/q0WXeEDB5lDpZeChE06OeMkPr4EkNaZv+
IczXLBtlb6dSxy9x4Y5o5tfqLxxa1CmZ1BgZnhsp3lVtY6Y1QKwFyq6r1qkU5Qh2YgmRyC08gNAF
VxfGvVvqpeyVTDy54ibsjBW3ezWHQO/0C+u1Z8PtglJvpxvw2LB2ixA9I4BWNxqs7o8/bowY/+eT
JStsuok5cr+ddJ1ywx5RLFAHJZfOtJ0pZXg84qAAlZoeku441DutSpzKrpJjI7SvRibuzCG9OJI5
hSBXL14qbs6eun0QPium3bUd18RxdGbYM1369ICxzZdJu3LeZd5awpeAhdaVGnjSPz3Qfq8W1fCq
ErYWVDL7EUAg6MbJ1Ygbt2vmVLaymRffw3gvvgcb3fRPQ1gsnb/um8evQWBw/mHJ7LPZBlLbVfVf
lqGWTgxB33+c9upe6KvJs1q8FITxbtqQb4f1c8FYGMVI/5F6wCuLEv5KC/Ml273QacYPkgDkSQ0M
RuB311BNN9H5ME/0O6eOKU0PebEVZanqxZvE1jZVTqahLb8vQx4b2CsuSoOh9aoCZGnP71gZHaYY
Tq4JjEeOKY0jki2jccKvVtcXsNs/bbLkJJhGCDwXT4FXm1cg2JgMo/ZAbM1pF18coBWrjPgZapSG
ZRbtSEUn8c45EyLie553qM1J5x5KXgDAN/XHQYidfDdTzu78bPFxey5UeA6N14QAy5yvXFocQIUx
nhfyJtwh09FldKOI/58FO3SeI3MNWfZDCUutVgdU6hp34h0/k4e3HzR/taAJediGIynQ8rUIDaeL
t2uKl+pal5Uw7RnrCTSbn4dVlW9nEdYVVVLXd6BbumNRM7tSbtXWCgTXxdVvRjTumbCyBMLHK2mm
+EWj6NJx7i50YzCYQrMBD5Fbvs7KW7oVshyWSec4mzRKOW5vnK1lxuK8Sdr8T6+O3o3k8bTfXvJc
wen1/v+gSqGh8UnFOIWhvpO07Lz74fN/HV+/+pkHbc8pbVcqvK40Q1OUBN4AfP6ExY7h9SHuK94f
DQjGUoFQfULHLdlh0i378mDZ8olimU1esQjfahnJmXPccVL/+mokPXs5DUo1AqmUmd/9ZD9CZ3+z
9YlK1AL2zGQo19U2oAloSG+mfktc+aPiO+YxMJVqReMRn1ONqfw5+hBMIxlU1aJ3UAafxlirNrvy
+YI/WgwGQiEwn0g1+wnqHnKOQ2ffu5t3W3JEf+ec6TZWXKWmP/Lb5TuOoBO5M/j3ZI9ALAgH1UB6
OVdtzfxHV59swWJi89n4St0LEejsgGTmExM0K7lPnunQGyFSW2lwSY0IWhoxUglNQzhvXN7xhsKg
2r7bwXVP73VvehOarw/vqEksBQAQv0CEzwfOSdadWFsvxShfI5B5Y+PhlmcjiAaP8yeAVFgLmp4z
g723eZ1ivysVqaaCoQUzNRno+RJLmGGAvO0r869ynssxBDBZMSTiel/qIJfZ5a5yv1uizVvFJETi
TVCHAwfDE7zWRBZb2/aF0VioUkFMf+NuXZ9PUHtPLa/9TsFV95F6JN+Y++2z9QZ0SyXnfYuveFGd
woj6Y5WpTndAAYP37A/QUFMV18Om5T19uf30XGTdPDTDmOhI4iT+B6N5pkt8F6dMfXb4LeSf926r
0JjFOnGNGukaM5RKYcRMANPR2llyMsDXRP0qN5A/0n1ewX0v/rOYTbRmg0jJWM53SVUw47/AoJln
6jtvP3RXgcKDSL8619FMszkI8wGzk5C74U5V921z8sfjzQzxqO2rbrMtT2fDNY4ZA0iDO4hQ4x4R
IV+zS5BUZU8cE7QGS3htMjfScpP9rc1DMJ7GCGj/FTg95CpWki+G4d0YyVyGOAbtSnQWNjNaesoQ
cr0Mae07+ZTrHlw+Cfo5UY8OoCKEey8yoImX+HE12oTL+1QPv5quk55LxfQh1BQXDYqFFX0rCn8k
HUhz0+p4qEWIZSwJeJzniHMeiG6xkWMXTGTONw/bKcB7dsS0HNgusqjEFpPUORT3r4ZqK+1oLaDK
DXLwtdAjZOwKxxvjpKI3ILvNV/8T/y4NmThToRT11QqiuMxi2FcuLejWs0ku5bj8w8nAnzmCYt8X
GuNJX828BqLqW5V2u6AG4ZXT8OHuL07R0BZSCAOxH5GOHA0ChDrUKE4f18tZx6Nd4RByVBrD94HJ
p5L9sYM9Z5KXkQ1HD0tqVSalaDd3PBQ5n2w8OnNLCvYr7Vk4cAdO9JCCNCZOheqgsN88a4LUFQ5T
9oj+tS4Dl7bh3AbgWylU4HgKF/xxEPaQkdAtY8UTXFZUKSnl9hY03FsHR1C1lWkEEKwdO1Gbl3ML
vSoub7vs5XZfZZ9QmXsCnbv+xIc34Z1D6QqO906Vwv5yJJvRR8xcze3u+mfX2DOlvJUf70ZWwHNi
llgSAb8LHjZ8c2rdi2mB+6IvAIjbDu0P9VUpO99XviFQl1ll70Uo6e90Wixggp+g2Jsi4JhFoAQ2
amusTPCmTZ/B250c2rSFQEGRcS4QM0DzjKtjBWOHrWtEcHUlRrgkABTMsiguz+wbCGgZhVCLzane
9oDxhv3N5Tr1UZ3meQNeRKr86SFB+a1A8lgiz6fZ40SCN3wrRuCT9JIl5s2b976aWvaE0a5rhHN0
jfJDulKLEcUDrbWFQOU/TInspyxSlqbmAFHqfteCCWx1jrgGjdU1kT/TIJ0O0SNNSxGLyyFpckz3
AlN4jv2s3Ngi3KbWjXXdJEmuAOFfbqdltgJQzOR1Zvt64aHhNGRc4PyrUPVY0Wlz54Yf1DDQUyba
aZxmgsJnik9pQUrP2Fm4AMmevFSG9n0fPDOY/SehePLqTKjujf0iJjldVlFZE6I8XnDXjplfK9v4
tGA+hzslt65RwX2U9l4g1ONrzjeHorHCU52ezn7rsaYP9yM6HNgOg5DnE7QUFndibT2UP7AmKcGT
phWz1ToQiym0vjoC4/2xkIW/if5k6+Bk2CE2qvB4toel16SR2qniuHb5YCmJG138LKDSvEegxb1L
YRlI355O7jUj3chRyp1wPpsGdguOwGhvnf6SUQhD5hAXfvycWWUBvJpA7semk33LXqjVMcLl5H2i
/12XbSlt5Lt0KAhHwVbpSGS9NpelXWA8tv+JqcNfHPpF60bG2GOILUkAbaZRV4MlKSrEZ8CsJJjq
Wn7VXTnTqSIyuZwAlSbteJDwsjHz67zc6TldvMiKUA9LjZoHrYAVOs34yTFhZqnMzBJAZTfIsR4F
nV5cwckemaEe0Ww5eZHNv824CUcqqDCHh8PbGYVcD7exVd38npJ53didyPu+wyVlYCYzYT6WH0RW
0WMn+4F2yDnGFdSAvU2T478hGhSK//t77HXizoPoqgTQ3HLFtK3m3d8LNmFnMJMGBNBbwRkjzMe6
QLCCSzFf7k47ylT+/OfCPiKzvB1uzPm36JSrif8WR8/EyoGo1/V9VoX3usgFoJtw/9VQN3/L9jP4
AK57YhkBq5lzrzLyRdnIjq6TGoa2FyY+zNtmoRczWLohOmHOf3oAj2j07kMaXQn8cAp5gyx2JAwc
JJ3xwEkIcB9GewZYZmMc5a2NPh70+WtU3RGYRixul6QiOG6UhS8JmH9B5JSGzG3H9KhmPHkPST8B
t0O2ym9yyZUUHHLlEeGdjq84dYKtFLuZ8zhe9ChAd6CwdemiyZypJ/25/V9CrYBkgqB0W2uIBTdh
YXkYBjCHhphQYfh061E7Gd1qslCiwrt7RoKag8wREBJ0LaZdD5FHr1W+lSsOEQCfIKLRmtsy1Rwc
CEATNjdC+PXdBD6X+fQs6YlPjTBgnfXxIk6UNsPKO1jusxhgJ2/OLL5m10apgGLek+DuNl+6Z/bg
AOYbcMzcf5B3lvPb0I4swRV419PKWHq2+Ek9RxyzaegROI/MF8MHRPtkgHwbaiH13IwmiwBlVSJ0
1XHQQmVgEtP5f7f4AQwBsPZA+ldTCRWdAUCmO+ZCBOnXGmZDIhXj4Lz/mIv2cuGBmmOg0YqfUUZ/
iCDD4Q25mtSmfk3k1QNN95Ts2qAee5sKBkHraUTAujkk11pmq9OKeb98AhUsZB2d/rhTvCsbMKhJ
UNFsafGr8U0sbF8kVmGoy55PWyPwdH+vFUUCZ8avCi90lo61LMrFVCmBPnTD+LdakwtSm31sCDfA
pprSu4Njm5O/S/neE45uMHvo1b1GPOchw8sp5+crkOb1k2RiYNk592dgh6z4FPHZDlobkCu+w4UO
HL5MOfi3BpKgEIY/blJgQGvw9S+1ngxyaIG3vpe/2TsQD4OeHAyYnmBUDvntjQSrhqwY1Jd95dtt
4GLJSNnL/JQQ+p70fDHkzj22RplkHXXD06tZeZD1r7yN9t+nHn3JNOBr4j5SlZwquxtuM8hl6Sva
QDqyXgKdWwexdc6asV8WgZls2f/JrFkVcg2TsxPqmuSmTOqPE/ZfuarQ+Phqh8KOsM39RZHod2mu
SHqV+RTVUH6D8GPRj0+vuY6lWdGIuD64SGGSiN28HH6CYgSrMZv57JFmlsB4rYmUpwXP+xx7DstE
JD/WlEqiEA82gD3ogPiY5HEr4B9Vwk+VsGutRUGFnANupVGh+tNk4P2/E7rb4I6yJ/9PThiHWCz6
iLeEjrNX2mJe06KAZADNyuBKmpTGveUqdhqGI4bhKpqEmW6snwi7qHa+8rmsrjP6Mf8JLC1sTLz7
sfQzEs2pazbQ169xFxKoZsq0A08fXKSH5KIDOx8S2lYVykENqZWNZmKAWw//J8KYGR6DWmBsxjFS
vbOShNrjD8uK1ntdrJnM58gNR4nB+TAfPxe1OGlPZAutTfWPWZmsp/0lADtwKPocJ2KvqXJa8dK4
fcrtB1l+qsl4Q2p7LFS3JdfilA2jb5JY9n2+4lr7130ef3L3fgU0r6hdtEVZsGFp6HLDIyREWf1w
Ws3fdH4/sv0gig8BZH+hwFFxx5G2c9UHys4UZXkLXU4qoE/RwB2yOPiruxc5jG7GsPOgq0MlK4qN
Savnc9H0H+sdXW20xffIfb9qnZ1lKhslJNVUxX/kjS4cc11OfayM7XESUBlsnumqssDBVzqa82WQ
0sFEn+0YhWFLhjUXSq4q4MgD+5L2DWMNSEa0RVfoqAz6jLCmcEP3vGgFXedPwY6Pa5Uv0V3g1ZYa
3hwSEGJQOwMkCUaMeri8kQ2gsLb5YcxWZetnX4GQcd1yZi2HETqEA0Amw8P16GNwCG6OTbv83zPf
+uYp4Nw6cvRDwvzu3e5LM3fVcFMQ3JPld1YvxmNTiViOMhPIO1O3UF2x9wuuCihQ95hHYlxrXsOU
6q/8hTdYzoqhzbevRzEjF871WsNVO9d7fKTrQcQLc5jM3DJQ58u1iZk/zb7TYmPy/I4RfQZh77tU
GjijxMq7eejF/AZguPhDXBfYwSObxsAHaaby4qf37JNb3EKqeuyuRobrdjTeXsGRWsGaddv8jRHy
HKLEdORqrDVIvr7uUIkaoLd7WuhniLzFbYNQnHYwm+RNlJTowh5wOw2j/tjkdRv2nPS3LeN8YPyj
+QWOVry+o/nDc8KFi+uPOkvb+22HWci1A9P4+zvrRsDg1SqTx7QG1wm8Lj6hPCVdgPY1p1h+qvPe
noFRK1Fbg/eustHsx5srqRVQkjBPAl7mKbQU/lgSFVSO58prAzpc7oGjVaNrOcB0vsdidko4KuWd
0P1Qz739MRD37LuzvrMKw3LJF4A69P7Lz89/NuqLqox6FhN4rVOyxOSHHU6KRVSqSGUjTUaspfuE
OpHm21FI1Qt0GHcqtKpCnaMvtWEeZSRFsmMhohxx+8iLF4UYhqpIbgDALUymkR3Ef096NGioT9QQ
PZb5XAP8DaNIhb83aRzPiXEKjwkjeznUInl74QUI94we+kg6AxXYt1J+L+A2wbwxOBpIDMRexhjQ
IVP6JYjH1WULwPgDpD4Oakce5aUNDbvHEu0UJ7AMthFv5EHcevhGUVZ2YKw7Ob29lNjU3eaKq818
eJyBalQailsWGids30EhPh35Uy3A4WyS2QtG1rH5JOO3IHF4jSUgvWNK+tl/9U56QzKlvhUOk7Il
WiA0nLdvLXqd47kbFLY1aA3L+4H8GOF7uRRh1Tl6nko2kM+/u6yfbbHdmaSJkYo+cNJ6i6rU+y7J
KuvxF9uoHc2BObIwTxt7QIPj/F5JW4VZFEoqCoMOH2qIrADPGofbdEPqO1Z3winTQvwFR7Lxwd7X
mXFnUEawZUiacH/Z+0G95huEQJbJpjeTM1N3ubRbQg7nmeieBQYYHVpl1fXhEyQgnhAu9h7v6NL/
QZHF4GG6oU6tjrDuMxYj89YVY7ypp7XuCtrdRoGzuFMb4QXhE5sMD76xxasSs9OJGde+38f1YwMI
2LsRL/9F32Gu24azAoOuy36b0u0qB16AcRw9K4PgWkDhEEU84SJgpfhwhN7ABN881lQB4V8/H57f
EuCD1wz1S+HAqeFGwxXJtxIpnLLdsVEoOl2VEGbE63uSQxG96Dz3v0Hs12M9T5mcAONEtKFZ7/vc
THapI2rcSLsAZQ2ExdzDEL0c8/QUjgPZuQ9/Fr4g4iJrY9JUBvTxOAPtooc1ZT0cjUsJmNXNc2EW
QJsaqgGsLS6F8C54QmJGRUBK2ChVci1vuTpdOO1Fd2BOAQzAMr7qL/hZIs7oyb0VWAhtNpE/lKR1
Vx4TuzADvcIyLLh3nFr4QdWTyoZJwX+ht+l05t0njbmFW7BvhshRnAfxN7ZtEZcnB+TL+No3rPox
ecR5Ri2i50bjjjwMG7+9cRBrVzy3R3AyQbL1SXMySzUpkvGEYwocFBU6mFUx2qK6DHrEfTrxzbqh
2BE1+NmGO90d6pRw9BTXvWBU1zv6vw4mlyZTKvTz2ZPOrQre2bsa1TCbP5ymMIsoloHGsRsXputH
JR8qfawCP/Ic33rh3/J45GiidGMrNcoRyfSFDsc4NK3djHTFrYf7jRVFSaKe2r8y7k4pllhrYfqu
cNh9ilXo+A0pOTmnDIbmedyOCgHyVYzLgdrNvAU+ok3eXtucDtcD6LlwJPuURfKVzzHn9+cHDthZ
1uRprRiZzjraeFK0bllyN4HD40TLbylhTQHIn4Te8vcCka6TnAFWAePR8kupLMwcGMgyM+/DJsjD
F+v3MNJnUezm301NO1sL3AhOF7trnKwyhIYf6MFypUDIM3CH1haBOF5b02kZ+uNW+YkUqhgT3V+t
ZkKfLGgL2szNKoLYvAho7goy+dxjwogOJP5+mxwILhBG3xNg9/I3N4lsRHn9KWyZiRt6n2WdarFd
iBqJMgk5tRLZ0CWdwOcrjOQpbGJQ3qfbyQUWnwrI8G0NW1f7z1R4CqrjrU2vp4wbZHW2HoN3dRyS
8WNFqrHcA7mqU+xdJUmwCCPxBL1M6K86CeKNzvctFIOjAQlQ+pPVJfsZdXb75f9g5UqNUmbLpH+i
8c7581J4nco2RrGiIsiY7mMQeHk3but+iHyj6AKr8Ys4wvBW/Tj3SaPaWcRK+Q+c8CAvgSjvQCtv
vGBhctotpzQFP1vdCkMKX1sAHxQPiElrECRdivG2ePxANOB7smVwahpgKNbDgrno43u+j9dJCAau
eEc/QYNfAtHLYmGs37hRo5VhYv2ldcuuXC5wwmXQCoN84Am5xxPl1n1ixdfgPS2RsZ0hFUYsf2x9
tIYxqQ7a8zUIAjlrlrQ60Snrd3Ctw1Mm1EVUP/l8RJB+b3JqpG3tj48DANcuniM7tBdmhiWc5Wzx
uJXGLFskZf2gzLYMQI2O5toRaN54ER64LHTmM0cQYNqWdlHqMtV3S9SDPx3UYA1V9tBKpaSNMmE5
tEUAlPAqfYgMhgF40hG3bgLLhW+9Se6xUKY8D+K8aCRd61z5AmoyMvO0lEe1/hGLFO0S+odJ+YVn
LRocLGVScMwqrh94CDYjgNhwU7YGeFbS8VCQRFS2WGtz+a4V8NJWPjIVQ1mGnMkKdxOxbh4rgTgO
wayYCawoo7oruH0vgEu3Jd+g8bz6nV14hDudoxWJI8owmIv4/SioWYUL9vHVt1yz7eNKpIGSg2Wz
8uyziggbRb6JEZrjRkZXurwmQ92l6fcxeUS/KpsWWlS99XC4ZnKe8HqxNRMWLlLHQS5OgtuI4Lym
/OzmX71bZz3twcTTmrXAIRRbcruwphnypxP7MdO8+qjZffHCnN4sWZctoPBlPv2usmcRDj2tS7Rv
WzpqLGpY5kpeeXfdnbHPeuRzlJukMQ/6WL4FC+//a9ZyfNYrhzy7qP3m67gomNN9ICo7rCE1PGFn
YuedmqcRuwS4IQ26HtdFxgZjw+hzW6W9o3xNvcvldiQL43oRJ5mlbZi/trgBrlZHFvsBB19V8qut
gjLYia+ZR7qI4w47IAL8XGExBz3LaPhJ28nKD1aFt7EsQ5WHLHpEQtH7JStZO01MDJsZ5SLmEAKS
GTZnKuT1XWtOssarhtHXAZBmBkxfRNtE5qY0KPUx/ETPx2ofarrOR7DQxtZcMl6fNPNKrzwlQxbC
HVf0EwpUHXskaQUGkmDC5FZ/F3fQ0Cvr4l1PfCcM4FmVAfFlcZS91btIopwMdtUY3KQmCBMfoUyH
CoLHvSGn50ouoy08lAf6IQiRhz8tZkyI0asOUPkwkYUoEE330iwxeGpGUxN2+zUD82ErDw869nLZ
r5I5OK+JpAm0VJkoR8aF4ghWp6BnAdpkrY/glDSu/W+v3ejeLbb0odXWUeylcR5OI1uEeRrMcDOY
Oky6bJXpTe8uqqNRRb2mMi6RdBIEU6lZ5CkOFBkgNPVjYoGNaOLaYENcinlW4YcahH+r8B23mRNr
J5kktB6KwNTHEO5j6oNuGasJ+L0jpy6fcl6Gzv4WNmNLTNw4jDqIrthbK+B2zMGBiHEM+SMxuNGP
HBuyg6gm/f2i+HIIl18SIGLHUyK3xXVAPbSkqfPMX122qw1hPkJ54TySzECuCWwWvMha43dSt4cg
i+fhlzhBDKAWPEIojcTYJkzO6v7EGCyVl4tKK22HgiySzbnGpNZ146vF8YjANxhn0Ft/vmtPRuoH
xED3uEZwivqc/gBrjEspRvelqLjkF8S64yRMemnvFI3OnaVnfs0lLi2ygP2Vw1eQhVcVocW/9f1Q
jcyTtS/NAjGznkJRcSl2OJ//FoWjoz1jmNILjAb+xeXZ2KQNOJLAdaMg56gA2ziZ8CNLDMTnHQxN
kvYMmiV+dt1gNNbikNPVXUotNY7mDK/2JjzxPUsMAA3cx7BUALqah7VIAn7MnhMh0+bY2x8jLrxc
t1Dcv5GRNEZqHyzTJ1MLgp5KigZ7S3gkK5xehkWAEH8Vc0Z2XOtZr02Tf5rdZ5yk63XNzUgrSQpZ
3kEy5bF/HaOir86fhvYK6gFaBQ+BX59k1bLGKPlhEJUKOSQNJY65U2276l/MvdJAAFGUlCbIffEo
L9jINJBHILil3/7EAbm7Ok+d4uc4nc/wzHJpS1XvI9ZVNXo4VgaHXVqombaU+cq52CODOPqFLqfU
sLq8WMQ+utw8WuySvziNtHH3hRp5obOBouK74eZEMHqjYCo1vjO+m2+hAJ0efKcJ2133Sauw68CP
/DnS/mt7dCk/8eySM2kr2gIocCp1G800migy6X4OPcWaNAfmANvVnTq3+RkxZ7VPdQUFJdWUSQv7
affjUAjo23TUvSfaSMzpEIAXrESKt3RL373lvmItRWZTp7apzlhoTppdSlhUcFrjSB3KpCAxUTs8
PqYft1FnCpne+OK1yi7YruAlOlpWRA2c4zfsTSvz9fiqKVF3NqCdFrStQMEj/c20BS/auRMuqIQ6
KiMMPdot5frDj7tMJ4tiZdkuFnAvuaxt/R//ar0khjG3efjzA+5ITs+aIXIOuZt5ewTWLYpHJVUp
Sjm1apXHuf+grGndwhlzYvjnMSyZR/z7e++R9eLaRDtcuycGifz8Kaj2I5sTjabfMaIIm9PWThs5
Tioz9z+HTAB8PHAS+p9iAfgYGicAsltCuKCaex08+R0kCk2Yxl4AlYOfXk76fTeY694nuKyyoHsf
WxFFHIKYUv64Rq6myAzKLAyMloN65GE5UKbJCgisrAnsUiW/wCWVAo1y6IYn+ApbxJGe1UPNCmDD
ES+i7SiRA3EllAuXk8+GWM7p+QVyP+l3e2jeceDoUno5INaoUkJ6CHPIt74Xwi6xYz54FSFf8R/4
hZmLwtH5chQE6vz128VfaUvTBD9+gU+XveUCRY5y9TFkXwgb/GfvCpBU4yudbtmsUyamBvSwvyTC
/keA/NyQ/sLQkwDkcF+jZyUxOmo/5bbJydYN8YvSPfJDUwD29LXNjeFGdpfE53nrhPfxgDDDg1MQ
cK/lLt+ezGR2yFjyVUY8rBAdtioODIgSe9cNtJTBZ1NUYzXFVVrumn5HnELj5BQXOnMhjhwnbFqk
WQGKVU4IRVaG4rmzYHJ+JWIrPSWlK6jcYCy9S9j+nVfE/XuO82Fov5PKGuHHY/5NPWl6pHBOlQBg
PyAGsPmIPknBrLM7fhDdPqPnOMBm+wCSNjukcIf4MLGgyoJq+llTgxRZquYwq9R3xgQfCMlpWIxG
/VAhW0ilYRXGsaWOX6jWmnh/GtBWSz9YDl6NiR1dQAvTl0v2HMr7kcYyV6vJo/1c8FOnAN1kZX2Y
GtKCgyT/z0etDKrfRVtyLdGHx+ChAtzg5Ze8yuowTlyeD8f1sHekPv9DCUYHpaiGdjqV/TwE0nn0
dOdqZt8Yl4xeL/Rjio5aGnux8Cpdd81HNAAjsxHLudJ3P06SVsJD38QeV1kgfLbtSCUdJnilduUK
kG4KOCKQe5rLwLc1FvU7/8s2yG1xdNd51Dm20tt9CIzZSjlP6oqamW/pJYbKjvi/xWM0pz2TB8x+
Pb26PIn/9sderLUyLJQBiemq/cZa90KaVFtQXHvVY/ZGIAt/OxhvgVCGeIdnOesM7XH+vjfLvmbv
4Qp8fS97lWmd3VE2tsnjk5gyE5ryfQui1kWR6zsFPZFEp/612xDCLioW/4SKsaQ5nFYNYU6o5IEX
RMBqSOiS49/IlxDRQ403MgC5MWlzB3+EAwjp0e24ewfYBBVzMhnC3daK3OedFVvhtPDfoOxbFGtU
VZXbkNU20MDb+FuWCxp/vO1wo10o6tRAGnrRBw4z9zztQif2tH4oxxMlNTUSyLndXlKN8eoUPcZ0
axvVLBoQ0Quf/wbLy9gamylgG97d9WNwgxVGbxWgYF1OFeyrGDNVc03MXt6Oc2gaWFf28lEOno8j
l9BN7HE/F2FMIcfvQaZV0mJBM2hWfQhgsLg9TqBm8MTmv5ICM9lF8omwEHyUdKj6+jMLt7yzrx/n
laRqrpE7MS0vtwam1WARQ1tZ2GSvVuHWgJAwl0nha9CX0U9/h5pYRF02JZffpvouM6AffjE56CYs
3cOW/Yu4ZrgugAhz+20vnTDllyLZYCOO/wszrSfucKj51QTWpbIU/4GWAtgYS65EMtC+d+sOo0Hb
Pwk7vRjv5GJ9bek1zXX1nyacUaJW+sIv4t0AcMwa6xqAuobnBrnvG2wvAAgAm7ZAFPPYkbv4/pKX
z2fwgzprYgtZC5l5pb1mVa1xUkjqw7k3XDbZvEF2oIqbQ6zA8jWoUNHoGJ8n2RQi/45Jk/UAYn23
D6IXhd3CLAN/XzM7g/dMzqop7sxSmEiHeVl5tY5HTN/JjqbEUnyLAjlSkcGjBtdkBImJJ1h5KVD/
iB9QQTkWkSKkNZJ7P4LM1GlXf9WeexTD1rHBdlOL7NVvjsFTYY6Z0BxC22kP6Z2BQGFierggd5JC
14ME7rSG1Lg49g+q3CSBKEkEHX2BI/sSDyRY666jUCNQxIfCKOBvp3lE1su8uII/vmEDXcBdOTr4
HXTEi9vrdvMy8RLI0Hqk2ThxIVofrwJXvkyYvwp4662NHKifqn5A/8s3DFKBZFUnwND7SYS5fO1r
JaoLrgbgRkEJ1Jn21wPy2wLIOp9raH/p7dZmeIB/Nlyts+oJ9qEAjIfq3ofySyXD7ap6q5njYvm6
SNeIFOgdO4QPFAr0BaPTxdiOJ3uPW7zmP9gf5lrfpm1QnSqmt3XhZ7/iFdoyASQSRFp8pof/Mmdk
Jc2NoyZCVXlnF2DPnkRDQwsq2bDsUUuor6wPP6M4MFHP/D2Wyyv0pyPjscJTYsIkcsUuLmugKaAH
GK2EQynSwHqfEKypp5AmNeCBB1OEwDQT51p4RDGQlo9URkf4FS4TGUGfVoKPEaSVMPjck4AgfhQG
5oK/572b1DYirDKiL8S+2NznAlNQdTUD7RHNf7xkYkmuR1D7o3mBYIf6fjvrtGYLsL1liAhTFiRC
R6z3gT6tHnlZGzUT/+LSmzzM2zZUGH3nDUuggLpL9mcvUXPVElgyRJLWVe27+qX/OlgaR70fsM5D
NvboLeHK9nLbepQMV5tiBIMQOoZVVOZHxLhh2TatC85FBAbgwfRRdOb4dfXHw6dLyal3QYdhPcjE
X18yFQ8cQNx9Ia2xggfFKV8rIKxmf5a5sjkTmSojSKsrqcRirLLOphgfrUrRRWrUYka1/WXzXI8Y
0hK4VcvjxsBgnwWDzO1SNYniGaBxJierfmTctS6/xD+Lr9tjpQquT5LdUbaSvpixfJd/6khj8UfN
RCNCfo3+G9fdkDzHmO7Q9FqnshWFw5tRqnskYaedSVSolfCdW26+N2YLxI22Kt6XZbNvLQjMgdk7
44++lHzFKY9XyV+1jBuKUE4cFlcTexFMusLUAPFBrGTn60GQya83tu0coi7CACyV3+6STYOhc7QN
HcbNfhD8pgfA465FUS9PJBdxz1srwCamyo4b+j90CjNeUenG7A3vsG8+nIlAa0juAUWWiE8F34jX
GLquNwugpEIXaMlRHx0PkbfqP6k2Exc66QaZZ9cYAyWBQnn8V3B4M3BCjUJRYVijcw3vVu3FolqS
jlvUtYw4MFzuea6DOD2oi3Z4ubRimu1RC4czL2wjtkZA0DQCmESsPy3Lcy+rmDXiSvqiAMErrm96
f3rZbv3n8e46m5v8Er/1FJpbTdTkY4BQSyOVI3Fkos442ESwJUKXxc1BtKLvP4TuUcDypvpvoC1I
8dNJX4y4T8Ibht7/WkoF+3I6IOktHMnfKQvh0JW0af1TF/gFF83k6U/OxSHvNQ8RvlspzVfwPz4w
521c5+y3OQntmtEb1L9gwoQWmHnlGwOMQ+fxQSmet61uzrNK1ht470dc3JgXTb2bPAd1cLEp2bAW
PFe99sApoZcz0Fd9PJbg5kFGAvJPCpzCQGXUp9XccdzSYoiaZ97TiZTVTcDhoFh3yqDoic/Cwpyw
XbRzjEVKAreakRQrdl+GIGRRK7j6A+UXbvzlnjOh0JHfVv+tayci5SRxR3PHKZA5Zc4KTZ1jbU+3
LnEH4c44fHip2tlFTkabyuwc7A9jAwRrM0scscpsiIaFCaFL+wSlBWgmpReJYAxFVIsW9qfKqHe8
tznIKQtkPtpdfbiX/JZV08/9R33QN/BR5vqAlZHBRtQcABTsSsllhHqP6nvm6y3CWqS/Qt16ZhXH
kQmDIVaWHvdcqBv5PRQ9Qc7tkDCLlu8QGquXS6BArotMYneiicqZK5dSqQOXtO7tGxbsR9vbp79K
OcizGHV2b22vogzJ7xlqTKReROhuj5OSR2jYqv9qC2Rzeo7Geb2ndJcU5gvgTvibBr/Z/66UIE1r
YJtZZNVGm/kPLfrXscVE9gGtQq57uZfq3KCInREiUFUiLKGyxFB7RXYZul7MZFubHiOiVsGgC3oM
LUtSfaHbAJ3sRS3qP/9pMjtlO44isjgdmGOrCBr5YZAAjb7NWiGSZ8xoXYfG19kGaOKhc+sWvP6/
P3fYFGiSzdBtnld87+lsiANQlZDnuhnu3pStnebjaeSg/oVlcFuoVIaNyVFOtwGaPv0NWuFP5Qc7
rH3ApgUSF1QEqhGmaT3owMqmb32VycYfcg5CWgPQOP9wLggT9nMTXcyW3duGc0CzL0O/ru71R8Yx
TzGrF28ZR6z9LBx/sIlwGzWQnSpPOv7ZTaWd7FPjsaNoS6LTLgFrBsOSCTLGrXN5iu6iyMVTxY25
bqVFoEHjX2ut7bLkyd+i9IQiNxTN2KRTtib+C6XTtRdVrmNlmPGhKpaE3hTsAkL0YvoDaVa4gTGP
qWaDyVmUAIvYvQLHf2uAzPicPLORTMqe+F2u0CQeENSVSxxDVHSQTIE0Z1B+2hmGt+nbAAsyREGY
tnhhGXrpuhPXWZjbjZEi/a4XBnPCnE0F65weJHL+dHjDYJ+wNc36Ci7PMl0EEQxyXdz73toIiJUS
9PQWCYjLM7opu8W5tyXH9g1OC7drKhG3/ECOLQ73jL23MVR4Tv4WVWDen3VzaIfivBjNtzWesfQn
gKZ/PdFDGlkynhDfpVXEFs6v5Zz4CA9uSG1ByonoZlKreuqyY8fZn/HUX+vJhORsKB4mD3t4dc+H
IPXCPg6gvMEEKQhYp5zdwDD20RxG/qOSuw+tHllUkW3S3SYE6ZpfLQ0rhj/uMUHB13G4n7oO2Kvy
NyUDIXBBLlwybBukFPjxDG436J3yfjCQXvOnlyVBlTNxEw852L4No5Ta+aYyzHo08sEli32s+vvb
HQ1Lcnlyu1KiHj/V/eUYmHbbwE9c3DE0Rr4x1XkTGf9VkzrIuvibVzNFsPokJS5erGf6IuZWCCgD
DYULLYVWiNZBrmzhrAOm/KMn+MKNkGduvOPUBLHg9HJUOhojO6z7LNB7hB6IRjvTtgvqOFaTQdyJ
9EMHzCedxRB1lNDYdOCBPUxjfhglCFcXVt6RIFQdNZT/nTJbNce4W05hQQF3wO7vu3WTuUK6v6Zf
ebOXFp6LSdKEFHtsnahTpAyuXb6Vjt9GaHtBfMzFrrIu2qnlrOorzhAbCz1LSYuzUtbfZgzcDiCX
xorhdddFrQUKY9Pna3uUOS3XJ1/CZb1yg2EyGpnYxbcGyMrpY+VLR61Gxv7/K/MUQX6SH1jbEmyH
uHiTq+yv3sBUKEtvdigUrAYwb3RXdGEYTGDKEzQw44WygS4YTZte/0Xn8O5d9Bt7ykU/OimWqAWG
wL862JgAjzSHnXJpZdTuUqlCeQ7tOVk3GacKkO++2a7GCjdWcHCYkaGFXBUxDvzPHU2gsXT4i8eS
D1P49AWa3hSQHiKa3t+kONGGAslo0G+sOBJn8UWntlvqjqABGpU/3HmZYeo/RvvQOgzgvlbFNKRu
RZptYBWNqAUgDtY8kzdB2V222jkY6U+xNGNGDgMtT775UrwnOToWlP/k3S2pU11OrhnSQvS6UpVP
xjVNAOOPAdcb5R3gQxO+DnGK1MzTwT/EB5B3pcWzNg2CKkchGZkpRXo4j5WdABT0/WjlALY8Ch1W
Y8eccp3KJmDCsKJDzm4JW0oE3D6J6QHthL0AeG6u5TTedvSRer8JDPOwMG6FpZdGu98wtJXaI0NS
0t40a5eEvKHBf9V8pieHld9QWx6M05Pyecod7CGmsfRbVCVO7svwxKyLD9kB1nHuyN+qsVOtNd9s
5buJGsIWwO3Mz4JFqCS7OV2frYmeyIrLviWU8OL9gBaOVT+M/T/IRK48FhbakPZyUep0bcxI8XTF
7To/teq3rdRKIKNTcH+oAjUMdTKr3psorFigKV7A6Y2ivnCIovieo0F1KcxXeW+oLVCah3x13yzo
+Q/fzdKK55yCoPfMpFRVcu0CGS6/Oe8c5lKTBBIwtDBLiButb/3rNZ4FoNPMgjo2bR0NZO8eZPmR
G/rQKQj3uZiZ6DuDrE/fnKstxPFT1CLml1WjL5cAcnohuQcNbyq8ZoJEC91tDKyes3v2N4PJzJq5
OKh5UeirO1kg7/fFU794dCf2osUUHrWpp7S+0LZ5NW1mvfkkieZOEPRLQL9y1Ah46ryv32Rpvyuz
/jhKHoSrD2YLCoMjQ6vfnx3pyIoUC1/pHXAmUhbnEhbhjHKdceKVMv+sn7eJfDV/lTaXIl4516PE
WLGPv2f/2UBmv7bDAa0niD4CGyLX2GXwa6lssR7YHEy+bQwOqUFXYgv2Pdn19DUbMRrYk7j7r4Is
h5qdH81rYNT6ryU23dVsgMYTdijq9ugMbozFYUQYXTN/yr6071Ns2WQKttXTAO3J7S2xXi/mS6Tu
z+mIDFdstfavIEyRAVeakHIX8o3tGB9mqrWoDWAypxyNrJH5fC8ggpfbKar44FK25G6GlS+1QIYA
r/lmbclz336w/rpikP8bXkTgEBnjBhx5wiB9gUvl8Ir0m9ToB+kbZ7I99nkr6tbMBVOLckyJ6svl
LrgOdwLp8eUz7vCZtWnzKe239lSpBbkuv8/PxHaU4EjmJ8J99lq/m+/AEOCLwPHE3aOVnTGgQUiv
xlvOvUrOTKxFi5erj4fWK2z7EF3/S+HAPgx8gpEHrGxnqLDvquX1+iz2OS55y+t/AyJ9SVeFwSFL
Lv2vBpTDDn0kjSDre7vCSnI2+PDlV93S51/Cou0+jGFyVeg/o1ttTEdQOj5XSBXrPXXwsRzP1+lI
IFIuwQw9PEBv4AYADlxv437H3D0629/E8GpE0j56AASCYSrVtQGA3QM0kw6rHryAYSXR/itd84Dq
op3EWKKsJbgF3bKm3AUr0T5ArecptuVUeha7fI7BsPaMHKHNHBkf+5b9zRmJffEjXxHDe0BcO3Fm
lxQl5hJ3dwNUbgEKob8f1vukMrZ+3YXNwmhKdEebXYGaVhEvm/J2pKUqr4GBv1rtc5qK/cnRNaPZ
Ev/nHkKxAK4H0KklyZuPB1C7hBFcnDX8YKNE61+kZbtJudn/neSFjUEoqrEz8vpAlLnJTgP/kc/v
04PwHh/gMmHheXkcMBYjBNGOfrJa25U+0+TutOSIJjG0o/HwTU/OSCzOl8GBkgKW9yYhc6PbUNCb
ZOD8EsThd8TQlG604Im2FPOR1YTas3KWhosU1uLj+0VKVJJbW3477mJcooX1xcPrTpq/mDxGWycy
d5Si9EHP7Of1r0VHHYnzoK+pnyNMai4sEDagOk3DBi5SG9e3hpwsFqR1dRBhSEJnrvPyjncavU3l
gldjxxOaCogY0u1oLn4A+Wj4MWyTMiGxbui6Ti1q4kc/8+iOfzFEFe4iNi/QqQMYdn76xHIPasHs
OYXYoGcjtypM5Kux7RsrmgWeUG9q1cd4krnTfNloft9PbJgxgx/JD+vHHP0qnQBp9hIQlHwjvzW9
aaXyWz0Ea0c5+0w3LTd1Hb1pWw30FoMxjospCkn3aq2/Y8fjV9tUq85G9NCESTY0D7ELDO0U5mkp
zXzvqWJ0rT2qPGgpSTPq2II+i/aik3JWZ4ROw0x1WfDsTlW1enW38sy6wq5Xyo4ndTXCxvVVxwMM
nfpXdnE1DBb82SZ0o1KPZR8n+j0+FHqbLVVtZKClmF2ZyFaDEKnhdDVKuxvUWVJbZuprLgTKubPW
JVLtJuw9QXsgvryRHRSIZ6xIG1EO0GBfD5fzpQ3R6tDD3NG1c2lLXQzQ5jIcZVgUWFrDki4CUIA9
+t70z1wLtZOk+KNQ1nV/qtEL/mvbfApWqTMIeol3PqmyNUb8pGkhr9rn+VAeVQpW5LcnW0co89vO
fxJFyczpHylfmmXM6mn6vLqZZ0mvID4u6tTTua+PSBYn884mnKwvXb7XBYTnGzPRZ5WmVXXORDqi
wlq8VfB/awSIP9pjy0Atdqati0vBBb0PSlh31WC6HdRWUxev5nF69B2MUlSDACWbOSO56xLWxuQ2
/aHEBEiw5DEqVdHG6Yn2f4byHhZVddn0Np3zlUr5dv/6LbZGwralAIYwWNUu6vqumQB10s78BH6w
TtmRX8af8YHLeuTTR2HVvw5GuuUR06aROw+StaVvJY1YEVKbo79LyEsz2KFb64jr2fx6sknU8Cpf
NUVrA2qXaE47IB9rtE81HrJDysWVTCppuTYAgxgWHKlb4Y9TNI2K0KSCfbu8WR+H7WL3cthuHZTZ
MV2hWhnU4REF9uirJAktvoYlvr+qdFTs++29koeXP8eIhKtlX8ZjRroXxVZ9G6QO2yDioSTxgVWn
xgiedKCUR5bJt/d5P3KfzKxkATCC3rdqFyOaE8HulPBJ6Am764svw4Gag5C2X9mSyrrzCZ094bBp
4QK9hhyoqlfZBQC7k6L1eTXJTyDNVpgYFml0F91j0dsxjNvoIjyuZESZURIqmZSClLUc/54YrBXr
d3DRoGHxsUhtqi64/ORrU7pLqdgTCIozWld3k8K4RRO7B55kkwzlbfLKO4+Wh2EXH7i8eaAXZgBm
OEZEmEnduXQ68gGBY6BNzqB4qVsFFfL/GBFwkauqPSSkED2tXUTOPEVF07Q+wPa709CBgXPRG892
1dSV0K188uooRBqktq4kb5AfwFwu0kaXcdaFUpxstzRnX54czbGAZ54XuSyU8zMsPa3+ZWYw4TwA
iyvEHj3srHpfnG3vl55BLn9VqRr/TxJlioZ2v47uI7ekVYxKMSf3JX0C74JytN6dmQ2AI/DfaC0M
iaIJPgNQYkMAJ2xRcYPO6UEN5Z4N3pGgazCqFtUlBUI1jKnSuCIN3gmzqaA8k7lamaWL/3y1jd1o
yXl3L+YdgcPetsFsaRhUzGSJG5iQ0bo8aHdG25fGjyKtVo2DPLEgA3/WouaQjq8+W0xES3G3JHOH
DYUSfCuk7nQW8KtIA5xmBpPUb7PbtZc1jp8s/nSQ2bptozqhmNrPVSWzQuTVqUOWL14Hk8iPsJ8K
wyQR2IV5LE7ucQBQhXp6faCNK5l0+KYt896Cni8+VAwJ1jCvDr5nPCtgvHbieyOXY6vWzamApi1l
khZS+4PqkF0ovH3mktkMH77cQNlvVXOZ6Fw098pLZVc/Y09xLUk72VT0cfnndHsYgd3jDTnlcRSt
aYCjiZQc8EJsNnvFoAkQlsgi/UVmIAgMFzItane6HdbkACXIHF7Sq203dV7wt/4vC54fmImTF0cl
wS5qkdJxX0YiyZ5bjvItIsLxxJvfkJjAek91mpchZ6JUiwIoYs3QS05GG++LgCMoUzmGytJEn6EZ
g5NK0cuAMkE1SpTm9xsTb4EeO2V+B8XCU7eYgXxsmTVCnzwMXwlugK9EFTKAHI4zVphgqsgYr7Fw
sSbW9ZBnAGTv8sTq07IEyAzlg8dW+YVqi+2UXvNlgF/r35YOTRKDMLaCrWQUZ1JmArAh3DGpe1Mf
1NoSMo4TpECeMDwqmH23qwOogBUbBJcACIXOVKSnZgQHVyv5xcXuKYkAmft2mgdRq0GLfqovKW8X
s+o54VsRw7VZc/7xljHiygihuTMe5fxBpc+qsPBBkRnSpGYx330dF5jsd0OptQgBJ6Kj49TTDrQq
YgYZQpU5MoxkzZju3VVnJg614Ol+9rQH+lmSuLRd8t71ENCP0e/nGxBqjHqFkTR1mgrMU4q7VKgi
dpUgGF9e5xrkEa/9WFFjen0uqqsRaZT9PPWrpp+Z8QtWVcmUfw9TifwwLqP2BrFhwRHvpf7WFbHI
YYk9Pf1kirXKhq61sd81U9WjgDtnpbWz9j3xpFC8HIc1D0Vq1RETXRj7C3t2GcjYqBEzo7SANRmc
ljo8XFV5yqgmaW97uffXScSVJo6xuzTHQPsVCc7LFrrj9IIyMsdoPvuo7xCpfygilcWy4OK6n8Bo
htCkpNptdhr2kzSnbIxykCVNuvL8DiMNWwfzDDXm+viIYFlHR5rLSLSciqQaQw+6+Z7bRvFMlKP2
CpEYvBNKBZxH6ua6RXhwP53+u1q+pk/id1nC9uEiW0/1wTEV08vOXfVHKMctw/Jr/Z3ypUeHPjqO
1DNEeK92Zygg3cm4XSjroCw4BAvPAseAjBg37vbcmxhZzmze+vku1rezjQCz8+QVcXJYLK48IyHN
WxdMAP7Au3LOZ6PF6AVsxQioNJK6FEHghPX8+iz6TN8L4UC89x1PYAETWieWu+rbRRNCaf0hl8dU
ENKIa5A2GhNYKWg1FUXjoKqslk+MIBbamaNcUXkshmDReeuL+VszLkYsHzOOv71gXFQ1h1A9HQIY
CU+UYUKA/V6JyB90MoOCk0WawdhxcBTo5BPRTUHrqlPta2JjQxWU2ZfgxpaeKAO1ZxV+5VuS5TRK
ewRSt3i7T3C5xgGd63n1B4LpoSTiEX8O0BgieIq24k8zdKz5YhE+Liwi+3pulKHE2oirfLUNxEeT
4IiGfW/HekRQKtrQpjf/qSp+B2HsP9KyHTpwQ6U0hCHqcnbCazPeH7ag5n9jjQxazVGC2TlkYDte
Oosdj9VHPY0FoLc4M0REiSNZcQwnYsUFZ2YKzAZA2OEXCkvUQbF2ZHusr6cUovHknqHT58vKgluU
yYEiaq7kZcapFgy+P+7iL+AzQH5RApC41i/4aLR1KT2LOR3gEcIJDth0YdBf9YQ0oZseMwnH/8It
HXQOwsfRcdbXR73KeyPGHq3uzbC/Mdwz4Cv+G3a51pMhU0msu4Mv1GNr9jZGzUVvY3swDY0dKUnM
NmXhHUy9HDCNhZ5gBOpfY4xRlzsbsLbeUlRd3mZoJkfEORtlEGqwvLbuoCpRI/nFP6MSF0Ejn6ZT
eI76f48BJt6mHr2ZFIAVXMPEOUGDClhNw1ApIfN18Bh8tKUgmroUp+/x9fqPMxLUtCO+OhN2DI6I
mNGFD8Ik6++6a2LzOwCnDO9llP/p1mGOCYJZcg1fUAQH4Z1279uihMXzp1px/CesbERBy36ojJEa
FQQZbQq/gQodqPY7MaP/ts9aOHmmRJOQ8yPma9/mQglmlRZnLt/6jPL+RSE8PW9BPceuzXadSyJO
cWrYgoIbFDAe0By3GteAQAiQb3ceL2kXw9tFoMvQG45vjFO78SB8PZgQ6H0u1v5g854NfCezXe1P
bzddE1qX1gBn1PtX0V0nGDyb46/STC55j5ljkDJiA1aEbD1Ihs46VMtHeb9DPbTAvV+4pcEg8dug
LGiQcWpADZ/+pOrdJ3vLAM8fPZ6P8tTytwHqG6v66RxcSui0vH+WQx6DvLyzm8FUI5CElU465PX2
qsVwDPEF4qTQSt/6tvzoBgY8hKF7z7SydPK2Pi+xqevnpKms8DszwVifDJ5T6d1aduCyv+KwF7C1
fH2F6eyW8kvTNWSyZvNmnAkvZ/Qcp8uIjAhv+FcrZlP/mOqQllss/yuKteCr2ECwSDVpFkflewGQ
woySq3E6cq0tCDtHdIgpPXmOP/W+GTc7CF2t5YVDrIAPGI4J+LQsJOCf10hHwp/xyf8JlSyBzxWY
AHHzlC57HWv30kLDWjpW9StoRuW0T+pUMUvoQ6VLnC5BKETIL2mZRWtHGJC63iZO13L7QTdEvuxY
H7W/Xy28rMNIHRMW4kr2AUcAJemEL4OynrWLPBVvbrEpU+R93TwLEhw12vTS9fClFhecLEdc2qWz
U9phCjdJtnTGA0SnKO63iEnjbZwNSsRFgChw42VUKIFLWcrrcQGnQsnpoFjOKqgIF2MHZXxdmESe
OQVAet56VGR7NpkG0ug6WcBxINPmLtfBzvK88e5KrBIjrl6u1EVH48Lxbso8K7JHLgiGblpowiEH
4gUoGvL0mG8yKKOon7mj0paYFzG967VzyjhSTgTfWQZTC3Cf+2DbTov8VYhP/Yi/rGn6O+6YYHPW
SIxzmb24Sl5imYZHzx3ex90prjSzGIAzGDgmTtCS6tRilCBp6lbXBIWjSQzEGGhNvBodm3YBUa4N
v6zE2nEQP2EPTou5fuVwIN7CbH7zKLOppZR8JsidhwUEkrXwjK6gU5zHFw37XTOghDRxx9FOm9Fw
aOqeK8gT+nSVVytNUhnUpXpYvU255wRvt7e3bTzD3QtUjbG/lv06Gswcsvlp76VRSLZ3FU4hrQ+R
ixtvRkwRH22Vle3BR1uMTu09mfJVILDzvlKf2VeIKpIHei4smjYsvWc4ysvSUIwtr6JYiSGg9PAJ
D6QU4JCuMpkYYsYxnyv57dwfiohK2NzJETrHqZsfpX0OHoMMyZ3QD+nLoBjXcD5VhrCcpBGlNFez
LeGJaYuM2RWMGYhR6kJ9yPf2oHBI1XO37EaipzmcJkfRj/2HWshNie4sA4BEMYGMRttgBhqYh5Ln
eDqopL2H1BaTe1MH/7tu7CMyKDBNZ4bNlWqCY6DlFitTSO5srPr+emqDRHrgC2zaAUDu1RWkccUO
y//opa1zvWl7aeFl7aq+Y7lx8VoBOGVz5n/3V6Ov4M2rR8UI/AYjzLijyYsPhuLHRAnrdBjW54hO
4hI11zn1nTahJPLxjZrgMZJ+a6DI5tyriL9q5RerxyDHaLpPOTMSQhTOodbs7QpsMbER7aRVVZaf
wnabsmM7z9iOg4xhegjHkXLJFsaPm3S4I/jwdjodDeAp5FSic5qHhxgDFCPZQ51iyRZC2MIIKNzp
F1ydBPEDH9KMcQPj2y2Gg4DC2Biz2xUOMj0KkgVr3LjGi8ovmUPP+RWEFN8hhp3w4PIxaF7spmrq
FVFm/wRZdrUjbehQ60avF+4vlRBnv4FPrWlxlX+Ubnd5EI4kLz0a3ikBUzWSmrzWrT+l4RPrfssj
6Ud0nfsOjZlk7eJ4KC4C5xqtK5m97WYvs3LRB4GUBRU+QVbxh3lY51CE/JtAP2cw5vnFi3Wg/Yq0
30aoy4lnyFGMNup0NJtAmdBNzXshdm306XC91p3Cf2j8tZwp/kpBHQBdpP9MDfFrI0UPxq3rHCIZ
J8tH2wwLn4GqwVfa9P9AKgLB2WOYN/vTvfb21jDX0CZoVNraGMvndTv7lpVnRWevoet81/361gSJ
soBOvP5xgLiw37pkbu+2MVvvZZ2xHT9vyA6ASrr8xSfHNs5M1WhvL0hFpr71C6aD59HP4+dYSQl+
mgQEDX2KI0bTz+AqqnAutKrO115tb43PTXEYCstT6oMx3VYuOXq4qtvFBavQHVyXgK45hVgyxcW3
YNPpbNe6W00qxns7xyrWcUF71z3Y+sbGyihrHFrBqVrEJ3Ic1mdx3SK0n5S4k40AqbQam+InV3JH
Uj6hQWkjHBqAtFDsey9XdezQM2Ws3hUN0wZrCNgFUfITWpj/pAXOcP/1x4eC7VSYlN4GXq9BXy8W
CRASHbQCXW/8PSKd7lA5tqG4aMh6TKr/n6kd+n6s/3lheJzZyJWRIlGpJcVO7SnpcuJaTR9tu+2R
rdBaAS7oUwbh7dy+9oSXziBMmkokjHZKqK5A0DfgEDDy6ikt16TyePgfFjkyUUT735C35PKjZAwZ
CBy4x0y5YuwGaLGYDk0xVQ6EhH5DAeJzazdCp/JnSEGSGwmCPWUjBbiLPe8r5xc0dkK2f0LOyiDz
7EynddKscm6qKaQZ2sfnI6e/9j+6ljKAnOSyDBHQyD9/PtXY9q9pnqYHzAZg3vqOamkUnhgCvFc7
kv9KqFj720RWJQryoCOt+il1OBf6/hbXGIgovLW+8jKUeR6iw8egw362CMC6K4b0aelfPkp9cGXX
w4NmHyWYJB1lCowEhGt+0xmkXEbB4rNUKeEs+5psNC1WeH2SAQMTDVYXOM8bCRPY5fONMaI5gznS
S5cIPf20CVYfrfiW9jPDjLKdlckjp1GSiJ1I1+e0xqeYnzkm/R8GX17bxjxDXnwjKqENKIxYjBNc
EkiD1tTWJipiUsPAfGFg7oRmDs6mH17SNKicoPj+a/nru1CxUKnCDgJu+TI/JWIwETKLZDplMQJf
VQSs0v+exILugcAlh6d8coEoFxzPZvwuVnVncENzZqTQlqXt7jeACPsww9aX/KjkN+H/PArZMUNa
GzgnMexaVX1DyEIKnc+EsatcSVGRYqoTbBCcr1eoIHVUDhHsP1uejaUpuBqPUi6ojBSCWWyJMNQo
OmS3wpq5tdkvlwLFMI1E818w2MYe5koUW3XXY4I7yEQt1usF6dLq5CrnnkkRQrzwLWcbSARwg26j
KGhoMudgLrrciHGwMqWY7N61wu+uGH023lua9fY8WN2QcHOjwxCHL8tCUQ6IrnwrCYImAsz5kVbq
o1bSkeyzgRekLdsSXdc+FfndD5m7t1MuUfWvKM0ldWVNgmi+vuiGxe8qMj5lSgvdBWYGk5JTmaJk
8wSEGdwgMnfyTUmcyukRQgt/KcGrOOoGT2hncdKWcDhgWDrb75QY0Xxu/pSExrRaG8I7HkQF9bGq
/l1aQJphzHUCdoCSrszbVqess8XgjKy8lPAkYHMJ7KssFnxV9WH+CEf5cKMe3HuU6up/sidgb+Kl
/En4k855tPsTVUqP6dMIijwnwHfN0XLdCirPNNX7sojQkIY5KBw8GBG2rUsSg2jirLlxJgoosKdx
a/X+9nMAOdo1F8uNvbZ2cZnPmgemllhGrQEy+hs+Gpuncw5dV+K4LCSgODBb27E1UUjC6eivkUxD
sBAOrB/ngtfJpAzD3JMupaxglaOSwv0SPXQ8xXRSfBCs4ySkghmzaDLVFaGL3WK7Xq365ERgusjH
qWw6t7/6GWp+UNsEYWAx8CxslqwVEpEUMxbi0gZ+pqDM6HrLsmjSIsfqyMTF24MGmENqf41KWLXN
NIN6v/HuVCNUDtiIXKovr8Ij0v1eimjnnI1FwRJtMO7aYt5JLOVhFfnjMqfw77e5ZNFWpRC4TtWz
qMgVw8EEZL/V2SHdak7chscqmZHsamIjHBvzwSTKy0kxKUbKpFcFQRx82xku3WqxDcdEB8XHtSWQ
Wx18sJUM6OVJQt1cYsbTbnm4Ekx96EooZW40CVPVYlMejXph9cedlFk0M2FsctyUUwmS5Cwgl61L
m/ssrgqv62qZburzHnqeU/WmyMW7E4XCGAs6t4C9Q/Ie9JgR6nE4hOSTAeB6biosi56Lh56nfEkR
RvMR6fulZeLCYsmJUZULDawQO2hHGVJetzvmM87iRHSwrmD5eQPrglVwkyWpRc4dRQeD1gWowaFU
T8I1opNTnoaPHEVIn9WwoTO/uN6rX1cMZTWszcy5iDcMuVDzwRK4FqhnwIhIf8DC22qeZXz6VjOu
FSIfDHJaCNFPeKS3uSlybdnKyRRwy5aLTI9Jj0S9D09y+6QLLKbYY9YM1rdenl/2VzJlCKuYTAdF
uS1sHfPYo7UubaZpchDHKakOKwsM1e6Yo52u+bTr9KTVnJSn5h+DnTmpZ98ews2jmEZpckrRsqza
gjUgnHepuCvwmgRJghtfRapPMtRYX5Db8OmR9q7cCEL66GxrovIFh8LTrFbbcXG8Fhtu/y9lCxWC
s8A33ITPLlOyOtTK6YwSgMOASbN41qgw1cn+autQR+Akd97f1gwZKLG6x0A4eLQBVY6Jju0Jmy48
q2fOFL+b9cIyMP+mR8ne8u3iic0LKy4d58SkRX94kegVLSjmrjmxP41dtF7VWAJFISfE0hMOmTxE
Ze2Hl8iR0ShZ82JEzwMnZIJw6eufMcqmpmxGOcpTAdiUmnCmV2LSYXdn6Bbwpwwnj6avXrN62VQ4
UW2dW7fi+VOxO+xNKNRZanP8w0ObTgWABKtzKWrchg5ckFux7j2wAWAnhSRePkgcdjzBlX2pQJcN
T2c6fleJbITyGOWQRU+tqhE2ImiS09zsSphywRR6xEXxjQ2GAjDg/6TYCTUbtLdUlE955OVDwnt5
NrDgYwnuGYpZd+bYPCP7hq5QUyvVR9uHBeiVWJnzZG8C2BGPmveM3gMryXXdqhGlBDPP0B9UxMzC
BAhE3qm86GiXRjlQdc6KTATPhUmHKs/tqKUu5ozLqYQ1tm2RTTMlFgsUE2ybV6b45ECxmUDxYE6F
M6f5sDUPn3NRyWYby65r6n/IpK8F/4aYS/4zPRApRuMP7gjEQyApOlVbmjQT3ZgoufOHyVDIuW7R
xq7APSe9aJV/XsGDXXCSGqTYDQEceuN0aTDbkEElFMZcnloFnHYHjrhvX0dcRjDP3PLVOHRA58xD
vGIFVcy8ijtOTAW32eItw/I03wMaSuctgVaZJ3efyqYyDbnO3t1baX5SOZzFDIyWgbOpt9JQAnqM
8FI67SEZcOZhDqowXa9j/wVTE7N0CstHAI0nAN3PiHAxTQHlYEcl8WqAfmZJr/3MxGSHOZRcH1rI
FJLxhTa1rKZg+6pQHsTT2UZhK0J2vtr+ioLqvyXMNmzFQfCGtQgTgXG3StAQdtHzQEUPeNEcum4V
DQ1pecVBO4jnsMsCfgXbE5j8E/BiXE42TOplfIxMVPL9fbzy8aG2Bvs9ildYXusMIuDxNDcHjQKC
qvIrFgPXhWHS9CpsHp/TlnnBi1JuukLOCqNf0/JJjRQ3K4lkoX85pP9dGK87x7t5YgagqhDdsfw3
N3d2XtS6Qv3HwbstgjePiWgFFqAHX+Ra83gU50fgRbaiPQRkSPCL5BxeuHg6hcCDTXzgDrXFyxcp
GP0PftH2Ar0TgTp3C2qArIPL+SJ/wiSA7uZXved0QGUTdHF4vlBJycTFS5jtrG8UndH7OkigTYuL
ewqm6Eluxl87xIgXiUthTo2QtffDymJ5ikyGZZZw16rndRdGlgCCEsJyav075IOJrCfkrnMmjz7V
6neRfBgj1CMC+YVC3041qbelzVzGCqvkhSjVavJuu3XLDTE+Q2rIndCba3zXDaMumOhUM6ttPPwH
fs+xgJ1oK4kX1czABorSmU97TG++XuqQhndxb6QfVkuy3E1SpCJx/mpUvzpXgh9HIZUNSoc6WUte
WfQLybncr4V89vhKmwc1RX9Iu7FWTd3c1Y4A7gAGor2qg/j7smTJimo/yqiFEQYJiL+lZHffMuLT
pLcuKtzodDAhO3zq4gCg2BM3bDlM045s1Ag+ECGYwzrDJmY06+1FbZH9DyhqxukSHA+XlOz1hQkf
+kObhO2q8jFQGeP8Gdhg2a4NwVI8yXQTaQ8QOVVxZOgCNtYlD8YiO5iNo9QncEUufq3Elo65iur3
dJSouobtp8ZY/TgliZhqZlqyg6zYa78hEs4njBRs75wY/Jr8Neypov8G7giqZSqUevoIgi5tIXTI
YH/Y9CdI9+J1wV6cguNRWTKkPewGmjs6l2Mh+M+ST0djteT0HD68f0G/Wfqrz0VQ8Uk9VPpTHnCM
T41X/NZcPrI+1wuC7g1Obat5qJrF8RgfH1c64Gcg2ffZT5luXZKB8cRyEV6wnq6dCxMvjzwUYgAa
Soq54tjrN1UTgZbGkcj60q9WszUrYhoPYRDjltWPmitrJmBL+/muMqUTnoaQKwmwhCV+Ad2C1CvT
bH6ODO6XMiTr9iYAP2oncdYHPGVbQZV8ptc3iBJpXvtz2cuE7eMXN4ZzdqXPl2MZ1ti1kEicBLCC
R7SyEy/sUTxcscFp3sn3DQYapPz0AabTACSwc5sWYck9VxzmoRC2oGmvSD3+5KmUmgl32nvyeaZD
Izt4xmHw8VDb/6JCAiWqNWospF1JpZhjxRr6ayU8fUb1GV0KgaXgbdN9REHJSycC9SU1h8mMLz1n
3Vw1CDJwxzrK0qPX4TwPmcXH0MvJ+O1t8k+o+shhFZxAP9OnsPpg7i4/FYsYKmuQw6xPat9iTAZX
BmtOxx9ux4Vcj9gyg2lDw2gB5pDwcq1/mcLNJsBDTbW1WDZEYgTp7/b/oV72mlvkI68wylpYr1C6
9C1GVmo8aB8Jk9MEcCJksi9iOBlDtxa45QQeNzjBODIYfMfFDjQjoQ2ywtHISvL3p4oiieAQLPnr
eDwvA1ivbQFdyg73CJGWJB7YiMhmq+hgnEe63+9Rt3OML8SrMwHiQahmjg8KksWgWWAaGyfZFh9J
/peps+inDyk6YMEQ8az7a03l5mDZSwCbkEzPvSDcJ8dzpW4fsG102rHD6AfRB4UuMuexkKocZP9v
m1w0bZDYknEzXWARC0PxXC1HxPcZWECA83wn58mnuuR96xv6f4igMX/ulZFT4YNF8hijEWBDuoOZ
Og4HflPyzDueUI4XPVxjh7a62TThFEeDiALSBpdlmqsTILuCiw1SG8BvIzZWairsk9wDYGvClOpn
s71E6g6mlsBPzSxIT5QlKtT2q0fdat8EqLvoUs0aeWG3IVXzcFyuLFXLZV4OUVq4XSY/VSGhSc7o
V7Z0iZFNj/oeYbRW2HdSubgb+kqST8pQiFacnw2l++y8ErN6Bj8VvQrgo0LIiyXcPf4lgUdQkW41
D6TyPLMmgaMqvT0f01bbemzkA2lmsk7PRpVbK7kV3HOfXYJiQQH16j3hDzWzlL6Q4k9a/ZgFG9/3
ZKTGxd1t5Y4xfHtNvgtHQ/+5yHoyAUW1eWksi4LorGG7J3yTus4o1z6texUXksRCgp3heGaSU9MI
e7JFxpyeHhcxltMOn6UgbbtFsqjrT2OJvdZFyCwbKJcqKPBRxPvKjlxBYqqctu98+c7nU3KHRhQ5
VTid0jeTe4n4pRDBmjRPRPDd7PhIULZv2f7RLQ3jQQi9KvW0vXZjTJIgEqBAlKsFWFg+XFvQKrmG
amGksO5XI2dgeSpQE9XiSBoDBkTPeG0jf/vCUZAnh1F4otQu/Nom82cOL1pOH/UF14dJl9E/n5ou
yOfja8JyGgB2WaqssAI3E0qTBpVsfgjfKw5DielbYfFuq/CYHMFowug8H1x3Mk68ZKAcqR59SS6W
ZyQVuzu4InGn1y/ONnKpUBRqS3viuEXOkBU3ItFJk3eBJhg2BGWGzWxIXAxTBtwZu7hm1GZt+huv
UkZVZbGkXW6B7DXG9oEMkk7y34bpg8M4G0dVaiTB8CZJLcTiaVVKk0gjUnEb8peohocVKSCri0DQ
zIyE4XWAJDnrANuk03Y4V8rqqCQ167sdTGgiONGco5tMVRnxeGepU3ApNoKKrGgTo1LP5fa4ovUN
oYFyLxRo13hnJyLQtB1eBSa1U8LtbnzWK7rSWvQ8mBSR8+RULig2AoP0SeuwxwxA9J7w32Vj863B
YCm8mXmyLzN5WxqG8vhPCO9iwc6CGselDdo3jEy/G9kdXvD9AH1msIeAOdAiJkjtF3frf0Y/imLl
N5VJW3nKl6F8t1ui0g+Z5OADXfitctsh7Dfg7ij6x/MlCkLofM6uHOjDKzRu85foFqUy5Qq+db2X
dWlfSJupZNke/Skb/zEBRjaYA5beILzVX6YbfyRpSd5FO+wuILEk1HK0RhGUee71QWH4Hi6vG4nB
4ua7XTeKheFgriCaxTM1WEfYOV6sJXXv+irDqRCtF2bP1QFPC7XtQYKV3WT09AGE8EJbt72wYxZd
eFYtGHz9OH1b3NtKQeGjfayip0s+ye2JLqvHZa08p6Re90TyaRIGxUTq1SXzqCSfQ9N2c2KcHc+8
5zUCH5YzbYBKJ3A2x/NxqmykFanCSlGeuVfDmRn+8pp9Q0CORz5MA1I7FR1FQHN5DYsuUssiFIIL
6WVchYwhWbL9tbJmAct4uf6du0Pte+ZYsMbnddvDnDE+G5XwvBQJUuAN+xsjGO1NpsBCp8oKELpv
nb0TvC7gYJorssnJdd+46pHISRCvcd7nOEJ0h92y9AmYSlgnL7FODvf4CEJH8NeJusH8ZjuKCyGx
sATqyE2j3R+qYjafkWwwHxyRnNqJBArgbWJcmUzvbAfqfNU8Kbn0tKJW/JbX1N51ZeOVY+bSZTW7
NVTPvC1xZure/Rcx4FAUqAyzq2Sq9q5BQMDVRoTNH3EGo2qdT/ozU4CZR6kL7Gfz+x1VbdDBtWsG
q/ojbTsG44+jDNwCuJuBS0hO/IgqBdUeJtglSuIt/kNyEtjw6laC2+YgLvyjncweivKe7UL+WOSW
bYgOWwhDlS+wLpeVO8PM8ocXXPHtgjrSqo2ZTTjhO9wA6FiZAkbTPoxtSB5WWgVohmsvviypW9+n
EHvZe7MiZE9vp7rm0Ep11H/HS7aT+H/CHH4O4tVO5HhNo9EShVHAaA1jos4EUPNaqF0D0FdSzyJ5
JLVOiHoO/vEUTJZ0r0u+ds05Bj4xZcZXITr1B7gnnwKiBcP06ek44fEunaHth1qdBIxO4QdABm20
BRJeHkE+YxVwsi28UbuEQkrVJVaoE5t6jtGwW2/+GE8E4aBUS9MfQv3mfQZxvmTzqftp+o5JGTXu
XdA4nv2fCcEo5Pp0f0/f8TmlZwjIsKcg+MiSj62VmmEYpNfRtun0g3MDBxz3so2o7IFwvKJ4n1u/
48e2geiUAigTmiugoWG0EPQD4kO/YhBRbITyyRv6S6iv5PJo/qqrvnih9XnHTZHS5CnfnmvXNXOe
8ySWGWoLwi0b7M+fapEcM4hME+oYN4l4I9ydwZvpYxTQDZ3wCFoMd60WFL5SJ6+A/TwfODoFDVTF
V1OL+nzsa8ev+JGZszg+aZRlg46o5z+GcgvFmmT3z25EWfTU9f9vAyO07vKn9HGfvH8MIMM44edL
h0FSyaK/YYjKeUm04EoHvf08HTOhl45PeYaKB1XY+0jQjZO4uwbCX3DDSq4ga5l0VgD8fdhbsOSA
c3hmiC1cXOVqvg0f953bNHJA2Pz/Ynsobhfe1SapuOdOub6sCcvfXpUX8VN3xc+VwlGNq3RUJ6SY
HeWAW5rW565XeUXe2J6Az01JJvlhajDveC4Wkb36p3x3kNj8QAU9n2GdHXakDXVlQkArYPC9lFiv
iOM5VNY24yWIaU7q51fg2ZZ0TqBMuwgb2hN0lB4IuBS7tz7Ama7z46HqI02kxnZDoyBu/ZoYo+jS
1zFp+0PZhrE0Oa32xwv3zlmBjXqfIjRjE75GQCbleeyYlvqac2TGgaYT6zOqg8FjIIY+PKnDWvFi
wie6slpdewScsry053XBTzCU/ssbnpDlyWkp0YpGCNrJ3JlapGoMKsBA4Ew47Hgrlz0HJ5qit2gB
Y4hoiAw6zlPxW8cnbjS8iCLp1BugF57Flrp2YeQorfSkJds3o0vj934+3zbgJ6s3PXViCosiO/P4
WR/37qZXsqa45BvlHfeF+SLLAZibDkWoBApihclPEz0apJiK5X53ByKGYy+pSb7/UCnPShxL+brq
mvx/YIE2jgGdKyFjMHiynPPNyTA/vUvPbfcZiwHxUaZ2c7TdmTC7NGia9gz5CInKxa0F+sANDrqq
B+ZAHpjp/IIhjSuWS28/WJmuujUazjKw7ixI3h2ZkfsG5tozvL5paQbCwNCGRN+hJJE/9xPynMkP
XPnFJLdFyqBsuPweKPMqKnIf6jJgcS899DJxYVLt+SE8U2Ls2fmeOLpk0IPu8EWtH7HHdCYPJgVX
b6DCjZHQsSul4cE+06Bp9MhIHPrzlqflvAK9fgAXHSieQ3GHxcSZJTclhxrl9DHf7pYWyvh8Dmxo
NcnbSSMrjirWLxCDujYEWFlgH0RaDZeRWq1/pGwr8jZisyVxfDcHLZBW9jRTtciSV9UgmyfQs9I5
7/F3/kcKJ+kmpm3g01XvXoB5ztYC1y4edkjE8bfy2XoYbVGdCzaNwPeFDutqSKjwACRHGep817vR
0hXhjtY+2wsuPdco8nsPQfthdKbSLZHTv9i9Wn0KoM3ne8ETLQixf03zvd54CgoqRofkXntZUA7Z
qT7Q29u1hfZGgGJPJelFjgP7Q07yDl8wdHFQOELi3Tzf6V2puAz2+HaeaKt/QeCLYGObJHQoSoJ2
EQWbV46ODU2MP/J2zxP4ARxQAQu1ZvzwcqIN2QJHrH8XbVPeUKVvhidBte50Cl+FKoGfux+sk3Kq
KSTJ8m2owYSl9/e3CV6uo7DSF+VS3CtfSg3KtuJzYg8atkUtk1axJwOyIpyKMVHGu+4W03P6o2cl
Pg30VtI4DAmxgkLTL6Yc8qtmwVPUbFo0TBTjEtGQpZWmL1S9BWedvJKtv/6C+fPG5Qp7mLAW8U4F
Lbcp7dHsW+MDM3gsKq68rceftspOy1lOQG3nFYBJy0riHycUJ0EbED8aablWajib3kpLqSQGhUVA
cqdO3RGiTv3J7rd9z4deWGsDEXGUQiL4xqkQWXWVdl1Zdy0vkl850bW99VvDpvGVyLunxJp21yoa
43QIbSjOIf6bN1lDPGohdo2IP1KkCnzc/9tSLAF09dZsEh7PUmCn6pVzfmdb/L21EhMHlThMzeQI
wT700nMLmqU775+Hzz9HWneSqvc3TCNfoVMbNSiI7Q2HyAP8IGPvMNE75AjVNkh2Qj0MwjngmLbu
WkfugIo+wUPw7DJCu/KQ8wZMs4tXZpK8D6u0yxIMOA0nrzTkc/gOGX6OIVmGy82DxFbQu4UhDfSb
8sQeK2qUju/Ojw2R7c3RfXCu+ofeXpP6e7yNx273N35Kg3WbhKGV5X0EsGn2RHdQLzAOAy2aHPTi
CFzR65fz3RUYeaMEgG7GNSw5/PMyDhvCMmuhNQ1NEJGZ1RykBdQs1HVGDHbL6fBx9VSH+nf1D1y6
L5gzjIO1QpE+CXel5NIT9uk1qZqeAzfuhnPjxTpC6vaQwyzqeLwf8qNhp39N2dP9T9o6fF44kJt3
SB/2mSeMx7JFk/Ae/9yWgGwObKzw92e/64ujlQrJfmnWTGD0/j8pq7DkwGd/9lV/zv/sVMtz/ygy
V5rqXT+syH9/CmX018iqdaCDOlpl01RIP7tOwMZk0R8Of4+vMn4CcOfIrOR6cWjPWvdMntnrNhBC
oA/G5re0yyKW7mvYRJkUKdqZjCt3nfPWrR4Lev++K0YAT7TuSd8GsavQ95sHn2dxAZD5B9T7QHYY
gLOwkF5QTQExLldXh951iMb92DrpSSsxNJzev7KJm7TzoRTtdhQY3EwKWl2WOqaIIsVbGnWzlWLN
+wP8IC01CpdKJHxMIMql9BPeinjNkH9qt1dSspxpKpQcEyYJwy0vuxtdkYn/4vqGzaruBR5pfZ1h
Lw2twkU/KlMR6zxxsIzU2RIfC0Ifir+q6UNLsotZbW9Tr6hWL1c+ITz/0j/0Zj8lT44HRpzHQKQ2
7fMtNKRz/17h725isi/ADNK1hvb/A9WpaGO1CPavU7i2SgeNpI+23oTjDysSibEhZjGKmtJyaho1
W5ks4TPJjfQfPbOCz7r/XeRGqE5g7KTr6zUyNBxjU6kp/gP81VXDtLufSFoH8C14Djwfpx6e7FAz
mve6twDn89qf1BdIjKAanVDzCt3FX0kYiNdMRlT8ougH0G5Rbn8HbBvFGnUfRMVXsg1Bmn13aOrF
IaxwzqySOzkUEuUEwCcWJtymKaZsW0pBMjFJig/Rms8faGC5+hmsQNe78YoSz6EMDj1V6Bq0YMTf
pMiRbakECGVTRXfWlraKQcLlFY+6XM10eE/1/Bw3xqLusg/yJIKVOF7nueEe3AlqLxmj9K4I2d4J
H2toM6H4vpJtqCQE7CJdwWoR4vGB4QtMAeWr7ZL4rMP2Hd/25pib6IVOquLQvc3iQnTV/HsxbZtn
j97I2MtK2gbz26vUAW5eVxt7Gy4yGmSiBmx8e28wTa5lGwqOYCj8lLOOuE0Ku+pCmk1ig/62ExRH
WEvd3VbknyJY8Q/yQrihlIUvLjC0/MXdYx8NDDbsFWQ9fpXnJVGhsag6KKOk0prZEP6ZeQBD3kLQ
d36+etuvKnuDt9RGjHj/Lrb5svtHDYljeobh79VfKq99fEIpt9o0AYRRWpty9a7AAIMXdZQYS9f9
Q45qTF73y6pHp9v/rKa256K1sbzsLNRxGVACN5r9It77UaJWCv3+plQ1vfWx9VGIRH/Zuc5f2DXc
EKhzrjMRzyRm5KEMOSwqU4IMzdTyidcCDlZdA8rhrv/oW8PgUtSOvBnuCPnGE6g5oAckqN4WDz/z
grda/9fR1TSGvspJWOxzSze1HuFvLDAcMieHRXgg2u3jBqUmDesJ275eIQmQNauAxPlFmhFIR0hw
FeJkHD0qwweicuSEJhCtI4fznHqxSShMotKwBgcW7tv8HNRAHa7sx6Rxt5mVUVFqYQKImR/UTfcU
cLgA1PKBOd7+0gbKJP7uPKGlczKuB8OdQNSJDkeS+zE8W1k82Rr8jJ86dLsBaY8ONxwISBCaR4mz
0q8DIgs7LOkhVbDNOuCLHJlVjTaC/6jwVlnUtW0rmccDsrmz4EPAreVkhl3SraZ2I+ypkaE0A4MJ
RtJVimygBZtWC9Fb+iEIYr2iUmMgCKQqxlVOlxaN7DVrcEGU1uO4xGmlq+/h3B3Vg6mZ4WJuJ+yJ
yLkZdgWZb7Rlp7X3mezfY9Mb7VDh0Qlzt3Z9I1QkQjiDdgtOzxdaslLUcYG1t3QsZOVPSXnKsBxS
oJFovKzTCEVLg4VRTVEQleEfoL+gsANloQ9fDKZdWh6OdwMmtd9R4x/xg+mTLE1tqCRpcmfnF++x
sPdBQUE1xoVZA/ZF+x4KF4AtSHATa6GCcWVr4Ez8tCoKdvyG3EAtkeLxNZj7kenScAzGA+JMOIYC
c03q/A+FbYTVz73YlV0V+t8I9UlydlvC63pp+K2zany8l/Wr92yC57DP6dPymSeGsvKFW09zFh1x
NioMph4icLQS1f9eVfTsPPIA9tt+vLfsTWugnnjLTCYSYqCJwITfrXh7smgMDQaXA/yayCvWb3YA
QknreJUkbz8qqyKl4fiwzV4hWmOLU3lPO06RYd2Do/v289CqaiOOp/a7CIbbBc88OpiClgW9iqqS
lB6bzqG/00mXKNcIRWAxU6fkjJuqdqSeqwXsYdpM4EHownx/hOtjTRD0DuMuyklska48Y9HvZ+I1
6J3NWd8uw6UO7uDDXLNeLKCV+St268klNY9EmcJRJ+VfX4X990TsIrrVKhykF6wJqRPTTChEHme5
Sohhnrivzdo4K8VBcrWadzMhDneCI83YW15wZBA8bcSi2GT/eOzPmKD7E75phyoIQeO8BKJlUE1C
IXQlpKhfMwru+cgSVgOW+jvDWUBCI+9/SmpLyZCvPJmfZ2gC9R+1BS/kYyKWInD/fb16mhKccIep
DEO25NC4N/Jv000QVCPUQDrhI/2u1EhHGmZ8UNB+KwsJpU0INbc3PVSFG+iLAVD2kmI6iaV1CiOY
SjujjE8xDM8bhmFkrJ9S2xv0CGvIQDOBjiCrJHVLmVjb9tw/ddTyFzgkQSoVMMBn6OoLI6a5UJxy
XwyvL6+aqadL3Ek/X0rIWcvpD377a1t8kwMlAi5nVCOM4PpnKvQVA5RZmFipQuProJcJLOxdl1xU
/Llv4AmpXTXTnN0L4RBZu55V+RTV7DbwERDcHWohJ9Y+jmSkXdeYSTdF8Bzg7FpsI8QY5GfwIQL1
5hjnGpcVTrrl2eurR6O/L3dtR0m+nk3nqG/2FIoGo4YCLZvBYuVmoBf18JaGgw12o5XjADucuV1L
MLqdXK4Xs5kSUMpxsco2NOFbC9/Cx1R0U3l+mROMiMXBl9ufJ0ypyc8XvEy2PFSQEj/nK83uuZ48
ySC/LymdU5QICM7McaD3ff44bAPoaAatSqGDlPZuqtJ3LdGDMiZCgFWN6+AsC503KF7B5lRAxsYm
2UC/Q6W/uzkIHBDGzZeVbyoxnAwOPB4whDAM2UrOR7nvZ20QChx3YnPBJp9R6b1zovBEsHDMhjuV
Ni+M9TEJ02ZT28/RBQ+fAZyS9b6msaIp9JHlAugZ+w0omrNJ4mJz6ICpu/8Q/kNJ9jckOosSIHfm
lR3rYE+kLkPlI/bOOr8o05l8tOx8hKZZBMD2GYfHZQYKDG3/DZjsNMGJ85amWbWW/bmv8kwRGPs6
+3NTSAYRcxszkjjt2eR/3OLDePEjeV6z+vHcFOIARcD6zXq8L4emwIqxmii+IRmXQmpgA3bkc8Q+
1P95n8SbnhJKv9/AYblJcXjXJjgxiVvZev7Q7hf5+4Hug8nGTxMS/Ey+NYSVYgkl1iEY1kIxnrzJ
HYj0TEjNer7wAdKKqyWACZHFe0usjr8VAnnEiUC6bLm4krrQgEFNb83EM814DMAOWaC+rw7b2Dqf
8sMbrVmy3xGK1cGMxSDC5yGjb3qpoCbNzdEV9pPDFxsROpY6MQN2KwaZ++DTBzu89yHXp7fOfftz
9Jm4vnK1mt1+bLdsp+jC2IHtqJBYlYCY7Xit3vG3RbhrHjhK1ZsdehE215YPoaHIDflZEemIFXWO
xu9LR/VPTAEfz5Ac86T8o8cJNsN//OmVEo+NHOEU62E8HWMvUP12TRw9E52Jlla+OgZldDErQ+xs
lO28b9DFZRw/TQNQZQi9j/q2U4AvIReGUKqqzbZYdE7fHEb1gRsthn07Xo4SzfQdQc01Kct71nZA
NNWxybHlm4AyZo4R9VaoVHq+flwEC//j8DpsdCLIm5DtfDVXE5DMiuQ6FVKgbGdZ6B04cl/1QS3v
TO2WnCrmRRYub89bvph4QfNbn4NRExHTQoeXlAoGzrju8q//PW6zNBJAiYwpkFocqGABr3XrJTO6
vXEFGzwYNs8d5hBoJYsAB4XSJY7hU9Hqv3/XauiXkoGQS4znFPxpC3XkuAIsKtNSUHkp9MWdbqdK
rriTe4o4KmmoUJ2AbTkZgSXHMAG3PPJxbo8MSOpv/s8fOC6OMDuJTEf9i4BE7QikqGMx1f8nK3aq
UboLZrSo+D7dzvpHohX96lBmHEXPKOQk7Gfi3tVwj9Kwj6uPaJW+MVgMM2nEwvLc4dQbJH0iQBRZ
H5/t4fpmMLBAxQ6fcxQaAK2bSAxO1fGd4grtMlrOMd9TPWWcDA3dcSeEnozjh2uNsdncyThQEriS
tjZ/kPvV+zLhCPdwwtAwiIFXqAaMLUAnkhCWQxcmUb6jrW2QxcXjoGChwy8RGOTvq4IrjQsgCCgF
oG7ZNHWxFo8mlaoGyrmObRAQwA8MZ2Ogaihny29UZYxRS7GAd1egGu18iODIeA/GDt7nOxEvysu/
MFTiw8NFP88/bMHOqLQDf0p8bUTct11KQG6jMWiLbowou5ALreyeQdQhV1OKHEYEeCuaJlscMC2C
7Pv+VWpbabXiRJgJ2WuoekddSeZUH7VzfxfpDj5Q2FzRaB9gVoxjnJ6x+eHqkrUhZOHk8YOObQb3
QYyMKaXfrwTE6hLCdWzDpx8T+Fib/xLFJ566tyltlS+Wbb5g9fT1AXTluAtPL1M8AAFrJ0GZBRlM
N7ztylBS+uFKs281zYyiaLz1F1pVxzcrF66B/6YrCOIlBTcQZNQybNVKlrxGkRhDkMjNiAhr0/US
ISuG5xCwX17ar4rTr7C5096tn6WAxAJGG4ee124MP962hc+oJCwNUPYqLAPoQXTzc1hHYPe+PCIo
zrrTqnMXuHKPhE/EWPkZrAUTgqGaf3/yHpp6bLwyUg0DTgCu5N4FNAGRoEHZ1jLwZC1x8I3l/ySY
Mm1j7MREy83q6cO1nGVnniUrPgKJzkA55C/dYLaVMhZmscua9sG6KOx9KrBXuACD+uUeEDGHvIei
56ikg7KDTmCt21Zhlg/Uwz1HM+NhaiJIuekHEg850UU5LSqReOXwstoja32afRv85/PvqSmKUYFd
cCvsTrkH3JbwqRT420/v4zokvRrd+isiM6s/zmqpkck6jnSNxLoRPOkdWvHL1FeObnLbl3L5sOCe
MJkFlBZf7wOoD5wr0FWxscIgTYorwQYH2n/I1bTy4iz0baDroUFx0z7zl/nnBiHVT/h908SId+j5
M4XKIgIjxD6j+Sh6TfLtHV3EvqOPt4KPJQadQUo8Ul/4oXrW6HuGC/zAf/dg7RHRNBVyGjuasKQG
PzJ3EWPGlth/HQfNAI8kXVABGjMtQi8aocyoaQh1J9fI5Xewz0jpQyVGNQQWtjDg3Bv0FHvEo0j9
d7EtzcwteBN0jAiKdUs9pmh7HH0j8tk8SZ/yiIHRz1M1DrsQzHu3k/xWGy2Mz6DCpn/gYpYy8ELw
dRz3vHtYBsPqvgm1KHyVqtO3XgZwQpg2qVkpxadjQS1bX2pgzCFpv7C0c0YjlXU9ohXoWq4NoFLq
nAmqKpgj5g02vbUm9io68wK4PsmnRALeyF7a957ZzuyBj4yYu+8W5Y2X/isgnN7OvizLOKaNET/r
WB3rAnDgK3yehfcAevB1PFT8EEjB5FeHA+R3w/S7Lh5pz1R+aQsI38icRgX/HQFFf5XE8Iyb+SVG
5aqMJNxTuvQ2gTVpOKeKA9MBOStg/sDKusWINXMYlpUT941YjPz5pg6jGt8858CG9JXD7brmuN/x
NDB+mUQsWdnXUtM1XBEVvPjSSOdkHD2Oneg9a08ZD2VBjqj9wr2b6CYAlRepo69Tglph7Uc7LwhD
93o7Rp+3NPrqb266vNBJebE7i84q+pG/kj2CKIoej92Kg/9WEgmmSY1tW56H5qGJT/u9YIgtqkLA
DXoCsLaFqlio8rtItNAX0rJgGeZlxkuDAfaX//JUjlK5nKtS8jNbOrDlpZxoxDCeZjT+iebu+jLg
OLt3L0Cr4+AQc+fRAMKWJcLP0Dx1Q0ywJ1oqvoX88p5vyaehl0Rw3CU9cRimRxCe7S1wlk8Oa/fE
Iw1Kb/ssHUFKWNkjblrE9VpPDk12crB4cq8GcCgJlODvmquRlgprc35KJHpMZKH4K5/jJ0dYqrdw
yQw5eu5bSbJWp7sWZp7VIgY4Vpcl4bhDMnU9zhYcm7iDJ7UmF5vqZv+TDFAait304uDtfnrfN7AD
28FJYFLBlnqzY+GGgmvqGsc0Zd7+ABShRVaM5skGB4BMtlAu5qpPHqFJp4iO23SG7CuqfYyVTvlF
GvoqvgmJ0a/f7fk2T8mQZg8j+yOODb16y2kmiumwjPCzoxO7UDdqEmzZcgPG1Asf369U8fwMdbXQ
HapDDkKG/E5XxXb6Xp8io/snPlQGj8O++xCVaWnSCIeLYevO1PeFoGUtfXNWu1ae4wh1vh1uKC+C
UScuK2w9Bqo1p825B3Rj9YhnYD2yp3qpGN31DQwnmWFnCocFw5ppEJstYiMGohookLdbJEo5Mipu
zBna5MjgXsjPYCxg+QJy+g+Ls80YPdeKiWdOPvSAs2UL0aVg15B7d8O37o4UVDEHqNCiK+cS1xId
T/eAR4mP3PW94C7fpVzLkLm0smIdSxSj+VpPPhz33rLWercfswC5I7sLb0YDrDppwXqShIW+e8UZ
D4WqpblVmL+mE//XTxHdUejl5/JtHGg+Ki1AADplzdCRAAVaQby8bJ62we11O+69FrLirKZsB5bV
z48w1rij0wUn98xW+l3BwLvIZMfmOXXmhKAKf/P6F0SxL8KM2EhAegB2nCapv9lkIhski9/t5qsW
a1/5F3/YHEsVJk3csnJA84MfG+K66z/sU/sQAdKB7wR1VChfFl8f0u/i/oHeYe72+H71YRSjR5hZ
CER58x3nX9iqQQJaairtqJIMvWqbFc9fb83l23DhaFMyRy4WcD7mtOGdNpmwfqs6H74ZY2VgQota
OLV7meOgMF5beQcWNZD/E0o5v2e6TeDv+HCVDXvdYtyogwGIhDrgcIBReryiBNW8/rSE88ZE/zbW
ge5B07f4lYGpGog2y03gHB7Z4gB9NhQzbljbh7v5yP6r1LWnHg9QF6qJuEo+Y/DvYR+6zZtzJ/uI
y3ze1foW4SJHjH8Y0NaBi10+i6EusYLs2oG9H9fw29RM6KUsLyK1LtLj3rNmUAFU5OV/FREuJvtW
UgqEdRatbw/6WHeJjx8plqcC5dOVEKXfIN5YaWSC9ofoooVq0JvnOSBUGC9E2OUZ995vLcnrFJUU
5q6u6j8am40qU60NgYioj9MzrsHEJwK1fHxltiMvEEDjIBAGMQj2VDjoiePipJhfIf+XIBmdS3hn
FYhgFEuvyDFnfpZ4ObTelGKYi1lcSdeXQ0a4FAWsauJJyfUHhLRsGpc84uAgr2ZpxNvrug4Vn9iC
l8HhUDKPc4eWasxoh4XLsCotUC5wP2pdQfAlIxCTTDbaomZ4XTY/WqbFJ3lnlTFbuqbXze2GqbAr
I1amuGDfhdNi3w1cDinsWUIHJ4krg7ASTNBZdProzYWJxj5N7XRmz9bXLBf1a4XprCmo+IFBTcDi
BO0Pl3To6RaDUUqaj3tvDYi+9aHBNCp1QrwazGhrWQml07J38BRVLSd5guVq7nusaDba9kPnmyKi
h2aHPKBuOv4DT2aVK+6PSi83y7D8pjBmXLRu+9AsTS+0Zc/xfDEgWaWsicuR6dWsqYfvIJ7LN1sW
v3Qt62McMCbvibBwgvcFprU1TJXqbW1hz2R30JkdM/gvl2do3vVL7UAJOHVMLONfqrO3qqRz01hi
OiIeEqboLbFBO/1ruMPztvvfbR+8fXtnqX6CJtiQmnLmdIv6tPQvdOvrj/inUlLQSpzDWi9GqgW9
Li4nYkuz9nP89qiRislzGegbk3D6WL/gbsxxZM1SuigM6GQqeexY3QsvksJxyfi8hKwwsB9i/DDg
gl8UyuvrJ7tbw4lAUigIq0EJ7PnLChFNmmhlCGvgfHxnpidyqj5C6JDB5pAwSKC7BJEV3CDrZRJG
ijz6jzZAqXtS2G1z5ddxb/Rx0oX/GuuMHyO/FR47woWtjYWxnTEVn0ymT+VXhHQUMbZW9qRp94Ho
dPinJJLYJINq+AcyT7MQvIPfqQHODa1LwjwjsYYbOtGwY9mwGg7DZxKgNsB9HGfK9HSCELt19XGH
yudk3CPPWi376GmurMZW3gJtPZ6i5UOQVZzI+YbVQlqRbBHNd+56QrPoeKC/6h82Wo0GAAPPhOTm
FR9Wkdpuu8VK5TnAPzhMHmKYeV6I5Mobi+o4ul2FvcrPvzw/hAh8nF3Fthjmqq8kN9ezXoCwMBwN
BXyM74Gu4UHyaoe+5L3gL9dN+32FdH4GyZDU9GI8NQU0dL75kjlOq2mXDffNUNiQX8OU97NoLtQN
4aZ0Eb2TbnrW0WmVQ2bBXEO2wqsKlcI938oIzDS8jRiiLlw7uDLzWpelF4G1OkNtf1hzhnsSOok4
n8DOzowckle7walns+898FdmqnFsKtVZ18IPFltZMZGU4epV2+sr4Y/sCygtm17RkPD5uFv9wfov
vvPZWgj1l5RRBuFcZ1AyjWDPYSNIC2KJLpiKwPFdAVWKOqnnZN/YXPbX6On+emNHakRysBDyWkxG
vKdAw4LHg5DFdjS/gWQkV7gDMdowgwCnh24YrfwwlymJTmR+zk09riiQ7rGe6f1eMsmC283CR8a8
aHUTNI4CeR+JlKQjTaaxj//V5Y5ksjxGWaKTufjCpzLdqzk/602gahcFB3/1C0oTUSDaXHuFr+CC
T+IyqSxaaG+ydJFNMaTmENlh2Z7UZENxK8hVt3T1za8/qiMiZMV1cXE3nth2lvuUVWxWyUcFSAx2
FJ73G9YwxJqxaSmeo/SRW6++x6Zit8ODx7XRhlw6uIMsbiB1edtBB2VwtD1Pem5i3wtL4wxUouuI
iy+tFMpqdX124uhraWayuQpM1vXjH7htS0LdA/bD3KLvVljPdOcHzd6NUoPqk/YO0f2T1M8K9FGl
fTPLHNE9rc7Ka/SZRsW1i5E4by4fD2g85HVIbnBfYe0El7kmEotn7LB7RcIrpD9fueea1/oBZsWw
Ea2rtH0cMWZ6tV1rmUaFUifDxYRE6RM4mx7Kw7jjW+9+LR1Ni4zMNAg16lyV59eyyhB+Zu6tUg4J
UTKTbWkIwISpUMEBMFXOFB0M9QlY1y4a/EpsPg0iHslJxZLTXIhmWZZummhnq/ngsromdI361Aen
DoK9uxvrofB098iSES+4NX/j0KTwlnFfdPSzLdoG0y9YxIQDuz+tPunxT6rtH5gEQSTVOtCxOfOc
gsN8pEAgXBFzqahSlDmbJtocrCOQauklhezm+7nzZanQjVz7QU6mTELUT+jLdFxG/m6CbSj4LlSx
c49NIIhdS0JIEsxrJXY1qJBJYttgv8rNyGqJlGJAKdYMvuzCEMduSWVgPhdWM8Sq7UBGiPgdtxdp
CIIn8iqK9XgFgxMbV1MaQm5XhQG03vmdbCtweeX5tcIhW7qKvWWSosjM8jeNGYOLkXZqZPzppIoy
x504m/9ax9pBQrD9OsXZOV9M+Og4RvAHTjac1w/R1pF9pD/8pF6NcnycAxJVycQN5iz7G/BiHya9
1dtHbw5FQkdgY10LF89WP7NNhKvS11zzKF5ShySysdMGBVTN5uVHwOXOIRaJkrIc6mlyZONBoc30
HEQg1ih3DTpbSdjkO/9ROmkKT6U61D2EL9P/7k3Jg4C0rROoAOW61YbIlCx3MC3M0t1WKajBmo/B
VRRscX8VzBRK7FSSaAHsaSZJPv1gm+Euw1zCYPF+rOj2IX9CrIVy5lxFgA+kflxlzbttr+mJhyTA
AhUSsuBun+eTDVbLHUxLcO+hL1tETjrx/3wO3oO50gXpPEz4V39inQcGcaYClaK/KDIUmslp++vF
oPkCR+YpvOzMCdP5BpvdNogSXcIw/YJPjfLRiwCeme/6c8M/okGTY0AZl9WFDa3gKBAYpEa96xPW
6V383Kv4UWjnogWJ787y2j5NUp9ouy/7jfWYTAWXV/0uyxWXUnh2lD82RBchXexnA6X+hN/O1Tvt
wfp7M1p65nCQ5jIUU/9FxmUxgrmy+OJ04S7LLcZZPgdDbT0cBpMJ8zpP3PBydi6Vl+19rDKu2be4
LgBfSsYbIHHrjUcZ8JTv/YIqaIPNWaecbapXchNXsgFpFI4q/DU2oKuMxTw1VxCDD7i3FIEMmrlG
tMJX8EQ+lNoPMhhD8vl3CBENqaDerBqo15PCkf09sUTbn0zIaW8VFALFA12mGSDj5LLvjjDXV1v/
+10lx35OcrMe1QrodgKCkUJayMzuA/P2SSbu1EbxBE4aYRmmb2afGivt9RFgdQq6xT/2u3n8BVqM
060GF19kQwIg+5eoOpOMMra+/doc4OZ+kTxjMOQHbKRJcbSs7vsloxWVXU4KdC8qQtZ+gHIW+ZhN
gsyZy81rqT0hMhox/AD5/apEhOVN3XtCWsIGCgFCt0U+4Cz79WDdUujgHi0tXqGlecf3E0dCmbe9
RiyOqrbwzbJHUA2iyUJY1yuXPq7Vcim7+/+wAs+T1Fc4iMGXm7lCW6vQ9+9iZ2UR3aCLPmq3i1Fi
4bSyTVN5x1zSlBk4et70mQVWMEVN8TwNsicba/qq9lNumvDdI71swjsBA73+2pHxZ2IrnQgZWvHe
MZqNjkAXp5vtVy4+rjdpjZbv6/wP8ZAiNu1+hMV0jPY+7EspBwpFqE80kjl+O/WDmB9JJncuUpjF
nnAxX6grCFkz3laZ4mGaAz9Ssr2hh+X4wGAT7JEwirWmVIiD61g1A8pYLwKYvmCuLzD4HM04uvZD
Ozq0MMfIDHo7q4CgrMFmBWw51lVL5IfTrNIejyD0cTdurMoGfpITPbqmoP/seOWK66Imh6eUpv7e
aU8lCWFaCGzbmXYNHNH9MwCpSlT1OBhjSGXLcf5NnB+D+DivZ5qCouwtGqqZRfEs41tYh7Ruojuv
4F079vv55dyI6rICis9NrsDEHWiGcOmhMN3U2MSTI/VKLslwhqRh/AO1MkAqQpC6bq+W9HXBdRRV
QfyXOPhWgf02lu1AYSxBrvio+8LC81nOAyGyxswe6hy6g3S7CKOfy3Swr6mABWlE2svdHKLWY/SQ
SopTUH6ODldLqSFXMkZW7M0ei8NJf2krbn7bP/zqqXZtoLEpOfQAZT+HBUGYyoPIPFbBIm7VjPgj
JCIBjnWZXl/vJqaZ2OF0nHqw2HOIq6D7/cG00RJVlfGGMBPpnTrcjwLUfOVNrywg1ZtMgINtBUgT
9dqk06WGLZHP3MX+A3jNie/IY57OmMsCVxmVotXN7g4g2enKlT6EsekKQEIv4m+KDpx7yrc2bS13
9UudeyMyGcraGvmJ0jVFNl1CVf/t3A1dwv3VpaJ4QdeJq/t1HPQD2wsLTRMKW+UrsHey7ja/jQKi
QNrUFN0q5Vd48cp16T5WQl4aJ9AqBHixpgT7CO106sCajse2IVWoQh4VpsozNp5lGHlWIOfZIfMH
L43osVteuHsRDy8Xfzi4r1GQfQtO8Y5AeE0U5Myq+DE/ieroHfC5ZcqwNqWHxwleA93rikTGzGRT
rtoqKsyHDcXK+FRSzis/mERPnlhbafe1EpCnhm8V14w+MbG8ATz/sygexuXHMNSztirvBppRtuEF
3t4ncFxZNu01fKZ84ucCLFrFDtqZRLsloUEZcJLDswrY6Ah/4WpJRR2SV5efSaF2BI/I1ui0TUZO
PoZVYWmmQGUHS9XJxW7qYRHMJOtZNZzHaoyvo+LQ/26R/X8K8S79YzQ15WQxZOHYtLjjeV0/7ook
njunm8HRYS+PaxNNE/aeqKAiCkUUdblEStZ42yGZKsKK8egET0NDXwKpcdH/eNve9tumDDeK1pPl
swcyTNPZnvgXE+DZoqcgEIoF+0O/i0DprvRB2C+gN5Wzq/GLGMnUt1h1BD7YrzPjS/w8U6QoNCpY
RbB6JboATzPHrUiaspSlh8FQmcu76CNIPonQs9GGzFq9hp9oHKGv0I056MsGA3gFK09tiqAKjMzz
EfcDfk1P7vG0GT5QSJ6zXPBZA87QB/oeaN2QKsT8uynXEdeJpQcvhvi6H5XO9ps/xpPoHN601XC7
NGuMkWVFhGPRU9ourpOAncpwz+I5Ik7gVikfpEAxFalX4BR9LHdsTH5mM4He0JBeQ+Io2FqKrlsv
y1Ek7WvMtYaQyasnf+CXcRW8h89kB+VgFy1top/PnxzrAlKnA7+zFi1C1NCEBENkSElPWq39EAL1
sbK04hnjKHNA3+I2sqkF4I/khnKsGbIoUfqFvryu1Lrl1oB3a5DSxI+lH1OHG5HYw2avzOLMCKm9
oI6wOeAcMSnmdXWiW+ICairztfuv/QCIbFYMtvnabPI4b5wEyeGh8eqzBcsB56aNqmb6t6f0/igC
ZPb27V5MYihpB5pZmojXPg38UR735AMbuwd8wjD65JtbM88XRp1G0MuT2piXrt/MPCVe/geYLEGc
F4k20yZLYkUU3X5NT66EsPLQxZk0Q420lDJdRjJJ/ylklicWZgzIcDNjH3I9b916B+ei/tP3b+Fl
TiYB3rwyjJorCYDBUz2jHEtFhPkTHNJuK1vKuTmdmI4qlQ+BJ6C5tv/lfDaqzA+OZf1fQbe0j/fX
Fb2Mbit3gxGHyMoIs97GqYHjkNtCCT1MQ+kMemPM9b4zUBJkor0T7isK/Q/AGfXvwK6JN7270T6b
Hoa8P7w7o3eDXaDEITzY2k+OHNfu8Khq+6Pu9lyqUsNxurdWCMwe3atRfjkXNbB1Kf2VbDWa8yUx
/XXqpFWU813oECgXpMwY9Nr7WPr/wpfVNZ0mX/YuPvSmsNTmDXYR+zUwf5SLQB9/gAnF2oFJUFY0
9lRE5D1zaEXtEohU9ehtatnSZKDJzRLnZuNqZh6c54yUaNdgikiVA+Ap65WirEgmQKAHW882PPje
lTsJLD7RUWPjLF60h8z+ejAp0eoh/GowZpyEx4K6RqE9Q55DbYuEXiwYa3TtzhfX+slLYW+WXh01
0AUF+irQhXwMwdI6KGQW1uiM9T6kkz1Y+Jj7+pWTMgl67e3z0qtUeaJhtgGPGvQAhNuDtkgQZERv
sHQIVdz9nKrXzKbP69BAgR0aLFpw1G6YR8ULcWyeYvUvLLUy1iyFyVaaucseBHHTwZcC0CFLKMY2
IezqyjBIL/sS7zq+uO4KU4/RA0A2UyK4EZb8dt6MkQYjQ6LH+bRCcpseA3VhSqnZiG/GYQiBp/KF
Yh5MjeqTpdFM64tJ7GrVIDf49YyEeO9ggr5vnVEQ1jTwx6x21W4B8lZyxHuXFr95qRnMv2aRsUPo
+Yeh/jxOyTaQyc/4AgTwAtXB5PgaWUZk7Ge9egOm5BSKYtV6wjXCYcbx12hMOUQ7lZZK9/yOn9ss
XrhLzIG69N5pUBfIckXgTnLsE1V0P32E08VPGZCXgCBxg/GpTlpwljVkW3/iDcI40v5vaIGv/z6l
SLwY+C1qAH6evd1fsa+WdkaGXSYHCHMFV95110vWe08AUX2Roq3GdwUyFw9ACiIj3R0GLysndCn8
AqpjH2guudx4qEnwYlRMF7Mji3ICZujiLXcx/my4mJ9CL3ncX8BfhKOSRPqWfLR2+cBYD4XIXmVD
u+hNSBCMSI3D6TK+sDWRsG/4RekeLDKUoUeMFqXpDI3PO6sD6jNx485zUg5CFHnohGfH/mjfdVmu
RR3lisEwtsc3/t8MDZTL6wKB1zkovyQHdP0GthLlnXLDAq24WYMJ1FF2T+TGpZm5IRyXt6XjZk9Q
NV6pwZdp8yt/N6AnLIMTYWCNVH4Z8pUjC4jlsI4qHd4EmURoPGhoeCcWqxdOiMCsEkj/PH4TfJnp
9UrtypzYcpcST+QV5DD448O5azurbLlYejz/C7wWBLEi/mJ+9NpeefV1BVyM3BNVqDf9F7WyNs9i
/iDFlIyLu4UsmP2lcmazlVxF1YYU3+jwiiCNWQ6ZzXAcnPRS+Kpw7W+uRs8XWJuqXPglOYxhKX0p
rpX4xMbseUphiKz0fXKoi3lsdN9+kvNMM08JD+hTwf4ygu2eO6iSjv1AlBgsDe4fDZVf4G6vacAo
2odhubJHRfgjbfdZ+0pC6r2GNqQiUqCJ1U4uw/2dQdkfdAvEMvNic2T6eDRjha/Ez4DRJnzOFBl+
9jTRfrutN8cSqhnbdl8iSBf2X0aEV996s62QdftyW8xPPYVDeb3qvWaQeq/Qcvj4epaF6rFQj4gm
ptztvx9FMXpz+cK8cmCyPFk/VBX5Q2QzNqiZM5JfDjnxk5KDHMSRxT1OawS6zdkGZeiNdiymira8
FWf1aX+0whqk/d9pI9A81bSyfEhnEoBeE25qzum2RHQhMfmkq0OUD9N+enGIqSmi1EYe2JMpmgV7
9fOvehCYZPEHNEnjZr5Si0qwEzj9ucZ6v7DUIQYWUi1JFoZ9/MtZ11TqZbs58CJLi/l7QzN0twD3
Oz/IpM2rfRN+xRorZpoqVy/ADtHWtnFOxvPR8lXBeicxtCksS1NyR/LNsBgzq9+mc0txKI9xBfI/
BZvgEAu0J6vtF+/Z8sbbKIHGkYtIrh+l1FNznWxBtFen90uMQqsqH2KXWHeYdt/rTbX3XidqZbXd
2LRKmbdXLpYsSGUwg8VEANdUfowc8J9VBMYVE9FxfLhGxfpFWsiQysvZ3sBy6TFwI5/5A/oPZ+Ut
Wl3k+YZ3fgXyMn2OaLEeKcf3wVXSChaLHf5kzpkdJpXxN7Q6RNWcGNLc76pjrK+TwitjizrR28q1
vNjVVt6fvQxzu8SofiSDC5i6ItRCQ459vhYZKvCXb4nXjg1TcXWn9BRMNuHbkt1esjPySuBx7BF4
MdVX44hBscvOfwymvsH/L3SXjpUNbW53MMjhzbZvzicyqJLgFklgfDhmL9pmofxsbCwtp9VPLnKn
b4tPhbrwRsrJrAjxd+ExGmz6+vc2Mqnt7KRq/j0UcmhISjlOX2dpRXvDcF9dMVj4CcWEzhlCeH6l
JCGRQ2NVRpMsR7DET6MV/o4nEoGXiEebS+jprxIENbOY7j8GtE6uSBFo4wTxhDiPLqARzN9xd8yG
aKJKAAiHRxYtl++/iHBkms2FneG7FWd/KigBszE53DeemPmvLTsgnmtL0xuWt6nbwWoJv/GN6rnK
5aEB4QOy/s5bn9Sqx562l1/ZK3sPxX103YB8g4ePDbFVfBYCsN/H1Jso/Qd0hK8kWo+HnXmzFD2y
PUaXkCqDw0Yn9FWC5F75ZCzZ6Qw5fNZH3ythWU5XOUdHCwgcxGBLSXhg7ctsWl2tLODPXlfeFi5d
ULuKxHaSosng8jU1V9Tf4xK/wxMwnyI+tjsPR73oqpL+T7CbPFl03yr9CcW5odHWQLvWRGM6wV1d
ewdWgsxfbWryTcJJy8SWX19IKFXfk976TAC1zRgVj72wXokKQLj52w6of3qG+mM8lopYZwt1Zxw5
94ETMD34FHaz9ZaKTGQ8svL+gxjQATJpP7ZvQ7yPiPNg9c0Ee1NHJuwiEKXxPnCqDhzdpwky449o
EvaP7CbhHu+f1quVaduQjUvBq/vHLdNGTypJv1pKLvdwlWNhXyGQIpBFx4bDSCIquYh7WjLbLIjT
RvPZzMZgz2Im/sDTmwKQmclmwig3KOMYfGqGm4oPWDnxfCF9ZUpC47Ga+AD3vAB0Ld5giqiGi8mq
f7tn8fO7nq4DCJlPx2oa7KEItByawXqtLvX4wc7NCdLOVt0+OOLSkAG5QiYndBOBgc/47L6oAFi2
v+RVcKgMbkuprcMzgsIpCFtqVRms40Ou/+GNs0n8gyc5N+VobBhIXM1MeV795XGPHdqNj0UBRtNs
vfUkJHjGEOv/o6A/VVEDkYieAGfkJ8ZKpEydbdoywVB2EjZlEakwD8sDc2pLybe5Nwsr1E5XqbCr
NmExMsM4OK80QjyY1Nj0bdkNM7tuZym+CaFJYUoqD1+6DhIBsf52Xjg6n9MaOki+8Tqug7WnuEkA
bkJiCQvwnmud79L3o0tUFleFRUv+ZqEVAztjM/rHD2nNILj4C3FY2M4fdSDG/4YnBx2KZeba5yIi
5ic28KSdvtgJuVl/pRp91+659kp5j/vOxMnaaeX1p6oMgtbmBeus9f9PmB+K6+O8Pd40kjRHis3c
yZr4VN1skktb/BJbBv6HHOdotjHBp5nVht24Tlvl5yU/zBtNSDyF4ByPlH5GIly+rVei4IMqlKLj
s3euq5Agb1uPsz2RlbgEgfX6lvuBMs4KWGAicDqSAGh3lb9oVDf+Yb1YC5M0Xd+Yk8U7xiINTQ57
7jl0kzTYwVWxWm8+Crj0Sz2/1cfH1MHb1X5kJ6/Vv1gIzZvQKgQ0BpTptQ3m1+xxdf6VVBPXbzrb
lL8I/wi1Up+iX3ASqvbOU4o+kaMrtpsEtPrw/wkCquMmsm+ejV/nKzj3lc5wgHLM/P4EfbNPgHmH
R6w7vLLsbuFPForn2ILSsn44Y700JV3zDSjZH9b/bzcgBob8eGUzwWVGmyWwaLLp6x+cyWfG/JgG
ZXpy7PiYXE5OcfVVpXrYU+1WCMggv9g+hhk8/cH6cNogi24bDLk8PkQq4YTnl7hyuuGSzzI1ZgG2
4vH4BC/Vmcu3e+mLAuF976WhIxI3WPT94J+RWeJ6hHajNt4CaYDkrvaFEDB9ZOD+PLmrsktnFlix
WOgY0UvHKZSV+2c5XtqdM5yyN6GbtShb/2nzvUJ70yRhAyHAtzZnPGo4OvG3YyK8ngXDkXI3nElL
Y9de+PfPEPhYrNqRHeY74nmC48EnkZF8Rf/+Uwk7M0CaEa26B3l9MrAhVhc6rS9I5IO7s5RVu5BE
bNOudzJB/0D4LovBR6Ga07VxbGwL/VfUxji459YYCz8pcjaZzSM+PjYiToAAR037E9tkth4k1X6k
bdEIzdQSaRRF6ZXSvyUCDwdwLCU0ZNlic1MF5pqdQtTlAZ0EuPyW4P4n9NWDYtj+Dgi+vhiFBXrm
wfPWZFhGizy9WZfnGsMY0ftoVv3M5Q3+N6Wne/jUCibBwNl4OfkGybRmHpW+CVWDVL3XCszl/18b
7vpl250EyCTW1pyTxRvBYwez63O5GI84VdAGyQ3Kh5bnuc22TKEdr/8pKuKG2+7hCuOqXdrwnha+
qN74yFs/i3GiD3aK5SryB2PxLzCVDD1SsswIeHHSo//5DRCSueJnlDcD6RAaeAEtffFHnlF26LMb
Hq5EMkQl64GE6mdupYNMXJC2kvrhwnu7uKtWvCqowO0beMzZi4lZmFJj6iM+z4RT8tpbcLyM7rrh
w5YpZ/HYIF06ja2qGUrgkT5QmnyaO9aRHANXvqCQ3dwQ3CkGuLYB8nzuAUNpoS4WbCJZOkWhZYAj
9dfrHD/ZqFGflzrJLQcG3qxG+oowWowpuwWNbhtPRubK1dzZxga1cl0LuiAhO8b3JbS6wuq9yPK2
maqRuxNApkt5VAQ7rHIIaCxj1C9jJec7rMimy2Wxj0khRm6ytsw+O4RcJrfHzkwK/5zLeb2+bmpK
/LS4Dc6PlURZFtJUMHVJhJjmUzhlG+MOmlyEGZbamQPvRtG086gD2H/eFfHgoMgL5wMAMIm4+Rxa
/AUgUMLpujzl6/l7Hw991Esdv31dNo8C9a8yMw/6diX15QKDiTPSQjaNCUAxtMrZBSkjSwjMxy8X
TOdy8AtzeU9+QXYGq6cyuOceMmOna9kFF7VLPCcwPf6NT2PbWgkOc9JY7caYTmMl44YYD7s+I7YT
GlFg17WxqplGWdTDYX9C4WZINEmrTxykZd5nrKRWB2n3PIY0aq/S50co6tYfrse/8ddKmjVkRT8t
zjAHmH7Bils7wvv3aJk4+deL0kdLlMQpHOZ4yn7tKyr7ks7PS2VhTmhcMNhYVN/pE0BAE3e0q4FV
h4VXDqQnv/j0wUHcQfjsIOIiAUjNxnEFT52xXAySiXJUkCChKo30RBJl6np1+XrVtGF/lv1BekbP
NlwQm7wr1huA78/G/5Y1M6VUSshDNEGohBJWKMGesUqlNOKMaF4kNaKK6dojfPfXbDsX9QTiEcrD
UbY+dexcpM121Zi8w2NsXkfTaJy8sJqb7hTAUkuMO5i3Abjtk/z4q5GQWE8u8lo1I0bpQfB6KkaJ
oVh1Eu8sgz1JdimN+wFGeMVgPaPa6KWTV78FeI0EbJgY9qrF4JQ7tBjsrqPjxdiUgpwP/6QCZLtz
+CCWbqBDyXxz6alv6kHCsAyz/i4vQcy9hOrA7PyCQTA3U7f6CBtUDUwdMOGm9TH64C1sowMtEsno
qPeGVjcc8IfFeefR0qLisI8o57e3/AShZ9+MR8buKdAH06wAxe08IqdYrXO7iIiDn97RKSimKHu4
Kt7a1fiPCbk1Ui7B8TdMFBinz96G5NWmiKAgUgpLu5ozUlsOBAUh6LMPYOc3bw2Jg5qwCsg9uBBo
z7G4gHAJEewWd54ViDVqHV9RlWEXdyN8y0AaY4AAoKJ6VgJWxoiNuUXfOuBozrLKqEIgsufD/Bb0
s8pfj7xfV8uJ6g3N4EV/jpsHWWZdIKjKoOfTQF9yZIwPVPXc/pTenkXuo7DkgxGehey5fCHO/z2Z
/atwwrx+Ly9iE7ofWlvDwAEKQbrwfB/FbZxyotngLSxcxVuMoI3iiTvsTJUINAQF7V/eR9LLiTAd
SLVlMM77JNz3J0WO9o86f4g5sfegDs9grVsoiIDSK5S2re1HedmR3+TxDr5nlpVBxAh9f5awGJEt
b/aF7Ap3FN3hGur9tFQghMwd8giEr7SEkeIosyXbgKtlopjKPTPt/8ziCJVg9GgSGwOR/pM2jUX7
RXAC6xpEm6Ai9hBxh2RyrCuz2GuRoEHYCvXGQfrqAt/Pqh/+asPyjTHO6ZcjXq7kPAPhg6lfjsQN
VPasaW9Z0x2qFF1DwILwHYQlUraniKzbtOwhZ6HcIVySzyT3ElAsgRAvpHhDtxGEB1OAj/sOkT6k
5unwD6gj9ZBnEujGLBySI4TAp6gOPsr1GRf8gQCq3ETuvn3tGmOiWNyEQcVFL5xr206SaiOEHODX
tppCMwd4wqViOKgpwyCSwbe77PWQMeXnyuJufhDHMgMcgkig7hNPQXa4KTe5pMt1sNYk6eJlzxB+
HVUlN4XRo1kiNh4Fa4tJdRrgMvLKZg6SyTKK6+jMqvN4V2KiGxzCdWRczwjsWzpjmuRXwDUnovjA
I6yzFuxzx1g3OmtPWvvKLG3gxg3OoilCiENteSLDnzzTVGyZ473FYupPT8kiSaw4xyzRwS1wQO8r
UxsuV3tiQQig448qrwJcmLBBjaQoDn5iOsBDcRBpy1wmVqED7FwbnwXosXyXVFLRafSaem9lA5IB
t0asXW1EBu8MmxZ/rxclYe97RDmgJAIWDu4YWI2iiyEYJn1jwfISigSmKVDC6sK2GiGzZF5HB0Md
LyvZHOndFuflXZsIG/xABP2A2l3UeBd4/uMFEww3CrBYe07Y0XogIyLYKUIdmxYBZ6iWBZlmL0Gp
uCxzC2r9lqZGIArQpBA6VugxuqUWIeV6EhG6N37pqUGFLVHhn0iS3xtu7E87JizjEDQCmtaRZ2Lr
VpDoo9J0GKcuXuvorGIZ7zATiGZZO1wIYfa/h+YKaCVrh5TPzelO/M/rNXP55kqaDpMGwfhSHGqW
Ig/kn2S2GtKRjBFckZv07SaN0YK+EtDmsuMqvVMoTk7P8R3C0Ut0ZWtGxxEUf+dsfxFZe+d2ZRa/
aaUD0+7J13zAJdigV4FJufabksRdRQ96W6b7qDENOePuveW/0l/sNSp4keNvbdpiLPGeA9W5ASLh
R5MgJxtjfjiS1mgRYC714H66XrOn2g2m7yGjum1c28f6+lJXZkz+fozx6G7l8PLXy2teJMX8jVju
QthohbpNpbAJbrFJybu1Zljx4ut5EVlo3Jg5SYI0mlVvqZ7dHk1NejRAg98t+xZeXoBNExewfcZS
ejrKKX2di4O2TEXklznXfijjTQ1jVxYm0e8Qr+UjAXrmDG7HkrtCjWdToiQsj+mmyPN49Ups7aCN
0BWLy2XMpJ6aRugOu6GpBR4PBxTNW+PNwKEHjSBprdZlUUwPKDdNzazL0ft/NDRNgn0xvtbOPLma
SypJHBV95zl04aokCdTaVuyipwWStxgTtf6AmADqkSGb2/pDtGBs2e7LXEZAf+gwMKJFHcF1qFaS
tXU5XtbPTucTBpbxIPGWYSnpllLo/SSE9jlJ+MsnBaafKEYJ+lSZtxIkTX+7SYO6eFi4BebY56nk
ZTPEYkC1fGeCytgasGVm6q1gPiS0fH1XYImyss9fJCzdJ3dFh1HQvGUj+g+H8scDi2UBfgDw7hHC
g5Ih7EZ03jHP8x5yaoqZCIrIDpEBZ0ca/07xV0AcyIWKsplQfARvSqQSZtL5qCAPKBp9ZMQdzbZ/
E6dzjaaPc+KTRW1rMUEiceSt4Z9orDKCyxdFFEq/Crg+zEs+XyorKzC5GRKK+6Wc6b2kdBuEMOr5
ASZmROjnupTj06ye6hPousajkQBSQwHx51B0V2MqgMoiglyMxQpFG3CEyT+EGvREr0gHuj+DI407
H74EjMYzb8SbSwsofbvuEo9Vw6/Uc7sCHhzQezYFxO14B/XqFLiAIpsQveKXEvRilMPeSMpYH9b4
BZGZ5XnJF3x9htXcPo+J9pCCe6DuChHN31dkmzA8U4Lo+e5QPAm78Xbd9AUMHGcfwvmmqO3Nfo/e
hIVS2cnEWkbwIiBXKAtBayEdUUhDmJ/H6ejhM5HVo4WHH8VF/jQOJQhrSt0Ty5vxGwq0Sf8dJfBE
FWzYzf7RxiwC0kLfpzr15vgrAIHXiDpNAusT/ddZClgLABsWAzmkqae/zgcxC+LtZMcZ71s0Kw7V
1uGJLGeWz8jaZOo/uIEZqBUjiOIC8dUOz5M/ECMFFkVa3aelVlW/5u7g06QtDPr74u3yaMKsXQcX
J538vRUkKQOvsF0pkeQcUMewwWRhZefDlfRGa46hq0cAXVtV4vQi4kQQx+/20uHBgRHw+xNilgPr
YxZ99jyLmp10vsynh26/r8U1bmqh+SeyXtI//Lq4/pBYFf5nmPWvkpqCj9dt1bVmLIhbRECpu7Gm
vHW9gpLEV5N3LXxeyIhSSrLVTMpY6XJMUfKEChlaZea75bIOSgapnDsumpTC+PRQqzYGQFH9XiUH
iXlgHJz+y5zWa/VGOgNMx7DonUicu4UL1lKaW9IuzfVe4fqGbFsc4Lx8SKbmmkBwq6R4CkG3fACA
x/CE9u8h0j8UiZwl6feVC8KVX0LEyGPsLMll4iJDKzhB2nUQUbyL0JEyMYxXoiroOJhCDw1DDnIJ
sKHzG8udkVNmce0lftJieCkDlyQpRg8wewLz3hWxjE9Xih25YiXr4cB2YCwDmitPCVcZZoBI0BWy
qSfdW0XMq0lP/oUMyUz3bJX1hnZTnr7u7tLkj6+fGdl7sEjQF76J2vrIEWuNXLRyehhAzgwGtnrD
3osEboz1VI+dufh1tTo1KMJ8ncVStQ7ZkamF7Wo+ZbgXXPY7NIMgwltqIJBzicsU2LOoWjrUlgs6
iwSJEwbnr2uRX2+V/2rvxax+LgD0nI+17swWBZRmf4h8xXP90Ee1mDJYouuOm7XMG3WMf2xFTKMy
ey3of3Yp539XPd7zAmDWrsfivzZQSUeoSu2p9UroT+hdMdszBzcbGiA2YN4if8cg5qjkjgclI4Ms
0KF0RgShr5Qbl9615Yb/ioYtJZxcd7YTwXGmu0CLJXb+GgxM72JfyCKEoHln7Gth0m3oC16BkhFS
TXHqzz2T8XSSO6i/NwTCAwQJN9kz6loMywxKX4xRiTsZLTRWgrd2GfwcKBYS8UvBBxyRhVOgjklr
4TLlzrpGx572ijiLDWX5pqWbuX1HIW6D/KUhVs5kEnPqawKP8F5Z1BFp6mXsyeVdalq/H5j2rvkx
RYb4wuHK9L594DdPzEPRe+BpV1r35OLQLCGgrjKLNrJbDlYbmGrDgl8FHlP9UH1+aahqBgCnOpCe
GhCHNunaOiojEOYHvMjq6ZZnMIBwRlTmkcjTnwFTseMRxu8f7iBS3Y08rEi6zxPgZCZu7sLmdBa0
KJEHRjrZM1tLZ90p+EcFaC6Fiv4QZnV/woMhEY3O5hvI/ZM3z8zLYKCMRHz4aqDkoZ2Axn4PuiJ7
JEZsj3lGIHsKFonCr0J6nMdrmowXruq6ISB1nmtk1+yZCbsPqh6gXUrQEYVcakxrNo4jXHIhO/cR
/PrI/+cIvMlIWWnhAhYkkcJDIXW7kezYcg4M2GW0v44sCKU6fzrlc43FEAGg6VLjEln0gEjV1LEP
Z+Fs2o5KEreUz7mpQw7gTk+fLUvIRBTBIeKDTLBZgntsIk8u7HdMKWgz/vfdbVD431yIvBeTGuUk
j8ejB15GOu/JqWBCYTvlT9xD6YRUV+BNCg7lykcm4+Lyz+kgswfZQgqFYhLEyfABSnBL9TshnoEJ
PGFLCsK3sAhpJz3ICguIzcwzEEBruJPXiAlUNcinaTcKnp9DmOPdygsvZm+w/EGN/qPSKiyprg7I
8Brfi8kc4hOkREKxQ30twORbv4UYK8QsZFPFBRR87KRMkftGvWbSPdWv0WdsRTJF78PNFKwqaafK
wLBEHU39E+i5b7zlkXpbxO2am6aA++OlsiWzMYHu52vgmT05vYelx6c2FSPFM8JTOY+vQZi8z7D1
KvK8IHkDYyHgphp00HHXdRSmv+mLlN567CQSy8u/lFWCfJkpqjH/JdcY8f8WJW+0+o4p3rPhltSx
FjuHi4jQgavhC8YYrErNUoK1QxiP1cNDAgyO9ERUz1PpmCMqL6d3GU4YIuALPjgsU/LtjIfDQo91
hVYtHobE0TDkyp0PQexv4+pQiErhDKOETAqHS+MYeHJsdFXY7JL9riu4oZNAUUMITUA0+aJ9xyTc
mT5rrXwZcaeaOaSWs5Ho51a/Y8D8d3tLdJxqZdw6Owqdac3Gm/0jl6/z9IazPFsXrF3YsuMdh95W
dt8FnsAijMcOPg+y7Jp8pMN4gxPTNeIOTspITSCa/nY7mPIBxaBhdDsn8YwFq0Df
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_GTWIZARD_init is
  port (
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_1 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_2 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    mmcm_reset : out STD_LOGIC;
    data_in : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gtxe2_i_4 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_5 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_7 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_8 : in STD_LOGIC;
    gtxe2_i_9 : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    data_out : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_GTWIZARD_init;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_GTWIZARD_init is
  signal data0 : STD_LOGIC_VECTOR ( 13 downto 1 );
  signal gt0_cpllrefclklost_i : STD_LOGIC;
  signal gt0_cpllreset_t : STD_LOGIC;
  signal gt0_gtrxreset_in1_out : STD_LOGIC;
  signal gt0_gttxreset_in0_out : STD_LOGIC;
  signal gt0_rx_cdrlock_counter : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal \gt0_rx_cdrlock_counter0_carry__0_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__0_n_1\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__0_n_2\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__0_n_3\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_1\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_2\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_3\ : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_0 : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_1 : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_2 : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_3 : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[0]_i_2_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[13]_i_2_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[13]_i_3_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[13]_i_4_n_0\ : STD_LOGIC;
  signal gt0_rx_cdrlock_counter_0 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal gt0_rx_cdrlocked_i_1_n_0 : STD_LOGIC;
  signal gt0_rx_cdrlocked_reg_n_0 : STD_LOGIC;
  signal gt0_rxuserrdy_t : STD_LOGIC;
  signal gt0_txuserrdy_t : STD_LOGIC;
  signal gtwizard_i_n_0 : STD_LOGIC;
  signal gtwizard_i_n_5 : STD_LOGIC;
  signal gtwizard_i_n_7 : STD_LOGIC;
  signal \NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of gt0_rx_cdrlock_counter0_carry : label is 35;
  attribute ADDER_THRESHOLD of \gt0_rx_cdrlock_counter0_carry__0\ : label is 35;
  attribute ADDER_THRESHOLD of \gt0_rx_cdrlock_counter0_carry__1\ : label is 35;
  attribute ADDER_THRESHOLD of \gt0_rx_cdrlock_counter0_carry__2\ : label is 35;
begin
gt0_rx_cdrlock_counter0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => gt0_rx_cdrlock_counter0_carry_n_0,
      CO(2) => gt0_rx_cdrlock_counter0_carry_n_1,
      CO(1) => gt0_rx_cdrlock_counter0_carry_n_2,
      CO(0) => gt0_rx_cdrlock_counter0_carry_n_3,
      CYINIT => gt0_rx_cdrlock_counter(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(4 downto 1),
      S(3 downto 0) => gt0_rx_cdrlock_counter(4 downto 1)
    );
\gt0_rx_cdrlock_counter0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => gt0_rx_cdrlock_counter0_carry_n_0,
      CO(3) => \gt0_rx_cdrlock_counter0_carry__0_n_0\,
      CO(2) => \gt0_rx_cdrlock_counter0_carry__0_n_1\,
      CO(1) => \gt0_rx_cdrlock_counter0_carry__0_n_2\,
      CO(0) => \gt0_rx_cdrlock_counter0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(8 downto 5),
      S(3 downto 0) => gt0_rx_cdrlock_counter(8 downto 5)
    );
\gt0_rx_cdrlock_counter0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \gt0_rx_cdrlock_counter0_carry__0_n_0\,
      CO(3) => \gt0_rx_cdrlock_counter0_carry__1_n_0\,
      CO(2) => \gt0_rx_cdrlock_counter0_carry__1_n_1\,
      CO(1) => \gt0_rx_cdrlock_counter0_carry__1_n_2\,
      CO(0) => \gt0_rx_cdrlock_counter0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(12 downto 9),
      S(3 downto 0) => gt0_rx_cdrlock_counter(12 downto 9)
    );
\gt0_rx_cdrlock_counter0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \gt0_rx_cdrlock_counter0_carry__1_n_0\,
      CO(3 downto 0) => \NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED\(3 downto 1),
      O(0) => data0(13),
      S(3 downto 1) => B"000",
      S(0) => gt0_rx_cdrlock_counter(13)
    );
\gt0_rx_cdrlock_counter[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[0]_i_2_n_0\,
      I1 => gt0_rx_cdrlock_counter(0),
      O => gt0_rx_cdrlock_counter_0(0)
    );
\gt0_rx_cdrlock_counter[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFB"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I1 => gt0_rx_cdrlock_counter(4),
      I2 => gt0_rx_cdrlock_counter(5),
      I3 => gt0_rx_cdrlock_counter(7),
      I4 => gt0_rx_cdrlock_counter(6),
      I5 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      O => \gt0_rx_cdrlock_counter[0]_i_2_n_0\
    );
\gt0_rx_cdrlock_counter[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(10),
      O => gt0_rx_cdrlock_counter_0(10)
    );
\gt0_rx_cdrlock_counter[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(11),
      O => gt0_rx_cdrlock_counter_0(11)
    );
\gt0_rx_cdrlock_counter[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(12),
      O => gt0_rx_cdrlock_counter_0(12)
    );
\gt0_rx_cdrlock_counter[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(13),
      O => gt0_rx_cdrlock_counter_0(13)
    );
\gt0_rx_cdrlock_counter[13]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEF"
    )
        port map (
      I0 => gt0_rx_cdrlock_counter(1),
      I1 => gt0_rx_cdrlock_counter(12),
      I2 => gt0_rx_cdrlock_counter(13),
      I3 => gt0_rx_cdrlock_counter(3),
      I4 => gt0_rx_cdrlock_counter(2),
      O => \gt0_rx_cdrlock_counter[13]_i_2_n_0\
    );
\gt0_rx_cdrlock_counter[13]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => gt0_rx_cdrlock_counter(4),
      I1 => gt0_rx_cdrlock_counter(5),
      I2 => gt0_rx_cdrlock_counter(7),
      I3 => gt0_rx_cdrlock_counter(6),
      O => \gt0_rx_cdrlock_counter[13]_i_3_n_0\
    );
\gt0_rx_cdrlock_counter[13]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => gt0_rx_cdrlock_counter(9),
      I1 => gt0_rx_cdrlock_counter(8),
      I2 => gt0_rx_cdrlock_counter(10),
      I3 => gt0_rx_cdrlock_counter(11),
      O => \gt0_rx_cdrlock_counter[13]_i_4_n_0\
    );
\gt0_rx_cdrlock_counter[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(1),
      O => gt0_rx_cdrlock_counter_0(1)
    );
\gt0_rx_cdrlock_counter[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(2),
      O => gt0_rx_cdrlock_counter_0(2)
    );
\gt0_rx_cdrlock_counter[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(3),
      O => gt0_rx_cdrlock_counter_0(3)
    );
\gt0_rx_cdrlock_counter[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(4),
      O => gt0_rx_cdrlock_counter_0(4)
    );
\gt0_rx_cdrlock_counter[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(5),
      O => gt0_rx_cdrlock_counter_0(5)
    );
\gt0_rx_cdrlock_counter[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(6),
      O => gt0_rx_cdrlock_counter_0(6)
    );
\gt0_rx_cdrlock_counter[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(7),
      O => gt0_rx_cdrlock_counter_0(7)
    );
\gt0_rx_cdrlock_counter[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(8),
      O => gt0_rx_cdrlock_counter_0(8)
    );
\gt0_rx_cdrlock_counter[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(9),
      O => gt0_rx_cdrlock_counter_0(9)
    );
\gt0_rx_cdrlock_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(0),
      Q => gt0_rx_cdrlock_counter(0),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(10),
      Q => gt0_rx_cdrlock_counter(10),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(11),
      Q => gt0_rx_cdrlock_counter(11),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(12),
      Q => gt0_rx_cdrlock_counter(12),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(13),
      Q => gt0_rx_cdrlock_counter(13),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(1),
      Q => gt0_rx_cdrlock_counter(1),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(2),
      Q => gt0_rx_cdrlock_counter(2),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(3),
      Q => gt0_rx_cdrlock_counter(3),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(4),
      Q => gt0_rx_cdrlock_counter(4),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(5),
      Q => gt0_rx_cdrlock_counter(5),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(6),
      Q => gt0_rx_cdrlock_counter(6),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(7),
      Q => gt0_rx_cdrlock_counter(7),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(8),
      Q => gt0_rx_cdrlock_counter(8),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(9),
      Q => gt0_rx_cdrlock_counter(9),
      R => gt0_gtrxreset_in1_out
    );
gt0_rx_cdrlocked_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => gt0_rx_cdrlocked_reg_n_0,
      O => gt0_rx_cdrlocked_i_1_n_0
    );
gt0_rx_cdrlocked_reg: unisim.vcomponents.FDRE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlocked_i_1_n_0,
      Q => gt0_rx_cdrlocked_reg_n_0,
      R => gt0_gtrxreset_in1_out
    );
gt0_rxresetfsm_i: entity work.gig_ethernet_pcs_pma_0_RX_STARTUP_FSM
     port map (
      \FSM_sequential_rx_state_reg[0]_0\ => gt0_rx_cdrlocked_reg_n_0,
      SR(0) => gt0_gtrxreset_in1_out,
      data_in => rx_fsm_reset_done_int_reg,
      data_out => data_out,
      data_sync_reg1 => gtwizard_i_n_5,
      data_sync_reg1_0 => data_sync_reg1,
      data_sync_reg1_1 => gtwizard_i_n_0,
      data_sync_reg6 => gtxe2_i_4,
      gt0_rxuserrdy_t => gt0_rxuserrdy_t,
      gtxe2_i => gtxe2_i_8,
      independent_clock_bufg => independent_clock_bufg,
      \out\(0) => \out\(0)
    );
gt0_txresetfsm_i: entity work.gig_ethernet_pcs_pma_0_TX_STARTUP_FSM
     port map (
      data_in => data_in,
      data_sync_reg1 => gtxe2_i_4,
      data_sync_reg1_0 => gtwizard_i_n_7,
      data_sync_reg1_1 => data_sync_reg1,
      data_sync_reg1_2 => gtwizard_i_n_0,
      gt0_cpllrefclklost_i => gt0_cpllrefclklost_i,
      gt0_cpllreset_t => gt0_cpllreset_t,
      gt0_gttxreset_in0_out => gt0_gttxreset_in0_out,
      gt0_txuserrdy_t => gt0_txuserrdy_t,
      gtxe2_i => gtxe2_i_9,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0)
    );
gtwizard_i: entity work.gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt
     port map (
      D(1 downto 0) => D(1 downto 0),
      Q(15 downto 0) => Q(15 downto 0),
      RXBUFSTATUS(0) => RXBUFSTATUS(0),
      RXPD(0) => RXPD(0),
      SR(0) => gt0_gtrxreset_in1_out,
      TXBUFSTATUS(0) => TXBUFSTATUS(0),
      TXPD(0) => TXPD(0),
      gt0_cpllrefclklost_i => gt0_cpllrefclklost_i,
      gt0_cpllreset_t => gt0_cpllreset_t,
      gt0_gttxreset_in0_out => gt0_gttxreset_in0_out,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gt0_rxuserrdy_t => gt0_rxuserrdy_t,
      gt0_txuserrdy_t => gt0_txuserrdy_t,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i => gtwizard_i_n_0,
      gtxe2_i_0 => gtwizard_i_n_5,
      gtxe2_i_1 => gtwizard_i_n_7,
      gtxe2_i_10(1 downto 0) => gtxe2_i_7(1 downto 0),
      gtxe2_i_2(15 downto 0) => gtxe2_i(15 downto 0),
      gtxe2_i_3(1 downto 0) => gtxe2_i_0(1 downto 0),
      gtxe2_i_4(1 downto 0) => gtxe2_i_1(1 downto 0),
      gtxe2_i_5(1 downto 0) => gtxe2_i_2(1 downto 0),
      gtxe2_i_6(1 downto 0) => gtxe2_i_3(1 downto 0),
      gtxe2_i_7 => gtxe2_i_4,
      gtxe2_i_8(1 downto 0) => gtxe2_i_5(1 downto 0),
      gtxe2_i_9(1 downto 0) => gtxe2_i_6(1 downto 0),
      independent_clock_bufg => independent_clock_bufg,
      reset => reset,
      reset_out => reset_out,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2023.2"
`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
TCSZ2Ye5IRfHavlF8Mno1PI9xixWuSiNh3ssU1FQtkjW1fmNtc2c3x12slL242UQayI0rzZTqe6S
edtecLHTOnzxXpCZjjU8NFmgLPerTSDZ1W5YhyIi9j0Ap4YBpvaA1ojM0+r0Cx+dMOXohQGeyljq
+fnTaFTUe2678DxpqHk=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
NIzZVKMB1/8UX2qb4BB+VXCafEKXsrn7axB1cJDPqDCzSyt/2KG1NEEZTDHZVzIr8Bf9501PyXmL
VowTAAXX/RopKyKOM1xJN/qLtqXxegH2a4dIkUxDIIclIcbv/smna9VCwI7m6JhrnKsNciTTilgR
27S/h6JPpZsZAEmsNxxTC70WQhQSM8TlHJjZg3KDc5KTnvC/mVTk6I05U6x0Bdd1YR9GBvhwRqhP
B1ukL/1JVOwR9Ce9p+EHFE/xyApypCjQPGwq+8IFQgS8wltVZHX6eSMw17Q0wGCY+LHduRTA+abV
LvAR0NPf7PKQUSCECe2mBbLPO7wD4BO5RAkJeA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
udw4XxxzcaDNM1qWnRgZ2JEM1MMqnKwKVqha/krU9EyUAsyATjQEMBqjlOHw5QXMU2jjizlL20Nl
h2pF7iKo1S+7TS54Y/UIJANp+Dl46V/qfy6/yBnE4YclHON1k0jRao4C6T951tgXuCAIQEmXbr87
aJfL2dNqORH+TDKUBdc=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
JFe89rPDaiIUicPDNoXCg8pJUoYRyVDoW/5yE4T0Cp9xDHtuIyKQVbC7jVb92OsgJ5GHDm7DH2D2
rYZKrdCIqPt2jo7DG6bcJuDFcisZb11HLlYWNsK2Vqs9DdsTPViykeE05CD5AgfDxb983x8F1meK
w8zjeGoD44djsaRA+lvP1zLhl24q5LWFJdPSyIT7uWZwhxHqlyJu85ToXLuwZQZO76Mp+1mitxDy
vleizC5rnk/4hqxfEFS21Qi1TwCz5hdU+H3nA3dTe1KRY+obbFP7sRWKfmr9Rcf9enRvbaEbLoJA
9ADkl72jc1Aqlnd+YCGq4EmbElbWLxblpamncA==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IV5qOrW4jXfiGt6hz/YcNm+/H3ij0v503CF3Kvut91tUWldzNzyLt4pIZEWEzSmn6RcpcLNN88po
1kt45UdSBz+mL5HDQaw4J+VGD/cCBmW1jnOclCf82kwju1MIDfa2EKicjqaykCUROxV7cwg07FFp
clLfIwd4kxgSWnGzeZi1IGezx7OpBsAkBTz9ha4WttEm0+D29DF9O4GaQl6q8IBeA0QIrO10EESt
slfRi2evxdOeTZBVFoXU91OszneH/prZqyCsHeyvTa8PABTZ+Y4CH6ICZCXRn7QTNJgoYSGABuPs
87saNJgzomjyaO6IzGl1fBgMIsIurKw90DE8Jw==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2022_10", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Eek/EeBa5kEiakvxzHC3fZ0NXCsvWnLN8FYKLyImepfBUv2jdVDY2j6Qs928DJaMO8pBuO9SGcu3
47rhcN1DAjZza5Ac83W24fngY1+YWblivVc8AoggXS1t2Y7Dy1vf9+ZxUdOvq63sje+fDJxapZwK
3HQGdtBX86RTaUS5K+HyI1FTmcIhUYmJWmxQjIxLla7FF1QZ4XpTCfqAG5i7ZKlYSoDFb8sjCRG4
XWFuk1dbL2UfZPxXZ7XHIm+03Ck/JsHtsjLCc8oTB/9MLom2HX9SjX8H6tFbEXR1NatCFWQ04JKL
kHSYD/xDlwjhN9CRvowRhNJaYSmKQT646hlNoA==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
gaID+cgqOdyoJPDKM9JAyimEjoxNakxuNjYf52N14HEyn4NQksF7thq/bXWc82vmfdc8aodx1+ky
i8uuKszW1WwV+apGSqk7YXBCxx3ACfMsPzNzeDQ2HVzGfznpQD80Eu7I7iwtz3k5Mr31iaeM1kQa
oddk6CkVESI8CD21PQHMVeu0LKLZJp8k8NHf3i0UOXsP5o768iecieYQh2VYXZ6HORDDyd+IpDB1
CAFBZctXco8C1w74wCB0LXUSYInc5ythxBURkPPTJ1GBuXpoQGZD2sNiI2Htl0y1toEdfgExWZ+0
+4Docnd9TgOGhAhZzUcj3c+6cQNbgCB847/G6w==

`protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`protect key_block
tEBTlBoXowC0cOrrCu9pEZ6t9FjMS6GWThbzsXMvR2xb9HBcccdvXT7EfASM09KkNzvlYoyNBWct
0TRl1BNzzlomu3G857U6kezS+CCRF/K2qOhhxHFxEfuM0qblRVdNHoCGGMM4PkE/rt9M7IqYoXQg
WOHI1ydpZZn08aVL9QYJgz6ZuVHNLwSpL6rjFFDXV1cB82gVFBkRP/0NxpGW2WH6YA/MJ0czV0ji
o0umOWluEwUObdytKX1lfuNYimI0ziWrovqq2osL8J7NBKDUl2R6gJ51DObsTBgC8uyUHVibyNHy
nhzTpwcBeeXdtAueCg1BlHDIwglcMUdy0sBZEyHM/CLzpxgr1A+uUcmzlWx1drrc8lRNwGMFDDJQ
9OzoHBABtNt8N3bbO8A+rE9HtsjMVr1TxHhUTxBhWcypwra+xzsGykln/IP3JBwwQR0+d+V8/Vec
5Bh03crJTvJZUbYidozNoaPOfnHi0NxFDNdL7L1i75T+H6bqeE1ADR/4

`protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GM0AtcDH85MjCjnA/pijf0ZCJap8X+OYUX7W1StOwznqG2XPd9DhtvTyqD0c8/7BTdeCzGUK5iqe
QiGwEcy1dCrSVZW0KtjFXllkYV2ai3/Qn7Bgg1YuzxifEFKe6ClTsByfgjqRdyZeNSAldwvx9ZtT
0ZhijV96K37zXwfXFeKDmxOZOV553ovWfXGekaS1EPmSluoDYBMQKc2XV+ZUXR7n5NI/6E3QdK7K
utsZyrFYyJdYW8Po28hQf1nWeQP6+PxQB6wi/P6sUzudntNcQ7uLRr4PTz6twPPqYwUF+7YW8baL
p/2EFPf8y6fBb+DOBCnzmGZvmq+M2qQot14r+g==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Ot2lizvbpt8emXxoZl10oi4H/aTQTzrHDg0mf7DDs3BS7iyFsRmaQCG/BRF/mwjlw+EbST4/x/DJ
d6Hf6LIa6mllnMD6G+uVHQ1z31eASHPw3m+WnPMr/zCNuebPcDitgiXWmq3SAS9byYvxiwcDvjn6
CMh89pvlx8xLiFUoo5j/lAPe4cPBJwSMleQLLB989s6rByi0lVW0QiLTzakaB8DHBMvhIYEfi74m
Lxby6+nYRGrAUKPOemP0Ag/LW83Eup/Wa0jVOtxzlj3foiYhg2mWCt2zyFhgQsDA+oEsDa/KZc0F
OUzOI8vFDrwPmYRwd2ejFI9Nz3/1mb05VQmDRA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
OamBwohUIPOIa5bsJvu3upqnGl4f3jNYS35bg4S059C9qVDgQJcw/it81deNA5zFvzX3Cq9CiO9K
zl02VsdpFgNPjSwEO8F47LJZ4fHx99EmESBogsNwUNitzkuYTLCb7F7ZF2WSJExQ0KsYt+TRp2UH
yQEvpM1lHQYUXxzjw08qUI5ssSnOsQFydvP8BwA/6aGrVJ+LuEgPVdMqLBn0EeAmRsynxJ7OhPGV
DlvHQwtVuBrkvjQHED/Ye43ZIeWPm/xOcjNfZjYeOvdEJqTbaviR0Fo1LFx8EX80uvdFeK55ywDN
wzoZ4cVH87f6VwR1xHdo0JpVxrajZpSZ0jPudw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 17776)
`protect data_block
zGVL3ji0I3fTqDJdPT1aiblwA9LMgjxCGUEAwxMonK14Dw64f9ZaoRvZpfZmX70tS4Y9KXuHN53B
I8EKQs8vxPRkDKvoLhQZtS7uvDKP/1vsv1MgUmrOQ53r/lRv/rpdCpaEKPCgF2dB7NIB9bc3emkj
zrm6z4KSFDPlZWhJeaCuxjgH0g3wGZ+I2078yTGyeT+kJuI2IwUAt0abiA2nAJyabQmqMTagQA18
f+7egu+VJWRvqELMJpbimUssvpXPWGa7Y01TxZ0r5IJQIGQkk5MD2coVBdZrCmHwQfYbInEsbija
tQB7F4wTYNwYQEms+pKsvD8MM9o50SuPi9dFw0GsC0VNOGY1QHJbQmDYP/Ok9BnxiXyDMN9qv3/u
15mEtH/4eLsWozx5/FyPef7Ix/4GNMIzzkNpioyAPElCw68UDkl4aIhXVAY6NaN/BnDsMRDiMFBI
R2qx10PdM/NHacuNN7A6tWKNPk/Eo17nMDM+P2CnMreui9Of7XYb65hSSfTXBtEBgqOhGHG1BjLu
ASBcQnGAbQJopuX2ybVguRe+jlAV1bvV5DzYwJlfmpLFWjqxxR0g1kT4yamOaVdd1tGR4N8262eI
RZM+yz7VePWYr6v+IAhsPblgVqreZUikrPp+VH5+z4Di94wcW2QSc3HKp+v/Z0yz4OkFBt03TGg+
ImhPrXXjOG2wU0A7tVFCL7vsdYnp6ZWC9iFLtaBRpIGYr/jEuLcRRx3iuy9lSLF/cQQ+Oef7FbUm
T/EjmTG8KRkTRqpC6q1466v39i288UhlCGLIQDKTTyuCRRLurMjA2WDGAkaBbG/kgB/OF8OQ+Ov0
gslgj4jVpU4ZU0ZLwyL33ST/po/FYM5pW57yB9aF4SkDQq/dORCKtp0rcjCqNqaUXwF4qW7NPJOA
Tx+BLty7iH39fMS+sBDGDWV66sdS6+9URYj97yi+sP0wiBETLzTHj8PXFPzLt9z4EhVUQsfyXnF1
c4fdKBfiWm83uzaMyrIJmRt5RFrCTK2lKBiHTrtf4XoFqF2zGPm41bQUHQltJk+m0qLLJJubtwO0
lEiBJHB5Bc8p7DPc/fyxFmJhDN7DNtVbw/nv1+bbeAmUFfDAZGsxhn9XinixoQGWFhs7CuLPm6zu
oDMKeAADMjsddv4Fn3zOUIlukWvHbadReootwhhSafCCpAQXNp2WUj6Ovho5IdMHPd2AIP23eAE2
rKzKF9S+M8K5rQkk2YMYfEV3GsDWoxUKbCluPp5RfsRovPXeoxHSHyTywO+KUl6Ym48QEFgFYn/N
fsiZX3ct3cY+C4GhhoKI1O92Johw0q1B9doqaPIbo/2HmTZgxF3VKwBzZIG6PPA0sH0LHna5Q89U
IBSnTTYDVBKZY9zzBHPdqoGnicC7RtLi3CI7BFGMDo0C2UkzSrOzFCCmt6jrauACpDvR/WZ6ZdDz
I+yn5Ec3w2B1YoYH47ZnAF+Uc2TZETnLX0CMimUAU33VwOSC6SytjdC6dfRWm28O70rJ8uID2gyZ
h7LplHsHmMZtMEi8/7hzt9xfjf2Z2/et4ajnIML9d47NuXfzARoAUlc1SOu57DjXjZdYkmysnhfx
RhhjfDaaDec2y4jCduVzUCILqpX8CFYq5U3wGxagrMxCuxrcG8K8jhDET88BjBsUqHVq14n+jryz
IIvTHtIVfiz8/DC4jOwQmekitKC6prpc1o9S+I9N2oLv0vJ1CncEYrIZO/Mm5M1JeZwfDxgpzwfr
Ci453NrKCsSEF9xRG8h9sHptuiirNAhsLUO5avR7uf8zlxsQA6yYwYEpqeTR1GoUhCoEJBT0CkGu
9TfWT1WBYAx8fIfUR9D506lJmizhqpmkqevGd/VXIj0DNaVNC/u6n5P0PkhYR/L6k0cp36OPc09D
l8KIxxX85E0TExEray2VGMYboxtBSzSVP3H+uR6AhByThsEMO1HwRd4vjwcBC4PA/l0360KA8LO7
WCIZAdxTDKKI1IzUFefoTo2RDomNYPIPxk2pMpQvTpoir+AefZ3GyvX4+V3yAZ6Gt7cxrO6L31yU
ss0PJnXRKcllFDsn0IVfJkRXapc7xy6c+mqkQ3LZfJhwi/R20E81ZaNlIoDfFoHRDxDVR9ts4xJw
X+QdpVwU5xyqPwVe4LyQkaQ0r2CJ2PGOxFF1hXP5rbZ8GUib1Ks1zOktYGhweTazj/rRa4hXxC+E
EEqpM4Dua0FFZRcP7y3K9v4fA8L2G+VAFqSnIQvhrj61fgNOrPlP1xksyDc2vJiROOmrUTm57nko
9z/YNosT1k4HMJQTj3FGYfeIKkGNeIULvbztvXJpIdUaY29FtVlZ9tt/nUvnbkr5x8oDpOtlLEZY
0jWe+jqKCHQ4WgjfYHYZ8g+MHG0l+nhld5Q/jRL5rNWDQsSyh1scXdKsu/khJi5jai9kCgSK12t+
FfjMT8GBL4bXfQE7AhxtSGGOWfypwriJKu+2HiM96YeZiAl1uX4VHpDEc8WEDYWssCLCYz8H322D
ezgmEbJS6vwPJxYG9C9XArkXEIVO9yHAt1O2StgMNphKOICVC0vVAEmKjud4yMGhLPVruLyZevgH
bTh/3YLIsp+sp8jUmer07SgQVJLGQM7hVzMp/MzGyZ9MFcvOee0UowVUJVWywhnKnGcMX5WgX24V
TjuOUQ6cA7kblQIFjHPRhksVrTy3O5DtZGC68Yga47dw4PU2kGJULIw6hnARel9KMnDzkWronJ7O
8d9C4zulV0miXRXSM1Y8DjIXTbBVF76ZVEKHj9Gm+v/dT9O4OhexgvWiIhcL/o8KslijRkJYOyS5
Eq8nidRFKjOK8TZw+ljVcRfCzsNiUMjL+ZZu2wfCWC19IvHYDFu/Dqrxa5aGOMyyl3+aSTGpSWgm
ubEfPK81u31HSfhma0UYN70a+Wp21f+IbyeoKCXbp6HxjJ7n7Q9WoqolSxpRz0uM1RvGvOn/j3R3
Wc+UHmWlk6gMAUVaxb1Af+ZopOPBkWStz+9+e0GM+Nw8PDE30hp+ojJoIewaGPbA9hRWtkpUsPR9
ebIHlL7QqD7PqoXKtbB6blMozwWUfLwxEitK60lQHzKWdsjS0fNYP8E6OyMIzDI9m5p0Fn1+Iwja
aikmHRORfRzZhMQb+B8jj26v1c2P2wC8UknoTM49aSiDi/GNLeP2PT7W8Cu98VdSZLzdOfzv0aWt
wODyUsdTY1RHRgXmjAhkrcNruqNHIm65TIR2Tkm4PV5hDUGF1YMVeEgsnV0cwW4kLmS+7dMKmS/l
PGXgECm6MFIhmFfpkZqCXszccRVsbOp66Escv1wlAxKr6+3whMc7VR3OcTm8Tt46zTpx0lk736Xl
DQt5+ETq3RbAhmnW41a4y54cnVxiJz/PuUuKQ1IDk8HFUtaQdVsWm29jjj+B/QbJa+YrZxTLkhNE
WjGrDAeAX3jVXvN+KkJG56G5HmcaDN+yJmWI5knGQlGgBisHKYqZSx+xCTlXAFikAhrwH9+jLxD1
ZpbYnwTGN9YMmzun5cQXE2o+MJBWLF6Nca1kxxha46FX1zHh393JTAbJm3sj3soi2QP36PvrXtQ+
25x6gnr3qbqkV7bdm/IqdMS1QTyDm3RpNk/RIifjR6mHrr3xlLXx9XLmKltmCBmQ3FvKQiWRYSnb
/QmgDfw5RTniilVM0WJtojbfxbPJmqZrAUIj0NHuHrt8BdiF39NaAU6fh1DCBhAd6e7OuhldxiKC
GA7rYPIat3TRmGcTv8nMnTa8/uGKcTb169/ucxiR1YeR7HaOhlgz0LXe9j6s+KUK9eAVn2uz/4V3
6OyLfUDUDkOk712pWDydUnmNkil3j/f6lYzLF1sD+7Nq3uuM+hg8l6aprj1tERL4ITLSSiX3pNqt
uQiReRrner3GWPeYp11DsO+hEAVkOyDLI+XcMuDQADVE9gucJoHxH4wOEwr/vwPzv7SQnmEJsNfj
smRjft70wCEH6UGbGc39HqUWqBA1N/02UEliYUysto/NJ8gfJ3WIkik7aDR+tM+0liE15rsobjuz
FuAmqQxGmENXFFBPZPRoyY+MDFf7wA+iHOq0PhMOSjtVRu0MUMgIRdZFW3nS0unyjh0rX4dlBGIe
simUl6O5taUQzIhQAHjJBHsDafbWYN9I3uwx3mGilNv6dmraQE+spxthxH/Til7r+WAazWQV0O89
Nfhxmi0JAnyPTx5bj1EOKEsPhv/QO+6YbFs6ep/wcdmFUFUDTj0MZKqBxwpbm3zYWX+T2+nryXEB
Mz9/hj6BPN4PJCc2TyWJs3slEtBiWOjaGrChROK6n/3EzcQFDJY82Zo021IxRH2hJGxwrgw5RfFh
JsFDePYOPhhs0l7MFjIoDtQFcM3Fk/P7hhHCoiK06NzV3botawrEO6jp4CqvIEHIl22Q9+Q9sRGT
rjvWIQm6TscxDkzKFyY5IYiGPcF83BchFpLuECakBM8rFSYHdBypGYBcuFTCjSdgbJPXWuvQxZFU
1XYevyUr1uzN/vn2E4h0xM9/3UDsFV91Jj9RQIAVEbVtXnPywZVKkjW4uPir6KiouJwlnKA0vnQ5
cGX5F4jg+uznSGZiiwkDwplbCSClXkqLoIsB4ImXyoCNqaQCAe/f3R/VZyBTFvhH3mH3MN8yS9Lu
LCbb9bWMEhQ+lMMBCv2rctBkG3458Z0bdb5crV3sYrl0esMV00OnMhRa5mUaj3M7kmxx9HO7w6BQ
s5uK84RlJOBT9aJg/gWPK/97YEmESRwIjzEXddSTrqkV4B06hrBDK+KdHWcJinFRxDBZh4oftzT0
dc5BW4YSG0JxunArLA2nn0GjSfi4Pi/XrcjARDmV02DdmNf6r6c9YLLMvgFx1zDN75eo06LFXTto
89FsdEtIPptv10rVDIB4r2cz2J8Kke6/XBrLbTvqTxlLjqeQX84d39JMNYj91OZrRtK5HbNLkkSi
ds8GXWunthB9fLweiy0YwwSDZLEdD3rUjfiU78ce8b/CJYbmzKMbzuDvpGA/DhUuf4ce6XaoKQzl
pTnvQ6VeltOvDqAqFlCsiPfzIgleR54raPy7tUySsSXYbwjrczJ66eR/M4zX/+LRenLMYcw+Z2SQ
6+7QHY8naW7NyYSp9t3IBgu9HYyIeVJjV8rbezbraNWEOR5hn4gBkmaDJi+BA4h7epnfRPR3Hume
tBUynyw0V9Fx9tdJpwH5Z9Sp2hvV94iSgmrJh1h7aYTrBWahX8b05SJr/PmOnSzLWE/Of5YGQ0+1
0ErS5PlywQNn8RjGS+VsAu/mc2YmpSl3KlZhJSL18wcF30KrSwmFClGlrtdJgoAC4KHLFFtJgNhs
TIgWMLtSIbTxgjbbve3UFxHl6C0WZI6WxU7TU4wwbNJohO2/wxOrr/q4Sz2m3Pvno6ZBJARt2cTN
+RnCcxGAnR4CngzcoEtW+bRdi1pt3bKvz1IOaZeF4UIynTeyOgfsp332P1Z9Ygn94HC9N5etH+F5
PPlkpGEtoCxG5W984Skh03rPkE5fOg52cz3Ha11fRHK3D10ycp1Q7G/Z6Tx61MLqVfHu24ONdzk0
LfHY2NkF6PY7YAWa5w1YSffqLStRUtGVIJ0F32KZNTMN+SvHovjBwNVM9OwEwofyH7cwmDvcEGlA
3GQsidcQtmeJIPFIG4iySSTOpBZydl10ndSVxkg+7EzV7Igs9qr7pljX6v8cO9WHOtNNIZD3xide
xonvK/e5wyMwh4pujB80+FUN8y7jpcyZnR3h2A3WpvtNhXkjkJPq2EBZ+wjtPZeYdf5cHwnR7WeW
8f3A/NoodnIN3Wz+sLzZXfy+zGlpq1fyljg4zxCZ2n5doVYGf9N1vvYUJKZwRJQOXkuPliZiShrb
4uax43hW2FWt45Cxn6G7KU8PgzFnBR4nBq/33sdNSv/LRNIzeB4yxjvI5GJmWUOZ8GFrQ8gEcean
2YuMro+/3uCXqboGHTy9OaQgl8aM7twme/RufnPZU9+U7g5NVwFmdVtWFU2VcJC5JJSCa2nAQPfS
Y+FLC+BWFJfD+etQ1qRZHxTgItk1TX/sDSGPb7VHOZGW7qpz6Muv9Aedh/ONXC/3oHOb7l81LBZ6
hhughvCZbzbFnkONucJHojgQJ5UJa5M0It30xaB5/sSmjd07xiEC3B6yo6+ogjpg6d0Xp2kLyXcr
B4XYIEWv6peHYySAr5ReXPeCHCAlkv7Ms5Ahd2B7tT0pNLt0vObAPqirKWoRPhw9LyfhMN+CgQia
DiHgGYQFw5+x7XhPfMHuLMc+ZFGros4LJIHL594tj3/5WPHJOTY3pLSFkfmdZmM3XVNuWlemYUfT
6EtefdG5xoLHR5azsWI0zP8iJFtx1ov41xJrru0X8p6O3Yb04ZMORQzIiB6EnZmtC+VnHrCl5B0e
70WFLZ/MjjkbSlOmD4StKAVn1+knrphVB6eh1u7BOjWMduNWp8i0ExjwWdQZA39vDy14FjtgPB4C
6cG3Fg4fEaK2BkpHpAk70QI902ZeV+XrBmkrhpN6YEzwlqA6wFYrsBQ+Fx0p3d+uvtI7t6bt11tq
hkZh7PgLkQEpGGdsAlsVJ1+V/QF1D1zgRvoBu2+aAYU8s2jbtNTmlVNqgP3oRZE9LHvriYbu4BAX
6pbCieevNLcaTgDWTIGSbgNG87BlW0g0CmX2/m8GYZgA/PLN13d0qps1ByLxjDmzwI2kPPFIXK7S
RIPcqTDp+YV2ftCLUxXjOjxrQ+AxpqLKsQVfaKVgXiz1zV74KFDZWYTr64kdUjjSZCijJx0L9DgF
y5GVR1p6fZcvPb9OajP8XW376nJPSGVEkfriWsdDQ+ZBceoeK8/HaRDeemeUvbIUBzpta/UnkIB+
JvOGDj8P81WOh8FGo0QFOSX/wiRCycDiwtpT0jl39TSEFjhK4L7a88HdwZAsScWquZkeuOQbUa5I
edlPe9HOsGD5gvlO7/d8V+YzQFVzY17cTNdkt7CVqwBZ701xol8AlZDIhZGtP5cYA/5XYKe09ajC
gmZvfuO7FdNkfsOiOsDRzXFBKd2XTS9j4bINVZZ8f0lkkX/v2mjEPbYK3Cya+7/UaQ0xFBkydTi/
+dNPWEKcQue01cg1b5cmuQEcMZJPCInZjbtTcDn5zEd5YBRR9ucr4A3yMI/ZBh4bHi3mCFRtcBzW
NLkNhWbgZBY7BX4C6nnfmlbeqoPrUGcmTX/3KmzgyYnpLvLjEStLY1EVJ0fff6TSBXcYLuaJlnId
1SO7qWobZJGXIcvHuEO7Xt7hixfv5HGeeK8eLMsmNELIDAJXU4piw6gjd0EvvguivMsX5BJPq80W
1gew7oqeQJakfRjKcN2rnC/NmaIb3TxhfinlenWsHgf1PeRxbOVqoxmxpWwodou8wakDJfZyOPCq
Jk/sLaen9aN9qZlIDL5gs4y4C7mwl0fBOJt3HBOSvlXaFR4Q7tkYrnngYSiYP48ACk6qZD5Nqf1h
Bksap+G1Qr/Nhr7Cv2bHufOCh7kOleLzk5on8toq09DNje9fUvWxCmopEf+bmrHs7IWiWp3NpneM
wPag46014GY9ALflxBolmKl+Lcf6DQj/VtvVhxE+moCK4S7XvyiJ1z+JeRqMMuF3SM9B+B4YjYfJ
ufrz6UPkOolGtejFttc6QYLOHL4to3CY1PHo7er5TF44JokJGb2Sl/5uZdKnw373YNRpFKkD2+qV
oFwq9ODNhBLLPy56xqmrlUo77sM1YJkTTlDlp3KALxyAikjXNTXxHSsIg7nt42M17ASciKHoRuZ2
+GtbSwyNlCi3JoUdWftmZQMT8TxsiagJX7IMXfWCIk0X400OPfqQYqYq6+0o3Guyi3iZQP7sACnY
g4OGgw0mJSHCAKhDgpdTDOClCnyEA3x/g2yT3aGrD8nKKodBZnAfxPsxMKgIjOTlNxYnRaTuPkFx
L/eW3C9/PbqO680mThOtLaml6Dd7XPVLp/CyyDuwkLIfxy6h6Mmp01y/+UEOa1nAQJsj+LiJYIdm
gWfkSjaIXWz0q2/9opWxo9Jm3JL/kO+q/6VfkRxFKlI+k+TxCy5lxLnI+17sHYkbWWdZZrFUerkZ
B3K0z9v2ha86Yf4XWDDPWdARrjzYBWPIeI17yVCqXfH8B6Kp//c05NQp1sKz4kdqFcZeQjJ/7oav
YRZtbKRfEeQZ+kfYl2JNn2Wn6B1VcsjL5QdAdg1c4gRm9znWs0aDtJNqufV73TgUZdnXBjlAaUFq
6V4WvUD7A+XaQxhWn7X+rdLXpTlE+sAxVEPQxxLRCJHKo8xy6fr88olx50ropIBTPAXowAoTrEKS
vF5w3hH9yfnhsZCne+n+6pkFJmCiwLmUchyx3I5GWddfVqIo9YoO3zG45saKPVEFm5GY4HlChd3C
1/jJA6mcx+/nveNcGLtbGDVrLzoSsnOnFAqxCfKKa6gFhMHGiYrAxIQ599hBPtzNbxdSRdL5CHCV
D3jG6n8UOhzCoPjB3XvuNG/wZzwJ5j5m2f4K74GIpRbGCGzNCTgNfHue7fp2re7eEhPAi9Th3u3N
QthFqp2/BINFiZRWWmlrgiad3oclqeGkXVbMx1CBpw/0vOkZR4KsbsiYQ1e69FyvT4wuwV0kBtEe
ogvjMBPK+P6t+js6T+v8tKOr6mKnmn0sSIUowVlXA6dzLNavE6VOwAxdQn/U+TKO/MAFsQyBZcKX
O9MjPE5wS5xvExSvRFs4YkDHfpQnVKodQs70T52ux7ZotftAt1yuiAjV0JzmSWg3a0YDoi+PgzF+
Q4nZhtVl+5aX5kdvLtGdJM5v89ulOuYdYQrsv7f/vZI5HtdUVMpkTlgUYsfqYxb9URTsfPQM11FF
WppnARSaIlRdQYeMUodMmA+6dfBcObMxLyZNtSzg4UY9C0S2dITwt4KDJIAIGFRvfV8SaXp+q9PE
OP5gRK+vH/kFeIIizLrMjSpIpCWCHdtYh5DyzfhkV08mooA5nZRbI1CowHuF2+zAo/4cNbWcgx/2
JrLGyRoys4UGzEsHTCIVct6N9OQehV44uFYZK1R8g9D/Nfw9JYfxjkOI1d5WlY2nBpV+sZSui/t1
ELVgcdC2KGjf31ZeG2ytxisn5Biv4YSiwo4/LxKloBGaeZ6J8SMoLEZmMtUYQedJlEXNkcLrpbSN
nRneRe49j3b8UZ0iAlq16cCvGImKAAW9ZfRkeRxGzWbLU17QR+6TqCY8jXlTDw8ibaqiliWhwsgA
TZSBeCWVWwhtZEi7jUytR7SSz1Xvtv8vRePOZtwmvVRVBMN3zbVw6enWB0PwNRw1iYTa2F/N926I
iNjEOE80ldFXFgtnQEOuIFeYmbT88kB8Ifo+iPFkhJzeoNbjBIqY9xx4ZyZMnIKFWO4yj1AEeZq0
1f/ZHCwP6/LkxuyfhHs6jfqyx1Pw/ww+YdTOmZRWOb+cqybdqXubLWN+e6SEXeEDk+6lrCcADJsi
2m/CfIwtrmi7i6wSWUZWD5dHFwZnl3/YHBodfctsDD+tNuZpRIsLdMWNToNaffplq+v5Ygc5MH3U
2fxcubL9ClnrV3/8SCvatgpuMls5U7cKVQreG8fRMH/OJQ8+8vy8jo4K6ViocTdiCQL4gvvnWSOP
ba7ItgIWEh4sVga2H67RlHMHPQEY3yT5S6fQlI06iREVQGs5HOxXBJsYzx1qQZdhE+0mFJAG0pWH
DOrDiRmnUEywbhaUQfXHCeYPjIKEnHJNe64HYaVY6MT+o/gObeZ3Te15kydBZZxaNZtdvEHKyMCu
QmeNQet0qwQ2FA7u3rTd1rdEfVvOyzwdXCy4kUVHtvsrO3Dy7Cr1FTF1AelmUvQDQuy5ubfIx7Ji
vb/9oAR3Q1HRWOpu6HK+kNLhEXsitIxqwCQhnQ7/eI6YzeH72Nmfohpb8CpZ06Iy0qASfIxm4ko1
MboC4Efkh3X6iLVGzoGUnixsxEGvopFNScPzNtZsk9xr3Jvx0eEjShC1gO9zDM0cO+iN+Aun/cbx
huymjRtFlZo/aCnj6vtwoG+/B85xb0uYDCKYmG+jVL0lh/XdQGE+mkPXebmc72Pl4EiUAk+KFYnH
ZHFvdlY5YE9uSCur5MwHK+/OADVzJ3NAm7ag27dXuLJLSL86OYnvLs+NvHa/iHLBw9PmfuvhF4Xt
Jt6PXm9WLryqiDoODLN9HuVZYzyR20Fw/RBfvY0kjTs310pareve5YUPKcjaJyJ3len3rYSKOl/W
lTSo4Z5KGl3PMclvKGrlE+UVHmi6goEpf0Qi1n1ENFMlGXannvaEG9VCQCMEgKOCQpSCKyqdmdK+
n4m49s6nTeveJiqPSfIH6rybc0t2i/C3/OL0AjdidgYBiDMoWLdGvl89EhBOj7FFk3UwFBn6F6OX
yrg0R/AiacvoDbI6CB3DlXnAvtXnucGkCRYsecjtvQV/6qz9uTXhYh8YXQ6yuwZ98r3LotFWo29b
BdaRb7xZwMMxHhrZHbc0pNSXk4RpmkSnIyBCatUZpusAS4aHlaW+2Iq+VAXk181YyGRC5JatheE2
G/FWdY6tMMHs3ExUvhNnwhLUVES1eYi3rVh4d7PQSnJYD+f0QlKy+w+GJMsqtbVt/dOh/9aDPcc7
sVpEZpDKWSwh04L2PwMftNxKnUs+JKWFzUxqvaHBfxhgbYK5GLLaW9LJLud17pNrnllC5B+kssRQ
BNN+/OLYjDCAAm9V0PHsrEGXf6wLS5ocAkqH2XAB0ezfVSuUg834hXH7qkjFMahMkkGoozmjMXy9
LwQaLlovUxTjnUEXspnArR/rm5SUO2ja4wY+5ck16hEhSNVasJTIJjomrSi97vi4bQh3AR0GPXff
9zV/w9hLnPC5zt19ZuxHhPl5efTuMSxjzJ67gFvybszlUyedQ5+xppFuEIJfaePtLwgT1jWezEwM
TGHMeFpshPJ8LBYrf7MTziSo9v0dO8prTGyinuClmH9txYWnoN9q2x9b+RiVSA6YfsHHV3ODkOkg
7jbEOPZUg4Y5TyjQTfhf4OP/CUboZTxjt8B7BPZ1z3LWuqdVURmQoT8me8Wf4Q1R37BcszfYIzpE
8Z/Vy3/qvAPg1Md2R3tenpaykW/eeK/0/jwsCBkhaoApdmKBVropPGazpIllDNgGGZuoRH9OZlCB
2PUlsE6WYusmPV0YNAmlO81SV31SpEbsxu3rwnCrs2ebmT2BWxXVqUyMAlmtzWVIx11HVxENTW/A
kfWKp7EZbZHPEoNrU+UnFgS/zxDa7sa6X3wsiIEhKkE1eN9RGDIi3mRg5xmAnj6gKF0p82uydTcs
WG3Swih53y/KR2GByfBj141uSeeBK1VJfvK99RHwSnXrOFLEBvlKNsXgE1WOhkoPfxbEqF3oaDEg
C8o5mYM71h/uHdjnCfjjyDgjUrA3CCElAdRoiY9pZMW3+V8bcvmUazBzV946fV8zghqTpwmnrCl1
GgvXeWFJeJu6BrL2/e7mBK29iwB2byWUkQtxvoQQDErSmHfsV2nKvaQgUnpQoXpiJxxn0TYjnF5e
L4fkeYAEpD7qEcSy0TAkdQX+sy0Nu5Zw0BfX0fxECuyvRfrFJSXFaBEE6O8H5ouddhS5BbFXpvvh
pSbXXGGhy9JPIJ/AqLO7rDzyrLG2PFegtkLOacbw5VKT2KATp9FDsC08TzzAzQ0alfn+bx1A6s09
f4W8YFvM2ICPzH0eTmCA43IuJgIuOxbsIvde7vkbKlP2LqEWGuPSsK+w7tXVGMFxf2+lQgMLzu0S
saagWKHaJsFQKxuqBpMax4FirUMAn3eL03AAkhOpZhgpE6EwlxCDdvrt4lLWVhMp7oEBbmkx8C0E
NbLh+MbDLexe1POVsLblZRGP8y1Yfjkvt65hBwrO8E2zxvPt3uOURfeOKNfg+JlwxAlEbje6HVHC
pEwqmFDMhd3RbQma+HMo9mJfzgykr5J5k6r3pHhHmWGfSCL6O3Diu4F5xooplz4NZCN/HsLdZWJH
h7OUk55py+sz3YFCEtECe1FWdTp58fh8tvHvmnuHG/hQ5ubwvQQADXzpscjRpwC6QDJPcSa8G46T
nhzN/zCrWYXyw2julgecdwi23L7b1oTTllc6WonTrSAQk2Facs5dwYrYTsGNwaGkUgD1IrgCZQYL
DpM8/NmJOtIydNnuaAg9hFt87++ng4R+IBGmMEjuNfVv0cuK3xb6ubkpNX4sB58Jc1m73EK8tjGy
uroMvIlVs+cHMFof21VloKW24Khyw0Xi/ifDYgRria/wTNHna9SymKJWET6oUnsVW+b/nG5eWvm2
K3r/fl55uWIbKiKeJ/AhOuTwNqz2OLHIXIIKey+QU253NqYaW6gbZCPF7CSRzbat0aztRkQdtUby
CjsRmy5IkGDiAxjwJf7BdDadey/Y3D7Y3FdjegYRnabva+mkqnkV49YRm+JSpNhrt4GxjFNnJuwf
eiZBnkYcIMiPimyNItwQWr6ntPfA+3zEh26CKwVQ42Z3C2aLAoa+a58xjbZXPlPiFtIwMzbC1/TJ
4+ynykpaDmeBA1FPD8KQ4c9fK+kwfaaqkw2Ar7j/ag2WnPJkh1JS/8yuRBCOT5yKrbSMmOOCOyyb
TieRHsiSW1QtSO4ged8ut1dd3jn9UIlZoeq9RNK+/d+nI5Qy9xUCN/tgHyJLyizmPAlstfu6K5cw
8XJGkauGDPFmxgox4tuY70WIZ6GkLCLiuIHO4BfdGOfZIrhtiPpLeHyoprFwMF2UcLLot3CtAa2f
RWKXo908kB6lyJ4flyJpIrIP9z1QDjpgH+8z2ONpyC1OfCec1GSO8cd3ol1/Krz3OprZqfTctnOI
3Anx0+IkZHNvtSSKptUeidWf3IoK2shRCONzweIvwEx0XGoQnaNmxqhQ4jRPLuTuPlZt/4/Wy4xi
8np8PmyYrqE1m3tBkUc9vyMloWLAa1rNDBFiorI+nDMQ0MwmeICHQfZAEvYLkydDXaeuiygnOqmw
863RezR/zD+zRRKf2Amt5QhL/B/58StamA3XJrl8Uhd6yWyBYlcVfF8bhWYa5J+OcQQfmFzz9fjn
EoTHA7CveLvx8nHpLPZaPvakh/9MXu/jfzgNpFGFflChOY4etJoZJV/AdvmD5DlKY7ZtzWisxx9k
EBVk0eQ4wuG7jc9sRJzah1lsCG4LSl4TjBRYS9F7KZEOMODJpFs9gn3txNUkeT+YOs9TBF0FZ8up
eJNJeXPaFtgOK47k124cIxVlCQRyAKkgjqLRr9dOVKfqn/cn5eL4nZioOuPuk4FGE5+OeVoXK2dX
328xHI5+Ff5d8CFOpdNqIsUWb+vc8fJ3lK3bkLyJu+YB6xZo97zJePuhyY+2k7MQcMpqrEE2vrFG
TjVh48A665xcctY1uVCjUJ2q54O4txk8DomeKhcKRvpZXhDqegHayc5uQbPDwvc4doSk316poOpU
WGchyYlyBX9M5uKzn0txUVlfa6F1wsPNR738dZCgRdDt8stXhlyzfRYUpRko2SUyAIiJV6CC71zp
v1ycchHZHqZEUUO5Sy2/YZDK2t3sO5YS+fju9dnaLYkic0fI99g8oyU8pAezU8ykh7SoPJ5OL6qA
CkKEINWhs1o9Lg+sOkAvpv3c38uumDZZG/dNJXGhh/3OriXY1YX0YAdCIHqaC00n+SOShviUKz4P
/m272CMAcCODC23BLK63gU2uSKJIt2hsD0g2ySgp5aKIzYfPTSjjQHHDi5Izn0gdPjAnRf1Lmrnx
dOB4U0vyOwLujsHp429MOBjcXb2wDstQiMv36GtWuWMgoUY8fZJ/BHWjEJ07Dj7f43zzwU/Nk0cj
PJ/w8ebk6WDEUf5c9wZyqco81ITxrC1UE8JyEKuOzxjKB9EjwM+RAXmvvebsrHdx3EUNEhI5HfKb
GUUYznyCnXHo04Pzajtvw10Fp0ldOLt24b2/Vp1B7Yayz0xrW9c3NRLo4W9zR8fe+pyupG6tv0ak
0Qhg+P9GbcEHBFV3PPDdw/GEQ1m3eAfLwqllJrG8X2KuMOFCt+mORfWBtrYkfyFK062QloRi3uMz
onCPL8i32Oxmhbs5dbjs1GPdp0JdYoTmYcoX5+7HeTnEdBnwTeQRYnz8V8l7NudjImkKs2g54LY6
oWJQ1dbsAq1sOIsHRcpZldkpnZaavLtbVFhuMzGuVPD/PqKYW3exQgI19ooEWnOGXcd6mvDVrxfK
PecLtL6FCtVYy++61i9deXLNwujbBDJIRduKNRMS7EyuTphlp1kKoaMs2cYdNya3W0rbq55zuWpN
ySn0AavRni0NXJpjAswCFFIwQXicgAlmp4IcfwzGDSc9q/oZzUUOLbUhbjbwbJiKYPHnXCxkGbNg
AgImzNmrMmZtmWqfqZflAfFtT9Sx+lBiikibymZYxdvHPOfNz3NWc5wkE/W9Xe0KS11orB7WI5k+
mEsgYe2CokAlJzRkcSSpzSz0ZThWp1Uii1zLhyTTtu8LKgDmrNkvp9IAfpndzBr1AhT+wigPttLS
GGNGwtHo2mf4qE4IDfeHZbr1JyspCpdmEwkE/Ou/q/tenvhORlwSYlqkql7qc6ijt4fPBYZnsF+X
echZMRkH2E4VWK58IbjiVlzGP6hqr3671SOBAnSMjj+9A+W192CY2ARlVUk9lQ4sHDCxxsmekjc5
DmwjFcbxKu6f3cuF2VhPU+Fi8rG9T/cNr28n6/uzQEN4l1N3JvYZpw+CejqMEBp24/X6DqQuW0pM
n6x/9oRTiYThERlz96X/0fdyw1lxZ8/VCsf/Uhq5n6BVdhnksKgPSk6o9vvJt7B8P8SNqn+IXphr
QkoyeHH+G7CfOhx1Gt/SQ7vgWDCK5owalor/YNH7qHs1RANYj6VZ6uUj6gd5SMw9xpVlnvmTNK7u
G0CynqHWFHUHCO+mMNyIs6TQVmkCHvUxJYNWrxHLnZRXbZje6aNzudCi+r52IWQY77R58hJgp1AD
xuMd9Y1piKxrcSueaYkVm5MAmWJvndYBvSzhMijT2qeQdxokO7Wahc1HxCN2QwVLaH947iDBzRFj
i3FQhym+hxuMCEHPa0zUIziRTtkVWorMM+WpG96ZXyvFyKuQ4OSIJHSuZDXlcVSMO+IM5e5xCQpH
1uFr/6OYttMz0n1AkRkU0/T8WcvrvtLiT+CiKloDiLtmiq9Vm7ly+YkY0aUW2QePAV87hvaeWhU6
fdfPrLYuMtqcBD5sduw0Z7SjpUwI84rctq8033EOzFJbiAZB6w4Rkcnae66wqiyk17Ud8oLhrpLh
RycK7zPPs4jvSOHvY/lrklN336xsi4f5qSwMzbV7ySFXdRuo1zQ1BgE8/6KwqsnXZNeFno1KxNka
2XjeQ181Vdhg/2Jo15uEEqjXk039YtHdaoQHN6HzRYgAL/UXOXj3IB7cFmukwDkoxIcy1MXI1VB3
i+fYbOS+6DuwDpREd3thfpENR2I9Abz9DbeTbIoPfxUO7qTawu6ukE9A0pjWlgWBD139xEzq6qwJ
UA6TsXS/cHkNknzHaL2nYFi0pfGQnOEX5OQD4+HwkC2f5zLYHGfOHegIuvVGXiqCW4QFiOIdnnbm
O19k/00mwV7GDTACmKJxR6IbeufXgz4942wnOgcb9fYf2Fk7gSuu/Rbi53SwRUbeEtv2nyXeWDPI
YOL8zt1XW3zi79KZ6pYxQLyJEs6thT5975G8c+NHz5j59JqcUB7Kew+b1KyhBRn5DG4jH+BHmMLb
8HOrlfFRgNXsTJNph/m5cAjP2jUHHdT/QM7XBaz+Da4Fy2PKhIm/5i0jtkw5L92fr8VHnCzyKLfN
r2c+6s8wlivq2dSEL77aHFhldNaYlfvVaCNFUDHIeCCRuX/duCM7i1j5HNKl1hKETicWZtk+ONuw
kwT7IBCXQHkyhV5I8qxff8zu/5HKd5fl3zRFp3ymMgAyKLC7YlwF3LRAzNk6CZiNOX6gsTHfPHg5
iZKhZ7FEMxYv1tA3iOdrGHmsXuf+jxiu9SfQyBqcvirpvrq6Z32wJ6nqDU8kXge2Xnn1jgcsWRDE
cpK8qVwMjOXBuYUN97Bv2pZSG+dHuS3pYu0/5PdymIX73QVDpzJuEpsGLNgodvyOb4Pq/cDSxFF5
hOtmSAAVrCtk1bQPjxFFMDckO497fnkRUpVvQtgwKukIUaY9YQlgyrQHvfe3001NWEiYSnpbAo6n
LdAhO0cR5Ob48+G83rBr3+w+hURXu1TrobTLc+xeeaaNKiNWIVmDmn2TExsHjm/HlREnanksUB00
VDv1bIy7lAaKknjOuyM30vctsDdPSmlq8DKLc04FRQxSzyNrrtfUNpSCduufYeVzRZ4H6035sGNP
CgMcXTcGJ81wynIEJnE3L07DKunmFloheWBk0zN2YgshCjjt8JkB2hciWie/OeMj/XhnTQQi1MgS
ly8Do8g0lnsnj8iUzhhuzq6KJ7wJMw027SQ1iuD1ACFZIa7XbLKmPFpZ9IMPQt3bspbe2NNF1PdZ
MZICC4SE1GIlasyzxN7dw6qxsHdro3d1C/ee2mC1BKOfD2WSRxFEyKfQvPqmP+jQYQFjI35PdcQ+
bMKYrsTkmTNccdqTNX9+3adYtIXWO2nM6GxlwKoitXAEktIHMFpdW2ZCKfDYZ72G5vSEEiZin+wJ
LbFhAFdOoEwEli1oD59arBq3s7unZmcXb6UnNUuMQbe3UuTeka7kBGlvnhXjZD+rThG8eZg/MEvh
GNYZ4H5ORUHu8UuB9Ht8VJNRjMfOF/KFxYc1R+/+AAlkSRh/B+yDgajEExbn++jSMwy/N9u8aXmq
Rr9C/ePk865Gf5BSIRw9mIhDuSrLWsOivVzoaTp84dI+JDNixhaJ7odcNWGc+MBftDsVZb+LTsHB
U9Z+eW55iFXbFiVrV8PIYEUAvNFgSULpXCjTf+QDYtcp49H4D1fXaC1POiDx13+e1H04XihZ1Rl4
hpU9tzEclAif6Dp0wXIt2HBg0oYbTITJJlICSwLO5PRb2QS8qcwPzsQi4n8daOQpH4kaXpPXUdMj
sR7ArBh++fvhqR/DrlMLPtZC6aSqW3n0o/90iLyYFwOyyK+8inu0G27HSXKwjzxoe1itCDBRY/2w
pDewox0Vkdp6DMAG1+Gl5gTXHYpJXlPFTSZASajbyeJ9udfQDMvGgbIKYdqCO4Rd7EOrjd+bAdKC
1F5yYj6I4muFv+UWciUy9vophWChbjtF0KQtp8d/DWT9239olpmN1UVxyKF/XJLDF13CXqe2lVSC
pf5MpIfo6rIm3bqXhxBn2ynCIpnFuAVFeAs5zlckT09UbwCodh7u9ucVn3Eb2utTtdXNm6ODXTX+
P0/+az696j1X9kHH0LO7KUwF8NENCFb05L7VAlHA1PzCBJwpeORWQo7MZNiAOBG8xWdt48KmBwAr
ZyQJ8lc76mQWXJ19ST7Tf83zEL6DPV8/JeYa4npC+pbCeD+MknpK6X4QaG/NHkPpxf/4PXPUi0IP
Zc9mUhfMUcJ0WzhONj1HXTlTfeeA1dRM1siEgxWSQqKKkJAUJO/9ZWPhBp5QPYrM4pncE/7W9n3U
o5Za+K9qEYmbLvZpSCZy+5M0W0iWOozO3cnxQIvsmL0Ff2txrdoxk5ucA5t3CwXSiXn+Hn+lEg1S
w85zTUoFazN9B6roJzTI7R3DmhQQ5CEaDAsmMIxbZ3ROrcyJlVJbHBl4q+koVGpDDmD1ibLq5uT8
8C2eK7qsOmH8ZhMewyCAR/dc0b02cDCFq4/z5gyiJk8jNJP4UdksT14kh6aG4Rp1dtBtJJcLARY3
MkBRCK22EP1vRuBde3FTgVY18IFp7+Xf62/pH1rMRyfC3NqEvgEUoqRyFPAVl5dWQlAMogH9VgEc
AGcX3V0rKlAt7PGqMKjLWK4PXJ/tU7JXF4NVrSWXokRuKg3BcUWiV8wJLZBBTBw3ZZiS6zJTYqgv
MVdrpSmw7orf3xjX3WVy/OD6E9Ewh15b4+X5COg27CH4c+Bt2iiNp0630NFL+GTGJ/U4fiAPI5Fb
m/KCbCeJB2RfGzEQPU6WDCfvTb+JEAQYJU3V79pYipTwi3AGuXeaInsyZq96wEG3RmybokshGfDD
yjD47AjAxOnaPhFD+i1ZKzhfZ9T+E3vmwV3zuTJbFEk1tS2t2M294Yk6ruSP83cwQlSd1a0zpj+m
XhkvlZYu+OlYNX2vOdAB2EVob1Sq4xNLRVszpU10DNE/85f/YaUkAmnrvXtvR9zVcXGzqrjRPyvv
lvRfpq+XCmivjF3jFCOlJ5Jp6hfQlMkWL17Xfh+cBiBshd186hMpZyWPBAad2zo9nwtFSJZN8s7S
n19lRMp5moLsZifWC40SPJ7+nHdigH9eIrrRdztbBEV0K5/omNLnOHAKh9nCepivZIqHwcMwvSj0
n12kz/zUcy3mfDMQSDCB9Mhwk8xtdOwdF8Sc401o4yPPT6xFGcg5j+dqVrKmOIdVJgHk3IYbJ0Nq
8tHT9A8uxOSRxnUzEgr6DfV0m1pqRwFLBw7hCvaOBAlH2Awsgki6tSZG6MplZD5a/xgLjqY33CmX
OmPowo/f3Vh0f7/k3sPrtz57ucf3yHcaj0w8Nf/WHutYlDw4VRdPSQd2/X1D/lh0cQytD9z1YTl3
ZInvrJlGMMtY4jfYRdW78yeyxgeI91nEMDR7K+pGARUiRC3kSDnK0pEV2WzP9KAMVbZLtPo48fXG
Ql80+03MB3E+Lwj5SBeOZYru+lwtyHVy6N5K01IvMtci1QAjhDI1Cldb6vXR19JqboezP/bFNRrk
hnmxziD7VHn/t54+7157XEolJOcNlqacAOyN4XgicTHm6zeh2d6Hraqludg9V6a3DXe/MvYGDh4w
ZDdAI+gmBbIQbq9bsK8GLEzzl32H9Nq3UeAbiaf/kmfVBFp6Cvoq2uAmal1ymRxB5tevvgGR2stf
+LTfJ0icxu8q7crq/fHVxYafFZ0crSMqXEZ1Wql2kLpLTbehCcqBcekniX5iycmAeVBc0lYU8Qa8
aSvF+wNrbXrMVti8wWEI1Pgt84w9F+p/I99cEOaJ2hyqW2u3U3fqCY2d8hO4iE45aL92GC0Ogymy
GGMx02qOSpkW4Do12rRXlR+s+jRVz49VzLPFMqYqd1mbtV2eJse95MHXWuftJCy+dvy5zZKgRJj4
MLkDQ26S1Hy+OgR9SCmuGNnFCSxaIspJzdwvPQx6y/wgpTDORZHvrNXwA6301ABPAGLyC5yN/P/V
sT5lNR2G+RKihcHlJkeXD+DHfzzUMVxhbIQG4jmndSFwIrX40O6sFtR4J7BBdLN9k4qR/8KOE7DQ
bT3zoizKoMH5FLAxzdxVthBGMZmsoYTktLjkv5RA7xoyv6LM9crbxsD9ZMAsa39jO8U6/T/gA+ok
HiFTF00yVKQYHFIRs6ImDf/MkRcKuhn2ON9W9ND6pmGfIRo6hlzVNbxBvVeHmOl9olWcAPWgeLCx
Pbo7xco6iGEVbwPzjDTSLFwgXJilFw8uV31sPMRyg6PI5wpwElh+TBh5k30DqzJ8U6atZtSDgBfa
ut0aZxWL+Txbtvv7qmUHQtNiKmpgCx/mCCXGVIpqGpshrtgKEOU3Kc3VVaFmWuk1v8tWGjvL6btW
1LknRvx6Ir2nA2pk+NDoErG8+6F+hZJT5L66PafKBra/x3u63EroFhyvET6UVm314tjsY8lb6ual
yIIRtJnhX9RtkfqTIf41Qz7VqbNM4cp0xooIKPCCLHw9B77QLX2NAufyCFxiCiSJFrykC3Fy1+Bk
3yj2xnfEjVFjK89jJ/Gnan/D0EA7yHl3Tbb1EwK7sBLs7q/RTH+bHKBInGMjhzMnf1NrVB6wmKbO
HYtNDK4kt730SLYIMM5B5TJ6io/pCHJl5jqizWzoZGRytJahGOfQhwMGkCOpCfMIF7X0BpUK0hC+
/Oqkj3kjD6dPr2/HkrEPXLz9Vjf/uB45rw0EW7+h7TtJXOG54pUYU5rUiqCaG2GOmJPsio2gN66c
eLO9NVB9R/h/5NTk/ccHBDx4Mn0J6p4Tk8aWun9AIPxJ/41k2UvprMlaIbp9A2rBexS+RbZ/yLoE
tZzjNueTxRXMiupRsfcaB6kKGVZonAQd8Oh8Knw8mGe+kCKWLfMkfxKsq8o9TgmA0+oq8bKEgl6N
2iW99edTSzzqOlyB+A1BsGCAvYCiXZyMAx6cLKy095uvZUxLsmn5lUEeC/KQ/jqv3VoNWTVLRCOQ
137yzx4Kz8LhDL+fUv/ViRTnp8F/yJMyP3rGKGDVCwN0+zKH4nnNLsj2UT/20TGp52UIbbwaZT79
qZDM4o6X+nXMk9+BoyFkLAA7eUUbV6eobFqZN16wDCbClLeO80FOj7a/jMvXqea3Mf7JWYHqnTdq
vJGHI6visNJXHp6txwPt81ye0DmttFEeKswaHL/s6swjbwh4EWdwFoereTcYHeRwFtgz87ECIC/Q
2ruKim2XuwY/myPtNX0E7x4SS9sZerP/P+2lk/0y4xFVpbkYTByEI8do4GJ29ecYmKZJzGDm/CW+
UcIMT+Z7OTNvlicZV1auuw3i2uFHMLEkHVqRAo4AyLtkgF+f3HwrL7sFC9xq9Ye1YNf5cTkO9EMg
3heUGG9FWpFs5NKTJB/moyNbVcq+YfIvG0vWDO6YMg9hT/yRohvj2loaD80d3kCvlSfPivyfswt8
F0AQvAMGjvbClFgikQzMGGn7HctdMMtHgiSVXPlHGul3p60bz6+iW5OpHVyT6qe5SIF5QcBM3SxH
nMDJNze4UkQm4oRppDjdrV+rmhOFWSBXhDOyn6T05feb13fM44NfHq17c/m+xlf2KpzfG6+0A5CL
uAJJo+cCIg/ndUOEF5xo6ngMkNc0+TMebgOK+a5F1NhE631xLl6F1IW3Tk/BQ5ecCN1SO80qNMbg
zSP0X4QjugGcvxz/KJTy08E2GemP3oPhhNRfuiReJbRZ51J2Ev6pKwA//RiZYswmgkBvKEu8w57k
tphKSqaboyleS8meHr7f6NOrbGCHyCeVP5YTkGV6LcBFoXwhiA+a8Ka0Q7Ni38DZIORdOWdUbxSD
TiwV/qKnZacsoSOxqolKvItcc+6cNsNFMA80ekUoQd55ZAQOZ7i3kE1RgY8zE25tY9e8VxNEcGUU
oIM7uq1jtOA2IvU5iKeRJycs1gGHe5RNj+ekE9oOksUzihAQ/dPw84+wANYyx25crI2ZPrksr/Cm
PDH8PJIAT2WGF8YH1lTTDNhDPB003FkSRpAQEfCbLjjYYSonEjqNBxwSOqzgj7V8FuDg4lINvAJi
Tzz0Ae0aoXTYPL4z63flBOr/I3QR3Fb0rVchN2OHm2iOGAb3MRKneLdCSYoWfy3Nxft4+th4WOEe
RnQFMMFyOET5IkUCC83EeuBkK5euBeMxCH6557+rEBjmF7CZDiR5KJrecrw4PboJe3pIgfGFc01V
7ucy/LWxweBsOzOf9/gUIfMbljjNNbRsK0OQVwRpfuGF9NzD/zzO+Q4fH8/gp8ad8K7OpnFca/SB
B5eOFeL8G2gVguyTNl1OEQsZSb6+C5ZqUgEeKGf0NCp/Z7znSf1VYVPsnEdAh6jtoHjN7kni2P0t
yhZcxRtGm6kQYBIn6PdXud2lKeyEqrinCb4CjoXUu/ZZgAPJ4x66KiTtmfeQEARtOHjFnWg7vyBh
w9SFGnf8V7fuMs9Ow/aDTJ1w8tPqAmIx92/zM8c5e4b/+aLHzCm1MyNdxH427TZ8Oz+agsgW5VkW
UL8Fp7UMURPK+1gVM+tgCope8eDuliiYtHyMDAE959TMuFWWCdlTfkmn5FuH2sgBqvVqyEDFbF3Y
8NOvPW6KCGI4rVBgWSHxCs8dzxb6B2c6Pi3+V4OGSEiiXEpsPGVjlEyk+lDeQf3bPYtfImMc/frj
Btc7Y38RkVNoMBCUSjqBrj+rSAtBXv/uFZWQpIXSK70PLR/Nbxzzs1I1oGG6jwdYeepLt1ZRj1Fe
pJo8NJi1iINUsWeEdt/EXQN0huMXE099jwyUtVCppFkWdxh5OKw8v/Tmu3AcdtjpfZyy5QTP+qxX
kaRI37UeHH090+zwX6ahb5rqUIpphzTBJX82kYA3Gp5XoCuOIEbDMKmRnwh0xmaATxtiZPmQ53g9
9wwqvel6ktWljDmpTMYZ9ZZ4c2McyEB7AMUoSeKMRQfuzWSpy/ykXqz9x3CRo1MZHOW69mudTEn8
8wv7LLNJysx3lJlRlQN827hIrZCgtP3WsRVY62Yftx7qzTUV0yC8Wz76IuZsQyiUoz5r5oqlQkEF
9LNhzvh4jrO7Pp+5oDn2iVp7Z42up5W+pWTlO5c0tfNYKCe2yG6+Mrl2Retspw9zDNLU7IDbd82A
MArwdyLk8d6ZBrS6zxLMZG48gkGc8xtpl5ETydxQJ/lWEsG8xll0LxMbkCy5IexsKD32dmAfjKeN
OlkAFL3j7x55S8sv9GT1AC7XdP6AFra3g8mZPztS/TdCrh+HTtzMRlfj7he/u72j2O4kz9gN2mm+
M3J5i0wzxxIzN9XwWlX8J/XyHnZXpY7WFaL41kXaZk7XPPZvcu42zlsD8pUXOfmjzz8cUjk2e04X
Q46UHzoti0s88QwzlhNheiK8Aj4vyMZwkUKPTlURnHdeGktQ3nYCPkAaJs3JaR9Bs1gQXui65iG4
nPfQR6Wg/WOf8cRb6QqWw3KfKIHhUSGQ2j7xyyvJfz8EOsjhwAktZ0rKvGzPTk1yBteAqGNwMLrW
92z+oeznPaVj4iX4VxUmGTRQ4x6R9Hn1nZ3t+QnjhOz93FTQPx8K15NsLXVlmON0upn4BjIid8/T
UqTjuSaSOK/hJik2F1f4K1O9R1H4kLVRIzuUsZh7qF4g5Kf43oTSSzrz9BrSZSnpcG/16usk4/B+
fdXpGkDMYgrGIOSan6MsKBudAKl944jznNteVbg3xlLL1lRWHo+ov6G2CxdNTy676PLabkeLUcAp
7jmb15S54lY4meDp5wcJZTI57UbrBoGfqL7MGxjhRUYDGcISAEAQE+yJHoqMRZg809o6T0TGoloz
tCN0be0QLIGYegfW3lHDEuFAUEuSDCqmfRPre05ih6e7dS/HRU+vTCPnhGlUcs4AWhOTlTIRSVP7
Ejl6fSGvG2hLUSLJhdsvAoEgYF1QAtNWTVmGN8nRg0wtNLgrJc+Q4vF9Zgnq8WkR3ysUXDBhqLNv
d5GAuJp6ZfJw5Gg4+QjDBqpVjdKHSoJ1u22F22WHb0ycf8iY3hkBC9xLLOdYClNzeDeoGnTmVgzd
kvu+hd21Cs2xNLpOQRoAvWyeDYGE5ODS+vYYEnoVLEf/xe/OM4l0KcJcfiahbhCCmcbbzFeoWQjK
8MVktjV2nrRWQLVcj/YI5y9PkAobbQG70H1OQmOaP//HE/2AYjXhQksr3/hyiVY4+RGBooDPbNbf
Oby5Oe5anGEI7y1duZkVvR5IkLbCZWt+QL2UW3wjcPlaE2j56k8ww1DaVEGXt3msGRdVeQ9Dtifj
Z1qog48j8naWdFGcqWtdKKBjwTngFWdqW/HmZfudGFQkPCyD+JxYKD9PL6JVGqkKUCdprOUAAcWP
xcJFQIcm6n49AcPwH4pqhdePAjimJNYJMeLDjnrkqDlVJbjAgpoDFFUvv/CJk/S/qeCokCVDwyX1
rfFSito7QxxSFqLmUNXZhvs+VhPSKHpOJlM/YZ5IVxqrdtynyjnumdCHeiTX37qnCCOCSl4htbzA
4cw4694zYvwLqYIDjn//hHdbOsKLnlkW4WiYUJsEJlTabDWtOxBt8PKnEvBcar2ORg==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_GTWIZARD is
  port (
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_1 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_2 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    mmcm_reset : out STD_LOGIC;
    data_in : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gtxe2_i_4 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_5 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_7 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_8 : in STD_LOGIC;
    gtxe2_i_9 : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    data_out : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_GTWIZARD;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_GTWIZARD is
begin
U0: entity work.gig_ethernet_pcs_pma_0_GTWIZARD_init
     port map (
      D(1 downto 0) => D(1 downto 0),
      Q(15 downto 0) => Q(15 downto 0),
      RXBUFSTATUS(0) => RXBUFSTATUS(0),
      RXPD(0) => RXPD(0),
      TXBUFSTATUS(0) => TXBUFSTATUS(0),
      TXPD(0) => TXPD(0),
      data_in => data_in,
      data_out => data_out,
      data_sync_reg1 => data_sync_reg1,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i(15 downto 0) => gtxe2_i(15 downto 0),
      gtxe2_i_0(1 downto 0) => gtxe2_i_0(1 downto 0),
      gtxe2_i_1(1 downto 0) => gtxe2_i_1(1 downto 0),
      gtxe2_i_2(1 downto 0) => gtxe2_i_2(1 downto 0),
      gtxe2_i_3(1 downto 0) => gtxe2_i_3(1 downto 0),
      gtxe2_i_4 => gtxe2_i_4,
      gtxe2_i_5(1 downto 0) => gtxe2_i_5(1 downto 0),
      gtxe2_i_6(1 downto 0) => gtxe2_i_6(1 downto 0),
      gtxe2_i_7(1 downto 0) => gtxe2_i_7(1 downto 0),
      gtxe2_i_8 => gtxe2_i_8,
      gtxe2_i_9 => gtxe2_i_9,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0),
      reset => reset,
      reset_out => reset_out,
      rx_fsm_reset_done_int_reg => rx_fsm_reset_done_int_reg,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_transceiver is
  port (
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    rxchariscomma : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxcharisk : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxdisperr : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxnotintable : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxbufstatus : out STD_LOGIC_VECTOR ( 0 to 0 );
    txbuferr : out STD_LOGIC;
    mmcm_reset : out STD_LOGIC;
    data_in : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \rxdata_reg[7]_0\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    gtxe2_i : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    powerdown : in STD_LOGIC;
    reset_sync5 : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    txchardispval_reg_reg_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    txcharisk_reg_reg_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    status_vector : in STD_LOGIC_VECTOR ( 0 to 0 );
    enablealign : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    \txdata_reg_reg[7]_0\ : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end gig_ethernet_pcs_pma_0_transceiver;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_transceiver is
  signal data_valid_reg2 : STD_LOGIC;
  signal encommaalign_int : STD_LOGIC;
  signal gtwizard_inst_n_6 : STD_LOGIC;
  signal gtwizard_inst_n_7 : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal reset : STD_LOGIC;
  signal rxchariscomma_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxchariscomma_i_1_n_0 : STD_LOGIC;
  signal rxchariscomma_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxchariscomma_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxcharisk_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxcharisk_i_1_n_0 : STD_LOGIC;
  signal rxcharisk_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxcharisk_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxclkcorcnt_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxclkcorcnt_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxclkcorcnt_reg : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxdata[0]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[1]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[2]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[3]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[4]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[5]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[6]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[7]_i_1_n_0\ : STD_LOGIC;
  signal rxdata_double : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rxdata_int : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rxdata_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rxdisperr_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxdisperr_i_1_n_0 : STD_LOGIC;
  signal rxdisperr_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxdisperr_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxnotintable_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxnotintable_i_1_n_0 : STD_LOGIC;
  signal rxnotintable_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxnotintable_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxpowerdown : STD_LOGIC;
  signal rxpowerdown_double : STD_LOGIC;
  signal \rxpowerdown_reg__0\ : STD_LOGIC;
  signal rxreset_int : STD_LOGIC;
  signal toggle : STD_LOGIC;
  signal toggle_i_1_n_0 : STD_LOGIC;
  signal toggle_rx : STD_LOGIC;
  signal toggle_rx_i_1_n_0 : STD_LOGIC;
  signal txbufstatus_reg : STD_LOGIC_VECTOR ( 1 to 1 );
  signal txchardispmode_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispmode_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispmode_reg : STD_LOGIC;
  signal txchardispval_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispval_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispval_reg : STD_LOGIC;
  signal txcharisk_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txcharisk_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txcharisk_reg : STD_LOGIC;
  signal txdata_double : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal txdata_int : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal txdata_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal txpowerdown : STD_LOGIC;
  signal txpowerdown_double : STD_LOGIC;
  signal \txpowerdown_reg__0\ : STD_LOGIC;
  signal txreset_int : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of rxchariscomma_i_1 : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of rxcharisk_i_1 : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \rxdata[0]_i_1\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \rxdata[1]_i_1\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \rxdata[2]_i_1\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \rxdata[3]_i_1\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \rxdata[4]_i_1\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \rxdata[5]_i_1\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \rxdata[6]_i_1\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \rxdata[7]_i_1\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of rxdisperr_i_1 : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of rxnotintable_i_1 : label is "soft_lutpair79";
begin
gtwizard_inst: entity work.gig_ethernet_pcs_pma_0_GTWIZARD
     port map (
      D(1 downto 0) => rxclkcorcnt_int(1 downto 0),
      Q(15 downto 0) => txdata_int(15 downto 0),
      RXBUFSTATUS(0) => gtwizard_inst_n_7,
      RXPD(0) => rxpowerdown,
      TXBUFSTATUS(0) => gtwizard_inst_n_6,
      TXPD(0) => txpowerdown,
      data_in => data_in,
      data_out => data_valid_reg2,
      data_sync_reg1 => data_sync_reg1,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i(15 downto 0) => rxdata_int(15 downto 0),
      gtxe2_i_0(1 downto 0) => rxchariscomma_int(1 downto 0),
      gtxe2_i_1(1 downto 0) => rxcharisk_int(1 downto 0),
      gtxe2_i_2(1 downto 0) => rxdisperr_int(1 downto 0),
      gtxe2_i_3(1 downto 0) => rxnotintable_int(1 downto 0),
      gtxe2_i_4 => gtxe2_i,
      gtxe2_i_5(1 downto 0) => txchardispmode_int(1 downto 0),
      gtxe2_i_6(1 downto 0) => txchardispval_int(1 downto 0),
      gtxe2_i_7(1 downto 0) => txcharisk_int(1 downto 0),
      gtxe2_i_8 => rxreset_int,
      gtxe2_i_9 => txreset_int,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0),
      reset => reset,
      reset_out => encommaalign_int,
      rx_fsm_reset_done_int_reg => rx_fsm_reset_done_int_reg,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
reclock_encommaalign: entity work.gig_ethernet_pcs_pma_0_reset_sync
     port map (
      CLK => CLK,
      enablealign => enablealign,
      reset_out => encommaalign_int
    );
reclock_rxreset: entity work.gig_ethernet_pcs_pma_0_reset_sync_1
     port map (
      SR(0) => SR(0),
      independent_clock_bufg => independent_clock_bufg,
      reset_out => rxreset_int
    );
reclock_txreset: entity work.gig_ethernet_pcs_pma_0_reset_sync_2
     port map (
      independent_clock_bufg => independent_clock_bufg,
      reset_out => txreset_int,
      reset_sync5_0(0) => reset_sync5(0)
    );
reset_wtd_timer: entity work.gig_ethernet_pcs_pma_0_reset_wtd_timer
     port map (
      data_out => data_valid_reg2,
      independent_clock_bufg => independent_clock_bufg,
      reset => reset
    );
rxbuferr_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => p_0_in,
      Q => rxbufstatus(0),
      R => '0'
    );
\rxbufstatus_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => gtwizard_inst_n_7,
      Q => p_0_in,
      R => '0'
    );
\rxchariscomma_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxchariscomma_reg__0\(0),
      Q => rxchariscomma_double(0),
      R => SR(0)
    );
\rxchariscomma_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxchariscomma_reg__0\(1),
      Q => rxchariscomma_double(1),
      R => SR(0)
    );
rxchariscomma_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxchariscomma_double(1),
      I1 => toggle_rx,
      I2 => rxchariscomma_double(0),
      O => rxchariscomma_i_1_n_0
    );
rxchariscomma_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxchariscomma_i_1_n_0,
      Q => rxchariscomma(0),
      R => SR(0)
    );
\rxchariscomma_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxchariscomma_int(0),
      Q => \rxchariscomma_reg__0\(0),
      R => '0'
    );
\rxchariscomma_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxchariscomma_int(1),
      Q => \rxchariscomma_reg__0\(1),
      R => '0'
    );
\rxcharisk_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxcharisk_reg__0\(0),
      Q => rxcharisk_double(0),
      R => SR(0)
    );
\rxcharisk_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxcharisk_reg__0\(1),
      Q => rxcharisk_double(1),
      R => SR(0)
    );
rxcharisk_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxcharisk_double(1),
      I1 => toggle_rx,
      I2 => rxcharisk_double(0),
      O => rxcharisk_i_1_n_0
    );
rxcharisk_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxcharisk_i_1_n_0,
      Q => rxcharisk(0),
      R => SR(0)
    );
\rxcharisk_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxcharisk_int(0),
      Q => \rxcharisk_reg__0\(0),
      R => '0'
    );
\rxcharisk_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxcharisk_int(1),
      Q => \rxcharisk_reg__0\(1),
      R => '0'
    );
\rxclkcorcnt_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxclkcorcnt_reg(0),
      Q => rxclkcorcnt_double(0),
      R => SR(0)
    );
\rxclkcorcnt_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxclkcorcnt_reg(1),
      Q => rxclkcorcnt_double(1),
      R => SR(0)
    );
\rxclkcorcnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxclkcorcnt_double(0),
      Q => Q(0),
      R => SR(0)
    );
\rxclkcorcnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxclkcorcnt_double(1),
      Q => Q(1),
      R => SR(0)
    );
\rxclkcorcnt_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxclkcorcnt_int(0),
      Q => rxclkcorcnt_reg(0),
      R => '0'
    );
\rxclkcorcnt_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxclkcorcnt_int(1),
      Q => rxclkcorcnt_reg(1),
      R => '0'
    );
\rxdata[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(8),
      I1 => toggle_rx,
      I2 => rxdata_double(0),
      O => \rxdata[0]_i_1_n_0\
    );
\rxdata[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(9),
      I1 => toggle_rx,
      I2 => rxdata_double(1),
      O => \rxdata[1]_i_1_n_0\
    );
\rxdata[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(10),
      I1 => toggle_rx,
      I2 => rxdata_double(2),
      O => \rxdata[2]_i_1_n_0\
    );
\rxdata[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(11),
      I1 => toggle_rx,
      I2 => rxdata_double(3),
      O => \rxdata[3]_i_1_n_0\
    );
\rxdata[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(12),
      I1 => toggle_rx,
      I2 => rxdata_double(4),
      O => \rxdata[4]_i_1_n_0\
    );
\rxdata[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(13),
      I1 => toggle_rx,
      I2 => rxdata_double(5),
      O => \rxdata[5]_i_1_n_0\
    );
\rxdata[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(14),
      I1 => toggle_rx,
      I2 => rxdata_double(6),
      O => \rxdata[6]_i_1_n_0\
    );
\rxdata[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(15),
      I1 => toggle_rx,
      I2 => rxdata_double(7),
      O => \rxdata[7]_i_1_n_0\
    );
\rxdata_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(0),
      Q => rxdata_double(0),
      R => SR(0)
    );
\rxdata_double_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(10),
      Q => rxdata_double(10),
      R => SR(0)
    );
\rxdata_double_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(11),
      Q => rxdata_double(11),
      R => SR(0)
    );
\rxdata_double_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(12),
      Q => rxdata_double(12),
      R => SR(0)
    );
\rxdata_double_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(13),
      Q => rxdata_double(13),
      R => SR(0)
    );
\rxdata_double_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(14),
      Q => rxdata_double(14),
      R => SR(0)
    );
\rxdata_double_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(15),
      Q => rxdata_double(15),
      R => SR(0)
    );
\rxdata_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(1),
      Q => rxdata_double(1),
      R => SR(0)
    );
\rxdata_double_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(2),
      Q => rxdata_double(2),
      R => SR(0)
    );
\rxdata_double_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(3),
      Q => rxdata_double(3),
      R => SR(0)
    );
\rxdata_double_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(4),
      Q => rxdata_double(4),
      R => SR(0)
    );
\rxdata_double_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(5),
      Q => rxdata_double(5),
      R => SR(0)
    );
\rxdata_double_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(6),
      Q => rxdata_double(6),
      R => SR(0)
    );
\rxdata_double_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(7),
      Q => rxdata_double(7),
      R => SR(0)
    );
\rxdata_double_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(8),
      Q => rxdata_double(8),
      R => SR(0)
    );
\rxdata_double_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(9),
      Q => rxdata_double(9),
      R => SR(0)
    );
\rxdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[0]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(0),
      R => SR(0)
    );
\rxdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[1]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(1),
      R => SR(0)
    );
\rxdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[2]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(2),
      R => SR(0)
    );
\rxdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[3]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(3),
      R => SR(0)
    );
\rxdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[4]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(4),
      R => SR(0)
    );
\rxdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[5]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(5),
      R => SR(0)
    );
\rxdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[6]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(6),
      R => SR(0)
    );
\rxdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[7]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(7),
      R => SR(0)
    );
\rxdata_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(0),
      Q => rxdata_reg(0),
      R => '0'
    );
\rxdata_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(10),
      Q => rxdata_reg(10),
      R => '0'
    );
\rxdata_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(11),
      Q => rxdata_reg(11),
      R => '0'
    );
\rxdata_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(12),
      Q => rxdata_reg(12),
      R => '0'
    );
\rxdata_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(13),
      Q => rxdata_reg(13),
      R => '0'
    );
\rxdata_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(14),
      Q => rxdata_reg(14),
      R => '0'
    );
\rxdata_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(15),
      Q => rxdata_reg(15),
      R => '0'
    );
\rxdata_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(1),
      Q => rxdata_reg(1),
      R => '0'
    );
\rxdata_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(2),
      Q => rxdata_reg(2),
      R => '0'
    );
\rxdata_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(3),
      Q => rxdata_reg(3),
      R => '0'
    );
\rxdata_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(4),
      Q => rxdata_reg(4),
      R => '0'
    );
\rxdata_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(5),
      Q => rxdata_reg(5),
      R => '0'
    );
\rxdata_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(6),
      Q => rxdata_reg(6),
      R => '0'
    );
\rxdata_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(7),
      Q => rxdata_reg(7),
      R => '0'
    );
\rxdata_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(8),
      Q => rxdata_reg(8),
      R => '0'
    );
\rxdata_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(9),
      Q => rxdata_reg(9),
      R => '0'
    );
\rxdisperr_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxdisperr_reg__0\(0),
      Q => rxdisperr_double(0),
      R => SR(0)
    );
\rxdisperr_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxdisperr_reg__0\(1),
      Q => rxdisperr_double(1),
      R => SR(0)
    );
rxdisperr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdisperr_double(1),
      I1 => toggle_rx,
      I2 => rxdisperr_double(0),
      O => rxdisperr_i_1_n_0
    );
rxdisperr_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxdisperr_i_1_n_0,
      Q => rxdisperr(0),
      R => SR(0)
    );
\rxdisperr_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdisperr_int(0),
      Q => \rxdisperr_reg__0\(0),
      R => '0'
    );
\rxdisperr_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdisperr_int(1),
      Q => \rxdisperr_reg__0\(1),
      R => '0'
    );
\rxnotintable_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxnotintable_reg__0\(0),
      Q => rxnotintable_double(0),
      R => SR(0)
    );
\rxnotintable_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxnotintable_reg__0\(1),
      Q => rxnotintable_double(1),
      R => SR(0)
    );
rxnotintable_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxnotintable_double(1),
      I1 => toggle_rx,
      I2 => rxnotintable_double(0),
      O => rxnotintable_i_1_n_0
    );
rxnotintable_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxnotintable_i_1_n_0,
      Q => rxnotintable(0),
      R => SR(0)
    );
\rxnotintable_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxnotintable_int(0),
      Q => \rxnotintable_reg__0\(0),
      R => '0'
    );
\rxnotintable_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxnotintable_int(1),
      Q => \rxnotintable_reg__0\(1),
      R => '0'
    );
rxpowerdown_double_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxpowerdown_reg__0\,
      Q => rxpowerdown_double,
      R => SR(0)
    );
rxpowerdown_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gtxe2_i,
      CE => '1',
      D => rxpowerdown_double,
      Q => rxpowerdown,
      R => '0'
    );
rxpowerdown_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => powerdown,
      Q => \rxpowerdown_reg__0\,
      R => SR(0)
    );
sync_block_data_valid: entity work.gig_ethernet_pcs_pma_0_sync_block_3
     port map (
      data_out => data_valid_reg2,
      independent_clock_bufg => independent_clock_bufg,
      status_vector(0) => status_vector(0)
    );
toggle_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => toggle,
      O => toggle_i_1_n_0
    );
toggle_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => toggle_i_1_n_0,
      Q => toggle,
      R => reset_sync5(0)
    );
toggle_rx_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => toggle_rx,
      O => toggle_rx_i_1_n_0
    );
toggle_rx_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => toggle_rx_i_1_n_0,
      Q => toggle_rx,
      R => SR(0)
    );
txbuferr_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => txbufstatus_reg(1),
      Q => txbuferr,
      R => '0'
    );
\txbufstatus_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => gtwizard_inst_n_6,
      Q => txbufstatus_reg(1),
      R => '0'
    );
\txchardispmode_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txchardispmode_reg,
      Q => txchardispmode_double(0),
      R => reset_sync5(0)
    );
\txchardispmode_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => D(0),
      Q => txchardispmode_double(1),
      R => reset_sync5(0)
    );
\txchardispmode_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispmode_double(0),
      Q => txchardispmode_int(0),
      R => '0'
    );
\txchardispmode_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispmode_double(1),
      Q => txchardispmode_int(1),
      R => '0'
    );
txchardispmode_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => txchardispmode_reg,
      R => reset_sync5(0)
    );
\txchardispval_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txchardispval_reg,
      Q => txchardispval_double(0),
      R => reset_sync5(0)
    );
\txchardispval_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txchardispval_reg_reg_0(0),
      Q => txchardispval_double(1),
      R => reset_sync5(0)
    );
\txchardispval_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispval_double(0),
      Q => txchardispval_int(0),
      R => '0'
    );
\txchardispval_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispval_double(1),
      Q => txchardispval_int(1),
      R => '0'
    );
txchardispval_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => txchardispval_reg_reg_0(0),
      Q => txchardispval_reg,
      R => reset_sync5(0)
    );
\txcharisk_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txcharisk_reg,
      Q => txcharisk_double(0),
      R => reset_sync5(0)
    );
\txcharisk_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txcharisk_reg_reg_0(0),
      Q => txcharisk_double(1),
      R => reset_sync5(0)
    );
\txcharisk_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txcharisk_double(0),
      Q => txcharisk_int(0),
      R => '0'
    );
\txcharisk_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txcharisk_double(1),
      Q => txcharisk_int(1),
      R => '0'
    );
txcharisk_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => txcharisk_reg_reg_0(0),
      Q => txcharisk_reg,
      R => reset_sync5(0)
    );
\txdata_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(0),
      Q => txdata_double(0),
      R => reset_sync5(0)
    );
\txdata_double_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(2),
      Q => txdata_double(10),
      R => reset_sync5(0)
    );
\txdata_double_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(3),
      Q => txdata_double(11),
      R => reset_sync5(0)
    );
\txdata_double_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(4),
      Q => txdata_double(12),
      R => reset_sync5(0)
    );
\txdata_double_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(5),
      Q => txdata_double(13),
      R => reset_sync5(0)
    );
\txdata_double_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(6),
      Q => txdata_double(14),
      R => reset_sync5(0)
    );
\txdata_double_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(7),
      Q => txdata_double(15),
      R => reset_sync5(0)
    );
\txdata_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(1),
      Q => txdata_double(1),
      R => reset_sync5(0)
    );
\txdata_double_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(2),
      Q => txdata_double(2),
      R => reset_sync5(0)
    );
\txdata_double_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(3),
      Q => txdata_double(3),
      R => reset_sync5(0)
    );
\txdata_double_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(4),
      Q => txdata_double(4),
      R => reset_sync5(0)
    );
\txdata_double_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(5),
      Q => txdata_double(5),
      R => reset_sync5(0)
    );
\txdata_double_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(6),
      Q => txdata_double(6),
      R => reset_sync5(0)
    );
\txdata_double_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(7),
      Q => txdata_double(7),
      R => reset_sync5(0)
    );
\txdata_double_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(0),
      Q => txdata_double(8),
      R => reset_sync5(0)
    );
\txdata_double_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(1),
      Q => txdata_double(9),
      R => reset_sync5(0)
    );
\txdata_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(0),
      Q => txdata_int(0),
      R => '0'
    );
\txdata_int_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(10),
      Q => txdata_int(10),
      R => '0'
    );
\txdata_int_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(11),
      Q => txdata_int(11),
      R => '0'
    );
\txdata_int_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(12),
      Q => txdata_int(12),
      R => '0'
    );
\txdata_int_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(13),
      Q => txdata_int(13),
      R => '0'
    );
\txdata_int_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(14),
      Q => txdata_int(14),
      R => '0'
    );
\txdata_int_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(15),
      Q => txdata_int(15),
      R => '0'
    );
\txdata_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(1),
      Q => txdata_int(1),
      R => '0'
    );
\txdata_int_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(2),
      Q => txdata_int(2),
      R => '0'
    );
\txdata_int_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(3),
      Q => txdata_int(3),
      R => '0'
    );
\txdata_int_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(4),
      Q => txdata_int(4),
      R => '0'
    );
\txdata_int_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(5),
      Q => txdata_int(5),
      R => '0'
    );
\txdata_int_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(6),
      Q => txdata_int(6),
      R => '0'
    );
\txdata_int_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(7),
      Q => txdata_int(7),
      R => '0'
    );
\txdata_int_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(8),
      Q => txdata_int(8),
      R => '0'
    );
\txdata_int_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(9),
      Q => txdata_int(9),
      R => '0'
    );
\txdata_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(0),
      Q => txdata_reg(0),
      R => reset_sync5(0)
    );
\txdata_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(1),
      Q => txdata_reg(1),
      R => reset_sync5(0)
    );
\txdata_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(2),
      Q => txdata_reg(2),
      R => reset_sync5(0)
    );
\txdata_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(3),
      Q => txdata_reg(3),
      R => reset_sync5(0)
    );
\txdata_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(4),
      Q => txdata_reg(4),
      R => reset_sync5(0)
    );
\txdata_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(5),
      Q => txdata_reg(5),
      R => reset_sync5(0)
    );
\txdata_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(6),
      Q => txdata_reg(6),
      R => reset_sync5(0)
    );
\txdata_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(7),
      Q => txdata_reg(7),
      R => reset_sync5(0)
    );
txpowerdown_double_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \txpowerdown_reg__0\,
      Q => txpowerdown_double,
      R => reset_sync5(0)
    );
txpowerdown_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gtxe2_i,
      CE => '1',
      D => txpowerdown_double,
      Q => txpowerdown,
      R => '0'
    );
txpowerdown_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => powerdown,
      Q => \txpowerdown_reg__0\,
      R => reset_sync5(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_block is
  port (
    gmii_rxd : out STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_rx_dv : out STD_LOGIC;
    gmii_rx_er : out STD_LOGIC;
    gmii_isolate : out STD_LOGIC;
    status_vector : out STD_LOGIC_VECTOR ( 6 downto 0 );
    resetdone : out STD_LOGIC;
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    mmcm_reset : out STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    signal_detect : in STD_LOGIC;
    CLK : in STD_LOGIC;
    data_in : in STD_LOGIC;
    gmii_txd : in STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_tx_en : in STD_LOGIC;
    gmii_tx_er : in STD_LOGIC;
    configuration_vector : in STD_LOGIC_VECTOR ( 2 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    gtxe2_i : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_block;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_block is
  signal enablealign : STD_LOGIC;
  signal mgt_rx_reset : STD_LOGIC;
  signal mgt_tx_reset : STD_LOGIC;
  signal powerdown : STD_LOGIC;
  signal \^resetdone\ : STD_LOGIC;
  signal rx_reset_done_i : STD_LOGIC;
  signal rxbuferr : STD_LOGIC;
  signal rxchariscomma : STD_LOGIC;
  signal rxcharisk : STD_LOGIC;
  signal rxclkcorcnt : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxdata : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal rxdisperr : STD_LOGIC;
  signal rxnotintable : STD_LOGIC;
  signal \^status_vector\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal transceiver_inst_n_11 : STD_LOGIC;
  signal transceiver_inst_n_12 : STD_LOGIC;
  signal txbuferr : STD_LOGIC;
  signal txchardispmode : STD_LOGIC;
  signal txchardispval : STD_LOGIC;
  signal txcharisk : STD_LOGIC;
  signal txdata : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 7 );
  signal NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  attribute B_SHIFTER_ADDR : string;
  attribute B_SHIFTER_ADDR of gig_ethernet_pcs_pma_0_core : label is "10'b0101001110";
  attribute C_1588 : integer;
  attribute C_1588 of gig_ethernet_pcs_pma_0_core : label is 0;
  attribute C_2_5G : string;
  attribute C_2_5G of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_COMPONENT_NAME : string;
  attribute C_COMPONENT_NAME of gig_ethernet_pcs_pma_0_core : label is "gig_ethernet_pcs_pma_0";
  attribute C_DYNAMIC_SWITCHING : string;
  attribute C_DYNAMIC_SWITCHING of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_ELABORATION_TRANSIENT_DIR : string;
  attribute C_ELABORATION_TRANSIENT_DIR of gig_ethernet_pcs_pma_0_core : label is "BlankString";
  attribute C_FAMILY : string;
  attribute C_FAMILY of gig_ethernet_pcs_pma_0_core : label is "kintex7";
  attribute C_HAS_AN : string;
  attribute C_HAS_AN of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_HAS_AXIL : string;
  attribute C_HAS_AXIL of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_HAS_MDIO : string;
  attribute C_HAS_MDIO of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_HAS_TEMAC : string;
  attribute C_HAS_TEMAC of gig_ethernet_pcs_pma_0_core : label is "TRUE";
  attribute C_IS_SGMII : string;
  attribute C_IS_SGMII of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_RX_GMII_CLK : string;
  attribute C_RX_GMII_CLK of gig_ethernet_pcs_pma_0_core : label is "TXOUTCLK";
  attribute C_SGMII_FABRIC_BUFFER : string;
  attribute C_SGMII_FABRIC_BUFFER of gig_ethernet_pcs_pma_0_core : label is "TRUE";
  attribute C_SGMII_PHY_MODE : string;
  attribute C_SGMII_PHY_MODE of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_USE_LVDS : string;
  attribute C_USE_LVDS of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_USE_TBI : string;
  attribute C_USE_TBI of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_USE_TRANSCEIVER : string;
  attribute C_USE_TRANSCEIVER of gig_ethernet_pcs_pma_0_core : label is "TRUE";
  attribute GT_RX_BYTE_WIDTH : integer;
  attribute GT_RX_BYTE_WIDTH of gig_ethernet_pcs_pma_0_core : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of gig_ethernet_pcs_pma_0_core : label is "soft";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of gig_ethernet_pcs_pma_0_core : label is "yes";
  attribute is_du_within_envelope : string;
  attribute is_du_within_envelope of gig_ethernet_pcs_pma_0_core : label is "true";
begin
  resetdone <= \^resetdone\;
  status_vector(6 downto 0) <= \^status_vector\(6 downto 0);
gig_ethernet_pcs_pma_0_core: entity work.gig_ethernet_pcs_pma_0_gig_ethernet_pcs_pma_v16_2_15
     port map (
      an_adv_config_val => '0',
      an_adv_config_vector(15 downto 0) => B"0000000000000000",
      an_enable => NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED,
      an_interrupt => NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED,
      an_restart_config => '0',
      basex_or_sgmii => '0',
      configuration_valid => '0',
      configuration_vector(4) => '0',
      configuration_vector(3 downto 1) => configuration_vector(2 downto 0),
      configuration_vector(0) => '0',
      correction_timer(63 downto 0) => B"0000000000000000000000000000000000000000000000000000000000000000",
      dcm_locked => data_in,
      drp_daddr(9 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED(9 downto 0),
      drp_dclk => '0',
      drp_den => NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED,
      drp_di(15 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED(15 downto 0),
      drp_do(15 downto 0) => B"0000000000000000",
      drp_drdy => '0',
      drp_dwe => NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED,
      drp_gnt => '0',
      drp_req => NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED,
      en_cdet => NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED,
      enablealign => enablealign,
      ewrap => NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED,
      gmii_isolate => gmii_isolate,
      gmii_rx_dv => gmii_rx_dv,
      gmii_rx_er => gmii_rx_er,
      gmii_rxd(7 downto 0) => gmii_rxd(7 downto 0),
      gmii_tx_en => gmii_tx_en,
      gmii_tx_er => gmii_tx_er,
      gmii_txd(7 downto 0) => gmii_txd(7 downto 0),
      gtx_clk => '0',
      link_timer_basex(9 downto 0) => B"0000000000",
      link_timer_sgmii(9 downto 0) => B"0000000000",
      link_timer_value(9 downto 0) => B"0000000000",
      loc_ref => NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED,
      mdc => '0',
      mdio_in => '0',
      mdio_out => NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED,
      mdio_tri => NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED,
      mgt_rx_reset => mgt_rx_reset,
      mgt_tx_reset => mgt_tx_reset,
      phyad(4 downto 0) => B"00000",
      pma_rx_clk0 => '0',
      pma_rx_clk1 => '0',
      powerdown => powerdown,
      reset => \out\(0),
      reset_done => \^resetdone\,
      rx_code_group0(9 downto 0) => B"0000000000",
      rx_code_group1(9 downto 0) => B"0000000000",
      rx_gt_nominal_latency(15 downto 0) => B"0000000011111000",
      rxbufstatus(1) => rxbuferr,
      rxbufstatus(0) => '0',
      rxchariscomma(0) => rxchariscomma,
      rxcharisk(0) => rxcharisk,
      rxclkcorcnt(2) => '0',
      rxclkcorcnt(1 downto 0) => rxclkcorcnt(1 downto 0),
      rxdata(7 downto 0) => rxdata(7 downto 0),
      rxdisperr(0) => rxdisperr,
      rxnotintable(0) => rxnotintable,
      rxphy_correction_timer(63 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED(63 downto 0),
      rxphy_ns_field(31 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED(31 downto 0),
      rxphy_s_field(47 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED(47 downto 0),
      rxrecclk => '0',
      rxrundisp(0) => '0',
      s_axi_aclk => '0',
      s_axi_araddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_arready => NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED,
      s_axi_arvalid => '0',
      s_axi_awaddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_awready => NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED,
      s_axi_awvalid => '0',
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_bvalid => NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED,
      s_axi_rdata(31 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED(31 downto 0),
      s_axi_resetn => '0',
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_rvalid => NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED,
      s_axi_wdata(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_wready => NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED,
      s_axi_wvalid => '0',
      signal_detect => signal_detect,
      speed_is_100 => '0',
      speed_is_10_100 => '0',
      speed_selection(1 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED(1 downto 0),
      status_vector(15 downto 7) => NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED(15 downto 7),
      status_vector(6 downto 0) => \^status_vector\(6 downto 0),
      systemtimer_ns_field(31 downto 0) => B"00000000000000000000000000000000",
      systemtimer_s_field(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      tx_code_group(9 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED(9 downto 0),
      txbuferr => txbuferr,
      txchardispmode => txchardispmode,
      txchardispval => txchardispval,
      txcharisk => txcharisk,
      txdata(7 downto 0) => txdata(7 downto 0),
      userclk => '0',
      userclk2 => CLK
    );
sync_block_rx_reset_done: entity work.gig_ethernet_pcs_pma_0_sync_block
     port map (
      CLK => CLK,
      data_in => transceiver_inst_n_12,
      data_out => rx_reset_done_i
    );
sync_block_tx_reset_done: entity work.gig_ethernet_pcs_pma_0_sync_block_0
     port map (
      CLK => CLK,
      data_in => transceiver_inst_n_11,
      resetdone => \^resetdone\,
      resetdone_0 => rx_reset_done_i
    );
transceiver_inst: entity work.gig_ethernet_pcs_pma_0_transceiver
     port map (
      CLK => CLK,
      D(0) => txchardispmode,
      Q(1 downto 0) => rxclkcorcnt(1 downto 0),
      SR(0) => mgt_rx_reset,
      data_in => transceiver_inst_n_11,
      data_sync_reg1 => data_in,
      enablealign => enablealign,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i => gtxe2_i,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0),
      powerdown => powerdown,
      reset_sync5(0) => mgt_tx_reset,
      rx_fsm_reset_done_int_reg => transceiver_inst_n_12,
      rxbufstatus(0) => rxbuferr,
      rxchariscomma(0) => rxchariscomma,
      rxcharisk(0) => rxcharisk,
      \rxdata_reg[7]_0\(7 downto 0) => rxdata(7 downto 0),
      rxdisperr(0) => rxdisperr,
      rxn => rxn,
      rxnotintable(0) => rxnotintable,
      rxoutclk => rxoutclk,
      rxp => rxp,
      status_vector(0) => \^status_vector\(1),
      txbuferr => txbuferr,
      txchardispval_reg_reg_0(0) => txchardispval,
      txcharisk_reg_reg_0(0) => txcharisk,
      \txdata_reg_reg[7]_0\(7 downto 0) => txdata(7 downto 0),
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_support is
  port (
    gtrefclk_p : in STD_LOGIC;
    gtrefclk_n : in STD_LOGIC;
    gtrefclk_out : out STD_LOGIC;
    gtrefclk_bufg_out : out STD_LOGIC;
    txp : out STD_LOGIC;
    txn : out STD_LOGIC;
    rxp : in STD_LOGIC;
    rxn : in STD_LOGIC;
    userclk_out : out STD_LOGIC;
    userclk2_out : out STD_LOGIC;
    rxuserclk_out : out STD_LOGIC;
    rxuserclk2_out : out STD_LOGIC;
    pma_reset_out : out STD_LOGIC;
    mmcm_locked_out : out STD_LOGIC;
    resetdone : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gmii_txd : in STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_tx_en : in STD_LOGIC;
    gmii_tx_er : in STD_LOGIC;
    gmii_rxd : out STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_rx_dv : out STD_LOGIC;
    gmii_rx_er : out STD_LOGIC;
    gmii_isolate : out STD_LOGIC;
    configuration_vector : in STD_LOGIC_VECTOR ( 4 downto 0 );
    status_vector : out STD_LOGIC_VECTOR ( 15 downto 0 );
    reset : in STD_LOGIC;
    signal_detect : in STD_LOGIC;
    gt0_qplloutclk_out : out STD_LOGIC;
    gt0_qplloutrefclk_out : out STD_LOGIC
  );
  attribute EXAMPLE_SIMULATION : integer;
  attribute EXAMPLE_SIMULATION of gig_ethernet_pcs_pma_0_support : entity is 0;
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of gig_ethernet_pcs_pma_0_support : entity is "yes";
end gig_ethernet_pcs_pma_0_support;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_support is
  signal \<const0>\ : STD_LOGIC;
  signal \^gt0_qplloutclk_out\ : STD_LOGIC;
  signal \^gt0_qplloutrefclk_out\ : STD_LOGIC;
  signal \^gtrefclk_bufg_out\ : STD_LOGIC;
  signal \^gtrefclk_out\ : STD_LOGIC;
  signal \^mmcm_locked_out\ : STD_LOGIC;
  signal mmcm_reset : STD_LOGIC;
  signal \^pma_reset_out\ : STD_LOGIC;
  signal rxoutclk : STD_LOGIC;
  signal \^rxuserclk2_out\ : STD_LOGIC;
  signal \^status_vector\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal txoutclk : STD_LOGIC;
  signal \^userclk2_out\ : STD_LOGIC;
  signal \^userclk_out\ : STD_LOGIC;
begin
  gt0_qplloutclk_out <= \^gt0_qplloutclk_out\;
  gt0_qplloutrefclk_out <= \^gt0_qplloutrefclk_out\;
  gtrefclk_bufg_out <= \^gtrefclk_bufg_out\;
  gtrefclk_out <= \^gtrefclk_out\;
  mmcm_locked_out <= \^mmcm_locked_out\;
  pma_reset_out <= \^pma_reset_out\;
  rxuserclk2_out <= \^rxuserclk2_out\;
  rxuserclk_out <= \^rxuserclk2_out\;
  status_vector(15) <= \<const0>\;
  status_vector(14) <= \<const0>\;
  status_vector(13) <= \<const0>\;
  status_vector(12) <= \<const0>\;
  status_vector(11) <= \<const0>\;
  status_vector(10) <= \<const0>\;
  status_vector(9) <= \<const0>\;
  status_vector(8) <= \<const0>\;
  status_vector(7) <= \<const0>\;
  status_vector(6 downto 0) <= \^status_vector\(6 downto 0);
  userclk2_out <= \^userclk2_out\;
  userclk_out <= \^userclk_out\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
core_clocking_i: entity work.gig_ethernet_pcs_pma_0_clocking
     port map (
      gtrefclk_bufg => \^gtrefclk_bufg_out\,
      gtrefclk_n => gtrefclk_n,
      gtrefclk_out => \^gtrefclk_out\,
      gtrefclk_p => gtrefclk_p,
      mmcm_locked => \^mmcm_locked_out\,
      mmcm_reset => mmcm_reset,
      rxoutclk => rxoutclk,
      rxuserclk2_out => \^rxuserclk2_out\,
      txoutclk => txoutclk,
      userclk => \^userclk_out\,
      userclk2 => \^userclk2_out\
    );
core_gt_common_i: entity work.gig_ethernet_pcs_pma_0_gt_common
     port map (
      gt0_qplloutclk_out => \^gt0_qplloutclk_out\,
      gt0_qplloutrefclk_out => \^gt0_qplloutrefclk_out\,
      gtrefclk_out => \^gtrefclk_out\,
      independent_clock_bufg => independent_clock_bufg,
      \out\(0) => \^pma_reset_out\
    );
core_resets_i: entity work.gig_ethernet_pcs_pma_0_resets
     port map (
      independent_clock_bufg => independent_clock_bufg,
      \out\(0) => \^pma_reset_out\,
      reset => reset
    );
pcs_pma_block_i: entity work.gig_ethernet_pcs_pma_0_block
     port map (
      CLK => \^userclk2_out\,
      configuration_vector(2 downto 0) => configuration_vector(3 downto 1),
      data_in => \^mmcm_locked_out\,
      gmii_isolate => gmii_isolate,
      gmii_rx_dv => gmii_rx_dv,
      gmii_rx_er => gmii_rx_er,
      gmii_rxd(7 downto 0) => gmii_rxd(7 downto 0),
      gmii_tx_en => gmii_tx_en,
      gmii_tx_er => gmii_tx_er,
      gmii_txd(7 downto 0) => gmii_txd(7 downto 0),
      gt0_qplloutclk_out => \^gt0_qplloutclk_out\,
      gt0_qplloutrefclk_out => \^gt0_qplloutrefclk_out\,
      gtrefclk_bufg => \^gtrefclk_bufg_out\,
      gtrefclk_out => \^gtrefclk_out\,
      gtxe2_i => \^userclk_out\,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \^pma_reset_out\,
      resetdone => resetdone,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      signal_detect => signal_detect,
      status_vector(6 downto 0) => \^status_vector\(6 downto 0),
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0 is
  port (
    gtrefclk_p : in STD_LOGIC;
    gtrefclk_n : in STD_LOGIC;
    gtrefclk_out : out STD_LOGIC;
    gtrefclk_bufg_out : out STD_LOGIC;
    txp : out STD_LOGIC;
    txn : out STD_LOGIC;
    rxp : in STD_LOGIC;
    rxn : in STD_LOGIC;
    resetdone : out STD_LOGIC;
    userclk_out : out STD_LOGIC;
    userclk2_out : out STD_LOGIC;
    rxuserclk_out : out STD_LOGIC;
    rxuserclk2_out : out STD_LOGIC;
    pma_reset_out : out STD_LOGIC;
    mmcm_locked_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gmii_txd : in STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_tx_en : in STD_LOGIC;
    gmii_tx_er : in STD_LOGIC;
    gmii_rxd : out STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_rx_dv : out STD_LOGIC;
    gmii_rx_er : out STD_LOGIC;
    gmii_isolate : out STD_LOGIC;
    configuration_vector : in STD_LOGIC_VECTOR ( 4 downto 0 );
    status_vector : out STD_LOGIC_VECTOR ( 15 downto 0 );
    reset : in STD_LOGIC;
    signal_detect : in STD_LOGIC;
    gt0_qplloutclk_out : out STD_LOGIC;
    gt0_qplloutrefclk_out : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of gig_ethernet_pcs_pma_0 : entity is true;
  attribute EXAMPLE_SIMULATION : integer;
  attribute EXAMPLE_SIMULATION of gig_ethernet_pcs_pma_0 : entity is 0;
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of gig_ethernet_pcs_pma_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of gig_ethernet_pcs_pma_0 : entity is "gig_ethernet_pcs_pma_v16_2_15,Vivado 2023.2";
end gig_ethernet_pcs_pma_0;

architecture STRUCTURE of gig_ethernet_pcs_pma_0 is
  signal \<const0>\ : STD_LOGIC;
  signal \^status_vector\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal NLW_U0_status_vector_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 7 );
  attribute EXAMPLE_SIMULATION of U0 : label is 0;
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
begin
  status_vector(15) <= \<const0>\;
  status_vector(14) <= \<const0>\;
  status_vector(13) <= \<const0>\;
  status_vector(12) <= \<const0>\;
  status_vector(11) <= \<const0>\;
  status_vector(10) <= \<const0>\;
  status_vector(9) <= \<const0>\;
  status_vector(8) <= \<const0>\;
  status_vector(7) <= \<const0>\;
  status_vector(6 downto 0) <= \^status_vector\(6 downto 0);
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.gig_ethernet_pcs_pma_0_support
     port map (
      configuration_vector(4) => '0',
      configuration_vector(3 downto 1) => configuration_vector(3 downto 1),
      configuration_vector(0) => '0',
      gmii_isolate => gmii_isolate,
      gmii_rx_dv => gmii_rx_dv,
      gmii_rx_er => gmii_rx_er,
      gmii_rxd(7 downto 0) => gmii_rxd(7 downto 0),
      gmii_tx_en => gmii_tx_en,
      gmii_tx_er => gmii_tx_er,
      gmii_txd(7 downto 0) => gmii_txd(7 downto 0),
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg_out => gtrefclk_bufg_out,
      gtrefclk_n => gtrefclk_n,
      gtrefclk_out => gtrefclk_out,
      gtrefclk_p => gtrefclk_p,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_locked_out => mmcm_locked_out,
      pma_reset_out => pma_reset_out,
      reset => reset,
      resetdone => resetdone,
      rxn => rxn,
      rxp => rxp,
      rxuserclk2_out => rxuserclk2_out,
      rxuserclk_out => rxuserclk_out,
      signal_detect => signal_detect,
      status_vector(15 downto 7) => NLW_U0_status_vector_UNCONNECTED(15 downto 7),
      status_vector(6 downto 0) => \^status_vector\(6 downto 0),
      txn => txn,
      txp => txp,
      userclk2_out => userclk2_out,
      userclk_out => userclk_out
    );
end STRUCTURE;
