// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// Copyright 2022-2023 Advanced Micro Devices, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2023.2 (lin64) Build 4029153 Fri Oct 13 20:13:54 MDT 2023
// Date        : Wed Jan 17 16:49:39 2024
// Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 38 (Thirty Eight)
// Command     : write_verilog -force -mode funcsim
//               /data/cms/ecal/fe/fead_v2/firmware.2023.2/FEAD.gen/sources_1/ip/gig_ethernet_pcs_pma_0/gig_ethernet_pcs_pma_0_sim_netlist.v
// Design      : gig_ethernet_pcs_pma_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7k70tfbg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* EXAMPLE_SIMULATION = "0" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "gig_ethernet_pcs_pma_v16_2_15,Vivado 2023.2" *) 
(* NotValidForBitStream *)
module gig_ethernet_pcs_pma_0
   (gtrefclk_p,
    gtrefclk_n,
    gtrefclk_out,
    gtrefclk_bufg_out,
    txp,
    txn,
    rxp,
    rxn,
    resetdone,
    userclk_out,
    userclk2_out,
    rxuserclk_out,
    rxuserclk2_out,
    pma_reset_out,
    mmcm_locked_out,
    independent_clock_bufg,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    configuration_vector,
    status_vector,
    reset,
    signal_detect,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out);
  input gtrefclk_p;
  input gtrefclk_n;
  output gtrefclk_out;
  output gtrefclk_bufg_out;
  output txp;
  output txn;
  input rxp;
  input rxn;
  output resetdone;
  output userclk_out;
  output userclk2_out;
  output rxuserclk_out;
  output rxuserclk2_out;
  output pma_reset_out;
  output mmcm_locked_out;
  input independent_clock_bufg;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  input [4:0]configuration_vector;
  output [15:0]status_vector;
  input reset;
  input signal_detect;
  output gt0_qplloutclk_out;
  output gt0_qplloutrefclk_out;

  wire \<const0> ;
  wire [4:0]configuration_vector;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg_out;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire independent_clock_bufg;
  wire mmcm_locked_out;
  wire pma_reset_out;
  wire reset;
  wire resetdone;
  wire rxn;
  wire rxp;
  wire rxuserclk2_out;
  wire rxuserclk_out;
  wire signal_detect;
  wire [6:0]\^status_vector ;
  wire txn;
  wire txp;
  wire userclk2_out;
  wire userclk_out;
  wire [15:7]NLW_U0_status_vector_UNCONNECTED;

  assign status_vector[15] = \<const0> ;
  assign status_vector[14] = \<const0> ;
  assign status_vector[13] = \<const0> ;
  assign status_vector[12] = \<const0> ;
  assign status_vector[11] = \<const0> ;
  assign status_vector[10] = \<const0> ;
  assign status_vector[9] = \<const0> ;
  assign status_vector[8] = \<const0> ;
  assign status_vector[7] = \<const0> ;
  assign status_vector[6:0] = \^status_vector [6:0];
  GND GND
       (.G(\<const0> ));
  (* EXAMPLE_SIMULATION = "0" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  gig_ethernet_pcs_pma_0_support U0
       (.configuration_vector({1'b0,configuration_vector[3:1],1'b0}),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg_out(gtrefclk_bufg_out),
        .gtrefclk_n(gtrefclk_n),
        .gtrefclk_out(gtrefclk_out),
        .gtrefclk_p(gtrefclk_p),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_locked_out(mmcm_locked_out),
        .pma_reset_out(pma_reset_out),
        .reset(reset),
        .resetdone(resetdone),
        .rxn(rxn),
        .rxp(rxp),
        .rxuserclk2_out(rxuserclk2_out),
        .rxuserclk_out(rxuserclk_out),
        .signal_detect(signal_detect),
        .status_vector({NLW_U0_status_vector_UNCONNECTED[15:7],\^status_vector }),
        .txn(txn),
        .txp(txp),
        .userclk2_out(userclk2_out),
        .userclk_out(userclk_out));
endmodule

module gig_ethernet_pcs_pma_0_GTWIZARD
   (txn,
    txp,
    rxoutclk,
    txoutclk,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i,
    gtxe2_i_0,
    gtxe2_i_1,
    gtxe2_i_2,
    gtxe2_i_3,
    mmcm_reset,
    data_in,
    rx_fsm_reset_done_int_reg,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gtxe2_i_4,
    TXPD,
    RXPD,
    Q,
    gtxe2_i_5,
    gtxe2_i_6,
    gtxe2_i_7,
    out,
    gtxe2_i_8,
    gtxe2_i_9,
    data_sync_reg1,
    data_out);
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i;
  output [1:0]gtxe2_i_0;
  output [1:0]gtxe2_i_1;
  output [1:0]gtxe2_i_2;
  output [1:0]gtxe2_i_3;
  output mmcm_reset;
  output data_in;
  output rx_fsm_reset_done_int_reg;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gtxe2_i_4;
  input [0:0]TXPD;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_5;
  input [1:0]gtxe2_i_6;
  input [1:0]gtxe2_i_7;
  input [0:0]out;
  input gtxe2_i_8;
  input gtxe2_i_9;
  input data_sync_reg1;
  input data_out;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire data_in;
  wire data_out;
  wire data_sync_reg1;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire [15:0]gtxe2_i;
  wire [1:0]gtxe2_i_0;
  wire [1:0]gtxe2_i_1;
  wire [1:0]gtxe2_i_2;
  wire [1:0]gtxe2_i_3;
  wire gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire [1:0]gtxe2_i_7;
  wire gtxe2_i_8;
  wire gtxe2_i_9;
  wire independent_clock_bufg;
  wire mmcm_reset;
  wire [0:0]out;
  wire reset;
  wire reset_out;
  wire rx_fsm_reset_done_int_reg;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;

  gig_ethernet_pcs_pma_0_GTWIZARD_init U0
       (.D(D),
        .Q(Q),
        .RXBUFSTATUS(RXBUFSTATUS),
        .RXPD(RXPD),
        .TXBUFSTATUS(TXBUFSTATUS),
        .TXPD(TXPD),
        .data_in(data_in),
        .data_out(data_out),
        .data_sync_reg1(data_sync_reg1),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(gtxe2_i),
        .gtxe2_i_0(gtxe2_i_0),
        .gtxe2_i_1(gtxe2_i_1),
        .gtxe2_i_2(gtxe2_i_2),
        .gtxe2_i_3(gtxe2_i_3),
        .gtxe2_i_4(gtxe2_i_4),
        .gtxe2_i_5(gtxe2_i_5),
        .gtxe2_i_6(gtxe2_i_6),
        .gtxe2_i_7(gtxe2_i_7),
        .gtxe2_i_8(gtxe2_i_8),
        .gtxe2_i_9(gtxe2_i_9),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out),
        .reset(reset),
        .reset_out(reset_out),
        .rx_fsm_reset_done_int_reg(rx_fsm_reset_done_int_reg),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module gig_ethernet_pcs_pma_0_GTWIZARD_GT
   (gtxe2_i_0,
    gt0_cpllrefclklost_i,
    txn,
    txp,
    rxoutclk,
    gtxe2_i_1,
    txoutclk,
    gtxe2_i_2,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i_3,
    gtxe2_i_4,
    gtxe2_i_5,
    gtxe2_i_6,
    gtxe2_i_7,
    independent_clock_bufg,
    cpll_pd0_i,
    cpllreset_in,
    gtrefclk_bufg,
    gtrefclk_out,
    SR,
    gt0_gttxreset_in0_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gt0_rxuserrdy_t,
    gtxe2_i_8,
    TXPD,
    gt0_txuserrdy_t,
    RXPD,
    Q,
    gtxe2_i_9,
    gtxe2_i_10,
    gtxe2_i_11);
  output gtxe2_i_0;
  output gt0_cpllrefclklost_i;
  output txn;
  output txp;
  output rxoutclk;
  output gtxe2_i_1;
  output txoutclk;
  output gtxe2_i_2;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i_3;
  output [1:0]gtxe2_i_4;
  output [1:0]gtxe2_i_5;
  output [1:0]gtxe2_i_6;
  output [1:0]gtxe2_i_7;
  input independent_clock_bufg;
  input cpll_pd0_i;
  input cpllreset_in;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input [0:0]SR;
  input gt0_gttxreset_in0_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gt0_rxuserrdy_t;
  input gtxe2_i_8;
  input [0:0]TXPD;
  input gt0_txuserrdy_t;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_9;
  input [1:0]gtxe2_i_10;
  input [1:0]gtxe2_i_11;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]SR;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire cpll_pd0_i;
  wire cpllreset_in;
  wire gt0_cpllrefclklost_i;
  wire gt0_gttxreset_in0_out;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gt0_rxuserrdy_t;
  wire gt0_txuserrdy_t;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtxe2_i_0;
  wire gtxe2_i_1;
  wire [1:0]gtxe2_i_10;
  wire [1:0]gtxe2_i_11;
  wire gtxe2_i_2;
  wire [15:0]gtxe2_i_3;
  wire [1:0]gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire [1:0]gtxe2_i_7;
  wire gtxe2_i_8;
  wire [1:0]gtxe2_i_9;
  wire gtxe2_i_n_0;
  wire gtxe2_i_n_10;
  wire gtxe2_i_n_16;
  wire gtxe2_i_n_170;
  wire gtxe2_i_n_171;
  wire gtxe2_i_n_172;
  wire gtxe2_i_n_173;
  wire gtxe2_i_n_174;
  wire gtxe2_i_n_175;
  wire gtxe2_i_n_176;
  wire gtxe2_i_n_177;
  wire gtxe2_i_n_178;
  wire gtxe2_i_n_179;
  wire gtxe2_i_n_180;
  wire gtxe2_i_n_181;
  wire gtxe2_i_n_182;
  wire gtxe2_i_n_183;
  wire gtxe2_i_n_184;
  wire gtxe2_i_n_27;
  wire gtxe2_i_n_3;
  wire gtxe2_i_n_38;
  wire gtxe2_i_n_39;
  wire gtxe2_i_n_4;
  wire gtxe2_i_n_46;
  wire gtxe2_i_n_47;
  wire gtxe2_i_n_48;
  wire gtxe2_i_n_49;
  wire gtxe2_i_n_50;
  wire gtxe2_i_n_51;
  wire gtxe2_i_n_52;
  wire gtxe2_i_n_53;
  wire gtxe2_i_n_54;
  wire gtxe2_i_n_55;
  wire gtxe2_i_n_56;
  wire gtxe2_i_n_57;
  wire gtxe2_i_n_58;
  wire gtxe2_i_n_59;
  wire gtxe2_i_n_60;
  wire gtxe2_i_n_61;
  wire gtxe2_i_n_81;
  wire gtxe2_i_n_83;
  wire gtxe2_i_n_84;
  wire gtxe2_i_n_9;
  wire independent_clock_bufg;
  wire reset;
  wire reset_out;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;
  wire NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED;
  wire NLW_gtxe2_i_PHYSTATUS_UNCONNECTED;
  wire NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED;
  wire NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED;
  wire NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED;
  wire NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXDATAVALID_UNCONNECTED;
  wire NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXELECIDLE_UNCONNECTED;
  wire NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED;
  wire NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED;
  wire NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED;
  wire NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXQPISENN_UNCONNECTED;
  wire NLW_gtxe2_i_RXQPISENP_UNCONNECTED;
  wire NLW_gtxe2_i_RXRATEDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED;
  wire NLW_gtxe2_i_RXVALID_UNCONNECTED;
  wire NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED;
  wire NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED;
  wire NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED;
  wire NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED;
  wire NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED;
  wire NLW_gtxe2_i_TXQPISENN_UNCONNECTED;
  wire NLW_gtxe2_i_TXQPISENP_UNCONNECTED;
  wire NLW_gtxe2_i_TXRATEDONE_UNCONNECTED;
  wire [15:0]NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXCHARISK_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXCHBONDO_UNCONNECTED;
  wire [63:16]NLW_gtxe2_i_RXDATA_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXDISPERR_UNCONNECTED;
  wire [2:0]NLW_gtxe2_i_RXHEADER_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED;
  wire [2:0]NLW_gtxe2_i_RXSTATUS_UNCONNECTED;
  wire [9:0]NLW_gtxe2_i_TSTOUT_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  GTXE2_CHANNEL #(
    .ALIGN_COMMA_DOUBLE("FALSE"),
    .ALIGN_COMMA_ENABLE(10'b0001111111),
    .ALIGN_COMMA_WORD(2),
    .ALIGN_MCOMMA_DET("TRUE"),
    .ALIGN_MCOMMA_VALUE(10'b1010000011),
    .ALIGN_PCOMMA_DET("TRUE"),
    .ALIGN_PCOMMA_VALUE(10'b0101111100),
    .CBCC_DATA_SOURCE_SEL("DECODED"),
    .CHAN_BOND_KEEP_ALIGN("FALSE"),
    .CHAN_BOND_MAX_SKEW(1),
    .CHAN_BOND_SEQ_1_1(10'b0000000000),
    .CHAN_BOND_SEQ_1_2(10'b0000000000),
    .CHAN_BOND_SEQ_1_3(10'b0000000000),
    .CHAN_BOND_SEQ_1_4(10'b0000000000),
    .CHAN_BOND_SEQ_1_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_1(10'b0000000000),
    .CHAN_BOND_SEQ_2_2(10'b0000000000),
    .CHAN_BOND_SEQ_2_3(10'b0000000000),
    .CHAN_BOND_SEQ_2_4(10'b0000000000),
    .CHAN_BOND_SEQ_2_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_USE("FALSE"),
    .CHAN_BOND_SEQ_LEN(1),
    .CLK_CORRECT_USE("TRUE"),
    .CLK_COR_KEEP_IDLE("FALSE"),
    .CLK_COR_MAX_LAT(36),
    .CLK_COR_MIN_LAT(33),
    .CLK_COR_PRECEDENCE("TRUE"),
    .CLK_COR_REPEAT_WAIT(0),
    .CLK_COR_SEQ_1_1(10'b0110111100),
    .CLK_COR_SEQ_1_2(10'b0001010000),
    .CLK_COR_SEQ_1_3(10'b0000000000),
    .CLK_COR_SEQ_1_4(10'b0000000000),
    .CLK_COR_SEQ_1_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_1(10'b0110111100),
    .CLK_COR_SEQ_2_2(10'b0010110101),
    .CLK_COR_SEQ_2_3(10'b0000000000),
    .CLK_COR_SEQ_2_4(10'b0000000000),
    .CLK_COR_SEQ_2_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_USE("TRUE"),
    .CLK_COR_SEQ_LEN(2),
    .CPLL_CFG(24'hBC07DC),
    .CPLL_FBDIV(4),
    .CPLL_FBDIV_45(5),
    .CPLL_INIT_CFG(24'h00001E),
    .CPLL_LOCK_CFG(16'h01E8),
    .CPLL_REFCLK_DIV(1),
    .DEC_MCOMMA_DETECT("TRUE"),
    .DEC_PCOMMA_DETECT("TRUE"),
    .DEC_VALID_COMMA_ONLY("FALSE"),
    .DMONITOR_CFG(24'h000A00),
    .ES_CONTROL(6'b000000),
    .ES_ERRDET_EN("FALSE"),
    .ES_EYE_SCAN_EN("TRUE"),
    .ES_HORZ_OFFSET(12'h000),
    .ES_PMA_CFG(10'b0000000000),
    .ES_PRESCALE(5'b00000),
    .ES_QUALIFIER(80'h00000000000000000000),
    .ES_QUAL_MASK(80'h00000000000000000000),
    .ES_SDATA_MASK(80'h00000000000000000000),
    .ES_VERT_OFFSET(9'b000000000),
    .FTS_DESKEW_SEQ_ENABLE(4'b1111),
    .FTS_LANE_DESKEW_CFG(4'b1111),
    .FTS_LANE_DESKEW_EN("FALSE"),
    .GEARBOX_MODE(3'b000),
    .IS_CPLLLOCKDETCLK_INVERTED(1'b0),
    .IS_DRPCLK_INVERTED(1'b0),
    .IS_GTGREFCLK_INVERTED(1'b0),
    .IS_RXUSRCLK2_INVERTED(1'b0),
    .IS_RXUSRCLK_INVERTED(1'b0),
    .IS_TXPHDLYTSTCLK_INVERTED(1'b0),
    .IS_TXUSRCLK2_INVERTED(1'b0),
    .IS_TXUSRCLK_INVERTED(1'b0),
    .OUTREFCLK_SEL_INV(2'b11),
    .PCS_PCIE_EN("FALSE"),
    .PCS_RSVD_ATTR(48'h000000000000),
    .PD_TRANS_TIME_FROM_P2(12'h03C),
    .PD_TRANS_TIME_NONE_P2(8'h19),
    .PD_TRANS_TIME_TO_P2(8'h64),
    .PMA_RSV(32'h00018480),
    .PMA_RSV2(16'h2050),
    .PMA_RSV3(2'b00),
    .PMA_RSV4(32'h00000000),
    .RXBUFRESET_TIME(5'b00001),
    .RXBUF_ADDR_MODE("FULL"),
    .RXBUF_EIDLE_HI_CNT(4'b1000),
    .RXBUF_EIDLE_LO_CNT(4'b0000),
    .RXBUF_EN("TRUE"),
    .RXBUF_RESET_ON_CB_CHANGE("TRUE"),
    .RXBUF_RESET_ON_COMMAALIGN("FALSE"),
    .RXBUF_RESET_ON_EIDLE("FALSE"),
    .RXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .RXBUF_THRESH_OVFLW(61),
    .RXBUF_THRESH_OVRD("FALSE"),
    .RXBUF_THRESH_UNDFLW(8),
    .RXCDRFREQRESET_TIME(5'b00001),
    .RXCDRPHRESET_TIME(5'b00001),
    .RXCDR_CFG(72'h03000023FF10100020),
    .RXCDR_FR_RESET_ON_EIDLE(1'b0),
    .RXCDR_HOLD_DURING_EIDLE(1'b0),
    .RXCDR_LOCK_CFG(6'b010101),
    .RXCDR_PH_RESET_ON_EIDLE(1'b0),
    .RXDFELPMRESET_TIME(7'b0001111),
    .RXDLY_CFG(16'h001F),
    .RXDLY_LCFG(9'h030),
    .RXDLY_TAP_CFG(16'h0000),
    .RXGEARBOX_EN("FALSE"),
    .RXISCANRESET_TIME(5'b00001),
    .RXLPM_HF_CFG(14'b00000011110000),
    .RXLPM_LF_CFG(14'b00000011110000),
    .RXOOB_CFG(7'b0000110),
    .RXOUT_DIV(4),
    .RXPCSRESET_TIME(5'b00001),
    .RXPHDLY_CFG(24'h084020),
    .RXPH_CFG(24'h000000),
    .RXPH_MONITOR_SEL(5'b00000),
    .RXPMARESET_TIME(5'b00011),
    .RXPRBS_ERR_LOOPBACK(1'b0),
    .RXSLIDE_AUTO_WAIT(7),
    .RXSLIDE_MODE("OFF"),
    .RX_BIAS_CFG(12'b000000000100),
    .RX_BUFFER_CFG(6'b000000),
    .RX_CLK25_DIV(5),
    .RX_CLKMUX_PD(1'b1),
    .RX_CM_SEL(2'b11),
    .RX_CM_TRIM(3'b010),
    .RX_DATA_WIDTH(20),
    .RX_DDI_SEL(6'b000000),
    .RX_DEBUG_CFG(12'b000000000000),
    .RX_DEFER_RESET_BUF_EN("TRUE"),
    .RX_DFE_GAIN_CFG(23'h020FEA),
    .RX_DFE_H2_CFG(12'b000000000000),
    .RX_DFE_H3_CFG(12'b000001000000),
    .RX_DFE_H4_CFG(11'b00011110000),
    .RX_DFE_H5_CFG(11'b00011100000),
    .RX_DFE_KL_CFG(13'b0000011111110),
    .RX_DFE_KL_CFG2(32'h301148AC),
    .RX_DFE_LPM_CFG(16'h0904),
    .RX_DFE_LPM_HOLD_DURING_EIDLE(1'b0),
    .RX_DFE_UT_CFG(17'b10001111000000000),
    .RX_DFE_VP_CFG(17'b00011111100000011),
    .RX_DFE_XYD_CFG(13'b0000000000000),
    .RX_DISPERR_SEQ_MATCH("TRUE"),
    .RX_INT_DATAWIDTH(0),
    .RX_OS_CFG(13'b0000010000000),
    .RX_SIG_VALID_DLY(10),
    .RX_XCLK_SEL("RXREC"),
    .SAS_MAX_COM(64),
    .SAS_MIN_COM(36),
    .SATA_BURST_SEQ_LEN(4'b0101),
    .SATA_BURST_VAL(3'b100),
    .SATA_CPLL_CFG("VCO_3000MHZ"),
    .SATA_EIDLE_VAL(3'b100),
    .SATA_MAX_BURST(8),
    .SATA_MAX_INIT(21),
    .SATA_MAX_WAKE(7),
    .SATA_MIN_BURST(4),
    .SATA_MIN_INIT(12),
    .SATA_MIN_WAKE(4),
    .SHOW_REALIGN_COMMA("TRUE"),
    .SIM_CPLLREFCLK_SEL(3'b001),
    .SIM_RECEIVER_DETECT_PASS("TRUE"),
    .SIM_RESET_SPEEDUP("TRUE"),
    .SIM_TX_EIDLE_DRIVE_LEVEL("X"),
    .SIM_VERSION("4.0"),
    .TERM_RCAL_CFG(5'b10000),
    .TERM_RCAL_OVRD(1'b0),
    .TRANS_TIME_RATE(8'h0E),
    .TST_RSV(32'h00000000),
    .TXBUF_EN("TRUE"),
    .TXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .TXDLY_CFG(16'h001F),
    .TXDLY_LCFG(9'h030),
    .TXDLY_TAP_CFG(16'h0000),
    .TXGEARBOX_EN("FALSE"),
    .TXOUT_DIV(4),
    .TXPCSRESET_TIME(5'b00001),
    .TXPHDLY_CFG(24'h084020),
    .TXPH_CFG(16'h0780),
    .TXPH_MONITOR_SEL(5'b00000),
    .TXPMARESET_TIME(5'b00001),
    .TX_CLK25_DIV(5),
    .TX_CLKMUX_PD(1'b1),
    .TX_DATA_WIDTH(20),
    .TX_DEEMPH0(5'b00000),
    .TX_DEEMPH1(5'b00000),
    .TX_DRIVE_MODE("DIRECT"),
    .TX_EIDLE_ASSERT_DELAY(3'b110),
    .TX_EIDLE_DEASSERT_DELAY(3'b100),
    .TX_INT_DATAWIDTH(0),
    .TX_LOOPBACK_DRIVE_HIZ("FALSE"),
    .TX_MAINCURSOR_SEL(1'b0),
    .TX_MARGIN_FULL_0(7'b1001110),
    .TX_MARGIN_FULL_1(7'b1001001),
    .TX_MARGIN_FULL_2(7'b1000101),
    .TX_MARGIN_FULL_3(7'b1000010),
    .TX_MARGIN_FULL_4(7'b1000000),
    .TX_MARGIN_LOW_0(7'b1000110),
    .TX_MARGIN_LOW_1(7'b1000100),
    .TX_MARGIN_LOW_2(7'b1000010),
    .TX_MARGIN_LOW_3(7'b1000000),
    .TX_MARGIN_LOW_4(7'b1000000),
    .TX_PREDRIVER_MODE(1'b0),
    .TX_QPI_STATUS_EN(1'b0),
    .TX_RXDETECT_CFG(14'h1832),
    .TX_RXDETECT_REF(3'b100),
    .TX_XCLK_SEL("TXOUT"),
    .UCODEER_CLR(1'b0)) 
    gtxe2_i
       (.CFGRESET(1'b0),
        .CLKRSVD({1'b0,1'b0,1'b0,1'b0}),
        .CPLLFBCLKLOST(gtxe2_i_n_0),
        .CPLLLOCK(gtxe2_i_0),
        .CPLLLOCKDETCLK(independent_clock_bufg),
        .CPLLLOCKEN(1'b1),
        .CPLLPD(cpll_pd0_i),
        .CPLLREFCLKLOST(gt0_cpllrefclklost_i),
        .CPLLREFCLKSEL({1'b0,1'b0,1'b1}),
        .CPLLRESET(cpllreset_in),
        .DMONITOROUT({gtxe2_i_n_177,gtxe2_i_n_178,gtxe2_i_n_179,gtxe2_i_n_180,gtxe2_i_n_181,gtxe2_i_n_182,gtxe2_i_n_183,gtxe2_i_n_184}),
        .DRPADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPCLK(gtrefclk_bufg),
        .DRPDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPDO({gtxe2_i_n_46,gtxe2_i_n_47,gtxe2_i_n_48,gtxe2_i_n_49,gtxe2_i_n_50,gtxe2_i_n_51,gtxe2_i_n_52,gtxe2_i_n_53,gtxe2_i_n_54,gtxe2_i_n_55,gtxe2_i_n_56,gtxe2_i_n_57,gtxe2_i_n_58,gtxe2_i_n_59,gtxe2_i_n_60,gtxe2_i_n_61}),
        .DRPEN(1'b0),
        .DRPRDY(gtxe2_i_n_3),
        .DRPWE(1'b0),
        .EYESCANDATAERROR(gtxe2_i_n_4),
        .EYESCANMODE(1'b0),
        .EYESCANRESET(1'b0),
        .EYESCANTRIGGER(1'b0),
        .GTGREFCLK(1'b0),
        .GTNORTHREFCLK0(1'b0),
        .GTNORTHREFCLK1(1'b0),
        .GTREFCLK0(gtrefclk_out),
        .GTREFCLK1(1'b0),
        .GTREFCLKMONITOR(NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED),
        .GTRESETSEL(1'b0),
        .GTRSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .GTRXRESET(SR),
        .GTSOUTHREFCLK0(1'b0),
        .GTSOUTHREFCLK1(1'b0),
        .GTTXRESET(gt0_gttxreset_in0_out),
        .GTXRXN(rxn),
        .GTXRXP(rxp),
        .GTXTXN(txn),
        .GTXTXP(txp),
        .LOOPBACK({1'b0,1'b0,1'b0}),
        .PCSRSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDIN2({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDOUT(NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED[15:0]),
        .PHYSTATUS(NLW_gtxe2_i_PHYSTATUS_UNCONNECTED),
        .PMARSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PMARSVDIN2({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLCLK(gt0_qplloutclk_out),
        .QPLLREFCLK(gt0_qplloutrefclk_out),
        .RESETOVRD(1'b0),
        .RX8B10BEN(1'b1),
        .RXBUFRESET(1'b0),
        .RXBUFSTATUS({RXBUFSTATUS,gtxe2_i_n_83,gtxe2_i_n_84}),
        .RXBYTEISALIGNED(gtxe2_i_n_9),
        .RXBYTEREALIGN(gtxe2_i_n_10),
        .RXCDRFREQRESET(1'b0),
        .RXCDRHOLD(1'b0),
        .RXCDRLOCK(NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED),
        .RXCDROVRDEN(1'b0),
        .RXCDRRESET(1'b0),
        .RXCDRRESETRSV(1'b0),
        .RXCHANBONDSEQ(NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED),
        .RXCHANISALIGNED(NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED),
        .RXCHANREALIGN(NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED),
        .RXCHARISCOMMA({NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED[7:2],gtxe2_i_4}),
        .RXCHARISK({NLW_gtxe2_i_RXCHARISK_UNCONNECTED[7:2],gtxe2_i_5}),
        .RXCHBONDEN(1'b0),
        .RXCHBONDI({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RXCHBONDLEVEL({1'b0,1'b0,1'b0}),
        .RXCHBONDMASTER(1'b0),
        .RXCHBONDO(NLW_gtxe2_i_RXCHBONDO_UNCONNECTED[4:0]),
        .RXCHBONDSLAVE(1'b0),
        .RXCLKCORCNT(D),
        .RXCOMINITDET(NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED),
        .RXCOMMADET(gtxe2_i_n_16),
        .RXCOMMADETEN(1'b1),
        .RXCOMSASDET(NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED),
        .RXCOMWAKEDET(NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED),
        .RXDATA({NLW_gtxe2_i_RXDATA_UNCONNECTED[63:16],gtxe2_i_3}),
        .RXDATAVALID(NLW_gtxe2_i_RXDATAVALID_UNCONNECTED),
        .RXDDIEN(1'b0),
        .RXDFEAGCHOLD(1'b0),
        .RXDFEAGCOVRDEN(1'b0),
        .RXDFECM1EN(1'b0),
        .RXDFELFHOLD(1'b0),
        .RXDFELFOVRDEN(1'b0),
        .RXDFELPMRESET(1'b0),
        .RXDFETAP2HOLD(1'b0),
        .RXDFETAP2OVRDEN(1'b0),
        .RXDFETAP3HOLD(1'b0),
        .RXDFETAP3OVRDEN(1'b0),
        .RXDFETAP4HOLD(1'b0),
        .RXDFETAP4OVRDEN(1'b0),
        .RXDFETAP5HOLD(1'b0),
        .RXDFETAP5OVRDEN(1'b0),
        .RXDFEUTHOLD(1'b0),
        .RXDFEUTOVRDEN(1'b0),
        .RXDFEVPHOLD(1'b0),
        .RXDFEVPOVRDEN(1'b0),
        .RXDFEVSEN(1'b0),
        .RXDFEXYDEN(1'b1),
        .RXDFEXYDHOLD(1'b0),
        .RXDFEXYDOVRDEN(1'b0),
        .RXDISPERR({NLW_gtxe2_i_RXDISPERR_UNCONNECTED[7:2],gtxe2_i_6}),
        .RXDLYBYPASS(1'b1),
        .RXDLYEN(1'b0),
        .RXDLYOVRDEN(1'b0),
        .RXDLYSRESET(1'b0),
        .RXDLYSRESETDONE(NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED),
        .RXELECIDLE(NLW_gtxe2_i_RXELECIDLE_UNCONNECTED),
        .RXELECIDLEMODE({1'b1,1'b1}),
        .RXGEARBOXSLIP(1'b0),
        .RXHEADER(NLW_gtxe2_i_RXHEADER_UNCONNECTED[2:0]),
        .RXHEADERVALID(NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED),
        .RXLPMEN(1'b1),
        .RXLPMHFHOLD(1'b0),
        .RXLPMHFOVRDEN(1'b0),
        .RXLPMLFHOLD(1'b0),
        .RXLPMLFKLOVRDEN(1'b0),
        .RXMCOMMAALIGNEN(reset_out),
        .RXMONITOROUT({gtxe2_i_n_170,gtxe2_i_n_171,gtxe2_i_n_172,gtxe2_i_n_173,gtxe2_i_n_174,gtxe2_i_n_175,gtxe2_i_n_176}),
        .RXMONITORSEL({1'b0,1'b0}),
        .RXNOTINTABLE({NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED[7:2],gtxe2_i_7}),
        .RXOOBRESET(1'b0),
        .RXOSHOLD(1'b0),
        .RXOSOVRDEN(1'b0),
        .RXOUTCLK(rxoutclk),
        .RXOUTCLKFABRIC(NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED),
        .RXOUTCLKPCS(NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED),
        .RXOUTCLKSEL({1'b0,1'b1,1'b0}),
        .RXPCOMMAALIGNEN(reset_out),
        .RXPCSRESET(reset),
        .RXPD({RXPD,RXPD}),
        .RXPHALIGN(1'b0),
        .RXPHALIGNDONE(NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED),
        .RXPHALIGNEN(1'b0),
        .RXPHDLYPD(1'b0),
        .RXPHDLYRESET(1'b0),
        .RXPHMONITOR(NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED[4:0]),
        .RXPHOVRDEN(1'b0),
        .RXPHSLIPMONITOR(NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED[4:0]),
        .RXPMARESET(1'b0),
        .RXPOLARITY(1'b0),
        .RXPRBSCNTRESET(1'b0),
        .RXPRBSERR(gtxe2_i_n_27),
        .RXPRBSSEL({1'b0,1'b0,1'b0}),
        .RXQPIEN(1'b0),
        .RXQPISENN(NLW_gtxe2_i_RXQPISENN_UNCONNECTED),
        .RXQPISENP(NLW_gtxe2_i_RXQPISENP_UNCONNECTED),
        .RXRATE({1'b0,1'b0,1'b0}),
        .RXRATEDONE(NLW_gtxe2_i_RXRATEDONE_UNCONNECTED),
        .RXRESETDONE(gtxe2_i_1),
        .RXSLIDE(1'b0),
        .RXSTARTOFSEQ(NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED),
        .RXSTATUS(NLW_gtxe2_i_RXSTATUS_UNCONNECTED[2:0]),
        .RXSYSCLKSEL({1'b0,1'b0}),
        .RXUSERRDY(gt0_rxuserrdy_t),
        .RXUSRCLK(gtxe2_i_8),
        .RXUSRCLK2(gtxe2_i_8),
        .RXVALID(NLW_gtxe2_i_RXVALID_UNCONNECTED),
        .SETERRSTATUS(1'b0),
        .TSTIN({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .TSTOUT(NLW_gtxe2_i_TSTOUT_UNCONNECTED[9:0]),
        .TX8B10BBYPASS({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX8B10BEN(1'b1),
        .TXBUFDIFFCTRL({1'b1,1'b0,1'b0}),
        .TXBUFSTATUS({TXBUFSTATUS,gtxe2_i_n_81}),
        .TXCHARDISPMODE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtxe2_i_9}),
        .TXCHARDISPVAL({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtxe2_i_10}),
        .TXCHARISK({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtxe2_i_11}),
        .TXCOMFINISH(NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED),
        .TXCOMINIT(1'b0),
        .TXCOMSAS(1'b0),
        .TXCOMWAKE(1'b0),
        .TXDATA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q}),
        .TXDEEMPH(1'b0),
        .TXDETECTRX(1'b0),
        .TXDIFFCTRL({1'b1,1'b0,1'b0,1'b0}),
        .TXDIFFPD(1'b0),
        .TXDLYBYPASS(1'b1),
        .TXDLYEN(1'b0),
        .TXDLYHOLD(1'b0),
        .TXDLYOVRDEN(1'b0),
        .TXDLYSRESET(1'b0),
        .TXDLYSRESETDONE(NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED),
        .TXDLYUPDOWN(1'b0),
        .TXELECIDLE(TXPD),
        .TXGEARBOXREADY(NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED),
        .TXHEADER({1'b0,1'b0,1'b0}),
        .TXINHIBIT(1'b0),
        .TXMAINCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXMARGIN({1'b0,1'b0,1'b0}),
        .TXOUTCLK(txoutclk),
        .TXOUTCLKFABRIC(gtxe2_i_n_38),
        .TXOUTCLKPCS(gtxe2_i_n_39),
        .TXOUTCLKSEL({1'b1,1'b0,1'b0}),
        .TXPCSRESET(1'b0),
        .TXPD({TXPD,TXPD}),
        .TXPDELECIDLEMODE(1'b0),
        .TXPHALIGN(1'b0),
        .TXPHALIGNDONE(NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED),
        .TXPHALIGNEN(1'b0),
        .TXPHDLYPD(1'b0),
        .TXPHDLYRESET(1'b0),
        .TXPHDLYTSTCLK(1'b0),
        .TXPHINIT(1'b0),
        .TXPHINITDONE(NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED),
        .TXPHOVRDEN(1'b0),
        .TXPISOPD(1'b0),
        .TXPMARESET(1'b0),
        .TXPOLARITY(1'b0),
        .TXPOSTCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPOSTCURSORINV(1'b0),
        .TXPRBSFORCEERR(1'b0),
        .TXPRBSSEL({1'b0,1'b0,1'b0}),
        .TXPRECURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPRECURSORINV(1'b0),
        .TXQPIBIASEN(1'b0),
        .TXQPISENN(NLW_gtxe2_i_TXQPISENN_UNCONNECTED),
        .TXQPISENP(NLW_gtxe2_i_TXQPISENP_UNCONNECTED),
        .TXQPISTRONGPDOWN(1'b0),
        .TXQPIWEAKPUP(1'b0),
        .TXRATE({1'b0,1'b0,1'b0}),
        .TXRATEDONE(NLW_gtxe2_i_TXRATEDONE_UNCONNECTED),
        .TXRESETDONE(gtxe2_i_2),
        .TXSEQUENCE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXSTARTSEQ(1'b0),
        .TXSWING(1'b0),
        .TXSYSCLKSEL({1'b0,1'b0}),
        .TXUSERRDY(gt0_txuserrdy_t),
        .TXUSRCLK(gtxe2_i_8),
        .TXUSRCLK2(gtxe2_i_8));
endmodule

module gig_ethernet_pcs_pma_0_GTWIZARD_init
   (txn,
    txp,
    rxoutclk,
    txoutclk,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i,
    gtxe2_i_0,
    gtxe2_i_1,
    gtxe2_i_2,
    gtxe2_i_3,
    mmcm_reset,
    data_in,
    rx_fsm_reset_done_int_reg,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gtxe2_i_4,
    TXPD,
    RXPD,
    Q,
    gtxe2_i_5,
    gtxe2_i_6,
    gtxe2_i_7,
    out,
    gtxe2_i_8,
    gtxe2_i_9,
    data_sync_reg1,
    data_out);
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i;
  output [1:0]gtxe2_i_0;
  output [1:0]gtxe2_i_1;
  output [1:0]gtxe2_i_2;
  output [1:0]gtxe2_i_3;
  output mmcm_reset;
  output data_in;
  output rx_fsm_reset_done_int_reg;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gtxe2_i_4;
  input [0:0]TXPD;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_5;
  input [1:0]gtxe2_i_6;
  input [1:0]gtxe2_i_7;
  input [0:0]out;
  input gtxe2_i_8;
  input gtxe2_i_9;
  input data_sync_reg1;
  input data_out;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire [13:1]data0;
  wire data_in;
  wire data_out;
  wire data_sync_reg1;
  wire gt0_cpllrefclklost_i;
  wire gt0_cpllreset_t;
  wire gt0_gtrxreset_in1_out;
  wire gt0_gttxreset_in0_out;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire [13:0]gt0_rx_cdrlock_counter;
  wire gt0_rx_cdrlock_counter0_carry__0_n_0;
  wire gt0_rx_cdrlock_counter0_carry__0_n_1;
  wire gt0_rx_cdrlock_counter0_carry__0_n_2;
  wire gt0_rx_cdrlock_counter0_carry__0_n_3;
  wire gt0_rx_cdrlock_counter0_carry__1_n_0;
  wire gt0_rx_cdrlock_counter0_carry__1_n_1;
  wire gt0_rx_cdrlock_counter0_carry__1_n_2;
  wire gt0_rx_cdrlock_counter0_carry__1_n_3;
  wire gt0_rx_cdrlock_counter0_carry_n_0;
  wire gt0_rx_cdrlock_counter0_carry_n_1;
  wire gt0_rx_cdrlock_counter0_carry_n_2;
  wire gt0_rx_cdrlock_counter0_carry_n_3;
  wire \gt0_rx_cdrlock_counter[0]_i_2_n_0 ;
  wire \gt0_rx_cdrlock_counter[13]_i_2_n_0 ;
  wire \gt0_rx_cdrlock_counter[13]_i_3_n_0 ;
  wire \gt0_rx_cdrlock_counter[13]_i_4_n_0 ;
  wire [13:0]gt0_rx_cdrlock_counter_0;
  wire gt0_rx_cdrlocked_i_1_n_0;
  wire gt0_rx_cdrlocked_reg_n_0;
  wire gt0_rxuserrdy_t;
  wire gt0_txuserrdy_t;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtwizard_i_n_0;
  wire gtwizard_i_n_5;
  wire gtwizard_i_n_7;
  wire [15:0]gtxe2_i;
  wire [1:0]gtxe2_i_0;
  wire [1:0]gtxe2_i_1;
  wire [1:0]gtxe2_i_2;
  wire [1:0]gtxe2_i_3;
  wire gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire [1:0]gtxe2_i_7;
  wire gtxe2_i_8;
  wire gtxe2_i_9;
  wire independent_clock_bufg;
  wire mmcm_reset;
  wire [0:0]out;
  wire reset;
  wire reset_out;
  wire rx_fsm_reset_done_int_reg;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;
  wire [3:0]NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED;
  wire [3:1]NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED;

  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry
       (.CI(1'b0),
        .CO({gt0_rx_cdrlock_counter0_carry_n_0,gt0_rx_cdrlock_counter0_carry_n_1,gt0_rx_cdrlock_counter0_carry_n_2,gt0_rx_cdrlock_counter0_carry_n_3}),
        .CYINIT(gt0_rx_cdrlock_counter[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[4:1]),
        .S(gt0_rx_cdrlock_counter[4:1]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry__0
       (.CI(gt0_rx_cdrlock_counter0_carry_n_0),
        .CO({gt0_rx_cdrlock_counter0_carry__0_n_0,gt0_rx_cdrlock_counter0_carry__0_n_1,gt0_rx_cdrlock_counter0_carry__0_n_2,gt0_rx_cdrlock_counter0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[8:5]),
        .S(gt0_rx_cdrlock_counter[8:5]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry__1
       (.CI(gt0_rx_cdrlock_counter0_carry__0_n_0),
        .CO({gt0_rx_cdrlock_counter0_carry__1_n_0,gt0_rx_cdrlock_counter0_carry__1_n_1,gt0_rx_cdrlock_counter0_carry__1_n_2,gt0_rx_cdrlock_counter0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[12:9]),
        .S(gt0_rx_cdrlock_counter[12:9]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry__2
       (.CI(gt0_rx_cdrlock_counter0_carry__1_n_0),
        .CO(NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED[3:1],data0[13]}),
        .S({1'b0,1'b0,1'b0,gt0_rx_cdrlock_counter[13]}));
  LUT2 #(
    .INIT(4'h2)) 
    \gt0_rx_cdrlock_counter[0]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[0]_i_2_n_0 ),
        .I1(gt0_rx_cdrlock_counter[0]),
        .O(gt0_rx_cdrlock_counter_0[0]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFB)) 
    \gt0_rx_cdrlock_counter[0]_i_2 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I1(gt0_rx_cdrlock_counter[4]),
        .I2(gt0_rx_cdrlock_counter[5]),
        .I3(gt0_rx_cdrlock_counter[7]),
        .I4(gt0_rx_cdrlock_counter[6]),
        .I5(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .O(\gt0_rx_cdrlock_counter[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[10]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[10]),
        .O(gt0_rx_cdrlock_counter_0[10]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[11]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[11]),
        .O(gt0_rx_cdrlock_counter_0[11]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[12]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[12]),
        .O(gt0_rx_cdrlock_counter_0[12]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[13]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[13]),
        .O(gt0_rx_cdrlock_counter_0[13]));
  LUT5 #(
    .INIT(32'hFFFFFFEF)) 
    \gt0_rx_cdrlock_counter[13]_i_2 
       (.I0(gt0_rx_cdrlock_counter[1]),
        .I1(gt0_rx_cdrlock_counter[12]),
        .I2(gt0_rx_cdrlock_counter[13]),
        .I3(gt0_rx_cdrlock_counter[3]),
        .I4(gt0_rx_cdrlock_counter[2]),
        .O(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \gt0_rx_cdrlock_counter[13]_i_3 
       (.I0(gt0_rx_cdrlock_counter[4]),
        .I1(gt0_rx_cdrlock_counter[5]),
        .I2(gt0_rx_cdrlock_counter[7]),
        .I3(gt0_rx_cdrlock_counter[6]),
        .O(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFF7F)) 
    \gt0_rx_cdrlock_counter[13]_i_4 
       (.I0(gt0_rx_cdrlock_counter[9]),
        .I1(gt0_rx_cdrlock_counter[8]),
        .I2(gt0_rx_cdrlock_counter[10]),
        .I3(gt0_rx_cdrlock_counter[11]),
        .O(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[1]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[1]),
        .O(gt0_rx_cdrlock_counter_0[1]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[2]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[2]),
        .O(gt0_rx_cdrlock_counter_0[2]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[3]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[3]),
        .O(gt0_rx_cdrlock_counter_0[3]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[4]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[4]),
        .O(gt0_rx_cdrlock_counter_0[4]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[5]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[5]),
        .O(gt0_rx_cdrlock_counter_0[5]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[6]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[6]),
        .O(gt0_rx_cdrlock_counter_0[6]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[7]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[7]),
        .O(gt0_rx_cdrlock_counter_0[7]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[8]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[8]),
        .O(gt0_rx_cdrlock_counter_0[8]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[9]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[9]),
        .O(gt0_rx_cdrlock_counter_0[9]));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[0]),
        .Q(gt0_rx_cdrlock_counter[0]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[10] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[10]),
        .Q(gt0_rx_cdrlock_counter[10]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[11] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[11]),
        .Q(gt0_rx_cdrlock_counter[11]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[12] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[12]),
        .Q(gt0_rx_cdrlock_counter[12]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[13] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[13]),
        .Q(gt0_rx_cdrlock_counter[13]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[1]),
        .Q(gt0_rx_cdrlock_counter[1]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[2]),
        .Q(gt0_rx_cdrlock_counter[2]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[3]),
        .Q(gt0_rx_cdrlock_counter[3]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[4] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[4]),
        .Q(gt0_rx_cdrlock_counter[4]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[5] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[5]),
        .Q(gt0_rx_cdrlock_counter[5]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[6] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[6]),
        .Q(gt0_rx_cdrlock_counter[6]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[7] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[7]),
        .Q(gt0_rx_cdrlock_counter[7]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[8] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[8]),
        .Q(gt0_rx_cdrlock_counter[8]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[9] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[9]),
        .Q(gt0_rx_cdrlock_counter[9]),
        .R(gt0_gtrxreset_in1_out));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    gt0_rx_cdrlocked_i_1
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(gt0_rx_cdrlocked_reg_n_0),
        .O(gt0_rx_cdrlocked_i_1_n_0));
  FDRE gt0_rx_cdrlocked_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlocked_i_1_n_0),
        .Q(gt0_rx_cdrlocked_reg_n_0),
        .R(gt0_gtrxreset_in1_out));
  gig_ethernet_pcs_pma_0_RX_STARTUP_FSM gt0_rxresetfsm_i
       (.\FSM_sequential_rx_state_reg[0]_0 (gt0_rx_cdrlocked_reg_n_0),
        .SR(gt0_gtrxreset_in1_out),
        .data_in(rx_fsm_reset_done_int_reg),
        .data_out(data_out),
        .data_sync_reg1(gtwizard_i_n_5),
        .data_sync_reg1_0(data_sync_reg1),
        .data_sync_reg1_1(gtwizard_i_n_0),
        .data_sync_reg6(gtxe2_i_4),
        .gt0_rxuserrdy_t(gt0_rxuserrdy_t),
        .gtxe2_i(gtxe2_i_8),
        .independent_clock_bufg(independent_clock_bufg),
        .out(out));
  gig_ethernet_pcs_pma_0_TX_STARTUP_FSM gt0_txresetfsm_i
       (.data_in(data_in),
        .data_sync_reg1(gtxe2_i_4),
        .data_sync_reg1_0(gtwizard_i_n_7),
        .data_sync_reg1_1(data_sync_reg1),
        .data_sync_reg1_2(gtwizard_i_n_0),
        .gt0_cpllrefclklost_i(gt0_cpllrefclklost_i),
        .gt0_cpllreset_t(gt0_cpllreset_t),
        .gt0_gttxreset_in0_out(gt0_gttxreset_in0_out),
        .gt0_txuserrdy_t(gt0_txuserrdy_t),
        .gtxe2_i(gtxe2_i_9),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out));
  gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt gtwizard_i
       (.D(D),
        .Q(Q),
        .RXBUFSTATUS(RXBUFSTATUS),
        .RXPD(RXPD),
        .SR(gt0_gtrxreset_in1_out),
        .TXBUFSTATUS(TXBUFSTATUS),
        .TXPD(TXPD),
        .gt0_cpllrefclklost_i(gt0_cpllrefclklost_i),
        .gt0_cpllreset_t(gt0_cpllreset_t),
        .gt0_gttxreset_in0_out(gt0_gttxreset_in0_out),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gt0_rxuserrdy_t(gt0_rxuserrdy_t),
        .gt0_txuserrdy_t(gt0_txuserrdy_t),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(gtwizard_i_n_0),
        .gtxe2_i_0(gtwizard_i_n_5),
        .gtxe2_i_1(gtwizard_i_n_7),
        .gtxe2_i_10(gtxe2_i_7),
        .gtxe2_i_2(gtxe2_i),
        .gtxe2_i_3(gtxe2_i_0),
        .gtxe2_i_4(gtxe2_i_1),
        .gtxe2_i_5(gtxe2_i_2),
        .gtxe2_i_6(gtxe2_i_3),
        .gtxe2_i_7(gtxe2_i_4),
        .gtxe2_i_8(gtxe2_i_5),
        .gtxe2_i_9(gtxe2_i_6),
        .independent_clock_bufg(independent_clock_bufg),
        .reset(reset),
        .reset_out(reset_out),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt
   (gtxe2_i,
    gt0_cpllrefclklost_i,
    txn,
    txp,
    rxoutclk,
    gtxe2_i_0,
    txoutclk,
    gtxe2_i_1,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i_2,
    gtxe2_i_3,
    gtxe2_i_4,
    gtxe2_i_5,
    gtxe2_i_6,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    SR,
    gt0_gttxreset_in0_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gt0_rxuserrdy_t,
    gtxe2_i_7,
    TXPD,
    gt0_txuserrdy_t,
    RXPD,
    Q,
    gtxe2_i_8,
    gtxe2_i_9,
    gtxe2_i_10,
    gt0_cpllreset_t);
  output gtxe2_i;
  output gt0_cpllrefclklost_i;
  output txn;
  output txp;
  output rxoutclk;
  output gtxe2_i_0;
  output txoutclk;
  output gtxe2_i_1;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i_2;
  output [1:0]gtxe2_i_3;
  output [1:0]gtxe2_i_4;
  output [1:0]gtxe2_i_5;
  output [1:0]gtxe2_i_6;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input [0:0]SR;
  input gt0_gttxreset_in0_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gt0_rxuserrdy_t;
  input gtxe2_i_7;
  input [0:0]TXPD;
  input gt0_txuserrdy_t;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_8;
  input [1:0]gtxe2_i_9;
  input [1:0]gtxe2_i_10;
  input gt0_cpllreset_t;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]SR;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire cpll_pd0_i;
  wire cpllreset_in;
  wire gt0_cpllrefclklost_i;
  wire gt0_cpllreset_t;
  wire gt0_gttxreset_in0_out;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gt0_rxuserrdy_t;
  wire gt0_txuserrdy_t;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtxe2_i;
  wire gtxe2_i_0;
  wire gtxe2_i_1;
  wire [1:0]gtxe2_i_10;
  wire [15:0]gtxe2_i_2;
  wire [1:0]gtxe2_i_3;
  wire [1:0]gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire gtxe2_i_7;
  wire [1:0]gtxe2_i_8;
  wire [1:0]gtxe2_i_9;
  wire independent_clock_bufg;
  wire reset;
  wire reset_out;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;

  gig_ethernet_pcs_pma_0_cpll_railing cpll_railing0_i
       (.cpll_pd0_i(cpll_pd0_i),
        .cpllreset_in(cpllreset_in),
        .gt0_cpllreset_t(gt0_cpllreset_t),
        .gtrefclk_bufg(gtrefclk_bufg));
  gig_ethernet_pcs_pma_0_GTWIZARD_GT gt0_GTWIZARD_i
       (.D(D),
        .Q(Q),
        .RXBUFSTATUS(RXBUFSTATUS),
        .RXPD(RXPD),
        .SR(SR),
        .TXBUFSTATUS(TXBUFSTATUS),
        .TXPD(TXPD),
        .cpll_pd0_i(cpll_pd0_i),
        .cpllreset_in(cpllreset_in),
        .gt0_cpllrefclklost_i(gt0_cpllrefclklost_i),
        .gt0_gttxreset_in0_out(gt0_gttxreset_in0_out),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gt0_rxuserrdy_t(gt0_rxuserrdy_t),
        .gt0_txuserrdy_t(gt0_txuserrdy_t),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i_0(gtxe2_i),
        .gtxe2_i_1(gtxe2_i_0),
        .gtxe2_i_10(gtxe2_i_9),
        .gtxe2_i_11(gtxe2_i_10),
        .gtxe2_i_2(gtxe2_i_1),
        .gtxe2_i_3(gtxe2_i_2),
        .gtxe2_i_4(gtxe2_i_3),
        .gtxe2_i_5(gtxe2_i_4),
        .gtxe2_i_6(gtxe2_i_5),
        .gtxe2_i_7(gtxe2_i_6),
        .gtxe2_i_8(gtxe2_i_7),
        .gtxe2_i_9(gtxe2_i_8),
        .independent_clock_bufg(independent_clock_bufg),
        .reset(reset),
        .reset_out(reset_out),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module gig_ethernet_pcs_pma_0_RX_STARTUP_FSM
   (data_in,
    gt0_rxuserrdy_t,
    SR,
    independent_clock_bufg,
    data_sync_reg6,
    out,
    gtxe2_i,
    \FSM_sequential_rx_state_reg[0]_0 ,
    data_sync_reg1,
    data_sync_reg1_0,
    data_out,
    data_sync_reg1_1);
  output data_in;
  output gt0_rxuserrdy_t;
  output [0:0]SR;
  input independent_clock_bufg;
  input data_sync_reg6;
  input [0:0]out;
  input gtxe2_i;
  input \FSM_sequential_rx_state_reg[0]_0 ;
  input data_sync_reg1;
  input data_sync_reg1_0;
  input data_out;
  input data_sync_reg1_1;

  wire \FSM_sequential_rx_state[0]_i_2_n_0 ;
  wire \FSM_sequential_rx_state[1]_i_3_n_0 ;
  wire \FSM_sequential_rx_state[2]_i_2_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_10_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_3_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_7_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_9_n_0 ;
  wire \FSM_sequential_rx_state_reg[0]_0 ;
  wire GTRXRESET;
  wire RXUSERRDY_i_1_n_0;
  wire [0:0]SR;
  wire check_tlock_max_i_1_n_0;
  wire check_tlock_max_reg_n_0;
  wire data_in;
  wire data_out;
  wire data_out_0;
  wire data_sync_reg1;
  wire data_sync_reg1_0;
  wire data_sync_reg1_1;
  wire data_sync_reg6;
  wire gt0_rxuserrdy_t;
  wire gtrxreset_i_i_1_n_0;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire \init_wait_count[0]_i_1__0_n_0 ;
  wire \init_wait_count[6]_i_1__0_n_0 ;
  wire \init_wait_count[6]_i_3__0_n_0 ;
  wire [6:0]init_wait_count_reg;
  wire init_wait_done_i_1__0_n_0;
  wire init_wait_done_reg_n_0;
  wire \mmcm_lock_count[2]_i_1__0_n_0 ;
  wire \mmcm_lock_count[3]_i_1__0_n_0 ;
  wire \mmcm_lock_count[4]_i_1__0_n_0 ;
  wire \mmcm_lock_count[5]_i_1__0_n_0 ;
  wire \mmcm_lock_count[6]_i_1__0_n_0 ;
  wire \mmcm_lock_count[7]_i_2__0_n_0 ;
  wire \mmcm_lock_count[7]_i_3__0_n_0 ;
  wire [7:0]mmcm_lock_count_reg;
  wire mmcm_lock_i;
  wire mmcm_lock_reclocked;
  wire mmcm_lock_reclocked_i_1_n_0;
  wire mmcm_lock_reclocked_i_2__0_n_0;
  wire [0:0]out;
  wire [6:1]p_0_in__2;
  wire [1:0]p_0_in__3;
  wire reset_time_out_i_3_n_0;
  wire reset_time_out_i_4_n_0;
  wire reset_time_out_reg_n_0;
  wire run_phase_alignment_int_i_1__0_n_0;
  wire run_phase_alignment_int_reg_n_0;
  wire run_phase_alignment_int_s3_reg_n_0;
  wire rx_fsm_reset_done_int_i_5_n_0;
  wire rx_fsm_reset_done_int_i_6_n_0;
  wire rx_fsm_reset_done_int_s2;
  wire rx_fsm_reset_done_int_s3;
  wire [3:0]rx_state;
  wire [3:0]rx_state__0;
  wire rxresetdone_s2;
  wire rxresetdone_s3;
  wire sync_cplllock_n_0;
  wire sync_data_valid_n_0;
  wire sync_data_valid_n_1;
  wire sync_data_valid_n_5;
  wire sync_mmcm_lock_reclocked_n_0;
  wire time_out_100us_i_1_n_0;
  wire time_out_100us_i_2_n_0;
  wire time_out_100us_i_3_n_0;
  wire time_out_100us_reg_n_0;
  wire time_out_1us_i_1_n_0;
  wire time_out_1us_i_2_n_0;
  wire time_out_1us_i_3_n_0;
  wire time_out_1us_reg_n_0;
  wire time_out_2ms_i_1_n_0;
  wire time_out_2ms_i_2_n_0;
  wire time_out_2ms_i_3__0_n_0;
  wire time_out_2ms_i_4_n_0;
  wire time_out_2ms_reg_n_0;
  wire time_out_counter;
  wire \time_out_counter[0]_i_3_n_0 ;
  wire [18:0]time_out_counter_reg;
  wire \time_out_counter_reg[0]_i_2__0_n_0 ;
  wire \time_out_counter_reg[0]_i_2__0_n_1 ;
  wire \time_out_counter_reg[0]_i_2__0_n_2 ;
  wire \time_out_counter_reg[0]_i_2__0_n_3 ;
  wire \time_out_counter_reg[0]_i_2__0_n_4 ;
  wire \time_out_counter_reg[0]_i_2__0_n_5 ;
  wire \time_out_counter_reg[0]_i_2__0_n_6 ;
  wire \time_out_counter_reg[0]_i_2__0_n_7 ;
  wire \time_out_counter_reg[12]_i_1__0_n_0 ;
  wire \time_out_counter_reg[12]_i_1__0_n_1 ;
  wire \time_out_counter_reg[12]_i_1__0_n_2 ;
  wire \time_out_counter_reg[12]_i_1__0_n_3 ;
  wire \time_out_counter_reg[12]_i_1__0_n_4 ;
  wire \time_out_counter_reg[12]_i_1__0_n_5 ;
  wire \time_out_counter_reg[12]_i_1__0_n_6 ;
  wire \time_out_counter_reg[12]_i_1__0_n_7 ;
  wire \time_out_counter_reg[16]_i_1__0_n_2 ;
  wire \time_out_counter_reg[16]_i_1__0_n_3 ;
  wire \time_out_counter_reg[16]_i_1__0_n_5 ;
  wire \time_out_counter_reg[16]_i_1__0_n_6 ;
  wire \time_out_counter_reg[16]_i_1__0_n_7 ;
  wire \time_out_counter_reg[4]_i_1__0_n_0 ;
  wire \time_out_counter_reg[4]_i_1__0_n_1 ;
  wire \time_out_counter_reg[4]_i_1__0_n_2 ;
  wire \time_out_counter_reg[4]_i_1__0_n_3 ;
  wire \time_out_counter_reg[4]_i_1__0_n_4 ;
  wire \time_out_counter_reg[4]_i_1__0_n_5 ;
  wire \time_out_counter_reg[4]_i_1__0_n_6 ;
  wire \time_out_counter_reg[4]_i_1__0_n_7 ;
  wire \time_out_counter_reg[8]_i_1__0_n_0 ;
  wire \time_out_counter_reg[8]_i_1__0_n_1 ;
  wire \time_out_counter_reg[8]_i_1__0_n_2 ;
  wire \time_out_counter_reg[8]_i_1__0_n_3 ;
  wire \time_out_counter_reg[8]_i_1__0_n_4 ;
  wire \time_out_counter_reg[8]_i_1__0_n_5 ;
  wire \time_out_counter_reg[8]_i_1__0_n_6 ;
  wire \time_out_counter_reg[8]_i_1__0_n_7 ;
  wire time_out_wait_bypass_i_1_n_0;
  wire time_out_wait_bypass_i_2__0_n_0;
  wire time_out_wait_bypass_i_3__0_n_0;
  wire time_out_wait_bypass_i_4__0_n_0;
  wire time_out_wait_bypass_reg_n_0;
  wire time_out_wait_bypass_s2;
  wire time_out_wait_bypass_s3;
  wire time_tlock_max;
  wire time_tlock_max1;
  wire time_tlock_max1_carry__0_i_1_n_0;
  wire time_tlock_max1_carry__0_i_2_n_0;
  wire time_tlock_max1_carry__0_i_3_n_0;
  wire time_tlock_max1_carry__0_i_4_n_0;
  wire time_tlock_max1_carry__0_i_5_n_0;
  wire time_tlock_max1_carry__0_i_6_n_0;
  wire time_tlock_max1_carry__0_n_0;
  wire time_tlock_max1_carry__0_n_1;
  wire time_tlock_max1_carry__0_n_2;
  wire time_tlock_max1_carry__0_n_3;
  wire time_tlock_max1_carry__1_i_1_n_0;
  wire time_tlock_max1_carry__1_i_2_n_0;
  wire time_tlock_max1_carry__1_i_3_n_0;
  wire time_tlock_max1_carry__1_n_3;
  wire time_tlock_max1_carry_i_1_n_0;
  wire time_tlock_max1_carry_i_2_n_0;
  wire time_tlock_max1_carry_i_3_n_0;
  wire time_tlock_max1_carry_i_4_n_0;
  wire time_tlock_max1_carry_i_5_n_0;
  wire time_tlock_max1_carry_i_6_n_0;
  wire time_tlock_max1_carry_i_7_n_0;
  wire time_tlock_max1_carry_i_8_n_0;
  wire time_tlock_max1_carry_n_0;
  wire time_tlock_max1_carry_n_1;
  wire time_tlock_max1_carry_n_2;
  wire time_tlock_max1_carry_n_3;
  wire time_tlock_max_i_1_n_0;
  wire \wait_bypass_count[0]_i_1__0_n_0 ;
  wire \wait_bypass_count[0]_i_2__0_n_0 ;
  wire \wait_bypass_count[0]_i_4_n_0 ;
  wire [12:0]wait_bypass_count_reg;
  wire \wait_bypass_count_reg[0]_i_3__0_n_0 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_1 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_2 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_3 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_4 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_5 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_6 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_7 ;
  wire \wait_bypass_count_reg[12]_i_1__0_n_7 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_0 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_1 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_2 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_3 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_4 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_5 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_6 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_7 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_0 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_1 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_2 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_3 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_4 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_5 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_6 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_7 ;
  wire [0:0]wait_time_cnt0__0;
  wire \wait_time_cnt[1]_i_1__0_n_0 ;
  wire \wait_time_cnt[2]_i_1__0_n_0 ;
  wire \wait_time_cnt[3]_i_1__0_n_0 ;
  wire \wait_time_cnt[4]_i_1__0_n_0 ;
  wire \wait_time_cnt[5]_i_1__0_n_0 ;
  wire \wait_time_cnt[6]_i_1_n_0 ;
  wire \wait_time_cnt[6]_i_2__0_n_0 ;
  wire \wait_time_cnt[6]_i_3__0_n_0 ;
  wire \wait_time_cnt[6]_i_4__0_n_0 ;
  wire [6:0]wait_time_cnt_reg;
  wire [3:2]\NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED ;
  wire [3:3]\NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED ;
  wire [3:0]NLW_time_tlock_max1_carry_O_UNCONNECTED;
  wire [3:0]NLW_time_tlock_max1_carry__0_O_UNCONNECTED;
  wire [3:2]NLW_time_tlock_max1_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_time_tlock_max1_carry__1_O_UNCONNECTED;
  wire [3:0]\NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED ;
  wire [3:1]\NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h2222AAAA00000C00)) 
    \FSM_sequential_rx_state[0]_i_2 
       (.I0(time_out_2ms_reg_n_0),
        .I1(rx_state[2]),
        .I2(rx_state[3]),
        .I3(time_tlock_max),
        .I4(reset_time_out_reg_n_0),
        .I5(rx_state[1]),
        .O(\FSM_sequential_rx_state[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000AABF000F0000)) 
    \FSM_sequential_rx_state[1]_i_3 
       (.I0(reset_time_out_reg_n_0),
        .I1(time_tlock_max),
        .I2(rx_state[2]),
        .I3(rx_state[3]),
        .I4(rx_state[1]),
        .I5(rx_state[0]),
        .O(\FSM_sequential_rx_state[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000050FF2200)) 
    \FSM_sequential_rx_state[2]_i_1 
       (.I0(rx_state[1]),
        .I1(time_out_2ms_reg_n_0),
        .I2(\FSM_sequential_rx_state[2]_i_2_n_0 ),
        .I3(rx_state[0]),
        .I4(rx_state[2]),
        .I5(rx_state[3]),
        .O(rx_state__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_rx_state[2]_i_2 
       (.I0(reset_time_out_reg_n_0),
        .I1(time_tlock_max),
        .O(\FSM_sequential_rx_state[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_rx_state[3]_i_10 
       (.I0(reset_time_out_reg_n_0),
        .I1(time_out_2ms_reg_n_0),
        .O(\FSM_sequential_rx_state[3]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0000000050005300)) 
    \FSM_sequential_rx_state[3]_i_3 
       (.I0(\FSM_sequential_rx_state[3]_i_10_n_0 ),
        .I1(\wait_time_cnt[6]_i_4__0_n_0 ),
        .I2(rx_state[0]),
        .I3(rx_state[1]),
        .I4(wait_time_cnt_reg[6]),
        .I5(rx_state[3]),
        .O(\FSM_sequential_rx_state[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000023002F00)) 
    \FSM_sequential_rx_state[3]_i_7 
       (.I0(time_out_2ms_reg_n_0),
        .I1(rx_state[2]),
        .I2(rx_state[1]),
        .I3(rx_state[0]),
        .I4(\FSM_sequential_rx_state[2]_i_2_n_0 ),
        .I5(rx_state[3]),
        .O(\FSM_sequential_rx_state[3]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT5 #(
    .INIT(32'h80800080)) 
    \FSM_sequential_rx_state[3]_i_9 
       (.I0(rx_state[0]),
        .I1(rx_state[1]),
        .I2(rx_state[2]),
        .I3(time_out_2ms_reg_n_0),
        .I4(reset_time_out_reg_n_0),
        .O(\FSM_sequential_rx_state[3]_i_9_n_0 ));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[0] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[0]),
        .Q(rx_state[0]),
        .R(out));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[1] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[1]),
        .Q(rx_state[1]),
        .R(out));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[2] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[2]),
        .Q(rx_state[2]),
        .R(out));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[3] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[3]),
        .Q(rx_state[3]),
        .R(out));
  LUT5 #(
    .INIT(32'hFFFB4000)) 
    RXUSERRDY_i_1
       (.I0(rx_state[3]),
        .I1(rx_state[0]),
        .I2(rx_state[2]),
        .I3(rx_state[1]),
        .I4(gt0_rxuserrdy_t),
        .O(RXUSERRDY_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    RXUSERRDY_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(RXUSERRDY_i_1_n_0),
        .Q(gt0_rxuserrdy_t),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT5 #(
    .INIT(32'hFFFB0008)) 
    check_tlock_max_i_1
       (.I0(rx_state[2]),
        .I1(rx_state[0]),
        .I2(rx_state[1]),
        .I3(rx_state[3]),
        .I4(check_tlock_max_reg_n_0),
        .O(check_tlock_max_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    check_tlock_max_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(check_tlock_max_i_1_n_0),
        .Q(check_tlock_max_reg_n_0),
        .R(out));
  LUT5 #(
    .INIT(32'hFFEF0100)) 
    gtrxreset_i_i_1
       (.I0(rx_state[3]),
        .I1(rx_state[1]),
        .I2(rx_state[2]),
        .I3(rx_state[0]),
        .I4(GTRXRESET),
        .O(gtrxreset_i_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gtrxreset_i_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gtrxreset_i_i_1_n_0),
        .Q(GTRXRESET),
        .R(out));
  LUT3 #(
    .INIT(8'hEA)) 
    gtxe2_i_i_2
       (.I0(GTRXRESET),
        .I1(data_in),
        .I2(gtxe2_i),
        .O(SR));
  LUT1 #(
    .INIT(2'h1)) 
    \init_wait_count[0]_i_1__0 
       (.I0(init_wait_count_reg[0]),
        .O(\init_wait_count[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \init_wait_count[1]_i_1__0 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .O(p_0_in__2[1]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \init_wait_count[2]_i_1__0 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .O(p_0_in__2[2]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \init_wait_count[3]_i_1__0 
       (.I0(init_wait_count_reg[1]),
        .I1(init_wait_count_reg[2]),
        .I2(init_wait_count_reg[0]),
        .I3(init_wait_count_reg[3]),
        .O(p_0_in__2[3]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \init_wait_count[4]_i_1__0 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .O(p_0_in__2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \init_wait_count[5]_i_1__0 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .I5(init_wait_count_reg[5]),
        .O(p_0_in__2[5]));
  LUT4 #(
    .INIT(16'hFEFF)) 
    \init_wait_count[6]_i_1__0 
       (.I0(\init_wait_count[6]_i_3__0_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(\init_wait_count[6]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT4 #(
    .INIT(16'hBF40)) 
    \init_wait_count[6]_i_2__0 
       (.I0(\init_wait_count[6]_i_3__0_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(p_0_in__2[6]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \init_wait_count[6]_i_3__0 
       (.I0(init_wait_count_reg[3]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .I3(init_wait_count_reg[5]),
        .O(\init_wait_count[6]_i_3__0_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(\init_wait_count[0]_i_1__0_n_0 ),
        .Q(init_wait_count_reg[0]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[1]),
        .Q(init_wait_count_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[2]),
        .Q(init_wait_count_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[3]),
        .Q(init_wait_count_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[4]),
        .Q(init_wait_count_reg[4]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[5]),
        .Q(init_wait_count_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[6]),
        .Q(init_wait_count_reg[6]));
  LUT5 #(
    .INIT(32'hFFFF0010)) 
    init_wait_done_i_1__0
       (.I0(\init_wait_count[6]_i_3__0_n_0 ),
        .I1(init_wait_count_reg[4]),
        .I2(init_wait_count_reg[6]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_done_reg_n_0),
        .O(init_wait_done_i_1__0_n_0));
  FDCE #(
    .INIT(1'b0)) 
    init_wait_done_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .CLR(out),
        .D(init_wait_done_i_1__0_n_0),
        .Q(init_wait_done_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[0]_i_1__0 
       (.I0(mmcm_lock_count_reg[0]),
        .O(p_0_in__3[0]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \mmcm_lock_count[1]_i_1__0 
       (.I0(mmcm_lock_count_reg[0]),
        .I1(mmcm_lock_count_reg[1]),
        .O(p_0_in__3[1]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \mmcm_lock_count[2]_i_1__0 
       (.I0(mmcm_lock_count_reg[1]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[2]),
        .O(\mmcm_lock_count[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \mmcm_lock_count[3]_i_1__0 
       (.I0(mmcm_lock_count_reg[2]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[1]),
        .I3(mmcm_lock_count_reg[3]),
        .O(\mmcm_lock_count[3]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \mmcm_lock_count[4]_i_1__0 
       (.I0(mmcm_lock_count_reg[3]),
        .I1(mmcm_lock_count_reg[1]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[2]),
        .I4(mmcm_lock_count_reg[4]),
        .O(\mmcm_lock_count[4]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \mmcm_lock_count[5]_i_1__0 
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(\mmcm_lock_count[5]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \mmcm_lock_count[6]_i_1__0 
       (.I0(mmcm_lock_reclocked_i_2__0_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .O(\mmcm_lock_count[6]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'hBF)) 
    \mmcm_lock_count[7]_i_2__0 
       (.I0(mmcm_lock_reclocked_i_2__0_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \mmcm_lock_count[7]_i_3__0 
       (.I0(mmcm_lock_count_reg[6]),
        .I1(mmcm_lock_reclocked_i_2__0_n_0),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_3__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(p_0_in__3[0]),
        .Q(mmcm_lock_count_reg[0]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(p_0_in__3[1]),
        .Q(mmcm_lock_count_reg[1]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[2]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[2]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[3]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[3]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[4]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[4]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[5]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[5]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[6]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[6]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[7] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[7]_i_3__0_n_0 ),
        .Q(mmcm_lock_count_reg[7]),
        .R(sync_mmcm_lock_reclocked_n_0));
  LUT5 #(
    .INIT(32'hAAEA0000)) 
    mmcm_lock_reclocked_i_1
       (.I0(mmcm_lock_reclocked),
        .I1(mmcm_lock_count_reg[7]),
        .I2(mmcm_lock_count_reg[6]),
        .I3(mmcm_lock_reclocked_i_2__0_n_0),
        .I4(mmcm_lock_i),
        .O(mmcm_lock_reclocked_i_1_n_0));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    mmcm_lock_reclocked_i_2__0
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(mmcm_lock_reclocked_i_2__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    mmcm_lock_reclocked_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(mmcm_lock_reclocked_i_1_n_0),
        .Q(mmcm_lock_reclocked),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT2 #(
    .INIT(4'hE)) 
    reset_time_out_i_3
       (.I0(rx_state[2]),
        .I1(rx_state[3]),
        .O(reset_time_out_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT5 #(
    .INIT(32'h34347674)) 
    reset_time_out_i_4
       (.I0(rx_state[2]),
        .I1(rx_state[3]),
        .I2(rx_state[0]),
        .I3(\FSM_sequential_rx_state_reg[0]_0 ),
        .I4(rx_state[1]),
        .O(reset_time_out_i_4_n_0));
  FDSE #(
    .INIT(1'b0)) 
    reset_time_out_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(sync_data_valid_n_0),
        .Q(reset_time_out_reg_n_0),
        .S(out));
  LUT5 #(
    .INIT(32'hFEFF0010)) 
    run_phase_alignment_int_i_1__0
       (.I0(rx_state[2]),
        .I1(rx_state[1]),
        .I2(rx_state[3]),
        .I3(rx_state[0]),
        .I4(run_phase_alignment_int_reg_n_0),
        .O(run_phase_alignment_int_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(run_phase_alignment_int_i_1__0_n_0),
        .Q(run_phase_alignment_int_reg_n_0),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_s3_reg
       (.C(data_sync_reg6),
        .CE(1'b1),
        .D(data_out_0),
        .Q(run_phase_alignment_int_s3_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT2 #(
    .INIT(4'hB)) 
    rx_fsm_reset_done_int_i_5
       (.I0(rx_state[1]),
        .I1(rx_state[0]),
        .O(rx_fsm_reset_done_int_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT2 #(
    .INIT(4'h2)) 
    rx_fsm_reset_done_int_i_6
       (.I0(rx_state[3]),
        .I1(rx_state[2]),
        .O(rx_fsm_reset_done_int_i_6_n_0));
  FDRE #(
    .INIT(1'b0)) 
    rx_fsm_reset_done_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(sync_data_valid_n_1),
        .Q(data_in),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    rx_fsm_reset_done_int_s3_reg
       (.C(data_sync_reg6),
        .CE(1'b1),
        .D(rx_fsm_reset_done_int_s2),
        .Q(rx_fsm_reset_done_int_s3),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxresetdone_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(rxresetdone_s2),
        .Q(rxresetdone_s3),
        .R(1'b0));
  gig_ethernet_pcs_pma_0_sync_block_10 sync_RXRESETDONE
       (.data_out(rxresetdone_s2),
        .data_sync_reg1_0(data_sync_reg1),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_11 sync_cplllock
       (.\FSM_sequential_rx_state_reg[1] (sync_cplllock_n_0),
        .Q(rx_state[3:1]),
        .data_sync_reg1_0(data_sync_reg1_1),
        .independent_clock_bufg(independent_clock_bufg),
        .rxresetdone_s3(rxresetdone_s3));
  gig_ethernet_pcs_pma_0_sync_block_12 sync_data_valid
       (.D({rx_state__0[3],rx_state__0[1:0]}),
        .E(sync_data_valid_n_5),
        .\FSM_sequential_rx_state_reg[0] (\FSM_sequential_rx_state[3]_i_3_n_0 ),
        .\FSM_sequential_rx_state_reg[0]_0 (\FSM_sequential_rx_state[3]_i_7_n_0 ),
        .\FSM_sequential_rx_state_reg[0]_1 (\FSM_sequential_rx_state_reg[0]_0 ),
        .\FSM_sequential_rx_state_reg[0]_2 (\FSM_sequential_rx_state[0]_i_2_n_0 ),
        .\FSM_sequential_rx_state_reg[0]_3 (init_wait_done_reg_n_0),
        .\FSM_sequential_rx_state_reg[1] (sync_data_valid_n_0),
        .\FSM_sequential_rx_state_reg[1]_0 (\FSM_sequential_rx_state[1]_i_3_n_0 ),
        .\FSM_sequential_rx_state_reg[3] (\FSM_sequential_rx_state[3]_i_9_n_0 ),
        .Q(rx_state),
        .data_in(data_in),
        .data_out(data_out),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_lock_reclocked(mmcm_lock_reclocked),
        .reset_time_out_reg(sync_cplllock_n_0),
        .reset_time_out_reg_0(reset_time_out_i_3_n_0),
        .reset_time_out_reg_1(reset_time_out_i_4_n_0),
        .reset_time_out_reg_2(reset_time_out_reg_n_0),
        .rx_fsm_reset_done_int_reg(sync_data_valid_n_1),
        .rx_fsm_reset_done_int_reg_0(rx_fsm_reset_done_int_i_5_n_0),
        .rx_fsm_reset_done_int_reg_1(time_out_100us_reg_n_0),
        .rx_fsm_reset_done_int_reg_2(time_out_1us_reg_n_0),
        .rx_fsm_reset_done_int_reg_3(rx_fsm_reset_done_int_i_6_n_0),
        .time_out_wait_bypass_s3(time_out_wait_bypass_s3));
  gig_ethernet_pcs_pma_0_sync_block_13 sync_mmcm_lock_reclocked
       (.SR(sync_mmcm_lock_reclocked_n_0),
        .data_out(mmcm_lock_i),
        .data_sync_reg1_0(data_sync_reg1_0),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_14 sync_run_phase_alignment_int
       (.data_in(run_phase_alignment_int_reg_n_0),
        .data_out(data_out_0),
        .data_sync_reg1_0(data_sync_reg6));
  gig_ethernet_pcs_pma_0_sync_block_15 sync_time_out_wait_bypass
       (.data_in(time_out_wait_bypass_reg_n_0),
        .data_out(time_out_wait_bypass_s2),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_16 sync_tx_fsm_reset_done_int
       (.data_in(data_in),
        .data_out(rx_fsm_reset_done_int_s2),
        .data_sync_reg6_0(data_sync_reg6));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000100)) 
    time_out_100us_i_1
       (.I0(time_out_2ms_i_4_n_0),
        .I1(time_out_counter_reg[17]),
        .I2(time_out_counter_reg[16]),
        .I3(time_out_100us_i_2_n_0),
        .I4(time_out_100us_i_3_n_0),
        .I5(time_out_100us_reg_n_0),
        .O(time_out_100us_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000002000000000)) 
    time_out_100us_i_2
       (.I0(time_out_counter_reg[10]),
        .I1(time_out_counter_reg[12]),
        .I2(time_out_counter_reg[5]),
        .I3(time_out_counter_reg[7]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[14]),
        .O(time_out_100us_i_2_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    time_out_100us_i_3
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[0]),
        .I2(time_out_counter_reg[1]),
        .I3(time_out_counter_reg[15]),
        .I4(time_out_counter_reg[13]),
        .O(time_out_100us_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_100us_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_100us_i_1_n_0),
        .Q(time_out_100us_reg_n_0),
        .R(reset_time_out_reg_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF00100000)) 
    time_out_1us_i_1
       (.I0(time_out_2ms_i_2_n_0),
        .I1(time_out_1us_i_2_n_0),
        .I2(time_out_counter_reg[3]),
        .I3(time_out_counter_reg[2]),
        .I4(time_out_1us_i_3_n_0),
        .I5(time_out_1us_reg_n_0),
        .O(time_out_1us_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'hE)) 
    time_out_1us_i_2
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[17]),
        .O(time_out_1us_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    time_out_1us_i_3
       (.I0(time_out_counter_reg[9]),
        .I1(time_out_counter_reg[11]),
        .I2(time_out_counter_reg[6]),
        .I3(time_out_counter_reg[8]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[12]),
        .O(time_out_1us_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_1us_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_1us_i_1_n_0),
        .Q(time_out_1us_reg_n_0),
        .R(reset_time_out_reg_n_0));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT4 #(
    .INIT(16'hFF01)) 
    time_out_2ms_i_1
       (.I0(time_out_2ms_i_2_n_0),
        .I1(time_out_2ms_i_3__0_n_0),
        .I2(time_out_2ms_i_4_n_0),
        .I3(time_out_2ms_reg_n_0),
        .O(time_out_2ms_i_1_n_0));
  LUT5 #(
    .INIT(32'hFFFFFEFF)) 
    time_out_2ms_i_2
       (.I0(time_out_counter_reg[10]),
        .I1(time_out_counter_reg[14]),
        .I2(time_out_counter_reg[5]),
        .I3(time_out_counter_reg[7]),
        .I4(time_out_100us_i_3_n_0),
        .O(time_out_2ms_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT4 #(
    .INIT(16'hDFFF)) 
    time_out_2ms_i_3__0
       (.I0(time_out_counter_reg[12]),
        .I1(time_out_counter_reg[16]),
        .I2(time_out_counter_reg[18]),
        .I3(time_out_counter_reg[17]),
        .O(time_out_2ms_i_3__0_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEFFFFFF)) 
    time_out_2ms_i_4
       (.I0(time_out_counter_reg[2]),
        .I1(time_out_counter_reg[3]),
        .I2(time_out_counter_reg[8]),
        .I3(time_out_counter_reg[9]),
        .I4(time_out_counter_reg[11]),
        .I5(time_out_counter_reg[6]),
        .O(time_out_2ms_i_4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_2ms_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_2ms_i_1_n_0),
        .Q(time_out_2ms_reg_n_0),
        .R(reset_time_out_reg_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFF7FF)) 
    \time_out_counter[0]_i_1 
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[18]),
        .I2(time_out_counter_reg[16]),
        .I3(time_out_counter_reg[12]),
        .I4(time_out_2ms_i_2_n_0),
        .I5(time_out_2ms_i_4_n_0),
        .O(time_out_counter));
  LUT1 #(
    .INIT(2'h1)) 
    \time_out_counter[0]_i_3 
       (.I0(time_out_counter_reg[0]),
        .O(\time_out_counter[0]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[0] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_7 ),
        .Q(time_out_counter_reg[0]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[0]_i_2__0 
       (.CI(1'b0),
        .CO({\time_out_counter_reg[0]_i_2__0_n_0 ,\time_out_counter_reg[0]_i_2__0_n_1 ,\time_out_counter_reg[0]_i_2__0_n_2 ,\time_out_counter_reg[0]_i_2__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\time_out_counter_reg[0]_i_2__0_n_4 ,\time_out_counter_reg[0]_i_2__0_n_5 ,\time_out_counter_reg[0]_i_2__0_n_6 ,\time_out_counter_reg[0]_i_2__0_n_7 }),
        .S({time_out_counter_reg[3:1],\time_out_counter[0]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[10] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[10]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[11] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[11]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[12] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[12]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[12]_i_1__0 
       (.CI(\time_out_counter_reg[8]_i_1__0_n_0 ),
        .CO({\time_out_counter_reg[12]_i_1__0_n_0 ,\time_out_counter_reg[12]_i_1__0_n_1 ,\time_out_counter_reg[12]_i_1__0_n_2 ,\time_out_counter_reg[12]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[12]_i_1__0_n_4 ,\time_out_counter_reg[12]_i_1__0_n_5 ,\time_out_counter_reg[12]_i_1__0_n_6 ,\time_out_counter_reg[12]_i_1__0_n_7 }),
        .S(time_out_counter_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[13] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[13]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[14] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[14]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[15] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[15]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[16] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[16]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[16]_i_1__0 
       (.CI(\time_out_counter_reg[12]_i_1__0_n_0 ),
        .CO({\NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED [3:2],\time_out_counter_reg[16]_i_1__0_n_2 ,\time_out_counter_reg[16]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED [3],\time_out_counter_reg[16]_i_1__0_n_5 ,\time_out_counter_reg[16]_i_1__0_n_6 ,\time_out_counter_reg[16]_i_1__0_n_7 }),
        .S({1'b0,time_out_counter_reg[18:16]}));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[17] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[17]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[18] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[18]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[1] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_6 ),
        .Q(time_out_counter_reg[1]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[2] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_5 ),
        .Q(time_out_counter_reg[2]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[3] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_4 ),
        .Q(time_out_counter_reg[3]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[4] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[4]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[4]_i_1__0 
       (.CI(\time_out_counter_reg[0]_i_2__0_n_0 ),
        .CO({\time_out_counter_reg[4]_i_1__0_n_0 ,\time_out_counter_reg[4]_i_1__0_n_1 ,\time_out_counter_reg[4]_i_1__0_n_2 ,\time_out_counter_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[4]_i_1__0_n_4 ,\time_out_counter_reg[4]_i_1__0_n_5 ,\time_out_counter_reg[4]_i_1__0_n_6 ,\time_out_counter_reg[4]_i_1__0_n_7 }),
        .S(time_out_counter_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[5] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[5]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[6] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[6]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[7] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[7]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[8] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[8]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[8]_i_1__0 
       (.CI(\time_out_counter_reg[4]_i_1__0_n_0 ),
        .CO({\time_out_counter_reg[8]_i_1__0_n_0 ,\time_out_counter_reg[8]_i_1__0_n_1 ,\time_out_counter_reg[8]_i_1__0_n_2 ,\time_out_counter_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[8]_i_1__0_n_4 ,\time_out_counter_reg[8]_i_1__0_n_5 ,\time_out_counter_reg[8]_i_1__0_n_6 ,\time_out_counter_reg[8]_i_1__0_n_7 }),
        .S(time_out_counter_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[9] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[9]),
        .R(reset_time_out_reg_n_0));
  LUT4 #(
    .INIT(16'hAB00)) 
    time_out_wait_bypass_i_1
       (.I0(time_out_wait_bypass_reg_n_0),
        .I1(rx_fsm_reset_done_int_s3),
        .I2(time_out_wait_bypass_i_2__0_n_0),
        .I3(run_phase_alignment_int_s3_reg_n_0),
        .O(time_out_wait_bypass_i_1_n_0));
  LUT5 #(
    .INIT(32'hFBFFFFFF)) 
    time_out_wait_bypass_i_2__0
       (.I0(time_out_wait_bypass_i_3__0_n_0),
        .I1(wait_bypass_count_reg[1]),
        .I2(wait_bypass_count_reg[11]),
        .I3(wait_bypass_count_reg[0]),
        .I4(time_out_wait_bypass_i_4__0_n_0),
        .O(time_out_wait_bypass_i_2__0_n_0));
  LUT4 #(
    .INIT(16'hDFFF)) 
    time_out_wait_bypass_i_3__0
       (.I0(wait_bypass_count_reg[9]),
        .I1(wait_bypass_count_reg[4]),
        .I2(wait_bypass_count_reg[12]),
        .I3(wait_bypass_count_reg[2]),
        .O(time_out_wait_bypass_i_3__0_n_0));
  LUT6 #(
    .INIT(64'h0000000400000000)) 
    time_out_wait_bypass_i_4__0
       (.I0(wait_bypass_count_reg[5]),
        .I1(wait_bypass_count_reg[7]),
        .I2(wait_bypass_count_reg[3]),
        .I3(wait_bypass_count_reg[6]),
        .I4(wait_bypass_count_reg[10]),
        .I5(wait_bypass_count_reg[8]),
        .O(time_out_wait_bypass_i_4__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_reg
       (.C(data_sync_reg6),
        .CE(1'b1),
        .D(time_out_wait_bypass_i_1_n_0),
        .Q(time_out_wait_bypass_reg_n_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_wait_bypass_s2),
        .Q(time_out_wait_bypass_s3),
        .R(1'b0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 time_tlock_max1_carry
       (.CI(1'b0),
        .CO({time_tlock_max1_carry_n_0,time_tlock_max1_carry_n_1,time_tlock_max1_carry_n_2,time_tlock_max1_carry_n_3}),
        .CYINIT(1'b0),
        .DI({time_tlock_max1_carry_i_1_n_0,time_tlock_max1_carry_i_2_n_0,time_tlock_max1_carry_i_3_n_0,time_tlock_max1_carry_i_4_n_0}),
        .O(NLW_time_tlock_max1_carry_O_UNCONNECTED[3:0]),
        .S({time_tlock_max1_carry_i_5_n_0,time_tlock_max1_carry_i_6_n_0,time_tlock_max1_carry_i_7_n_0,time_tlock_max1_carry_i_8_n_0}));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 time_tlock_max1_carry__0
       (.CI(time_tlock_max1_carry_n_0),
        .CO({time_tlock_max1_carry__0_n_0,time_tlock_max1_carry__0_n_1,time_tlock_max1_carry__0_n_2,time_tlock_max1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({time_out_counter_reg[15],time_tlock_max1_carry__0_i_1_n_0,1'b0,time_tlock_max1_carry__0_i_2_n_0}),
        .O(NLW_time_tlock_max1_carry__0_O_UNCONNECTED[3:0]),
        .S({time_tlock_max1_carry__0_i_3_n_0,time_tlock_max1_carry__0_i_4_n_0,time_tlock_max1_carry__0_i_5_n_0,time_tlock_max1_carry__0_i_6_n_0}));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry__0_i_1
       (.I0(time_out_counter_reg[12]),
        .I1(time_out_counter_reg[13]),
        .O(time_tlock_max1_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    time_tlock_max1_carry__0_i_2
       (.I0(time_out_counter_reg[8]),
        .I1(time_out_counter_reg[9]),
        .O(time_tlock_max1_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry__0_i_3
       (.I0(time_out_counter_reg[14]),
        .I1(time_out_counter_reg[15]),
        .O(time_tlock_max1_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry__0_i_4
       (.I0(time_out_counter_reg[13]),
        .I1(time_out_counter_reg[12]),
        .O(time_tlock_max1_carry__0_i_4_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    time_tlock_max1_carry__0_i_5
       (.I0(time_out_counter_reg[10]),
        .I1(time_out_counter_reg[11]),
        .O(time_tlock_max1_carry__0_i_5_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry__0_i_6
       (.I0(time_out_counter_reg[9]),
        .I1(time_out_counter_reg[8]),
        .O(time_tlock_max1_carry__0_i_6_n_0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 time_tlock_max1_carry__1
       (.CI(time_tlock_max1_carry__0_n_0),
        .CO({NLW_time_tlock_max1_carry__1_CO_UNCONNECTED[3:2],time_tlock_max1,time_tlock_max1_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,time_out_counter_reg[18],time_tlock_max1_carry__1_i_1_n_0}),
        .O(NLW_time_tlock_max1_carry__1_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,time_tlock_max1_carry__1_i_2_n_0,time_tlock_max1_carry__1_i_3_n_0}));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry__1_i_1
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[17]),
        .O(time_tlock_max1_carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    time_tlock_max1_carry__1_i_2
       (.I0(time_out_counter_reg[18]),
        .O(time_tlock_max1_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry__1_i_3
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[16]),
        .O(time_tlock_max1_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_1
       (.I0(time_out_counter_reg[6]),
        .I1(time_out_counter_reg[7]),
        .O(time_tlock_max1_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    time_tlock_max1_carry_i_2
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[5]),
        .O(time_tlock_max1_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_3
       (.I0(time_out_counter_reg[2]),
        .I1(time_out_counter_reg[3]),
        .O(time_tlock_max1_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_4
       (.I0(time_out_counter_reg[0]),
        .I1(time_out_counter_reg[1]),
        .O(time_tlock_max1_carry_i_4_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_5
       (.I0(time_out_counter_reg[7]),
        .I1(time_out_counter_reg[6]),
        .O(time_tlock_max1_carry_i_5_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry_i_6
       (.I0(time_out_counter_reg[5]),
        .I1(time_out_counter_reg[4]),
        .O(time_tlock_max1_carry_i_6_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_7
       (.I0(time_out_counter_reg[3]),
        .I1(time_out_counter_reg[2]),
        .O(time_tlock_max1_carry_i_7_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_8
       (.I0(time_out_counter_reg[1]),
        .I1(time_out_counter_reg[0]),
        .O(time_tlock_max1_carry_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    time_tlock_max_i_1
       (.I0(check_tlock_max_reg_n_0),
        .I1(time_tlock_max1),
        .I2(time_tlock_max),
        .O(time_tlock_max_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_tlock_max_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_tlock_max_i_1_n_0),
        .Q(time_tlock_max),
        .R(reset_time_out_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_1__0 
       (.I0(run_phase_alignment_int_s3_reg_n_0),
        .O(\wait_bypass_count[0]_i_1__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \wait_bypass_count[0]_i_2__0 
       (.I0(time_out_wait_bypass_i_2__0_n_0),
        .I1(rx_fsm_reset_done_int_s3),
        .O(\wait_bypass_count[0]_i_2__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_4 
       (.I0(wait_bypass_count_reg[0]),
        .O(\wait_bypass_count[0]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[0] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_7 ),
        .Q(wait_bypass_count_reg[0]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[0]_i_3__0 
       (.CI(1'b0),
        .CO({\wait_bypass_count_reg[0]_i_3__0_n_0 ,\wait_bypass_count_reg[0]_i_3__0_n_1 ,\wait_bypass_count_reg[0]_i_3__0_n_2 ,\wait_bypass_count_reg[0]_i_3__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\wait_bypass_count_reg[0]_i_3__0_n_4 ,\wait_bypass_count_reg[0]_i_3__0_n_5 ,\wait_bypass_count_reg[0]_i_3__0_n_6 ,\wait_bypass_count_reg[0]_i_3__0_n_7 }),
        .S({wait_bypass_count_reg[3:1],\wait_bypass_count[0]_i_4_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[10] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_5 ),
        .Q(wait_bypass_count_reg[10]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[11] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_4 ),
        .Q(wait_bypass_count_reg[11]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[12] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[12]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[12]_i_1__0 
       (.CI(\wait_bypass_count_reg[8]_i_1__0_n_0 ),
        .CO(\NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED [3:1],\wait_bypass_count_reg[12]_i_1__0_n_7 }),
        .S({1'b0,1'b0,1'b0,wait_bypass_count_reg[12]}));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[1] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_6 ),
        .Q(wait_bypass_count_reg[1]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[2] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_5 ),
        .Q(wait_bypass_count_reg[2]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[3] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_4 ),
        .Q(wait_bypass_count_reg[3]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[4] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[4]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[4]_i_1__0 
       (.CI(\wait_bypass_count_reg[0]_i_3__0_n_0 ),
        .CO({\wait_bypass_count_reg[4]_i_1__0_n_0 ,\wait_bypass_count_reg[4]_i_1__0_n_1 ,\wait_bypass_count_reg[4]_i_1__0_n_2 ,\wait_bypass_count_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[4]_i_1__0_n_4 ,\wait_bypass_count_reg[4]_i_1__0_n_5 ,\wait_bypass_count_reg[4]_i_1__0_n_6 ,\wait_bypass_count_reg[4]_i_1__0_n_7 }),
        .S(wait_bypass_count_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[5] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_6 ),
        .Q(wait_bypass_count_reg[5]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[6] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_5 ),
        .Q(wait_bypass_count_reg[6]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[7] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_4 ),
        .Q(wait_bypass_count_reg[7]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[8] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[8]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[8]_i_1__0 
       (.CI(\wait_bypass_count_reg[4]_i_1__0_n_0 ),
        .CO({\wait_bypass_count_reg[8]_i_1__0_n_0 ,\wait_bypass_count_reg[8]_i_1__0_n_1 ,\wait_bypass_count_reg[8]_i_1__0_n_2 ,\wait_bypass_count_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[8]_i_1__0_n_4 ,\wait_bypass_count_reg[8]_i_1__0_n_5 ,\wait_bypass_count_reg[8]_i_1__0_n_6 ,\wait_bypass_count_reg[8]_i_1__0_n_7 }),
        .S(wait_bypass_count_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[9] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_6 ),
        .Q(wait_bypass_count_reg[9]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \wait_time_cnt[0]_i_1__0 
       (.I0(wait_time_cnt_reg[0]),
        .O(wait_time_cnt0__0));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[1]_i_1__0 
       (.I0(wait_time_cnt_reg[0]),
        .I1(wait_time_cnt_reg[1]),
        .O(\wait_time_cnt[1]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'hE1)) 
    \wait_time_cnt[2]_i_1__0 
       (.I0(wait_time_cnt_reg[1]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[2]),
        .O(\wait_time_cnt[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT4 #(
    .INIT(16'hFE01)) 
    \wait_time_cnt[3]_i_1__0 
       (.I0(wait_time_cnt_reg[2]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[1]),
        .I3(wait_time_cnt_reg[3]),
        .O(\wait_time_cnt[3]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT5 #(
    .INIT(32'hFFFE0001)) 
    \wait_time_cnt[4]_i_1__0 
       (.I0(wait_time_cnt_reg[3]),
        .I1(wait_time_cnt_reg[1]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[2]),
        .I4(wait_time_cnt_reg[4]),
        .O(\wait_time_cnt[4]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000001)) 
    \wait_time_cnt[5]_i_1__0 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[5]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \wait_time_cnt[6]_i_1 
       (.I0(rx_state[0]),
        .I1(rx_state[1]),
        .I2(rx_state[3]),
        .O(\wait_time_cnt[6]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \wait_time_cnt[6]_i_2__0 
       (.I0(\wait_time_cnt[6]_i_4__0_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(\wait_time_cnt[6]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[6]_i_3__0 
       (.I0(\wait_time_cnt[6]_i_4__0_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(\wait_time_cnt[6]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \wait_time_cnt[6]_i_4__0 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[6]_i_4__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(wait_time_cnt0__0),
        .Q(wait_time_cnt_reg[0]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[1]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[1]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[2]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[2]),
        .S(\wait_time_cnt[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[3]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[3]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[4]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[4]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[5]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[5]),
        .S(\wait_time_cnt[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[6]_i_3__0_n_0 ),
        .Q(wait_time_cnt_reg[6]),
        .S(\wait_time_cnt[6]_i_1_n_0 ));
endmodule

module gig_ethernet_pcs_pma_0_TX_STARTUP_FSM
   (mmcm_reset,
    gt0_cpllreset_t,
    data_in,
    gt0_txuserrdy_t,
    gt0_gttxreset_in0_out,
    independent_clock_bufg,
    data_sync_reg1,
    out,
    gtxe2_i,
    gt0_cpllrefclklost_i,
    data_sync_reg1_0,
    data_sync_reg1_1,
    data_sync_reg1_2);
  output mmcm_reset;
  output gt0_cpllreset_t;
  output data_in;
  output gt0_txuserrdy_t;
  output gt0_gttxreset_in0_out;
  input independent_clock_bufg;
  input data_sync_reg1;
  input [0:0]out;
  input gtxe2_i;
  input gt0_cpllrefclklost_i;
  input data_sync_reg1_0;
  input data_sync_reg1_1;
  input data_sync_reg1_2;

  wire CPLL_RESET_i_1_n_0;
  wire CPLL_RESET_i_2_n_0;
  wire \FSM_sequential_tx_state[0]_i_2_n_0 ;
  wire \FSM_sequential_tx_state[0]_i_3_n_0 ;
  wire \FSM_sequential_tx_state[2]_i_2_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_3_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_4_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_6_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_7_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_8_n_0 ;
  wire GTTXRESET;
  wire MMCM_RESET_i_1_n_0;
  wire TXUSERRDY_i_1_n_0;
  wire clear;
  wire data_in;
  wire data_out;
  wire data_sync_reg1;
  wire data_sync_reg1_0;
  wire data_sync_reg1_1;
  wire data_sync_reg1_2;
  wire gt0_cpllrefclklost_i;
  wire gt0_cpllreset_t;
  wire gt0_gttxreset_in0_out;
  wire gt0_txuserrdy_t;
  wire gttxreset_i_i_1_n_0;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire \init_wait_count[0]_i_1_n_0 ;
  wire \init_wait_count[6]_i_1_n_0 ;
  wire \init_wait_count[6]_i_3_n_0 ;
  wire [6:0]init_wait_count_reg;
  wire init_wait_done_i_1_n_0;
  wire init_wait_done_reg_n_0;
  wire \mmcm_lock_count[2]_i_1_n_0 ;
  wire \mmcm_lock_count[3]_i_1_n_0 ;
  wire \mmcm_lock_count[4]_i_1_n_0 ;
  wire \mmcm_lock_count[5]_i_1_n_0 ;
  wire \mmcm_lock_count[6]_i_1_n_0 ;
  wire \mmcm_lock_count[7]_i_2_n_0 ;
  wire \mmcm_lock_count[7]_i_3_n_0 ;
  wire [7:0]mmcm_lock_count_reg;
  wire mmcm_lock_i;
  wire mmcm_lock_reclocked;
  wire mmcm_lock_reclocked_i_1_n_0;
  wire mmcm_lock_reclocked_i_2_n_0;
  wire mmcm_reset;
  wire [0:0]out;
  wire [6:1]p_0_in__0;
  wire [1:0]p_0_in__1;
  wire pll_reset_asserted_i_1_n_0;
  wire pll_reset_asserted_i_2_n_0;
  wire pll_reset_asserted_reg_n_0;
  wire refclk_stable_count;
  wire \refclk_stable_count[0]_i_3_n_0 ;
  wire \refclk_stable_count[0]_i_4_n_0 ;
  wire \refclk_stable_count[0]_i_5_n_0 ;
  wire \refclk_stable_count[0]_i_6_n_0 ;
  wire \refclk_stable_count[0]_i_7_n_0 ;
  wire \refclk_stable_count[0]_i_8_n_0 ;
  wire \refclk_stable_count[0]_i_9_n_0 ;
  wire [31:0]refclk_stable_count_reg;
  wire \refclk_stable_count_reg[0]_i_2_n_0 ;
  wire \refclk_stable_count_reg[0]_i_2_n_1 ;
  wire \refclk_stable_count_reg[0]_i_2_n_2 ;
  wire \refclk_stable_count_reg[0]_i_2_n_3 ;
  wire \refclk_stable_count_reg[0]_i_2_n_4 ;
  wire \refclk_stable_count_reg[0]_i_2_n_5 ;
  wire \refclk_stable_count_reg[0]_i_2_n_6 ;
  wire \refclk_stable_count_reg[0]_i_2_n_7 ;
  wire \refclk_stable_count_reg[12]_i_1_n_0 ;
  wire \refclk_stable_count_reg[12]_i_1_n_1 ;
  wire \refclk_stable_count_reg[12]_i_1_n_2 ;
  wire \refclk_stable_count_reg[12]_i_1_n_3 ;
  wire \refclk_stable_count_reg[12]_i_1_n_4 ;
  wire \refclk_stable_count_reg[12]_i_1_n_5 ;
  wire \refclk_stable_count_reg[12]_i_1_n_6 ;
  wire \refclk_stable_count_reg[12]_i_1_n_7 ;
  wire \refclk_stable_count_reg[16]_i_1_n_0 ;
  wire \refclk_stable_count_reg[16]_i_1_n_1 ;
  wire \refclk_stable_count_reg[16]_i_1_n_2 ;
  wire \refclk_stable_count_reg[16]_i_1_n_3 ;
  wire \refclk_stable_count_reg[16]_i_1_n_4 ;
  wire \refclk_stable_count_reg[16]_i_1_n_5 ;
  wire \refclk_stable_count_reg[16]_i_1_n_6 ;
  wire \refclk_stable_count_reg[16]_i_1_n_7 ;
  wire \refclk_stable_count_reg[20]_i_1_n_0 ;
  wire \refclk_stable_count_reg[20]_i_1_n_1 ;
  wire \refclk_stable_count_reg[20]_i_1_n_2 ;
  wire \refclk_stable_count_reg[20]_i_1_n_3 ;
  wire \refclk_stable_count_reg[20]_i_1_n_4 ;
  wire \refclk_stable_count_reg[20]_i_1_n_5 ;
  wire \refclk_stable_count_reg[20]_i_1_n_6 ;
  wire \refclk_stable_count_reg[20]_i_1_n_7 ;
  wire \refclk_stable_count_reg[24]_i_1_n_0 ;
  wire \refclk_stable_count_reg[24]_i_1_n_1 ;
  wire \refclk_stable_count_reg[24]_i_1_n_2 ;
  wire \refclk_stable_count_reg[24]_i_1_n_3 ;
  wire \refclk_stable_count_reg[24]_i_1_n_4 ;
  wire \refclk_stable_count_reg[24]_i_1_n_5 ;
  wire \refclk_stable_count_reg[24]_i_1_n_6 ;
  wire \refclk_stable_count_reg[24]_i_1_n_7 ;
  wire \refclk_stable_count_reg[28]_i_1_n_1 ;
  wire \refclk_stable_count_reg[28]_i_1_n_2 ;
  wire \refclk_stable_count_reg[28]_i_1_n_3 ;
  wire \refclk_stable_count_reg[28]_i_1_n_4 ;
  wire \refclk_stable_count_reg[28]_i_1_n_5 ;
  wire \refclk_stable_count_reg[28]_i_1_n_6 ;
  wire \refclk_stable_count_reg[28]_i_1_n_7 ;
  wire \refclk_stable_count_reg[4]_i_1_n_0 ;
  wire \refclk_stable_count_reg[4]_i_1_n_1 ;
  wire \refclk_stable_count_reg[4]_i_1_n_2 ;
  wire \refclk_stable_count_reg[4]_i_1_n_3 ;
  wire \refclk_stable_count_reg[4]_i_1_n_4 ;
  wire \refclk_stable_count_reg[4]_i_1_n_5 ;
  wire \refclk_stable_count_reg[4]_i_1_n_6 ;
  wire \refclk_stable_count_reg[4]_i_1_n_7 ;
  wire \refclk_stable_count_reg[8]_i_1_n_0 ;
  wire \refclk_stable_count_reg[8]_i_1_n_1 ;
  wire \refclk_stable_count_reg[8]_i_1_n_2 ;
  wire \refclk_stable_count_reg[8]_i_1_n_3 ;
  wire \refclk_stable_count_reg[8]_i_1_n_4 ;
  wire \refclk_stable_count_reg[8]_i_1_n_5 ;
  wire \refclk_stable_count_reg[8]_i_1_n_6 ;
  wire \refclk_stable_count_reg[8]_i_1_n_7 ;
  wire refclk_stable_i_1_n_0;
  wire refclk_stable_i_2_n_0;
  wire refclk_stable_i_3_n_0;
  wire refclk_stable_i_4_n_0;
  wire refclk_stable_i_5_n_0;
  wire refclk_stable_i_6_n_0;
  wire refclk_stable_reg_n_0;
  wire reset_time_out;
  wire reset_time_out_i_2__0_n_0;
  wire run_phase_alignment_int_i_1_n_0;
  wire run_phase_alignment_int_reg_n_0;
  wire run_phase_alignment_int_s3;
  wire sel;
  wire sync_cplllock_n_0;
  wire sync_cplllock_n_1;
  wire sync_mmcm_lock_reclocked_n_0;
  wire time_out_2ms_i_1_n_0;
  wire time_out_2ms_i_2__0_n_0;
  wire time_out_2ms_i_3_n_0;
  wire time_out_2ms_i_4__0_n_0;
  wire time_out_2ms_i_5_n_0;
  wire time_out_2ms_reg_n_0;
  wire time_out_500us_i_1_n_0;
  wire time_out_500us_i_2_n_0;
  wire time_out_500us_reg_n_0;
  wire time_out_counter;
  wire \time_out_counter[0]_i_3__0_n_0 ;
  wire \time_out_counter[0]_i_4_n_0 ;
  wire [18:0]time_out_counter_reg;
  wire \time_out_counter_reg[0]_i_2_n_0 ;
  wire \time_out_counter_reg[0]_i_2_n_1 ;
  wire \time_out_counter_reg[0]_i_2_n_2 ;
  wire \time_out_counter_reg[0]_i_2_n_3 ;
  wire \time_out_counter_reg[0]_i_2_n_4 ;
  wire \time_out_counter_reg[0]_i_2_n_5 ;
  wire \time_out_counter_reg[0]_i_2_n_6 ;
  wire \time_out_counter_reg[0]_i_2_n_7 ;
  wire \time_out_counter_reg[12]_i_1_n_0 ;
  wire \time_out_counter_reg[12]_i_1_n_1 ;
  wire \time_out_counter_reg[12]_i_1_n_2 ;
  wire \time_out_counter_reg[12]_i_1_n_3 ;
  wire \time_out_counter_reg[12]_i_1_n_4 ;
  wire \time_out_counter_reg[12]_i_1_n_5 ;
  wire \time_out_counter_reg[12]_i_1_n_6 ;
  wire \time_out_counter_reg[12]_i_1_n_7 ;
  wire \time_out_counter_reg[16]_i_1_n_2 ;
  wire \time_out_counter_reg[16]_i_1_n_3 ;
  wire \time_out_counter_reg[16]_i_1_n_5 ;
  wire \time_out_counter_reg[16]_i_1_n_6 ;
  wire \time_out_counter_reg[16]_i_1_n_7 ;
  wire \time_out_counter_reg[4]_i_1_n_0 ;
  wire \time_out_counter_reg[4]_i_1_n_1 ;
  wire \time_out_counter_reg[4]_i_1_n_2 ;
  wire \time_out_counter_reg[4]_i_1_n_3 ;
  wire \time_out_counter_reg[4]_i_1_n_4 ;
  wire \time_out_counter_reg[4]_i_1_n_5 ;
  wire \time_out_counter_reg[4]_i_1_n_6 ;
  wire \time_out_counter_reg[4]_i_1_n_7 ;
  wire \time_out_counter_reg[8]_i_1_n_0 ;
  wire \time_out_counter_reg[8]_i_1_n_1 ;
  wire \time_out_counter_reg[8]_i_1_n_2 ;
  wire \time_out_counter_reg[8]_i_1_n_3 ;
  wire \time_out_counter_reg[8]_i_1_n_4 ;
  wire \time_out_counter_reg[8]_i_1_n_5 ;
  wire \time_out_counter_reg[8]_i_1_n_6 ;
  wire \time_out_counter_reg[8]_i_1_n_7 ;
  wire time_out_wait_bypass_i_1_n_0;
  wire time_out_wait_bypass_i_2_n_0;
  wire time_out_wait_bypass_i_3_n_0;
  wire time_out_wait_bypass_i_4_n_0;
  wire time_out_wait_bypass_i_5_n_0;
  wire time_out_wait_bypass_reg_n_0;
  wire time_out_wait_bypass_s2;
  wire time_out_wait_bypass_s3;
  wire time_tlock_max_i_1_n_0;
  wire time_tlock_max_i_2_n_0;
  wire time_tlock_max_i_3_n_0;
  wire time_tlock_max_i_4_n_0;
  wire time_tlock_max_reg_n_0;
  wire tx_fsm_reset_done_int_i_1_n_0;
  wire tx_fsm_reset_done_int_s2;
  wire tx_fsm_reset_done_int_s3;
  wire [3:0]tx_state;
  wire [3:0]tx_state__0;
  wire txresetdone_s2;
  wire txresetdone_s3;
  wire \wait_bypass_count[0]_i_2_n_0 ;
  wire \wait_bypass_count[0]_i_4__0_n_0 ;
  wire [16:0]wait_bypass_count_reg;
  wire \wait_bypass_count_reg[0]_i_3_n_0 ;
  wire \wait_bypass_count_reg[0]_i_3_n_1 ;
  wire \wait_bypass_count_reg[0]_i_3_n_2 ;
  wire \wait_bypass_count_reg[0]_i_3_n_3 ;
  wire \wait_bypass_count_reg[0]_i_3_n_4 ;
  wire \wait_bypass_count_reg[0]_i_3_n_5 ;
  wire \wait_bypass_count_reg[0]_i_3_n_6 ;
  wire \wait_bypass_count_reg[0]_i_3_n_7 ;
  wire \wait_bypass_count_reg[12]_i_1_n_0 ;
  wire \wait_bypass_count_reg[12]_i_1_n_1 ;
  wire \wait_bypass_count_reg[12]_i_1_n_2 ;
  wire \wait_bypass_count_reg[12]_i_1_n_3 ;
  wire \wait_bypass_count_reg[12]_i_1_n_4 ;
  wire \wait_bypass_count_reg[12]_i_1_n_5 ;
  wire \wait_bypass_count_reg[12]_i_1_n_6 ;
  wire \wait_bypass_count_reg[12]_i_1_n_7 ;
  wire \wait_bypass_count_reg[16]_i_1_n_7 ;
  wire \wait_bypass_count_reg[4]_i_1_n_0 ;
  wire \wait_bypass_count_reg[4]_i_1_n_1 ;
  wire \wait_bypass_count_reg[4]_i_1_n_2 ;
  wire \wait_bypass_count_reg[4]_i_1_n_3 ;
  wire \wait_bypass_count_reg[4]_i_1_n_4 ;
  wire \wait_bypass_count_reg[4]_i_1_n_5 ;
  wire \wait_bypass_count_reg[4]_i_1_n_6 ;
  wire \wait_bypass_count_reg[4]_i_1_n_7 ;
  wire \wait_bypass_count_reg[8]_i_1_n_0 ;
  wire \wait_bypass_count_reg[8]_i_1_n_1 ;
  wire \wait_bypass_count_reg[8]_i_1_n_2 ;
  wire \wait_bypass_count_reg[8]_i_1_n_3 ;
  wire \wait_bypass_count_reg[8]_i_1_n_4 ;
  wire \wait_bypass_count_reg[8]_i_1_n_5 ;
  wire \wait_bypass_count_reg[8]_i_1_n_6 ;
  wire \wait_bypass_count_reg[8]_i_1_n_7 ;
  wire [0:0]wait_time_cnt0;
  wire wait_time_cnt0_0;
  wire \wait_time_cnt[1]_i_1_n_0 ;
  wire \wait_time_cnt[2]_i_1_n_0 ;
  wire \wait_time_cnt[3]_i_1_n_0 ;
  wire \wait_time_cnt[4]_i_1_n_0 ;
  wire \wait_time_cnt[5]_i_1_n_0 ;
  wire \wait_time_cnt[6]_i_3_n_0 ;
  wire \wait_time_cnt[6]_i_4_n_0 ;
  wire [6:0]wait_time_cnt_reg;
  wire [3:3]\NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED ;
  wire [3:2]\NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED ;
  wire [3:0]\NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:1]\NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'hFFFFFF1F0000001F)) 
    CPLL_RESET_i_1
       (.I0(pll_reset_asserted_reg_n_0),
        .I1(gt0_cpllrefclklost_i),
        .I2(refclk_stable_reg_n_0),
        .I3(CPLL_RESET_i_2_n_0),
        .I4(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I5(gt0_cpllreset_t),
        .O(CPLL_RESET_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT2 #(
    .INIT(4'hE)) 
    CPLL_RESET_i_2
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .O(CPLL_RESET_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    CPLL_RESET_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(CPLL_RESET_i_1_n_0),
        .Q(gt0_cpllreset_t),
        .R(out));
  LUT6 #(
    .INIT(64'hF3FFF3F0F5F0F5F0)) 
    \FSM_sequential_tx_state[0]_i_1 
       (.I0(\FSM_sequential_tx_state[2]_i_2_n_0 ),
        .I1(\FSM_sequential_tx_state[0]_i_2_n_0 ),
        .I2(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I3(tx_state[2]),
        .I4(time_out_2ms_reg_n_0),
        .I5(tx_state[1]),
        .O(tx_state__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_tx_state[0]_i_2 
       (.I0(reset_time_out),
        .I1(time_out_500us_reg_n_0),
        .O(\FSM_sequential_tx_state[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_tx_state[0]_i_3 
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .O(\FSM_sequential_tx_state[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT5 #(
    .INIT(32'h005A001A)) 
    \FSM_sequential_tx_state[1]_i_1 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[0]),
        .I3(tx_state[3]),
        .I4(\FSM_sequential_tx_state[2]_i_2_n_0 ),
        .O(tx_state__0[1]));
  LUT6 #(
    .INIT(64'h04000C0C06020C0C)) 
    \FSM_sequential_tx_state[2]_i_1 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[3]),
        .I3(\FSM_sequential_tx_state[2]_i_2_n_0 ),
        .I4(tx_state[0]),
        .I5(time_out_2ms_reg_n_0),
        .O(tx_state__0[2]));
  LUT3 #(
    .INIT(8'hFD)) 
    \FSM_sequential_tx_state[2]_i_2 
       (.I0(time_tlock_max_reg_n_0),
        .I1(reset_time_out),
        .I2(mmcm_lock_reclocked),
        .O(\FSM_sequential_tx_state[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT5 #(
    .INIT(32'hF4FF4444)) 
    \FSM_sequential_tx_state[3]_i_2 
       (.I0(time_out_wait_bypass_s3),
        .I1(tx_state[3]),
        .I2(reset_time_out),
        .I3(time_out_500us_reg_n_0),
        .I4(\FSM_sequential_tx_state[3]_i_8_n_0 ),
        .O(tx_state__0[3]));
  LUT6 #(
    .INIT(64'h00BA000000000000)) 
    \FSM_sequential_tx_state[3]_i_3 
       (.I0(txresetdone_s3),
        .I1(reset_time_out),
        .I2(time_out_500us_reg_n_0),
        .I3(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I4(tx_state[2]),
        .I5(tx_state[1]),
        .O(\FSM_sequential_tx_state[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000300FF00AA)) 
    \FSM_sequential_tx_state[3]_i_4 
       (.I0(init_wait_done_reg_n_0),
        .I1(\wait_time_cnt[6]_i_4_n_0 ),
        .I2(wait_time_cnt_reg[6]),
        .I3(tx_state[0]),
        .I4(tx_state[3]),
        .I5(CPLL_RESET_i_2_n_0),
        .O(\FSM_sequential_tx_state[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0404040400040000)) 
    \FSM_sequential_tx_state[3]_i_6 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I3(reset_time_out),
        .I4(time_tlock_max_reg_n_0),
        .I5(mmcm_lock_reclocked),
        .O(\FSM_sequential_tx_state[3]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT4 #(
    .INIT(16'h1000)) 
    \FSM_sequential_tx_state[3]_i_7 
       (.I0(tx_state[2]),
        .I1(tx_state[3]),
        .I2(tx_state[0]),
        .I3(tx_state[1]),
        .O(\FSM_sequential_tx_state[3]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    \FSM_sequential_tx_state[3]_i_8 
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .O(\FSM_sequential_tx_state[3]_i_8_n_0 ));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[0] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[0]),
        .Q(tx_state[0]),
        .R(out));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[1] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[1]),
        .Q(tx_state[1]),
        .R(out));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[2] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[2]),
        .Q(tx_state[2]),
        .R(out));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[3] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[3]),
        .Q(tx_state[3]),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT5 #(
    .INIT(32'hFFF70004)) 
    MMCM_RESET_i_1
       (.I0(tx_state[2]),
        .I1(tx_state[0]),
        .I2(tx_state[3]),
        .I3(tx_state[1]),
        .I4(mmcm_reset),
        .O(MMCM_RESET_i_1_n_0));
  FDRE #(
    .INIT(1'b1)) 
    MMCM_RESET_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(MMCM_RESET_i_1_n_0),
        .Q(mmcm_reset),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT5 #(
    .INIT(32'hFFFD2000)) 
    TXUSERRDY_i_1
       (.I0(tx_state[0]),
        .I1(tx_state[3]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .I4(gt0_txuserrdy_t),
        .O(TXUSERRDY_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    TXUSERRDY_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(TXUSERRDY_i_1_n_0),
        .Q(gt0_txuserrdy_t),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT5 #(
    .INIT(32'hFFEF0100)) 
    gttxreset_i_i_1
       (.I0(tx_state[3]),
        .I1(tx_state[1]),
        .I2(tx_state[2]),
        .I3(tx_state[0]),
        .I4(GTTXRESET),
        .O(gttxreset_i_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gttxreset_i_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gttxreset_i_i_1_n_0),
        .Q(GTTXRESET),
        .R(out));
  LUT3 #(
    .INIT(8'hEA)) 
    gtxe2_i_i_3
       (.I0(GTTXRESET),
        .I1(data_in),
        .I2(gtxe2_i),
        .O(gt0_gttxreset_in0_out));
  LUT1 #(
    .INIT(2'h1)) 
    \init_wait_count[0]_i_1 
       (.I0(init_wait_count_reg[0]),
        .O(\init_wait_count[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \init_wait_count[1]_i_1 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \init_wait_count[2]_i_1 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .O(p_0_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \init_wait_count[3]_i_1 
       (.I0(init_wait_count_reg[1]),
        .I1(init_wait_count_reg[2]),
        .I2(init_wait_count_reg[0]),
        .I3(init_wait_count_reg[3]),
        .O(p_0_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \init_wait_count[4]_i_1 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .O(p_0_in__0[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \init_wait_count[5]_i_1 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .I5(init_wait_count_reg[5]),
        .O(p_0_in__0[5]));
  LUT4 #(
    .INIT(16'hFEFF)) 
    \init_wait_count[6]_i_1 
       (.I0(\init_wait_count[6]_i_3_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(\init_wait_count[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT4 #(
    .INIT(16'hBF40)) 
    \init_wait_count[6]_i_2 
       (.I0(\init_wait_count[6]_i_3_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(p_0_in__0[6]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \init_wait_count[6]_i_3 
       (.I0(init_wait_count_reg[3]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .I3(init_wait_count_reg[5]),
        .O(\init_wait_count[6]_i_3_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(\init_wait_count[0]_i_1_n_0 ),
        .Q(init_wait_count_reg[0]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[1]),
        .Q(init_wait_count_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[2]),
        .Q(init_wait_count_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[3]),
        .Q(init_wait_count_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[4]),
        .Q(init_wait_count_reg[4]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[5]),
        .Q(init_wait_count_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[6]),
        .Q(init_wait_count_reg[6]));
  LUT5 #(
    .INIT(32'hFFFF0010)) 
    init_wait_done_i_1
       (.I0(\init_wait_count[6]_i_3_n_0 ),
        .I1(init_wait_count_reg[4]),
        .I2(init_wait_count_reg[6]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_done_reg_n_0),
        .O(init_wait_done_i_1_n_0));
  FDCE #(
    .INIT(1'b0)) 
    init_wait_done_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .CLR(out),
        .D(init_wait_done_i_1_n_0),
        .Q(init_wait_done_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[0]_i_1 
       (.I0(mmcm_lock_count_reg[0]),
        .O(p_0_in__1[0]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \mmcm_lock_count[1]_i_1 
       (.I0(mmcm_lock_count_reg[0]),
        .I1(mmcm_lock_count_reg[1]),
        .O(p_0_in__1[1]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \mmcm_lock_count[2]_i_1 
       (.I0(mmcm_lock_count_reg[1]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[2]),
        .O(\mmcm_lock_count[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \mmcm_lock_count[3]_i_1 
       (.I0(mmcm_lock_count_reg[2]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[1]),
        .I3(mmcm_lock_count_reg[3]),
        .O(\mmcm_lock_count[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \mmcm_lock_count[4]_i_1 
       (.I0(mmcm_lock_count_reg[3]),
        .I1(mmcm_lock_count_reg[1]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[2]),
        .I4(mmcm_lock_count_reg[4]),
        .O(\mmcm_lock_count[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \mmcm_lock_count[5]_i_1 
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(\mmcm_lock_count[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \mmcm_lock_count[6]_i_1 
       (.I0(mmcm_lock_reclocked_i_2_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .O(\mmcm_lock_count[6]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hBF)) 
    \mmcm_lock_count[7]_i_2 
       (.I0(mmcm_lock_reclocked_i_2_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \mmcm_lock_count[7]_i_3 
       (.I0(mmcm_lock_count_reg[6]),
        .I1(mmcm_lock_reclocked_i_2_n_0),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(p_0_in__1[0]),
        .Q(mmcm_lock_count_reg[0]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(p_0_in__1[1]),
        .Q(mmcm_lock_count_reg[1]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[2]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[2]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[3]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[3]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[4]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[4]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[5]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[5]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[6]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[6]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[7] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[7]_i_3_n_0 ),
        .Q(mmcm_lock_count_reg[7]),
        .R(sync_mmcm_lock_reclocked_n_0));
  LUT5 #(
    .INIT(32'hAAEA0000)) 
    mmcm_lock_reclocked_i_1
       (.I0(mmcm_lock_reclocked),
        .I1(mmcm_lock_count_reg[7]),
        .I2(mmcm_lock_count_reg[6]),
        .I3(mmcm_lock_reclocked_i_2_n_0),
        .I4(mmcm_lock_i),
        .O(mmcm_lock_reclocked_i_1_n_0));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    mmcm_lock_reclocked_i_2
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(mmcm_lock_reclocked_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    mmcm_lock_reclocked_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(mmcm_lock_reclocked_i_1_n_0),
        .Q(mmcm_lock_reclocked),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000CD55CCCCCCCC)) 
    pll_reset_asserted_i_1
       (.I0(tx_state[3]),
        .I1(pll_reset_asserted_reg_n_0),
        .I2(gt0_cpllrefclklost_i),
        .I3(refclk_stable_reg_n_0),
        .I4(tx_state[1]),
        .I5(pll_reset_asserted_i_2_n_0),
        .O(pll_reset_asserted_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'h02)) 
    pll_reset_asserted_i_2
       (.I0(tx_state[0]),
        .I1(tx_state[3]),
        .I2(tx_state[2]),
        .O(pll_reset_asserted_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    pll_reset_asserted_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pll_reset_asserted_i_1_n_0),
        .Q(pll_reset_asserted_reg_n_0),
        .R(out));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFFFF)) 
    \refclk_stable_count[0]_i_1 
       (.I0(\refclk_stable_count[0]_i_3_n_0 ),
        .I1(\refclk_stable_count[0]_i_4_n_0 ),
        .I2(\refclk_stable_count[0]_i_5_n_0 ),
        .I3(\refclk_stable_count[0]_i_6_n_0 ),
        .I4(\refclk_stable_count[0]_i_7_n_0 ),
        .I5(\refclk_stable_count[0]_i_8_n_0 ),
        .O(refclk_stable_count));
  LUT6 #(
    .INIT(64'hFFDFFFFFFFFFFFFF)) 
    \refclk_stable_count[0]_i_3 
       (.I0(refclk_stable_count_reg[13]),
        .I1(refclk_stable_count_reg[12]),
        .I2(refclk_stable_count_reg[10]),
        .I3(refclk_stable_count_reg[11]),
        .I4(refclk_stable_count_reg[9]),
        .I5(refclk_stable_count_reg[8]),
        .O(\refclk_stable_count[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFDF)) 
    \refclk_stable_count[0]_i_4 
       (.I0(refclk_stable_count_reg[19]),
        .I1(refclk_stable_count_reg[18]),
        .I2(refclk_stable_count_reg[16]),
        .I3(refclk_stable_count_reg[17]),
        .I4(refclk_stable_count_reg[15]),
        .I5(refclk_stable_count_reg[14]),
        .O(\refclk_stable_count[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \refclk_stable_count[0]_i_5 
       (.I0(refclk_stable_count_reg[30]),
        .I1(refclk_stable_count_reg[31]),
        .I2(refclk_stable_count_reg[28]),
        .I3(refclk_stable_count_reg[29]),
        .I4(refclk_stable_count_reg[27]),
        .I5(refclk_stable_count_reg[26]),
        .O(\refclk_stable_count[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \refclk_stable_count[0]_i_6 
       (.I0(refclk_stable_count_reg[24]),
        .I1(refclk_stable_count_reg[25]),
        .I2(refclk_stable_count_reg[22]),
        .I3(refclk_stable_count_reg[23]),
        .I4(refclk_stable_count_reg[21]),
        .I5(refclk_stable_count_reg[20]),
        .O(\refclk_stable_count[0]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \refclk_stable_count[0]_i_7 
       (.I0(refclk_stable_count_reg[0]),
        .I1(refclk_stable_count_reg[1]),
        .O(\refclk_stable_count[0]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF7)) 
    \refclk_stable_count[0]_i_8 
       (.I0(refclk_stable_count_reg[6]),
        .I1(refclk_stable_count_reg[7]),
        .I2(refclk_stable_count_reg[4]),
        .I3(refclk_stable_count_reg[5]),
        .I4(refclk_stable_count_reg[3]),
        .I5(refclk_stable_count_reg[2]),
        .O(\refclk_stable_count[0]_i_8_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \refclk_stable_count[0]_i_9 
       (.I0(refclk_stable_count_reg[0]),
        .O(\refclk_stable_count[0]_i_9_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_7 ),
        .Q(refclk_stable_count_reg[0]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\refclk_stable_count_reg[0]_i_2_n_0 ,\refclk_stable_count_reg[0]_i_2_n_1 ,\refclk_stable_count_reg[0]_i_2_n_2 ,\refclk_stable_count_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\refclk_stable_count_reg[0]_i_2_n_4 ,\refclk_stable_count_reg[0]_i_2_n_5 ,\refclk_stable_count_reg[0]_i_2_n_6 ,\refclk_stable_count_reg[0]_i_2_n_7 }),
        .S({refclk_stable_count_reg[3:1],\refclk_stable_count[0]_i_9_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[10] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[11] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[12] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[12]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[12]_i_1 
       (.CI(\refclk_stable_count_reg[8]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[12]_i_1_n_0 ,\refclk_stable_count_reg[12]_i_1_n_1 ,\refclk_stable_count_reg[12]_i_1_n_2 ,\refclk_stable_count_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[12]_i_1_n_4 ,\refclk_stable_count_reg[12]_i_1_n_5 ,\refclk_stable_count_reg[12]_i_1_n_6 ,\refclk_stable_count_reg[12]_i_1_n_7 }),
        .S(refclk_stable_count_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[13] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[14] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[15] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[16] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[16]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[16]_i_1 
       (.CI(\refclk_stable_count_reg[12]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[16]_i_1_n_0 ,\refclk_stable_count_reg[16]_i_1_n_1 ,\refclk_stable_count_reg[16]_i_1_n_2 ,\refclk_stable_count_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[16]_i_1_n_4 ,\refclk_stable_count_reg[16]_i_1_n_5 ,\refclk_stable_count_reg[16]_i_1_n_6 ,\refclk_stable_count_reg[16]_i_1_n_7 }),
        .S(refclk_stable_count_reg[19:16]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[17] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[18] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[19] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_6 ),
        .Q(refclk_stable_count_reg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[20] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[20]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[20]_i_1 
       (.CI(\refclk_stable_count_reg[16]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[20]_i_1_n_0 ,\refclk_stable_count_reg[20]_i_1_n_1 ,\refclk_stable_count_reg[20]_i_1_n_2 ,\refclk_stable_count_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[20]_i_1_n_4 ,\refclk_stable_count_reg[20]_i_1_n_5 ,\refclk_stable_count_reg[20]_i_1_n_6 ,\refclk_stable_count_reg[20]_i_1_n_7 }),
        .S(refclk_stable_count_reg[23:20]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[21] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[22] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[23] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[24] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[24]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[24]_i_1 
       (.CI(\refclk_stable_count_reg[20]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[24]_i_1_n_0 ,\refclk_stable_count_reg[24]_i_1_n_1 ,\refclk_stable_count_reg[24]_i_1_n_2 ,\refclk_stable_count_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[24]_i_1_n_4 ,\refclk_stable_count_reg[24]_i_1_n_5 ,\refclk_stable_count_reg[24]_i_1_n_6 ,\refclk_stable_count_reg[24]_i_1_n_7 }),
        .S(refclk_stable_count_reg[27:24]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[25] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[26] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[27] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[28] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[28]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[28]_i_1 
       (.CI(\refclk_stable_count_reg[24]_i_1_n_0 ),
        .CO({\NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED [3],\refclk_stable_count_reg[28]_i_1_n_1 ,\refclk_stable_count_reg[28]_i_1_n_2 ,\refclk_stable_count_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[28]_i_1_n_4 ,\refclk_stable_count_reg[28]_i_1_n_5 ,\refclk_stable_count_reg[28]_i_1_n_6 ,\refclk_stable_count_reg[28]_i_1_n_7 }),
        .S(refclk_stable_count_reg[31:28]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[29] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_5 ),
        .Q(refclk_stable_count_reg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[30] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[31] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_4 ),
        .Q(refclk_stable_count_reg[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[4]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[4]_i_1 
       (.CI(\refclk_stable_count_reg[0]_i_2_n_0 ),
        .CO({\refclk_stable_count_reg[4]_i_1_n_0 ,\refclk_stable_count_reg[4]_i_1_n_1 ,\refclk_stable_count_reg[4]_i_1_n_2 ,\refclk_stable_count_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[4]_i_1_n_4 ,\refclk_stable_count_reg[4]_i_1_n_5 ,\refclk_stable_count_reg[4]_i_1_n_6 ,\refclk_stable_count_reg[4]_i_1_n_7 }),
        .S(refclk_stable_count_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[7] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[8] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[8]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[8]_i_1 
       (.CI(\refclk_stable_count_reg[4]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[8]_i_1_n_0 ,\refclk_stable_count_reg[8]_i_1_n_1 ,\refclk_stable_count_reg[8]_i_1_n_2 ,\refclk_stable_count_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[8]_i_1_n_4 ,\refclk_stable_count_reg[8]_i_1_n_5 ,\refclk_stable_count_reg[8]_i_1_n_6 ,\refclk_stable_count_reg[8]_i_1_n_7 }),
        .S(refclk_stable_count_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[9] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    refclk_stable_i_1
       (.I0(\refclk_stable_count[0]_i_7_n_0 ),
        .I1(refclk_stable_i_2_n_0),
        .I2(refclk_stable_i_3_n_0),
        .I3(refclk_stable_i_4_n_0),
        .I4(refclk_stable_i_5_n_0),
        .I5(refclk_stable_i_6_n_0),
        .O(refclk_stable_i_1_n_0));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    refclk_stable_i_2
       (.I0(refclk_stable_count_reg[4]),
        .I1(refclk_stable_count_reg[5]),
        .I2(refclk_stable_count_reg[2]),
        .I3(refclk_stable_count_reg[3]),
        .I4(refclk_stable_count_reg[7]),
        .I5(refclk_stable_count_reg[6]),
        .O(refclk_stable_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000200000000000)) 
    refclk_stable_i_3
       (.I0(refclk_stable_count_reg[10]),
        .I1(refclk_stable_count_reg[11]),
        .I2(refclk_stable_count_reg[8]),
        .I3(refclk_stable_count_reg[9]),
        .I4(refclk_stable_count_reg[12]),
        .I5(refclk_stable_count_reg[13]),
        .O(refclk_stable_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    refclk_stable_i_4
       (.I0(refclk_stable_count_reg[16]),
        .I1(refclk_stable_count_reg[17]),
        .I2(refclk_stable_count_reg[14]),
        .I3(refclk_stable_count_reg[15]),
        .I4(refclk_stable_count_reg[18]),
        .I5(refclk_stable_count_reg[19]),
        .O(refclk_stable_i_4_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    refclk_stable_i_5
       (.I0(refclk_stable_count_reg[22]),
        .I1(refclk_stable_count_reg[23]),
        .I2(refclk_stable_count_reg[20]),
        .I3(refclk_stable_count_reg[21]),
        .I4(refclk_stable_count_reg[25]),
        .I5(refclk_stable_count_reg[24]),
        .O(refclk_stable_i_5_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    refclk_stable_i_6
       (.I0(refclk_stable_count_reg[28]),
        .I1(refclk_stable_count_reg[29]),
        .I2(refclk_stable_count_reg[26]),
        .I3(refclk_stable_count_reg[27]),
        .I4(refclk_stable_count_reg[31]),
        .I5(refclk_stable_count_reg[30]),
        .O(refclk_stable_i_6_n_0));
  FDRE #(
    .INIT(1'b0)) 
    refclk_stable_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(refclk_stable_i_1_n_0),
        .Q(refclk_stable_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h440000FF50505050)) 
    reset_time_out_i_2__0
       (.I0(tx_state[3]),
        .I1(txresetdone_s3),
        .I2(init_wait_done_reg_n_0),
        .I3(tx_state[1]),
        .I4(tx_state[2]),
        .I5(tx_state[0]),
        .O(reset_time_out_i_2__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    reset_time_out_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(sync_cplllock_n_0),
        .Q(reset_time_out),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT5 #(
    .INIT(32'hFFFB0002)) 
    run_phase_alignment_int_i_1
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .I4(run_phase_alignment_int_reg_n_0),
        .O(run_phase_alignment_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(run_phase_alignment_int_i_1_n_0),
        .Q(run_phase_alignment_int_reg_n_0),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_s3_reg
       (.C(data_sync_reg1),
        .CE(1'b1),
        .D(data_out),
        .Q(run_phase_alignment_int_s3),
        .R(1'b0));
  gig_ethernet_pcs_pma_0_sync_block_4 sync_TXRESETDONE
       (.data_out(txresetdone_s2),
        .data_sync_reg1_0(data_sync_reg1_0),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_5 sync_cplllock
       (.E(sync_cplllock_n_1),
        .\FSM_sequential_tx_state_reg[0] (\FSM_sequential_tx_state[3]_i_3_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_0 (\FSM_sequential_tx_state[3]_i_4_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_1 (\FSM_sequential_tx_state[3]_i_6_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_2 (time_out_2ms_reg_n_0),
        .\FSM_sequential_tx_state_reg[0]_3 (\FSM_sequential_tx_state[3]_i_7_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_4 (pll_reset_asserted_reg_n_0),
        .\FSM_sequential_tx_state_reg[0]_5 (refclk_stable_reg_n_0),
        .\FSM_sequential_tx_state_reg[0]_6 (\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .Q(tx_state),
        .data_sync_reg1_0(data_sync_reg1_2),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_lock_reclocked(mmcm_lock_reclocked),
        .reset_time_out(reset_time_out),
        .reset_time_out_reg(sync_cplllock_n_0),
        .reset_time_out_reg_0(reset_time_out_i_2__0_n_0),
        .reset_time_out_reg_1(init_wait_done_reg_n_0));
  gig_ethernet_pcs_pma_0_sync_block_6 sync_mmcm_lock_reclocked
       (.SR(sync_mmcm_lock_reclocked_n_0),
        .data_out(mmcm_lock_i),
        .data_sync_reg1_0(data_sync_reg1_1),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_7 sync_run_phase_alignment_int
       (.data_in(run_phase_alignment_int_reg_n_0),
        .data_out(data_out),
        .data_sync_reg6_0(data_sync_reg1));
  gig_ethernet_pcs_pma_0_sync_block_8 sync_time_out_wait_bypass
       (.data_in(time_out_wait_bypass_reg_n_0),
        .data_out(time_out_wait_bypass_s2),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_9 sync_tx_fsm_reset_done_int
       (.data_in(data_in),
        .data_out(tx_fsm_reset_done_int_s2),
        .data_sync_reg1_0(data_sync_reg1));
  LUT4 #(
    .INIT(16'h00AE)) 
    time_out_2ms_i_1
       (.I0(time_out_2ms_reg_n_0),
        .I1(time_out_2ms_i_2__0_n_0),
        .I2(time_out_2ms_i_3_n_0),
        .I3(reset_time_out),
        .O(time_out_2ms_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000800)) 
    time_out_2ms_i_2__0
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[18]),
        .I2(time_out_counter_reg[10]),
        .I3(time_out_counter_reg[12]),
        .I4(time_out_counter_reg[5]),
        .I5(time_tlock_max_i_3_n_0),
        .O(time_out_2ms_i_2__0_n_0));
  LUT4 #(
    .INIT(16'hFFFD)) 
    time_out_2ms_i_3
       (.I0(time_out_counter_reg[7]),
        .I1(time_out_counter_reg[14]),
        .I2(time_out_2ms_i_4__0_n_0),
        .I3(time_out_2ms_i_5_n_0),
        .O(time_out_2ms_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    time_out_2ms_i_4__0
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[3]),
        .I2(time_out_counter_reg[8]),
        .I3(time_out_counter_reg[6]),
        .O(time_out_2ms_i_4__0_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFEF)) 
    time_out_2ms_i_5
       (.I0(time_out_counter_reg[0]),
        .I1(time_out_counter_reg[13]),
        .I2(time_out_counter_reg[9]),
        .I3(time_out_counter_reg[2]),
        .I4(time_out_counter_reg[1]),
        .O(time_out_2ms_i_5_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_2ms_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_2ms_i_1_n_0),
        .Q(time_out_2ms_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000AAAAEAAA)) 
    time_out_500us_i_1
       (.I0(time_out_500us_reg_n_0),
        .I1(time_out_500us_i_2_n_0),
        .I2(time_out_counter_reg[5]),
        .I3(time_out_counter_reg[10]),
        .I4(time_out_2ms_i_3_n_0),
        .I5(reset_time_out),
        .O(time_out_500us_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    time_out_500us_i_2
       (.I0(time_out_counter_reg[15]),
        .I1(time_out_counter_reg[16]),
        .I2(time_out_counter_reg[11]),
        .I3(time_out_counter_reg[12]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[17]),
        .O(time_out_500us_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_500us_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_500us_i_1_n_0),
        .Q(time_out_500us_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFBFFFF)) 
    \time_out_counter[0]_i_1__0 
       (.I0(time_tlock_max_i_3_n_0),
        .I1(\time_out_counter[0]_i_3__0_n_0 ),
        .I2(time_out_2ms_i_3_n_0),
        .I3(time_out_counter_reg[10]),
        .I4(time_out_counter_reg[12]),
        .I5(time_out_counter_reg[5]),
        .O(time_out_counter));
  LUT2 #(
    .INIT(4'h8)) 
    \time_out_counter[0]_i_3__0 
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[18]),
        .O(\time_out_counter[0]_i_3__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \time_out_counter[0]_i_4 
       (.I0(time_out_counter_reg[0]),
        .O(\time_out_counter[0]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[0] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_7 ),
        .Q(time_out_counter_reg[0]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\time_out_counter_reg[0]_i_2_n_0 ,\time_out_counter_reg[0]_i_2_n_1 ,\time_out_counter_reg[0]_i_2_n_2 ,\time_out_counter_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\time_out_counter_reg[0]_i_2_n_4 ,\time_out_counter_reg[0]_i_2_n_5 ,\time_out_counter_reg[0]_i_2_n_6 ,\time_out_counter_reg[0]_i_2_n_7 }),
        .S({time_out_counter_reg[3:1],\time_out_counter[0]_i_4_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[10] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_5 ),
        .Q(time_out_counter_reg[10]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[11] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_4 ),
        .Q(time_out_counter_reg[11]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[12] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_7 ),
        .Q(time_out_counter_reg[12]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[12]_i_1 
       (.CI(\time_out_counter_reg[8]_i_1_n_0 ),
        .CO({\time_out_counter_reg[12]_i_1_n_0 ,\time_out_counter_reg[12]_i_1_n_1 ,\time_out_counter_reg[12]_i_1_n_2 ,\time_out_counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[12]_i_1_n_4 ,\time_out_counter_reg[12]_i_1_n_5 ,\time_out_counter_reg[12]_i_1_n_6 ,\time_out_counter_reg[12]_i_1_n_7 }),
        .S(time_out_counter_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[13] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_6 ),
        .Q(time_out_counter_reg[13]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[14] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_5 ),
        .Q(time_out_counter_reg[14]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[15] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_4 ),
        .Q(time_out_counter_reg[15]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[16] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1_n_7 ),
        .Q(time_out_counter_reg[16]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[16]_i_1 
       (.CI(\time_out_counter_reg[12]_i_1_n_0 ),
        .CO({\NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED [3:2],\time_out_counter_reg[16]_i_1_n_2 ,\time_out_counter_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED [3],\time_out_counter_reg[16]_i_1_n_5 ,\time_out_counter_reg[16]_i_1_n_6 ,\time_out_counter_reg[16]_i_1_n_7 }),
        .S({1'b0,time_out_counter_reg[18:16]}));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[17] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1_n_6 ),
        .Q(time_out_counter_reg[17]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[18] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1_n_5 ),
        .Q(time_out_counter_reg[18]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[1] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_6 ),
        .Q(time_out_counter_reg[1]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[2] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_5 ),
        .Q(time_out_counter_reg[2]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[3] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_4 ),
        .Q(time_out_counter_reg[3]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[4] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_7 ),
        .Q(time_out_counter_reg[4]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[4]_i_1 
       (.CI(\time_out_counter_reg[0]_i_2_n_0 ),
        .CO({\time_out_counter_reg[4]_i_1_n_0 ,\time_out_counter_reg[4]_i_1_n_1 ,\time_out_counter_reg[4]_i_1_n_2 ,\time_out_counter_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[4]_i_1_n_4 ,\time_out_counter_reg[4]_i_1_n_5 ,\time_out_counter_reg[4]_i_1_n_6 ,\time_out_counter_reg[4]_i_1_n_7 }),
        .S(time_out_counter_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[5] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_6 ),
        .Q(time_out_counter_reg[5]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[6] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_5 ),
        .Q(time_out_counter_reg[6]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[7] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_4 ),
        .Q(time_out_counter_reg[7]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[8] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_7 ),
        .Q(time_out_counter_reg[8]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[8]_i_1 
       (.CI(\time_out_counter_reg[4]_i_1_n_0 ),
        .CO({\time_out_counter_reg[8]_i_1_n_0 ,\time_out_counter_reg[8]_i_1_n_1 ,\time_out_counter_reg[8]_i_1_n_2 ,\time_out_counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[8]_i_1_n_4 ,\time_out_counter_reg[8]_i_1_n_5 ,\time_out_counter_reg[8]_i_1_n_6 ,\time_out_counter_reg[8]_i_1_n_7 }),
        .S(time_out_counter_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[9] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_6 ),
        .Q(time_out_counter_reg[9]),
        .R(reset_time_out));
  LUT4 #(
    .INIT(16'hAB00)) 
    time_out_wait_bypass_i_1
       (.I0(time_out_wait_bypass_reg_n_0),
        .I1(tx_fsm_reset_done_int_s3),
        .I2(time_out_wait_bypass_i_2_n_0),
        .I3(run_phase_alignment_int_s3),
        .O(time_out_wait_bypass_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFEFFFFFFFFF)) 
    time_out_wait_bypass_i_2
       (.I0(time_out_wait_bypass_i_3_n_0),
        .I1(time_out_wait_bypass_i_4_n_0),
        .I2(wait_bypass_count_reg[5]),
        .I3(wait_bypass_count_reg[13]),
        .I4(wait_bypass_count_reg[11]),
        .I5(time_out_wait_bypass_i_5_n_0),
        .O(time_out_wait_bypass_i_2_n_0));
  LUT4 #(
    .INIT(16'hFF7F)) 
    time_out_wait_bypass_i_3
       (.I0(wait_bypass_count_reg[16]),
        .I1(wait_bypass_count_reg[9]),
        .I2(wait_bypass_count_reg[12]),
        .I3(wait_bypass_count_reg[10]),
        .O(time_out_wait_bypass_i_3_n_0));
  LUT4 #(
    .INIT(16'hDFFF)) 
    time_out_wait_bypass_i_4
       (.I0(wait_bypass_count_reg[4]),
        .I1(wait_bypass_count_reg[15]),
        .I2(wait_bypass_count_reg[6]),
        .I3(wait_bypass_count_reg[0]),
        .O(time_out_wait_bypass_i_4_n_0));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    time_out_wait_bypass_i_5
       (.I0(wait_bypass_count_reg[8]),
        .I1(wait_bypass_count_reg[1]),
        .I2(wait_bypass_count_reg[7]),
        .I3(wait_bypass_count_reg[14]),
        .I4(wait_bypass_count_reg[2]),
        .I5(wait_bypass_count_reg[3]),
        .O(time_out_wait_bypass_i_5_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_reg
       (.C(data_sync_reg1),
        .CE(1'b1),
        .D(time_out_wait_bypass_i_1_n_0),
        .Q(time_out_wait_bypass_reg_n_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_wait_bypass_s2),
        .Q(time_out_wait_bypass_s3),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000AAAAAAEA)) 
    time_tlock_max_i_1
       (.I0(time_tlock_max_reg_n_0),
        .I1(time_tlock_max_i_2_n_0),
        .I2(time_out_counter_reg[5]),
        .I3(time_tlock_max_i_3_n_0),
        .I4(time_tlock_max_i_4_n_0),
        .I5(reset_time_out),
        .O(time_tlock_max_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    time_tlock_max_i_2
       (.I0(time_out_counter_reg[14]),
        .I1(time_out_counter_reg[12]),
        .I2(time_out_counter_reg[10]),
        .I3(time_out_counter_reg[7]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[17]),
        .O(time_tlock_max_i_2_n_0));
  LUT3 #(
    .INIT(8'hEF)) 
    time_tlock_max_i_3
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[15]),
        .I2(time_out_counter_reg[11]),
        .O(time_tlock_max_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    time_tlock_max_i_4
       (.I0(time_out_2ms_i_5_n_0),
        .I1(time_out_counter_reg[6]),
        .I2(time_out_counter_reg[8]),
        .I3(time_out_counter_reg[3]),
        .I4(time_out_counter_reg[4]),
        .O(time_tlock_max_i_4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_tlock_max_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_tlock_max_i_1_n_0),
        .Q(time_tlock_max_reg_n_0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFFF1000)) 
    tx_fsm_reset_done_int_i_1
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[0]),
        .I3(tx_state[3]),
        .I4(data_in),
        .O(tx_fsm_reset_done_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    tx_fsm_reset_done_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(tx_fsm_reset_done_int_i_1_n_0),
        .Q(data_in),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    tx_fsm_reset_done_int_s3_reg
       (.C(data_sync_reg1),
        .CE(1'b1),
        .D(tx_fsm_reset_done_int_s2),
        .Q(tx_fsm_reset_done_int_s3),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    txresetdone_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(txresetdone_s2),
        .Q(txresetdone_s3),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_1 
       (.I0(run_phase_alignment_int_s3),
        .O(clear));
  LUT2 #(
    .INIT(4'h2)) 
    \wait_bypass_count[0]_i_2 
       (.I0(time_out_wait_bypass_i_2_n_0),
        .I1(tx_fsm_reset_done_int_s3),
        .O(\wait_bypass_count[0]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_4__0 
       (.I0(wait_bypass_count_reg[0]),
        .O(\wait_bypass_count[0]_i_4__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[0] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_7 ),
        .Q(wait_bypass_count_reg[0]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\wait_bypass_count_reg[0]_i_3_n_0 ,\wait_bypass_count_reg[0]_i_3_n_1 ,\wait_bypass_count_reg[0]_i_3_n_2 ,\wait_bypass_count_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\wait_bypass_count_reg[0]_i_3_n_4 ,\wait_bypass_count_reg[0]_i_3_n_5 ,\wait_bypass_count_reg[0]_i_3_n_6 ,\wait_bypass_count_reg[0]_i_3_n_7 }),
        .S({wait_bypass_count_reg[3:1],\wait_bypass_count[0]_i_4__0_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[10] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[10]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[11] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[11]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[12] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[12]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[12]_i_1 
       (.CI(\wait_bypass_count_reg[8]_i_1_n_0 ),
        .CO({\wait_bypass_count_reg[12]_i_1_n_0 ,\wait_bypass_count_reg[12]_i_1_n_1 ,\wait_bypass_count_reg[12]_i_1_n_2 ,\wait_bypass_count_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[12]_i_1_n_4 ,\wait_bypass_count_reg[12]_i_1_n_5 ,\wait_bypass_count_reg[12]_i_1_n_6 ,\wait_bypass_count_reg[12]_i_1_n_7 }),
        .S(wait_bypass_count_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[13] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[13]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[14] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[14]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[15] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[15]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[16] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[16]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[16]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[16]_i_1 
       (.CI(\wait_bypass_count_reg[12]_i_1_n_0 ),
        .CO(\NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED [3:1],\wait_bypass_count_reg[16]_i_1_n_7 }),
        .S({1'b0,1'b0,1'b0,wait_bypass_count_reg[16]}));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[1] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_6 ),
        .Q(wait_bypass_count_reg[1]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[2] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_5 ),
        .Q(wait_bypass_count_reg[2]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[3] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_4 ),
        .Q(wait_bypass_count_reg[3]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[4] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[4]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[4]_i_1 
       (.CI(\wait_bypass_count_reg[0]_i_3_n_0 ),
        .CO({\wait_bypass_count_reg[4]_i_1_n_0 ,\wait_bypass_count_reg[4]_i_1_n_1 ,\wait_bypass_count_reg[4]_i_1_n_2 ,\wait_bypass_count_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[4]_i_1_n_4 ,\wait_bypass_count_reg[4]_i_1_n_5 ,\wait_bypass_count_reg[4]_i_1_n_6 ,\wait_bypass_count_reg[4]_i_1_n_7 }),
        .S(wait_bypass_count_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[5] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[5]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[6] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[6]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[7] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[7]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[8] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[8]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[8]_i_1 
       (.CI(\wait_bypass_count_reg[4]_i_1_n_0 ),
        .CO({\wait_bypass_count_reg[8]_i_1_n_0 ,\wait_bypass_count_reg[8]_i_1_n_1 ,\wait_bypass_count_reg[8]_i_1_n_2 ,\wait_bypass_count_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[8]_i_1_n_4 ,\wait_bypass_count_reg[8]_i_1_n_5 ,\wait_bypass_count_reg[8]_i_1_n_6 ,\wait_bypass_count_reg[8]_i_1_n_7 }),
        .S(wait_bypass_count_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[9] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[9]),
        .R(clear));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \wait_time_cnt[0]_i_1 
       (.I0(wait_time_cnt_reg[0]),
        .O(wait_time_cnt0));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[1]_i_1 
       (.I0(wait_time_cnt_reg[0]),
        .I1(wait_time_cnt_reg[1]),
        .O(\wait_time_cnt[1]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hE1)) 
    \wait_time_cnt[2]_i_1 
       (.I0(wait_time_cnt_reg[1]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[2]),
        .O(\wait_time_cnt[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT4 #(
    .INIT(16'hFE01)) 
    \wait_time_cnt[3]_i_1 
       (.I0(wait_time_cnt_reg[2]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[1]),
        .I3(wait_time_cnt_reg[3]),
        .O(\wait_time_cnt[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT5 #(
    .INIT(32'hFFFE0001)) 
    \wait_time_cnt[4]_i_1 
       (.I0(wait_time_cnt_reg[3]),
        .I1(wait_time_cnt_reg[1]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[2]),
        .I4(wait_time_cnt_reg[4]),
        .O(\wait_time_cnt[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000001)) 
    \wait_time_cnt[5]_i_1 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0700)) 
    \wait_time_cnt[6]_i_1__0 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[3]),
        .I3(tx_state[0]),
        .O(wait_time_cnt0_0));
  LUT2 #(
    .INIT(4'hE)) 
    \wait_time_cnt[6]_i_2 
       (.I0(\wait_time_cnt[6]_i_4_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(sel));
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[6]_i_3 
       (.I0(\wait_time_cnt[6]_i_4_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(\wait_time_cnt[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \wait_time_cnt[6]_i_4 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[6]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[0] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(wait_time_cnt0),
        .Q(wait_time_cnt_reg[0]),
        .R(wait_time_cnt0_0));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[1] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[1]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[1]),
        .R(wait_time_cnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[2] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[2]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[2]),
        .S(wait_time_cnt0_0));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[3] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[3]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[3]),
        .R(wait_time_cnt0_0));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[4] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[4]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[4]),
        .R(wait_time_cnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[5] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[5]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[5]),
        .S(wait_time_cnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[6] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[6]_i_3_n_0 ),
        .Q(wait_time_cnt_reg[6]),
        .S(wait_time_cnt0_0));
endmodule

module gig_ethernet_pcs_pma_0_block
   (gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    status_vector,
    resetdone,
    txn,
    txp,
    rxoutclk,
    txoutclk,
    mmcm_reset,
    out,
    signal_detect,
    CLK,
    data_in,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    configuration_vector,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    gtxe2_i);
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  output [6:0]status_vector;
  output resetdone;
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output mmcm_reset;
  input [0:0]out;
  input signal_detect;
  input CLK;
  input data_in;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  input [2:0]configuration_vector;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input gtxe2_i;

  wire CLK;
  wire [2:0]configuration_vector;
  wire data_in;
  wire enablealign;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire mgt_rx_reset;
  wire mgt_tx_reset;
  wire mmcm_reset;
  wire [0:0]out;
  wire powerdown;
  wire resetdone;
  wire rx_reset_done_i;
  wire rxbuferr;
  wire rxchariscomma;
  wire rxcharisk;
  wire [1:0]rxclkcorcnt;
  wire [7:0]rxdata;
  wire rxdisperr;
  wire rxn;
  wire rxnotintable;
  wire rxoutclk;
  wire rxp;
  wire signal_detect;
  wire [6:0]status_vector;
  wire transceiver_inst_n_11;
  wire transceiver_inst_n_12;
  wire txbuferr;
  wire txchardispmode;
  wire txchardispval;
  wire txcharisk;
  wire [7:0]txdata;
  wire txn;
  wire txoutclk;
  wire txp;
  wire NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED;
  wire [9:0]NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED;
  wire [15:0]NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED;
  wire [63:0]NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED;
  wire [31:0]NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED;
  wire [47:0]NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED;
  wire [1:0]NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED;
  wire [31:0]NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED;
  wire [1:0]NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED;
  wire [1:0]NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED;
  wire [15:7]NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED;
  wire [9:0]NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED;

  (* B_SHIFTER_ADDR = "10'b0101001110" *) 
  (* C_1588 = "0" *) 
  (* C_2_5G = "FALSE" *) 
  (* C_COMPONENT_NAME = "gig_ethernet_pcs_pma_0" *) 
  (* C_DYNAMIC_SWITCHING = "FALSE" *) 
  (* C_ELABORATION_TRANSIENT_DIR = "BlankString" *) 
  (* C_FAMILY = "kintex7" *) 
  (* C_HAS_AN = "FALSE" *) 
  (* C_HAS_AXIL = "FALSE" *) 
  (* C_HAS_MDIO = "FALSE" *) 
  (* C_HAS_TEMAC = "TRUE" *) 
  (* C_IS_SGMII = "FALSE" *) 
  (* C_RX_GMII_CLK = "TXOUTCLK" *) 
  (* C_SGMII_FABRIC_BUFFER = "TRUE" *) 
  (* C_SGMII_PHY_MODE = "FALSE" *) 
  (* C_USE_LVDS = "FALSE" *) 
  (* C_USE_TBI = "FALSE" *) 
  (* C_USE_TRANSCEIVER = "TRUE" *) 
  (* GT_RX_BYTE_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  gig_ethernet_pcs_pma_0_gig_ethernet_pcs_pma_v16_2_15 gig_ethernet_pcs_pma_0_core
       (.an_adv_config_val(1'b0),
        .an_adv_config_vector({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .an_enable(NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED),
        .an_interrupt(NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED),
        .an_restart_config(1'b0),
        .basex_or_sgmii(1'b0),
        .configuration_valid(1'b0),
        .configuration_vector({1'b0,configuration_vector,1'b0}),
        .correction_timer({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dcm_locked(data_in),
        .drp_daddr(NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED[9:0]),
        .drp_dclk(1'b0),
        .drp_den(NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED),
        .drp_di(NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED[15:0]),
        .drp_do({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drp_drdy(1'b0),
        .drp_dwe(NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED),
        .drp_gnt(1'b0),
        .drp_req(NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED),
        .en_cdet(NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED),
        .enablealign(enablealign),
        .ewrap(NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gtx_clk(1'b0),
        .link_timer_basex({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_sgmii({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_value({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .loc_ref(NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED),
        .mdc(1'b0),
        .mdio_in(1'b0),
        .mdio_out(NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED),
        .mdio_tri(NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED),
        .mgt_rx_reset(mgt_rx_reset),
        .mgt_tx_reset(mgt_tx_reset),
        .phyad({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pma_rx_clk0(1'b0),
        .pma_rx_clk1(1'b0),
        .powerdown(powerdown),
        .reset(out),
        .reset_done(resetdone),
        .rx_code_group0({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_code_group1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_gt_nominal_latency({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1,1'b1,1'b0,1'b0,1'b0}),
        .rxbufstatus({rxbuferr,1'b0}),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .rxclkcorcnt({1'b0,rxclkcorcnt}),
        .rxdata(rxdata),
        .rxdisperr(rxdisperr),
        .rxnotintable(rxnotintable),
        .rxphy_correction_timer(NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED[63:0]),
        .rxphy_ns_field(NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED[31:0]),
        .rxphy_s_field(NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED[47:0]),
        .rxrecclk(1'b0),
        .rxrundisp(1'b0),
        .s_axi_aclk(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED),
        .s_axi_awvalid(1'b0),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED),
        .s_axi_rdata(NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_resetn(1'b0),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wready(NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED),
        .s_axi_wvalid(1'b0),
        .signal_detect(signal_detect),
        .speed_is_100(1'b0),
        .speed_is_10_100(1'b0),
        .speed_selection(NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED[1:0]),
        .status_vector({NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED[15:7],status_vector}),
        .systemtimer_ns_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .systemtimer_s_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .tx_code_group(NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED[9:0]),
        .txbuferr(txbuferr),
        .txchardispmode(txchardispmode),
        .txchardispval(txchardispval),
        .txcharisk(txcharisk),
        .txdata(txdata),
        .userclk(1'b0),
        .userclk2(CLK));
  gig_ethernet_pcs_pma_0_sync_block sync_block_rx_reset_done
       (.CLK(CLK),
        .data_in(transceiver_inst_n_12),
        .data_out(rx_reset_done_i));
  gig_ethernet_pcs_pma_0_sync_block_0 sync_block_tx_reset_done
       (.CLK(CLK),
        .data_in(transceiver_inst_n_11),
        .resetdone(resetdone),
        .resetdone_0(rx_reset_done_i));
  gig_ethernet_pcs_pma_0_transceiver transceiver_inst
       (.CLK(CLK),
        .D(txchardispmode),
        .Q(rxclkcorcnt),
        .SR(mgt_rx_reset),
        .data_in(transceiver_inst_n_11),
        .data_sync_reg1(data_in),
        .enablealign(enablealign),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(gtxe2_i),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out),
        .powerdown(powerdown),
        .reset_sync5(mgt_tx_reset),
        .rx_fsm_reset_done_int_reg(transceiver_inst_n_12),
        .rxbufstatus(rxbuferr),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .\rxdata_reg[7]_0 (rxdata),
        .rxdisperr(rxdisperr),
        .rxn(rxn),
        .rxnotintable(rxnotintable),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .status_vector(status_vector[1]),
        .txbuferr(txbuferr),
        .txchardispval_reg_reg_0(txchardispval),
        .txcharisk_reg_reg_0(txcharisk),
        .\txdata_reg_reg[7]_0 (txdata),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module gig_ethernet_pcs_pma_0_clocking
   (gtrefclk_out,
    gtrefclk_bufg,
    mmcm_locked,
    userclk,
    userclk2,
    rxuserclk2_out,
    gtrefclk_p,
    gtrefclk_n,
    txoutclk,
    mmcm_reset,
    rxoutclk);
  output gtrefclk_out;
  output gtrefclk_bufg;
  output mmcm_locked;
  output userclk;
  output userclk2;
  output rxuserclk2_out;
  input gtrefclk_p;
  input gtrefclk_n;
  input txoutclk;
  input mmcm_reset;
  input rxoutclk;

  wire clkfbout;
  wire clkout0;
  wire clkout1;
  wire gtrefclk_bufg;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire mmcm_locked;
  wire mmcm_reset;
  wire rxoutclk;
  wire rxuserclk2_out;
  wire txoutclk;
  wire txoutclk_bufg;
  wire userclk;
  wire userclk2;
  wire NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED;
  wire NLW_mmcm_adv_inst_DRDY_UNCONNECTED;
  wire NLW_mmcm_adv_inst_PSDONE_UNCONNECTED;
  wire [15:0]NLW_mmcm_adv_inst_DO_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_gtrefclk
       (.I(gtrefclk_out),
        .O(gtrefclk_bufg));
  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_txoutclk
       (.I(txoutclk),
        .O(txoutclk_bufg));
  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_userclk
       (.I(clkout1),
        .O(userclk));
  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_userclk2
       (.I(clkout0),
        .O(userclk2));
  (* box_type = "PRIMITIVE" *) 
  IBUFDS_GTE2 #(
    .CLKCM_CFG("TRUE"),
    .CLKRCV_TRST("TRUE"),
    .CLKSWING_CFG(2'b11)) 
    ibufds_gtrefclk
       (.CEB(1'b0),
        .I(gtrefclk_p),
        .IB(gtrefclk_n),
        .O(gtrefclk_out),
        .ODIV2(NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED));
  (* box_type = "PRIMITIVE" *) 
  MMCME2_ADV #(
    .BANDWIDTH("OPTIMIZED"),
    .CLKFBOUT_MULT_F(16.000000),
    .CLKFBOUT_PHASE(0.000000),
    .CLKFBOUT_USE_FINE_PS("FALSE"),
    .CLKIN1_PERIOD(16.000000),
    .CLKIN2_PERIOD(0.000000),
    .CLKOUT0_DIVIDE_F(8.000000),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT0_USE_FINE_PS("FALSE"),
    .CLKOUT1_DIVIDE(16),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUT1_USE_FINE_PS("FALSE"),
    .CLKOUT2_DIVIDE(1),
    .CLKOUT2_DUTY_CYCLE(0.500000),
    .CLKOUT2_PHASE(0.000000),
    .CLKOUT2_USE_FINE_PS("FALSE"),
    .CLKOUT3_DIVIDE(1),
    .CLKOUT3_DUTY_CYCLE(0.500000),
    .CLKOUT3_PHASE(0.000000),
    .CLKOUT3_USE_FINE_PS("FALSE"),
    .CLKOUT4_CASCADE("FALSE"),
    .CLKOUT4_DIVIDE(1),
    .CLKOUT4_DUTY_CYCLE(0.500000),
    .CLKOUT4_PHASE(0.000000),
    .CLKOUT4_USE_FINE_PS("FALSE"),
    .CLKOUT5_DIVIDE(1),
    .CLKOUT5_DUTY_CYCLE(0.500000),
    .CLKOUT5_PHASE(0.000000),
    .CLKOUT5_USE_FINE_PS("FALSE"),
    .CLKOUT6_DIVIDE(1),
    .CLKOUT6_DUTY_CYCLE(0.500000),
    .CLKOUT6_PHASE(0.000000),
    .CLKOUT6_USE_FINE_PS("FALSE"),
    .COMPENSATION("INTERNAL"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKINSEL_INVERTED(1'b0),
    .IS_PSEN_INVERTED(1'b0),
    .IS_PSINCDEC_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER1(0.010000),
    .REF_JITTER2(0.000000),
    .SS_EN("FALSE"),
    .SS_MODE("CENTER_HIGH"),
    .SS_MOD_PERIOD(10000),
    .STARTUP_WAIT("FALSE")) 
    mmcm_adv_inst
       (.CLKFBIN(clkfbout),
        .CLKFBOUT(clkfbout),
        .CLKFBOUTB(NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED),
        .CLKFBSTOPPED(NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED),
        .CLKIN1(txoutclk_bufg),
        .CLKIN2(1'b0),
        .CLKINSEL(1'b1),
        .CLKINSTOPPED(NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED),
        .CLKOUT0(clkout0),
        .CLKOUT0B(NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(clkout1),
        .CLKOUT1B(NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED),
        .CLKOUT2(NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED),
        .CLKOUT2B(NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED),
        .CLKOUT3(NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED),
        .CLKOUT3B(NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED),
        .CLKOUT4(NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED),
        .CLKOUT5(NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED),
        .CLKOUT6(NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_mmcm_adv_inst_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_mmcm_adv_inst_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(mmcm_locked),
        .PSCLK(1'b0),
        .PSDONE(NLW_mmcm_adv_inst_PSDONE_UNCONNECTED),
        .PSEN(1'b0),
        .PSINCDEC(1'b0),
        .PWRDWN(1'b0),
        .RST(mmcm_reset));
  (* box_type = "PRIMITIVE" *) 
  BUFG rxrecclkbufg
       (.I(rxoutclk),
        .O(rxuserclk2_out));
endmodule

module gig_ethernet_pcs_pma_0_cpll_railing
   (cpll_pd0_i,
    cpllreset_in,
    gtrefclk_bufg,
    gt0_cpllreset_t);
  output cpll_pd0_i;
  output cpllreset_in;
  input gtrefclk_bufg;
  input gt0_cpllreset_t;

  wire cpll_pd0_i;
  wire cpll_reset_out;
  wire \cpllpd_wait_reg[31]_srl32_n_1 ;
  wire \cpllpd_wait_reg[63]_srl32_n_1 ;
  wire \cpllpd_wait_reg[94]_srl31_n_0 ;
  wire cpllreset_in;
  wire \cpllreset_wait_reg[126]_srl31_n_0 ;
  wire \cpllreset_wait_reg[31]_srl32_n_1 ;
  wire \cpllreset_wait_reg[63]_srl32_n_1 ;
  wire \cpllreset_wait_reg[95]_srl32_n_1 ;
  wire gt0_cpllreset_t;
  wire gtrefclk_bufg;
  wire \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED ;

  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[31]_srl32 " *) 
  SRLC32E #(
    .INIT(32'hFFFFFFFF)) 
    \cpllpd_wait_reg[31]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(1'b0),
        .Q(\NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllpd_wait_reg[31]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[63]_srl32 " *) 
  SRLC32E #(
    .INIT(32'hFFFFFFFF)) 
    \cpllpd_wait_reg[63]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllpd_wait_reg[31]_srl32_n_1 ),
        .Q(\NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllpd_wait_reg[63]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[94]_srl31 " *) 
  SRLC32E #(
    .INIT(32'h7FFFFFFF)) 
    \cpllpd_wait_reg[94]_srl31 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b0}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllpd_wait_reg[63]_srl32_n_1 ),
        .Q(\cpllpd_wait_reg[94]_srl31_n_0 ),
        .Q31(\NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED ));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b1)) 
    \cpllpd_wait_reg[95] 
       (.C(gtrefclk_bufg),
        .CE(1'b1),
        .D(\cpllpd_wait_reg[94]_srl31_n_0 ),
        .Q(cpll_pd0_i),
        .R(1'b0));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[126]_srl31 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[126]_srl31 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b0}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllreset_wait_reg[95]_srl32_n_1 ),
        .Q(\cpllreset_wait_reg[126]_srl31_n_0 ),
        .Q31(\NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED ));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \cpllreset_wait_reg[127] 
       (.C(gtrefclk_bufg),
        .CE(1'b1),
        .D(\cpllreset_wait_reg[126]_srl31_n_0 ),
        .Q(cpll_reset_out),
        .R(1'b0));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[31]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h000000FF)) 
    \cpllreset_wait_reg[31]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(1'b0),
        .Q(\NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[31]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[63]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[63]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllreset_wait_reg[31]_srl32_n_1 ),
        .Q(\NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[63]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[95]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[95]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllreset_wait_reg[63]_srl32_n_1 ),
        .Q(\NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[95]_srl32_n_1 ));
  LUT2 #(
    .INIT(4'hE)) 
    gtxe2_i_i_1
       (.I0(cpll_reset_out),
        .I1(gt0_cpllreset_t),
        .O(cpllreset_in));
endmodule

module gig_ethernet_pcs_pma_0_gt_common
   (gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    gtrefclk_out,
    independent_clock_bufg,
    out);
  output gt0_qplloutclk_out;
  output gt0_qplloutrefclk_out;
  input gtrefclk_out;
  input independent_clock_bufg;
  input [0:0]out;

  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_out;
  wire gtxe2_common_i_n_2;
  wire gtxe2_common_i_n_5;
  wire independent_clock_bufg;
  wire [0:0]out;
  wire NLW_gtxe2_common_i_DRPRDY_UNCONNECTED;
  wire NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED;
  wire NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED;
  wire [15:0]NLW_gtxe2_common_i_DRPDO_UNCONNECTED;
  wire [7:0]NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  GTXE2_COMMON #(
    .BIAS_CFG(64'h0000040000001000),
    .COMMON_CFG(32'h00000000),
    .IS_DRPCLK_INVERTED(1'b0),
    .IS_GTGREFCLK_INVERTED(1'b0),
    .IS_QPLLLOCKDETCLK_INVERTED(1'b0),
    .QPLL_CFG(27'h06801C1),
    .QPLL_CLKOUT_CFG(4'b0000),
    .QPLL_COARSE_FREQ_OVRD(6'b010000),
    .QPLL_COARSE_FREQ_OVRD_EN(1'b0),
    .QPLL_CP(10'b0000011111),
    .QPLL_CP_MONITOR_EN(1'b0),
    .QPLL_DMONITOR_SEL(1'b0),
    .QPLL_FBDIV(10'b0000100000),
    .QPLL_FBDIV_MONITOR_EN(1'b0),
    .QPLL_FBDIV_RATIO(1'b1),
    .QPLL_INIT_CFG(24'h000006),
    .QPLL_LOCK_CFG(16'h21E8),
    .QPLL_LPF(4'b1111),
    .QPLL_REFCLK_DIV(1),
    .SIM_QPLLREFCLK_SEL(3'b001),
    .SIM_RESET_SPEEDUP("FALSE"),
    .SIM_VERSION("4.0")) 
    gtxe2_common_i
       (.BGBYPASSB(1'b1),
        .BGMONITORENB(1'b1),
        .BGPDB(1'b1),
        .BGRCALOVRD({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DRPADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPCLK(1'b0),
        .DRPDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPDO(NLW_gtxe2_common_i_DRPDO_UNCONNECTED[15:0]),
        .DRPEN(1'b0),
        .DRPRDY(NLW_gtxe2_common_i_DRPRDY_UNCONNECTED),
        .DRPWE(1'b0),
        .GTGREFCLK(1'b0),
        .GTNORTHREFCLK0(1'b0),
        .GTNORTHREFCLK1(1'b0),
        .GTREFCLK0(gtrefclk_out),
        .GTREFCLK1(1'b0),
        .GTSOUTHREFCLK0(1'b0),
        .GTSOUTHREFCLK1(1'b0),
        .PMARSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLDMONITOR(NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED[7:0]),
        .QPLLFBCLKLOST(NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED),
        .QPLLLOCK(gtxe2_common_i_n_2),
        .QPLLLOCKDETCLK(independent_clock_bufg),
        .QPLLLOCKEN(1'b1),
        .QPLLOUTCLK(gt0_qplloutclk_out),
        .QPLLOUTREFCLK(gt0_qplloutrefclk_out),
        .QPLLOUTRESET(1'b0),
        .QPLLPD(1'b1),
        .QPLLREFCLKLOST(gtxe2_common_i_n_5),
        .QPLLREFCLKSEL({1'b0,1'b0,1'b1}),
        .QPLLRESET(out),
        .QPLLRSVD1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLRSVD2({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .RCALENB(1'b1),
        .REFCLKOUTMONITOR(NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED));
endmodule

module gig_ethernet_pcs_pma_0_reset_sync
   (reset_out,
    CLK,
    enablealign);
  output reset_out;
  input CLK;
  input enablealign;

  wire CLK;
  wire enablealign;
  wire reset_out;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(CLK),
        .CE(1'b1),
        .D(1'b0),
        .PRE(enablealign),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(enablealign),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(enablealign),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(enablealign),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(enablealign),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_reset_sync" *) 
module gig_ethernet_pcs_pma_0_reset_sync_1
   (reset_out,
    independent_clock_bufg,
    SR);
  output reset_out;
  input independent_clock_bufg;
  input [0:0]SR;

  wire [0:0]SR;
  wire independent_clock_bufg;
  wire reset_out;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(SR),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(SR),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(SR),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(SR),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(SR),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_reset_sync" *) 
module gig_ethernet_pcs_pma_0_reset_sync_2
   (reset_out,
    independent_clock_bufg,
    reset_sync5_0);
  output reset_out;
  input independent_clock_bufg;
  input [0:0]reset_sync5_0;

  wire independent_clock_bufg;
  wire reset_out;
  wire [0:0]reset_sync5_0;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

module gig_ethernet_pcs_pma_0_reset_wtd_timer
   (reset,
    independent_clock_bufg,
    data_out);
  output reset;
  input independent_clock_bufg;
  input data_out;

  wire \counter_stg1[5]_i_1_n_0 ;
  wire \counter_stg1[5]_i_3_n_0 ;
  wire [5:5]counter_stg1_reg;
  wire [4:0]counter_stg1_reg__0;
  wire \counter_stg2[0]_i_3_n_0 ;
  wire [11:0]counter_stg2_reg;
  wire \counter_stg2_reg[0]_i_2_n_0 ;
  wire \counter_stg2_reg[0]_i_2_n_1 ;
  wire \counter_stg2_reg[0]_i_2_n_2 ;
  wire \counter_stg2_reg[0]_i_2_n_3 ;
  wire \counter_stg2_reg[0]_i_2_n_4 ;
  wire \counter_stg2_reg[0]_i_2_n_5 ;
  wire \counter_stg2_reg[0]_i_2_n_6 ;
  wire \counter_stg2_reg[0]_i_2_n_7 ;
  wire \counter_stg2_reg[4]_i_1_n_0 ;
  wire \counter_stg2_reg[4]_i_1_n_1 ;
  wire \counter_stg2_reg[4]_i_1_n_2 ;
  wire \counter_stg2_reg[4]_i_1_n_3 ;
  wire \counter_stg2_reg[4]_i_1_n_4 ;
  wire \counter_stg2_reg[4]_i_1_n_5 ;
  wire \counter_stg2_reg[4]_i_1_n_6 ;
  wire \counter_stg2_reg[4]_i_1_n_7 ;
  wire \counter_stg2_reg[8]_i_1_n_1 ;
  wire \counter_stg2_reg[8]_i_1_n_2 ;
  wire \counter_stg2_reg[8]_i_1_n_3 ;
  wire \counter_stg2_reg[8]_i_1_n_4 ;
  wire \counter_stg2_reg[8]_i_1_n_5 ;
  wire \counter_stg2_reg[8]_i_1_n_6 ;
  wire \counter_stg2_reg[8]_i_1_n_7 ;
  wire counter_stg30;
  wire \counter_stg3[0]_i_3_n_0 ;
  wire \counter_stg3[0]_i_4_n_0 ;
  wire \counter_stg3[0]_i_5_n_0 ;
  wire [11:0]counter_stg3_reg;
  wire \counter_stg3_reg[0]_i_2_n_0 ;
  wire \counter_stg3_reg[0]_i_2_n_1 ;
  wire \counter_stg3_reg[0]_i_2_n_2 ;
  wire \counter_stg3_reg[0]_i_2_n_3 ;
  wire \counter_stg3_reg[0]_i_2_n_4 ;
  wire \counter_stg3_reg[0]_i_2_n_5 ;
  wire \counter_stg3_reg[0]_i_2_n_6 ;
  wire \counter_stg3_reg[0]_i_2_n_7 ;
  wire \counter_stg3_reg[4]_i_1_n_0 ;
  wire \counter_stg3_reg[4]_i_1_n_1 ;
  wire \counter_stg3_reg[4]_i_1_n_2 ;
  wire \counter_stg3_reg[4]_i_1_n_3 ;
  wire \counter_stg3_reg[4]_i_1_n_4 ;
  wire \counter_stg3_reg[4]_i_1_n_5 ;
  wire \counter_stg3_reg[4]_i_1_n_6 ;
  wire \counter_stg3_reg[4]_i_1_n_7 ;
  wire \counter_stg3_reg[8]_i_1_n_1 ;
  wire \counter_stg3_reg[8]_i_1_n_2 ;
  wire \counter_stg3_reg[8]_i_1_n_3 ;
  wire \counter_stg3_reg[8]_i_1_n_4 ;
  wire \counter_stg3_reg[8]_i_1_n_5 ;
  wire \counter_stg3_reg[8]_i_1_n_6 ;
  wire \counter_stg3_reg[8]_i_1_n_7 ;
  wire data_out;
  wire eqOp;
  wire independent_clock_bufg;
  wire [5:0]plusOp;
  wire reset;
  wire reset0;
  wire reset_i_2_n_0;
  wire reset_i_3_n_0;
  wire reset_i_4_n_0;
  wire reset_i_5_n_0;
  wire reset_i_6_n_0;
  wire [3:3]\NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \counter_stg1[0]_i_1 
       (.I0(counter_stg1_reg__0[0]),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \counter_stg1[1]_i_1 
       (.I0(counter_stg1_reg__0[0]),
        .I1(counter_stg1_reg__0[1]),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \counter_stg1[2]_i_1 
       (.I0(counter_stg1_reg__0[1]),
        .I1(counter_stg1_reg__0[0]),
        .I2(counter_stg1_reg__0[2]),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \counter_stg1[3]_i_1 
       (.I0(counter_stg1_reg__0[2]),
        .I1(counter_stg1_reg__0[0]),
        .I2(counter_stg1_reg__0[1]),
        .I3(counter_stg1_reg__0[3]),
        .O(plusOp[3]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \counter_stg1[4]_i_1 
       (.I0(counter_stg1_reg__0[3]),
        .I1(counter_stg1_reg__0[1]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[2]),
        .I4(counter_stg1_reg__0[4]),
        .O(plusOp[4]));
  LUT5 #(
    .INIT(32'hFFFF2000)) 
    \counter_stg1[5]_i_1 
       (.I0(reset_i_2_n_0),
        .I1(counter_stg3_reg[0]),
        .I2(reset_i_3_n_0),
        .I3(\counter_stg1[5]_i_3_n_0 ),
        .I4(data_out),
        .O(\counter_stg1[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \counter_stg1[5]_i_2 
       (.I0(counter_stg1_reg__0[4]),
        .I1(counter_stg1_reg__0[2]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[1]),
        .I4(counter_stg1_reg__0[3]),
        .I5(counter_stg1_reg),
        .O(plusOp[5]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \counter_stg1[5]_i_3 
       (.I0(counter_stg1_reg__0[3]),
        .I1(counter_stg1_reg__0[1]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[2]),
        .I4(counter_stg1_reg__0[4]),
        .O(\counter_stg1[5]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[0]),
        .Q(counter_stg1_reg__0[0]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[1]),
        .Q(counter_stg1_reg__0[1]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[2]),
        .Q(counter_stg1_reg__0[2]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[3]),
        .Q(counter_stg1_reg__0[3]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[4] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[4]),
        .Q(counter_stg1_reg__0[4]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[5] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[5]),
        .Q(counter_stg1_reg),
        .R(\counter_stg1[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter_stg2[0]_i_1 
       (.I0(counter_stg1_reg__0[4]),
        .I1(counter_stg1_reg__0[2]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[1]),
        .I4(counter_stg1_reg__0[3]),
        .I5(counter_stg1_reg),
        .O(eqOp));
  LUT1 #(
    .INIT(2'h1)) 
    \counter_stg2[0]_i_3 
       (.I0(counter_stg2_reg[0]),
        .O(\counter_stg2[0]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[0] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_7 ),
        .Q(counter_stg2_reg[0]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg2_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\counter_stg2_reg[0]_i_2_n_0 ,\counter_stg2_reg[0]_i_2_n_1 ,\counter_stg2_reg[0]_i_2_n_2 ,\counter_stg2_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\counter_stg2_reg[0]_i_2_n_4 ,\counter_stg2_reg[0]_i_2_n_5 ,\counter_stg2_reg[0]_i_2_n_6 ,\counter_stg2_reg[0]_i_2_n_7 }),
        .S({counter_stg2_reg[3:1],\counter_stg2[0]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[10] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_5 ),
        .Q(counter_stg2_reg[10]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[11] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_4 ),
        .Q(counter_stg2_reg[11]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[1] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_6 ),
        .Q(counter_stg2_reg[1]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[2] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_5 ),
        .Q(counter_stg2_reg[2]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[3] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_4 ),
        .Q(counter_stg2_reg[3]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[4] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_7 ),
        .Q(counter_stg2_reg[4]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg2_reg[4]_i_1 
       (.CI(\counter_stg2_reg[0]_i_2_n_0 ),
        .CO({\counter_stg2_reg[4]_i_1_n_0 ,\counter_stg2_reg[4]_i_1_n_1 ,\counter_stg2_reg[4]_i_1_n_2 ,\counter_stg2_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg2_reg[4]_i_1_n_4 ,\counter_stg2_reg[4]_i_1_n_5 ,\counter_stg2_reg[4]_i_1_n_6 ,\counter_stg2_reg[4]_i_1_n_7 }),
        .S(counter_stg2_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[5] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_6 ),
        .Q(counter_stg2_reg[5]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[6] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_5 ),
        .Q(counter_stg2_reg[6]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[7] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_4 ),
        .Q(counter_stg2_reg[7]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[8] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_7 ),
        .Q(counter_stg2_reg[8]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg2_reg[8]_i_1 
       (.CI(\counter_stg2_reg[4]_i_1_n_0 ),
        .CO({\NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED [3],\counter_stg2_reg[8]_i_1_n_1 ,\counter_stg2_reg[8]_i_1_n_2 ,\counter_stg2_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg2_reg[8]_i_1_n_4 ,\counter_stg2_reg[8]_i_1_n_5 ,\counter_stg2_reg[8]_i_1_n_6 ,\counter_stg2_reg[8]_i_1_n_7 }),
        .S(counter_stg2_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[9] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_6 ),
        .Q(counter_stg2_reg[9]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \counter_stg3[0]_i_1 
       (.I0(\counter_stg3[0]_i_3_n_0 ),
        .I1(\counter_stg3[0]_i_4_n_0 ),
        .I2(counter_stg2_reg[0]),
        .I3(\counter_stg1[5]_i_3_n_0 ),
        .O(counter_stg30));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter_stg3[0]_i_3 
       (.I0(counter_stg2_reg[3]),
        .I1(counter_stg2_reg[4]),
        .I2(counter_stg2_reg[1]),
        .I3(counter_stg2_reg[2]),
        .I4(counter_stg2_reg[6]),
        .I5(counter_stg2_reg[5]),
        .O(\counter_stg3[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter_stg3[0]_i_4 
       (.I0(counter_stg2_reg[9]),
        .I1(counter_stg2_reg[10]),
        .I2(counter_stg2_reg[7]),
        .I3(counter_stg2_reg[8]),
        .I4(counter_stg1_reg),
        .I5(counter_stg2_reg[11]),
        .O(\counter_stg3[0]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \counter_stg3[0]_i_5 
       (.I0(counter_stg3_reg[0]),
        .O(\counter_stg3[0]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[0] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_7 ),
        .Q(counter_stg3_reg[0]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg3_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\counter_stg3_reg[0]_i_2_n_0 ,\counter_stg3_reg[0]_i_2_n_1 ,\counter_stg3_reg[0]_i_2_n_2 ,\counter_stg3_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\counter_stg3_reg[0]_i_2_n_4 ,\counter_stg3_reg[0]_i_2_n_5 ,\counter_stg3_reg[0]_i_2_n_6 ,\counter_stg3_reg[0]_i_2_n_7 }),
        .S({counter_stg3_reg[3:1],\counter_stg3[0]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[10] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_5 ),
        .Q(counter_stg3_reg[10]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[11] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_4 ),
        .Q(counter_stg3_reg[11]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[1] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_6 ),
        .Q(counter_stg3_reg[1]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[2] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_5 ),
        .Q(counter_stg3_reg[2]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[3] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_4 ),
        .Q(counter_stg3_reg[3]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[4] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_7 ),
        .Q(counter_stg3_reg[4]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg3_reg[4]_i_1 
       (.CI(\counter_stg3_reg[0]_i_2_n_0 ),
        .CO({\counter_stg3_reg[4]_i_1_n_0 ,\counter_stg3_reg[4]_i_1_n_1 ,\counter_stg3_reg[4]_i_1_n_2 ,\counter_stg3_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg3_reg[4]_i_1_n_4 ,\counter_stg3_reg[4]_i_1_n_5 ,\counter_stg3_reg[4]_i_1_n_6 ,\counter_stg3_reg[4]_i_1_n_7 }),
        .S(counter_stg3_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[5] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_6 ),
        .Q(counter_stg3_reg[5]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[6] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_5 ),
        .Q(counter_stg3_reg[6]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[7] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_4 ),
        .Q(counter_stg3_reg[7]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[8] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_7 ),
        .Q(counter_stg3_reg[8]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg3_reg[8]_i_1 
       (.CI(\counter_stg3_reg[4]_i_1_n_0 ),
        .CO({\NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED [3],\counter_stg3_reg[8]_i_1_n_1 ,\counter_stg3_reg[8]_i_1_n_2 ,\counter_stg3_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg3_reg[8]_i_1_n_4 ,\counter_stg3_reg[8]_i_1_n_5 ,\counter_stg3_reg[8]_i_1_n_6 ,\counter_stg3_reg[8]_i_1_n_7 }),
        .S(counter_stg3_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[9] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_6 ),
        .Q(counter_stg3_reg[9]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h20)) 
    reset_i_1
       (.I0(reset_i_2_n_0),
        .I1(counter_stg3_reg[0]),
        .I2(reset_i_3_n_0),
        .O(reset0));
  LUT6 #(
    .INIT(64'h0000001000000000)) 
    reset_i_2
       (.I0(counter_stg3_reg[9]),
        .I1(counter_stg3_reg[10]),
        .I2(counter_stg3_reg[7]),
        .I3(counter_stg3_reg[8]),
        .I4(counter_stg2_reg[0]),
        .I5(counter_stg3_reg[11]),
        .O(reset_i_2_n_0));
  LUT3 #(
    .INIT(8'h80)) 
    reset_i_3
       (.I0(reset_i_4_n_0),
        .I1(reset_i_5_n_0),
        .I2(reset_i_6_n_0),
        .O(reset_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    reset_i_4
       (.I0(counter_stg2_reg[3]),
        .I1(counter_stg2_reg[4]),
        .I2(counter_stg2_reg[1]),
        .I3(counter_stg2_reg[2]),
        .I4(counter_stg2_reg[6]),
        .I5(counter_stg2_reg[5]),
        .O(reset_i_4_n_0));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    reset_i_5
       (.I0(counter_stg2_reg[10]),
        .I1(counter_stg2_reg[9]),
        .I2(counter_stg2_reg[8]),
        .I3(counter_stg2_reg[7]),
        .I4(counter_stg1_reg),
        .I5(counter_stg2_reg[11]),
        .O(reset_i_5_n_0));
  LUT6 #(
    .INIT(64'h0002000000000000)) 
    reset_i_6
       (.I0(counter_stg3_reg[4]),
        .I1(counter_stg3_reg[3]),
        .I2(counter_stg3_reg[1]),
        .I3(counter_stg3_reg[2]),
        .I4(counter_stg3_reg[6]),
        .I5(counter_stg3_reg[5]),
        .O(reset_i_6_n_0));
  FDRE reset_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset0),
        .Q(reset),
        .R(1'b0));
endmodule

module gig_ethernet_pcs_pma_0_resets
   (out,
    independent_clock_bufg,
    reset);
  output [0:0]out;
  input independent_clock_bufg;
  input reset;

  wire independent_clock_bufg;
  (* async_reg = "true" *) wire [3:0]pma_reset_pipe;
  wire reset;

  assign out[0] = pma_reset_pipe[3];
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset),
        .Q(pma_reset_pipe[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[0]),
        .PRE(reset),
        .Q(pma_reset_pipe[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[1]),
        .PRE(reset),
        .Q(pma_reset_pipe[2]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[2]),
        .PRE(reset),
        .Q(pma_reset_pipe[3]));
endmodule

(* EXAMPLE_SIMULATION = "0" *) (* downgradeipidentifiedwarnings = "yes" *) 
module gig_ethernet_pcs_pma_0_support
   (gtrefclk_p,
    gtrefclk_n,
    gtrefclk_out,
    gtrefclk_bufg_out,
    txp,
    txn,
    rxp,
    rxn,
    userclk_out,
    userclk2_out,
    rxuserclk_out,
    rxuserclk2_out,
    pma_reset_out,
    mmcm_locked_out,
    resetdone,
    independent_clock_bufg,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    configuration_vector,
    status_vector,
    reset,
    signal_detect,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out);
  input gtrefclk_p;
  input gtrefclk_n;
  output gtrefclk_out;
  output gtrefclk_bufg_out;
  output txp;
  output txn;
  input rxp;
  input rxn;
  output userclk_out;
  output userclk2_out;
  output rxuserclk_out;
  output rxuserclk2_out;
  output pma_reset_out;
  output mmcm_locked_out;
  output resetdone;
  input independent_clock_bufg;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  input [4:0]configuration_vector;
  output [15:0]status_vector;
  input reset;
  input signal_detect;
  output gt0_qplloutclk_out;
  output gt0_qplloutrefclk_out;

  wire \<const0> ;
  wire [4:0]configuration_vector;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg_out;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire independent_clock_bufg;
  wire mmcm_locked_out;
  wire mmcm_reset;
  wire pma_reset_out;
  wire reset;
  wire resetdone;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire rxuserclk2_out;
  wire signal_detect;
  wire [6:0]\^status_vector ;
  wire txn;
  wire txoutclk;
  wire txp;
  wire userclk2_out;
  wire userclk_out;

  assign rxuserclk_out = rxuserclk2_out;
  assign status_vector[15] = \<const0> ;
  assign status_vector[14] = \<const0> ;
  assign status_vector[13] = \<const0> ;
  assign status_vector[12] = \<const0> ;
  assign status_vector[11] = \<const0> ;
  assign status_vector[10] = \<const0> ;
  assign status_vector[9] = \<const0> ;
  assign status_vector[8] = \<const0> ;
  assign status_vector[7] = \<const0> ;
  assign status_vector[6:0] = \^status_vector [6:0];
  GND GND
       (.G(\<const0> ));
  gig_ethernet_pcs_pma_0_clocking core_clocking_i
       (.gtrefclk_bufg(gtrefclk_bufg_out),
        .gtrefclk_n(gtrefclk_n),
        .gtrefclk_out(gtrefclk_out),
        .gtrefclk_p(gtrefclk_p),
        .mmcm_locked(mmcm_locked_out),
        .mmcm_reset(mmcm_reset),
        .rxoutclk(rxoutclk),
        .rxuserclk2_out(rxuserclk2_out),
        .txoutclk(txoutclk),
        .userclk(userclk_out),
        .userclk2(userclk2_out));
  gig_ethernet_pcs_pma_0_gt_common core_gt_common_i
       (.gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_out(gtrefclk_out),
        .independent_clock_bufg(independent_clock_bufg),
        .out(pma_reset_out));
  gig_ethernet_pcs_pma_0_resets core_resets_i
       (.independent_clock_bufg(independent_clock_bufg),
        .out(pma_reset_out),
        .reset(reset));
  gig_ethernet_pcs_pma_0_block pcs_pma_block_i
       (.CLK(userclk2_out),
        .configuration_vector(configuration_vector[3:1]),
        .data_in(mmcm_locked_out),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg_out),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(userclk_out),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(pma_reset_out),
        .resetdone(resetdone),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .signal_detect(signal_detect),
        .status_vector(\^status_vector ),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module gig_ethernet_pcs_pma_0_sync_block
   (data_out,
    data_in,
    CLK);
  output data_out;
  input data_in;
  input CLK;

  wire CLK;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_0
   (resetdone,
    resetdone_0,
    data_in,
    CLK);
  output resetdone;
  input resetdone_0;
  input data_in;
  input CLK;

  wire CLK;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire resetdone;
  wire resetdone_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    resetdone_INST_0
       (.I0(data_out),
        .I1(resetdone_0),
        .O(resetdone));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_10
   (data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_11
   (\FSM_sequential_rx_state_reg[1] ,
    Q,
    rxresetdone_s3,
    data_sync_reg1_0,
    independent_clock_bufg);
  output \FSM_sequential_rx_state_reg[1] ;
  input [2:0]Q;
  input rxresetdone_s3;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire \FSM_sequential_rx_state_reg[1] ;
  wire [2:0]Q;
  wire cplllock_sync;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;
  wire rxresetdone_s3;

  LUT5 #(
    .INIT(32'h008F0080)) 
    \FSM_sequential_rx_state[3]_i_5 
       (.I0(Q[0]),
        .I1(rxresetdone_s3),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(cplllock_sync),
        .O(\FSM_sequential_rx_state_reg[1] ));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(cplllock_sync),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_12
   (\FSM_sequential_rx_state_reg[1] ,
    rx_fsm_reset_done_int_reg,
    D,
    E,
    reset_time_out_reg,
    reset_time_out_reg_0,
    Q,
    reset_time_out_reg_1,
    reset_time_out_reg_2,
    data_in,
    \FSM_sequential_rx_state_reg[1]_0 ,
    rx_fsm_reset_done_int_reg_0,
    rx_fsm_reset_done_int_reg_1,
    \FSM_sequential_rx_state_reg[0] ,
    \FSM_sequential_rx_state_reg[0]_0 ,
    \FSM_sequential_rx_state_reg[0]_1 ,
    mmcm_lock_reclocked,
    \FSM_sequential_rx_state_reg[0]_2 ,
    time_out_wait_bypass_s3,
    \FSM_sequential_rx_state_reg[3] ,
    \FSM_sequential_rx_state_reg[0]_3 ,
    rx_fsm_reset_done_int_reg_2,
    rx_fsm_reset_done_int_reg_3,
    data_out,
    independent_clock_bufg);
  output \FSM_sequential_rx_state_reg[1] ;
  output rx_fsm_reset_done_int_reg;
  output [2:0]D;
  output [0:0]E;
  input reset_time_out_reg;
  input reset_time_out_reg_0;
  input [3:0]Q;
  input reset_time_out_reg_1;
  input reset_time_out_reg_2;
  input data_in;
  input \FSM_sequential_rx_state_reg[1]_0 ;
  input rx_fsm_reset_done_int_reg_0;
  input rx_fsm_reset_done_int_reg_1;
  input \FSM_sequential_rx_state_reg[0] ;
  input \FSM_sequential_rx_state_reg[0]_0 ;
  input \FSM_sequential_rx_state_reg[0]_1 ;
  input mmcm_lock_reclocked;
  input \FSM_sequential_rx_state_reg[0]_2 ;
  input time_out_wait_bypass_s3;
  input \FSM_sequential_rx_state_reg[3] ;
  input \FSM_sequential_rx_state_reg[0]_3 ;
  input rx_fsm_reset_done_int_reg_2;
  input rx_fsm_reset_done_int_reg_3;
  input data_out;
  input independent_clock_bufg;

  wire [2:0]D;
  wire [0:0]E;
  wire \FSM_sequential_rx_state[0]_i_3_n_0 ;
  wire \FSM_sequential_rx_state[1]_i_2_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_4_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_6_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_8_n_0 ;
  wire \FSM_sequential_rx_state_reg[0] ;
  wire \FSM_sequential_rx_state_reg[0]_0 ;
  wire \FSM_sequential_rx_state_reg[0]_1 ;
  wire \FSM_sequential_rx_state_reg[0]_2 ;
  wire \FSM_sequential_rx_state_reg[0]_3 ;
  wire \FSM_sequential_rx_state_reg[1] ;
  wire \FSM_sequential_rx_state_reg[1]_0 ;
  wire \FSM_sequential_rx_state_reg[3] ;
  wire [3:0]Q;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_valid_sync;
  wire independent_clock_bufg;
  wire mmcm_lock_reclocked;
  wire reset_time_out_i_2_n_0;
  wire reset_time_out_reg;
  wire reset_time_out_reg_0;
  wire reset_time_out_reg_1;
  wire reset_time_out_reg_2;
  wire rx_fsm_reset_done_int;
  wire rx_fsm_reset_done_int_i_3_n_0;
  wire rx_fsm_reset_done_int_i_4_n_0;
  wire rx_fsm_reset_done_int_reg;
  wire rx_fsm_reset_done_int_reg_0;
  wire rx_fsm_reset_done_int_reg_1;
  wire rx_fsm_reset_done_int_reg_2;
  wire rx_fsm_reset_done_int_reg_3;
  wire time_out_wait_bypass_s3;

  LUT5 #(
    .INIT(32'hFFEFEFEF)) 
    \FSM_sequential_rx_state[0]_i_1 
       (.I0(\FSM_sequential_rx_state_reg[0]_2 ),
        .I1(\FSM_sequential_rx_state[0]_i_3_n_0 ),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[3]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT4 #(
    .INIT(16'h0200)) 
    \FSM_sequential_rx_state[0]_i_3 
       (.I0(Q[3]),
        .I1(reset_time_out_reg_2),
        .I2(data_valid_sync),
        .I3(rx_fsm_reset_done_int_reg_1),
        .O(\FSM_sequential_rx_state[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF24200400)) 
    \FSM_sequential_rx_state[1]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(\FSM_sequential_rx_state[1]_i_2_n_0 ),
        .I5(\FSM_sequential_rx_state_reg[1]_0 ),
        .O(D[1]));
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_rx_state[1]_i_2 
       (.I0(data_valid_sync),
        .I1(rx_fsm_reset_done_int_reg_1),
        .O(\FSM_sequential_rx_state[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEEE)) 
    \FSM_sequential_rx_state[3]_i_1 
       (.I0(\FSM_sequential_rx_state_reg[0] ),
        .I1(\FSM_sequential_rx_state[3]_i_4_n_0 ),
        .I2(Q[0]),
        .I3(reset_time_out_reg),
        .I4(\FSM_sequential_rx_state[3]_i_6_n_0 ),
        .I5(\FSM_sequential_rx_state_reg[0]_0 ),
        .O(E));
  LUT6 #(
    .INIT(64'hFFFFFFFFCCC0C4C4)) 
    \FSM_sequential_rx_state[3]_i_2 
       (.I0(time_out_wait_bypass_s3),
        .I1(Q[3]),
        .I2(Q[1]),
        .I3(\FSM_sequential_rx_state[3]_i_8_n_0 ),
        .I4(Q[0]),
        .I5(\FSM_sequential_rx_state_reg[3] ),
        .O(D[2]));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAEFEA)) 
    \FSM_sequential_rx_state[3]_i_4 
       (.I0(\FSM_sequential_rx_state[0]_i_3_n_0 ),
        .I1(\FSM_sequential_rx_state_reg[0]_1 ),
        .I2(Q[2]),
        .I3(\FSM_sequential_rx_state_reg[0]_3 ),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\FSM_sequential_rx_state[3]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h0CE20CCC)) 
    \FSM_sequential_rx_state[3]_i_6 
       (.I0(mmcm_lock_reclocked),
        .I1(Q[3]),
        .I2(data_valid_sync),
        .I3(Q[1]),
        .I4(Q[0]),
        .O(\FSM_sequential_rx_state[3]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hFD)) 
    \FSM_sequential_rx_state[3]_i_8 
       (.I0(rx_fsm_reset_done_int_reg_1),
        .I1(data_valid_sync),
        .I2(reset_time_out_reg_2),
        .O(\FSM_sequential_rx_state[3]_i_8_n_0 ));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_out),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_valid_sync),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hEEEFFFFFEEEF0000)) 
    reset_time_out_i_1__0
       (.I0(reset_time_out_i_2_n_0),
        .I1(reset_time_out_reg),
        .I2(reset_time_out_reg_0),
        .I3(Q[1]),
        .I4(reset_time_out_reg_1),
        .I5(reset_time_out_reg_2),
        .O(\FSM_sequential_rx_state_reg[1] ));
  LUT6 #(
    .INIT(64'h0FF30E0E0FF30202)) 
    reset_time_out_i_2
       (.I0(\FSM_sequential_rx_state_reg[0]_1 ),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(data_valid_sync),
        .I4(Q[3]),
        .I5(mmcm_lock_reclocked),
        .O(reset_time_out_i_2_n_0));
  LUT4 #(
    .INIT(16'hABA8)) 
    rx_fsm_reset_done_int_i_1
       (.I0(rx_fsm_reset_done_int),
        .I1(rx_fsm_reset_done_int_i_3_n_0),
        .I2(rx_fsm_reset_done_int_i_4_n_0),
        .I3(data_in),
        .O(rx_fsm_reset_done_int_reg));
  LUT5 #(
    .INIT(32'h00040000)) 
    rx_fsm_reset_done_int_i_2
       (.I0(Q[0]),
        .I1(data_valid_sync),
        .I2(Q[2]),
        .I3(reset_time_out_reg_2),
        .I4(rx_fsm_reset_done_int_reg_2),
        .O(rx_fsm_reset_done_int));
  LUT6 #(
    .INIT(64'h0400040004040400)) 
    rx_fsm_reset_done_int_i_3
       (.I0(rx_fsm_reset_done_int_reg_0),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(data_valid_sync),
        .I4(rx_fsm_reset_done_int_reg_1),
        .I5(reset_time_out_reg_2),
        .O(rx_fsm_reset_done_int_i_3_n_0));
  LUT6 #(
    .INIT(64'h0008000808080008)) 
    rx_fsm_reset_done_int_i_4
       (.I0(rx_fsm_reset_done_int_reg_3),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(data_valid_sync),
        .I4(rx_fsm_reset_done_int_reg_2),
        .I5(reset_time_out_reg_2),
        .O(rx_fsm_reset_done_int_i_4_n_0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_13
   (SR,
    data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output [0:0]SR;
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire [0:0]SR;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[7]_i_1__0 
       (.I0(data_out),
        .O(SR));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_14
   (data_out,
    data_in,
    data_sync_reg1_0);
  output data_out;
  input data_in;
  input data_sync_reg1_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_15
   (data_out,
    data_in,
    independent_clock_bufg);
  output data_out;
  input data_in;
  input independent_clock_bufg;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_16
   (data_out,
    data_in,
    data_sync_reg6_0);
  output data_out;
  input data_in;
  input data_sync_reg6_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg6_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_3
   (data_out,
    status_vector,
    independent_clock_bufg);
  output data_out;
  input [0:0]status_vector;
  input independent_clock_bufg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire independent_clock_bufg;
  wire [0:0]status_vector;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(status_vector),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_4
   (data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_5
   (reset_time_out_reg,
    E,
    reset_time_out_reg_0,
    reset_time_out,
    \FSM_sequential_tx_state_reg[0] ,
    \FSM_sequential_tx_state_reg[0]_0 ,
    \FSM_sequential_tx_state_reg[0]_1 ,
    \FSM_sequential_tx_state_reg[0]_2 ,
    \FSM_sequential_tx_state_reg[0]_3 ,
    Q,
    reset_time_out_reg_1,
    mmcm_lock_reclocked,
    \FSM_sequential_tx_state_reg[0]_4 ,
    \FSM_sequential_tx_state_reg[0]_5 ,
    \FSM_sequential_tx_state_reg[0]_6 ,
    data_sync_reg1_0,
    independent_clock_bufg);
  output reset_time_out_reg;
  output [0:0]E;
  input reset_time_out_reg_0;
  input reset_time_out;
  input \FSM_sequential_tx_state_reg[0] ;
  input \FSM_sequential_tx_state_reg[0]_0 ;
  input \FSM_sequential_tx_state_reg[0]_1 ;
  input \FSM_sequential_tx_state_reg[0]_2 ;
  input \FSM_sequential_tx_state_reg[0]_3 ;
  input [3:0]Q;
  input reset_time_out_reg_1;
  input mmcm_lock_reclocked;
  input \FSM_sequential_tx_state_reg[0]_4 ;
  input \FSM_sequential_tx_state_reg[0]_5 ;
  input \FSM_sequential_tx_state_reg[0]_6 ;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire [0:0]E;
  wire \FSM_sequential_tx_state[3]_i_5_n_0 ;
  wire \FSM_sequential_tx_state_reg[0] ;
  wire \FSM_sequential_tx_state_reg[0]_0 ;
  wire \FSM_sequential_tx_state_reg[0]_1 ;
  wire \FSM_sequential_tx_state_reg[0]_2 ;
  wire \FSM_sequential_tx_state_reg[0]_3 ;
  wire \FSM_sequential_tx_state_reg[0]_4 ;
  wire \FSM_sequential_tx_state_reg[0]_5 ;
  wire \FSM_sequential_tx_state_reg[0]_6 ;
  wire [3:0]Q;
  wire cplllock_sync;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;
  wire mmcm_lock_reclocked;
  wire reset_time_out;
  wire reset_time_out_i_3__0_n_0;
  wire reset_time_out_i_4__0_n_0;
  wire reset_time_out_reg;
  wire reset_time_out_reg_0;
  wire reset_time_out_reg_1;

  LUT6 #(
    .INIT(64'hFFFFFFFEFFFEFFFE)) 
    \FSM_sequential_tx_state[3]_i_1 
       (.I0(\FSM_sequential_tx_state_reg[0] ),
        .I1(\FSM_sequential_tx_state_reg[0]_0 ),
        .I2(\FSM_sequential_tx_state[3]_i_5_n_0 ),
        .I3(\FSM_sequential_tx_state_reg[0]_1 ),
        .I4(\FSM_sequential_tx_state_reg[0]_2 ),
        .I5(\FSM_sequential_tx_state_reg[0]_3 ),
        .O(E));
  LUT6 #(
    .INIT(64'h0000000000F00008)) 
    \FSM_sequential_tx_state[3]_i_5 
       (.I0(\FSM_sequential_tx_state_reg[0]_4 ),
        .I1(\FSM_sequential_tx_state_reg[0]_5 ),
        .I2(cplllock_sync),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(\FSM_sequential_tx_state_reg[0]_6 ),
        .O(\FSM_sequential_tx_state[3]_i_5_n_0 ));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(cplllock_sync),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hEFE0)) 
    reset_time_out_i_1
       (.I0(reset_time_out_reg_0),
        .I1(reset_time_out_i_3__0_n_0),
        .I2(reset_time_out_i_4__0_n_0),
        .I3(reset_time_out),
        .O(reset_time_out_reg));
  LUT6 #(
    .INIT(64'h020002000F000200)) 
    reset_time_out_i_3__0
       (.I0(cplllock_sync),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(Q[0]),
        .I4(mmcm_lock_reclocked),
        .I5(Q[1]),
        .O(reset_time_out_i_3__0_n_0));
  LUT6 #(
    .INIT(64'h0505FF040505F504)) 
    reset_time_out_i_4__0
       (.I0(Q[1]),
        .I1(reset_time_out_reg_1),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(Q[3]),
        .I5(cplllock_sync),
        .O(reset_time_out_i_4__0_n_0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_6
   (SR,
    data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output [0:0]SR;
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire [0:0]SR;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[7]_i_1 
       (.I0(data_out),
        .O(SR));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_7
   (data_out,
    data_in,
    data_sync_reg6_0);
  output data_out;
  input data_in;
  input data_sync_reg6_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg6_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_8
   (data_out,
    data_in,
    independent_clock_bufg);
  output data_out;
  input data_in;
  input independent_clock_bufg;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_9
   (data_out,
    data_in,
    data_sync_reg1_0);
  output data_out;
  input data_in;
  input data_sync_reg1_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

module gig_ethernet_pcs_pma_0_transceiver
   (txn,
    txp,
    rxoutclk,
    txoutclk,
    rxchariscomma,
    rxcharisk,
    rxdisperr,
    rxnotintable,
    rxbufstatus,
    txbuferr,
    mmcm_reset,
    data_in,
    rx_fsm_reset_done_int_reg,
    Q,
    \rxdata_reg[7]_0 ,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    gtxe2_i,
    SR,
    CLK,
    powerdown,
    reset_sync5,
    D,
    txchardispval_reg_reg_0,
    txcharisk_reg_reg_0,
    out,
    status_vector,
    enablealign,
    data_sync_reg1,
    \txdata_reg_reg[7]_0 );
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output [0:0]rxchariscomma;
  output [0:0]rxcharisk;
  output [0:0]rxdisperr;
  output [0:0]rxnotintable;
  output [0:0]rxbufstatus;
  output txbuferr;
  output mmcm_reset;
  output data_in;
  output rx_fsm_reset_done_int_reg;
  output [1:0]Q;
  output [7:0]\rxdata_reg[7]_0 ;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input gtxe2_i;
  input [0:0]SR;
  input CLK;
  input powerdown;
  input [0:0]reset_sync5;
  input [0:0]D;
  input [0:0]txchardispval_reg_reg_0;
  input [0:0]txcharisk_reg_reg_0;
  input [0:0]out;
  input [0:0]status_vector;
  input enablealign;
  input data_sync_reg1;
  input [7:0]\txdata_reg_reg[7]_0 ;

  wire CLK;
  wire [0:0]D;
  wire [1:0]Q;
  wire [0:0]SR;
  wire data_in;
  wire data_sync_reg1;
  wire data_valid_reg2;
  wire enablealign;
  wire encommaalign_int;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtwizard_inst_n_6;
  wire gtwizard_inst_n_7;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire mmcm_reset;
  wire [0:0]out;
  wire p_0_in;
  wire powerdown;
  wire reset;
  wire [0:0]reset_sync5;
  wire rx_fsm_reset_done_int_reg;
  wire [0:0]rxbufstatus;
  wire [0:0]rxchariscomma;
  wire [1:0]rxchariscomma_double;
  wire rxchariscomma_i_1_n_0;
  wire [1:0]rxchariscomma_int;
  wire [1:0]rxchariscomma_reg__0;
  wire [0:0]rxcharisk;
  wire [1:0]rxcharisk_double;
  wire rxcharisk_i_1_n_0;
  wire [1:0]rxcharisk_int;
  wire [1:0]rxcharisk_reg__0;
  wire [1:0]rxclkcorcnt_double;
  wire [1:0]rxclkcorcnt_int;
  wire [1:0]rxclkcorcnt_reg;
  wire \rxdata[0]_i_1_n_0 ;
  wire \rxdata[1]_i_1_n_0 ;
  wire \rxdata[2]_i_1_n_0 ;
  wire \rxdata[3]_i_1_n_0 ;
  wire \rxdata[4]_i_1_n_0 ;
  wire \rxdata[5]_i_1_n_0 ;
  wire \rxdata[6]_i_1_n_0 ;
  wire \rxdata[7]_i_1_n_0 ;
  wire [15:0]rxdata_double;
  wire [15:0]rxdata_int;
  wire [15:0]rxdata_reg;
  wire [7:0]\rxdata_reg[7]_0 ;
  wire [0:0]rxdisperr;
  wire [1:0]rxdisperr_double;
  wire rxdisperr_i_1_n_0;
  wire [1:0]rxdisperr_int;
  wire [1:0]rxdisperr_reg__0;
  wire rxn;
  wire [0:0]rxnotintable;
  wire [1:0]rxnotintable_double;
  wire rxnotintable_i_1_n_0;
  wire [1:0]rxnotintable_int;
  wire [1:0]rxnotintable_reg__0;
  wire rxoutclk;
  wire rxp;
  wire rxpowerdown;
  wire rxpowerdown_double;
  wire rxpowerdown_reg__0;
  wire rxreset_int;
  wire [0:0]status_vector;
  wire toggle;
  wire toggle_i_1_n_0;
  wire toggle_rx;
  wire toggle_rx_i_1_n_0;
  wire txbuferr;
  wire [1:1]txbufstatus_reg;
  wire [1:0]txchardispmode_double;
  wire [1:0]txchardispmode_int;
  wire txchardispmode_reg;
  wire [1:0]txchardispval_double;
  wire [1:0]txchardispval_int;
  wire txchardispval_reg;
  wire [0:0]txchardispval_reg_reg_0;
  wire [1:0]txcharisk_double;
  wire [1:0]txcharisk_int;
  wire txcharisk_reg;
  wire [0:0]txcharisk_reg_reg_0;
  wire [15:0]txdata_double;
  wire [15:0]txdata_int;
  wire [7:0]txdata_reg;
  wire [7:0]\txdata_reg_reg[7]_0 ;
  wire txn;
  wire txoutclk;
  wire txp;
  wire txpowerdown;
  wire txpowerdown_double;
  wire txpowerdown_reg__0;
  wire txreset_int;

  gig_ethernet_pcs_pma_0_GTWIZARD gtwizard_inst
       (.D(rxclkcorcnt_int),
        .Q(txdata_int),
        .RXBUFSTATUS(gtwizard_inst_n_7),
        .RXPD(rxpowerdown),
        .TXBUFSTATUS(gtwizard_inst_n_6),
        .TXPD(txpowerdown),
        .data_in(data_in),
        .data_out(data_valid_reg2),
        .data_sync_reg1(data_sync_reg1),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(rxdata_int),
        .gtxe2_i_0(rxchariscomma_int),
        .gtxe2_i_1(rxcharisk_int),
        .gtxe2_i_2(rxdisperr_int),
        .gtxe2_i_3(rxnotintable_int),
        .gtxe2_i_4(gtxe2_i),
        .gtxe2_i_5(txchardispmode_int),
        .gtxe2_i_6(txchardispval_int),
        .gtxe2_i_7(txcharisk_int),
        .gtxe2_i_8(rxreset_int),
        .gtxe2_i_9(txreset_int),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out),
        .reset(reset),
        .reset_out(encommaalign_int),
        .rx_fsm_reset_done_int_reg(rx_fsm_reset_done_int_reg),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
  gig_ethernet_pcs_pma_0_reset_sync reclock_encommaalign
       (.CLK(CLK),
        .enablealign(enablealign),
        .reset_out(encommaalign_int));
  gig_ethernet_pcs_pma_0_reset_sync_1 reclock_rxreset
       (.SR(SR),
        .independent_clock_bufg(independent_clock_bufg),
        .reset_out(rxreset_int));
  gig_ethernet_pcs_pma_0_reset_sync_2 reclock_txreset
       (.independent_clock_bufg(independent_clock_bufg),
        .reset_out(txreset_int),
        .reset_sync5_0(reset_sync5));
  gig_ethernet_pcs_pma_0_reset_wtd_timer reset_wtd_timer
       (.data_out(data_valid_reg2),
        .independent_clock_bufg(independent_clock_bufg),
        .reset(reset));
  FDRE rxbuferr_reg
       (.C(CLK),
        .CE(1'b1),
        .D(p_0_in),
        .Q(rxbufstatus),
        .R(1'b0));
  FDRE \rxbufstatus_reg_reg[2] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(gtwizard_inst_n_7),
        .Q(p_0_in),
        .R(1'b0));
  FDRE \rxchariscomma_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxchariscomma_reg__0[0]),
        .Q(rxchariscomma_double[0]),
        .R(SR));
  FDRE \rxchariscomma_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxchariscomma_reg__0[1]),
        .Q(rxchariscomma_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxchariscomma_i_1
       (.I0(rxchariscomma_double[1]),
        .I1(toggle_rx),
        .I2(rxchariscomma_double[0]),
        .O(rxchariscomma_i_1_n_0));
  FDRE rxchariscomma_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxchariscomma_i_1_n_0),
        .Q(rxchariscomma),
        .R(SR));
  FDRE \rxchariscomma_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxchariscomma_int[0]),
        .Q(rxchariscomma_reg__0[0]),
        .R(1'b0));
  FDRE \rxchariscomma_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxchariscomma_int[1]),
        .Q(rxchariscomma_reg__0[1]),
        .R(1'b0));
  FDRE \rxcharisk_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxcharisk_reg__0[0]),
        .Q(rxcharisk_double[0]),
        .R(SR));
  FDRE \rxcharisk_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxcharisk_reg__0[1]),
        .Q(rxcharisk_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxcharisk_i_1
       (.I0(rxcharisk_double[1]),
        .I1(toggle_rx),
        .I2(rxcharisk_double[0]),
        .O(rxcharisk_i_1_n_0));
  FDRE rxcharisk_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxcharisk_i_1_n_0),
        .Q(rxcharisk),
        .R(SR));
  FDRE \rxcharisk_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxcharisk_int[0]),
        .Q(rxcharisk_reg__0[0]),
        .R(1'b0));
  FDRE \rxcharisk_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxcharisk_int[1]),
        .Q(rxcharisk_reg__0[1]),
        .R(1'b0));
  FDRE \rxclkcorcnt_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxclkcorcnt_reg[0]),
        .Q(rxclkcorcnt_double[0]),
        .R(SR));
  FDRE \rxclkcorcnt_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxclkcorcnt_reg[1]),
        .Q(rxclkcorcnt_double[1]),
        .R(SR));
  FDRE \rxclkcorcnt_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxclkcorcnt_double[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE \rxclkcorcnt_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxclkcorcnt_double[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE \rxclkcorcnt_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxclkcorcnt_int[0]),
        .Q(rxclkcorcnt_reg[0]),
        .R(1'b0));
  FDRE \rxclkcorcnt_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxclkcorcnt_int[1]),
        .Q(rxclkcorcnt_reg[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[0]_i_1 
       (.I0(rxdata_double[8]),
        .I1(toggle_rx),
        .I2(rxdata_double[0]),
        .O(\rxdata[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[1]_i_1 
       (.I0(rxdata_double[9]),
        .I1(toggle_rx),
        .I2(rxdata_double[1]),
        .O(\rxdata[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[2]_i_1 
       (.I0(rxdata_double[10]),
        .I1(toggle_rx),
        .I2(rxdata_double[2]),
        .O(\rxdata[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[3]_i_1 
       (.I0(rxdata_double[11]),
        .I1(toggle_rx),
        .I2(rxdata_double[3]),
        .O(\rxdata[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[4]_i_1 
       (.I0(rxdata_double[12]),
        .I1(toggle_rx),
        .I2(rxdata_double[4]),
        .O(\rxdata[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[5]_i_1 
       (.I0(rxdata_double[13]),
        .I1(toggle_rx),
        .I2(rxdata_double[5]),
        .O(\rxdata[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[6]_i_1 
       (.I0(rxdata_double[14]),
        .I1(toggle_rx),
        .I2(rxdata_double[6]),
        .O(\rxdata[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[7]_i_1 
       (.I0(rxdata_double[15]),
        .I1(toggle_rx),
        .I2(rxdata_double[7]),
        .O(\rxdata[7]_i_1_n_0 ));
  FDRE \rxdata_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[0]),
        .Q(rxdata_double[0]),
        .R(SR));
  FDRE \rxdata_double_reg[10] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[10]),
        .Q(rxdata_double[10]),
        .R(SR));
  FDRE \rxdata_double_reg[11] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[11]),
        .Q(rxdata_double[11]),
        .R(SR));
  FDRE \rxdata_double_reg[12] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[12]),
        .Q(rxdata_double[12]),
        .R(SR));
  FDRE \rxdata_double_reg[13] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[13]),
        .Q(rxdata_double[13]),
        .R(SR));
  FDRE \rxdata_double_reg[14] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[14]),
        .Q(rxdata_double[14]),
        .R(SR));
  FDRE \rxdata_double_reg[15] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[15]),
        .Q(rxdata_double[15]),
        .R(SR));
  FDRE \rxdata_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[1]),
        .Q(rxdata_double[1]),
        .R(SR));
  FDRE \rxdata_double_reg[2] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[2]),
        .Q(rxdata_double[2]),
        .R(SR));
  FDRE \rxdata_double_reg[3] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[3]),
        .Q(rxdata_double[3]),
        .R(SR));
  FDRE \rxdata_double_reg[4] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[4]),
        .Q(rxdata_double[4]),
        .R(SR));
  FDRE \rxdata_double_reg[5] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[5]),
        .Q(rxdata_double[5]),
        .R(SR));
  FDRE \rxdata_double_reg[6] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[6]),
        .Q(rxdata_double[6]),
        .R(SR));
  FDRE \rxdata_double_reg[7] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[7]),
        .Q(rxdata_double[7]),
        .R(SR));
  FDRE \rxdata_double_reg[8] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[8]),
        .Q(rxdata_double[8]),
        .R(SR));
  FDRE \rxdata_double_reg[9] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[9]),
        .Q(rxdata_double[9]),
        .R(SR));
  FDRE \rxdata_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[0]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [0]),
        .R(SR));
  FDRE \rxdata_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[1]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [1]),
        .R(SR));
  FDRE \rxdata_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[2]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [2]),
        .R(SR));
  FDRE \rxdata_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[3]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [3]),
        .R(SR));
  FDRE \rxdata_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[4]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [4]),
        .R(SR));
  FDRE \rxdata_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[5]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [5]),
        .R(SR));
  FDRE \rxdata_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[6]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [6]),
        .R(SR));
  FDRE \rxdata_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[7]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [7]),
        .R(SR));
  FDRE \rxdata_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[0]),
        .Q(rxdata_reg[0]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[10] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[10]),
        .Q(rxdata_reg[10]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[11] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[11]),
        .Q(rxdata_reg[11]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[12] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[12]),
        .Q(rxdata_reg[12]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[13] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[13]),
        .Q(rxdata_reg[13]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[14] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[14]),
        .Q(rxdata_reg[14]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[15] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[15]),
        .Q(rxdata_reg[15]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[1]),
        .Q(rxdata_reg[1]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[2] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[2]),
        .Q(rxdata_reg[2]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[3] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[3]),
        .Q(rxdata_reg[3]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[4] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[4]),
        .Q(rxdata_reg[4]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[5] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[5]),
        .Q(rxdata_reg[5]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[6] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[6]),
        .Q(rxdata_reg[6]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[7] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[7]),
        .Q(rxdata_reg[7]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[8] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[8]),
        .Q(rxdata_reg[8]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[9] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[9]),
        .Q(rxdata_reg[9]),
        .R(1'b0));
  FDRE \rxdisperr_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdisperr_reg__0[0]),
        .Q(rxdisperr_double[0]),
        .R(SR));
  FDRE \rxdisperr_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdisperr_reg__0[1]),
        .Q(rxdisperr_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxdisperr_i_1
       (.I0(rxdisperr_double[1]),
        .I1(toggle_rx),
        .I2(rxdisperr_double[0]),
        .O(rxdisperr_i_1_n_0));
  FDRE rxdisperr_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxdisperr_i_1_n_0),
        .Q(rxdisperr),
        .R(SR));
  FDRE \rxdisperr_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdisperr_int[0]),
        .Q(rxdisperr_reg__0[0]),
        .R(1'b0));
  FDRE \rxdisperr_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdisperr_int[1]),
        .Q(rxdisperr_reg__0[1]),
        .R(1'b0));
  FDRE \rxnotintable_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxnotintable_reg__0[0]),
        .Q(rxnotintable_double[0]),
        .R(SR));
  FDRE \rxnotintable_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxnotintable_reg__0[1]),
        .Q(rxnotintable_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxnotintable_i_1
       (.I0(rxnotintable_double[1]),
        .I1(toggle_rx),
        .I2(rxnotintable_double[0]),
        .O(rxnotintable_i_1_n_0));
  FDRE rxnotintable_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxnotintable_i_1_n_0),
        .Q(rxnotintable),
        .R(SR));
  FDRE \rxnotintable_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxnotintable_int[0]),
        .Q(rxnotintable_reg__0[0]),
        .R(1'b0));
  FDRE \rxnotintable_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxnotintable_int[1]),
        .Q(rxnotintable_reg__0[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_double_reg
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxpowerdown_reg__0),
        .Q(rxpowerdown_double),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_reg
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxpowerdown_double),
        .Q(rxpowerdown),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(powerdown),
        .Q(rxpowerdown_reg__0),
        .R(SR));
  gig_ethernet_pcs_pma_0_sync_block_3 sync_block_data_valid
       (.data_out(data_valid_reg2),
        .independent_clock_bufg(independent_clock_bufg),
        .status_vector(status_vector));
  LUT1 #(
    .INIT(2'h1)) 
    toggle_i_1
       (.I0(toggle),
        .O(toggle_i_1_n_0));
  FDRE toggle_reg
       (.C(CLK),
        .CE(1'b1),
        .D(toggle_i_1_n_0),
        .Q(toggle),
        .R(reset_sync5));
  LUT1 #(
    .INIT(2'h1)) 
    toggle_rx_i_1
       (.I0(toggle_rx),
        .O(toggle_rx_i_1_n_0));
  FDRE toggle_rx_reg
       (.C(CLK),
        .CE(1'b1),
        .D(toggle_rx_i_1_n_0),
        .Q(toggle_rx),
        .R(SR));
  FDRE txbuferr_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txbufstatus_reg),
        .Q(txbuferr),
        .R(1'b0));
  FDRE \txbufstatus_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(gtwizard_inst_n_6),
        .Q(txbufstatus_reg),
        .R(1'b0));
  FDRE \txchardispmode_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txchardispmode_reg),
        .Q(txchardispmode_double[0]),
        .R(reset_sync5));
  FDRE \txchardispmode_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(D),
        .Q(txchardispmode_double[1]),
        .R(reset_sync5));
  FDRE \txchardispmode_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispmode_double[0]),
        .Q(txchardispmode_int[0]),
        .R(1'b0));
  FDRE \txchardispmode_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispmode_double[1]),
        .Q(txchardispmode_int[1]),
        .R(1'b0));
  FDRE txchardispmode_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(D),
        .Q(txchardispmode_reg),
        .R(reset_sync5));
  FDRE \txchardispval_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txchardispval_reg),
        .Q(txchardispval_double[0]),
        .R(reset_sync5));
  FDRE \txchardispval_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txchardispval_reg_reg_0),
        .Q(txchardispval_double[1]),
        .R(reset_sync5));
  FDRE \txchardispval_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispval_double[0]),
        .Q(txchardispval_int[0]),
        .R(1'b0));
  FDRE \txchardispval_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispval_double[1]),
        .Q(txchardispval_int[1]),
        .R(1'b0));
  FDRE txchardispval_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txchardispval_reg_reg_0),
        .Q(txchardispval_reg),
        .R(reset_sync5));
  FDRE \txcharisk_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txcharisk_reg),
        .Q(txcharisk_double[0]),
        .R(reset_sync5));
  FDRE \txcharisk_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txcharisk_reg_reg_0),
        .Q(txcharisk_double[1]),
        .R(reset_sync5));
  FDRE \txcharisk_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txcharisk_double[0]),
        .Q(txcharisk_int[0]),
        .R(1'b0));
  FDRE \txcharisk_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txcharisk_double[1]),
        .Q(txcharisk_int[1]),
        .R(1'b0));
  FDRE txcharisk_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txcharisk_reg_reg_0),
        .Q(txcharisk_reg),
        .R(reset_sync5));
  FDRE \txdata_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[0]),
        .Q(txdata_double[0]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[10] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [2]),
        .Q(txdata_double[10]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[11] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [3]),
        .Q(txdata_double[11]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[12] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [4]),
        .Q(txdata_double[12]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[13] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [5]),
        .Q(txdata_double[13]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[14] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [6]),
        .Q(txdata_double[14]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[15] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [7]),
        .Q(txdata_double[15]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[1]),
        .Q(txdata_double[1]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[2] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[2]),
        .Q(txdata_double[2]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[3] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[3]),
        .Q(txdata_double[3]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[4] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[4]),
        .Q(txdata_double[4]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[5] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[5]),
        .Q(txdata_double[5]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[6] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[6]),
        .Q(txdata_double[6]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[7] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[7]),
        .Q(txdata_double[7]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[8] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [0]),
        .Q(txdata_double[8]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[9] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [1]),
        .Q(txdata_double[9]),
        .R(reset_sync5));
  FDRE \txdata_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[0]),
        .Q(txdata_int[0]),
        .R(1'b0));
  FDRE \txdata_int_reg[10] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[10]),
        .Q(txdata_int[10]),
        .R(1'b0));
  FDRE \txdata_int_reg[11] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[11]),
        .Q(txdata_int[11]),
        .R(1'b0));
  FDRE \txdata_int_reg[12] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[12]),
        .Q(txdata_int[12]),
        .R(1'b0));
  FDRE \txdata_int_reg[13] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[13]),
        .Q(txdata_int[13]),
        .R(1'b0));
  FDRE \txdata_int_reg[14] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[14]),
        .Q(txdata_int[14]),
        .R(1'b0));
  FDRE \txdata_int_reg[15] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[15]),
        .Q(txdata_int[15]),
        .R(1'b0));
  FDRE \txdata_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[1]),
        .Q(txdata_int[1]),
        .R(1'b0));
  FDRE \txdata_int_reg[2] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[2]),
        .Q(txdata_int[2]),
        .R(1'b0));
  FDRE \txdata_int_reg[3] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[3]),
        .Q(txdata_int[3]),
        .R(1'b0));
  FDRE \txdata_int_reg[4] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[4]),
        .Q(txdata_int[4]),
        .R(1'b0));
  FDRE \txdata_int_reg[5] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[5]),
        .Q(txdata_int[5]),
        .R(1'b0));
  FDRE \txdata_int_reg[6] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[6]),
        .Q(txdata_int[6]),
        .R(1'b0));
  FDRE \txdata_int_reg[7] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[7]),
        .Q(txdata_int[7]),
        .R(1'b0));
  FDRE \txdata_int_reg[8] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[8]),
        .Q(txdata_int[8]),
        .R(1'b0));
  FDRE \txdata_int_reg[9] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[9]),
        .Q(txdata_int[9]),
        .R(1'b0));
  FDRE \txdata_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [0]),
        .Q(txdata_reg[0]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [1]),
        .Q(txdata_reg[1]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [2]),
        .Q(txdata_reg[2]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [3]),
        .Q(txdata_reg[3]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [4]),
        .Q(txdata_reg[4]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [5]),
        .Q(txdata_reg[5]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [6]),
        .Q(txdata_reg[6]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [7]),
        .Q(txdata_reg[7]),
        .R(reset_sync5));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_double_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txpowerdown_reg__0),
        .Q(txpowerdown_double),
        .R(reset_sync5));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_reg
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txpowerdown_double),
        .Q(txpowerdown),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(powerdown),
        .Q(txpowerdown_reg__0),
        .R(reset_sync5));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2023.2"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
TCSZ2Ye5IRfHavlF8Mno1PI9xixWuSiNh3ssU1FQtkjW1fmNtc2c3x12slL242UQayI0rzZTqe6S
edtecLHTOnzxXpCZjjU8NFmgLPerTSDZ1W5YhyIi9j0Ap4YBpvaA1ojM0+r0Cx+dMOXohQGeyljq
+fnTaFTUe2678DxpqHk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
NIzZVKMB1/8UX2qb4BB+VXCafEKXsrn7axB1cJDPqDCzSyt/2KG1NEEZTDHZVzIr8Bf9501PyXmL
VowTAAXX/RopKyKOM1xJN/qLtqXxegH2a4dIkUxDIIclIcbv/smna9VCwI7m6JhrnKsNciTTilgR
27S/h6JPpZsZAEmsNxxTC70WQhQSM8TlHJjZg3KDc5KTnvC/mVTk6I05U6x0Bdd1YR9GBvhwRqhP
B1ukL/1JVOwR9Ce9p+EHFE/xyApypCjQPGwq+8IFQgS8wltVZHX6eSMw17Q0wGCY+LHduRTA+abV
LvAR0NPf7PKQUSCECe2mBbLPO7wD4BO5RAkJeA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
udw4XxxzcaDNM1qWnRgZ2JEM1MMqnKwKVqha/krU9EyUAsyATjQEMBqjlOHw5QXMU2jjizlL20Nl
h2pF7iKo1S+7TS54Y/UIJANp+Dl46V/qfy6/yBnE4YclHON1k0jRao4C6T951tgXuCAIQEmXbr87
aJfL2dNqORH+TDKUBdc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
JFe89rPDaiIUicPDNoXCg8pJUoYRyVDoW/5yE4T0Cp9xDHtuIyKQVbC7jVb92OsgJ5GHDm7DH2D2
rYZKrdCIqPt2jo7DG6bcJuDFcisZb11HLlYWNsK2Vqs9DdsTPViykeE05CD5AgfDxb983x8F1meK
w8zjeGoD44djsaRA+lvP1zLhl24q5LWFJdPSyIT7uWZwhxHqlyJu85ToXLuwZQZO76Mp+1mitxDy
vleizC5rnk/4hqxfEFS21Qi1TwCz5hdU+H3nA3dTe1KRY+obbFP7sRWKfmr9Rcf9enRvbaEbLoJA
9ADkl72jc1Aqlnd+YCGq4EmbElbWLxblpamncA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IV5qOrW4jXfiGt6hz/YcNm+/H3ij0v503CF3Kvut91tUWldzNzyLt4pIZEWEzSmn6RcpcLNN88po
1kt45UdSBz+mL5HDQaw4J+VGD/cCBmW1jnOclCf82kwju1MIDfa2EKicjqaykCUROxV7cwg07FFp
clLfIwd4kxgSWnGzeZi1IGezx7OpBsAkBTz9ha4WttEm0+D29DF9O4GaQl6q8IBeA0QIrO10EESt
slfRi2evxdOeTZBVFoXU91OszneH/prZqyCsHeyvTa8PABTZ+Y4CH6ICZCXRn7QTNJgoYSGABuPs
87saNJgzomjyaO6IzGl1fBgMIsIurKw90DE8Jw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2022_10", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Eek/EeBa5kEiakvxzHC3fZ0NXCsvWnLN8FYKLyImepfBUv2jdVDY2j6Qs928DJaMO8pBuO9SGcu3
47rhcN1DAjZza5Ac83W24fngY1+YWblivVc8AoggXS1t2Y7Dy1vf9+ZxUdOvq63sje+fDJxapZwK
3HQGdtBX86RTaUS5K+HyI1FTmcIhUYmJWmxQjIxLla7FF1QZ4XpTCfqAG5i7ZKlYSoDFb8sjCRG4
XWFuk1dbL2UfZPxXZ7XHIm+03Ck/JsHtsjLCc8oTB/9MLom2HX9SjX8H6tFbEXR1NatCFWQ04JKL
kHSYD/xDlwjhN9CRvowRhNJaYSmKQT646hlNoA==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
gaID+cgqOdyoJPDKM9JAyimEjoxNakxuNjYf52N14HEyn4NQksF7thq/bXWc82vmfdc8aodx1+ky
i8uuKszW1WwV+apGSqk7YXBCxx3ACfMsPzNzeDQ2HVzGfznpQD80Eu7I7iwtz3k5Mr31iaeM1kQa
oddk6CkVESI8CD21PQHMVeu0LKLZJp8k8NHf3i0UOXsP5o768iecieYQh2VYXZ6HORDDyd+IpDB1
CAFBZctXco8C1w74wCB0LXUSYInc5ythxBURkPPTJ1GBuXpoQGZD2sNiI2Htl0y1toEdfgExWZ+0
+4Docnd9TgOGhAhZzUcj3c+6cQNbgCB847/G6w==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
tEBTlBoXowC0cOrrCu9pEZ6t9FjMS6GWThbzsXMvR2xb9HBcccdvXT7EfASM09KkNzvlYoyNBWct
0TRl1BNzzlomu3G857U6kezS+CCRF/K2qOhhxHFxEfuM0qblRVdNHoCGGMM4PkE/rt9M7IqYoXQg
WOHI1ydpZZn08aVL9QYJgz6ZuVHNLwSpL6rjFFDXV1cB82gVFBkRP/0NxpGW2WH6YA/MJ0czV0ji
o0umOWluEwUObdytKX1lfuNYimI0ziWrovqq2osL8J7NBKDUl2R6gJ51DObsTBgC8uyUHVibyNHy
nhzTpwcBeeXdtAueCg1BlHDIwglcMUdy0sBZEyHM/CLzpxgr1A+uUcmzlWx1drrc8lRNwGMFDDJQ
9OzoHBABtNt8N3bbO8A+rE9HtsjMVr1TxHhUTxBhWcypwra+xzsGykln/IP3JBwwQR0+d+V8/Vec
5Bh03crJTvJZUbYidozNoaPOfnHi0NxFDNdL7L1i75T+H6bqeE1ADR/4

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GM0AtcDH85MjCjnA/pijf0ZCJap8X+OYUX7W1StOwznqG2XPd9DhtvTyqD0c8/7BTdeCzGUK5iqe
QiGwEcy1dCrSVZW0KtjFXllkYV2ai3/Qn7Bgg1YuzxifEFKe6ClTsByfgjqRdyZeNSAldwvx9ZtT
0ZhijV96K37zXwfXFeKDmxOZOV553ovWfXGekaS1EPmSluoDYBMQKc2XV+ZUXR7n5NI/6E3QdK7K
utsZyrFYyJdYW8Po28hQf1nWeQP6+PxQB6wi/P6sUzudntNcQ7uLRr4PTz6twPPqYwUF+7YW8baL
p/2EFPf8y6fBb+DOBCnzmGZvmq+M2qQot14r+g==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Ot2lizvbpt8emXxoZl10oi4H/aTQTzrHDg0mf7DDs3BS7iyFsRmaQCG/BRF/mwjlw+EbST4/x/DJ
d6Hf6LIa6mllnMD6G+uVHQ1z31eASHPw3m+WnPMr/zCNuebPcDitgiXWmq3SAS9byYvxiwcDvjn6
CMh89pvlx8xLiFUoo5j/lAPe4cPBJwSMleQLLB989s6rByi0lVW0QiLTzakaB8DHBMvhIYEfi74m
Lxby6+nYRGrAUKPOemP0Ag/LW83Eup/Wa0jVOtxzlj3foiYhg2mWCt2zyFhgQsDA+oEsDa/KZc0F
OUzOI8vFDrwPmYRwd2ejFI9Nz3/1mb05VQmDRA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
OamBwohUIPOIa5bsJvu3upqnGl4f3jNYS35bg4S059C9qVDgQJcw/it81deNA5zFvzX3Cq9CiO9K
zl02VsdpFgNPjSwEO8F47LJZ4fHx99EmESBogsNwUNitzkuYTLCb7F7ZF2WSJExQ0KsYt+TRp2UH
yQEvpM1lHQYUXxzjw08qUI5ssSnOsQFydvP8BwA/6aGrVJ+LuEgPVdMqLBn0EeAmRsynxJ7OhPGV
DlvHQwtVuBrkvjQHED/Ye43ZIeWPm/xOcjNfZjYeOvdEJqTbaviR0Fo1LFx8EX80uvdFeK55ywDN
wzoZ4cVH87f6VwR1xHdo0JpVxrajZpSZ0jPudw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 153744)
`pragma protect data_block
zFBO8m2RuI5RPL3R8E2E5Wh0jzcFTl8kLA4a08xWUHPb1d1EOCr6JrqJ0Lv/3ifpfxvqEld/mPCD
WS0gv+mmlTTud7j6uKqWn2M7FmmsALOx77APe036+xziUjhF+T8fnMQRgHq62aaZ95Up6PHExI7V
COk37rPxjlfW894GQXapSB6l30ZGBqGCcdIB3GJmAEBNK+SodIe4Uz22MQxsDgsp/gABvWaBkoII
86L+AcJmkZZ3e7Xnp4h9/zBVJdcFovx6ssmvC8NsBBg5SVuoYAd+o9uSXJR+iakSWUDQ7Cv4VABZ
4efedSoAy3Mncqi0FLQn1aosnRH1z6o0pDXfm05epQ4tN9QMyf2YTGg5V3EZQUY7DmrakB/3mVsD
1Ar6aD6oEvqbfEp0BrSkaPYgNjMv67mvhFb1SGby9E4T7ghQYMGzyp5W74fEps0CJrwzy8e3GCak
QtN8BUNlzi90aPbPqCUTcuMNpbk4HcEP29Tp3WUqhfy3pSi4UDl34aGSmdSPyuzP8rPFTxiQwRZz
VH67TOSwejpkod2FVgOwPo4LPrqKffolHB9LykfNBxGu/U5jedNbzL+fGCQ2mrXTdOYfJYQ4mWbe
d8OSE7Msq62KkBxbNhEKufIp0/ta6uhxk06dUTz+Zs2Jb+9Naa5U97iaKF1ndcKnBRP6lCPZeae+
h7TBUFZ5hPah4IQDZzvmVlYilY5Cgkfs6Y5CHW0lbzW5rSRE2mpJcozmBEC2th9nfO1toqUODdMG
wTp/w3VhCKWx2Uodk7C3/ipC3mhMG0hwYHonSc2AyJ8tZbnsItK/zobi9/cE60o5q0CX4NKy/6nW
DVoe5IS6p7ctHZIHeDIyyp7sPNhPqVrL6Aa1zaNpXpYZIzr+O1vNtf0/DsQfBbYQuEzBcdQHJyni
8mL3e6H1H9czZcl62/yaHh0rq8TjTpDyDbjAVDIDlrIm3361luL0Qrpn+hBI49p9ixj5e1irLLn8
6KuEXVSsu++4HfJy5H8g0pXsdr/ewJYmb9gILhlwwb4Z7etPOf8olanho66P8CBN20L+xqpZGowd
6X88yLW9kgV4bQhu7nmuaD97AUDKdVnVmRorda0vQOwwFhCvwr2BAnJQeFeLtGJKrA7yeBTYBZFS
wWmUn1dIkjAMF0jsn5MoCcJnf6Do3VD2BxmWnzXPJ8e39hYxrrX1bUxhXbdPqnKfbS1ji8SjJrlT
jnkHQ5h2LMItGQbQ/DgxYPPGQXuJkSkIxYG3ZJ0ilY8wRZ15Y1zXnz4LqBP3+Zdw5kp6dsdKliAO
Zuc5JBT6ZJJYOfIfRCcbxXtMGPCwqYD8iqBld8sUAIJ0Vf5vKKCamSBgPp7oBVdVsXA99SfOLHQw
2s9qMSYmFOu8Pp8IVVuNQkreWZOdDPzeoeK4UMXihxIM7DI0BFQC7GJaazinC+0cGUQkQN4eL0il
rOkwZN2xmlZLDYWf7fg1QnnYcu6QyycN4cszU5sHPoNeeBdTInywYV4WBTCgt9Esmxw469YqsewJ
53zbdJqiwn+QD+fmS077e+I8QqOsTSRsSntqy5yMDbiYIbgQxcHDUNAGotIifY2LmdNdsneiRZBx
cdQmN1xpGh2hmHm05VZViYtaINRvkQBR6qTe0V/NCT/6Ds/EO3uCNMMVhHyWDiHYyPlwkAFf0VpH
aXVpgRQ78JmvE6cOE0Iy2NxrWwbfcl0tfe9Bm4B3zxCoUBPFJLFNijSqQQoPdmo3yMTugInpLJPV
YUnhhkEHwgoHf0r57C0PZyVBDHDl1JsHHZYzxShsY2LYDUihVbO+7iIp1SaYkmutfFzzzuMDzvcl
P00mm6fA2LIbTN+gpqvmDFqf3SRdmKZZDlsfgolTPNL58NGLTTIAo4nlOPR1wBBmFFpJVtgXq9P2
tLZWrdbRS1cIFqWW+Jq8XOJD1l4rXneWc6l5smZxNePZhbPWteG0kVDedFdUSzoyuf1G0jHk3ziF
cgtcYhVPvb4z39Bs1QSscKW6/UDpCcUmkh69kI179spSpjJsUfehug0pnQRDO7gI/9C0KOYuaFTX
kAU2MJk1cS4nXFxIsas+vXgGfiGHFl9GctEQV1BkK7v7qEOBcL4DT+xS4zJFgXxrRZGkmarEgByV
imakR/yhMTSeF3GTorLT8d661r7x9HTP5/3M8/pJ2KlivetXqjhu/u8awfyC3xGrHqW1jU8e1yhO
NCDtJhhBnM6L9jCQ7ChxHDivl6M5r5PO8Idss3IUvtoHlnUVlg9TpeQXoLRhTZaSsoTu/H/XbbIZ
QQWs0yyfScpimiMDebU5B3XOus2ijUoZ4L/eVZPGi/o1kUgQt4xzaF3kgpdA7jJaCg4LmXgST9S/
4G4lGe5hQuW2iTUmO9FLRiIH8J8/7MbGxukpOonI8c224bsQA2oIXmkbcpfqYrvItM+6XSL5+GhJ
mF1N3f/c8tfoiSck/e8huGfaSw4/+OVBoK5w1K1sh4xmWuEwOaigFwAICT59NoNzeWoJYZQJBt9c
6hRb8+JZp1T+/AmliB/KHuCXRHAKNtaleJwoGe2iPuQ7HUm05eIy4dn8KtclsHSWZhEjKYLmFhr2
AkwDLOKXivorrN4lVzJW1oV6zJLh6eh3C+kBVgMv/IMnLQKRH5a6gyxAGTiyBWcffQPx/8946Kbf
p/ph6bWq9gFzPM4eckfihZYKmKzZahWiy0HtcfWKZlidEpUUlwJEtcNiIXDB9PSs43Zp8LtpiUyX
pr4Nesxa9Uehts3wtDXy8Kio5V1rUB7kkzikTpLZSNYE4ne4b1ZH3Fh4XWpW3U+hQhmCYd/8Xm7Y
hLY6w3CfFnBwz4J1rYqneRAROGHblT5mkU8tHkZcX7naQa7oHqiWSgq/85tGilB5AOWbuQdFC4vU
D6TVFLRFDq60FfJn9Lvoi5KGy73eL0JTeBV1E8hUu22D7tI1y9pzhL21hQWUQuQQ4r4gnAQEJ1VV
3YbYQNyuh2JvwD4UVPhfYztoXJUdpRKwm972fvq8v2qCSKlnSYXQ01KxBiV4miuz7G15mfT8Rq3M
oy35uwqAJmivy4BwMRWk4ob39XoYMprNrCQCerwRv/UkNeOYJxc7sUv/JTj8Iga65rSDWhyIxs2L
W8fcHHg7kkpAZiBt6OcDMPHqkwuBic0MkeNSynNu9xiBynggttn0PwiXj86E5mKvdgq5Co31bvZF
VZXBsW8OsebQXCOOgPu9AqT+vuX1s135IQdhe3FVD5SnxfGNFy8DPcBiR93JIBNSoYxheksF4JDl
0G9Qan1kQHysRRboeIsQqsUN+4BGRdZmGXVmTJDf9/02bjErvOlKHjeALmfluv7GECcgv/HATmBG
Gel/eEjhBCDtCJyoPTvTiQQlaqkdKVXVel3ekpulKKqqdEFMusKLBi2gX+mddf/MRvg7E2HYe7pt
iHiMmTULDzJPkvGFIfC0enhvhll0onGlk2RBQDIXq7/rFBwo42EShjuLdvxmy8boUkaMUyoagEDr
OWDJJum2glRJ/3YfAhG1txaNk5tHxUZ6fGG0+ZHJ8lX78vqFX8q8Q6bO4XT+g/Y2PArWHgiiDOVz
V5o1Qky9kUtc+GKEVXop3GE4kjbfFjtu9Ewy8bLkG9p0MNzTvOYsamBq0dVlJ3pbsnBr0+hvupi0
sAVTYl8lf5JX5n7pkiLxJGOQ1clXTuRo3XXi8ED8ekJGOGafEHKoQXcWAb0pqvjJdwPvYf5U3u25
5LIuMmLFsQ9bZJCRLURhUYfqysmNZOQCJN3hZsuQx5WcPrhhHwLYZ+QSAbWsOnmNYzMkQy8pppbb
ZL+2jSOrNdN638SNN5D2N5GdW9bd8yqo/dRqpBGu5l4t49OgGoFTwF3h3XZNyh85o71GqduAJT1w
Yujaaq1qdqZ0Z0PcLdwv/VdwDPViQvipvB8F1dkcD49+qyIkdulWCPCTYH0uMJQooc8NgYs4IxAK
7E9SWJ3yKSSMEJYwSKCRj67G7qgck7z+eIOR59qZYdeZe+Vl7pAwf+Gj4bzoT7DoYevTZXctCLv9
bei2LMZG68ybhRhCaIv7Y/YL05nBmgShK8ZLbsqOfeMzQi4MpZsuTirB8zUXA3iJHrR+nVZQLGkF
iD3p42h+tr3ifHCfJcAlUjeAfTR7Ol9IXpQOjuUH1zGuOTQQhsr0XVWSL/kojjIZe2LsZaxv3mNs
IjL7bXwoP2fqpqBvZKGMYWtO6YM9Y5WsOuUMfIMgilJOkEMvIgkcBfopO9tSOqYQkiYQ3IAfBkeT
zi5C1b/zDcy2n5YJTHc37CHUQWLuPvJai6UuNRNLKSoZ690RzyqJIQ+gD0Wez6z8LwJiV16OrEtD
LFS/VnsFR8xURPDVA1WU08JaVCk7lEiu0beaMvd2wTBmCeKF3/Mrd3gFtTee/oGr7Ug+KKpakMVT
ZnPb5J0VhFMDcz4gg2jRkLkfIHGi6jz7odIdXbpMY31FLSuzB+sPEWzFb0xQLh5diVMv7J6N2YGI
vt1kKdYpjQWUe9oLEp1+SgIYmfNCV+aoVpBZ2HcxZ//W7z/ij1MjZcTWEGvPIJPInc4H3KhB7Q1Y
uF9ZN8iCxocByBCua2hvBVq5GQv+bEUGRJZtSLFDTxyN75uBHNd0kgNeP2sVp41kpxYPi1XlYNY+
Cf4akvb9dbpHE4JKip8XCarTY0GGxJOe+GJbyM0jP0P4V3e9iBTBVkVIFzGlQxN7otLVHJJ0Ae6y
sklx99A3Tw36CNoWkJRMQDLsUQ8Sshx8uKlmop8dMHyw8i1r49nnpHJdhT4wxWP3KeSvQ44pmwTr
rcClOLZF1AVWIXiyvsSbTLcY8cHhjYrJN3/s+9maKzgXALxoJJaLGWjiUDVYW4oLICiJAsSK0SwP
Q//q29knqZJkoyWk6IcxkkCFA3UDX2j0PgtkpwxffKYja1Zf2PP3JCu+qS4/5PzzUXFDSSdX2yLx
DscOLCWkkM8yezCMqiMqcffOWVcr8QK7mwidNezMZ/TAjchTje2bBMAD21VHFxba4jyzGNtr6M28
Uqn4MWF8WRBMXsMWw0BOvc9P0uZOMJfkgZzhiDwe5Fm1Sw0HCynYE74uCVanp9cEeIFXvytKATp3
8JtT2rfwUyv27WqF+1/u0/JD41fKCSFGMjaj392hv0d1dyc7+e8Hj/R+oBqIG+j2QAih+tPLsboD
7vywHPOiXCPaaUAd5F+WqX0l5p77CLgiADrhf/p9FhgPJkjXDnLDanj3tc493O703a/MPgQxR2zQ
8hoIprIYU2pq4BBT6/ruLJkGGbp90kqzhC2abEMgoN3aCjfT3HcUiHfbH707tYDezgl4x7WkmTIx
kRLaMm2rEneJQkHFcVpoezBxl84Hzt8e0/nQgAdvROxAqkeWGHh9pmxeRme+dw6Cxgb/aG+Rt5gw
uM7jgWMoybfTOgpDCp6eZuALxdFcMXut10YFu8ZMJI3FltEKBxLyZesIlgwPmW3pgcOicmNeEdKL
/x2Nu5CCKgFOvalY2nmS869ad/uELWMM+T7aGL0UWIFnUTmt/sJ4QSk6vGWj6iOk6FulFfoNVbYv
jKTTTvMeQ3bBGrPjIBMupjlwJ/b/ntdP2gn2jfqTz6BVmUZB3/ej52vsN7CWz+6SRRLqmDJF1HDu
s+LeDD/Gphmrz3q2RIbY9pm8q0Ovj7OHQhwSzXiwxRkXrPLC20Qb3XCSSNd1XY0k+W0fTY0KwOsY
fAj7Vh4PLRfKvJ/a3AjOm3y6YRYYsE3IShk4nfDHkr689EvI7YT+51x6QqkyAZ36z9B3fWVJEX0p
SGVbuYyqENlrEXjtICeKaPlXWQa4y0xcGnD1MSr40DSmVmlSAKc8GrI084EWJNfZhCEQGHDIZ4Oe
Azhok0URH4ql7KGMZzIVB+KL9hjzga0F/LI7sve+6y4T4G9HyqYbeDbeH9xDjkKP3U/HBTOBR/Sy
/7mIcyruXA1l7Ikpo9ybuFTR43W8fm+v8qkl7xkE4m6cUwWtBdf3MVbLcQGHhLNyADTbaxQPtHBh
IAtPd2NU9Y9BSl+ff4xdu00vmrQEIcE32zvyutvG0/135XLS+tEeTJ37mEiC5Duz4qAjTb7waecE
Uc7F8xxlxbS+WjZT/jxK+JjJWLkNfzy6RM9PxjgQrvFTjIgoWqL/KtdrE5A3/rW32qhTADL4S2PH
ha/A8wYkt3Q3psZbmr9scTB9e9LQCV+Zsx/aBOf+cSODFfq+lU2OfpR9wU1YvP0xaDYs7NxozcrE
wB/UIUDM6+xUMUaPjzQQdDOrBenkgAHHHkSn4b4QxaL+frvVEaRibE1dDjIkPcxlWcZ4xTI6rkWJ
pzpItkvryIS5Uq+KyNQn1Cfed5aEeXzJu3h4qnGo20BtQlyvoJpmrAQXr/wPAbi6Ht3GcpEpCJn0
e2kDf9iH10a1R5qCvIsXCVXhtmYVseitSf5ySKbLlliBXMX3ENy1clFlrpOtxT/8RHo6aM7DPSe3
x1DntFjcTwiw+aHmVWWhcn2TE/wQsG2WJ1ezpkrwveIt9icFPtv2tfw5v+Z/TtWWaJ8hO6vz304R
h9sI4IRhCVbtbJ5jQpjyyuzKB0b9E3ETavjpbXLBNNH4JSl0CPFyRlWgVpz3ddy5O8s/aWlArqrp
quMT67UZKHt5UJrHBRXwA6r0wtMWmf9L9dWEStsmRJV5lmL36PeAbPwGvNV7QU084IVEEApG3nGz
M4b140ncfHRdZ092Rm72k3ZW9fFPtIZ6ckorFe6je6zgiG1gIrGNfjIfJXUH+obT0VSc/JXKxxqL
/9YDXoVhgKuLWG578lYnLhl4tyuTm2yy0CA6O9JoBiYDAIKG1R1G/KuRH1JgzXT9GAkEjmd+IJqB
j9h5wza1mtixUBuairOtASdU4qUDBvnpy5pqtFm3s30FyuG87viJL95OOa+TE5YFa8YO0sYLwX95
pfjx87BgslGQeSQgKnNEi5k7pb7Di5giYrhw5LWwEVI6246rxgmCaKeFX5n2QU4IG1XEpROD2pTr
bOYhXe2gzlK06qeqKyF8aW9g//LXTzxDYm7gLwlrfxcTIpKZ9W8w+86wy9Wk53vkPFs5ambxrIWk
jvswhB6VHVsK83gsuykM4YVzGPppQRs0/yAvKm4InGsNdxiFZiSDzIhhy+DDzqEL8dIrheXoCOw4
yGWzSvEcmqZH0i/ZTiwBxvmd+RGPbKSfNn4bjAn+0OD5bczHMmbcsVC6N8Ts2Q7nnOKZuB7PYm+J
lHrzmHt0GdNIsMkgjR+iWW35kl8rOSYub9wgb3IUUMx7tvLOTTLSilirWRlwMuKCDCm72naWOqY5
bMwxyp7Y7UZNH960h6yW751vrAltjFdgbhpANv9YiMZObUAj8PvkaPeLvIRaGeltHZL0T7EU3EuD
dlWiNP9rpuFS5V3gVMnUVpJnU+ac/fGa5EsDJJmUvWvbDlo9fyN94WdH0WqTn/y5iPqSbGSmr8YZ
c1Kafsb4oY1zCZYNl9nkbHdcxVHetfqKSAZ59VT9C+Pxw60gdlBf8SafGdnU5BWDqD/AMHGdApAq
5f78FsoHVQweIvhU3xIlR20CQgzxhH6EupYjIoCl9se6ptpO9lEqRNlIvs0RK4GQqzYZOwlMvGhg
DZde8kM45EbhpT0Qvlo3YCUA8lCFazXEut0CZkkSCEcX5VfE9xeNmEhOK7zLT0b15PTo1XouZUZ6
AQ7xRvlUBaQs/s72fM5qUxhD7KskU5JqCmUoXRfxwAw5F21CR9l6/nEtgBPol3uh2u69EgF9gi5c
FF9mmW15Qk+1vShjfBMt8r564iUiVnqF+FmloYZDAYnKwC3tp/dwAE4BXzDeVrzJvRgxV0Is5HX8
pPnCRmgxJ5oKjzupKypwNpbPo/KCQ2K3k1qFTas0gIMy9MeoGCL3AePcvVKuP0Ihev4C0uL4w5nr
fbvSPx6USEftbAtG+htxDJxKL1BFfTSSqubekSeH/k/zuQK/yEpvNOMJQ3FGf0O70Yfnu3OI2pww
BJCCa/s6v0vCqXW21aWzqXfq6RaO3ZWDkvE1knQLEOrhBsPLB/N96O7O3pkBXkUCN48WJ4mMiPZF
9/szNY3A5rOgzQ+gn+pqw9a0NXfG+iOFZTWTQ5R43R3T7qtq/s0q+Aju2kM1oe1URr8EKCZfKGrE
TA/rZ9TAyhEJbe/bgkYz0vTqelF6gSBLB33Mm/tKRqBmgWN43vlt0Nek+1t/V6m/tNX+kfFIufZK
UYwOiJ+ZhlmViHtdxafOAP3NYRFohG2nrsdPaQ8MUdzw8TJdC1j58H7wwp+enxmXVVM/QmrvqPFb
MbJPK4rx2RBCseLYcwfjBDr1Sj4OVUIqVznl4UvnAikzwAjEd+ebKmHbM1MdP21AlSPXWr9vKbnu
rQLoa8wmttL/wfWieMcvsbsaAJgZnVS5gLBkvXOtcF5/ThZ/LsTXie42PjbyiVrLGiKGMGP0LK8f
+K2SaoWL+swTGCaGvASmUuhfpDLu7DCpYcSvwIE5wSTsfcCuDrcCaX+npi9eXf7nnUts/e10gQq7
LSrJwirbt6A0UsBJkiayG5ju93L0Xd3hQ1hknOWLcg0AW5NKq3HdjNe6Vd3VmGebY5HVtvYoJYD+
52Ya3smIvkxAy9vhy5HTFdo4cyUB+KH1opTSb8+11vtYnYEJJi5Nh64lL8fzDY9k3a0njv+vin5R
dpLouLPjdN0PZpNICWf8S1GKJgONvOlver16wAi2V2W0/7+fyRqdADnB04dAuzvySvqPM/PoeilR
Xd/hU/DwUgI3VqRlwkhh6kCohUUUxyzw6q+hO4JgSzDJ8CcDXFTm0ZB4rkVqkMwPTySV5WxbmZiE
QyUuJmqJvuSsYsw+n/I0mz0TLweKcspYVRw3ZI5/HieFzXxiGlQt8okTyX4Tp0uHp68/lErl1GUH
1TFeYuFfoUNycyLMg6Q7OgtBaAUvLZOTTbDUWkL4dMi2gj9XHEyDT2qGHepYJOhZSi+chahWPzaV
KCcmKHMWBGzPTV5BgX0whnl0mBcnI9o3gA4U9m6KvmD7IkwOwM82zg6jHqXyAOMWVXtwIG/iuhH6
JdxSHxqQuzA8exz/w8ypfP7wX2qdBcDo13hanjeOSp3t5MZf+OXlObDinE0jX9hU2MzuQAr0v9/g
7r2Vh/b/UduSQXTBQkYHKV7Rhu4dTws74xki7D30HTMUpcW8QPjtDhQyvCFpQr1iD4sc8ClHP0ms
OTKAmczFmCfaj7jrW4kAkZzhpWW5LVM1rrbVMFL/WsDcOtGLSXbfHAGpHphEHYKrPDgzqfFi9gQB
b5wQOifx2BqKaGwD9aGyyD2hU0Eg7hvww/m4BKc2QW1FJ5rz7LEkMppVRbirrBSQYF/PgYwMG+io
ZDq5fy25YHivGeyEO3e2i1km1Rprpk817Q8kFqtIzQkqlian8VoaCKVYIJvqet3MSFhIY16q6Y0s
oibtwyAulJB/tYbFd1ww4rObDc98bS0zqHjE4YOs0/rkuMClMINSplRoHEMs0ElMSaC7H9OFTyML
EvKnV6EA08MnSCV+GBJRGluQ4LKiaP/+Na1IbDXZBobWHh3FpRtQrcKXvq1gZJ20i63TQflauJhy
DpVsI2vSiryA9IUqwevkaOqdSdG60BxnW+E+QgiAtmXsVKyrtszL0+hmUGa0LJEwGYLsfA2o1kTV
/QOIq0mTdbBliYMTx4DIDrzzzyrMYRwfMDUZ9+ia31TM6j5pUzYA9coy0b8cU4UTORROTx9LYpKx
CJQxHZozWkSKKumOkdPi3+gcDp3kKu8ObVy2FrbaqlfyXcgrhs7jmzeREbC9zB7+jGh9Zag7qYoM
TlMuN1/Liuw+lmL5elX12gh7oDOFwRZ57oviHtsHVNC1t722Q7m0fduY+4shVcDGiK093FqmHj03
nNzIE6dgTxAUM89BVPeD5V1Z7Ltr4RmC6zQ3Dv1uH2XYhYizPnMaozk+jN84GKwoId5RJ0HUHjVf
aHMAgHvr6IYdNX1csrHT+h3VswrEULAIdSBoCYffzCyrLCqAhWaN1Gw7mR0NWHClKf2JXJb+nahi
eJLIVpanAF7wrPuIOoLuFWXSHcs7Kj55yAUvg6V3ic1KdAoGJ/wUpbAeHesxv2ZMAJr9bizqMEq5
noO6gaBpHS0KwS1z/zt5W2hezdRiw8qNnnSEiYXKIxy9Ur4jzvwFH8HCKRssCQVWHzEIfDZrgGQP
pl2ZLg8+cR01IETt6ivVK5ONeDWX4CJCg3IFI4BDVJFQ+NwwPdA0WFddx12pMAzoH1eTOLPi8ak3
YvxhoKjsmLHQ52H/NBFxJ1GlcG56SIkg6UB0j0FhffP1+IX6falW/Lh1ninPPC672cSpOxvC/9r5
zULwnQKyYtmB8JuaN4ORuSp0fkZJCX7Q+CSogKZbTklZ0voJyV7eDIf4+DfHdX4sy1Gg3/nUf2hv
hF8/a2ZuG0EmVwX6iX0ir2vVhtt9FgwFvmfPa20BEqsDNGOgCPu905v6JQ6fxSwy0AP01X2Zofym
iQKDt/Yo+OcVBRhuS8ZwX9X1q/urI6cbxXVggE8gFGEuUQTbbUryhnhYepBXLbIvVhw7R6TNCZ7p
nrre2nqCzFqsODPgKCzCy0koFq3r9JaVEHHgAA+dePGcs9Zan1sC5Kw15QBc/ISIhAfZslcJnyZa
Am/WV4Nh7cU0i8fp+UHuFvbAkDwpx9B2IPcXBTQxfhgjNF9CPrQJIZvdPvYmyW9Y+IiP8yhHh16Z
315pNWTcGnKpYNVWhmfijlULvy3SnFvrlnRQCC0DmHOimLvC5RNgp6Xep4hJSTq592uNIxaCV/6q
N+vPH6QO/IBeBZWDY5bgVPxkNBTNLA+p8TLT32BAOB7ZCvmMcvZAognpBnkp4hgG+vVvPyKDZ+/V
TeV52r/A8PH9fYF5sj7RQVej+jfVi8n71qulhuDLJH4ZJ/LBKq+qlhe5o6Re/T5ogq0WN6Lzinq6
YvP8Qiv9uzDt0YjI2879pzTCWVVk2RQYoXo8eeZRwKoKIjJPuXQPKBeNM037il3SNlH+DwSxhBKr
BHt+plxi7vq+stfdtyu8MtR2EG0JeXBC+f3H17DAdohhHQ97pC8Fq3VblFwvlNECSLvFR0IaBM++
RCVbkzVWxmzEqoIHxlevTZfFE52gxWjIJdIbeJP4dn8NDlJxclsmDXk65oLM5sDsi4VKfPDHJRfD
ErSByBqLMh7MyayMHk5aYxO1DK5LP8koVdWaauapopMEo+D7qN53+Dp8KLExp+0tS02GSLZckoZx
RWe1dCP8I8ZYGPwuGzl5oogplb/qqhKb/qyr+ugVTIl1IgLdXV5cH6Lv55XH+hen4RRKrMpWiD/I
hR37wXbb8Ty+ZRxbEtWjX5vx/+kNbXOSdbv1DxkT+gxmVKuzN9e84cBT0RhPuOJ6zZi/umOk99Q7
HieFaO1qkkWiTvWzPB5Hf25DVvxKJI1Z68FQyQKH2N19yidA6BJJYPkz6zn7uLt9DZLxu++7QbwL
yd/wYe/OU3HWAdi2+5TyrH87W5Pu2aM3GGW1y65QbmFw00Ac1IlOBl/hZjN8XyJ4IC0dDqHNFeM2
X6YMjO5HYTWnun/E2YTRv7yn4QKFbW81CB1SYbqK5d2Chgkn+RjBmbWjvkD3k6LWKtYZVQBsSfV8
QJShj2ROOgblEhYoi05aoqBoqsoaZmDdhdhuzJdSwApZz3n3/IA0iSZPIN1aokAc9JFwMtHye6ed
ighClnoT5vzFlnszhJ/zMe13YScfCWlm79tM9wef2nHKquA96zkzc/S7cJezGANitYf8B4RfRk+U
xF2Vl7d5Mx2t2UGv2rYsHfp78wHx2V79Q2RV3pU3NrWtQboZmOq1R1BNYb49oPZdBAt2KNKgtJ1R
mj2sEn/6puyiVq/S0s4wV2v4ztDxzFaUo8JdAqh7GfQuNv7leF0tcIfVRLOA5C+gS76KOBsk868Z
802/GuGI0BvxEFmDobaZpBF50ghwHfFV461TLdbEkIkMRwK60qDCQOD5Hzkps0KODnp2aLJsI/LK
h4IIllpBvFSL0fVpwK8N0y4Eb9b3Z/XCBdOfvHUH90Qj+3CQhM90AXU10tW9Mv+HYm0/h+d4fqwj
QvazEP8J1ZVkYhqS3fSDgiTUDr8ArhZJW/82l9bQeqmv3A27yntF1ev0HEy55AI8x59HNPxgG1kQ
Caawb8Hyp0rBRoCFlZua1AlHyzmwIz4RffC2gjalDkcyYHLiDRePKBPGRJmtXPsamiAIilYoBLCu
dsI9QoamByvAzgDvZI3/fzioNFIFZIZhOtlrRd3HqlsDm0/w/xWVDyNWMLHQDLlWxvW7KZCLAf3y
DMIej3IbFtgvj8FEYRR657kOL/k0jrjLqgmf1RcUN8SuLTgDW2bB5P10gR++j6F57crnSEZbv940
C1WYSyrgyPfrHCZalnvSStHDKL8YkyMF6un8OLHwDh4WStvj7/N2kZUvMEJmEtEUPl2k9Zo/N3oK
o92ztDSqfaXDn6pW/f56ULdTGXabAgdrx8SqvFspKHSaHYitxXmZzgUdpUrU7v8AMplgxZlyqrBy
AvRzryC2505XOYyMQWv1qOzlI4YLIa41x++uGkHTbMy72cqY2g4wE/qPvyf5Ivy9A2Hez9Lf9naQ
vGM09/Uh2/OeQMosXrRZtuU0M9k7BkdrArP5YmJHzdLnU6uYUn/b2B32pqCZlGWNHGgYqXX9Xh9F
oWWJ1FednV/OQV/WGYqPqhBWB0uTdrhFip0viFL5DPO3MbTELw/gLdaJJ9jjHzRG/GaelV+Slshd
A9Ci7USKe+16o6LIhORlO14YcUviL8BI79I+zGaSsS90U4n9Ec3cxAuncJih2zCDk9MJW9sdKs1+
rJlrFNCbmGwjdP24g7YY++hJEUUVKGCjr0tNx90DuFn2+XYo9OCtZoCKntYPvAPq0f0hm9NRwOao
bt4gFHYMHBdUvyBivtXvj6H/NVi31iehCT8rwT6aRBStK+/1EMox170VjkDgVlQAk4BqcWmppXPT
HpAhXP5DjGgUbmA3W9lelnnntfk9R/GW3cO+iOprf2kw+Cr28C3+1rDYr23+lYNItuG6WlI39sY0
lOYcWLXZzWDeDV4h+SRLpJm7o/BNXMvzb3jD0qaaK1iSdC4TMwQShrRETJjJVcJu2v22c4sIwecW
WYLPS1P/LUhJDEuNbb2QtHCQaX1dvey3rdhTmPZwwax+D8gS0JXB77Ikpg2NWh6PKpAlUQBXwDcL
1AAxUSocFBJ2+RE08SLoUmuRacx7G8uoi68x5Ur+mA38KrJHRfEcIctf/DkxUfHvkcEVumuXZAbR
B2RT1GEfsuWHcgb6V7MlPypuVNPIt9KSTp/R2WEDn4PPfsiwmhbX7tZ6n05YcMG6P7fsgAAGgezF
DQ1cH4B3osx436FLLMLNhxOqlQ3gXahJTwbv8D1BW4V5UKjJTBtAEFbVHO/zN8LBscpR2fNa/gku
8TKw6xEGKgUEwj3AnSIFhQI4plmGsEfwEbf9n1jYhew63pENXY+jhZyIlY3aST626RNi0TPAh/pQ
tIMBgR31DZRQ/EEXcoJBij7sQBULFed7n3AU0B4h0ZyyedoxAwrl6GLIG1E7N2scC0QeJz47txFp
3yicpK6iGbGuj99PLVcJLBlBGikNw0KZTiTtqKSQ2odqY5w58tFkXOo8/r3mk+LH3LaBkKNd+QlS
IHcuvkp5EDdM9blXQaX2H2g6HO8Z8WJzVXTPqcahI8jnnZgQFD+JC1bqbOju1antinjjMPvzu9WL
6knnvioX46YAaucVxsn3OT0wQuT6WrxJCyQ0jiqXhKmJ2xBDW25UrCx8sfNVWr1zMYZ+RWY31Drd
HnBQPN/TCG+JoRf2vcdUSCd9qVog4IZz2gj44x+TqelMqYluQYt8JoHD9IyB/MhoN/YMd2n6Vmoq
X7Wnq/jiTS+rTB98wV5sqDoAD35kC6aqWxv2j65sqVwExqoGybsfopwkgRLi8PCxXVfUSxHJmHFt
0DQyfWx9Alo9NyGL/jPU5OBjgmnD2lroypVa/HKFX28Y+/ffJaGgj7Ff27hZYFN05qsPCMHp6EDo
sR6Ue7b7Fi3Zsr4ztqNfUUCpJPZCme2WlvsMwEa/DeAi6zBUp5LeMOAzfxsa3KP3o+3uQRwJWqiI
EN/7bw41Sf8KedkSs9gyj0xLlbcet/bMTgkRxKy446wZDAne+JxKnM7RNKmFdTnBtW7OC2EvS5mh
S001Y7/3HzwGDCocWAVwmliyLGCefmYyBu5HoxnGrb8EJf3TYkb4vNsLHMM6GrYSlDYkd7tqu/nE
qm6q/Z/1lYGYb3sVA5vd7TRRUX+6ovNfjEjoibbvLfZLwUHHU5SDhOguHCEX1q0eSqbvxZbAiasn
biGmwjAVzRnEilJ4SHYfmukWHLkQO92NekdVvM4LUkPOTb68Ml+22WdKnN+P2WbjFkM7jxedhySy
g8TvkWWHVlMLc+PkB24qbJq0hIoGLVgWPspdJGGnYl1DMSQ3QzcBVefCQgh2PWGH2qzt4CnKPyiD
maiW+mzleM4HtizNw/RYeI5HUQzuGSs0aAt+X29b/kQJcQ1mIE13Fwy8oChgbM2fBjn2baYkm/fv
tpqLucGN0FOL0hXYJ5KUT/paeIofTcmqC3OBbDmIilg3f6rLojB/KfoGey//lY84WYtlxaC6DgHN
KUGTmo4m7Us/OKpaNXf1NwuFLbAwBtNMUWoJ34HFlcVH9cYgIRxjCc4ydQlvMPkHRLOtHj7InVca
j4JgRwBMHCIg3CHIgSPok1Iw289AkbuXqvtqlfv2dP1G4wEgqjgE9EzvLRcn/qYtZuRq2sEWkRqB
NivN2Ja/A3vV89kRHuDlLvDc+MQimnlcJuj6nHx3i9VRvvMWpRRJUIWJiHK4iNZ26EsktbW8OcKV
BuT+4vnaIJMUCZRFldnzvULAeweaTQt9qATiIYwLqKWiG0SgXmECrcQXuIzwmnbluhzNGqqXDOqy
v5oinfxPRWsGZjOfM+8kKs8chit/yW2ykg6yZyGWK2bjxGO5b0cv5r926o0w+++MA1G/LAw3oNLs
/h5xRhI4h33wN2h2uofoavAq4GrMRvJnQO/wpM3Ugv9qnVk+JLAh6Dl3F32XX3uDCkh7smgURGnS
/iAN7FoXzSh57yY6lCFnAIpPdV4ZYXLmUBteWwrvc9M5Z0AqQU4UYAMU/ngy+OpkcvmKnvVQ7TNs
N9a1JxCQvbjXmJ9Odpb+NAc32LvQCgcnTYRpTzWhGfwCG6sVyp323iePs1dZb/KU1ffLBho4s/K+
QhglvnnoYQDKX6o/N+iYe6HJoO/drYX70MlGyFR2pDkKt1XKxhD9QMUmQt9eheaFW9JBq/JYLj16
+DQNmWViNBzwPn/eTR/cPxl0+PL5hpTXzIIZx7f4Wuu5jtWuBv4i1ELcLMzWp2Ci1fNCUqsOv5hV
0RwfyMYn5NA1FlAORHxH/FAuZUp38VCsfwdRJ7vfDmP340xucZ+3ZzfjzQkG1R5M4+xSe+yKCs1C
civYNFcVP0vRG0PtxZ37irozgxEFNe9+VzOD0Sc9V/EEOZg4qHmdxbLI4oAL/N95r6XT8WhsD44F
t2mahp04k/pAF/IJ618QVcVFdhYXQQN3aem6z1J3Agxk7wMDEISm0z7aJ9L0xYvUuk/q71bL9Yi1
suf/Ju0brxY4FyecqHtS+bxw9erjZ/9OKXnLSx0HzHDLYc/9mKiRLcyAPPm91yYWnQ46h1k1A1tW
/E4g8Jukfqnxu3heBtq4ldeUtX/Si1TVDbP3S25ca/3XX57kPAjVKXO5sbHmsGWDuessn/v3n9wn
aP96sjEOLqRUduDDl58wrgfnQKlP0Rfv7d3c1EVNUsBCOc1f6OFhmv5xrlPkwDV2772OnDFyM0pw
A8rbDobDVrc1bWKcI/6Pe/ubaT267KGREQxufe062cARurAmHXw9NLm7fJh+H2DGfBOKEDLOXJEx
djaKtfrjGWONulMIHRXUFuhhiz+0eNN1kgJkX8goIEX4f1tPW5gqUGhA4TwRiy8yrxwJcoIR7BKN
Vn03/cxTYhMqDzDMEFrx4t9mt4kmMhXQ+lmmOgQhNq+kf8oSRJ3oxBGmFn2tVuZZDIpCcdQss/QV
xRx6C1BYqPedrIgPHrgof69WEvQ9/lUykhsJ4kfh0Fe7UVpGQIAQCT+bJ8NnOTaXi1HprhlgRetR
DTyhOmapWR+jm7OUhF0O2vpY5xZi/RYp2Eo7Cu4P+clEtlo7wl8AB3KVdXOZnhleq/2PTzCKTlbx
055gjqI7rxS/ApGLy6V8Ut5MfTh8lTsyRujzEA/llUida0Ew54lV3t/tFs/ZyXvlaKVXmBInO4M2
xTwIwyrMxHNs4t1KKwtz0Z/L7DZ46BX1SFMvcTC5nzOa36R8smZZeCbLr/rnGxXMkr/Wb5n3I8+v
JVvc/3oqChfaTj+LaTS0se7c6CkkRoSBmGBOULNE5KU0tfCi75jAv79lscIh5dXIu9Jfqnl2RdZQ
iPZVDFMj0YblX47wKp8fH1M+PI8FsRhSBwfsNUkg40KoTMqZYPWLokodmPwKx9NISXnuriGSj3pF
74pcyQmCTdruns0iD/s4y4juBoKuPi6BPQrpDTN8BYwzLAcSqHeA1qykkSeXtS3vQhYJl1IOiVDj
T42+Ln3EPPAevCrGbVY4P9kuGq7Dp7cM29y7pc8byzi6fw9kYbidH3IZYfYxGouinUy/wGL0Zxzd
Yna3DrhhqawHHX56Uw2x6NE12kWTWmIyeNwjHsJfDSzbPgsLhQGiN2OMHPMX8qdHDcWYqgCbvZzu
vR6EdSB22IyYkWWOjP/U7mZcEi+eUDafZO9j1Z2bhCiS7HYYRcNHzKcMNNqPaYUKDSDKWErWMyfc
yc+vopCbYlEm4VAJ2p2Fjxg7RtSuUz9/a6bx2UyO4khkrKiZyRg0UEVR19NdIay7DoLx4m+pQ225
XrUh5lkApMFU07G5ohgi5ZFZx25rr1HtoMaQSP8gWri2h7Su/ZBTvuNKk4whJgYMs1Ob9eO0PDLB
4iA7hDjDMb9rqQC3m3HwMKTnFvZtmw61haT+Gj8jsA3YbxW99n+1PnLLeUdY/FaDj23e3Be0RR32
ReeDzhQnLyk07UBB8b0qECKLMbb1xV9dwbRTbz3f8dGZ0cnFnAu2Wuk+we6dpItYYxu6kTwMIvrI
6LNRNRVnqBqChg7wzqMzhUMD0Ize6gFNluSXahyR+ZCN9byPw1djWtTo/d9bd+eduboBTPGgeWIZ
nlzAixycNPxIhDir4XWLvhTgxsaCHOiYRdZZradkls6rGKwtbjn8II5oNTiv39JNHzxB7xdfwnGa
4emk+ppkUzZUHlfx6efArrpl/KYKSwXNdUtuxVzow3k2iwve8aH6jJIcDtRVKO5UrmpIrnFkl6Je
FScPvQ5YsS2jjXAPrrAAijGd005WiQssDAY/jiA4/2xTypGEakfn1GoRYH7cuSbkrxuU9wgAdIEX
evABQDx6Wsk47+1TaehV7/vEJKR0KR5iEywW5iIhLVoa5Q/onDX531/JQvBG+WzJBd10HVyG0f1r
WGYikBX9vVZWpC1TZiRSTrdayZVusGCx+JH/JnYi/N12NatYfPOxhaTM/2FSoSzNhpymzA11A3LW
KGjGvSALNSCgCh1NmbOOcw0m6KHjoR4rLkPaj5FRxEgFjYo6D181zjPvOPApfm53rBABnjElaNFc
YNbLJZ/6cDT5/O88oCTUtkXd/TMHN2juf/X0J7JDyqcUrPz8TknIaQgFhCyEby8jj+t7Q9koa3Vl
3/WN/GmGQZXMwho4adZvrT/zJ6J69rn0pLgYFJlP9MPsK9d5OfDTO2E1IA1EX1aqbmy606wZdNrk
TBZ91vCZhNE/F3+ge7Kc0KQZGJYZM057zxo/aRzLwobC0823JRHikJoOLhvo0rX5OLKeB/mcN0Y2
ocL63lt1KReq1JzVlnOLvsfFy6MaH5loeNvSIFQgcj9Up0K4fCrGQn0q6kCH3HFaw2J0s0sZc6Lp
2D/uDm1tIAS2whMG3APKTMPZsl/xQOpw5j+QoEYI2icSRBCBjIOqGHyeXtjr4cxcePnLICpt7xdR
AoYHSL7gM0awrdSKbUCo65Pv7pAefnEuxOaDadONf6/3Z2g1BZm4wetnY7aUvEpd4UEu/gtXZjns
uiPXeY+9g4kpaAgCQRJAk/M3aPKk6hkXLbU3TI5+cnrZ+XjpQ5wfsOcKICm5HWJOMkwhaImenRfd
d4/4gkUdn+2I28kHp4KynWfztA9mKNiIvm2zOtpFf0X7XU5OgIQ/qR257ulZEOZv39S65cGH70OI
ZJ4zA0D5qUZa9S2Ox+cF+L9ZoP42Pq3aROVk3sFz95hbInZfuaMEwF4qaexXALWBznAHHXyrr5D/
Q+TfZw3xwV71zA717/+4/q4J9uPQAax99UUfcoq0lSOwbGZAx+sukmXrzs6CvkT5DwU2PjrDmAq2
7x8xhpf6oM9FMzwb6+X4knDTIxMhoTA9eNRx1Oex8V8HCt8LZ4XDcv/580emImuw15Zv99A3TIKJ
eGmnXW15gujF/JY9sNcrpG4GToNjmQ97RmfnQi698L4tNiRWoGJKHtLNWuvJakvheI7s5UJveLwh
qMHzFpyjLDKzyp2Ag+14Rlt7YLOQ8MHqgjimmXy3cWoKVyYwndFA5QaA3hWL+/rbRo90VN6eLyTu
pKMECWLGEif+kwqfNIoyk4nT2MT9TVb9qwdvL5eh4j0pJmRcsYScXXUNcG1Q/D26X/LtQHfM4oc9
/7Oa+OJjZhcehdSgQVOOwqxlQcn+N3jegf/c8oZcKpFT8Ee+dXRsZ6QbduChwPtaRpMZPmSEvVCu
GWOMesKbd/gkQAmFWaZAIySiFjFK0Sx7d0BfrFS0C7/vfBwS2KcwvHGlGxvwGeOi8g/+EmBflBz+
huxyF2lS3vYYTO3N+++maG3p32ycH1ruc4x+g2isERrB9F/tChfb2QFpf18RNux8DJtidzde2Y4h
8ml+OG4wEpn5jn0nZKfvmmwjevpsaWMh0B0W5CONVTTwt9bZuZHlyGaBmHCKTetp1WBeRzuexCeO
IGV16wZaFcvHB0p9tVrGuL7R1R4IYjpgJFdWK3XuHPSF8LPscObuRejF8V9mFUw3YkoGRQDsxkq7
7U1mv+Qj3dLo5U1YF11FuKAo+XH3dDDomHW51ALhwwKLo96n5Begiw9XGjKkZTEetav0Hs6wszsI
SyXRsqM51FM/fokdjcO35BCXu7dPIySTmiIkqNPbQU3BKIAJkaamjxLJm34/cp1ENIktpE6QXj0E
D4244MMWJ2yFgZkpctHIj940sxn25BLEheZ6+i8hgdh26bITiJN4W2VxDyiFGKSs2es3WQle0VBZ
n0hL28r/dDJaB9xsEC7l3Mvh0HO2RgaYww2rvVJF0MlY12IaiZNn7OmWj+5cLqULm9Cr1Mp2pmSo
OiG2jveDRzjp6mcxTo9mlVrpcj/NkUuMaRzXx8hhrK3Ss/uip4m6IGQNmkLgDpDdJzl4/zwKoDX3
tcFle2B895uD54hdoMY5KzS9jO+fOg87UErFtDJSosegiAD70YFoNecnjEDzeyCOjkQBxzTArNt8
pr3Y+zkdE6AnLpdoStayPKVmgMQDOTEY4DeIP5aNffAojYfEp93XfiC0UetiSrg38s6x8YrlpJat
lR39aYnisrprs/HVdujCr5ay6LcXFPepwKVZqIJmGjke2eeQqeSKXbGgMW7JSXz1uCW++80REBaM
sN547r5p8s1RHPWb11iHlRjUoLmJ1KCCN1f5CaHo2Ba8vd5gN9wl06r7Vz3Sljc0x1ArPuE463Yr
z9WsYg8vbRQvVJeDT+skSWPEGIK/8heqrN6j0piywbx+yRMKzUmFjfJgLmG8Hr+XR+0o97Qhd8na
9j4MivBiJLe4nJcVGIeL/x5H5+mQlGRj6zrRK7b60WdY4WZdRSz0zJ5s/Q0zHmCwJMCAkiSkL41+
KCDQ4uRrjFUEvQ6EOyUabaial2MACado31zJ7RiGsnhvpSgMQgvGfhNfVfNiF+Duc7Xy9vxs9c8K
LH+rVMivk++dj9y20ulDANYKtWGmZBUwYHQ8O92C4MWpPiku+HNX6K79EL+XGEnAmMSdjTe0b9T0
KJg4Hq+ZrY6iJ9suWld3BxhnEN31iE7xugqRto2L9466QCztHVxfOH47ulgOz/VD4GGzDCMY1zda
9C7hbilJAkZKmyFJPI37sItRjwOnuxu3iiWgKtrgUXn+y8U/QeuMmpZVpXtcN84JZgoIyfL8YhcR
rRjfPJa4x2AkYmXhx2u5tHt/JMduZHqg7vSbLCnKB5xkk5MoLgxQMZRT0nlX/cHGB53BQAJ2CWPL
ONCQwNTbSm2mPjTqgf++C+uFItmgM13USbF5iZ5Mhnwkbc8xN+a0RBOmIC5+7RVUtsHahef72GOk
t+X2/4QcdqCJ4RL+bz1n0En8NBS12AFegsZm86pr2hFFdt1oI1JRvDNTGBJDbCslWNoBx/dpaau0
rXr3VfIBmkNSmVFRsp1aU9shAaZm5aS6AhKYC5lUjErovlDdXa0DfY1HN0OlD4TOz25xRyzXt+5H
XXHzzPtz63h44eJSZPQu/vQQAfeHPOV0GqspF1V17CJb28Cvtx9wJsrFQlnKR/+8ZCa96IX4dzr+
/BhPa7axcSaI/wrgvnUD9gNprkGWymQzeQj2BZ1yESQP8Mualw3Fow7w+IZFydPrCqRbQkiDPJ45
cTD5qbDNSV5K2gfRPghNukOpFZLfl5IDAia6cfeJqR/bZ0UaEwiTg7ZQGhkpaSSb2dSds9pNwTcK
bH4u840Sm6fz2TAADW7ZHgSHIYvxTiPo7Lklh73WI9u4lJJhKxeKWLolW9G11iEdTMw1FTSXNlGs
zhLRc+9lv8SDJB9XhREiWOw/ALLTx1Dvv1BcuNEsExDQ/3dB6Sqbz6XveIaubrW4U7FycWJmfgim
ezh/gAv+KrBPI+7P0xltqzZiKaNNbpyH/7b42ffbd2Lr/qXCCiEYdjXHHpG7NOiobfNE7Iz2OoRi
kNz3b6hBjBvyH9ybwi5iMbe5egT7hxWfzvPoS4qJJlNPBgWAo9lh/Qg44dq2MYNYrYLiPuzguA+y
ZVPztc6soCq18rCfOK+/a0ZWlTB7+zW8NhGhF074/vvVkaXKAYOSsQW1kGRsSSIilk8kHoDgglQ9
7gceL34YVMRABzzDWKfJsYl5E4r3rT6LRD7u5HtZnSmmI3HV8olDH/JmOqrgn6nBYzcz6m3qQxK/
rYWl36omoVXqkWYbu3QyCvzScxNABTKvW+vCFC7xlAKRw9OJN16+16qutG6ClRDqj5ePnwqB47Vu
HmKi2QG7bpgfLEEj5UDNATi1BTehX+ULPN2rlQIB/F+TuSjntfkUGuEPVOSJSpW5Djv6GZ2wxNLR
vuYhGAuDV39K56tnODwSxUyHMHDeucd/TnWGQBOCWQ76f3TjAdxp2d7vHvuQQpZzpwugq0dVMr5i
7a8guYgrRpBZl/ofJy9sYb+cVfulA56JYmRZLwG3XLMuURmlYuEWU01iRypdGz2vvkXVQbmLl8iT
ilqajTFtjNHTM3fGDOzwAc17/KSWedT/yFz12Imu/BpG4wCTxL6po6kiD5/cp3QhVnb8xUDEXoxu
cnmFUji1+XuLa53je0idFUbehro2BbrGDQqZ+wT6+VYPeNnYd14hsG2GqaNKiFqx9OvWCuGK+NyU
LR20s1ndy2ux8wszdz8SF/rLu5LyZbfjoJjVUQmonIRp+hqdW5sGU87t6aTuJ+/wfTfqxd9UTCBT
ThdZw/XsHmL0UHHfyz8O0kiCe7TwiBopxANlkU51g7Gy4Np+beE86Xd++GVcxX+eD7XKMT2S3kMT
8UfcqhCyjRKjl+VeyNg3AvXwYM2Eejg9S7vPSGOvH0aBCUJOABDGuaJDqfZWvAZuboftchCCZtbA
k+/sRSfaqmRJ99qytSArBqZX+Xjh6xB7FuMeoDZ7mwzyhWOuFvODQ9dfR6toiRZby8VdeUp69o1z
caBopxrIzLauLliqRbuYzxhFUXjQ4IEc+OHlaHOIRg7FVxlDM+v9Aq49D+R9A0CnAMPBoMF6JIOM
LOs+qBY+ncUFqxjE2KQScbAVU0dqY4TLC69ZNoyKgwlnN7LUYf+/qWbATFAHtHq3k0Fb9UiCL5go
4g3L+Zu0tSmKIliVTlOqcJxdf34k7noSzUAHiUDYH4gn3BClsGab2UUAv5HxFFXLjUj1vowWHg5w
JkP8thTUZ5lsO5NinER9lE6f/EwaKHzTahR5jhM17QH09J0fofSSgeTPD/qUAw58+mmFZ0rKzAXV
CYMoxu3QF5exvmeHPdUCEU6SeIwaV5y1HUegjs4KEr9IQjO5nLd/42/xwq2LKlQHrp0QyUqr4FgJ
HPauu7DL7YmbvqnJ+DiTvlB+j3EkhhC33OvZi2za3rD5QBBV3bD2Zv29aALiHcsIHm79B3XRfcnQ
0ll/0t/P3zYXOYD13IcEe9ZxawOfIUQOHmLeHQsaL08Y8+nHyanEDUaPEg4q7r4Ox3VhxvTi4yyY
SvUUpCSQ60VJaCFzhZWAwnzQM+0nDMFDr8UnuBR8c22Qgb/sMbCHTGnI7NT3iB+nCKhBCmliAg61
DFPVuqsiasNamhN7BdhMFMewS2sBwHUeVSCkfogQkQ7+9inoCx71Zqur2yGten6bi90CUSbxs3Wl
X0qoMEcziLWip0ZqtC6vKQp7MZ7/0TgefbZCua8yl4hU6lOo3cl9fiHFb3WSuQC0IqrykE8ZNgMx
lwmUs5q9hThyPJLJzmkwdL98naPpl458O7sDnffINfeZB0rNOKQAPGW/OG56fyVvouWbwShjdJRE
WHv+KYKB9RIh6O0ztMengAkR3MmcBSjTLiT1K0v28d6GAMfEKi3xfh32T386tfXI7OKA2DgauiU+
a8BR+WdpylVIpGPcf5l0HgXvl/tF5Q7DIQMEP9jS+1ociEqriCpmblX5BIvhmbThrNv9WQ26TiFB
SuNJI0/cqKjl3FrcTfIEPseHnpy7abGodnfcBZkMTX80wXZZxaQ9uLHpcTutLuUeu1tS381iPjlp
4jrywLua0x+YYJNRmZMqz8l9GJ+5oTdLUzAGQAqKcbXRvgpOPnKQD7N9cqETwaHgUQ0E9odCigAY
wgy8uQSlFAiGE8w04Xjg904jvd3OnDztocrblgCSPzhJg9OWB6YZ0xmWpj8MyXQDnFRDZ4mbh4Rk
9hP5FGlUxgO/tDPueeGRA3C5UMqe4zngUa2i7bxUeLtH4hW6VjPEObM7ua3GvPw3fRK5PCWGsd+D
C6pnIdb41psTs1t0SRzUp9iA7Xc07z4COaNDbQQ2YoakPr3Fr0xGf/cSfpqC54546I7jLxN6qYtL
avnmUGaiz0xYPcxHIJUyYiNIt+f+zKQmOp0lHcrd8Y+A7mhU5+w7nZbRqNdlW3UINkRs/WceZAE/
eYLrCK8m/MSlIeXgB/mu2B/R83VmiNR4aSHs50deGb+vo1sUha8GhwlZ+DjLg6aLENh07nxtoFHB
KTAorVz/bxGDZTvK4FHp9qlkkxhVtSaTb1mtCRmBD5NCsBnoxJsuW98inab2Ew+cZe1wLXpmzfNR
NDKtcyA4WLsmThQoBUGKy/HMGVT2smKhbWpVz6wdCvcKp/Px7EmdCZ74qiMBaSiR28MWolvxyq9b
Vp46MueHkhcGn8H5MfDNooAoS9oMEj1zYCBCuDyZ3SMlQ23cyGm89Hq+vk1zxwfonF1mACSVhRFE
UBWOPtLlp1aXpQJr/O2FEF+Xvs9v4GZ/HCNsYk0ZsriQ23sbeHimSyyC7dB9OQHyNYj/SuN0gsyR
Rw4b5NvLN7kB5mJ9OFXjjBZpedXCyOB3zYyMdyCw2L2Hy6JxZswdV4hTCWo3tyRLb+6g8sICuGJv
WMYlE6gHw4dtTEdEu1brqAqmImJ4xVnGu9iP/7brwPb97uU2bFibNiRhxx+qxQLpLqn39snbGjn6
29+Fqv/7eJLvZyPkPoQrAy9mxRCCV5LvvyJ2Ra0zkH+9W8kICQSyLikfVeRH2UTtRbQzDDFiwqzI
+SQ83j46sbuwtdakZvS6NX8qhSZ1gP6Py/wabIzT8ZK6AzgtDsGlppsq3L0VXD7RqvpN9knW/QNI
3dcYXO3+IU1y9OuFbjrZ8hX4EW+QkuiVy+1fftudg0V+aruEMtLYl7LHEPtHfXC7eZx2aKBcvNpX
JHOAIkO8558+JmTatYMwysNuK1bRGZqceHNItdtwb3UpWkP7OnpEAUbu1/LjbiaHQFTyjVWO/ieA
MImg4//PJzjXhmy2e+f7nU5wq1OQ0qo4w2XJ8dmv5gIRhQxR+XogVZaLQeaCQcy9pkxC/x6x/ISH
hl6dtNBnejTy/zYutEjMUHOEd84UagCIJ93cFPQ5pIB2pfnNY3DWTRLrhKfOJYi4ItIpEwHI4yjN
fE0KjJKSq30lSHZALivYbIT9VqF8ZSwxJcw4WU0XrRi+1S/6KgAiBmS58aI3kYZEJ2bYMMEClvyK
kyxPfmOnQOcqHP4QOLPK0iX6WtSJMQjufzZRQJgavfXB54pLWsaasDVpSgztqyUOmJANrEevjcJJ
cTB+Zn0FW3942BB7NY3cS8MdZOZf5pC19hBmCOKyYNmtQ0jIUOZLjAM1sraTQaOqShWLPNeDEUv0
Nmeyr8hFLA0jSxAHtbzwY8NUz8nPbMdMcWiRCKdxQSKKH6rgM+OFaJMHn5R9NbdIU70VjUfW8WLS
q4c9Djej02jLfCii3i7m53hNR0WD1NEQp8bmhEH4S6O4o0miqvVnS2g0Bf51QiO72uTfH1ujSfyD
yzyFbMRD+2uGyu3wrFo3iH4YF8rtEYArfErr4pYr6g+lv512B1Y70ZFH6GLh3VachpIea9NU4UQB
QqZdL/fTb/t2AWLtKhK40UGUmc6oxmU+63ndp2VAhEFTSBVEMDlahOzYwzMf/RVFRLWGjOnXP6T7
A2/nlN6Zej2bfJI//7JQMm6IenwjJkf0LPYLuQgBiQw55hTTFdj1XC9Dtv98gNtW+y1Wcc5VmHXh
GdvY8QvpaWb5XXPkkd43EOVg3c1xJqhLt1JeTuO39zfif8++eOoVeWDwJSWYOV+6WeT+Oh8sq0zH
8rejSRuz2AISdK2ojLJOaVFyAdo9axsmhbowoLsVySQGtbBqw7seIMlI+HzbpheeGw88DOkGZ4Jj
umjYkBv0RQEVjX+j2xM5BAlADBwAWQBe5OIito8iSR11j5ricQc7NhSbY/fYZdDTTtja3aV6tRbz
Obt9kDljjGUnicduoh66pjzEmJhpu3RwvTkDpoQb2qYPx2F4szU1eybxr7hJ9BHoFS684grrXHOh
kQuazV9zSjgqmwUtKn84qQn0o+56Pr+MVFit71v61SxcnG7tP7b7xD2iok09MKEeayrF6JGWQqbW
eGwOPQggYubuc4rNinvsYDsGMWQ10oSJZC/nB7IonSC1qP2Iry2VOv+jn9lHkEwvkQUFlurpDCmO
N5NlMyBGMFnt7K13ZXI1VtUExKITrx+EpCLT2zCh06gR2PsocUwAc8+oHMKffKuzQ+9erau25CcS
ZKksKwvmaWVYNEJzR4xWMw+r2ZA1LRtwJxovAwccUkssX4mdl4AB+rKVECvig7JK/g33s/XznsQB
TW+iElF/daRr2eUGPA+BB9zit8ryfqHKSjb+slpbP4HUISTFvZBHUTJDviFjp454M5NUksMPBB89
6EVyiJtTJPW6YyIB6gO7/Z/NP6xNVzfitGlaVJh59glHgFHVgHOZBAp73jsv7oCitgzqheRrmxCl
TdfpYfkooJ2uZ8cU8vSKqTYRH3wCpVKZBHph/ibfE0rXdjAYS+ET3dV9Pq73ImbT7MTYoPmoeqS3
A+8rTWhl/NvGM1nMg83bxn9T7kYWgVRTj+rhAmQajDga+CtfFlKXyvfq+oApT4FUkUz1BuihpSIW
hDeuUc9hfcGLTZrK/1hzrYhGfcr9OUn4AKe6LBQJ4sjjEiRvcv07wbvWxAG1VH47tcghge3iO/Su
yCftnVltOzPg88BjKP1ePUwe1ws/aPGP5YUsVfol5LoBfVhBGHgeqIt3xpmxe31KUgNxgp5YjJxS
bE2UWP0LxSYL2n+o6hKOIAfpwdU3ogLm9ZpGl/KF+D3zQsBVfV7aowOVfo7FacG1thViapwud3nv
WkY/wDfF9/wPnJ6kR7JIQ6sDunFHKbJTwC86T9H0n4Wj7JdX9ATieYYD977FLzoV9bh4dJgR+vbb
meEA+w5T+lkSacLx3jHqj9MCd8RfCxA6aZIueTbYUzkH+h75pPe81P5yDdhn2QZf02Da8klqp9ZU
SrYnuVsJCNUjhhjOMmiRSnHY7ZYQDWWOMfDJArUVlK5Lhh5m+P2ZwvhJHnoNnMoZ6s5iuMM/Vu47
mEkefXCfsj95b/Q05xYG1jNdohU4KmoLmokJpSXen3CSNTu4NgO5VGX7ZqoYIGAPj9/gVcvew9kP
jCBbl8SCH0VSsu04a4rpqSWSEQEaxd9Mq/TfggYPx6Bmmj2ibi+r7TKbtItMQNAaBVGeLuzTBsKj
8k9llF4/ilXQEDAzQhG2Y0YVGxfJ8tmXcRlE86aCm0Fm+QdMWtUuf2W/oyIMa4nSJ1PcqsOTPtQY
4KEEFoHZeqWOMo5jMJU49BPzbC/Mx67Cf1BbmzhaNv6eKk1Dlqt+17kRDwONIKn0Q1R5j/uY2NkD
bmG0OPkENlgNQxTeZM4mD9ocwEDS90FepIIRTL6f2hf9l7QY0xL8U1feerYcnEty04v90SZbibmJ
wjN/85NN7haLSZm3TqmCTY4avDhxnoX5AOZPnyhfe8rZKHUHbtBGmHb8zTCdX5ByMWSM/vGBlIbT
zfHPgUAUsFsTf+ppSgTIW6Qbd7+gHDOzUsgwTNH07G4SXzahc7c3MayQGvjpUFrC/JIPaghUVuwU
BnqIIq6/M/Ui+vFJcD8s5W27HbC/4BMVMxryP8b89qaT1rE8wIpbAjQyevCO4hCPRZ/Oat/HZ326
3+vdP6l6h4e/W4j2Dq9knlPciDupUU7kqdUMfqCBooi9DJZDyH6ghJbNnXtIOqiVvP2tggBYqrGY
9uj/IXlxzALFysJc0d8dIp3cbSbGr7TwKYDnP41eDUMZ8TOos7IrsxxMtxDarJNMzLL8l6V5BCWI
ZvV8k2zZ+EA+R7i/UQonvH+1FsL2r8bJzply9zfw0OE/OLF+Sk5sOsMJeCanLSZNke9zhyt2in6j
M6lZypF/AT4NgjfNu4LDct/YtVqfcnSvL1J3W9XWi8oUPA3a5q4K68jGKEr3UXe7a0+nOoUHyZfm
aQIPikOmif641Pp6rfxLYTxuBoNTsNqavLm5UtBITxHwARRWM0tf7cHWLuz8zvmRMVXS3E8UvA1L
sAC13QGkl2Gt+kcwXVaipdtVxgtMLfPMrTgwYRCwlvpdjU0tpooW/EtiQZAaKlqgqdt6h9HSO4Z0
GbyeDKsPjf8ShEP+9jZJlHPUjbZ7nDp6dAKkgMbs2KDhA9qvJiezBWrnC6R49tZ/gtjGcrNcMtuA
ve/QwIuGj6ZUF2CNjeOqaUnEgjT51Zbw4TrIcHyJKkbyfc6wG+QQJ6HQfL6rURcYoZ4x3BtmxdtX
LTYijnyYdFS/GByGogAcg3U/p5cfoU39P4cG++fBqCktBIqOCSoyQlIYi0WiMcrU+1ZX6TXm0PzA
bnYzlYRcHDychXYa6oOXnd/koeSInljNA2fp19Qyl8IJreOMryl3oJYxulmw3GK4WuTjqvRC4xOo
4OEW4F6Iw49L5GJbfMr4lNanz+ut4xTXv2E7yRKwJJntFXxWdiq852goj80aP3GtPA85R6d+OwBM
cAoJLREMcq1qbPmqnp0dmL0qQlfaogKH9hap5C1GzTjTsNNKROqJE/9XKW+qoF3MJ3qAC7nQlElc
CPLAHf8L1EZnaMHLAYT0KjQPYbixLLpvsyGzuAU6DauURXnu52ccEH6KAxagDISHK4X6dZfZ43Jd
mZCVUTC3mCk9cXnL7NqAvVRPLwvtRR6t20Sb3dQc6thqRP/Rbq2A6/VlWcH0GX/JSg4w18BiSFEr
f/QqrdwGu23N2ZzWn2w74W+0LBLj7SZ/f3vykyqxsfdLRr0tXV0djmb8uCvxa+AkxxEgYxLkY4CB
K5QMNYovcWvb1vkbs1W8s6vBrDDTiw/usm0024z3Cy3ft4FiXI4JOW8Vepuq0DSppkc4bspF+dNC
hm9arVJ23ta4mmEhtGXukwWLGcOdTYUxvRVmTYB7UfTfArAnzv6my3SdFynYdPXQbzo93t0hvGd8
B9mI80rqOoPqTMJWxFSE5LICc3AbqY196pF12DBE/XeAyYCSN5rK2XeL43jhuWAhPZLhcgUcOLYP
W3YNc7ZTZhs03as/q8bvJhwjCX5onQ60QLNcXOEdermnWEh0fJp83Vf8l2FXpUnIYPjpqKR8MI3q
EVJ3jWRd3KUXgyGfsxyeZcmbNDxpt7AiRpy1r+xlVczf8/f8ceJPUdyfk+lMmwaCHmcCBXdDS+JI
0MZXlQX+HbEP9eAMbxadaVdKxM63lHjLa7iYTUTYfVwi+OoghAaC3gYeFLzcnRW2ueyifMp8aWoz
XpT1jIL0L7OfD6bf55rwcZ90xlPj2jRMxK13e+7k2Dvo+SkZ3EPjMWNCPizDrnECuxNPIk9CKJJW
8pQAFIYk8GbZgC1rjaH+zQs2eqC8GdMYszVT+qX3wuDfNvqvOwls2Au1XiF+K/fRoJjkwOgEGNKs
JFMOyRgh0VnrziWvxGOwdXC8ST2dqVaE34qzQmzs7lzdqQI0yuz9JACiO9GQaLWX9fBY9vUXJPNe
C7bcDm1svOa+z4sKEDKxLVhg3jAMF+JGFSUIxj9lnLj9d3Rw5i3yhHM7X385Z2esDCaGCEMniKOL
Ix8KaUijzMiQCjPS8uxdtXKPWRrd4fK7dmg4M2snqWCvKKrTf4AHcRyI8y7I84oPvvnWdyiZuBLV
w8mDQRm05AGykKu27Gez1OkjOE28/NWImhILwrKKMJaXqeIb3MXK5eWLowyEWQTmjA6LjH0oqAtd
kz0C6NCcvatPoDVJjdc1uOYRE37pj4pjCWCpwLwo8hg3RGTsAHWVbjmfx9yOIh2NA+LlP/fB1b7J
v6wMjNixQXHPBXiW2SCaK+v6+0SLc8z+a3no4wUpV7wGtwc16aiXNo+cHCgt3UXy+CAQ3toJwYkV
sGzYrv7eYKmB3d4lmKQ07J51RLUPofSVr4SjlLP1A/ZQ70ORB50sNAAiFdg3g+Tta9bTGGIicEqQ
vlk7SybluVQbxqPyeMeFnKRhmzbZ/yF7P/6Me600wuR0pcrPKwWKKUDN4VMBEgBSgPaT4/Aq+6h6
Vg7mS+Obo4fzrT2KBfL8SZP/gO+kYDaTcZtTQHGF6h0Alme/h3QsG/Ja0QmPKmif1PpTV93T0s7i
eeEeQ5vSVNcU7Wey37mDVJfyhjT4tHHUrQoXTNcGk59v3lM01yxdud2d5ijssckio+qWBTns6dad
GMguz9G1iXBW8uQFe/hkkN3vVuG6gLAVEzmZ/95dLPMsX0WFr6E3EDOllTnW1VgU15kPvBc4arye
axYRHWmKOI9vjknkSt/qpN7gBOfHX09AHAMSKwWDPh2wneMn7V143htPcyIT5UUAs+Bnmw+w0eit
ZLhZtJfSjgSGwL1Iokn+BAySfLfzdGA285b1tnfJ8jdMVurAdsuitV4khsn+9DQmPGGE+awhum0n
F6M+UmMyVFuxz2TNBittazCiKeWBySA+1plSfDFpyuSBUlZ5CjTG4n0Ya4VeOMcato3DPb0+mbhx
E2kSl2z1Pu38+jDlp1CDoEF9m+Ym070YFp4ezIqzDkTfL4TwGR7Ga3aBfRkMk4ey3xqf0/AnfG2B
mNntVI3bvmfO2V2pOk0r7t23s0AW538Ggzuv46/gp+wstRIYL0GTiScbbJviWbYHhzQdaRvuFO23
+t0oXlUhoyle7Bn2UejUv9T4qwpCL7gPAX/J+e8df52jJIlybBwFcbuTFUF2dSp3ZdzC0MxAYE1p
ZAZPkTz6HG14PJglmzlox93+FAqtJXk2fyiR3UozVQhx1cN2ZuUAChDWboNg1F0pRfaVO5OfYIVk
112RRrtbFUqnk+9imVYB2XElHNEsAHyX9vvxA2Oa3J55vueI4sNWMX5TaBfAou5Z3lApVxb19+MH
zfK0ddU6dhESlEpuKyTlq77nx1cKmWTuqh0+ao6MveUslIVqN4vo4Qa1Khlvbflx4CIaNeobzEe2
CUC3W7E9Od/BRGRmzc3JLHvfqo1R00U3qECSUCj6NiBW7IPfzNuIxECcy3SVEYsjDCtBb+/iTOhE
CQLPCM3RL8JDG1xEeH+U/PzBC/usi+7zChJdPmC+escKaBWiiIvwOIMF0x9Yu79OGaFCbz4SENk9
6Lg2YZr9boTsv/t2xpAZH2dnLhKQnwKqsoJCZ+zXUGtVKIn2pmJvmgX7AoeWvdqdSY+rSEWpXi2G
B/RLWAfJxvtTjQlPERGyMxdt3LCy6QnWXgl5FzsJuTmX0nN1MzXl6Ol7pgatWsuUZUnIH2RT/cl2
bQDpf3uWzvRNG/tOmEOFgHQoXIa+HhZVwEyOllDLCRxTEhoLgWR2VvIuTF/cPKVfG+4+j8X/rBi0
we+r4p8USOvhRZc5cRxatZhb/Ea5L/X24vyuwE/Ei36UZwNsSivAkceJ347hd+zuq/bEAJQ3T5Du
VGDI1nmUwAFwSThApgGTK5ZTF2jMNaA/Maqf03XVnL5n5NBmphTESBP2WJsA+yBnZuWLRDYgV/WP
DdJriTN4wgKNRWPI6ALc5dWvUctHJk7OYRINJdK8rUdXLCbE1hak9s09vL4/LXetMnRpij5cl9vx
QSFfWINH8CiGdPOm396P6UqEvWGVOt01s7bq2HKFbzHYmDrrhVmuySq3tQ6jnuzMKSaT0N5Xtm9j
X97pJCXVGP5SGE2R0dpGCVdrLlaDmkM6HNOjui1xIi/AggGg/3giVK0erhc5prKTls2REP2xpY96
lP20Nt/GrM8pWcHgJVdu6DT9cG80TRmuFOlHH8LsK9XGv+LX5U1DVlFLYuM0xlZERpZRII36v91t
oKjpfttN9VPCD0niDgetJy0fvcCri+FGmZSsdvB1x/fMs12yskdinF7n1L1SOWiurAqOz82DyRbi
rdCuS7ZoPGjqyYEn70HimwxJ/5vGh3lqr6ZJUG3ILGPrcgAAoKcWO8PW/h8nF+mZcScw3Em6sU3N
xmhWXjDVpdueIZjQe/PKIkca1Fdc7xoANfePPNtmJZD0A699rKE/ak93Rt6an/ggFjUZ9kPJgY+a
QJJLTGZiMZa/SM8VyiNj7uGmr+nBH1umY/bimmDxuXUuaA2vXRDXOPrtGthDjYLzvaJ+woJ7Ywtt
F6SdEBZc08CEMZhhalUCx/mG4Q7ERH+ujYy5yWWUQa5fIuoPofkNA0FdKe8xFc/NfiOP0fOhCWvU
2DJO+iYQF62ARPeQmmbaFBu94Y8Dox/4DfRguTemp22ZVDAUXer92Ombtrwrc4AusoYdYUjizarj
PZVM9T0Mb/qyJPlFLGVhnjCFhOcg/cfBE8ZylcI/3Kftf0Iw0/Z2miTlsp3ME4RNmLgMLldSpwm+
Oy2ZF0zUzG/HwK85waQBnYIN+GirxfPnJg2JEbafnRyfgTXORGtU2CqR7g4c73tVpAX3F28yP/ms
WE05qx8cBulxk7B8Jt62d2IGr+zfykT6cZhh8IGRSF4rikkZgYWXkeIhr5wcVUB6Jgnmd+ZNG7HK
BQf+iNQqRzyXtQmDjPk5RMzKVey73TBV95c5eN5vv5ozuiZo6yk7I4qxAHboWdaB8322QH8+WRd/
YRaK4uGPh5iEdI2P/omskwAl5V+x12h08QLUDKJkQDVgGdWmwYc8hpW5pwpwGapz2YFHCkO0CO/W
isNfPO0ISTuwB2hso8TqbqLEaEpGPLFLLJDhmTBLXzkMxNMUZ2SZOz+Od8fEFlL/0n6BllskBs4v
DJ3IhxhjnhPmeHBGIzI8W76aytfKLtZm6i+NiTZBurnxJ+9E+2VRFseY5Y15RaJvI728ziyTk84H
D9bn6omkmFsBe8b1EXEtLojIVI5qw3NDiq3RDxHCiAxrNn2xQRwbaaxokDsFeQwt3568Si4OpGAR
x1CL23wBavUw2nuNRhOvkEEK6vAA7//HxMDi0IOPUegvVRHUw/AbCVF3z4wPxTMITn/UqrIwwfux
kVSL8Qohm2Vhb6RZfXUBw3gCNCGO6Nk3Lyy9EkaC8g3T6u2IhQ9aD5cJ/TqDaIHtCnKiWnvrdYXv
aeDXRIbPsY9oIZXsSjwzhE6yZw6TVaWsgCMeqt9sysYgNNcDKazKRHn8aYgjv+A2J+a9JHcTDgLe
tDJNlYyJCemAkPiVOUg+4/xX0Wq3uvvL45+14hTRNvCCWsUxYiLkCApLkVuTS3kJlGGj2/hEctpW
Xy4pRWnwf7tE0cSQH22u96xsBkekq1FLw0/Wl62+zb4gQgehfGqTV1i3VDxV/tr8AYqISKBeotN+
APn6KQWMzz2KMXFtrkmdebVVP9+HsPQNOkaBfKyg4TGYxFa2UCzosOHmq1xpuvDEZGGVfSJtILOj
9ygzxSzQdSlj42mc7JDMjfCJxUzCb+Xxl/vBSUTMaR2jBnQZW3PTvdJ95PRbL59LeDNAc4wzUWYn
nZLUTCq1svwIJ11hf4+EMzBxI90Oweoa+6sTNR8YfZ3TveqTp+Gu1Vp3cBbhqG4zkBt7W3xEeTBm
WMx/2SfOgkFUHVI/BQMed1Wz0jQTu8ybZcxM+LzzPbHkVWiyNCGDNIl01auyBT6xbtwqhym/LU9U
u14GNsfWHog1b7GlDEWtR70zCX0MmJyNKpvjzQZa4FoYNPZ/7NlLpSGgunxspcLAbABzeK+les8I
jYlB5A/IFQb0puFFCgoXD/8OYz5Fp25fpA//nsPuH1pWsuwB4WlvdWK0EkF3tOwn12T2f3rRrfc4
jS1THvZ1WvrrwiQkk2q81m7bwofC7+G+QANaSmqxwSf0W4PCFFVHr0ThxXLPiniW0PW18YwexE4i
2lpKieO6r962Gi6OgBSiI533FpCEukny+pGkLdq5/+5pzfi79+4L9hH5KIrtInR+85MYkVgAmdOD
trA501HmGba63BT7lIlNGmyJ1zWYluuq7Lqc5JMb2UW+E4WZQ1AVta3kdmVy3BuQ9biY6i/qAJyh
WYTr5nVHnaTBNKTwpEl8E8JkLZCtmfc1F/mjx4bFUimyLTbz4iWS/88/Q4QHqjbui2a+kXr0yQMd
n4+lE22Nl0f9Wgbkuy224lP+I1XNP3EomDDeICFEQDdXXXGU8W0uQksOlT1SNEYdHSnskHZZy+IY
2mxEsb2KYqrlaIFtOApV7pNEtaijsiEpJ5CI9WQZYhIh3dware3+nDKeLeTbFgkKbcrZJujHTTAC
90z9zJPKid0jiSH9n0/X5Ax91S+mZbM0zBbNTufCF2oEcEjT/avTCFvnWe9sfxHYlNqRkQ3hQ6Ba
5jM0hP6pSOGe+vlpQ246roTZyy6Hz3JhpEkdpf3xTFRSuOk5VwpnCyj/CRe1siY415Y8w44W9Lb7
5ejNOOFaMs+ijuWhzwLDd9TKO7Dhdx9AH+JFvCZNFA4dHX7p+8oW0K90af/i3V2lYb3e/krjmENc
W8cDsP8R04SMqw8Lmx12crjjJRkJWOIxzJlRsBf1wS97QooyfPHBL2+IknlawkWgDZLux3bsosbV
7pQMLQQHhqfz/1+HfjfTMEZkAzJe6ODPnRTXWbvs/czbwLo2NQe5oGECDDYWywm1tO9igAPpJiAV
npJdMDQqoAPFSkdMYdhGiCotoChjNZr0QyVvn6NdI7mh8rpSKZb833thfip6rXwQAwl9djTffxdw
B2OQPlmg6JwSVm6bUZC/3VVnhbRqFDNwtx5HE5puyEKCqG17L8jv7rkqJo3AK/dL9FapgmachuCe
TfO8Y7Kz5OIpZml9qml4bkgDcFNcMH/tMg9aMZqo3miQNiaN3mISMukIfI9ce3EjNpnt24UOpciO
mrxh2RvU8HkezlX8TMAUlPBS7RShQldLW+SMAWXkENQPIa2u4p/HwZv08X14iu52XhbZ7Ao2vKP+
XDxDdvbmnQXDIvzQb4VCn/f+lVfIMG8qsY76AMEnheULaBmXr+XgEVQeRod97NLGMV9QZEnmzgQ0
vS1XUK/mUgehtqmZm/obS2rP3ucrbHqlWC6xz5NvL5ilQivBLLWPsLwXLA5aoHcLnY/iXrjti/sg
9iNkIcL1c99/WMsZAX+gkwQNg37EMETvZn/4m8gEs2YgmWkvJp+hmwv4mLkmTX9oGzNOo8l/n/Gj
8Bewvge6FCTIJfYu6ACZ/vOjgEr52yc/rFYXdZvo7FjSHNBqndNI7884NxQ2aw+u2liD6MTy+qEf
F//SvztWEGpiXio1Zt/XIDF9A6VUQEttiVk9ceXOnej/G4OsV59HznL1X6eaQgEebs9KAoE/kYgA
tIwpuLKm8cJJ3AuMh0pI27FbuauKXvdjAvf7EsxpbkIvvBDOVTjhzRBO9lFYg9tVV2riUD5UHGyt
VXAcrrN5cejfl43XjSuKe1vO11CRh7Jt2HwPyir+i3faXxQVI4Z0M25blMERdRw1hv6Z/9vnntdx
fnhGsHDi4yYo4hPnUJur8gV8MrHTWWf9iBvszERdsBgdgMmyEufa1AmIrWz12ZrqIgjeuA9u/4SS
xe7ervadQVzVNRjVPHLLCls4IxoBqp4PaS3b4wJqA98vzv2cDT1EY7LPasfRPKLs+egqCYPfE8KK
QsoCAoueb9xNHxPBRwKG2yBOhg7oNPsgTYpVy5crUom/HeyYIm5KfTDydeR+YW7QCWbRPv3tli+D
kliCtoKHoBXqqxQ/qZGJt7wvVCJGeXTh4tP6T0V8NJg7y6eUHY0JaQI9nieS0d/O74K/jSlF5Qh1
p3xG+LhQWymw05HyPnrB5E3hwb9TtfHljOQqenglJhZBFK3WkbFUr7EDn6r6sWQBzEOFmybpXDEt
SHqL4PKGSYbesS9LfK+SD8eiA4CrsI11GaPzD1FMeDFP/4iKP83liUTzVjyejBrEzrNDJMLp+D2c
PNUje16WEMn8kSteVkefZedG27KZ/B0Ih2gQdCTFFwUSyLmXs3CliVT2b8n8erPiJusnZCGV7II4
KwB0V7mB0dUZkY6g7tLPuL8afPKJNqb3V2sYg+90sVpybVZd4gIvGMACvLlGVxSz6B2cl2o+5cPh
8DFyK+xF6mCCALVwi/tAY0JYrGFNWGGlfaHSEuhkW1HHIqS0QDQkj1h5Ad+GiiV1clQw2dL113Bu
0n8PyJSVsL6qgmuElXKQFPuyyoNNp/6HpPiMbNqQVyZxrt8RYS46MxsME1/pGk03hbWdK72lQbIt
HsYAZ2kIHMVYldhh5ZX+bSkr/9EMX3srAUyxoJ1ddZppKKKn5gw+/cbHcS6p/KNdGJc4HC0wS6nU
ZTUhgY35AjWo+5HRZX3dav//9WskenbNlqvqGlmwIVLcp5x2BVsdkhMH9zGNQut9nL1VloI1PT1l
1moGY/XCBRdnQi0owjdER7Aur7YZoYspkXd+UOt8rAuVtm1G/llgBJfeSBCgKmEQo148em1xxEXn
2sICGQeT4uXHb3GjEt2uoEoOl/Vs1pE8uFlDT04t/2WUuM/fbKQxAC/wsRbObZX9/VGb7dyW72Ta
wqpUbGSTzjdDS6I+Nsr4b8Z9yDkh5V6h6FtCQCF8+NIVVopyROu+a5+Frq2Ob4TN+nU1v1pj0aFT
s93ehIeeWd2ycDJcGvQtICN7iheivB0zJZihiOqdxXLVmn0SJtN+Ug8tSJ8EnOM8Du78QnQGTqb4
WtlvSZNnIJJIpGZjyndAumxkTBBDO74hV3TUsa1PUpo8GaklLxf+V7U9LkZkguQgNbWrJ2T5vmxZ
/008A2/2JbaTsCOTEOxH9GDmYtsSooGB5Xic6Rb0a7MJ9asJb4PWmrR7d2kQhosYg2vMLxvkqWp8
PMn9S8GOmKADzxXmryQx+nHewbVweLNE5QMaI4fECp4LhO5OxCsWi2TWU4G7/Fpk/Gu/lM7HBTJO
JnMiEkIOz9Tz4RcW35s/v3u50c8ayJXgBjEZWU+25JuWJUbvujS0ARZX8O/Wt8t119F6UFdpKbYe
rmeWkMKb07dawWw9h75e/+WRhv/oCAlVYf8ZIS1YerpMf+UrGkojIImJa5KsfPPVZbdStPLVDK61
X45hcH8R4fup8ORRWk9UDfNRnUECwRBdlVfwjC+SF+LYA60OjG+UaUEVWhAw0DP6osXJOZUktfGC
5thizreLJ/ysup5U+o4qF9sEG0/oG2xtX/Zqv9NT819dc4ZjpmoPO61E+Q93BkpDO5qT/FF0xowH
r8xFRfgl+Ukgq763lv263ZhpXAkwmRL25T6bMQLT+Jh1WV0oZFcUXXC8na4GCCXC8AX+K1hnvc9m
QgP4DucgK6xC56ZpDLHLNpzLcMMs65tIZ6w7Itq4ZGP0TLzy+UC+Zpw1x+ltzmIT7wD3010sHCPO
mVMmZLTIiNmN6PmHY0xQ4cwrg9/je4U6B+6RXV6GZfP/pLAZy2+38kX+bvDuB/wbwGxxOCp21Zdi
ppmlBl9zaKFB1oskFEMtz26PDuc6ng2O5e2qm9br/seuJuSMrOUmJJYLoQ/g9Yw3HFQl9pZIjpdI
T/YbDgFi8QSjq1ajJFYegcuvZLgQtAj4ozzvqq3kQSpiYQXeWdHHYL/Jd1tA28uN+cM8MC6XF+Ls
RtQe8L89e8wB8Qycpd/z8pSjHnyvNrb7yG1hjeH45cBTj4vdun+LnaNz3MAxcIXNMO/euw2MZaWJ
/ET0LADAh3aEo3GqB9+jZAwtCPzg4eTC4WCBlmTgoBLoKBZkz+xpe8MqpzpNOCQbqQntl20bTPkY
yZi/LlNtR9kpETzRfYPX7ai0aMMZBg5vSJhvr/iACfGd8xo10Mrxu2YJeDX8LKllSMvzxry3v08R
NI2pQoiBIo88cBHpAcaE1AqQl0GOgWBp6Qg8cuEV8gBuAStkyMFA3Mx3TRo8gj+JbLTX5aO1Q+x1
NLwJbGAKQU1kqb97kK/8qmpDBJnnwHQNw0bwukHNB6ol4Bxnj/JCSHkAlexy1NUFEQx9jmZxGSJn
2jile57SRjGDZaLBDD7TIp7FvCBfESeoOYY0KGSy0R9rNNQ0DduE06pWo7fboNyAEaUSAZHJidvy
J/BzZS3ANJWtd+xiLAk3QimkFF/njfE4FTZwC362KMkG0xW7BICchitrHnsvQbjkM/HwYWgkd0QI
VYgnj0Sr67BWUzbhSfv6njGcMpVP0lnwc1IaENtFHAywyHhbo9ZW0lrfxzZSpQdOXR4ABbEwZGk0
oex49Qlw6proJbRKNsyb3+FbAY9Nn1Usm9DG8C6NtU1EHE4WWQIKNIs618k3dq6TuHSAP9jQzJtl
HcSZFNv7nPghPPzP9xB3l/X7k4qZceQW5L5Oqy3bfmKrp91br3ek+1sGMpEHWLGKWA5JDHR9VRCB
3J0fV5DRYPiNjFvhCzgmVbFMoBG+RocvUqtk61+gL32QtCqDVMseNbuHtJB7opxv0dC9DX01ht2i
N2LOq9b4xg6AMi370kZZJ47i+6D+jtMBRCBHiMSHfCivJzgv5bGOlPwphX+jFeyE6MqkAb1oeh8x
Qo3CMi50RHZ+HfKkx8rze6JMY/8WvJM2vGKYsit11OP8YLBlNWMvROcMQhV4FEAWvss20zVW8V7U
93beZk49/S0ozmuQWOVJGF2noH99nwjGOAHGb3fpcDJWWttJ0VVl8l/y8i2hz6L5V63NROA6rIR1
hgLpAYblmvCYshNWcaoAYSouUq5FM2OaqM94eTrMkazKdRBLSrceoq7ufAi+U6gzv8ypCVwL4mHF
Zo1MegXN7ezr2DkuA9MYYsqIGyWlNKIoutlx/kJdnOptXgWEKVgCFMsufKJiLcwiFSzNi7HUlAt0
vq9pJTHNQbBN1X5jeZvoSpua4jOf5k38RpJiwKtsKZAKN4P9+MO9zo488evXIgEoxf/kXXCsXHak
6JxnlsAO7t9FD90hnASOLJQJwIeHVoNMIdlqD8FYCkp65BCIHj6JkKCzg+T2pNlllaePYZIVXv/c
HWYPXYOte8uZa/np94PVquefX8k19Y9ynoHg+J8MEdJSk7Iq+9oY9N617UffnagM3DG/+L3Nsj0V
LGp9VpwtJbdYgMEKmGevKb+1RP/TlvRT9KhmBI6LS7dS1CJQ4UMJN+pfvZWPzWshgHDfRcUlfgUN
/pK4UYFSFLHM4zJyi2UNzEXYjnAG7qeC21+ZxF8EDiworzoO5aLB0O9rgu4vswrloL8gI90VCmrv
8Cm7b1FCm0GaE+8nvp7BAK/AVfYx862p9yb81ttcntFmgxTLjQsOH0WYXgnWeweRpW1NvquhAyo4
UCAKo6nrIXOMMKyNfcmaxybHoxDqdf8dS9VEPoU3X69wpk/mgKQnjV4JZKpt9uRfwVnU99dLMoE9
khPILAbdnUKDA74VT/7FjogcZDakbhlB+B3vHJDX+YBNdHShSeMpX/ZPQq554vSN+/9yZBQs91MI
Dx2QIfXeFWIV9IOqNLXS0WnltXfNAAZY/J+PaD9vuGKqYTTg6WtMInVAd5b/bus0VHgpNsYr6Cok
e4ncWR5xjxteX54/UtbMgJOARiGT5Sp6WZDXB/pHwLIn8ql3b9NxQLOWjOPhO60xkq6g02hGXfvr
DL5WqXm+BOsen8DCHoEwp+yWmPC58/vmAiMq4P39qnJ4onbCfgzOydUJyvx4WRWKeHX72i+Bl/Ak
9fOTor1h+D1B7bJXHbFxNMA34WSewvfOlaW0js7S8Gri8W+hNta4orMvr8IBv/CP2mCTcwO4CUPM
znZDrD3jiC7W6vWt6LayLbYlAxo22Cpu6JW6rzqRbQLGeiYE2haZU+u6nqgfvrmDfZ8KUV8vFLdl
4G8gaQQt2lS+Czg5dIrQFAAfc88N4vMCa8JaWZCyqZoigaMu8llwFzclbVKhQow51vyPUQzl20Uq
UZVAwhb9ye9UJMgnmhtRhNw8xJpnv2nENkE9Sudom4F+YQXf+uMT/8+Oo2nFY224dFuyZdhtSQtY
ivK8j9WtwhM1sfhI2sLbjV8Ax6nO5XiAPNGJ3I7Nirt7ucjDf2imlkwKE0z5syW+Z2MBX1ETabWH
kOmyCeh0DjGKE566PBgiBnJX+RqeKVX33NPMi7H74aP3pSPYMJtuJOudjmu2Qhpq7W9P8uM2IZAk
S8InZ5tGjm1PzsMNimKwVeGkSSNqOV9MJ7QPQ/XhnJP7eukbzlJG2M2F9cdYhQO6SX7s2vz5sc2N
v4zo3YQKWtN0xMy8e/wYDNtJaFLvNJbv3ZNLAyPy3Gsz0fU3QeZjlAR9s4vAWadrz9d/smJnDMJb
tDCAUTA6UVRWGti7E18MJsugPlEJE6WtqoNp0Y9bWoY8tB4oQI1+Qn8FI76vqj5HYeiUEtDn5ePE
8snJ6gkDwuC5q4h2E/L4olq9ypg5Em///taVWFQmZswVVQ1wBEl0k9frucpjAGxgAfchO09YW/pj
fjW9EHFrae3ySo31+Nrtog1arWOUk23QXnOfiOdCu5hh1ldorB8TohlCI4UtGXIyEPaiq0wwUXaJ
NhcIkKUtbDPowqbhNlSfllWSAhdjO9JzqTv6BAKsILxV1n8i7VmSHQ1TFTT1Bi7A8m7lKvCcqk9U
zG6uir6xf0zKEF3/Kt0hJS6G9TrRs7Kxdsxzmg1rYnKe9ueCe1UciY0rVra8NmNwL02aCsmcpeVB
VFpNyiDGydNcYRRokHzwtY+eEWytvkrxKQloXsG+mTzjYgBc06yCP0YRD9skrKyrOTdHyiS+utR+
SUfCQz3a9gEpTv/RygHJhnoGm+YDOo37NN0QVPIzkQONZWAcrvrarQeR0FIGtkcttOmP8f9fsU6E
Mo1FAlyamULoIWcb4gM8j9wT+xYZatEpWnA4oaIFeYW9Iuv/W5k9T9fYUCxNWh+VFCwN2FG1DnhB
Jy85w+lY8DkBgfC0qtmMXMuEjPEpNgbWHECrYvapK5ElWADIzfOdg21iyS4Inex4GOCKJQK4bssZ
ZA/v6ajCy3ewh6BDE/RSB+bejzwAQ3ThGpPwlhTKspz0qXCNTz0T+iPd3HOvgWDHS7RlYhRzd1Ua
LuBqAtPk5iZo3pTty9ti1MfJixqLuRKncuX62ND8vp0uzVxPcEIAMtq3oz3s9QXafDD29ZhdTMr9
TbQDybq32dOFCFbitohKAiBpAGU2Se5fuYK/+kef2r4E7a96HtGWUmTmUzhgtSe8GO7C84tQ+YYC
RdomFTOOpZzUGyvCXw9s9bgdm8npIcRqXLOjfoBHuo0ThRITmIMhCAGnktWMPxaHeqm8yVazvBfc
ysQmTxVO89F4r3rDJ0LSJj0yfYnI5CrXdO45XRtnD+CC2J9neAPE7nhMVUHKSyGd4obBYc8QD0ym
LGvcBUf5zDxOyKlLG/Y2oEPkaKHAQi0b0IztuoEJg+1YS/CIpSs5mOGl47x+KkLoLYjra6LkF/Lk
hDUEs+XKMN72VFX5hMxLsQL3FzGtdGRQDXvX8RfR0qvBmgG2Mv3ny5uudoBoloz9g4uNNU/fNpK8
8dbjpE04A49vmR0APiKUSi/QsTzjyrZY3/CD1qePQbHYheHlFK7YLEAMaW9Q9mwhzNdNPHDKLEP3
rSySYRV0qZEd0c+zCDa/qWA9q1HXI1NtBNsFu8z6SERAE7EZFJ+Dsi0liAHUMcHNa+7JN3w2j2yO
AId/mtPnEUwyeiii63q42rURyxCTEoh4CHYV0J6TWH2NKlH2/CGIUyephOhNJtV6Y/5y4VGS3zWJ
ZBO4aJsaa3xHJL8FYOTyI/r1QRYlG5lHyJauT0NyktA9w1oUPcIe+5hPyt51XZeE9dBkxNLB+ADI
I25qapZOUTkLX5y6peUOQEidwV1gERb6lXzXusEfOrL+kKAveI0w9gogJA+4To6kvuhplebf7m0c
o/+gRLS35eFc4ac3FUpqyt24wwISYPv3rNe7QkzSZQkJK8+9WejA+ncZe/KUsKQdz3tBy4fZLEnW
ww57zHWEDiZZFL08JSkOiiESWicgRsyA1NXWbftcMx0Upj5zM+Jssgn6ErLhvcOMy490lv/tK1eg
S/zWCKlsBZ2S+j65tBB1NwgLLS1GnVr0jdC0uiRMnfDQQECs85HusfoxJjnIf7us3nQKrqNo/xoQ
71HASi2j4lWb6tw+t8+LsdENu5YU0SjFhfMQm6brbD/lIADnZdv1yrjAf8jUxcPdvhLkHLgS+WE3
+JzGBJR58sBwiD5WFy8Uc1LotpuWv404FYbj+czkl8qGHSgpOgNSiz7FY5tpFI4safVYcIGoUFvk
9xwe1qiUZ7MudIA77OcBjnB/Ul85ivR0XWn6k5qm2HlAcDQEe4fr7ZNg5xqRV8sjKlmq+3aWOBW1
Bz7Kne4vkFKUETUGamh4m4xmGqqtpx9YLZ9w91tA4jrz+A+EPyRlwZ6VoIdoucBA9Yl7+Th0k2W8
gd4gVobvp5K7T3EWUgtevJbL/UFn3f6BcclT+pknQMVFdqS9B1nPgW5eGgLjg6d1kEW0RkrP/w8x
PIYA11jOwQuqR5zzQ3NkmlzK+sOy+ajm0u7LcJ1H57Q4qfDPfieZwI9hkQFCuuhSdjt80/MdS6yH
8DZWB1eDMQpzLtp7voFviyaQeVjXduUBCBiAT5J/O/ljMBR35f5TOD0AiTYA41pnaZUgs8EcF0XW
Pcw2EVhoz5ka+8Lt6L6guisXmuIqyJbVdsjs9V0sXYq7oxesWO/tt6kZZunXo2mStu8xQiZFZXRO
uYzmn6EusOV0Yx0CrKPYZDwSOUtJqHGb145FX9apsw4I2Pd2oPX6XiKmW/vV3OZiOuaEMip1llp+
AwczkoJpbPjPK1Y3M6kh2a4RZ61BE7FViXgIG/5EGQGbrR7A5adcXErkrUQcf82dVzB735GV/BT7
tTCYn61TN1IZc+/Poo1jzN4sbGQ6yQmbpbxl9bRfYU/sjC4gvzLrGQkBwtBMiiANeJ5Fyh+dRlzE
0+r5RHEh/ygAeJ4jOFR3kd8Fn62wT3xfojC2Wi4xoBrKUcjKJeTb3mdfcih1u7fOD2QGxj8k8D3G
u5nILhWOzbqUNvociBLSu4T1nUzdjTx1lfi+q/CQ4J2clmMdMJ1WLyiSDmhBVkUy1j1CQpmsKYIJ
GYShqRBsuM1P4zE6Cu48BkG/6HvKxeAFt5JqxW6OH0Pc2JBjK8L+B6yItikEA5qbwPnYQjyAXctG
DSWk/E9DT+3PFUMyUa5qYX/8chX263aSDMPt9+H+/zh9ZJIfZXZVjuWLsJ4ge1W1QRK10hDygF7n
FxpNGGyGc6Q89ApTn7nTd6CxccTvsqAfk7siD/IqFhMYX+DLHGx4TkhHAXhQ3T0TUXQTyM1ye4tR
FVrl5UHSUdHPhkAkN1kp97xRQOWeJd8FwF9hi6/s8/g41Iw/RaA76+CXV8jKH4jd6yVNx/gFGZfC
6vTV05zhQwEXLmj81IZaLxOSsuSIgjtd7LcgPv3l+K222l6KEZ88iDNhs+7msXOhJVvJKvdaFXXI
dZLyDj+mDaZDadaozoR9nc9dVG6cPI0AAG0XLIRye3t85WWjeJGFvQqCcZTTn6OUsmIVTpCYt+6J
q3q1zSEReMgcLpPEbdqGwaAFZHPHDPKjp8M/WZBdA/MC9lwwDo9NiJVA1CbTfcqhElRrk7XdUau8
lzxIA0I3HDIGQxWKjQlJ11FF0Bw3JRB+Dt4PRDYv4slr77d566weOyYgdxllGWpLV7X/0zd7+5PL
Jd8G6f1xp7rT20DKGK777VMNGk++N4upuNj9LakRrjhbp/+ziNcgve55RThr/50cogDQybrF4ZsF
CstKUvTkR32aOLD8Epritj+LVLI7/0GNzUZFfbN8YxV8Ux8EUcY2fd3CWz3iUPF1rRTKON0eEQ8V
hLkIuJADLTl+ZIGjgeMquHVf/Sa8iKC2DYwdazdCMaGA+oGiREJW7s3KZyOjvrFPXahGP8e2HtJx
Z1ceOkFApbXsVHdWRhPq4ysF+kTNfcU26mJv9hzO/hexeMdXXyqEkMHHiz0a/CSi563XK3tk+Rqj
wp5IvnyGR9EkZ5OzuW8l/zmybaZrvDtS4yk1Tm9LYaI0GUiKKMdMdc46oh5vvnZol8lG3aPRkz9c
Aak0mdJVG4L8RFk98n8Mluk9wDU3CRdLTQgmod14P36W99TOYcYkMwwdrMnpK4uakUnU9DY7h4Ui
OP/6a7tFvASDyCf5Vq9N1adO3MKwyjlQvTcgyHM3y6NDxMCWoLEHrJH/GtWIJPfjLuf614wXUXi/
jcnKAEnwai+zdYWTQowiB6iRUF6/farPGEjYpZsMVtxKyaAJfjmXT2Lk3LmKRveIeWjbdcSzAo2c
yh0BChGHK+Mi0uo+8cGWduaZUzM+ccmbwrlciFiFPTGUDROXkq8Rts5KHbLmKQFac6OthxIg678u
Yosj6GHpYldg5cLBhDOYtPTLLCBwaB2jH0rHR6JQwVNv3pKHB/JCjLO8JYHdUXoNn7Ucg1pQ7UC8
7dlbUYsS1L1bQr+im4pK2fuobvtKjztTZndOsjPLcyuyR2dW0yIBA9ykbvNmmgEM/OlyZNRjYSTq
BtGOroSKTosIQP945yZyVrJFpvqYVDzbrRV1uzz5oIXbFHltoDjJe+uI4LswutQYFyyqUVEDKGHs
/u9jOtoL4Yje/DGZCirea0O+kG+sdsVYNPRTt/MB+8HSVnj46YBnjkeMnIllHl23UHSV+DUdp5EF
LPJTzUmphgDcgjaU/lbZc+umJk5PP/cBZXisxGED0yjKB6CoIOKN4QjFVAFhb53ueJ6lOLUCCz32
58sDTOljyKxSRjd2vReBTyWV2wSA/WFeHNquFMrAr/nP/qJ+0Y45p7Fi7gV45KvO9ps0NdqcnOIc
BcDtMazMZMitA++RBJ7N14WYLT/vwWzj7Oxjw1K2+96FCXbwTyDF8/aUP04sfZwvnA/CiVQGZZRq
PARr/0DAl5TAJ+5CbEImtL6WfNNlN9KJEex6Oe5PiJ4OwIs/ekRuElHGVMUsySLCMbUBBQjUhtZ+
OhK81Yl2suWwSMkhkUwnJgvMJMc4u5953zFkEuB649hOKUTp+CYKkcUgHS4Tch+wU9OnhnC1sd9T
2/QBt5CPzwqtQUtUTTUUHnCnMz4ygZ6Kk1W/RVzFBlc3680Cj/OlSd1rlHIahQYJwXNE+55gfZzf
pUvZ0LH0BhUEEqoG0fx5lRx88dnMGnqu84ftCxuPqlNQhDwBo2cxneb4nHVKyccMk69Rkl2B8xQ6
X0MgVP9uzWLjbPt+ZgsYzK6TUo3eMye4aPsl7HzJpGJG5Hyhmua9myp+kdkYIF5Q2a+//uor8l18
93Otpiar4CpeEUgvDABSU3jFRxlgN1+27b5vQlHLLE8+HAdhnReiMiIW/Cw7tTfAKtmxu1OyFI33
cVFkfa2qATzWf/xZV2HWmahQK/nIvRbJvjINiVPOVnHQ3CXJ4sksGIxwuMb9oJYrrJR6LckjKCur
tXj4Yw5BiL03DsF+LCvQQWYzr/RY/ZHNPEZOvTvkh9C9KfyRMieGnDJbPW8hzSNV4CKSX2pXTYRH
xOIu5ghjWDIjdPwCYUQUQn0jQpHh0UpezoWoNVz2LaBBCUOWDxYk1Tq3C2qcaJqc/Zmq5fZIOI9P
brmzeYoRBYAa4DiHnRSbyZeCElMgHlKvHkJ+REe95PexdyZuMI79j7vNekiapmaaOb6vgL/j9maO
xX8uZv/W8KkxFgjpo23LPRhzTuzzfo+OgTnzwqs+LAPkDGDpGqqSR9E+vh5M/hz4+Q5BXQ6QMyR/
fVwxMJO9r68AJU3a3BbTyEAfIOa3YPxClhenrBKe63BA1ynQkMzVUe4hDKgs03M+OHxt2mrQ32qz
ig/aoAnjHRXrq+Zbi9hSrjBkMcI6rAJ6726HwLb6/Jvx5dxGtLt9y2QDVU/BY6kw7LYLCXr9/LqN
dcg4ETOk5dODyW4ChyS+elnXnqIGVfisfEBIfuN0/ClVDtJUsmaw1AjxUPtJLn1zjWHd+kyDchJj
0pMhXOwMLYJ+h+6qVWbR04Rb8fwkDoYdFsPbRjCEdtzRU+zejqx+tsqw7WHabxoRmxhk1lBcCmhf
EryxVXz12N0DHnfzywAIjhc8Tn5ePw8R4guK5K8TSLIWGhO1hbE72VAfAi6oy8SfPxFqa5oObPuy
w4ISrIO14sr+fSHQ/NRB+6+XtwuGVKZgbmFNt3HVPr/5ylAlZ9n9x77/yR/pe2jBFESMwjTerOgL
fQXWZaGfqQVJpTLxYck9j/DnllrcmNIoxM2RFcJpsDBilaTDXSINX1mIBXzQPL3lqNfNxPJJbLZM
X64R4WsGJtoGQ9yrccjFxxzRLcZZkqSAh5EA5uV/D9AOKx/Ah/b0OwdGIIMJ2rhY/L6w6wNJxYAx
vFdXURIZGWzhAAH/pAvn8bstLtxo8wlGs3xLVmkKk1ER9seGZA/9jY+5+N16ZkKxpKCEPLn1I+c5
/seIYHUupGytrDtvjLocM6042PDcBwd+5yVYilK2Wk4uc/bPjzj8pigOV3fUG9+cF7eCPB/3pXNg
UGEv5MsV80HITU9BoA07xgZRodG4jI+cjr8x28H97iKpcINvCGmSEZ4Z8HBdWaH4yaI46BWxI29F
FmIAPELFkOf+pwT1IMtYHJE/bEJ1eVHq9BhKjv5SL8I1qVyYRAbfbo86C0AvU8quzYhLokBqhWTo
lMjVKlWMQ0iFT0ejtBU/Xe1l5FyonnfXDYT+W16pDaaVRw8k3oWJ4EZahCU7lOQz/8ES0OsAwTqx
OaaKyG3ccl6P+ksd6p5EJiIpaNydwIsO0k6L3jG7Eu4JVplupnkUgCDm7QemDkCKUWOdMkMreBNt
9DohzAbE0llC1bc9Wgz5rBI9mctz3ftCGyr7nRrfbcbi28kXlu5ut0KeI6XEQghUd2MzhF3L6izp
TqE8QZBKW2Ls+NADAgXMNVpqYOXwmnmb1CwLaDSdzi4/+FqBSKd8CQm7QdJ6M7FwLrtwue1Grkos
+TkltP5Pplv/oTmDGDp/DViioT2KUeTPK2Tppw2S32i8EpFZ6kWfCYwjoX51CIKJdDZBqbTajRLW
rP3kmGXU9W5p23iSE1mno0NOBar2yJLKUEif57VtIuN0QdFTBbmLl+oI8XsodCkhm6enp+o0MK37
W9N7YDa5HY25lG9VW+/CX7p8n9r7og4/m+Op2uR+Iaww6HAAK4Tibh6rJ7NwaKR66X4glvEHgnV1
TvIJWmbazq2XIQoqBrTwPWjiY6mQmGeOA+NRJNLR/qgS0b1ACu8g0CPTch2Dv0BerP8H0Gkf/fI2
qjwGabhvMKsmazAmWPW8AJ3z3bwDCHGpsereDTL+8F2ZmnWVI952+BxxN71GyLMm1ziOheZI20YI
vO5mAQbGSB8/Knz4iS06iOCxSgeMLxQd/r5ZSUkOSAu/j4ryLZ8z4XGByniuvZUj8Mxe89vJNEk4
vuY/PCaW4coiYYP4CO6J71RlAlet17/ecDK4LHk38NJwctlDle9g4PCsOz/OvAp+X/otwj/M/92Z
s5fN7Cpyrd04U4htKaYcSdodj0MF4mE+KwRqW8pPY7b1EeCrIAHcyCYh4Bif3oZEEjBgVHK4zS58
dCyVUnf0VperFsi8KinJ504UY77//FsnqdGl2hhjh/bweR1GpIc6P+rJVhFLJJ/F4r+hxK6MjAFJ
aMRX6OqRrPXPxcJewzs6leKKkJSDoDwCwhRQngZXISDCohQ3shwxybs6GNhrHZDGp22/+40g4oHC
UvwD5JupONdwf2/sbwYom5E/bM/E8dh9N50PSntjjed/fecDcd/aT5IdthV4fwwev1iUSfxMiWMt
OfNSqecC8QTYBDBFz0vFCFt+UcAisP3IhxYZZJFJuh5i77pMGhYxKjJ6EK0ddJX70/Iwq4yuca5B
bgukH5u7VdUfJQ74Jq7aYJQcrKUVWNbwATmRerKMo0Ci+1a4aDjhg5fpqwWN4WIcHL+vPkhrlCeR
Jns5Npbg0ND2WQyqNjSqDW9+kvPVU9gy5+bkuSpUaLKuFawLO+qKFjPwbbqAmlR4vJNw56BIegu/
eK4fhJE5hFTTYUhGRPvvNHtOeulIs7jHJv3XFHuCMay00Afz/cvRHyAHcsZmWuDJYu/8ZnYmYtTP
b+wk6NjdhGj2kneJpWGaJSy/BYO0iq3+PjwgEg1DcmcWmbpvI8BNi9Jendm5ciebtpBFrWltcROs
+ng3cT8fcmzqS2D19KvmC8B+jSFIQQX3W+mo+u+cTlc7OqqRA70iSs3SuO9sioaiMjJp29qggpRk
6NO79LZVjtquxH6Z3zK1rSRhtngZTxN2J+VS7ZjYQ2Jl7vEuULkxe5h5U77weF3xhwSCekyELL8p
PX3xefo/HJbKKoRRUvTcsxMsGFODb2fCSJUNyVequ0z1Kd+q+vnqxExqc2Gw2G8xoI63gqaR9Cvg
eBsqEA6rDy5TaibTaQPcoqBGB9mYwDvgT+4KO9bBNVscFfOhFBRhfPZDm02/U/+b1scQ687AO/cU
QIbf+cIWypnU0WsXxXEmWfwFcLTbHFruBJWbwRz+kYePqEB+82Y66VIf1VTdsBTblv/U8cFTc0L8
HZESZlS/W8D8UwmUbYnphCT+W8W+yXfYWpaTCmP3SjAC1IhIGojB4XJLRFR8B4eRBF41J6Wb/Wov
PfHLL95HSDI2CVY8NM8aHDScwN2qWvAPzEb1r6pmT9MQX3CNA52qtcDgqT4PSv6St7iSzjGPnoBQ
GGReM57OjKmWyqB4d1W4rDb/FehmJMosULWoJwE2TCgqHBVZvPxXJMnz+SIsHocX+UG7r2wyGHSz
N6eQftO68J4AvAXStJjd5SrVIIWLsl58EJ2urkLiYgC1dadRlCvOFHdsIQaSvDqcTAPl49C5z45u
NHpd13v/kt7N5CmpzJzUgPVCY3V5p8zbbtvi2SqHjZu0pj0eqCkZQiB97Uc58+nkBWQUKehKL4Ua
YCkZEIoMd8FMoIrGsuzn/Tzmf7My+CkrN1Kg3iy/udFFWiYbaB1cHCS6xCYAmtGYOczJjArFkldr
tdYUWjdlHu6fQe0hVjVsAhvOt696ZsKPuCEAQkgx1+hqtyfFtm+wfQrnO/s/je1Ll2CVevugyV98
EwYnAum+8qB5ARAJtvc3D5aLKsykXjMnooA7xMAhdn0W6a35Y9X0uq1M7J5xL+KAWn0EavgeAsf2
7XP7ZCD6Mkn6X7j6C69ZL9Sm7ZBlSHqP6WWTkGWDBxhJ4G+K/vLa0UyCybtazhkY45jxljRQl95j
hQJDpbx71xSG5D5rtGzlmGIfH8xI8+fgwlxDDRiaurgbIPxOXfRyLvO81bPFei9DPlZ7AvJ7rBex
V4gL3lTBvOfsNLw5f2czDBy4/uQ+OuyEboWq+R3rSiiqcMygmqDL7J83qDxmZmjGo9f0bW9xEuDy
MTs7y4581n/TeHs6nSj98oI+uulYXRwy5FytEjqkU3i3seHFtCEjfJZb7a3gi3uw7sN7dhy1/1VM
ivr/d/61LQwuGabCMvu5H8Dt+qkazXxmcMPkfbYLXk2ZGc8qZf4gPQySxAEO3DpdOAbJ2GZtWetG
a9CCXsRh6Z5G3SCBAIimXFUQQJ9PRsPanWG66QSLn0XQ3j9mKKHG3qOzTjktD607T5XRGmBGiajU
UFC4b6DmBZhdYooCgH1+oi2PPeAHPP6vYc2HtxKIJDNaTvUrdZv1PbOIdT4wBzSvX4LZoy0ZLHyY
l427kjFKyJvIdxhuiXZIxgtxPmJ6Dw8aUu16wleWQ+y4HdxBGYmqygyTovQ68M3XNVGEn/qo2CP/
WsZYL3AIUSt/THIgDaDpEoUfSIDnJap7cE2lIUrbF95lhoYtDG2MtBuApQUCg8wWzneBYlgJQQoP
VH3SOpdS8lNmquLyMvBem9yeZ9vJFkmikLzzPCXvKoYnDqWJBCq6ljcFadisU7U7N/njxAzFg8OF
6aN1viZ965GtXdDSs/DL2dQyfMOep6J86ax0/5cSyZX0rUL6dfvzkGiS1jpODkMO5FszhT1sqwp5
/i6jX43uR2F2ecEoRLQQQInY6UtZOM3Wzjio0qTIEL/hM8rQnVcGnc3734JKGk/XcaHNjEmpKguf
A40HJd1BBnfkekv5j+7mZr216kQuEGINy/nk76uuAlvZOHW2xZurWgc4F/3+b7XatlNAF/yeEuMK
rFyReCnu6x/teulj6edtPnDZLCe+8rHNa5iBPQJicjAVnWsKlf6FlWDCgoNPeiuWnHaicwW02MWf
c+OxdaYie6xixUthVSl90ZFfIULlqhJG+ykGPgyuR8MPl+Czg21LZX4JeaPA2ro2qm7r0KJdJDcu
vc2bv85iM1xVhnuQ6aPF5F8cj3ep5/LN4qL9CvJLPFQJku/TbrJuLjATKvpc4LzDvTmgApB0g6zM
yAdSfj93mD2QLsHE4VEJBTSft0VsYICh59B8+7FvNC9lwfMywE0hGcd/Hjm+8MBFTRVifesGsGA4
nBesJcj5rOeCRyLZoNyn6z4N2Xv1luBXL1U+lI07I/rWH1nzmtsOeFbVFdUm+JKHFZYhO9w/NqTK
P1tu/roqK1qA27UUzdlp2Qqow1D2dA+WrxQsoQZySNXnDXKEhqvASJBx02pj7mRyyioRTIXTJjis
64p/BaNZqnExrykM6KmYaDmMCjFMlObNDW5pjLzKc77HVgguoLwTJa1+sPdX4TtwmWRsXlm+mIaR
ADgzEf8fRXYvNUB128YPxLOd9ks4jDDBOF0zRWG2V2b6HNAMWWHY5fqDIu7f3a9J9iHlmUQQJVAc
n0wt4LJlzUprQ2nTBQ1O2VD3F1KhJaPvSHp5RFtIvceY7lM6HC4Xt21hAKyAsfMBeGaOJKe1LJ/+
SZDkJF2P322P3CrO55Nt+TLreeX9/MkIGukS9wir7oGmbvI2YuvJPo9AdmxAQgvGNjxQldcrT9/y
OZH4HEF3z2qmO1FQYKib3Wzy4A7H0OOFBKWaIBMrP6a+s1yV+n1wGE9WDY53kTrlVn/2ooSt2z3V
4nh5kdBRw9uTyck/Lu1SHD9YsTJVnuCC1xBVPRjJoAU1pRmgkM3PAI3q7p3Q8I99Bk2yk3cA8LuV
FZM0w2gs+VaxHP2cJfQuoGkkdWFRIwObvoyJ3njawCU8AcprFBBbbEPvC8V4R1bQzsGIqvD+zSy/
LkapugKf9Furp7u/8n2SPE3UStvop8dlsi6Nm+wVKzevYbEXBdp/Le7djXpoAn2EQuJnVRVfZ+Bi
fAT8I0i4x3WpqsaojGXwD6cEnX3D31bRFItKyY3YtTsf3Oq9RjdWwKFJVt1oZyBQl9Rv2vZqBsxV
Xz3LsPKqj4zWt+EbVj1q5j7yZ3nYo58hn3IY57KKzT9IfhChlNWnC75zlE+IE1lJoZWWpS+TC45E
faiUK5Tm884qRuNr+IASKWg3Ss9kAmtsSNb8Tm/tG/UIU84Ofio5YlFW0A/uVEr7CPR1qsr3n9MC
R2EnFjdKWrio5xR/DyyllN4HbMMoaYD6AMRIycxejSMM7aWvykzgtg5hS0kih2K50hzxC0Lgzpop
gTjtg7YayUjUk6VLjQa0X8b7ItTHsz+5jLmIu05ZZNud5UN1+DRkm5EcN4TeGzwv3R8JxxUx4hSS
LDqyZfKDocU8FTssV0kgN7ADQgnq7yGOhTvtknH4rleMwy5YHjzg4xTc9L1q7/PE2uRaTERS0HC7
d6TL4QRUY6KzROF/K6X4swpAokZIfCUSn+ZqgN+pqiC1dApMrM2u99w05fq4GkH0uEASi4D+lPbf
ebwdJcfSwLejB6fCyav/4YFXa5x6Y2TB1d525dsfCdnGcji75pzd+sKFifhqxfztcritqEk///6Z
NkX0vSUNySPe1Pz54hgYryWFtrRIk70muIxFZ2idQzT9YP8cvxoFSakGStsZ/uS0TAyqBA5JHwse
Ek3O1dX80YiCvTfhY1RB3CZvBKUYXoSYcwTlL7lmB1vCbB4FNY8ZEpMukYEKGlkyUO4+9KfIJ6wr
DTrmdnKMacX9T44Qh9Yfkvcte7jl6ZiRWjgv6KEBL9DyTbCUvA2Nq1qXzUdshrq7BBl5Zb7MfED6
9BcQhHj8/GiuMS1HCTdunC/UHIrE15iUUGlaDX8hbCXty1mZliKsUY0DSTzVotFHlKSLuswZ0YaE
6Iylb7GP+5alNf6USabmTAXYv6jAFTM2yWix35y9mAjFKWtRU2P639/2wVv4RIlXCtA8ThKIsN6o
YYrCCPm2wOFd0SkqpNsj9cYu6oLPWZUcMjtuIlVwoD2VWbRR1ocdzN4fpxN2Rw2eHeDBjmlTDrJM
F3ZRexknu0duD24wsAZ1T6Rj5W8rR97DRG7S9hOlUiwzJDV+izr+DicyttPnasA9ET1cDQqQmBGB
QhnYvylI2JXsY5jyXIIJ63LMfgOamqxstswsQ9mmu2jNMT9U8TCPPZomEFGQ+DNBS5VPeCJa+Q/P
rsLDjo0Pf7bkkY/I8BKIREHjtdc2CPOVP6eQzhCcLgwq2K7s3hdu5B1Fa30HZ+k2U+khKh1McHxG
iCfRbUO55OwMUWQNlOkBoTOqORtM3LNo/3Wbs532OqbAixlYaOOjaH598xy1JGlDrBocnVEko49L
N3tqi36OiVR9GoHFuQOW1OATuQ+dBjCZdWVy0Uf12RMBeR9+Gf6wWQtHk1ddlAwt0ebrHy7lbKNy
68Sn9KzXg9oM32rgn1ox1e75uNBfk5eYBgv0/88X5FbEFdgWzZBffOak2WqFqkV02heX0+3MYiLe
QXiaGDNR3BkRDz15whpWk44I754GHpcc32VVVxl6YY5ZojhD+RuwsDWKf1FWPIzHVP2gdLTw0YeM
Eo1du6MMGitW0rxTnxPoU3qux4DNT/RDqmiJuR8Ymc9P1r4Q8HnYLdOS9hhvNq/Fxdknkwm7pVB3
lfcQ00Hz7R7RfIK8XQhrw4qmywKQ5rtdYMvdVvy8jPtCoEfIvlHfi/yLTq+9LjPDyEppGKvlrt6C
ILSxxBlXC8Iz7mlvt6AEfzmWklpk3EqzAodQs8AegHKBJFvTFtotyvz1MdpCyiqbQAm3pn/HD9lW
chQp8LEw1GokOcuv/yJ3qqU9ySEMSagrx3cTwGA+8fxA5PVYhbC/Dr/GrDtZQHB4D6HKxFgxqpzT
IYh4B4egOgWkkDlteIsO/EO+qLCAGrdLZz2jtBSUrEZto7ivpN9NQkjWPJHFlpr/nMFEmaSViOWH
mGRbkitQsn07s6Y1oMIHzfSBkCnr4RMSdl5ghIbmdEAYU9tX9czOC7YAjNJq2BVFdr21L4KMnIxe
AqsFGdZ+cspjZwVB538ID14eZniJYzSdC2VyuYYUwv0OygVtgOJnIg4uR+J6H4MyqL7ma4+W6huU
Gq4CDh/NPQSFlShtY2gJM8s7GGUabNPAHyBRzKvO0y+UFB66GManz6JyIhWI61dlT0ootV4NilVj
ADgtiLc8iZB/YwXTxgZPMcHBotl1pQCku4+W/81sg99zoR1wIUUjTs6itoZ5QXxLzlyHSKn8MlZj
iyLwmmfrqjWfC0QQNFeuS1I4ray0ZOaj89WajF98z5oJi46w1cxfB47LbPHsY84vswxVblp2rBw6
L20QCjXiyWAG+FVXjLcSfTShTzbmSi1Vx9SaXpoV4vRloWhc4PMaCZPnyxacazDsm3ae8uOsTwpm
V3d9b9dVBhdksWUuiGn3vD3yLrEXFSKTBMHDiPW9QTNJo0iDAyohbtk9oe6Hht+gDUAWKxAAwjWI
y5WYDqA8ZRmUUuAxEC+NR/WzoWdsIqdEoW3vKrzb4iPjcQjaKpZ8gp072jTokANF5AmtWQVORFp6
HVWP03zbf5XTz5DdevjjL87GPwSx5o0seglNfEEhu6dK0ttqxUn8Tt3GwRxAOEiq9WTpVAVGbYhd
42Q1+djYQQjWlmSG4uQjShTYOGzNBRt9GnHsTbrz9Tbv49StbR9tzOYN+oZY3VIH83TRdxwcEr+H
xRln1vrNXeFmAaAed8pcWOrc1+upNhitb3aqK/PHxvF6IsJVjLhJT6Vioq/6bWgTKxQcWsX+jxr6
dhSqyv45gLN7L3/eWI/lujQp16h7JJdybrjGlOJSLKEU0z5d7F3LAj2py6oPFJd8Fo8XgXr+X50B
V97UlgcjyevKyb2ohF9WAWh4+En7re9Bwj2swe9DEDmxjGupaQg/nCsqwOwio//LRGzABiYcgTy5
ZQ7c4gdXDPawDgGoG6RTwI880YEx/cTaX0J6fM208D3nWvD75nByooRb47eRcqKcaQ1WxlhGl4fD
45x6dc+3dxPa5qjrh+Gu6alIsR7/meoEPfEeD9QMGKyPT/0B2BLPUNc28CaZwjwB5I2sDLeHxcxV
dKGaGrJlWC+7EbZ/McFTcEmQELV4wZHp5HIWqyIRHRQRfmkotMROJiUx6ehdWWOoqS6f+6B4mESM
yGNJYyYeQO8R6XMsSgmgVIAsv5iIuGqvUHureJRP9je/m5+mOAwrVLeIwqrMImz7FCdtv0pIm40I
RR2BwmaGi5zcoQUUVWKoCJbMD+eMkTskVZ+5ONf3PEIpiyxwZmB+4zMg/wr4HQupJGtnhesUXv0w
7j6Rre8dfSZOjEYdrzdINqW9cY+szhZ9hOA7/i8St44Sin59agJB3qMN+XIv/QG0Q2iDEaFbPf2y
om8aib0pK7LG/CnRmS8G/oCCm0H2DCYQlY5V0mtJkD46389jXdj+4rtN/1fS3F29fGB9/cu2JiOq
A/8eixevMpSjJ/cJ4TfeVvuToRiaV9jXIU4kVLfQIcimOb4Ca97/FvuYE3qwP+2riHxBWJFwaSB9
glURmo40fZ+f8v3/JXrwUtyZGapEfb4NSyjKUWA7Cso1f3ik49fB9Pxg5Q+6zWufhHCifPCai22P
eABvvK2B6OtyKZFphTeoYp4s+rfZBtW1nmQKBs5kGM4p7jPzi1K+sSiUvOKn35aHrl2W3eyFuuOR
4ZBIHIcxtF1bsU9Y9M/vHZTlM3EehXeGaYPmL8EqNUz//DrRtsJbdS/joifoR9Y2mwfifhHQCwuU
hPAYXD2yiGEa0LTXuMy3Uzu+HG35RhvGqJv2w+J1STdlSsjOfIryGI5iYYQu5Nn0X316rk+zcX+o
rJvyaZyOpAsdC5NBw6b/wy9xqj8Jyum5x5gbz/TeYPCpkZLrDcUUn2v4caLhEQUtcAKY+UuBDcqZ
LJFcsA/ePY7y/CZRwgP1P7ZRkzT9yc+LREm+4Z810+6AHd6zUeQcMRjbVzftF8pq2XiIxTJjpKU+
tIhh74TCnO6amniuAu4CaCiKNPrHUaQm3LLT647S9WMP1rLwLxTR62GIquYp/HP08wJDizDGt8H1
JiqwIjPPjtt2srfNQ6XMkoRs8kGEVNZL2KNqBH3ER/S0lp5bDC9f2xIAUpVKxjKM9SHWVOibKUPq
VxHARitKt/Rz0MPk14smg6dmgUKlrdqFQZNwymaVrvhRh2NVe56jjGcSupGJX7v3RRCXR1fZW5I0
Rb7P0gAaAVUkkydUJndI5fSd2AJPPL6lFmvmCJP8X3lsVpkvT4+fETMAdtUfaGGnIPk6C2x3hSoz
ELmRVRenuFHL3blgKwhob02ucIpV0Khrd/bge4olK7xgzSdN5pGI3I4hCUvzqIugT4g1pCLnyAaX
VhSWsgvMyXCuF3bnn07MShThOzF+pWetPh8TTp+Ul4ZRedGEI4lm0/TQYKLKZEGQ0Hcld/GjE6Ta
YBM5bsdhUwDQtQNxo1bW8NT8aJvCSIG2QRkdFT5n/kBtQH4JS0O8fh8zQ8LWkaPdaXzgalxhsRYd
kTT6jvVYGrSA6pqXp2QEki4dahK5wB6AFJPJ7/OBW8XG+j7Dmz7R6p+lrpZSHI8VhkxRpJ1h8jGg
CJcIasaGUAzS/0UlkiVQQVcEgkVkl137O4k+a8UcPY2K6uXBrZDTQrl/LqRHbF9QwK+HXLEgrwt3
diMF3ZCV7xLr6vQZEF1lgUx/dzRbgrwnb5lrQOjVyR75XRxK+1ootJpQAvtznBuXXc4iRkZ27Bk2
y3zMW0M10ql12j4IKIVA3m5kPTs3jOc4c+QmZw5y03DSwEdONOPI3Qm23MjrpG6InhpUXkoNHaK4
kNaUOYvks6mNU+8CLscpqBXA+ar9CXcCNwWEMx/uwWbcMBnjS461NCygBrwqJUdAkW8IfUu4hTHs
Nevf2IUTkhSS4iM3deA1mEst19t0MgyvCztdAIY81qiWwRYHC1RQQqhi4xL/ZZ6ExJw+H2yNecMg
Ur4ZVwYSNlzefyNxpWE98cND8EtS929dRG1u2bO4wEGANgouaYFJ9SxeJl9k4JnbVywIpiYlTe9W
vfjdzrS5lpo5WywZ/TpCGJPhTILFdDFRLZ8fbuzPkx0Ts3L+uRmCfq+msqrlruvGGDK7BfRur32J
W526bUXT/n8RhXvOWTlY4ptbQOHqnn+v0QItkfG2FsuyYktspsYZpOzMQ4f+4si/1k0tEgo8rgRd
5F9pJJJQFGc7/GOkjYXCQZA4b6tZU3jWy1oaE0tMFlYoyVu/VSNFG9wtAkvJ8LaEIFH0SnT32XIV
aGzS/W4snmnIQyGL2XGOeRgxmtDMFdEliufhaZFvArOdFbUdW0Me8Zuz0yidAz48qBYzhV3AazFn
4JYt7ny/22sdiCKQt1wLJM0c+goVnWoSe5b/k54LC5yDATf8AHhDENRScG1kmzDg6R9sfKcq04MN
bO/OKB1YlwkVA1YqTrocYgnPR14RqjRUJCmBXray0OFKdSrevQ3sM5MFCZyET1K80mfCzXUBg3RW
1zHglPkiaDtB/k/LgaYCWP1c/5pTNTfPYFQwO/0WrJe404Lp1gW8FinDOwCiGiWhJqVlwi1YfbbT
O6pN+pz1YTe44W86WpjcYmMLcWcHJphDay92QNi5KFHQO7yKWHXw14OdH4FtQVwQbcfn+1QenLq9
TedfxYWWvRFn4kBirpSLSXf/+LwJCaAWVe94oAwgi2SzmltNFmb1jwT6vO4SXFr0zWCgV/HGTPd2
tQczc7k6NQUxa4ZtqOmCj3gGqtWUWEZ0opnCXGrWKkPgUmiP+hyIKsBeJuNzdeos/ltfcRYofnxP
PP//Wi2J91lg7dhxhQN4VXI97SGOVIMM63gJIamvCMLbLH8XWhU9GCw21t1ErwtLIj0j6b8PoDaX
j3/fxPaV6kOZDyz2Xd8eoq7WvuGwHE93eq0qBspWYE9hN68c7IYgB7m/dfrnajIwNznFiprZ+fm/
mo/7N/LLE6+qG9GpLJ8A7xKeQYZPYwcyXJXwQFPu+OsGt//pGK1tRm/E2qriTk9Y5KOTY7Af7Og3
+QpAFCXHaFfRzMWLiL7PABQxTci3KpJaD2ndIuNxZomegh6f/vkNuj8V/4u8b1CLAxtibWUob0bq
w2n1EFmr6HD+T1RHBv4EiEDYhDOFoPVWhj0+k0ld+xCgc5xzfi8ek2lfiQVtmN7WCLMCnUL6yiGz
0gjiNXn4s8bIRxdW1wbDC5vbYpv9GjJx6zhI7s4TmDQ6qR6s9fVHzCXuZdNImh99QBJqTmSy3uji
NBpdoKDvxpqBOureC/kbBan6xuTsDy+dxCfXF1lrtx8CkOsqLKXuabmwQuPqJ0aARlHM+9bFazRQ
jpb4PvB8qmhFYBj2e5238hVFbmBTGLqeIMmxcR/fmOC5nREU6Yic1+D87RWkfW/pXVGgILFwXVxQ
dXOasBUlywsYhjciRgzzQP9joAxQVNIul02kbu0ufS6Y/Zs2KgQfeu36HbXxvFml++2nOZHB07Mr
cAWVwaX25wiTnNxlsiPKWB/4y7I+d7fopmoC1RdKrmLfBbJZ0me8lPLP5KR4b5v3i/Q16NV4XUSz
H3ooYQFaa8l+D5VLrNzRY3nN7ghXqlKchn++TsEl08ohdf++nlfXQrhIs3s4ws2wjZ3iVCZheEXY
gyRxoT6Dx/zy0hLa6HUdSrmge3Px6JJoaD5CvWLeQdUyiSx+IhcizC7mdkPZ7DdGjQZ57iZvH7YB
hIxDnr59qsb3CQlT4dpJ3/mAc9JLp6r/rSm/keGznORj1k/ooq3Y9mxBTu4ondV4OiP28RE9Sghk
FcLd57Yk+OqKIKA3VQu9Mdah0SGZVY+81HNUCwIO7UFxy8Zw1va4PRp7w2Opdhj80ImY0NTjNClt
Mj6h1Y+BCml6ATptLgVu+5YjQTV9VmzLInJv95b69LvT/fjAy9wmvn68LaMJZIVnUyDbXEI3R6WP
HgDVEWbwPe1zKFBHW1tiPv7bIUfEEP0x85ZUfzNyxluBcTwQIDb3dX5jHd2yC4g150DggRTitN2L
MS5XZpPfTTCmvkO5iwP7TMmrhDrO8tZ0Y+UZotJWzOyLY/arpTGQAWuWTqe9cu4DhU238ggxKJMr
l15MKk4N8iB+DbV8z1bGzyJtkgWYmybb3bz50XZ/GYeOPRwsgakkEIVAvQYBRWfb8H/PUzACuhZP
ndg79zpTJeiEIzMMHW0iBfgmqKd3oTDGkq2IZhFuqS/8IGnBiUXySy3KAq6XtQPHNEBAKapf3L1H
b+wycd1Ca5wB96CZ+YkmWrALFYwltKT/PSatNI+XNhI1PTR04Ql2ZdW9U9Um5CfbfbpigfxJDKip
z3yhOTmtddYdUuV58JBH8FE6125eQil2YglS12TIatP9cll2IYt7vhYTCIPE+3XOqV4nRLGoIgJ6
DFO9ImDcnjLY9eoa0VOWfabL0UduQT3LYvdtwiQGXJRG3IbPSc8va6hs0zugAMc3KigMQkIug7PR
nqK3ozU0iT2XfVRNbmHhJyMpL3bz4V20pDz2Ia06heBfzWEu/7Sqj7S0D2TbpyWBXqjO8ghHK85l
segmjVx1zNkZMlpPMJDPVee0mhb+MLKpovf6SPUjFzbKhc45758OyE/+iBSmjiYyVnsVHwDoGzyo
6nvb58EDuf/84iyqr7RnGLyJX7gEr9drHWKZ3kUlaLzVVqighfgYY0VSEwilAQ76W1D1GAYg02Uz
3nxNrgjjp/gK9Z1aLNSvn9qdvCeGxKTDHafOtA2hmL/ox2qJ13rXZY3BSdSI2qoic4tlkn5xcE5i
7zK0Wo5No3he4EHj9qkI0Vkoqa78v7lz3xGTUkp+WAmZJUio97GpgqHIwFUA5Q4sy3JXCU3nOoMp
5TXDDhpR2dy6SVQJHAno3b8G2Ref41kyKXmJBGRlQKrZ1ZE1f2qxLYLTnBkc3jyROQ+or0+Xu0yC
FlHPXraj5EK1o86LKjYh5hXuEIskVifIExvNaM9eqeX3Oj/uDEZhZzrZzfxxVQsORPwIyPywLp+v
vcrv7r+eZKVNa7LVAo416QU6GDn0dwGao7laXBepFfP3k6mqUDO2T/eMVvoUXWLBhavO8OSlKDgc
3ycNOIEoM8wNbfCaEdeOVX/Il8Y526gWjph/tosJFOt+5yUwVvht/67krqzeRGmykCnTmb/sA1hg
FDf/nix7eJkTdBLKRnGNCjTgAR1SAmWOtz93uDPYnveuwJrtQ+B1WOY4D/Zmjhr312p6tznlAvwz
QNA5vKenSZJvlTa14tzxPP3gmOI5DBX56RRzK8hfFChlriwZhEoGax+qg+8FREoZi4eXOEsQlBg1
KoyKxd3U2qcdsnsn55DOcv8dcYMuGkQS1V9NnMvuzjG2acZHVdLkLgqTP2NR6zpda2fXksfdgUNh
wQ/c77JoiceRKQ9j/cxsejWI/7JH+IebpWvXBX8Ai0QifDrB8cR+/o8PKkt9HPm42vhN+OFtMV4S
HqjA+MUyLQlE8OnSbjXJOJ55atqw8zLa0eMiRUDGa4o2LFSLYkNKidu4aD5Kl6KW9roTonNm2MRt
IqJBjG/QcCeXAZK35QXOwyAzLAqRpt0ENvbfImAaFkojlpuX4WRbG9qAnhFib5AtfPECd56BirqK
HbGaPt1DCsotlHpIs7hbUj2HS6668O+4xvNeNAf8KUM7HfrvNFB7EMVRFnAFdYtQb5PTQqc3aEMI
tQxzy7J6TNb1B8M9nmVNMki9klNFLyfvA5ji3URTioamrqOZieDNSU4p/1jUGwTny5MAilRdm8la
Uc+BtNLYUWknW8mb19bUmBa90+wVOQ3iL7IkNwr41bdxh59PktKQns00F250z98PQcN29UFufWGu
e9iA2lqomXeKeFq9SszR2VieNC1l3OaZ9iX7MHER8ME9NrCbemDUMH9Elue5McTJ65qUkQXIN50O
18YIp2KV0wBIAUG+xN3ykyxNgwAZn9ntY7KV3cATNZcFolJ3yTaCnAVpSAhHJH8idw5/o9c8ImJh
CXeT4XhyytpCC8UnWrO8OM2thF1UKOJZ5pUMZfTGH1oyaKJ3SXBClfVT9zuST+4yoDve93pVYAyO
NmXmc5pRxNAqWI3BvdXbSJGtYCRtJ71OIDii4ahOP4ahUXBVUJMNDTOOowjsjhGhMNt78CLLa18E
5fE+kaIXmoGFmuO5Vwp+9EuGzzROVKpFOzN3ktKo8JtEptJEWdblCjyFbfEF9nBvLJmsBd8VL5X+
kqFTJFaFRa0TYSz8G60d074WRZ6ZDFc4N1l4IFYhUyfEqIzqz5WaPrPZw7xEIyNIEW342ns5YKz+
xjHZknX/eJSsOeOqFtvu7ZbnktmDRE/f3vcQzCYy1i90ISX+mqAaE2IgS0NkwtviISk5gzTHxBUj
hXADrgCTL6bw/P2QWJSlDp72N0LgY3Nvkd6pKR7mYv+ynM7M4aNBH0JZWJA49s5XuYzsU+be+KpQ
Ikm8Vmlw9d+m49QHtS3hT7iwkumwotuu6YgdAD7Vid4yC5LGvK82+T8TisB4JIOGcvy7VippElXJ
vzg5cTtbp4/4sIRrcpY6xS4ab+Q6MrFTix7bLlL86+62qzPOPbC3GJhu+sa391/LY3/USjoRPZ1U
g4oPLnCcQT7tUM9CZh+uPThIbTSZdAUWLYN4exOgZdThIJ5Q9w90sXo37H08qxi8ZGGqyiKMZJLC
oB2T/sJsjRIZVGGU72XHCT2srje4J+i6OqRy4gB/iS4lfSmPMN59fJZrmVMBLonOx9oqFvXXl0PG
ln6LbI70TATM83W14aZ6hxugMgxH5OkaNVDybM2bokJo4gC0UmlzHDO3fSOgvCQn12pJi2uSe65R
pDIE60W+70/f1fdSMRqAmhjRn7gn59cSMLQq/07Yi3Qx0qMMU+3sYie6v+8whZZwtF+w4JFSKKdf
G/jrMmpcrz14WYnMjqu7Pqkbx/c8cnLxh5lsmzBqllVqvJ/789TglqGoS3c2x9Gb4Yyp8usLXdbA
AwB+gxnLKs3FjUXUs10au0HsAeNvo3tVZh3lTG4xX++esLLGskmrrkmuRpF6AznxHiRiqyxNRkj+
5GkY5QI5vlm6kU9max8RpZsUMhAu7An5P7dqLcQlqMBGF3MK0iobJSP8LTOBOiWzZF1KEphjFNBK
czD6onRbPgzKZb//iejAErO4SMRYvabZpOMrJpYjssg4v7eNs2cuFAdZUfSyS0DtlWjTiPh8HIT+
wVOeriZ49OEcUAHq4eao40RU02CZlEy/7QlxfMNPpbgW10eNoj0yN5owhTckD+w8BnZoah+3Xsks
HFeWmQO1rW7Abprtmrd5ltAsHRFxifOQaEsWoXXySvcZwlJ9RuxmvpTwuENySXL7KTwOd1pjjDh3
nuvYLymnzSW//6RERJ0+rdSFbmwJgvFutG8sQOs0B3YMscZOm+6aCfuRuvmVdvKEU1xRWHMrjKYD
UqgbFV9mQ6Z6MkIAvtaLYaOnhin0Ptub50mInhCQmI7A30paF9mhPXDh8v9Kegf3ar1Oc8ZX2J+U
9YCD+uCFj3bTQxZtkHzIS5Vm2Tq6Vkkdmt0RWOPXnnGXB0MgkxzZ1e/But/3ShMFjTA4D9lspfWV
vPf+OlsgilUtzwFJi6Sa9pF4ZfGKLjx51dvcS3QBbqh2D65tpqj8LGOqr/3kf5K6SIxiqyTbqlep
2kUj+xJGe81mkdDJZciarulDCDBSyuKYbVFOJbIoMsXXteiVTe7ZVjSp621j0MMYycmXzZ7dJ3z+
TzKoPJj5gkKlJo6SjK/mEqPuX3acnXFa7UaJDgNRwycZN0HXAEKSQ+4DMMvi1UhfCBSycMh5ekU1
QsVZFUAtRn5hebhwIgZ+32qkSct0CLXGfzInIHnH01ysCmE//k/UKh/9jVReimEOwhtt8iUvLsgu
sbRSc6KqG8+dlBSagVsy8GXS0eMO/wUjn7IjH7Jh0PQ7uz6qRSXTO8XitgNPuEJO+DUTJXnJlbcn
oP5Rn1Jit0YTbCny04tM6RJalbUSU8P0JcKmvjeT53/Kq4P3C6yiFzFiY8JEDoVm/4Cw7c+9zUPU
AtIBs3RU80AuZkj8gVgmiHr0amn+XNGzoDL+K031mK07ttlozE/wPyzukLQuqE7p17uzv/nDmNCY
pJ5WziLVoHjnBkFBZQsgLRWl/tYBRDhnOk14WNYllDFB7MSHhDXZN3aU8H6EFqdvp3VXYDtTWziW
jEjI8zApVwb8MB/lru19iCjHBuJ7LfZHCy0zkdNNGhxkTKmIDD2hJvZ58VH7Dd4S49iyS8gXlM4b
kNw9oTaPrfyxhrzt3fVdHMB3bQs1d4vRs3eQjtOThED6Cdr6a1MKbht/mM3fRsRxHk14Aqy2ovlj
irD6I8xz8wiSVl2pcf6iWkFTMu8jUTUPbUlMFFOS3K7ri6q0yvWyPyidGcbaii3167KDMqKz3o84
rsO0EDc9IttLCTir36KS/prwciiPxL3lbCF+v7cOaW4twv8fk8kVfkDrhfWykgIc7MhqqsaIkT+D
+794yVOdAdkHfSrq1gGYq1LBubvwo7wO7QJF4fzK8T3JC0mBBYkiHbWHIhy14eiTN5eeKNKTD4+Z
k7QW1FbsxvSVvIdrWD+w/z1Q3u587NgzKUISaIACNv6FwjwyskrQWC92Fqzvvi/ORV+AeNSutdhW
4sfH+5gK4+AEfN7IkKSKKRyEwBOxcbrI/FHro1S+OOPZrmiqMcy2UwjBqoKSDhb+uzrq235Q9o/1
Hj/yVY5GIM/2pc2FQsfSh5eRq0C7+WInzchZB0dsmVusriAQWks406g6H8ffFSE+Qai9kP5giHfN
+g0sUu1Uc/Gm1dMwdGMM9u+Wp1RJxkJejN/Vbqxy9kQxFrA8z5CQfUAfVAc9qqKzD3sJniYmKSsb
9f4D42okVVQ7BpJUIRlDHvW/ZuZ3XpoCVeqXdIArQ748A+Xp7LuoyFVoBTf6D5yyaW5hnFmPrzdY
BNehgfyGAZKC/MQIjU/9/JQa+ketqVE5yDTVVsu2YX3dJ9ftHyE9LkGlZ+9zf8m/Rb0AA5S9mAn3
IPf9rQpafomozhFOqhrrISH9vaVQkszVx6Rzz7p4a1YnSq1ss6i31Hagej1CJ5yZ79xhpMP82TdJ
T3AhpHI2Slf6jObm+hn6+x5s65nulNJqjy8vQk0OYE7ueQaSQgu/6ZAi4UlhGCaXWAZEoQAKlLya
Dl+jGjn5iUcKkZ62RCO7d/a1eifzWAMvjDcD9OZtjc9SAINUpYCi4/dtVlRsUFZNKIsIVf6L+4BN
+yU17d3PkzAjHvsOBZ6jjkwsaXc/wpJYwjg0gmI7wgtT/f6UJhTPG+9ZtxCWA3lM/eV7OWmhdjvL
vJP+vo3KGw4k1dXSMfwHN17LRYoME7dDj9h5WPEOl3+Tw8vsQT5Vn7wxfJeV+Mvpoz1wRms9doR6
XdNVjdSroRMPExyyQJPlnBGlBfPegBiPz94ys0PEBLPX5/O1AxxSj+eeK1SQQWKRCCzQ/GV/KvPu
zouHO+mbIiOySZELU9yFGftcYWUSZRObu/cqDNvae657uE5g2e5bPS0PROP2+nFFMjZ8PB3dgY6n
ArIG9eQCg1LTuDMRP9//JCN4nV+LCvDnE81vLhsSV5zDBzVUr6eYKEwgXYiu0yswzs8eGinh8OrU
v69GjwUl/KLbJ/azKltpuWIFgaXKXEg1M39Z0fH3Soi3a7dyxm14lhe4vu2knn+cpjAvigFW45sb
an9MVAGSszC2J32tyMK5UT6Pc/3cIow+uqRTSQ6ioF91M91CgHY55e2hEo1s/Vo0TQYZWyG6NhQH
H05cNgRy2DGjbdB0kHXQEcv3KO9LKd7mD4xjLWCz2Ks2MP7CeIh7lfchhx5nnm4sV0Mc3y3ecIXk
HTpa0g8ca+oO+B4KQqHPPiUP/gH3yZzXrDV8fnUt2j8yqhZbnyZ0ZLaEIKyvfOhL2bdyHBGYOLH8
tpKiAfsVRUhDxT9tUysjD4RIN2kngDzkhNe//5SDKu/5iHkGtc+wskCsY6UmcDpgf0GKVp5+SYXh
6s34rfpPQFX8ePhkoeJp9jO1iEvi6LW8dHS1Au/knUxdFEymMd0aBjtKhwOM/hqDv+lgIVzorB9m
mcxmS7f6b1+lqEGuemurAM1niQspfCyN1JRwiUlNig49CxiXOYn7crxvV4Ng2QtmFfjYte8xfEg+
0MfU+IqqYMSmIoWuBuJnbIKmwmlB7M+hxUrilmXNym1t7yPI/6rAZQnuHKvJ3V2IaL+Z0bdSI/QW
9pAmBvAhOWAo/BNW/NlV1mPHk9P9Gda+xrqOXH9StjFWJWTUPVPYvvjyVuYZLbVh+TjkjOMr3RIt
H6HTRilGK/MUmSwCXQFa9QhaqUcF7Tf2TAbYPNLNr2RB1wBQ91qJXlfMawlxQp1cS6ruV1km5DZT
E3a2136FOBQ7yTNu1/Yp66VCtVlDWCFpkF96soLPGQKvbElWpFEppcLCY54x8iPDpy957ATRozUS
B1qeUxnB19dOHvHT8Lg/I64NwzaclOICMIqEVjPgTXJnyF6anagD7mNtV2QJtObR1mPoWeBr6YsL
ZpnQ5QOGrk1URJHbbr8rGvXIRseF44L+qVb+6gvkKh81KyMOjQUJciiRV9glG9ZTkImahvFOhTzJ
n/jYYJ34xP2+keDTbMpo+Tk8BkazP8T9hanj7eF9SrIDYsBQxEKwhcHBBF5qi4ozqiw4NdH2Q9pL
giSwTcoCa1T/dfySQve+jVdr5PjM3mGK+h9EiCiq8GY8UJM80sOhtS51odI70t+S+eoJubd5j2/E
P3EHAS2DDgyEVOHhsXZrf5N/bA4PEtTwmImEpJdkYD9NYMDVTOKyTnOe6f+bXrLBI06MUziW9tLr
orA0FQG30vEPNnFMlR6g/yyeL1KY5/YoSHpLFbRmFmtHaR88MenrOLbNsdVaXQuf8S1xuF4S29jQ
RximCf980N81lP/J6vxztHfHPcTpzZbuLaeydFM9e2o43MYPyRR7h7CK5Ixe4WxiZ1pQK1HdyLyc
3b9MCxIQBP2YqnZlnSAaic2cTaPq7OP23y4wazqTeZMb6TqdgwnWI6SYQpGjclHPLjRHerr64YbQ
S4MEdAt/riSC0lTDA/WqVMCEaVy/foozzMnxBr/cD/abj+Ytd1Fw898QTeTckAGfC5tzXtjZKFn6
3Y2ZwswUwTRjQisAK+S7rE3zqb7gsAWyRO9z7wr/FTBrL/FohVwjUFFS+CLNX8oBV/wRBYKVemrR
fLNaQjQgBPdzOdeKTj+qlSywd3pm6yeXflqNP/zin1moQ6gcEzdD6CeHJUbZGYIHFh3CZN62frsa
NT+iZgrmWfxDHTCJYydffyjx9yIH7h5eK9GtCNgOfH7mw2gCwiSFBc5iIfwO3DW+pMi3j2SIGKou
VYGU8970e2m1B/kxPOHxPYF7Z54X6aR+HbUetp6EkMFNuKo6MgeHz9AmPt81fWMy9JonEzERaKVx
QWQaKNvLz1iYTAuCxYK++HLQPnbUdFl5t+AL78KEtG8Gk3Z7YLmWHMjjqcn28QPvJG9FYUUaU0eU
evVB/GC/SWPzs7mbdVYIIB062ojH5aEKMasYK3l4FzO6TFgsQakUDPw7THC1E1WjBovAn7BQcSx4
BR7ki1ltQUvG1Mr6vaX5BGU3TEvLBdqfQGDTySILxMF6NVYeIIcriwFc24WDC8YSIeKLexcQTFKo
81u8DNVSd9anVlo6kiYhgw/4Yg2Hp0sNfTvbMRY7nR5OyJ8cxL9JIoD8q8MKSM5x6N3ovF+ardNP
L9hTV5RJUn3s9oQxwMWaJvxBbwJuDkyHkpaSVB5AayawL+inEWoDAul0/4DUZqgU8BjvmTaDS9nt
AFouNh96r7FuL+jv3CQdOaeII1e6Iln11diYHm2WROa0dYU2czjZ7gKy1vCD6W2T4FS6onl27uZm
N1VX6bFB5A85YB3wvZqriIhuVzsjokJecdIaoyGwwdGwX66hcee887HQERkOq+FZmRpfx9z4ggKe
TLWBjM03hwgPMpZ3GR7eCoNQHsgVynDiaGEZSOLrWMZZfrdca7jol8ZN33UtK7CSTwrqpQhxK2kM
JizARV4ae47zvxKxb3CAmfbdfMy4SlV+wyXeuBSr+cP5fptrtO7ht2SsAZixzZ5dL+7XIT4TbeJ+
S/wsOpEyJZF6xjcDWLFJf0qf89mGAUMTlrgLURN/TRhn3Gqr/Mqo7gfi5ZsTPUa+Tlyx4rC32L26
rfCH7kXXurFVTtmHXZlvhZLiSQWw8GmnxgDgih3ZQF39NqkR/Q/QwavYyPbpNu3qjFxzBNUpjmUs
NK2cQ3HggIQxNz5K2mvL+kzQ+WRl+R31e8hiyFyfWIovxxd0HuDY+SdrxERiKgRoojF5bNkp3/eG
y5OAEHDtkVVj7uY8yPlOew+yPaGJuBLs3CQBm9ER4dyFCif/EMBN35OluQO9BfbY099SNRZcz9cB
b1EDv6wm8FU5L8FeuPa/kjqExrnLbhi6Xh8JP+2jgFvABwlsevpl2epxABjBYPrZ3LvHx99BgLix
SaKTuzb0vAaM4OKb8gD5ETvsu1QvCH/7F9KRgjkXoQa53YQdtTPw+SHzPIxIZbrlpuVa67T2bOi7
eXdv+Fq6IjcINk3XJg69KySTAr5PRibyFjCLkRjicHfV1D+gF9RlcPCGk20oZmp9kFvKg9U++y22
xGf1Ej8xipPxvkxQSz9H9sDAUqQtA+ioKUulamdYdkMtLTl8HNcffKT4igC766wgs+Im3o7B3mmY
XJfVDcROILVBL5NSqC/pY24VdMfNKM7pY/JL82jY/29dbT4Do9kaid8zmJ6pHqNUOF6BfYgfnVbo
nzxt8JP94CsJknt+yAGAq8W4WKtUbxZgOWS3gR7q7593EQzma4wCx1i3DQq/nccdNlAwuHjAkO0t
GW2t9p1UmHHdUnnmTYobOCzNbpb88TUvCv07/G5fvdP4YP4EQv7QQ/+AmPbEqCF/Fy8y5XSHgP1G
reG71p5D+90Mmkd58ZtwwC5wrwHQuJ5qo9n1ncL6LigZqjeLrjiMVpXcAO31z6T3N9ISkf2tfryd
RlAhHfsxgTALbxdSBzl9WDekPQ5fbFnXjSuu6MkCasrLFWCiuyVJohPYPnxv35MHBXdU7RKC43sX
EXKJQsQNoA9m1voeVv3yz0zRv4OzLcH+VfYyluS1QaKUauMXBxn47c7y1SPdNG8opC5jaE9i9lbv
WrKNDAnxJfwknO0G4NJqlL/nFAQHXYUJUDOey9oX1tv1Z0nigyUw8Bzdhav1sCLS4jZqp1PaA6DF
QE4vxOKFgmBsT9F1kqyEOu0Jl+8stjfRj0DYhIaZjXbFjzckWf4SWJWGPPZTFBe5ntCqUgrG8f85
VNaHt+iqKliDpRoG2BJY2opwTFMY6bNGL5YOMDeJX7Wppsjr9jFM0PB7He9+wy5WF+VDooQNTlvB
xgv4yQM8SEpOzcSM6tRZElljokHVQHJK8M9QRGoHodfs1JanUdxiS/vVMinDDGSaZFCrwUnpGKXM
OpIoZr4aYCjxQ5bCniUjBh3+oy55IEWR2Dk9pQ71EnaTMkIV+/dEoW8JcKVyrWZwZoB+EXg1BwCE
YXeFXtM15orUjqyuDjjnOq5gKQcvCZL5z1+f86vaiL/7txqqKtYTrb8Ry+Fkj8HuPCvA8OVjbdYT
+9ftKL1ySaL+vX2CY1Q8eVBosrsZcBArOvf+C0oYng5mOaZUIGMCmwV1d02P2GzbP8GS3jsUFwin
Khjw1NCWdsWQ4qgdGv6/xADRcxvFERKiqbINVSr2t+da9R7JIh3wyLqO6JD5jtBCYnGvCctbXKMv
aL1psVHhL8uI2rVPco/zUtvW7iHZrbs6qwnqcJcJ6LtJvLCErwJhwx32qXiTRwPm6rFVCnWSAqGz
CBOHcjhLBFVvSEeYvy2sgDSgS3Pi5tJWwaEpEObLCcK1OQZ2TnSeURkgkMkQMcqMGt5Ydc1YIlyP
BUHKSJ202yXeU/t2iCxlJFi3TP4hoIvE99/blDPWeIvVu4a6PuEOZ/0u84bhpdlcY4d2KPA7bT0m
kZh/VeRkPeSuXbB0+33y4HIwHNgcBI7mlcagMC6N1Mm41wm/4bznpyP8SQTYIS29CNgAtcWeSxJF
/CUSVJWKbI1/bwwPfyvSELI9H10qwCK5N+yxMFMcbdpmQ+Y0DDACr45jKHDr13BePSyDB80CylIo
Qhn2gPTEtbUtO5faFne+RHX7rLphIKILqlkSTH41Oc6v1fJtuRMRLfppfEjoGzdkpur9f32RqF1P
V6tpxDpjCrAgb85xkYfq/b+BNTF19epHQdEhz4tvT86FNSFVEPAlLYiOU4bOCjc5w1Dhqr5nlOKn
mAx4Q0KdRiQVA3PLvawrubh/SUOrhq63qwUL4JRE8qd0dfzygVl3RGD0HQxzEna52+b987sCugWY
BZ/axxGOn9GYRBongDIRc/YsxhV8aeCfcNNEkuqij5ikMzN1IL/UBFxgtkp/7MP704oLi/XBCR9O
fL1hYtay8a8f78KmBB89vKr2YCbBUUCJPgT/lIcaQpntVAxRRnc4IxPFS9iumtUh1gF0Ni7g5aGC
GFbAh6/MvHU1pUICJwjiqF4dDf3qgCF0ldSi6VUbucmfTeAHenTKXZMx2b8VTHfNne+G6Coi2ugq
TEELsETrqOMMZUbVdNZ4ne03fqCZ102XRfNZBg8mOSXeYOc880abbrDhKuzrRjIWokrs3TEwcxYu
X7WB246ZBkEiqKNcYl5v0gpgPv/ia3ua0ehAVqqWxPFm3Ggn+rDpru2cTgk7UFtUfzFpI5zYpK3K
p4I1rdIEN5ayeAVY9oPkJsyRNTiblFp5A4xHtpfaYlSvRpfVjd9nrH2TZ3OALxoc6DEx6rjHLhDT
Jr4hFvOq84Dsk82/ap8y5ie4GaAzDcA1i3VP5rRCGI+PPrB5/KKJBtwpO0df5lv4JUhBhAQ2HM/b
JwJmvDhWczFko+wN+CPNf/LAVfAMZvx81v8jHppSNS38rzefIBXMfsk1cmTDl5nin9XSXlHyxR3T
JiGJiR6AtFmRcjX2BpxlfAbbWqiSVKyT/7xl3F0n01uirhsS7b+ZCeGd6vBgLm+BZ8+ZnLDFPJHs
Rip2S86XxPsI7wcKvN5J29IVtZueQJJkWVnD5Rpcli8Au+ALrPLS1Pw9o1LcDZIMsA/RKcZC1eSs
/pMnO09xlkR9RiGdDpVT8g8koXWKIhXJ/JrNEyiNmRG+bx+9xccCBSOUbWIgBUJ9qLZXismSlMQi
wzZC6xy3GRjegkwWa8YUkFTxRCbQ7t9rLHBsfkMFsuiiIa6OHvgmjW2mg5raISTp6Q8J8QKP8LK+
5KH0L2nv0oQGGoGXPGdU3Y/rtEr6Y/w/KqwEfb+hdrE9ZyNchxmQIsKE0FwT6KTfUvvARzqis92n
Sy0U4rvwdSGocIaHgtg96xdCtzXnsDLMFbfxecreHFJDEayIVLt7mIu2VlZAKkcb+1bQ60n6zUoV
rmp2DxRNNHwfW+k3z7mFnDhtrWgqTkLZDT4RUQxWVigUoFTuVMlL78dkpeUZzsiYL2S9BklnqX5S
q/b8NuMk/DnJKmPtoNaV3QwKVDa80WR5nJpIMtmM0KGjTEI52ovqEaXozRh9NXCahf4PiG3HNusF
7o0cVH2cidmUepLdVK1y6G7hMh2FYj99ndmEbeS4ZeKDo4yCTSQt4N/z/BWd4jq/pPbQc6GghLs5
URszEnsPJEAqrGuWSOV5u2Y90eb2Nz+Acz28BAu/aa5UsDoY230aFVctpH67JrBt8WRMAokuI5FL
Kz7tO78xv7D97YwALOIlnZIE7r989YfLW0nqZpcPH0DbP2LQETYX81F8mWcTKZa0iz0NuWmRf0Hw
It6kQbRhWu9kguUrmChWFvW7zptYjS5W3UccJtKRlU4c1JaNogKmSi4v4/ai77vYDw7bO0UcQpXh
N5QC6jb0NGaZc35KcdrC+hCX8yCUJuzJvNpCUPrb9dUhH7yR1jrsLibhBSQpOG1dBqszlcycluLK
FezSp2ubFlikwQtjMh1Q8iidr60qu7T7flau5V829HXU7vxFGpKB/MT1aPcnmtVVMeoQYof1tXCX
/XTWob11MD76S+gzSxC28kOQfdBP3M4PBS7VnOm6x7XBo2s+M1PlQpH7u10yH/da6/2yvp+tJeTZ
pL4X5D17Qw0nU986vH9+bBFRapanjQKe7RKTceB3FYVe7XaXaQwjdVt+26Fx5jcb9fqCoOlxt19O
5nK/Ab+DElEfZr9HrBE75S170OuSMtPJ5tg/oUqrXd/Zx62RwcO/7M6GqAgSX/YBxvbv//f5yvdK
uJnQzWWv+hmDzCAtf52PimztwtESoPUNSlJ7qsjiVkypa9G3Q74jFjT08Z34Q7N8fgSpZP3Rif10
BklrxKxZ0aqoVl3NLrA41NkFSDZuFP3yLcaCVMEkZ1akzx7b1Xn0o9vlybdnU6zy/+HtAtQMLTW0
nINYaDqR0NwRe/1vn9x8b/PBRYNWlw5Lw2eudj/XUc3ikqb7U8nrdu/g4VvsMz0iXiMcHTaqNOAa
CnYITYQCvSSXLfW7udAaS3DMwKVBsVokRJ26QQa/RsqcUACtddtAJRniuvDm2lW52CT/v4dwSrH5
UWatfWfHf92JXiLPOsd7Y/3Bppwh57Jo6OrkBr7EpeyIB42S+3RW/yhzsUcoARzeGitQ0Ing5u2c
033bJYPSNKGn+GPafbLdarW6KyS4zSPk8XBMJi8TkoQ6GM51wr9yHs8/VVAEHnPvGSzxrtGJ7QO8
dhJgEbE1FQW3oVezt0G4wfMAhm0Zc5IN+VAj9OL9nN2FbzsvMJMeP9kYit373yC1hfXkAUeOvMRT
qOcmjaOklJ9/bm6lzw8uho+G53sYm3u5dVKDYn5hUBnftBC8dFkKtH8l59pm7oFZ/Xq4CDlKlB/d
kkW6+sg6k1SgMMHSpW7WtKnV+L43PRiEBGMazI+pNSMvCokr+nuSXgqkBwUYr7xfDT9yNPGJeyJ1
PCEZ+M2LZoxDbyc5Akiql3ixRBfMP4Fc6oYUx34J6DhxmfB089WrTDl8oFgYIdAa0r9zW+sDoWqF
qHNcLTjXf2onVcCDzvTabAX1j+O/Fe+niYAKGpRr7GixPCSDySACAlK9z3gcevqWGB8DHWohW28p
o9P/zkyJgIQo1meDU81M+b4vf6cOr7Swic7oMgO481PGopVKN+yabGpLsBHISrKDM2sCi9IrSjSR
usySM8KrMr4q5DOJyNbMVB7DmRf4Cl8NVrvmaoYZbgYy+/ZRFTkGXBBg11F+D+JL8/8RYB8awRKc
FAZIRUQhi6KfHyaoYbGuM/VYT7xDmkvKdU8x0iIf3EmduGKLzXOUC+aDD0V8WN7TwaC+3GjcsxJN
m992MX8F/XcP68BzXUWwSAGrL2dpR3XQtfdo9h6XBEGMCv00GEmPMME+96LvGwiND6s2rMzhn2Vj
7JuJgyxwuiBhtrIYdioAZKfeuvL2GxS30FckRcB0m2xIHTNiRsb3VFJZcUIKVYfvBWsjPJtSFGav
e03v3sixx7jA/uZOnn2wcuk31LkUXEJruNvyvhCCUdrBLQro2gqaP/xI1aFrkqoqWf0fJ1UKQTEZ
+jFZHnVoqrB+oi8dIZvIaH6eqsf6behEDwijCSVxTsQkUzvDjXcO3SCDRgJ1YZMWFb8RubBuhShz
LRMfPjS0m2kzyUPjpMNkW2Fk+2aRJYiXFMBrG2I6obg1pi3sowg2XzLTttek18ndoaiXQyZgY0bW
MgDLMqblqBdkb5O45N0jIDJPkQDtpV4TBLl6Ieau5l7ovLao6uKA3vQ3Bqx4PKCqrB8x9d0Iyv6M
EydevMN+KEG7KqI8m6ydStPoqnnGRD46I8e7GgprSEF+d/9EtI243yt3RICYQC6fXBGQ619XbM7u
zfBqEUuh9wVguPaeAMUC/2oBtRFRV6cI1FABhkOYFl4Tu1+Bp9zqxkEKKmzsdAS++Dww24b9qkJx
yTsIu22wQICBBIPAxwqIZ7l/yk0zgOxcGOiLqeqlOq649d6lYoUMZeK1Tg37pWh4LGxon1dEm5Rk
xpT1MKuaSeZOhw+gv76tPBkh505e17i2fvLIty5ELqqyerc8Rww7hN0qHu082CGBPq02GcNe1CEr
TspKGW3+v+uEJSi08dIzW/TrlQxUNz7HZtrTkVRuaUgQ+SJ0fV3eGLFdRkaCu3Sw41rQZHUaOFSx
aC89dEOXLBfeJPqwAz5SuZ/6tL2gvimJBI3tcjjxSVQ+hVASkskNqqBhM5VTK0t5+xieOZpudYko
xr1Cl6euXcxPXNcVvJPMHjFqwe+q3jpqHxbhASy9Rhq/KN3brasSVWovqJ31nKaGKA7f+4Inagfl
U7N8xpWbbohNpzZeB6qaAm9IEFYgzB7NEMzcbEJqXEKFPl9FwsZX7twAnz94GmKUkGNDQnbdIUgc
RI5CtvYQ2ObbrGwbMJkHfdpek7yHDevM41V765x7M80+0gj5x2TJV12j6vnYE2bCLu14Rf0jCZA6
FhJq54yjy9YhFbQGtvEj3QKy6ZLGFsYScnp4CFVpgRatqri1J3dt+sK+3e5yzhpRwnLbulW769D2
/C8XbUv+79cTaBociZp8Vol5I32KPhsOfQLDoYgVfWvwrS0zgZreNmLpyWL5X49ItHLywgSeqzes
fPalfCXa7APFuA83i0OlBZp3uolacc72ko580NHJBIjcV3yo4+ovCIjhZs+B8u2QYjZwX95K8/k5
Fvc4oXaCQgShj6FPy7/hELiVKCeISxRJA5nDTdFOWmzpZuxalEyPEbw9cQ1Z3QAEru4xusICsGFw
otsPVa0JTIILc01fOrWYltPFcKyhcnuVRo5ADjsYReC+AU7zsls9eTWY1gAUrOQIDZr7Iv6T5v8+
tOTvjizf7Jq0Kd5Vq9mcpfYEIo0Te2gRpaPppPbaILH0fGY+m9NWVQ4VNku8Zp01QLqmKydNqL8d
5LqZeeinUTK6BA1mxWchTqGBcbQjjhPcMX+zqMdZlGMA3Q5ha9/xtelXEm0WpeoD+ptbfgKVvJ9P
nUdIsak6UGvHkl/yKTfNLLOKnWmsbSj4TPsUDd3feQhsvJh1LUOYMFUMjzbuDhR/rLUsRwiIEIY5
7Y/dWkleMujVYGgRkFkt6ToAuk359i337LV2uAC4VaTQtbhtfTD9UDTQ+IKM2o6jDQnOznh1Dx57
q0Y9bzg0qJVEbCdtDNGKIde9IlEjxxLP/lLHrcHI1oVwBMqNzzA1vlNx/iv5v4Ehy75YmBfQ0BT6
OB76cNo5fN11I//lsIOKC5sjaC/DAd3LezB2e/pju2Y4EJMnpA/rll18HyMh7WRycjTWYJ7w2qaS
G3aLY8duIE0CGnMcJkNzSHz1a9Q6T3zc1ZIotzSuhEWzWftybgJSvkMg+ADrkGVUb1hR37/kkmeE
HL9MN0sMKnnuP6oSybk7ZwBebzE3jC0z3RQ5ILcnNvdE46XqpYT8tWpG3zMmHjltjZeqPU8V63uu
DcJ82CXnj+OwU8DdhU0lX5olRxDVWdQOIEdShvUMVUHdBQVSkMo7/3UF8G15dGUZISaKCGI0zKBj
vryGMz8k8Lpn5hCLSXAqzMSc2WyYli5ZCMXzuTGkVmkjf2LhI9DUCB3u60Qi+WwGolgr/TZ2xNyQ
C84AWusa0KyEBqnRrpf2rzpJ6dhIMAoUEd6GmVePgjrvVuMiu+5utzD0CFYRpN8+F2nr/rBR3kB+
jzTdHeiZVkxJXclj1Vmq3VZ9+9QgBd9aHRReRhmsxLstSu0Dny2YNHr8/DHwMCids7c/mA/othsD
CRDU0CM4HDofPG1Vic68KYy0j/RsqgKWpJ+e9Ole3rB4UyP07JSYK2fEZGN9v6cQCX/Axk1rlQ8l
bCiviv5cns9RdX6EZVWv9Hb5Ov8nhJ1jC+a1HptRc0Diqc17S57wOUDgPPNRMpFPQW/f+pUmVepf
M39/tOMcBRlORLFwlFifGGlZ+mlSrJQMVxtj4gohmQpQlDG2rgx2t1RH1jpNaKpT/Bb2mc3dDdsu
McIGcH8uXOtYlzSlB1vwI/jjJJFNY4XYhJ3cRibCpaPodY41G75zCN2g6wVqb3qjat+c5bWpyWcr
vMgBFRLzmzhDUWidy4J46tcYn4lkWCyg0A9n7M43vNgVvKs88Vqw2wwVECAZj6SOwXW6QkESl/RJ
j5xpvXqNMG2eCWO3kjrRGpbDJq+Wb3TQ8a7MsgvCFi+AaWlB/pEhp31RfZ43QTomisLGTBxIyTEZ
SBpVe7OPYLiBUzuVHpPU2HLvq+1Jbcci+hpk1stY2kqb5tL0W/6qUJm4nfKmPX97wgqTyBMhL5eE
ubtEXa3oB5O4RYPodJ50qJQ8p+Fkwm6Dxc+qEA7rrfb+eenN5jRibqvtOtRla/jVXY147/5fA0vq
3og5jqTM+/t8GcnMoa9mc73uPMEbXNA8Ih9C2+GHg8RnlWMn352sMCXAaapIdKyZRxuz73MJr804
vZMEsm9wpQf/3uMAFAJsdubXOfyNyMPGqJ82nUjeFoQPTVEdTtcUyyHb90Z2/sKI0lHnaA5TSbxf
HSeNMvCsX4kkv44rQlajkSlUSIggHoJmc0tqlSxdG4SQ0iJ3Y62StgyTnWq6pjPbclVVTzEqIrf3
bolXdi9pSxnKeERLjgizZKs68bmqZ2paQy6fZfKQc95BitMST2edXvNdFySQOgYUPAt3UlE3xnkC
g0LfO/6lPHJ4ttVwwma3GMkjDtxtTRYNVq1H5fTRwDdVSkNZA+zq6ZYHXc/dIOXKklx4CqFE9RBf
WY4cCP3+A+limNV9tXmCiJq9ViCyGAGmFkg3gCqlrmsKCR0Re4iqytsgRucnxs8teYznXrxeO9Vc
g+UPxcadrFMjcHNGTtF6YaZ68aRJUT+PE+t8O0/bIOH/VDpN+ewbfwulB3mzqJHyTCfQws2tuaS8
bWJGcQ2Ciztu5OgIGuMbUUWn+cr0GkvRoGoKTu+gnamc0Z4QYwsnfiIK4pn7aLvXwPuP5aSO97AG
S8/8fToSkEVGVfH+kU3FXyWuj/VJb2CMB/rE0k9XoquZkKCekRKAKzzQrXTERjWugK55o16M6a5g
WtWBzJiOQSWJAgzRqNsq5Vjda5DBM9ECSUHkZXmIvkrY1auPmGCdAwOxfkjmnZaSa27p8qN2tJNU
//12Vv9frX4Jj75FmvUtK55zhgS+zzIZg7FVCBN8OfS9TC1L91MFwRod4gspPp8Y9UI11EX5SoQl
kPN/Ql+bMLFCWqNa22FriqXZ1QoG7b9tHjkVGvKiqkTLFXIoz1fP3Dm+gJGMNhT9P/Ub8StEIhPV
4K2i1Li4YnYjskicm8mjeeWeNBXY5KDrTIDliDeAHdD19skGa5A58J950rzMt/WAwNYLfqnEOx3z
ZSabnl+M+7qgI7MOuFAie3UvCqzMy/PbzyhrkSysRNargxoQaxDCieSg7LW1OXSg6WylhvVwOY1T
kHKpCjFAZh9lJK7mWmBPZvhjWejlQSbiJ8c3kxseeAx7999zbck+vgAHSNUdc2r5fnje8KEFrOiZ
4Tsvc1nNsW9Q8NM5xkH4ABKY9na19rEU9Tw1QVnWT4cUOr+5WRki8FpDtMXUwgqVf6+FseyldWF3
c+EDNZQPq47JYi42wKdq1Hg8hj23mIIjCSimcEZ/xll+1keyo3ccImD4wY1Fn+bqfcyXgGWmHuMY
UQKHt3w8kp3FLNF/KWqhiUBy/9nwV5qKeMz9M5tI0vpY6sy5knijo+eta5AgyltMDda653LEnb3X
/H2tDxCQ06UDvuCkhEyiJM5zxdVHtTig3A+95cG5xy1s+a44BUXRTXp8w+lIqrSdLNvYEAzxZ5FM
aHp1X0VTL0oo10X6tPcFkb03tVJPg1RIgEZfzm6kCdZwDWbsPbWw0ywyIZX97uxKLxUCvOE8je7E
D3PAqvAX9k3jYUxpxG+aWMlKrZGBAl6ZyVlxqZqu5zQ1WjihmVQffYZ0sgIGuBaqnF9rlLQDF7oP
yT2WQatbolFOSb0/zb7Ee4CJPDsU/eRb1td8a30hfM08TUzhxJ50yH0AP+bNCxZcYjgSrn84K5vS
n2/Tz6TPeag+reZH+BBz7WFN/VwnTcX8GA2LbLEf1qvN7QrFN6nS30elQNoIk5wNFcrxeRmjRaAZ
PqxIdqd0/IGmd2y95MM9D/HNxAzQ1IlqTMkCM0e1hUOlr7L2O6SUerIMvKgADkE/Hr8emMVy29ay
te7cf0H2LGx17C60kb5DODV8qm+mmxUjRqJGhrjOgmbiIF/gTy2gFaFFrtJacab+v8bSrjzLbXsG
lje5Hh2bpF1OTRd9o6sm+l4TrCYoYkLBl12/tUiinoIE0qKz5t1Y0/u6wTKUGGAQHW0IIce8J2qc
LUrC6r3hkk/OTWEiNz7R9CFFu56IAAnJN8HGrnCf+eecA9CKqz971T0BnnQKCf8m10aU+q5zWX3q
o3nKeNTBVA3foyFBrJwKrklZIbXEIwcR7HvLr8eHr/qnk2JX1/RNg5/hVrutdI5JsVB20Wk5cSc0
BCIUyW0KGriUyxDPwEgzZi864ySIlf3JmsOu0w5y1CHGSVpcqdWpUCA+LI4oT2+wHZVB8Q/n3ZFf
ISPddBEFfUELYkFOv16eW8GfANqHNg5IiM3e38pzV2DHmd4BqEJzbCfTwnaciM8orHI2NQmmpWZY
ue7w72nFREiKOTIxkaDnEEwDIhmNq5J4KHN6v+/svsCYTHPooAmCFgC/Y1PYocoLLS7oQ5I4+ZNx
k1xyCJNpO0NGCJNySj9JA5FEvlApVmYeSIWWthxEEbGp8++d7wuyR+AXJTr6WNZBbEMf7JEZWVQy
fmMRDs2IDlpxdYxdAS+E8qPZFri0kvlX07obcYpPjlVwTtI4Sd+nXOuc2siJRSxScLHUlppex03S
B3oBp+VjsOaI4cJdf3HJaHyHVHudgDmBU2MqcsScZf3v6JOIt+P3WtXpLTElHAptqP6r9UyFBQmW
H22iw73i4EOIhF6kLioZ9wn7bzpXAc2LfEnd2ecc9UhJhHzHsD5kS8yBJj6EihP/SS7QhUjp9cdI
gmBRRJriF0eamnD6QmD/dkgIm4tvGZlZux/f9Rmm/++iy+hmG+MPiAWp4N7WvWUdpBi5STkEKWdI
UiFokVqj5w8LniBclM8AxrrvExlfxmAWvGCNtw7W30U/7TY3IL2fGVF7LBdrJJ8j+GWbuF9xa3ov
mWCWArBmp5M7epGPSy+dgMl8LU9WTbyv6P2LLFJ0aSVADx9iRTuqfwWUirzzq1KF67IL04a2fi6C
Ax+uyCIVVIwHz1P1sfeAl3eqCXeBMexfqNAAaCjE4fSfBk6Gn+MwRc0oJo/jctnKh3Ry4HJ8w+Aw
CWzCpze/vcGE8bQehqBbhqPDHlH0GV4E4WTGSR5ipRn5FG5W03PRUI6uHoLMgkOf0iHl3Xxi7cV6
TPl7FW6iNg9BJmJnsSq1dX6vyJ0I3fU77KwNQa1JKXCmTBqrF+4KF/Iq423u8SyCdcf6og7rT8sX
mms3Y8fQvmZ4tZD9TXcoku/lizwl1fqkN1KrH9ITggEqBCQQdrs+Ermr2SvDj2qENSVdmVPNkFrZ
rVYFEcu2mOvgKXdsvxurvBylfNsPWcSAnnassHCMPZEi4y6QWEfd3Us+kyqgbt/Ct1aHTnQKCuB6
60X7uXcn8Ppgj+S2+1/R2LJbMJjsbcyeFyxTF9HwdVehxUK4cpM9If55Llz6s5LugAwDGajZHHY+
fK5rrIHG7aW6bbQI4W1eJvzHjdk4oD0ZePtOrt6ydpobUtsCmybilpNF20Z+fCu/F+XEdiyi5rg9
JUpwmyHiQz5WUWvyLC/TQwqRG7EGVw61bzhADMDUJYvavWTfVuoY99VtZCGmW4dVf6JqP9A85ec7
3djkaU3dcmXa2FFddwH/N55pttI7GnYesdcbdsuOU6Cm5962h08pfSmD0tyaj3qYg9IYNg+7z8lg
0O2XkiLNKGWEW2fzqzR6fGGzZjsZIKAA0CJk7cY7++vQMrkyeZCAjgLb4mIMVnEu3lUDNKEPsX1d
/n6cBihAYJA8ksT1lrb6dFfgNF5KCVzO8BHqljbxtJ7f2eUSaLZYUinJvIxmoLfeOHeXeeBozW+8
QgmwYMXKPD5n/1vXfz3hJ//xPDojnj418+b2hVX4RupVYG0qpnFUBau8uEDXR7wBxnTuVhY230bU
jJMKF4vGBgP1zU4PVCYvnIGN4hNt5adAmDCGNZJDhfmK9YrYgrmO4PcHIzB9eamiTi3xwag+61zJ
wl0llBaldztV9wRJTbIQdWf6hJ4wkDny2XcsoqJlHw4qbXoVGR/i9j/MtbhG6f1Dq2GsRKXjZ9Tn
NgWE0RN4nJIP4WN6kxgOQyn95MEUzDkWTD10R5cLyAIXiS4Yh6tiM2iqP5aJx5S4oeVJ5Szi+Ieb
MKJoKIr0YxCiQiZxy1fGlrtXUgKzUTy5HL7XJGl5fxRQbO7ejTm4rEkPD/15EJ4GjSB/dM2B5fEW
zN9H8fsHOogCvLuxUKZwljg2CUh91TYtJo1TxHQKkBc+OLd0wcSTXJdwaiz88prijYv1cMAbjdBB
CxCNQZA4yftr6RrdQzHe4ThT236PPV9SDnJC5Ka2v4zJoFWMRiiIqUTzvxxnrIi1McarjIpX6C75
C9dbj3u2asMRQlVR2OZL7gTDgp7pYG4Fr+52zbKFkxK6Lz5qPxXGUvZomZKdgcewfi30uEm74oug
PCkjDmJYyQxW8IEWRiN97VpBOLK1zcL/5ukMKFGp4+qfCleYDu0YYUth3x7qqItGWh1BSmXLX1D9
iUWM97IMFtjTMP56XgKjfmFXCABRtfcyXZZ+cFBiMggT5/tGeCy68429uCSRW4sIx6UNN5q9u8LB
cqrIH6QE/gkiGWKbGrBtILdUova1TjRqROw4lB81wryEkOHNIDs5CXzPpLjKEnb3wKlF9Q3qe96S
YVc/eOgJ649v+XthFqWgzElGN6/g3UZrQPOFIgMCyMUws+9Z7QhwTY+7VRk0IVCmOlc4bR/CTrRN
pWuo5C20ZJxY/T9ze27iAyNnLanP2Sc4Kxq9vgN1H11vARxX1KZ+PiHCL/G7/JcQplPBav8LJ69r
40L1IXeq8/inBLZLaRgUhy8XyBAhHunGo4ihlObX74C/TIfjhV0u01Ad7w6nvnivwL6Z401OgP1s
ib15m7cHnGH/c1/OFfWI/gNB+W+zasGdPsH1U5HFtrdINleOw8QC86hclShQtA6HqwlRS5Exqncj
E0dwq90m+GG9mOj4rSkU2JoXDkh1GJWAEe+d12Soj7Vp2elg2RyZxDVemnTGPOtL5lo0YHLVzZBq
7IkgIEaiUbj6TRPjWFtWvFp/j/f9kTyn6M4HdMXEkyhQDC8PqeFrxJjTmSoBThRGrflkDrcpf5tE
mHY3M4hNp4XulKndSDzBGTYIHwlwo8hDTNyOLG9GkRE28+FXH4srlNSYPgZ1E0m2fEt0RvwGqrYj
15qWSvERDbmM6q+qhuZG2T/dwP6drrt771fFNL9g+ZvNZC9OzxIaSqoWvZlATzR035xq7cjTrhwj
9y08Oq8BPugBSG491FmCdqokUpjbKDnBPdZRhVgMVNcksE4GmKVQJmuG8sogvOqYl3UicfkPHawj
BM7Hc1kEZsictOmXiGWFsbCcKUm4wEw+A519wy4HPJhmSnDP4rX5nQzWeXtXL0LSPZVEINp+82PU
KkE/mK9bIeFeiBpcqxuQlE/VbgEKo1KEUqfKhjitHlSGWVD4g3sEJ2LTTRDBkQfPPWbEtxEF6iSF
48+MjIRP47fFWGiwUyR3PBEbSqdWdyvvCUXLlakhGOOaijV3i35eyT8Q3LWuGYW82Se/Z5pDo1Iq
bRvA05n3kWcYfRtc4bbN/Cf5K+G0G/EO0nOwyUzLyhZgeZnGA+U+Ir09J+F9fjT0FmX/HPQC/bT5
HRKiSiUr0HiCCiqNYZF9NxGwiV3aIspn8a4uUWr3aQoGf/sKPdHMFhTtLDc3XfHOLqKVp7uQwtbm
ZMFSYpL+eQVb2BuKU+pmj111oZSRRaC18ooE/9GHHVeM7blgTTtvI+5AQEjiohrq3YGM1hw9TJtJ
yrvP96nfhyKXV4F6/jrzuGpM2Tvudp0lS8wjFOr4WRB7abZKBtC0JvTYLTmLnwhdsXZebWrqcdiH
FzQquo+LRRpVv7FMsfo7bvkIzM7fVEk1KGE4U+YRKaMNVXLp4y7KdVuxMzNjzaMw6C8wHdFS/kcd
aUbAghYyUuwa9bBYmz3Dz4AjtfOeV23IbNrAKbL/m38TFjNgDKFcA98EV8SIQDEjCBPunHUdfZL5
wnNxhGzEx/V7vbwyuRwgn7SRAdlTmynizIe1kiX+ymIXavWSVmE0Zqt55XE9vq206EoTTkPuIgWQ
Rnr5FuN8fw79A9oqueTmcmmirJfifBU+1mEhEBIOX1TFVBC2wZCt6LWQbVzxBhsWbNwZGcFTjWdm
ykHBptsBiekiyBXTdHXZWT3S6aDiKJilnbGWimRY9ZPsaOIJNpjytfX46401RqT9IxgeayxXpWnh
AD4YIwYD40O3oCCSTGxp0CuXvjFNqQlwC0wUi8Qg3TXkFnYjbhr+x4uoQdSSInit655bzNlJj+yb
VSxY6YYllS4V5NOxgSAAwtiSEN47pisdk3tRjpj40clEA0KD7PiC/yCGejwK2xR9/RCXhTLgtW4l
y3vbqqh03K0ViU910O8dPSO9FlAgfU6c/9FmOesI1/cCVEdCRf7MvDyS/TYjLpTrrg1w6lqbscPW
tGJJ1Hqnp9tZCdeM8uPKcGMNaPRPFto4EpvvUMqUkzQrwUVAeeqIchntlzfe5M6g0Ll49aLYqvMB
ovlivjP9JSmw51lV61kgTEHEuqZ27aQlTkhKRo07i8tHUWnQTvrAvxs48GTsd1aOooIE6BNUijff
QHCnaE2gtciMUj/zvXXELWGW8gh6FATfKrqfHA8MRNpzw3kW3dkzT9Lspf8Rbzgy89qJzrqNFQRu
WWcREQvQ60NFq+sP3NakP1Ayk8kZPuPdL0rwpWd8+Q36rtIVttDyMBNy7XBHmNlRlEpOdOYk+kLh
SA8IBaArjBJlJ8PUr+lehoKamqcZJspk9uPOKnLrp71tol2sFcZX94W3rIJ5s2INy9pFzmxuAf7H
2pF9ZeRQ8lqxg0FbXeObdBa3EN+6IyTgvSHLFaYCLcw+C3i8ldyU0JTzh20WZT4fCHzdfq9MlG8w
mjN8vIpxfuIiCiQ4I0FsqR+dc+aQuSG9gFuexOczfuCA6sL9rjs0XNRH9r6SFeZn9Ll9QNmpCpml
PNEUtux5L9MqWl5OBiPcRpZvf3Fqha7+VnYW4ZefvsY9I6c3Pu/d33Bd+DDDrweN9fwl6KEh71+3
bpifNcjThTfsJiE+ZULy0mXMrNDtYuSby/CbsGIMyi2mqxjRuY9r1fcAs8bnsqJps8lBj81HNr2k
vXzFO2HvLJnEEXLwz5Cyp9W0TDHZXRhBc9QHPE2Ex/Zd7T5kd4FjGtWJXyJUOyexWXmFzQSBluyN
cjYNvWg2ebWqxjd6/IrGk/pz8eGEwybcuwqeWWCm50CU3YQ1wyRXtsoGOCiMv0R6M5+n0dfCx8hL
/fn/V1VejL7H8YIorPwOd7y9yNRmw1tSUfbYmlLvQHb50XOi/jhjwiXJszq4Y9Qbt3ShmYHvQQhr
VvldEGQg8o0QLmNm0qLWumYPi8LzjX6P0+YghyEye1QOAK+jnAOsGCl1XZDk+lgZxlfT9rU3LAkU
EMeHTBGKiADgJTkQfbhfjda+A+IaTouXcVop7dM57T+MTXEQjLb+NR/krbN07yA6L/CTWeN7Dt2p
kX+wTvq4hsZIWTIivNRcLWtsRuVccfYcy1Vz4VksrhkincpYH/jlwlAp5KxTrH9nl7K8RN6Z479d
J4F10mQycgCpQtcuGSb9m4fkJhecbZAQ+T3GG4X3vw1ROkJJ8DzoXmoLWQciwf7m9KlagMRG1jH8
mHVLhXlU5moB5yFOSX+nqvkveIcTKKt8Kck6kG0kR4PQ2+b/o8D8QuYgxtiEKtBgJPJIgarPttYq
ijMTOcxnpP1le4DLxrT2prDkrM4kaeCIjAQz3eSwTG3nfEObCVIBqAzOQOupwJTupH+DGsIPdUEu
Qki/W981tCGrVWbuqrqQXEUzd7u6k+MFU/dperJYp4RyzLUD/uZJTBhJm8Et6lxPBR3sBY7GsCgg
mPO5dSTacAlCbyftaeOWe83Xtm7vVVBroLkJCAIUBfV8emKwodZXX344I4RhwCUJ4Atl99Rjhpcu
8h/a7I9CLClDYkLfLu9jVdCA60JUK1GRy7BSlWY0aRxIr8CZmOMQEPHCZOsjGQaFt/ZMEgO+s6j8
NksWUBaDFhJy+pyVKXchRkMcgqvIq5pO8FPUVguRkz7DfUUnSXDzklOjFEi8/dMM2zcOAdkbR+Tw
VCf+f+281N3m/ci+H1CJmcrwLU0dMnCKwumWaVXYZu0zWN6Udts/+tEm/IOECEy+VkldqTjAa8bW
Vinf182u1f1u+GOr21VaiZ8TY3jSsYDI7EhP/rNkzNutRIbSrA8XCdsIuju+fyW+PKTn0JyU8Bup
V3jilsdXPrM6hB1hySlEj65r9KNksI50qUO9uGDc1Li+qxWhEdLGwbu2zY+Q9iV4LHeBBT/AGsew
W50u6CWLDfXx2n/67Ib2vZ5K9zDWLa7Tys+KLycdq46rMjARY/nyteHesU29E0b9r1BvUe+iTTJt
xY1AxlM2cE5jhRE55F6qooSfIpz0fbqUeTxqDAZS9JafIjpNoxAu0qSg5KvcCdc4GGfixgYJJIMf
VcDSX5WXzhsbt4TcRwcBQELr6Kzq9VrnIHWCWvmT7aE5ipkm70cmIZcYOZvz7foXGDtnW5dQTQYM
iOwPkCh1GNvMjvNCp73aWuWAh7VWvVC2/27PcXTwwArqCr8Og/+oGrkJS+d+UNFJiECyNAEGd/op
W9Z7wfnoTr0nWhJORIyXD0fSYcOc37wInhtYPEqg27fnWR2oreDl7k+4kOGZZd7AxCMm8GkjPXAG
QkA2W6NbhNWLGy5y4j7uhvkLIQ2hAxyaSkaU2YXAg9bFDffYKWOS7zYeQN/3pq2AnjVUQ2W+6a0B
Y+I/diZu1SIG84BKJ27YowqzLUYDr/Kfh8m51cVc9YY2VYRbXRdfF3MpkYI8VLzop+kYmylsInHE
2hLgcIZAuhkAMPeXcfe8BwActFPnlY3gPtDmgVPb5T7mESVmDTlEUyzfRpG3Rnvg6CSgPvpryyE+
04yv7z3q1ULE68AlhV3SNqQx0cufxrF8GhBap7WhGn9y+w38sMBI5o6heUE56g/h+fO95a30bCLh
zs/kI3LO5NyZbZIKoMJ6wwteYCF0SerEvqRa9L3CDTN5pXNodz0oJO67h+GCfeP17zeQdPbX0efY
7YwpBdm6O21ftj12t2nKNey/fF9GBn0sU6ssELzpW2lrKoUJhRjtQI0nwF/m2X8uCclNfoz5Qvb8
cIsJONHVwJJ66xcg2f+p9VoXgG8qyAsdi7udbnGZIbVIe7NEhcr/BOyiGcZkfDuTVPMGVZ8CwFmp
TGFSMROrb/eXEiw0NooXfhpL8wLieodZdlprP3pEjjmL6qneNy8xx0hOiGgwzOpls30NA7v1aqLu
J+W3qBl4MpIDe1YgjYspJ5CvuxaZkaZXeyYl5zcB5lsZ8bIbbxT39B1XkYg9twm5poFdnni5JdeT
rBct/5+Km3R+ZAc64bHrx5Y2qEzxdsb8BB58OlpqWXJq+jxzE7RVnso0pq8tcPXyFYv+fVUW3vik
w93KsEi5w35QIhMOzg6rSIwWP6XcZ6xQxFoQRvhZyFpUfXe2iVRIRaVZpFhuzynlTafRe28axkC3
dzFEeb87Kg4wG01GoNdObanouLhzM4yyXSSlGZET8uu00521z6fo2Y1y+VeAQfX2Lk6MwIUacqYr
l1gN5qhJFhU0onaDPH6O5WAb3+kRq9EVzl42yAhR43h9fVIuvkGaa5pZ2IOFC5n7C+inW/YiZZnn
RIzrOiN9N0xZE9fAKsAL08csd+ab78wvHo1Rf7O2XKv7eFJcbZ2/CHCk7sGvOespjfCS8/fL8qpB
rRFMCd9x8/7x4IORIS79DQLsV/cWl97q0Ug8rSOhHt2LH8BPQYrmJIw48FQInkUVjoS2C8oXlRqH
QI0YHJcqM0Y4G+XtJWHn0Pq6YwVBbU3oq5/BrrV4vHBb/5rlEE6lIm5CNePjsN7mvopULLEEzz6A
alqo+k+WO2e8qm9J5Dt3NX4Dfwd7VbjyiRIvbHug56CdhFr6a45AaclCCV2KsOwM69KahdoEoeiA
UOXFsOF/VpYczBsj7JCNLi00HcE4gn2sIXBTV9zyy0VycV7l/AYYsuUkl6Av2E8kTde7CxYhdXfR
f2oxAAhKv+F9z16LGLv1IoPpDcnQjU9y4wWuOeJhZtOcZRkadcadp8mVpxwuoTdShbhv4b+bMxgO
SAZbX+5wUwoko7bbe8qt149x3/DPkwF5GX/GSu11Jlsee6aoFR2Wb7S6YEMvAyUpiGa64/Da3Boe
F37WR+1gIdGcYjWpmkD9/8ZpnKN8Aqh4crqe1ppxcQPR+Vc1OvC96URQsqC9riaMxITzyNbNh6PX
oPlzH2G6F8jq7JqMCaBZ+OYtWossfqxbN+cka+g3+cMR5gePs5aVy1j9mbfmtKcS5nRnCVhlTU5b
jkRCgXSE7Xjrhduh6njnlAXPphMw2aRLkX48v/t5rreM5jYxAqBBjV+QdTEjIdX+ect6PzJu0Wd5
zOwozeNQyx6Oh7BsggDLiikdTPq+EbPU9Oq49UaNJGTha1beeTFIHMxBmWq7rBhw+lTKZuHqAIFO
Yn3zn//PfF2DL546lXyHbJObWmNQq3/ZYWP1zzqoSofklikuu8GSHoa3SrYcBCJlBm3ckwt4hOCG
U1UYiotXangmjs8UyJhZmxuOunfrVxZUbjCf0CNSwgs+H1QaWAuc140GaBqO3/09sWoStDAp7eik
gKw/EXaNnW4AFbW3VFvl2hYADTFsCDEufZ3zg3IOBcaJ2VDpUe1f3N/DgBp1ruuzbPGJEH6hz64d
H1tMhSG7BID4tqiMzwYYvMfr82Z8G5Pq9b0Rv3vKCmSiGu0mLyI145kjsERobpJuzq0RgCGohP2E
sNHeZgtBxnfV0NZhOOE654uaMpg6MMV//wPxJxjhpE06WJVEWDyGgERdjuYnuv4sgul9s6QjLPFI
HVkS2vnt1QarOi/qarTVyl6lAtXGEtOEW5dCg/62aDWjZEQSaslgp/DLjEcXNNispQv9o80cDD6v
bqVJ6JrDKzxuHvvj5VmO1e5JX5Dz7XXUj9qMkn2X7X3ICKxgQ2NiMY+AEBaRNXlKwZbSLozWop4Y
yRTD3eEKdszmOYOLh+gFY6lMnLzkNWsDldoOZ+4YHq1We7uV2UqHxQEVvF8B5R4lydKKqtlAQ6F7
Gx0XjrCFrP21y88vAsNiqa5ONETyjzBFVvWWMidRdhxcy2blpUOZWFG5oZIOrhN97wionrPdkuwz
4piO8MZCPIaQVzyt8Acg1llbP0wo7rz6GsoehKa+YKDQEUr60eFVBLTLBngMjzRIEKx2YbTZiFRp
RB0O64U4YBLM1Y/fqUTqAqHV+lpbyN6zlHZIuao04t9O3b7Pw5U8gsL7po5gqF7Zr2uwxmMvNoQa
xStwerfi2W9uRAwVKM5VniusKJ8kQNRjjqmB+llb2FDYUNO0sdkcfCIWVc/uihQwu4QWrsBxBi9q
ZkWZBbaSZB6nLtjSnXdEb+nDdQeJIkpUp6BsJ3wWBgMtZMSj9uT+C169vA3o2DkpfHO9LCmQ081X
QDFpQljHAQM2eE4Ieo0b4qEeFl+GBMf0QLbWF+EU6Hf+DS9u5NTIkBMwj2GX+ZrUFP0/NTGo8Ry4
0RSt5IITPUDw3Rg1zhtFPHSjx3JRChQJIrHa8E8BZtr2NaKhYvtmPNOrW/hi3fqf2QvQzzAbbFb0
knHsqh32QDxMTpggi/wfDr9GRvsCfq+q8vknJq9m9ejXN6FZoFo1xfXtHGiz8seqHMM82wLfSIcA
KqMHlJCUnn4WOR84r+EULdiFu/QoOP3ZWbpQI42PQCff/Ka8aTfQZL87yw5YwViO/yLUTQiPIUNj
7M1zigU8DIYPVHQJrsy/BkIaoQtl1wWNz19V1y670J4w7Cz8XTFsIgFHo1B10+CoJuxYafiqPuMV
38F5zjHmJbc13FqNC5JS9KqeGyrzXTcsh2jA6Hy4DBPb0k21+Q4J7Yaa0wRM+MC6W4vnnkMf6MYT
/rI+qmLMDnRiFjD0oTPuyaq6pgXRyfWCqpNzO4fW4/bxYbqMbefCiylwKKl1mlhKrY3KvzQ4G/UO
0XeL9gYP4kOdxCVYFKA6nPci04hmOx8MA/qH9Bu3G1YVGFPf1qbBKHrT8GNHJUeUtLsXPYMAtlZu
fUbJWymsOlgOwi17pKDZY0jGVFPeQUIRU3D6bLrddHA8BIhCrwN2tH9X12g/4lKwzzwU9N4OLUwt
IJUBN6sfDXEEVA4S46SzMccZhj7YyGGN0NbjRKBTYIw8S25D9suMNQ9kCzy03WRE2wESLPZblzt4
nClobLudphO9MgPqSgAWgsyaN/ceUAydzWf+wP1WTKzzKoTmOoVGh3XRor06yj5VakowCWn4TTJt
nBY2lY5ofTLtVfkuJpnvJwRbuQQazgdJ22KHytsI/YsIXIsKjSAy9wkGvvu0KjHSJK64Utp4u+ex
lWKx/VvZ6bg2DTsqY7OiFNOCacMoosfj58QUY5HOzll9Xt2WZZ2NaosNAGo3riY98PFniJW+8+76
Pi4CX+WGL80OwFDPNfel7URWIOUrAA5LtdbtuA/jlpYL7xSCV3f+7IYmTLLuAu4g6RcUW+tODmuR
Q5vXu/zPAfmp9tuQzGZ0k6FrTTuRdl88XxKCLuAqmXjhTzHQGQWSCF6RxQIJhLw2ovEGWt1AOLM/
rqCxC13KCixappAOG1T3opwQ1Ssywsv0hKtL284SwYBDZXoXg3pGly9jpijxJSvlQ9JrQKmGtSJw
thutnXpgAT+f73//Sli8l28IlijoLm2PseM7lGZOpGaoW63Y6e+Qg0Lm3PpKHDWovsGMJA5YbXhA
151T6jvg0iyXb2drGqCi7R6HoXN8hbe7PIewUEkuQh5fvARtynjNaPoYzMQDW9IeO9yqrFg49bvI
b/fThzoQ4aPAfxBc6FuJorpui/Nj6wuoyjoXhTmH3hcAzQxETzoAPWqVan1KCh8KewiKh1ArwQ9z
VjlH80IMmGRMnJq7X+yOhOFKnE2FcbOS713dZvV/gxYQlFXllb1gdp9jvdaEsWCmCldeszsWE3ar
DTYSjeDnBhqcgSDRcpekbq5Ym11Eb06aYc6XJVl7ltAwUFQgegon6TZwsPfQXKhTehGpjaUMEhvB
N7dlT6mWPV9GVVvUS7uuNjyrPONbyjH8Z5UnQktoF9LAMP2gu/IfbKq0Rv/prK/fuc7CTzhbwTRU
Pw5oAbOZ3gxo5Y9fKuYEqPYiQwumQZtGTmQmTCuGO4oTmAMpWLnCGz+BEcbL3cOYQ+cWoPdBYvuP
v6Q1ewfhYbOnHmziPR+wvI5atwMUlJRyEnS/6xrl0ZE8dyLUMY/WYDkQyI8YwkswaKzyuCN79zj2
dLuxOjpoyaUtBEsoc9wqrvG2y+Ddd885ou3POoTM095J0bsSkCBZtRAB4VK7oP+oA1ntqApMujsM
5MjOrMHQWUYBerWs3BMYCHMotjgg98Ttuq1OQubZ/hOldh83cVrYTzM6a6I8Qez/pWJhUnSQOuPS
hV5POnMg8PEPfnm7tQMvpfuJJy+DBcvRTTWK3d3bFj8BKOCtiD8jytCaHAx5UyFxhwNypa87v4yk
xCnAXVpChVFTd/zdlgyxGNZ4o2Jd5yJ059/EiuddlkdAk6zls+ZB3rUPkcfYa7COb0QmRCqQczFU
foA4+5Q8bT1/jCyPoR8QC83XQFj5h5WeSKwsSsjbc716pC8eoZZ7Jt/jt7RVPtPvvU5aggJMV++p
288/LXI+UZ3o07Fn4OLvx7sHBXriXBuNi6Dxda65h32qwlsy3l+KmlltjkYmK5dfG7D8rnpzFW7g
RUliLAU18s/+oIPw1f3BVxUcUl3dsBIjSRuiY2UN9qhfvCZOsesESujs68bKTDJ9WE/xjo8brqjS
DX45E395qEV0PbEiKRdffm3IxtWdDOnlS35Ox+5D+H9imgieR6uVYJILGuoz/6igE3l1AQmf8zXB
hISHVHvv9HA5DnaA9ElZDe+Qj8AQ19tFVcNqdwf6iRScWJ+BgY0oEu0C1k778gAzd82x4OE0o9l4
IhP5wYt2f/pPK3MPZ6lHvYS0UOX7B6kTKR31Rent9lpGYLvPh2YlIDt92Th4YYayYaavBclnl38s
KaUjfbSN9uwNqtX2m7cAMbzYGQLegZWo0PEPeWdeo95cpIobIeSs6GYBs8eI0rODALO58+2FpAvd
LhvyWqPa3i+V0m7cYSK2d4A06eIDuwjNtZju3Ozp3JX2v4CS6eZSuAx+hhDrNi7F+dmEovNwUVXb
GrJo6/wNIRzbEfC1I9lTqGLxnuKAyDdXP0HVZWNX80LC119QeCa93w4rBd5j6ApU/pGUvRSJB1je
Qd8kJJUIRsgAkpCGZv1kYyIVjYnbC6MAyc8p12pQm4IRhZd//z4tyNlM8q4Rf8cjDSFWlPrmS5cc
0IDXwREakBP0AoyvP5srBl07bebqOdPy1ZVeCssfoQFaNPgENZ68e7Mu1orTgXD1eTULpAlwlFoO
XU9P1Kxrr/nx4V5k3VzphzzEmXBbZ0OIe0p50FW7KGmbpt0L1ZJ0OMXp0JuN8IQgbt4A8MFMF9b0
/ogc5LyfEO7Yq+q0gKixUQ3Gf41QyioWcsccbvFpdyVDawHlJezqDoNIx3RUVpa54lhL457hy7jI
oYDxfmlxuZoII2KTvV6uYRKicF/S9mGbIbeFy2DhdAkg6RtP76HqlEUjA7kZy3Ol4FSQ+U/n/cCR
4KrclnhJ7Jte5aaiDk8uL/0VDqSQhWW11KbqlSrwRGMbW+enLuQ/s3rGNWMjSXhke/9OUneZvnrV
thOxHoxSsdYIACKTYupR5CKH45gxgt5KRbAbjiWxV2VZE8Xaqu5JVzGxoDWqkxs3kuv+rNthrS+E
G1+nhVYjduse35PGz4Xkqw+7FUtGYGgwWBAcp3ed0bQFoU9q9wtYil3/R+fDPs+0HhrWCm1Nd/I2
0rOi5JBylozmDJzIdLYENnsNZs24gmWnYTeTV3NfCiMc2ZIriKBbLxnlD2rZKAMkkbnGByW/RL29
J0ewtG2hTOVgc5g7lbyBQ8XvgUV8K5yHyyVW31jrjaeD8HvPmx7m3RvZv5WC9UCb8UCF7zQokgIm
9qTAtdL7W1JlLCYcHsgRaWR8p9+9QRewvCjY4XGArCJICBDOX0fgTuw5DP+hdH0Fk57Mj+wIq0Dl
igmjSbJBM9K43hlA3DTTnFlO1AMhDssyrCKlpS7im0Shv0KrgkWuaTtAqKSst1O68e7HLCR3WMzU
CT6lcIWSUJlVDRuLVjLGP+gAkrYyO/3paIoNBqm6CNrQiWcHFl5pzyGFVj3zuPxCjgV/MRaf/3Sd
PRuMg+CNhOFLUiT22cVQ/RFRnkDSHHQJs+FbDphcrh+X2rrEeM8haaEN/0WuiP0U7fgxjcNWKjWS
xJl2bcPb3PvUVTxm6UDigUIeKuMleqVVrmUDlHY8GM143TzNHth5T0CORzl7WEaaXdHmOQI3GrUi
GPlJJ/ZbWGrnt/XMF5H93fbF3KJw8gVts7dmLisNqbr25D7zM+8tzE5rSsMIs2f44STkc3oPQu7H
cULCbyRP9vMSRcRvHcCz7FJv+BMA4AIx7j6Pk3QkZEXatOTfzsGr8Ygqgx8YediPlwCiAtDWHLeQ
U3U1pFyOHIzF4Mqf9SOXXtUbtSkf8uWhuVoobxL8/+y74ing1Mgq7nx8MxNeHycpFUb6VXPyNf0o
WCDR2RKWHNUypOEyzgR6z/DCnnZA81cVBCKVOGqWe3ms/9sUIssDTm2vbGVEXvkbUpgUCYb5qp4O
KHyhuLrirnGCaw9wZDfZlCDsKDh2jo7Bz/niHLnjB/4sXCPZ6JlTL3/5W4AFdQ8INPKLe+Mpwox0
ZOBTbZWYHuJDpcXpfN/1/H8WerNf4jeLFYxAQuku6qDoyEByCVTZNk3Gl+rx0a+s7dRKJlF5ox2E
TiYbuFBZlB25OJcUzN30ffVYKD5NZ9eiFfXo8NhRjobfrQAR8J4G02gQ/20Ygi5irT962cHOeLW/
r8UBb+W5YjhmFSLzKJn35G5aUaPcnmjQjQyYGsKG9uDrDveT0IkB19yS+jhi21fgtmlfX7FW/qpM
Dp5+zUh+rvZoC34u64FBEk3BnJN5308pmTnf5+fqk5KjawtpB/Xs1BXtP/ExXbZX1Yuw6EXZj3HR
DGCSAdyzxMx4RwVZs5FPz36lSQplzOgTPdM/vakG1MgALIxgbMDPhUE7sstYP8fOwR+0UAUOf7Nq
A6cbevmbc1gUWF0LEVg7hN0K3WoWGoZi3LtJv0F69+ezkqGQ8eaIPUiL1fUzf/LyrYUc89uhD5HT
c8k8Fyd5OYky2rbk59kh84dGWjztcbNyU1jxTKFAlYV+F4WxRyvSXOzLkrhDEqI83suV6Ut0q9uL
SnUa3iSNSFmoX50I3S+NmJP08EsNto2eJquTnDd8C+bdQ4x9iwYgl8nNLwa5jcn8BG9ImRYLqjCL
Krp1lopzyfvziV+SXwfVmn6UdAks/Z/ksw1daoi23E9Oo9lyNvalX7JKUQAweqRLv8OBhlV8ysty
Psw5i3hqwGLVcqpT+Vq9pz1pl+cg0UVWtyoBgFZJBUm4zuvkbbTQHx5gYhqegtA9z+Pqm7vk3aEK
k+BD/6PBM8arzwMbwjLZ/Yi2Pr/93h7QqkZqurI3e2xzLI0GPlIZJZaCV73Q5z83it1X0JVGgqMp
QYBMaidp+qSn8SbKnlcFDm0uZyTd6/BnFqGzn9AygiVQDFrg6KiEUBkYJvv6vVZHhHM5qcmlG0nB
ohysJXsjLT7uAVfOB/kvHRFPAaiG9ZMv4MMWqUwoMpp4sbsY977Ai44J4GkH1zX2UyHOBJyD0KKJ
BD8t2nlKuVEl2NzYpsXGjnVw6TA+8t99dHevGx+/CD+FoFZhuu+O3dghPBGLB7IAShCDShlW2K2F
rkWRuO0Z7SmfnXqcu47TC4wTt4GCNs0bd2xOAyvqHqGB0+aDzbP3fUj25HKYHVbiTE2wgwvD5NAe
B2kQ+Qt6WIkIfc95n0DYe7WuuQqcrEAw8okblfbIAVkkHc2DAQ9NVpCAXuRlpNDNLq3YxuJVdNR+
kQxIIMP0hLpuUagyZEuoVkNoSSC+Fhyexj3XTwVJLACTJlpJB/MKSzZ9V4gbPSEtpXehLCggzkAU
JVCCQhipjUdKOI04O1lgrghlI+T24WBKD7Mu6f17/H+ID59Na+r4Hb49uLvLtV2e2ZoAhKkEvArR
ej7apqQ4X2YG6kPEndAgFWxSV8u55go5W6h6fDc8gbb4EewOWOjOG2oCJts19KIyuB6Drsznh7mv
uZbYKxW6DpfcOs7W+k1itapo/yfSoa262c0Pj5Ub7MgKxdI/gTXcvjtaQwT656e7Yjca64+3+kT2
oGlEABaEa4Wi9jbVJx/wESlp8+kmjV1kphzOBGGxKoipsOEihLTmbpoNuWNOJoFY+SZEhCLCuXep
jvMHvOQb+9rGLvkdKxNFQdyA36Y6C4t+ziGRZY3DduE+DohjIUy60lh7MNe3Bf/oQN9xyZyRFc4f
ykbYQ/FJ6Dgwfp8i9Z1vwgVSLsuSwRDpH+bNTanutoxE04I+vqduSEpQvKZLj9GjPUt3xZoHUb/z
aYnf4MPleE5fPbUkGXrrkgqecEIHoGQ9a6ulqvvf5BhKrnNRwwT1+Eko4v6Bho2pqUrfZvE1HuTZ
tSF5JvumCHNCCFbAHDSGh6krMyM1gkC2UW4XDoHvhb1jwjCkDh7lnb625CSOoq1mhk9EwGLUMib6
5cAINHTavygjNo0IGjVHD32E/8kgIwd5XpubLVyQRfDzoBkXyrVQc71DaQszDsOPyAJZH/d8P2mQ
tQVKOd1ETx4NmRdcOM4ThzRihnt2707eR8FL8Z35PE+HAdEXpvzZNvDgVJechZZSYDXz+yy0fxm9
dQ4uUvHsJgq8MJHYm82R8suiYpYo+51V/FS4wVpJYTU6ar2kbk3a+Iz6mZZ2N6Rw/fH4uwhh1RiL
s62AKw1iixVVOZIBDRfW3VEfVezjgRbvmdEz8vuUiSqPdIiTsntT7k+u158abpkw5CbgTBXTNyZN
Fk3Zgt72WCFQjn9l44nrbsnTKWGCI6TQWv+QdlIkU9pzJ2XhH/j8kMRCPNzGC7sDT7f7TO9teOcB
fjPwAaCp9voSWyxlttgpxZweRnqFoS7I7IXoSkWKuLPi47NfrCugRPigOAytkPQxK+hgTCxtBr8A
wf5SbrtNhD2umg6ExY7Oe1lee4m2AX6fZoG+Pvlu+BzqrVMsz0xdI6PxCzK7TzZu0rzPzav72uY6
5IpBAMRZIjVpVMTCgbEr0SQJqToMX5FuRMyYk2kmh4KiNLZuUd2IyHl/0q2BToPVdyPv8EiYqU2o
T4tR+0SkPgKtjoLn8u9/ug7VftawbvUDoX7kkHkw7//MNH5uvbSuFp8uGMgSE80bR/VxEf/7rjJc
4MoG5ZahDLDruzcEtVVv/gEUgzHONoMb9D3Y7OueMo0Qf2QLLxbw4dAyr2hUb+Qty73SuxqgT70F
TERZtsXc25vVR7DxzvfUkmiPj3tQf9Hgv4YLH9q0pWiz62comWsYhnn4GtiRfwb9DOLepokH9yQq
fXeeuIcKmApCrI+mkKQPyUOP0fgkBGa0gfrYykEgyWjLIAXlQfBRjNqVYHXUMv8CoD9r+1G1Ejra
lOAv3HOP/NesuAI83DglxX09YqtJTVVGXHjkV2zF7HIFwHLw2SXpUO9YwmDxAaVv3auINmK1OYxz
pEwiBJQPXaTp28ID7IN4yTx9gyGedtCLaXeCi/qli5QH96KsgmBaGLLWU7a1LGJ6vG6R2DxVBmmx
cOZZlipw07JfKkJ35Rcvmi6xfbOidKdHKdIKF+7X2SciSqse4KJP07Uztr+o3XEWm91siIKj7TUD
cVEqQO0EbVGz2ZUmBNoZWzGpx7ofcjxBbL4CTL99iVoV+7G4+wfAAx6YiKa8VZBg1CdW8MchN/ky
+X+3HaYrppC79TyDKXSuO7Up7GYq1tveqvfTtGgZlgHMLfbCncDZSdvUlnMnuMandOEAvcDkQld2
i+NSdHZC5sP1tlGla7J+GncMAMLa0N9wuf/31/X7GPsvzm1PFzCd/n0GJTMsRJQiRE+Y0/dclLEH
/WkB5T67KOcB2oYcofs1aD8+v28ruBSkwg0I5Y7WhKIoKgavbLKw1l8qBx9e3UwJBrx/7c33D6ab
ephPNYfoqeQGcPimFswUwyqv7ZADPTQhqyrw12+JKuUQALzhIfZtd316mCKX+XstI+t+jzkMC8Wm
1mZoBmkSF3m33464VWYqsYTsuuXWdLMHkHA7hwesm1lwrHEKPrE1RPFRJiRGcB1Kq45gIOE1boGf
nxAuxSAp2maS3zQq/vmjTr35vITlKC/e+AdPArakeENYTw34RVHiL/UcuGQ/3kMxJQxoL+HU7/+g
QYHC7YcuhhbRSDJ0zT8o3/gGRN0WvbbjxuYxbCrwQUtwhiEY753+DvdhrdSfSAMv3Yb6XTy1bQTw
4SUMyxLspNoyUIiMW3AS5wwZga5EDrarjxnd5LI9hZQVPFpUZh9n2uP9yVh07boD88AmlmArlECZ
AjbMjlUG0Biu7hnKzpI33/KO4am/UT7iHkB4OBJu/13q1Z5O7Qgx2CH/wHIAD8epsdjgcnCnNTOw
zw8uU+1Rz2QeooLlmYj2v9NY5DHIIRQV9rn9LvEgS+sprAq5ziEEkMf3Z6w3fz9/RTezNUQpTYzS
qgMwrAyty7CxrsP9D+hD8Y8/RjxkvfCLdjIzaDY3wMD9KfFANLXtREsz054LOetzA3fs/fkTCLnR
9mnKkeqDwDjSeUj3kyEky5rcVtOOLzQUCFR8CdhitAiMAwpkLq+8pSXVchZATXozBnyE+0CfLZXk
mb6P63pIs4U0Mnk3gkE4aczpwQKGtWN++N27EtjnS2Ar9IvZa5YXXkQj3T5qawkjphOYVIv6q5GG
Uwc5AMT2ta1tS8/edAuCwDFxmAz9QvxXLaqtTRpAqmFjKKa3HYt4Q8B245d0un8BDZgdtJXnL4aM
4BwcI2hy4oTqPICQuojNNSh87EQKl/0YGFug24+Ccy2iYVSj9a11gNJo3oJUQ1nRr+xb3uQRj+P7
HkTnQvrP9aVeI9hd2uuxFY8ecMZBhQXggHDCtEXSP/4kfPtMD3WgA3sNONu/dBD/5y2+HZ8muHwg
vpIjXazgbHVlZcTTjEnMJtjHN1KXtkH/YWkcI9VqusovYkkEieALwTvdax2+L7Vpenwe/vLk1NcQ
KEjGBor+7NsVPWSzDrk61A7Uw392gPTxCXA9udABz5ab+G1l9tc1B299vReBKtU/KCx7jl2f7EXG
PIcj8qV9U8ZESTR73M7eLkJx1EmeHcHyna39s0VYx19eJOKmRj2YzlJIDAGZsd0re5i3UBMJGmer
KW4C06dOgpMP9bcHCSxM7AgIL/weBkvcEX+Kq8z4x9Lutbk8DJQFM0ZmU7M2Ym0piMtl+Iz5xZm1
gbRsgRfI/QxScnKPoMAFqpGs5pRzc4N+l3uPA6jCLp2/QbdRR4tvvE5UuePFOFtUWRFFBmhrFcbC
bsBSoWbfFYSxSkerBp/lWwiS/NW2b90WJVId6jEQxbBo3KckNz9EZv/jh4N+oi1YQSmHzPxFT6Qa
fU/fc1x50m6+aD1cxLB/W/nFIFGxMWJXcU9UcJoem9hl4O4QbfQ5eETKQOlv7025FwsloeM/T3w1
cW8u7T2fEahIJuw4wCAA73SvaydcFc74MpHXoLCX5eN2csq6+gjYif1uHnwieYbCE+/CneB0c4Jv
pZGflQIcrwqdRNafLthiPEovVV3Kd6XWtrzLm1DRXwWhoDWjQfpmDXqv4TEslmtWbWbdibjutizA
I5Uufv6JjZGkZz/sXkQS8U2tKhirYill072U/aJlVJh5ZmG2qK/ysAl+fvI792Spd5esjgi/ac0k
3YAwFN3BigXJ2Pv/eqzS9tVIb1e8aVsFL8yPWOyPtXFxBA/kFA2tvICRNRPOkWB+NYhEwJOSb/vz
/DYVD5O0g9e1XY0l5UNb8J9wemXH7cig+/q0nR4zBKvHHGRFfszPj9GLLAHxWBVWVGeKvHyY7pwq
PHPgEqd96bfCs5E1kD3fhxk7WUtoNr/KfmMt5soarfCsdYD9XGlC+PiaBsdulXY93qgEwTZ9xVkJ
N26e/IqBKs5OjpNsEKhFzn9q7JJcEmTm1ERYCpfPiGHtsPvzTbenFY3TcFdMYIgf/Mj94M/FUTH/
77qAZ1pI1QmL9e8KS0QTkEpupkNAT02f/YNhdqeVss/A/zPH28IIlNSsKLD+kfxTeUEcgsD2m1Oo
RvTjrTCIO0OSFh2ck2A0q14Ukcww2wQODbn3MwpVTZhqGE9mZYpfO0XU8nk21XILOEiLsNvNSvvS
U/UC959F2wcHO1Xf7b1ZgkI8zEze0/bf06suld7i0A7vgun7J4Gzb80GSTaLbPKmdlRk+UJsooaN
qcdFd+/kjhFVH85m45eWL2WnfQUP9SSjaozmNV7gJ0iubq7GOMObxbbPTzOqFp1VHJ5wc3KLX+gE
a7xAwDkEKkEAT3DEBd4hhA625wN2mCjqvJys6cre5TplQe+2c+qX0KvMA8OhC5izHiJnaElXQDTl
RJIIlq8O4/Z7/bXLDhu0VfavX8yc5hx2mHjMN4yi4JA4Eg64MnuvoY4PHBcFaBTBOZTdDoQyqNiW
zXw0oC+qkuLGpSHE2R5EPJ3mJBUgVo7RXcDj76WcOCBsCnCiBQ2PyXxFvzWj11+mAmTSBBI197g6
+uqi2LmE+ESHSabi6ey5Rl/JqqNyQo0Grs7PyuaaVvwX87euNeqzqpVIwqM21qL0Zv+Qd960No6m
ZC6zMZGRX4GHD5OJYSqCl2fN2WsEYNsOIfv1WcYLchAapl7cgtkiOIULAnJedEtdN88dVPrDoEx2
YvG6UAvil0TR3RZfcz/Glu3dyvE5c8WfVtSZWncm+w60BKa9uAvqPhG2BX0TfpLJJr2quP6pfSdl
E3s2PnnB2NY1uzBnBh3kPHElMtfw5xfULEhK+yObjbcXoS0aoEJtB1aee8rAj7Cqj866baM7KvFu
YHHT+qNe+AkhkQ3BC0QQfnREdkA5N7XCAKU2+dz1SvqOR0MIZR9UXij4IwZQlmzCyT4duI+I7yuf
b8vrqCdX254OmcDzaEAhYW2wJFZn/KLCQI/8X3J89bJJR9QBw3DpHCq8kSKWjRPB6IEBCQGJT0+m
tOJzqCwySBnP7xCRTim8BIdzZpIraQBO2Rw0rZFCqBtET1Vgbdvw+z7F0DOUnTspGrS3zOckKxQI
ypC8/LD0vZbagFgtRp9pqi/BuYl2QMOLIeGNPyiDhu+jVvauiVGPT3cLun0ZPOvDVwwhVqLBdLHY
gfP8eQdVA3dSnL8kyIzKeqhjWnKOZ1fTsplZESXlfPK82AsFhZ4VnRzNWdlyQ0PtUs3Hq8Q+kmDn
HW6qzHN1i+kJCc7Zhc3k7fV9Y/NR4QrgddVjrnEGkDNZ40PWSn8VhIEWsI+YmMGx4F5p+Ewem2pn
ScHZ4T+grzla5kZucgCTAMEV7PhsguHaEQgK6UpUE3gUY8oct3AkmPIl1ANquKus9oNMD6dFY1BE
TZjl+o1le/Ki2l1TLVommS6tWPXcKHSURt3jdTDdRXtOF5ayNaGeHBbIkUpHb7IOaa4a5WxScEGO
9tZesNvo61Fd86Ht9TscTZBQZvFixAVbjFcD8tm/Z7EhDa9W8bq+bgjql446RKQY525UGyy0LIyO
SeME5vR5P3e4UWxv57mYwRYeDtQ93xbDFJaWKuk39AJCSKeeE/t9nP+b90/1pYrO9jZoC9hiQMKi
phjLYIowlwyMStXlLA6aLCpSz208IBj+FPWhMpJJ7Y7cDqAUGiA9Q921bUfDSBmI52ZWZ94WX5/T
85c7SqRB0U5BiHZyS145JClAfeyIVreG52pfsrzEB0dBACEPFTVkheW74jk+d5QwtKX+Ix3fyFlq
KxZ3E/5gUv8HYzpbAFrILrt04kk0GAIVsInYjw1uf8paRweMGhWyU8JP9+/53pUsBCwHdioJZhDR
SwUV+sNIbuWYVlSrT2xCsr1qd+3jAZd+0ZiLNstrCyWj3J56CudwxbzUCf9/kgt4vfFomYsoMg+/
G5NW12wBOaFPh/wRQdU091zkBqBFo1b9jMG+JHrfinKaOgYhifVetWdc9/oNtQMDICWAnc/dDPAk
dPzRMIHt+hxJ3lL+k7L6t0CrO45oyhk2yH5w/zzFV4Q1c81oMYsd/hRMv7y86x8y95OUzsb5qKMl
rip+e2uvbOfKF/ik1CNT4ycn0WwCEnFWh8eSa+iWw1iMBcbF+jjyUxn/+hISen08c2ve1YFxHXIs
DZT+kQRhPYiDzufvDHuo//2DmWxmfG0Kbg/1UGtfKY5VD3DhpXneIjWVJNbORJN7/08nRONCuUVl
dy4WNgUPg4cEcBHQnUw5P44d5pIu+kMN4GoB/mbeWYD3JvS8MckyxFHFEGdr/UeCWpNXsNZ44OdI
SGolwQaXAEfBZDZzdkFDESMziHFSiP3RkiAbuo65dZUl2aNZER0gxhvyYeXJsqpxwX5bjrTZAxGZ
gQaVfEtT+Xe4BSjxMt7QkTdjBCS3aJrTtv/PfP+nJMJE4FjwerssMpPykpIR9ZRMU5WixobwK4G4
x2szmSP7UuA6F2mNL0SNuEVSA78VFgspWn/rOFAXLqdGJogkPSdJxi3yWZaaNJQKUO0EIrgHTOgW
0Ypkpe/xGVYbCdAZkSP+7fFpCdDSfVPO9CFMtToghJotO+2TMg6FaYYAp2d7QU9zCbgQtlvgGspe
Vn1ainmG5cYux9spe7idkEmv11CyiAQigbLBiySc7+axbQfkbul59RQ6GpWho7cKkgB7rc7U6arl
brU8Sz/mD/S474OZIcRMaQV0ZHmsFN2AZdIIn5TBnoNotcaqKWhznb8gPPDDPUYLUUvqzfrjzOr6
v9nKvyBOQuFKnn+0zoevVW2ni15pTyjUCndsAbG2hQsfnQNib4Dm8bDnhau8YtfUWP7op1ikLHYD
d9KQShnDx7QoQxaBNkxdshj5GtiufMpfsDg4vuNMrAmqh7ik9Zn6ieWMEfwfAR6QZmsvTwMK8erh
Ih69sOm+DPprj5k2am/4R6i6ApNaDQU3Qd0T8EM7qKCQcge5NH5/sRz+2M++gyxlTCy4dLYg6We4
PxcMmIfE/vTIuCiwhHH7GldL4j87LaE1jRhPSfD/FBah4TG9EDDxT7f92GfVrZ2PLi1WcclSL8zf
zVg3WeKZvKTgVDCSkLchjc2oWl2eBuo5mFaCnVPFMzwf2XHQCeW2DVwq5HCPL09xGOIY8jmo+DQ6
tq4RYV1JiWi1paijEYrbuWov85/uQ7jyk4NA/6yIcQoCZpbBteQYAe02T96Pkraju4bnYRde8hGS
FOYUcqJLsUlvXloxLYY26x5dTMSR8XHIalOuCAOzFkC7MgSNm3Wzz9D8N767gQVoaa4IaCwspdt9
slbZA01VabKZwPUjkPvJJ9SigvSxZWgeWL5U0+mKv03kKaAxT2dBijGQMcVbWIojpoc3FbNiw1On
NoG3cmjlLcEl/jowFpHKw/dFyKwQI1IX03JO3iUNfINEY5BWxoebPn2/nJKaLm2OMTFNVd8AY0HB
wlVxwygZtV2m1a7rJChNqk7LcYD7CBl2nNTxoS+wAN2/hunYiIAWqsM/4PNEi+StDI8B8mNFODIm
Ew9VtI57Iax29ZSZft3hv9YK0D/aC8VcaQc+DEU17iJ9bEIQwzDJd+kRbKRTD1yE1haVAQ4e7a9N
TUmFOYJwJ4tpN8GDQa9yEF5uyiIm0QJ3+VIeNe9P3h9LC+khemCC+3/na8dzW2LdmhYT4rDrOKqG
53jW8PhzY9viJkVVAmquBKDAPXvoKYRo4isuxI8WJ1WleYKNt5fVLI9g4rXBVjp9pwqdgo32ZH96
HblvOQ+cWsdN6Ldo1CQzc45DQwgUvTzVBo8RdmjIQcEvv9pGwnQRdbicMpPfXqtUUVjMiavwq185
wKMctVwl0GKxi6MkhFePav4Zn/x+OI6xAslzrybSnn7GgUoSLgx1Y8Q6+cFv3TL7qtBzQx7dNkaX
68IPyZ+apSPANePAwvrNWCbqB4gLbNy5moibVbGGDdRbyRNsW0VpKqZD53uWfYdZ9GEAmNbGvWOO
wy8zRRzLB0sNyXfuKgSLfUm++X+TDUkdYzgq9ygYGkjYB11uvCLUZFjVNxYt3aJhnuhZoNIUioTN
qjh+34Hp4FUXlmMM2MmVyL4vu4zNKh0b4+mP7bk46BbCdlaBWRAbtWZ5Dt5OGPGJoyx1fAcxEN3g
MMUWO/K9ZrL3ZjK3cGzvRKCnC6VHVglOebNXdrt7pia6zYOsCsvYq2I0lEV32klKuFg0IIkfBCdD
hQbVRDiuyRBmi2v7x2UslScXUoZ02OOW0yUBYzgyqjgtsqddqjZdGkfpb2Av9jk3HPUbQvMkP1tf
Lh9hSd3x8sEb0/aj0cKuCkXzCAq28rpmqHUNtW3JtBzIpUHeoPngUVXbdY+bnRq8s+LTwfee5yYp
IytJAqVS5HDYmPZLSZvMPhd1H2Dckan6IoUXgdaRlvDM4Y5hih6F53nAyBarI2wnCmJf4g0Gfre6
TXhNK8LhbPo+OhkurZmg0uQ4qPQU1GuO+t4WsDRn9hNXwAkbOnvjHw4HVAGRZzIfjHGlK8x0jJ2O
1jdZk/HP3II46kRLDTFgEGu3ThaHdWPkCU7B94dHq9yNX4NY9BgEU3o5XJhp/cRgBuhjlGwcMZzq
hnLojc4m7paK6t5apZuBlzl35rhmeRW7jhwlL7ayUWqNGYZrvEU5FmAWzy7a+XYXUu1y99aHczhx
4aRAVRcnKkMYilt6r173eznAKRS8HtTTSCZX4f2d0V88NqaynGrsS4csTKSWafFZXDuNLLq8sn7c
S15u0U4DJDNq6kK2YCRpozPNFbsxBjIn/K3B8x1pxLePU/U6Gl7sDFJvSyyROVD8dRg2fJ79tLGH
PpmeobKmHNSXLO9PphXDLrDrcHaWi6MRivsLvD4Vdckw7Iw9kWzn7EDG/rEWdLgIPNZ1YyHjRJB8
YUuXS8x5wMf6t3mtWDgf84pNes2YWeg4PIEpPYOuHxh7e2xnIORGveyb5mOT78nTLB0zJNCoLBkG
rrCjFbJ+Ek9qDHOmN+8rSFch8lo0xZAuJZ+E8/kJIo+sDQyQyYR9vM6v3eSomZGOy2DOizqGCdSQ
6tpamq4vd78ixPNZAiWMqQB41KbkBrX20J7XGnFnd8Owzs+/CrUcfEEKBUrBNcV6MOrrqYslrFmH
sBptt9x6TA4/Um5vgTWNDcSOAUr1R0jsNqdWOoaNWUHjUGfPhgnDiits81plC7vLgs4C7aazfNQ8
JLxDpEjs1x5pEMmtzNeJnj483L8osgL4A8/bWYKJT9VMCN6gQBkAJ7YVHfB6iwncWeddIOWI+YYR
lM1PHpnuZ7sFzEdL8KUXi/RPJRYHBXfJx9NgzpnQUa8YdaLeLAf1vlerNkiGtBBPNiNWC/mjZypN
iGlxiJWvhcVmAMI2I0RBsZKcWUL9Ve1DilK8H2UtDdr+slXSfqwWhIOHcBB8EEeha+9ZsWrCthEN
+WAEjw2Hvjfj0u8ovy/pSWQ+ctrlwGhafD/MV4cHLUgbN465jCoBEZbE05vEgwtAkTUyl0hVuxnC
zjGdKl6Is3PUw4pSoAkVO+IIXdVsz470Vh+svglgjSF32bM5HwmDUlNb71gPKWT42amdbZvBoBvE
CUEXaISj3LVPREvCY73wtGfGrswZciVpNfELJYQTT5rMPtQZRdFtLbe/tIpFUelJqYQflSx/j76Z
cGhDHUFg6j6fui5JCr8Y4lCufj70JD0tSDeZx/SPWyjeDrO6VS1DjtUcSdSLdPddv8J9T7xcLlpb
EZddl9C8Q+v1xWoQ3Ga2qYp0UC9BMTUA4Oo5m5JXJ3sBUbOMvn5m7XC8vVS4zP0/TgaqJ6ABYHoA
ihgjy7MNxlv8mhLWIA+F3mHtnfiSgvJ5rWrM0ETPSzXFavM0uO7HJvYlpy09f738rckDtBNit/Rx
OrAMXrZ0wRuMmN1cumZxe/F17ObFBtXHfIAQpl2ALoy9R125jxfpY1mG//mmJiq1ZmTnF/Lmw4jC
UggdPORuQ+kfclfRhkUWaKvAA8/5wIiXVCs66AM/DrwPGqJGh9ln12R5Gg0xw1kQe5i3qQPNe/Pd
QjaYIPBtm4cPeJffS6SfmXQbJ4x1WzcJ3t1KUHo4dJ/MJHxyWlxMRakRRT31IV55aWa+8JJ7wCSR
/LjO1K1e/exTktVvdrMqoXSQWAiHctEM6D1c5Nh8jCUYHI760Xa+XY8GMSuzIfEMoG9/16Qo9LE3
MDAtJwRMyRJg0vk/0oajlT/wfUAvAvyJ08fyL7SdkLDg1zSz7zkTm2Wc1vypPctlboJjl0gFdjHV
smNcbm7UCJ3iV2TlT3KlY3n43cBtKgiFX8JUKOwPO66UtuzMnlAiimIM8O5pntIByMphR53Tq11t
7A9guI9svUWe/7gIMeuLYYg2uJzc+CQnao5S4Je4gWxvfH6B1xu/k8hxe/Hwc+wSlklLyccwBX+m
aFOkcRQOCV8jRaXxLJt2E10UIWJGH15vd7hLCWHTnp00M0riT6O/UmmCEt3EZBBUmsyhZ/reOlvd
lbqBeKb3vbhAV/r8gggUAg1U8buwhlhEKznsMzAxzautIn0XDXlvBRpGoHWJS1w9FHfPhPiEJhja
t9SeD2+73wNXTll2+ChjgtLQVbAJ4uRKWnMMRwkTFYy6QDcxB9zUDFh1OHk1QiT4iiveero7nRlT
aHG41gNhtV5oDIV+cdT0SE4vesk3VS+3LHlZC5HSR2f6EOvNEkLf+cAhWK/7jLMw+RstU25pMMxq
+M1jQExTipLM0C2/yQk3fGnQHM3keKMmOtUkiWwuvDzeHhL9RFsqAB3Y94EyAMVPjcPkgac3yMxm
Wvi72scLXl0cRR4J8QuD7TejmRMJ1yb1xCPm3jJZXVqeANKqQulEqdTDQ31+uKV9QabjcnHJ2nrl
5CpPbKz7DydIZTTdxQyjX5q1EsApdrNz98gbRzQ65hTe4OEldaH+DgYN6AdKQsliUvfX0jWVGl/v
UnYA84ThmhTFETQGxz90YyCySDdLppeeRn8x6l8q4cdzmKfjF2Sg0BkExMq8c92mPDuNiTS/TrSN
m27U4njs6qXWj71prEc/73L33HZLMK3RA9h28jDR53rjtSRL7hxl64ZJL4ovfzLDMXZ9m4Xa/CD6
JUFT30YQmN7nwvr+B4dkPXDjhy6wcoUoImrvVcldAGK6JIAuu1XEwcmb7OvZA8muKGbMhpTpTQPK
cdheQ8q5LU6ASXOTx8yEOkCwOEgGbnEjwHixAVgFr1GW9K0WYEQCPKCm3Y9sfwpodA5BTLmkkNqW
ePdCPRGxvqiRF/v95j5YyEsK3ajJehFz61jwH4aqnwBucD6qqEow7wI59cqZ0JmWw/uFD0Gi7Vod
n//7FvGUVRq/YyMk48v6GsreMyav8LeWRx9uVGCbK/Cf6eT/hkYjE6B3sHwqDjumCrgJS9ttAIio
gG1g2Jt3Ai3Zbn2gDeYAm/L24bkz3wKJfUO5MxFcji4Oa2b1vpRfCeIlgOlEoevw3AoJth3Lcm7S
Qm0mPjZ6AFve2U568ShCi6Vbkh0vlE4fyUiEseGdvwSpMAR8DCvrIu8XTEtlWaWIOuQ55ALVa6n3
0PIMlPWXx1dV3tqcq7hcBED/1RiNX0rY3wn/aKWElWuoInsaOI3JcVAydKGwcylmoeCpthxQwc6G
6S76vVBkYaHLVfxbr07BpIlQlS78wIueXc2NWDYCTOoHMghTafR8MRuRDJ4Hw22hbdEMmpB3QYTj
YFCiSOLosEWzMY40eXYnm51wxnwLoAArFsQi96rwq0v0QAFD7eqHoTQTuIr9qe9QAPEiQdgzdR/B
U6S+vje3ogNq7ZtQczpZRkW+uHGKRHvPJyHTU5Tso1mhMirnBc5E30008pj2eehIkj8dPrkpNNvv
benPZ1/l3STam2iaT63CyRdMmzaVNmxQYlDHDIwVCG4IBPXnvIo65em3tT3t5KZZbF8DIaQ2Kp8V
yLCzU+Gc8CHYDBjNptHklIB9tRW5bksWILGh5MNrL60OCnitWV9rnreN2BFntDlmuBYHgHoDtY69
70cF1XpfCRIGlArKcokbOPfZGTgw0VzoHLfxkxx0wIymhGZJRPO8M4JfoCe4VpaT4zSj6Nc3/C48
kQveqfXtgvJHAEeSAD6gdjAXKFiGuT9eIu3oe8bwmFoicO9yfiE3Wkc8BOLxhwb0/YOgEBYdk5nE
qMPgidjPXvdwb8TeZlCMDtwShP9Qyyed+4wQnNYcVgGdJOHr/iDs0CMQdz3OXD7HyFEIhixa9H5L
7/Ky+dzPZkvPPlWY0yxcKmCjOsrljpIK+BQ6XJjc3l2Y2sQgLB924wcsMXHG6ynpdLCBQxjZKrJj
Ywyzrr+PMeTh1lf9n9b2QnOLN8husXHclxvnOr6FtX3XdQNzNCl8Nf7jFJ6/nht8T7p+Y1DyYz6x
aylQH3fJjp/Cd+gKMc/eocVxljTZXrvospq+TUkCT8XlzQtBbgPxkghhGSNcyxxc2T5FPXrY4l3U
9Az+uo+tMWVE+o66HXoMNF+L3HWKNB9j3BVJyRHo6H7f4E8GkDIruYs1FwxVkAvn5z18BgNAk89Z
ocvAZOUl6HVXN4224UfVzt2YmIgDYmekUQbd8B+A4Fs450lytdZ0nDCySwsmtKdb5Rk7mzV9gAO9
4SgH7bwOyW+SJ30ARv/Pb5bntOtasBE2Y2rItjwheHlI9HpE70B4umpusJuSOSwdTAfK2n3OPbWr
vxwdD0sO6t2O1/TGR7m4tW6LvtTa02KwAt8Pxb4tgZM3a+LOGRhPfO6dPPUox+rc8hEQLLoFtLYj
fJwS10o3ElsrTxLEnY1wyeR+gPBrqrKjZQcPyicvx4+rVeI/37SjT1cBGrhl8MyfH9cW98USlM2G
tUVlBf+Zi/ZbJDB2GJ7nlv0P3kYMo7ca4wpy/REsIuG/Actr5OvTjABDVzEAoUpZWDeim+RZ1j0D
liipCbjjld+NjZZ3WO7YFMF572wFzBEs3Ei7znhWSJ/m00fONN/nJVF/CzbuEkEaSXLVP8wrHC2C
AUOK1Q9WweTLXotc0Xqi3nPTBxbbiFj+SL9KqjBODYRwGpYtGueT2XmDQXJ+2IeRNzoQgCyQqYmB
1LoYWvbMfod+jmg8jfQ/fHvSdn9paKc6UcNy6hyV5Zj8QReZY931fNs/86SRoHyiZayakBS0N5rk
fMm4bAdSSRcy34pYOk/HVI2QUv9hzo/h8xDej9nfXqhhyfRBhAD0c2cQpQgVayF9P8KuGpvNTULe
6dto23lBsk141BHJ3P2NqLw0NGkCCAm+x4Q/S3jg+zdfYsVADB/Pwy/nvlX+2Jzb8trubHzvkMBD
FRZ36U510+S97A9evaO1lyLpC7ukweZYH8hs9GJTTJ5s4UCmNJWSJdHDTN7lz4nYJGoCLwNT1PuG
HGtMyvM09767Lzz0HVK1Q7ayMExyCuD6FUykAfrACycUD8+LJOodUaHJwahyW46e6k+pi4652QAX
3cnalWkYQ+6bDpyxtscSuqRbJrQl0D9DTKrgTjx2bTOyCYWKVVw9aHcJM1WMvm3k/Gfmx/HHF21K
lkbn+6JWqfeL8ZXDpSif36F7ybkZWW/Z23KIeqYyKLT0aHmyP3zRpmcMxY3ujTXeRMuwWyv2VUYR
kBTTAkZsyRMR3aNM//Juu+UejlrLS6Yr0GZtdWKEqwTLSp5yk9b2FlyVkc0cwZ03EnDVBaNu4981
hJlgbkv9devBSHeueFuvpGeakjshIp8MKxW6Gfa4iYDFenqD5S35GpTiuvw75mY8XyJTObS0Ac62
rvyHBlXjJNuPBfVUVKQ7LLzOtXEeTmRETGTNCDJbx/Tb8X4bmWyQiiseiPlanjpn2oBIBL1jrE+f
0WuganIM8v10IxTf/8GLJybQHWfE6omKkUaSc/dXOCf2chsFbiNbu94+HY1ERM5G4FDSbrMCBPKf
7XNg7k0DdhWIs4h4HBF1Y0/Et8C5mUMH+zXZ+SvTH9P7odhVhklNpzCPssi0Da5cxt43QloANIDD
o/3Im8JPmtDjQbbIjNTn/n+ktOqXKyHygmBrc8TVVkykI+aPvIaOLljX1gHIBvAdujtJI4QUKKje
yTUu+7wMdHSOtUCpiY2ddTqTJAhOJrdpPCwCcd4EwIqwSE9jIG4kjSOnWWgBQAodZkSnvd8fqaPe
7kG7OWTSK8xrpNGCuVwyEQyORambFiBsTVEGt4JMLdcov5BDTRdI594b+seGm0dlAthTNfusqVNq
xDiuNF2W+c+Wlsbs9s+suKBS0QWrCk/TqpQIymhdX7kiEa7NsVJC9IPh0B2DN2uyMKmzZ3ZDTFb3
X2N9b1cIlCR7Z1gVskSM9jySUJunYPbU17NMHcu8hzg1EcPlD8FSUm+Ss2tmF9hs0V1pK+UCCfbT
ewugl6PsQRvUpr1Pq4b16WqXgeTjVu/quE07UhadDHmGLI7fovG46hYWPedkBEQvY92qhahVR37c
FZK3nPowaJ5vbslX4M0pe5vyly6+Evb7yz2jLdYYKXGnwtu20e7I6w7Gt4g6GN1ZwtljKurXKWyA
389xrVVu5JVRh5qBtvHXOO/uKO3KOOy6XMrGyBEfgYa5WI5yYzXqEdimMMO5ipkP7hdfdi6pLCoF
DEzEgF38LVS1q2kWziiEzTbkwsrkihZdwz6zkKa9+t5TL62S7uBJBVKX5MMpEPf1uBhj6Ur87aBl
pJhRj+trDU/63QDW+x4QcabYQwwr40TB47N8DO9+bihube9yWo4ZYp9rlZWn7K98k/xhFlsmXdim
bZHVUOcRgFX8lHoPZaJQaUbJegLCdf7wzLIho0LY66IoLbt1RBzURExglMeEereJmjVW3CLcYZ31
8860eTC2G+kMgsa31ZPEoBj7/MonseVe+YA5NRKQWNSj3VReR81ZA4m7VsTXmi4fTAi38Ct1wcTC
EnND0zM8JDRo2PbL/cPzwChfoymSlLYFFbusCz7eYYXPQfv5jXAN4iuEwzxhsUFP3EjVguVm6llm
s+u65tLylq7ynUZ2E5mbns/1ZslCzj1m3oKEb8UFu5pAu0YJHD8F2yZR/2Au5EYSlyrmEmmy9glB
lB8QQOoUeRd8RGVzkhS8Q1T6R4e36MxJp745+mFsc+lTs1++ykuOLSnmYAZaGwaD0Q8bgZ13X9J8
cR36w4f8+uq/emHc6R3/+9Dq3DSc05znvHjJWofLf6NyYa3dD82Pi+QCVAM/6BhXsOFbmLOIhIvq
D5CbykUDwwlYgASrTxHxrPAxapJ4oCkAaDSylirW6hlK6tymoErL/06UlItFVVw86LfNvQpsOZMn
2IqRe2Ik88VT83qcQEZcFjSM2xGyk/2An63dg6oTBFyKkXOwhXXEMh53m7+JkVzAaU/Qv9dXCWzo
uXEnw4bF5ehYhI8YHWNBEf7WkS3eGHyjAhjj3pQZe1YOPfqLIW/zmskMJ04a9Vg1NLIibCbSOpJk
BOfO1azDw1bKGCGSs1Rjadmi0Bqpm+X9t8AwFjjqJn5N+rBplMMu3s3JRS0RsOFtP0stQjZxdT3Z
pgvdwbbInSD39/XK7OSmkgyKPNjzfnta658c59HUpqUgADLo1jPyaDa3TP5OxOQE+Mm5RDy+Gp7o
RR0xWcJP2GEPELa879shHI/74UAlwdPzdNuO1u1oPV/s3klD0dA403tJSuV85B7BEidqqdJCokD6
FRT/3U3Z6fijBD0QIMkkHK6Z8a87t4y/ZZpP2MQVoxufyXzPDVdX/HXZb44BbDdPUEvuVthATuI6
/Kn3uCatkP3l8u443+MiIVJQElIkClar4KSlD3zHjQc93oy7/fxiKc21WCUS27Cp1IoZYHIeLzIU
UU8TiffDk4gmUp4oe9dZMdWUPejL1E0RJFMylF1DNyINeNmTafmXsnf0asnMqqDUOW8JtHYAljNB
VFiHf+a8wCKDyw4OvlyqCLmRe7LrFVrOTI+IcpEvecLeKSGr8POEm2cUWvJmljkNpJh+8HHArEdK
mbUziHiFYrIyu6eOWurlgL5UPpQJ4qIdXw4U7wWsZIRsXqHvJXhSfl3rwwls5HzyrwAddgFk3Gjz
CMJQQqlzAH0zwzF3H/2HCQ5wVWC1yf0YG9fjctSqt5y7fyHvs/qYt7TRjdcH3dVo4uBnFdMMtjjO
9OCNICAof+zuzT/ML4+HhjG7RxsOcEfSDj/C1zTeJ5S3tS7k7nB1uuermvZQCTPTaCLvOL9Akqig
1uB8U1MFWAyFOvbF+P8AkTPuzq2ckhfHS9jBXGm7F6yX51UGtWKoMBb1vvwroK0Yh2HSH0WAy4WJ
ZoQEMPkCGDicycpQl+/ABH0pEkyUEGg5C7FbpRVT0CtgJm2wLIVs4rvIu46d/Ph0TfxijnR1YVem
HpPet0N28xSfXU3EnQ+Jg7G7TX67CDE95FSLClk7gaeManrhHDsNXepeeRVvtmz4CfRQNx7/IATu
nC/OFXYSYc/7ASPEzZL0TOa/x1ITutRJNW5DkgBCu16HfQ0rBmZ7EZM1Aoo3r4yU8UkCMQowMMgj
Xz1isWA4WEhIjYykIK0k6ZgyJ4h2iy/SvGvioUFsKgzVfUwIHpXRXsp4gRjr56O1t8IvBQB84J/V
UNYf5rZKBkEk3hpa34298g33I0qK2deI6oDjIlEjkOdBb+MUH9gd+6h3m+99rDiS1sd+ehHLbFjk
LgOKEVEBrheLOTJjlbYpFZz99i9Hm6kmSv0nmPV3snVd5EFcwe+iGoTRbeY6XPptz0Wr1ZquaKSg
MTGwpgxOgWz9qRbVduHxYvC4hdY6fwKHScP4wfSkNkKqOowCnetX9BeM0UxNbEqqLH9gKH6xh39M
i55JooaNq8V3ugH0aRgHBOQHmWqYx7IAHcSlMc1Vxn5yJtfvTV3o63RfLUe9yq0hwgsSePBXDEun
1TbZNMlI+Q2qWTVa6QWh5JnlCdwEml2WITh6w9QYO6ROSb3UB/xjLKCNVlS0hnMaUVHDs7f9JmsS
WWV664VHvUUY5UsC9+tdjAeBedCMRePSoF492XdxPCIPUWF9OFIFoImgWm9MHVrOCrhiyJVo5DJ5
jjTs55qDK8Jfzuiwv88d7sp9N99IU3SZj5n+gVPqgn7UWwVgAIxRN6WiN0wnidcI16Gatq3GoqM9
rejXXDCkeSWr4YRwi79buz4oqd7qccggibmSuwCwtw680Hx3X95wcIbpQEE/hMCCpmvWE1uB0zHt
dgmZnJbclJf4Z3v0DJHHe70K7/KdLaeHwHdGrNjKgAgE1orzjrZelqjcBc8wuiuB1TbjPz5ankiq
SwEvGelaLOa5CwVrQVJdAXqMm3YN8yQDMrgs4FoHLf93as0HD/p5lvX6+Hg3RkK6JVPWy5oIBQCi
Zwrg0idLDNS8uCfliguTh0E7vdpgLuzZe2etwJ9Ye0gsOnjSVvttrepn8qg9EfvI4D/cbkPPd06b
zN7wUfZWwEXM95ZKyseTahVODTOAqnDQJrdCKqzA2DkURv5QNZj8ojCRgdOOAu4pWSDobcYABMfv
jDt5ELVfgg6C4zxNCSHadrxxZJdVZAYB/dJsCy3lc/6XtmPSXrc6gfdxOIoie2P1FEOKX745CHST
kGDHEWKGBjIWiLthXF6dBd4GGUq6cgpJT4hHXTpqlwaZiVewvisFgy3/40Un0GhkfNSlffVNUncI
0kiEqfpG8/EicqMDdPxjCvXATC9HWT6loCQ8Mf3KNQhu660nj2OJ7O+gio2kCpWx2KDIoY53c+RB
kN5xj1hjDcgY1oICNkzv0JIBnmYNErYyzOHbLteZYaQYnYBb2kc7EgqoWG54LAxpWrck4RgMfGdN
r4OOsJGGSPGy4dAGCW1Quj4UlZUcF6sfhkkQdwmwWTFuoVBjdGz/djVse29xij24e2qfew0BssQU
8qhT5oJG/q2vncWGPyxvl1i9Xx5NWvVXt0E82vvScXDutwwuPnojNGmgd7AJhrQV7Fzu3m0Uzdrt
GGR5+JlIt8C5JMVrDE4Z35E1LFp9S661KQ+xpl0eeKL6a6KJbnVY8F4e8K3kSHZR6AVgn4VxhiQ4
5NiqWByQQbqWl7R8yXigf7+sTLAjwMYfeUTNm4Qc0crIyLFTSIfx85I6HbV3zXaDCEuIBMTteh4x
yw9KGaIU0lmJoMQYUhCgWgliaTf46fqT0oZOJa66PstsUCogoWD8GY9OstTkelWKM8GCGjib5eyl
0EoGZqcVENfOqSeIyV4PExsgn/c5rz/rfMy+rE0neSkPRdy7A9xKnMcypLqXk5elss4T560YZj71
h/Z0bybsA7+6AQpN463Y9ZuKfMnu06RbK7onyjx7frq3W5cINjPiKqiOKIatQRJR7IRlil61uguk
+gbrQ9FlmA3DRO8l2R4RETAng8QI9IksR8/k6gca3nm/cnB07mRASGqcMUR1k2U7BCIxTX7JN5eT
CkSzkOneR2DQz93bSHdPbM/6AiXGhn4ARCzfAr9PmI4mXBw/nmJm13iwyRreYXOpBhuwxwGrnyFh
CR4f+aM7kU7JmsQjkdBv7nDfeC8deTgx0qG8LHA+r9gVPOLqYrcBJNWhl0swPFj9v0Ok4kAzcyUW
s20CwHNXn7H62bIrVXMQ4ciYRYYo4+vTu6aXJV1gjfuB+2/dN/jb+o+PpVuGltFocsIdI8ByGZFx
Wi4y6U4DdOXCTUh/24sAkeazylqJHBGJkYjYRTf11Yb+XjXo4fV9sADT3MZBB5YmXNWzq9Hy+78h
uDdsiU17MrWrDOcR7Nn4qCLhuHEETNguLif+0pBozqhtuqMG2wbTpm+KBQmzhQs8gNNviIkXSNds
Mj1l/OeNbVpIPrMLfmP9VJi8U7kKvsxNCtP3RZuxPMnk+SJQECLMIhKMLNwNDqFQFiDaKAwx+uTp
jdCqULPjWDKYj7EAkYhcw9iP7ez0rp3SJp1u4AIA5t+ADk10CjZQ+pQ6osg/zf3dSBoEjnDTLFCp
Oi0AQOWYGzhRrqh2PH87mlclNlhRjHSz0JA5/HA4elMHt6fTTuabK2KVQg328sLA58+RHbwib9U4
EMO6E8sMnU+/q4/BxQmCc2FR+kTFdeXcDNqb9HcnxiGl11cQIJ8TxEY9Q11TVTIwGhVb9+WnJPma
6cxe8TuJvNt4RIertOylBqp+MBnK61Q4fjUz0HzflUt/iSlLfYFLGNI4p/L7I6F4hQmlLsmKiKxL
33axMdkRxnQo4m1sxYHQZs5yfLaWKNehoZUTczbzTAarFjiyvyupPsQwJoptZFUZFknbQ0clK/L7
7N1IxrTjFCbjHIAcNGMZh3MmnwaIjjBEX6+QzSQITjLhbXTfGXuSOjVCMPCWmLf62zHDyMqlPASo
+HoJ3gv9lrYstCLQ+C55RwZxUU2lM38+lHp1i6IcLm+4uLZXOeGODv5NZYr8NNuNauDN6Xreg0IY
ZPNBRBtSTcZimuofy92SQ8AFqV0/Mk1AhkK+wJvzYNEuNrOawI8jiWtb4McMiZBTYiQtOrOltGoo
TKdEVhGr52IVmfNM7m/G9dnRSifqhaPX4kSofQvPI5aCeyrl9H1A20a/rWmMI3D2CcqTsnqTLcht
/5czlOm0Rsy3f7ybsNgV88tYJYAqrYxwCJNqiSzCVhHqsTvyjSg5ytKuaIrVbH8B7qn20ydbGnrm
jqosS2iuj8FJUfRCWo40/iSJs4eWUUUN7lj5eGs3sljH9qE3qLRprecL5z06HyTEKv23ABjNiacu
dETIrxKvLsYaiRmu+JUT/P+zNVh7LGG6tsPxx6GmJ9VaFVid7v2j1xqvAVfPlx2ZusSkrOnK7TGg
tu3gYaVrzMOMBKM3tUDNgPON8Lm8Wh5ElbEOFZeyTRufH2pwYCWX/XRN1Z7g+hyW5bF0+++7Uul8
WMGqjHnSC3jVz4giaOfX+FIwwG8EMEubESxRisPpDbIUnid7XlINI64292hITO/+2aZ4fb30jvuF
2THQ+13F89junJdTNymMn6tV4nBvL+UVSu+nsGMlIFkfqn1s6FcxFZk9nRmbdjAHYJ5FTrcQXgVV
0gX6NpmDhm+KJ2KeKK5Hppa2SoAY3BLf0kKDqMG+7OvGjji3OHqe0K+u6EUGilrhUNfN8zsWD2fp
CpAVJhbqXJWssMflIih9YPJJq3uu50ncBXTDLs1lnuILj+/5nBH54G2S/6RYdPxU4gJ9myUVmmGY
ODFp1bwwH7f1PMC+vL4mknQDOLDYJrCgepAS8GJWmUf7cvCXbKYyDpqhIwOu+jZPAH/UWua6IVrx
UkWZG3NDZxhr8MdkX9XAk6Wor55elzyhA1dPkyyjmWW3FmvWa18RefH4qWmouxZikKwtN73ZPhsm
3TVayCNOASn3HgFgXNg2wYEirzPtUd8TPu/b9e4sTz6jJHOcLz5Myg+0AdI6lupkCl9FfAxPBTMG
JKuDgXlPco6awivbtQkjQIsE4NuFf/u6FP2vQPChh4+L+yJF9DdHGCzAVGIvxSXESE4ORMEevyoH
djjnN70r25QGD+DNhF3cXFWi3VT4+AGSFLGBlwzbsuVtv30kGzSurYN8Rr1WfCfbEnaPIKnOL37C
lZscu39QTGTelc4BK9CvuT9nLalETtfDjs6T8hzd8OUaeesIRY6PEJI6lSeZRjiVsfGJkrmwOG85
9PRO3G603x1tEF+1T59xc8oyJGgFEYumJdzHJ0V6VEh8csyU3cpqMycZhqRzQ8Bz0N+mh8JX/0GJ
utbAavANAZNykv8/Z/l4JJeiTEz8FDYsYYoe6cXX8xnv+wOUGIOVD3n6UN0z3pjsyNlR2BGAH7z4
hXidI9Le0jk9WhLQilDMPkWk0zj+Uq/HAfi+rp7HHhc76IFMSwDCuTYeDL/63aCe1//j+MYCKzKj
u8ZM7HFf7NhbFBfH0o3+6jegs0OZFVrMgFvGVR/ObceocIhwNlW0sqyonCn0wsyYcReIEGKujbMK
jHDFMAsFlAatCjAxqoY5qzrhSxKXJKEYiDPWdAjcNPOs3p60IaAMC8qSzocObCltRYoLUwZsZujf
x2/68e2zkXVXcKPLDzJnLpul/ElWQ40I+pHu+l9GbJTCGYjpUo0R4VoLXpgVD3tsBhd9G3ATvile
K3vCS+ejor4gKv+5cSlLYc7M5D/oZevaYrMNI3Bx/3LbjB35c5E2BltFYyLjunrNn4kiUEI+31MH
J5KBoEQcpQIC9If4qwXy+XkBQdDMKRjhs6fwi1PGu/R3vpWA+HoQuA5ZOf2rTMcRbNHKhIvyHCZs
glHGOPfqG4OOx2lnVSLHuZ7y3g1bCezi042Fwy7+CVmuNY6Jm3F2DlNPAMMp/osOLxLgzl13DCqO
lYc4attVs6cozq7FAkKyA4+Y1O/skAwz1/NfQN8hLPuHEB3AO3NhzvEpm4OWycZnyOSCFlCcSyJP
0I8zbkPOQOY0VJH25AmXOCHz5ONW6hikqxGOo7WOM5mEIkHngYpVRSFxKp03H1BB3cCDzn9c28eT
dZsD1wcJpMC+7fYY59b8nYOc7YvoqCRq76hRwiBkpSCDGIbpGkztf/wCTZGZOoC94fJn1WTHTT7T
fdShIxi/TNQiO8tlgG8roKDMB2qFV8+QDu9UEUKlpZmGvo7UoZH60Xx0zuLo5h1oEFJ3Y6WO5455
UKdhKpNRGMpYBq9wJfo6BnSHUkrOtHmh8zMuC75iD/sIZSmzSNwUR8v02BRqpN5TZ+V3FLXoxDMA
+pQPcICA1sN0F86pZSWTgBHn8xLcarU3r7WusfsaEpCEaLswVe6WL/h6M3+TnBNnAXKrRvxAulWR
OtM/ZO3NrepJFUdFt+E5/RhX2uoLb/LzBQB1ydFbssNO0maCcl8WBUMswswyMueBWYmxcpnen4Sy
+Vreu7T0KcqrFJlFeBmg4C2Ga3YbQAd4x+QjkTjJDS2F6+k0UOtczgxauocd/vVb72spPQciqA92
5mxM/sKFhtODrVADzk2PUcWSkR86npTzvvrmMvSczH80iVjZw5vz/btpHSW5TmkYYHWuMjcwob3r
+QdHdxmgrVCvwWwGt0nKATmmIIPxW6ToMyQ3M8lAAofEExghhvo0oqq5p60QH8ILe+tYT4FPU/Fg
CJo2mZyY1jXxpi2a+DYArp/CNkmKeR2BpGkFGUB0Hg0G1sdZEe9xSN3cfQ+tb1ySNdcChW6GSpmQ
MMixLnxxTflvteha3mjzTW1dNkHgkHTXESEpr9rvGUNfM2wH0n8VDZWMvrqZZgOUmxWjbc9+R7T2
MtRdL+lWBerPgTWjAYidty1on7ZcZEjarv33JJAzO4akwcxHp48SGiDg3SmERsdpPWlvhm9KD/vj
PmGA3qPQaH543NhmqunfDvAtpeNQWwFbT/UmYMOMDUBPVoCUCyVuxUtZHzm9Ob8NcjvLX0vu3Z2Z
8lH2Lee/oBGVuZLXpcDFFDMw/A7FdsKsUUh/tnYFY0WNb0PnnvGsJ3ytJcX50wasAG9JWnYGMLQm
pxwN046QTJI+Ii86QaNVLmLKv+QLXFRTl6QxxdIX4LLqF/VoazJG/jK6obtB7l3683u9bZQHcYMZ
E6S2r7ntdmFLezelDs0GKeTgWQE+Xke+TdS26I0CU8RJdkKijawaFdRNJFoYTThLRld+GxwqKcO8
HSw+cuVxdZ1CKp2jINc8wHXe9Qhu2OJ1Jg7PWE6YzgDMXANoYpNXQg/H6fixXmX710AmVxg3Bbid
MiqVi5sE4KADAZHtQaG6+61fIQgfB2iDAVNFjf5snmUmqs7xTuy2mfv0+d0DVKCldiXxOwSkp/G0
hjl7Rec2flzBvIIyQNyGBnuc6yVWdXY5GH4Iu8W1Dl+29PuyyMXJftaKAZ917J/XsmDTas2Ack+w
hwx7RsxS2YKgi1nE2LuCJCINllAKXv1GAdZCvIwvTWprYAWrLopNkluK+UZNXeQondQF8W0KgB3u
ZrN1nvjif1l1TF9leKMvjiBMpU5Z49QGirx4qkaX0Fz11pfKDHkyIHJQNJ7T1ZWuRO0lGRmZC5QP
QSTDTQnFvjvmlaJpy3r80Bw3+NpJeXERnsbLSD4mZovIhTS3xor9V+bfkT9kYh3IlNcyHcmMxyZ9
sDObK1+kCUksR5itkWASU2NmYHoQD5u75C439OMlDYLCRiPvv29/4moHafSkku5zLAkQNkEVO0yu
26NMO4ztg1OP2S5fnpUS96NyhF0WgwxySqzMZCurAb8Keyxwj4/kwLjIv5LqbdDsAaeD0TxxRcmC
5p7B6kxnp0qh/WYJZo1Y0EoEoJescizVqriMSrbJTBUFX3JMcuCG4yV2i7M6Cjxub+0k+rZ393xr
OHkCQ1fUJCv3klq/TJDV9fCuwhPsosNH1UDgRSoeKr9bm0gmWZfxyFxQB42pVhueepsdbuFo1ebj
+cQVZkoVRWhp4FEjeFTZbMp/zu5RcCIznlHsSqtdE+vGPNwNFn2GM3ybnVD76lk+lNS7wqQyr0Tu
+GdZxUZM1yjpyJsJRvsNLuKvTSfct0KoqOrP3d0UggAuzgtxuOc2/6pRn+FAAzVHG7qDkoYibmXJ
FUIGr0D2xBJG9bGVTcQYtvw1RpavkGwV8eladZ36aF7rs80/m2KZOgY9ExztkVb5wc5bhnw4hvuH
JPI1oErabJQHQ1XGghIItj+90ZdBGbAWYjgdHsyRpjBnPIlAe74VCvi2bQ9+kkqM7siQNwnkcDx2
pbvRk/yl8TWB94MqdkVj+WqVxfnH1iMRwAiISajZFSSkLlUqMaMQ+OPYeE5lnAt/awg92FpTrUPq
fu3qt6g4QQR0hvC2i5x/GSszWWtQI0o+dHiVoZd6PjX2Xa8F/y4OyW04HXp7cjrY9oLSJSpIEWAu
38Nuc1FphKqwlChRfkibQJ+wh83WY838ySaNaVjd6a9W8cb59ono/GPrOVmJ/nm6Ekimodo1wZK5
aepQYCeqHKa7RU4TIihQvGCZ93uPrD/nmlz3YEVJu7WEIKQOVUY57+KjEWuE8UvUxaQkWc4z/W2a
ibz5wga9IwSYW9PCXjJfTAC5APq1TFZYlIi3rvQNPlVeG47k/oGZP6g9K/pnbB0IaUNdB9Is+dK2
uX5Sx5nUCwT7iNoFbegc18lCx6uYhA4qINGJffAPFgTY/WmcPtsZ5iLAss7IMuwVhRda1NpU/sij
z/UdkPCif6vmOGf9CwC7pzgg9z3mpwybJR00Lpj4bMyIiTbFyhkMcUnH7w8MBchsjgYlFobhCtaV
AQZI4FslI+KO4L3XOldcflq4GiTxro1se/ExAXJIwrK6dIuhLaT0BQcNdkfrf+WNqoyTGK9ju8iu
UsjrwayAg+v7GCDaExh1nhO9/9wlovRVDJZVZqj9vdfozoJMhgJ/EQ+CjYqinx401BF0YPAmMpbC
tAMJ5KKJyQTz4Mm/5pXRYZcslSWob0VALRGDwbrw8ZbaeIMj4rOwcZEz073esHiaDIKkW2znx4kh
DViIcwur4qjsuS5/ddBIBcfW2qkAguCpsZWjQEPZbXaL0xdV+dRGEaA8oK5hUkgXygxMr6auE6Fi
GxbvHk8UZbnyYlxA5GKzzWBJ9rvN9TlH9PvGdDRXcBrKYZ+iRBoT4EOm9/0IpgPHGxIMtz8m2fBb
mvbf81KjLlGNqN3G3uMbM/iLK4/Omy/4V1wpIH3vbFirsSkiL8YUqT9crvGll2KzPY08+Yqqwpjm
zCBk5FcqzagWsX7G29XPeH/jaIML2/sVOeWuEgfPQ2pu608pzahf0pFv2Ud0plJZuPlVpkD1kHE3
CwdYub3GPXBg5DkLcMgLdM90AgLEwfzvKUOlOLBlMAe+vWuwgtuPO/3yKcxsz3piJ/K7UWYBMPXp
M0ueS1bCBR6k3yJ6ha0KbudznzBRZ4Kc1O2+Y+MwQ2IndqE8w9sIbdmS2LYxJsqkNzuQvLJpKktm
YmCkLwgoh6suSOfkjlEjZ2F6LgLHGPEgRc/L9SiuvlLDMGnEEdyK1IrGbheWO3EETkLacko+7iOU
HR4vqGvzsGDII6wULHK2HWr9SqaIohRJrwNAK1VKo3/cOShv3+3MIXPtMr3tStrZKZ2V1MRyPq/o
z3yQNjEohim8FoOaMpY5hswP16fkO3hsOd43uor8tt/BnbBKvR/aGhQjRsQTHTcQlBlmph1oUOD2
fcT3DabbCisBQBReasfxqesgymSrBdS//s84vaVYp/dA6PZXI4zHHyIyAiaNYf5dlskxCYsFbdkB
HzAgXzQIp70HD9cCOebNoeRjr+AEjnwvRmIlmh4kUZexSGGUh2rvSXtSZ0YzqdwOMdizvhl7DGQ1
uRe4P1IrMu00aE0QTaV52612/psk6gaL5V85xbz9qfiGMbQfhF1uBzINhK7Ru4lY+D/jTtrHf1gw
3hn6qpFvU/zyM9GBmS0K3mnI4pASh4Cf51Hktp7a5gSYyXyuQFthcqYFf5YCpYbOXYs1iAfVcKgR
dbcQugtmUBVzNmoOWMgKaTg0Wexu2XchFb1pc3X+PyL5h9NpLj9bhw4RlYzwLV3wODZviHzEbYhM
cN/vXxRxaizdHDOHbiT5Ok7mKFWrjBUFqJdJzvaRiVYylxaXyZtMuLhSdTav2YGYshMRmolhAGKC
F9r8vujL7fNWVwRvjHrh+usjyQ1pvZ9TF8zCdxbVR0vdV/C6Pfdos4CcYFR0HtKlLDbdCvWbDAce
I+aZod+euKctHL4/OuWSfjcABn7DiRUnQAjjiQ7fKbDaHQjcE9Vgf6Ala2AC3/DAOKGm+JGDTP2R
UZ6h/kilwg6Rytd1RW/a6CiPmVX/lYMjzvJwdaLSKnxCaH0F8aY02YjgaF5SWvnTLfL58E7LBy7I
dGIZT/uaWb+PAMg9NQ7dGKhfba+GrSZX6/M8977yYczkxsxrzprTt6BH5/fOUJLylWfyFgeLnxax
LsQW4KcIAdHrx1QFKBloMb58t8kgR9EVaJuANooRnOfy9LIFPs219S5Px1GIFjDEKDaXJo+W/7MS
B6qkGD0EMMUOToiELWcIu6XuTsmXSmIqN0Flwlaflt55CR5mRGNDoeFVLTBD8p17FaweVR08tb3x
MOhA5oQaaCGjkue5R/wAAH3ZoN+5ZzV9pc9x0oWP6f0gSVZkAJQCzVUcIRU5XZSw1keX60oOxMKh
nfPPDi8lmt1cV9Azf8cRbY0eK4J9LuFhGt3BQkoXdZgPUPIEGp2+aHqgsuqpsnjMxfB1WI6DhGxW
lF9JtSmRw5r0NpZTksp7QyL4ZG5Tzcs3POEWOWfhS5n6U+xR1HZ0EGolKQWY4c8lqdFTAeIOkvKS
ztVm1nvNCaYc8Nb/0+b0F/brXUArjoOZXnj0VdFQHf4spYkD+pr3HOpZblTR7La8Fso8etgBHece
X4Zw/jDryqtgmPbspgsPwf4Lobnh6LngItKMUGPHNI/uQEVP6ERwNPXr/OClltxnHFN1XAJuNa9k
mMdGitfD9yMDsU0B6PzwR3ILqj1icZoSl31OsuzjhnXEZzFKNYj6FKL+YlhKXVi6lFOsbsPthl9p
IqN2nzg2nQQ7sA0IRNtesrXXrWbBbKNvNXfm7juJTDJjBm+0WG/XGR3U4Y2fLZCCvg6mVmH2lsUX
XwgwW0tc/n8w19Wct4zUl/4jwbaymsRsDmIZKv1uXmqF9Lcr9N/+vNQi/BiPEm2uVaMo13jDNEJD
Nu4zz/Eqd+J8prDAgJcuHKJofne1Zvx0XcTyXBrAof2cP2iIOTLZ0RUdJ0zFe5o5hZQKW0DAj2yH
Svgq7xlbufnn9xXZ2MP7gi3GhN5a905laXiEdtrM2aurIjM4YL8m8smuXHs+JLO4tTeheeqXE4g0
+ftZT1nZtwChnYdDT9XgRukBSJlTlixXsscr9P44yNd/MdyiWU+EWcvYg7G5TCkl8RrSSJGWVWze
lmOfIvuhs084X7cW+GKrYX4JYN+wF9RtJrQtoT2nxCu8C/sBwZPnY5h/rMoe1jK4cEm9BM3dfX7j
hQG5INklh+SeiwZCovyRCSZAnSFJeL9C6LskSlSGR08eOAOVaQuwn5d8ADr3VbxrSQ1WN4PokIcm
kJxM3DIL9k/F5Ka87APf10ay18kHPuTb8zWvCvDzuLLUOG33l7sR26Wau3/DnmC55BpKJia7VqhK
JHvE8m3nWmnxI3VX9hP7UuyOklDN+4P1eWOehcCCMZXU/EXmSI7Vj9t35ot1Y3XHNlGRQPbFD4Ka
mtnBJynwX28HwGvkWwULquM/Txu6mZU+EI3O42u5yJ9DB5wXNOJ3L54c2ebB21KjyzPZFQ3N12Wv
u5ycskhSnyP1G7XUNcV9R33m57HkyfCPiZB70pUfsySgGO6TzQPv11P+OPNjGa/FBCtlA8igiV12
n6cCCMmigl2eH13r2UQLengq4612FlBabroigrej+DfRH+EF2pj1d8+Ztmgs04kOYEQ3nZ/zQOAL
u3ZnwFbINiviyaWjDVWCxMK0aqHPaAys/xSNs3iN+/lpAWzVBgCpnplZyj3h4JZx2EYEc0+8PWbp
8wh6sOqEU152J4EncIC45BgEiiWHfaZxwrPYf8n/U/nFI5OHsoAlmgCL6jn2mfLw1QZQ/gXkUSwn
P0fgBgiM9EjsJ8mYMlhrvvA4NcwMNwFwaavnwUlEjh+A/fe6bxblXudAfKqHpuCE2/WmqmAurhnf
4H8yP3UK0n3y6x4bS3Vfw7PkGl11LJJsozdbrEMOqbcNDCMOChwER1Lu8koaAB6le2nFShO0O2+S
s4mkosLANdbvHu4I9+bg97KhJYtew53Yge89yyTM2cOy1RA8sBZnx/IPXZBtnPqt3NExTRHelA1W
i9+Ayh+Q1AjxTRQVzp5o85tA75l/eNAqoPdDfaj5x3+0oAg11xDL63zcUxp9kiVUxJev9upKXNst
f/NzY9LTMqsGwluSaOAG7K9Veyy1bH6S6pnIs2pOD2pSOGWqgOU9BFzNAa0g7WKNhp5SESkYRqI8
tzf1uTWXREYbYUEXu5XEBKjSpKq0AnzyB9kcOzFKNqw9KqppqKVjG9CyUbCGJGDELDwo8FYC/4UP
W9zbE+OTPts51zRn6BisxMfdLW9hY3iHRvTNT2Lfneigr2jde61SEbMTu8670VKaQLNcg0G2a3Ot
FgZ5fKf9ogczo0Cl/C3LxeziqzzdhI/Yo0DxRSmsFx8MLZHv0d4U/xx8zndJ8nNymQNLPsM+S2qf
4fCXpJxbnv6C5XMRxSNSb3wLFy/w5uGTnr2lem1XrgJ7XB7Oxa6YmTwA/3RlsgJaU0nlw7vLZTOy
iNvkqcOoa36z+BQ355bSb+JrSnK5cug1Zxqib93zAkSrtlZnv2zPzZ8phy4wwi3ripHMhLvd9XpO
ed7HrvOh50lyjCE2B2CV5l4ULyr0R6fif0UlvHpE2dQ5Y+4T0KQYYLCx1Yk1bgk3AjKWBx7Cv77a
mvhMTeXv7Qh48AJQ2Td9Bi0TrQ4MSJZPJ0Vjp989tqr7n9bVEfPUvWjKIg8p2oTJp8P8jcc6DRXY
qSuv8QituhcuD8b29Ukwq02SApuvM3wT4r8YQuMIwSt2hk/PQQO1+FhjkENe/7GvatQW5W3vHQjR
10JVz/Y1PUflcGO6LO//mDVxIdfr79+fcVI+DlVruyGPathRNQXdv2ZHuTOMQNHPV864kjVKtnJK
7IXyEq6Wejt1d8dq9x+lvkPRwqXeG85jGnj+TeD70WidqyrzOmKUw4qnmeN5y9vcUM5nrWNzz5Xt
ZoEP8fv1isORtuQOkFhmMANhSFcWJsqmsYAaVYUT2kjOAWIKUcJ+exWIQXXn95fokFjVzuSrgWSg
Q/fVrV+hno/2yBW8VuqPY34zRbBq0YL/hGxlAs4P7goSrYIQeTGyyMWRCz76nJmhgvhfpgfWZsRJ
qh5lZwRdafdgLgd0OOloDWfm2yyO/e5X+ZjX+ZZK1UOuFFhqIPLTBdtseg3RxyJ5rueUzZxi6Ogz
qoSzXFI3ela4DmiR0Wn8HUXAF2PDZv9q8R41XXUrDS4B12H5EOah/seJx5gjOkKS/Ssq1foae/V7
UbBROZRUvQglrBnNTJMjPtcIpf832RkbnMR0aErgrWcMPyGNrG1AIvc+6SlfjluIBI14R7fEI56j
dqTY6VrcDBzMpQkHA9gWlm26VL8XEFpXRMIO8xDB7gHea5XuxFsB0C0gnUQ9xZb4JSlXayHcm8br
NCuGu4JsKUuddw12MeJmQJmpLoSKrOyei+At/satm9h7pg8KYxXRsJWDbczj9by2gZYExaC9vmSe
snTiHUIO+sCJO48zgRb0GR2iPARFe+97FGAAA1B7MMIp5r52ZuoUUb9cGS7c8lfNZUqUeKQfR9XF
QeyjkY2BjrGqQt53btJj3DkySII2UzKtMkF0O56fipHbH3ojQu0EyOQeFKaW3goRse1xNIAk4Rb2
uyDim2kQJbFqet4cjUWoW5t/tmvZ191pjMiWioycWDuKulfLIh/oPgbfcImZfB5ozmPC6BQT4kxW
EzRxcAAWKtha8uT0Hsxm1liyzDFWnCGm1KJEIWy5tSCqFpuzew/NWrRbkQzh/goKKSn9ebkMMH/p
ndEBkA3apQMv6N0uS4iTCh6xtdLA5e6Ae8677PhDXD8QSF8x9rkMv5rbo5F2tzm++ki9xor6ovKG
FDqXZR6XLgDRKGlBlBeZ3PKdrS6YNpv0ibY38nq2f+EdwpxSd4Cuk5Il9yol5X4bCihZXZnA1HwC
6eO0YohYCZKrdHAVJPOYIwygn/KeOwSidkEGr9XnndXuCMrY99O4xdqfi+QaDpXJsMR/mhqy72VI
RYZUfOhwXt84xtaKQLL6TMXeaMFbt3TpLqHPeF22fXyYgFdQojdW760ZTrGqHrrqUpohG5DG7M32
jlkvbzk32f1JR1sBpXmzX0WH6NcINcWVrGivghJjF+8pz4BPI3rCZIGy4MUNwE5OlHyrbKPy+qwM
OQTq6vPaUDNOhyID/bo39T0XNFEzRluqzeWRQGiTBMhHyq/BD8FTTihQjhPlC7/I33py5FySTyw2
wZI6RMT6IZdUeNKx7kVApSHe4JrGXgQMlqw1eQEs1eNv48JDmSSvwR29hXfASRSGchhkK8rcsr5L
J73HMY1mMC+yymb6o0G8TfX/avoWEpzdX2pyiXs59Mk0RmuXrxjr4ScartiAOSH9+3cJL2yOlbAO
3jphmNevLFZuzA6u1NBHBxMYoZkOWmyAiWlitTWspzp/2Bhq80OAO9fvne3zozhC3AtN1mNAT/vj
MP7naFRiap0u2OhkU3bruOCNJz/oBcnWd8PKkJE5ZFhUW/rM8NvyYhpHQRLFV/fjwnNJpY9U4daj
iani43wo1cL8G/WeTV0uOKMtmnxgwzDLo9LRaIwepxznDRuyAjf3xlEZhNeGl+oX953hRwZAErUY
YSbLcoEl8yQQ3a7kDqv1Tl/L3vMV7ZHVx00zzljA6nbgaA8xON53WkMzkb7HG0opirWQ6OtzBu1T
6Z4uoOAtM3Tw14J1dm2ouUCjn2NYPQE1XIvKmfLuyn2EazkzGEOI96tth6Vtzs6QGgLSkVP7CzzQ
pBpWzypIcJSAcrX72AxoWBHi7tJ8mQi+Mkdx83CzXtnnUHrFym1y8S4sARabC2fPw6PAG+eACOev
SzwHJxGf6aXiSBzo7LewZ6PbX7m6gl/N5yQgOdzvixlroj+2+dyh42ecxBCYKUV7SgvlqrulHwpw
HQ92mcvGR/mknNLPjZxZ370k25TlfNamgpvNNO65fDgwXg+5vTfpN/gWi87xtQ4383oDmKigOSmQ
SLaLNIYjlnYgkyWnflWKONFz9osOw9LaZaQ4Y58mZT7EAwTfGpHfrZ9P8JMwkosUz0oOahf0b2zl
osVw5yP7GpXYcuXF0FqXblzvRNpy4IRcY2D7ZaqifebX233QJJy5zcjt+pB0skgzliKqy5D6KleJ
BJN3dyqOcGrzSvpvnmJgdRFzmUbl/czCiJHkC4Dy7H1zJuRcbqwUzLToScGnB7Z76njqeqjUahab
0n4Mz0h5e28t+wYDkJVN7EiT9TizMpQI1XHdMqZWeQMrlxdA0Hc9LVlZQ+wd4jHHVjMhBjr9JfvU
CL0HMEBZBaHiHYUQ3OYflsw1cdVRAGHTDIrQMkl6xXJctCA/u2OYeJJNkpH0VEy6A9Nwe+DaLWGV
4P8LT1wfjTNGXr4hhAd6vpkg63CG6QlO9RX5+OUoIqPi76sPLsQ4usjKmLWFLe0fNdLQr38mhWke
QQCK4XyckuEYamfz037VREW32dGjY7Eb1AG9DDfuszGOy2ni0I6un0du4AWpHLgrzJ8k4TfyTRKD
iiXW2qBvUth8TdVJVmICPnc2XOggwLZJimKW0sOF8M5sxbB7yND2qIcGySjgFoZi2LGb2Dd1JWtp
i2G9M55IMP6HOr8Y0p6Rx5MEo0o/8FB93PmnGGcCgJddrbMNCTJ1YRXlGnR18Aty19SG5HSF6HG/
V+q/Y08YZldTEF3RbAeLrygX8Lt5Igdy8VS4D+7+NFmf2x/O2lZs4DsMB1EgyQCbORc/hzPbH8jk
pbT7EXFbDIomDE6yrIQyPhpJyC87PjOBKq1zXpAYgIrVV5VnFUfqRIvCzWFvQpyIA6bO1z1Rdpzf
sGiOrmVAYPVSoMfNNDpBL6mcMr600uxyOdAbYo4sNfoR3lCHi8HNpaZRlNVqY6KIRhokTdqLFH7r
fj7HTghXa/h4USwP9PJgrdajCTdZw6men6YJcr8ArXNj8xrhpMgel2f0dlMdsvhQ+cDaKMt7WHNM
G7y8D8C0oACW4ZIS9PBnweJRZxxrhdR+MWnrDj7julGKvvFuM83724aqJ8DM1NJbaay9Pabyi1A8
Dwlvo2VsyTuNi6I9kZC8wmaBF4lTf62K8A8azAF9v5HvxnkAMjNxC05OprAPTWKfNp81v8vAR1bq
KhpFReqWQv+b6gHr3ZRgwsr7wpUCIj6U3YXuOAQu78SqK+Gm311hi02pSZvp2bVpCV5gyZkfRJ2/
hpEHrlx6HnFhsWr7jfvcDenALfgDLnP5z4oMKPQef9K8aBoROOQIYqrY4l9vObp7nLAE3RWiHmtU
pMUso1z89NtbW+y6aOcf/FP82Jd6OJvuw55ouSQUcs/zVecsD6PFRH8XP28xaZALlSwj6Tm+39zF
X7kqnfiUeXyJouZGMSvtwcSinQ+6wKWQ8oXqS5j7hCdU0OcDByM7CcyBqvZFFh8CYr4g7U4zRWZ8
1DGKH5b6CyGZ+CjkPYX2SuqeDX14k6WIdUpGehgsIulgfnOvVmljZaNfBmR+3vsf7HBv2kllV0bW
Yr+YFVBwTOkfswJx7CKo0q0PjCD4fT9a+OI2ELurIhj5/UrDVYKX241yX/mqKonr3Q88RivSzGt9
vePD2YhjJBVCwE7QxHMTBTbXSKKyq8IBB3VhoJk63uFHANzwFE7CTmE0qATynBlZd/QfwRFY5CAN
ib9rI2XT5805a+ZG2ebAXaT9ycLmntY1ujAT/FOODU41MGnDbK+JeUItuuUXKeYZfrnZv3lkpW6M
FfnYa7ogRDkfgIZrCoFA/nS11HygmL0JxtH7zOUvCkyNQDgqN8SgeNJiUFod/NovM6kc7TyZ1xju
mR/tbEQpN/flulwP25/xeQvFd8HQB+5njAKTTgDPE8iyG0kEv2wocfoj4oU9QS6owMcIxKBIKQ8t
EevEzddWSxaK59pSlIg7WI9LblqBnZwwlDGkf8MRiPRZnt3werpjCJtXDG+tB5mM57iyBtV+uoG+
x6HX6pg+j0NT1uYLvAgj/do3L3ppy9HwTFeFDRqFZrNHa+BqMiq43Fxofp/HSo9+uEoEnEqMaY0z
YglhurmA08JPJBbDexYxdIyX7nKGOCh1aPP6ikZcCxlyF+3jjRwa8ZP+O5jOruXSO3NFtycuRlM/
SWFVOHQS7ZLMwgxaC1btabh+9lQnDxosLGacGaCN9mrBfpYUlGZqjF9gnO/nQr3Yf4ZR57t/7NE+
B6mEc4r8RtZJkB5msFHR1RyOcd+KwKiqkIUtmDtMRLOvPLJCoEQ2Y5XtZo585DhU9kJZ5rEeEFyn
yI/MiCJpI8/7ovmYiooEaCOd5XAsPPulYEY71zaHEoW0guGuTx6z2hs/rH1fRODQbi9etutz6dnK
zdeMMVYwiCfMtWg7J+hUKs8XA37zaj7jvxv8vSDOvw4G7Gczmc6DhPo9foUyZHOtbE7PXFQj8ytz
xp4sZaxE42WQfeeZHQ0K+yKRihlOmW44dBOlhF7I6CjFh7IOOlKMCPMCibo9xFroYlg6rRjrPn43
Ec0SXE01aQiDYGLsboS2PEDKcTlHviR1Zx8AEiMc8Chga5htUqk5zggGudaz7vePrwi5lslE+T0I
72fJJ92MwXWXMJuqPsdItIN7BQuNoexv5hJq5dzxb61jrPjf2XzTW92ovOR0KFw7bq1657xccqSQ
ZZElNevd3XB5Ah+PMTjmz+AzFPxwq8ajscZaiB3cbDXcq1o8enfLmIRdoXcJ/kBKFVa0RfniSydv
82aGM6UQq2XOjRK687yZ7qSKwBjYqVDWrRhlSLGYiJuS6nylNtagkemhofvWIH+qySJIEyhGgwy3
qgM609iljhL+ijq7vaF4Dq+rSs1oWBbnV/VeO7lMpXeBp4AqKxWUhFJiB+eKeDu9WHuG7TdpohbW
/0EOT7dOjZ+/Ul0gnF8MmMHh1A/5/XQkc2GJv3Y1477ZRG+bZiUbbM8zLatQuTr1vDEc5mhwnGOT
wx7Wl3NtChIl1FZdsyUu4sbSxtgnPW9nih3ESQSlWlYuTjnsokRzD0VxZF8BALure5Mi9mNcF1Ai
bX1fpHHLkMOVL5kNxZNj4OpfB+BqYt+oz14NcVwkC0zBwIfTcvkMkoXL18VM0mLmeuDTmc25/pMM
aY/p2roEeiXKdyk4peCzctsdSsPlByYfLP9o89un8fZH83fcHAaUf5voAqGfwiL/vOrthLhHOc5s
xGSwnU2+3DN4t4CTLaKo4QqjJy23YBMXFofnRxF9UXOuz5oH+O2qFuV+gmLXEx4EtYPgKzx5/1YV
GBC9QSiIYooHRmHYsCMY7+aSp9kxv1y+CVshbzPA6xpuR/aMkfkou81D63AyefsBvHXcWBPmZ8sZ
/kaV6KobXKQ0AVyFJj/SGpftxBifkMFIph9BcOgmu5/lcbT6NemAjKEsZLnO3EIJpv0O+rugrAMy
5AQhp9T/TevmYkME7KRmt/31+XTW5+9mt6D2lTp6BoTL6Ekk/DYMtJq7pN15NGMYSXy7eFwStUsf
4u8nvJwB5/7b3ThedxaJZ0QigRP6nmpv1ZdIfIeJABX6sppcxd1JvwnzpYRVWY2dVYUFbc3LhZ0k
DLQp3wXqaRBP8bMEuNx1MYcsjZKtZruheHqnPBIDhBAkYYf57w2nNuR6rjnt5/DRG1PAiDWSwC2Q
0m9bNrE0GcSo3Iajv0pK0Ztf52iqrTngK3U4dmvXDt/KisGXEEmWDdL+vNVNr8G3PtXs97swU47S
0P3ZVDjsT+FqZD8BvpHP1XTVaoYNBc9b/tVGo7ExfV7r3zi0vImbIb2l2Cnj4sQTIZS2gFYmHSHZ
Se4T5cKBS6XpuE0he67P+2KbBlR2AsXG9ZVelBVYTQwCbXs817NHtQLcWjxfB+/oX7/77Oq6IN5w
IwsJt/rjfm5jIEmdYj5yog8ZCi/PZdT8+zCJra+SUd+A87/r8Y5XiPcK2yN8QCYJr2YWtwL4nSwE
hCC1Uo+FnSIL/ESiNjoSB92G7PekzvDv5OHiSPSrDq64jw6yTEvrVe/gnCUE/6E4xXLEg4YGhvd3
3s3a+AQ8rYWjvIJZZxZkkcDtDC3e1bsG35gDhs9ZMkJ7c6hSll/bnkWTN6asRr4HwRhx5v5XWfyq
trl1NnrEmgntmL6e91TZZ9zXfi/uAeJnmBWgimVi9H9znBzD5WCQb339hdhfeoYJdBf++drFkioh
K7PqUSHc7oVEP3Ra3QYnUFzU+b+em4+PdmIKveV5V/HMcYZIhIZyyxXq3sg5Tl3SHZGFHOoGgJsK
hbj/5QdMdTi/1K5HxVN+J+CzcbMB14TA27sntcj9IkainqgYqGQGK7pVPRuQJ7RB1j2TthxlFHz1
JxHoxcHzm6TNsBIP7ZeKlAQM3d6eyC6zPnTcmBO/PeymffGjvwdXJIkk764KNsD2Vm5pWtowpL9K
jLnqeXyESVoSQ1FSU9KiX3TMu1GaUEUjoasggRXGqMyVEHCSUkgt+339FEDeM0+LVPp2uupMabBY
PAU2J7m3NPGW2LaMel/3iwFnwaX3FjWeEMbXJmFLcIgL2gvjrzWGO3cvMmDX98s4ltviAsVzG7Rj
tJPq0oLs5QuG8AI1aMucTM2gvyXrliCvNcxGzBaieocxR2k6zEGE8ojBLcx8RYmHszhoqgq1O7lv
X1RrxKs5n2MsMzA6ZpAvWd1zmJWFrWkALtqiDSLZlYMGtI5Nri51bpzP3vvofvdFJ2YRgw+uhvmD
Uf2814lBedMqEcG7VZnVny5wpDN75UKaul3c/Q9aGrFm08nhTD2O1gkp/4uTmHur901oKunHi5nW
gWxtQ2+2ManBTneOyw9iF+1vhmayQ512SeJmzoHxD2WoNzNWrA317W+qGI+RslC9lP0ZFjKnvbic
TfnyVqsKgG/rvcjPV0hdKkIl5GHCJCl5B2uaJBpwE9kuBUW+7D05x2LlS66jwyvVgnYGCsLptCDo
jcRIlP6f4F2ogbW3nC+FWoFhOhbJBnWwoYe20U7Bq0HAnFhP/9aWgM5qcfpkwepxRlinZrPcXOPO
QoQicjV8eobYZ/5QXYOreJLgNvR8+NKegy6PAgVDbQVMPubFo50d6ptp3EAxhem6P1Cw9gAfr4+C
IBs65+xm8v64m09pRW2697p4N5LXWgCi7bUxfmuSOb0+9Kyk8o0Gw4sJ8xxKkU8T1AfUUP25oHhE
6xvkos48ohReOLUFVNP0hKOlPEfh/Gojgc45swwp55/Dz6x2LRtvRasv+C+/XxDkbssP4M7LeQAs
SvduY4Qm+jjRp+47T4Twrl99idqGtSlBxwk6FKgL8t/5C4SQjv8cxGom7v8jPgGIB54WyMWRjDXu
oQIy7go8tpetoPy98n5+gbPKwJ32+zk/UKukcfObnAYjkvpH1/1a/oTjBXC5/XTrabsaJROKjTFh
DvLnE5REEGwQZbrfboOqG4YsHULXMk4esSgbovJs7KpV7mYbCmW+7DDvlMhkRSJStTWbum450kV0
Ldn2JZO9hz6gL0KT2TZf3v796MFktmjFz9B6QVA0BMf74muGB6gANhIp5pNLtacgZAwFIcBtn85H
mpUJgcVs5OQ6PcJLR3CfPLUJS69RfweVIFKth+ile7ortRZqRGi+6Aa5phRO690QMxMgtvEa1obb
t4FIme4UPnz90brdkQNqd8AQsLzVrL4dlP8+AWQhHo4rJ9jZoHRGbA9pvynUx5UL0zDYWonsU8cj
wD4JUXNY/nysh5lDnXtiaYMNs699Yu4eyIMyHrPMJeOmQqOj0NFpamAogKTzh51fQ+zNypKGhQ4Q
O4OjPyVi7AaXyieNHz2dSqWKWf7r9/tbyjuswPHZErBK1RDEXDcbFwmFxn3detXkJv3ZFCOKm84W
W+vIYJdLx3P6ezOSd6FUmG/5X828ZGOJyDp6f0Yrhi4TgGIxZTjCSJTEnaenoaWBONLfX5j9MqD2
Kci7d8vyxQjxU5UngjfWNPKKcyayWSV5OYNic/CnWRiq/t1nehzlGTrDJrQGdTWotR6F9qAz8Zop
Al98M3Cx/S9BooiHH5Tu0d5l+XkrPz/ob2NaFdEU2vfyzI8z4eHrurrC8E5Zf++BVWzVnOHYm8iW
jUvkf2Wh/LFI3ZpwkMeYcKcqbhnA4vTaWp0tlGxUGNJN0uw1EU8Iu6uAv2KdpyzUkppBXZbQ/VBh
bhmc7Y76E7g3gN1RJYeqaVWldDnVJjCmhn68F5lenzolXpgRakh/tsENkzDjreGu2nut9gDF13k4
uN9YZifGj3rOH1wNmHE8HnUXe8ch3UlyxKhgSIwFSVB0F9o4kVYSrZPcZLYEPK8FAzkw4eztSl/9
kXC00ZqS9zxOSAMd9W+j5Uc5TdVbfXc8DbVcxs0ZrNHH9GbmaEiURNSjh4isqYbsYJYcJXl2lgs6
jnj2omlpgu/alPvBNHme/qJ+3NaCheUBCngg7rxJSH69gCn6c+gy586sKFTyO9U7C3Gz25vl/Log
agzxMuj9As2uJ+2df/NQWpKdql2LxvsDMIl7Y3Ti9uL/715W8SwDNITtlUhMaysXVOGe1iaGWMxr
A3HTZcMMJIQyz2vOpti0cjL3GjolBcvEr2R/Zh5dHnL4mG3zgQ6XjVA7CP3dMf19/lCBnASDnWhS
PUdVm+Jc82tTGZ54p3yxjbI2MqaoRnmAwFCxSy21neYZk6lcE2yJPdg1GML1wrBZcXTLEieRcp6L
8jQNmcFtvgR+7vJ6MCRV0ohvQGb7lAmpmAdPriabX3USTrF5O3cU/nvQ27E/2z2MKv3FVqdgu9Ma
x3IfNGfh7U43+EL54cPaWm/BFBsV0PDExPW5i9gSjWUWDvCZy1NwFGjThU4Y5y0bs+pfnYcu7P92
qCkCd5zvi2pOUOXOGy6V5F5vQnBkIiMX14sEAHQ9whyWMrBrC++QMajc4CO6AnnUtI3B0H6KjLBE
h/JxzCikiszb/7VlimbLj/5BUfkCCWjjqVGGvQmUs9R28pmUiIto1nzrR6ycV9r3PLKh2bAxAhZl
X9aIYQU01mA4u45QgB5+l/oHs/Eo3pdiLUv/loz0Me8y8vLwCbzOMR3r6kQe0o6/6VbWHdsKRSuS
albChIfWRkPirr2FZFARoKiq47EfGHuMVxW91Yb9mFK8uPUpaWlxt6kX/2wSm82qf4Iltk/sK8nu
BoBSsaWRw7LGxGQCWK7bzlVAFAzSr0vVqBS86jDe520aVMgRNKHPTi+tre6TZfZutWL/XoyyZyNX
YK3BKpcrt6xf6AVrTeXgIxcWJmsPRgmJND2MUwEg7ju2rIm9zAojCTEL7szBEfB60T6i0WzRrJT3
xJ1td68GH0CAuTo8Mt0N2eltuq9PgLcloNYGSlIYSJvokeVKQc/NU34okeSZrtKvrX4Nc8+C6oK1
mUyujTVhVEaTimdjfPu7trLh+v7QDQfE2zup9VblkPHxApiRadp98nVYMBb4Kmztf6jWFA1QKakv
cjqnZIuEYwwrZ47aFZTmL9sZds5exjxpNLDVgJj/h1+Gy0W89bkf4vjBNbFjB4z4ncMABbG2I15w
zzBcDZdptn3xgCkeOANvY4hknb/EimkkRVHlnE8TABguj1Td6OGA8hb2xjXJWLe+/TXuC1EjBk9l
sXeza+mD4ak/UxBWlc0P22WYboByb6Cv61QrXRkpLAosIlITx9D7rqMogukANzpe6r2SQsDkeNnL
GsxQDmfHzgKdHiX0yi3lFqdGxqFW5AsXLOUOawJeuOt4kPdLsSmLAQbQacMVt9Dg68Zi1pJLXDxm
No2MVjYAhn0gptAImMHHL/c25q87xbA1mi4G8fhHeX6cz3NgT+8Q4i+5Eoar4y0VcUwDNxztiLQr
x+3FOcf4Va227K4mj6KBO4rt0s5h/i04bMygr/TqY6nughArB5zkaNFtMpAkpXw1WBcmCnP0J4fJ
nAHnTKiOYJRtLhd589nnO6SGsARegMWfmCDYgvVP6FFfbL+o8o1TtO7NRbcEyZe9cdlXh8KjAUWu
VQ3fcoujsZEZdAilMHeB4vG+sPv5g72eE+IG3NUmGGnN5mr1pJxFbbrALtUyQbdXmoKKdUjli3OA
st2NDWkBCEUMy1/d64Lysnu6LG9t355090o8mSyW13I3LivVnC3j1wwkm2Y/vXwO2482W6j7yZM8
0Nz+7lJpLLotdbhelHpoBkZ8E0t05mQgj6p12RTBx7dds1XYwKXgf9AoJqHV/zTUkwdNzxKQEYLa
wELpRpxp6iTFba0DYZxf5ikiorVPbc8GnaZO4w+UpvX3D3fSNpZofyQZ6Xgu46um8qYtEp/OLcW8
GpcAhN++cQlpSt16FU85l2gpKRoGRAOLtW5tbu5FDtlXdS9UivD/myu+UO4WYDPa1ZzHSqxUIkdV
3kr+FqfLU9dkHyDFBHuyuYZ7YQdPoczsKej5OIAwygd1vKtZiRvVEzQlzCHOthhIwhRPds7+wUDT
0k4Xi/dqG3Cn6/1acPzjjoK1NmIeCEXcOCMvTc5/YgmYCFyb/ceZBR7qnWa5b5ECn3b7oiAUg5Ee
Yd4hA4pQWKGibUvdJ6qUJi8kRFzmQe24ONjkSwTkt0RrdeNqeVMujOAtdF4R49DEA6arWpHordwD
qNjNgdQWCL0BdopTa7RMXuSG04JUlsb067hcMDf8flTcvWS6R1RbE+ZjrY2fne3BuCvZdd1bgPEw
gu0XC5BF84xCFWI6rzWZxzYI7okiwSm0Drd9gMnyoJxibvFwbQmJx1Y8yKLN7wmbQzs0U8rHA9N7
a4r/f/Znbn/wZ5pQtWrOdBN7Z05T0WWrwq49GGZ7Lj1ppTsIsgRvDpRiTmG/HJDsTKMEdSBuRcFw
Gy4kLKm/AqnGr7ZAveSsGczda3jAm2E2Yu7KGMUhyCz4Q3TZpow/0EGJunJICuHoMqckD6S7jBll
DNAA77OYmDh6cG9iTpklSttyds0DxDyW8rb4QXQDyxZ3KGjQa+S5HLYV34cI/pNyHeRRnSRTPgll
LZsTrKFGunVU57LB8Jk04HS90jk4rML9KDO0IH+KSY9IU3uNrvR1vU17lgULm39Cn9+scw/tFQct
xocAfHTn0RAbzECeKta59DRqe4kxtrUVt8z4JSRcv1uQOTJQAJiIhrIckqvqGRyhyu04dCaHLyP+
ZORNRSiQgQln46GO06lpkeHqicTXkWuV11FJDtjGOXlszjuSOdu6IkJplhjMPBlhXy6ln0IyBwtv
lpl0OhU4Rg0BCpeUktHnhAjzrQBpVluJBLrtenx5pgU/lUWACzsO8OjkXSzDCd+cRWFIzzoE9gVr
NB6OWBcF/4RC3jmspKlVQxjcDXmy137J75mdWaGvuPsM3qTLShERFW1YF8vT0X9Ubne+oXHOsfai
2ndXW0f9VUzCjxeoMn7s1l6XIO7jIEYxN2oUf6ieobqfIxSoll6U9Gi6RbOS3x+vVgmYTSCuDSPz
b7L8oyXunhlCSLS7gaZ9oVT6uNfNheOUmbaFBr2Jr5DIDf/EF2IrrTl2pW/02/qqTEgUxZdD8P0h
CegK+dXgPdZuD7grfst5Sx/8zrtAyNYgk19jkB9OvCOMpffzdx1m+tQEoraZTI/YsCeDRoiU5pU4
Odz4Av2F6sxEII/Pa++9SOvbWuuBaWthxyj7P0Xk/LiT70jaZfJyy+8v9gU1nVbJTC9Y09XTW9Ki
a9RpsrCP/UZRzLFVP1RA/x7xOKnK7sjq0e9UpepVomRG4UsXnZq8Iy24oQ0P5beYZhMSVxiCevu9
2m09yVtmo4yWc/WSecYKJrVoAH0lREpfwqHibGb/qD+mRZgwrlCTNXUzP0wEcXquuLxuniVvWKJO
eU2QQZMbBup3RbI/cDLM8BHm7qpxee8DNRVJvNTNNX/ub0S5dzTsp+vQwLFR19LX3/PVHaPKbRVB
9Vi+XBD6pLwGOeUJQLrmcznM6sqh2uSCgFLbs3cSZCUYLwi4RnEtxWPoCCtjnfKz+LsOy2THbrTL
TS1jCAYk8aD5vPoKIxjMT/kHOce8AT3JP3x7PfVeQpFigoDVR6ADJGVvw+by2PRAgR7q/zOJjOzX
KFSsneIw7bx2WteRmFvdZqZx0xqSgfDIK0cWEAchnJjUm470MiFHMGL5h3RVP9FNDYN5NyueaHQ8
9XvYyWIy6v7TvLIK9S2hl50F+TVA3i1+9Gsxjn8y+AeHnnGqEvJZ8djqzmJTvtDK5nK34wODG2TU
n2W3l1DVbFTUQaQABYqN3qHvTOIBcsprktYYrqYlgMrnWjd179/lDkiAz4g6M1XIWLotwkwnTs4J
6vPnbApUNq+TFZhtx9kcNv8qs/q/tl1VYLkqGqmg7KIMZ/G0QMMqgy+zOJNVhunFKG4RTP0BHmB4
rZeRAFXX/O1O9hC5BsevL0kb84Ui1h9nwB8JFDLOGJ+AkSXD4vYFCkEg1iubenFbM50csC8YX898
yL+KdBheCJICFDybRdnA9NpBwVZ37+vKBqLk9E0Tg1rFCiL2oWX7lvke6mbEUtrxae1Cu/oPJQrD
FW1+b9Pt6gKmOdZCmoKji1Zzf+ZDu6zZUb2rE5vc4HsV3hmu0QKPUYdH+vqXpKB+ePZu419DcLq1
JLNHbaLJjMedhApxfmZ7Sog5WFGYPLJOQ4cZiZXjI4Z7HtHhqCZBgnKnk243DoCvcBM2NOh/ob5o
I27S3btk/7CNEvAGbOhGWYAg90Uq4lyygluovNYTsiBrm43spCN6xfpxWtNmG7bZ/gTcsoLo30ST
soIYGh14ACTKjJOxDf7IYfe7Nm98ituOh+eIOWIUEyMrcbeqWXf7xdHwnwJszkJtv3MD94NkO3Ag
mHzHoiYPiyZHhcG/pPwBOhyL9Th5u5nIPy1VlYpu8YgPOi8pAIff+wfTnSOOACJFyfUN4pheGCT2
FVgm7gnGkfJK2GJLGj7FFO+hYDfo7LnhAntZZocjjci3Z68/eB5MK1CbAeNKKM8oeT+SbAAHlAAT
9klyvspxiQLlKKVauzrNi392yCMq1fHgl8xmPRsSJLgJaNdlkXlyKIBOW+h9rcbUVSSGXr7LMbEu
6/7dXSk1NawpSjA+FsxcGDspiQMO9HM1VRHrIl4cAVn7EeWho7VbBH4JnjH56VPvDFLVoeRM8oXs
xh7JQanuVyJtY1exsoTsHWOy8416QA+vUlu2HDNDZv++pcLekgxSF98/iG2ePbZjoQ/6AYmB0LNE
ahnRz1/0fVlrw0jw+TL20RNx0aKZ8rFHCAsWIxvmyDGhFotRxXTnr0YfkjcZNWi2nj5xCmep8gE6
0TCuFLTVh1IiFwID2MjsBDd6SOaTya4kZBSVryKDEkySLLQuptvhkdo4UTdS+8noEoCRQHRva3pW
FpqfjyiCsMZcyQ/nJ3xvn+DRyK8kFeCZofOd2b7XZSd71p2jFOE46kCBaQvyjyjaq+KCT6dzHYrU
qF53BMi5VEeBiZpCoNAZcX+QPO8m1flghSZSIl4F+2IlrUraIBGv1S07pK787UY9PFPXoALNunx/
tLg9H4+3EPP5cA4862pqqAb6u2xi25Ci7dt6zuED2bpk5FHtCenAtQCTke8F9ew3OPK7a33pNicl
6Oc4HTDb1YYlgXV4SUqs2a0bJj4sVkn72djL7F2jG81T7OV/IyezHivkDT6i51u5LefdqHUOLjQP
5IWB7J9yMO9CWv5qBt8if8hMBOD58eDxWN/9mN9C+FYHLActBlT7tBX3mizz+QHzh/M/wYyxmmfI
M7Fu3B7x2iMNwFqMwIMl5a9FTWJ2eEAUzJuyPbbTRnSjc1oxm9Coauk9iCFQt/zWZDKZxxBcn1hS
xb2+j7wLut+dK8PoK2FJW2FKs23lXWFvPeg1WV/1qkwgir4U7l8Ke8b7YTsvIBtnVReRJ89V5ECE
FwRMXeiwAekz5waRwBLGuXkzixJy3pR1BoaJWqQ2/qaaWTqvJ6swCl4urnd35YOlHNAr9/Kj8o8P
cyyL5QppirxTrihyakqBQsAO+vkz0LVGNj2C7xsbrOELq4hE3x98DXEGR6VdxIKMy9HAZv5tTeNz
AbnXtNZFZcK+CAP15gExwyEyKx47ttckmdKLvSCyokFdCcqo/jeSCrZ/kMF2pl6hr2R29nMRTo0M
wXH23xj2fCevQcQRyS0u9unXQf0G43lteMsPPQcu27/55bekEEq8sZFoLSAjV2MPXezqd9DqSFsJ
LqDTG3AMMhLIeW1W0P4z71XQaAs7Mlya2QnI8RQ785sLgO1KZPtWOMuqxiErqKsihwGWGlcqULiM
y0gb7egEOIcawhws1OCnULOiqKsdo7lPB7THZLirxE4tdD20LQpYgoKDsx2qmnd5h+9Mzjx5uJL8
ql2aqFPe15L5Dw52g+2+y7ybZd0pqVy2l4qD41aouVpDrOpa/99dZjiwuZd93BO1IjMhvnhuoxTl
gYY6xJjuCzYvy2Q8+z8K/Xm6FzznkUGzhekGkogRrbgae+bwEdVJ1Mcpk1qq4IOcimgGSaPh0/oj
f26PL+MVYK9z9Pbp+WTO5bwAoAKuh+NyqQfPiagXvFj5yultBPyguYDLOtMN7Ep/+Ato1n63oH6W
HSHggeRXQH1oHITvcq6j181IZd2vfYtsXmaJ6mtjCgOT8TFznwN5GQ3NQ4Ig1k71DnjGDFqP3xo7
LBMRlXrtXkyN1koawV330/QPbn1uoyMBlf3LNqG0kxBu8poc2YCC6QXBrjaLv3+4PiFWZni+nHak
uTky0bxI1xT7vYOR1XqglBSqNHMaIaKtagsOeg3pcinQv4qmba0oGVdctPCNBpZFCytPOOd/gGTY
WyjT9jPt53kiOpm/zLRq+KGffOVtJBvgb7RJfCK+aFnv3/E5agZtLtzAdJQSG2URY6lkI3Eapy6W
VnsD4RTjAf4e+kzXRZuYrWYUHW3TRZSem9GQR4Do8qYewDbUF2KZmchBpl6C59zn3W9tmmQAm6cV
5W8vvyDdR0cxD8LC+r8oUoVvKi5p48kGgJ9I2JnRy8L7cgF7WIx+v/QjlScV2iCEvnS4BBc28Ugi
WQQmy5wwhtUin7bQxXjxwp0jBeAUa29OnKePmhrU/5Hx/A2hvD/LbNlMzDZ9J03W3piacglD3M5k
lM1hyazrMWA4HSAcOLfLMhq91+b5B6R/0OXPl1mcOITPtrdjUOSwj4V3scFGSVACbrD124nEgSCO
N5KWx11B06BPNK7gOe4pKJAOMmdhKpYBRyn1cjuIv3CZiUxkVSjMmSGyim4YWcV9Y5MEbEES2cbs
kAvOfsgHglMSAh272jXbD9N8sSazWJMcGzM/LYeghZSAqbnQMnuPRx/U9QBUPeux84SA74MxvmPU
AzhCF8BxKOdGinTcSu+9Radh2Lxvx3Y8DbGFcK+PphC4KoutjObCibrln6jBcnnUXNbwdbg6GL5A
fGpmnNbostfpjDty9d53xOVkA1IZen+5u9x2wFESsMAnHfL9+lL5g4IKqScDpNgUHyIO/mHK1pT5
2sSXjJrz4qM0Vdc9Iu7epC5d19r8n96Thhsf1Ebp07HUpdiG5fh4T0w9SK10y97l/brsKqzxlG5A
kA9DUVL5vMAUwZsut06BxRz6246J+Tl7Yyv638+awtyU/Ah+U2vK8OnGlEPUyAx6bhIQd+6S7Y+g
qFjXgA1p49CSABg4lufMvVR1SPgJuUJ71Fy+KcvIqrSp2A5St2O8HkEXiLMCRaAujs/Edrw/lW8b
pO86mp5M4hOQo61SXJ4bNGIXliQc4wSMPJDIuakUmVaLDP9vtqkny5kD0G4nhTpG58o2PKEgcBNK
gvNMW0l943pj57BqnMczlDhWEKQeTlIjRMAZEXQ0FmjCIXunQ5UGq+z5U6X6xg/tca8OzE8SOUPn
nsl8bCOL0uNRaDOEHshdm+hfWx9JT3+6w4r+/nbZflguO1lTX9iubNVSBmwNOsUpqHc50ikOGWO6
E1MjMhT37wgLnjop2jKsLuOevS9SSb+FjHSylxKs/7VtRo9uxfDgp8O1HPcMQi2KW2n820q5eNGE
Wv1l9A3jMW6ibI54aj80xwYhyjm3Ah6FCme+I3a0unDvnoF7k5zb92Dbe4dhPt7wsMO3CGWYMw0l
EPav8olQTqk8x/o3f1LctxhujhllyXdYZDnhTUNr4p5Qr1JvSNr6ABv/FM1y9tUxTT4lK5h7B/74
6ZDefZjh2R/ySieOx9U62cDlbDfjI/jDwveufXJBfHA3nxJN+eKv9wpYqGcuBxLOYCyxttcIk/Jo
0U0Pd+rjWjE/1zzBVWScQk0b+i+9eNaxyejn3lVn70nV9J/OSNB/nAeTircv4itA82qn6DVHv2F7
UfJR5ZTJA/FOoIG7Be6WLOCkHSt9+r0vfOncv0OeGNDqNSLPkCpH+l/BirndIqBiaS3poCIGG9yB
dzBpcXiu39WaSQKPRz4H+EZa/nQSi4S4eSvp7oHmBZMnkaCl6Z+CG1lAFOpk0Vr3OA+CYE+G1Syz
TZMhJxGQp07xs7eGUMzHJiTExkO7jEQ/sIs07NexpQVX0eSxtthSJvcrgtW4m9KmX2bVCz+lePIU
iUoKMwUeZToSCNCJCUoQWFHlvgDB90AR8p5mbKVDMIJhiOkhiNjIvpmQWJlmaWVxtqTQ6LA1dcWf
1+Z315wo3AtXrJGdAV92M0pNJ/UzJ+W9HeJMjkPAc7kt2IDtzgcCDUlBxvJizEAEGzl8ydzt3xa1
7O/ilmOt7eDa9QmUzb7sCswaKoTCIqEOZXIGyJ9KrTjCCMCZ077n9+j7OOwIt4ulAvzpQHIunWk4
gxlFsnF5fbm+XSQke46W9odkdAOnPqjbOOHgYaUIjIi/NR+lmhtHR2qsy78oQ9qFlNm+W1fWqsx7
/C3AoV4y0hWD5oSTNNUB+75RUYUGcFNdXMvNIdYPWe40E3bJme7pp1g7b9ZomsTdHynZKjcw2I6a
cUZnElTC2m3nOBdqZA7rCpqbYRYudDHjuq+2lKTDxhlKiiQgzeiLv+h7r+CnjXp1Du/BU57zWZlN
5eZYWnHLQ3yZKzPV+QO5mjgA6DQ0WlmegGmIKgX4uYWk6EzvXeJh1w/p4IpdnDVelyP4tLFGFQJG
3p1fvn5OiVlszdHJ73xKPYxjw3zaN9k9kcwWZK3CLbIUdCp0brL3V+zshySeyzhrvSgb3DHjWSSN
IkCNTI0jRiffQAP3HDQexRDAAd9f6+MddPO1WITD76Tr59qSVjlZh7RFheaMW1OzxE3Ua1cYwoU1
YYs5vwwMq+87DKci/P2gXKrQSBZT5w8R9VLoDnro4E87KY+cIhSldTBQfQNzeRiU9BzanctzSo9Y
Zjrt3OYNuB9Sq2CMhZvZCavqIsCKuIOGRRGJPoWrDmyuH3dvwnNDgb66pOBHSR3AN7LJyjmygvKQ
dCO/NJ+gkNbqpVJVOBuv6tk+SnV03nYrwqb3PpbWTBD92vnnQfg3AfmEzo0zWi3xLgw9+VB+Iq6G
8mxmiz00JgDlbQ3tJMUCBxVe0rNERYPoor4oYHdfc6tOAVhdUt9EuGbd8/M3LFmPapNtuSm54lvI
5wMicaJUgLQWxTqJV89prEPKifnP9lPN3EZ2Hs+SNPAj0Nth9WpbPD2eM4J7z9DCkkk1K3vi910Z
cQkbnL0dfU4Q1UXiy5NZKjqAGDcMGgzlc5qdGa+dDxPnX0g9XX8OJQ/oR3f0Eyfwb3u+9jqxUEgO
pzrn+8lwsT3IdXqRIeuwXW/93d77az4+q5oRTs4tbDEKl4iqQm+aD2HogLm6jaLyK17urMi8xx7v
aS/np0tkcQfydr8Pd+HRHCpwtJQVGCMxEcxNc5i5xVAdSUs6tcgBgYMwQZgMGgwVvPz7M87Ns2+Q
xOwPukeyH+TnMnSYFh2oOlVbW3NYQ1IFGTUsTjFsFIAjzmB5XMEfcANSnp/0M3zuuqyuQHpxf3Q+
VqiXau3wHF8z6qnRgRQD8U3+fFCSfzN+1VegrENEJCADvZOwZ1M+wi8LP2Us9RCFrzzkpXWC2vLG
Mm3iptxQkUy5s87jIBt/OxpTutJlfhc6Sud+OZKG44yHUn0WO/nlv3BMm7YMoxVP8rao6dyB283p
YB2uLuJzvCr9lBvrCuJUN/fG9ghqWByEGMmxaXCrQsLp+uOd1OFo43YwknJoUvK/tWqAwS6cdPFd
L7UfoZ1Kcnv5dx/dO4ISwb6D9hHErUa1I00tra97NfLxpAd41+1A7RB26YuT5Z5WilUAfD+Xyabv
5X7cyXbaC824ezaBSWKpGlLtx5+5a3n3dkpKFs913WQ99WUR5Dg0jzW9Hpg7voRWQBgxhx+CcwIy
KfmKx8cM9L+zO92z1UQiG78QU/q1YxCt0jcq9Ty14xmZYc8S/G9ESyvUyX2TCYjRk+N7K9h15UQi
4wkJwHrftKBxuapRT79nCYdTjvd8IBcpruBOzvhoEDvwQ8dX7prbhW3WoJIQjB/6YTQcTULMPH2G
42nWCzb98X/rUJiwE3a7j6AFTJZBU+aFIQ9fKGZ7VYLieQvoJr3l0GA4n/kH3xiV9+XBcS84O5j+
HBy/sHWRTQHj667C/zI3RmhBCiaz0ZkTJRQ19OapfRxHqMpGi0Q5AgKRd7taq0OLGkPs+Mnd303l
QzUvOaM2jJLyovuA7qWWI4YZ1RnJpC8ZwFDMcznurLhoVsyMjXichkzPxkP9oMA7MdCl7/jQpIKz
DNnWrd0rIARZIGpC1a2icD4d1l5+JNGLEaKX1KxI1R6HyjzYqA3T2Hz4XMW3brUsvL0WlFVsVoSF
h9wOqKRk6dWXv+0hemWIPVk1Q3zp41ZzCWaImFULzGfRXUTvnVYZvY0MIgGENA94ldbEVq/t+f7+
/8cJIvBgwXAmgtY2T93W6AoehXcWJCIM1jpz6u0ZejgcjSa5s+AVAxdZbuthUYUZzkvahIX4iAws
aFB9FutDXdPYVUuO0fgha4o0w7N2Oqak7AcsH2v4Z/agxTe+3RayiM0fEfx8rzNGrEC7vzdD23n7
tP1Xqp1XVswgmoXXK9Lbk0y2DBS8qL5LF7xSyySccqrAg+CIsB37/NpcJKPjIDqKJ2PKqnUmUMTv
yTwVPeRcwBI0IC+g3BlfDaVcuMqFEwiyBt2QNgdUTPyq3krCt2qVCKndT7i6M9ONJPfCmd+NfRpX
8MAUSRx+dHNQ3a7hlbL1RGd87OwM3LLY0wmD9NGUBxv9GbXbQHsrUGJ0x8zW0Exz/X2IZmgVHKkR
2FcVSKPXIGKY2U7MPjJ1cgPsqvA0+VRr1WIENlVBcqea72Eyq4FZPBK/Nf4qChzyXjfk6n8L4+GB
7HkFwH8qlPPrzhkVBbW8hwkyxn2BwkMceXzJGOfdqUtjAVi94+dl+Mcz2ttEvlH7v9QThFKiCBPV
N3mH2tl9oJ9y/XYHLDh8yWipRCvVheFg02eimhklyrvJp1NYLa8o1LmmTg5f9dQHupKKMQ25kfEO
+awb4+XZmwZUEenOYbRFIzaBu4LfG3BkBiu52mIHGrgLC4T2hGlA+LPEerGb1uWyiI9fUObJ0aTh
T2udgGlo5b5x76ZfkTwPwYBaPU1oaFd1ujkLjEQo43jsCywFJ2Msl/3lkKvRlAQ97Cee3adNAXkX
q8ass/iAQ86HWTSuE4JgTh0NePDWtLk/nGbWjJm8sCK21gBwkaKY1z2yvMHnuxP5k1ly4CmbjZa7
3KRCxcaD0vvbPUz4hldinc/6kvlnatej7426hZPw/pxbFvdu0RbdDePt8MR4Vmh6ABs9TvYTet4y
wahnI9+521tDt2mc3eIp4UGXNWCFlTrrCi5YGnz9jk9yugT3If3gwXA6gs3oLT3eBl81lu8NET3h
ZWcVF58TRcYgoDeScdnWw0Eatu8H8259ZangLd/oJdUZhAB5HGV7y3jaTr8xTC8a3Er6K3Cng398
mgXP36rDpBWeCkR4OpQBJoNW13dexy9qTEcGNqhGgAcTgWYbTrJTcjFKHYnjE8tYcimeZWGnwQsF
Naqcgm3dT+WESJhsHumuGkROsBquDDM01aYxziFP/qv6UyMHyFpB8NtLm/jAPMe1v/rMRsUG7yRz
V8HocjOzAwuJtgjI8VlNTY+ty7CKJS8bfb9dI+zQKoD4TM2fqqv+AtVOR5Z7iw20R6Hu1zKm5FyI
Bg1w4vqeHoZmx0j4DHYia0TFNKTYQK3aPZ1EYQxSVbdHerTZkJy0F6z9LAQRwVpxEqpo3EOalWNh
jfAAaMre9bKYsez9/FgEgBjkiEU4TbMEteLt06IzV+WjypqPfgUOEyeo01NOyPy90QcLZhgQYY0x
ctaeZay1WC3i7BTq1rENT4huL3m/n0XUqcd4btagZ8+BTGmJ5vcnJyGbvkhBzHkwD/yhfj5Lyphr
MHCBpErHWt2AggFKZ5bOkQOABMNPk5VYryVZsy1RR/fJUBZSvz0YIRHQYl79M3JfgqfsQ1PtBZWe
43oAs1cx5/alpK0v5P61CuHrnVGPOjJkP/fxhqNnpym89rn3c4qn6QjQOIJRxUdRfVp1EaSffjno
lz1QbHJCkj2ptOoxj13KBjAP7Cs29ZSndcZ3RKw5d53x7m6B5cyrsPN7psaHTQnYQB3Wx4oDr1NT
s8XsDM0KCgfRI3HbT1xLq+lvUGb/gCqgFrtbEB/HlqlJcp5Ksd7eOg6vNOAo/g+HbIymbQikPUaq
tsjGziHvZvITVOoGBAUf5XbNWUjeV9SPzeBuk4B/kC+Tah4vOa33e6NJauLmae31JrRB8wg5oeGi
RRH2hXeDXGKjEBj/hPaliPui2NSpFMKyoA1uAkD89p7kmLZHF9GYpAaQEWwBVAN+aHSVCgE9DUM/
rL0pjlZSUzcZeWjUlPWu5AFj5kmZik9Mavdl4GgwW6zqMAeC7Qj5lSBGF6vwdrPvBlN2XaXxpRYs
UIzs/QmyR/eZkjsiFAxb+dlq+iXMtQLutv6wDz/GUjYM1Us3lQK1h1pBLMZFUs/y219fBX04GH/m
3Y9bjsrpK0eeXwjce2pm20a6gNwpG6wTb7T79NYoUPrsxEnzj4XHYqt+7yumD1+xs1TXwYo/4HqR
hQPlzlBwpbRGDRroHrg0JNpq8nRNOKzDSloP2mPxk4V26wcWyUEdCejAiVXay76a6oRYNXtDfXGm
6IUjPErYkD1KxvU2MAifL3EH0QDWvZPANUCLVl5jRq+KqKkkQb0vi7uzM8q5jZMQOvPQ1zEFa/h/
axCkVDFXMS1so1/Vnt+JngpIYXszZTqLEoJbNVVq7igydzXKx0vFdoRyTN8y0VInp8XAddbRKJWl
tr2Fr3PGy5qgCkXbp9WE2GN9DBgTR4VTj1NCLUBz5JGUlvO5Pm9cPxfuGeTSatChlSwnezhqLqO4
0Jf69F4qlxcy8rEhKiwXQgPxOysi1P4bNiVsDN/C35EkxrMjJYgSPJ3XiDBW4tspXnUclDcTU00f
uCof90yzYIZ2Hn5i9apJyDDT6PNKezZQ2Z9cLEKqNNcxyVl3AZL72dX+0FHlwIZyRfxyEnhiN1uR
LIUyGI1yJBPZhPHeSjQTcax+PafFJslnltLCA/qpwBLxbSQhjFSyU3JTTPA1RNN244ypbv67CVSf
ge435pVlCJ/rzcE4FuWWqHnJBcdKg57uhOqQBNnd0oU70GhJx5UqkYWvRy0+zXhmlGhzMvn14eIx
BX+moX570wDbvIX8UYP1UBlMaEJ6R3zOcfRwMlews5K3nKLYMKdABuxEV/knz4p4TROMLTzzbpeY
bSExandgtt8CEgLb22xd++oU5o/LlKzTAIQ6gX2rkp9COboqtnJaJUXNZpM2o1k9doDoc36LYbMr
UJPWSS5kTDtL8WBL4nNODUqLZr0LRStynBKIlFFU406TiSYMAZZJ6OlmtS4/tSBeLr/wa7KpbQfF
CAsXqo9/L0gJkb5f9qIVDv8BcoxlgI/7W2Ih5m95seInLec8SZSb7b8wx7woGpO8eQ/FeZBaiRpr
REOPEfkwbMmGq1NQnvXO+JNXUX5R7hTO3CdMsznS9v9GuJ+G7U+JM7Da5QneObA/O+rQQtYS/wAp
IslAEclyB8D/mJeQslTdQtT11PceqyJDP9EsbB5GkmrT40lJj0A3Szpg4936aeAxJdPzgtP7OOIu
ntJGJIJaUVjv8EdL0T3b2na8x36GWiihonGqbSS0ch2V6+RTrdic5OELIaoW02UajH+buO175CVv
mg7EsE/UPEmX7oACYTXdP4Endgz1IeXjF7sFP57FbNCwmTuANH4wfebmt2YIfi1SZWKfTyzgsYSl
wMY9xBM4i1vRztowKhxiLzf03pvVKqcn6AIAMLbhEBBrYTp6er3NUOfbFeqb/XK+W5dEzZa2yDnc
L7XRreO7bA99hkryNBxJ7vb3Zut752ZBZ8ZwS40xtse0TeWhOhsxwHLWasm4rHXY3bep1myuWsD+
+EJzwHI31tk7vPFRWANCoU2VTVFp8r3zoRTVy4jntMM8M8a98J0RYoMzlMizMav3S+v6zt5Rb9oh
gFHB0fsWVWMf9iTAgatXZLWpysZVlhsDoc297fEbtMMzAEmC3BVN2L/MjC6+EB4O5wQWkdqqhmDo
dwKLaC49ZGCD9UxpvlGSh5sY5EwEUuxbV1wVfLQbDbxfV8wF93WUNg7K5mqcHWYl91t9GCP1fQA/
7T5PB7nuzOE0b2AJvx73miBqgVCYxaf2LsRgy1c+JqsdeLFi5bPhBtND+g+4HA3L5+DA/M6Xy5KM
gf06rLLTkmBKMMCog6iSO0MwYllmFRcuOa0GrkCbG9lfkUS3/IuU87UVw4J7wP+SDslvsIMDuXXe
gqKcsy0QRGSHWoE0ViUevkhtdP6YcNkOy8mPIJL61tYXGU/IXPbifNOFZah2Qg9lP06Bo1+Ayb6X
MXQZbJ+wV/lcHv7oNKtoYuEe4aEk3XD/fvLaJQSwuIiihQzzD5nLv1sMUCILR97JNXGuwrYlEbti
Fs6zyd8SW8chizHMhJM1n8HmmcN5U4yuflVsJPnY5/HXdMNnrzbXoYgq2H7wP/51LGg7Ob1vi95j
OGAHofPZenP72eSiIEvIbRiavxD0hTJd4ZkuEmkvypi+4k6DDwIKbiqW6L49hS47DXDp8KbkOE3w
OlCalywF5xitpeimv+HKSzqHUtKsMsEz6nj+68Igeuh38H12cmaBo00laBHFj6kX0oCsdAM+7TdG
8q7lbb2ATSSSkuuhZlA9uwHCqLr3whLpTT3ZABy+eIQ4BTsO6/oRv8mERCcumrzE/UOzAgNs/sz5
PINulqReoGtkZ8b6rKZN9Ayg5MgFXGLpoEvjTs0loyDh72S5pLFr0zaBvRdlZq45JdyWNubDMnFU
Rb095YtsE80/kcKGzdkSaDJQ0J2G5MGMinZLQGnqZIWAJcfb2BqipOPV+pksRAkaCzv5cdxU209P
ix3Zpwpqk0eqZtUQQqeKaq02oaF84hrSYZRbZukIhanskshAk1sHzoLILjTCGdN0HU3ILWAWdXi6
TU+nXe+rZ1w9EfC1ITksbspknkJKknMcs25FsfvcazMWDUPn0/Ybl+OZjJyPierm0/gitFRCjrNj
EPCyYFNhbcIQdsJrGlPEDCLuohGMASqn7RmYm+OXxuVqiutR8tsO5nK4hjhndHKb6w76i1Jv9Y+8
LfyXcEVqP4MN+sUgveVmBPSIf7O9odA4uOsz6otoNkusUHJcyx/KW674aBGXAltIdP9KOPVuEahS
AvOVm+idhelMMktiIdfjqzntfQjQpGIqQIhbZoK/2BIzxnk3DoVv7k6JE7OWGouHj6JtTdS/Kz1B
5/mZ9NDmBP6LEQzaZPA9fi5t8tIRoPGgF1gQLF7OEkFN7O4iWmM8VLhVT6TTl+kRK9TWOY9rFclW
KlTcwxFNzUUZc+y4bGrzDxWvZ0w+e6ZP/zUjd9sAg797yM3EeKcC4rxgx08CS+kNW+6ZHb8pAhrS
zzEmsndpJ2xFad1XTF9yheky5flWzj7wCxEukvgxRX1+kdJZPyBH3VDyIa40T2Pb+JQSxwC2wovF
/q0sOC/z8/aEkRepEcNrQRoVRxCXBfN6sh/ZAOjh9ND95NvITXvJdyIRUCDRHfhADB0Ghyh0uhjh
t0yYk50gk86wrf9JKpfhQd6VHKw3JTJlCKjqzBuXxLiCnRYVk0wGfFeXEWBJ+Wdum3hR9TVF9VOs
Ns2TDuPbk0gHS++OYfVx94N7E90DPLWR8OfWV/s6iGiZNj7DsiNHX2VaJiMqs8E/2O/D3+dPiNL+
H/VhlGDV0DFPsvz+ozbPQcqDKCU4mHp4VYGu+NU581ObdaPTWjK4Q7BuBBTI38qS7SS3PXdRaqok
nxSFdxOPjfMiZ4/5FrHD2epXfe8QxfUhwu3VCqZPXTaSzj/NCT0dt3wdjcgx1dZWVXJoAZsWjQPg
NPKeKG87dIlrGdQCJF5ZI1DA93qeTWTlip1zxXkSkZGQIJEmr/GKrQ++qMsUHGzWNrrNPL/VTU/q
7Xof4nDIx2IT1JFw7uwLyKOMN23/bapkhn9JH3hVIZ+2PJMe0opR6J3oppv4Hf3NV/Q5fEHjsJ5c
Zjp8lND8HygTRqOKDylEao4iyAq5z8uag4DV5nAsOGn0A9hlcqbSAKwIPRVSXHYYgcHyIJUWOVV6
T19rqnnS+4R9yF15aBVQqQyjqR9AbtBwkAUcIHVeQwYlummc5N+Xw7ruPFXtjpl5iAptyeQOJVSl
SDZiGys+xuqQIDWff1rU03N92GZZ3OpYaRkdC9+vbC4Y3zdJMi5f8BnSQ//rpLcoCWwJwLxeYOcZ
/4Gl5tGPanbkU9npWv0Gw5RFIcjjEc5+owUQ40jJU0QISWcAdiH6Y2K6mlDpOAWBLLW635jwUHIo
nSfaDOiJ73q2Set7kM5tjJZRL7BJkD4gbcgataJucHW7/KNW2pz27D4E+TxSuCzA7SFKWG+1JByM
NDwAxGbk1sm0lO67GCptV2f+ZenjUFW/cT1gOhxq3WHcpII6qnj3Pd0bwoJkytX1FYoE7VJFFKOI
2914Frmd8a5ZkHUrImWfcJzbXJA1VrWMo2BnDuiG5VaQ7YbSfD7C35h/8sMS1ZkXTtDYziTM+fs9
crbl93jVq7Qky3Cz6R3Eq7PgOfYaoVWD9Nd46FvsODtbdFQkIgXBruhKaFBu2b1sJOxnS0LdMw/f
xsi9MD0A2US8ZvVBB3Bt2nMPxKXNRJv6afJuglBahwBAqHFmPq6gU3jRgtJVYvp8+PJd3Pvbrft0
xm8Yt5FRPns0D/pn0VCUVVG9EAYkgk28Hhz333QYFPegDGL3FaxRqNttfQp1nU0tEYGsZsGyILDx
rLP+UMFEySt4zz36qLSXbMQmI1eDAZxgZbAcGk0xLWOOrWzvpZK2G7grofFG9llQslqSG9dX/iB0
xweHEnzKz+JmJSUVdrvLCnuYRLeg4EvfBTl5/0SB4FjqUe4CZJRB/H+JJne1OnYMzKCiE363yvEY
CCbYEVL3sA3ys7o/yOJPLlRKzUcfsviotOLu/teW6CFSqORBZVL5aNpdYG0M6aqYy2KEePOLdAyA
zVoHhWpvwEg9yan6/7ezQmms++Fj34i6nR7d6m2B/xhLoiwZBOCiV7+LtafePGquMNwf2Rd6XQ4a
tFJYQYZ2e4XGvIJu4H1NsLUGY5PuAn6kc5xWOZFrXutEU3M0t54zb2ZRGvIKnXMTdm/ZipeaM0yh
m+KfwVgj1QQJKkLRXXUsS5ZdJwDJW7AfQcRkMzlBLCBYi4Dj22ZhrZEIbOhvwOMN/FMFz0Fp5aSf
2wvIvjRsnZx2NCLiDOj8a2bfty1duNowHYpV/6QnwGeFyQfelyxjAcQtFnOaYB1Gql8NyHpdBYJf
DSWtSO5ZCbUJXZ3K9x8e/lGuGVRe346qgR4obTZaFczMIpykMJlOBmy2/bjm5IsGkxLg+JUp215B
9D94bztVvzzaGYYTsbFhZxkD4dQRgnDQ87GLe89FBhxh6YxFNPOoPl8N1TsEha6cnirbQqR/tAnh
rYxmHA8P9iw12W376vBheeMiGXAjeXr8CLcHpwOq1vZnOONkLGPx9vkFCLBeEQThiRwWcPhi/4R+
Hz8yKIPIn39yicdJQjRgls6irQMyY7Y4BwTP8A4ptUrBuZnqrUYHPVzv/gsJujrGsyNZi2gRultB
Y27+2F/Y1QmUpIKTC+/YkfpuKKF9n68zB2IX3N8sIThMvcXcLThrXJKaMdMnVIISkW1w4PY09TCm
GCc0Yy98M3U6fhuspZhDu5gW04k7AON3xR8cwkC+Pa0rV4VhaPeeTiRjZbHH1CJR0Zpcjt1oN8wU
/Zb/9cdMcF427QAKc7wosewSxnv6TiUsOqfx82rcTwen0Z4S1su6/bzFREh3PkT6zv4m7ysmDRhx
KGXc26k3abGbdyHQW1+zJMs23y9b6T+bu2wnUzVyBHmeCJNNO1w1rJQijc2jVPnZhSBcq/1F/0ss
1rf+aEbqKj0SZdJggEwcAhaj+ixHDWX7GEHW2nTedppzx6lTGl93KmKwrs972Ur+wZ5GWfTfVd/a
cWgdZT1yxM0I3kQtoWEPEK/3QBqPZz/qdp/RHPbnI+a16q0e+vawUrmXfduIwDdGzv58U9vUTG5c
X9Ozn4AdaQHyIhR7xm4DyIh7B8Ezb8shiJD7RJtrX1vLrW4KedBBibVwPMf/SC9oYMLHgs5NVbV3
6/zWcmaHHtiDAT+W9IHFORZ/yofQ95YaJOyJWzpXAAjaUpiaCnM7+X3ADAORIaijoQIpC3BhCDoA
Snzw+dARYhmgyJ6NZCLbw7XWMDjeCzKJ7Y0uufaKGaf+sbj4AgYDoSyU7nT9/Av5KoIrLfUPOwwo
8BhMV0/KyD/JHp9XRzXIo1/eMMGiwSOIbxYlZE9zHA6Bpw8bErxPq2L/rj2d0YBxGDaQYq+hlyAf
/kCljHKQ595uWkzygmTjka+FGsw/JoIiKV2xOvB2X0hrhn5RGhcCiMDqEdvnrECNyczJVk/BwLWj
miJYIl91/9w3WQLXtWdBlAeRmbe9edQZkPYWbELs6uUEmCajPJR0t9JNV/bQinoh4tK2KrZufJIR
TxwZNw5rquoEYGHZnRbbZ9JCn7f9n8m0oRqLZeibHmp/kw07vJsDOs5YxttArEFFSeKgCbQxBkfe
SuYYrlL/jKMZTp41sgj5e8cKaVALyvukTScLJ/Qq9IqHmGa3OHgsc8tGUNlt7TDLKvNRbqZsPvaa
B9X6DSUNNk7JqPXWO6sDYJSJAg7PbHp2Pn+Zs1dCqWD5dNeOKVS0Efkc8+XKyUqJ5UublhbJ+ZEA
kIdFKI8JvpwqTE2dqhwFZHTfbt69Iv7wkWLu+PQSgjTOU/SClHFC8+NuTXNK7NxlKRrN49yxX+r9
nuxOG9zVg4TqMDLGdkD+o8hRFr43yoRpLLiz9Zb7YbeMi5p9nwf6ywX3Ccan/7bkG/9vNbbZsvG6
LpHQwDPgwvhAA7NyAEM8SykqpCEnZoqjTr+3WWt7A6UGCLkaoE5LLahLtNX4VYWlZChqYic4us4q
6kWYqjVfYf6v+h6zilj3qWoji/O3uRmvfwRJxaC8G4/zHMuScUg6X7ES2U653sRZYqHb1lxurlFS
jO5ed8bTxm169Ct0dbNOoPIpYo7VQ5EhInLN7QD4cC+1014MhN3EWA907m09bmD+tujjZ48XMSNT
Z6/Gd3hE0y3wBZLuAaxFyM/OaMHsge2iKCEngXbgT0/3EyEf3TI2gzY/IAf46FRNdtF84StjL+LB
+Ozkoa3ShlT1HTrEvorbjvxgtVU8kP+7cXp2H/f6NGehTKjXmve1Rgv1nwCgdIyt1qqPXoRtREsS
iFWxQsSX8po/oTnGqOcI6Ll4t9NXb0d5jU40W3zEPGfk9rnYBlpxyQIA1VTAkuKTSPTyaU49eS0E
Ej8NVOsERqZNdGyAQYcU64R+XxoIO6Advr4dKtvk5ESt2uWk3SZPJGnwvoJhk9FMHXXPcviOGmKa
0N+sfmWtJpWJ9o6tOqx2Nys7PQ36loCxERMJ97IFPohoV7djCuEi/Dw4NapbxuSOBwwJAh3H80Um
6iqbvOqP3Qk50aopt0owSK8tFpeNg5OM9noEz4XSpJ8lvfVeAc7N08VXX/zwThtRZPuTyAtxNk1w
E6x3MH4ZwzYNWZGgN1VDc559FkROhgnElZBhHPPn6eELUh7U3loAz9H5AIwksbijeQpJC1IgpXsQ
3hNSMojI7ReW7yizINkPElr/wQfSsNv8dZH4MV0l3M9qy5dMgoXVFyD9ca/YEKgK5MicAIaqC6Dz
d7VoRNnaUsqj8+WWG2xirlEz1karnW/IgXGU7iYEMzSE+swkzRwYKiWV0RTxgE0x+vbNZoyHxDoN
0CvANEqrcrYgm9Fn4EqyRruDBMr5vP22bBx0x9jJNdvPJ3Xn6TnSKgdW1y4SwUOK1hgv5MVEUawb
z3d3qjSDvOxqp7o1A7JWP26CBiTNCLCmTfE5D6AE2EK2dV9IOGct/tIDyXoTeIKC0nDfOHeyEL48
EgJmxoMFzgm+C8GUkSYQWms2TR2XZJ4EzrJHDuILRivuw19DNrKXFO59fhbB2NoX7D3a3/lmNuWX
RT/IPo91QzJWeUSbbnZ7+JeFYnpBu3xRCIDv+q9ZL12Nwiq4SQJvXLpsYFC2lnQMMFF5qaRMU96f
Ye+D9gr7y6O0vfFKFooEy1h6mYpWJSrxcGiSGvfJGGveeeZW++G79BfBioih3+7B/ZYoO09zL1Dy
2PcosGKhDzrerjPJDdkzwwUuaRrQ1stYnYrcsVIVo0jzToMWC71UDU2Q47LdTq1RnnmIRRaMFab1
bNS2qNHAojTISZVL/LPKvxdH7zM0i7JbTJl4B33cdwyQEiDSI9rOcyT8Z3Z52HaJWmmShF1t/Noq
rUJ32GGYB9xrodSk2azUDe0cFtec8aAvVQUu2Ail4n/UJzcad1gtft03QnIBtdgNfYRvLw+XcdlA
SxCPbbDH0D81LIct1Ni/oSvBaNcxiBhRjih4GBEKI6oNLdSoDl9h0O4WaGMpbSr3/SuMKTfNvRc9
7//pZSGwQf9E2AH1iBfBqwZstyrBj2UMmgSPR+8sb8EgaMKNgSjX3QqKO7/aoRfXJrUTngyas4gR
szkNBLjLja1IQB+OFXXyBgW40bxL27a03uvTiZFxKVjXa8rpvgQPe+oaht8AxPx+wdRiV2nMfey9
ddihZdMudGZ9z8dzlesp3sBwqjpIqO0EZ8ib0UxX3OWCEOv5pq4UvqiCLccxtaeZ8p1C1PZ1Wt6U
Lkul2HcZnjH8y+/TiLrm2RiA1C/pORO+sUxA3DZxbb0SMz8eeWLL5+GYnugI9Oo56HH2EG5Ac76v
5UfQirV3nvHsNS0ja76GTiIcoBnOEH9UXf9htAmYC2uTRBYWmdPh8W8voNH4hCyrb4zclkwRjHsE
IBXgajJQvLHF9OATsgAuLvAOfkM9sRjEkTbVPlglfeJKMpeqfYTEoDWyS3/L1mzgUX2dICQF14ji
eVthyVlQOdMrZl79PA0ixDpx7nCUvl44xSgfUQiOc8m0d0ttPVTAk/+8mO14YV0OJrHfBlxPIRjH
lbZeC60s0+UW1lB8UD40dMhp0nG11zl1LhjDVv02xvxwQJjMEWrZpn6jZWVckEmwK0oQ9cXz5QLd
d14QfvYKRg9qzBOZFWzEq83eRl543KuyJtZYR4yyVckb6RHDXlHWNq1y5JVIIj/50nY7esbBqNyH
2woxAUJzu+Ua17HyZGgxDZAuIfRmc0Bia/ytMX/n7FrpIhybbN7cz2ejy0HMquR8TWkpfKinRsI5
w2byjxrUCjCHxkEuCoHXtVRrI4XjvoCa6fEOm69IVvfWiUypLTyZBQlIKsAKF7bL14/SB77/Smjq
IizXvEiXKzYA34KFmFZDNwTZA6v7QqfnoyqGs0CL7hGnXdTRAf6XAYrI3HPtN1q9qfi3NenvOnSs
zgZFgHRTtJaqAK1+oFzvvl2oOTHIKg5NoY7GRepzglpTOu/0GvXLfqgoqTeZPBhpLUspVzgcdOVp
wCGuU2RctuL9dtJoucqA9pcri+LqLPyhrUyWSUW7zx+WkNsrmDUf1Q2sAaaOdVW5SjQpOmsEKM/Z
DRt/SXBN3ik6JC+yMyoK9hhvxw8gIZZlwXAxw3t16RTXhskDDudsOdJa1CDyRHKJgOlmOOM693zu
Vb9MnmXnQrA4ZqGOzBX6y9oyGyTSI/z7/2ZmE2ve8SFCSSe9/T/B1DFQQNDu0+5ehaoyR8hov77D
xRwn7f7Rsir00a91p9oo/iSKd5rkIFKBdN3hOCMh8f1rQzmukaWE3+++e+Th9AJaPJmOUGQYd3KG
6DqkAgrF1rToMeWsySqaOMvnHeI2jJ5sxbxFaFK5QY+rsY9qa5KYHqOR1klGQ3aGXhgrhoY66XQx
86Zq2OV+y7jbduu+obJTdxL29EtLpoivzlUUlnEuaMCAa1RdvTUOe0qcJqKaYZw4yj1IDBuqSrex
qu840J1WaBSqK2Yx1O/CExfE4sxlYm83VRpFZ5IE6+GfBSLTFmlmSgC0cci1/0zlGpph+Sux1Bln
m0/VW78Qxb6uUKE1hIuzxpTrv+MI2C4IWMdxiuEJvkuDJw8MJpJGAAOIHB1pKrhid6oUhqOPjF8h
F3MPdiFVLwGBRcpOq8o8gsWDjG/Nht3vSN8UtnFU+r4pyHGfnMi5dY0wIr4slY5C+TUwKZ/r+mal
oL/Kc4CS5c5K7XCFI37aXC0IJX4ubzsTVuYg+Cpjl+itD7TC11370Qql+TBPsOEgBbkdZyuBPZYJ
rBld4d/RRC+M0b/Adi9wQnDj+SlbDkmIqdZEcSPAfZAo3H7z6XIkQbyUhmHr1jFwA075NsF0T4hD
uZjho3DwKsk9NH2YGWI2kn6uu+8dnBY4fco1F/EB060Ed3K4v8CeD0JWN/Yu7tmBgk4dQa0EgBHn
TunYWJUu1qvMzOj+8Qo6KchoKaE9cQH+MUn2p/g5uH36nsz07oLqMVwpa6enAT6fPA9K6Iktx2NN
Ki+dgvf9RMzTeFykHjHMpwhJbU5gAUhy38UM7++iX1OkTzz1hYN14GyYjgE46bd2mtbAwbeJKn9Q
KnG3a/g80Xyzmj0Wilykf3LQJ444RKucQqS4TVZCgEVqD/Yf8rGzoUkqAZsJKLHvel13oiHX/Ym1
pLT4RhaW71nKBZuy15QPgN20xRR20O2Qr9gnVsc5watYhjPyqL1U5KkO+ylRVOOKwMvnaXCrGYOH
CjnQBBtf/6c7Nm+iRVJ6s118U8GwbjFw1w/ouUE71GRICoUPFMhBV2U7T9XWj0s9tRmnxR6iykhw
WXpKQNsRLrvmrXEeJb18INRp0reHAL1gvsHiyCoyNbMlPwWm3GI2FIKPXHiEdtJ2fwJLbHmurSfS
htzuzu5XVfQCjhma169lODhmGviyVt3pPqivvr0WvV96eCnr013CPvooqJNw6giHXEqDugZJ4+5q
DIuyXm0zy+2+3t0s3QPMZcDi0HIKridBmThEhSzMePzhcQFlHImz5mf0F6ejdpB4myaPXd0MqJ9g
4lpVvaIG2AAXoSYgSgcdjw/ZxjpkKyGFJsBlQ4xSweyWlkhF1x4B6/Atko+ChcDx+AEAY8vxa+5B
2/mx9jSwOvqXYq9GUvIEaOiFdGTpNMxgI2hD4L6xV/8QdqDSYQ25ccuYGxBCvtB8LVsMfCCHG625
JhpqKFIs1bad1eyV2knC6K++2vHSNYSdfdTPy+XrsnwjZN5iwQsHC6xiyL99Nvon+wM8f0W4Y+SD
APJF5Nc8fhq0lqix+WtJVk80IIMVMDyA2PQkF1Z8KFPFUACDPt2TXTMGCgFk1qzlnnIbqhjusdz0
iF3gWXHWrc/TnLLcreKRnFejQ9gcZKzOjQMksYr1LRk4XDh1xX70OoQAyEXHNxaPPhjSLMSoUfPo
6LrJ02If5FvvhqDZO+qmXZG7Tc71b5k1nwlYcT8J8v0+cmkfgSZ2Oy8TMX0TaqtsMACIsSu5efM8
KEIwbvx7E8FddX5NSjTP0QTFf6f2TuK3wecOIZs8kyr4hvlyAXJCn3Sqw5nJn3QhAXgyx3TMGbpT
Fce5SP1nZESSBThiXarRaO368Q+YwWM//g1KQJe2C0MHYulnqn86+m1jGx86Qxne4792FrYmsICj
WYvrDjv9YYoJZJpJ0YxoGFe22P7hD2LIUbq3G3PSVLQF78hf/MhI2yRLICJ/3EpEgvMLQL69dDtu
qovLwnrRTBYl+TR7/djsZpx4lteV5y0PmbKvVSjaCv1IaFp+XKuQx5teF0qyXZz3JIESRzFlvmHS
19c8zhrbZ2HyCJ2smJhSzLQav4VNquc77MQBZfGk26VmdBiX3cVbOWd+1+bYfuSpApkpxE3YhhYv
feUdsQHTMuhH19oZKgrOd4ui2gd/mxt652WSMgeJa77JuU/Rrze89rHK3CE38MCUeR+Ptqm9rKla
XYFf2ruZ/Fxwm4EcWQ9eqHW5Zw0HkgMf2/eHL2qi9UpFqi8og6GwnnraFKlf0QwBP8It0r/JnGHr
PtBBKTtg/BHvOVgFuvizF1FRMboB/qg570JzBjUHpWJxplsye+XtU32ij1uYrdYMqA/rVqabAPWl
RdSKjv9E9YoLqNHiKn0De5++29uk54v47NbBgpJwq4C8mwNBM8oKc8PIwrW9YCVaz6byv4g/6Yc2
Y725O/7Cd+ybw+GyOtgx2nyEsIJyONmzMhjwnpgjcK7+waeY03LAbcXib3SIZzzFJHzQRMVkl23s
331cllPzWJg6E7iza/PD3jfG6FaiAnaVBxFCUAGwia91azqj3+aJZwBzYoFhRbfDwQ+NGCaRsyOh
aj+oZFF6b+NGp+ZKsOWnce6LzmBcE8Ef1/ReSTspIAu0L/+SGnXRPrldalwSl+aelap41eQfsDYJ
b+SrZzZy0/vndP9LVFZuQ37otVtgz7xKe88M1dtOKBWPerCrAdAg0zDMAsdRrdCs43BwwKpatzJG
c+8z+4NLgG13Oqg5Mb9VY2gljPdIHUkpm3wd6qLH7zkIqRUcqaFnROcCd8k3tF/keEzA47ucRmnK
QB1ZsjnqEUIWNGIh4jQtQDMSOBucSMzQPIWRm/QGtDAt477gT3YgYZrCdXBeo3ksB/sf8Q/UPY+F
DEidqfuSH4YTuqdWrzUHvWkZk/JZcMFX3lQbAMeqfc4XyTr/Rr3fdDUok9hOyqu3kwT0ZMXa4YRq
YkB/tZpzXiChMXfEFImWlAmbp8LfS5xQuE4s5B4idP5axq02R13fkcjV2TY77HYydsfIuKJNr81y
+7NeSBhNqWhzns9j4X/xVxNeFdELHHFcsstJid/El80s3/AfWoH++/3H0REhiLYM59yHTskNDzpf
ZaIlGylN/VQVHVW2k84jBEIzW2PHjA+lVoMkAZIk2Z4JlqzLR55H4JQuQw4NHXo915MPgE4msbeg
OyXEzsBm954udnd3iyOOC2d4u72G+SV1i/ENqZ7AfJBlbkiWL9Ghhoz+rumWrpKAUkiqwUsz3aIi
NI1d1kRUjHWVJ90ku6DR0Smw7gVviWz+NUPNIuYwUO2eg+WV4VQxI6/R7llHks/9C2XhTeHMoMp0
eSiZUjh98x4NK01Zxp3A5APmFkSoA4wXoku2DZNbRWWOFK2Ef1SHTLFojFwp/aIOUpExJuk11RVx
gn7QG09w44AojM/NCJbV7aq3xxtTd7zyg4CjbHmxP69lzOrAM2O+UEblrX7PFDuBJJAc3Xzt5KqB
Hz2gmu6xP1ng4xSTspuCg+9qwKWRJu4dDWkejrsQX6ME6ZuZTAdbnW4H/hDwOTjazRGd4evUIuJw
cJpGVQYrIvz04rERKVG/2kqaUmQLMhI6g+xsPTNi3Ay37fDCs4N7WG2oYA8dDC1Ye0noX9zdt0+3
iboWQ5tw0tn2Tpx4f/tqVmAJrp2uBdPjVRJNIU8IcL9uU2X8vt7p4Oo0J3SwEx5pWivuxAo4T0MK
TobwWEdEkBUBjOm57OaYHUH2gvXuI2+xHajUKaxIJRZW44RT0AUwjmvk0sRQO8JdCFLa5aUNuLGi
cvV3yoG9yP161E8RZv1Z6BC5QoUJtGUFHjnk8SYJXmU//IuHefHrDGkK/6ePOO0g4OeG7P/5XGPh
CyztmDRs4vT4QrDl0BeTRmcdxpnb9uk1mE2QhYZrMs2p9adIRrXxuXDa2VtglGCRyMxIw1gySFiJ
yCHTjD+1gRvgBP3hHFM9dy7krLKP3KaVpNhsEQX5VxK0tm84T+eqE/KURcy4BRh6YjcTsstr5z6W
GBewVcdqoYExgWdzifqOfKxFWPjiqLLfGqX2Pe+tPNFtgnGzKpifNE5OldZ0FsA6gyCrqkVLSNcV
2/e/NSw1hdUuiO5vUCOsZkYRTy4WWuUsiR2/m7gjRfFjul8YyjwvuDiRIcoKkIK5uv/zQ5sW7UTZ
w/Hrivkd6bJZ0LRyv1xeT9MT5csfPTZN06glvZSUFnqexryWVqrWqoUCyfFlEOAfYKa+AATlfTba
W9x6dZaOpJvkbH/TG3AC6dk5oMv+XYwcfxu8eEO3L5PUcC/9sPHmE1u8cJhk3phwIX1T6cE+Jb45
iCFH/walXkBRUrEYVOVu9zbprSiur4d7C8nrvOB/DI0TWTURA/XqW4SrSA17rw0hhjraGlBEUUXE
qkrXU02qqbpHtJZxeyOOz7KfiFtgH1h8WrpRasPiAyAYyrLdeJ3osdwiY61eGHyUz5XyMgqmwsap
38hsbPz9F945rIuLQ1tnSCoL+9QydPNHACjgCCAM+eV/W5XR1MltQ2Gc0bDQS37lLMDuU+5My5KA
t/EGhB9hgPoDYt2Z9py1LbnCocf4HHoZZAQaamnduvJS/rYjIsIv/xtdoL8NRz3xmlIW94s0piAA
bICwdl31DTVCDqqgtGGVA54bh+/+3klQhmnCnv8kfUvg6ZC0e+GaHwAE3gxdiV1crlVZqE/ZPzer
/RrLFMqromOqehwbj/niM11hP50XZ2AzC3HFLFI3Oeb018Fg/dRw2asS7kXF4AjjDcmQSGO8v3ii
3qIQCaDOCD8BVgqbUUKn5iHuQigMfAusDlP42mZEmA7YO3p2pD1LUGsatb+D94zH6lCT2iNYCE7E
B49/oA+/YKYkNeg90NBX3nEMe5DscHLgfIh+iErSTLX9hPSwmu9b08Z5mm0u50JIdOheEnEppyfn
sNS7Y+patbC7cVb5LXiaoy3Kdbu94gyWincc4srecMGZ92xEB6sVOziWaOSQU4FhWam4thRFX66E
bzfy/jDHnY0W8QTlpz0M6g8qMhdRfOsjBkwFMY0zypiG5dhpEOyN9OenSS3GOC1q0qMp7Ba5Lq/4
tyUUnaKM98yS3Of8VIb74QiuUnKf5ygHnKJAE5uqZVwb7yZ7H+rlkIhhSHPOvpl/WHuSVeJmY4DN
5dgvyl5uQrYchEPUh7bdV9ZxCBrVye0iDofazndQMnIVAC884wvc3mfVl8NvvnZlXcCltvRGoFtU
C2JjCf8e9vPLQaL1Y4Z4IqvDerE/fBVfd2yXtIk44RBrD8UeL4zEoDuxGmyRzrzx1R5Zo++YCa3c
8csoQNY56n6SEGB1bojXbZygbpqcZ86gjjK8eB7gcrsrQ1xvKwVKnMIhZYvFXwAawdkE55Vfkd6S
2quIvqiBAxrK9eCddZSo7RClWVW98J5zMDiRbyzoNxgsIrfaTRvRp+0MJn+vxLpMGkYAF0bXeYW9
rkVwHZ3dipdpGuMCtwv01yxdZ82bYutOoeK8sqIpYFWR+gP8SaJ/XbV+7pw5eSH+jgLfF2ev3lvy
r5TCCcIneRI+oRSv02TJl+0rhgtCO6MrfKGuS7+ioOP5YmJaqrte5T0yYgINemlyx2RmTIMzbhif
0hYjDtpV4AsW0PUUhU9oobe27ntwI8rgLuu1DRBubsbOf3e4nPegZfWGM/4lTVDwljPQYMVRNf3f
bj4Eyot3CJlu+WOA8HQzolsZyNOaNFrtGfF8lbJwzspUX3Z/LkyFX53oE21gWnC5wxj6MNeztnRa
TJ0LyW/4a9xbcgYi4wfNlVw913JqGbMn5NUChHS9HxwpMaQ679GrAZS4tyV4NPkAVgHl6casWuHK
HBWizL6v0/piPL2bYTb7mpF40pPntWXMZYO8NMoDLenbEmEQnoxf5WkqXnGn1qOTtoEfRv8yu3cf
bd6pFbcss5N3GEFbmK66NQnZoQVkQ5Kxct0ddSLFnztVR/BTkaWesK8G4IIBMhDJK1lO1ueLiD/z
IZjzENE9bc2fRrmX98Dr/l8SRSGE/6kN+89rS8jAUh5IETchthA8ceS/9KQPiqNTkuSHb/UI6VXC
2SqcXzywYmR6ZEzEoFUvcM/zXX6jlgOSnm/xQIybgAEZGCDery8gfMqXmOloWmtyDPXUOEjZkE7/
fkQt7jQqGpCSJtFokjsVAISP8xU4RZEuCikW/ALyjBAdABIdfWu7/FaUliN6iF3/NIhinvB5Vviy
cjmR1k9+LU6Vb6drGIoUaJijUKkYLJIdvWy1GNasEm2Ik3hGk2VbM8Vl1d9diLfs9sYFitLrr4eS
hUr8pqLRXJiQqfG5GGVm0C6gLYGz7Fb0lp32DY4Cy7O0bceCTCeJUrLiZPpTweIxIdu1bti4Noxb
2HpFED/CExzCaD6wpDlS3su0VpdTJsQM1lHLfIRvflFABCLfCHTmX5+sowptIoiXpH59mnHl91Fr
JbbpBdJiHHNSe8TNv8EDLpcEG6UUBA2Q5maHXhX8TCmcWJSnA4iwFOezTbAnqeIrOGTgiI/yC+Ob
Atb9vFb8oW8YkfjuV48rz2ElPQYilZYO42JG1KSJMJYAb/4q1z/1SUf7aHIfrdStM8wJb+IWPe/C
1ogBKxpoTdLy/KTSGBaaxQUNT4sPtYP+MKKa84WzVVxgo+Ysr2EwxPLMb3H7TXjSgKqPTp+YjUYV
qxKnQgIIyaorpuxZ/xmcKJGqfejKO1CMZc+wDMyeqzHI++EqsUifJLgHVEU/mESJwYLU6HaF5Kyn
H5YOlmPbl4z3QMsxlz28YJ85FciU8m1uDUPOU55dPd03wWhlfZrGfgh1aq/u9qZpSphtyskw7hEK
I7TW/Pw4TXcPwb1stgQswSQcOUsD6pjxJQiLknW9PtYyebX3/cf2gjt0grtDE4AuvkS0DoZvikdM
J5g+hxDM+QozMImLLjUw1q0kLELnuOcCIAG7chvyiDRm5yHV4DHM78v957tJZ9aZmt680hCqOaZb
hEM5i/wsy2MCO7BVU8lhknrg3UvfHZCJ1g3ppAfWsBRxGOjNUjYk4F+DDtfZjxCabM3Y7w31M/uA
jybFFLuVqmPcR9/QH9q3e81FEtRPx8wo4LGsY3GxdNr7qL1FCSEjTCIHIRiQ0PEDWtP/cM28hLRJ
ZQ9gWBYOtwtJM7dIZz7nwEojP9G5LWDby9UbWCyuu+RRRtg7SRCocgQmLUGrXGQAaAxuZa8GlSK2
0AyryJqh/R5ajcWiV/wzi8+WhVqmXVGLKx3UTVeClp0iibAG1c5xiF28v55hZ1e98RsO15rgpkXV
DUKK5tzDLq2vwVp6XUwKXw7jbTstP+pBjuAjrbIWW6mDp0Vf6flKQAPXiXNhC0edoHazx7mr/RLi
EUgZrj+huuRYAgstBckIkjwTP0yv8jSJmXjYj1dlrglZ5gRUnExVXdeu8g7RuO6JokmZ3xmaI8+7
ktcHp06zdLF3zHIGoBb8lLcHMORl4VOLxm2SunBexqjrweIUsZ6hV9HHkl8duIPgwftMDEg5lSvt
GBFTVVD2k34XsRW5MkqeZJrMUNQfJa7rwRfkNdC+n7IuNItz6xYmCj1N2oUKs9ihLTzZcrWvlp9W
d4PGsPPLBuB4kpauaIcLlwH3O7vElTsQ64R5YQ5GpG5S51o3KIcFrr/p/ZiG2/fxKMx/S8uFyBcz
QpjhPkNZHok6zyWHuyn7vLiqE6AfLPUUQpqjTEwrjcRHQs9DwIJgnL9fiuZE73djIEZ9lL7HcbVb
osIgDXn3hPLqxCyXMTEiW2KuoqX87RlyEXe1jRXNwRF9QJzgLWKOrPDf0hCr8psWW/vemHwBYIiA
TGCRlttKqGchKTQmPeOUpMmo1QfomEA4AnjzCKfh3PxBWXpBtbPO939rS8l6B0F75LbLfQTvR2Ny
SwSYOsmLZC7pXzgujT/UBHycAyjRNDTT7EGt3kwcoUOlVJkB1Hpz49B6dn6AHT7ngk9rJ0B6ZOSf
RlkbxmuomjAT7uJv4SfXOmfATrQt3jsfTHaFLMjL0NtqVeG3+Xtt67Xg0Lk02L9RtOHdXvwscz3z
jDkF7iJccW8mj43lQNT8cPErlnopfjjd14ibOizvZaDE10VFiXZYOmlgHlY82eB/qo7W6NE5sxk7
27X5IfMLdoZwnmxSwgiMZic3xxZ5PvL0c+TiVfxyyw6StIbEL32LRAlCHUq1inwNBJ9Ah5Xoq1vU
X8H1nxYlsPitp9+HVIhAdfCQs5dsRSI9+o1ujx0HqlphgrVpRGI0zMQ9s/Am1YAmpmBbNjnN82XU
6g5oMlOO6zAMJgXhP1F75gYFYLWUqiXdhlJg3vXeNJFfSNJUzwrfnibjxKZODhxVHjqq1QDdKOiW
llqWPRwrBu95DGZfScPqTevSOuZFVFfthzrH1TnZbPMzHePifwhoRUJe0BHRMzqIKHKhTfKeBLUT
JVF3Dm+Vrn+ptsZDuGe/nDmkuP853IQocVvv8lCA0m/G/dLbehm6b+6Jlxs+2KP3Z0t71UjKoSYl
7/7zRzT0GIy0SQzsya/UMvcMLFwXV4s2i4UCOLjFIRgYffoYl0YaVZ2nl+O0wVQ2KxWOpQ/9bSEU
pDqwg8vjhGu+k2cHeUKbI4FJw8m95VybW10gXoV1dwvkV5BBnXwLv6PChLRdJSrjChAQpLeZjGSh
cZjPm1qU3LLCf9s6Xif6m5dWMO+ZEBenjBlcwAoA3fhv4fKV030q52bZ0pi+0YOA8x5KC72hn5+l
iyJBRvtsdpWAEPWXZeacXy/QWHsuWjc6niwY68K3pU1Rt5m8xEDIGxuLwxsikw8b5G3ulYseQCcH
x3QGsFcz+aonB0+HU++hwi7vqexIX/al2SLo9412p7wnFYtr/GW/7FO3wQhWNQtupSsi+8i2iOV3
9WGTDDD/gAVVuDhERuidnYC6Oe9v8YDml3VdPPCDHihUJFHHKf7cOXWERxRrP7x5XynkRQd44B8f
wGNbYzwsovWth6DOl/cMH0KgkX36x4WknJKCnvsZOx7k0/xgQgZOAkdHCtzGrCfLy9/oeOs2/PxY
AmAsz9OtVvkG6/71YH0vs3UsUoyoeNdxTXCGC88R36tFZ0XWGgVICR3GTtMgh9iqUyFfo+Vh2htP
U8dWda0myjx0waYdH6eoAfZ/2di7fjdd4hFqMCNLbbqNbk3ZLsYVclUEwzGi55AVTJMp2NuzvHIQ
eKam62xvs90dSBcTP7aotbQsVgtFVV/20jQOwZvIaqyNii1l1Vn8Ch9PGoH9I3VmZKhHBV/12tCB
/sMIA7PoLaS+57hhIJN6lB/fRNX5weiCHUwpbF8fgTEkJwzWXsSHiRRrnXvAT2Eltgjn18fG4uWW
LXpM9LxdM1AtuHUQr2W121bBzxJDNRuezhnhT+F2nN/Om9Hvk3ptjWMnrWN7r11l4xhukZQSTMsv
YcOTqh1Hdbf4jtV6hpXmwxNcyRHw49CECC6camR4eFRAEvrWiaDUfHJJFNUwk0x30Kcesx8O/peE
tgNVKA9FUyfPJS6X04wsFsXVTc3+P9fU0e7EBjSOnR2FikUYfABcZtV8esL/AoP3yUFb80ExaHhs
t/yAvOtPFQq41DM9T/E9aCfh0eky//cfLkX1W5TY1kpCyCih1QU8AkA7t/p8VeRDLm63YxsefCbG
1iRtIVh8c4zosQKX3Aw89fkbai7up/2cJnpskaDjIEC1SpRQip1JmKbzilyrBFxLWALRpszMeWWA
FKbNwmklTj4mrzIpLlsbsg5ANAM7vlHp0pnw34gZ1ZsaABoAunDLZsszuXe0W0njUM/5MJrEuobe
Dr9GT0aHhGuejrPUAUuw7vlNBupPPiL3zZzHOiL/K6qGkNwH5ioyNWdSUZPfaQRIx93O9qR0bIMj
Y/nRr6H1s76jhwKzFBfYphJEP8J3W/i14yhydpjQli5c6XqVuO4YhGVZxwMQYvMsLJDKZ9jx7kg0
Ca1e6ownKJ0rGvQthi5rNlyQYM0Q/nSASryvhLF+VcTyThxYrfMEcxCsQhLck0QXao/La7k0pQPn
rr30q+Xzofc9Da9AE8fkVHAgKzYcjJztK+4iWCHSMlxvrqSMPgfy0eayhWxRhVeLCFyWc8fFyxks
SJZdCUvRHHzJCWmGA9z3zKwpiGG/Tw+LC/w147pqP07Dv1vxcZXlc14s1mpV1G6KH0+O4wv3yVnA
p+bp8OlJ2ylEy9htQvvdZN82v1jdshrOTq5RnIzN78qcyF5ljFQBvjM/lFjVG8rHtKz50sQ2K4ii
4BZn/6iVnr82XwwbVOTKtlsvPUKztif4m7Dq3PIWKLyr/Ty0jiuxckkzwHR+3Tv7qpMnHKY2F5E9
5tqVxKeTzrbHbfpMzfVUYUQkPzoVrD6k6PyMOhP86pTmbJCEMJQ9EWIeKP+gWFQp7tIHKz+2qgeU
NDYmP5h87D3tMn2UjWhW0naaPrsXNPweBe8JnzWVX/gULK2DpWZhj8Ol7WXHmNzS0PtLWRE/VSyw
2SSduxw34kWwpL8d1gmtPEi3zNEl7EJSqWquYx26BYbRMNEDLOPd8u8YC68wUi814aMy//IvoTTy
gWmLgsnWKBdXlE/RAYJbAK/aOVuI3s3PGpAEPeXrVq/o6tRnOWHVfz+ujuQfUxTOzY5qxmLMSVVp
JcbPm81GIvrX+Fvs/mgR4FqlYagaDFbd3+S3BS5gf+uYA74BdKTh3snK36ZDHfeO4EHjt5U9VkAY
beGfHUGJz1AevICQw1zUdNjVRJSaI+xIGBUVDqv5HeXXsm8Gq8yu2sgEfARM5bQ8rvdkFT/thmiE
YeIZJJbxcQFOnVdBngE9apgBlsj5nEUdy7o52lW+dVYDmOlIxMJ8xuMe5ADCSYjZkVl/io/ItOH5
20J6/bHYB5QEMP+6e+P5fmsU3pfYUDQWlrjr76XA5UiS+4yCPqvJStqpUB8lY6LJgbSxep9L8tFA
lVBwCsvoIetA2q7QunIA/EnIqFiO42DofSO7wPQb8nsRYZd0RZVWnfzajrFXp3X+TQukf9TL4jZF
2wN3m2XIj2nzGnbqWoPh3ovTFpHp1tfnicB5akDdWBhIH54iWXjCAPM3tPpbVZgzmr90zOVaF2Sa
Y4YPY5+sCN4mBzknAcBa/kmzBMC+/BQOZHdlKtkQf3HPyUp679tom+KQu5ed65Lk6i4YEUpo0tHg
w9FlO0L09wbIA1dqUFaXxEhYdAkC1J/2Jf+pYPWRoF7dw0gqiMb9jUWmgpjY1g+92hbL5GXYVaxs
M8Km3W1IOni/lyLWM1xrMvnx2hB0475dJ3CxO+XMgqYNAl1w0VKgxhWEPuLHF5FZaFYQvClPig4K
WXXhXdnPrhNBjlkEWGkao3lh1XkJOAaT9GFtuDe80dRyt+2g53tiAAGzfds7k/advEQVEanB/+AO
5GRQsiGAsEJc3y66ZKio49l7hvOt6gmLTDMgoSv7NBiFJq92LcHLr+/4lCVqVQO1a1xf9sOIzsFo
W3xerDV79SiLF0l/gEazoh5gb4USEH9vou7ANylobZgEer6eIT+r7WqbQug6v6LNKaO93XDgAI9z
cBZXbOUyaFgYgUFShRhNZISWGmhbYtPx42n00HFaY3SNu48HWtagVD9KG+NkOkw5YfZxae7lIwTV
Qr4GbLhIgLKSAXFBZUFCtNzB1sik+7dpthkdx5hxQ2mSiEv0cOQdw5qXdlgUQpfZLDHJF4/+AGpC
2tJiuOjE2DwCfKFWm5A+UKjsAfvir/PpXHd08zeLgjTU+i/Kd/B7jEe8LbP1icRtLiPipSMwimzI
0HhpNlG/njdg57Sww8sEkYegR/V+OKd3QgPDTsxyPg6vus+T13g/MSnZv1YoCV4Z8A29yVHK4NgU
7dVXLtOS+oirOMbPSA+ZUyQ5pfs4l2PqbhR17Z6p4BM3b0Pz45cXlamwwRj3rzPIdBMvxz3Bw85h
wgdbnTAYEqvw2T8V1z1EHSIu80RfY6sOKZH6HnIATt/Uehozpx/LUmijo2Nc9wuJwWJ0MkRxPaen
6H87JgbBmGu79qLajd4VNIcBaOgbNexogCNqF1HmiHBM+gpQv1+/+zN+qfLefOApkcz/6PrQjL9+
Ug0Q/vE7mxrtWctWJaFyMe/YEt31xy6SE7rLicOleqbBqd03u99fj1RqnUP4jRzdToH9sIojUJuk
LIGxMZ0POaoEH8bzrFikYGN1puHfE8PKcjz86zDbHrhtfTu6aTzIfxzr3dFGbeuDhaEQ4o448bt6
A/Cvc3Tu+d12k489C50FVoooPa9XwXEbLP4vyjX1BlFmLCCf7/98xNLuHB50LgDEbNnwLLdNNDC1
Yrw945sW72jlEgNO2FCPVGep5l9h6kfWMINZujd8Tl45I4w/V+vF2kcDG1PH0NH5Qi2DVLwYQXJn
KqMTvm8873aMgK7a59X2h9T28IAGwpo2ElGL4U/3+oh7lng1WW6WDrGVsT/QluKHTnFU+5Mt9Okp
63ZNUWfuc476UUWM7WBs4YehPVcxrXGwT1Efuzq6g2WBhR5CqXCdWL+8Een53riavdqGfXOLETPt
cNGHDgbPAbEjiEcVE/+eOIEjRHP3Atu+cb1iZk5O/1Aq3GZR4KXszyK5KveFlS/IWnQioGHFT/pA
B08L9jvb12icbqDfq+0/oMqcmcBxlwJoIPhFRZT0tLIblesapndTlLAzb10+zeeb6Kl2i5nMKhmc
Dwl5fz/iWe7lzKtKKLZcNpkr0F8N1GhYlK+Lkr09m7lfBb7AGNaJW/ZqVd9ranqjs60Us5amNYbp
q7XekBXHxnJiIhEs2oPoukYzGZat6Omu2AFWOM3rMoQy/bNQKQ6zBPUpEgQULMo5d4o35hb6EiKF
7eeosG/LXr289tRySDMEQGvrCuewvZC7xFNm+BA6RwvgiOen7xmIQ2vAP6q3znG1p3TqWSfcr5AR
tS/hJbbZ023T6DzIDUTygvVrMQdKFyseRSnx6Wh/fYyQb9kejBlxkHKHp/jQc08iqgQp6WYqwPSD
6V6RUQJ2eLqEbgSxTTLIElKP5U4zG0JG82LEhX3hwA05PxQc74H2oSDM8l05gSLk0lanKDYJJIwE
qLI0q66Y8a0qjpIYTfCC/2QjVscVjRk/akElKAAY4l0gyeT5T+NkV8S0tWxop53oIUC0w5h1votD
ezqegWqejnc+P0ozA6VR1JAXGMEZU+WXDrtOAsnc8d6mLWpYn0CMTaPPlh7OMrguyM8vR1mOfXs2
2e1Webg9YaFAxonLm8QwvtCgWpIptsmx3+cvyWL9o2f8NPFXvYGLl5+EzyPEnStdMBft9dh2lmKb
0rhZ6ZRdWbEy7lmPAWcBZvF27/fAyfWoa2xqk/WGZ659ep16HJ6JZ8REUCbfruiPX6fxJJ9mzW1W
mVugReEoA2F7CL1uR2FwZV314pwmBRxRB35g4K78k2r8kPA/lcxfvcx5xEj6bX8YcNa6UhiQkstG
r1mGEIsnkKYEm9aHgBp078/TeiQuU56+YTIQwYWTOn5EHdirx73I0cxtKQjwvnGz6FG9Q4hwlbfD
uVfYeCPGcHN4BKQRx4LXCsa9K7GUCwL1RiBIyZHR4xRlQZRqq6NGlby+jRbBCyyi7khov/HOX+QU
3ymfOFl8aagLF3PMUskC5Sqx0qlHdQMb09MKm50Q3cK6tM9LV4vcLNY0laOXWxTyqHH65ZjnrNSp
iPpBlAxg5Fs5w5KPuABVZ+MM4XEOqObcnitRlKPSs5GXRl3G1ohU5rQBk+iJIfZ5chsUWNyKC0s2
nrNiogjnB9XF/KiRpPt+Fwj2FJnuYOdiugXns7jttQH+yQSRJBz95S16IrkrG3BwCUuNDAF/N8iG
QCIldzJYR1U3m7i3/luI4/BpAqkFv4ZoURqRBl6Ln+qGewgqHkI1HssOScJW19bmtMzpUu+aVvy7
y0d4Lhzjx5L+Z1bEkkmMffYY2eu7Z6lQ9xVW52Kl2A3w2hQtBQL4kjR2mjP5wW1yvsGcwTeyJwlH
oxTeUquwpvlarJcb5E6yDkkQD83oKXf5YSndn70BOeGDKU/zc9z8R5AaSshgRG7VW2SGQVZmIUDa
VtriVXtHeXNPgR93nsSBwK2bhj8L7fCIMNRgugmpt6NZFT300mjSyKHKX2N9Y1A+ta4QjyWujrD6
QhPzi1zhNxv9SyDFLM0TP0OYSgpesWT0rbsaCEKoLkAWr4FlcSI7UuvJD0lXWFXq+aqnYJv41nbA
cwPIMuwZrWXyoRaJP9klxYGoi2emRjENHlz6Ymc0iGlz4lCci9zYljS83ugQJtHXGP9043OIiptl
4X0cS5335I4ARRwAYWYQPxp2QA+/b9+rhvpu9Nzgx6mvD/B3PZZ1sQFwdPeyUXthljZpFbUnZjwI
k6/x+H14CIH3bz4dyKnUPrfo4B98flRhMOM+KeRAmDhs/SmwZ0Df3imVac/N9oCRwvwrATJwvpFx
MzsJ8Y6JIRODH/rI8DASvc9k9sHRl9L2c34d3rVX/QdZkcfMVXT5YxVbcWLeBGooO290px1TyFjW
xd44vsE1PJB020bTgUZ+c2CthKDLJNvP+RueVLutn1bYqK31mINDAFdqSqYdXYFzsCkEqV9Cz0QX
ONftHh6vwZDopntZOBUt9W97KvsTO7brb6aKokyXbL2PYWsMCCCGjs7vxQ+GZCG2NvBQoradOVOa
EK51ipFUFsQnQCSFJSmwDtscRSaBJlUgqjb6+YDYlpilY2nccBKj5hdS+QlRYRZQCr28Z/21CHNy
v4VoZGn0dH33FJXrNsFNpEDD9yQ/z+AvA2VgfEDp8FK3RuogoYRatuQR8rlYUP6F09b4E0wipG+G
LqdZZaB+w6kFJDqGBD+aLP9PIu32din7srRbSqndurj1oLYz+0lXnGACprDS5RDFoSTn5PtKat7J
kTq22b+XLRcOKt/WubuKk643GN0ZTgNdUGhcPnCzDenHCk1611+UOuHp9K/oxe2DA37BAvVqkQLF
jnDU8M3wrQCvKB9GV5nb70l1QPp9eW6pS8I4uJmcBsgSWhWVGgj4h46IV/43a4Mmre8gU3gC3fYb
egaz4ytoyifVdM8ZnvBcARyjgO6mV2VJ4ywfxnR5LwhI2OGr+4m2E3SXSYLLP0XTKmkyeGDqq6pX
CE+Tg4i2VzNWPUnxXnJ0cBQBFvpkYo2wRpKYXWnffxMuK8YygUN+WTncJST9+UlqUL2NPiXgaxbt
zXgfhkzfWKFT3eksUnBUw977I40qpGXt2yTqOPp2bTuXvKOdl7DxeaNvCvkx5fSUrKnn+08Wv7XX
QZJng7yXlhUUM/RazZR06RpjtHCFC7q8j+4h7xxdROj4/PXzkKSz1J0+q5CP1hYqtGbsliW3FvjL
Bru248H30u0NOLYwvbtT/yy+Ww5KREMYQQW5NrX8VPKrgRCRGQcQrYtFBInKrskAuM+8SW5h3McV
a06U2DaatjIL6cAnkEa0p/158FjBa+3FGDBWNHiPb18AL/qATZHiGyP1poIyUBQ5lEyhXZdAkF4m
PRQK+mWX3PWBQ9PowBqQ5zn4rAVOGUmNG9hNFN1gKiQjsQLqK/MSNnsTF91XyCPLwZZuuSBmcr3j
RIENNUIf84Z04PJK7ipS0+DJfHGXh+33vYqAWelBRzTDO1bVM7CR45EM6aQMBNCKZ/w7NrqVLwli
8d5MEITKy9fiQKQxuqF8owKNKA8x/hQqr2HD0DIHqsnyof981kAA8y9Omu2TkgY5b/wvmSvVjv0W
Wzm4bsc7j56dlmw6CFKxSwOOzQ1PUPtTtzT/+5tdikwGknDCcL4b5vXFF/DCzhLLcklewZ7VOOGD
eR3jbaAXiK0ZPJHfATReiWtF3BJ+If9jZal3q7wvfb1b+Ra/aKeV7Ch/wEnlQ0TVfxzBfdgllq9u
Dcpwa1pqp3wUTAbpKNJPIh/NaU7cFpbia4KCFFGX5ki64VaOMQRRtidI2F/F5IId2POioYn3hQkz
ZEEvA4GTZcrVZIMgcA93YBu0QOVWLs0OdpEi/eIQN6lej+koEaMQ8Vsw74Z3P3hYivhvL9pbmybn
OETEFKumaFTH/tTUhRNYCjpa3b+DCExFFRu3utTQzx6JlZaVlLGGQKtWeM3nfCQRiroJokDF2ykL
8DNQYN6JeHqvHZaq9L7BbrhlpZjnIV4fg9eBRcssGhtk25VpEMMTnzEkPX41CPfs3uLsthG+4vvD
2lxWqs8OJoB5P7FbWDmLgSWzORovHIOix5IrM+ENRieO8Ro3ijuzyCIfgZc2+6Zc9fcW+J1FmLpo
nAb6GEQ5LYtoonmZt5eDoYKuN/GgI0XZc/k5ysqElhecVRvURxAEa4/Rx+zqzpJ4A75c43eyROk2
R3tSvCOIjsAATfZu9Naxk+oELhxcNTYI5mbKf6liiQDXitoPP9sr61aJFsjxbyfjky7mYfzmElAh
yS9LmRLqYFZJeCyGxhtCjDqbc2G7aeZSsrciRHtsQEVx2GBUUd2+JCEIu5/T2CATzjNLKxuDRdAO
zOWbmWJhK0X6f4wo0Ek2vPoAC8/Ao21owE+W1L9iRslJfI8/hqiaXIZAF/gRLf3ocCPvz/8F2S6f
Z2yQeAEKximzzPsahNG9Rh/DGUwD+HG8EgAv6F05zzWbxDPPIkJF9CnETman8X4aklDhybvyfOE6
LpPrGklDrKcmBILhq9wrQFlB6ZMYr9bht7QOTnYcHm+HxnsnNX43pGeKFxsVX5X8MLH6OGVw5IwZ
kew2QyIG6Ta5o6tD8r0PtZkALu8wS3VVexrGvTQmHrjG0u5D23PrvD+36uIJyUvP8Oi+YC4SHa9u
6ecv4K5taf00uukgmuFPXrudDRctnNquQ7JAxoA3flowivoTqZTI6x34GECuYO2tc71Sc0z5KBw8
0thCpembJs/sICSc+9a48FRbdoe5l6oEAzZrEtX5PXry4CLENCPB5/yuvJl/d7WFTYuOU/LHa2ZO
6SaMO0KXo3vysXrVQ5IuxQnQXasZF7g9wem6o24zf+tzL3hbwMMmvSfm2JkH4cqsIgM4ysTqdm6L
vsyHKhUy12DxkVF8x+c7ml397yeMx5tcGjnI4upppYE1Ctfg/77KwV4w5n5fN0LZV7rk951GXeGw
iRuyL//58HjgPhpMyL3dCCSozj9LxbkAhc9Fv3CCWlrcA6cD+tTRthIL962j6f4ZR7wQLi6FRoue
kRijY5y6BOmFYPPJ1OK3f4Auan/7TmZV5cXehHQgYKb0kDLfW6LnOUTLIinYIzly3ZVLa3TwYof/
+J2ymtiGPd8KO5d51mucFuV837wPcfABhis/7motkzYj1YhNQkV7mzo7e5geUg8HPsgulmLzH4pL
Mox2aEbUtvySxZ3IRIy6wOJAj6swhByuRUE33YYa5qeAlJeWAWRr7dQjfgFgVeZY72xrFQEfsnyl
yoSwNjsVpTS4pou7/QpvUc+EblD7vGFYCh1QQss3FkXkLNWjssua4s3F2JNMFuC8qFlIl60i1VXF
RWIfmyKefacw2EKbjLSsW0eu+VD8/ZXltn83myRsJwGCLOn/E1+ZNecgS7+JF33Hy/so7/SvMSeC
Tjv6M5kEJVTRv+e+mztbyDwLDbw4M18AbNWlUt0dB/WDWTS81VDSe6LTNDUiJiZRPSTYyU11XIt6
m1LQIJLA5JWngMUGIJ9HWLFBqtk7UKHmTHZ7drpZKjIX5ksycuB9Kv0r9bFvOUw6pRk0/3Vl+/TU
eai79Eq60hRpXA71Ya8fFGJqst8idL5KOQ+b9u8nIevlHJrZjE2Rlty82KJ7tEoa3lJJHUhUhR3Y
0lDoH8TehKg+QlCUcGuY+Wm9BupHhAqAq+LYmkoN7ZvGe4FT+Ynv9r39Kkgm49NptuvQl5mdnGGo
uzAcHF+vq2PHv32ux1c45dQu2ucOkkUE4LGvinElqHHOma/C0bhwSDp/el5mjDGqrsA0FnFvjW6s
PmbehGG+8XPJfFp4hruqWBxYeun4zSGnklgtw0YesFX6cR3QldA2Yb703NlxcPrX9jK9ZGLwKV8q
hnmR5OC7ASIcVZRLlhzz0QSyl8FMrQ9BdR54yo3uACnQQ9iZZoHMqBv7WppMdC7SGtS8g0DRMVbC
Vu9AquBC8sXM1TzvJqHJfrKlzGMfZXJpUwRVnU0qYOkFrlAoqBq0RwxjQhMT0NWNkhylRXaVXc6L
7BrzlrGAbW1IZyiyhsVWd7K8mDME9ePnyypnGeYCtYRS2l9wf7fN3hVAdXRtzUzneFbAOnktCwDS
Q38we4yHLnC7cl3PNmJffGUH8S/V8o2pS4ydprlqTHo8cOrKFn9a7DN+/P8ZWNRESDM7Dd6JTgBU
itLyAK3L5CgovopaCsk+QYkZMvfFMwjYYV1N8/5r4FG9cixeFMQNwuBfu2p3/3POAdBRrSUDFzZF
IpXqmmzPZbzcDMFExJudhPfL0awDz5fO6e4KeaNexmdnbKFCcGzsp2YbvTHXW1tmzkXEug5uGQ+J
MfztFV3kD3VMwc5SeMFbH0rJMqfiCUkXrH1EelvtQ/BzHOfcTOckWUOJaF3GM7U0rkXhhfJ8U+p9
FhQumxgOXFlXIFc3o3TSL//XtqDAMaiuncx5nhCGXWcOYDkAvWBJ9mMUmyaBNuJww/dlz68LIUyM
bVo6NKNmfIlMkruGPOPHxwvXnla7ur0Ic772rEbrUcU/M6dslxz5gRiaxgtKLZQR1fwNpYkWk0Rg
9FLNXVUD3WEpc9ki1JrcAYRTAk09ILBGbxdBUs6WJGAhh3un64hEHFR2URVNlLPxdVyI2FPDCWnx
eOGozylO5onyJZIP/jooONcRiVmtTlc8aUED/dIQlgeqD6eXD3qi0mU2JkPxSOqS5tY3hR0TojfW
LqdtGjdS4Na4N6+q2SAuLxYArYpF+dGFzzNafGhrmkK+w5LCkl3owBpfr16IKFPoY2ylGyAsc1/K
jzcAQHYgiiavjSo3a88SGq3oh1ILFWlYILf/QykxBr+AEaXdRnk8DBRL3/pzlI+BRL+A0XGDDADp
ndHc+pnD7QNZpq50Nr5H0lZtTB8kvmeHZ+5W0MpVUA+ZNaPJE2wTMoV0tupLZs4LoiZ4L+pAwqq4
PQFYwe6PnPnoRmayTzfQucs951ujaxC5yCfdXbXMJfZA93+hXuLRefcpTiPOdaX7qFvIJUnh36ev
88nognyUr9ciKM9qaOiMmCPeu9RbSTxRuLcR+iQzh9MKw6omFz3JxQ8Ex21MywuKBpQR+vCmIyW/
4edIsNGUoc+dZGA0/mnRs5ePyHXovHG7AJJvaVXECkKqrtPAbnpuvRTH3y9WPGjFU1mtUKLm4s4h
BbhH1CnjrURHEaeEgy8DPB2/k3jSNBy0jZdZnrzQsYd2iSyONIPwQc6v7z3PiWWVK8WbqMsWD+Th
l+0I8QFaj1/obTE1Gp9Kl4nkf08Z+zy2f+7YZ3MzVFay4apfE2Q9owSdzhdQ9d04c5LHnnB3OWc/
B5ekGo5qOIldKBH3ay/Kmth0+LxOnR0uTldZ3gB4z/C4DZvWCv0D2EvRg+bDHw0asl2ap57OtAOE
fSOLBRztcu22E6Vp74Ab6wXy4xR7fouCXF0GKAl9skOHdRFWk8dSGD8J5Orseyhb7Lg80J2LLE14
hCCRJt2t5kR/w2lR7ZGgaXPvOQvXLV5LDG4GeI/udBxxWdCx5LbooiVw0H4DvH5XORVzd0scwwaN
58GKfgtbxqm4Cn9arTK8y2RLapWPJb12VMRlKF86WCO2wJz5Ew9cgobTBscN3uWMTsXtP9ES98Sy
QQxX5NZeyZeW4soTBjPiwBkhSxjHlbB8Xz69Cv8Z2pBz7ohcS8KdwGi2Bz22jGQCzYQjBCXqXdzD
ghBgZ7bfIsf9v0WcRlf7P6pT030wICP7R4un2Qb/DH/CdsLOwWiiihOFyqv/W4v8xiMCfW7o0F2D
caqth4JwlCW4p/rsIYpQhZ3qu8Zgj0dMcxXXxLnQfZAovDPuX3RgnGF5Y1iXvp8//iWODgmqfUrV
mmODyISpESoPnrwfErwdopxjx1+lYzDCf9obwHI5g/CQsAt/L9pklQqJ/isFE+djqYic8RbJbueI
eYHu/3/9+ZydVzeAMrGxxeuBaQxaHcsiY9sGaHu1FDI9mtsrdgMBv14lsftRxzd3jd86nAk0HpXy
q8eqhUDpTU9i+Yexg5waMWUnVywUpTeUvBdOtIro18Gt+gUMhbj2y2fAvx/aivXZ+5aw/JOnuSyh
EN2q3Tb2P77BpwLh9Z2XEd1/U54C1MpS7Za8/0EmcZo8CcIbm78VA/c7JhKGe95Rogft+GGHUgP1
fXxDMbHOGbOhmiGFy98TzP1NN1itU+yABI1oArjEkEzowHbyiHTj7QunIu/mwH9MszfdfZ1QTb2E
OLyys7Ew62WMtOy2HeE6HAcx1+j3IAgO8lVy2+YaAxrT489qUN1Cxq5AOqNghfncdiZM1eTqCZQz
hmtDaNfNU5ygn0lYojXrR1cPwNfACPj5ZSpv490d75qn1P8k7Xtg852bd9yBZX0iCgrWWKlY4Yyo
/k36WNSEhuuGJlMePmTRztcAn4mST2/Vi514Ztmvmf88eGDthSNT0dHO1o0UoQ8yv2rOwHaTDpbw
nk7AQYS/XSaFWj7WRr5GZTIhxBkIZVZ18JdvosfkK1N33jFvY3lkBmvNrWl1PyaQ8ZlY3Yj3HDcK
9F/ZcSbraN58OzAq5C7QTvwWxu20eksrFiClm0MkMCWKxB9AMxvFC+5YbrNSfD2HDnHw3DpkaJ3f
JkqvYpR0rU+puKxlI5E79JyqcznTl1QwTq/h7HBCBgJjK94YTh6t39rps+i6MgqpGZX9umyC/99Y
PFPkZ0mGE9KKFkUO+ft+bYYMMA4NuOvwFcHtuQv9I7ERqgXhWaeWjr4iwf2HuiwY8gmYPKnBHlH8
ofFte5DFG8ZiSXNYvMRI07HHUo3vTUZJ8MXfmPdnhVRnzx8a0ybGxUzvwGIXwbXnFP+fdbQqSCKj
IKMdavlPLhNrgU+AgSe2SKR+O1DtahBqRHuNx1zd6DSxk9qhk+YWZak0pVMfHpNh7mTcoFkI5pt7
Dbrd1f6/llMsJtB1GURx7drQvoFXuxuzTaMfbpqDfvjve13+yeiCYG0ZQdWlxWIpkM7IWtjVAia6
Up+w/bDaWEvr92aWiamndC1XEEDoiiEoeXTC27+Z/BbI/DYF6dLO2eE2k7/j1D/9WTAujoISkn17
1cjPQnai3ckPhVV7RtakhYdpl4ftpdfcHSubQzYKu4QFUiNbQ2r88KVqha0c4uCivhCWTLSmvBVf
c3cU8fkziHQJiDyr46xoG2M8wLHFbz3Ak+PAdxahJMAQPTDve5XsMg69R2V+chVSog7zWngQx+GA
o4sfMYQDiYkmRMbaknjhesjT2ocD8MnijtXbk2TYPLaJ+1Ktdi8pADFbtJ4Kd0SDUtRopGSJGA6G
ULfCCwJQmNAiMM7nHUaGH6iTrH++ITgB0CpaP7+bfBexi0QqYec2FAZS/3jbDoMeiDBFoScz2N3d
C1LB86iNH8vl5EMRHMM4u8LXy5ihlQIn/DFzkQ+B0xQa6RPFhbZaUQWTyiNUEIaDvIHeIg3jmy0V
LJEMe8chw8JreQ4KvxjqbvUigomj+Jkxnr/cqGA4vwWWXTmpKAzOIz/30XmV8jZDPOobFjD0Nnd5
zGw/okmv14053SSeI8U+BjO8MjKIKS8/cYE+fIBIqV4FPU2dNV7Z16XVtd+/b8P0USDGfiSSp2og
uHtuUtucT0qGSarmt/wZGoOJL54h8eaBo7b9vImnoA60rulKleXLoayLTVRnCo6MEgtuhhHs7rWJ
WH0wUl6kt0zY5KZxrKYahxxDtq1HoWtZ/AAQT2GDvHWtJp+Jra+b0/BKszfZfmvZVUhWyI9mswES
jhJ3w0RWt5Xj8m7k40/pbuZjb5JrT0hPgUAgQL4YcErl/WqTFynBw/B8itG/y3659wAD8QFvrQH0
4BgOrqmzn+DZvlGUrgb49hgf8Xibay5PZMtyLrgoWOi3i2iwklECnfpV1IhZu+gj37joMkb+cdWD
2yKglR4dCTHnvYN/kaVfLwg2zI4LdGHx0ym4nNjvTsSCgk0Mcov+gpn5+BjFz0FvIgVrhW14qJkO
nEzml4QaxKows5fs08Qec/yVj8Gi8FSxQ2NCsHjDJxYjskDb7k66HOncHadXLPIHGe8KJNrJ2p44
XTctyr00cX88ZOZ64oYU4XHK7F4pk0+Yi7iHzYhvnYm+T7MpytEVdF1im+kiaU7SEsnuUHV7Vf+Y
7nDU/EjUPVYTkEqtYL4ZZKzy8wrD64QueqwgwrlAJqXsNiY3IOo/cRSQ6onKfKD0Nimgjmboxr25
E3uSkIs6D1NEGqBE5fIbKM7T1Z/wgdLt3jogpgd8I3ydo2Q9Ajc+7Y+YlRpyCbDzZqg5XxjnWg9n
ua62XP1bseiEKKwQL8JS0V/ZutisujprVS+Rr1E038WnWRR0WpzjM61VTPJTrMpNQK9B1h1PeERq
3D/6zV8oIB+CVjNPECur2hlVmHYmq3YeNKaaVISYHvDrIt2wzSZtKEP4VSGIM3ujl+az3C3faY6P
J8ozKlqQyZ6XcaJn0AlqK7PAzMJS6qo3QKSn84CRSQ8Y67TOYD2jafT4fMjoiauxc0OZ+Urag+ZM
I3SFe/CFWoe2+4RIbzb24LJpgKcFRV9vjCiNdveTTFHLUtDJZUaUDbodeHLAuFtKEdiF853vifa+
M0ddWT6fiI9eVmGrnBzqHEJ/lK3lOg/qjXqyOf4cNgFIAhV5qLAsIepLOBNXmk/awAdmwNXipQGg
hO7wPZqt3sS8UmJkAdmpRGjlM+CFwLgNAFXiCP598WotY46+rgKc5ASNmuDpDtOnvdsc6pkC3gJO
v7/hnlY6kFHkTvB4A4fKmlBM8ByU56kSw3w33wNH0rxXo5A+uQ5A4u6kKaaMrja1pmb0NhswjSOT
P/kTGBL3r4474+FmeBIWUeJxHq0bbZ8PGCXU8jWZ0052tWpZGNF5RGnaf3ABIHaEZzzKLmsx8M1s
FFz1m40wHvZdYhzUXWRAtHU6MeD0ohptlOorsnI5tkPd7mhqzNL7fDkUgDtcd4ImGuGNCxsd7b55
HPR5rvwLTBy2Dkbp37K9MceCG76mZsSX7ZGnAJ7kSqGnwUKeZvJHF9mRfS0tRnkqOiWZWhD4EeFO
JuSFNDV/xmQOyJ2HaEDnmOs4PYjjT0lAvumIifJtWrg7eCixfSRZHlAbTwxXNhZMapsvxp+Lz/t6
cgE3DEkV8VOuSaRfwv+aVtdSnbp3j634jigePcHG+qUJ33g9oWBSYW5XFvQ3vI1i9IIK9vK2858Y
rfCSBbTxn4csOsHdSvVkSB55lyoO1OQRswbmW42aih9BdB5B8JsW82Yc+acF3jI7n7XfqcZ+zPZT
KvATZvZ06bGRTfFrWhIYth4SgyO+0fIn7gTZR/das6A6+TUV41r6jMCTQa/Ar1af76ye6PVigd3b
+iuazyL/T8HY81mfyJvGDlFoNQonBPuiwz4CJfbyzbw4Xo4Ma3j5u2vYpJPYpBBnAIP6xd8SDoJH
pXwFkitO1sG2aBmJO9UZckNAMnhVKmrwKiLT4TFAMrHNdEKVv3e5U4bjWMa50dm+c6Yi3bivRDQa
tYxpdlL7lPa8SoioMa18A0u5gRk0UFJ2JeFhvkgW1QVp59zlfk5YogQk/0IOIb/+c/AGIqYOD2h3
lyFE63tvO62ACtD9dU3z4xvcLfrM347hsX/IUQc+qz8jmTjI+UDZ1E85PSYm60eLOg7LgX2CZxwL
s0KoxiygKb0vVWcsjGo/CUWj6UK5788ItVID0/EcArM2j8JdWx7hxmGRNc0QBK/aqE3r9NymO4W3
F64ZAiuRdFpy48rf6CBXIjN//peDNtYrRd9DFNc3tzE3UQXpuLmnWqWBKRdiK8WqFDYJbaJbC4ih
9zXwyqjtrN4Ys/c5Qvw/X/mD7+/lor+2GTJ6mNsO4ck3Jf/VWbJI937lq29aWFytvuulaw+W6Hfj
njVcml8nxgk7OUgttVIyR88aY8UuenWHfAqRqeHLTiv50PqLmQk8NETgC2a+xL4e1qhBVcINkdfg
Q/QPXt5+zPG+BRRWzTGaOSAVu9mkQ6d02LciFDhXsrlrzF1leLWQIvdnx1yNFiLEgvnxGtB2yhk2
96fHF1EfmCiu6mLUqqq7QRlFWpF7vcBPpa8/05U1J4mZY8iVS7x9T7x5xrC0lnK/ndDbl+Y7OY2C
2x1DH06EyH2idX5+8xRB9XUlD965wkV7xMEi4o4VEpFYtIVsTcstYlR9UL/AnzV6I+rDDkUjFw0j
gj82rXbllt9Lx5+UymmUGUVlUZruwSfOHWNihr6PwiIO2Qo+k02Pq/oUVuYX88e14nqUOz9tFlHr
5OkcpDnQQMxBChv+KbopOdr+49b704di90Vuf38Kto6zbrHMm6maFa4ZawDeSmW1Aq1c1tRWcM9z
UrHEtncsz9oIJ4Mml+bmR3sYrLpleBaDlwATllROrpaz+omWSXUNwmiw4wlsRovDUxiv8KNAMmrW
7+BN7/ykXyisj/2a3y06nC/N9V8MUu2rncvD/F67MNK+JzKlDpiHVwcbvJJOqRnXPt2CuddKHiTv
mIVXY65Rd3Oo5BQrEz/0n7w4XDA2+KGni7CnMwhESuuRXvSFr3L7kNTcdYhaWRL4LRiS0A2e8PRZ
PllOAMNEiwP/qhc0b3S4L69QAd4fF4n1CRnp75Ea1tMZyzqB0vEHM59CS1cbZ+uc+9MoYznm9ujH
o5HMeRExG4lNPoq6y3hTki6eTzBuktiI2tJS/ukozn7YbQd54BzpkPfRbZHvZwmz0YNQVCpW1BlN
QlbueYWlfZkdftIY/bvXQgsy/LxvTGSK5Xu7HkfEafn9FrR49sNKO/avNcvVBfhDlihBMtU75WIH
DrZZWBg43FFnVtV+2z2OLUriqI3+5GAuXQXTbwfgSPEg8f+4sm2R2ZsXvKwg0bjFjHcwV1j83ogh
H1ordVoe6jf9S8dsjPnQgfTzMcMsfDAxZpDp193GmD0E5zXyLKavmMgYoHs0raP++/s8D5j9kbBv
pd9BRA8cz7FAGDpGm2S2RQPyCmF31ZwAvjPOwNIDok9kbPiUISr48l7npaNT3IB0ksV2DWr8IGJ4
U722pUgtKR5TGcZNMxz0Q8JzKdlso58l1iYyr2uIb9eo/LL1qcpAg0K9frkE6FVf+cTb77TBilM3
e+PmRVcu/ruWtessYjc3e9gQBeDabGjDhX5CECAKnGFHRmilC+BCppLCJOuaUmKy+eAAtxU8dbtM
aq5kBSO048ogpvertylXatmOC+mkQwDYcL2LxhKFVhu/ZdFe+KKfimwxTJpxJmLFyTuaM8haw2iK
V54iKxR/qsLyZ2+2EacePafcexSgAVSLRURmUada/vQ5mXOydc1uqyi4Xh5caANrn1H5YgK6Ouy6
8l9rUdWaFrGFwBy4yTAAmIDHr68D4f5CRD3rkXEHEH5mE8A+9sVqTDkMZDqEEDwqlB1eqOLUSlVb
VVZ4AJNALcpLntvDV2qXxm5++8nurPd0JTyJ5b5mVhhmLb1dQpKbWkPjjS1rxAlfKd/zP/ZtRN1F
DD6+ANgp3dSg02/5U44NfpeKvzCWB/RoCJk5d2d5t/v2I5MfIhHEB/5xzf1dpAaMmw01yk2bHxi6
kOgvcE7qQ6jvbb6+a3WxbkHV3kri1YslMJbqyM3itCFM0++HYDixDxMhyS7CvtpH4mFWNMJjmz0T
kzl4TzWkadTUF6/RsRSq1J9YdUYpgeC9IeU0NpLUWa2HW07or5qXWXNkxcGcvJOChOTDDsLOeDVf
dOZ68X0bCUTnDRRydqIQKay3S6wuTTLSd9LLSH//id0rSjuUXw0FD+B4Ky/IJi0TeTnbBuWDu2vn
jJdI+Bsx1/a/ZMPRmGKgE4nVfZHyegNScLjYUq8rq2MeL/gJt5p12yD54eHk/8XqK8AR06aSBZQG
2/PKeTNlXLO3lPde9btPtMe7zIKfMcuRZ/58oNf3Gx9imKy0xZUtQOP40+S8SQ09+dz1m7LMFT7j
sgNNKYYKk35zo9B62S7TCxMurGH7gtFg8MxBulbpebl2zgzBXAvwu1QPKrmfnBVIHmn0n3DvPtum
E/wOAJKSeKkjVMb2A5JGdt6ESE435Ld8LATTZ2Vjfesi0ki0MzjKxbtJ0Cc3kJNItivW1bcAu0L9
tcDh1/hpTGu/BPbDsb2+8SSeIp00LJszYSkr7wQNq7EwUgB259mUd8ByZdI/M4ISDjH9+VlnpCFh
SK6/U20ay1WJhbnvLIgsRyj5vqAaG/gH5N2QogcQyBoRWoMQKjzTei2FLaXtlK1bkjChCX3Q6KYg
fLXqvIrdimUGJycIrNlsNa+rNRXZZ9UMf+6LEhy6x0yvd+8J5BKQzJ/zezHOf/rC2IMkBJDynfNL
oTctYDmRMllK5t9GEpSJiUH1VSDtcaApgu4vn2n2Kt9YMad7ZWpUs8zDE3VFhEyV/kgOSLv+zHDi
3M3+CMnkr4c529JZ1qUMjWpioe1Wi86oCyZQ802+E4Yl39mB2cgmTm61RO9NvGzXyVDBDgLjDQ09
CmNXBu3UaKqj6gny+OxomHx3NS0U8tcO+kdPVH2GmMySXA72MaGwxphHneQkE3qyDKyueplyaq7i
pwrXJ7+YtgXr13jicotGDzAWL2sN04FWhG3/LPupsD/j0baojZlc3VaSIUaZtqnP0wmap4zmFPup
GzRojRFfQ9+4lmcdxYTKQ3Q6Iq3IqCyLvnSEmdz40izQdVvkHKk6sSVkxxHo1SNGHW1xMTulOqZy
ihw+y3beerrqYcGbK+ZTppch0UEVEQoV6NLwa0i30JNGQP2JGOx0Cm5LehCIZRLwZakabL48o9nA
FGzK59CaMjSesrqAzy++vPkAWNtBA2v2vQpdcc6L2+q2NmPuiWbXSF+WHjGAnLmncmt+kDaWEkVl
cUCrbOR20yhHLbyyR/uVKNnd8NSJGGQwHKS8Za3IP/u47d8lEDguaU77jJUfli9IxVgEB/YpFbj2
ZXKakyLKMNwLXmlC5QK4fAl4+RAHtUQ3d+ojgY4xP8fSpbmddJ21SlKxNsAFJvdakVbeGGda3bg3
Wnhhsc64tI7IMNEVQ02mYKr94pWxmwauHQ/yyB2U291EMWFTCec1TOy1BoQeKOqI04XWtzgWsa9Y
bCR0neurpgbskhCY8/juK24somzOli7TUzu2pfynENmsLqZKqfYpTPxvW5MtelYmHLsy3fcdF9X7
nzObnoBPwG4+0KITqi3DdU6J6t4JJltW/wfga6tPqlbxSstm5KjJFzrc9e47WFmk3uyv/tKKOEyr
tLS+8swjIxAgicTkcft/WAscsK2qdP5Awp2rOM82V7pyTP2pLpV6vD3tF76C7bMbyjbL3yxXmkQv
KyCwAkI9zYIpdFb3Gbq77XqBFVS8MKTTySkpoGqaVie8gb4twoKLzSwCwfDEDP/K9xxcVfT1RxQo
gdBA0y5aLLixtJVRleFD+9OBUrZ6Djj2BGxOzz8Pctrqr0FFJGIos26kXw7suYl403AIcCz7Xoit
VcnD8jROU1wH+NoXLTFJRo6J4R+ZvVMqXf2OGHmfoja7RTYxPnC9i/8wJXCuD7I0HwPbGbF6Y0xM
R053WXx4fAvfgjM78qTmTU6YIay5ZYRw5XsQXhF8OP6MuMl78pKfTk9/CmZgJ9VNGRw/bZZaQKyb
PhGNKzYnQ1fytnhnC8BpiwS4yujr2/tY4D3KvTMDJPyNIsuvyXfPwl4TQ3pu6jFWT/n+PrWCSvER
AkuEPhnKWvubRk44sqODzbl6yF4b7rm4IoULM1JD8Bx9LLaxzafpk5PjJm06rFQkHCiVGf3eAF+R
rAs164b7UnugK8X8llZJGuymuLOP+c2YsedkAVa/Mbtmnl28FulI0ei26U34eqB7t23QKcuiJmX0
WluN7GgtbpT1f+6qRuVKPbdnxojmp4rAjiedPs5tlnky6nZLZgH2YtUrsTu1e3tVthkRdGZs0kcy
pvbH54hXMedNyTpqJnvwOhMZK5p8izc9iv7F9KOSa01MOmQ9p9812RQWxJ72F4+H6dU5JTYGMlg+
8nzpjBnqQ/S4d1yCH/1MCgIPGGVR4zfviG0KAo3za0gVHMzBKJQKyawxPrl2VHK94VjCGHJ6Iae2
ElhtcjGhmJBNF9iTHzHFcYLuN7VhuGf3rZXSEOK8m3ZbQmoyTKH7I2q4FVNFt+ZhAky68sTOP1vk
7bhqQrz5rBIYKhyC8TCCXRtO3qBXsUkFS7QHUTGr8UJIwzF3GPgL9ZSMVOmCxoyKO1ex47e7Th/0
QMFjZeAYo1r6i3I1OiG+Pf7lFUHFWp1HkTiv08S5RkIXABPU7VO5aWtVxIBN59+W3Zp3mEaNRbxi
B4IjDxvuRz1g8OIzh1HU4xuw+rBi7ssXLStsSIu1O2ntZGlb6CoTbcRb+4u7yl8OdOxNN9kW2gmA
eH940Jy0buW+3QwiwDutNxNxe7VtX5p8KF9RpkGmX1z2aaRnXnSvoQI4B+jLtNm18SIiIR9W/7Lx
ry16yfdZgEf3VHzof7DeVkzGiJu5glWmaxxhE3IRWqQqbdYO3eR6lV5wwIFjykwFRn5fLTFtHK8e
3bR3PfXnsJ5vClgwa3XgmG4zXdF7a8JRCQlzI12b0WKFVYpnbPkIFbMOOkj0pnSHdeh7LTiIQ4pA
aix1iCVIlwQ3PpAXCJ1ZWnyEM9NynPhjQc8Zkljc7rwAYtv5bfBYD0Jd9B59HuRgxfEtoj5D24DW
AdvpPV0zd+xDws+8uK2fhGUL5Ay6MiomUza12gJQ8dcMzQIMyYmVPs2g5F6kiLJdZ6w0L9kgPqPl
Vwx85715QqaheIqf0N7eQ0Onvhv/2Uz/cw8NNmp/Bq3RuMNMRhg3P5PhA1Htub/SmKLvJMR9iS9B
Be2NkLl9mXoPEO7BNNaGGwdW9upGC5sCzQLnCYIFt6OzVC8c13gJbOC+h/OjpArfaSJvhmnuRJr0
BrtjXAjvdrvmlUn0C8m5nOTQNcW6XQfGjkMyJS8qB6GKhbOW3jp+fVP4RxVpE0Vz6jKanaqBQ9Xq
Sfz2giSyYI6FDIDLnr40DfOe84JaLmHeXCqRwkclc+IVCwJmjR4Xyo+DXS8EX2AAVofq2z/Lbcvl
MbZxT8WDOQtxaTbKd+YwQPPxE7Deic8wyL/+jKfpraMQ43vk7AZRgJoH/iyM+bXtlEu1jIkP7YR8
cUcxW8rELal5qLwSK91UXNWd/VAdo9OktW0wPHKjg9NoiGKjNdncrIESVvjONrJ38ZVN5By8g47l
jMnJzlCCwEBhIldIwAZtZhK3sQGxggjpzUESKES0FeWT597kC+ommZqJct3RGhCTH06ibn33cauX
poHvIj1zuSZhUgekWHtRiS8ZIueY84JDD1BNcqV7hfRayqFQg/UTGBjBQvWvnBxBOV+PC2eLfQco
WkcqQjw87q/ZrzGU4/F3JuPVkU7XemLqnS6c3+YSX5Y0YHi9ST7xHFyZCNF/4yRqTytBBVl9vo0A
nM2Bx8tcmgcY61ifl9SMkJQzvFN47jX2vv4vEXor3stQ3Y4MUHQlKoBfVMKGaxEn+V1E93B6O2fa
Q0gv1rWolchCjvzJG23SuwM2arAGh/UCNfKL1nx3vpLzvpu/lXHOaULW0bhaFmpSciFtuqCYutPS
6uLGTzLSt4APu6VsBJu2jJ8AKPv9gqYQcIFQwiooFrkiIbX+njOCFDqT+wKxgHUjAJCuatXQKwVx
rkHd7OgcaOpX8iSmNKlkf6t3pf108eF+XmhyK4GEAqHNAaUN1PcV1Kprd1XW/IBqOhqedv7HxqSO
fqLXqjHjBIwe7wzLJpesdxNoC/R4bOS9X/yGWucD6Ev2FID7LdoD3rMujrcDXhNcg3zpgnr07puK
oSFj+g+63oGqTU+/iDnB5ofNP5thW95siEZZduXyIuaK9bCgBbOpf8GV+13QY0hxzMpFqZLvGj7R
gxwxRPNNYqx2DxD08pE2Ozw/wLFE2CJrpoW/I6MjJ0yKrtDKQbG4jcKVQHRKZABlhq8Jt0B/LYEM
x/7BxAjB61EZPQkorAPX9x5tkQOMGXp9V2eIbxZi0TldQIqJs0xjXwQWQo0lRD8/daQB/HaRNgPl
NnGGTGJeCQXKUsuVN23coXaXyW6Jvb1WFgAV6IaTV06Rt0xZu2DOPa3eMzeCwNj0V1PvhQt+2v3i
l9N9G2gf/E5d2e/+nYOYSu1fRW9sD+U2C0LM5ByG3To8s47WiI0GxsjYcO6bBKdGAGgudmf2mr5b
tixdh1pId3hPlryOtMIpZAm+5WWBEbMI6+TSu7ylb441CuoyVOrfPqHn48lZxeowFJsZqLjfqXXn
1K7rjLs9B8Zm7LYKIZRnhvl/BcKAZfRErV2E3sodLNQltzA62xwvRBkZScyAADE3xGe8oSShsyZ6
XTpFoqjAk2Abc5vs0OJaA1ESTSmpA362tIbiRkZsnKnFTUA+shh7B4Fl+rFOqsYk2hpYTegkvSvF
i+hcdgg8ud0RVBJ7ffbroX9/dopnf+G04/bnfQJeqyW4ScZdCfxqlHgFHQo+zKNgykMfi9vpCV1F
8Lzv7EaGeFEwfSLQyvpy62Mv1Jlfld8LxEnGrKVD/mT89RWlKF5DIIUfGX0KSt9iAFx0aLTzl8pT
Ggt0ngdbSQYD9nKXQdbxJ2FAF+35tKdvFXJrUDUbIEa8vWfUDJvx2CaGj34D+VTrdAS7DvC8po1G
xJ0fpE+UL3uNitrSqrYgxgGcRAyRqw6wgmP3crI8kPOrXexJdmY4QrtsQzoqkK8dsa+ugYLkBLhn
KsGfHTZ3Qjy70wUF8EtuPnCftWlcx4qhkdhGYWPujbf00OOvCtIFTTtEKH9Sqj795lCe2W4Ae9eB
OYzsBmKe+6gdyneojSzHWDjdHhQ36mtu9Nm8y8bRLEI/NB+NLzZXJx1US/LrJLp/DROTrZ5SuqLg
5uaWeBjqA0hU3acr9zjEYW3ctmiULO0PE9bBodQvYrtqjX5exHbLbB1DFIC+1pwQXL9SRnpJfB7x
lARp5+me4XzSUDDeF7FE7Qx+DDHwZG4G+gQZyXb3vCtQwBBfuyHJm3OZ5l4rG1wC1G7fjXtuVONY
J7GWmegZqgnTJsZ6uQWpmADlceUDQeK8OptoKP+wlnjnaIHI/+1VCWCIyuU9xyglgXzcNmSg416k
TRjnuFP96riMrQZGAotLfEIHokTb3j1wN1STWIMifISU/ljyNjovh5QEcWv8OcWbnuuLqQbE0O++
ou3fWjNth2Zot5dp6n1BSjDJfKac/5wDc0oYNrG7vdA+TFhFOAqSDfBORcQinn8jaYbhVWASSAMz
9nJcUEmUzzoIy96OChOUSFM5ICiDIF00qYTKKuKAViSAlaK6ETorN/i8a9Tth/EQutoOduEjkc30
oUNbI1rPS0Wu3QNFDHaADoKfxoera0cfNAOkvC/uAyvTtGvVM1q/qMNUsw6KCIfEbyLsRlcTgwM3
FSxA9lQya2+GXJsTa9jMMSvTMpXwuQ5M3RjpWqiGVENItRXFaqAmeJXlFIzcXvWljTjiGTodzJS8
Z+aeKmeV8VM45QhMvO96qZPJV4tThnr1vz/8gA2/WvxDZlryDw7amOf7TkT//wfju2u0vtN4VnmX
69uDdOqdxmOXBBJM/DQHoR2zt7uwESyNQfyzUpibyjTp77NFG5dkcUtn257tnkPKnYJ9S0W6QOE2
0LVhyRIyYvbdfvziyw/En0UErVn2dExVtK9WHRX3dc1MVDh0VWnhxHk3MYWFHrlvYmKD3EHkY0PW
jM1QpjuYEzWDP4ERSvBkKEThssgWeg5TP26lZ8d5eLO4vBXbKI575RQAfC8nPmeghuJkgT+/giUO
avHpULpktb//r23awYe0canXtT8XvpIrxrZedGV89hy3CIAq8UknLscj8MaOWq24AQmTo8iiIuih
xpy5ZNuwTk5xLUieip05BcBHA2kOaXc5r8c/ocPIC9Z8EHPdOu7YPbjf6BuxsbolLTZRxnqbM63/
4Dii/taGDc8cev36x0R6g/83n4to5lOrHDGFERl3dC8I8HsE1xHMWLOUS3Qt1w3kU7MSiA2iu3vm
EolNrMYsuan+ule9amzjZ5NBVFYzsdGDSG94iUtlpaRxr+DW+9p0jVO8sz7Pagjin26jndji6/gK
yqZWSGqPboROmJ2SplQP6YPtkOOBfc/KkfFaIRJ9zs2Hl3n1wH+rVGvl8CGG/h4POGKOiOazNRFg
QtrlceiO/AI59qoRfCCmvkhS77klhSntfQiqG2ZgyW/+T32jHK0jAuEnlcXRmdJDrPIIhgA/v2tp
KVWDodBI7pCLOwQKO2XhbWYJkh4FSHK65c3UN55gprSBCC92++debAPyAmDRG0cjXuG+2oFqVKwV
KRjaIQ6BspE4BsSWDQSpNDI3HggoVn+XB63Bczs3s4UP+yPczf9SGMFnQfBIZZ2iRFkWOZqTMHS3
LH6NQsvTiw5y96cLYyr8oRFbFRlMRrU9AL12zDkavin4mXI7JEOSoYIuX7EVRNx9KaFmChnTCp1a
P2vHEyBdzrioxpPpv3HaH7VnKXULuNRPHPhKvoYLLr7roniVt3S0/12Sb3zM9ZUngNzaHNeQ7v+O
laDX9mbJLGvgA2SMJlGn/9POmXkN7+cTeVFrgof7I5PD/UHFFcSKa/siLEcC/KK6qKRKUtvlN1CQ
8NecZgt1grSLC9TmwbG3TjOyIPgrrGdgikFTaVT2Tj5V4FMWhnYG5HLUqoOvSlMib6IX6kFOsPdP
zgGQ3cw4GXID8+yOBUm/Y34Ot0vUULel9oHhY614X4J1XH6/PjJ/tle1LT/2X+k+E2zn6zzv3TZW
c69CD36C5PFI/CKQN5NEluqrnjc+i/kyKjcqvxCcRhBfgIVHLGW3Z3EEg27fBEzTBOjl4MqQiWbd
CSeuJSv05UjCnqFpzVRqHdPrt0JFtzO/0dPCtZpo4zmRvNsd1zGCd6BXFrcwR/xPeqe4GdpgUrGz
ZolgMOt8h0NWjfcrO7/vyPov6xaBW8/HV+YaUSwMBLoCafCn8770/tMFFsUMSMi2NAzcvZ5+rx6z
Fleos9KvjzVUxF1F/whfiwMBQrZoX71n63rEtVIvKpMQe2jFghkThxEK1WaAgdHN5JKF+BgKA4t6
VORFvXga5f4SFTrInVW1jEX4rGiZDYki/yBNiuCMKBMZjZ9SyrhthMKFhszV6T9XXZRMRzwB3eoL
dC6+cvCrJ1GREeuw6re90zd+KGY04OoIha99KcPLOFysBpzRzW2h76nx6gMbXlb4mStB2dvG9TcQ
fbarZvcaRkDhGzCFRkbUqYNegtIVJ35pG9Cs2lfuze+w0NOxENlietMOdn8nyh1dRJR2vbzWHozA
CDuC1JceY1VvJy0dwAxu75KBuG3H6L8uVE3ZGxvbaafyjyy42aCcCPF2f2bgng8vGIZE/v4FFnUX
3TTgJQR26Ondqv7oLPX0vZKkrKRNpAUclpns8Y9uMmBL+lnPgVaQad2qya0heO4aTiF0/E+dK1h2
R+fkSrK2WIVmdNmqgy4B1uecqcuTY3ZhAclCgp5kwewgSJWd+dngp/iisiPoPeOaYmJ0XeUWNVn8
g3NlPCzdBLBqnkGJ6PQTYsjDU2BDZcVGm+ImGQ3tH/R40DvHG7X266UwECvYbfU1YWNs/31v+a4R
uOpnKTxEHHgdBHZZYxPBWfEWPIH2R0P7pA8BULXFndlWkN51+GVOKH3VyBXzOpCCX4ciIt2Ug+GA
OGEDfMpFFZlXhfKsrzLNuOkqz7yr6sboJ2hscxxZB/UKHLQ9Gemq2RipY2uhMyYE4oP0TWdKXwva
7O+GUILWbuJB4h4djgn2cTbNjXH0dybiSdjBIqQlizpb4rtjekneXGzdlGwvmQ2lSzWa0s/2oDCt
+bhnOIk+VVbSPlD1h87JyQCaPYoKBK+0DfXHUdlGOPish+WO1qnoegelVCbB8zPRaYYd4qVTAmOy
U7thQmf+DiOu5qTjDrPwl7nHkq4/9K4VQsOJUA/DrCUeaihofDjGp5ZlVwO0nXlJrf/Jn36YfIKO
uEBXYUVssjyBOMRrJ/Vy4czHcZxlF0tXskSpz6mXHl2iA8jI0QyvFx7Ou+awxetggAoHxuwwrNFb
9+B6PtBtHdYsBy6IjOwgJ92eDxNSt3k31lPiE78GFKSYcHPYLX9JXkmsmbltH7aqaJQ4B11paTCy
zQSTaOxTBZIFCl4+PBO+pCOKkqxZ480kHEGE0BBj5qNJMwxLEb6ZqC+hwJWfEGu0uCWBBI28wFLh
XmsSwr63g3NrEGy8HLFFRwUMU+x8Naw+d5oh0He5pxAFWUCp0a6fAsGcSmepo7WDAtgnFQXnwZLg
xD5KlkqSEIkU/0LD/zfMBK0ncLMwkqxRX/aoWTa9naZtsFa0rR1nbGVDezs5Nwg0pnMXJLajc44p
6ifLgjhxzHEkjAdgPsg0HhiGzXecQleegSQ/meLsq7+Jq5GXgRuw02TBoGuMJMSKGtlDJSo0qB2R
y9NsJYAHQaZP190TViQSfQo0bRj6KcNjnhVF327DHLidFi7Z1yOMVChIK0JYb6sSFZS+eSy2Udk6
owKWVuD8simlosecZmap82ppdiirqRi+vtin9yjTxeToE7vrWX40Dd8b7c3u2zVbS0ype5J8v8Sc
ACrOUlZ3oU1UleQJXP7OT2+eKfXaTnUm9oJ9yuc3JBg59mJ5iRMAPkabdzduq0ziVal8wXEuYdP5
5BcFhrR7RDewoqzksvx1y/F0rDy2XGFyC5SCpxezVTAM0oi56tThWhcSztX2ZDVwrLT6Og+akQpB
YpUcttavMI6W8uZf+U+EEuMhUQX4Zpv09waSe03/KcKyZ3X92uyzXWX3gc7mWy8h3z+YosJWkj2+
VrzRIRdR2+IF10JcrXghgKjcepVR+5ygqzNJ1xNDYFeM5pqEZRCZYDd8D2AghYgAARY/OS1V/L1L
2YHXxKhmCOPbhcecA6yaZeArjp93KUzOpGhKAHYXc6T1CZYstVLHhE4OMAuztAxYEPy/Lbg3MNk8
J0pjoj5oKCRMwavXAPebPTu58CUF9cgg/n+Txo5rQ8nlMB4m96SjOZn8dMu+Fpl5yNNgOflmKZrv
PJjXWKT7tKLJ6B+MOG+vV3426xgBF5VXmKReE/s2xhm9PxItzuC+q1E4bm3w/4dWQ8LKleHLJ2hx
CrbrpQTgZyXcfBNSUUOm05p5SpxV98JgWOM25LWSjK8wpjbLtTiEu5esrJZz8uUZGGSbV8jSJpk7
usEjqnExPK1Gp3vgGposTh9Notgnd3GWwXoPs/CMFq94SYbJiQ591WKTR11RuVVk7qs8kNaVYSIO
CBnbRpi1F3mTkI7ll3FqaObaBNDd+eiiYMOnOxilKQ8g5+QkcI08NQFxBzzgPZsZP6ysEpPjqDts
ab8pqhb/CEUcfPLt7Ag52UlJHnYlwjk2TmPL8j9NN0HBAnYk+4/TTcAcb5SY34PvXivtgJJGASuU
5mkYVhCUZOftHzn7AXnyzfPBoSWs5adyme05/G6HfCr0EIsqhJW/xvRWnq8sMMvIDQyN/Wt8x7wE
ox6u+pE7n60vM6Bow9Mk9H/9RKFsWR/Q37KvCFXJ0mOHs6XdZ17nahCfAKaVGS/df5iJc4V+19Ft
qyduGGQTNqBlP3zaUx1J58jwvKLoANGH/GhFArTKBdbUqvYnO+mIaRe5Oq9PqAJR2nNtCG+fP5E1
Yf1VXe1w9p2f6izMPW8tVZSAc8sa7Nnfv1KQyQw/Xn7ZRgOLTYr/73bckj6IYpCI5NTpNmiee2j4
mNvjMm4yHmfwGiMPHYkqtpCxeuzrAMUN9PXUaoShsAlNJhC+WAIoNJ4ujUc9zODxjhR8EiFCd5JF
CSrBYceyApU26phla8jN7D7xWc9NeSGS10UWtzDr3HRQFbJNQ1WRCLAKezsu7QyCo0E1a9yGo4BB
CeHoGsUW2lHlIL0dXKYSYWXuoa85Lhj5BtGo9XgphAxv547pvSvsSo2IH6ztye9hXk1KUov2dqW/
BeL+TAXqIaIBiqadkYWXyNgCanuL4S9za2sjHgLBySb9XtakaeZjB5VNjzb64yE8ieUpmUodgnwh
0p7wZNpb4m3S07tsbQoGXiG2mblSs0eztxCThnK+2wvey0TlGkWgOQr1vPaNra1uagM6P+Bdu8+8
K6SAw/wWS7X9L7EXhaY0RTp9ro6z86KXPphzayF1llrYcMn2SVhzK0l63pJqFzLc3/tJdN4lWGnQ
L0nucmeKT1UdvDweX/nBIhGi9mO/25vKX/cxymnKcrLGUqLR3WRGoFHLv7RpjK3ifymNrbT2B+Y/
VXLXbSfetCGVElYuMoZ4HvzkV+AuXvxX3DQgRXzjONxFusRnlCroHSEhcvws2xmovhIx6nqvE44K
sQwSTBjrPR5EkyUhROxbxjNq3ikPCCuDhunRFi9rEIHgcGSzy3cc2PItKXxxwUbDPdI+E0PvH2oN
TIrCiGud3JlF/r06gmz4tcVq7m9cdWYuu4L8uTFr10jOx4jSNlVU3GUSGqF4StCIufxH2sYsR8ds
rIEyZe4C5dUgbkCnT6lXMSN7wTLAA+Mt6+jb/2OCSy1tltJpuYdJnM/fI9mBjW6/qU6inocdmKUf
AMQgBCpSq1V+xDbcdZsG+x6lmKQ3Vc87kvAAdJ6+3VYkzv0wCsPw7Ct4kS/qw1K+4Eb6XtnVdsE1
A7ciR5kBbi4vvhanFzvNTlV+CoLsgXFV6hrD0Ma1rvGITnM3dbw1HENxBXTi4HU7BKETr4AQw/Do
O6iI9t8NJKaU2lXKZJPHHreUGwaLLarRY2M1RpPn2dy0WrMN0Ik5Jp6Yf4fCbZkTe7euCFnh4kMn
Nve5UxtNst9H2N4RrcEjKxneyFBkqe8nNNUexzYRd2us+/XkbhncMTTxhcD9EVpJtBPXuvTjGhCH
is7FBbqEieCmsWxH41eyBkDizEVQ3Mr9izOLdHID2fD/M721Yh2Ef02HzfUOR2JWDrGjSBpr9p2h
FW5PUaqHjtlOlBywnOEcyH5Px2hTfPd8djk+TLgakMcbJr9MQlW018zznjxWydh0WKdpNnbGBLAy
4NZ903WMbHxiCiwZNaOtb0iIBWZteNBwSiis9l/HLJ82CBE411DqSYt/JS5zAUhvdYatJrpZExNV
6YcXMfnuPC6FdisPVt9zTbjrsp3N/4hQByPvqWIDCPbpEsdKM5r30EPooP6zodHuGNfbptwPeX4W
TQFTDt6zFsu/sPuTojF7D+MeK3L6I5r3wkeWeyt5nRTkPnk9DPeblUzP2u42SCukcgW0xZNRb4hu
c8eZ+nuyfjGJGTouySI+MH9evKgokzLS/XPT8ss4s/hWHRNS10rcCXYBfxa80SVWjGmjvAAVcoXz
EhA8WWlJ6GU1KqGViiCMPTzNMHn3lt2QZ8aUTaDgWvPfzrO7AMOL/lyDonHYjfy4LBOROxh+8X2y
we7qR61f2NHELGEQgyi394mh9QexYmgSVK+tT8/QG15vHQqM5h/Q3sBcvJRJtvJ5ltSGSYA6DcaZ
Wua0csm6h8EuvUIXI4rAnrXqUw0ChnL9vplPE2kwQmYZL8sE6QrCH6ZF6SgUWjJyCK219CGMwEFx
9iF+jeQrueeJqXLfeS/ju7B5fjOgNrvoWGFmfhZMBPC+Y6X8hMVSWeDydNDhw1j4ZFqvm6fAYkjy
vjug0lSVtVWEq5n1FMr784qMuR5nIJNIZ9ntkcI4xpdVFeQ9HFDKTVsfwPMc2tqa/SXmdHKSsSDk
jVwvSedsXyUWKgjCi/D+H/QcNmTgDtE8VT0Cal11dEhRQ/D63VEdg02I1lvEhwvvK4WJZHWO1cC3
fcq1iKuqzDZ2ZGQIecP5EghlvsFJw9PTdeO1NvfmvxFi355IqaACFEQIPQ05h6m6m3lF6pXfEu+8
BZy+1V+R+XcTyYN1JrSXWBlnYAYhk2FFNAnH4xEjViKCk4gsEX3mJLJDIcXdRtIp6CqA77Z62WHr
XkJ+vukjwEXk+f110hj9QjQobYdd/XV3tA6Txq0Ls5TCnKt/P9M5D7A4PrSdqufGgGBS1JtLeSDj
WDQBQVjZUcURS3IENwWdRGWQQqRnQSkjLTWABr6cIdZWtlXsenaqcmDuAL6lZXOoL/vH3w2XYSxZ
hCd+6Vj4J9w5yif1iXmzMrV9ktRWZ5ckY4j3vsTVX2RWIDd1nc6s6m61KI6knoAzZYgGZyDzlDqn
2Q9PG+EDeMeL6JWApWT27ukoOAyu4fwJG2DbjbknKfFYJ5AUpatTW2Ey0+JFdrfi5/LU1ur7LzJD
Lc0cdIYRv99E6aV0RDAyeikeZ/blWrtg+aYr/Ocao54mgkB1Hlq+gH76YfC9qlBEwoReTDj1SIzQ
iEtcrSnltEFXdLxTjHWVXFYC0qfToyYF4Md4Kimrj2W6aVSqb+g+zkrQsRb8NoY3L7xlomaDYAxf
whQpk3jsmNblAHav0ffSGxZHmsq5arr2YOolVMxpvNrxQh3CZMQwOz7qYWj3J8l5sNxEezoEV3wb
KCvEZxdmlG96DXwt3h7W2LQTfIRrpHZ5mL2lX7ZQDMzVTX/MXTAg8bDRnRQ2IEhxEyDkt/0IHSOs
bllLXhigab+RQU7mGR30Rqn4BofhEvNWPCYKsJmiup27lEN6yn8/NaaftE3Trr1RMcPSvxnJiYys
oQNGB+wYbZHlsmAPMIZUYaDAS3/YAB+SzeyTU5LvXb9g174TlwefHuy/GRihGGJfIruvQMZXoYQj
48FZucS3Zchv8Qfa9znHfDUOOVTUriRiviA4LGCvHerxC0oso5Pj7ByhyQwUTjhhVcHblqjZFQAd
X/gV+nAPSJRtIKnxwprIlICGClUDD2rGQwSLHpgaFUsMt8UAaRD/g7Hvk9GH3yixBdQXpBk/cwsK
Dv76KHVrmO6BAIWMcDmXIaGyHzdA4bUC33e5orCZvtdaWogebyhKl934YhDVB3OqhryjQzq5FrxC
t/zP5JwwxmUbh452UJE0R/lvsfciHSZX1a9J+tPLL+v136TgKL4VKM8RWgqVuqDKpWY88gj/HgcT
/VbX1Jf3P+Zz1+W5eC8XXq4VrKFV5YX+UHu+x/cizavFY/C4Q9L64mZTlTCrZQJBX40Seudjt4g7
rgACcZ7OJoLQVVWfJFuKoBxtZ1U4pEJnophi+95aYSNS/f2FrRgPfFTHIBPpy8ZKZSG7mHgC2mU0
eNBcg7haaIoF3gCXyIhqx01n/9EVvtX8RCuv/qW5sqzLeNy2BZxaTZ5trMyoLf0PIpkw8nUlJ43L
ZqHyW89A92YaGkaXCHV4K9YD1fDMG5OKdtSSN6G6jGJG3I6h+VXve6OrddYlzXkkKdhS9+SS82IV
Ft8WKJ+BKNo8BC4b5RJRQtRZgQQLx8/CBVdLGRJ3JEHx75cn3KIvdjQl7JMJzuZilmcjMC+QYTyP
oaty4z8Gb2G8pmLl+qfbcDvuxICOPTlBGOAbKjMsw9VMkZuNSghqRK6NDd2JxjZ4B3B+3RO18YbX
+YpIREQg8SGZuOLNO605taW90nzec/SmXSuZSkGAEdD1b+q2GuiMQX9SCriT2ddf5zKrFGC0KTUp
nf20F//13zTLJH8Z4PgPjQgWWH4dksnzPLqgSxxpd6dbLwslu70eZHhgudAGO+BVhzhnKfxLqheo
31fHtSXtTl4+5W9ldcyChTapdT9YqoP8XxGk9fKnRCRgEeWchKrmxi41RvV7PHauj3SKARFCLOpv
8dUGpOLz18BjRqZ1c5Jo/QOWa4a1mQJmSqtS52WmrKb8+FPCtY8q1KA4rCG2PXvXAsta2a24aQdR
zuwT2l3HmqqMX05XQg6+nvYKo2aL7rj06XX10HkhdEvelDxXq0Q9ErvmIvELPyNkQH4N/6cpADjE
vpAitGv0e+ThUpFpwOAYXMF2wWXMGoIn3SAi2JNHEEI9/HKDqDvoJtgheXha3UrGPEXHGiQo9+6U
m7G7D9YRmRIuZFgtPrOojrK6lBrxH74GAaMGpti4rGCSDF+zQWF0kRIW/jUSvFt3+dkaOHhsdve5
0iomi6whYrvSjTVVYVfKzuB/xEmiUpJGmxAKcjoIaLcbp5qFd53bHnXEGbmPRE9hQrFzjPpBGgUw
P/62AfVT+TJRZVa/Uj/y6ryENsHw9P/a0q5+L2Ig62CvZ1xcJvx0huWiZfpo4CaVR/+L6370Y6WW
1uM3D+a8ivvPkDWHXCtTulJheG6pEhwXy1g0LDJyyJdFRLRHNgU2U8Dl3XZQnBDOtKXum7Bn0lQz
tD0GUCQPnhzGFWYTAoXW0q3deALRz80PrIw5AEMV59QvO1CozNRIX5orUDFhUdfSuTgDbrW8MQPF
qt5u4ZyEwOdcC4TTOtNHyb6WS+Y5qBjr4xW/6cZKMO6vW/A2LFhns4emsPsucNNBm+JKiv61KpcS
mIe285eQHV62UPekZ4UHtUewzaz8VZqF29TPL2ZmG+t+V85t21wIlo3LPJZiOne7U8zyX82a9DYn
6g9OGBvJZPNejKPlDUNbg3nphKNLCxHsxqbtFYnTJVWb01EdKedEn/65ChPVMcjznKL4tg3Rghiw
yxEPmNENuL0+HQCu0bpA0kdHVXZjxVf6U4DHMfy9zIqvYFmPMiN8cvb2N6VkABeDMIlNgC2ELvwl
qbtSHcMKqyIu25R27FzLZJquK/AbuZ9qptFjg4W3Kf1RRQ2RQH7VlMrI2vmfaEEQ9Ij4caakK/Vm
fjgFe4JqVsRmdD9BYGvQSXKR0Ta0iE49wh3551QWe7N78B8UzaAsD8cR9eRqSv+8r74Eqz3kwClW
nLICHYMJdyWDXk40LzB046yoNNLSaTiHtu7PuY/GNSD//AkSoI8gOPT6lklzM/U50NdUjdFzk4hz
xuomEYJ9ljEZN5EW9WTy3aM8hXR2j2pfJWweDOvkUgPaps3DJEbBdSWP5WoFaZLD9Pt5dBEAGfJG
2PBCWSTvfeqSh6TOY6QqDMiqh/yPdIJAT0stIcQWkfwUq7goovxoHoQ4oTNvWNQ2hqYSHrlC9EU0
Wiui8nYfhY+85ueO7dnsoAd8dMcvGGM/8Kny+r+ugBh8WsEXkCpQJ5jMo03xutQ1iEEcY8qiNU4a
JAON0vHAeiO++Nun2/ePQ+3Ntt6fClXaPg3GrclTgxDXaYN9xmcquaYau8A5l+HnZHpC9GTFdSCw
NVoWkdCLEJ5NAFPLk/uWuqTQ9fgl/oizW1D61r/YZC5s3CRyxtiwhulE51zRbVEGUgccN5EQ7QDa
zP2R3b9dpYSKXT9p2TfhFxa8hvZcwkaIZN0zYR+sUNcEZ+RhBA92JplFoXeBSgWg3CmbIuOysDIY
AHaolFAErlm8GQ7fDJxOkssG181BKY9XqlemL5bO7cE/c7UhyyfAUz5Br2IH3Vw88thkMIwbTZ0W
T+Sn9buZi/V1cAp6dBFkiAADLztj1bPXX/pYeBawY34LJG2C28Bs5rOfWMruyvFSgQSiopVvL984
7xjn55zoG8+BndYikd6vnXsnbaJ8WoN0d1IG3SZSI996c74MFLuyJC+V1V64CmYMrtJhHoYEG6tB
JDZy9jyuxMIdBv8Uj3bJjgWV2SM+vXYwx7025lf0GFRiialgyxDxwQncyUzrFgcqR2SC7aJewNtw
/wsuFOgsJsM7aTobuP5z3hHs3lki2UXOLEs3fuPLZ8d6s+5GjCKTD3/3uZ//J6j+3E3musw/8Q12
zS/Zhu3VKbI1KWwufDoQeJbRbivyFsG0qFteOUDN0OQ5GZbXNBx7SYhTX06US14wrz/cCqW4y9YY
y8yqOWd4uKGnsPVSUEeRkUXSJCnLsXlGdBxSY2MAApaL9br/JObD1m2ExsWxrphd2kvkmG6Ll40/
4N7h2d9XSoDY7fz/H7bcAbvC6zZJ5IJk6QZfNGxvYZFFtWJsKwtFF4H44CLJATPc2CZs/RCODNf9
aRmhh8aAi2YZ1QqqA+lFWrXDqGucKqmU19oTfBfvOx+K8WsaxI6UFKCxaxpnUDpM2ODXFejMUVCW
kPgRVG1mjZLP/Ci0kG5oaYss9JTym0qVwfOfUJbEeAJDllpaQroY/ph7efnsbC8oHFAm2r475HM7
BeGlmkjtbWEpKaIGf7cGI/x/jMcFPb4WcDpRQCYdwlEXMVfUFkV4untga+ShFgw4Fe+WcdzMTtJj
Z79lyAWGvL+QLvPIPG8Ltnnvzj+YQQMTNs6dFbwb+8KQyZ7HAgKIyaGjj5ty7f9BjDpdZDOf3sHh
A1+3OWtOQ1nOaYr/I6zNO6geEasXQTeEDyQvq6ZEJq8wb2jkn0pNYKpgK1xOtcFKseijvTDB4dv8
Pl8+qnXcE+ByxIwUqsf6My/BtbbcfjZITWUSGJCumNoEggirGUyS4wJl+TOCOQorJEsQf5TOrY10
lOPxhk1fLV48eh+FltEq9PCVF+tAJhXRmriWRMWKBAVg/tMkKFK2hp3OYimgyaWPWP0eAGCoSKcb
vMRBkmVm/hixCbVF6yG8akb1DwWtA74MVFqt1EmRTpTYIzRXz0A5VeA2TBqLWL0cd6ytGHZn4avS
maK8U7akilA1bCmepAKZa/5nf5mrzJohGK7TGuTOb+S2Fev/4Pav1PrG6JqJNdj+QviFroRouvLW
ygi5AmZCfvEXODp5kKaClpseFiaUz8TIEb6vrzkPrrHsu4RBKprUtrPHEwHaGzzgaV2w9tyQVTJb
yrOhy6Gnc8RjzMxz+vUP7TZQ90yAw/Z15xOx2CmlWO4tWANrRFMwSLhfLeNMiOVEQFMI0atITeOw
cDhR6AsSL+8w6dqYtTHTLvAwurlpY2vQmP0O4iBpo1DCWz5U5bZuH137qVgtUcZQdr6RwU9Xa9/w
9CJEWgw5jzqzNcIe+xxNN9XfyAV8zDziiVQ7noyL8VyOqmbSMOWs3abx8g02z+1sgAH3joJPsU/V
qH15/4xLy5UNcM2/TQ+n+xxP74g8mqcVqmqinCcoTmLaGfgC93VA9dCp8fEnxlo78JvzGTp6y/0P
qoWpEDO78xvSyjXTPQzEJSQ3S/I3PCXVj5dSGun0WIsbyuKBoMnJWf99udiFOS3kriIN0pFx6TV/
fX+vM/6EsOuo45aj5jLeF8roX8lpaW/7f/dXfPlPb4b+kM+DFMUZPaxUwbm4ddNXwRgbAaFLi95U
pVuSoP6Jo7LbpN5RuX0/itkF9UeMFBZCt78+uJGhIOQ9MuyRp6kDOhZKBRJTEZvRy9+N169ipp38
MNf09YOttyfOT6aawjgY6YXfo0x1CyzVzF1Hajl7LqirD5frkPyBirCh4yds/HnMMz9qB4WeYkRq
bMLY03JqcFUp7XxH0pivlEMgHUKWlq06pBoRActuYbguiARCUKTu3K9JWOUrpUErt8RQi3W6Ivmi
wH/w6GnLad5wEdihp6C04VJolpMPBvA7EFrnyujXD+kE5pT6D/+eCTjzg1Iucjfjh+o+OtYa/a+J
XEUInu7x9ZNGpypewmie4p2pHiIYTwX2VOzXkPR99N9IemtjJFObWlOPybHagsSZQvAyOYbQzVmN
Mny0W4ChxOTvKLvvbUQGuHfsUrohg8fdlcp82S4OjybZlC2dxvHVtQqhnpwj9ecqM5W4S/+EYsmu
KEWRkDcKurAKnJqDQUSR4JfvVn/g71eohIHoekzFRAWfQwRkVhZ/hmtVHEzFuR9aGIdCkIJWprOw
E1V5kwFo0nPtEuHTSBXEgyr217ZtllXwQ6MX/VXpCYclfUvjjRS81vjj3UntjlV5sOKs1iH3kO8v
oMxdjzU503lO9rmQILXlREv2xPymDhARjAp+1+hjHthqCMpeMyEg/6Lu108pNRKLWcmiqEMaxRaK
8VSVfeJypzj7ctk78HeJrfImYZBqaiiQynV2NGHKrDlqIzw4n9YiaDRcNKA+SGxQaUBbitkyG/K5
M+RI0MjAK/JGOzZ6tuLzzGATRgB2lJUYLj3jEwCAxDoLscnELslDINYwLB9dijh+YjZdsfhhSr8C
hnvk5nM0Or4P6YsK6UTlWuwa7NFnTlU+13WxFTwxQcOyWiDIuBPeS17OqPNzX6d50Ot9RdqPPbIT
zn6DslZNgDCDIY9OA51gdIKPtNwy65bfYcu2FuD9X0aRG/OK/Ppzxe/8qga+922OZOrw12uNmh1M
ObgWuMq71PENP+jREUSKwAQtCmDBo1Ui+X75d+cdhb7lJ/c+cnWsW+t/m0WF8hOVP/bTcvkIzYXn
0mT0SGgIHF65G82SmATxg51lqdrUtp3aTtBSmBFSAcAjpNe1HJzAqRqlOCPuykaxS5lA+uqaFjLb
T47BAnqw0YZRaJIGNZQXHVeE4zC4JQ4P7N58ga7bmDwXjYy11kVeoOSLGHt3RvBJj8HkXsv7Al62
CfLh5egt+qNYJVJ8g6r6HD95cW4dQ/gSyG1TFoIoUPg5OndNUpZrKx9OtsQtOS93Osg/HWAK8OTN
FZF7kqK4uiKoGQHdMKjpV2DoKw8xDiSRVDz3TQGs0XClHQbiuePVpvM/r1fQavuiZ7WC7aLzDCqX
Gf2awr7uy635rlPOHr2q/yrdoH7xe42QHjywo9qrrZUIIa9m6/kNFm2imJYb7BscTvSAT2sEYcZu
pymKh6lRKCZFXrSJJ1o4P9StnSAkNlYiAKmcOmfE5hGDjhWzMfAkRT4hxeAIuN96mHFTCnjMlPQd
6cKK20/9KL5aFhJ07aeRxzCEUG55hkKwLATDKC/3TUH8zrj0zsZIwF2fpuGkORfDCyAMsqk4kev/
VaaFgUph2BebmmjTyrO/mhpnjG8lONUrvtepHduhorrBHMjLeeRktKnPFYBaVPI4YqL+EV9r/vCY
dG/Ez2FJ2XX3mBRXO2/L9NSq8N1R9qeW0R797S474onzt/7dBm41lTQiWRUkCiXLCeIWe/SdLcs2
rQ7mn4FT3jAs3EKssj7HQqfnlkW89qdbCsyOUf+R6SRQG34nUReRIcWJMRfOWWII3bajm9WF232P
WvfLmSEtojqrNfgmM4s/+iFzWKUhG52myPQsDyZVGib+pc2SKPhAR2LKg702wszXMQDnH/Ndgkpd
lkkoXJ/OUTDS89ERfyL1x63JWGhD2++njVLJvu+VKPtXls9CVCzK9HyNYijIndQghQ5BA7+LOFY/
xPnvWF53SBFer2/Y0Yg0gwAzCNDKOSWIIXTJFOFELbdARWVjSwPfo7eO4hL5MZqScx2jvshqN6gs
T3hhYgIVCFXVcAobtzf9Lnd2p4QGmG4JoZE/Ntysihvp0kJcOJsCvvq/UpN/AmdbRzvkBySYIOBQ
awMd1FDDo2/yKvbEFnJSgcpZ/LdBbMGTYpDWPaDD/y9V2PmWEFuG2LJ+FYPIazTw1HC+S/tUXhEY
yuibX2KJWYE/S9SMtArCj/LBjAZxAI9ID8XTabcwWVLw2ecPMA1IsGtzhN4vGtl1mORv0FhW4QPr
nvQbQDGVXQg6pStEB0b+f2J/3n4bYZkho2o3sxgkdB7elhmwr0Xhvqh3CHS4ByCvtFRpAktGEDQa
wzGACzE9QLGemNEuZ5cHhtWpgSttcCcm5Q66wxiW4nuBBIGZFg9fzxFszpyo2Pk3b7J1A72KLVNW
wchHkC06V6laOU4lEynjrmCFu52W7ZNTaIhwiIGXDnIC11bMTnDkXSQh+VfzOk6qoLg/0MIYWL1e
+Oq1VfR/O7bttT4aGbFjK3OeXKdEwrUnk5gm79D58eJouYkRo78MB4N/ESE5NKq+++SSi9ISyOKN
tPub5WPRsWIeCH4khk/VlHp+UboUBkJE9Mdb+HpX/+Z92PUleE3TbxiHUE+PFRLfb6mMbFzh4pgi
KfKuDzebTn7Gyj73lMP3NiYd7eHxSVRRbgShOWCKAQ6BJanR3ik2i0RtbShsIluynE20ME9aVsZb
JQjsXC0UqXF8eblxlAKfNDPPILS0YZDmJeJGNa0IR37O3kwXioG4DvY6kwssbVUum1T8k9ObH+RQ
NluXq1SUl7sagT0TCA7YdsI/UNBsldZAD98udzlnhWlLmi6hW25VqaqBS7in0EJ5ql8n3TGcxwqs
179BmQHll+mFa7sepc7QBMZwbqSUayKQ1nofymwkAm48+urtZMNLKBZ0HHubmu7Fp7xAAPuArNM4
TsYsnWBPxQqpW+2e83SXjwjcc5ls6dL9k916Va5dFJDPgPQeYpt24a3ZNlANjvPJ4pt1Ozp8uZJs
3dV8z2jrgf5ubZ+ORkyhuEIW/nuMd1zT+USxpNFWYUPIzmxjsohUMaH4WzZOiy3DyB6blXIkqBIv
tKW3S3hX1omUYzxP7d7UIf+sSqBqqUL+holwDEdq3A5JDjIG3ErPAt2sBJ8NZq5B5g8cD38dkX0p
CIcw1xJIfrZxogGdMjVDltY7NNMDO3JXkXrSfqxEDfS8GOGwEENHiZsVxKmoT0tdriXZosYVutiv
Q+jALwN8q9zkA+u8cXu/xHyKm8msFfMmL+ixvncWOx8te8qI9EE7tYryZ8BHxypGXgCrLtR7T5QL
iTg+gZ3vfd9zGV9+3iSlHS0qbKgDfRoojDrluq892fgEPffU8SwYwkTuHKdBHSK811MLEx4Z+/lf
NBM+GZQaU5cC5rYMldzIqm6RwJtAM0EYpEGV9vbOIvejud4mrM4n6E7wPzSXL0sSW58lHXiXJlCl
bTFkacB5VECpK/prLhvTvQp63b1GzF22J2l8L3Ofq1FjAFRBG5bM8GvE26mVKfim1bf7FE4YGVyN
NSWoI9QWpj7yJlVCJEbXo8exRIj1MJCnfmvmWJ+f+M2YthDK1QVc+a/OFrFGRKKo7zrO5X6PewS2
P/EC8JSkgXp492r8pX7V91MkXnrGAoE5aL4SD181Lv+Nr+PBilORs7u73WYNqi9W0RAly7GKN+F2
uPREyE22SKmiyjz+8oFX0uZ8rdzqLvPtIrCjvB4lemmhwDU97ujI3rt27RpkRRoMziQOOPxSjCrA
yAbUl2XaUxZ6z7NgCNZ0h38TN24kvB9rpr+dIOMgHw+OwqyliYOkoeKn+a0VMIRCDaleKQwxhBev
ALqz7SnaIoLJlLqtjskRP5lRDS6HX8AF1uEwRT5Xe/dj1m3E4zg70u5ui3USfTp5w7JPQeAxKJvD
uOW4o3CtCQy5pdNj3/3yzSuQBQ2x2syUF1rpS2dkKAYwRVQLlo+OMXMClwV/AsVbrHfmnL8L75re
/23XqWdZd7nJo4WaEwkzXqrreI/jG49VuHLG/xcd2/vRP0zxZ8z0lllcVb7UnfGZLIxuun+QDZQj
sRxjn4kDw/yHiJnxbm2EbO6Wcy0/BettyjAgYxv8OIH8FpzoqL9KsuomIiwPCRuiQ2QWMpvQB9xz
Wg9qK3jmrpeibIftxS9Ui2J8u+LmgxS7W5x4dPSACepakHbYuSGhm/RdngNCgzdi+FVqsBI5/RbJ
nM50ijVxCc7L7IHmNMp+/rSmMIfg2ojItKliYDrKPpx6sIopcjEtcdqFyAcnufrKCEIsMX2cW7Pj
dXIba0Y2R/+6JWyZQj2bcu1tlPe5vJ/maSph0H7NkCLfOfNOLqSXTHgjt50RPgY/vZxC+opi6goK
yn+9jKUeL8FzTYRD/M+OYaC7FRZeunKccFk/bKHDmPNC1M84cRzgHh1hH1g0f6v/LW8op2guQgsH
WcBd2eNeugZQoUiocqbjv04Iek+5RVfSQCPX1eTVLpiKV9pblriqLwKU2afkkyEKtCPcRS6LKyz3
kWzbZC8MLfgnLsM5KDHd+0GBFgWVLaZkeQSM6/G/M2Nhv/3pLCBLdtDcQvJIRcQk/BRQZWDgjopE
TSL9rxzGVDp/brIFhTWZOm6mkL1F6XtwDsqdUq9KeGMmnXy2YxaRJAxuWeV8qoc8DTG9g4vr++z1
K2UVSoc7n9R+8Re/kfxmZi0AdTn6H1jfcejjVWaNo7DZc03gag4RTuqSnSExeBDTekgSjbMKNV4p
iDKXpCwtUlJVMXJzgfiGD74Ny5Q4aHlwq7xGScx/Sbs5C86w+AT1dVM5qGt1B3w1kNHmN0swSkyG
fesN/c5Z//q+xUrXiVVMyoM4FQtQDbIT7Gm/KCHw/Z62fu9FMNUZKfQFX3lPZvjT2t25VL1LUV8R
1lfXzO/m1DJg/y5FZ4OcWL/H4XJica6tOeetc64ueX+3U0E4Q1/jqh4QGEdC4NMyAy9zb6i5q3cA
GjNQl1Q0tk5jGqmMYlom6KwmEqRTxhMNrdCL6jrviUKZjS4qAI+FeBVwwVz96egolRBp3pz9IXPa
P9PsENc+2KQns/aVm3glcuhrbdRPjKPdVFeFJ9n3qfihZeDfmK/wQl3AZb0b0IbFdVusW2vE4WYl
xVezXFvmHO3l85oEEKiuLcK8sVTPF3Mqlkv1CagxV8Eu5t/kfeRyxPLgaYTKcQrX65TVw1WavEEY
7C4snM6iDvoZ9YtB1wCKsZ8xQxt3VcSy2E1dyIF5ydk2ihT5l9aAwfhgTO05S/l/dBTw6kWQaXoj
VUPyRH/kKV5kba5F1zbVTzn86aUA3P1cZLZso5dnjpsKTTLoo+ps6RYZHWh/gadVHnCeoDC5sBXx
E1Zsbm8/AdFFRhkTQsTcUrNRrvvhPPFsYJrlYzqPi9x/mtGquwIacsHs4FMDkdebzGfJGGL5RbvW
iQ187xygIr9+9oY1BlFAkCvSUhIFXp9vcruP3UwRCEiGc8jmdnikL4FBzdw5BqqnP0XtIi5hMzmA
L/Xf+b0TS7saOfLgMpyfoqkEStQtVJHIXLTFD86M5bDY+EVrbG63ZYL0YByBEqDRo+IJQBYaEV1s
FGwzqwjWmeSSy0DO3dNOkykFBREajVdQG2NKYOVDUbm5TknEnH1W2gx6CLGb8FUIInLfY4WLl6dJ
f4uWAViEi0ylNi31xGPJqCuN73L+U27knBMAHFgOZybFrxzGfEn8AseXu7JItX/HM7V4i2KlylwZ
QOxTQGz7LE5K5rNWn7JE/40j5oMZzWVO+pknMXZsLEdxLzBHlQGLIKVvl5bz3jdSCuylztwoD0df
rmhzDO84/V0TPPcx1cjnReyUNFStNk5LPuoWQfAmJ/y8DrRZUEBa7lMoO843H/tSil+eRyqwPa5x
kE+HoSYEii+GmFNhWPC1+Pq0BaOpgXYeir19kGpScjjUydBabiVwJwJ24j6huC0a/IJF2opDVU6a
TKPuTWwN/squOjzttkNOUMGij8vjutcG5RVoJv4XDqeoxTHhPOLD/RRP/Zfg4TEYGsejx82K/Zub
PeKkGAZGq+ZJWMMVsiVQT+X6E2Z7bf5f+woZq8pSFrOBSHVu5fYrIWXYn0kVRD6kEhphA9MLEO2+
ktr+R81gNXxdsjwL3pvGohcKPsu0i63f9xh9Tusx0QdWQ8+E+f2FqU26zpbXCkOeIhWQB9Qfl/2f
/OIveATnGQncfEnE2wZd9+Ywv3JttEpMJkCVNigA1y6ntuuibFgd+EE6Fq27kaL2tAm5S/5OsCuO
ekaPElmq2GRyIe+RTYGgaOLFe68qU3B3xX5a/sNuGoHop8eYbZAhiFP1LiQOLIvXi7glnEgz1XRH
pGZLe/PoEULx8gxp/DyL6blrAFBdVNYsFOBopV6BbVQlInijYOGi+v4yxurHp+QELSojNU0BwKnR
i2/cr+gH1FQqklP6T2O9MuiJqXYAxEOYUtO0TgZyw8Mb6kcC/ad17KOepSbexpAg0Ljb+vRLjZBX
U+Xa9axA0X3wvkTTr42mrpes4VzCOWUeZZkwTUEJPplpT4HckQoMv9NhAmWv6eBVZt9qHoKhDj3P
eF+wsLEoqkZyKgVMM1zfCNqMrtwlj/bEMOHMWK5eNwn9eGwnzumhStCfDEXFZLMkuU3FmYFMH0ul
SaJ46Ky+nFqdzXioZdL2nWJ3owct9J9SB8TNsHbg/0npJFmVfYmP+bDPc+TO+QJPnxhrbFjPVKJ+
5Z82Owg3pJls09c9GwmCauhSUttGrWKtdwO0JpWV3tb6sNOcIrSXnTMo945GFoIz5cXWmNjoO94F
hEdaHLuIoOA8AKVpQ8tBCUycHtJqwUCxp6gQPaBxTUia5R9dP13yiHDWe30zq0t+O3b48Vjr+tg8
0Ve8Lg29ZR+vtKDwL97wljnisDpNUjUQ3mF+GB+pY8eIxLFY+F1hUUUKrJ+G+84Tnmwrpe9VzH4r
8fjwXZ6kmVZcd9/W8XeUTg9fYOmY9FtlR19G3pd27iQ5xH44f8mq+C0xt/sKJX6dRM9kn1oNN4YI
mzDWoa3014PyZXo4LFJPRc9xkq7QvRF0utIr/G/c4BgAUnyBqFAsR6CrVYW/u0FFISe9d+IH0j0K
cm+M8p7DdWZ2jvBi3F5gTna5UiE2jVZxzXdT/N0TIGOphzNUSydrpde0gFj5LA55hJ88dAJK3zGi
Nf3xE2dsLXd5769FzwiFCgH70CDzr9a5SXYflSpZ3iiGsQuyeyxKyfbxeRc48JcrI0Q6VJtaAyss
doI8oW35Q3QdHqtWpVv1j1gVYtxSGwPOabd6Ml4AgU+ZErMg/1jnkz/sT9RDuWuBEaF/LG00uMM4
htpQOrwYmMh1v3E/BbpQaGIFkcnw9NycmlICa+DWBEqdYB8wr4uuybwTGmPekvKWXJ7MlS2wpjLL
vdF8dTZARPRoyCZXakqkiK98jk5faqA4uxf/jLD3rv0xkTecg5Uu44NSdGULvKjOM8hlHfzp1/DP
4xm+dAXGQlyG+oHoaGjKrhEhsK5quWqw1WuX4AvI/TGoEq+niZkH7FFQU4xTTWrglHQZeDik/XAN
K1S58yYbu2nYotVmaOfMoFQaR9BbY+HTzAwgI+GfD8RwRYZ07IxCNf+vOkQeQUnEZBzOyyAHTWWI
fbsbijAyjbtswqB+JzSCiLkx5rLBfsZ9Ri3YI5TynwTV6Co7/gmP9+JvknKJDbidMTCa/bHnOx4o
pgXqJZc5Ol1hMy6fqOW9jkaj5+ASKpit2RAbWXuOSXqKq5GjhuMjefDePblDV63cEeWMiMGmlS0l
3N3VYlKtvKKd9EMqYh4N7CIzhu5jBjF1OJ1CweytFLe7VauoZdhak0jVVngPNjlDIHYoKgTBR1zH
+TSIkmteJnJy5MOpi7FkMG86MMh4DmmGQOPUVXixCrN7gda6MYSV2OYSAvwOpEHv7o082/5NRM4T
hrCDdOytZyiHrfZeZnMs/6gxCpAk2vitQAbgvJIPFv/QlAJOywL7G2p85m405EOW3Y1F/rR+Sqhq
34zXRu7qeru1vPrA4lO3+vVsXCisDWsBc94tH/O3V9BvFTNcvUsfyuI1pCn1bplgzc2+Q479Ka6b
3CloF5d6cq/SoawdFlzc/+Q2vCZYyWwiYgwtq1OFhhvcfyAm1YeHfluPKsSbCGuGGw/CMffqatUE
rS5jCK5U9bCxXEE1J892hXP/PVyNIZCD8/V+bjvofW9f11zF7BoVuc3E3gu6y4U1/gAGBVkDxn/c
l++sXJT8Q7DLOQn11WkfoRX5DoFAnRUM1K8AP4gYzlD9Tqf8dkWAlyD0z+ctcPeEAVLnG99yh0u5
oyRxNBpLJ/ASmu9XUMWJRkpYx5dCoKGodHiPZDRR8MTbBKBWFNf1EwmOmO04mg8dODVfSXC92uzJ
Umj1fu6eRVnzmVfZTTlJVA0ChlnBr9Lsa9WNTfHF6b0KBHINCmVNNaH57SR0wvLOsnJDAe8LPdcS
gyzhaEeolfL9JcqFFpnLGyNMAIXhEWCBFdX7kJ/1SkNsUUjn7F8f5oPRv2MhcF593+tx6itTwQVK
uax2f9QBm1nX8vCKWqH8LcK6+sS+/YHcKxQk5G5CMumCzW2JlYcevt6ZGXhDfgD7s0TKmqd3LsPN
NUysy621AZlldHt/8auVz3udCS6M/0xrhDwxVpIlMS5N/1UiYk7TcYklgdCOzP5H7DbyV6ks4/lw
04wuHwl/XZrVUt2BJzxUh82YDbcmeJAtGXlRgni/+xK3L60Z5jmtuXzr81Zpk1LEzfV4vBNha1it
hYKe00dRD3/oDx9vbQZ2C8TgRRYElh/i+vzaLDkJv/rrGe3ZRfIrIaHm5GB7NvcdXFB64SAvEfa0
97W5cniyNW6nPGTsAOpddAgxPBiYlDEoVi8hRVdmkb3ztmagT52NSYyc5ydzec2vqnXRfvFcZ3JA
+3IhUZmYN1ik6GO7CrvuqtLmkTwHelP2CVjScW/2aSVOBweBx/dMNjKmbC5kl05t8qABU25Gfxj9
dhf0X4knGmyZwOViMsI4tKm72rZ3t7UPxxdS0ZiAD5lU0wJ0HNtgzbrmcqAzPX7S+VYYMW2A5Jvp
s96OtL21jZB0cTcBGr3kFJgLZeNqIiBhurtBsiYIbQdx1uj3VwDnlzJKYpqIQr9eO1Ie4JeXFp+Z
Y+s36JiByL9CYFkoh8ZqwF6/i4p4w0CoqAK5e0tFqqbAHLb7onsJg9ue6Z0x1ovyQkgP9T6Q0T/O
i7J9K14uqk/7CUVspVc0xADPur7YoLGknDGVXxKiC869WB3FHb7WBbj0zlJYsqoa0MjHQe0vIg9s
Bk6SGbBzzMQk2CpYHtBdfdghy1H7jwmi1sRKvTKSbVproiSxspoyiGTARnV4oOFzhDwrKKjnOEY1
mC0zUqMAsUF5uzETO55VRSeC3cKvTTP7ejAaskqdSCd9xsRdsGPNW6tgl4InmeZAERcnPfZA6EQz
cXjpAQ2G5tmfQoGNPyC4f7RiXUcWFvxglI62qGLK1nFYbbF7KoOwhmDH5W5UVWjvOk4NwhfICCwQ
d1uTtnkj3qvKLuqi7osUBAIS6ZgQ/GGrKfhGXMghV6TfYe3xeq3zV4C0kFlZX8HR6JW6qudRgu92
5vfmU6CKBnNCifrMOuw+uIAkXSCtYcBl+yZFNirKbukUgDZ/qfUQ2rBdQg1mFrcaQVHLhDQgHtuy
aIIqqTbkq2zJh88TO2/BWV/5gui8iOBHgsDA7k5jcsJhWgh03RfYe3dq5pJ65/rWC0z9NxhAUJED
Ig7NZGdrQrxbEWVUsGehH5AEQR3IktbawRxgcCtKXshKozng3QnXr2WOF5ErCdcg3egQzcObQJvr
jijsJ+GaAnd59LPgtHCkrhWarLjWDxqsBWCK331KVI8k5U0n3Ti/JdHZzQDOtgHnkvz8eCXyY4qL
nT2cFqEIp+jge4dXF7NQqwR7fkgrBbeow6BPMB5k/lQM1QJV44bXyPJtpH7X9CGIc/YOuvOWpWdh
lvTnfRKqHDbiLtI1XdsaKvqX65MMIfDTxjcGacOgmUJqE8glNOgo4TjtQldQsF1Kk8ZK1EETPYjc
4vWOr1uzfftOmlW16aJEz4mNLEFMXLgAZpexQyLTw3Uodle0sz/QAs2GGlnhqOTR0aKPtvIfAii6
+pMOV25n2RnBsHO0+0Zwnr9nznDigwlh1noolRTlXkAx2r88uO4ayt1Zv4vg1gjSRwfcklBlr3dy
hMRUNh8rEztKDecuJaDWOWHpGavu3WJuMsJEg0zCEqYACEMFjwvdfW+zc/rXQ6lZkq76hKoOhBsa
lFwbmsejliSAZjxvp1tP2fxOSwOnFLpHZXSP31JSSYeGqNaYSn2QvlCTwjI/eNIS+IV4coom6CJr
HZiaubSDKpXx0YzbZBBsAvl68Ul5GIekDgJVVbTqWNG7xPTN7UhGpk4u/SMwLlqw7PnCuSGeKz4R
Mj3UBI8Flcj3u45wCtReMWiNtS2QEd2fZUzf76UO2ZrkG0xPW+NsVNs/Gs7HR1Q3NNH5YZrE42uP
N8Tjx07p5ZN/481m2zepp/0o13m8BLGXLD1cTVnOuf85/FJsta3HbaUfPT6i8NLbb+XIDWJf31c2
ZxJI8eBEFCmStvEhxDc+jmOeJthQEavWuWR3Ep9FZYDB04lHnrbNbtObLKUza3TB/alOB3o5MwV/
iTCGORiPrmbiKVlMvyEUke57MC6NVZ6mBtdGoMvdiRlP28XgUY52bDLsfyPHxk07M0nLz9Q88vA8
0f3uf5RfmVEM7IFAFX7mzkMgwiApFAoH0oiTJeDAJfL9jVs8WbnMkRJg5qsDuAlwDRf5KUTr+nhy
hZsCOrnbVNcpuH7F9KDLUEKhjQFfUn6Dmlo+qseqHXNqKvmKu0a7qicez64BtNHnt6HzSfGEbCeE
vcDscqNy28dIaw72Pl0Qu7nrrlZ/G2yeydEiVLTYYqdWNJgRAAfSnT3o4K/nRpSyxCic5u2YrGeF
QCgoqcPhUUT+mI74fYyT7fF5cXnRJl0mCfTwDCFhhPdG+8bJtpxCPMLV8b73Wy/tZ6yuLUucyb2q
2e8TfZPR4aAkLbiX+YHtDhZMuZXd3tQy6gqCuORADPlCrKeuC5MvNX3vcoOzxRYFuTFBweeaipGM
woylSbZ37vPYZd4oJmQmbKUNpvbrn2HwKpACaaZFIWEdJB4cNS5G49+W6jB/n2rDr7ic0DTD+fmM
Xyb/K577fPmK4i2LLXmtU67rJJp3spURkySZNqyofOHCexFN3BZ0JZStM1N1SK7b5AdWcMRSG6zU
jX2ie+qHmE58MfvE6nFjSKSDskAzHPCLdmoSBFmA5T9p+jbLSAH/kgF+eeYmjTuxyIB1pkiRtjIX
XEGlvRdY+JSkrRoTAwhvV0IfeOospyLtrkr9dh3R9I7TwgQFeVR+/3ijtLnKs3ZDDrHFUHVY0fIX
asSbHQNz4ivLNocgTVHxNGDiIPzxBY96OJLjmuL7pEZpiZ7Pbv2VQq0jlbX/GgFxPKxDI5Aeo8hZ
xXBL9VTFfydc+DFjwWzCYa+an+HRu81yLtKVv0qT1TrOb4qyI/a1Nbf/k7w+yWob94qfIJunZlba
aN1owM2SRKAwHFl2qSbD5XiuWIwC/bNb2WnzQ6fpo/6+rLsQyuEsicoP0qLZyyXk0mDv1MB7UScb
BPtjE6DrLrMhggqSt/VD
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
