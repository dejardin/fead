// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// Copyright 2022-2023 Advanced Micro Devices, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2023.2 (lin64) Build 4029153 Fri Oct 13 20:13:54 MDT 2023
// Date        : Wed Jan 17 16:42:36 2024
// Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 38 (Thirty Eight)
// Command     : write_verilog -force -mode synth_stub
//               /data/cms/ecal/fe/fead_v2/firmware.2023.2/FEAD.gen/sources_1/ip/c_shift_ram_0/c_shift_ram_0_stub.v
// Design      : c_shift_ram_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k70tfbg484-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "c_shift_ram_v12_0_15,Vivado 2023.2" *)
module c_shift_ram_0(D, CLK, Q)
/* synthesis syn_black_box black_box_pad_pin="D[159:0],Q[159:0]" */
/* synthesis syn_force_seq_prim="CLK" */;
  input [159:0]D;
  input CLK /* synthesis syn_isclock = 1 */;
  output [159:0]Q;
endmodule
