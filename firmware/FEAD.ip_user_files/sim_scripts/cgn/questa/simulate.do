onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib gig_eth_pcs_pma_gmii_to_sgmii_bridge_opt

do {wave.do}

view wave
view structure
view signals

do {gig_eth_pcs_pma_gmii_to_sgmii_bridge.udo}

run -all

quit -force
