onbreak {quit -f}
onerror {quit -f}

vsim -voptargs="+acc" -t 1ps -L xpm -L unisims_ver -L unimacro_ver -L secureip -lib xil_defaultlib xil_defaultlib.gig_eth_pcs_pma_gmii_to_sgmii_bridge xil_defaultlib.glbl

do {wave.do}

view wave
view structure
view signals

do {gig_eth_pcs_pma_gmii_to_sgmii_bridge.udo}

run -all

quit -force
