onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+gig_eth_pcs_pma_gmii_to_sgmii_bridge -L xpm -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.gig_eth_pcs_pma_gmii_to_sgmii_bridge xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {gig_eth_pcs_pma_gmii_to_sgmii_bridge.udo}

run -all

endsim

quit -force
