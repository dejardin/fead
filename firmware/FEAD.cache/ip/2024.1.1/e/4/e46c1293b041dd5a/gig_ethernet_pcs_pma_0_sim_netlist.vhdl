-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- Copyright 2022-2024 Advanced Micro Devices, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2024.1.1 (lin64) Build 5094488 Fri Jun 14 08:57:50 MDT 2024
-- Date        : Tue Jul 30 12:07:49 2024
-- Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 38 (Thirty Eight)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ gig_ethernet_pcs_pma_0_sim_netlist.vhdl
-- Design      : gig_ethernet_pcs_pma_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7k70tfbg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_GT is
  port (
    gtxe2_i_0 : out STD_LOGIC;
    gt0_cpllrefclklost_i : out STD_LOGIC;
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    gtxe2_i_1 : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    gtxe2_i_2 : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_4 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_5 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_7 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    cpll_pd0_i : in STD_LOGIC;
    cpllreset_in : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_gttxreset_in0_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gt0_rxuserrdy_t : in STD_LOGIC;
    gtxe2_i_8 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_txuserrdy_t : in STD_LOGIC;
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_9 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_10 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_11 : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_GT;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_GT is
  signal gtxe2_i_n_0 : STD_LOGIC;
  signal gtxe2_i_n_10 : STD_LOGIC;
  signal gtxe2_i_n_16 : STD_LOGIC;
  signal gtxe2_i_n_170 : STD_LOGIC;
  signal gtxe2_i_n_171 : STD_LOGIC;
  signal gtxe2_i_n_172 : STD_LOGIC;
  signal gtxe2_i_n_173 : STD_LOGIC;
  signal gtxe2_i_n_174 : STD_LOGIC;
  signal gtxe2_i_n_175 : STD_LOGIC;
  signal gtxe2_i_n_176 : STD_LOGIC;
  signal gtxe2_i_n_177 : STD_LOGIC;
  signal gtxe2_i_n_178 : STD_LOGIC;
  signal gtxe2_i_n_179 : STD_LOGIC;
  signal gtxe2_i_n_180 : STD_LOGIC;
  signal gtxe2_i_n_181 : STD_LOGIC;
  signal gtxe2_i_n_182 : STD_LOGIC;
  signal gtxe2_i_n_183 : STD_LOGIC;
  signal gtxe2_i_n_184 : STD_LOGIC;
  signal gtxe2_i_n_27 : STD_LOGIC;
  signal gtxe2_i_n_3 : STD_LOGIC;
  signal gtxe2_i_n_38 : STD_LOGIC;
  signal gtxe2_i_n_39 : STD_LOGIC;
  signal gtxe2_i_n_4 : STD_LOGIC;
  signal gtxe2_i_n_46 : STD_LOGIC;
  signal gtxe2_i_n_47 : STD_LOGIC;
  signal gtxe2_i_n_48 : STD_LOGIC;
  signal gtxe2_i_n_49 : STD_LOGIC;
  signal gtxe2_i_n_50 : STD_LOGIC;
  signal gtxe2_i_n_51 : STD_LOGIC;
  signal gtxe2_i_n_52 : STD_LOGIC;
  signal gtxe2_i_n_53 : STD_LOGIC;
  signal gtxe2_i_n_54 : STD_LOGIC;
  signal gtxe2_i_n_55 : STD_LOGIC;
  signal gtxe2_i_n_56 : STD_LOGIC;
  signal gtxe2_i_n_57 : STD_LOGIC;
  signal gtxe2_i_n_58 : STD_LOGIC;
  signal gtxe2_i_n_59 : STD_LOGIC;
  signal gtxe2_i_n_60 : STD_LOGIC;
  signal gtxe2_i_n_61 : STD_LOGIC;
  signal gtxe2_i_n_81 : STD_LOGIC;
  signal gtxe2_i_n_83 : STD_LOGIC;
  signal gtxe2_i_n_84 : STD_LOGIC;
  signal gtxe2_i_n_9 : STD_LOGIC;
  signal NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_PHYSTATUS_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXDATAVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXELECIDLE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXQPISENN_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXQPISENP_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXRATEDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXQPISENN_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXQPISENP_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXRATEDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXCHARISK_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXCHBONDO_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gtxe2_i_RXDATA_UNCONNECTED : STD_LOGIC_VECTOR ( 63 downto 16 );
  signal NLW_gtxe2_i_RXDISPERR_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXHEADER_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gtxe2_i_RXSTATUS_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_gtxe2_i_TSTOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  attribute box_type : string;
  attribute box_type of gtxe2_i : label is "PRIMITIVE";
begin
gtxe2_i: unisim.vcomponents.GTXE2_CHANNEL
    generic map(
      ALIGN_COMMA_DOUBLE => "FALSE",
      ALIGN_COMMA_ENABLE => B"0001111111",
      ALIGN_COMMA_WORD => 2,
      ALIGN_MCOMMA_DET => "TRUE",
      ALIGN_MCOMMA_VALUE => B"1010000011",
      ALIGN_PCOMMA_DET => "TRUE",
      ALIGN_PCOMMA_VALUE => B"0101111100",
      CBCC_DATA_SOURCE_SEL => "DECODED",
      CHAN_BOND_KEEP_ALIGN => "FALSE",
      CHAN_BOND_MAX_SKEW => 1,
      CHAN_BOND_SEQ_1_1 => B"0000000000",
      CHAN_BOND_SEQ_1_2 => B"0000000000",
      CHAN_BOND_SEQ_1_3 => B"0000000000",
      CHAN_BOND_SEQ_1_4 => B"0000000000",
      CHAN_BOND_SEQ_1_ENABLE => B"1111",
      CHAN_BOND_SEQ_2_1 => B"0000000000",
      CHAN_BOND_SEQ_2_2 => B"0000000000",
      CHAN_BOND_SEQ_2_3 => B"0000000000",
      CHAN_BOND_SEQ_2_4 => B"0000000000",
      CHAN_BOND_SEQ_2_ENABLE => B"1111",
      CHAN_BOND_SEQ_2_USE => "FALSE",
      CHAN_BOND_SEQ_LEN => 1,
      CLK_CORRECT_USE => "TRUE",
      CLK_COR_KEEP_IDLE => "FALSE",
      CLK_COR_MAX_LAT => 36,
      CLK_COR_MIN_LAT => 33,
      CLK_COR_PRECEDENCE => "TRUE",
      CLK_COR_REPEAT_WAIT => 0,
      CLK_COR_SEQ_1_1 => B"0110111100",
      CLK_COR_SEQ_1_2 => B"0001010000",
      CLK_COR_SEQ_1_3 => B"0000000000",
      CLK_COR_SEQ_1_4 => B"0000000000",
      CLK_COR_SEQ_1_ENABLE => B"1111",
      CLK_COR_SEQ_2_1 => B"0110111100",
      CLK_COR_SEQ_2_2 => B"0010110101",
      CLK_COR_SEQ_2_3 => B"0000000000",
      CLK_COR_SEQ_2_4 => B"0000000000",
      CLK_COR_SEQ_2_ENABLE => B"1111",
      CLK_COR_SEQ_2_USE => "TRUE",
      CLK_COR_SEQ_LEN => 2,
      CPLL_CFG => X"BC07DC",
      CPLL_FBDIV => 4,
      CPLL_FBDIV_45 => 5,
      CPLL_INIT_CFG => X"00001E",
      CPLL_LOCK_CFG => X"01E8",
      CPLL_REFCLK_DIV => 1,
      DEC_MCOMMA_DETECT => "TRUE",
      DEC_PCOMMA_DETECT => "TRUE",
      DEC_VALID_COMMA_ONLY => "FALSE",
      DMONITOR_CFG => X"000A00",
      ES_CONTROL => B"000000",
      ES_ERRDET_EN => "FALSE",
      ES_EYE_SCAN_EN => "TRUE",
      ES_HORZ_OFFSET => X"000",
      ES_PMA_CFG => B"0000000000",
      ES_PRESCALE => B"00000",
      ES_QUALIFIER => X"00000000000000000000",
      ES_QUAL_MASK => X"00000000000000000000",
      ES_SDATA_MASK => X"00000000000000000000",
      ES_VERT_OFFSET => B"000000000",
      FTS_DESKEW_SEQ_ENABLE => B"1111",
      FTS_LANE_DESKEW_CFG => B"1111",
      FTS_LANE_DESKEW_EN => "FALSE",
      GEARBOX_MODE => B"000",
      IS_CPLLLOCKDETCLK_INVERTED => '0',
      IS_DRPCLK_INVERTED => '0',
      IS_GTGREFCLK_INVERTED => '0',
      IS_RXUSRCLK2_INVERTED => '0',
      IS_RXUSRCLK_INVERTED => '0',
      IS_TXPHDLYTSTCLK_INVERTED => '0',
      IS_TXUSRCLK2_INVERTED => '0',
      IS_TXUSRCLK_INVERTED => '0',
      OUTREFCLK_SEL_INV => B"11",
      PCS_PCIE_EN => "FALSE",
      PCS_RSVD_ATTR => X"000000000000",
      PD_TRANS_TIME_FROM_P2 => X"03C",
      PD_TRANS_TIME_NONE_P2 => X"19",
      PD_TRANS_TIME_TO_P2 => X"64",
      PMA_RSV => X"00018480",
      PMA_RSV2 => X"2050",
      PMA_RSV3 => B"00",
      PMA_RSV4 => X"00000000",
      RXBUFRESET_TIME => B"00001",
      RXBUF_ADDR_MODE => "FULL",
      RXBUF_EIDLE_HI_CNT => B"1000",
      RXBUF_EIDLE_LO_CNT => B"0000",
      RXBUF_EN => "TRUE",
      RXBUF_RESET_ON_CB_CHANGE => "TRUE",
      RXBUF_RESET_ON_COMMAALIGN => "FALSE",
      RXBUF_RESET_ON_EIDLE => "FALSE",
      RXBUF_RESET_ON_RATE_CHANGE => "TRUE",
      RXBUF_THRESH_OVFLW => 61,
      RXBUF_THRESH_OVRD => "FALSE",
      RXBUF_THRESH_UNDFLW => 8,
      RXCDRFREQRESET_TIME => B"00001",
      RXCDRPHRESET_TIME => B"00001",
      RXCDR_CFG => X"03000023FF10100020",
      RXCDR_FR_RESET_ON_EIDLE => '0',
      RXCDR_HOLD_DURING_EIDLE => '0',
      RXCDR_LOCK_CFG => B"010101",
      RXCDR_PH_RESET_ON_EIDLE => '0',
      RXDFELPMRESET_TIME => B"0001111",
      RXDLY_CFG => X"001F",
      RXDLY_LCFG => X"030",
      RXDLY_TAP_CFG => X"0000",
      RXGEARBOX_EN => "FALSE",
      RXISCANRESET_TIME => B"00001",
      RXLPM_HF_CFG => B"00000011110000",
      RXLPM_LF_CFG => B"00000011110000",
      RXOOB_CFG => B"0000110",
      RXOUT_DIV => 4,
      RXPCSRESET_TIME => B"00001",
      RXPHDLY_CFG => X"084020",
      RXPH_CFG => X"000000",
      RXPH_MONITOR_SEL => B"00000",
      RXPMARESET_TIME => B"00011",
      RXPRBS_ERR_LOOPBACK => '0',
      RXSLIDE_AUTO_WAIT => 7,
      RXSLIDE_MODE => "OFF",
      RX_BIAS_CFG => B"000000000100",
      RX_BUFFER_CFG => B"000000",
      RX_CLK25_DIV => 5,
      RX_CLKMUX_PD => '1',
      RX_CM_SEL => B"11",
      RX_CM_TRIM => B"010",
      RX_DATA_WIDTH => 20,
      RX_DDI_SEL => B"000000",
      RX_DEBUG_CFG => B"000000000000",
      RX_DEFER_RESET_BUF_EN => "TRUE",
      RX_DFE_GAIN_CFG => X"020FEA",
      RX_DFE_H2_CFG => B"000000000000",
      RX_DFE_H3_CFG => B"000001000000",
      RX_DFE_H4_CFG => B"00011110000",
      RX_DFE_H5_CFG => B"00011100000",
      RX_DFE_KL_CFG => B"0000011111110",
      RX_DFE_KL_CFG2 => X"301148AC",
      RX_DFE_LPM_CFG => X"0904",
      RX_DFE_LPM_HOLD_DURING_EIDLE => '0',
      RX_DFE_UT_CFG => B"10001111000000000",
      RX_DFE_VP_CFG => B"00011111100000011",
      RX_DFE_XYD_CFG => B"0000000000000",
      RX_DISPERR_SEQ_MATCH => "TRUE",
      RX_INT_DATAWIDTH => 0,
      RX_OS_CFG => B"0000010000000",
      RX_SIG_VALID_DLY => 10,
      RX_XCLK_SEL => "RXREC",
      SAS_MAX_COM => 64,
      SAS_MIN_COM => 36,
      SATA_BURST_SEQ_LEN => B"0101",
      SATA_BURST_VAL => B"100",
      SATA_CPLL_CFG => "VCO_3000MHZ",
      SATA_EIDLE_VAL => B"100",
      SATA_MAX_BURST => 8,
      SATA_MAX_INIT => 21,
      SATA_MAX_WAKE => 7,
      SATA_MIN_BURST => 4,
      SATA_MIN_INIT => 12,
      SATA_MIN_WAKE => 4,
      SHOW_REALIGN_COMMA => "TRUE",
      SIM_CPLLREFCLK_SEL => B"001",
      SIM_RECEIVER_DETECT_PASS => "TRUE",
      SIM_RESET_SPEEDUP => "TRUE",
      SIM_TX_EIDLE_DRIVE_LEVEL => "X",
      SIM_VERSION => "4.0",
      TERM_RCAL_CFG => B"10000",
      TERM_RCAL_OVRD => '0',
      TRANS_TIME_RATE => X"0E",
      TST_RSV => X"00000000",
      TXBUF_EN => "TRUE",
      TXBUF_RESET_ON_RATE_CHANGE => "TRUE",
      TXDLY_CFG => X"001F",
      TXDLY_LCFG => X"030",
      TXDLY_TAP_CFG => X"0000",
      TXGEARBOX_EN => "FALSE",
      TXOUT_DIV => 4,
      TXPCSRESET_TIME => B"00001",
      TXPHDLY_CFG => X"084020",
      TXPH_CFG => X"0780",
      TXPH_MONITOR_SEL => B"00000",
      TXPMARESET_TIME => B"00001",
      TX_CLK25_DIV => 5,
      TX_CLKMUX_PD => '1',
      TX_DATA_WIDTH => 20,
      TX_DEEMPH0 => B"00000",
      TX_DEEMPH1 => B"00000",
      TX_DRIVE_MODE => "DIRECT",
      TX_EIDLE_ASSERT_DELAY => B"110",
      TX_EIDLE_DEASSERT_DELAY => B"100",
      TX_INT_DATAWIDTH => 0,
      TX_LOOPBACK_DRIVE_HIZ => "FALSE",
      TX_MAINCURSOR_SEL => '0',
      TX_MARGIN_FULL_0 => B"1001110",
      TX_MARGIN_FULL_1 => B"1001001",
      TX_MARGIN_FULL_2 => B"1000101",
      TX_MARGIN_FULL_3 => B"1000010",
      TX_MARGIN_FULL_4 => B"1000000",
      TX_MARGIN_LOW_0 => B"1000110",
      TX_MARGIN_LOW_1 => B"1000100",
      TX_MARGIN_LOW_2 => B"1000010",
      TX_MARGIN_LOW_3 => B"1000000",
      TX_MARGIN_LOW_4 => B"1000000",
      TX_PREDRIVER_MODE => '0',
      TX_QPI_STATUS_EN => '0',
      TX_RXDETECT_CFG => X"1832",
      TX_RXDETECT_REF => B"100",
      TX_XCLK_SEL => "TXOUT",
      UCODEER_CLR => '0'
    )
        port map (
      CFGRESET => '0',
      CLKRSVD(3 downto 0) => B"0000",
      CPLLFBCLKLOST => gtxe2_i_n_0,
      CPLLLOCK => gtxe2_i_0,
      CPLLLOCKDETCLK => independent_clock_bufg,
      CPLLLOCKEN => '1',
      CPLLPD => cpll_pd0_i,
      CPLLREFCLKLOST => gt0_cpllrefclklost_i,
      CPLLREFCLKSEL(2 downto 0) => B"001",
      CPLLRESET => cpllreset_in,
      DMONITOROUT(7) => gtxe2_i_n_177,
      DMONITOROUT(6) => gtxe2_i_n_178,
      DMONITOROUT(5) => gtxe2_i_n_179,
      DMONITOROUT(4) => gtxe2_i_n_180,
      DMONITOROUT(3) => gtxe2_i_n_181,
      DMONITOROUT(2) => gtxe2_i_n_182,
      DMONITOROUT(1) => gtxe2_i_n_183,
      DMONITOROUT(0) => gtxe2_i_n_184,
      DRPADDR(8 downto 0) => B"000000000",
      DRPCLK => gtrefclk_bufg,
      DRPDI(15 downto 0) => B"0000000000000000",
      DRPDO(15) => gtxe2_i_n_46,
      DRPDO(14) => gtxe2_i_n_47,
      DRPDO(13) => gtxe2_i_n_48,
      DRPDO(12) => gtxe2_i_n_49,
      DRPDO(11) => gtxe2_i_n_50,
      DRPDO(10) => gtxe2_i_n_51,
      DRPDO(9) => gtxe2_i_n_52,
      DRPDO(8) => gtxe2_i_n_53,
      DRPDO(7) => gtxe2_i_n_54,
      DRPDO(6) => gtxe2_i_n_55,
      DRPDO(5) => gtxe2_i_n_56,
      DRPDO(4) => gtxe2_i_n_57,
      DRPDO(3) => gtxe2_i_n_58,
      DRPDO(2) => gtxe2_i_n_59,
      DRPDO(1) => gtxe2_i_n_60,
      DRPDO(0) => gtxe2_i_n_61,
      DRPEN => '0',
      DRPRDY => gtxe2_i_n_3,
      DRPWE => '0',
      EYESCANDATAERROR => gtxe2_i_n_4,
      EYESCANMODE => '0',
      EYESCANRESET => '0',
      EYESCANTRIGGER => '0',
      GTGREFCLK => '0',
      GTNORTHREFCLK0 => '0',
      GTNORTHREFCLK1 => '0',
      GTREFCLK0 => gtrefclk_out,
      GTREFCLK1 => '0',
      GTREFCLKMONITOR => NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED,
      GTRESETSEL => '0',
      GTRSVD(15 downto 0) => B"0000000000000000",
      GTRXRESET => SR(0),
      GTSOUTHREFCLK0 => '0',
      GTSOUTHREFCLK1 => '0',
      GTTXRESET => gt0_gttxreset_in0_out,
      GTXRXN => rxn,
      GTXRXP => rxp,
      GTXTXN => txn,
      GTXTXP => txp,
      LOOPBACK(2 downto 0) => B"000",
      PCSRSVDIN(15 downto 0) => B"0000000000000000",
      PCSRSVDIN2(4 downto 0) => B"00000",
      PCSRSVDOUT(15 downto 0) => NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED(15 downto 0),
      PHYSTATUS => NLW_gtxe2_i_PHYSTATUS_UNCONNECTED,
      PMARSVDIN(4 downto 0) => B"00000",
      PMARSVDIN2(4 downto 0) => B"00000",
      QPLLCLK => gt0_qplloutclk_out,
      QPLLREFCLK => gt0_qplloutrefclk_out,
      RESETOVRD => '0',
      RX8B10BEN => '1',
      RXBUFRESET => '0',
      RXBUFSTATUS(2) => RXBUFSTATUS(0),
      RXBUFSTATUS(1) => gtxe2_i_n_83,
      RXBUFSTATUS(0) => gtxe2_i_n_84,
      RXBYTEISALIGNED => gtxe2_i_n_9,
      RXBYTEREALIGN => gtxe2_i_n_10,
      RXCDRFREQRESET => '0',
      RXCDRHOLD => '0',
      RXCDRLOCK => NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED,
      RXCDROVRDEN => '0',
      RXCDRRESET => '0',
      RXCDRRESETRSV => '0',
      RXCHANBONDSEQ => NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED,
      RXCHANISALIGNED => NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED,
      RXCHANREALIGN => NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED,
      RXCHARISCOMMA(7 downto 2) => NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED(7 downto 2),
      RXCHARISCOMMA(1 downto 0) => gtxe2_i_4(1 downto 0),
      RXCHARISK(7 downto 2) => NLW_gtxe2_i_RXCHARISK_UNCONNECTED(7 downto 2),
      RXCHARISK(1 downto 0) => gtxe2_i_5(1 downto 0),
      RXCHBONDEN => '0',
      RXCHBONDI(4 downto 0) => B"00000",
      RXCHBONDLEVEL(2 downto 0) => B"000",
      RXCHBONDMASTER => '0',
      RXCHBONDO(4 downto 0) => NLW_gtxe2_i_RXCHBONDO_UNCONNECTED(4 downto 0),
      RXCHBONDSLAVE => '0',
      RXCLKCORCNT(1 downto 0) => D(1 downto 0),
      RXCOMINITDET => NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED,
      RXCOMMADET => gtxe2_i_n_16,
      RXCOMMADETEN => '1',
      RXCOMSASDET => NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED,
      RXCOMWAKEDET => NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED,
      RXDATA(63 downto 16) => NLW_gtxe2_i_RXDATA_UNCONNECTED(63 downto 16),
      RXDATA(15 downto 0) => gtxe2_i_3(15 downto 0),
      RXDATAVALID => NLW_gtxe2_i_RXDATAVALID_UNCONNECTED,
      RXDDIEN => '0',
      RXDFEAGCHOLD => '0',
      RXDFEAGCOVRDEN => '0',
      RXDFECM1EN => '0',
      RXDFELFHOLD => '0',
      RXDFELFOVRDEN => '0',
      RXDFELPMRESET => '0',
      RXDFETAP2HOLD => '0',
      RXDFETAP2OVRDEN => '0',
      RXDFETAP3HOLD => '0',
      RXDFETAP3OVRDEN => '0',
      RXDFETAP4HOLD => '0',
      RXDFETAP4OVRDEN => '0',
      RXDFETAP5HOLD => '0',
      RXDFETAP5OVRDEN => '0',
      RXDFEUTHOLD => '0',
      RXDFEUTOVRDEN => '0',
      RXDFEVPHOLD => '0',
      RXDFEVPOVRDEN => '0',
      RXDFEVSEN => '0',
      RXDFEXYDEN => '1',
      RXDFEXYDHOLD => '0',
      RXDFEXYDOVRDEN => '0',
      RXDISPERR(7 downto 2) => NLW_gtxe2_i_RXDISPERR_UNCONNECTED(7 downto 2),
      RXDISPERR(1 downto 0) => gtxe2_i_6(1 downto 0),
      RXDLYBYPASS => '1',
      RXDLYEN => '0',
      RXDLYOVRDEN => '0',
      RXDLYSRESET => '0',
      RXDLYSRESETDONE => NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED,
      RXELECIDLE => NLW_gtxe2_i_RXELECIDLE_UNCONNECTED,
      RXELECIDLEMODE(1 downto 0) => B"11",
      RXGEARBOXSLIP => '0',
      RXHEADER(2 downto 0) => NLW_gtxe2_i_RXHEADER_UNCONNECTED(2 downto 0),
      RXHEADERVALID => NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED,
      RXLPMEN => '1',
      RXLPMHFHOLD => '0',
      RXLPMHFOVRDEN => '0',
      RXLPMLFHOLD => '0',
      RXLPMLFKLOVRDEN => '0',
      RXMCOMMAALIGNEN => reset_out,
      RXMONITOROUT(6) => gtxe2_i_n_170,
      RXMONITOROUT(5) => gtxe2_i_n_171,
      RXMONITOROUT(4) => gtxe2_i_n_172,
      RXMONITOROUT(3) => gtxe2_i_n_173,
      RXMONITOROUT(2) => gtxe2_i_n_174,
      RXMONITOROUT(1) => gtxe2_i_n_175,
      RXMONITOROUT(0) => gtxe2_i_n_176,
      RXMONITORSEL(1 downto 0) => B"00",
      RXNOTINTABLE(7 downto 2) => NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED(7 downto 2),
      RXNOTINTABLE(1 downto 0) => gtxe2_i_7(1 downto 0),
      RXOOBRESET => '0',
      RXOSHOLD => '0',
      RXOSOVRDEN => '0',
      RXOUTCLK => rxoutclk,
      RXOUTCLKFABRIC => NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED,
      RXOUTCLKPCS => NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED,
      RXOUTCLKSEL(2 downto 0) => B"010",
      RXPCOMMAALIGNEN => reset_out,
      RXPCSRESET => reset,
      RXPD(1) => RXPD(0),
      RXPD(0) => RXPD(0),
      RXPHALIGN => '0',
      RXPHALIGNDONE => NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED,
      RXPHALIGNEN => '0',
      RXPHDLYPD => '0',
      RXPHDLYRESET => '0',
      RXPHMONITOR(4 downto 0) => NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED(4 downto 0),
      RXPHOVRDEN => '0',
      RXPHSLIPMONITOR(4 downto 0) => NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED(4 downto 0),
      RXPMARESET => '0',
      RXPOLARITY => '0',
      RXPRBSCNTRESET => '0',
      RXPRBSERR => gtxe2_i_n_27,
      RXPRBSSEL(2 downto 0) => B"000",
      RXQPIEN => '0',
      RXQPISENN => NLW_gtxe2_i_RXQPISENN_UNCONNECTED,
      RXQPISENP => NLW_gtxe2_i_RXQPISENP_UNCONNECTED,
      RXRATE(2 downto 0) => B"000",
      RXRATEDONE => NLW_gtxe2_i_RXRATEDONE_UNCONNECTED,
      RXRESETDONE => gtxe2_i_1,
      RXSLIDE => '0',
      RXSTARTOFSEQ => NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED,
      RXSTATUS(2 downto 0) => NLW_gtxe2_i_RXSTATUS_UNCONNECTED(2 downto 0),
      RXSYSCLKSEL(1 downto 0) => B"00",
      RXUSERRDY => gt0_rxuserrdy_t,
      RXUSRCLK => gtxe2_i_8,
      RXUSRCLK2 => gtxe2_i_8,
      RXVALID => NLW_gtxe2_i_RXVALID_UNCONNECTED,
      SETERRSTATUS => '0',
      TSTIN(19 downto 0) => B"11111111111111111111",
      TSTOUT(9 downto 0) => NLW_gtxe2_i_TSTOUT_UNCONNECTED(9 downto 0),
      TX8B10BBYPASS(7 downto 0) => B"00000000",
      TX8B10BEN => '1',
      TXBUFDIFFCTRL(2 downto 0) => B"100",
      TXBUFSTATUS(1) => TXBUFSTATUS(0),
      TXBUFSTATUS(0) => gtxe2_i_n_81,
      TXCHARDISPMODE(7 downto 2) => B"000000",
      TXCHARDISPMODE(1 downto 0) => gtxe2_i_9(1 downto 0),
      TXCHARDISPVAL(7 downto 2) => B"000000",
      TXCHARDISPVAL(1 downto 0) => gtxe2_i_10(1 downto 0),
      TXCHARISK(7 downto 2) => B"000000",
      TXCHARISK(1 downto 0) => gtxe2_i_11(1 downto 0),
      TXCOMFINISH => NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED,
      TXCOMINIT => '0',
      TXCOMSAS => '0',
      TXCOMWAKE => '0',
      TXDATA(63 downto 16) => B"000000000000000000000000000000000000000000000000",
      TXDATA(15 downto 0) => Q(15 downto 0),
      TXDEEMPH => '0',
      TXDETECTRX => '0',
      TXDIFFCTRL(3 downto 0) => B"1000",
      TXDIFFPD => '0',
      TXDLYBYPASS => '1',
      TXDLYEN => '0',
      TXDLYHOLD => '0',
      TXDLYOVRDEN => '0',
      TXDLYSRESET => '0',
      TXDLYSRESETDONE => NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED,
      TXDLYUPDOWN => '0',
      TXELECIDLE => TXPD(0),
      TXGEARBOXREADY => NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED,
      TXHEADER(2 downto 0) => B"000",
      TXINHIBIT => '0',
      TXMAINCURSOR(6 downto 0) => B"0000000",
      TXMARGIN(2 downto 0) => B"000",
      TXOUTCLK => txoutclk,
      TXOUTCLKFABRIC => gtxe2_i_n_38,
      TXOUTCLKPCS => gtxe2_i_n_39,
      TXOUTCLKSEL(2 downto 0) => B"100",
      TXPCSRESET => '0',
      TXPD(1) => TXPD(0),
      TXPD(0) => TXPD(0),
      TXPDELECIDLEMODE => '0',
      TXPHALIGN => '0',
      TXPHALIGNDONE => NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED,
      TXPHALIGNEN => '0',
      TXPHDLYPD => '0',
      TXPHDLYRESET => '0',
      TXPHDLYTSTCLK => '0',
      TXPHINIT => '0',
      TXPHINITDONE => NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED,
      TXPHOVRDEN => '0',
      TXPISOPD => '0',
      TXPMARESET => '0',
      TXPOLARITY => '0',
      TXPOSTCURSOR(4 downto 0) => B"00000",
      TXPOSTCURSORINV => '0',
      TXPRBSFORCEERR => '0',
      TXPRBSSEL(2 downto 0) => B"000",
      TXPRECURSOR(4 downto 0) => B"00000",
      TXPRECURSORINV => '0',
      TXQPIBIASEN => '0',
      TXQPISENN => NLW_gtxe2_i_TXQPISENN_UNCONNECTED,
      TXQPISENP => NLW_gtxe2_i_TXQPISENP_UNCONNECTED,
      TXQPISTRONGPDOWN => '0',
      TXQPIWEAKPUP => '0',
      TXRATE(2 downto 0) => B"000",
      TXRATEDONE => NLW_gtxe2_i_TXRATEDONE_UNCONNECTED,
      TXRESETDONE => gtxe2_i_2,
      TXSEQUENCE(6 downto 0) => B"0000000",
      TXSTARTSEQ => '0',
      TXSWING => '0',
      TXSYSCLKSEL(1 downto 0) => B"00",
      TXUSERRDY => gt0_txuserrdy_t,
      TXUSRCLK => gtxe2_i_8,
      TXUSRCLK2 => gtxe2_i_8
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_clocking is
  port (
    gtrefclk_out : out STD_LOGIC;
    gtrefclk_bufg : out STD_LOGIC;
    mmcm_locked : out STD_LOGIC;
    userclk : out STD_LOGIC;
    userclk2 : out STD_LOGIC;
    rxuserclk2_out : out STD_LOGIC;
    gtrefclk_p : in STD_LOGIC;
    gtrefclk_n : in STD_LOGIC;
    txoutclk : in STD_LOGIC;
    mmcm_reset : in STD_LOGIC;
    rxoutclk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_clocking;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_clocking is
  signal clkfbout : STD_LOGIC;
  signal clkout0 : STD_LOGIC;
  signal clkout1 : STD_LOGIC;
  signal \^gtrefclk_out\ : STD_LOGIC;
  signal txoutclk_bufg : STD_LOGIC;
  signal NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_PSDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute box_type : string;
  attribute box_type of bufg_gtrefclk : label is "PRIMITIVE";
  attribute box_type of bufg_txoutclk : label is "PRIMITIVE";
  attribute box_type of bufg_userclk : label is "PRIMITIVE";
  attribute box_type of bufg_userclk2 : label is "PRIMITIVE";
  attribute box_type of ibufds_gtrefclk : label is "PRIMITIVE";
  attribute box_type of mmcm_adv_inst : label is "PRIMITIVE";
  attribute box_type of rxrecclkbufg : label is "PRIMITIVE";
begin
  gtrefclk_out <= \^gtrefclk_out\;
bufg_gtrefclk: unisim.vcomponents.BUFG
     port map (
      I => \^gtrefclk_out\,
      O => gtrefclk_bufg
    );
bufg_txoutclk: unisim.vcomponents.BUFG
     port map (
      I => txoutclk,
      O => txoutclk_bufg
    );
bufg_userclk: unisim.vcomponents.BUFG
     port map (
      I => clkout1,
      O => userclk
    );
bufg_userclk2: unisim.vcomponents.BUFG
     port map (
      I => clkout0,
      O => userclk2
    );
ibufds_gtrefclk: unisim.vcomponents.IBUFDS_GTE2
    generic map(
      CLKCM_CFG => true,
      CLKRCV_TRST => true,
      CLKSWING_CFG => B"11"
    )
        port map (
      CEB => '0',
      I => gtrefclk_p,
      IB => gtrefclk_n,
      O => \^gtrefclk_out\,
      ODIV2 => NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED
    );
mmcm_adv_inst: unisim.vcomponents.MMCME2_ADV
    generic map(
      BANDWIDTH => "OPTIMIZED",
      CLKFBOUT_MULT_F => 16.000000,
      CLKFBOUT_PHASE => 0.000000,
      CLKFBOUT_USE_FINE_PS => false,
      CLKIN1_PERIOD => 16.000000,
      CLKIN2_PERIOD => 0.000000,
      CLKOUT0_DIVIDE_F => 8.000000,
      CLKOUT0_DUTY_CYCLE => 0.500000,
      CLKOUT0_PHASE => 0.000000,
      CLKOUT0_USE_FINE_PS => false,
      CLKOUT1_DIVIDE => 16,
      CLKOUT1_DUTY_CYCLE => 0.500000,
      CLKOUT1_PHASE => 0.000000,
      CLKOUT1_USE_FINE_PS => false,
      CLKOUT2_DIVIDE => 1,
      CLKOUT2_DUTY_CYCLE => 0.500000,
      CLKOUT2_PHASE => 0.000000,
      CLKOUT2_USE_FINE_PS => false,
      CLKOUT3_DIVIDE => 1,
      CLKOUT3_DUTY_CYCLE => 0.500000,
      CLKOUT3_PHASE => 0.000000,
      CLKOUT3_USE_FINE_PS => false,
      CLKOUT4_CASCADE => false,
      CLKOUT4_DIVIDE => 1,
      CLKOUT4_DUTY_CYCLE => 0.500000,
      CLKOUT4_PHASE => 0.000000,
      CLKOUT4_USE_FINE_PS => false,
      CLKOUT5_DIVIDE => 1,
      CLKOUT5_DUTY_CYCLE => 0.500000,
      CLKOUT5_PHASE => 0.000000,
      CLKOUT5_USE_FINE_PS => false,
      CLKOUT6_DIVIDE => 1,
      CLKOUT6_DUTY_CYCLE => 0.500000,
      CLKOUT6_PHASE => 0.000000,
      CLKOUT6_USE_FINE_PS => false,
      COMPENSATION => "INTERNAL",
      DIVCLK_DIVIDE => 1,
      IS_CLKINSEL_INVERTED => '0',
      IS_PSEN_INVERTED => '0',
      IS_PSINCDEC_INVERTED => '0',
      IS_PWRDWN_INVERTED => '0',
      IS_RST_INVERTED => '0',
      REF_JITTER1 => 0.010000,
      REF_JITTER2 => 0.000000,
      SS_EN => "FALSE",
      SS_MODE => "CENTER_HIGH",
      SS_MOD_PERIOD => 10000,
      STARTUP_WAIT => false
    )
        port map (
      CLKFBIN => clkfbout,
      CLKFBOUT => clkfbout,
      CLKFBOUTB => NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED,
      CLKFBSTOPPED => NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED,
      CLKIN1 => txoutclk_bufg,
      CLKIN2 => '0',
      CLKINSEL => '1',
      CLKINSTOPPED => NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED,
      CLKOUT0 => clkout0,
      CLKOUT0B => NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED,
      CLKOUT1 => clkout1,
      CLKOUT1B => NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED,
      CLKOUT2 => NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED,
      CLKOUT2B => NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED,
      CLKOUT3 => NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED,
      CLKOUT3B => NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED,
      CLKOUT4 => NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED,
      CLKOUT5 => NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED,
      CLKOUT6 => NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED,
      DADDR(6 downto 0) => B"0000000",
      DCLK => '0',
      DEN => '0',
      DI(15 downto 0) => B"0000000000000000",
      DO(15 downto 0) => NLW_mmcm_adv_inst_DO_UNCONNECTED(15 downto 0),
      DRDY => NLW_mmcm_adv_inst_DRDY_UNCONNECTED,
      DWE => '0',
      LOCKED => mmcm_locked,
      PSCLK => '0',
      PSDONE => NLW_mmcm_adv_inst_PSDONE_UNCONNECTED,
      PSEN => '0',
      PSINCDEC => '0',
      PWRDWN => '0',
      RST => mmcm_reset
    );
rxrecclkbufg: unisim.vcomponents.BUFG
     port map (
      I => rxoutclk,
      O => rxuserclk2_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_cpll_railing is
  port (
    cpll_pd0_i : out STD_LOGIC;
    cpllreset_in : out STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gt0_cpllreset_t : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_cpll_railing;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_cpll_railing is
  signal cpll_reset_out : STD_LOGIC;
  signal \cpllpd_wait_reg[31]_srl32_n_1\ : STD_LOGIC;
  signal \cpllpd_wait_reg[63]_srl32_n_1\ : STD_LOGIC;
  signal \cpllpd_wait_reg[94]_srl31_n_0\ : STD_LOGIC;
  signal \cpllreset_wait_reg[126]_srl31_n_0\ : STD_LOGIC;
  signal \cpllreset_wait_reg[31]_srl32_n_1\ : STD_LOGIC;
  signal \cpllreset_wait_reg[63]_srl32_n_1\ : STD_LOGIC;
  signal \cpllreset_wait_reg[95]_srl32_n_1\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  attribute srl_bus_name : string;
  attribute srl_bus_name of \cpllpd_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name : string;
  attribute srl_name of \cpllpd_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[31]_srl32 ";
  attribute srl_bus_name of \cpllpd_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name of \cpllpd_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[63]_srl32 ";
  attribute srl_bus_name of \cpllpd_wait_reg[94]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name of \cpllpd_wait_reg[94]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[94]_srl31 ";
  attribute equivalent_register_removal : string;
  attribute equivalent_register_removal of \cpllpd_wait_reg[95]\ : label is "no";
  attribute srl_bus_name of \cpllreset_wait_reg[126]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[126]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[126]_srl31 ";
  attribute equivalent_register_removal of \cpllreset_wait_reg[127]\ : label is "no";
  attribute srl_bus_name of \cpllreset_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[31]_srl32 ";
  attribute srl_bus_name of \cpllreset_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[63]_srl32 ";
  attribute srl_bus_name of \cpllreset_wait_reg[95]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[95]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[95]_srl32 ";
begin
\cpllpd_wait_reg[31]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"FFFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => '0',
      Q => \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllpd_wait_reg[31]_srl32_n_1\
    );
\cpllpd_wait_reg[63]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"FFFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllpd_wait_reg[31]_srl32_n_1\,
      Q => \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllpd_wait_reg[63]_srl32_n_1\
    );
\cpllpd_wait_reg[94]_srl31\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11110",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllpd_wait_reg[63]_srl32_n_1\,
      Q => \cpllpd_wait_reg[94]_srl31_n_0\,
      Q31 => \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED\
    );
\cpllpd_wait_reg[95]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => gtrefclk_bufg,
      CE => '1',
      D => \cpllpd_wait_reg[94]_srl31_n_0\,
      Q => cpll_pd0_i,
      R => '0'
    );
\cpllreset_wait_reg[126]_srl31\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11110",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllreset_wait_reg[95]_srl32_n_1\,
      Q => \cpllreset_wait_reg[126]_srl31_n_0\,
      Q31 => \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED\
    );
\cpllreset_wait_reg[127]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gtrefclk_bufg,
      CE => '1',
      D => \cpllreset_wait_reg[126]_srl31_n_0\,
      Q => cpll_reset_out,
      R => '0'
    );
\cpllreset_wait_reg[31]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"000000FF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => '0',
      Q => \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[31]_srl32_n_1\
    );
\cpllreset_wait_reg[63]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllreset_wait_reg[31]_srl32_n_1\,
      Q => \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[63]_srl32_n_1\
    );
\cpllreset_wait_reg[95]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllreset_wait_reg[63]_srl32_n_1\,
      Q => \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[95]_srl32_n_1\
    );
gtxe2_i_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => cpll_reset_out,
      I1 => gt0_cpllreset_t,
      O => cpllreset_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_gt_common is
  port (
    gt0_qplloutclk_out : out STD_LOGIC;
    gt0_qplloutrefclk_out : out STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_gt_common;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_gt_common is
  signal gtxe2_common_i_n_2 : STD_LOGIC;
  signal gtxe2_common_i_n_5 : STD_LOGIC;
  signal NLW_gtxe2_common_i_DRPRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_common_i_DRPDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute box_type : string;
  attribute box_type of gtxe2_common_i : label is "PRIMITIVE";
begin
gtxe2_common_i: unisim.vcomponents.GTXE2_COMMON
    generic map(
      BIAS_CFG => X"0000040000001000",
      COMMON_CFG => X"00000000",
      IS_DRPCLK_INVERTED => '0',
      IS_GTGREFCLK_INVERTED => '0',
      IS_QPLLLOCKDETCLK_INVERTED => '0',
      QPLL_CFG => X"06801C1",
      QPLL_CLKOUT_CFG => B"0000",
      QPLL_COARSE_FREQ_OVRD => B"010000",
      QPLL_COARSE_FREQ_OVRD_EN => '0',
      QPLL_CP => B"0000011111",
      QPLL_CP_MONITOR_EN => '0',
      QPLL_DMONITOR_SEL => '0',
      QPLL_FBDIV => B"0000100000",
      QPLL_FBDIV_MONITOR_EN => '0',
      QPLL_FBDIV_RATIO => '1',
      QPLL_INIT_CFG => X"000006",
      QPLL_LOCK_CFG => X"21E8",
      QPLL_LPF => B"1111",
      QPLL_REFCLK_DIV => 1,
      SIM_QPLLREFCLK_SEL => B"001",
      SIM_RESET_SPEEDUP => "FALSE",
      SIM_VERSION => "4.0"
    )
        port map (
      BGBYPASSB => '1',
      BGMONITORENB => '1',
      BGPDB => '1',
      BGRCALOVRD(4 downto 0) => B"11111",
      DRPADDR(7 downto 0) => B"00000000",
      DRPCLK => '0',
      DRPDI(15 downto 0) => B"0000000000000000",
      DRPDO(15 downto 0) => NLW_gtxe2_common_i_DRPDO_UNCONNECTED(15 downto 0),
      DRPEN => '0',
      DRPRDY => NLW_gtxe2_common_i_DRPRDY_UNCONNECTED,
      DRPWE => '0',
      GTGREFCLK => '0',
      GTNORTHREFCLK0 => '0',
      GTNORTHREFCLK1 => '0',
      GTREFCLK0 => gtrefclk_out,
      GTREFCLK1 => '0',
      GTSOUTHREFCLK0 => '0',
      GTSOUTHREFCLK1 => '0',
      PMARSVD(7 downto 0) => B"00000000",
      QPLLDMONITOR(7 downto 0) => NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED(7 downto 0),
      QPLLFBCLKLOST => NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED,
      QPLLLOCK => gtxe2_common_i_n_2,
      QPLLLOCKDETCLK => independent_clock_bufg,
      QPLLLOCKEN => '1',
      QPLLOUTCLK => gt0_qplloutclk_out,
      QPLLOUTREFCLK => gt0_qplloutrefclk_out,
      QPLLOUTRESET => '0',
      QPLLPD => '1',
      QPLLREFCLKLOST => gtxe2_common_i_n_5,
      QPLLREFCLKSEL(2 downto 0) => B"001",
      QPLLRESET => \out\(0),
      QPLLRSVD1(15 downto 0) => B"0000000000000000",
      QPLLRSVD2(4 downto 0) => B"11111",
      RCALENB => '1',
      REFCLKOUTMONITOR => NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync is
  port (
    reset_out : out STD_LOGIC;
    CLK : in STD_LOGIC;
    enablealign : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync is
  signal reset_sync_reg1 : STD_LOGIC;
  signal reset_sync_reg2 : STD_LOGIC;
  signal reset_sync_reg3 : STD_LOGIC;
  signal reset_sync_reg4 : STD_LOGIC;
  signal reset_sync_reg5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of reset_sync1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of reset_sync1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of reset_sync1 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of reset_sync1 : label is "VCC:CE";
  attribute box_type : string;
  attribute box_type of reset_sync1 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync2 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync2 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync2 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync2 : label is "VCC:CE";
  attribute box_type of reset_sync2 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync3 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync3 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync3 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync3 : label is "VCC:CE";
  attribute box_type of reset_sync3 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync4 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync4 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync4 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync4 : label is "VCC:CE";
  attribute box_type of reset_sync4 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync5 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync5 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync5 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync5 : label is "VCC:CE";
  attribute box_type of reset_sync5 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync6 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync6 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync6 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync6 : label is "VCC:CE";
  attribute box_type of reset_sync6 : label is "PRIMITIVE";
begin
reset_sync1: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => '0',
      PRE => enablealign,
      Q => reset_sync_reg1
    );
reset_sync2: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg1,
      PRE => enablealign,
      Q => reset_sync_reg2
    );
reset_sync3: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg2,
      PRE => enablealign,
      Q => reset_sync_reg3
    );
reset_sync4: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg3,
      PRE => enablealign,
      Q => reset_sync_reg4
    );
reset_sync5: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg4,
      PRE => enablealign,
      Q => reset_sync_reg5
    );
reset_sync6: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg5,
      PRE => '0',
      Q => reset_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_1 is
  port (
    reset_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_1 : entity is "gig_ethernet_pcs_pma_0_reset_sync";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_1;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_1 is
  signal reset_sync_reg1 : STD_LOGIC;
  signal reset_sync_reg2 : STD_LOGIC;
  signal reset_sync_reg3 : STD_LOGIC;
  signal reset_sync_reg4 : STD_LOGIC;
  signal reset_sync_reg5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of reset_sync1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of reset_sync1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of reset_sync1 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of reset_sync1 : label is "VCC:CE";
  attribute box_type : string;
  attribute box_type of reset_sync1 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync2 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync2 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync2 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync2 : label is "VCC:CE";
  attribute box_type of reset_sync2 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync3 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync3 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync3 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync3 : label is "VCC:CE";
  attribute box_type of reset_sync3 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync4 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync4 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync4 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync4 : label is "VCC:CE";
  attribute box_type of reset_sync4 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync5 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync5 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync5 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync5 : label is "VCC:CE";
  attribute box_type of reset_sync5 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync6 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync6 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync6 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync6 : label is "VCC:CE";
  attribute box_type of reset_sync6 : label is "PRIMITIVE";
begin
reset_sync1: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => '0',
      PRE => SR(0),
      Q => reset_sync_reg1
    );
reset_sync2: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg1,
      PRE => SR(0),
      Q => reset_sync_reg2
    );
reset_sync3: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg2,
      PRE => SR(0),
      Q => reset_sync_reg3
    );
reset_sync4: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg3,
      PRE => SR(0),
      Q => reset_sync_reg4
    );
reset_sync5: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg4,
      PRE => SR(0),
      Q => reset_sync_reg5
    );
reset_sync6: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg5,
      PRE => '0',
      Q => reset_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_2 is
  port (
    reset_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    reset_sync5_0 : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_2 : entity is "gig_ethernet_pcs_pma_0_reset_sync";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_2;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_2 is
  signal reset_sync_reg1 : STD_LOGIC;
  signal reset_sync_reg2 : STD_LOGIC;
  signal reset_sync_reg3 : STD_LOGIC;
  signal reset_sync_reg4 : STD_LOGIC;
  signal reset_sync_reg5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of reset_sync1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of reset_sync1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of reset_sync1 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of reset_sync1 : label is "VCC:CE";
  attribute box_type : string;
  attribute box_type of reset_sync1 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync2 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync2 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync2 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync2 : label is "VCC:CE";
  attribute box_type of reset_sync2 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync3 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync3 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync3 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync3 : label is "VCC:CE";
  attribute box_type of reset_sync3 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync4 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync4 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync4 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync4 : label is "VCC:CE";
  attribute box_type of reset_sync4 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync5 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync5 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync5 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync5 : label is "VCC:CE";
  attribute box_type of reset_sync5 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync6 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync6 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync6 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync6 : label is "VCC:CE";
  attribute box_type of reset_sync6 : label is "PRIMITIVE";
begin
reset_sync1: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => '0',
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg1
    );
reset_sync2: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg1,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg2
    );
reset_sync3: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg2,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg3
    );
reset_sync4: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg3,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg4
    );
reset_sync5: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg4,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg5
    );
reset_sync6: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg5,
      PRE => '0',
      Q => reset_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_wtd_timer is
  port (
    reset : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    data_out : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_wtd_timer;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_wtd_timer is
  signal \counter_stg1[5]_i_1_n_0\ : STD_LOGIC;
  signal \counter_stg1[5]_i_3_n_0\ : STD_LOGIC;
  signal counter_stg1_reg : STD_LOGIC_VECTOR ( 5 to 5 );
  signal \counter_stg1_reg__0\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \counter_stg2[0]_i_3_n_0\ : STD_LOGIC;
  signal counter_stg2_reg : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal \counter_stg2_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal counter_stg30 : STD_LOGIC;
  signal \counter_stg3[0]_i_3_n_0\ : STD_LOGIC;
  signal \counter_stg3[0]_i_4_n_0\ : STD_LOGIC;
  signal \counter_stg3[0]_i_5_n_0\ : STD_LOGIC;
  signal counter_stg3_reg : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal \counter_stg3_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal eqOp : STD_LOGIC;
  signal plusOp : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal reset0 : STD_LOGIC;
  signal reset_i_2_n_0 : STD_LOGIC;
  signal reset_i_3_n_0 : STD_LOGIC;
  signal reset_i_4_n_0 : STD_LOGIC;
  signal reset_i_5_n_0 : STD_LOGIC;
  signal reset_i_6_n_0 : STD_LOGIC;
  signal \NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \counter_stg1[0]_i_1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \counter_stg1[1]_i_1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \counter_stg1[2]_i_1\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \counter_stg1[3]_i_1\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \counter_stg1[4]_i_1\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \counter_stg1[5]_i_3\ : label is "soft_lutpair71";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \counter_stg2_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg2_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg2_reg[8]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg3_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg3_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg3_reg[8]_i_1\ : label is 11;
begin
\counter_stg1[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \counter_stg1_reg__0\(0),
      O => plusOp(0)
    );
\counter_stg1[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \counter_stg1_reg__0\(0),
      I1 => \counter_stg1_reg__0\(1),
      O => plusOp(1)
    );
\counter_stg1[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \counter_stg1_reg__0\(1),
      I1 => \counter_stg1_reg__0\(0),
      I2 => \counter_stg1_reg__0\(2),
      O => plusOp(2)
    );
\counter_stg1[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \counter_stg1_reg__0\(2),
      I1 => \counter_stg1_reg__0\(0),
      I2 => \counter_stg1_reg__0\(1),
      I3 => \counter_stg1_reg__0\(3),
      O => plusOp(3)
    );
\counter_stg1[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(3),
      I1 => \counter_stg1_reg__0\(1),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(2),
      I4 => \counter_stg1_reg__0\(4),
      O => plusOp(4)
    );
\counter_stg1[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF2000"
    )
        port map (
      I0 => reset_i_2_n_0,
      I1 => counter_stg3_reg(0),
      I2 => reset_i_3_n_0,
      I3 => \counter_stg1[5]_i_3_n_0\,
      I4 => data_out,
      O => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(4),
      I1 => \counter_stg1_reg__0\(2),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(1),
      I4 => \counter_stg1_reg__0\(3),
      I5 => counter_stg1_reg(5),
      O => plusOp(5)
    );
\counter_stg1[5]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(3),
      I1 => \counter_stg1_reg__0\(1),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(2),
      I4 => \counter_stg1_reg__0\(4),
      O => \counter_stg1[5]_i_3_n_0\
    );
\counter_stg1_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(0),
      Q => \counter_stg1_reg__0\(0),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(1),
      Q => \counter_stg1_reg__0\(1),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(2),
      Q => \counter_stg1_reg__0\(2),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(3),
      Q => \counter_stg1_reg__0\(3),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(4),
      Q => \counter_stg1_reg__0\(4),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(5),
      Q => counter_stg1_reg(5),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(4),
      I1 => \counter_stg1_reg__0\(2),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(1),
      I4 => \counter_stg1_reg__0\(3),
      I5 => counter_stg1_reg(5),
      O => eqOp
    );
\counter_stg2[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => counter_stg2_reg(0),
      O => \counter_stg2[0]_i_3_n_0\
    );
\counter_stg2_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_7\,
      Q => counter_stg2_reg(0),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \counter_stg2_reg[0]_i_2_n_0\,
      CO(2) => \counter_stg2_reg[0]_i_2_n_1\,
      CO(1) => \counter_stg2_reg[0]_i_2_n_2\,
      CO(0) => \counter_stg2_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \counter_stg2_reg[0]_i_2_n_4\,
      O(2) => \counter_stg2_reg[0]_i_2_n_5\,
      O(1) => \counter_stg2_reg[0]_i_2_n_6\,
      O(0) => \counter_stg2_reg[0]_i_2_n_7\,
      S(3 downto 1) => counter_stg2_reg(3 downto 1),
      S(0) => \counter_stg2[0]_i_3_n_0\
    );
\counter_stg2_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_5\,
      Q => counter_stg2_reg(10),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_4\,
      Q => counter_stg2_reg(11),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_6\,
      Q => counter_stg2_reg(1),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_5\,
      Q => counter_stg2_reg(2),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_4\,
      Q => counter_stg2_reg(3),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_7\,
      Q => counter_stg2_reg(4),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg2_reg[0]_i_2_n_0\,
      CO(3) => \counter_stg2_reg[4]_i_1_n_0\,
      CO(2) => \counter_stg2_reg[4]_i_1_n_1\,
      CO(1) => \counter_stg2_reg[4]_i_1_n_2\,
      CO(0) => \counter_stg2_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg2_reg[4]_i_1_n_4\,
      O(2) => \counter_stg2_reg[4]_i_1_n_5\,
      O(1) => \counter_stg2_reg[4]_i_1_n_6\,
      O(0) => \counter_stg2_reg[4]_i_1_n_7\,
      S(3 downto 0) => counter_stg2_reg(7 downto 4)
    );
\counter_stg2_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_6\,
      Q => counter_stg2_reg(5),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_5\,
      Q => counter_stg2_reg(6),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_4\,
      Q => counter_stg2_reg(7),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_7\,
      Q => counter_stg2_reg(8),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg2_reg[4]_i_1_n_0\,
      CO(3) => \NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \counter_stg2_reg[8]_i_1_n_1\,
      CO(1) => \counter_stg2_reg[8]_i_1_n_2\,
      CO(0) => \counter_stg2_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg2_reg[8]_i_1_n_4\,
      O(2) => \counter_stg2_reg[8]_i_1_n_5\,
      O(1) => \counter_stg2_reg[8]_i_1_n_6\,
      O(0) => \counter_stg2_reg[8]_i_1_n_7\,
      S(3 downto 0) => counter_stg2_reg(11 downto 8)
    );
\counter_stg2_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_6\,
      Q => counter_stg2_reg(9),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \counter_stg3[0]_i_3_n_0\,
      I1 => \counter_stg3[0]_i_4_n_0\,
      I2 => counter_stg2_reg(0),
      I3 => \counter_stg1[5]_i_3_n_0\,
      O => counter_stg30
    );
\counter_stg3[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => counter_stg2_reg(3),
      I1 => counter_stg2_reg(4),
      I2 => counter_stg2_reg(1),
      I3 => counter_stg2_reg(2),
      I4 => counter_stg2_reg(6),
      I5 => counter_stg2_reg(5),
      O => \counter_stg3[0]_i_3_n_0\
    );
\counter_stg3[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => counter_stg2_reg(9),
      I1 => counter_stg2_reg(10),
      I2 => counter_stg2_reg(7),
      I3 => counter_stg2_reg(8),
      I4 => counter_stg1_reg(5),
      I5 => counter_stg2_reg(11),
      O => \counter_stg3[0]_i_4_n_0\
    );
\counter_stg3[0]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => counter_stg3_reg(0),
      O => \counter_stg3[0]_i_5_n_0\
    );
\counter_stg3_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_7\,
      Q => counter_stg3_reg(0),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \counter_stg3_reg[0]_i_2_n_0\,
      CO(2) => \counter_stg3_reg[0]_i_2_n_1\,
      CO(1) => \counter_stg3_reg[0]_i_2_n_2\,
      CO(0) => \counter_stg3_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \counter_stg3_reg[0]_i_2_n_4\,
      O(2) => \counter_stg3_reg[0]_i_2_n_5\,
      O(1) => \counter_stg3_reg[0]_i_2_n_6\,
      O(0) => \counter_stg3_reg[0]_i_2_n_7\,
      S(3 downto 1) => counter_stg3_reg(3 downto 1),
      S(0) => \counter_stg3[0]_i_5_n_0\
    );
\counter_stg3_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_5\,
      Q => counter_stg3_reg(10),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_4\,
      Q => counter_stg3_reg(11),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_6\,
      Q => counter_stg3_reg(1),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_5\,
      Q => counter_stg3_reg(2),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_4\,
      Q => counter_stg3_reg(3),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_7\,
      Q => counter_stg3_reg(4),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg3_reg[0]_i_2_n_0\,
      CO(3) => \counter_stg3_reg[4]_i_1_n_0\,
      CO(2) => \counter_stg3_reg[4]_i_1_n_1\,
      CO(1) => \counter_stg3_reg[4]_i_1_n_2\,
      CO(0) => \counter_stg3_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg3_reg[4]_i_1_n_4\,
      O(2) => \counter_stg3_reg[4]_i_1_n_5\,
      O(1) => \counter_stg3_reg[4]_i_1_n_6\,
      O(0) => \counter_stg3_reg[4]_i_1_n_7\,
      S(3 downto 0) => counter_stg3_reg(7 downto 4)
    );
\counter_stg3_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_6\,
      Q => counter_stg3_reg(5),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_5\,
      Q => counter_stg3_reg(6),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_4\,
      Q => counter_stg3_reg(7),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_7\,
      Q => counter_stg3_reg(8),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg3_reg[4]_i_1_n_0\,
      CO(3) => \NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \counter_stg3_reg[8]_i_1_n_1\,
      CO(1) => \counter_stg3_reg[8]_i_1_n_2\,
      CO(0) => \counter_stg3_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg3_reg[8]_i_1_n_4\,
      O(2) => \counter_stg3_reg[8]_i_1_n_5\,
      O(1) => \counter_stg3_reg[8]_i_1_n_6\,
      O(0) => \counter_stg3_reg[8]_i_1_n_7\,
      S(3 downto 0) => counter_stg3_reg(11 downto 8)
    );
\counter_stg3_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_6\,
      Q => counter_stg3_reg(9),
      R => \counter_stg1[5]_i_1_n_0\
    );
reset_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => reset_i_2_n_0,
      I1 => counter_stg3_reg(0),
      I2 => reset_i_3_n_0,
      O => reset0
    );
reset_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000001000000000"
    )
        port map (
      I0 => counter_stg3_reg(9),
      I1 => counter_stg3_reg(10),
      I2 => counter_stg3_reg(7),
      I3 => counter_stg3_reg(8),
      I4 => counter_stg2_reg(0),
      I5 => counter_stg3_reg(11),
      O => reset_i_2_n_0
    );
reset_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => reset_i_4_n_0,
      I1 => reset_i_5_n_0,
      I2 => reset_i_6_n_0,
      O => reset_i_3_n_0
    );
reset_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000008"
    )
        port map (
      I0 => counter_stg2_reg(3),
      I1 => counter_stg2_reg(4),
      I2 => counter_stg2_reg(1),
      I3 => counter_stg2_reg(2),
      I4 => counter_stg2_reg(6),
      I5 => counter_stg2_reg(5),
      O => reset_i_4_n_0
    );
reset_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0020000000000000"
    )
        port map (
      I0 => counter_stg2_reg(10),
      I1 => counter_stg2_reg(9),
      I2 => counter_stg2_reg(8),
      I3 => counter_stg2_reg(7),
      I4 => counter_stg1_reg(5),
      I5 => counter_stg2_reg(11),
      O => reset_i_5_n_0
    );
reset_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0002000000000000"
    )
        port map (
      I0 => counter_stg3_reg(4),
      I1 => counter_stg3_reg(3),
      I2 => counter_stg3_reg(1),
      I3 => counter_stg3_reg(2),
      I4 => counter_stg3_reg(6),
      I5 => counter_stg3_reg(5),
      O => reset_i_6_n_0
    );
reset_reg: unisim.vcomponents.FDRE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset0,
      Q => reset,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_resets is
  port (
    \out\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    independent_clock_bufg : in STD_LOGIC;
    reset : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_resets;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_resets is
  signal pma_reset_pipe : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg : string;
  attribute async_reg of pma_reset_pipe : signal is "true";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \pma_reset_pipe_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[1]\ : label is std.standard.true;
  attribute KEEP of \pma_reset_pipe_reg[1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[2]\ : label is std.standard.true;
  attribute KEEP of \pma_reset_pipe_reg[2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[3]\ : label is std.standard.true;
  attribute KEEP of \pma_reset_pipe_reg[3]\ : label is "yes";
begin
  \out\(0) <= pma_reset_pipe(3);
\pma_reset_pipe_reg[0]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => '0',
      PRE => reset,
      Q => pma_reset_pipe(0)
    );
\pma_reset_pipe_reg[1]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pma_reset_pipe(0),
      PRE => reset,
      Q => pma_reset_pipe(1)
    );
\pma_reset_pipe_reg[2]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pma_reset_pipe(1),
      PRE => reset,
      Q => pma_reset_pipe(2)
    );
\pma_reset_pipe_reg[3]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pma_reset_pipe(2),
      PRE => reset,
      Q => pma_reset_pipe(3)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    CLK : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_0 is
  port (
    resetdone : out STD_LOGIC;
    resetdone_0 : in STD_LOGIC;
    data_in : in STD_LOGIC;
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_0 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_0 is
  signal data_out : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
resetdone_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => data_out,
      I1 => resetdone_0,
      O => resetdone
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_10 is
  port (
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_10 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_10;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_10 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_11 is
  port (
    \FSM_sequential_rx_state_reg[1]\ : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 2 downto 0 );
    rxresetdone_s3 : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_11 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_11;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_11 is
  signal cplllock_sync : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
\FSM_sequential_rx_state[3]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"008F0080"
    )
        port map (
      I0 => Q(0),
      I1 => rxresetdone_s3,
      I2 => Q(1),
      I3 => Q(2),
      I4 => cplllock_sync,
      O => \FSM_sequential_rx_state_reg[1]\
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => cplllock_sync,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_12 is
  port (
    \FSM_sequential_rx_state_reg[1]\ : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 2 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    reset_time_out_reg : in STD_LOGIC;
    reset_time_out_reg_0 : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    reset_time_out_reg_1 : in STD_LOGIC;
    reset_time_out_reg_2 : in STD_LOGIC;
    data_in : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[1]_0\ : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_0 : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_1 : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]\ : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_0\ : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_1\ : in STD_LOGIC;
    mmcm_lock_reclocked : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_2\ : in STD_LOGIC;
    time_out_wait_bypass_s3 : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[3]\ : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_3\ : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_2 : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_3 : in STD_LOGIC;
    data_out : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_12 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_12;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_12 is
  signal \FSM_sequential_rx_state[0]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[1]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_8_n_0\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  signal data_valid_sync : STD_LOGIC;
  signal reset_time_out_i_2_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_3_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_4_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[0]_i_3\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[3]_i_8\ : label is "soft_lutpair41";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
\FSM_sequential_rx_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEFEFEF"
    )
        port map (
      I0 => \FSM_sequential_rx_state_reg[0]_2\,
      I1 => \FSM_sequential_rx_state[0]_i_3_n_0\,
      I2 => Q(0),
      I3 => Q(1),
      I4 => Q(3),
      O => D(0)
    );
\FSM_sequential_rx_state[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => Q(3),
      I1 => reset_time_out_reg_2,
      I2 => data_valid_sync,
      I3 => rx_fsm_reset_done_int_reg_1,
      O => \FSM_sequential_rx_state[0]_i_3_n_0\
    );
\FSM_sequential_rx_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF24200400"
    )
        port map (
      I0 => Q(0),
      I1 => Q(1),
      I2 => Q(3),
      I3 => Q(2),
      I4 => \FSM_sequential_rx_state[1]_i_2_n_0\,
      I5 => \FSM_sequential_rx_state_reg[1]_0\,
      O => D(1)
    );
\FSM_sequential_rx_state[1]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => data_valid_sync,
      I1 => rx_fsm_reset_done_int_reg_1,
      O => \FSM_sequential_rx_state[1]_i_2_n_0\
    );
\FSM_sequential_rx_state[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFEEE"
    )
        port map (
      I0 => \FSM_sequential_rx_state_reg[0]\,
      I1 => \FSM_sequential_rx_state[3]_i_4_n_0\,
      I2 => Q(0),
      I3 => reset_time_out_reg,
      I4 => \FSM_sequential_rx_state[3]_i_6_n_0\,
      I5 => \FSM_sequential_rx_state_reg[0]_0\,
      O => E(0)
    );
\FSM_sequential_rx_state[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFCCC0C4C4"
    )
        port map (
      I0 => time_out_wait_bypass_s3,
      I1 => Q(3),
      I2 => Q(1),
      I3 => \FSM_sequential_rx_state[3]_i_8_n_0\,
      I4 => Q(0),
      I5 => \FSM_sequential_rx_state_reg[3]\,
      O => D(2)
    );
\FSM_sequential_rx_state[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAEFEA"
    )
        port map (
      I0 => \FSM_sequential_rx_state[0]_i_3_n_0\,
      I1 => \FSM_sequential_rx_state_reg[0]_1\,
      I2 => Q(2),
      I3 => \FSM_sequential_rx_state_reg[0]_3\,
      I4 => Q(0),
      I5 => Q(1),
      O => \FSM_sequential_rx_state[3]_i_4_n_0\
    );
\FSM_sequential_rx_state[3]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0CE20CCC"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => Q(3),
      I2 => data_valid_sync,
      I3 => Q(1),
      I4 => Q(0),
      O => \FSM_sequential_rx_state[3]_i_6_n_0\
    );
\FSM_sequential_rx_state[3]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => rx_fsm_reset_done_int_reg_1,
      I1 => data_valid_sync,
      I2 => reset_time_out_reg_2,
      O => \FSM_sequential_rx_state[3]_i_8_n_0\
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_out,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_valid_sync,
      R => '0'
    );
\reset_time_out_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEFFFFFEEEF0000"
    )
        port map (
      I0 => reset_time_out_i_2_n_0,
      I1 => reset_time_out_reg,
      I2 => reset_time_out_reg_0,
      I3 => Q(1),
      I4 => reset_time_out_reg_1,
      I5 => reset_time_out_reg_2,
      O => \FSM_sequential_rx_state_reg[1]\
    );
reset_time_out_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0FF30E0E0FF30202"
    )
        port map (
      I0 => \FSM_sequential_rx_state_reg[0]_1\,
      I1 => Q(0),
      I2 => Q(1),
      I3 => data_valid_sync,
      I4 => Q(3),
      I5 => mmcm_lock_reclocked,
      O => reset_time_out_i_2_n_0
    );
rx_fsm_reset_done_int_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"ABA8"
    )
        port map (
      I0 => rx_fsm_reset_done_int,
      I1 => rx_fsm_reset_done_int_i_3_n_0,
      I2 => rx_fsm_reset_done_int_i_4_n_0,
      I3 => data_in,
      O => rx_fsm_reset_done_int_reg
    );
rx_fsm_reset_done_int_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00040000"
    )
        port map (
      I0 => Q(0),
      I1 => data_valid_sync,
      I2 => Q(2),
      I3 => reset_time_out_reg_2,
      I4 => rx_fsm_reset_done_int_reg_2,
      O => rx_fsm_reset_done_int
    );
rx_fsm_reset_done_int_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0400040004040400"
    )
        port map (
      I0 => rx_fsm_reset_done_int_reg_0,
      I1 => Q(3),
      I2 => Q(2),
      I3 => data_valid_sync,
      I4 => rx_fsm_reset_done_int_reg_1,
      I5 => reset_time_out_reg_2,
      O => rx_fsm_reset_done_int_i_3_n_0
    );
rx_fsm_reset_done_int_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0008000808080008"
    )
        port map (
      I0 => rx_fsm_reset_done_int_reg_3,
      I1 => Q(1),
      I2 => Q(0),
      I3 => data_valid_sync,
      I4 => rx_fsm_reset_done_int_reg_2,
      I5 => reset_time_out_reg_2,
      O => rx_fsm_reset_done_int_i_4_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_13 is
  port (
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_13 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_13;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_13 is
  signal \^data_out\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
  data_out <= \^data_out\;
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => \^data_out\,
      R => '0'
    );
\mmcm_lock_count[7]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^data_out\,
      O => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_14 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_14 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_14;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_14 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_15 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_15 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_15;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_15 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_16 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg6_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_16 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_16;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_16 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_3 is
  port (
    data_out : out STD_LOGIC;
    status_vector : in STD_LOGIC_VECTOR ( 0 to 0 );
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_3 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_3;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_3 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => status_vector(0),
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_4 is
  port (
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_4 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_4;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_4 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_5 is
  port (
    reset_time_out_reg : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    reset_time_out_reg_0 : in STD_LOGIC;
    reset_time_out : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_0\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_1\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_2\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_3\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    reset_time_out_reg_1 : in STD_LOGIC;
    mmcm_lock_reclocked : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_4\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_5\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_6\ : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_5 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_5;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_5 is
  signal \FSM_sequential_tx_state[3]_i_5_n_0\ : STD_LOGIC;
  signal cplllock_sync : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  signal \reset_time_out_i_3__0_n_0\ : STD_LOGIC;
  signal \reset_time_out_i_4__0_n_0\ : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
\FSM_sequential_tx_state[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFEFFFE"
    )
        port map (
      I0 => \FSM_sequential_tx_state_reg[0]\,
      I1 => \FSM_sequential_tx_state_reg[0]_0\,
      I2 => \FSM_sequential_tx_state[3]_i_5_n_0\,
      I3 => \FSM_sequential_tx_state_reg[0]_1\,
      I4 => \FSM_sequential_tx_state_reg[0]_2\,
      I5 => \FSM_sequential_tx_state_reg[0]_3\,
      O => E(0)
    );
\FSM_sequential_tx_state[3]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000F00008"
    )
        port map (
      I0 => \FSM_sequential_tx_state_reg[0]_4\,
      I1 => \FSM_sequential_tx_state_reg[0]_5\,
      I2 => cplllock_sync,
      I3 => Q(2),
      I4 => Q(1),
      I5 => \FSM_sequential_tx_state_reg[0]_6\,
      O => \FSM_sequential_tx_state[3]_i_5_n_0\
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => cplllock_sync,
      R => '0'
    );
reset_time_out_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => reset_time_out_reg_0,
      I1 => \reset_time_out_i_3__0_n_0\,
      I2 => \reset_time_out_i_4__0_n_0\,
      I3 => reset_time_out,
      O => reset_time_out_reg
    );
\reset_time_out_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"020002000F000200"
    )
        port map (
      I0 => cplllock_sync,
      I1 => Q(2),
      I2 => Q(3),
      I3 => Q(0),
      I4 => mmcm_lock_reclocked,
      I5 => Q(1),
      O => \reset_time_out_i_3__0_n_0\
    );
\reset_time_out_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0505FF040505F504"
    )
        port map (
      I0 => Q(1),
      I1 => reset_time_out_reg_1,
      I2 => Q(2),
      I3 => Q(0),
      I4 => Q(3),
      I5 => cplllock_sync,
      O => \reset_time_out_i_4__0_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_6 is
  port (
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_6 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_6;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_6 is
  signal \^data_out\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
  data_out <= \^data_out\;
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => \^data_out\,
      R => '0'
    );
\mmcm_lock_count[7]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^data_out\,
      O => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_7 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg6_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_7 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_7;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_7 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_8 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_8 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_8;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_8 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_9 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_9 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_9;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_9 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2024.1.1"
`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
GNlNeNP2E6l/J7IS2LF/ItWIQpPIQxmqiENbpRcUcYDs2zMQssAZwDKWmGxc99etc2e+6UESUZFU
1OoN93QWwGY5lwtShrTc9cE66KsLO3U82lRN16EHmi+RZOdYR9rf5z7OgjqfdL/T03/rfwU37+Qw
8V3wDE2i5hqr8BCHEwM=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
afRfkAd8cICiTJYylAj89MfRuyfOa9d/tbr4mvbd5mWPw07cLRhiyi8gIe4+Ayw0CiOk7/EjPgWg
Tg/1hR99BWRw4VbbDTWu69lZfP8CCWoBxtdt8WRuUGz7PLk6qQMaoDf1ogw0xyXTK96u8N6bef3z
7mR3YInM0ECnk0+8vQDLRo6ETSTf4oQYcckMpPfbPQ+NnHRvcHroYdddFGNYasbeUqwn7tkHnYO+
ADUKING4g3DhZO3UAIPZJggQnH3ayID9nf49Oa7bj//jtWD0qELIGFMQn48spke+RcfDkdFLHWhY
0esV8oRsRwIWfwXwqK6i7aIis3paozzKSWbiNg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
ZI9ECw+Xpdoykt5zjVXLh0QGbCnqQDaDo8xSAWsYltuhqetS6+j38MNsVABP/vySCUkxp7yLlCMg
506S40Li7Nmy7qD4YvDLHMe+fIgwi3Hb193aoDluxnxdVk/aJ7VOJWXd88+scqfUSpbbFv7rk9La
8woMpCSiDNVKi6NB7Qs=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
R1h0p6UTerhva5Yb3duDj5nR3CyyGXjeE4dSD+H9PUwvK6SElm7FlL9AjlrnVaE2OmUXbsHu+WYi
T1XMnV1ekw5xTRICAudvAhAJrZYMHEjujHOfgXPmKtXHivF59pFlTxnEtD+dnchOL4x/jMm/lCgH
k3/wzvZbfOaxaKFceauCooIRZkqZa5FA3KG53cEucbspEldbnPaAcHsiO04PEwklvjo+2XAiGTHB
YB9XEkRUOprwSG+TeOWFNvz4Yu2FYHffCsobvgsF/8dSJQ1SVv3rM1uHf3coB/s7NIgZ0ye03NO/
wLFRL+3OuPxWNTqHO9YYXL0XTd1rPaaWB/mCcw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
4l+Avk44zzaOVxRJDzFVTXMyJmyE6qVo8wAiHogr6M/NkYt3AgnEITBHDJ9dF4SVZQLiV7ZY6shr
asWzrbISsiBCAaZgyPZLr1zlBErx08H5Enf1NkLuocQa0x2Dc1DwG+mO+flli1++tKpmHM6ADCS0
3/8fREk+ga01VkXbOXY6B7Wt4G2O3LBdFCkEvd0/MEx5uYrJNrtpaeSd3h2CpYLsfPsxxbAzNJN1
3pvNKoS8QwUnTPy7oe/k8bFD0TOzZmLvT/dy3x7SFlB6oSYr7FtWMOOuH5B08Upu75qdujq2/6ox
I9BucH2FnuLsghHnPuWfjh4WTDPekeCy51G/7w==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2023_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
r1TCY1FBxgJgPdlaGrkCY3ZPZ26hJnsdsOKnUu75PgvjzuIkty6RkwizEzV4coW2cxRBcWf87Rmb
xbDdy2cDu4f2qP6QUVBsGtY+ITHBIqBPqY7IqmHvnmT8DQ7mta5QD9HpSpd8E0HAu+1hmp2y9R2T
j6cEj7KNzaIGnuiJLket69e1dx31L14fHppyx7EJTOKaIYVHOTGcZeHi6ozbitA95N9bUNiSCS93
sJcjWWTWEB9Cx8arBeHBMIflTsCrUzgwd6f7Lb59R/pt8Zgvgr/BoV20ZGDDy6BCwEWgGSw6GDBL
o5ppIN+dICJOxCzX0Ij4ziKkTNPa1OnlJpcXcA==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
U0o+hd53NUTkWW/pm++iDbvu9EzyJbrE8cgDdW35QL1go6ppv1cnjM7W9LNAUoYpay5lYpZFkUZP
qgnoVBG2BG64sAHeAjjMVH8aUuTYzhhM3n6pvSqx5Ez8rezjhdOlMLX17yWFSCq3x15rfV2si91N
p+FFtjXOD3BmuNjRLXeeRjrvt6sxfN9IwN0yEZYhwp2ukPLeIyrYpRXZ+xMErI/b7BKLZNIljMSV
Cx5wPeURNjIC7AKZOJmo8YQe2JGJEHps80MGm98CsBtbIXDa7uAxib3WWstVs/UbQGgqjiyNIOSu
11VEYQSI8nO9bNfJPsLpVaeo9V+FUKdFcBgKbA==

`protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`protect key_block
JlAVtvWu4RylemkeXAwQaOcauzsF85mjSoJqBa5/i7p1cOK0PM2Rfou44syNOWyUG43Mq5DdY2P2
hSdJ0BrVnIF/JY67xayAWorKfMTlvfZN3zVnvvFrLORWpe5MwPzwHomEMtQgK/WjTmMnFJzWxYti
gedoe+hxxEoZILnHN3ge9A2vnRikwcJETpOKLK13mQa1Td8zo9tsQuagX91Bw+AWLzMrHGR2bmQr
grI95HpSioNMY5CjomezDLSd80Rlkr+6SiaUQt3pqExFVEAsKNJdtppxNwugqojHWEqm1lF0wKuj
aLs/KfYF2Uv5wNjccSP2gHLWWaF5DKs5G5u4c4FjEiS/slCZgrt5Ymwc6XKQHNOHbPRsQcbbkPMD
V/YuoNzw1R2386n0pOd+vMD9nTtpB104VrwBNBxlGPPa0qiFM4+xr58vMzHKNT91irPu/EirEIjt
wEtLynnUeSKMKz3aAvXZPRYeEurD0fgZ6iczWVNZOsltSIQZjOp5ErOC

`protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GeMop+4rJGQJTsAts4IyQAkQMeKt+Fa026naAEt7J5/8nhAvWlIS4YQDNOLHMMUe8p+sUA4yE6+s
hf5STrRcmXzM45O/bYrkBzWHNqGcsEuAHfdUd9ANbBZmpncQ5ywZbXLX0oI4xVx55B8sn2gAj+p2
NQEPjYWPZXJNML4IilPsrW6TlgTi1zhp0JFBFT3O6yHY35dAseHXtMWYgjr0+6WBtVCCvC/z4bA0
UY58Skgytan1bUM557AlikG2LqNB6YHVUQihnx4ZProlAL61WY/j4joeXcDH8Tpm0cmYcUM5yUax
haoFS6FezWAqD4KpiUS3xpkUVET2QpN0NnxTvw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
1x9gXtJFU2nGQ9CWRMPqjLG5B7nyL7ZKhgWVKPrQTraMot+PY1MLqNEQ8371fSgOtWrUw7wBswaL
CBsGdy0dDZt13psueOntqYT0wcW3vUbYzfkMCsSjkk2yeYjnCYclwJKMNk3fGxfxpTLQsScOFING
ZyUHBaE+KeZ7yfdqvxwcpP+rUXdNX/2BGvcr+a0ow/nFsiCiOMdYIlAgllnBFoUV7lDr001O00Hk
1MDqVCRdpiO5q5FnggoIp1sVIq1C+JbqiuyPkv5jmlk3iIfZiHhSSYoxP3DJf7VPvlJ1Z/Hwz6Dw
/39nFCTotl6l9hqiLHTmISLBvSYZnjDEcOPtPA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
mxE8AqszO8jEcufcEIxsyswviid2I+vp0wiBEslkbc/wVr2vlpOaIaviDrRuTwIVx162O0AViWa2
3/lH2PLY1lcjenGETw7njsuJd6MQv4KihzbHhd1HB0TKpoL1n4E9u5PgNIGghcgNu7s2hE5rpffM
FAxMXxyhZQ3j4WBKTgRtbqcexvVSl/whSo87Oa/V48wwF7Z68uoZdaD4cdNihYwHMLOm0LYqYesQ
cScpoz43IdSFiQkBEM4YXuqn7BFIsJ7UAr+EkHAFxWYqt0IJ3+/OCuOYW+y/3p2tXT2YSGnEsOzZ
SLIsZpsw7wAiEfu9tVZJlTQ0OUTZqfQXK68i9A==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 119184)
`protect data_block
pPE/AeOZ9UPPOJP/gev29vz4vAy12ukSJ1lWuW+/trX3UqKXCb6mfQF7fZNZ/2mu8hjfCM/az8Xc
7eTzAdzRhNGu8HFD8sbbFbGrTb3cLp8zNp3ivYIquKrOP+Jo0pHAOy+QVpkmXOQbzZSg+J9D6TNd
hTf+4T8SWQ25AlzMsMlMqlkwxoKDVr/h2YtLFm1koDXJFSViu4KO8991VxAVLsu5w2DEgdX4XoDt
WQwtcMseCdxiIBnRba+CGMYjbfGV1opTLQE6DXUiFwyiU0mT4qc3fQlPfWmhR/NXQkUrfxc95JH9
TM/z7pBq/fvly1uZ/IyJNyh3xpMJDIygHSfZuvt3FGtdUPGX429j+uBfRd28mPLGbOnJFac1s//b
qQf3RR1myjEi2OtCBOqAN4uiy8+tVUZAo2igT7N/o7hCvuCwMWQ6K8kMVw13LHOPxgNTRk0gNeIS
AP+9/Ykd3FlNb9SC1pgcCwmFug1jqbRB1Cy70bti/OlsWfu5ggEYp+NCeBvTYqpgiPMq1iZhhor6
hY8aQfhaKpQ9ZzUDM43/soGVUTSwvLRHF3GrI1QFXDcowxEUWG17d2bQp3ilIFqkW6FFdBej3kkO
PmEYk7SkxUpCxxzufDi7IFbh8TUYcZJ5Lqzd2U4zoPsiDYMfIELsVj4a0VJSpErGuZ7UuDznYn0g
5lcMzfJSbhDkxKp7GbXY4QAHYsj4VmOtB0rzGZE65PoJQtIV6CygDWyIPJ2S0ihG1FxK11tgjiA0
MNpyVvAeyuPC1NSSENfE9TObBCyke3QSniIUpc3LHLS6Ud5b2/oZ5pDM6Yuk71px/kPc9jwZZ0o7
tzVkoJE0qWtf8MSgKzCBIfYGZjFpbvsL4zLMtYf4KPJx/zv5gdqWMC+zLICPrw/rn0STEP/SL/gR
x+jWQAkRDSgbW8mCHzeIs4JDxxHOCuNMFTxUC9Mt/YTtsY7rI+Vi5FdCR9io03+a6yGlJ/PGqFOz
3E10bCmzx3qJ8fjnThUru7M8XnDUUOK0z1MdcP3gG7GAVc9H/m2li6cGlpe0Rrs+zKlFaw3vslCd
Sn7Th6boW2fVGvBLhFT//HsahYa4ghhnbYymaFveVfYD4SdNqOsAftzR61aRPtbSm87+Qls+e5IL
594pM5+GhFP289y0gINOmhMcNM119JN5mMpdf1OXWjU6w1zL5ULmKNJdghLRIrzF/BYe1vaqYNsj
0YRdACkxtRUzvCpUvhkMt+m7Di15Fg9mtfeeLwlny9S2clczmXx5zKqDIrks5Fs/CWQTd1wVudc0
WVz+fn86j77e0n0SD6c1trm2qCv++t5jyyBfqvYdXBmVZPSwtD3V7hOg5ep4Cz7mwLAbyiV4CR1d
pRf8kXZp9FTpMU116+08gU0G9nPYU1GLpyqJqeJBqQqwJPGID3Gjez8GT8yycGlQzgZncsSGVdN2
PLlJ9S/JNv1ic2S6wnM45jE4U1JVkzzyz1+dPdU+X1VboPw6uppaA0ewzj+DcsxGzJhK5RI1sctE
3/d363AJAj4tw7OFA+xPB55nx8l1qUrX8AD/0DfMbMRP4dx3kVC9knr4BVjgTlTUUtA1uz02ZJxk
SeyjluLUtwQXyXO1YMQjD0M2KiqiQeGoMZg7XPWylED5iy05NkEdy7EceYRII2kkOLf8UkPRQj0C
T0oajYlu0MMVxF5xj7F+XeP8er3VwRnWikWh0Syu7yrBmboLb+8aWVUxbRQ0AFt/6BkVQrwFkCYM
r0+ZnN2pZhJR4w/6OZLYZDAwCGmL3+3BiCc5jzf1SATZCkI3SPTS6JSX+c4d1psWv8bNoph+Tkr/
610dKAmiQ+Lu1g9lMAMz1Q02ghBZKREGwsiJxNvKjgBG2+UmKeRfMWajJvZp/CPObi4k2Vp9c3Ws
yDoT5n2S5SQ7zmN0u9UzEYzh0kTyjDP04mkFMa2crl7tmdo3MFd7dC2Acy3tNkdOJ2mBU8zU9CnI
JE8lHasAvzXnNtlhB/5bfzbR+Fz8ltoXTKDvflvIKD//LMV+rcWEjM5/ZlkL7m8LvuZQ6ZEjGdPF
KHnEIYAZnjpV4fyEayZ6V84fQ2SmYblqm3W9MXl1sZAMxcRK6aMaJT1czZC9+bTyxYnwxZzBYo+9
EvReg/lvq6fE/F3py2GmnJ2wU3jwE5oEsFTAx7u97C0r4TMx78JZ1AYOw3ZIimkb27ia8sf/vYxj
E1ram9RK1+MJyAHZbUItHhq87ghB4J7skSsjCgIpReTsNWFWUjKkWc8lkUg37i+ngBmy0SKflNRz
BtTnkoQCwqO8EzW92EHwvu6zbzT7gEcJgdqhmlBDuhiFd/w/slpaU2eUCow3YR0tCpzvz6v10ZHD
7zcUGUheYqlkM6zmNh7KOePxfNHj0za3jB7d+YfWmnPjiUFooaj+uO3E1tpKinwmcPEebeMNyMk3
d4503xmBSRoXM3cw9UqBrEdBmUOSstgciH/BwsTE4+u4b9j4JE4cVoPjDmbnCiU3yhyWsJ9ST4Yd
RwD4MT5U7Jf8KVThlYtPKHvT8YWOg5gCUm29pckb52rUnZA0WvqI2Sact9G+Lv/qK71JLA/g8Qaz
rdgL0fTEhhgiBYJmVNyCNObaDLOYkGb5of1zSFXXksFdZDFcZRohQJCR+Twhkqo3EKWwYt+v8TCm
TZ5LSB5uBBl+sd9Q2O2fJBHBAdHMVb7+u/AOZvL5sdpzn6kErl9qL4q7fbj0TrgeVZ0JtZ0uGviZ
soSVMPUsH58nuULJBmnlIXZZA7g0JPsUIHg4/Kk1ZOQz7emPh4RhLwuxzLg9Y1uC32LgSY1u31Fq
2rgWG1z6eOoTgk8k2SI+zsd9GPKtiYYg81SjKDu0omlWD3HU3NIEb5Hjgyo+XSBaeZPRLsTzgKqy
jS7anxM3wHLntg4YAq4s19D9f/5PzgjETCsKrdroEOVkH6obQJ1E926ZqR3QN+K42QiN0FibgbC6
eAL9Tr5sN9necVmmopAJMNXF4ow2DnQBh5iYYttPw6fqptqFo0Bqj/rR4fJVrjERN7or/ve7JiQf
AH1YhGIWOUbazLvrNB4zDhvrRvm1QqVM5Po4tnR82yCsHFet5VH11ZfTcAebCT7YnJ9rHQRdHR3D
hJbJQIKPHu/7O6jjL946dJV+IjmRKQQn9lDaiZ03pnX24rukpUJdqMh6vgB9PKxLovb53CRFHbIQ
3qd/mQCXp8Fp9sADCiULEmPxsHw1xNbNiWGWy7t906GIs4680nvMHbDMDbcSh+Oi9Pge+TXPusDn
9YAQpT/78PV0wuGDs+zVpqe1K1O3qzgstiav5aOncR+ECtKGn6kbTXTXQaLIlFehWQMNAaW2s2DH
VGqdO6+1C9KFPY1VdX0avixw6L8DtR1VmSgp3uq32fTUp0l1bhspAkvgNyZ1bqf+m/tPmjt6uFAX
vGlExK7+isoYqpl1PqKjpovIrP+WcSBUjSFTeXNOY4A/upSr/i3J1luA4TRO+lWjmfd1rTIwgflM
/oJ/MwstJY/yRmZruXJPQNHPeh8b8BK2zv/CyV+d4+5JR1hBvG05NtarAHMgT0uKOrN3Wu58YJNU
HO42CroWNCBQAagsQflHtV0DfSeG23qP1C/Nfsc1HtN8w9ZWbqdUCQibjfv+js9L5r3eDVU36w5O
ebEAQY2gUjAsAjYt+dOLZBAF0Lg2H13xtrRKchbY9DZpZPB8n3x/CXdjJgvSTysRwZD8Lt2ZNIA1
bkA54VKGMjuHa2P8aIViVfR8pu3zPBBe8hgZNRg6D2Aav/w3cPHXl6mDdg6d9plViio52z1ahtfa
KKv8jWfSigZP8I3YI9gSNgHlngN7LUrMSEItq/bvklUF7RgCutO8+MBDQbh2GEO2R5CVAVwssfhm
4jxiydDidX2EQnpzMqTGGFkAhIscNkf3EKLisnRm0Xt+VmIo+OIlQDjYDq+TF6qaQHSZsMzR1u2C
0iYNih2kDW9Mrx0rmoj7lIeIcqceZV41wfecatGQnJdMvVhHdRYPqiGpM4PNPEsKdXY87NXhVwSZ
QLTPO2VBbx3fNrDfJwLUkgzA3KhgbDE8utqF1DJ5yn7GhG1DsO3z6c1i71jNklihCVLynScb93zE
OFoMFWpfGEyubcWYlKVhAW6vX54FiihKz/askKRvqiwQn/+HVTVoFJFBlyJaHAUpfFAavsmNzQn/
9iQS2/4H0JbSu2Rrj7xvVywfBu/UHApI7S6/lXBFWJIPDpKcUQ5yqaV8rUOhr/ofcripQWfc4wll
RgEEQubI00UtEjxUGQ+NGcOmusUhb1Rn8QTGVBeIqwQIZPbtp3fNtbdHxi/fXUNRU858iey+jVW/
XGxj66326uFXXfHdRetdJBzoD3lP9jflIsW2DXrqhdpNvwIpE6AIOLRi0pjO0xuqLfjRU1f1xibr
mHNmnmEDmYuQ/Cgzk9fLvhrowJtPyUOEdRYPmlv2Jlo7j1FGc89sMDV8CILEIdsR2ybatKkNNfaL
46HkslulKmP1XhytF6R6zgCPvUsnwM68acctfvpQrZ9zzzZZbFPWyrzkm+4hVi+lecf5EzetNwMa
Yv/XAIRmNgXMAvk3GaOE1+i+xHj4O17FXeosb6XFsFYJzy2Kh5oYG/xQTkBtEoFeTimzJ/UE0jMI
/8YJYPSky08sXjWvJZzfMj+XsjpE62FIWK8s98UY5rUU5gW2YnEO03a5tzJuWbUZ+qfIMHrMX2bn
/P9mNCGKqZZg3tguenxM75usHxs5VNz3ghUk64hrq7mOkISvWSogE95JFaOTkik7+McqFPPi7mbX
27dvng9ppLIytahyueZ0A3dsWEy1LQUR7sxEYOMUk0CNf5z/j5NcYCk0Kx4zTaRhl8ghUQOakvjW
cQMrvGxZ7M4Eof43TFTfFhs4BKAgDHCKJhtXnr+ZOq/R/zgxl01phCZMiWYHeQps8H+PTdsddXvV
25VKspi7CmKu3YK0C0hrh5vykuKOrWwTk0EleHh8RaSIkLBt7wFcLOzJovlMFW99rBuoMjAZDk1G
03wfyMkdIjYDIL+SiRA3cAb18LaKto2Oi6ZIjDSos01Paggz7vELeM2te6orxLFvt9R1eEWvOAmf
/l0pGDz4IyEHG6zUxoiue2vN4Z94BpQz2paML+gcoKyYoJQ09mGvmaw6gDVJgGUiG/+Amy+G2kj0
keDtte7xZjVKgd+YMO5o44YhQEnJ4hZoO4SuwAoFGl1uu5J1UfrbO9Xbaeeqs6w0IQpmd+RodHMa
3eSFEJuYqQvKU0R4EWd/ODLsuczpSWwfcl9KP5uTOKmhZap1fjPA54/ZOfgaJ57NMQwxWDr5EBNm
SopG6F5M6MO9TBRrw+ScUdzWbiN7CfohMhKZbQ3I9aR05jc2yB0fQIJtud3g9dPspYxE1r+9VZjI
PPcUUpthcZj1RVrPgtI2HnPWkBiNLaXolgawvsEQXs9WgSQUlnuR1PmmjIB4D2o2S/hMiN2HmtuW
vtvF1dJ058NjCECZtCZKJGKr+/hPtNk3baw7ISWHGGu4KEfJFmY8TRhX9bXjVky86wWIurJQ3tFe
M0TXcoysDt9e3KfCYMBUtam3Ox0ttlRB7B4sfXmUrDRNaMGMl53yDhYQuqD/j0Cr2GYBYlmJyFzT
XvKiCspxoUrMAyK3sk5Ihy+st94BxFpinY0rEbIKxn3n4FF00dWzuYmKZzNuI7LYkelhsBERut5v
V4/lOV70Pl/VSMPmxTE8nrNS9Ww75CKuIv+7aqflDXdjZq91ID9qFmGo5FBhN6vn1z6Ld4dTS5e9
+MZ5xdSkJtF6hP2g31CEiuB9S6OaXdfzZ2RI8MgOVmuIDwd0TAMdSGjKKwxYogtsGVICNqqoUzk7
TbB/0VP5yM2CjJLxIMVkK9/MqIUnQjSgbOWbCtQLqRMeh0pK2FceFEDrWa7mLUQoEQK99F26NPvm
op+gEZ34ICDoHtOlILf/FQ4C2ZQSYo+PoxT6LbdTdKY4JJQOi0VuhuX0UyzSgwADrR3Zjs7MD+37
oRcACm4GeM25tIUHjfZcDkhSeElOmZZwMwKVY4V/T9uURri0D5dougJjqT1yFdRMinj9bFg+ZDFW
7Z8MmYzAnDRQ8jfs0h44cbMzkSBhC8vEtOiKk5627oty2JsFjk4bYxrOtsNNaYhzJSZnka25sj8Z
euCnAlUqEWATQUIU3QfmYTaWKkHGkIlvzs1AodjYAurhgSSC9mZTqfbIQFQ6DeJ+DNZCdfQbWhLU
X0NmriJ8w8Qfpb9I8ORYTsb1xdtoeJAaa8/rOy1fS2IS2NJtHybcNNqvFKSee1K2MBXEvnn+0HB6
RqCGuoVTwonYmhHwqq+EWVptGCWdAi52M3EqpVZCf+NBQLq4P53KoDcItv7LVv1A0KGXxukAfnOH
h16x/CEEyjEE507TC9WbE1Zv/A3Q5GBfPD5k+uKseV2sd3fZjadfzvIyqPx9n96D5fq4Fp/Np6T5
ghdrcfIp6PkLgHDJZHe6RIORo2hXricGasbI5WIO913xAufImMKlIWOQYbrRiHBCo6z9eR1kfzNb
EiWPf1YanZIKIyrDXi7wusydW1nPzQwLXvsv7C+TnOfWk5waf/YF8EIo0CV0X9X0eNXRKZscPa2a
YmuPKYWMXOtFTPyO/igFIxcq1/emNCUx4blqEAImk9Kcp2h7OFfHA2Nc2VN1VINEbYeXgZeFCBZa
i+t1JiVJVeAvC5VTFrtHrZ1Wr9tgYwU6pFxa/YqlELzuQsA5i1eZvFeqJocVz1cgBOxyKrFK7uJH
Vk/elEkcIouBO9nKVtUkG6PTff7GOKfKotb1RIGcbQ4eaecYiyOxsT6VIY0F4iP9IfXhLqcblwNO
kERP+xsHJhAqtdC5pdCr2U5HLNt6I71XnN49HZpyf4BdGsNNLIZot2029UO8BlrWQNGxznnQ5wJR
IPgqzO3crjzipHUsNg5jkJ0ewWw2lJLXWNBl3hnV50LeSbjrRa0N14iFDYVRMlVlZUJoSPsQjdZ4
ZRuT9VoZYI/RHbpUcxBh2AnZWMqGBtUrvBu5BuuX2IJzqPvEarvfchvDRxgbGriAJMUJdsUCTLLJ
b9k2YN/HNzBUB3iGS7N9Zxblb6d51Ic541PQYw8MatNhVLV02ndHmZwxkbzAO1zEPClUagj/WYu3
ef/KFjQiUugVngY/PExx+sq5IT2OTmvOyrydWCVBG9XtuxKq6jMl9s5pGdFA2+CF+C++OtQ+NTZR
loAUWfCSYuo4jJNQwHm0Jsge+MLagQsrdpCJcEYCG0Iux3uPAM1L3C4SgEMRMu/Ekcsw4yG7xcyr
jnFvultIJhRtWcsIgBfG8F6WCTriKzPVzECFiVLUPBylQYOiH9FV7BY6v2OU8SIk+Gxz1Tv5pObq
NNTXGFvjYUZSrn6TIoWyiwSp9OJ3iXJ9KBiWLprgvD228d6gMNNMmn93eTxd3C2Dxw7unTqrP5Kk
R9rF0PCW55ejcPMdwQ/vYum4HiwReP/hJzVuHJiXTS8OMBT12K2vcUVubVzVa3vhfZW2WH3otcON
IfjHoqV02hnQw1s8PDigcRj4fJCBNpJuQpBfEnQxdoLVU44SWZ2P22CHfMAuuwpQOIrAZv/k3Osi
s+kYZlKclVwBkgVNsLjC6YyAJoIt2VoLg2+NlcWf/exoyY8AWB0ZdxRrJWOkHTWs911EpeMfoQ0Z
3BxBw8KT4xVDpv77EPLEdhUHVWQGg35Q7UiSkTJ1h1gXJFDaqGwAhzMoHy4xu+kt0204fEi6tg/o
EZ6AAOEOyUrhnPxq9d32YAt8Co9l94wbDGGv95FRZvk5Omr0Q5eHAfEIH/NibJWvtnPqLT/MO+3f
nXRBxBhZx5ylxWVutmr49VyMLx53jokD769i9W6Cfki+wUsn0W9UorEURBeFwGYB3B/Vid7U4jI0
nfvB7k8Xjt29flcJpYXdt0I/tOrSa/ZVdynApyYZ3CxTp1Wi/fYlFr4Ly2LEzBTa6Rnnsf47CG7l
+VuKzCXtVzqlMZgyP1EB3BztoOGyrUh/w/AFVprABpsDAf1A8Y0I5VuzHhmpm+Ha6KIHsTjiB7rl
HMqmeJSIgOPYEC3o7GpLrd7OqKuoWWkygh3tY0Bi2AYfmI/gZxs2iAhMUzyPl+AjGXznyKggZ+E2
PZZqLw9DdKwAlpTSq9tDxQErbf5oVg5dyf2qWaZSHlbERkP66MiDcPRuEzWxeUZYHWYhql59/t0/
71G7I4flEvUC+yj0YnyQ4H090zPSZRm4JEm/VPXlvgn6jNRHqcdwXu3rZHvSgIPRK8XmQY12f9FL
ztnM7oRNMNlaW894BSVYDjtpiphy+YUfN1ogl+9BLUjCWNLzeZd9xEWk4E+f8M292lFHI5kDN8eB
7OjYRPdbfq6agcAVvo+pjcgIqriBqcb4pNMY82zj4ePnafVvBk2BlAJQYjOjDHkAIQz0hxvITl4D
J5RtSeBiR+0wB/dEfAfegH/JZy+YEtuCc2Cgj/xnAnS8L2UAZdquv6odgcQ2Xpfh3dp7PVbFi/5I
Hl8eXLYCosf5g86jY7UvrwvZqxV/Nkl8zpxXN6HAU8XctF0pLw6bG+VJb+aR1tc3sJ8yRDVYd0hy
iAFfDlQi3ErjjCcxdgdj7D0cHTnEUDg0YYGH5dVddWEoMitOqpO2NJOyuMAdRTPD1hxPe+IEuKyg
06Ooj2NuOclnOQ8ec/yWdmoH6nTNtAEGikSg7o5kl3lZAnq0aU72UGQk2K1LYiSCtszF3lC26CaX
7KEQrMdK2GgTq44KeB/ZjE/Srk9ZevcXd0QY7ky42PzIQRv+Uus7Rbe8duefyUEJWS0Z2ZGt7xFZ
I/7R1Q9GfCIcIv65jqxvspMq1vnvSIGxmVaI3vMP1+R5nXy0UZa/byFsBzIrtUJGS8ZkMei/qvfL
0JEhpwVF8xELb0hXRuGXFJKzi9Wq+zIyxGg495D8f2cDZDEqbRZsl4I95jQXQjLaN2m9KF6pT1Bv
87hOTpDovAxcGhXrFlF+JB6/RW6Wq5DQtgsKRJ4Z99vn7ZDxRXuyG39GjP/4rAoKhmrhYwPC9EfB
2Q4tvOnT9Ak4aQRx6EcLYrMntRHYCKzAvuJXC4MUbAJ0EXf4GStpFIJ2uAbbSZ6bG5tb5s1omtln
dGvnoK8IgnV5VbEl35a23MxS2KWNzIboukhhOzfIIth5AWvf98BFFefsqrb1xsc0wKEsgk3L6b3H
FKPmLaOK9XytbPBJGFfu5lTnSsT/oxhiImNSnMkQbR78nd9dvv0OL4/Zjl9Q79CGSQZWXZoEcewA
1iaNO/czC8a8XiYMqhqn6fqwY24RE6UB+/otXocG86p4vPnyYCxFnp+y2sIKabKRqI1lEnzx1g6v
zXUmaIn+QwyXpaRPx7RyJyHnq+CbJzikjgWEhn4x01KE3PTQlY+Bhicl52N9MV9OQLiZQbsEG9PB
HnxBUojHGRZcYpdSj8W0TArzqcle0QHXS9iu+9aDy22mNIDFL0EPs1eWAabO4pSWW4O35PfaEMIg
5jfD27lRTwMSV3rh/6a7woo2WUxNx6/afMfI7Ujea8hzFOJnpBNkh1SQxNmLj5wyfd3/x1oPWQXh
4iDY84UH2clBuOrIlusJ5Qogo5EpB/+Q9itgbEohDS9VpC6CM9HSC/poeGkKjAMl3229IhlXws1C
EMsOpFplC05Oypkplp1IwuO5ZZI+xEsfCOK4L29CBLssd5I97qwb2IaM2h5ERr0McNrX7TUzO5QL
fbJv5wO+MuIKzhbXTVm/I640F+FSF9X4gkU0mHab2V6epO50ARLAx9/fnFy/MEtkLd9xN/DcCAe9
pEYJBUz8CIRkH4viCgu8Fng2xZDSuGsEmraraNhWTuOtBdzAAp4sAsEZ1UQgDKeQehFjy2vxeVEs
Cnn6KIkGlq3VlCapKIWhb5rGsZyGxCPJK2Gc11TI5LQCJ1/SBFbZK/OBaqf1PmJgXn4jVz2wYjRA
BAFtbmVEai5taSEBB4S0wG0v2xKXCel8ZZkyZPpq9OKikWLsmBdhj4CMhJhRTzw8+oV3pRq4Rjhr
hGlS8thK9pdBRz0btNI337S9O+75hon8ebfR+HPY9KOwfpWMz5u53aB6mDqq/uFNaaj2BIEqYpE8
+j9zdnzV5NqRxvuwLCtyNOkEGGTLmoh9pqK9cHn5ExvlR1+VgwnQjkNDsi/tp3hexG6flv0YkdD4
G3vUb1c28GAs4KISt74sw3/cHj7SqzgdWBJhS2I2A2Um1XlhFxBzcNDKma3vxei8sjwG81B0f7XG
fH0oRTS9C5Pe8nI/Hxsv/y/vcYjF0XrauIWnckdyCHjbaOdwPMtK1zlasptmBgTKKcw2JFbsa+iO
nw7kSy3eXK0ay1TsTd07KDx1Lm8MN9mHDF9NhtwBYMWeSjaamHzFt1RvPDT0ouY3RPuxmHTQWl8S
BIxGtthlQepmNcgOhmwCo54qOZfv+7o5Z3M6HP3r4tX9tekU8rUCUQG0lX1tBYaUYF+Jvy1KlXFB
nT3LsFA4LsEOk7ayr6Q4Fqki4CVOPHpAluzgmvDxXPUjy3fV47ijDpXmKIJl/KSjkxe833+2MxnE
NV3H8LjuhinAX4RHyiJSTS0CimWEPU2IzaeukvjFhKMaGcXepGkThBQPIrQHBsIN5PH4rHRFI3dY
cnsg2XrCPGn8Fo7GyBpuWDyxIkxGF/3tlwtpwbuloGzZ9qsX/a/u3yuO8ii+LhRkLKZKaCM79yHo
owBlsTwTdUdR0NImQh5bhpL4voX70lUxf1MmW7sLo+/4/7Jb7LzCvoOWFZZ1yH/54/qwFuE3qhI8
fR0SC28wX1I20YZ+z0JCfImoP2iDTM6PSWlimKnStDNylMR5pH21+2gmSA0sumIW7U3az+R9MazS
adoJiatIu1NMJorpXRdqUOO9x2NdhWMB6VUkBwr6pvXjlVL+Md5zEpiCyZiGe7LnL7ebyggpvZJe
4jQT/WMU46xXeDYWBEwcvRKFdAJ3HtAAXddJM5Zbx1OYQRy82G4H8siX2KHX0NMeSJVPpvBuqObE
TsuP1QtuterOArAliFpjAnvhbVELpDu3V9DW/sR0l+xVO1JQuIkY7a3NPYjoo6lkjr6oDDA5YfrW
rDt4CuIqhiZ2yQGG5oa1M4dCBfJSrPFiLvhAvELQEBVYdr2N3hH3QW6mZjbUWIpUI9cDMMobzSfv
8VcLTp2WnkvSNRzj5F0eE5Dd4pbydW1H7rJZOUlZebpuxBBUat2GqWJlSXUPbjdH9w+5YvEBIavt
lBYJ8XmJAUGIfCBza/OKimYl233cUed2W+Bgo2kqZ+WNt2uAe4ThZWXMhTFM0oSRbhD2gdcNGHRJ
wba82ptskpxEhujBn6D7Dy375QJ+hYTJzk5OcnPkB2nHiWMc14c1i4JY1hbJ0C/5ccGc6hb4ideF
S5w1KLvzwkXYs+dk0ucD+zjQ6Ppu6uQChgOeFpIimc1MmyBQlIBnkkeZ72Nb7WSi6+MqVdKCgb2B
nlGSkt1lqNjuRrcVX8K3GuOVl3zPs5++ByNPmDeHkbxr4eUzgssddeGDijY87WYOptKseqPvm4bv
jOuQnANTIFB8HZ7EaibZuyTW/XcMfac547WNTIkZVhQSrdkBhBXuJeUizTGk0xv+lz5eGhgHLCaL
oCclCZHh2MOyLcv3f3FGKLA/IhJHs7cLgw/6sAjc9mzVEdeNik39JEVJsdQAEWjeILr6GqsPivmc
Nr6s4JFafhaSvH9Km8uUun4G79A8caLHzXi5RSDh0GGML28MHWL40JlPfPvxMWJWoF1HxrZ5WXwF
4HhhICVp1W+gRRBpai3iJNuHI0aWhMbc3rLmOrCU+v5yMs4SnXjxfkhcjZFsNeYwYjsy2HmeVti2
2sTKdzIwikSM6G9UTWQu1iMSEnHyjG/UWres44TQ02dWW+awPvQLfv7LdytZdEILVac9xoEuA37w
on4LAWc7hAJl5zAqgbcxCy1uVcnm5PiRuoqgxTumi1h5zNqUvuDuxR0N5BY9sSspzGHL16LtOKW2
IJbZqtHUG3mRRrlQbysUVdtBnN7WRhmPajBOEWf1HHCGsPZXS3OfNTqMR43g4BkWQL40B32GQl6G
NMf+wiOL49znyvb6vOyxa0814LdXAqWOmJG6qg/NmjNffBrj2mNKVTySnzgdoM4BztG5/odmfRiM
K+o+4tvs50dY0W/JfoUJ25c/yh8OU8zUQpkM2QQpsUA3QL3XoBb/VbntFSXkrTGcuRW5ddg8Vkb7
2sfmWUZn3BXUoUYFQGbdEtPklQLu+m+40GHPjJYhKRfZeqRPr6iBttkiMtXooHTqD4oG3Cb0Q765
WacqCBsu3d2H4bS0UnFeLq4uiSk9Fsap6oHp7myZq62vsq8y2iqIwGYO3aq8aL/TjtGwegJn2z3Y
OFvuV/czaH//fpQxxClkvIf0EPHbs1wIhW22GqAyYENL/X0y4ZETPX2Q0foa/T6r43FgdLDOOszO
iC82D1XpOmxVKRRjo83Wniwuqmx8LoLcYn8r+60d9TZnEel9npw5gt3UoP0HLs1fiRvcYNdRo4nz
Wl3jFhDawT3LHI8359CRxQhAvmXv9RPgU4DbFC+eT+O2R6AboaCRiFvgpClLNBhBJ36PNw2fvH0K
g6zQN4JfioBJAv36j3mFeeb8fvpgEUhv4WM2JB3x+/hGk5HshXh49JKijl8MDR7EMwDORuvQIfFw
kRTEWKgRmvUJo/sojiLEZjMsoSt85xj+6JFTM52tBHAcoMR1qp7fxS/Q5bXHZ543rNUoPimp4iUB
5tQGsUu82DSHkYpP4f58S7MzC25EZ9jhJFVW7kXikhYy+Zo03OPoCSc7Bd31L237QbKfQd2YrE4R
WBSXwnKovb6U1En2wCaAIK7iJsplUxT1VN8ZCVaUxXQV0JhZQY1p8360B8yZ0qK5/WFXuoVbYsjY
i9ISKG1HrKeGIF+VBWfao9X6yQt20L41Yi20tUl8au6LKtSXjsTsrCF8IcuotBDV7oLCPHnqP+2I
j5KiFoHq5HXiFji79cMMrloIG5/XNHkuhrRCTLBv+UtzISo7ezwzwQyGWKSaQgWJKDjWpmZK0Puw
nfQMiPH/r6H+AC/MTBPXKKyPgQsnbmL5Qy3eKYt9ujMbiRxEZCRFM9PZCxmPFQ1HRZvw87kln4cc
Qt+kyun3LszytqAs4FCxZPT50TJegfcgLPppRufVjmUm0yclo2n/dRJc9pXqwMHTlSjXzr+R4Krg
DlIagEF3dHn2z/FAdsTTilphbb/17ZiL/yUVmCAXdOP0O8tMpQwAaPzl3gZDveHgLmTAR6iyzyVy
O3w62ESa+orgL0VxwOKZFinUl+J+7+6QYxw/qwukoBGdPmbrGGWzlPd+LSAyGQ+J1QjlxBBUBURB
qysJfshRq9M0gCCJf0V2DaJwDEMfzcVB5UFmVnfOQq7OY8YYJxf/LZhrSDmBkkzhBEtfZujU3Oi7
rG+e2xazeq96UTGvXfDpsTqXC1kmUXiEdR5P0lD5VeS7biyrkq3YmMNhebHb+zpGnyM7+nW8Szus
9Sa0Vnr+yeIHrl7kqeQ3SXL8JcHZF66s/CTslirzf/0QDXX4AWDw4CGLO2TfvkKlGJbifzAkXFCg
GfDE96IYbPg97vQgnN4yiotxA5049hOldv+BMK0Eq2ONwHJIK0Dd3n90ex3ppkpoNhCZ4hm8rcR0
OZDghZ+XwDUP1zjD1EHotvxn+NhIBm6UuqI7JLIx5x7xA0S51CvB9e+eobQUoUp6pOPkyKtPViyl
C0nUAS/RZnq2Ch/YvVUBgH1UiJelzGEew3P7V4TzAIsAcRWYyfrEsLC3/t2ecN+L7KzaJ7IlLtSX
Upxfjm4zoO//ehlqfnbEL0BoD1Otbvb6F8I7OurhbUjzghCoEhSlLVfuOlBz/dfJTla3DXuhbwTN
KT7cTxalPmbKj7I/2N97bZn+Un2yG6xbqGvYta/Vgory7tpiEFPqVrEw8CBXOBcC5QQsYGgJQgb1
zK4OInI5eKD/SIyfA4jGsydIkGB0sSy2Xl3Mfm3jOElhO7DvlL691MnhJnSmyZw3XzHGXxEJhSPI
3nvfnb5FixazrbnpKCV70JVXsgshsE0OA+ukrKxSMlJ+Gb3kkDdmDzsvYkJzrI2msJsS6nYZLAdl
FtJhn8Qc/TSOXLyU917Xlv/pWlGsAosSqfBkD6MYFdIHGab915A6PXQrT4fP14rAWwlgIRwlaa2d
yz9UexQVs3623VuuoKUyM14gxMcjrMvWKP2j9HuRn+YNxPfRW2BwpzxwacVXO3i/0LKe9BO8bsOp
xVYBoqiqnhO+VytOESNV8ksln16jIx+kC8wbafoJVGXHzxv482/QU+SDNmHzE8T8q8C/8wr8NXO2
N151GtcdE34iYfOQmFLKfF9Tuu0e2j0nchvMQcvlGwd4FGVX22y9ykEbgOY6OGNfz69+lCptdsDQ
AN8NaNSk0J26jR/9EJgHr33JC8nb7N/KuulfcTLtfyu18KHWsoCi45S4q17mzi1QfF8jBGAXRw9o
CqHatjOMqOPYjONaMAQDYLCvm969wMGK8nPNZlGfZTxt4GGMm8tyLwF2GJ/KpD+U+qQexiSrhwlk
N+PTwK3SghzFSd0zmfi1FVAecXtX3V6oEoKLQloh9lQKRPVM9ckmYWCyzjCj32Ay0JQ44QZ4K8bA
FMxykgMU2mYGezRyLIx9fEXi4H5lAqy0tmlV3/PKI2tBWVAr/p+8kj301ySy+UBJJbwNkE5cEgkJ
OlKRHkCxBcnkDAuQkdAn130XbVx8Q8DyPXm8BPXwQsfd1V8FEmN7IbT3ZzMRdbBp4OO8PoqasFcA
2pjOg4c+kB4wnr6JnzY38ku07+YZnz2vWc1ZyuSq06DdaYse2i2xhxGMAv3kPJYJkC0J1A7Qt9Hh
saXJlaI4HFGuijuitl6vZZcHK9nUVV2/VeuLR6hfyb1h4cdPY0iR27RTvbM3LVXAUwLpq7FPAPXG
bRJNTone5PaNJwm2PcqeIwTm8UtoAdlxWuUYKf3hz0RRAOSXVhXJ1aO6PMaI0T9C9mkkXMHnI/4F
VAStZ8S+YZ+Dx2UFTefPN/VVdU6jUTB9CyhrPbn1GfG8Me2MhSKJMFa0KnP/5DEjZjx6apyuS4Qv
Edy7b0EwgLPzCyt+Wr84IrbzGqY0bpl+3E5o2OQed+Ldz56FJlSQxbyG7pNce8kM4s8cn31TQvwn
0R0O1tLTcyUOzMXW7lG5x3OEX+PLulZOvJRsxZi+QpUIglWI1KNFx4cP8BZfsxyOWBA3GMV85hvQ
W1mFKQVZu6YuC8aGLSIUEMkn4vndhQ3NHYoxNyPrKf3p5juHMICiz15v4ESaH1aaEBWyNj5W8//8
NsKrEB16Q3R0wjrsdf+6I9uKXAeVrqJcSmRmHqZjGFgH+dfSO5Y1J8SpGqlXtHD0J9dF3rAB49y2
+vv04Rj1MXmtCdv8FGMeKbPPDl4RbFDqrGod11jjUs+7SKYvIO9jv3wkRuKeuJZ6LyVKUn+mLi0u
zpOapwhzLR1Ahq7Ri2WZHFDUzBhVykPGU/m7ReQFI/d7s+scdsCiqIuQncy+cGnijNwkGlVVUWmZ
I1ZDG3rsJ3Q3KWMNubOJaBA+WmOeYo9KsePRtywhNKbbsVd5zBhhzZBiuXu9Xt+8e4vaS21tS3DO
+jZ01XA9MmpAzRJteyLAkRGHs6Ri/pI3P3Nnnstw2s956PtEXOMoEymJ0d8ZFCcsSi4tNWZ7rrUS
j5HOvykNU/Y9Gd5Z8Wvs/tGuZuSTRCJvDe+DudRwaJI6A9FrMQ8I1YB7IfJdn/61sViF9S6m7zQW
UaMlb1GLNlbS0hfORwQRhl+H9kszspkf1KjBnQ0OT04CikHtr4FdcxMiTxDxvAdavKijgvo0Cml4
NwBaiDS7zcCpAq4ekMgD6bn32erqxePhvLXLb7tDxv7VidQj8nSVoRewGQiot8hE6Twhb3xq+KZk
Iyfcy//XeiUG6xHp7CZJupkvcGK+/Q8QtrSnzOgBLDWCjHt1gqc4Cr7UrjdNgx9MghORaCTA8iPC
DEQhzABTQhd3j2atawkK4lbV7L/fIRq32knuNLXcRnJVepxp3OG8JOhtiS/WTlj7KkxaqJG3CtRF
esrWf72wrgqfaQlx1lORhPhHV9pAuStDBUlIjDYvkWvnZtnQjWXdYB+1yaaMPzKlG65o2nDABM/3
D1IiWH19jIOZikDfdGstxZ1RUKvDFZcEPoBANslYoJUUNTgU/qsLYtke8qWdTMKQ0F4cfxuocM2k
u6zY9gAaGQaQ64QyoRrEoFssVtHBl/3e713FKX94Zt92ZDfwwKvgNE0J16eRv/mzFbmHFa1KOTPI
ml2dvMstKJlsOnkuQRfPXfOfZzE0XbTXAlfEX69sgLEizv5kFPt+DiARAwNARqiZNVm4U2UIY5TQ
jLEABhmsjMOXG9CKz7Aq0RSUBAzTElUQ3m+c7qXM3B8nU25XKsau600pSGIxxLymRm2Mh5r/wK6e
pw3eUtYWIFAYfwZUHZQhf+iaZl+0i7faodyiLS/WuN10PNjBO8R5VZm+dWozR80wNdTfNxCmXdrq
3/iM7TfCkgGWFvF8DN4++lh8vwKBDxIPSekyPPVf0gA7s67Are6aCm10Jgb7UaAeXK85PD9fsD5K
nTsC6jzQRXJAol5izzna5IZKqVusXdgkJqUaF5YNJpW5aIHQG6PjZZxd0CJHfw6AoJd7ny0X8w35
JTEV+YyRHoDUMQT2isdUiUKHrH3PEA/ALaYsBJMzqfpHGd0rBuhA3APHwNzaI+Sw8u8MsnjQfJ+X
gyRBPqG5Hgj1aqTPKnolDbNZdTHMjfad8/diE5IDXyilP0/7Bx80I8YiUPfsATcARMIATT99eKuS
KjYm5ig6SrbDWPbiNQZJhUblfMjcE9O2+GRTk8Mr8gXP3zOopJQM/Qwba6y9BTPcQwvt6DDMa7Bi
wdJ21mU58Uet1YTHSIs0r2cKhpU4tDdSVZhYalEmywNa7evFKuRw+QIi4Mr9yvROJYaj60mYS83j
y6a5g7xf7sKxLPoRsE0tIsT9GyTp39qufYWfqHN+cMyG8t4pWNavEDGaIEO9TAGhxWlOmeT7o6ff
G9JUaHnjsowVMQH/nddcB5gVwreTpIkqTVFo2rqb43WRVXn4HyCsTPHm+/s8Z0z8XWJCgWKAXqm8
awpDrGr7f26MLJwxgsSbh1LEZwDsxVvkWtpcw2Zp/6g/M6eczlQgM/h05vhgtFZL9Gg2PhQp0Bhc
T+5RY2RarE3h5fhtDtDvCvssgheSL1hqrKfcsEcVFgUWz+XkXCTrOOltHAQ+pXlyWQttcsWK58eu
rZKjBqQl/1bTd1f1WKL008yBSo5D3zm1Rz8QP/+EkDEP9nW4rkJ+t2xsfDDF3u+wN/aaA+t4DPjE
LFM/JbVe5NjHs3VfLE20z9vgKkHEa4zpLwJlxGh5zethfHmD2dHwHHaWFMWhwcDIYhC2N7EDqBC7
/L3ZMqGckZ0TEBbYCe/50tDDzEWhUdJNA34GaCFXGW0goi7cVHQJXUsB/GC0Y2onTAW0j112VeL9
cT2xwq/j7HDIRnQLw66CfP9KGCfZMN74c7kfnwzTxvx0POqNHE4KCJFWc/Mk1nfICNWcNZCOa7Qq
xt7NlZs1ZwZ6o8P/yvpEsUsWkbh1fbdOe02UhnX0hVw5fxWXcqmI6KNUjOYveaaD2uDHLMA6Lcpw
RtNXuhP0Zv5uaVOzZah38QbdOwNiAPM3Uy0ON0Qv2fC7ULUDW9C2nozp6HbdgelUeSUSN6p5VpfT
SIqIKL485XLrASmg+UIaQjF60z9CL+01xreFDbnu2TxBhSjRTqcKANUhw/O6KBGTsEqAnLImNP+L
GPgwZNyeeO8qt54PO1mxHGOCqf7H04FtMejvdM0ABwjZq6wUCxwVgL8z3cSXvu8bPnADqrHSUK00
U7fSujjshdVApKTKAE1MRMJebtTb3xLWcjWsDceo9gOsqvfdQYmdIZupYJ2jn52KaCOiLzfPFubf
8HQzctwbAtC1uV7oXO66+rxtY5z76X0oOiUzOsLtWZEbwtlXQse5ijrINXzC32+h4uFM9jBDmItm
Adqu6hUPbry71908uz12Yz52JO/50VYM5R28CTmV1zIcsdwO7naEopL2M32KzPW8fVJZto2moSRT
ZRDM3nRC0FKwdJ89w17ojeiHfWuPgUov503vaqr6EBPqXWM1pHTy1RsRcumIjVUQYWRBwgnNMX97
0tS2Jr/cTfSrcNkmVRlKleLgcR+/PMY8hSEOtKvVzdP1ADo2WUzFyfJiJ3D0AMEInapmjavtyHKu
oDPM4m/aL6IP97yL1K1akzGPmiA9g7+2YYzLnhdKXo2JdZ1ozxCaJ5M6CZl9otTzThs8+rKa5KXn
9DbhK3fego25044YWhmeA0WyppO0ULgkSSObif0aDsG9GKBy5bdZyjCExvPRZSbua86eY4EWpKWe
SjTn3fRMUFdhDQy4S0joxu0vrKBCm5J3mCjjEjXUydIGpU/n0GzFV2e8dvZqcegxa+eyC+RTNY1E
i2fW18CREgQkCKiihmbiKLHvc7Cue90z+XU/Pt2aczzNZ+5ZNglVBm2QCbuXkFSBfdBM9Rtq7SNU
JlYQFKru1+08xp8enHm49SmV2xOuRyBhbgKZNSMYK+ulXr74Xw0TyfEESpzrXHeQf6LWX5lh5yYX
yo/jQxnHdtQUExeRTK6ZNIi0QVL0dyN/iq1H+JurlDaEh9EC2k/ZZ3IaDa5htWx9LZjYlv34ZygD
keCdApJjcqKnSTcWUhiAHKzVuseHMUV8NvvXKXxx6dirqKZU3NvLOPU8cW/50FKCR4ZazzgFjheo
sGfIxaOjHSXSpQ6roEMXk8kiPKITj/id/sEL7lyCgHPMN8B0WEopNa0bumtOsGkDK53DkN56lKzI
24/AhY6gekMxOwFjJ1T7t4IASYlxpOqcvEJoWfmxutWf+dW28dIIo84e+kx6WJ9sqv4M5N5d3ZrP
jdk2hNzpwMtq0FPfYj6FdFZEXgCP/pnKbhIvKYB88VW+1HF50o7JAPtCB7a/c+hg1bx1V5HuAQBH
dK0nz8JwVBM593UtGdMGpRiOb0ULwZ7stTEQiXgL2DuxC0KlMpChvXO4HvfIcx9oUdW651uOBjIK
tumhIH1j9SuavsuXHz77REQgvxMbu4fXvN8fTfgg7JAiZQCaEq0n7L8ehLXx6yUtUqvY9Rtpn3ry
VG6jFafEx+vvXdLkTMsHpNUcFxivEknn5V3yv43ofmqrmWNbga1xbqg6GaPuVDi7iqs7bPkJ6+zJ
PSXeEQ9eIDobgezgNBuNBuEeL3rlTvNQQq93JF629N/PGn6tJYTTg/G58fJgAstzkjk1HTXzAadV
sUhHYumX8E4DM/sQHY6OQJypaaCiJfkf26xnpmKPMXRwOzwA+fbiiUeQAbz+Jz4CU9mx6AZolqqe
sRAKNbfZ6Bo/elkTtmipMtURSOdGTWKRcK4uMaviLlf4U3y/l+CaHyO2nXsGv6SeJrTgsCHp3Ova
vs7rANtndhHzb9jA7JTpp3lc4roQSWZdx8JSgzdWOjKYrmN+TGYO0fLg7iAou5zc7KXbIBZ0hpof
FSBlHsehVSFldfB8mVT5StLkdSEZ8X8KO4j8ZfTRief4vPD7DcCGbGqbv4vzUDCqJGOw7g29hIbS
8AwgdmtoMaHjwUlK1uAh4KKx/IwH7URRU2xhObvy0KAW4kDnQHhObWmF9tenXhOViFsC9gaVNCbN
0CrgZdCNNkuiI/fhiqTcq9UXizH/MSa/T7/5DCnuExpMUvZ9aKsSzu4vJm+ZdAjhgneXT27VPV/L
bcYES3BVSBlF+0bYR39iUzse/LderN4owwLGDdPvKa9tMGNH5XaZmbjJEV18TK2QOzOYyeBoc3D2
tvrShAJdoqmTTJTHOfov0Sv2PPckWPycyZOAf8M4KMAgjVgF/YOCkd89FpNAr4JUYjXQAoorfipO
ris5pdrhayMJQIC3KiZ1WvMLw+i3M0R5qL6RPVNUmgGQVBgvr3HvQJX0qPJXU/rz3Y7ZBwdA+IaG
l2emKL5+fSuIK++dLnNpwW4ym3ukawAueV3VnKJEBbMT01ycC0UNE6bMFzziWyKBQP8Cm8Xt5K5i
bwxFwcG8cXT7qrZCnNMX8ZrMRXoNXBEtRmUXVSLwWS/iU1RKT3JJt4sjeLwXsFim/GY4UQ4pXUE1
P9sM2JhqjUvDKy7C6bE771L7J15arUtrY57F1kh6KvvGB6vYQMw/uNzWJmt4tyDILhTZFGKriEvV
Q3SMs09of6G9RLJBGDLHBJak7ndYCiGs+l77Z4MVtBIdKGRDWgyGmy2av7S6ZLeaYagOpuWVeu6c
MOqxp1SNeQ62DitQQEGpew7JArqYk1mhQPpMTGMclYCfGu63gHnt+34cCqgjbVVBQKhxdTnOnZ7C
H307cX8Ye+6EAsjqMyM6DOhMu+JWpnS5NwIwcJr6F425IiMo9WpfNVP0vd0Be+9hZUNI2xTp0gz/
Y7/1B0K0nTBU9REYiSgHnv6kx+LpYuVNGGJubTUA7am5Qu2lysLYTl37oDRbOJy/QuACmx2pncAQ
zEAnyLVfk+vPjr8LXQGCWExSfNAdBugOVXAXuILxY+ZYWgM8gQgzYC8a5o1Rx2xZ77wigH6MtSj6
p9dj+xoQsTAn+hNRlRUOSIM9bY2mOflZS6URabcdyEj6G08Y9l0RqGHOn89OHuPVtahDfTht669D
0QJ5jF6voYhDyc2ooWVzsPjPrvgPVRttfDJbNetUrZTmFgC+qKq5zEoEhB9GHzM4+SEJBIB3xmhW
xpWHaksx0HOKZT/myhiO8k7xWjXr46nH4myT3dou47eb0fPBWoUR8O0UZqdnhhzb+saTAMksr4iG
p1mjcTTySZhVrwR/aogefFs75af3vgoeGjtOGdaofCb+VTNI0z1nQo42R44d1f6rSH8uQL2JuL4z
w5pvUfzENHPUwU0dpJptdtJK3i0S78T0+4T/4SCfnokJV5mBjr/oZLgSK1NZhhJ/6UXSqDcWfOz3
wIYUzn29FA/nnovfI2eqcwLKz4/Lwx3XlWItAN2ZqXVnIz5ZVCPR5Y3D5Zyf1UfVDGTdhvrXLH4H
OUOWKW5keFXoarvHcuZ9TY7OhjCiVTDlGVKoANJSy3FhkeUK7sESQEg8oWa/AmlZO/ZhOQEsOQG8
ZoqCVlUaybmcWbz3lj7M4oLNSqyx0duKgRA+GJSwMtFyv0IczRLfqjCohb53BjYH93Mny/zZgMqS
xfBXsqPN1Jr/Z8B0h3Po7BAWWQQhJ5dVRnMxZDJxchW8lJiZZauy7k1i5V+ctCcUfTcLeZFs7zKD
AXyNGbcgD5SdUs4j6iq7YrygQgsouapkArqsMew2fD5ZpWHwB16Pt584itBV8/hrT8ri/eyih4ZB
sLeYFOPdQiQ4g3Rmz6n08dgQ1wxc5bmfffmvSkyCdeIHdmaHlsPn/zoE8k0HBAXhQousJi6uaPBY
vR0jzX/RtdHJbuLg2lXELF4LVykPqp8RlttC0SspUdmxBjbwf91D7CWJBMtGnq0nMlfB4s7ew6dd
B766P94/CVlEEEpYjWj2Nc7Mw9ej1zoOLJV4K20PrveDYzR7N42wa28aULMiwyLNUkTGcH7vLczF
ZDlvaZ4er+OASe8ma0FWa6JLRaZIEEV538Z4FHWYrqK62mC4OK15wq3UkKkYIy94BwPB8NIhqHIF
YNSWNgJTcHREW0TzhqUwrcOc84rEES7sFLsE+8NNfe8BY8dY9BLqgfDXWONrrA5sAw04TEEWplsB
t9oDPwzMz8lhMXZhsuV3SYcayDWJcE5l2d3DO7CSLtkITCp0phPkEZ6OzgulHPEXrFvbjNJVOQrH
nGHTUGjqDIlj/tyHMrOkHCzuJCrsnOZYaCiCGLC/Q3GkcwQif65sRIozZzGbV8IjoVoLZkLbdn5W
QX0NRBRRRISMSl936bIeqhsTIUR3TqmlX9wDZiVDgdGcZIfrztQiGlAjrMPzAsdt/cw9JSxWVPTb
uEHgkAd4l3dv7MGW+M5FjMSuoPhIrQQ3JirETrjhL1peinHOa2ANm2OjQsMcaDR9CLZZWvbxGEOH
EQcUe2k9O96EyARDZBoWLJV93saPus8UQfxreSsG6PUhHaIn7enzKyuRNX/rHfH3TAGLaoaG9WI3
j6CAUmAOFvuF8Qh7pqF4awUgd3YeOJFSDjCk9SCV4lMHCUPfJqTR1CW+LTv75DgvCnv9wngeytke
rGNY4SqG2Xrlti4TxkPHJhFF4YEPMwhUxTJR9yVSh1G30bXWtR8hf+wueZaU/NQT4iFdgExyCHHW
S2yKzNk4FrIaYErA0pkMZg7VbV9J5uSqmF2aYOISExhgKpJAH5XJOhP6m4yJqYp1IIeyfsZtq4O9
E2jNiMj2i+WlLgEaMyDglEPgiLeGcjB4IqNVOrcDetmWeSgt+C51v7SZuANwFFm7LFL5dIGmdbX5
scHS/gWGdK+cKndJPNoyI3IINrT3+bfoqFX0QK4SYDX2J8s707oRR2JinXLC6yMJqy/UFiaYsWnN
E0DsOYLW1SqIpc051DRo7tzAqxFluwwgAvpVKeEg4tMQy9fjmP64934GmCKKq3vurn8apuQLf0u6
9qLRt6r79c+p6Y4Ysx4B/R7kNthuEsQi491W/PM5T2G6z8q6jA3btUkJhhqMWYNIBV6GqeilTX5l
9JZUCtjvAJ7KzVPgmWUR2U6a8M6d8I+t7t19HzDdmDrZSZW0GkWUS9zNnx79/FhIbeYlUgy27cov
g/O4ll4nFcJIONQXl3hx+mzDJ+I921XBf9BA5OJZhbOJslUlUwNVWYRrPB/0HR31kJzvm7M8+FjF
3aWF28opE3MAXZ2Xf2ixjFZ4EYVY0r33ZWusV+0zIKBC2OfJHCLyNOiRHPVralT+MxqEX4GP78vW
DM9a4kHPpuaqwTQ7BBSugM4xEc/2p5flqA40bdwxffdpHkWcQoG2zYPiwkp4MkeN72QfcwZchi+s
XNerp4JbVKVHVc9pt5g+5ZLLXi0S8ZGQVJk/lbwD8NwdP4O0FmjVRIifgjlvX1BH31hxbDBwFtLo
H9GytyxMajn8cDPhYUM7nCg79HKIuRTwaqbjrHBVgx1B+S4+WyNG/CgXIe1dklTW0vgvf0fTnMny
x4ttUHeQx2zYvZuva4QcuBbCoLNNFb7q0etGEDp9aLCw4fhndb0LEHVlzdWfQPMleWFC2uOUXtBb
+mrh6M9aXWNUPOJpccJzGrP1hr+BUCpv/t1HXPnj6RKmjuNteGw76U+cxWn0cxW/T4tNMHh97i00
eILzRIkwxRIbHcgycW9U+ifoa+Fd9HfQ8CXSuf0lhE3RR3Ah3UrtjnDPTtGWJxBQEIdfKQSy/qie
xsTpl9H17RBNkPVk2L0+99fYggI3epexh/kwAkHPPN3lPw3PApUA7g3sDsKLB1/kE5r98Xp1SDHr
/2ce14qpHAfUZM0vhoy/nYahJ3UvHYiR3CTMFZgWlUJeF/MkXP8n2Og+Rxow8TuBf21MOzUH8EI3
0DRX8+gxrZvG1JDZDGfNwbXKgLviuupXUV79YCkCxyJqxPXJtozmVP2nlHHoMGb2yj7YWUNH423n
kpxYYTGmOKFrQqj03Fi4o46D+hvDTmlpwp2Ox/V7TJlcdDeb6uZYbm6DOYsrnv5rYISfHDQIpIVO
VkwqWw3nX5FsHfUTvvJwccEGLom1niY4zGieXBQNfj1c1FY+T20sRdOXms/cqrSLH9GL6pL0blsC
DIUIspbvNlvNZSmm2rdcawo4QXBcPaVLD8HgprJ1uHH2MjFX04nOjAOtBfCT33+fvkBMVpWsruJz
emat4YoQEs7z4nop8Qh4DtUcdcqK+3ftZ8KrEZjbyFlkR0s39+uugcKoLXGe2AMIB2032CMK0ylC
e4aQohU0dREwr9WRy+DUQWgG+9xoYQPQ5S+qyY65sSOw1wL2Y33DaAfsj6H4IdIMF5X1HapLzyAq
IPxKL6wavp7odelAWGxw8Qg1OAV/4NOcdxA6Ft5DiMMvQFX+0qeJ2MX2EFrcWbf/m0FTOGctwt41
r2w0kUIUHXo5eMKWbBufNpMVW42rgCK6EHIwoGhTvguGA2ICNCKD2AvYJtAY7IMDPTTLZJJjgqVa
kMoR7Q0Y/+fi3RrXkfrCsZ7Uh+5zgKyRbikYWJw4GQSpoq0c9MPrazKCs/7VO1mxAduqhzHAFJ5Y
g2y+17vzxYvYnNB71ZrWrVumV+ZUabUQT6SMds4dX82kYtvwtxvSra9GOsNgdAefxm90/uQHJnpq
3GmNXh9XghistVQHoBQPvLThjiUi/V97qHOMXK7zPu6BdkG3Gp6NZevRcgJXN7v/ql80mN5E5rSu
KPqflgZ7fjvar8JENWlAx/fA0SduIvYD4qIjVA+h6TXL6hCxPCOFU0AQeyh1iW+OkJmpyznDkjK0
PV7RQR7L53rQ/z/cJnd2tvrsam5Ni1LrhzlyxRmE5hYfIhSKf4JK7YVrCmi1kKzid4hunoQQntwA
EQOD4kqt+1lq83Ud3zjLcU0yo6dgV4sUYhrGRSDwp0jKOY6c3x/h4nMjEkXriZ4HlAzB2noMK6a/
kd5j0UvJ3nKnT4AiGd9U2/UBUbbOHOp5RACUi4fP4IM4DuD4wx9Nx/GkkCYPHHIqViVwQ+Kv0FAk
DLH8mmQZSg2rIaplqeNWvU/ktE8elgCpolrKSasdegvhkpAwaZfXqILPDXZjepOrRKcJrwQpy9D8
x8xrhdNCZfMbdjuMrqLgHKV4UHSsBmEgnSfXJIpsJ4ArG4IUK3e378NAUHvlkSRQ25vC3LpootLi
UFh+zMQylp0eVMkRUhDc+fN7U2u9ZKnxG3zs8onjL3MEcmpSH0N3GpF/6yhvEdkNnv4N1vqmmstx
Qrt161rEpGM85yoUiCsUBlFZWqHFz6LV8e52be6ww8roQQ0OB6FPfBr++I8JJb9fBNdf4qhaWGuX
+c+Q5giUqGLqruGDExZBVC9WXk29UHvTFsYGlc7pG+kHaD95VDtpzUdMrNqkwPg6Jc5uWl7hVQS+
HW9mVjoWrlsApo5MvnnorJxeC0UP0XKwNCVmccWyXH2atdcPhWhBvcBbnLuNFMruwbINGnQtOxJf
0M8sCTX0GlpsccTt5mWDedAAWMtuwwmEmO4SVJATJmvSzyfbvL9ZlnLLLhWILXkkyy8dkI7x5lUG
9G7N+dM/HQA4VK+/e3S4+eoCmqZJ2eSHpBep4T0NmF048bkymg3HQKWx02R3XOz1mUdm0W0HAtj7
SVCQmHuq6g6XZWJYt6rd58JmRaBRoSA7vaLLXUXmKBE645JQRV+T0YpayWoyEyRdyT1oEJ3C8jQW
oMBcLSFjTZsPKiEcB9wZsjUO8hztmMQpR+HNOAIJxHsTfsmEFpUBmwh0/eQzTkKbnEXKSRWqGM9v
ITzQ+xJ6wVL4NG8HQTUMICydiNQF4GcYM3hfyre0GXBJbwiTeATOxMZJlN4LdKrQyVKMteN+FifW
WEgX2ZufgaQo2yRGmU98+qYje1qKLGoDlALwXdL2XYqQ9lduIBe7jbiD4Z03T4puz2es4SnL8ou5
HnMhSvyg9hMPak/dS6WFt+EeFIwrjSWy37QtVRXuSnfcElmK/wrLJhVJaGVNVj5dfewsuB9sB7Jb
6QyS4+q/GsZkYBZUGJSyHnn9FIgEsdXg0bbx4P1FM3Kfbd9rcb17h8WHYW4Gufa6u+Bvf4ZUn+HV
+6svQJBZBwL6PR/UtGZFCPjFcu2Ji7C8+KgefNjQJ4jMMrpXm34G6irpkaXfxMTgK5hiRFjMdFYK
FoPaC5QU7YbsYiFNmQvVjpAlbZzQdxd2jqXbnbOW91ZpUwUoTseJostrSKHqsH8wH84i9xSSChWL
oLta6IHSVRbETPyUy0/keVP5+F8iyQQZD45y8oEJnNBfuB9M480GFWrb0YY8GkcfIszN9qK/rkeP
XEvOnGRUY4e8FoOkJMHjsqQKRkoNrNqraaQ5algWdQzl82VxWZUQfB0/20vcT9UvuWeuBWg8jH68
bvorquBAr/friXrJ6cpJ2JwLeFxBdijv7QYz5CCkEQV3qgiOxrYwjC6rOTwJapMUwSiouHq5bDF+
NlRKXJyRiV5HvXm7Og7ZqXZp5no/TGsQO1wn89JOl9Q0VdXSsCN8taDimsy5UkBgWxq1lg2dmzuS
huRtoEUnVBvXJHOX+tUVLwl26/j3TqXRc76R0HVrfMLhwmgc525r1OExUBBhhH2PSvwurhHF/4pB
SEgvSCO5/yhyXmdPb/anhYr/1r1ecJgabrg9lHB5MzC5Fr8ZFYhl/UygZp8Oglq+5of2gjuY5mMW
rCWmyxTOMtwDKULfwQjX+60Nkow5gmw1pP04PNlVdlcWZXOyvFbUCnkTI8KxcuoTGqVZpNLz7q0f
jSDId+XjS7e9VvbfdoDXAki30Oe8R5xjIhaZpBIqfDhjHNxp86+fy9m7iMCUjEFCAn28oSCh0lEG
+O7Ck6o/XVXnD9fz0nJ+lmGQQiOhCWdmeKllquQHexI+bd5dGWxNcwCzSRpGhxwENXYUHFmz89Bj
wFM29WO4nblXPjcB678E+Wkffb2DkKaDycvCU+XY9F6XO7pfkbfcjhZKhyPYMW466WU8l+/bi0I9
o804Y5ub03IifnMxLmY3j+TgTroI5PLIuO4OCkj9/FK4SilW5l0YKNwW2kLVfijZasCJhlY307Lg
Id7AQxqAlNueV9qV8MvZPbiSEpogIJFRNWe3RFmHiJ80SBqP8Tg2VPC8W5aiEVbZLCO/XK6Vz6Vt
0xm8Z6LFzQTq3RwM4LObIlAcTqLlB34wuR6opi8GzdDY99Mj5dn+hX0BtgpP8sYPTRV+82xRi3Fq
eK3Uqdos7I81PfdH+EjD3G4YrOfMfqNiMmCEYbIS+QPG7/fKEiH20vXzhMVJadUbwoJSNEtLJppE
PjqJV6sJq66712ppJMwZQ400Bon6kZ30Yqn7PhfL//SmvtKRJ1IKxABv3b9NL3pq8cQvRN/+5oYB
JfmKyJk6xxZlxmJIy5E/h0fl5Fj07666LxX3amgkk1ISPph0qc0Cha7FKQa1NhCy1XPglu73okGP
AWvOU3EarE+YygYGA9j+K0FI7pMc1Ec30YhVIedMuh9vc1B0pcfU5rqxFudZch8b/B+atSPjv+Ea
WbaQOwhucUTKehOl5v7ftLKAia2GrFwgEcW+a85q2ZApmt42rK0Iwz1y6PCBrAYPkCgZovz6eX29
2NYLeKYYy2sRDWVgv0CX5gYWHU2kfIMsbBvI+9PxDCRRKpnt17cpu0jG0FIHJqPCL9539Zfc21JJ
luuSjr2aDlneF6t2dr5VH0EijKbNKTi4ZNISVrT3ui1yzHzfAWzHMK4a4Ar1x76kaCtoBiTH0717
474DMpiKRTPO1seITqeS33kumv+72u8bLxpzQXuPnxtfct+j/zDpBX9pPVyGzyMXIR1ZkgihrbJG
nrr3IsYaZvXtsiJu480KfCRd2PCICsPGS3ckDPETHrw4YXV3DQtoipvvfKvo/S8NMx5u5vqa7Maw
xbOJ4hEesgG26JlBwjlRhRSQzpLgJvnmQcKBXJ2lx1y+PwjpeADBud7+/M89I2g9+Q+1MIv9CE8Q
/qr8IxFUTMaww73e66JdM2EUD1RribhqsyT/YyKWKLbikkVp4FGJv17YRlFgW39YVpx1ioTKpocz
EgvW/8JeV0vqaN2O60RUHt+tKA56zbM8AnWZQXxQZEQIlItKrFc5JX+pFe2z2kHch3cn7tf8gf+1
VC870cClci71AuSEbvll+ckLficSX+sopu15X3MNCRTBs2rzOuoWIO9eKBff2J7H10XTFXNCfI+j
iS1z5lBHoBNS2ZojLCq4k9lZRIFJKUe6g/rTzpZd7Jpk+wPq4pOfqskvWQKKBntcglklMhl7uh/C
d8HBBgDNfeH9dqvGcbKYu9QkpvaLWjZMx2tOWPs2+U304nn0B48MEvd8cGES3EpljxKSMqhTNlKV
yaG4GaxsoyspWSdZr+B4FAi8WqviFhMaXX5d8Xs7U0EWd2W6UDTx5NWWss9wGTOfNgha9ZYQt8OL
C27AYFJ1lPLm8YpCYfhmeXNEbEjM2uHTEski+SCk4/MwVNf8fgEF1LdP48F7/mvAX2RzyWO7cW7q
Ok7OtRAi+Bx8VaA/F38LbdzrCn97TB2i2nm5Oo5ZtcsxT4X8oqRmGRkGJza8PpGieQAYuWJuTYdU
NTNyWFgo0KvaibbnXjesNKBP9hLs2vUd1ViNemCGb8ii5PNg8IUkk7KWBbsLItM0Ypi5RRyCoQ1I
NS7t40xhA5I+d2J3KjOw0mBApgdQEOsQfQvHNngnU/2MdZGJU/zVcR8s/xPPn/kThXKe8EVFVBxw
4YY5q/qzNCUlRLDH1daIRxOV13blcNlKzpA2fF2Q8jlIt3Xc4vpd7riOs1loVv3lIKUdL2Byolh+
BQu2DAtCoG8C8DRhJc1TIS3lDSWyNzRGKRmXidrBNbHVUXerZD/AjPteJccSTNFb6VU+bNbyCzoM
U52UkeD8H3SQcFNKRluGeL48rqBi7YrVBjaYwXfEqBICJ5yL6syX3dZpWea9JW/XjV4Uej6zlH1g
x/L+4vCPNKv1V8ZX6BOsbbE1/mF6WOlNwEPI5xSADKZVFmoul9Mt/r+cFZrjN2c96dvqLa/KQFHq
B2eqeJfleA43S3jNgoen09FgYmZy+Oqeaukocmiw2wu6JPPEz6WrLgPrOKyUNY/gKDmwGzvOk7Hw
HT3651WoI3f1M992Z+NhyQqsEgSChcoOziwQB89j756enC7JYKFMaNzEcaVY39dDeAZ6fTMsEVkU
vzdDBSes1Ge2qp/yW+ELePKU8J11dTX5MUaXiId2VSMq51/3bZeGjcOlQY58xfi9uCYHRhi7j7MR
kpSYS4N9d1cUhdBwbtqbiv0sGspX9MFBrW7K0MWQRLKsn2C2gNJUar5+e46IcyJ1NfsqqhqjdFPV
BJ1WVLIPc/YL2rt6hXzS/afg59CudZYTiXKmslf2oA5GPuPMmSvTSmj47+NCDmk0SJcQtF8oQqLj
hfDHhTsqs47OCQdfiOAUvdqbi+3dhWQLgCFybTh1OjHP8gipZFNF5yj2FJoZRwGea1/hvWSbt04P
p+xm9rJu6veKxtchTxYplV1gdc5REJB3u8QxV21cqQUNJJvvaxw9eV/AhjAFpmDQmSx69eAxK2zD
v1jZYRsaG/HtHuVQpHB3lEePIU8I65JgdHu40YtNmDXjkFRoSZ1HnynBiNERK2uGJBQ8hUSk8W6M
SwmhCgu3cok17JGgoUez0o8tS7j7GMjKOjMUN/sEwYZLl533w2I9Q7jbqJjFScKqni5pP1UdJDM9
Wb+O3mxY7l/vfZGVwJ1rteq3xvYqs9P/fwjnCEO3xubIpmIYdrC0tjjod9plf9k4cvcyU1/WNZ4s
p5Owc4GoKAvk42XU7TsCzqFE1ZILn3Yg4XlubHnpTCPwDVKpP3VhNYaslgOKSwKi8dkDGYs1Vylp
yr+NKkNWDuClLXOXB/iBNzAgnNAuDEssuq+Qq2Cs5ygLzY5YvFsWny+gEWomoHRy2A/3ioeyIxt3
yrvwduHLJ+7zLbfQqAKJ2qKqwCqSA1covYHZSdkhPepZ1bf1O8nNvO9Dd4RUKZqU3IBQlpFM4r6c
orN0vtQHWF/2Nba9JrvReGkx9Ur4eVGQvicB7skfYG4GELByhBPxR0xAH0tj65r7Mau996qYt7j9
kDu8QXpQ1yVPYUkiPyZHf6CsSM/LFikgjOF6X9l9PPXzcLAcstJ/gKJfDhyCuoNx9Yw7W5B4LQhj
JncFHMsM6/6lL0lhPv34P8/0jr6GuEZtlackNJagNYQ+Bng01ifPgdF/8zkyK1OrnK39ACHbtLP0
OjxqLWnorAddRqXCFQNigyTZCSdZE42GOE2dVQddV+c0qsmnZTpQvgaZUGcxPG7hyg6AxYGqldYU
s8xiDthkkSoS7pXKxO5tP+x7VRVDTIGiR2P/rHvPxhVmOJlkQ0CS/Ff3Vf/ZblLaVd4bmRQZlxVb
Co1W37drT4QsaTaSbQZDmMOFNrbM86fUevEJqEjDdBaAoJ5c3y1G0ddnPmIoVwYz5Gu9hVqwKZlL
oW0nrqAXi0rCjqUVFbbPBLKMiF99lLZuQ22OfRUjBnMw9PwkV9lomjgmnkK+SlbpcYujdnAIOJHq
vJY6kwvbA7x9h1rDag13VXm1+BrYaWOj4T77zuB0MTFHzdijcrYKpM+p0qVHie4bOp9De8Tq5VVt
wTM/8JEvlaXQr/BDizs8UQcK/sXPmm/hDKig9EGYi4jN+a3HK91bdYH/OalFxfrbZ2QdEAvecTeQ
5/ivyvRVbwAKIPMdbP8ZPUtRB9hwRfANITky+au0reR4OMpL7EWql+Gs7REfnbjmn4kJ+giN4Dzd
Q0/gDyADw3wqYC5fD1/74WjeDBfV/0DVV/yTZMrhLFqravNwv5g7T5wVVW+p87xEjRDEmNR3giGU
s73YR5G+F78+QZLSdI8VW2ImnifP/JRGjGClP0duh+PiVUzIGC63d6t904yWid41hUcKi2G9/SpV
Tb3V2S5NcD0V+DB6YpT88t5xU7fL5ut90zyC3bK40kpynq46ybvjKpDyG3F3wGr9Nxjxg67pZtYh
/L10doGOqFnJJybCG/t606/sMUG3JvKHbghDpUyUMb7UhBnOfDG7qI9yIsZkzD+EbL+NqPgSwatk
5RKsfQKT92gCbmLq+GSO7qEw+0+sJLFcfOkwooQhJmx/PegYVj0bEelMuQf7mwNby6jn161ccx4K
lYGC4WxpVVHkO+0stg+aBAH5a90W/56zHbYNZ4u1Xoz3H644oYoVYfM4faTr3+t88JMwZny0Vl/o
SGuMTrte3DsW4kGSr6JvlNA5OMjHgUxwLh4AhTu1MmEzAjJo+mxGHF5t+0wATTfXVqUe8cc6Nyfg
zH0aMWvb6evhDAvaNY9o17OU9hV0VIiUSEUFgqkC25GtlACfCp1BANsIon1qqrn01i+hG1NMp453
Ha6yqXlay5g5fLo2yzjJmZzAZiKNoTfCOEmqJKg3SUb9T9W1298JZWTte2XDNDGCFXTxl9kuYYaF
Li/IjgPDN4wD2XxOSlqEy1lJe2NpA1Ci9aZ/qJLNpDjECREwKB8IkaR1aT+L87xV0SKRabpP/oiZ
reD8fN3fCt3IJhO2pQ3OPrb54kcZVSP4jOokTbTRQb4yUTVCQO57KgKCvCM5GVwe5FfbGEurVj/i
BY1E4xkAGqfCcWZQRuRioe9zsGyPc23rzpEoNwFVwU69K356Jn7ZkNJJrQyDi1BIgdgfu3nLeP+o
bfGa3dpRvAaDC1i+xIdTYdEYnDWeDoYmud93aeGlAma0Ux4qIRA9GyNr0myge4cQyXNMDpehZkvb
/ZtICXUR499tGvVE8jioMNtYnNpMYaTdZ0DsH2rFskIi5NWaOX5ArX3bUKTSSw6TOkcDdn55X4yw
pd7ZjWdumqjVeVzkvm6XjcV17HHHtAFIxdkqbGqqPi67YfzcP8j1o5aikZDrI74UtFvxueDFFyZt
KYp9I7TCs6VkD36bs5a81jqufpm2+9ax8prTMqq5N8NUlA0PUhXavSwnly6gN9I7SBFMd7feVbjz
GF+xvTjQZ+onOt1uMdMapnEjkUjQsf6TcEhxwK4Lk5RsuVgwLkkqpwWWMyMUPEUXKegQdc2YzQfr
WC7lcJcuI9auHTP8nUKGNHoNokgimHGsGb74FLp/vu9S60LRO/jeaGxLXy21/DMZ2Ve/eTvIJj7f
EaTqy8SznMtbfxxMCU67JQzlSexJU4O4FVsI0YthT8BAX6Cj2ymAZ/XFx5i2BvRAQO4eD2G6Yq44
0WFwsxX2vLuDb9Ek5WrjQhbSOJQikrDTtj4nobvrWWA8EfV0HyufefFsPFWJlMhDn0sk4IV1HMj6
haCUoUO5/usjRH1AFJC8t3sVLTYtzkcTj1ACyGP1Y81V5R+o8OURWUhru3fZaYxHCMqK1KhK4dnN
ydc6A91CXtuJWhen4xXbZRX9MlFul+IR8DkXSdC9TQfqTYWVX776ckhLBatJGJSE7CnrnlFOiX6v
ZUYddZ5BfJJuo8byh0y2H0QvcA+LfXzFD8lpfCDh/GOz3TWRexxxF0Lx825oEzYgM2rJnaXDxzZU
NJ0Au20LmigNnqJBpZEcmL6fcTKQWob0qrcHgL5+/QALtfoP9c1A3kLiIOl6EEzwOq7MxksM8sC0
xNKITZVnM9HKNc4lk2FbqRBsaFtU40V2hsXzsOIh2CHr/wmQ3XfD9sNdPHUAM4KJ8jYYlpaNoPPz
vU82F1CiHH5sGJQjM0ebmXHi0HZu/uq7wGtn62OxvBh4VrX02OPs7R8N37zZOrPx4a4MZXo3Xo9y
GRT2UlBI3BRSErI6FM//5E4Uv0sP2V450SUAS8MlAny4CamwdAJzf9MquNQyagRina0ChGaQO8pv
QABfSvbx4JzlU7cuCo6CZXKRIqQcKoXLflpHEJcXUsvvLzcY1pQXB1Pg7elZ6yCmVNtjDKpVxFD8
Lt46lgkdScMCJKMjdQVkgfNWgu+PFEUO9qaSvSESIcJaTK0gI0Ws2Oy/+R/udgEX3urrTsxC+/yk
jb5+M//qjNYFBKUv22j3CCL5Mr/dq0pz8NlvGmSykflGAzJNNJgaWJGXQtA+VNl40z1+yU0nx2eD
Z//okKmkQIYftQeA+/r2DgZ4JUe220EglpvQc9EPNaMq6yRjJIbOECUPbkkRXkOxkou4BL/mVUP2
Zhq2I0dRdjBIkDdFdfmZzbsRwn3kJ+OVP1l0wU9zd8T6AkLAIiNi5hXCcvdtV3F5dpmTOpf2wvvM
z40Qhh7qgspEIAlXyKAs2D0X/WeudY8dxtOgBNY+4u8WFDbxOshI8Nnwi6QqiSVEw+98U+83vDsR
RlJ+IRrCZd1JpZB99AbvLyw7Qg4cIxYgOx+2it2dZVk5vbuohQ8FYC71lxooiiIY6DhvwHSgY/ik
epak+jUEMtb9nKivCvEoIQjjIw4ZDjbnTC+LZH7bGLHUe08tUkOea092CyMPDhUlF5/m2cNj+9hq
VrqJPoVphx8j1ik9t3KWU/glczvnmXmCktykAHyh+QqSFYMTjqZCZpFPP7Mgd7utRbY6T/g2DS0D
hy07jWuQ2Ww7wcYwnVVLA9o+z3J5OvzasI4aJYw4tW/B+SYBsp/Uj3JmtPomPGv9E3nqH4+h0Nt5
krvgaPVRdK/ZpN3HwUFEsxg4RH+MGS11VRVFCh9xeke6+syD6aTeaObAx8lgKlqQV8iPca+JkHoN
Fu4whR87ZzYiWuFu+As4H5lS4Tr3/ueAkXaOepq1KF/+6qW1upKjyX7bQ3u5qAothiilrqZwLzeg
1OIyAebf8sHRxWfPurmpW6z6FSdU9znaX51BZ4nrOMSgv+onOpVfAEsT1ZAHkF9+qIJvgJC3FhVS
WsnNkNrtay7v9F8RpzcCZc23a9k8q9E3VkRXcKopKiwuVrqRs2zENFBeinIfzHbsHPeQUGZuo8yH
ZAwAczxYFWx7Ev3aVg9J0A+BCJJ0SvlQQs4CmIqnF/k0apaGytxcK2LEa+y4Bgq759Wh78d/yh5Z
2G5MyzxMmClEybtKqcjaiik3ps2ERbwdoQ5gT7bbp1JPFe48iqyo9+iK+VR8qOPQyxjdp6ooGt4S
F+tSHmluyoHCDOvIY3VNSOj7+iBDwjA+Xcc0sfhr0eUFRhfve1+q93zrYvD6PrSxtmiqSRjfncWn
sIDJ1RZftrGecGe7ANP1lj59ZHKqAw+N75Y+z6Z3tvZ0Iq/JdRTvvxKiRLNOsBS8L2maB4AvtiMF
a7CDO7DPafI5NB9BSSil0SF0qfql5sWduGD6yY4UTeR0QxcugeTDos2t5h73MpwfqbAgTVQuPChs
DcPFb/dJONcpISmNSlQ6OFaJacKxM8MTTTXBFYA0um4nyUx4+icemKjfYYOIOu47kC21wl4pFnpX
mftAIqzjDppSjhlvAVfZ3bKsd0ogfewzbl5ffX7SitlqjRr6AnNhbLLowcRnSXLQpMtU/OMHPspQ
XNWfixs+4pp8L0AufCt5qvoXZ4teGPE3iCDB7SB8VzJcNXv7/pspx5WM1e87IfAYivdmF1oZDwha
mWKe86L/pX+JK2YiPRmUV30ZQD6RYkObIxaVw5ByAjqHKcnSTlZnGG8JsSZzR3236a4iX5s0MPB8
l/TWrXEVMRkCZL0xYkMrSpA0iCDClbg5B2OyTDqKsCQ1ibQRBzAhVH1dQCaYxbzoy9NfEhY4mJzM
kNYfSFFXX9QRZ6/x4Q3gJmiPH90uetLzsQqyyJFsYd6tRWq7QP0M3TPJ1ikeRH5yESxjysd1WN2l
1CMrIWzYhVRUYJ0EHKIF2BlZXNPq4swn69K7/2plgMs53SsCZtm71Z4UKjg2xb5kInAM+iPJJh81
w1EDo5S4uOBMVWQ/OsrHHCaBg60IqoofhcC5qRwO12491tT9ukb1r/u4wQCyYsaXkf+jyAxAirEe
Ofo1up27O1lLNQCNDwp0EaxCSFwhA5Fakz+uiT6JRIG149X68d70ZkvBcFWAyTvp7SzhBDxBt05z
Iwc/9+x6VMUuySSX7gF4PFzxAxcdxU87aLhOh8TQLXVwFidx4IPWoaw8elBSIrqyUQAYJ7Li/Cxh
EvH0C3Q2WaJuooSiN98k6nVgjxTJNZ5kfu2DH4biSvSnxewUW335MqMTiiVP7pxcQH7VOSiw/odb
Ha5B8ts8lKRl/E65tF6p7nhTmmGBdU3cq4JDYZqe+FDgCfAr6H1/173EXjHltKi0abKxAy9FBNdP
mkU1UTPKYb5DZRMOuxwgsKL6Qvvg84TAc7PR13XnsIYPVb2Dm0C7gQgmlpsL9xnfsvNQE7Hd5HRO
qSSWi2W0CGB6WhD3oQ6Mv8ghU3O3AKi+LulsWtnB76BTh8vO0sm9ha5/92hnp0KrMImQUZcS25Sd
DqETFrvPLB4w5MD9HIno6qhbtp+4mDq1t4rWtHTbnpAbwzI8j1mMwGZSBtK3/kFHfHUV6UIoYLRS
MtM+g5JsMgotW0lybDm4jhYkqeuhI9THZNAEvWGhWbwZt+1TJP283v2p81r6fJGw/9RJwKu0OjJd
dREci1sjHQhnsfGxYTLmqo+XUoLXqqpPLGgMnIDyz9dO86b4tCtfmPgorZAf3q25SI4V7oCBEm+K
bZoRZIRQQXX7Wy8h/Y9XJzTRBIA/7geYY3aCRGpgdGGtF+YL36BVbIsHDr9Zt7kCfueE/Vs7GDbZ
4RCSHkAj14QlzqDABWUI4Wo/NX6S8m7DwDk4v+skaKeE6u2hlS8L+RtN9oTy1uvVuJiZ38PpOLJH
p+cTHDZs4c37TjVshYhdYv8VMJoq1+uRo0SO2BsccUzPYiAE4uOnGm6u0ja45f7DxyobLkkbfSxh
DyFS5u5d1OAnSi6BE0A331hHkM/LBwaUWdl691Ku81P5+AtoznlZ0nbRgtw0da4V1oVZ3IEHmXKK
ug7BiYfzYXXLlL16+XK8szz29uqsVXPFMWycAzxKVeSWHiMlvFVVGIAXRL55YFyMVh/4GgLnmXyj
lceK27Aj5UdANdEcg+cEHqeh585xKa9ep0oR5uHPnqfUcwwqtHlY5sR68LZmQg0x/Fsdfc7P6dHZ
5cy6WNWrVFmC/WokwgsaOw06ffZ9OvwX6qd3tYXNgQyv+rd6vyx+bA3zYp5TwXDSV+00EuwMn59W
plvv08CC4Sj/wBw7aNZ6ESucot7HsDNeKukJn1lrDcyS7wQU0nQdETeJdfuLb5dBx74vuSJT+Pyp
diT+Cm69ZCQwC62h0X/PtroZSmZI8GXzwPm0uztIdqLImHQHllHSBipCALPi5byIEwTtVEB8laLW
hd58WBccmh+zwgq7mSYthkdDLEmsu0p+hM1XaP/ig1iZRWs3pjcykUhmelc+4AANOZbcq7SJmnSM
J3gNWQz/53K/W3a4anYpOYij+KL80a81+N3L/Ph+ordrsJp/u19MWFafJPbST/PpZY5IMo6dOV8h
LqQc4eXPBiFXvP+5PGw9H8prUfzGgJFvw5Ki0Yx42+K7lTc+XEyVSqIL6N2px1Ohyt1hRJN0YMTg
/TTbVdFoIXJVoEFYvwrsFF5/I97osP/8XNUMDuQ6m+t2MUYyWaAAFM/7IE2S7OtC3s6cDVCrSa+y
rU6C9vG4VHQ+H3vvnsVslJWONb14XHw2pMwqvNSrcHKQp3tIFLttPE9SLI9A9xbQJ4xSiC+9BmiP
G6SeseJ/9lkzclt5lUV10cyktYA+syEX+2ASgGjGfh8flmGgRyhSZqLAhGx5biwEyS1/owx0sGCw
kU2ZheqbgNDeVlmP8y0wp4BM5xRilvQsRJZRVji+QLg77fNFnGEMgCF9dXeXdo/Cxqjt6vMT07C2
qwjb//2OqZJcQPUqSDTnK0bzUuri/ccLgd7p4DSOWn8lShpZ+lXsTTZWQoRCJrISZqJm7URNTrBW
Y3MjZUIt4IInAj7UM+rrqLE1/MI2u18sbEbXbPqBfbN4B3tQlqkISxH3YeiHSbwcj3Qt1pp1nGBy
6H8jqmqtUW0Wr5qq8JtZ/+hOzMtKQUzbt4PNxY1zQvn+8xbifRLrHKOHcwjsqdxbvYESS62RkIwt
a4l4mnrPNdt5mXtEb+F0QvLnd7J56/pP1sjGjV68CzauaGqwExcirzIrogXoLnBqMIiR+XmdooTe
bpWWY3Ff//92L86ir3nWsGDF7sxuQsbOPMXeEmmN2Wjbry3UWFYFp8lYtKWmGNN+BrPa3oUCiCgl
i5RpT80wTdLyb0ju+kAcRzTdX7wTo3+aCwDK2LzAKUYQf3Hjemg2JcGqVN59BlVkJ3ixaq4LHceP
Fhhk6HwiekvRTK9l5Guh6/r4aVDQCe+/ag2xAekJT2qH8mwMZwo5qy7j2kjZjcxZEYJxqHU9GGPg
ZvjLdyzp8e7TupDf/JfAwn2uQF5RKlySrSLNBu056nX0uD4BddOrHyjwAO8NMNcy31wmrE6itYrv
iB7iJdXUMvVTSrCB+5T1DS/23AHU0RZOJNLRgZqIFgM7faqGhuNszBTNEPh66T//6adlZO46mGmB
qj1aZFoRzddgJpul46LzFUq3OgDI5FyZdly9ulSMZ6B3I4MdiyhpuFe4QSoMOZ+X6zz4LFFspEDU
roQlrc+lmlY1QpASypXAmz5Wm6hPfymwc5bkh+SwB9n2oxC1nzPb9THzMzrdzTR6dpvmx4ZpPDEV
NWeaZM6btdShVohQd+kHtCWhLlprjUtNWR3SCRPhzXqbqXynCoE12yaI5bhhLkp+GL32zZPxHXMH
Sij0NmMZ3JPpAE5Ytdp+M35keoRzIzUASpxTGHAmcwghlzF8Fe9ZcmKkePaSyW6ImS6Kb6cVmHCA
RX5qZiDZ4pqty/QOPigM4Ji7ASh2qbCI2WpR4/b0puuXF3Vb6Vt+0BaUFwsa6B+ndWPmKh20fNBC
YQQSKtXXXRi2dL1JpCGZXO7hg6+p3pDeIRAUA1WNq7YlmKD5fPNQqBvPTrO1oI9HhBMHY8zTmVUj
4YR12eNBqrCBneza1Xze7WMyNrWhRrhSqh/7PNeCMyx4mb8Hoed1RMrWJew+IBiOS2e3S6ObOLVR
nLJCtS2DoUJcu9WmndiaCcGdU+0l3Xr/yKkkZQWJHjHev7PwxL2MBNqL+7zaU+/ra+SzS8OnBaS8
JegI290MkvfBIVWcswsC5FSKsK634PPaUhCk3xzDinEruniaJolj59P1TftOeUKyBHScoMaz7wQk
np3MeqiYGJziafqaXYatv4XYWVJGpWBiFU3rylanRhuhRfQYtSNJ80EiFt/hm+w+kLaB5QNls7iS
4i5Ch/+CtrZubkPjg3rCDUkSfFz5mWo4MXBEkapQNWfTnYIsMXTrMu10fjmGbluo4PQIZWWyZbXS
tBphXLlAvVi05xa9igdb7ytdsx24WCTUXZ3ub0FmUgm8FUEF71/VL0AZyTZ0VnPFC1mXZu3JNGsl
87xVMr7kD7EwA7QqgnjMTm9mJrclk1BO1uKJnZ+m6glGWq8gNRj3qyISp2g2ixEI1P0HmPlVeM2P
ACwJVGyh197dUmRi7r2efGTU9hBfR+Zmxp66LQ/aAB0UiumwTvik1Pwov9CJARq4sYAPjXBC23Em
J3kRifloqakgH02thnB7f2FgCHtznLZyvula3tjvsKbyxbweYISWAxy1g8EpwHv+2AOCnyQx2y8R
t/B7Es9+X8RxYhD34Qnenoo1wvk6tXE/rTr+q3PPrgZSmKdS94AUxivUhz59ySYpijn6WfP7LtQf
m4l8L5z6aXakZhO8UWCrWIX33Qs26dRiDh0IdDhvvnyT3Tg3EbQoV58xQ1q1/OXmjJLq2TTTLr2e
+54MnF/hcDjuL/dQo1PNFF8/NmUhOSRK4zPRDUI6afh7Qgm+FNG2LQowstei7xweZmmrgqJhldOD
WCQxETJeeAv1XPMTnjZO8EbkY2bgBD69+ZIZ2asjq83rgwd0X4QqApCPbIo9Ai5Hm5kaal/kyN5O
jNCDqGVYJVqr+4Gc8SM6F7Z0I9rjcfBRlCaOs1CJgIgWnTyfsi0tjs8XFgYoU53dELjSfgxCeLIx
xsoCG0NWvg53UdQFe+oVzi4dxAZY8saDyZxO5VO9+AJos/DuDjmJaaPIZCxon9sU8RhLIpaNutYq
6q78Gj9Icrpj8ZjvE2wis0ZLky+3KSUTrZgKcPj+Mc6S+aXJXDneSMGnRC/wLpKATTZfJ848lY5i
nHbw5YlXKjzb9I7/JcRjDjpOLHgZmzfX5LdZbR3RR0MB3Iy2c287BnweYkpUBmqV1Azkgsb7elmu
NwjRsVv/7ioaSturMTguUTIDEzXHMn/CNsJdNf32n7zuAn2mhynkCZEVMVZan/5Ptm0no/d22Yk7
0q0xOD/lK6PWQEo8S7V+1oUKfyaknO18dhrYpULWZxi3XePQLVU0gH8inHlsY4Eiky56X6GLk5eQ
uLek7Rr+5uYpf4mohujKFyS6Y6gjm6slGwk8I4FJldnplIIWDhXG5BM9gqh20Lbz8KgGYRRngsCV
+ABbU1t/YTx/j7A/JJG/pebLNqJh31lL8IjYXB7T1O3B+oJVN40MATLxQte5feLy/uBNISH56KcD
RUZmvyJbckCGqmo9iodmbVFgjj3f7NicC/GBsb/5BLlXDu2Tb5S3u0Ie6CF+lyuQyke7XqTBSvt+
KC6OYTh/oBPocrRjdkibBXzpeSJo8arCjENk9Wy/tZuURfD/jcWFd7p8caqPkshc+9OZd5N6lFFh
9tYY7EKPuz7AYe9jk7rAY1x8mJKayER+g4VzLm0o15L7ZUvCU/tDIJhfooQ0fKVXyKCoX5yjO/ki
dSh4NbRDh/hMbBPXRFULqUyV4dYJJcacQaVsvBd6vJo9uB0obgg8fqGH9S1su4tFFFl/NHB5cBsB
MIf2edOrGRNkKcu4AzrUsmGOME7WxlhXUh3f04nfg4mlTbcCABUTff9hF+rWLJwP9wEPD/xs5TRr
8fES95Vjw3oRrJVtCGcxTjE6Cr9AVYC9dljMHdb476iGmq6NtnlmaFbrVtlJ1LkRx3FI+VpXC7tU
04i5oiD42xec/O/ipBNY8Z9IxGUE+v9Z1Fr3db7/iCvq2MMIF/fS57wHo3K7uTHeEzaCDcGzQ1GL
C/LwK1ni9Zklr9IC8vVJXNiRVhbVQNYOZPVWKbITGJtzRzEh+WA0sWU48a84hp6/SAxfMK6Xzxyr
FDPzFSf2nrrg7sXRzUFwtP6ARSwaWTCyZKgHKietOsT0Tr6WcpbAwlwvK0omdtFwbbHY91XFtsAl
sR/P+EXNKmPZZktW43I/d0TuToccFLd3D1PZPAxEiT7YAtqgK5FubfHN2RBk16SqQoPlTfkE9TVj
veyJjdOfsDb9MeS4y1idj/9KuxxYTBCwCdCK2DOul8JsTzrdYfW4tOxxVxY04ElpXI8AI92hkm7a
zxQDDysITZfdeMExZ1FDhY+i7/ZGSJOH0HwWlc9gCU/yvvWV1n+YTbyRm9y3wa76LRz+Y05IZHNY
scqPFG9HQdW0o5HPFIiGRSNhalgnN4gzhfkYH1T6qDypV8zhv8aig8JeZSQC6KzqxiqHdZ2gFpoI
csN9nTPk2I0xktlm+5Svb62xNYDUN6I/HTIB1NYyX4c+okBzmdrMSIIYw3x2zEeExjoK7RIJDvQI
qDN/cgQwKcjURUcgbG2ORm5Y4PnB2zoPynHZ+oal7dBY6uTb7OQzVpaSp9gA+sEmcxsTivd7B/vl
y1z3jw7zQPkGr5c7bJ890ws+8UhwXF9XkO26G+eT4cOW0t37vYDTJKKtcZsJBbXn2G4/MibqQEsL
ovjgYpT+NV5BWE6U4+P6DNNy0hOOgA4otcL2MW+DdniX3HHBvpHDb8DVn0dqyH+DEEX/jAOfzuVj
MSEVNX/tQ+UKpYhtuTFh1vx3PMNfd8oo7yaKyTZeRK67YjVallVkCbA4jEcPPU1izCIxzsSsqHp3
GNFx0ILZl6MmFP7NkIUi1llpzfVuuXdF/OXh7KAWL0fZ00q8+WAFTcHwYjXSptXSQnR3QBpdaLxN
vBNSf1t496dxLDAVtPS15dlvTwYcY3ntgPXsfmwXzesrN4UKQz11NxuLN0ly4yFbU0EThUrOuthX
34fs6dxyaYkJTYYcz2zJNzsm+pnqXtlbVF4L+nYEEAeQ1zqZQc7sf8xiEQm5GK3h2iLKt1aIcw0H
/EpMWX7hc6Z2E0jkk037EPcxnhtpDgKC34J08qqrMm7YlARP4+Ncx+mCrZ3AWnzao+0Pkm3yPWA0
1o5tuusnu77xehMAV8UcqDeyFtcFqjtXJATsewxLuj49/I04S/AxoTtvdNlWRfAKWiDofdYhQWWN
B5WsqxAdAxHFj9KGnGv60NlaDdo1mhqTzwtkRgL0xfhfScnrPIE4K6FlUA/NEtlzkx9KHZEH2irW
khep3rUUpZtuSTjz5hOtHedDl00gxUmt2B3oS58nzc1BOSiaAvquiRo9T2/3S376zMK/9AhWs1QZ
qNnIq8fXzr/VYp5B6iHm8xXBPchZNaIQ5P9Mfql49OvhxUDgUdPVTnAguyZQoCRv2gfuwY4Vyhqg
sK2tRLHhABrmDH+lFJf4h6MS/QSGwHMr5n/xz8cwSt6FXTocF2lD5hNGx+/pSG0FTXPVRHbdbNwS
U8aAWlm0sOLb9giGoYkHLVMs3pagJhV8A7Im35zs+1H6lLODqgNLIE635mEPyu9sN3w0ZvnBeWGl
3HVNW7DUKXgzDEwNvvd4gc+NqernjO+Ah7nrPCSrleTecf5SegILuS8E9kYc206qxqihvGtfqJFJ
etibJwEU40lwWfKFuNnmhSwm1qkKfJ4ICrR2ogINR9C6hstvvDgGBnd5aM1VuNmly48VgzC3srb+
poVUNqT3Bqcb+n11ElQPWAAs1KjddsuKQW3+ZU/uP1SxWS/lQRcoM+/g/1JUCJHFguObEDWaGrId
6KY5RTFMnALOZCE/+YHj5YWdKLvZmhRssdnmRaKQWn9libtMYJFOF8qXibUnUYMgBDozTnV9NF2m
4VfE4doO06JWHO4CKRlrWeMuvUCvS6qlWq73TC2VBDPXtG42n3pWu9+cga4wmTmuzBmOlBW7Z5tl
oyVyDYZU3p/ybVHESsK9hRQjE2GoswgvUTaSiZls+0u2D01If6L4IDPdpJUw4dsD7cJmqd1xuazf
NbUv3xiSj6IS2MwdIpeNuwoHB6O+jmRSAgEE2sqVpJk7rLwhTPCR914kP68XFgCYp4U7/mgQxkt0
y0pifN4f+D/s1pHZecD3Gh8Gfkm0XcNWgzq1zwclAKWwVIcSI2zSbbNfWtYmkPoMtaX53Yaz2Kgz
l4m8nSUzLPX8sDELvFeq9ZiOm+EQFun2HX2UPo4chWPmYTjz+xPvcrJZoRdXpd5EIRjcoAbf9Ghj
Qs9ny5XpV1piKGnlYFnt/hmWj2Ry8oouUNyivXb5FxuSoMFxhY6vwEJ6BedmVuQW4pWmHyrH1Zzo
TBHymoTTK+elmpbv0USqMi+d/71CIg0wbA1rTagTgzH97EZv2qn1K42ij9LueSRlmQ4PBpCqeiY9
aEL2sPh5IFK0MJf9zYCnkpOUZcxG8St4LCYI5F6gkT4aRr+4tzbBQfr01ia+nFt5/WIObmJstzqS
wDog7UYFjbdPTUJhmVSilUMcrHEZpdfHh61FVy/wp5ERHlRcdQ+4NQdtCAsbaPpu8DVZOcuVj6NI
XOj0JjSAwXfCpoFB+htEgnnFEYBWzalfbzGlU1l1c3E/9C2ny2BrRDAI39ANiF/UyHgHMjfWIPAt
YNAdM0Ce2QxbL+2rrfrLfgD0BFaGpJQiqK9jEiSTKvr92G+CF70wsdmm9wzOZlPVl4msrzitrmoG
jm2n1GmZVyAETrVBneowvKZSd0crhsJW3CaqQUNQuC29sX6DH4L9MQLqbIHGUmHgPAHpNH02KCCj
fUo+MyNE6XjVZZqfrooBSEmBdJr2QolCI6odb3UYWtJZpBHkaWZlHOKlfyQXkv6PMDyf7kdYgT0z
zAKzZ9CqzOVMJ1hDa8Me6GLnOQ7BPBcY94/enD1f1lkagiX67QvLCoXUOZalqKK78BI4kfrZ6MYg
H0V2/yn/iJQSqRHwJYNKX4QVgm6G5ur7+TdguoO9C2PwOAjAby5wVXcKKGFXab7mnndIujSCzlbz
EvOmLkTSQSo3NwQT/hIgaxKV5mFX6JAQPG4Fz/GQznRwrO5RnZ6sNnefhLsXbsIsvluSpDLGiG8L
ENV12tIQMDDztQ+sYqDqW3ERl64pRN68C6nrzW804NjZrsKWyKHvtSXIjHbhK2YN7o8UsTgj6kRv
HiodGHMd1eom4VtOtquINmeeiVTBP1/X/kcYdzz+IWMiEAMk/QSmZXWWHwjY9pc7CerW5KvwCluc
n5j9s+LAULvCtYfZDaEX/v6xAx9rllxRivCX9KeM3aFiDnPoT6145aIGf+a3dxjRa8Oq4mj6QM/K
k5QHylBSukT4C/H6a8Hfm/CcUvs0LDc976J+rWbSHNY72ckpe86zBASP1NnxmK7brWONsJ+40aeC
La8PCqKxakl5ZvRGOvq13CeoebeeT7K5vBm6cUYUaZkBKinm3XU4X9icCc6tZ8M5B7FNzlUaAqgK
udoV9nHqNt3kk4/+5KhDeCApbaxSv6ZFfunq0holCCYq/nSKvY/NcaJ+39xA/Jg5htVTHn/QEZGL
ofcAJYuosPeWzkzQVsG2LdolSkta8VxFnB09VYWMu0bQ+WZ/2K62BEGLIEO0MRc4aXklekmZSvWg
FSoeKmwVOWRvW/zngkM7YIvaVEFf2DUcNGPOsd7gw6mGKlpE41YLOQWmM1e0IQTJCUBZEmk8dVG5
7ZpHe/ulBiiZ/nKKebxdy6Yjp0EOgKFrJvfMw8rcoco9hVcMpZhznv7bfrcXa/2aGMdV3CJ6JbgS
35aZ3YEEL0TC0cQl+FhwXrY/cPtuoV8vRqyf/ChrPGHa3IOsdaV73Gd0kPTQMnnLsufHMnS/puQh
BfwRL8KhshSR2l5LPkeBtAK3Q4fuqT/mh8BVM5aZnCh5Rw1M0i8G8TizNYamNfFrX5O+aOWGa5k0
515d7W668oVMWq3hY4UZrz4WRDvNMmn9Gsl4Tvxo6w87DLbNUxmkVbsE9DeK6K1H2O/5EHTxSjfJ
oREmt8vxKN6mX4HkmhywHZS8eOif6UAJxjAuWcbagrU3mcjOv/3sfcOTc6PY4OhJKp/yfBwWl9Go
7zvpJOonmrsPO4L6Fnm0nK/VoJp1dTtPy7oq/lxhxMf2Y75d3pZzX8WdacOVSvBdddWYA5mUkJxf
/K75YnvNHLzYAC/ncq8PMLJPURcCdg6XbY0Vc+Iyb0o3Y/T9kwMfzYBW7EVghMGSJyqP67hUSEgP
KIgxZA94dhht07ABHIYZ64bnj4gPYVKUY2A0S0Ui4GOnotPAU2wVjA/D8K8dQjoZyvBZbZc0bc6G
ju3YMEPNoSpOysZSheC8hMbn14CD6QvSXYl3+6FpN3f0l5twbn+JxrKwX2KpieXuTj/qTXwTVAd0
n2KchTBZPlg2Kjey6vWbTnsJOeqs4KRrBHAiiV2KTA102RexzA1cTtsDD1WftISnsjefmvYzfxYd
dApRmGno9X7OaMLS3frza6mIN78FhqShWqZUAmnWHAdgt6wgwTOAb+vz3C2bvfU3MUhNfR6nuk8k
HJ9cHFOlBgX+dkiEQOlvgWW/lmc2AOCrS6+lLmAavjgP6r0X6sMK0Hfor0z/Yvm7Fu3d3Ly34j6B
H5kHztkEsi1Y4Bdbiy/eod7N1q+IT6Vdc85cjWIqr3OIQ2JRBBTDdulRBeM95VjpPhF7WH6Q4aPT
i7H2df/cmuievdC544Yu/uzVzzB2qMsfzVVtpxfOSdnCiVzKNwPS/85rrX08ryG5C7J0pYlsVATR
O3YvaIVlWOvUEBYw68fVQ2LjlNags7MXHq8tozQIlDkoWvAR80tRkdLvlijffIc1SsNCZBpmK9O4
vDdfm9XwcINyx+N6YMu9cHsQ6RU3YI6R19uZkgTBSklSd1DIlQ15Tc55eF9EuF7WBNyesjyk+43s
ekSNcUiGWCtElE9TSeglTBfe142ZVhJe4jcb3kRLFeS2WmJ5KlVzM7z25eMP7qwsWn8AiCCMUDma
oGA8/HJqWdjmQwQSpaY9ktmGNtTwa5VtmyPPBYhziU60Sqsp3/l5BuQNKjyiJTqqXeLDSLN534U7
wBWX33jjPgOiSrTCDTk9TUChOSTPwrE59laS4BSTiUP/TJQMa8XwaaUq5gTxyercFed4xS0mPvRX
VDrtNhcPizagx5JD7+qzyLr+K6nFTNeZcF5jwLfNRYhcXYoEV8vNHv+T8isjjoSpzOTOWO93hNLF
/To7Zyn7epJJZEoJJIo43/1WGssJt5S+zN42eYWafWcu+OwX5Cy4LWVBcZwR9hUVp+02BFkAPLrB
3jWp/h1pMX1szMMp0xB0qurBrZgXy6NNGFAzojKiihnwnzTD90FOB75BKz+CtS+lCm1o82tAZuZW
fjv+JaLtOvdp/wSelr0zwUPLGnYb/jKjghwk4SljBq9gqPrNfjOSR/uLCSIzoZstDA4nfrFzTbQA
RB0abD/OsO8sECf1quudKulwB2asVHsOcvaLAICEeH7WidkXNRRcJxStnITLi9C1ay0Q13BbZda/
Xs6GSZG2ydiWFARHmT/zmTwehhozvWhN139j3HCT4lY9Iwbg4uONikS+hZjXup7YObN4tGbqyn/a
pWOR8BQDUVRWnoSbK7TuPnnolQbeC92cm0CSkCCwu1/JMKa62/wyeHz89pk9U1RxVLRd8QWbuQc0
q4GS3YOyYk+sW3BamDcSRDtYbZCfM09EfZALiVRJJE3KoxjnZ5QvFiBSGQ1jgo4emAAp9oEIvvz7
kijazfp5VnQZQTtqqWFq/+Td3t1twZJZI11ytW8aZngZENu+pEuA2mhj9zhtN/O+vuMIzkJsvpYE
d0rJw/lYXSGNgyoI6P9L/r3PD6L3Wv80iN0i2HXhL77oA/qktKWm1mjTBcPUKKeV8kj4QObrd66R
I4u7LMfayA29LfpqlXhlCByOKFTIOpr9qkiPif1OIOgy3VVJUVm91xINbo22kG7fvBgN7pQYa7kI
NlLrWQWCYU77w3c+htJusQzJqeAO9s5rFRfYKkxPezqfxnTyG+caWhMJdaP1ZfqGBZvXoVH1NWn5
hXWeRmbgPD90VlCJ//NzlJSXsfV3iM63Qius3hOr9uJZzPffruNxWsaKlJuBveLGuPesTIQRigCy
wYuTlkdhr/72arKckFykF9Q0yJBjSy9uJfma7bZ+I9O/jCKCbgqTOrxvp6PiXRzNI99VNJeQ8hR6
vzNhnqp5AyKE3j6nx3JdEmkpezqxpUCd4aGTbJRh6rCFBon0O1Zi8GQZ6Nkjmp42MSWTGvvp36Gt
x6PaF/AdMwpHdugKt0IcpXvB0mU8borlrxVvn9lq/gtSDPB+D//Omart2UxrtEFM5+IWioHrb2Se
ewWAlmE51IlhOABcPXph2DYWbII9LETasvhZeQy1tTv7gzH0uuyYx5x4cI86SVjw5/Cni+4vc64W
WCphmPhdoVnfGMkpwy/CS3Lp4lsCNXTiCXSDzToUu3ba++95LbLEhutj/VzDDrgAgmj5XDebD8Z8
9U7uppo2vzefKRgeuJNUgo2ArQO4DJV0Ic7fec++NvsJMjFAY5n11B3y+WE3VKTQHFfbBE7errmc
vNWC1+FuDgSqaeLOlAP4h+EmL0JYS8hDb25wrtmNb5bTVG9bqdQLb6DQJW7cEZsdNIJ42lD/ap1g
0oplfJqOC7lrSbevsYgqi2027HoGjg0ETZ0DLjLrx78C0OxXDQzqP6dIEn1qeWt1xoXMl1t4jhPL
Z+5i4GDvIcl4Hg6p6fLLmWFItGBo9ecapL0oP5OaBNboMdMDrEpYKNgdAUMaeuICIduaGkLtVMmn
Yj6h+D8zovUBzxgf/S0e2pxmeXlZs741jpYAGNDw5LQWrUgXGdZ1rkHzghtbXJ0mS2e+yDWTy9/2
v+Fd13YJRDWx9xQJ3IpTmkKi22Vla29H1g/ksrbRe51+NUPNWrv10gnJ8kxttVDAQf8yPh+MWY0D
D9zSoREW1VeV2WSD+2IrAQMh+DKWPrjYiAvhcVdp1GpHy3XF2qmOBZcC4nhwMr/xKkiOVYwy/38T
9k0ZkvTqNfwb+jh86CC3liQ+LbUwfLqGKq+Rtlzc/VzOBljI6FRYlBgtTLo/gYvfgc13EMu3UYgW
1iNsRjxXzKG7X2VAepGJn/3qgflWPNwt4typyKujb4wRIf6pgjcGwNUmF/s5L7s1h0NYJVhJVtmA
IhYls1GD6ZCKWZCqTAJTMab1DUAB9bx8dOYsahBNDYK4WNPJgb2TXr7a9EO3qoHiqVn0E3knknvq
L5qAvEwMZndJV4weLhuOrguQgeoNp5fVTtG5LWNuh9uioUPVbbiMtJg1RVRCQdx7C3zOKGbeij1x
svuUmumG92nkr9sAH8bG4/WcRFqlLuwBMaohjeQhKveg5uMARlkqwcO7SONEOCi6H/1M7J+qcqJ8
DLxi7M4VtuRY8NmnfXBrN2urkdEzR2dy5Czi4S1sG95f1q1yaofPF0UESclaRh8F8VgYXBF6wng7
0gJSOeU/0rF3lKCc1bmiGHRR+MT38MmdUIw/ED0Ke8WGh2DTHXM7UDlExyH1nnWoQN2IFwOlxx+D
/Fi2hiZfV+FAyWv+zdHnzleXe8eZsVeE2SWvqaq27vu8ZEeXHJw40qWcFT8DZ5K76ZfEiXGECJ8F
4QYjFy6Ht3pQFI38v//NHe63j+7FabdzpBLu8jlQVNcTnStvcMnCJz0EjKFGuD9W5FmAOehzHDQI
ImZW/dkoJUA1VtcAijLh5XHA//bWzzKC6a70nZ1qfTIWiEHvWla4czNqWcOdK6SGKConBXLNMULb
xMYTr93TcwgGKp9/Y9iSRVEQgzMlgEfRxql63nfSQ3oM8OIPfSKoe6TFRuF7dwS26isySZiLDyms
eBiUngtDmvjcswj9DP03fHJ1FxanKkiWiW7AUEu1D0655bamYmWn4aiOFdxDluph2PORN6v9gdR8
XJBcjAETnCM+KHfNx6aHTS3R/ymtXE1I7DCmP0jA/XpbfZkFlhH6uYUD7OCoomea8x17InEzqDcU
BFSTXK8AipiGM5NZW+M006MFTRMts3izQlYJ/bbix90D/LCK2VoXQO5DGpLNLVwpa6FIaTBpR9GC
dDNP3wSdR6lEK/W4y5nVBIKR2nc63ncbQCxATliZnYAvNU5dCAtbG+iypqQ3zAhBG2kxanQblwqk
mvTV90rQJmIdrK8hh19aOJey8Kc1T/IAnr8I1LjyoeIS0EgC8d5V+7Ucq4QnozQESr6GZc9tCT52
be6i2jlhLB+kQOzYJxqBLPWuWfE/A5up8FRJfPAbRx/njZemzU3L3umiuepm/A9FENPv85WCM9+k
P4uaa9AxlfB3r18SZFB2nCVPcBH+0qQPCisgJPStNnk8cYFM8F7l4hUx+LrsjYuSmbPTnfDyb1tA
ChiP+cGcmNX5WBVSNTEoFBreeb2bF2a+wC7n7pFgCL5DZc3/DO67Q9isYotCzf9w3qia2cpIIXz4
JwDDBxMcMCXKa0D4EtsPunBgXWPThDWl3zdYaHLL5GLIub12LvcIBEQWrJPL79IhmGvFkWg0WUE2
hZiYVJGa1QRbq2RHxIIEebdGrBXZl/U7NLRs/VmebllqpndYRY+quJCH58lR7ZEtekXGY6XGxGDJ
9EP7ZJHhqTuvYC10m6IQUvgt+4/Juw3QfJFn+J3rI9HUObIf3BkwrAF6s+vtErCkmzQOUbLCkdg6
4QGU8RzYmLEf4Nwn6YJgc4n7czh5U+8YZNgLg+s2rgC7i/sg+HMrOu4LSYuKWm6+lbsYZ87FRpxi
ppFlLF7MOOnwUzGU/rsvAYkTtEG+HlrYmI8grM18vR/aK9SSV6NPvyQFECJ7TS4MyGydmIUM7weV
cPeFsssaZovYe/r9XOyPHyZVxloN2tReIOBAxBikEZA/Py0F4cGX9BE5XELW598guJzOvTiPmVs0
ZFzy4grao+TFQzCYkFXEQ8TFEFAuzQOxYyZc6ObZe/6qcXxj5D/n/WrsLsMFmsQSzP4aGOSmT05a
buLcDY4ubVVpySVtaa3ot8UuraNkAPlw8l/toDsTrARHqenkPneJR3WvIQfdhv0X0pquoOxm97wW
NI0hW2eirZQYHg2rZTPlPN363SNJ1DOdnEf6esJLIpvIx01fhRDYoKo+XA4aQTfVY6REPq+dyuTC
3jH1TiDns+jEVQzBiBCiSnrPycOWaLfhX2As1xW/9fzncgIFiVE6RaowgykN+w11AsIhsd4tJBI4
QgtZPLBKarG4D13nrWJhYvRLZYWpZZwDYpQHBvtkIAOr72NNU/jggSvrsMRpZby6i6qEvso2HDAD
DzH8eq2zP0JIvf/a+UeIgR9DI3cIHHhysIIId/AtdJAbwt1l5FoJvLbhn3fvVW60z+ydTtrR6FJ+
BxDuWuPnTLaOgcfMFRdDLL51wJ5Zqv+5UmzpoF17+c1RFc6jpOouKVkXAI7gm5kgy9n69OGkSyal
y+ZYpKVgHB8Zr+4LSyCC8ewjjYChBpk9+P5qe9G74ZvbusZ3vSxoCLIo059K+96ngfhDnwFf0/97
H8TBZYwi9vX/3mWKbRZMg32PFPCbt9ndX4HNMvwDXhonnwgQLkBo61pT/cosb1MGF3xik0xpIVj8
lK+OMVP6+iwKQq4YZ4i9SKHwbyJwDXVzvUCo1ZLW5oek6nvlnKjg9HL+mBB1vg3pvQq7DYwaEKre
iLCx7iPCVEOZebKoYzROxaYgIW/1qvv0G3Mk/o+dNqgGFWJLWelCjCLqRSAzmWA+u4akPVbQt5F3
uCl1/ar05EGXIBQG6TKJEmTAJIdIlxwv0sBWLEjBLq/sfzntJGUTs7n4cIIc/Wy3DumhNggDaeX9
5I5MPcqZA6LWf1RO5YYGYTV81OJCqc3J8L8oG8gXd+flqME4YkkFuNF02w3NhT74flJBLV7G23kJ
bp4on7WWF3om9yELHO9yOt/ZbTn2v5/Zqb1ZrzAZlEjpwzUZmNdc0gLWM1h233DjDMMvZZYj8FyM
cDa+kH7MFoU1yKRmB337FZTSPQV2TrWnuPDhd0D2k0OrOjqaAPlw8+MSfr4Dv5ie/0iNYCSrZvTG
uuD5lHTjqWqJlgIeeFNNjiftncIQRfDjrETI66qol4Il7U/wS6PR0AvWKuSshyggI882U1jKjxDh
4lW3WyX38waJ9ezy6kibQZsl6OONJBmWsR1t3xLMXwViKRYYu8M5/zHLcyPy1JkUR2iI3d/hJLER
WKviO9jjhlwc0+J0vPqyQAqtfX1nRxVE9M/6V8ypj7/Cw7TtdgIEJnvqVtwHRHhIz8spJ3DPTjLZ
FoL+BTGtNF/mr1FyzcG0Oij95b/zKzzIKy9Vm2sOtFZOdszZl3EqJgPMhlET/7GA+G6hUunjxSTZ
v9Xo87tyqD6DBxQqJkCSqM6ICz7rNZA//9wUOTM84sWyLOtvJDCcC6ZUs1x1foj9r261yOgV7ALR
zg6lf328SUPgw96gVwluehERuzyYGWTIA77ej8zGdL8qjGmczpmw9aCUZ4FcIoWWIzC3ML+8pVmO
+JjFdxCHAFsoWpwP12JKPvqFYLdgyPw1K8woSa76b85qDjNseTaVXuyj+zLPmX1M2ONq3hKeo0ki
GECuzAb5jtKVXcaLUYspYu99ZEu4HvskN465MHDvj8igImCBqg27V9CGS4ANprrzq824XcyIdJXW
UCoc4+ZE9DsOx1DyBwALq1Nr/nHle9DY7GU/ZtPm9PhyDpHmcMO5eTi6iq2PipbYedYEXN45dDmn
XH4yDzK1AgNsWl9f79tUY/fUqF0eCgFGnc0GNAOza366pdSbIF5q+K0wNyGdJkM1Id3Z4dnIi+by
j/LdKS5LXO/OADippNfEiadWbcunF71hQNesU+5MRzJzopuxUgiCOwO0lKvAAQkpvqDBoRswX2/e
3Ri9xNX+SoQUThq50PEQLzTmfzjr03AtBizpkYM6hHoKS2FEbbqUj/OCz+gKrdRLmJ7I5MhVyrp8
BpqN14mVGwT5aLg3/79MNL7vOIi91X6nFLv787+BoTVH8OXnA9Gb/94zUhzpNXjR1fV2zIGuRaxD
ka47CI0vCQDUt1Zs27nudeXrnZ65SrSHY4jwPkTYu1n3pUyIuA8CM+EGfIqA01VJ1sSYt+xxm2+z
sanyzaY5V1RfIG+/GUj6ZDaXQ/wb3ZtVXJwJ0PzrdHZp7lL2Wda79iq9U6mTCobuP9nVkiWd+ZZa
Vs4Qz2tGncicX2MiimJD6u0hP74/yb8YRcITz20BzddCSbFiSJEO7P4tjf9r+QDWAb1isXx2dkSw
q3RP5pGs9ZOi65qcNHQLUOXtYJMZgEDpmJCepGtTihyw56DIM/AQ3CSJR95Dur9JGT0i4bZUnjCb
ER8gX6d6A2uSSmwWgRqmGpRFYcqKpq+ZZX8uxr5oz9/cuqSuDyMjlW44iU9R4fIsE5k+Tn/If3CU
K+ZMCGftLQVifKLaykC4hxuyt8oKZsqnAPWwnGJKg/w/+kaq3ey+DtmbF1K2fl7PiXN2UCxpUWaC
Kgsvr+UXTOOq7uE+shz7x2JmciherWSXzFjsydOAfe7csRJHtGjjMvzjFi0k/6HLKNXbyQtVJre0
wI2RzOXO18hJX46DVFDF+kexShm+Y7tx91348SybIdLFRUzDAQ4/bizBKp7N8dM3jSqjtVA76FxO
ycBc+/W24xuhrs1QKYJB1gdkj46YU5ZrDm78he9rw1R4IJQz8xbgqPBYaDycMze9oVMML4jKY+Gg
6Qz8/CtyFPFMS4kvZa4HEYbVg4E9y+htLhkSaegqNESaA2kSfbFhLZSvvPSZYjXD9gSwpAj126+o
W80AIlaMtz1m7I9fm5jqyQL0AO+2KAHs++A8lUjgBoRpOw0+yWSLE+BcONPX2ewCUkopbIF9Ls2w
tbWqGcHuduAAmtfDLSRDbxc/GrqyHsIScoWG5gBxxyOCnY5IW/0EB4mEsip333SmXEaUlgi6e0Ny
qouyNZTpHCx6Y9z4ORBKoRT6cTQWPrh6SYFb3u+ufiUzeo4k0cmsBzmZ+BVTfvVGjnluK9jTFomu
y6jt7NzsBoJIEo8xLBg1IGhFz9Nz6Avk61OkvXGJOSk1vP6W/6ufbvuIXDdl6hLILo+MR81t0Axk
DK2W4ZwSYAHCzRIky80NmNkcfN11I59mj5fQQ7GAmF/Yvr6d57IQyOhsqKSK3Hc4AZn+lONK35+k
yff1k/Afg6fSQ6xnQyz95uvAhiQ4Rrv+yefqkMDvsiI5Sf3Sh0MacBSSXci/LkIi6j+Hc7kpPFlP
6QLrsGUyd8rRjzO5UWkkLZUvg4/n+ALykWBA3zf38w8GZ0KbBXbg7Qe2Coc5s6euG6sN8r+KmOz1
g1MNYwzwDEorKtRa8pnLgQc9HldB9Z/VPTByuSHKciBm7kqfo5gK54Kl+0av6sm53/k+dCZDAF7e
l2VcZj50SvMotyTnb4pJhcKNvC5fhBCOkU94dOrNFrYrQu8ZxeRzM1n6vE6+BKIwkKqXRQRxJ+OT
ImChcbS6qEQO4LIdvxzRieb84kB6RltVzBsQ+ysjeKT7LladJzdi7GdkbVe6KTi4g7bYpPOol5lg
tZkQFhm7mlmPI0l8jbejRxphCXC5UhQXlQthTcv62j/oAR76M7/1Z0SZxgPqCMP+LgIbDqGB6G2n
ALD7wyIVaR3pPw28Klf3jl1+24+d0Ejxy9EIOjJMLU3mnEY6KU9hAZoSFbh0KIxeWi3mYncTIL5E
2coo3ue18CcNPQSw1yzp4Eq3Y9dg+jBI7V5NBvS1bHqejhR77kL3ecTtthwNCFgajY6iXZPu8rh7
zk8wtpsjBlCGziFNl1OE89RpSrUqi8KMq2LiOBNytADetWD+C+Aol1mZFPh77TINGBbKZktXjUu8
bw9YLhXWuMb+Es+KK/rftlgTCfEDnAMxxImF8ZEC3pcxbxcZ7gEF+d7LkK53NBRpUm5t4pflJR+x
RF8fKSA3txQcQkPqBrXtXR4z/c2zF1iqO5smiAvW8FPKWgLKqz7b9gPeMZUYHhOw4L/5meIxMBn+
+t6e2peIdNlKVAHU+Wl/WUHiHUh5f1DK+4TWwU2JZdaq7jV+7Snt2bJj0CI42PAmcVknXtrQBwRd
pCtrkLgr/P6rFZ00Q64f+Mrv8tf938mdhcGRezj0xgegwhYf9n9AMO9jHa+lP3Mf/GzI4qCwJxwg
g/renZ/vLDmfPL2AI3TKbk3DKIBvNjxRiA9RZb3uNgsU8tnwGETDa0oeRnWvnYiU0jdZ6ILbN+8X
tJYImwsagncUIxf+p0IFW1qiVnHygqsTbvEUYWt73IA57Tt/ptfk7S5u1ZCwz7eRKVbVB+R2BEBN
jwCrVQi/wJD1ybLiTx73Dny3JtEqc8DTRoTwKLIBsgTwi1zuHGfhvAklcDyHnrPu2WzdpJOWyqD8
Kqzi9jsmQs7oaK+YYYzEMspNC5M9ZsbNzoh1IT9MiZRe94pRptt/pHYw/77xXnfh5GLoMeAIkcyt
Jus7iVqgrrCri7SiFzyVt9sgMnc71Wib/C9cLFNR33Puc844zGz8W3L5cmpTKg26G3H4GxPsJw6p
c0z4tcbEiaRkj5Rnc7y4I3YxyEqlFv5y+wvmQ155jh8hINJOOdwb6Wi4Wz8nd8guWxnd7Ntjhf2d
qWFlyw8LHOZxZPFC+/38ZzmFlEUCtRdKvkreee3H2iT7I18mFukzATCIRLhj49rsHKwNGuFPYoSv
qvhq6BfsGRV5U+kuCleQKvuE1/c9G8naMzVEShkUPQTFd3iO41Wst2nzia/3IFj/f78WT1/3n4g/
Ddb73R2S9OFMrOiP23sg/wC2oFRNAVNK/L/+3dG472GI4Cwagv87DZ6NhFrDvyJ74p4h3B/lYv1T
3OdCFD6H/LNrXPTTvOscNr6p/aXxm7yl8bIfvvNQCl0bKL8iQbGqIx8zao57wvtDbQcFTeT/YipV
1ZxzEkkCwQBNC7weUgS6JBwXrUvieYveVPAUTPMOVZD6hAERmZLhf5guL03qgOEMl6keB8f9Uk3s
dXPaSobct2ne1qnWVt98IGmsbar/Cz67NoOumFH44/1FBRHtpKE6rjgdgjsX+rtcUmy6xvR/4ICa
PnP8Y0COwZjBy7TsF16t8hpv6eWuPEKcgIFmIMnSdklDCZZuEOJsMMfrPqqHqoZKohyVm7UUzP+Y
rQ0bdXhbGp8s1qj34CL2XGKbZ7m85GlTXCHUF300Wi1awRNCZs/gO7eRSYvDxrQmSYFgtrc7ttW5
taA/MkK+aEguIF0TBV9Tkpy2z5ash1Pos/ZIJ2fBiu6ACdiGxqaaCnY1r05G9PEj5YQSHpwgmRp8
VdksWs6DKjZyvKWgFZr2sPhw4/t46YrK/UFIO/dlqn8rcVxoFMlksVGk9OM3OVCqJ3WdH0eXTCFt
3fWLg5iR9RWEaq8ohDTPRLXSWhk6RZwfBabgUZOSpLUxjNZtQbZ/1puKf9Wl1Yk+SiK5zXasxFod
OAMD2mdrHuVMhUIJ8iPb+xPzrUTQADRLGt7hIWSksVryqXyojksze3SO7S1ieo9cj2dNUMxqyIfp
t9A5q2KXXjUPav6hHgy1VTPBxrB6PuJ66WZ4g/tztUZ2ZvWtPUe88BIlCO3cRhgTNOj5qbPFmeSm
veinPrGPcwLo5zo4955NBxhPuozSCEezDf5GTF07b/TX9aV1xARBKBnqgKPWBi+G66//qOoqjlZs
zhS6tizMsghajDV/7h3dY+Ta21WqRqOI1bVv2eG3N2sL2pSoUa3+LoSfX5fwqKVQfEHLgB6D2+kZ
n/eQlpI7CBa18kfrvM8vFwUkORkC9L64qHmBUq0ZX66N53CtldjEqdnvcPD4ndciIiNhEvvBhVgg
vFy8xYS01urh3I2oqWD0oDK6gKQQY1i/orWHnqDUAxKlVqaHSRWD9UeiKLRl9TB1elZtWIVub+7I
HOSHEScyjiiA9d0nqc3MYEhsNc2UMmCd5jiD94z/AYN5IrPcAR7GB/x+0VlzD5b8BejrNBCVE095
UNlB1A3pgJ/ITMnfBmAQSvCtF1XP3EC1sUbV6956G/7CGmvzd1S3+Q91bVjOGi51Ud6vcLZ6YaOb
4snna2IlKf49UIZ2Buy1OrIsuyw2E8cLKGToDUxwT9Y7Uc34HUNYf9EQNQVo3FkJ7LPfSuCXPgDn
aljiF4TtWk9Mz3Cs6pZ0qdqkW0J0hDeUxypiJJIsw6biaA6RdFJzG+RswsWKJ3XDXDt+cIJfiSPx
nFhtVSfJWt0ERdK+sBgCA9u5wggy6pX+BmZH3Fm6sZSa3k4sFzhtCaxWq5lJeKR+8xPjoKuZh4jy
a4WxexZRojZUAEpPuGfw4CkjenDpIbPHDfITS9zP1UvHr1+XUUksRXYcW2d9ScN0KrkTkS3p0fSy
7B4MGOvqNpisD2gaCS8UirOIq8rTWpsrrrJRYjrT+7nKeFKnT6buyhg+homHtPWcextf+e/CL1le
FHMBQH2cXQRy86oMXsF0pQMAXdrd8viSAzXmsr4AErFf8p/0G24vN6N//fLf3MhOUMIkn0eGCv5w
LNyK6oPPCag4MpnPNl7uwF2isL0+NSLcRKm+oG8pXv4ukuQDoGXqcmkAOQqsWUFF5Y7eYZ5ZE7+x
aS9Fepl+noE9+Ku9j5gXi2/sUeeVqYv7uN70YA/Ha2MiZ0mMuT3pl7LkR09jpI+Sa9Se5Q27uhz0
845wGb2oKJRqdV2Abdm/hyneN3Y3NhZ3xVJSF1/NYF4N03o5Uo/4FTmiqDIodtfWSMnvwgBzIpBS
qAxA1EvePcfjbGwtfrc9zxRZ4jQLsQLL7epxR/oRro+aT6yXw9p2XzQlchyHpsMSLtZKMLx2X8yO
Q2PNs82xZeQmu1B/zDO9YZd8TGH4o4R7BRkx82rmItOzG30oa1vKMQzfdyAR2RgYTfHosxONoOBG
34pKllIYfTe7Hl9fNrqoloMfPbsHMhAZhtSek6SX/5V6M3FRVgMmGzhzPAjkHGEUL41e39x4S3ei
4hOZSPG862X5y0SfRomNwaqUjgNaL09DIvqspNujMVBv9/Ta+jP5Trr8mC1N365IIjr29s24WUkc
TViB1MmLYdLowya3DXtZsuqQYh3BGGs8MNpGJgywj69XUr9W2i0vm9QSm2ar3MIk8Ko4W4JJQEK5
LL0nLhniPrr5npsPZsMKo0NqDHTEXkTZiS/wREL6bxtUrCEo8yYDvcvC6Ew86SfDBNhUXiFVUZXJ
bxqNT1kZjWK5ihzcPF4QmYqvVDEZL3aB7+wN+RfO0j/A7GTTbu0+w7bvs/J7VS+TDUw/uBxYjYBN
oQp/zNVhxdKMHlG3IST05uqVefXvmKVD1ut81W9/f7dBwzC1U3tNiMJ/DQBqRAiY8XovHa6wxe3L
fnVptIoc/J11vOeteuKcVcHeTn15oya05DLnG4FPUVADgxSx6HBxwmF1K90guTTh0jU6Da+eD+qk
q3ALgRa1VLFi1uKiu/eJ/lag1eXwkWIzW/bauk0VBab2QWfY97u2dNqpVK453mleXUkK93kharAV
e2LlRyMpJQLUBXtpK01tX1LEMQo/kgBsWSdVBJAaozGzBuVtgrdgc/mjge7xaVvt6Gti0mnLpIjq
qoxElcZmbaM0aQ6gh5teauM/hABIz2SfK/Nrr1Zqf5A49MqRTiQIGC17KpY20k/PxXhSQztkBJpN
F8QUlFOv4SD+hnh4uwg+qmzdf7XAj6IGDNIdQ+h/RCEh8kR3H3jhQD+8CGKhKbXoAwUNgo/o268P
JXI7nwY1W8iWfIfBuZkuUt4+rh3cBbYXRoH80L/pHMBqZwT3AbIbZULP4ciMzl0IPb/5mYvi5c2P
jEY/tu59xP4xeZKoks9AYEvbFy5NVQGYrkgdVYtOnarsmKlxmd9VK2pMPa1WHeNxUXJgIl89k4aA
kqL5e1coAUBmx8od4kt52ZL3BkSph0yRJo+L5z6t+CNe+/xl7R7VmQcC9wg6HH5gKVI38ZECY9to
RuX8aKjQYKzTM77ZT5osx8HDZwHqYHsd1nRseYaH9EfKkzUgH5MKFocD6cHNnZbrBL60LqP9Oh1r
AglKeXWMvrYCzt5cjDauQcEGnVj9zb2F/mvVMqszNZjv0Td1YkR6A6Cd8RHryL4lEPJSN8PbFl+K
UZbqve8bZd+fu9MzKHq3CgaEAEWSatBrzI4FoEhnm8xC6UMwhkuXQyCtMOBxwSCMgaDtMBGXAE9W
V4eVkaPuVthcmSUreWcNHFgsu5ygVeILDFAD/6JZxGqjLadd7dCg161JpdRsUtGCdnO9QjXO021l
wf8IdVlJ50nMZyOsX2+PUXyGt87wWRHi9ndCQY1UXApuOvgRp11J2ThYaAl1phNkcsxvvbVTx1bT
2nrJQ/O/cSIz4jnnwGjPUz/r3FuJMf2apxh04T4nDUCfmlqy/fM5QNfBOLe5xYdoBxH2lmjDknTn
QzyC4VPTU1Z9xB/npWoP9NoDrORHw3eOHobLhWzP0YA6VNb9a9keE24UBBYcep7bhDIuLrTfDZXx
Yx2gQ+bPrWCDj2vVwpdA66+OhksNtK4JtJ55v4zHk664VM0W06GTXX5gYxCQu+8Xp++jfuhOFJuz
cTyhR9ZEQZM7V53SdLccByw8YvfnA0v4VIjLT4aC60jdUfTsSU8CvMcwaDjBM1JnkPhMyirj4nDv
KQYSUPCbTH6NlGED4OgMYHhEyos3hiqi3Lbkeq5TfL4b7M5T1czP8MEG0HtM0VbhlJDcxDKw75F8
puBAx/FZM2h/1/BfUghYdBKwSkMmMc1jUvE3eb7eBYiPovXQtb5s9dCEqH17azx6BlkwezY6uwJg
B5Zfp0zXK/fpRuuozeeGYk7kNhSYUfoTjxonUeRLMh1tiJQ5CJp6pMTOv5PlfNOYqdGxE/Uw/Stb
+OIh2mOWFPNFxKB5flSZSLdmeaC86ALvYbl0zR4Kp8Y2eDV9xSEZgr9I+PpjKl4esxApBMIkeyoa
wqto+CXaYhbrxsXGb4BWu9kefToauT3ma8s8X9LG+Rs/pbALXey9+jrPNU5UBLjrgrdyF3+W9P7H
8HcLSw6G3VtqA4hebWuC4Q09qQGWjqaO5jkEMJ6Ab940O5m9HDt/XNu6l4Np7ZOI7Ugn2CVF7+Xs
kHItq3g175t738ZUjghr4B3vXsLpj9CiwcT/CMm8noZy8bGt0+SYxNVNKYmM2tzQ9JGaxrlRPxLd
m5e/kUrfSihC6sWzyMKNMTVZVHzZ2VsR4l4eVgR1srBoOJfT8bdEKdbXe+cqoS2H1F0mkuleSbtA
xbSnr6jbuM+WXeCVpgG4gCMFiDQEFFjcULZkHX05EmMbghCBweK1CAufZfxipkfMHPQG48C4MUBn
ZpGegH3EB5fpJ3U1+JIBJPoe3TursEvFnGEA0dD5jHNMgnomGwFKKhJ9ilB7P1CWB4zOD7RcHJu+
CKHw37IawQ26Dg3iNN68IToNtOOTgspByj+sV5wqCxyjEeWgIoQ6w9m0khoSuE+T7I8No091J4ag
sWBWHNCi5Wbifx4CZwe1bDatzZrrnKRX2HTAbSXt8WyBzkcgiqKifxukbhAMPBPJL1ow3MSI2EjZ
o0J/+rxu4BNAyYbO8VyJO5rUNnqs2tswiwJg1vIjpzD3qixcFhl3MbL+D0aOoAkeAauPmxmCmVLL
OlDnnM5Nwy9ihJU7FuNKQuvWC2iJw3EeDsjcnT1ma6r9MxjHZdUn5FVhYBzOYMaGfDEqFDjwZyki
lPyibSgvlCc9uUR6QW+nHAf0e4Yj0kuBpmTWNpRXd1yTN5DcPbKyeZqj+x/p58+Zk7SlJ0kv9LDq
4czEbPeAryDYtJnUdfpf0/ILkYVRg8EzEiWNKxbJazGpBGcmzuviqOLu/pe3RtH1M5MBHty5r34c
XYl5cEOc3DQ7DESYBo2b0t+RyLOR9slTqXYwjzbSYgEUzF1p1sZOExNCfDQ+5XBhPmHnTomF4DOp
QIcLTkL1rr/qCg3omQi4lxJIW6Jldjmyt6h4aHxsK4POm/rZjkZOUem2n6cNL8KoSdRWUweyuRWt
jR5I/F8If0WVhHBoO/FNzHuQ8ZlAD8f40aApRo+nzM/LXpTNeRdCYEQ5rb40Thh9nn557pLDvjL5
bdDf5i56hy8Kyozby+ylXo/xa2NnTi6BH0tbmgn5+c53gg7BRmPt92x9H/gs3xE1kC+rd1Epa8eG
7wLDpGQn6AxMsVqZ3d8BGm/MC3LBZHGippIpIC7aIxPvjfO9B5GXyHDRIQxSpQntJ+QJPaPy7ugt
6D4tDwDg2VqazhyhPnrl+XsXjraopyj6jxdWgRcb7qjWFvBjAMziH76VE9y8hzKd9P1VEna6+G0b
nRLvgEI18zXB236FyAeVyGLpXn4t5BBKXnInQ+pTovh2p2qWFBIY05So2zCh77oFtXj4JXVeUbby
g0NrREa8CbwjYZ6zK1CXONO77KLDOC3FizvHO5T/0qKEfII9uWjwanE2QJjJGomyrGqckMX0epLH
95s2ClViNdVVi3ZzO35fcYNFDI9j+yXKwlBMAsZNh5Lk1rIFjvLcZF4D/9vETaW11eYiQwgWQG+g
2cdiF+MMJ0pAhrRVREeYhKQj4SWahPP2HxHk0eGU2WAC88CcnjWbzq/utGH63Uzl2g16H3w4kLMg
Ig2EMrkFngnjfWVgea2hCDxj5rryZ3qYeZJGwesc5/4gPUj5Tn06Xc7Bnnt8czDd7eu7ETjuxnG1
Er8cm1sQa0e0qjCfQlZ3LRv0h6pIqoV5bMhs4wcYDDFLYTZUqKuohYNoW1uEyb+F1fGbOklf4gw8
w9vWrWYJz/gNFU6iET9D2k6K4jkcqoCYY3oojAz9gme09tLayu4usGBFloNqbh1pZthHw0qz4zqX
DmuZLOKB1aFf6d5XRCxcfhWUtOwP7soX1CIR8fkkhNEUXJsO9CAlpCj6fCdy1oiFIwKnFwLGGNuW
cwtr+QZGkpzzuKKgR87yDEGyiisNYVZH9qAa6ShvsxVwGSKw53jeybodSitRjhGrv/xaKMH5XIv3
JzKqZVSwLGmX/02RbmqrzGdLVIGvrHJv0C2rLSGVMO4DQ8hf5OstRNy6l/VGU8M4n1CwyEh7UOuA
c8fxKrCqdYdjARwhpcidHdPjpeRkC6/3UFbtSizyGsIGnqstf8kr57ezGWri0r6gC1ThVc7guCaN
qpLekPSRHkvY+2wicZUivg9jtK3vXJrkZIpOsXoC+vRKTIjihHv6u7IGg7seL/Ck6/q0nIhGKXVk
y6s/n5OWyCn4NGAyypPL7BsQAJXzjPRCqOAFrahT8vldbblheWhfOxLu4542G6I+b9gYWFTHqpkL
7KGhR8NhFVVikhUt9cMkRY0b2DaLlMRY9rp9UbbshHCBgCwNVANnn6AeNnYLB7mt3c3+MfqVp9SE
EfJcZ050HXUtoIFpYIryCBZOkinWqNQbIFO9M8nK6C3U9hdhL441PsiBWrXepDtH3hQvwKIgIsN6
M13/5wuKPEjkGNXXs1Sr+YreyZpMHNobLMLvr7jLibNkSzRtBpnFrbrxTQ15uon5ga1KMY18sw8o
UAlS8CQPVxkx5rSOk8Zrt6NGpr1kwoZzqzo0vbcUcjcIW8s+zeBQmszxLOP47Wp3GgH+GJwuU1up
AEPmXBuzdjjyZ9RXGLOaWwAdE5pGEx12rcgi6ozd6XVhvs1O30s1aZV+B3vC9FjjDCh1BoPl9MGG
HfYWcEFdmJroqjPGfJONE5kpS/hqHv0dWpo+MZkJwJcbfarl11/kopHs2WAWSNfpnf7RXM2Am1Dx
ECsy5LaMzaRsWkenINp7L5rTk3VKOvU23FXZLMGtwDImzbGLHTsaXoQ+sOlZG52pyakYlfVbu4aa
Foof8tKQpotDXNUl8cPkJZ5C4Oa2eQJUXMGEOBIFxE1SF2cgjgMIWLfdn2X0t0EJK3lTQtBTVq92
KQOaYYzJeEswmJ30LHbmBafO4IiMuOZOGh8SyC0GFtICDarBdFmU60MNTlTHBWiH33cX4g8jVZRs
hWHLFCTfNf0QF1glzo15Omi4HsV4SWEVTrOgIUU0I507Sl8G2RAGcOsUR1W//bqZXHkyJxEUUJaG
cChnAWbgtIy6WPTgrwyCneyZGir3Xi+iQGpXrakvHPnb39rDLc+tKbPiJ7kmIn5I6A1j4c1x5xi7
fbADyOZwNShRQ3YFqfVQyXenzkZHz9P53nxuwY9FYBAm4TJgSLLHw6OZ5RaKRZD35JUzgxOi295S
YJBbH0J8P17n1Rf4qQjEdnSIrQxN3kgcinOzkA21E4oppIxL7AXUBD29/RTb7a8YyKhZ09nCFAsR
bKofTsQ53AQXO7bQJT43tvtakNQCgVbODO2x92+DJuNp2k6nyFvxPeBR/8+EGoNjzhCzcfu2RmgQ
hoOSI/RGpwzcWsCaLF2OGOfIU8eSEDjlKMW9ODdbUOoqV4YEjkKYypzui1AiB7k/7/u5MX62Dz9Z
7wQP2DWABQ8gwRFZRYVP7Y7ZOpcEOPlxB0k6TyWcgOq5XEeXEi8uTzRLWOdbng/vqw4qxs+PXo/l
QQu2cSAWtqQZLOinDIJOAA5Ve/lUtrqVv60ouNL2+fny7UKdvGqaHpg6BgoYknp2CsBw8hHYyate
aK34QnNxW1/cdOqcAK6Q2AXk2qhN8MdTpOCst57fvnaDdrrf8MCmJ1IAXJeG8u2RlUxg8L8CRfQ9
mm8Sz8zL0gMzI1Pta1WNhIL72K+HKv/2G0J61WTFtToCyoPmds/EQPn1uddd2flLd+n005NHh6jj
TyrLljFwCOHxmbu5exlQVPeeRW9rZjeTgt6nE3heaTPoaSGJPk6OwHDwy4N0NP4g2TLEPK5pyxib
D9CZlJnXbqZX9P5todLwXPcKHc3zwuuWx/1ViXuNJyiZT8Zn0NiKN0XRBbfhlv97cJUr23zJIzS2
oY5xo2WJ0SPbCiuCdGWgL+bzadvOu44NzT/5I26uVrmyQ1MA1I+ftutxBoX3zcysucuTv70nN1Co
5iExOv4xiubEBaVRQ2UMSEiWeX04OfkEuSphMjm4+zXcV44xC4rZMzajYSjD3inWwqCN97C47UVY
JbgZ3Me3/rXurXf7zYJFkajW+s97/oeCmfp1pZ0iSqplYsUUbRXWM7pelrPoXUzCsb75/Zp5sV8F
Q3t+jtwXLErzUTqe2P9sGuMsxDnnDy2x2SRamT51dxP02/EDzov0W2dVmtSLcGdfIW9ER0lJwMST
YXyfgU4A/Js1Mzv2MbOZif9lHvd1ok6eW+mWTJXQt8xAAQ+eoCUrmRWIe8QfKK0RRnpjcMs8keiU
nCtpeoya7bgcZWR7jz/nDzrCoqHiXik3iofgeQV+J4CAMmg+xxv/QaBOGBBvGiuVKAKtdQgNVXXw
MwrbXvo5vH9yWLpoTdV3B9qEgnFBRDgi0uDSEIOkWqFAHcV4jGFLWSc4XLhTg5RL0qqURT9J7/EZ
wcWbbroaU4s6cGdBVyLzh0K9SaFCNlOjtc7dBITodp3EHHk6s3I2viyVOGsz85qIGfDdCZyBiES6
x2h61dqiTXkZ0ilYyZdasr/4GjFmFWVmcBfw6Sc9/o7B59QsBCxXC6eGwGtTa/xGH8fqfrIeANgN
UMrsWTSmPI7PaT8uJ7Nl6YLIjmoN+spRGlBw9RpGwo79o2LBA7rBYQFP4e+Y8oOKvkp2I9jTCQc0
Z4U8HDc+ioZpP8wPhI4MOvQZICiOU/UKiln18hbPFeaqJAYzIxfye7+dbeD94525aELazcsFnQsz
vOFsGMVK2JvZlyTX+m+Nlp1u171RLGFbJYz1QzGntog+8wxNsVEWuz+5aFbwCat7Xzy2A7/At8Yz
lSp/aHcuVczYCoWbQmQHi2oYkohvibU45AvuYHvDs2nC42IW6CVTwj97n4KSVgRGg3v+dUvtjxbK
pJB0UvXJCiGc4aGo7oBGTcvH2z88XEIZuNYrzVco+NQ0jqt1OW1hLprUu1YkA/SVScSSMk6Sfmi7
t1H3wS4pQ3rO56LNgp0jfdpGsvDS1HcR2jOfSkJgOPognXy3jWr8G/dBHXmEhDrvbx1X/fmNLcTB
/eFbKAHKmdjgPaaMKELABb0/5fWnpeRizFjbykuAH0T7g1kVPJGUT4HQo1HJ1ayYvXbWOGAxZLtl
Sem89LX9Pm7yZ6R6Q3OUON7iww/vj13h8ahVCLbh7b+rr+j8vRJhBf4p6G6sW0Gf1Fh1gRh+SfTZ
x+1PmjJum1o7mHMXKpi9L0+HuDvccjZ50VhCpEj5yT9gfj5WzDbY5605UqY8bmka6u2ZFU52Atrd
KXq9DfhCXGYcCIlou5i+si6OAJfMzek1MLU3eXrCXGGwnuvaFwZnZ0pJR7Pyx2wwZfAo3CmlKoKd
tJ111w8SBg6KF09T2+zEeUpp3FLm9fMfMZwFOFkbN06SQbuwFLcH756m8kI5qCbNZjXnNiXLosEB
knrwq8bjVwV4Arc1rP3pJFEEqQs4Iv2RuQF0keKyNU2tyXIUJd8mGUfvCV0V6JKK2OpfuRxCoTNJ
8mZ6pvmMiLRbrSKbDb16WUy1A368E9+lZVNg3WHUFo4QqCqWk5iH9LYOcYqDe1CVVPuBZdEE9vY2
pijiQGvco33b7o2Kn7D6wUca+nWqMuyolHIIKSKzQipAFIdOb6/2xIcxGJfrAza3Cz0qAs8k5WTO
xEU2O9YaCN0TFs3mYYGVLe00RdsThmYczgcEvnhMR5I/cdo1LWge2q6bwUXO+gswvNM9HOHDDy2k
a9dfOrP3QvpU2qzW5nMObvV9Fl5R0cI9UcBW+kzgDMAd9CasdaLFFtSTFUcZKzZZPF6iuTmWhaYt
VShdoKdojkrdPAMKKVF9BR7k5Wi3hHEUYOO9RNvzguArUVVRGqwDftg6YWlB+TR2GghuixSDvTo3
R86jiG6ECOef//iFbFA8IulyEh69YufYVv4roYhlvo0qvKoLRF6w8Q9RrHVoSLxC6/ddTx4S17PH
2X0ZspVw7i7Uc0mHwKcfHVufv0vUsUv7iTJYvUs9CjfYeeh147MRWgVZ/s6Lt4MAMoFjwNtDb9/3
BiPZqUGTHuLs4ebYRyIlxSXmXTbUeoa0/7HkAz/gvZm8EDlCkSRVFJicXCn0gV212cj11ObdIawd
UJLr04udLt7pXblf6YBqp1zxUSL7a7kzNAYy98IL8a4ZXv8U7JCUbOVU+KxPa9Zztjyi6vCIT9n5
ZjMRui5babcpnd+4JXw73AJ0HXsgtUV3+0II2GP0w30ktoOoD1LDfGeBuGe/5WQuamvnevDb5iR5
iOsuXMjYfYzp4Z51nS/GhFfpM6XLGDt6Dk3lc/ZdXqtvurXJ+XXFT0l58lVEGKBAi1Hp/W0OzS4W
H8YrbtF09iUtPNI9591b1Kh30rjQvHaUdpVjfGAce8r42xXwBbVG9xSWuNCYwZXVYE152hHKNID0
syj5QUGybmtbYWVU+zC60rPv2W74Bif0b+7zZpBkE16gZCfP+xqc741HWjN/prrStFPxsU2oL2P5
WW4uOuz/1d/6hvZTnRnocb8alJL8RfwxiDfr7Cu7PONzBwsYNjkGdSZEeLDaVlEMj6ShAi2fEe8l
CkJ7pjD0cKTb0W0L/gATdyDDMEa5aTRRyXL9Sd1K5c74qL5DPHeWoRCdSDkF76QzfH3uIkY+pEl9
6mZ+z7T9JB9nzA+ymN14WwhzyJmsUuQ6Bi1k1Jg5wUdE7SePLYLGk3A9rOjnDuDOXVt7WK0n0zpY
pZsfy0q6B23Cq0+JARvIkGgEIXM8o70ir2F324kVWoFt0pS5RJm7614zydgWVi1I56RoX8PzrtVR
01SOD6EoA1CRXiRno1xRIlVEItsEzl+ZjpUPBJZnrgbM9vu5zC4NZNf64xqViGOo2cdmsL4Gaqox
M58+ZPOqor+aK5S1GyJEjqzqcjHmk/z4m4VENlqiD6cx2QTJIyd6/1KbVyfla4Y+Hf0Y1VUGNqGV
8vFytqQEpgRl2slsxVYNty1NwOIjF5KC2CH0MBtCI8jOp+9h/wn9hwS3mQvU2ZtdsjlrRETY9CE7
bVtEUsKS8seKjL/3vZYv2677uknqfoD+1mGIGmPr7UHNHKAAOrx2PpnsLsMb/kwc0gQIhtFYvuP9
uFVNi22Upy7zigYa8IHk0exOtPmMEfVPiXhA+d3J83iBKwBED6i6r/gNWIvPI78mNXpdsD+Tqxhh
mmzNawrsBIDLnDwITNK40dcx/MWCPr7rXWCVjanvTPGD6v0O1fSrE5fh4yy0ZqOKQ5+KOfAiZkGm
7L16DJyGRsZwDySjkM4/2xrP50065eVuLxRTNsSSp5lIFpybYBzhJ4iqG+g6nGRxRPmbISWV1L4B
uO3lP53Q1L8tMpxJlZpSOAC14cD1rzlMeDUy9HZlVBtiHPO6wjhmIZ+U4beM4m7pnV4McbFc0Hal
doEP2c9mzvr9vvkVhZG1Kz8ANu1aMxGOTYSzK3E4GrKBsROPXTQXwabqdKcb4wCc8D4HhvoJqI2B
moh+xPVQrTi6FgnGsXfJZuykmVusoUy6OFXIujVrvbk4LQYQP7w0AzkwjDjeHVw+7L1vOzb76Omb
Gveq1IL9lJFNwILLP/FLMP7elx3Eaf2RFVFmmcWuaUfF+dX+PAocC+5lVo6Y4F/QnlzHV6QOsuKP
Gx5Agf1ni3Q6d++UCDPnHv/y1X/HcnCiDk0hhZS8Tkqz+6uGVVG0/AIreuVdXjbZIVdHyX/v8Y4g
pWMqsk94jIlhtkwwuSNjCLcUaa7Ly07GSy/EsMIkolFw31szD+8+akx5wbw7q/tBIDyF+AvFLs3X
Fks0l/QndaaB9f0jQhY1V/Lv0iIYSe99Wdl+f0XIT0oRlwm2NDUUUzVBbuFqc2w7Bbg2/y+5OCvX
gE+pGl9tXzvAs6gas3fo0iJiqXvpIGiaEKy9aSeBnXbQWDByfdkc36Wgsu1tm7QsSOUa21JCaOne
3ND1OhAXUKbdOxxDt6s1YPuFv3I8AAnUq0IPq2JOvUvqPp4+uGIlTbG0AhWaOpLD6kKwlgmucn+E
d7GnydV08Wp/B9fFws7ognq2cLhUZeHYWPNY2sCxZjFTNqsD0uFHK2oBQksM/JUI2+T/BaGZclLE
NzlM1gXifDEYsb2br3e4wFLl4Fiv726edXZKVOY1v0MusQaRb6DC4PEcX9M4cPR1y/5oAHL7gnOd
XoPMil7ziOWRzFlhOzRu8f67fmNvlfXUKPhF0LfcJUugxKTalJ0EIAKc/97Pmr/WUYZslKyjWMIv
KBByPhjHMjq5Mhr9hpkd73q9g9NgdsxuDdYw89EwT9r5xGC+b7UEfYkpuvQevWZ5Bc6YTxVNgzSy
w3Lt079AalmdlZ8o5s+RrUK7A+EpvnuootVOSixZpfVov8ChNNYil9KT2/0w0Wm5Px4WgJGm5Fa1
6uci+fisUMBIg2cxFdi42RGvkHqt2lryH5XiN1rdx0J7gVqDoqTeuKY7uH+N4SdTmiL/jD0Ot7Aw
U8+xhBBqN7Vn/5MDQtFAnIvye6GT0d3MiWligJ6Kt48sOVFBu1lTD4Wy4g7rSxcHBIdFjuy+XU56
OPOCjHC3l4fECphmqwVOp0iJXFCWYdn6y8ihu7DN2GhKdvAbuXMNRkRshFRTW2/DxCcwl6dhRfV+
xkr42SxMBVZxQ7veJjrrAmDX+StgDv58gGnN5jLyQk8Srd3eWyIGwlVAtMFAXuRvSBLt+YgBYlNF
O5bJj74ClfDBrEFbCoNkNkpxXi6HH2cbrqeDCG14ZmhdYm9GKoT3DiDFDTtFUoCOVvZUTeb+nMlE
jGdZGa8JRy/XD4ANZA+kvUjxT0rrOlJNpMC0uYrTQW0a024H6uPPgVPSslln1FEkKn8Mu8Dsa6nh
G5FiCCSgi2iV/1dc15q8J/HiguCM1H9RbSruY66g/MA2ecqoOYPdRvHb9821e/G5lFdwprreVPRE
KS1hh1mwW8kVnqNdV0XT+arZdJUu7p2qrnvn0qmS/gbzBwuqJDsHwevXK/5lbF7vjnvT8375TYGc
cscesj0pm9y7w7bcxXoVHeaYQYjf+pu0DxzgGsUgm3R8WTteFgCUyxMZcKzjG70kkJ7iUKOLm979
jH0U03wxyzjtCYhkmuIccNkeubMQIyQ2pNqU3lnxy5tnUz4h573PTUurCIrSY8wZqxHh/oCkYClS
xaiSUXqP5GiNfJGenEKwdzIT9nD1840EdjFCeFtev8gziJvLszy+XrMqsrbwRkmne+5aTVyX4kts
A9kONQ9vZDpadFiAuztbBdwhyEr5mfhEGBvj29OL3AVSsHY9wMrEyGyKBd+AA9cBmmJIIWveXf2i
/nvZp53sVvBLxP6gqSSEqmslOYY/R+l5u2eGX9/RzdGGIT9OBTa/mD13GuxuNc9vzSMY1kAA2hdJ
2LSKEUHRhoHpd7LkSdU1CxcY+W0iMqjE3aDacp3dEedQ38K9rULVimPEJp+026FMQOh1U6aU04WO
U8j4yfxzZ2GvBIhw00RMZa3UsReX2XW+ccGX7Iv984MeegLXp5/PRfrLZ+gXqikTQbilZNaSTh3A
4Ivq+sUPaX9mQw3xPU8HbGznEhyPVYQz7JVunbegrxzFbO7HLJScceLe1umQ991kBCl54dz1eIhD
Avhitt/+pxMeBoBn+HMcKj2o7gR0DwqcGx5mtY4q4myJmGMaBfAdIw7e+5z/37NjXqXuensbt/zL
4itQFJ08elc2Vu8hjlhS+/PQwxK7P6Vghsma5RBEpiE4sHYqjPC4pkNX9++/EyE8QdkdmROj3yZT
sLyRmgnP3NPTPdqLB2PWIZfB7/xq1C5JZxMe4AFsH9UEOJSWqoRU2sey/SCcsMa8aYdjZoqQsJdM
kKaUlv8v15nb4CaxTS+MIDzuzFOKMGwOAeGPIk2ZvuGlXInTDiBBenkN2bg0nkbV0oVrGzspf0gY
++Fq2p1qnEfe5+6wiaY7XpXIILzslNs3TndnUAzWOFrFIYRO/yBoQkxa0XFhhXJVfeHcUdxoCb2U
aa9xyu0sYLqwaz+eXTLD8BwK5iEiMxLh0En72NS6PDRV/B2WUKhy5yqGR335LPxhIC6pG4dvkWWt
zGYfWp/igbZdaFfNI0qm1OXYItvpeElFSvz5TDrjzY8rhFen+vtIoMo7c8uMCYpM7rOy/oxF0I7c
Tni/bTMaqJF6du9L0/469MDmxPSbP4q2rqLqYF12eHM2R/ZpAI1AhB661pFDXb08M8YkpU6fFLXy
j7CsJm/L9W4HmSLH1Vmlr3vBvjCXO7PVmv/J4xEItkqfj2/t7UBFPvY9+nrmmgUDovurk6fCX1kP
Wglug+hDtZCw0tGV9nSDDXf6qvZzOL1700GEi2un9aWMAVNMVzTKbXkygeCmRjUNooosjz7eXpH3
ydfFSPKBSLPQTTjIlIP88nxmlG5Ur5R2f06NF9hTU9Hpuy08f+kmwzCS3STyYFShpX3y+RSH5MkD
DBlhAbB3njV3elCQG2hog9CbXbHo8p0uDw6hVWEntciiNIBAteXXrNK1adY8Jw1NhVRHIG9mSW85
fVK+DyzMCssVG1DiHo+lbpdcvkCRepUT6E+DVhnNoo6kpATxG5sdD2jWYtmc7tp2v9REHS63+3Th
lH03slilAnz8TqarkgkZuP8gTQS8svdKqNIFi+LuLwn/s52nYs+VDfuAcwVYhdp1rOWY+MIiaG4V
gFsown4vrNTcJmTSnWVhoBYrZPaH+BA6LyeAw6UiE6/ey8TwBVWKh7TZuZcGnLpy5OEBJEOaVR7X
23AQzGweFVR2lNj02BsT8n6J39gWuMwrTj7XnMUjLcTAnvWfEAWz8vV0lwpsZt1Yzh8Zc156/xwK
Sco/ZiBMo0Pmz+d7xCGat49NpHcLSP+6fPmEsbwq9CcuvnjMoPIVgY9JCpmeRa3iHyRpQb48djx9
zELBOEjndoRYHRc/O8mBhnwVUTs32IomDJBZ2fK3zs6IuCI3D1HP0GXKRnYhfPtJzQfzANwP25I3
fQiornTGzlWU5Mqlhf85RHDFO7raRFuP9x6AQLM21I4lhngncgM8dNFLSubCKDB4YgfTNxXrR7cw
kpM2TXmsUH5lgi3IXA3TLBSXwFk/+KYxwWDfX1SW8y5CDp+I/pzL9ak6C1DxnWoVGK7dfBIy2s3D
GRrLRGmTe6FHo4WpnBvpFjTuXNhslA7RVaBY5/wEGUwhx9XcLuCeZuTxf7onMRsSGlE9Q3cp+ZRl
aFMHp/qGwaiKHY52bB+m+oun5uBOy/1N7oII0JROKEnKu8vvLl6UYWVjwrCHPutBi+c0uu1esSSx
udOZc1W1TXzaOevp38OwpHip7ObHJ+ZRNAkQG7OtgsL4uZvfZ4WBIdlGIm74g+C8pXRLPM6GPaMb
k8SxreJFbdBmaJKb8QFyTWHZ9yJYCCGWLKmbpshvY4/mV4FWzivLy12ECCtujaEz1Nm6syYbit7p
jI08PJAlBnSWQEuHj7dMi4XJaA93Jax7zEarTQPcXkhosnIm5rw24xjkx5ANP0yK2tGmpbSbwsrR
+v95bi7Gc0sPAXo1AawNSwjnGB7eMhllCxbl9t9WreuKurG7B72aCEg7vHL3HozsshJbJ25PjIhb
k/2QR+pu/w0iZsdA7xlVTHpdGoIMVZqZYwP/uliv9OAX4Sxk/hxLtjbOhM4xivMSCVAnVBamZsWR
3OakB+4U/0b12ylxZ+fiYP5kK/QL6wvPkUxT3e9uzP9Zu1GR1mPDSMFVhPCGt9fHDJBDel13QCWy
toMlwWMlIY9oXImNT921hdx9epo+ZmgzT0WMLIoI+/DTbJHe5F5Bx9oIMTWbHqWFHMVO5V4QM2dJ
iq4TrdFmtvTfUzeIowT1cwWp+lVHY2QyR0L1al2nieIWtRIrwdPGV17k9iQQwp7qiUtD/Ou9RjaE
IIY2kHIvPxUYjUR6PMw8qd3IYqENb/heHE0fVKp+DAaHhltr2ltKNaUiLA1sn1Ja3F273EggMEaA
fmN7oyGSlNfESfV7BzdA5yMr5lzrr2B1yND2/OQ8v7AXhfKFXLzC29FvJXD2N+lFBTIf0AkUJlb7
Q9W/2uY5+t3aSsDT1snqq8xp90of4QTfc6RO6JCKGR5onCCVugHu/wkPneJGjRPwWDAcRdd2Crlo
2/jzc9+AS1gDwUNhrNcgCPH886H4VZi44LxFHxvuqzWdcr0ctVJu97HRhhV6S46ODWsSOkqgRBI8
6AIQ+EAt++K+noelFsP8PDeYb7B6ufwN2tlCr4IksW7hVnGp0ZgYwkDaQfxL9wUbfrORi+dIPq16
NgtSxtXywmsj7vs3QTKh9bOW/zQP2qYtHx0sSEW56XCaBKuDbL8kU6dwGBNK4BopcWAIwiwAfpcj
pB5OqTXPDAY3LndjMqpiwsvrdeZBn771FOYVkyxo9yytNqKm/C99glOxNVn84DaUpAGxhrWaV5Vz
sU5hmgI8qp65V+TgVCLOGziBxVRsUqZzKVjRnZS/O5dF+FH/EP2IikcYgD5aEwHx2CYZzI0WEBwV
cjqAOurfcO9fs0TvKcDLDA1n8xX+POkncLE61jCbk3W27jIkmaSI4XYSeW4U1seIHVyR50zwOI17
Wkyo+3zKI1v6w+k8yG9i7uOmpA7BaUQdpBCTBByjJrkFBKPb/USPQh+bpw831eXVBFO3wmrdUC0q
VBExwW88Fpw2AsGHj7NhD45bQnkotGFg14EpGZfSllbhsey36Dcn7d99fmcgg/GeayKx1bqHeRGF
1G6PszzX2TO4svDzvzzldUds/DSYrmkenIzh8h1tr6q+/fbG8M0NiHiAUWflJJyDgpfOhwqk+ZJi
34Bt5KIUkDjbYXj7os8qDTeor/YHi4gk3UdJe+eIdJeP+c7mHhdHi1kKs5DhhY4h/BMyeP9P661o
ZmUMs/06aRjk5PVmogrZOsB/MIITLuM00AomfIzXEX+RNDrR1wilnQnT2OdMmScFAUf9Gg2BiwzH
75OD9BcvAS70uAqZHCVM6Sb1ffxh7fJNzcBiLyJAb0aeNe/u9n4XK0eJEfIwDl65QuF+y26Xit7k
3zhaCkAyNCcsVYSGig2EqiHSHgFKtgoq9TPg77JYkAPdLxJ9NO/LJSt2vjpDreAe4RIYrom4pv4A
TCHHsqS3nnD1i/b70K7I/ReugipeRi1Pl9yklejZoAfHhBca591MLdjg9VAd5Ed841ZfMLNMMdS7
tHqdrzw4R2+oPplp3gRiGskhmybNQppzFwERFWrK3Lz3GBMfCMokYE6B6MCfGI66gBCriaNGhtp3
uFnVWMqQoF8iqqgWa3Y6LvFldua0Nr0XeWyOkwvl9UL6lanlYa37HLyt5QxyxbO/5nTGT/uzXflh
ZqwCOWWGkYwcBquBBzDhvtqgUPHZbFMS4dA70yFqo4ibRHCk3Z6zJd/c6HxgC9EWn89NlAR3Rox9
ID0Z0zuLGjgpHrIdHlwNOmpueRVjiCcZ+duMFan0mCoAKOyzl3OIGq9YsAE6wTyZoLlzU8s5Ezlo
U2NtQ4gr9sDIPxCM4wC97eR8Y7SHZB9c0a1vV9ejFFJn0uuWLVNdH+GRXS4KGxfkb2tcqp9eXJOG
kT2wb1Ms55/VRdoSFWSgJH8jSon45vgD84kabRHB20SIyeTas1tvh9NnybW2JQdwR/UBv/3S9dUS
LaTxDJXC8GquG/5jLtwkp5m/mlAX4lqVGxpXpy1N+hGgsrV9HnvrGtkH38DYm2D9SWw4+FaJmR1f
TEO6v9Ml+dI272FYbKe29myqmURRuk388KS951+mZtfzR/XlXYVEgTyc2TSxOmcRzBwnE4AaxTfd
a+VWM++EmkijUnQoBYpKwKeckM7TPfKm4s1lf4rf6oXcL5jInQavTv3NCOFXI1+hbfdZzimE/scO
o961Ozztm2fvoVX+H2QP7cAB560KI+o0ATXP12KKNNAi6RAx9rSwxcIEaRSv3b+ytFZpYWwQiQNu
TkK4zGzZn0u7gA8mTdJpRk1rgQ6Af+S/TUA4xLCy2EwHjnL0lmBQ/76P7hFgBgHf3RmNeevsYBh9
PGweQkfnrATGLPFXg9293PQj/e65uz15RbfIV7k04K20r8yawcqD2xSQ5Fxho41+1DnHngpQXwUS
I6LXDklW8xQuiDLZ525QIGiLhHjAX4VZE7A5ZvJzPYk1zpRMA9+dfardypcJYB+B57sRPL9BoNIU
TykS4eEjP5/6tiTFPhRjP4zJO3xgk5N68pGtJg+4BrDq+GMZr4ZLHCQ06jRdBL0qXfIaZuPcTAHk
1v/ePaz3+JBeT0nVB6JLw29+3v6un+FBwlETEB3ubWtWtktQ52aOz8RWHBe+t1PepmyOCpajvGkx
cLxiLlElsNh2giSYei8/Z6FesazMI/PdgDMd0qeK4g36E+aOxgzmYADwh7vybI8bIfjfRz1VwFBB
y4vzVGlk65ii/70xwE7Txcdi6UmDM9oXEKNB/M77O8cW6rVrUtEGA5NXRSrkt57yJ77KVpjSibyn
y+cznr24uEGNBTMoPuKloYd38alIjS+3d9ywgxAryTVpzjB1N/ZV9MrzZrkxJG5QG9hwmWq+5ufU
YTHQETCuEHQnHQ9woUSCCYGmFK2+q2wlUHPH1X/gaH6GTOym1ZmciCz021SoXRaFTi1qFQR8uFVi
cWxYQV1+3x1kA1w8DX+ODphkp+lKuMlXa/GvnFfYi+0rXXZuDZFcczqB166JEMVQYM7oUESeWTkq
mT6n7AJGyinF31r4DBe4lMMXvlmnrfZYML9ODkc+Wb7v075fyaNP6h9IGNH66/YeluMdfKq9dm2D
aELflVF3215DIV6/qFVyVXz38/z+DAaHF3ucf1J5UPsItHZpFyip8in61PpYxTjtertRhLIRp2X1
JzwA6x6x+SwhdFv602cwOwyJoYx5f3bJbX5t9vAEtOcgViw6wk4drwuosEqf4CW+CEbQHg2HcGqn
XGy1/7/MlgffUSe6fkEui6dvw87a7O/ikQlLFh+2J9E9RcPqaxyBoQ0DoJFi0U7VPflyRiW+ZS2f
ik1o13USdS8BgB8+pClgW3sXXqd4TAGL0xITKR03oxPMzJ+c1MVof8SYt7TO3Wd+DFdDhw1cWDBb
EVIe5k8YEFsih1PtGQc5X6WL9bqwZx5Fy0lfSKfCob8XYvX8/yxWkG+Lj61QRCmy95ZRaAWrCS+2
/Z4hkhI3jKJof+teyaYL5GL//d4hlp31a+PN+n3APkeJCaXz/+/jAjLBGSiHhfRayP1lzoOpNwf1
q1VIgdVFStqMcmEM4jPSo5BbBTvugoFKr2oK1H8k6UTYy66NpDV8hsUbvG944Dd8HCyl3U1SbnCJ
8eTQlrom38Nn1DcesIdeilxd4nYDSSn8gIK5xv0i8L25aNUke7NRThiyVOmaLk4Qj6+vBs9s+51b
rFonvXGxM9y8h9ZQxKixfWWEr++gKeCqQEudGmXsv6npEy66HC0HPLrm3Ia1kXHdyyrcr+N/cggt
rIFsF+l3p1a3imi1BAdn5nfzSRQi48ulzW352rAYQ+YKSDqF5ndwKPV9XVV8URnG4HPYKgoMYLUx
IO8hKhCQEZXu7uEsP8or/r/1TiUm9RsDvf9uB0luvP2SIvV8ElwU0Tdo4phsOiM2XUVrnmG2pg41
f9YccfvOuFzQtLskzhgn2JcLycmk6+ZB5qQOoK70y0PoKkywKxS343fXlHqEul2t1XWUy5mS7k9J
ZrAsszE6p98arsPVfnlDB6zZsxnFC3sXpfQ0zBE9CcOd8bW9nc4+rmgMowM2Ff2boJX1MhF/Kwpc
UZtFmdhaQphmgzdUfkIVYkJUzfDAyY4LSy4mjGRMcdvA8UzIOV7RMvH3JWHBvV/Z1YlddZMb0TT1
sza5hvvsYFEdx1dP2gjgn6e+ndFuLIFieaHezUbfo6BB+t7gB/uNiWmX1SNOmVsq5OXtLRBAZ8Tu
OaHlJVgC0OPOxtizrGVIwSaYhou2YMgjdUG9XlQ0/Yl4aGrHQq5yfJPdtJA3gcbAuEc+FYCMD051
zcHhr80XgWr8TDAoG8FuuTVGkZMvx8TlSXnlSJ9QrvNpTqVej91mtJC7eETB+EIXBuPHuLNj4T6y
XBuaCWhxJJDPCYjTmkoQ0HB0NwaA88Nv4ETUMo6h2Jzc76a3ttXyh6pwZJ6uJ1HZzuDg9CSXQuaI
K+YY225hiMfDQufcl4BaekA+n8WUWwKEAr6L1y5PP2Xt99eltjcHDC2ksl/jwi+GGSP4c5NZrYu7
HQpEb71B3x2hSjP65RJPgOKNXPY46iju25PbXj9GqrLP69Bz+mCAYFO9ioI5xw52cF+AV7WGuWGG
vxZPrbZPDHndX5k7J0+Uy7KE/RRkZBNu2FSTXF5TyznRDgoCRFjJ1Vm62oE3/ttkM5p6qd1aeV7y
ZnmJNrnZaJi2WaKhVTwqQzObqLeICoSg7hn+cY5ceWnXocF+WXL/nfSlyfSJdJzBr5cAuaGtS+2h
3SIEU3TO6rHBarQAvwRBTh5qrRr37XZPV/QNdXUxx3aMfm0VyY/zVAqmZvuwySsTEGdSWBEmGZQ0
e5RfM3UODcOWDwsp2vG89otwKaw9AMtGfLN4dMPJyH4KmnQYW7PghBi3wltkA2lUMneSHoJRRJ00
iEt4ECoV2HG7f1RxtzdeFhzy1hvEdwJjT/GEbzuBNKsI3Qf5WulftbwfdtT3VRE1uABIlpKUGDrk
c213M7UuSGTrZckqt+6wJTCT5YsDnsSqHHudZHGhVa6cNzS1+PuTbY79K1CfEqAMqQSTQdNdo8hc
IFR0nnSPVIwzHBsVRsS55b+uTinUbISbkwiSGiEj2oSOFtpMfFZmSHoEQmRKqpt0RSxdcjj7ErLY
MwwiQleoD5nHsyLMPlaIRLUXdZrqm/5Vuv5l4hCa42FzmXRgkUB9ySJoyB1Q+FETA2Vzqc8FlE3S
R0wPDeqQIVtprjU3F6zAA/wxV4NRX7RVwzKKXVgXASjItSiEYcOuaiVa99wQMfLiuCF9bMnmw3Fc
hZLpAQexB+h19J5DI0H5EM03pSKG7+JoBZpv09oxJTAsurtaC3Za0Sbnf46mMiy1eQf0C0QLreMB
o9fFr+BSsUE7piD8rrDTvtdUuj41tXT+CbEg6zgEgnI0bhivkb2En5y6mH/zPFGCNwCg1xe3lbyh
XrPsF8cNOAIfuhAkfYtiqUm8yaLQ4pGGdleMq8AWenq5wVQ66iKlMynq0AUC2w/dVfwk29KUw4da
dI6hs1WtgGRiaHXcsZcZ7R8VKElMaYeeu/+BvP7M17sFMKbdTLIcajDSacC7DmbSF7eEgRLLBsAy
n431HiEyJCRKacpgFi4D27Botic+ojQ+rzXR3DmGITQrZ2XhjxVxCp9ADuhZ8/9/QzMyhjg1iqou
c1JSLwATUwEerTpp0mXDEDGHIErZb+2XumnDy9M3zErI9I+RInTYYjMJJ8YWzd/CJcYG7wWh19TX
lfoBHQjbIUtSLh1jkQsMbZMV42C3HIgEGdmAS3NIuocJfKTwiTKNLyNCszxhSnFS7ldvN7mDscNe
9mc+ysXMO27dsoxUYjvsWQFxLdXhgapYUMLYVlHa/grUPaZPjD/AQtzLG278oOiu8nQdzGtq+bRy
rl9BNQhzZnlFDYm/SXuIyr/wiqDCIetC+HxfaDdNJFgjYJ1AOxYfWdgpWVjBNhnnz8iPj5KT6ojh
OQUB8WOehLWFLfDAWBBm1asYd9wXGehJG9/lMn9OL5yGfN5PRF91Rq6kmOFooBmpPHKkmlQG+xBB
h28xRH+detYGrxHSBKRs9Sj68M9C18B0ngR2VcPIblbz/F6WC464nQSZHond8RKWFkt6Gd0IbA1c
H8CZKV+5YwrH9GBQDaNOsiE85wPM5xaWTFAIwIRLWx31U/N9OSDu6u0boFSxC0LrJCGgvxy/Qpem
pUwA9M2j06SNcKh5czzQOcj6CE1a1JfKJke2zP1vZTK8WIXkLz836JBU5TLFdk+eIcEd3GBI+NAd
xybeeU+x5hXL6poJ6pxrTpChDN05OAt4Aa4l6EE9II5+KvqlIcWHk3mCuP87Jbxoe7+iv4KpRJYk
0WKDVF/cITKVHN914qUuy/SJD/Gy1BEyCI3eYhrKNt2oog/yrGx5uuv17SzbpOdVI4VbBSq93ss6
PoXM/zHNl18Sgwkpi17uxvCYPtHTMAnqzvvPIIopYaWQiO1PBxZJhWhSK6tsC/md5eeagwl9RR8P
mwia2z1yNkOYeGTQt2AblvGgJnSvjRwPVqi3sBQBK+mNtZIvz/35T7PY+ZOrQFO51y+bJoWWZD39
dl0Ui6aXf0n1ze69+nPrLwYVrZ4jyNM1WhZ8XgGWRjF+DMvVSx6qSPQfElBvo01nBQ8TVxpLiGbC
P7EwZnXuUxbo1RdEo7ywxtLRAZ+xwdfxlu/BLgcrVS/R0hKUWt481WQZwH2BZV674JEY6zjs6WDW
QR117zFzKWiHEY8U9qJbJJ9AoeVShZ75M0W/CZn+PrDa8xRnNVUinsIk3MuzmUrhSocaaJDOvPRE
xbeXql/CHAFQPM6FvH5uMuHt+0c5S498yS/VTcX5h4lWmp8Z9JxmI96XoNnhHSclOvingG7/7ReH
mLxlyyNb7qIccIBT2ufUQSDSnSp5fQ0LE9k6QHWg1Gflc3NOBcn3dkxc+rxGCBS/70QpNPrQcibr
5mqt3cTQbJ8wtcsAQgO+GN0YZZQn+1QqfIhYUo0S+AmXgNDyghCVzR6XtavHR/qFvaKtMrzoepwl
hcaeMBAO5pNPGn9874xcff5w4jludkKD2PzhR/PrDctfX0XtqLugjaiS3pYQt/E2OZtzCMD8nneF
Ha8riYnx1qUdr9S3QM5AR1XsYxl9rDm/zntlt41n9M23tK7LFN9KKj/pWbhCPah02sFPXu7oMm9u
lNptk2eajsj05Lv0xhvjnRXc6pEXTIyFrXT3TQMjNpXg03ETzbmkAPan3kNnPJda6fP/2X3y3KZ4
GpYJeBLatlDV2uIC5B4QiapqjwvDShiiEeGM9kOqeJ/051eeM70qJS7/wN1gVvapD065f16bEJdo
Opq4PH/X+pAs3uh2o6LVtr3pmm3IZdJpfFe1o69RuSJSoTd8wMZkE/G6ZX5v7OWGWkWxDmmjafcS
s+iyFCBNLugNKM+qrJZEA2GG2TLgsgkLz/zttN+C1WbKlXbhojAO7sie3U51z7Rf85XsEQhanL4Y
70JI0XcsGoILAVDekMavsP6FUemN6nk0KhF0HElTgX4nsHFy5APmOoOzwwZhwXtKe2QzfwwsadKM
4yRt5NORs7qNhnQI9r1Q2EpLsyy8Hkaoordht39wMxKQxcDn1MkQDawV2XXQJCF3ICEYyeuFS1Bw
ew+ppIjoOJtidFd/Ahi+gc7KN/82EggJFKK+VwDriEsnAymwf0PLErz4vv6XK+ohE3CcBPQFwKg2
5XwTPpm/i3fTIhaua/k12TaWcOt87eNw1YG/TU3jIsMdxw4xA9Uq/35lutH5IxfdyWr+3DkXWsfx
KIpyr5jN5v8/VzDjwWWQShCWxtlyZobgeLjxEBE6Pp/VnFRByPxXcLa5vpxWgeB0lcAvxFaTWkwu
XGHki5wJa2lIyD4M4OvuM8Usk548hmVR6jU0g4iVI7keZufPADKDsBkIrDvVFjmn8+NJrFY858xs
XNhPqs6lt7gnrDzCis+0YuLeXalwyog8vxwJQVG7Jgy9PKGaQO2zFv7f2T1Y71DuJC/ylMWRnKe1
LtC+a/Vjm8pn37zPE8LPMLUAllAExd5gn3jUheEfaO1+epbsj1qiiXpW3eel73Ah0VItdCClAM8l
OeCawpkRkl/UU/ZyVSEfahbu1IgNyb15TzXTNu5I89VOtvEXQGLx1oSCIgsCeZbKvUTxKPXeehLk
5wgO/sCvjOTWKjv7jVZ205kekJnR3dRezQCjYdGGYInY9FOOL01RfO9F3G255QXqugLhbHQM9XvH
9HUes2yzbNhu32pdGOxtXbYaRh47N0RLqgHvL3ZNUIhpCwltL+ZL7HmUnWiZgc1zLU9/v5JRPLvp
CfwM03qPai2z3WBnc1vksdSEoqGZ1wR+tv+MI6lvTbhV3N7MU1K0jjmLpXi5uxRrTSlvM3SglBp7
yaBan2PxVU+iTi6UlQKHWFw6f+tq0OkldtCs3Qm41HORtpguvvJr1ou+MjJAraA9yGoWm7s0dmtf
aKmMBnLir5tQL3wOH2KNRS5E1R/g744lGEadIMeuxUjLE9laIZTwbpR+OwhVdIeHU1qP/YiscBVo
bWwXXIjxZq3APsPevGHjYcVfSSmsbohs5uN6cv43jZjWzV3NAGgWVcnqZD31WqZ6lVn/td03i23I
VuPcS/cCeRyw9JSP7jHLn8saFhrEi52CJ2TTytzSY/Pz8jZ7FtP4OKSKQ9a1uD19woAO3p8Nlbob
AsB7hlYhNMiAQQWQ7UAF6u28iR95jV6yT6nzJJRN+lXUZNcvybWSfJMpPB5dGKc2STl6AMqq4N1K
zBMiivAXHsEHMnDKUmL6Uxz8uwNjA+Emwygxvmyx4hNWidFgRa70cW0b1JwyLpZifZMJmXDMaMlt
8MFHk+Ix41JEGBPrc6dTSElWhzUSuUKxuanTSkURKTLoz/NDfe7PxM+APfvFLyHnVz3zQ2uflrde
AsSTdw446iDlscjuqZJAqGxR0NdTVWAWIwSzbBuG+RsE1rHgfuSa/5AuDa0Gnhc9e/rG+OiERw6Z
82J6SmFVvQo5VRKzWjrzbV5G2qXLumrOSAFb/sHLj2sTl7HiyTw4+2aBnj3d2pQ4PwAu+sZytBIy
79CUBNuyka55T7m+4KWoxGq1I/1ypq1kdpG7zvDANMDU2/2X7iwNoAXZToHYjTSgHm9Y4PB5hlCK
xpkX/wp/1ZoCANewF7bv6W0fxx4xJoDkK7LmT1yjSofEbvquDSfejDLriDM76fGPaZr+Wh0l8P3M
xaSWC1nr6i7rGG81L7Yt1BsVJFB7+wydn8+NY0Be/OCn2BCCsxN8ei4nc73WSqQy3/4j9QKlhpYk
EusLaofnRX8hyqKvf3i84QYV0Op0iKLFR0E4J7E/iiMoBO89PjrgHD3uygT7EjN9bkVhqCLXbC1O
1wER8ciUGP9OBw7muJQ2F5nj8bOK5RhihxIsycShByjGwXVgjsYtqrP9HmReGaNyZjsH/d+7qGAg
X1XdFOg/me3haynAngf3hCgPH5DN4fqlkVdkYV+zRHcX8aT/g5Coi+dcQdXdJ5ugmLNMui+gZOoS
4F5UCOF3TyChvKHVMgcNZmbooLHO+grCud3oHzGNAdoNdKFRvI6A8FWR81TIYXNLp1vVJavEpVBW
J8i7GIaJOjVzC2N5hkOjs56hZN4YmqbxCBFJ5pG5QpwnZkdhb2E5niz9DP35r/WfADFrnzdzRBcp
D9ePnT9EzNmSjFSkm8UHXonyau1eWPZ4ojhsP2qJ2D1unKGKpp2p4rkoKRWXzUck6/TaCuybCW/r
kyZlsl2yq0Ade/L7MSMiO22his91haAvM8baYEgGbI3W05sShBkvy1AYbC17Ktr2gI324/7Gwj3a
Jbijkpsqv2oX22+Vca+zVptEYqLVUOqtdNzKkJwbrWRvOTXqY53Y9y8XE/QuCRx0fXUE4JTTx9KK
VRB928OeG6/8crlgYDmdx6c0akyPBJUWUPqnlQMD2OREt/Zuj0gpKrZA0H4i/J0DRCnzQzHip72/
+kshX8QuWMFABJxLC4/JldYGCAQvsYL9yQZZ64wNmP7eb55JxNyZCLIoK2Ar+oRZyfNigHgWMAl1
dULYNkxBNEj2cZA6ab1cfV7R3fUexri9CEhiBgZK+1r8eGygsFLiNn2ynuefq3OUJhh/jDx3MUDy
LQU+nIZ59382Og88Q8S1tCCqj+Xf2GkK8pQO5sgADvudEg7R1tzDWtnCCjbVVuUfervS9iQoGWGA
gEBIiJbbRn+qmreADs8WMQ6HaR7vHhzpcnBkKWdPHllfSvdiAanNLfgage6ZzSCpi1kSDDQIslua
uoACr72tL85Gs2Q1WQZGNQiMeaIo+RqEYUPiLfjuv8X/T+vGRMtISmBMq91x5Q24li1dXy3ZJNk+
Rxp2/BvCmjr24K8MqzTU5SPw2onRDfsO+iZb+gMXvHqsBmtCNfYI0oRD1p6IFRKoFu+zCJ8CDR5S
puprlcnb982O2cfVLjMCSibfx5CAvssWYulEzgam8fm7Hk4WAroIx6RmyiYGMkhAI/5TTEbM9hrm
rOo+nvJcFoS4jJt9+fHLqT3yYu0TCLO3q/tUPfeqIlpO5kgyNeVoIi6OHfpgHOyC+vt0icxEQKW8
QbFvX1JxkjvGoPuyxhsq4uGXblwj8rKsZZPvJ/F6+/CJQpX8FCF547B00QpvWnCIp7cytPAu6foC
7dPFgqH5Qd5SEJQbcSYiInmgSGcOyItrF48LGgHNsGCKC3GjwJ3kJY0f12uZEWal4SO0vPL19aGG
6aZDyNa7GtuE2GApcs2c/ipJw3MGK9q8L71CPLWzwLOxCR6MEj9uwWaXH9lEiUT7tDf4+QWs2OKC
yEd/IGZlF+No15etD0XYhbRAO98eY2vvA64LRXa11Te8wb/yi90Xybg9sQXobVUCf+qbZ0XyEweR
tQNHB38f3sPU0pmzkpezrMLQerNMEu+eRFwpG6OCAySKziTaM9KY3q+gzEdWQItU2S74BTKzB66G
iWkwlaz84sTIYJu/zhO5+Efz3aKl03aP7YcxQ04PFh/FOGoj5Vn5fQNwHvg8M/QdEPKXADKtOnQz
7Dk/vclrNcNWIfOPUV6yPJZ1Bm3vlJY7y+daLMOKCYWQfW1fMkq1fzVT2WGwZ6SkYOT6D5I9DyEQ
srlS2X3LMdmE+fva6Jux2SEuR2i9VoM3yfnLZ7OHy0cz4RI0/BtU9kmFQGqvSea3Hkkqe2+RsAyU
hl1pv2EA+kcEJqOPDihu6DkFI+aWClhJtIR3oXQ2fGhAsYbyKxb6U+cGWKzzmdlFtAupLKvLtKdA
0MITsXvEDOZc4Q83m2AjXI0pMluGIfDtGd3Kq0GMovXXHw08ghrJyRXyTu8nm3czLeqBT80H6aow
uYou8Ash+Dbp/wD6ERbOhweghiR+lGDIlUGGdegCMEPAA26X8nBVfdavegbC25yppr2diWfVUxJL
uJRs/FZUwrPC639UfADJvQHMFvdX8b4o5IRAjbN8gPQan2elrFUI2Tckks8/HhslktAWyYCOQAEX
b5jneNd5D/vDatfJkhcga07pNEqo/HURYIieCs6p0zz5yNcq2HML0dlnlXRrBj/+yLf8yv8T4zLF
P38V5fGviQoGkfdrLAnT/D1JwSKtGVL8sAf0Z1j7418yn3uqCV3jk2gv0fOokKrovphtlPyuzVoY
fh+zHTMxLOF5Y10y9rPp8TcRMmQv/bnEZzlu8d2IpPhI15W5X3EkCvfbGkUw3guK55g5FCeb51OD
RF7E+6HgkoEyhgeRaxhCeaqy95WKlLd7HZa8XY3CtuFoGqhvtntOCN1n9OrdMA79xpHE92dYC03D
GU91Au/44NIu5q3A1UUoX+rHc9ycKRkwacQTSzghJMULWtk3VydkjoeROHTMifnT45XVsj65m/HI
GqS8k8gnQeaYvXX8VktqiJDMWCP26G6SAgZ76nhNrpY7X+C4MfTJSsbbJfxMp90dn82K+ecM/8ki
Ww/9tvuVeL3srzVMCNMKZRtws8EtV/Jk5BbZqXl6nuDfD8+Mv2dn6HozJDny/BTcDnz5tkWrS98b
VpQ9IwInoy7E8+X9FQn96n3M/FS/c+E5o+RqsYWE5EpajSP3X2E/i7wj4ZAVBbodzoePROAe6tSd
cw6Oj7kbqfWVnVnhMQkzo+yjH4Xvmssecr9DMuh1DK1UXmtid1Kk25CnM5o9EPI8rIRXVkWTvdOy
OANku1EGZqBZNmZpTRhE8C0mVu1VNhk9dM8fkTPz/8/w5pylhO221jqJMvFowjpuUseDy3RTjqUF
q+Vb7zTFCNqh2q8b/8ySRgf7lIrLqRsugqvA6FcKY4EXZyosAEjYzOSLjcUe2v41Ea5qcuttZ2vW
tFN1d4mTn3vBOUC7OgMmjTciEk4Z+CBYz+Cnx5AgdfTI9E+ZJFK69sUs9reAd+ahMlFFBoZulUsI
HpbaTOP4oIglteUAZjhu0RoGxGkrN4EKhOTJcFvmnwb8qfzjnbjGoNXVF/OG0qxhESwTJGLCiEXN
2ub1Avy4nMESgtnTYL7XsIAAC6beJyk7CmO9wk9UJfaBZipzXOFYl8Cb+G15xnM1Flpqlw4B2ZXl
95FfbUb5eNYJ+iFV7qeI9xiCJhYuimJ5TwlppfbjcAFuqgSWtbPa6CpYFqd3ZUn7HfrVBnStabBC
NbSF27bYz3rx/rEEgaemz/TPLK088KPF2oSSVdFlbEDWqjXFMc7OirYm3FUITQcoLvvtq969w840
IoQDouAkzrhyK4vV7nDwj+42TMXqgf+ed5OTooDW/bmyIeirtLxlRbalz+k78MMUGjAWS0ZG1oRJ
RXGKOXzWdmZ3IhnYxCxbNknsJCLRm+TJEBOlgDN8Wks7OqIeH89pV+/AhlFjVkz2AH7jedQG3QHA
xWjNEWGjXTeu9AId8M5omNxjGmoGC21teN+0uEYzNTR+CWIiUOjN2RhE3X0H2IrVuD9Tg9V05Lox
ZwGjdh9LAHhQbAb2XSuGoGMGjWgWjXE1mvTYnvVDGWJBvInGmLI+AuFWj0+jZOL8a7W57kI1ENrF
b4wn/NYRS+GrWoRR2v621kBkZJMvmziV1fWmqlxb+04KQbYwG/BSamtWAiMj+LStgR1Lv8CGqRUU
n8D7VrFoCOZI1WYf+0Ji25WNBZtPlzdg8GI927Wo+RTcw9bSMmhn9bKGL9ttpal+T1+I8ffNFx2J
E3+T3kitE8T4cTv30Y4TXww7Kzu8AYrMOrey5FAYRSKdtM2Pfpx4tKIukYaXebUZ3hB+6MguloxL
7Zl3YVkLYhFGeuRUZggsObwJeRN7i+bm+M+9Lk7hbhdf0xGR3n+YfbiKBhDJWLH84Ozt9mQb5qKM
gBWjxha0uvFLPLr3KWVrerscn1hpXBn3NNuBPf20vOCfAIiNh4pd6vW1nunJfg6nErW1RIJgtV33
yOlwzwWsb1NtTWk0eMRvOw7GbJVAIA5a/yfLI9+HQYrlr+hVs5LhRJ+8QXHAuOoW9Ju1vTpXKE0v
P8aPoPPPu3Bdwg04m3hZj/rkHrIdX1jYqFEZFt3EAnUL6d18Xi35xPzrRRSuLRcasLPJnhJuPbwt
gOkQAk8GeAfB3NTSTKRsYbC8acfgPcjegJ34CyUZzRbiMYyyqSVxVDprduwGkbEOiDAZCyhbiN4c
M+MHt4cxrNAWJdTlxTq1hFyss8hfXFW7yUbFF2hR65TyS5U5RU4QnxM2qxw7ZuLw58SBnLbsBGgc
E7B+ewsdIMqFcSYdChp6D9jW/tSxP9T4NnxF8+F7KY2CZG5p3sFMK/lq2IFfuP7P8GPe6mYQ+gJ1
Qse9mFKf/25MavOZk2i37/uXUmqJyi+z9F7QRyhRgIGpxUZtPFLdcCWrOip5Y8/KZYcfk5yfpaT9
ADT5gjsjwtiQpO6hRm1zU8ZqwchSZ+yznBIypEBaanKwjksVCEW56rdrPAzCsSVfxoy/5wLjkP0w
0MQUe1mpQXrFkj8QLFvkYhorWkUZOQzS8P19FNu+qKM7wPl+XwpbbQGmOJ9vrzEsKlqJU6cnVvEh
BO/PS5XpTPpZyEcQ89k7kmkuYY8zCuwb2Oe/78FBM5CAAVHmwnPG9lgObt2HI6qvslhw7w3QTMeZ
7xo6lnNya2s2UGb43wFfHpVlyuDIuX5A6J4b8osuiPtIuzsT4RenPNLP05jMEzuAuHlMdLJN5vdb
z3mnCFpS1B04ry66Ak75+hUoFN2QRdBfh4mmxhsixipHzB6jRBURKKZ/MRzcisvohtX0ND/XyxD/
26JiHUsagBEhJxSdyFz3mWDIDc7PWZzJBMYVM5rorVplFkQgQAlgSnfoYfAUMkn3iNeA+tvDlZT2
d4fUZlQjPTLhPMRdeiJeDuu2JOlJsO6FDdgMKaK/LWky9Cd630uUqxUa+LHJh8oS+A63PckCC8DE
6nY6937Mu1nMcxX0yVfogINWfVEbW+zagfmu8bF47AaHuzoJIoEt/UZ/xcgSyj4KdQ9YYIoSpTru
pWqCR1Gk4PdBmHt9DTlSieAyHmOoArpCIbRnNUCZp5w20gd8hoD03GWkJsFlvnJdsQNEMgMdb4k8
vOqgAGGdSINFQyeA3IfdNQXibLUb6XRlH1gm1NeF4E5N0nE1j5NDMLDdoEkozK0ez7HeqnUKaoX9
9iKb4FBT7ONFTAp6+HeTYy0Pg4ua5htj6uU0gVu/OkXy0f5xmMty2k4tohaV/1zeOZhgCPEMVZRf
IPBFNWVSeK6pp5GpaL2fpmTbUWVWqNh7ZRYXRR/VZRVW8c2JxP3Ugn5RFQdRfBPX/ue2SQI87lOt
WgWSzJkur59TfJYAI9l+odXg1cXsoHCNcf+Dxu6ysDsW6BeUDEEt/0cTPYcSRcYzitiuGLEXSYIg
OicD3Jp1gvZEqFAo5aGBVtt1LKI5E+3p60eC0YCFw87g2+uUhizd0u1UjRcZ3BITi40pLq00ivC6
DDbFuRTjzLG21UbPN1Ixl4+ZJIWPaklluZGgtSckb4OEzFiScf1ky4mIGN5tR2PHMFhb76yG9rCb
Iq+lMvt4Pn8GfLo0VlfXoDsHPyLO0zzA3hP99FAJYGGSUWB1+5eWiIfgaim/KdeCRwI/T/odLRLF
3VrxUGUHHkyaoJ22hggCfQw6eAJEz8GJI6C05VwvOe5nXGfbOj7TiPy06GWdbSOjdGjxwcq3RF2s
E6zozLAnFZl38mUZY5nOMkqsjLm6ayPx6N6Wjr5V/IisaG1RQayUcX2JykuDwoJQ8NbY+Yn7wc6x
8Evz2nVqfupKZZcNez7mmjmeEL12pArgI+K7kkeeiSlWLWq2QMIsXPRlgc7IdMp+kunR/biOMXNR
DK1ApFRPQuO8tcycetDbAXPn4lTJpZZzh9l3qB/Fd8LUFKgKpkUSKPuqMyEZq1BWasaUJJdTGLjB
/4cB94DdXNb3lGEDXoEetuM0xIo24UBGkQx4FgWhGC/6WbV3wGiIB0V6ZGkEvRMCT8bZRm3O8KfN
tje2eAoAVk5GXgp9KFrr62XwkEKpo/Xs8qT93QDPBPt5dtMEX+UV1v005j2DcOvJ5vMmgtacl/Kk
osz2O9O5qCsdrxlhaW5LBG3G6lUj+bEo575irM9RbPi92mWCvoYe+ahq1PMffrkhyBoE0IF84DfD
MqwKtMzM8LK7GGh2SEHfncTyHUYhDeiaUx/eY74LozMXW+hn6/+xwmZbGC1vVHE05LtCRw2qEU9B
BDzsMo3mXrKhjhI3Rw70RMyovhQPCDlMphvVgHWbwkFa3xdIic5i9fQH/4ENX4Ex1ynHBSCe/qKj
/qvMn/GZmCWxFiP2c1t0/M6QcJvwObJ6ubOhciyZahUzm5QJF2Ya81b5G/xDGPAXeypZRHOAB2xX
4v0NfahKWvAyTUgMIzq2I2NCfuX9voKVB44eqTb1ak4X6dloFfnJSGeUX6XWlWTvXJd7wnZnQEkn
w3tHT4/jahYmljSQ2skfgm18nLEoT6lBkI0l26SJV2vjjuyoeXjAN6tcXdPBQtRJgj9VA5bZSCbJ
uqVJnQJ3P/IWhKVG26N3uertXlSVRe0PrG5zQNARCffLU6vxOQ5naxrneq7soxx1Vb5nbFlAbQpp
KJndR/Il6fPn5fSnM7we9jGtjYzkQZJXBZf2SDWmjcNArWVbF9pORoR0E28NMRO7QJ0ubCrs4A4w
OGdk55rH5CiCyxJYNZ7bRzXj+Ay/If/yguOKlfD7g67/lSzMZrX8qT9XVWDwTN4DK4KchcbaCy5+
SCqS7XaUYrOgd0I/NsMDOxBYfm5CpeMn38itJ9yWRfGbYhW7CMlgPMEMYCJb5VEOs8R9gOBJJdwL
cWLvsXhI+zheVoyoeNZS0kMz4mKCiqmLs9Br6YU/Sk14PPPZXe9oZBgRGoMKVpuLkXA4j2RnXTsf
RvAIGqejIUvTrNYWM7gHS1EwQEVXL/G2z8afex7Lt+6UzA5c5MuEziuIBykRhOZnO+O2/iXB0/Jk
hJcL8ma9OY6tsEKj4Xweuk8+XArKlQ5popFLrqjIqbZDKWOxhgDScpVEu7B59pqSqQD5NJhS3bNd
NiAKJ3pKNlmh9qNONvyEDMTvN/WnRCWQlLyz9Z+FwnqhEalCMiQ0Xt2xeX6mPAVyocf+PE8jiMkl
V5/sBGcaygFf9sMR1De82G6r2uJQ2wmeL6i+wDnrBmk3fYHG+Gvt/YiKR9RjaFn++SwEUyzc9j4C
4qHv52WVC9sQcn5uXmvgMOQ0CPyOiwYftf1KCKxXbO+ZKcgXMs++OacpcpsS0uwRS+cxDkXzTUDB
wxpFwohnKLPWcMAZrU8v2jPd1dVW2vKUgolQFuARsOASOeTkZx187Hte1qCYxSqPEK0McDWgxDrl
oquxM/67tL4qNlhLoXerQl8iVnkMSBCetXxyRKx+L1jq+JvjDUtNXN3+jMSrBxmGfNT3vJ3lLJiV
MyjZCqRXDAC4rwzklGUiHEKZV6tm7viqJ1pMlFJ54fExQnjfHRqor0ebooS/fmaJs842u9/Iny1v
Ttz8fOlbNpKXN5gl2ad+DFypRRrivIvldgD6Dk+/Eup/CzZroifMJI+oMmHRFa8f4UcUvMy1ozps
Uxa1C6uwZ1k/USofN+ns2SdnQ/tWlcsdrbAiLw/dOYumGcYTfn55f6t1UhlNhvq5paNv+nvwvTZ8
kqu0SRU9OHDZUIWb4tsb2fRpnCcuYp3CvJOWm5GwGyjZ75cYNOk3RaOQMWxSEuk3eId4vCXKuFh7
V2rFFG7jvl7JsR50SpJyyrq22rFGvSjvlK6j0wFW6TU4DLLdis7vZNyaIR49dsKELpptiOhVaMNC
KdkZC/9Om4WDsVImsMwxkWyP8lzAFLh0rdsjq9NwWwn+4BapUr8dzdcDkGR4AYnuMnT9Pfxjq6bI
Y96+fs+yrIZ94i9yEW4QvenyrtmOgp+LsUK5P8aL37hV7Iw5lnXoRZMYJ7dhwo66YBmME5tXCWY5
1fnj6KzbM30mGQs5nSSi5jaYJpcL8DnDuD2v44ztL5PhSfPEQXtD0weyQr/vdza+D5b4Q2NlE+rf
Wno+iQHiOwLNvGRuxJnTaqkwQWp8jpPuome/yK/O0tzItWQnQG3YoFe6fod6HMyDz5qYZQfw+lRN
ir0tVmEOEmz5oBDyvvO8OGtIJRJumR9AhIWQSCu/atxDzStRTOKOGkWRb6SvhUS3tYIb9+aJszba
KXjhs60jrPzrGbLWCkmecDeFeZPkYVaSaC5lUvRP8KhIBYAEpXVWiCEOAse2no3VZrETo4rD0cQW
szUHpwT4g0Lm5jWCxbjpO14ETw3J0OjTl/YtstlT/3UsUuT7oHvnNnTI6yspUpnNFyRz4hKxMA2K
pnnfDVKKxEM2AiNYVHuq9qQ+yuwnMJXekd5FqvHx9LYMdEyNdP1YEESDWjkG3HH9U2I7I9o+ISJn
QoyT6Vuxo33sM426UwQ1MP3F3VkLTsbUp9w6iWpJTjifRc7Wbh8finmF16YKj8ch/H1pIWrO5s+I
lknuGxJC6M2RvgU1l4NPYGXXVmtdE7X6dmbjhgfX6NjCOFbnN+bDPp2iDB9JGq1cYy806d+obsLY
PDWanFBm1+20wvqTqc/RmbvLXQQJQtWJAZePNthP/WUQE5sdhHjj24wOCiEDO00qtCbIXr0sZetG
LLkSOuzNU+84gRajLEjcfS+jyQ4dvMbr1q+21/1iR1V/cDL7tk7kATkWgkkI1EFAFqVrngBLNusN
LG2X0jJbZtXGNq9txww0Ko7fep4EHgmMS64Fx2fZDHcNy5BHe7RiFzJBb/X6Fe9HOCBNZ+93pbSa
r/RnmhnMRpHA0gbQJJwV2bMHdVtJ1ZbSkcyDPPnvdrvQyGgyziQqd8/hUjhqeB7vnsGLJGKeVNc+
eN6lZ0Y4zjyE08uDrY99nugw6toNAPVEbag4Auw1Id6Qnal0hfvv2HDXfJewpiBcHiVqtQ52ykXz
E/FAR6Zzr3Yua9RMAUDvIH+j1FnfiEW2tSPplL7huj0vBPcS5is8Hm1asaZMg0uXsfTKVTaEmQUH
AQYNQJzoNTJEPSXxBz/m+SrRIYjcOQwCldlc1kQc1Phez4MCv8WBEC+Mo8ZUjADaSvRR+fxnLC3h
+0075BSgQZIsOBEfLUcoQ0OcQhm//MOSEqdIWHy+2uvK4GtEMYZDgJzOn5GVLFyyoBPXdeaUZrXq
tmc+NscljkqAQFrDkbgGq2BMBfZENQJ4Ik1LwkoPAJIEnbLKZXzhKsCA+JALPGC5uIg8lfs2X0F3
Y0X1zhrjUJbBQIiWtLWosX/fv5K9jjupbRHjOOiqIsAj6AZ94r2cXsFQeOlYqpcwHppwqXqLO2Y7
ZWD3dtKSAx0oz5A4N3uGY2qEwV8kkShe3GHfAivxIqewSsoJuG8Tz+4HXFFQD5cSwwN8lp+e6xdF
QW+hXCp4AnaC8nnu1rvyA5uLzpr0Ix3t1UDE+/kHmmfXCYINEsqEuw6KofWWu9zUDltMUjJ4DpXq
PW3VzStBNL7u/k5H/X32nlX87UwrMvdVilna4Lqtz8A8H6jPz2UedCAIfTtFJJwDffQ1wA8Ttd1Q
PyNE/f/9Se7eAyEhe6PrAFQtiTHA6AtEyS7o61RXY4QW6SkLH/ffAe03k6o9tvAQi2gAIfUMkdB8
UPiTAqMnXKnyu9ChDkbZsBUFb8zX1H/3nHvNBnSL7MdNFE4aWUaBnoJ3zoDWiIxSwAwybID6A60X
D1yMM11Ht6Wt28jVMMveHLKWLePSmg2i1XQ5LG/PA9nfvmba1ZC/5tEOTON84v9LJY6KBj1DZVOO
Rlc6DM4Kca2Z+wYVH9rf64LTSvUlvmPA47byxFFvr1YpMqAvcqjt3Dly4hExOde66OBt++YJafHw
rrtt2ZDv6yZp+jZCR6IKDPsMNi3aw+hZrRSp1AcabqmJlmFh3rR+RevhQO1817ayJZgcE3YXVNoy
bjPuAWiqKOnnPL8CZoSWunSzIanL2pQSXAxEZkYgs0axdFouZZtbOM9bbsNdqgMAUmJ4LD40J4XH
rVhARBaNRi/7zZHyidI/TOgbX+8go74eojRfU/gHqBWlIfqeou9VKJ0nfAHJQ9HJtkKGhNQ3MgUd
5mTHlb/8q3DFjzGPeBXcxvUhI0hWw54T+nwvUi/BjvZHriQaMKziYSM2WpHpGfbevDCNWFXq1w4K
sD3DqGyovxTj1aJd++PGfjwl9Uii9YNEijfqwwp0Jn2YN4VpdU+1GxT0bZJX9jH8qEnbvGJgnhXT
f6siowltiE2f7PTh1w5AcitMSoUHvmcD208tSpLKtuyR9AS8CLD9dWbg+VRd/FF9w1j+uAPVPAKB
9RDtcAy1szF+XeCxPWnYIPQLkdZgOIfm6LcZrztTccJnhbu4qbBGagJsOUWEUQsY4iIiowd2wNvT
DWGTl6opu2R5KSsmxHAIQrNkEwdkn4XH/M8aLn43uQT3ePxmRJxZ+qSZV4NJfadye8TYrzjPnBjr
/1NgySr0ixtVk6dLMKzSDuumYpQBPcqQDXXTZ7ptZqKRsRi/AmFnaqGSAPpuMF076GJhs9QjV1cx
gCCPI40gBvHiWizqAKt8hULiW8tv/dH+paLouo9M6iX9immWj8N/AF0vo3Wxq0PehRhJF6mcy9iq
pkJ8y1h09/z3NrwbpSZtKzX2uHMxmCDyPYfAxOXWXF19yI/zPWRww5w8uUluZiBAtq7HVbEOR3iE
oJaq5IkSzNB2PM1KnV407NBXynCTKmEgzNQaZ+4hUJ06dhjeH5Tj3H+LrCUcporZJ0sOjPAjgJDO
/tTRmvgFO6lyd3/32b6yw9UCBAkLP2HhM4RX6IuK0Rwg7huVntR6jwCx/pOTQT62hj4S7djOJ6rr
2bKBGRtvdBhNSVN4gr8l9uwDfj67/P2MPmlSori2yX/xxo8hQg7aKKOBNmax0Q51eLaFr0hDui5+
r/r6mL/rZUSXzILB0tyqqnS6bkTjjvtigSoFNo3H3ldFjMDXc8RHNN4GSavz13oRrtV6946+dj87
GKYQ9jrVuhFUKGLZNLNYZXYx8XatQD+5MVP/THc83dcCEzD5PIiTG3N/imFZrs+4dZ888EAQy9K2
Segv/uLEFe2GQTBIrq+fGus3unjY2W27HjyHhFEhP9oVREjDYZy3GDvzr9R/SQ3/Z6fvijn1nDfb
oJpfKF3VVINzWOfnugmpJYqQjzc8LUJpxQIMeyZ1iQnWSCAwHd/C38rEnXFzlroLqq2Mka9wGiO/
nOwA5kUdx/1/uectQsxTqbVKta7wm03Mt0KqBL/FpDvzt82cndh3GNxinImLuep2d1nE1zHu/PmB
geP3QNk1wjvxq+P/sKVNYlQlD24jTcchdRKtDNj/uUJ3vsrv0kMgAdVcIbzR1Yqn6QxTHqvjDqYo
lxhGDY87wQNZOKC5gmqgijPq7Cf9aHT/HJ+ytKPkoyQV7UFAPA9+DDsSd2jC7fhN9mWbwIGT9Rmg
usEeTttT+iWFfQei0GprDAvVtsrWUmFl3DImCQiMXDfmHCQCHkgC7WDJ8X/7apv3A5uNouQLn1tn
KNytyGtSRoFF1lRcFxZ8nMMDQ7Zw7w4xw6N88TiBSxkHrIKXetiZk8iDtMktA1OVgPd6p4ursuqg
lunKYuaOSk5cU1vE2/PjWsZbVFa02yJCjBYbeiaBwMwwnb925LnZQj83ilwf4E988pKlG0ntYkpF
whUPlGvGDwOAuDecFZZVWjqkGI/Ld5tYvHG4QS8vOBVQ3lMJhyawpM5UkQMg5MWWlmZWwwORKWzP
oIqCut3Zot99COXuUyPris1VnbGA3Oj9K5WxTuAIanvzWAekCU6TSt4yE+W1AX/rLB7bBFYNZQRz
pQOCrdlr3eP8dOsmvyP/XtgFXI+fv6lBtKNxSLg9NJBFm70ory0HYJ2GNHBUlVzfgDUT7814utbK
3IdUZnkgv5phgDI+xq7TYzPZKnZJP89DP2yYyx2656omUah5g1+MtS6dqHq92O7NWS5AMBUtSciN
gFXGCZ4VAryD+KDye1taRZGmVUCkuX62oGpZEBdYP6BJGcDXs4S0Qh+9affWCV55ZllvIzzlHOJ6
7+Xxs+sHlMqcx3Ht+JpbrphrIiO78rB7jp9KjT14S7qiHqjwo8GLMOHmbn4fvtmnzNHlHpUSuoaa
NdzFViQZzWLEcfiKt6yjEKBCHiGVzsb3/6S6wO20JjUNofhQ1MQiRO7NTj8pti9GPuJ258bmMKlC
/gkMkyczYitWRQVwHy4WAWxmxfX7Q+TLPOzqqA+xcaBDCMncq/v8sheGPzNWeX/TH2vEEZcIN0FU
V+wn8DAfoOQJTS4tmw8nBB61s/NK9GskNzb4s0kUWNfXFumTvQkLQ7Kqf65ewWugXwVZC3uoxEX3
NvbhhDR09PshM+F5QjM9h3h0LZYNlBPH9HcsfNmT8WsOmhzU6GX6q3Zg52xBi0I44h64Rd+jDMku
5vl/MBXD6LbuEKvZD53wft8r53SRzAVJkOJuhaKa5RaFYWiDfP+3UW9aEnpPn4NWo2BLurVBeUrv
xVXWXqHhU5pkZW6fIOjF46jxSezhc2S/lCqNp389Eznugt/fepA9wZvGQFWilPFY1iyvBbDJxy0F
ZSJ/pxxdwRlcGzOsVOPRMFKU5hyCxPg3flhfwb/umzJPK0SvckeQWqN3lz0/f5iUrdU7kkviScg3
c0c4V435OvTkBd69nI3ctPoGydqRWMswyu5ov0TYleArsmmarIdpkq2uIV1fJYCT1j2VDumUZN1/
fY+4cV4rD5m4md5nTn0CCgEBIRZdFkSzalSRcm4Bif2VpfPRasnHYq7W98sNwu0Q1X7y3v51I37h
WIorjUtFrKnXaQXN1qoIL1z5x0+u/s52DvIwjDI+MA67lghBKvoe/Fa8ZqQtC6mW2PYKUJDGjffS
WITb68MgUTBMH7bX6ypI24Q4ZGS+F/NjeVRkYz01h1vAdtb8kj7nxOnH++LNP9atHGAEC0S6je27
SCKvaRjVvghAFkUr62YO19OkuArkXDEAGE/BEkhYNsFJww8PFBpgt8jk40jUuQHK92L3dtT/tqtL
/c9snf5OKSJH0qoJ1FqcjAKED2mN6WvCgTs/Dk9e+F56V371zQ6sds3t3Id9UiFtvBX1S/aA8mcM
2iG4TzQEIZpS5qVJSnFVx5OHXIuufN9R4NQQCoK3ntlnwcFGLJp+eBXdB/S+6oH/giABwVGp1woy
p5eFx7PEoMiDxX/IUrFec9PLqpnRV7vZ0okkUoE8KV7vwgDwyHbkUaGKnIu0s4RxJuASuT1Bj1XC
jDm3z2gTk63GyBQJp92N6GgVmNVBsyA/9Mm+mgKlfeK+a0a3Qbf05mQtLZTbnM8v8iIsaJARyn2J
asgYBtQC8y6QBQXhoVvDe67OilPIDp8ixRpQBQpTaN/FUqPOuqwWKknTrq9v7vPHjJs2n5JbARRq
sZqtdWA2QygfFvQVB5JrlXyuo6VC6ucEpt+yExgcTn1vFV548CVGu0VX89PsKYN+Z2elDJ1prh4j
lEnDaQlXejQmTHBLgLSw+Df+aP1XiuZE3NBD/NEXXDMzuerAt4eU+DH+W/4AXPsAODeU21ueQBxi
rtoIGoHvxT4MKFDhoQ8XdBbXGiEq8HmrNpu8EEVGnQ+IZ5sPmM1IgeOxFKde9ufIy18T+FjJo54G
LjdPHJHgp+rA2ZYzez1WYYaDHMW0JWQMWDjCOO2rCstlU6mq8JCLxheiSJL32BPFsCHwfxT6908G
9coFAS2/t78mDt2Ea8836TAlcoYIQGI395enN7xfAq+mUfB07+wuIgXqCFHmyHahcPBpKK9LYOVb
dyFA1MvTiTs++GODb3s9znNaZEbp8z10I3Yiil6IGRMX6pl4ekS2ShfNgHidK8/FEyt9yOXFJFQA
xRMq07htTEBQQ+HzSFe6SxUgOQcWqVSHNcTPKL8VlSrh93hCXrVVji8RhPR8KSRlBFVatWNVroWe
FoM1DMWskoSmLFjOSlVJzLymZ8UI/BbX5/WCXF/sdkoQTwWoTnWnjcmNKR6jqXGLPuQFoJ6LR7at
zuwgFVf0T7iuyXSRSNUfZmxv45TfrMlaZtCXxPX6YThZO6FcBcslqammEsi95url6+bsj4r72IIM
gnCp6ZMbJwOZhPd3EeLXqCnCx/3lNqVuyIhjUZv2pyHsFLwtsqq7Q/eRh4hVLAEGCAKgLN63pwUZ
VjqzPjaXhjy0WMTP6KkoMszeNk137wjqqd1g74DlSNrl8amrtE+afI3L7J1ZgWwnEQ8feVkQuPjS
UeBzC9vlI/I0c7ZaQGW9uckQWgtwmkcxAmXaTDSqG/iJHBqS4yAdzTJ1A3cxWemSsJyLEr7OkTpi
Ugf1LGhoNC0iKcHAvNQJOUxGZA2+M1UiFy9kmWRY6OfrP8PvMWiiqkQdiqbHqEq4KqOWsIy8/EOR
RcIiagIGUtSakI2Ny51wfznT+mb9a9os3G5uSLkm0OjJTCD5Tul3fma06mrugHvcEez00SlZRhrW
RvJqjeimobPRjcp1MJGbTGovLclDlHVWeWm1+4e/Ofbbw1mVJIYlcOWzkOwxhKvzJ9Os84fkBkIR
RSQMumQkNCFl9n8iMCSv0OpeS2FT+kg4rsHlR/SBPa1og+Mwy4O2dxlUu/RdspyKFr0obLG9Q89b
nEgJLGbeoQAzTyesg+DlCLfDw4ofoOjs7y3Ho3MYj6glbByLgR1kNPd2P95J405NQ2jNDWHsQQ5H
NUzE/ct9M5B++9ex8hZhfh0YMIo6is/7RnLujUA0zTWdd1U692Jgd5cYXK3l3uDR+8HM/+Ldzuw/
npCt2s8CmieB6xy2PA8SdYGT6EstRzoN8iE8jvx3iaNxZ2yhvy2/tBxzP/2WvsZigL29R58eZBeD
X1jgMD5tUbtdhnyrgc25MgIjsEe8FLupOp7mDpX4pjRR44CWy4r78gzh1+r8q3pdUW4k9G4RTv1j
yWSWUrkHYjPOgBKPSRyX23xD0ciS67aNpGcAnNnbNoso5rxtvSf+hulsepANlsN87W7GNNDOwfBR
UpJTU3JgF67o3GFzgzRug+5aqfaJkPYHc90fZ67uj6LzK7+wR2xlK7JCZAbeW4q28aKIWanQJpt5
6JD8mI5e866ECj2lHS6DgkZYNi2gcL8S67JyzdMifrjCiasBNCwrT0hMGmFPiEtVg+XCui4UaajC
bIYudo1oH5sWVN6J7uuaWCosde7CSj+0VNANZSZR+ivifTuEXmE/ajqDgPfdy8E5VqcOI/QSyKdq
4HmykQWBM20NmkJ/mPU46vjpnNQQViUwcuGU6jMNFeJ9F2w2bW09+gZgQ/kJ0XRDzuQcpNgjn7Vy
mn5mqZ/XRCOVNTaO2yCSsAQMp+LOcLfpCJOlpIFv10p5tvVptJ81t50j8YGHdS32EZTNMZS0Ash0
4N+zxQEfMnkEtFAHrndbpuCTuUPorY3Jw16+iKOfc+Na/Fm41l+77K8uc1Hlsg2PfFVpsyvGBlgk
QIl6CUHmOk8G5IYDVODhA8zdqROsjkkjM3PqM5qdMZMyDXTOa/ItD793OPk+R6aI5QiMzIKBSVbr
eo1Xwv9KfXo/3LwE7FHQa6NL+Tp5KrX+9TH8UYkonpNj7Lsf9LMFxQ9yitPk8RqO2oitpmBH65EC
x8ybxQfYIaInvueQL6eQsjSrxmRrIyKJMciX4qDHB6lVaLKLLDkoYNa1RadQNE7++YTZZfqae2JP
aJ5o6mTpDonO87mc777EPBfJ3n59xnRh9unc3g7kd+TMRKJxtc2owhdsc2LxrHBbnyi6SAaUXXKN
FBQz0qAAZe8x0XWubqier5NMk7aNcwSIuom3l1r70QOLPYprcTZUMM5v+qlC/NOgg6gpAnuxNTwK
0Tm4XgtUV+JPUhpDYO+R+PK3n4RRdAz0Q35RU3JAGEkaBMnP/7V8D3wElxZ2KPhKz/S3G0gEtndy
UE8UE8zKeQ2DND6iT1kquC7Tb+IqKZOQQkyq1fSTfn4mfJvY4fmFvyLt3vcXmprnkZYs33qtsmmG
t4ADwmTJv8NqJ1QXtfuzRVgkH7wAQnxKY8UaM62bzrZUe/Hxkq0CsBo4gT7iC1fnwNptfVGsxQlN
uDwkJk220vOA2YRvnT4ehUeNwz59hVNpB6hQNbr0zrSaFG3Rh+hBbq3vJMTreoD2nhofedGIVaV1
DF2EIXx5LqTIHvk+1eiI17ZXdOnQSqnqp/E3hiNqwG6V3XqoCbRthT35V2k5TzaeuxwK+HEN1TJ/
vbeMgk9Npl5M34UwBhM0iJ5kLz6cpJHp4xNFhIPggQq/LX3LMAxBw7IQuFfBQWaFMHR0boatfOL+
toX/wypU4BE9YOmDEMFZY8YK0fjcB/EW4BpxCu1By8G4xK/z1A2Wy3XEUWF7q7pUrHwmGqm1+kMT
mTkWYancrhlrJGxdQnU2Q2NDhJeFC0jMmG6ocXiaYIrscij71xlGI+/m95PC+1WZtGzohnF/0u5E
rzFZOSM/W8A+UvCvbXEIsUIVfAttHD6FDyc8r3jnflCQK/UPgMcVSBRWQZQpKjPaejmTWIWL5ENI
468YlE7TYlCW2shlLIxrkqewahWPzpSez6TKy68g//dq3Qo68jPM1O9kwy3ozQRGb03CwoYamJ0m
M1XfYrRPNSu4uPAcWqMUMZuVk1D6iEYwAxR4RO34NcmdLtSJsAoPU8ja0npo/7mt5UUIS6fOf5qY
eOJIWYQH5LmbEDjUiHZ34M9b7cqN8vlTsWHVW56/OdrYe4PcIPSoFQJOhsXwQdw3EMXpiEFUQNHB
obAlFHZZowQ0pxKWj4hNLcxBcV0K5ywWt7K7Q0XlFtzuY51a3Ivj1C2/uykdnDt0GzsQkF6j3QQC
ljF8LJiN7ca73HCs56iPJFMaE+XXtETd9/bxmS7LmTlJ3KKkF0vD3SzIbjfGDoEDu1drw3nK/jMz
6w4ieustRirKrlfVxHl9pEN2USL4ZxmVjfYUo6XU9z9NLdvDOt72BPgUHDvCeTlLpcyWajMytDM/
dnr+R5wxRRmRoX2p0j9wS+WBJeWg3p1j5sPpb2bT/oZq7NlzPWGPbT3TrZsxGW9iOdPo4r3AOHU5
7WA2jaSKcWSkFAWBNiSk+UsgWKE5oLHZvpIIgLqppnVHhBBYWQKIgcxonG7aHcZ1ecSl3CHmWnkm
KAQMk9v2fYL8Jsefh05bb8sPqlBFc4g8C0yzQ+DcMxdGVWVKOPokFsNM8oRJOQl1eAe/aUGXVZbT
CD0ul0zRYGqZoCXgPEJqDa/L6/y3aPK79RsW7PbmK86vVjAmbrgVJT8ZRY4RzVriGO127EooyWR1
E3XoPQnHuY0H3GArVgbTVPhhfx5xFEiWXmjo2pYfvmUVBab2HdnO8nmslcnKxEYkauKd8Bmvp8Hj
fmhjqM/gyo2AZPDzzm1LAfDDZ5SL5qlcDRnMA2WZ52r6JSXY0d4XdDRHt8eGXw6M4uhHuSR/++Zy
/TdZkIMu0mYWE2lAkJw5KUK+Y0dlAqv0XayquC/0YxxzPoMvl/hGDcvcAIhWQLkH/J5YfXnJsuhg
1LPyEq/BbWgN1ehww8w7UtRdRZd0V7VW3NXyZEiNZz1C22oCcdlvsy5hrysBHBfCs7njjU2ArU3R
wjTA5WCH5ZREbNVwJXgLxktcL0Tp0cq3ErZfvHFdMFZDefQS59e7MQnYdNuZcUlo+3O3nWW+WDZK
kVMJ5TesfJI7T8kUgrjx5teiqKFrmRdJZb2iDQV6B9vzU8AKnOa9km0vSoFAsu7rPfshuxR85w7v
Zk77dgQRqoRIkZnKCE5MRCnU5gs3ZFCAvWpirzyRmQCqryO4fdjVLBvTs825xmPEzsg5SwvxGq5L
4+AF0e7SxraAbdDRY8ApeIqva9mjgccHFxJIkXb45S/OENt/cz2LJPeNrftB9U2+cy5Oy8R5HO0K
/XEwBJ2BYNAJO1164z3np931dGo0bhNTB1lFg026KGpdI12jQs1ZhYQwZvLi+ZnTLAYrWqAEO4dM
Kf+mrhqQd8CG6x11DK+b8mfv65uxiwb7nI7r1TT+1W3wC2Mdv14IV+a/AQR8iQXppE2JUDLNATZh
0A+tJLWuBjdrMXkfoBKHx+0VzNRS8YIlAWCSYneaJEdhQdi+U/0EauJZi6LzCyxx1N8KwfJBqO4O
5XtX8XWdp5bY7aBVZQ5UJpNkmAVFDlgWawjWL/8DIdLkgKq28lr1D13WvJv0QuSQzJd+mHX+5yyE
UXeMvdl6AJeBTh/Dd1Nr09WQ5ZQKOgNL2Gm+iMyTmQPmlJ9mqmKB1xnyiBLQ/8X5LsC/q0tHkbdj
h+HhJlesG3fYTnddEv+qS6YCq8nvDJDQJGsvgcC93tKA5uCy/zcscDO7xtsvfgCZxwWw1LOwW5RQ
5s2p3hmOLTANqF7mVxOcLtQ7zH/k+XnkScIRQgajuP97682H124RTRywatPffgm1MfFGYr09p7Az
uAmpw44y+iO874gR9bJkXWIqRjRErwLB9nuEz45YDlyLruqI1jwCXrgZVABbWEKb+yd7tXfnoe4Q
v9ygaoXVkS40ICbfMi/mdnPxBPFBnCgUh11ySvLFus/eBHR69dz0dqSq8M9rDXZY+opAqW0MTTfU
L8oXNr4gxCTxtWjo2ewKy1VOQ6QP8/88ohqV4jRrih+OpU/62UC5xXnR/yIwN7hCg4kTf8peB+r4
7uaIxiUHyPftILt3Ldi1YYdVKr8vMz0h8vOuBYDaTnTDsI2I4CwJoQEue4mO990bCtOE4WUyRMku
uc+ZjO1wWFZsHi44nmUHArBjhqLR66T3OqIwMMOJx/HIsorl26RMUZZi+IGUxMg3fLuEazniqosV
fLX/yOhEK4q6wgTaWVjamECqqyFx3pikf/RNA/Tp1bKdN915iSe19clcCVM3NVb3aVT85EETdsK1
IN0j9jVO6Si09PFxFyMWHWlgt7mvMn44WHXCd8FcoyPQ3/ZUndanSRFPkyYZMNJfsuDXDh9LUdMW
PdB+48nOA+mQCBgTM+A5P/H029v6gs8ksPvqFKIgbgeLtHnEJ5BFdquNF0wDTStnkWWRoQ14/iyy
mz2qlolH4WkVYVc/2iFC9f218ccGlybtd7jjilXX7GiU83D/W/ixDkgC9RGBFWSWX7aX48sE4+ij
xRRCP6x4Xnbx2lVOKzf7eeWK6Yx727yUrCEGmCgQ61UWuLw5Ez6s8I71HAj2sM2MDTcF/4qm+V8r
tXQjAhAta9KGbOPJhcTKfB5Sm8WWYeoKsGd1JvBC+mVi422UabTCfaudGaRLyAGHVFIoMOFXQNhU
W89Zwc1511wS/XbRtNpV12XgZU/rsAKBxjfwOOPpX1SCx5O0S2JndodH8rL7L7WIh/yS1HOlbchP
6F1sKtQSnVXOjwhuGg2XcBnYHqFJ98Uq+v86jA2mqMRaWaF8HsgvqL7Mw2CTaNS3eiZZ8ibIiLgN
Mux9SetaoFuD0LmNMp1AImQqX2qfQd/lHqAIYaXhTeMCjMcbn+YFu4+Js7xVuxOPWwBMs5uJCVTx
ISsl6ceMKLfkScFul5COuER4e1nVTO2wlVJYiEN1KH4iJeeykI98Ggjn1wkMdmYoEQfAr++1QwZ2
JaU5PXCkrurCYFUUUrbO9Lz063eWvEnmLg7BCbDZUo3v4mfdRIEj8tLP+HsydM2+2sWmgY95083n
+HZi5c/Nj7L6HhDnxMPZLsafk70Nx1LxmrCghjJBXz/Yt037w8nEXSnp/obsbNBK7gxwLT2zMsiP
hUqOzYaAE8HJqhzLFFSzznNPm0iGNRjTNhG7TVET7PnuiukfdpFITBCEDEGUQR1kujCkpJ+xp+tH
1mTt82AaL+oMXcMT4vNwARk3il684ikhXrcpY7pQCB8lQDqXCcy88uYHS9v7OGuUynhmGEzOTc8b
6VM6fXyNJ+W4J12OpZMf5pStHaDL66jPOsg8lvM1sK9qRMHUGXz/quRrZLqfdWurCPpBfPw3MZ+C
XaWL0dk5AdpBuhQKH8Xc/4TsSVGUonqEE5yABoU6ApcXMPPw4eqE0/XooDpQbxFY/Gd1y3vAaHqu
uuot31uYWIgdsf1Mfd6NexfZ49QSxI17VBaSMe7Rin3wU6aXPAP4SJcDVh9wBuTwHyBKvcl+sStF
78+gks0VcH+nl7sQgYH7OIGfdsC5qQYvvYG3fXItE4venhsZQldnwGOTYJmbqgNsourgWXi69pxX
B687djq8D4WBWC84bNqixojgG3Q3aFJna0EH0etpW0rlzTdGnf2eyF1rzYu9DR0zP+Y2n5ETNwUw
ACYnHlhHfA4c5vYY9qZEUrip3SF1Wm4/MTR89MxHeWQ+49D+OPv4OgbzwJdgr9n7TXHWcYXklic1
GelAK0Aa06WdqfQRh0fT25Om2NeyyyvnErGuQE745JGb0cjepTJkTc70WKuoLvRUXZrkOvWMalcF
Pi0nechwWvOl28nsg80LWzpGsh8m5POLBerD/UDNXczv7jVuh5sAg+UGsj9TV19QbNlI1X5N/i8q
qR61lG3xKUuIzK0Xa86dV3W5tFbOOuczsIncZcXm8tFYRAg6ykyYjiWeNlJbpm35UF3EdYnx6Q1z
aUEPu1/tPHJ7KVPmVxWRePlAMIGst1+Zesj/i+bR4dFQAuNPuweDm6Vf9wmNuXP4ysKIqch7XlMn
7/EUc9TGl5jZWl6rm/j1IWKUPM6j21BCiJG07G9nTCwiLZYMvuG0mkHKoTWell0hgtXxnD0EYW+Q
/iNElA4hBIHJynSf8wetksUJLUNKmbZaRqdmXKxhN/VSmNwaWIc1drAa7akvHz57KKNnlRjUREif
D2N175dc2hqmXRJpoozZogY2IefSMM+mlr4Z9eQISonpmJXSKSchNfykWa4qWAzhe7blCA22diVa
QpcjN5X7h9KSOWt+nBFXZW6CXxI2q89aReZMgERJ6JOGa0rzWQNGrTM9mqcYJdm9Z5vmlqnLF+2Z
j0GpxEabdBSPZmoIu38UC1wEX66RPwe7qDpiOAv4AhtgdUZMeVzctlf/54J3QF9DOShCCuWawgHF
+yf/eaZy47e3ne9Kra0nsElSIDTgxG1Cy3kwBv6IYP0uRGPEalN2DmTJsjebM7wLVB+j2eLm25lD
7LeGlF8/n6atVtqbD0FDYKTe7V2hi2DA+tPUvk2ccvyBpE5yFUY2hURFNdHxjBa4X3XoesIKAza4
Vu1wwot2ByJN/Tt1gaTepn2AZtjN5j10TbE2cmV8CAy9+luOcibvBhstBtfdODLU57IYDEADMxCR
ts33999JpBSO8aTuuXowlp4mGis1Cl+sYZoS2uvZTll8j9aLMaM1oNy5uc+c5h9s5y65XEnACmuL
/FTNrbZ3/MRzByLf//UBQMeE0xPDzCsnKJGLBvvB+Vn15yECKd60i+hQoYG8+djCqEk4LLI27Aez
vsU2EHScN12NkQAlxqOxZURx2EmdMbOmNSQ3N2hWT9VHWSDxKmwDJg7FYTnsK5f77fhcfL/T+50j
bMpwoEfou4g9WeAxUJlpP6qNsCLQkOo2jo5eoX+il9v7dvee4JimLdBKJ36FEbG8yX5MXncizv3x
xN90hcMM+b9DpOudeogYdaYbr4jIZDbdMZAujrmVziDagOWiUioutp/IGzvj87V2yhcEOUmqCUt0
leUyv5ZMC28Zmx/hzxtnnXYZrqJQ4RX/Mb3S+fKiUm3APmrG2A4OCWP+CYlvDkt90OVBdJWl8BHB
7CUAlsVHKEeLUkTsi7OChGOmcL25ZkGmkk0VXPnQYzBDw8xQndRh37vfbYFa+8Oqrd1rZYc6/dyu
QE7Xcyh7+Kh7r6xJLNeeKDgVaNQkZtJmyRF1gs4HQIqSJ8l84R4bLHo9UyWmqouCbs0Cl1YXCgXj
CBjUYltH4RhsSkkJ7wLbtyWUWzAWHYQSAkjZia59nFNA9CftvAgHL2zcKSz44+JgPgD6FJrNwZS4
3Op2eK5NlgcNzoSCJuMEGca62NnYlKLfd3MiCC9lCJCk2YACzP/u3xJQB32TE6OapgU0XLS0LOdR
xsZJNKmc0SDKbGKWGuBdthc7uahHayLaliePC0wiQmANImbQTLNga28MsSsZDII924KZlmR934jx
mEvi7VjORDXyzasknSe5UUwZSfO2/PbQnhuCAmY/34R9IaJBo5Eru3cwWfQYoK9K/UhAgncuHJL0
qsK2nnGQRmqvI2GNz5lhz+izDybj5XAkjJX9iTjNBmOXa04P/SeDNh1TACXu3RE+TOJwcmchqILl
SE4jBWYb7x0sHneVWVl3kh3iPHoXtL+ZwsMUkD3WrQqUpvhlxGlPuAxAO4UlAr/i3/e6UBHsQ5Ir
jNUQ0fOG0V6szZGibE/iN70MnHb+rFydIBmeP3SUJDR1uK/RXrX0iUqIF/uF05Q4LeOmf3GcB2VR
g524Ydah1KIsit1JkHbofXbsleL7gpqmj5tfe8e+oufarGXLOtg7K9zjCiWVp1O4PRPY6wgsqzB6
qR8A4BUt43DfhO5DEJzT/YwvDG1EqLs7D0OMDIbGqAUA9Z8oHSXKQTY+1Mh3TdfvDfbKYRZ2o9SP
6mFTXw8LpAyUMWvMgsvip4pqKucLF8PcrKCvMg2SURvxgCtzVTLgJZpAt/RKQBrge1vDeLtl6JXN
JyjrbfJu36kPzfjq48nMYGvUZFK8/T5E8h6ZkQ7bBX44o3HoGdw3k7N5qlPqYZ2gh44LbD1EtTgI
QhqfsSrlvW1Y7DQv92FOzJZgWWEAK3Imkbh5lPjgZshw4S2m1OfDJ/vluQuK8NjBWGODGtP3FF0G
d4a7ELzv8EjrzB3oTYoGqnJL8zs4H4Xb6YP6N59mVBidwM03b4/XaLQTghcH/Qjmz2UW4ugBbJeo
HWGx+rSIMv7Rhn1N9WJLrqUyf3WFwAxuYTr5/XbqHpIxwTlOykibD5AozZ2pmHQQhZVhvSB22YaJ
Mg9clKAE8vVoVClw71npmOwuUYp2njwbjm5yqoO8zuekg4C0MTGuzB0C3CY8CGsuCCp2kH+cu7DC
L94Xlyxn4KPNoh8EqgQtwfnxggofYh+P+0Z1QD3F6J2SQsvGUnHe/h5Nf89iR44KdGfUV6lLparh
Pwin6ug/8E9yftzE+mpPxn0Gw+cG4Nds6WxC//G8iPFXWqtSAEWKG+Arg3Yv2v71ZA+3nEyTQY34
1NxvW+QjonbB+WnoYugW1WFfVNq3ztRHSip4bMZiYe/fIt0+u50283YuvyFPG9lH7Xed5M/19jNu
50sNAi33LEGGLWhY0lEi0b1QETbnYD4K4PLrapF8k8t6lGKhFSbcOHtlxqdOli5yy4Ky8wzpbFl1
TAOEjG4cs4bSermq65sYFlz8wUzFprggPqi7TWS19hOuOS138ZxrCemYh121cRRE3kdVZHw1R3M4
YiFInApkMNQmQPKDCL32kSU74eO42JDv12yrPtlU4R8sr82GyStaygF35lS17QXZBuuoTOnFlYNz
KFmyb5PTJWHNa65BmFKPwHeRUhKHhbduixwFY9lGAz95k7mrCFtwsOceRFKSs1VwiRuSAcOW14iU
Pr67cScy6wlR9nVjcBGjuju2zEl5qxmlvPECK9j4h/EKE9IpA0g90tFSBmCzm/k406c5ds08ZxKn
VcoM6flh2XJkl5xdgVeb4RbhP2peiER+UxDElxQFB3bK8z01X0SGsK+8gMabzARlr1OzUWcdXnZB
7NKO3hddxbiLEsgRLIWVD2mml4Nb/wLVIBQX9dkllbFBM5BYI45FM6GJtCpjMkARtWOUpRNfRSt6
KDg+LJHPHBunyTXNYp6W+WBDasVyw34+bd2uuyJ4b7uyqNULVAFG9mysm1C3m8/mMk4FhNP09DB3
xFK6wwoIR+D+qGydI150XljLKsF0Ph77WOzDcza2DeIpHlouyL7nWwuG9HZxfQywHGOh5106+2Fn
URN33Wj/Ddp9puY99x/rwbihTmNOt4p6J6C1fGL89fHu7NOem+I9YgUdsEuslxItIbs25paxHxdz
bytpJSaa4DJ7fZz+68la+Cd5TZWR+q9Ekk0V42vxl3wdvywNbPwHz8y5EyU+RLcFtfqxrSveHpnZ
zLHd2DBivlwPbEROYh/TAOMDj1uIBUNNNjg0AX980fOTAX2SA1BGcIQ17oDKCYxGZjvXb/w6FgdW
ivuvRqGiGbGZlo4gaPpTZQye1OdmF9ns06r1Q6QmISZfbxckBUP9V/QDKp8BMD2lfL8mgr5PSvnl
XHLNi3jOqScoiAKRt/92pDALC57YRs6OVqbnNiqSAxDhtMdOnYMZxLkNkyYdPhFKvlDfhIMGxBCv
fnQwjRi4KkMbBYuinpxP1rKvvZK0zyrY5wb5oXPgf6y8oo+xQfGSSPDZchzuf15hLzWi2ybdmx96
uPgAHEvK2WUqOttVSyrL9QyT/Dw2wbMRWFeOFTiTJR5q0Rzm6buZcyxoUAeTibaIzXn9kTqXECDP
zEzE80bsPYijohYlbrXsovUKHpRq48oMIbw/i+CMEIZFYAZ1TfxLf2+tZ7qcRY7ht0P9TZ3NgyKw
3CrFdMAk7WihH1a8YSys0vIdfasBTRev9+Paqshw/l/QKvGBBagKHfZ5GqPi5oEJrSzf8U6ALvNA
4FVOVcfNEZtikpRXQsFJr5UJBH8Ey0U3p1ffjM96Zu4Yu98+Yu3ZvpxtAHqwpn2pzojfr2/Hvm2S
o+WyRseyktCybilI8FYjahhiqcaUkfC85QAJ5MBDyy4U4BvD6webtQlua5EuZ2lSTAqBCWnHutIL
yivdlJwi+KZTsKytk1ZvNGh9Fwv5IY+P9OsxLnApMAp3yZQI1IzPt1OLUNnx5JJoBteZ3ZRMfpjr
CtSsEGHdQBuSLwJGcC/BiRqrgUjSYIGGJtm6JnxkSLsD7hnJzSRWlBQzioGQrPDPMLLAs8OVNBbY
B4I88k+ZkjsiNOyNIDjaGi3rYiJsQxDOa2zwnbqq5xDhHMIuemUEgDQnvutDgSPTXmWfFVJUKJou
5+leaaM4wgfPawrAyhlM4mUY/CMVZFeVdO4L2ShElGlHHReVSz3/ikDl0NI/iRI6KOCiIDT/PB7N
og6vhU8M83OQrMRJBcGFPNEEtG/jw02XuMA1EVLhUqI42nKyd3fLqb/v/mVSE8nzc6yiyK8pKNsE
TL7NEaX+I1CJpk6KOcMpZNcWwCvc9y8Vx1+wuQuOuPByEgyEZiCAPon0AianGvXKu/xdwE3ZmF9f
wfC5BAR0eXrbGh/YlL4ZOh3axMZm4qBLVdMuHFC+pM4DZ9mvgkXGMekDjTq0hA0Ho3BGkNp23JF4
jswBZneI6oxIG0rG0qlVyszhPaQncyYywIUP6/B2Bevc/sw9qV8S0WFoo4f88NMeDh9EFyfc1pEB
hV9hYrbxYF+CVnmr9a8xp8ZKHMzzQhR0qvhgBE1VSN0pYtBPbpqAWK+Bv0GyE3OfttsZiwEkRZ6z
SmakHEUkIGR6/PKtmmZgA4Zjbq4ihdgXi7tUgc3fBjU+7WiMiycXmHaxJqjiHCiq72a+z5Z9fT3p
okqOr4gF6vtTCIUSTdDK4iVfynfEKAez4lpvK9YcMG1CxSPOMBWVrtqu1rYF7QVTNOViYYZ3img7
Zh5gkssUvjLRm4qJJhVOJ0Qgagao/TC3uxoWabIjX4oU9SpBhuetTqhTmBDeHdQs/D99mpCl0iFA
WMxxtStMIfFUHZ2FtXDR7ZR5/e601kyEkSDS/PE6zMHLuIEkhgBsQKoNgIlJiMxk7llH7MXPFcbA
bPAxkU2dkKRbC8qkCEW6QMHlI5dpwcTu4zNnr/lCvX5Lhetjp9TmW7M/vc2Q3+xROAFccsQEqHiE
bpDDYDgH4mYbTg3gprMcqhqMC5Tq35yHx/c8HZF/VAuN7WKqpRPSHdia0KX+czRLEF1KlrRm+sxE
RmeBKgg64HFPt06W3ErvhE3qozLYI9AkyhtQpTWDa/aC9PRzO9UEka7HoBFohc0oFhm/J+7Ocnvk
zmkqL9kcoJdVvMsBlHB9bWp2Nzu7uaKGkSDtHFjLBxni26Y2CEvnEtkte/2vI0HbHdIe4z6P4J2C
KwtfJDFuAnCGZ3MRgcpbuv24lml4pR1vpf2KT5PyqSfcgFV7r3Blrd/DbVYyvYFF1gT7+Pey14d5
y9oA/xZ4bDOgOw1zECXR18DzwoW83G+dFdRfgSML95QMV3eDWjxHHvYQXCYCKBGqwL1IdLMZHDQZ
Zz2kbWFdIrwiZ38mquXJJRL/7yZb3he0q87H+yorVJZuZdqMcaO3HGjhpzC+3yEyH3dOBUx13uKJ
/oqd3Z60qkbagnmL4keZauF5r/mtkn8IppNPJPpVM8Oln7Kzd9BvkJBkGttYPDAQvBhDgH1XrhPS
JOyk07gbxNZn/JJaZOLLgzSj7lMwaSkxq/9d3bloKhsY1DiLprAXY7ClAoXOQqWQdrDRQw18l2ZB
qaP2hauDjfeJMMKY/CvKLKWXkmlzl1GoSjJxhSSsk0FC77TUikNGO9K6rtq6diGVeNhjQpM1XUAj
iaJjOrGodwVIJD6c3sepcBQR8cGXdZICk7w2CttMaMwS8UYu0Dzj0pZwwOvdqke8Xf7VLX7qiHkk
UDsGceRA2cTQts2j/CQXPFApk4mWTxFL76TN+9cl1A0vtPRA6ATveP17fSjBtu55eD6+uTDYMqcP
PCryBvFbyYJ6A/rXsnlqTZ3l8j97ReJVEKHTECyzdJaUUTrF5eo7lSvHD1Ot5H0qYjH487q1Pa2y
8j9PNsqqu5XHhaAGvkHVm2IxBGZ6SZSKMLPotjRtwaUgoG9y4HBjGbCERW8E19ij+dqacB+qiQM+
QFLMIlZbgPcD6SLSzYkWiI7jmbLA4oYvsmdV0QTI2AIfGsskpWjI9gAkUKzz/fTZcToNP97aursh
NorAIW/jpNRrTPkSKIHuMcd/agFLViBi9L4VKdjFc0yHlW+9kLluCAw5qdjWSnTYCoYjtXbMVDPc
FMJQd7XsJNm2piI3u0R1icMoerDe3pmE1lwhLFSG1HmTPshQ0cYOc/0lrBEr+Y9FRsBuGYtmMZhz
rvw0W+9ksbWA7iudGYEGsJaC96MkZNtE+bhNyGPzdVIY8FRmK7BcFVxp6oEw6S+sUnWWAJgvm3bJ
UTj8XgRaSVQX9gwIdCBeVoaI0MbuGBoKj05LVK6P5g9LPITaV8bT+u5h0E4C9UIqhFY8cCXGUvNH
Gd2m98XbakWGFaok1td1i7l7oIkau667oc+TlVdTrhdFP7AOOLi3g6YPyXAr9okucDrCZiFjqp3m
d6/o+6mSfKwEPrVYHQzXnFGznsJ2maJ4yg3dcKP7DNWYbNaX3G72QBrEB7ncbVEYV7unZs5ZOArN
4Tg0ME56XdjT85AUv/lLCZleC6ypZTbcgJo0jdSiOWcUceD8hau2GDs/q0s1yq5nmnVSQwCAbAAn
BINJAkWQXfM2aFg/8eGuSnmydfLmEvkISiopkWEru/nyRZMneUVBDjziomz0LYzk+iE5lUwIRZdr
EId71RLqynv7SVs2a3o85tfpktc7yXJGKJvW2Nr56h45WkxNK13x3+HNBUtsIMlbPBVO30UImcMo
mZLa2l5m91Zi2GcsKDeuAGYQiO4gQNQvpEn39MBaXcMavqkyodxFeBZ+BMwDF0lLVVVOUj27BOoP
cNhX4pmRN0GDGWk0m3NQcc9fkfRey1k2gW340t1Tp1dXz4/L3uE9H9s0kVEoCnRPCj7BYWJRqBjd
OXjbR8Mq9Lk8xfzStRFfpKjbXkwkqUfrnY0grJ9zMHHOPEaAugeATafSiKQGElJh+nBH4taX0Gv/
EQi5yIw6hNtuuKIcNdgz4Bbf6i7excQWA+ql2TnCo4S/dfzubwvD2bUsN5uLPFn6OWtaKa8rQp/2
MY57xdKbF4b7k1DI0cYIZyWbDiNKjIlJfACzNX9q4f/LDEqffPX8+AVaqt83bc9IoE0ECzT1dgfO
6tKb8RkVCex9izJ0Lv4yr+1m6rm3PJjZDgDeRjJH63GDrN4fZZmI2EbpW3wjF2OMqKHETyo2HFvd
wv6cBLHKGNyRkL9knHwmfkoGKdg8wpbgxtV+aKwSKJSofpDT2QefKPoFi7oNOSvYp99WWZYM6yLI
7j1YVbywKg+Jlv7Q4BvJ+wXFIXyqnR62fGeSbALk2m+zCRJzFnMjO2AVAgiyJ6gS5+HOsmm9hU6/
DM9e6pCXU3URg+RoEpSUNlm+8/+BMrPfweV2dhZWc0NWVF0ZJoMQJFqbyEfllvFC3CywF/nCxvbv
k2wyalq9h0GHpOvSw2szXx7Z5xz7anSXVu/INBEdihSV5vyHZbk5XAZh/RbsbyEff7760eCo5LVN
Jgi09di36KHfi3NUeI2bXbtUXJRr7+I+DK5NWGd9o3CLvbyMKK1faKKGOeSgVg6KUlP5jWQs0b6V
21GJ+a82Jhrj0WvvO+M2nUgra1x0K6kM0L4hRKpKdxoqRUxc/O1JHTjBHbUOaHuktdvZnB4aRo+T
/ZnKTVsJx1EHlETKq6aOrm50/sNey/PyrT5YSc2B2pYOTWAweyvXm3XaFLFio8OKJDRwT2MQ9DkA
2d9nBZib6Pn8xvHFpoX8pRLq08Bz/XHV+tx6ZwVDN1VpJvRg8Cz8Vq4+mZW5SxOsr6UCerzhGSid
aSnGZs4cGlm1E+m3Psi5myqKXCt3eRl9GPGdFqwYFbZcPrkfFXHH3hHE/X+DhQR4jzmXI0fMKye3
QylJ/jz45yaBcGfjWLFkkqzNJ4rrndkh7RYIvc2oHajlRwD/hSEzdNBBvx1J9bgcDHaUcRKC/loX
5wxzUoQrdSueebsCHiDOJpXOKkroKGHjtuJHmX2YtAprcRYwf605eaVcU1l9vkG7+anBKEO1hVYC
1RmgTzF9LNhHX5u5H/v/16JXl/jzPauxfWV7EsFOyYUArXBagIl1UsJFPFL35Ac1BZCtVdUESMmq
D1NyzpFyLSnRu1k2cNunI4RvnP4jL1zaESP2K1lVgBwbQoEN0s9rQ+8v3XaZIDFgF5ifeNUr3tnm
VdmskHeHBS2Z/uacq4JxD8aJSAaT7XxJDEygcyQuzEw8k6GbN3nlxyFgx/zkvO3PhTLNvU3QEsr7
UEjIzBHgWxi14zFMQ692s57uEz5/Hj7j3Z9Snrq98Bf4ZNcAnl2qDBtTgq4+F95O+k5Xq7Uo3urh
JyfZy4dlymxSPXa0CccliqYwP3Ir3rsrS6nWlMdS6AjLsZflKokg5vUT1dMd7olVGeJtvLz7jQ9U
WFhy5s46iq8goWdlcOT7Us17daLWrW3vJf+5lisIyBL6WwVWTEbxLuZrO5I7Vy74DimB/SfFPCNJ
HOrjR8OTtstn0te7wangacXhxhrYpqxQvGxGDD/o2gCyktMOAuwMfc0/rMaNyvl6N9ttY+R0uVvI
yi3+45kaQPp1V/7HvfpZhywe/2LcHg3DK98RjvGyyx4BJT1/Av3mQP0hjLtGmIp3nZCau6/TRJxc
RBa/E3lNT+LqXuMuictlBUyt5/eCJG3tSM26p23M23qEKUkXQI+JjPxIIL0XYKhfrsGMoj9UrJwx
bSyJlidlYagE0fuicqPEVlMHB3kw3GXO5jbvdxYiKHh1Jm7Qe45bU468teENFF0VEyrSCb17T5LS
4jIJTCN1wy/MgAUYTEnNbhCed8R5vk9ydRYQBb/Io4k2Ovrb50qO+coA/jLl4TUkoWuoYM4Fcnsg
TKCxkMD0EIvyTC+bKRp0UWrOweFHM+BvhcOZaPsE9XUtK8+WNI2MVIM3zdeMHpDWrLPW4dXhsN1L
zXGRIZ2qnpR44lo6M/EwuDl3TrvdedjYIvwS4TB/R8wbfy7Du86gXmR+uM9NLiT26ZWf8dq5KQAv
Mb3zbvDl3xjQSKfDlhaX0EOjDCLPu4CA7eNKcm/JgWcvit2YhRJW7kL4JHnvqNun6kKjuBD662dW
JH7m24odl2Xbr4AJBhFQaalvHdes2zZFXWWkEozk7mx2Uc9V4mtuwiG82JO3CWGX3h/VCq5oGRAs
qkQXxmDVgRf3Dxir8Xm60HPU0EgqPUe/H7T4040ncYa5A0Caybx8FZ7gM/5b9MfLDgxUdKYfLSnF
rCCijuuq37AgocQPSeqvqzypF0rNSGf6mnAe1u4GamOLhNZPYa83CTOJp4bN/O8u+OYmVl7ZeOPO
TF0tbFh8QwD8jJN8Pm/O6KD5aRJa81cob7UJznXnb6GlEfmDzaAU3LhIs6UhZ35zu48WjLVrZA/1
ZVzxjgkhfUQDGgZeJIeot+ZzNz/mFxTyMl8C5Ra1wFgr0qa7BgF3IC1gGjG8wiUHaJPQHLnc22R/
i5LFjVNLNMP838UAoyWH6JflkOkuaAiPlbt53fpYM+wAM66YIkC2Zz82HLtWm5BwxLPC241qw0Ga
/hCDeKWJp5LHFl+l6Ene5TqSdHBd55cBMpQO9xBUjwjTPyzVEbC5uUP1tclNNN93/U0rLTnEyj76
06QdQpYU0lJ0Zrk3IKMyFA3jPaOMh63rTdrOuUG+wup4aL5C1+46DqWrfNl3zKXYeK2ecHDG6F6I
uH6N/MCxZjOWkcS3znaRhqbC3ng78gnH04jY8HxJS6FLNCaKyVWC2qE5BXO7+yXVWvsn/3bxsdLq
b8Wj/NfLT5zxoKH0YKQoXtLhmMMiR+wePTSQuLt0nyN576TY4OMr08zoDBDY77CNTtd7w7r6CGg9
aZUw3hea2jMv+jECvpEVIIe7K0qNUNkuoQf3mnJ010/ssV8VcV6eJlf1qGP655DPa8yrJtnYgyRI
JA7mnNPzdyly0S3HTlXNaPmHOH0jWYKF0jgDCzbBz2/0gnG3yISEYx1IKpeMexx6KC8dvS2rW3lX
lfH+JiWznriAyaf5aTfT5RjzhO4cHTuPahEe1v4KpXMaucLKbUglXcpjZWV+GKSv+5OdeRSe52ta
Ageh/WWvn1KKW9bYUXUGYCFBst3/SkLsEmDRzpTwh7lQhihjFdO3Hcv/VBSNi952RBmOQcLzkxTj
1gKo9L0nLzziT1ahcqbhVvsxVLxOKr5ogM7R6J3ChEz9aNK0tOQt/2KRJ7JlkAgLhWnrjaFfLhFb
HBYPkr62dTwK81cwK8ifDdS4kcz0dH0C8Swju7c3pWK6D1LJrM8mnosZy2NxRbKIaDGYSpDRigoc
988JFrXIaQgmlfT4J7r8jJ3pValIbFqFTs6CVKYwEIYKxqCJMTR9Lejvv+Mz0aeNAKW+wK8Jnt7L
gH9ja0cWu5u9Yr7dHZIGOvaoqE5cVDD01l49OFxyJUX0mnjZzGUsExpL4+E+MF+RqS6qg7R/ACFk
99NoRZa5TpTf6dARVpPaIoWSJ5tYIFEYpRYltWHHSCAwX5tRdMhKclTOmuk0wZh1clsjQwhXN/az
grIg97NuXM4hupR6lINrnQudU54/bBaMRgt/CNEs+Y0ks+nZ5oqmb58whzfAUvFkqB5cfmRZzrzk
tel3KPfUDjWEp4yKHIfG5GtiBf8CkQNv2abibd6fgo8KQrEzcZ8hvvaV6jSf0oCmGuz+dlS4j1DT
Le2Lc440uLbTZG7DPvjWnoKajf32terx/Spf1gpfrKrJWEAXETtRFYRvQOIMuwoP3+f92/dZwSv/
93KCJZLHRWeiiyJ6yxXd11XpcL0xOjFwMPuIFMS1OIhMsPKKjttZARgg6FrOaJvq57g4OP4B4rZ+
zAZ40KMutkso2Qh4d2rnQS7BzEju/ZciCNUCMzPjLhQdu8ncIgQ14PU8Sa6RbnGml7MWmbPNcTyO
kV6rsMoC4VZd/c14NCNVlQo0BDaQV9HaBaBZcYZzbIZ7cmm+WrUQ93KJ99l1KjCY2aald6bHClJZ
XrnpE4od46MARTZZvA/aQa3eLwzh4DzCDrwOSIG9klEU6xqhE/vSQTwRBvlV+YIKmMDuLZk/vmHq
5n1GNOvWIaRvGyvlkF6QL4rbfpoVXnyPXL8xnbH9aqH2qrCLQlfZlwlQIlT7I1/oAFzbWzgMUcKi
7RbuhZ5k4y/AIToNiIWkEO0BIAor2wcnI0aSenbHf9FFwzzROpMgedkVLPV77EbdFBFFPKRDJGxg
gRa9Iw9p3EvtT2Q60+3UE0KH2haQ4VMJQ3pmhOond65qbl9Au/UedVdDJXpuqZXa64APEaEAEh+A
BVTkPMS1LDcX5PDeWIHAYvhtinSz3yx7eOYQZn7iMIkzgs+ksqL9N6ZNBkpA8Tb2Ro7oZVDcxDzH
LuWcqPsUcjILS1C9vBBpn3vBHt0E5o15s7TXJQmzay7rSOIWi9/eC8HKS+HH9oLQHN1ZN6OPCWf6
YZO5LOzvBh2Ec68tBm369F9Qa+KlB1dQvnz61Rn54hJKBhOSaICQRHasCPWDgodogMgdmNv7oIT2
h/9TZynR7fSfOMYl046q1DSJPos5XTPWAawESafq8iVp9ZJy1xqwKrUiK9DJlpGDFgndoQ1VQUEa
TgTPRPqwzW90CpBucvTn9MDXKQtmukhrVf5BxYgvCHrvHXDW9PWjDexhPMxin9uIHdTWS/elf1Yb
WW9vsHljWFDdltwSlri/t4+vgKT9MqMXS7lTIC6xnbrc/+SkK1wcUYAO3yzpqvXB7kaMagHqtzzd
NoTbZG7YTjzNlw+VWDWgqEiJVK2cas+Mfm52cIA3GRhya0VCE9wEbOFZZt944iKkdP/Uq5aGV8tZ
xanna2syAUXZ/wVwr9ehHEVltNDQTGD1Z8Vht3VeAwxZLBB0hVUTGAZfox5qd0OZg6Efuf8aaVpP
OXREwvIAIsMfxjAAzkybaCRFIFEc1csE/SQn5n3sxC/a/y65kofkz1YCNnqlvvnLsQe7lchpvQpN
9u+3FG1RS0aowN1C7bOCULo+PSU9fO2NF1eLyYv0k7HPECF3HMEns9m2CICjDFoJcQrUsMLtiqDT
HnndtKBBC75hs00Rye9qAq0fPFucWQ5hRPmSxjKAhtJ9ot6EXQJ2fSCIPI9Xxs52FwhCjhBZJ7NB
cqQxYcVBAFPvaPFu2WeZC3ifUrZqtH/W0RZ4eeIIYynx50t+2ifzqMZcXzPKiM/kW6S8Mwn5uIL0
xpYZzQCef+9mP7Ezqe9tpyASxaOpQoe5HA6kzHInRJSoBlY5bxaNAMAbmXZn0ZCttu8WgVdAC1F0
nR92cyeKSRcWnt0K5Ax4dHzDccJfL4OZztEKANCxItsCBWi70OEOAXQR7TcVmIJ2iXqa2Ohh/vKh
URvmaNHhWu4XowwyfGbpEm33+vO5vXWgemNo6CatPMmDWyYB6dNtmlc/gPsR/MoGgcV0VD3l+p0y
+EhxCK/sh9qC8eUHGZ4Wn7M9VHMh4daQsr09HTMm2q/09ZvSdZ2dnwGOb/0tNsIdr8QrqMFHnFC2
sEea0pzFHXoKP3IR0IjZnItfLyQYU15zyOwQic2qMZCf+5oBcABcD46lbqBSqqHCTbeYizZJ7P3A
IZoFSyTNZS33wIcoAAlG5gy8C9z/4zbMxGRbKZJwpYR/Uwfq0ncWK8yfi6xalvWb/AfQFKRi+MHf
ZDkzEiYNvQeoZ41o3bdg3pJxv9VdAujD22a5md5gJ1emjBc5CHpknd+ijcVVHKJTRrBqB/FasMxd
m/jco7jzS36ILN9qfF9TKSfcbh/e62VL5TLWtb4ai1hitIRO4TBcwV8BhIcWDpJCViF1n7tkrR12
uJjlIJdYSJoE9wQBp1DSven4CR0BGy3vmpkDHSe7fsGchCSxphJq3/RS9dGT+bpPGfj/zN6+cz4I
Q8neJVsWyAWHUc2L9JOEpW/6u8ZlM2qmBXoC+SYs/ZG3XVbj9so8r8Nj3//7KB747ahZgqv/WBkZ
F6wY4U5R2PtiQ8gRWjIgoVBmoStcmj9lXzd+ZgeWEoJW2JZCiQkabDZ76oaWTILnHWWflwq9roIf
XbJwcOiYbffT5qmZTK3c4qQTJ2B+FmIYAkKwjMSK7Npbq5xNdBbQL16H3yUQ1Ar8oHN3RtOeYSzn
dsWyikdcbfx8bmoPlsoSgnSpX2Zs3yW6KXEEKqLXH3s18XFB2F7Bi8Kryi/13ugI+n6L4Zraq2oM
aqcI0qxuyM+wOHPYNdcB7FZ7+1VQrHQGdc3WOmtOt7M9fm+wq2DhxFZlxqdKTqoRgzOPCWiKYZ3k
b/cFkMi9vUZhL0fVJJBJRol2EplSfWPQlDBhi3aCRBQB3cyvReFcmIyqBxqacTPLOi6Ghfxteznl
GJ+OCTwA7Zd1cWPkw6zdwTHGqkn/OMiKLaRTMIKCT2flwRFUchVhDeOMmgYWde8oEykBfRJOZ8BR
7KkX/Q9x8QCBLszWv8H1lO7V+l0WyR1OrevmxdSh1Qneo6hgUmh4i+S2mJjrlLpjhd5Uwq9Q9p0I
svLezKv+aESkMB/+owWmUwteJVKTjC380hfdEZeuWe+Ul013A+147Au7SaYEIFpDwqZXdEZCBMRT
m/sESdB/lBfOyBby4T4pcCHjkuo1lf0j+c/IA+TYYM10CfHMl2WbBCes4xBM8plLCQtU01LTDGt5
BG277VT1U68bOTwEa4Kc0i01wx8Zq9/Dfr+V6nUcO7NNTU72NjRds5dixJvo6djttIijNeJ5j9kE
y6E4qRDVv+iSRD9qZVcqoynuiR+daDoKw3nFbABLJiSJKZtUTtJbzz0CyrhqvAMEXo2cfiyVoERU
Lz3asdXn4P2A7HeygSKcXp20v9FW2LDgqJW7qTWsu4FkCrfg2B8ncX3vkLuWatX0fH4yeLZbwfUB
5j5mFXu5k5FMvqmiB5iG2TmWoiE4tWpDrsgk47ZLFcjodCAErzOpPDrDbTHxICz1K0IDl9OCjTvt
2OytS5IxHToIssMy2uttXZul6nqAtpczwFBrhwXQekIeh6vMJtzEBZD1XzWqWR622+ZdMb51bjkm
D4w+/epDrM9oc+u6CbjWt8twexCHrPEY7HIMwCgueAut57XV6FUxnWeFdrSIyR9Rxz04J80LHrV7
4jX9cDqG1pnxaGU7RvFoPeQZRzbv5dd7vy0XrScn/CGqGO0IgEH+Z5XwUasdxkeesioAKXIMOX8b
/nMpap42wRVeuf4j3M3c2t3KqNPJuTy9Lx4SQFuim0JHucaiefXJCkaEZhL7PYIThcgH+S/herel
tM9YZE70PyB+thxPOyDHNoNNlEyx86lSELFzTDmayvDJTBkgxjPW5wBHnmSEb7jMYZrlJTotS7iv
DgXLRztbhX/NwdIktPog4KL8Q6jRHrJRezQeCNBioOpo8DdXQcGu+GU/wPHLuk7HkYosd+hxBoNh
7QYsxkQqOqrEZmqxZBsrb6jIakYJRz9/DdXuKqw44WklLDO8ushTlywl355L4RxNlPOVsoXTkdyn
Mq4+eZ8/wrwZ35pNBbo6Irwqkk3vrqFPVqxCnjI9lWmnTSOYVeUNcrPiuYQOFXNrMoXuCu3+/DIz
6u1RALAEz+wNzMoQRNJxQyWEsYjXe/6YgKFUsrkT6L1FzXbvIEbhcufiqH7o7BvjD8dJdgjVEe0O
VOwYcp2fMUIQ0owoVZksI2l7e+KevTAka9w/agpf/EK12wtVetP/q/uBz0j/H4ZhEvmk+QjFoXRH
8KLSzG+114Sw/Wjk0tnDnGkDS6Z9hEOP6UfqOVFintYYrGt5I86j8fZDq2smmO6ZEvVUYKehb1HG
LBSH9myyd9JjtTCYCmWtLDsVtlFAmDxJoEwoPWnUYVaZ21RZ5uH+OVJuhsGVsNL8x65V+q3YhTyD
GDATjQ5S5V2OEU2F6ybe0T1L4QuR03ZgY2lJDxTcN5dzcZeTBvzsPl2vtj6IUQZrPGNcr8Ec/Xbl
WMNqOfbgpWnjbZGtf/BVNiFfYGox8A2EskcInogj1VTrfFDlDRV5zRrVhDSOpfqM8NYyEi1RZ5PG
LSQfA2X4+8Ig3dKHG7evXpCYjr3lt24sYMEcJVgCKRcr0pklntu4KUlKTn9DHrD7BQWOGBGoqVEi
7PL1PdJcoSMWqfOWXbxBTxudv/Nvr4SuNDfs04LJRmpfaPGno1JwGCkr6YB2LZqRptO1vGrDPmi3
dzhZnheweJazVHhtvKYJuIOBmCazRZQIV/PtMYR+ooQQb2370uzY7dXiyx+6qXGI7ZfPnzgKux0q
TTUi8oYzHW3/9depeb77cOWVDR1HJrtNWvSG1sNwVej+RWEw1jVxYUfUw8+m2Z+6/x9kBMw86W/4
FLpVlto7yknnOQ4Y0+tQp2EisROOCCQbaphiDbA+2YCjS+mUdWWaFAMEkr4c2xZs68pMfATI1aOj
LW49lsSe/B7lFHDtZSUDUv8J+7iDwo2BPHxMlsqkdVf9lTnWJpc8fJGzPocWpo4eggkmr7kxNbjr
EfyIIfckM5BsB0fYVPqTm6c8SNY0ZH6G/jPFB8+7y/hgd2fHbJRBrMjPniisXl4RNc5S5AygAoVK
0j1PcQyvhASk4avnyM2jxMNaozYUrNRmEqh1dj0c0GlIdI6jC3DmnT+/nGK5SErjtGZCPGLrQ/ZJ
5NeHAm2ckoBQbFqa8ZS1B0HBl0paRReJcdoAdbXW7vv1h9LtDpOqClBeYqknxuJbXkj2iI3/jfOE
TuNAnXJAKqJX6DdUFOHGiHC9oCezfJunwwpnIowKnM8IlmeK5lK+RCXJtIEuItNihuSmLGET0yxT
7KSAhT73KMzLvl0/2hbblOdcxURz2QNiwkzknvWEtOQkTDMPzD3U6TkPp86wUeQH2SA7ThQHjd9Q
9JRT/5odbQ+vKyuH4Nur63+XD8TpAIV5jSJkU2PgZoSEJZDGCK2TG+JgTmLWcmep0DfhyDJRsHvL
lVuf2wuVv0rsoEzVJ/ZFxcu7/39HcZRM5rZhme6aCkOuCzW/35jpU5SNa5/9Hha2jxR3t9Rl59sJ
mqkEvFWuJBRTqMdyIHIKhQE0gZDOwKNxXctzN/m3TEZBsKPdoMQiMdD0ONwJMNNL2VVADu5LcCEv
JGYvKrbaTy2lhUvN0sCLSfB5htbirYMkX3ywkT7EXN1Mx93wopdX8Hf6oj9JbFYtXcd2SoLRAnAO
4DCWG8lQwF0lm3HJITOzZzEqVuyAyzidphR2+hCYmT9Dy69mamYryiy0IoWW5nRRx+1JNcy5FyKP
dKXc/ozKOLurni63rTSiUnP5NPRF0lfY9jCPiBpp1WRauU6iIv+o5Ugvo/X9Y1i2v26cG2wbDsKE
L92KK8CC8QCPzh1U7KYXeF7T9cfQ3cLb6G8caBubIGv9hxPROgRAIwa2IdXEnMrw6pYGiG3Nkzeo
/m0kVcMY7+8fteG8pzhAoCOeo7G+kdhasTXJ5XfeF5oR096zwyv6RBCcr/rsW7waJP2hvNxavW2l
Ddidv7TNk2AXaMajAmqdBJuste+RXoJQZW6hN2vRyTz8axkFNrDgFu9MXksX1/4Fw9rO5aj1cXsn
IUL3RMxVJ1FbqayxjzqVZ3/W4GGMmpj1rbKHW3tzuXHx7fCQXeMNuM2wNOkhRxmoQmUGOZujjUoG
QC0EjJH8XWUNPRDFSK8iKW+dD3+wBV8iWrEJuWL8oOskrdx71iyg0LEzWCqgaahjsC16rZtJX1eC
ls/54Q2kVVXsxd+k4SOSjaGWSQuGGJgbSYPrfomSqgov8/b4YaplPKymztaQYd3UtZJspeCPM7OX
qFA+CCy9xfC43Bx/6e3iHVGVaT4Ou/06jGY0E6FXVSDZdFXa5dIA6rNPCPUm2ZU5V2pSY14HOElL
souJZPhmHCRfvp3IFnPZOhl75x6raUHiSIVYaZWXODtHsv6UwQb1f4KrVGJGMHiKaZwgkEqBf3o2
i7358HR24t0h8PhJKLXLQu4ZfKoHSKDMAQLQ49bE0qz7gehojmIOGjZVHMbwAmAFMcI45D63wM8O
CXA81dnrUQH82qenaJhPsXV8vJq2F+XFBMrSzKIsieiWP/5ZZLdgmvSnXakkcZMCz7/LmYmx+mV0
pr7gETWHn/dDEw0gtaGwbd7YcsnDjqpbdI/8tDdsMBqL3raXnAGJSbnbpltYH6HaDvwZUU8dif2X
U5Fs9zC0eDE0O0NCi9A7KTVdHqlmxFAamhk/w6vBy6AEYHjsjSM9tlG9XTmuHXKsja0g6ct/lYoX
KK+lT6ZYIbSdsSTHtaxry3xITwtWa0P/MKC11+toq/jI/JgWrCWQUfx+wLgMyBVz6Hy4RvV0aESl
gqAC0gwyW+B2dPs2vLkuYwQX+MGP5r48HdDole7CNqTMLmss0k//xX5LAQPt/QW/7KmzagXgIUuv
ZeoVk0YQ1ADimVJZZqKLwaT2o0h7yj16RhuGPiPrzbXJGRsz54yuTWukZkCaqzD9RfC5lTvkZ7bf
MotYYDBtAqoGQ7O6G3ql0k4tsT8u5asiGLpdmYHJ9PahcV3DN23db49/sd/PifoMS+BWfilQH/Az
Qi8C5HVfAFJN/fT9f0+ZYQRKPxEoWVn+NLoy+nw7MUOoDwMcrZOsIhw3bEn4ulx3Oj3rLiBQZa8R
ZoWQ5RDHuT7abmM7OgLjAiSXRApCkAsoOvR2jt0O/4R+NZU/l5vZFu+Ui45cugR5+v6NpPPwXp0Q
cUGfsO9kcNP+hiWB75I2H1ksfaRs0eiftvuoHs5Y8OERfA/GkpRMD49EOd/QD5UKqHTZXVa12VpU
tFKi1TdpSwiWqPMhTuXV6IZjgXlbdpXVot8lOVVWp30t9WNqTtTJ/16qGVZCsvI5AsKDSKlVz7E7
C7IJV/E2yWgwIAFHJbIm2dq8Q+q9MGL+rHa3ScXBd38DMc3EKmACL0ZFC4Zd+p85hGj2Ro/v7Jh2
QBhpy/sAPaGlNyk59Ct7Am/8ckaqlhkrYiYwvnEuan6xEuNMX5iBIXPzNv/+C9BEoxoFRt+pfLWx
eCHTtTahb8U39ZvmKbhA0deLE2mI4i4hkGUPG2c1zmi5MMJt4JMAfODyxzvB10+K/AX+x6POIYC5
qe2TRB+ZpGBzJdNrs8adKIn3DBihINo7QMRj3T3FP/OFWfcbmyLKNI6r1943y86hTOrnYgzWQEAb
yKQdYnHKGWtmLIu8/9oWDFtzRFIIeN4GIs5VlDc/IXoSlzjtdOtademwvD6vnCZephmxtFS6kvNM
x4Lb+SsF7WCd4zaoEDrgNzcICmtkrsMM9LQN+P60PE12zFm0FuqayZ8kHhdBYNPr60jnzVfdc7pM
bXgAEzf9LiTXLpzaI35id8CMQByApmxglXfwjuuNfkneey5Vu1AwHd+WpxDuxYHtJjedAJ3AMX1Y
GAGBzit9l9RColEZS9U31sW2l7eO6P8Tpz4bZtaYE2UEdq9wDLzZVMDbsiTXU+kyYSXasNZJRiLN
1xRnnMw3uzjcKW1wybjHOipS9eA+3zb0s8YNjomiqBwwfuDQTFUpFfjGv4MCxG70uFUSSfW1/ue2
030nLt8eMbUAjstzu3z10KqRLWSNdQkdhDgEi0rofTyNu3Lf9TEXNCKE+mnzYXNmfYuvMsdt4966
WF9653c/F4qHX7WeARpZpGIvUqY3UAYCUIb3VPk4U1NZjJAZb49BdIRnc4wtWfTr1e8q5PAkfk4d
dDPDWVO8BEt7Raljr42CtwRu6Lf5XrrOgHc+ML/QBxc4n4XSQ9XSfu4Ju4XDZ62cyENtopNPb+6u
QpN6UaCF0OZhslIZqQThRydz6t7JL7NC4DOAWVRzwzFiMVbF+6mmd2oQS50vf5Xog8q47dpALxOC
2xPZiORz1OddPtnqPg9AFzSKx/DGNovfdYylm6HuMjfo851RLGi8VvQQUixWMqj/4qCqTEZ/nvWB
udoCbNGbBJ2YzHmDbQE/oTaUJpGZg6+gh3E/INMrx/cWOxaV/F90/LT+juqLXT5Po8SFNz5IoNy2
2LpFe5btV6ADFlexzmikLNKtdILTWMOQflGDHVZka5wWIQH6rrKI4HCOrzJPv+9/FYT6UNTPaOOn
nMBhbWxpJgxYXp2hHfG1Mz+7LvfTuPgI2H99mRwfAmfS4BZawmY3O2Ia5UjScg/n4Trf1HBa5W/N
YaNlX6nIN6Aave6ARbJmw1oWUBsvxGymTJwb06wFAog0pb8GUq8fz1JTrvt8ytXHlOHPSaIvYULH
QPoqk3Y4Np7EG9OmcHdXCUToQTFHTjeu/aJ6UULY29JNcHOnHC69moJFCZR3z6DXr5FKZaG5qL9A
y8na+9qT4jTv3YVBBGt7XtWfKDhAXUwMANqGNUjhIos62tyKUTEA0Ig9GpNZDt1FoGyYPPP1jMKz
7Zf41562UgCe/8Ku9pkQnfL7PPnDmkEKthlBVXoFCYuNHxzaDk4Ye6qwigtBGiBJYJ16DIcL0DHl
wHgThuGluVJrVUTy4LuZxbrTaw2uJzVZN+uRXC77SRjUSkBM8s2s5VFBTEXU8QSYz5hZHVjMn3yU
dJMCO9onCSR6omHMm3oxbBuGZmPBZQTMzTE7B+hLBaefL/3EQ02E2+mu6tynKUfOl334bHVIElUH
sa/jHmxUApgZRuJvNtFySUPGNHjLy8cHvhSxMoB/GeYKpeCwJGiyEJjGB6byEWd78pyF014iFDOO
0s/t32Q7yVU8zthO/QkecsLpR/SPQ+6HnvSMsmY94iURSdnlppQXIr50ZIk1sy4R5Z1APCOHcbDi
IBMnqnjwpZFfpgi9TzqtI+AuRpSeoEdYjZCqT4YE9ESkjR8ao1kvEKS06ZiNB5mvDr2CRfk/QKWi
4S32BtFZswxl4+d9/HShOhTrylUnMuRSbAd8KLAZfT7qvvH3zyl8uOcItpHa9dznsszN4HF4b03F
A41Xzt+5MuJ2JszTew8BxP5N+qeCPJcC+WygvTnehGjncJz1AIMAB8e8FL0BoDJlNS/ynErHkgSU
5SuVX1vMWWGeuQCrtyHHG7yVFFeZPFHaOG8rfZYVH0md3ZJlTZR29yuVgfTXj7GjxBuUgGXPJ93D
nIgryMH2LxLQX6AZboKLqV5fFxMk6kQ8eo4xzBd4iabXQ1DmE90hL3Gb4gNV3buaT9On6UWQNp7D
E09zmVsrJu30SYTkYMPJr10p6YCgLC7R/D4q7CnRzTjMFnKGbxAMfXymRf89YI/eG0KuQR+vQSrk
wxuMu2pxwrzZuLJFpOY3TmY3AX7U6ZfRCMpQBSNytxkcL95ovw288CfTba6kIJvxGwi7FoBasPf8
l9xkMOgsfg8Gz/4IlGtq5V56CREu4j8s88EOw4Iz3K7Jn/98N096MgF5jWS2VgOSPfcsDchvkxQS
pGyratLDhr/lg6Jrwzdw1JxX3jd4tQ/B4kVDBgJ+BOUEenRJIl3js7Ta5NgQ+/8MbjF7VCGrSAsj
UhKe9GeNpPdeoGiLSkeKmCNUssz0kDNEcZI91j/+mod3QGGlHgE7vpwSHfDytJBp68Gqy5vUN2mu
X7H9eLCz1ZIS3wIU6Mb/w1Li4RzGA7xBeuK8vCsVR0Rl57uKlQAx5vuWY7Gpr0P5M/3qC6Ilv34G
i69GfvOJCWuT6HevTUxAJX6FzxthLDK6A2TIe58GJNweXJEssAB+od0z3YzbhFovFBnIAc2FYYpU
gH/cBPzXKaRVjLBk1QHxiG8bMW7Ey/8Q81SS93SZ1uF8mzqiQaMreYfj2Rz86oyAaw8NWf8S8n96
UN+HPDnmSipGlLMgQ/l35spK/ZducuSQtyApXKdOmkyZ6SqeTddfK7lD05XlURdENvF/dBVKt7YK
LiKxxRhPQ3HklwQf4eIRNqxfs6TTwiorgnPvEIaLuhAkYQYxzzvKyreJx1H171mX+u3TGBvZWtrR
+/D5fnP/MxqBokmn18HoFiAAoNKXrTQoLqxIwZHJBW3wThFytIwWgRFRzCtpbjCVdANPxK9OOqjN
3uh1nZiJPja6S50zLsWig6DGVspe2kgL6RfBbLEssrZWdVu+QO2TjnimipyIqooc8/PWwB5m/XDK
StFpY+syhwXHxEjvYyg3rus8qCtDj+Ly+FpuM65TqCFVqUIWTdnjm6Rvbh3axxdHeYecO0AfopiP
rdCiTbbVPYIIBfqHe9FXw5yd/JRA+pVCxWdyiJe5oUGwqXWG8NmssWrJleFUdHnJnywAzPie4iF/
qJ0z5NXsg7IAsZ16GDVYl8rxmbCjQAjN0DTuacYZdOvRY+Jpy60wg3VUjOxNNtWM5AUHiXK3+Aso
2IINElscgmW1a0pMCQnEBHX+QGilcviOSCOlxNeKcKL9wPnkAkl91D/RrkgWLTwyIONVcIvTXBH6
tmqii1+6Qvqtl+xq1tsMGYYyuzzXKkaawfrYkm6a1UIIm2AjoAbo4EcKibrvoOThEgnzwSWqpLLx
npuhcsQnOjZWr5xHu8Wg5LSMb5vGvHcbPowgwgnale4CSx1k7o1e8Ujt4UBJ2uUYZJek0gQjZPYm
EjazILU6qYFyXPWqHQLHxLbqSYC35EFrsYzxBrMrwgaOtAQDFh/Kzl5zbV6D5HbBEjnICZlRBUe6
J8cZxBXFz+QFp6GrWz7fci4JGa/xVfQ1EIyAQav3WeTZ8OMvOczuWhaPTBNQ4JOWCeTRYxAVFCQ5
Dp2QG8EBkvbr81+qlRYjqAULSe4MbuCvKCrOXs66OhFB0E6510Z9k6OpK1tribRJI+gGGAmk+txS
SPzJ2XiCRFHIM9w46QgsUj1c5ZEDkDX3gnm+TJlAwowih2hwbWlR53ZM4p+PDD7HQaZ3rY6h9ERM
ROFrnLp3j0mQNWKwoW/PFb6As5HAhlss6zLTBYghpj+GZW6tLCHkFuLe40Gr7gCu2Oh1Z1wWF9hs
uXhw8vMP7FsEVXvMzUCun6MP7JjkLFpvOY0kRZdi6b79loTIrKcaoqVy93Bws/8MQ8Dk16myvwCm
RDFswTMEODoDvFZDml0eBYCnMhNiqyeu303edFrUsGzm05EKcIoCP9GbtlY3qRrEyEFLqnVVSdJH
hFIVgrLXrKDocIAGsNPPeJPrA3cvYVKMMMHRU5KffmUEvHJxdfprlB+ltjV4UqQqh0G/41pTtt89
M8QqQ3vEDT4wBhkFmehts9ITS3/+rogNWtYPO6jTV7dNF35NdKoy8oUU/elAjzL/GslSQN2WLLpg
GkzfatL8xr2j5QO1XaVidqDivRbD4tctE0oaGVqWKDQS34xJL8f0ELMSS3A1agz+fH3ZwjDJlq1J
GDGqiSRffW/eI5c5poXedbJ6IY2yuN+AKmRNPqmhHq4ibZvqooc6TYa/pA9Ca6V48b5Np4KVN1Qa
daHSiCNcAdnOSU2PbLLXsFj5uKprWMW61XlglqFH+Gm+Rllip6pBpthsgAtPNUR2F/MEBQ5/ntv2
AOEZIygufLJsYQRwQ2EIgYbMpdD/P8jsUvUMpRYzW+y424pmEjFRPrOntChR7x2+Ow1swm5FjbX6
i4ps0Az47OQ6B+/CUZDPxOs5SyhuIzPO7lJtVLeZTDgJVOs+hmek2TC87/qcc00SahdrCOfxnyOR
i3IsMBnAAR0MrfBS9F4aOP0sa7zGBvtLoXFIak/vX6tY/369rS0a2QN1LKUdvw4Uw6kcgDAU3niP
c/e2sMVb2ldREpcgo0bkghUtls59TL+lUjDcfzrWKkl06SQzIVKe9vDY0wW1VLmJ+R71E2Y3VjT7
fdgx4QeptKIM0tO627Q45udKUIxJ3EOcKetVMc6rsFPp7dWIHIanxEhuQS4mg1eYs6sdyrqH0/yO
ZCAc+3BGhZbRlf0rEhiFU//0dRXS5Yw0T1yl/r8YFlrWjhSkATBiip302eIbYVlv6aLvazb+P/cR
1vhNKpZfiRnAaXErl82LKj85wzGJVFJ4RnFQRaqhQFu3xULP+4OIll5k5PZJqdGlfe3yI9pu3vwH
duuxFS7nOsDatpzVmsRABZ+1P/UOhnsGuowukxbRFzWSCO5x7UytS8r+lv+BBojth8RYimmI5hZ7
p+38dQOyfHIKaEN+n1d1hJGDUjVkcUfCxuun2Cc3NKytNDEZ8NgHusDoLBggwzZ0EjOXfxUEkiu2
plxu6O5ulTP5/pLBjKM/wpTORpextYDgUWLNCe2Dt2Vosw7N5cSz1yrmKu6OmHZj3joaXLWfzZsB
LxYBsh4R8cShQb+MDITrUU37eqnqsm/ulvKTStjYrQnSAKs7OhO8J+F9+2K5xbQUzJ7ERJFF322H
M7wgRbZ8ZSXvs5RGuj+Hnvw7FVSIzW71Z708DRy0v4yKDNDceE3+RHwMDPd8wFrKlXWVkhLolifp
DLIQvLOrKQMQeWT9doSeLJDDE8fO8Tpi30gedrM7fHxhesqjhqYnUc9SKzAhqfdTImJuoazDWleE
mH/Jcu19OtRGWIU2GjI8zsutVUSdH2PS/t9sfKPd9B1X9CUgPOiJZYLwHfFhdcNXyAYwbibcbOHB
5LX/bejGCZRuNZMDjIfwPPvFXWWbMvrwWPhl9KJk/R4Yir0IY0O1vaIphZFvS1q3aDmnKllxndu5
OMbiQG+cJfLNOQnTIT6BYWRiSapyI51LUY77RAgzwgxfrKHLJaAdbIfAVDSSRlBmZXXuTGOSFJ33
Zflnj+CugtBB2AjeF+UhJuboL2HNKJ0e4lNeJZH53ohRPeTEevCxJw26ibFA3f8ZRzY8vPmhX3xF
PIwExa2yBTWt0i4Waikm2ar44rHiNDEVmqFsGMMSdYB/p27fEKmk4yrmy9XEZUnRkqNRRk/tXSIC
+MVuCEeURPl2dw6tKbJCRzO1tnMKdfCU/ahDnDvjD0CYGpGY3SeGA0L9APL7DEgNpQn1Agbf9eU+
kQi5axHHpKmGv6qB7jGeJrjjD7DGDnv+2n/RvnIJYz3Z1enc4ELYIZ5boCAfns49wsbTatBvXLHS
6t36g+OGiTvUhMKQ9M0XG/WQ3bcWSeeHByX7I6Pr+07v7rtMFkWcRzNZzve1Ge1Sv66rtcLSA1P2
pWXVkS0JnPouSWg9s3xa3LGmvyQRJ/XUOkPxy1VhKDTll43oRktQW4+pk2c8uwg4syLvqPCTxTLB
LcLLdOvL1OuZk1c9wF7KcsWhMo7FxVJSQ6OgmHYFxeIXad1VzgKa/HFsTDo39ZUEgvH62ZIrLe11
k3mxg3UyFpjtbS1LVZrpGAN2YIqH1IigDOx1bXRbVRuv8CEvZy7X1FwKv53QoZWxEeLTl8QB26Ja
Eaa37EqYAxEkfyZeRcFtAFa/14zeFhZU/5w+LrOMLrorukx+0Boqkeh4y5CQz8pIsZQKsPbRzOj0
o9VEekKaqRROUnf5bAX19qnyYgdx0n+0QwvcVH3iAhffzhAMACz0wEog0A3k/IlTKBRuNs1yY1IB
MlC2SFA7OOp7dkDEPqQ+6LaCrbJgA/ClA/i3JuXE0BfPYOcNKnHceQMx3F6BGF9pfHAbvr5MC8I3
J0mKoh1TwsE5RDuIpQaM5h5JWi/xGwSJHPcHhXzVD+U9DE7NtsOdGWGcvbdeUvL76xa1qLNJNA5k
NC+WnHa3U9+yA8Ge1ueHOh4/wExV5IosKCRi2LRuFzj2sfBV3IRuG4t8+/gHOxD22GKw3/WiITri
KdgXEereAb4O4FZxB8JTd6/Gw5GhoAwXV0EKZqQBzOzb/j7yEk5cZ9+6NuZWoJvG5+jIj7pRqDeX
vFizG3rfhxWXOKZDvyqfsu50rMY0m65TkYSzFBdapjz6a1ANBGv0k2brv9G9hSwkkS+L1zFEyL4o
t42D53DDZ6d5dIsQE6Z1Ct20I7oy6nuC8PxV1pUXp7fRWayp7dKeD5CRWiStb0rVwDr1OUBgWZxC
0jvNIyr1z3Y9GE0JixDTy9hOWmuzZHlWwsLaOrN2Zn7EZCAKC3EpvLOI4lsgE5B6Kpdcz2kgql5u
HZwAWgI+JVKDF8aVRqUoan6XC86JvifhzjzmEMDeuCs3mFm1fdIOCUC7InIt2se1nKtrYEqLcGQl
3Ukwjkcupcm7cIUvdNwSYriESeR5q2e4Ginq3FQ2f+HRLosj9ll70A2gnPGkAf3wsCqYraLHIX/V
LrviK7icbXGAMtNfkpWjM3ptvsz+gsCDurGHv78MOt/USpZKynwNmAx6YklxsBrzIBBxxHntL00m
uaIlszr4VNsJnFtcb/ko24noF76XzbrxS8x4Ld6r+2OKVXgVEGQzV/OmYkGEd68ZsZ82bdMjhnfT
SAuFPiLUMR/Fl6CdrUSwk0UpB2bgsIUdjsiWKpDbjbW47bXrpZcpGZyk17Mx7MdZEUVKkgY4Ovmz
ALNyvjHyYh0LIHG4FBDobmH37nuGQOV22EALJYYkdwRSFsmTrdqIlh5EqVds1Ja/FB+lY/s4O1nt
89VNKJWRVwdCJXfOLydBv05dLs4nBC0KYdo1nUx0JHpxWUQp2C4ppzDsHieaICBEd+ftimUU4W6H
xNx3hFCyMw37Kx16ivqigKJOh3jipXTEseBSAV04oj/g6OiKvG5PeV6+Ede6I6R3qBn43Uv3+edr
HPyVnDBJY3ZsiU9IuFUPiCwu85CFyFpsOSEAXSU3QRS9CV7MQpQMSwwiCPD5lDo9aHREpvUM0HHb
pmhoyzjuY7vlXC07+M115QIONciSQRVhDil4mP9XQ3sftvjn7QaRz47osHxgfq7hh8ubJBmIcQYl
YaNTE8mColHhocLwt9/G1i91LglGl0orjHihkz54C4kVn5GykemQBd6LB53DtVQRCZmQJPjtRnVd
8V7ABhDikVCmzxJO7hwJZ93xp89Ew84MxI4ivg8iklFje1BOo/vqZ+7BD1e1CpSryerxzPPJrT/u
AAGx6efPJntog/TxfnyIhtzzDNCQurgmlEfRkASjlPQtNp3v3olaDwZiPcKZyXPDp3Bb/33w9MHN
3AhCJAjxIAzGWKoYuPwirvsuTaIux3Dia5MkfOcb/ju5IB3zRmdMn7GqdlNIEPE6ttyR0+jezRZa
aF3xPutDtz8K9AcRl9TjEeDNQy6OMXqNLvyqPo4yIXIZem3LWzB/SyOAeTAwmPq0EZbte5My74Eb
vGQCCG8VCgLaqPtm9pH3OAtLVXkAhI0M0dUEidHky+mMrsgK2btwj0TZk20wmXgBlF4bJRFiv+/d
114VWCjwQrAdQR38uuOChFa1TcMmZ6R0P5dqcitFiUkSTcswXtSLcr0MSn3nzaiqjEuIdMG1Sv0I
V9r7YpiHxAy5NnJVLinFQNgi9v9sI22WjOyze6YXKvrfj4mnX40oM1lMHggM6GGzAGJ9NApkkvPN
SWiQl+Bt9fDW4vQGUoBdam0bCnG1gkOCEFZGvE3j0N1bWGqOXOtBLYX0gQ9XZ5614sO3VnhAH+rv
PUlt3DiP4gsTG3uPWcon9rTubBb2oXioqOJ8a+CzMOXuiUN//9BGbzzGeOdBMmeI0vScHompgiAI
/j5mhO+K+HshGjVvwfVEKnVfhc4qId70AGca4i1rcnTWdKYu6ZC+PgzqOlsmY+0V4hv9JAbYiR7/
ufBZNKvtXsrS/D7gnaxFf4nM3js4CArnzvJrLSKUNSAE3Ov/1vcRQc94NPh8AVGR34hfh4nXIzrJ
q8tY9VdNk9njfmSgIvzRI0xfrAXm3Rs3GFEuum10hHb2OroBeLE7jE4mbGMUkGQCQzm8QREKYHlK
K4JWtcic21UtH4Ud2o+tcNuZcUIMK6P4wby6mttvSqXsli4ohSJAwd5Vu7ZGV6mwM7QcLddU6Kwi
wxRFDsErPNZ3iZ2CeIoCVBPh44NvAzFJtYkZ8Mg1HKpcfoEL9ao0wWYdaUL0jxRkDmFQ5utjrSb2
EYF6Dzrm06u9NRIZ86jIBQ26GuxRvY8/TjaL7sBCO4UGmwXnc32ps4yM3Rv7ql5NzPtxKg/JAYH6
xQZjc39/C0f8+y6FTVajpk92zvgAc7jPpUXUMUIv0py/D3DXxH38tM3dprZnw7c/CMHRj/Mc3dZi
yDZYfW3fKSsuicA5Gx1zKAi5XGJwDhjC/xcTPV2rDqTE9EW1BjcwNmANYGqz2/YHtm4L/3ZyoiHe
U8S6kltFrzzfFdKd5dndgN//Cy1f24W/2vPBsiL2aHh2SX/O1NMR6AELfYb8HWWGZ+q+5FSpDaoq
Rd9PJAI53AkfQIrKWBV2TgTcBdFTxqFBPb8rCF2NL1LBsdgySWuSHtV3ZeVzxRn8H2hPw5OMrwdz
L7XOzsw2zAaZIwxAa3XGUQpORY0Q3lydn0w8geGATARQbrYeyWRVrlPI9N2SIC+08Gwe3qjIWoiS
8mn5XUPiKjBSGM8s/zbmK2GNsXda3N4yJFWdvGUXKKJwXC5zKp/RnotOennwm2VHpehlHFKPQWsb
36cbpfeff4eWg8QnRcx0W1ZyHhH6n2JgKT30QnPyYbS0MmPERf32WTM3SEn2Z4CLqnJ2J7+/4cbN
FhaWyF80U/bOyiSzAF2jrkxhH0htHVcMhTWalt93dHo8TASSvkEG0aqflQo1nDkxXHtMYGId8sff
EuL3NQ1jCSjGPPsk8iaEXAkKTYVIx9EQpnlzKM325IVsB/d6jhaC6QVR0dWxQz7Hz5A7CwQO9h2G
vB6aFpJWCbAwf100B48SGCsvjjMpQyRitvPFKGcvYbGYqjMxFx0z1UEkIXV5ZOPWxYGOWLhZQEB4
d+Z7wii2EfqO7wVDmnypYqluAr45I0xFnqIMwzMNoeNCYa208fVaQy2YtJ75Fg4R2UeVX4CF8sfW
oYzYuy90zQXesBZill2igh20+T4L7nCVoYVvVCCkgXSiaEJ32exHtTb7GywPFE0cSM/AbqlB8RTO
4DvGPaOn4RDZOhqVw/nVZpVAJ7dFxgvdwLIegXt8ojRQDc+ng50yFz8YJUCDO2GEwJJ2qv/eNgiK
5ft07mVvS1pgF4n/3TltKZ/mBewVjRH1qqCYRucYfhvz9RGyxzd/aZ0v1fNrEuefkRmhVYkN6N9C
axyNBd0petXA0trOq3fhNT1lKvlYI1KbNOTLPIb6hyukgvFrX1sXGP86YsO1vZ2IPTrLigJlyKi5
k4btfH4MZOTCoQ6J7b+HvaOy1MSG6vx9KzCQKvOUIWkv4O2ezVaI9ZZudtD07x2i+/37pH2Rw78e
WrK9U0xsTcORA0g22nJyRGau8tz1CfJXcsYF3ajde7ZVnSgAvgJLjqJuwkTonkkLeLD5PZzPb37Y
YdEj+UHi3oY5WCTLtxL4ro9QmsEG4p7NjryxRXoDeVOKAPDfJzCedQLSy7r4gVyyvBMhjkkCYhWI
U3/e4cnmDujBY4yhmQB0Zf3MAlkDdv/UlR5vyvUrK5ne/LoRBwLAPXNKYH1YluP0AEBf72RrSX8u
d5BaFpCRKn9nfmgW/Kg98fYERgz0scFZEz8rqYT/OJ/u5PWcTRy7Vyyk+9r4dyPuu/R92Csq0OTT
0Ntkr3CD3sQ9ygRnRXWugy6Hnw4LOX5fy0yoFvW7dLIKI93n8Z6GL/sPaC+OCdav0U4BcT3bAeK7
jUOD0sytEVWsobtfEtv4pGoKa8KoD+BL8AHTMTofe5iFpd8hSPosBmHhw+4roCRqRMUFlSpcacNP
5WCAq6GbwJ3p5yU2eUqmI109Q98sSafO5ZclqgHiuUxqoMbu+z2g22COVkPfByUXfjeFm2lM/evi
s9exdCn8YZfWNyeX/IJaN36ba7HtfNM1xH/AZkf5u5zegtzBz1SNxucRJCSpFXp0z8FhOEvsXqZ1
RaGbSs2QyLJ9EQlZgHbFMBu9q8LQYevtGbon10P5ALLyQ0ptw6s1IfmO6dAJ8YvTZ6skRh/pxLlf
OJr0XQU4vPCO+Gf21xkWhGeDRKRfi1ySbKAZA9jj7WTk0nOY8GH5q9s+pL1oU6VxQBpTixGtez0P
a1F1XWPDBHZW3/UHW+e9cnac5N2/7SJQJgv74N1v7B+8tYfEHuanD/Mty3iFBqNcYTwcqQm6ctX9
1wBc3vC8pPC2Sez9kxwSNzD0qYhvFmKc0fBzwedWZqiocSXVDjpJbpk2d2OK4nvPPC+TnImGDR5e
0acluoIflxBrm83uWBcG7lkZHbgiC9PSGMgNa7BZSrOw+jWYafeT9faNIh3XLgjGwZILRMJzfgPD
Mt10uCKom7JOv7mmw+u9XBVm7cm3Io3CcknlBtBxIqLZD+VUrAjRaeJnrA9y9arP2YnvtHsazZCL
kNsDw3z9xsgm9kbcglncStz3e8jyG32nqwTGX5iqWrS9Kv+KyrPXCcCubKFbr9mz0bfFiTLIhq4P
kuvBsnIoBPPWYNYvHctvbQZyYh3Fqw2J4mlcZ++7hvjtaRncITcUNTioxO9NmCttcSpbsNqnipWm
7t9nbosvBAtA+WVF8N0eqx8X6OSmlRaIxgvzUxbB+6naFaY8uzu1B65Np9nxinMtGQ9LHyPzHOgl
7t1ij9EDIYXV5/ekkl9agNmssA1l05k4ktTZeVu8PDO5jyb7T+fll3hZb4F9KmA2Hi3X6dx+0qOK
i9F//jhohxTyvThwmPgXTStgdXNFdXRHt+LkXDzzQf7UOAKWHcq5yzLXg4TVbwjZrzXkqZWupOO7
/1XOXrMMwtlG0yTAeXOzm1fN/V7LMwCmRUIaRQhs1R4EWsbB8ybzu46xZwn+9kwVwdRVwFo7apSJ
YXUmVIDn8jPQ3xhTfih8mX/Bhu+tWNBLpBiPPxEcK37RMvlQcmVJaUnfvxHyXyAI98Al71Rkov+w
fxT79l2+88nvxN6O8wXd6z0U3qWh2jyeuFfj+jinvdHtncNxAPseY3Db7ljaK+prq7CTbL3TG84z
ZCgO+Rw7OKYd6z2OOu2fnuTMIYLWy4cNY2in+bfGwQ1qdiFjGWyGqazEvHdyqjZcKWpcD6JEvzIF
gbmQEW8LWQuKrl3IYDlhtH42uG7vNxhTdeQwpt8MvDN1UyHYrrYAKFG2XtpSTZVwWzg2gTYZRQte
uwTaQVv9zxmjWNm/puWI/KFHS2PLYFpJmI6zBkhuDYNCt4a/aKwtEMbbnchmbkDvIvZYX0kOYuld
qGQFb1iecvaMtlOjXeh3AX7vQ8dReji8j5DkwOyCx/D2xxNmoEo/Abu3/ZJcHqL9psy7ofRp3vcj
PTO8AjEIXDlc1BrfiRDx0M05aRSj63Of5bxNvHahG7zhWzjNgVVlxKIgvo4EW6VzcTV8Fny2A+fD
M3ZCkENW7gLymClbRYq+b61G3XHlo6LsNFwfMjl4/Y89v3IIJUCrnw02Jat+n/mFwo0PM5r5cNZH
huiCTAaQ2FxdrIGqW72u+m/ygTgEW7SxZ/l/YVBB9TCtcyK+xUYp+Cldz7DjeDzBN4v7v13zOWez
HnsO6OfHfbxDEnvTME3lrAUqh0d5JswtOZ60iA6p2y929XOxnbZ2WhKexMHQwKbJkksW4kzG1WCF
qp4YjAv10Qs6PgXmvrge0jn7Z4mo0+ucK8jQYHiadlFId5Sv9npIncVT0ouPGXIvgUKatttUv0ji
XVtIpPn66PzGo2fs57YT/9bhgrxoB6sqhi7UFDYfYWOqXVeM0IYxDmMA+XV/WETShzo3iFCBES29
ZbZi21K9L/xndLmDvwrcOfGtbHRALOnx3j8Y3H/b9HaJBchVF4UN84qjpqyqMHTyi7DewjA1JLM1
ntm9dg9Hv45qUT7oU59gCpc8+HHOvFf941vMbvKiFdB+haKtHjrhK3mw7e2SMO/FK89Zl+aderuB
Iw2mu5MroQ5GOhh8zbu838NC6bH6xH5OZlqjM5Tat+2dWdO8Iwvw/9vXmq7yhniUTnYZIQIB7SSM
6BHt0d4FAhiu4rvtU/9tuoA80rPaaRzBzKIwtcRyUzFpDxMJEmXIqJCiaHBRBoIPGY+E5aLLT4+z
SNMZwyhGJwhKTg2vp1flXdlN15AfOQa2zMfPQhQO3HRSLmYr1Ca1UiOxmH0/XuethP0eqSP3h1Zp
9fRD2FYkaM+vt+qmN7qWglvRPSD011E423gprvjKiBZBxr22lFp2aIKW/etYwpJKzY1VuoPIPIZi
bmn5vs049E7X/dJz6KPhDPiQ5PSvyz6yzUmiB8SeCl2yldPYaUHj6evOgpWk2KF4KFtk2LVVdKf1
9Zeu5o3iixXOoDF3R2QGVlAeIP62wkss4CFGar9Y16x4AOsgPQkKo2PEWS3VrLzk7Lk8urX+ydm1
xKmqv3gW1J4BYlQoE87URdp/veNXZxmRb3DEexZBZ30dr2fYkmTt/P5Sfki0743bLq1HgUtaClTt
uZNZ3+3uXKIBTdq7MKLNa+QtTirsZ1yN/U2gK5++IkvJ1RVQ8wgOQZ8bxLBzP4DJmHc34taLeqrM
QcZubNP9l9xDNmMmxvhQuPUM+9D8dw3KipCNFTLyRHimbg0Nuhvhs4ia9xHSQUn+/4qoHRbTeAq4
WgooktIcnNFaHXslyLem49MXEiFXByzt6xFFrMLi6SjtvHAqcKkFGUnMEREH7h94Jz4HXe/qV+jA
p6UE6Urce0PyFugQjDX4y+Qfa14/F3spJuQxEz2lDYuWVF3gpA/OjoKqLe5tRHLxHcvVkQLusI+X
7XRoI6aKNbzoOaZ3g6sSFJMMvE2h3J2n3V1Kdb8qVfrpO/lu3FqzNk6NQkl+7nlxSSfNQN/4cI1P
A6f77J1vr9wQ8YM6U/mlc6lEW0009+lnTmbcqmHVQ+fseqcc4qGB+xU10jFT/wMqGpUqKXYNU1tr
VPslCRzryunJT6cy9DR8DQ3zPKwNyopYfAXYbAypf/xxtcst0T6D0mZwp7aZOe7cL7AhoVyoxCKh
iFG6d5Cm52BAuUl03eNh+O6V9+41JlQevhp4InVuDE7Qn74Qua/bj+2g0xRpY0W4WHHmrYqDpihd
SK1P56kkzNmC4vhdfS2JrleefWEedpwl6XqEc+vFMYCGSODNP+S6zD+LkUTTl4iZMOR/jCMwkoKW
kd15S3Ez4pXvzDpJQlGm2WY9t9Xj9w9HRgKAD+XceeMLpACZhGGFnXPxXPXHDo6st9SFuRVyLwl4
u/IF7rIvrmjIWBnFJnKIiJOKSrUoWSuyHHNedit4KAp11DNUZoy8Fop3mX/ns8an56YsanYwhOs1
xUpqGEJYqzw2Z0aAEzcRq8KvmdhTd7tki6akCDVYZPg03HeXOPzpnseOkTDnfpTMWZp4VuKziJTd
g3LJO7fsWPJc3os9GMHzJj9G7/RnW6gUrAO71EEp/rvGTRA9d5KJbSxJAdsCB1cRXsCBf9NKKMmr
2FovVUCgGzaD8UPUIYOqBkJuC7uFDps4mXBbdAR9csOHCcJ1WGw8IDvQg4z5IzattOhuM1zilisE
USN98kPcTo600R7A0R2ycXFykwfhcv3K5uduzIcd3ath7DTebQY2Dl0FxaHhlQgyZ71M59pABNU6
z2q0LySlc6JXIboEbwO32fWBrUo2AW93AYp7ckx1RN59AaMuJWzvzFQWempyqDYQl1HQqstEiTvy
2Rj/0Wc44Z0Os9IkccWqsnmqPoBbhCYm0tCNd+A/bZoS48Q63azITPGwuvHILZaT9dB8cbOiQtUi
fxDvHDffTKC8DwcJ8X1P4y5RahwBvYtuVnHsnldhycsnjuUoL58orLWK+nGf3y2MSNuiDVVPWoC9
FbcbLODRojZTmhYy+Hlz86TCc3xv8Bgvsv0urS87jlMIES44zbpQ+Gvjdx/BsZQk1RSQF/ZTXOQ8
V/4gN5WywB2oOM2jpD/iPjDd3+eFcd51PDA7riPfygyMOFGNNKNgYvzg0bq+iqwwb4w814OdbQ0W
iJH61E75PE2YITSwIFkUPdIhF6bxT6sIy+bjTO91JZ4v3HReNJ1Xt0baJFAJvWo8inMCFQM1QopQ
8y4wj8UhXfk4V2QIHIdyJTPd9RT8xATNyy5zLOju6KgB8OA/hNsrxVvfr27IpsLTORJplHSaXvW+
xehMkS/VPUkDjzie3+bvjU1DvTF67GNi3XdjtyBYD6xXkjQhVbA7AIJTN+9vwXd0NHpzbWdJtmED
iYhV6vIqnDlFQCwgm6yt7BFuCnhJDY4/HQ1/88kZHyy7oNIsIhpl7GG/bTHb5RWD54V+VDiQtqlN
mUnFRjdxAF3ZIJChSCMvekjAl4gVToof5N+DESbTt5CUo/8FyIP1OREO4POuefS/pajcnr8otYcA
qfOuvjAWxPajeaYlAl7HwLVSo06S19Od44m9plfKF6Q8ycaFhSO4iUflgm4bfrulNaf0QfYDHmF7
Os9PE5TmgtKgB6wqDwGZHBcNSQCI91b2kbnCWKy5lUPOw9c7uWN41IyBdW02t6sC6Y4nK2xLvsyk
J8BNJLN46C+xK3k9jPAl7QmcoV8tiIfdTQYbk7efk/bzvT1qLatuAr0UZLIOLseGFfqK1ZB5PVJV
bcCaGd8++UyIabOZ9SuTqiKo5nhboyLFEd1IN3ehtGKvGplV7ktDk+QYC0knC+Ie+YzMKb6evlZk
MPtI+2j6ABEBdVgTn3BKb+LZfSJPYhlusdO7kRlUJIumK67YNO4nPZc1zChwyZGDlS2xMAJuw860
aSX1Z/Ugd1V6a60YJfSd7dQLdW5pUasHuos2FE6Aa6arm48eudsKpVKGEw/gIhkvbj5bKL2JzZEN
DLIbm3Hr8dwFDGBgH2NOX6ujrrLOU7zfJHxz6W0VBf3t5t20vWj3fT9kMMXOhqbzKaKqJe6mBrKV
01ndcToVlX+b816egpkXAyUZEGHJ9ZTeev9Z3Orm8as5GicGeJRt3i+h136S94aHIhXvAMC1c8RA
VHC5qltRx/Yr3HR1fTcQ3FeXY6RD32jcM5rbENlr6Djdqkrl1i3fO/a5dLVM79dzsQGAVnX+/cFx
q1BmRdYrqlHnveKvxQFxfvIo1aFKSpGjSq4IB5rwMwbWZEZjXK9jZ9WBFRUr7moWqqAvyklN1p+F
KeqybMjNpszodSGMxPG6kGW8XZkEC1OGZRqeDN9H47aqndW1LrN00UNq7Q8lRzG+iugON3mYo38u
GQNuL+I3enLsjzn93CeAYNtFKe9kZHlOFTvCGoTCIAQd+Hjtc3UO3uLDlWQcdR92tfCgkidaTlkS
4BKoqkpIssiANG9FHytIEJP9OiZHT1odm1s4eI3SOS4EOcn7/29UbmFx3VyNAhwYhXNtjOA+SgPm
3gW6aihHQIl+21MtwoogiuGBR9TALanJc6yE4S66PBmZ4+eb+9BdYIiKPtC8um6E7Ux39pCn8IPX
5k+3vmg4GjGB15UjTiMETH4scKdwkzYu4HhGuYpW0lzqKITt/AeH8ZM0DMCoGRO1tiLXtGuQvLaH
IqyVuWY/k6hxfU5G0QUf6E1navddZOKch/QBIQLMIIBd5y+wVqIetKU+iOuTDJdmoE3r78yVW7/k
RFxoG2qAznQSr2QkOpFY+AySnXb8yvvTV1RTgkFsCTl9zGuLquGb5dUDoTOw83VDqJsla7iah4kF
59jSJ6Bwp863A0n2FV8ySlHAk3kSeVWp9oKSl4xVxhT+NRjK1rRWtvCEX3s4qAL8UIVrjHQs0AnR
OBnoOTwJuSckYaEzMleALySrkTr+CVWtOYvdCOt2OmR98BUodWjpyo0ve8OfJ42cNMDKtE4M6zCn
M9EG1C3mJ3nflI8PeryGb0DJPF3709gW/pMdgrJ6XABYTmhwHVzX5ZS5cdFaHy1JSo4idH99izE2
bYzRdt+8F3aIWGoB7t/LIauZGIkHwuEbdyFbNhalwN//TTeL59G1JnsJYmDb8rnNtyvJ3fwIX/cf
CKaOax7H+V8lreOOkt2wR/RKgehIW9LPrPy+CFQgJI5Gn5AAFE/6YQYK7sTU9KTzfmEqpFp9dL9y
ZVh+S1KqU7Di0QWgFGchV+25PeIFmRJKllvQ8ED1ya732hBXNzk6A5UfWPATGUexxDAwHgbYXoPj
eO8vzEQkHCJtoQ1MURfdOxoTB9aXhgKLx95exVnEOouEeusNhYkDA1rP0Lu8UUnYlGXhopmT/fnv
lyB3MrNnHlJdfglZjMAlOBwiMt8UvLK35TicFnNEk5PL5/TXguv9WpLpMygNaV6f7QaU3D5bW1xZ
YSlOrqiGl+Nap28w+Cz+mujU6qeATj0JWErpkxRqPdhoBIxB3riA3vayvR9CwsyUdyYg3w10+5Xl
FTQCp2XXZAg9NGff7asdJBuiRoqlTnW14qDszaL/XFKico25dyBa/ezT8qwMndxiHZa+1qigRXro
U8qIVTbqJluVuGKSYWlI5wUeUgmdbse7N7S23TX3jc+6t5QCXpQnMNrP1LzfAA86in14kEKMpQ7g
T9KMq+vMG9+Tc4qj4m3n9Q0MtHbZLEUTJ5rXXrz9dSAQpcUp9+VPYGGeD6EPWcgTbTq7KzLxsJy3
+SsE2NjFroqp2Zgzj24IRlED8E1QEBOZqmF8XwAitUzlO41s94o692pyGXPCphdxGfyiM5ys1qY6
JBZiKMXnsNfQZU1f+eGYOfwavpIqxXWXwaoKl9OcJUGnYcHF70eDGq1N8NjLqVYtjVALKP5OG2tP
5b0eSIHIAJySIOcxE2r7ZmCwlpQ8BtdWoYZrBVCP364df0jNwu3+SXWtnBPtb0mzM5TtlhoDfDwX
FqewpytLRHdJSnGgjGcXHvP7AZ/whSTf/Q7wQ9C4iKnCEsjsk7hli/hwY15zpLpUjApDHU0y59UQ
2MPZrY9PCkOmaw/Fy7em9g7EOnv1vJYrwXXw8TXqT8uqJ0MuRHXP8K4bxBE7mdSwxbMt1fUNjYOr
WjVhb6Ih+rO34kvcq/edxAvJWTykqLbv8Jrdb0RohiyTwlQYV+KAbG0RWYbNM2moPBOY6scKPaCK
ImfuBwbKcpgCPvVzv9u8iqW/QzF6JBLyD0cUuzbQcPmyj0CmhPcHcWrn51kK22kmP7Q5AJSTBdOO
L4I1BSuFfbuB6+CGM121A8k4UjNngtb4ofmDK+bLY/R3/uWzVihGFSuR+Muwd9+zX+FCimfw6bGg
r4wK28UK0QZupsH22JQ03KJNvsQtXLXnZu5qngAi0LVNGlvoAbPXYOY81GDY4MWdznz4I9x31eTQ
aJVk0AVcRWzQL7RDNxltVHSiJ+CZt/YY2sjxAzB7vGs+zcaNFavlyd7VEwsQp/s2yakGKR8X1GQR
9gP1G2BqI2guFnv2YphqIXpggf/kZsY5AhvVhbVSPr92O7njJCIW6Oy4JNZ7T4H4xxc+B0Y1ziSk
N1sgijo8rS7eQDlUqrXTfe5FWtYokCQ5vgctTXffeoCQXEUgFxrKrzblUvE4OEliDmV/VfjeoBbe
m8gUpwrDsi19+umeoyuidpjRW/nl2zbY8L2SSYPk9jkAvBnNW/77pzj/1tAFmff6ItZiO3FHVXUi
aAvhC/uhI/Gmv6gUR8xBk66VpDb2K5lWtu7akIbQDezOmvkLMEr5EPqNaYSXHvJCsceBy+n715gx
1hwiYyYvVFeBQh/LmC/VU8XFyGOCTHN0CuRAtA/94MNqQQLBvDmYP6JOgbIR64sDOB5DHQC8rhZE
s4FWb2IyLQMrLzIo/2S/NyFRdKYrSB6ccONDh7StRHXeVT9OxgJy8ENQ0WaG5R9rB7Ay4mzZM5dO
XRrzzCathXZ2cPbzKb6Y1cb8uBXJ4evksdtUaG5EvzhMaHCRrGu4HVxY7Y1ih8uuTJKPnmBVoUSj
1oHsvnOtbKPnycgadElcwyhB5N3L2IX1mKQRc02+tme63UzNqys7noJVNX17sEGcncveUdcvmurf
+tBJNgiQrG2riIiR/aAU0w1tZkwBhvp21ysE5sO8UNw8A99KrtbAxqsxMfSCq0/3NJqZEJX5pvJR
f6k8A5HpOnW8P6Ex4fufE0sXa5oR7jYC/lbctdkKMbDhhsgnjbiAj06VFY6K6H2pJjlmq6MzyMbS
L2kb6KrugHBBed33gGgxKPwGJVCO9GEG8j//dTNSJ8PDdiaCYxARzx1lrdqc11BvRgAs8y2Qpvt8
DcysIwJNRe/5rp9jcDMjl2o5jFxkapYQa8S3UEpmWEGAGfK1Hq7KZrMU0xMMVgbsywrtLkYHqbiG
3PgcuQt7848RM5uFIWbQdYvfo0ivpdCFZ3yvmZJk1llbymmpld5SuI4uWNCcsxJ+79tq1fbeGhH/
djsnvPh3tTGyrKZI+hvGNqyipbZ1P1H8Pmw5VnD2UcntLEUIN4ahGw7poaySx/bmtewE5adyj/q/
vBT3pTiPjITtqkjFmkNsCQceHXWDFzJzBHYswMxPdAHgl36uuD8DxBP7fpEcYIqAn47d9Daq8zI3
wJHITeVup94O1Y6msyWZCNaTyP8l8HyKX7H09B1RBjeAuwAICNF6OzOpQk2tzbQzLrNH1iNat4/R
48Hkh0N+RjBRFK+ZNg8SfdY+eUgBBRQyixZx/DDhkExXM7YiQ0xjQuVVkDjXAA/QIgIEu+O2brts
JZAym7N+0tMai4BwGCrmz1HVjzzEietX5sxI7CcGpKRZhs04Ce3sQu3ZOZZyeyE9Ls5P3lTzhOF7
5EYbgAsaikNR3YeZ4GxTwAN5McHWYZtlHA0z0fPvSArB/fys/3azLzboPRraBizu7x/qO8ugEg5Y
CG9gaTx1knN8in1hrtNeGsuriU2wl/IyVxcghmlxmhBZ23hcKwlZ0ClHl9J9WGExOVVrMNvAiH82
8NrXAiOR1H/rEl6iqnTqzHBYMFqRr1HZsuHgf1euXlqBgHL3dnuz624gxlbhLC3pmA4NDF5cV/wj
HWyDTopFPErnPu6Qnja9yNZst0aLvNFxs567Iwhe/TgO2KSacBQgtUUEFvI+p1EPjsmNydx+sNqx
2chQHgNSJ8JRdetVRlufYNS/x+cZ1wtLR+Svd5jK9aP1uU1Xq5Got8Wd9Pzlp5zhyyl87rsCddFo
rl6M7NVOtB5JRYXQdMdZ0V62rTKqy4MXlBS7QS65q0/zWeHt0TXv4nlwNyktUrKeKkmE7rwoJHqi
wmtavQB9PjZE8Vsb0XW3ze67NdDTXEJGIS+l4V/ZCTBklLqPQTy7GMebeSAlpungGND7aMdTFi6j
vGut0WHAGGVXz0Zj17qtw6+BkNCPHZ+Q1teHHzygWGpBpnojpmKfn596WNq4kUxxgooNpCOaobFm
jbGaeK+lqo9EtAD4dcvu8tz/Xq3qH8FsEV1WGT9PMwrnDQEgCkAUyIwNkoHtprujeaTXXOhgk+fD
QmZBbRsjl9QHaROJ/H4y4nVEBBgS5M8fMnaFftJj+kBhVXAKDSABR21hCtgnDfhAQfqPvdGwgv8c
ZmyQr45WdbcyrOBW5FvQ3w8tRrsLxWz+N11dqq2+QL7Hao4ESL0kDrjImTyp3DxIwXJArsHHrNmn
pkSpG3MPZHx5KE4NCAPHLe/eqRmnokrZ057GEq7Em/tMYhAoIeytDxST1RFY4psStGHRVawlypgI
40ABkfzdtMAD8ipdO1FQknZlQZous52KMtUsBQyF+yi3MeKMcK/9pawWf0wGsovz9L6JWK8l1NUD
aaDPqsvkxbUJg9DqgvQ2Om2aMuPMDJoSgnClqmQ5mZsh9S0fa7P62OCaYe7T/QXIBUIeM0UCYDZp
zsRsNuBvq77N3zTqDfFrBa9s+0JgwRbItuw3jy8UzFrwAcQHyUqvl9YhRRaGOVP8oBccLvUJk1CU
s3r2fiWUBVi+GkDjUFUKCWsaErRV6c7ptLmV26dinFHEBZNUgMW/KOCSBHqwP03y6IbC+wlMEfBH
aEv9VeebA8ZvgdaHadzk46Dftg9q4N1zkkfGURwB/jNPol4M38WdKpQvoiiRIM5m4P1IHC/ze/XQ
F03PWXnLPludmAc6D1Tiyf5fzUCGKzBqtr9DeCGWpp0xEPT06LYMpA6Ens6ateTXNksD6GTsaqtf
jQC22aAt+NAAdLGjkoBM45u37yG9CZUa61clr78lTRuKH+5nRZlbVIzMaSJ9MXlfW6cJN2Z3p8Nf
MWK1ztL8h53a9sFpUTPHXJKdUpFt5jqqHntr0N8XVc4huBBdL3i7I4Ry8d2gN7XWJvLec/7FlpuF
MGmL1N1IDFK1+UWh6N7HN1pNQc+GVDY+drYc1bx90bF8gX9UznQZdUKWf4EW+dZMRd6dDY/goqcp
Q+jngkt/TqryIS4PaftNJwb6L1zgi/pWJPTMHfMH3PCU6j7O7wmkVf5YCQRy2u5D+YcqmmVCSoin
zjZCD9qdk7vpXTtnWyJqz1CwcMJvEEP9YT3vN8lY3ydRuNMrrAT46jvx+hLgpQMRLAQ/NPzAEQMz
++rMdJxKBNf69n1+FDyNb3HpOO6xsqqY1lzNzLGKkOlbj6Rm6RsH7NS7Bg+hnet9jXVZSeElDepk
XJlmjiONsTuh4vEb4Fo3lX9b2aZT+2oHShx2kAa4jQKbmIb+5/JfNu2Q4MlnuldG/QgOMIDeGH8l
4oh6egVUD/wQrbqDnjpBK7m8nS7DWpJHDAF7+yI4+YBl6ri0RvvzrdFhA65vKgFeFZy00FAPMs64
lRpRBWl9ZrA3EZW+fAF9XQ52mk1ndPD2bkHr5yaWDy7jZ/B9knEK0zPgZj1wEHdfmNh8eCaKy4G3
+FYAQjJePuAKSUSNEn3nAGw6A9Sr4QUt3DwoMYwE+1hrdS90zIAdKIpy6+EdFCHWcJSROsVkBLOB
XqeNG4YfE8wfib3icaPjcHQv28OV1SSvSCu/D1e4NoxpZUI7H667263Oyw52TmGOgcteOrqZ3DYm
Wp91VVMw018XpHhQ+HnsYxPYxYgTXUT2ggMnmxQ0KcnBfGoL4iOGjdK+TlMK16n9dW6EAY6DGMBw
/3gSpl1ckAvROXicKNUiHJiX8jjIjtbZCdNy7cBQMw4tgzQe6TJ8qsk9gofPuBjfGeAT65TYA1hZ
WlgG0UvxeNrHHg2MBCtMrbGMdlGWT9sExI+m24yMcOVsOyFOW/fXbrAsIv6IJw1wSrVY5JZC4mF3
fLlUHVm7xshx8MsCHMF/2myfAp4AohFZ+eAi5DK7GZ5CX4uzdQnferPdbP04YBDiKieHs4X5aYZn
m4bbwFVI/hcKbrez5VRFPzb480fC2tp0R2KKTbN9EDhthm1U4cOIxJhDIU4TA0SB34vvxFWgFUEO
07gRVLuu78YLKzzDuTIzALz5HHjg+Yw3FhISyRjJym8YCBYTnNFctAU1xPVszkqz+u1YthQgH/ma
QXp22MohjFxxWHxUBtrQYskCCbluOm6Wypi5OHAiWp7SHvVZYcE1gOJ7KdT2hZ0JiCCU2/FHcKu9
7mxpLjKhmX9D+bbF7Pub453+GLp+0n1DvzFH8f/CrlJQFWdXDgVT+hhlSAajZ2eKuX//eRIidwXJ
UT5pDqTKAGsgo2QnzUuQ67DEubnmB0KTY+oEXKSsZT/DyslC0tmT4WRhouno65JvBB72R7wOboiE
qmv7vzsCkiGb0LZMSbP30mrCm+aSlYrrNizdfPBEdGqwSrhD5B+QcpOfaLiQsMgtfCYRmsuH8vae
iA5LXeMfAvO6TfI1+CWUWdP3autLcz/OcNaTr6i91zNcGezafpCYkCZ21j5SfF54WZ3pkQLCfHuH
4vSLYcpdsOpRsHEuAXXj3YH3+u6RPYNuBT+CF8kMC/d4qeyQBYGjfxvITyMof8CnFStyRLicgzWS
pXmesAaZ8Kl6yACxrjjmf2ljhEzx5XQDE4Bs5cs8RrHuY0GAgJE4DvWUOGNcU/nddmdI04vvc+pQ
dPXE8fzkrYBFjizhRPVXSXAyfnSRNM110T6xFahaYK59Jqok8//rmAFY69iqLZr5UoEuGf+jDpbn
3fdsy3B2j/uCAe3v73cY2qeqjFUKsGNW/+q4nxQHFx9cZcMQ2vyBN/ukp4yRU0P+EH/v2EKyFNnN
EXWs/qvLpQ4PJSPU87PoijFf5iPrR1J87dkH0hyduIIprZVn+MAFWEllgSHbiEubjF0zGy6O8adt
DxkwveTy6HjlpPs1WnDvqalb/0PrsTl0qFFMM2eFhDeWrgsQZA9D7/PU7/rsD3H1ctDniYT9gbeA
uNkFJJoqESfIaXPd0500SGj+iKzOctBH7lKRmnr850j/yE6W2JXfnpf14lFsOCoh4DUPlN8jRfxV
AGz+E0LkOJ2CjZ+r4wR79wasKAoHTA2KCB1EaKJtHlvOL6VmtqEL9WjLSj271iFU/pRwBOA9NsFb
0GYoBItUcKfn+gBNiWB72IFvADvVaJfk2zIZ+uJTWfq1q58Laaic0rFcGhFrSVLoih+ibgx//6uC
QNBjWmx1CK8zyvw44P2OWB7Jq4gUPdds8VnxDgKs+NvukEu9rIXyGzahZP2gY62LCjxb4GY73Be/
hkHtfI/GDut3jJeIw0Qy1c/mqqWzSGFwpQ+6X2zfsmGva9TZ56hgRZRla4APKJJHOxeDCgXvUrFo
+7LXjiBjstGrmWGILsDw64yeqzAoFHWK0Hm5a5Y62jHObeseqO/CaFCJr4kJSNXsdxtLTuzbft9F
ddCCc92VsWiY9Ero4cRkJsuwJoKHTFK6m/0MOXDR6WkMXGIgy3pQECPcfLlg9RQDodw1ghQYLaoW
2Bd/lo9/2EpB1HxO0hMdJz0AjZfHG6/9GSUjpFj5otCkkDSj3ixGtAj3tx/E3cJz2399v3mIBkGS
TPeMJ905ScELFGTyrgnFUbjFWwi09Pi3/gT7nUDzAk2P+f7NidCC+Ta99K7y0eXBTWJRBmVtj4TX
7qmIDQ5OhqL/dGoE4bs5dwHp1CvpaYPzS76FvshUIHmsp3FSLDQ1xOyX69y2ZU0Gsg/K7Hqmc3fv
xooCDi954BXps8nXNN324SuRiGAsquun9Iy2/saJm6djwd+4QwhGdsr+zObPCWXYFU7EHyihc1Os
VsuJAoa4oHHEDXEKMb9CnOSciJCxUVd/uY6phVMoP7Ni+m6b/17lIP5Cun4i9VdXZx6AOzqINTyq
1rHA5zJr70pIB39jhY+BOjHyV4mibQ0c9+Lj/fzn62hHP/l1PRsAYgkMnBmFXPJuZCcO5NYuqw76
P8GzbpKVqGt4uWxX2biGELEdi6lXZ9jRC5QKgWmV8cMaAdHuw3cnznuMCcsUPDZikTx8fGLImSQ4
MHIWffGc7H1qDYIYfIGbHOxsRl2E0P9g5cG2psHqabwQiP2hsPWeN2T1cU7U9m6l5ORsJ7qurnYk
QG5Pu76QqC/NbXFBDtEKCKdgnpctxei2Z6Q0Ln/g+wYUzR2DaNr7UAPBOsK3hobUGhB0WVv5WRMG
5Hkh24gCJZwjv/TeHYi6avkvvBSMpjGA0uAbDfptPkUeFP9jnqE/MbAcu/usFd7dWbINjuDTQRUu
yXi1pzHTXRMxx7GcT2fSW4KGWKXRwoAevHs3Lx8K00mcH3rca2BR8Aa/rxS6dTGcOop5Wo9FQ6ht
CKwIDlnDl4Kr1UMsPqyDvHsaKSXZeHFxl+kA7zptzmaD0d54Sx4+MM3IHuw19IzrKpLLsUbn1fWt
tmWWO4UudgntkLpKs/2Sm0qiVoujRzFpm1sQTw/NPQoet/+49Iq3YvMAAq10CPdKrRYAPWwvOQoF
vKgavQCOu10I6kkoLmnlUysnIjSfNBl8deYQ571D90teI6n6nBZE2fPy/XsN4UenDCIWhR7y2hpv
83bpBzOSPTwghP68gEhq6sheahRubO/PAYttrFoJtlnE70D9GHTohKonX3ZhCQtRKfBGRjX5mXSp
RoRCsHicJf6MLhsQswaE+nmFUVmYM5XX07EcQqFAda9OWezjlpI/VravcLfq3gkMBiViJfZzwjnV
L68dQ+rceMhaxQXHiLzZlUA3T5P84UrRZpqvw/PXndXA1qIzdmSBuDDxHAUy/UDGAYCGkfl2IFPf
ZZvgxs2GBrXqVZ4Yfq5o2LnpN02OPgdaXmFeOhUAk3hzwLwatow8a7eU5PBa4pas5av0nL/LI04K
6F2UgHkK2U08xMC22O4UPO1AYnAzCob1Vo6ywAKb/yTcDU9cAYnvUzeG2VqYGcI4iYv8oCOYJ3sK
gJE/6/FOirYDNfFhiU55+KdzNvGBUbTBnU06AiSckgjI+OC7t2bgDBYTnOEUK++QhwyveRv0jkpk
XTuq7yilkmzn4+rAzcqAPHcW0nq1ftFTgs/xVrmiYGrHzbLfXXkPI7v9PB+GPEUUAxqMt6ojVpD/
S44azhkE6AwwjI1nfVSrdkXw+Bh3yymAiVmWUcDG2O+nsr86sa5tYG5G6ghXNTD+0KkP0GwIa3Ls
6arFYtAbBSZh+AiYzJaIdXC2gYkQU1ez03/GX9g1gAJ4gohrqB6+BEN85zfCKQJgFYe8/o6DJezV
4OC3XdfPfJRR2EP04zjbBCmjfo2nu5HYJS5GH/hgSR+HD9bw7bIuJU7MnNMggCjqGT60c15dT77+
H6R9Q0aCwJHfBPmSUeT7O8G4A1VJXT/LTHzWEJuSwQVSbeFgCw0oYEM7rcWlfgKAiw1/XHu3wkjp
SqSWcQ3x+kbLtMNEPeNM8Zz+D3JzHZcS0lm02lJqQZxx5HcyqHo/T/jA1rEMnc0gVkY1grmMJZMu
gcNDUtxuTzW1fKIr5TwWIG3XkyIC/gSxMUABmuHJEtENgypmx37M4Y8bi6kEzPbh0msG21b3uA63
pjzN9estHy1dufuyfyHK5XpNnP/3tSsAZJC8hTxsSfPRzjx+mu1/hp5UQ54jp3rN+MC93ULV560s
vsGtg2WTDqY9MywekvvI2m/USjj8l4nczjj2zhyUoZQED2k1AnAHs3GP1yTy4nSTz7NYnx0VRmki
qOhbcuS2vHlZGM9oZchP8Jdsm+C0XYZF0pSEwwqmd4Ah/b+JfeSRGKfUKHYMAtdC3gL+8yrdS2eC
/SS5/aJpoq1QSxd3HabYVO9l3eweA78g5EwDxASe8MxmiIer8GObM4DpyGq46eMB+nFqYcZVkIyo
vxzqmsrWNjsig2XiZEWJG84j8cidxSxhHzw0bZZuXAYf6mJcr8fkQSpNwVwWZmnT7cX11cQbP08j
1PFBazawj8kHdEXlfMP2XsPVJ5m3xA4m2KPWnF7ASsQmtCRol7DhtK+3Qaw3yhYQuafogYs5WyOH
siV/q/4F0wrpveaVvpRbzUonsVE1hdfXLtYnFd0KnZFPssZfh8Ph84lMYf6M+HZSvYgzcImywLM0
ZDvDmdKSoMESCkn8E7pOVeHyuWPHh2+QIUAB3jhmSsw2Uk+B4+FNMleuONvntaBZBq119DRfgELO
IZIrQuYSkj+TZcqkuNnlILHez9Dc7nuI53YHgqbDgROUv3J2YaccxsdmymBS/Np0O7aNJ4NboXJF
J0h2N1fzIYt3JQix4owrFKxfv28E/dYNpduk617nZgRlF3o9s06niuHibbQ9/hr/Nm75zuB13/md
3ogS9gt980j3iRhDhaowBUtlo06MRyn+wc0TzLgJ2HaCc+OuZO+LdbSL7VT0kJ9pLzZA5+o2Rz3P
Mk5K52F/bPnQHG4CO7/uGJrCcLSMhpKyHBdyDzkFowtuM8TaaoF8I/vGwC+h9rahmeKTayMhb0g8
rUxdV3B6JoYj97WghRQrKhn1ih3UGzXtTA8V6PbBHqxi0ooTeHjguWy4GRNbuG25Zp3bXDB6dMRb
0q3BilkfKwLg0wpTxKfNKzzImv5FOaDz8c1XjVsMgzc18Bwg4oilKvjwn+qgqQQPliIcG/uF7OU9
7Ve2Ql+v264uJzGaEwF27kUqCNUKCaaWtNB0xPyCnI8AwaMmPxY2TSutxnuYVqek6XJ7Pug7XMLE
/C7y9as7PCXT5ty3aT/u0Hm1rPBrdJ7dcyJ3cNzT/duiNQv/FrGMGcXeQQBOSSHIAMn11bEfh8X2
7eOUNkvlGI4T1hIo9LRUS4EjPYdL3ewzjluqsC8LROpW0GHSvAwAw51yJiYa4lXMteo92Tw9T2DR
j2Ndhyl7RjnsXu87PDLnOcxdMdnx3WrQlBWRpD19sdN6NXTc/e/Bb/zyBQzu0crSCV/a/3AyS7mF
HF5e1Eru6kIqtg49W7L/FRpzrdjrrDBfJd9ZkAzN3MY/dCrf1tInSpuP/GxDG6v17JWw0ZDfaaW8
h5hwVLHeR4UaUREQL9gPbYEAoQQuV5CNS5gyMzeJud5Nl7n7q8D85ynFkymqKrd81mMTBwpGcygb
KfO76CFX1gfuYnCpcCg8fZ6aqMIQ3/M0a4/hG6sWlfkn295wXBGoAy319g9tYBoZ5pjUleQihuxV
wCLYemtz1B9DgDFiPFi4u28hFS/TskqNlRuxUpeVqsbSQ4im7tO7HEmlEXdds+dMPfpbwATvpDxz
sXJ+8i6lHrAQNA3cQKqj5Paay6XpmX8ZnjA47OBLENjrGXCeGCWqtFAi/o0nvf6shdxQ3/1JgBqO
SrhhyI3acUz8OA/ccnSDv5epiX12456JrTxXYchK15WW+9a/UTRKmh3eIgV7B5BW6oZrOtjknMkD
HOCxFMov6SghtXj8OSxPQ/FIoSIjH4i9c/k55DTzSNaLu9D7NRlw7i5EiPjO1B4Nq/6hH4L1i3nE
59vpOX/pXe3NlEWLPpBwYKLEME5zoTz1itU6q4azM1C93U4lekDty+JkouLREFlzhQ5FV0ZT2w+w
FLxtKfc7Ns7TL0HE0OeUwzW/0fS7r3Rgp9KTKEtFThIpYBU3U7f051NdFeDrwiPPG//EIboMV0RY
8MV7d8O21XcSvaVMtBe7kgGmCSVfoIuNpNtX14d2kulhuLpkgMeBoNQiiKcRoEx00++zEmu86ahI
pt/xAC4cKxAKP8P8Ehm1BkZ+oqnzqmeEDZQmqlewOeKYZOdatnkOXqSHGcYRJvITvNITi3zFFYiV
RyKdPIQAmwtsDqWK1mijxdpb++mXANykj4NWdLnxISk0Lfd1hELbXeoNlCu6mdY2RHWb3hsb3idz
RgsN+HhwDS/SLm9KC3ff97RVpJ0NaOJ+ipubDKlWqZLisqzh+80Ici6OCr1c+ATN6aYpt2HGsuUO
lV2wIsDnLFl8yJqgepwWFMufi3wHD8QwqvwtVodB4mwSV/WBOWCyegLFpoC9htwxM5ZMDJa1MuKi
4hP/fzOFM4Bube/NMTX4EXV6SWYAXJ8FDWjEhsQTgW8N7dQelEADMYhI/u9phVLPc0NKA20tGvjH
IZexaREg2vvBGpzHQY13U1d+qLJg354Nr96syhgi4G/qsEQsTkxUz157vsDJUIDRzbNLwm19lY9a
FzVB04SHreOKE9MGgv7igQzyijewgGx+FTcr14cNNsUi7YbwFVQxpamrUvwvwEF74SVzDQmM9fja
r0RQ61qzYFpXo/tUDFEgv45qwR23xQj+UxJ5Buo0nOx0uH9pmUm8R1c3wjudJJjSkRVETZ4SZRVe
4W3vZOpwrr7KY+FOnKbEs0OrrsmkInVEQoa7rXXyGNfLomECuzoIyomHhkzHUZoJzRmWewhToZxE
orMavlDtStwCI5O+w3ioRu7JAa9QNpdylEWrTm2qFw79W+t8bi666GZxtgJt6nE9DjpByRLiDsQk
MGetmvkokoOtFSSV3e7YoZRdMW/8axdv9MnNMIcNrvnNh+7OB65Quuoovwje+ERzqd+bPeHVA75H
2GoA0fwx5o/CLO6ZBlZICOBPHeSx799KQ5CaIy6QAa0a+nnjoCQoaHX1I4gOrn7OxeZQbk6fba92
aVb+sc3TSnfY4Zjamv7MFRlxtdL7mt+6/jO7LWSx+MRoVL5n5io7Yd0Ea8yCU6oCJKEdNQijZihV
JwSMfgp8RNUpHZ3NL34sYXc358BeZnaYCfgjWp7h9Ne/iT1NQ2hJsOaWc96mU1jzf9XyFDsYkvcD
i/zToHQVlGfaxi33vu3EN1CSuvFCG6ZHRRWzX3p2koWiYECq2LQ+roXXLKeTMbuOpVlYvRJKku/J
RrFcc1E76HQ71F+JEoiVRQku/Jp0ZmOhZHhDQ4Z9dgNYpVZV8QZxHhYtXKV/xn9vX03mCI+xAsbZ
ssvuViwMZxXJZZIzNUFB8BZ3gkgEHvyZcyByJWWoN+he5VTokCttdqZE2EFaXpvKrUzMDzM23BQi
uqNKyeofPJVj2sr2K24O04Ca1x5rNZDLP8InhMyeJAASx+0bEB+3wEsI3lMm7Xs/fNMVSnlmjYG2
9JeG5cLjKThQLuD6t26WRG4qIdFIJYhfT+8Qz44toKUWqKKce45YGdizMorpnog3IWq8c7BDIXLJ
R+wkx0bqpitAxb8uVwLmUKaHTS+1jBMmxDVOJmHPkav4SAURESzATIw3Oosd6Fy8ntxf809n/kTv
i7nFPlVP1WSdhzF7iC67AXNexHVEs9z/3wna/gqtbW6Pc1h4hxAWw2pUHqh70yCEVbOS6CLyr8D9
dEGBHl6gYmvlEXyeaxfU35sDzfCdurxrsa3zpQH8baBfvV8fBx8QtmzLZ2FFMeneNDO4G0j9MwmH
3lYEsdE6SZC7lx9LOfTs0F6HqoWoLe9FZQ22aVP/+/gbfmKtYP/CvRN+fWRn1zYdR72vq9p4kNu7
RNdgL9v+jxmDdN3OWIlpzL1DhLv1aSdqzArSyXCX0dr/EG+qN6CPJZfOE6vSahAtE02iD3bBROpS
MF3WmQQMWqR3/v/4r5ZsiEk0YaVA5t5tzNiKtGhHuYDVWkGLShqnkBvzZjZwLZdpE9Oc2pRxCwU8
L/kdWLAHaCAwvj8PRitmx8E/+QUv8uHIQYz4Ya2aufI0+pWqctLSBVGISKq4v3rKdnk+r9LPQhoJ
RPxuIX2pL3snxnMi8quQXnzm3I9Bnl8lVkNg+w6wq6g5uaT+uuhbQgocJq/22dPocPkVO4IWznF0
UxgeLuq6uG6tny5zKE7w07kPy81goPsOEQDFfqU84hQcTEd3edWodq857v8ecZdQX8d0tuoaVO85
X0Qc9/kLnIz4MLLm07YDT4FFz+ECP8g0BJ15X6fXFzXU/vwDbh0Vf0gfm6c+TzeTxIlDgHsHeted
DspSc6/N0jXrO3h0V2AOI7e+7rwoQGa/GRSaIJi6+2MrJWvn0TlbZSRjtOr7lEMI324JafhYT3Hy
1DLLEsiSI+h4xM0k6nlGLc6S7hOmERKcv4RVQAWscJmhQtauu0ssrVGd49wP+x1ABybUrGrhcA6u
e0Prt3Yi4KFsTjoCQSm8tWtWrgDYtB6h25YJ6IU8oxbKxoiKZpJZDIrTltDO3XUoJDIxMA9wCUW1
v4Fs96GUKgnRWusSUh046yKcGUeLtpA9ZqCn8JrJj/lUSBGpMqavKdWwef2mBZRhJ9RONrf9PgvW
jsbNRvp9YEnkuAvJubf2a15Z7woa4aQus8DeSWNRwaruhXHeW+8UGKTwBNYH5z31ob0y0xUVStbP
O6EF8si/VOsCE19vmmGFlHnJ+Fofu2VzrQUtPgERcq7U+yMlx0xeoE+xLN/OvkKYT7fMkYRKIO0r
Xlr58QbCXC1rBGuT+GNjteI+5J33PxZenaOk7UZ6WvQNqlUtPfvuVRCgwMlOjTJdwxOTRIe9YXa4
KYL1+GDIkvtIO2QTGasJsC8Sj2X0f8JotrtZgY0oH98zLgVl0/RhjI05fq+4Ks0oPJ4jZAxengOq
+iDo/F/TxJsE2L1lEIbw7trUzL/86JpgQ+zfcxLWMpFRlMmgTLkFNnK+sUlP8vzbZlajSSqh4Wcp
t7vKEhqfDpgIEWHuC6oxDaM0j1RCqYHFijagWULymNoJLXhweNWlajc3DQeQ//lCFoNql3TNukAt
phaOLDnoXKKdnrkqtXAY5LAjEzW98q1s0fsbVHFaroFI7Etcd3WUBZqWMk8ZzmKw0k6sbbinkWV2
azvTujUlxmmewHYZfqTriATS8WBLp9lfpsLPN8XOurc2rZkv9wpLSOzXZqDWScCWAP+910rVohFi
8X+fivzpJ4D3pDctFTWDI0B5hS6QP7i8UNeKVZtLUhPIajyCcPZU8E9KwTItzuuheKq0Np193AUF
RhVLrwsRSzOfHJsCsubj+zAfYLmQZLuPgaK0TEtw0AIpk/6vwQIht1rH+4f086UZDoeQbFEvGKPg
IS+vc/Q9j2PeclRa8Pu0oRP+QS2zX3ExJLFjpeS3BD6zOnRWoA/epM62s23hOPNeojQ2Kz5KNMms
Gr7qZ4rLKod6uhwEAhOrlaF+v9Xj8iQlI5PTz8+Z/A+1y+OvoBxQ38V6IIdsBbvSkqNPW5Exb11T
RgfwiX7N3NdWrxupvRzO2XZKypAF+uj+HmcjBSWiD7wP66dvDKeTinITfTT9e92kIKiiFVr4cuqY
6E/qwsOGvKkw/xhs0BY9nafPqMwKGKbMv1R5g/2tPJz2gICaFKyN5Zi9HnLiBgHVLQGJ4KQ3bLWl
vahloxKAI6vmSsV7wLOiVNbNuxbMWzijaitYJhQkOd4lVWKjxIEU8c54DMByHjB3zsOi/7ET24G5
jA8b4uBtj9hpx3rFg8PIZebebb6lOJfamHH5g433ukrxMaIJUVeNbde0mfOKrCFV2vq61VJcZcB6
P7RiEwyZAhl+TtjkuM+V5YM2CioejutDXOQh2V5PFZ51Y2INALeFwLttV3mN620QoZQRI3kOzBi6
uwEjT5pBRhKwwKTgRXgQyllYIPCsOrD9Eggj71uqCB9hWGfOz/ztZCf9bSTmJXgvRZR7EljlI9Ol
H3yJ+UExZalkfTENRBAiczDSTUvOKLEM7plTbr6qI2qMh2QYLyzQkPxN2mjHuWAOH6VFzgitPdYf
+wrTc9WEPhEAE4ITxQ5YEWgu31AMkhVMZxg0zRpsmEdwODSPeqf2GmD3BnNeKhx+BiEeH06D+mpQ
dnKmHbgsHnhuXE/NLO+2qKXgkUM/v106xX4GHK2FviZo/lko30mjl1Fg/P8Jfrnps2x0OdfegqQV
l/orIrdGOjQ4s/xnghi891G4lXdSVMDOnMMXJVxqUAAXkskGNiSjIu/3aBHpaBtHYImrYj9GAnKM
tzVRvucy0YNRpXrUHg4If0eYLw2aD0DYHvlXMfK7PMPkZXJwPMVTDKjAfDCJczOR31cpAUOUtsZJ
eSFUulAJrIVXscLeVtboAEl7BCKeEgaIeuASsCCymQg3CBwSQG5inuvHY0/qJzBuU+6h1coVJ9AJ
wHqAMQ/DPBE24DOTDCSaELrM7CkO9WcPwDuZA7E5/yeX6qLL6+ne6COyKY2LuBXOSVwjQRXoQ0SR
0CZwyHUIan8HD1ZuVYBK2kpEzuckGr58RAWeMS++8xYA6eHDq3LUfiVi4qB0P/flNe3rGWT3L8zE
PFbMaM+4+5ip+yMhJsp+ml21pDLRPaz2DRruBrWzCcMmXpBpot4kymLKDEtkqNa9tmF+iAxq1/kj
AbdZgyfQi6oQc3BK/x2jtteCHUr8QTTn66Yw8qwVTBFe5NiGH+qHdxLFnWMbkz0pbw9sG/J+3JXD
Nmon9kvrBp13DX6cFYScqQ7hdEHuu3ufxjXkIrlwFe/Rq8tsBZYNktr48ayneaseHHFR8JBhuq3T
UR9yN0Yq+fVur6OhjAQ2nH2lOCJJak0i+GlKy/MZS1OMKJwQZfQG189Q7s8iDw0DuvK35WEH35Ex
PxDumBNJ8Y+r3eo1sXMEiDOBuNCyPO/a6D9ODkIrVxI3WdevU+gmqDL+m/yOsAXX4zWsXdaMQW9A
4+JQ4jREgdIyXYFrZKKpMMN3sIFBjHfoLeEbSjBxgH05T1LabOP4KW7ejNDGPXYHMTPvmxdi/u2v
ogRuJzqTUcgADUBNXw8nid7UWWXyK82lxJj5a7pinErv0jd0bZzbMcFuHNFvvy2SJHrN7AX6DlwZ
ifh9cjAx/XHYuY4IDgp7bjuEPhvAELWD7vEK6F5Rkjeqsp8WTI1bWWh6hiwF4QgAH8WMEEAQ/UoH
sAEyYUPGvZpPh7EAsZd9lVmg+kI6y0uHZkbYjlxgD31Fg7fzc4WR/Ck1GG6G8WSfqUQ7qYbOnHDu
z9q4ZN6d/Uq+m6U6simY6KVBMjiYwD2ZopI3km7rXbQg0F1DJ9NHNf91ueMpLNQGyF2RwlU5wdIr
VF0FyJ9pHH5htrP/QRwbiXiaCiBIVoBwCedn0MKsprO5He21GDgSpClSVi0k3RManmJfxdt98S9z
jO/aUIVBU+llJYzzkFuE3mt1K11+I6/RjKpXqb1Za+1CDY1WM7CJXzba+WT4EOugxbWaY8aKyzRl
jLbFSF6luS3YIPmcuJYY7//C6fMGXRYG+ft8Krob+rQkurmO67VetSiPUXoqlUDg3WMJahftf3PM
4guohor4O40bw1ckh62/ruAfhIFyl9K0xLhBSAqmM/9UdkB0KYJ87ATaQAoN/rW2KK+G6u4MDKET
5hEMq+QeURN1aGjNnDALsz2RFpoqM7BanYVYJphcUFpdRc+1jyWqd3atvjD/S7HpFwreR2ThFFL0
AWkNT0UKSRwYmMxGgKvfsIVuXtZujA/r7nQwO4Z7h6gCHXlxLRYbUitm8FcnoAhH2mc89j3ACuNr
W3kI7huzt+pQ6VD3s1t60d4gFXCHanasT2l5akUySr6lXRsTuz/Z3ZryxpZ9VqWOgPfDeIvkECoV
70bwCQ8CAN1GTQiGFMs9ZUotlBPuXIOk+ZETztnIiEXLv5zVPSn2yU1MmTbc5NLciajlXrK/cYKD
r8mBER/vwcN7F2FpjAe32dVqIS6q3J3/hqbCXgiixG7xvBUYNaViy2ofc7HViQV8a2faAxWol9Wy
wOPjRhVWB3Zg7nAn1f9aoTVkKEFBXuxF6+rGvKs2C0afPlAGmDPrWTvQrl77xBwrtXYOCRtPcHfc
2pOJkazBzOKVf0JO8LWoj5ezIIdY3WYkZShYB76WSlM30uIsNfZB+wpkHInhYbuiTJMKko9Aj75m
s01Q1wd29LN2NDW27zRKcxiZ9jzwpGTDNj1YAzNGh9Ti61yfTRgbgvJz0VZmYZIEoBcMZUew+Ug2
UmA57A9YwWakTHaPU/XLW67pujjKn1mjFx6N1l27uIEKv91vKL4J6TVRwbgxlRls3qcrPLu6Gcks
RG6I+sfMNOMYoEMosufp4n4kunUsu57HFiKz2zMwRC86yWNe2C4M4bmcoTiZPoqAWlozS3BBjMLU
ox+2QW9GeUPJRPaVW25gZ4MqQKuu30G/HcxriiMVnp5h0wjio3RmnT8mI6d4p/erka26z5bvsEyb
sIaGKsj5IZFdRrZRAbFJshiQG/efh1ZCcI2qu2JtVJFJu+4G+xY1xyHNU51hlj0OACsBs3tS06Oc
hEdFsElc3xgpawE367yvXEGzT9TSKZPbM6g7439R7DNdzVBAgLpQFdMW/ktlXb93xzJJ3tcpOGbT
GvPBSk0snkyhmLMMr8VOq50k1ebfSn8K2NbMhLo9NywZXXZ9AZOdXZqOkHkD00991NdEBAmKTRGr
lYP122sTHaeaw/BUDo4v8C4uFbIR+Ft8QNEJaJheBYz3DL5G1QilZVZyMCJPXhe4VSZ5FJnbtU/B
Ip9VZV7c3OK/YUDFKnVHNTsz/aI7RXTkyq2zJh/Zj25NnXi9JDwj0HCHLbf9+tDlPi5GLA/2Lqrl
lFVcXe/k3j/X88l5HuDO7RfmAsxSb4iZcb10yQD3kktZRy+mQMsO9UxZNpFDctrZVvJsG3nPfiV3
EdCtqiDRvArgsSHvd+EhUhSURk+k1ADMcJYiMTi+f7kdNDFJsRsr3VLeGovBUlWj7jvW2A5JQbTH
QXw/k8B2Q89e+getWZEmArfaHi0ix7VLK/3BV2lAlACs4KFNQR4YNB6EbGoUIddPBQCw0sNogpqe
aGMgC0PjwrHGvPdhGMgFB3ETdDt93b0kc7JPsIEDkO2UJY6Ta7AkDn3sfnjcsSXSEIij1sLmdBua
OH5OdNwtljLor/cw4iRb3mIFPYxq7AyQUaIQBc6RVnr5R1xp7WpEhDdDFWchFHnWZMs7jeS0ozv2
B8u9wM+vDFBhHCsyn/9UPZVYdv5q6pTkmAQrac6IcmQ0NSPctp5vgoaxPvA1qTvNJBryhiCpFoE7
6YYUM2qbv0GL/EjDmzRMTN+ZsGyPSp9ExzHHnD5aj4sa2xI2DdLNz0f4K7LMUUl+kH2wzodObsZT
1W/KQ79oydsVedJZgYIc4eSTuvPZRuP+BbbEBC5ZdcVlBFiNyzZKh7iOGmY5V5n2Y37RRJo3MIas
pbOP7NZLUj+5BfJWAObe/NxRsHPDf4IGuiqFP/j6DCwV0v08JyL8j49TrvBFCN//mgM3BzNbQBfo
iY1vJWp9lTm3aYwZSqTjrMwLJvMRHtE2eN1cQoeacuttihV2p/g12UfvG9iEO/EsNdGyh3RRtf79
fpwjLM6xYCazinYo1ljcWjdURKZW8nh4Oh50M3Zb0BKndBczLqCvzOX4LVPBRAggxPLe0mUUeu94
4E7POgGEtT5cqAGVm/0QN0LoOjjmlRK4bj0QfA5HpsaGhj5RP2TyhnUzfmbZfrhjNoxKlzzFq7X6
iOADpZ2lzShRQ1KmvK/awtOdcj2wal30oemtXAakeO2n/IknTamQAJh3lfz9XQU/gQvhyAG5+FZd
HmXcvsFuxcBtSz0kkTK7tW5MlIQQXaUseT51q4i1UQtEp7msYME8StkshDFgjINprTnOX1nfqR/s
t4wNdBGYoQFVQQYUV9Qx8UpUxr+qQ09HmjN30weBABU5lfeC3xJZaD0Pg29H9JAhD3DhbNuwwBIR
UGXOMNRAcjlQ98/XAfqNkMPSSVUsHfImrX4x6l5to9aPPosMrVNdEjwRXqIPM8AejCDAlwBe1tf3
exZuWi/UOYYAKg3x6YtG/YrRnLIOGYxePqnjLc2tTcdqN4G6hAgX8Wi7c6HDyccSVPBMwnVmfJ0Q
uLmI33UcUIffO+5Q7hlAOhsQlPfjigmWRib3ryc26szYm1f6FiUctQh9D0iK0ylB3WABXM9kAaUd
cTazGtCixxghLPI4C10VQLzl1TRsXQaoD7lX4ATK9b4gVcx4K05ZHRloU9qfkY5Wl3SCInANJuzS
PWAqvffRRzpHsi0bg/4vagpfCDN6KYCBhCCRgduQLoT7zJO/1W0ZHw1vu1/+E2Rn4h2IrrcmmpLz
xmlGNxpNgcuS70ylR6ZjWpUN6HRk4NlYCQadcItt1On1SqolifVs6cOY0liVby2xkIKujAqlR3cc
d7MTZaZnY8ICW+TxrZUZscSEGqVwwT5+2MNQ46dONJ5dIlJVRNEPRN7USKTv7lw0HyslgfqM3F99
BEG8nZ5E1+ZK9kdKrryYS2QHp/lKSiiOedANW78wf2/bBWllz9FflckCIDFXbU+XKRLnb3Yvlabq
W0leofBFpBMKGGEzZfmxv+9x7Y0xPBD2pAdjLHkkV0PQCx/BCvjNUGgQArLjUbS/A9MnOXirh+G1
tILrZpP5z6YKrKFMyuuCSXfOY4LxGurrVZBNTYu9FCvrBAPXZ5HiMkuQrknVBmmvSOrxPa02orIC
WQgAuBcxPpqoUzLravg7M7Bys/VyyiSrq2/M0pdDMgts1os4/u9cP7miwEZRwmDZOu5Wa2dlBseo
nkr5OD1wfFJy4klRDD5HBZQT1D4RwBk95Euvc80dWFibdiC8BKKDwdD6+3k+qDJjE2zKt7Etyhsl
/oeDoHaZ4JuAxG+o+mHY0V/7oJvwxRCfTwl8lud8ZhENRIB8C9FBDuM0njBiFwZ//k4dWW2ARLN0
q8fDKW59cP1g2AcaiyJ1fQ/2UsHRb4BfhYZP5fcFQZS0IAgbj1sd0JMPHpsEgm8kBdDT1cFqjWcZ
d6Q8ST+sEK7HrbYUSVz29fWW7xKApRLLpSjqvB+62OToKqLt+6WNouB2m3w4LKEhM5CSjHdTeWVu
VRRkHgkoJGFEUOFknV4dI8i7UqVJPOrb0uqx35KNbHqJHT7KywalaReyCMY+dgI+mWRW9IM/3f9E
1zjaI1tqqlknhddK7IdElY7kt3zp2yaxDmMHT7uicZVkUuuZGBlXx/BRTcCbp4W29WX+Q8isk/Tx
8A6Vmt39BvL25HRTW79D5cevhPg95NKZY/dKbVn8acHug40xXTpjjOUih6+Gd7igoQlZSzUP9l3e
xkew5W9IZUcBX1CKnI6LacgKSKrbEwD5YRa427Q7k1RvH38b4jibEXvXiBfPh9gVhsFcuauWgBkv
UddiWcsWKBX3zRSyULTrOodS7924RGYEbXfIsZqb/5FRLSHyrf43E6We5to0Z66ExtIo9XSv0jKy
R+f9sYeIqMs6sinXyYqiW5CoseiW6BlIQIDA+noQGmBbHuvQZPTBeVSobfpOh21vbIaQ3uvSmIbL
ZjmD7MH/eqZBk6g9wCP1xixk0hri52OxsuN3ayqLc0oDfdIbCk3Xc+uMfxw8BfP6qi/fhAVx4AJ3
yw1libCM/2LH0cFRLJecPRq/IPk89QpJ4aXOdHXJnVHZbotJqvmBAK1fCkce0Wihym6TaHIHCoK+
KivtfHbBTjCC8N90KfETOBKbJYTnR7zrlPWAGYkm/HBregZ+xrZYzgRkJ+ujkpFJ0itgWRNWHTMn
VK/ZjE8HI94MizdQZcHVMx7keVknkBnJdHBvLYmck4KkGS0Hv/G40My2qgA6iQqrevRfIt5i2mZf
mXrCNtegNTLR0AVBUFDmQArv9NhFVkLwHU1v1bFboI3mXp9HjA4AGf4jcrL295wUYGrVhkRP6XqT
RwvSbPb2gPYUBCSUFUlbt+9tHn3Z/1MyXdzxBom5AIQuiK0+KxUc/sjYkHrRGOVmGzh1/wAYmfVI
XznFW+aro6s50rW4/R5Wi1JKwwOYONa5wUqhxsxL/K1H3AbYujW/g4tiaWGDkqQKRx6Y9tOW4Mfc
JVdEbtxLKpOjiZGLWFa5FScFO6cP8TZwxZu1hn2BOsKysHYF7wj2tsYaXJ7NWT/lBHi9/ncjYKB8
GMPKX8FR9zK8A72mrHa/fMC7QQqt9Ej7sNV5wUaUJpKOP4nbdBy4hFWc60uQqFgVGeLdrXlNtPhv
Rx5WrlkR18LocXGCfptv+5wra/EgStHMxsk6LyXmJfCWjkCrm9Vjrq+9OSA5JScniNIacPhKUn49
i3D4nQwDnWQpNa1Jd2CpiLSGP9pTI1xRwk+xUhRd+i8Jzuud8aKioIAM5ZGd3Y0FFnHBO9O2+mqK
bS/BGY7cAKfbhxEE8N8/mMa0fGGoS773Q2sCdM+Tyffo9oIfv5iJVHGZ/OikkO6xVy9q/Jir34c+
nZHFkMy50i7Zg4XdZ8NZ2BZ9ostudtpollz6LHJtzL5bzPk3kaMAKH2fHttuPD9URkdZyKmkU7ZK
X/HDdY5Me6DFDVa1kPDM5ZHuufrlvEmZxActiUKRJrIdNF7lS6F1Ahox46OcNW9fghHBKvL0lnwE
Aq5DWWxpz+nYGLNCwQLoMyf/3Zikm5XAPAHOt0pes6+oKAH/DgXCRz+cJ2/+Y1x2BGOi3p9L2CGr
Qsxklit9F3nSlNhhAnD035ehGUj5/olsNREcKhXQ+yf9E5lf6oWycMQEJDHflJkInS82xxlqaDJL
S+pVfpYI/7gF8i691KF8HqQulux0tiPe1zw6vEZr3Fi+CUNeGCipW730rmJeZu/fmaXifaop+7iS
dBHyW2P20aXsGkGIn9NOAKlqvbOCA4txj5NIi/k4eI5+YTSKWRQCwDzym1B8jq3dZXHO4u0qxp+c
uDaYRag2BNVnxW/wbsJwfOTByS3VLoXCSeOQLp/rq+7l3atLaiAXV50ThSZuKR8JmFk9xUbrYZlM
2Ti7Gj5frmVP8SwY5LOdVVkWW1HcQRgp6zrGaulPj2FhZfDR3nNB8wc6y2lrQjy6RHr8OEVYvXTI
z4goYDiLFRGwdbdU5MVCh62x1AYyYMRaacM0XtXIufeoQ1Y2fj9K8EcFklOkF3NofXe0J1dWqdUb
+WQvTs0E2gtrHYFDGXTsDAheLQLb9AD939KwWCLK+tY8lYe2IHBWRWzXzvy9hO2+AWnejkzuRRYy
NtvFwaEiHOLcxJKT+JE6JlsEaNkS9ZV9SRAJXlrpTlSVfW9vZaH98vpqqUUmuc8npvI5zWtG3pd/
X1IwH1aKWfQqdYiTZIdsj5pjH6QZBqAqJtBW7PhhgjQpugiA6LcOaoZJqvQmHSKAxsA025X1lSox
4jm777ZDfpP82kBdobkvQeombNybb/7Kmb3lw331zfhUU9v5RKN1aqHiuWP5nJNdbaD3Zxt/1Ylb
hew0vuRGFb1xvpRcuwwJK1O1X3pMZCIvUhf25A8yZY+YSWv9ZFRSOXMLg3MvEYwpc9XaciLxt4Kg
vfxy1spRsruRCzu5O4oA8T7jt2XTf3A3iwK4lA/ZP/nt215nOuYnVUqsNDBqYycgZWpQy6qFvZf5
VfS5IUeF7O6tUIgw+wmb10mjKzzTV0bieRphi4MVN2cHDm9XBLCjv+dcYSJOkBEJhMl5g6tEl2B/
K9x0nMorKn4y27nHyLOXLDwWO2ME1qQtAnSZum5IeOseF+ngxqZCt/lhmraO8iowxRZrTX3fA9gO
pMmqoqd9Hrp4YcNZTReXSKakoDWu2h5DtfzAYZf0LfrWxz20q14vXaE65iwgk2C+LuoBdvFfekUi
S2bPfAM7TD3dHMKdrcY2CCtw1jzbSUBXFpcgR/inJK5Iz0gtKkmQ7wYcpQgrj7WsC8dPQnpw8zz4
BfF9kIAh08DlqDyTf3/GUh3ACNMwEwJJmJmFmKphlqdpvEVAyiC/wPs59TT112NDTVMNFmG/hlIY
vPv5oTAD9whznBsaDVUZn9AZTydJCeIGkKvm4gqqE6v7hYMtmVNHyFAysBq5FCLFSe8ba0XZCtmk
/SyxMyRuXHULfHYbbAIcV18sFAWnxtcTzkPOdAXYJST9KLklpOOY6ZNssK0wIcZaKErykdAHNwqB
7/oVjnALy/a9E1leFHh/U9+HgqyzUss8/SSuemOIeNRm4ROHYsZd+380tE8BaPoz/bi/ZvpZP1Cy
yarIGbvB+0MVM2Xs5JEje2d7yBi+YPddYHXHnDUT8bqEexwPpm1XP46FwrgwKWmxLXtVkhNtkANz
RvIVhQVIua7+iU0C7Jp6IUGUvBhgAMrK+j9XAZ/q7tU7NhEZCbVuDNxQtlyJnrW5uunqY6MCyQLq
VzviwxwVq0LFULLRR2oMuMS9Sh/z7tL/XnL8CjEueiHRZ7o44ZhQNO1Wt4FUHeHEIuNjw8CDS6c6
BiSV0VWT3BdDX0zPR4xrcQkCPmTz9ZhX0gORd/T8IiS9TeZR2/l9OCYLWJ6rzVwG7/HBz0YEy/ZA
l/ZJUukQ1VttxqA0KYsxKHZJIG70AiMvV0v/R973E0EJnUm9MUKRZ+nGNxRDw5xEPf7YyFhm5wyo
oDocgxW98Lo9ARCNO/Ixv+X3rpjhJtlf9M9POzT/dS3s3ZJflqTAqqXJ0LWhwBB83cCTbT5EOlHD
d5i3tYbqKvTLilffMtMz97OqMyW4gwm8RUYsrD1TP+UNbrbDbF5oGOit+Zu2YnD2uYxI0Bc3puKh
8ztdHGNi3Bxc0Mh4ulXzEunPXZUcf26EQuZNuUZYgS6jQA1nOLTkgnXbRiW+i80hORjjyvVEg8N+
SdK4RptyL8wmvmV29dNsglGoWfBpaApO7byzxMRLxIc7VvhHCBF2me+y9FW+JSiLb0gIvuMVaDtc
V6nuN+vXTjzXuz0iKeRdGEs2XNrHDe6U5DI2a77ZHvmkPgaS6rU/p7+Zk+OtUCY/g3Qc1rFBOuFW
SxYREfKCdDm1ccfID7L9S0ldpqOvUlK3w+dntvXYaEZKmf2lFhuGNHH3sIOuCulAWtx5BZOed6xc
7r7Yq/wjWP5WC1wfxTxNKelGnyEYQPCu/oUQGAmYeKulKVEeCrfyVGnF7HVuDfDnZL7/XALeLDqR
7aEbv3GQ1R0FgvjMjMfW9RmAZ96H7LNVZ6fEeefkgXYcb91R+FIbUOBvS7lRZlxecbevVi1ocjXr
mrjvlnFj+IaDB1F4a9dmLNiviQN+3kFYvCw/BknRaOFTfffe6gANNnxmhmGgJrmu43f63QjFwKUF
c12mIv+npJGbIzb0iB8EtIV9hiiieek7XWCCRa7t6+zSk2nnpyA2P54xXwjXZOpzJhRhM6Kgn9Hh
4m2QWrl1qU3pJ+wNZlV2P9uQ2aWo/+GiRA5OQ816WBawEVYK73Pzpj+0ugTPYJXZoH5lOjJxy6mo
7KEflyAb9q8X4mdVWSwmcIYgWc8ryqH2evAE15+tEjFZOfXapgFRSzi5R7+2JyhyTH7GgrqF
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt is
  port (
    gtxe2_i : out STD_LOGIC;
    gt0_cpllrefclklost_i : out STD_LOGIC;
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    gtxe2_i_0 : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    gtxe2_i_1 : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_2 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_4 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_5 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_gttxreset_in0_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gt0_rxuserrdy_t : in STD_LOGIC;
    gtxe2_i_7 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_txuserrdy_t : in STD_LOGIC;
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_8 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_9 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_10 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_cpllreset_t : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt is
  signal cpll_pd0_i : STD_LOGIC;
  signal cpllreset_in : STD_LOGIC;
begin
cpll_railing0_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_cpll_railing
     port map (
      cpll_pd0_i => cpll_pd0_i,
      cpllreset_in => cpllreset_in,
      gt0_cpllreset_t => gt0_cpllreset_t,
      gtrefclk_bufg => gtrefclk_bufg
    );
gt0_GTWIZARD_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_GT
     port map (
      D(1 downto 0) => D(1 downto 0),
      Q(15 downto 0) => Q(15 downto 0),
      RXBUFSTATUS(0) => RXBUFSTATUS(0),
      RXPD(0) => RXPD(0),
      SR(0) => SR(0),
      TXBUFSTATUS(0) => TXBUFSTATUS(0),
      TXPD(0) => TXPD(0),
      cpll_pd0_i => cpll_pd0_i,
      cpllreset_in => cpllreset_in,
      gt0_cpllrefclklost_i => gt0_cpllrefclklost_i,
      gt0_gttxreset_in0_out => gt0_gttxreset_in0_out,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gt0_rxuserrdy_t => gt0_rxuserrdy_t,
      gt0_txuserrdy_t => gt0_txuserrdy_t,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i_0 => gtxe2_i,
      gtxe2_i_1 => gtxe2_i_0,
      gtxe2_i_10(1 downto 0) => gtxe2_i_9(1 downto 0),
      gtxe2_i_11(1 downto 0) => gtxe2_i_10(1 downto 0),
      gtxe2_i_2 => gtxe2_i_1,
      gtxe2_i_3(15 downto 0) => gtxe2_i_2(15 downto 0),
      gtxe2_i_4(1 downto 0) => gtxe2_i_3(1 downto 0),
      gtxe2_i_5(1 downto 0) => gtxe2_i_4(1 downto 0),
      gtxe2_i_6(1 downto 0) => gtxe2_i_5(1 downto 0),
      gtxe2_i_7(1 downto 0) => gtxe2_i_6(1 downto 0),
      gtxe2_i_8 => gtxe2_i_7,
      gtxe2_i_9(1 downto 0) => gtxe2_i_8(1 downto 0),
      independent_clock_bufg => independent_clock_bufg,
      reset => reset,
      reset_out => reset_out,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_RX_STARTUP_FSM is
  port (
    data_in : out STD_LOGIC;
    gt0_rxuserrdy_t : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    independent_clock_bufg : in STD_LOGIC;
    data_sync_reg6 : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_0\ : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    data_out : in STD_LOGIC;
    data_sync_reg1_1 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_RX_STARTUP_FSM;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_RX_STARTUP_FSM is
  signal \FSM_sequential_rx_state[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[1]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_10_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_9_n_0\ : STD_LOGIC;
  signal GTRXRESET : STD_LOGIC;
  signal RXUSERRDY_i_1_n_0 : STD_LOGIC;
  signal check_tlock_max_i_1_n_0 : STD_LOGIC;
  signal check_tlock_max_reg_n_0 : STD_LOGIC;
  signal \^data_in\ : STD_LOGIC;
  signal data_out_0 : STD_LOGIC;
  signal \^gt0_rxuserrdy_t\ : STD_LOGIC;
  signal gtrxreset_i_i_1_n_0 : STD_LOGIC;
  signal \init_wait_count[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_1__0_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_3__0_n_0\ : STD_LOGIC;
  signal init_wait_count_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \init_wait_done_i_1__0_n_0\ : STD_LOGIC;
  signal init_wait_done_reg_n_0 : STD_LOGIC;
  signal \mmcm_lock_count[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[6]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_2__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_3__0_n_0\ : STD_LOGIC;
  signal mmcm_lock_count_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal mmcm_lock_i : STD_LOGIC;
  signal mmcm_lock_reclocked : STD_LOGIC;
  signal mmcm_lock_reclocked_i_1_n_0 : STD_LOGIC;
  signal \mmcm_lock_reclocked_i_2__0_n_0\ : STD_LOGIC;
  signal \p_0_in__2\ : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \p_0_in__3\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal reset_time_out_i_3_n_0 : STD_LOGIC;
  signal reset_time_out_i_4_n_0 : STD_LOGIC;
  signal reset_time_out_reg_n_0 : STD_LOGIC;
  signal \run_phase_alignment_int_i_1__0_n_0\ : STD_LOGIC;
  signal run_phase_alignment_int_reg_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_s3_reg_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_5_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_6_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_s2 : STD_LOGIC;
  signal rx_fsm_reset_done_int_s3 : STD_LOGIC;
  signal rx_state : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \rx_state__0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rxresetdone_s2 : STD_LOGIC;
  signal rxresetdone_s3 : STD_LOGIC;
  signal sync_cplllock_n_0 : STD_LOGIC;
  signal sync_data_valid_n_0 : STD_LOGIC;
  signal sync_data_valid_n_1 : STD_LOGIC;
  signal sync_data_valid_n_5 : STD_LOGIC;
  signal sync_mmcm_lock_reclocked_n_0 : STD_LOGIC;
  signal time_out_100us_i_1_n_0 : STD_LOGIC;
  signal time_out_100us_i_2_n_0 : STD_LOGIC;
  signal time_out_100us_i_3_n_0 : STD_LOGIC;
  signal time_out_100us_reg_n_0 : STD_LOGIC;
  signal time_out_1us_i_1_n_0 : STD_LOGIC;
  signal time_out_1us_i_2_n_0 : STD_LOGIC;
  signal time_out_1us_i_3_n_0 : STD_LOGIC;
  signal time_out_1us_reg_n_0 : STD_LOGIC;
  signal time_out_2ms_i_1_n_0 : STD_LOGIC;
  signal time_out_2ms_i_2_n_0 : STD_LOGIC;
  signal \time_out_2ms_i_3__0_n_0\ : STD_LOGIC;
  signal time_out_2ms_i_4_n_0 : STD_LOGIC;
  signal time_out_2ms_reg_n_0 : STD_LOGIC;
  signal time_out_counter : STD_LOGIC;
  signal \time_out_counter[0]_i_3_n_0\ : STD_LOGIC;
  signal time_out_counter_reg : STD_LOGIC_VECTOR ( 18 downto 0 );
  signal \time_out_counter_reg[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_7\ : STD_LOGIC;
  signal time_out_wait_bypass_i_1_n_0 : STD_LOGIC;
  signal \time_out_wait_bypass_i_2__0_n_0\ : STD_LOGIC;
  signal \time_out_wait_bypass_i_3__0_n_0\ : STD_LOGIC;
  signal \time_out_wait_bypass_i_4__0_n_0\ : STD_LOGIC;
  signal time_out_wait_bypass_reg_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_s2 : STD_LOGIC;
  signal time_out_wait_bypass_s3 : STD_LOGIC;
  signal time_tlock_max : STD_LOGIC;
  signal time_tlock_max1 : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_1\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_2\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_3\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_n_3\ : STD_LOGIC;
  signal time_tlock_max1_carry_i_1_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_2_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_3_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_4_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_5_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_6_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_7_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_8_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_n_1 : STD_LOGIC;
  signal time_tlock_max1_carry_n_2 : STD_LOGIC;
  signal time_tlock_max1_carry_n_3 : STD_LOGIC;
  signal time_tlock_max_i_1_n_0 : STD_LOGIC;
  signal \wait_bypass_count[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_4_n_0\ : STD_LOGIC;
  signal wait_bypass_count_reg : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \wait_bypass_count_reg[0]_i_3__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_7\ : STD_LOGIC;
  signal \wait_time_cnt0__0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \wait_time_cnt[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_2__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_3__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_4__0_n_0\ : STD_LOGIC;
  signal wait_time_cnt_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_time_tlock_max1_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_time_tlock_max1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_time_tlock_max1_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_time_tlock_max1_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[2]_i_2\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[3]_i_10\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[3]_i_9\ : label is "soft_lutpair43";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[0]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[1]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[2]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[3]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute SOFT_HLUTNM of check_tlock_max_i_1 : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \init_wait_count[1]_i_1__0\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \init_wait_count[2]_i_1__0\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \init_wait_count[3]_i_1__0\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \init_wait_count[4]_i_1__0\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_2__0\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_3__0\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \mmcm_lock_count[1]_i_1__0\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \mmcm_lock_count[2]_i_1__0\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \mmcm_lock_count[3]_i_1__0\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \mmcm_lock_count[4]_i_1__0\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \mmcm_lock_count[6]_i_1__0\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \mmcm_lock_count[7]_i_3__0\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of reset_time_out_i_3 : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of reset_time_out_i_4 : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of rx_fsm_reset_done_int_i_5 : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of rx_fsm_reset_done_int_i_6 : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of time_out_1us_i_2 : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of time_out_2ms_i_1 : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \time_out_2ms_i_3__0\ : label is "soft_lutpair51";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[0]_i_2__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[12]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[16]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[4]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[8]_i_1__0\ : label is 11;
  attribute COMPARATOR_THRESHOLD : integer;
  attribute COMPARATOR_THRESHOLD of time_tlock_max1_carry : label is 11;
  attribute COMPARATOR_THRESHOLD of \time_tlock_max1_carry__0\ : label is 11;
  attribute COMPARATOR_THRESHOLD of \time_tlock_max1_carry__1\ : label is 11;
  attribute SOFT_HLUTNM of time_tlock_max_i_1 : label is "soft_lutpair52";
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[0]_i_3__0\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[12]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[4]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[8]_i_1__0\ : label is 11;
  attribute SOFT_HLUTNM of \wait_time_cnt[0]_i_1__0\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \wait_time_cnt[1]_i_1__0\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \wait_time_cnt[3]_i_1__0\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \wait_time_cnt[4]_i_1__0\ : label is "soft_lutpair45";
begin
  data_in <= \^data_in\;
  gt0_rxuserrdy_t <= \^gt0_rxuserrdy_t\;
\FSM_sequential_rx_state[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222AAAA00000C00"
    )
        port map (
      I0 => time_out_2ms_reg_n_0,
      I1 => rx_state(2),
      I2 => rx_state(3),
      I3 => time_tlock_max,
      I4 => reset_time_out_reg_n_0,
      I5 => rx_state(1),
      O => \FSM_sequential_rx_state[0]_i_2_n_0\
    );
\FSM_sequential_rx_state[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000AABF000F0000"
    )
        port map (
      I0 => reset_time_out_reg_n_0,
      I1 => time_tlock_max,
      I2 => rx_state(2),
      I3 => rx_state(3),
      I4 => rx_state(1),
      I5 => rx_state(0),
      O => \FSM_sequential_rx_state[1]_i_3_n_0\
    );
\FSM_sequential_rx_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000050FF2200"
    )
        port map (
      I0 => rx_state(1),
      I1 => time_out_2ms_reg_n_0,
      I2 => \FSM_sequential_rx_state[2]_i_2_n_0\,
      I3 => rx_state(0),
      I4 => rx_state(2),
      I5 => rx_state(3),
      O => \rx_state__0\(2)
    );
\FSM_sequential_rx_state[2]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => reset_time_out_reg_n_0,
      I1 => time_tlock_max,
      O => \FSM_sequential_rx_state[2]_i_2_n_0\
    );
\FSM_sequential_rx_state[3]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => reset_time_out_reg_n_0,
      I1 => time_out_2ms_reg_n_0,
      O => \FSM_sequential_rx_state[3]_i_10_n_0\
    );
\FSM_sequential_rx_state[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000050005300"
    )
        port map (
      I0 => \FSM_sequential_rx_state[3]_i_10_n_0\,
      I1 => \wait_time_cnt[6]_i_4__0_n_0\,
      I2 => rx_state(0),
      I3 => rx_state(1),
      I4 => wait_time_cnt_reg(6),
      I5 => rx_state(3),
      O => \FSM_sequential_rx_state[3]_i_3_n_0\
    );
\FSM_sequential_rx_state[3]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000023002F00"
    )
        port map (
      I0 => time_out_2ms_reg_n_0,
      I1 => rx_state(2),
      I2 => rx_state(1),
      I3 => rx_state(0),
      I4 => \FSM_sequential_rx_state[2]_i_2_n_0\,
      I5 => rx_state(3),
      O => \FSM_sequential_rx_state[3]_i_7_n_0\
    );
\FSM_sequential_rx_state[3]_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80800080"
    )
        port map (
      I0 => rx_state(0),
      I1 => rx_state(1),
      I2 => rx_state(2),
      I3 => time_out_2ms_reg_n_0,
      I4 => reset_time_out_reg_n_0,
      O => \FSM_sequential_rx_state[3]_i_9_n_0\
    );
\FSM_sequential_rx_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(0),
      Q => rx_state(0),
      R => \out\(0)
    );
\FSM_sequential_rx_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(1),
      Q => rx_state(1),
      R => \out\(0)
    );
\FSM_sequential_rx_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(2),
      Q => rx_state(2),
      R => \out\(0)
    );
\FSM_sequential_rx_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(3),
      Q => rx_state(3),
      R => \out\(0)
    );
RXUSERRDY_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB4000"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(0),
      I2 => rx_state(2),
      I3 => rx_state(1),
      I4 => \^gt0_rxuserrdy_t\,
      O => RXUSERRDY_i_1_n_0
    );
RXUSERRDY_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => RXUSERRDY_i_1_n_0,
      Q => \^gt0_rxuserrdy_t\,
      R => \out\(0)
    );
check_tlock_max_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB0008"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(0),
      I2 => rx_state(1),
      I3 => rx_state(3),
      I4 => check_tlock_max_reg_n_0,
      O => check_tlock_max_i_1_n_0
    );
check_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => check_tlock_max_i_1_n_0,
      Q => check_tlock_max_reg_n_0,
      R => \out\(0)
    );
gtrxreset_i_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0100"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(1),
      I2 => rx_state(2),
      I3 => rx_state(0),
      I4 => GTRXRESET,
      O => gtrxreset_i_i_1_n_0
    );
gtrxreset_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gtrxreset_i_i_1_n_0,
      Q => GTRXRESET,
      R => \out\(0)
    );
gtxe2_i_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => GTRXRESET,
      I1 => \^data_in\,
      I2 => gtxe2_i,
      O => SR(0)
    );
\init_wait_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => init_wait_count_reg(0),
      O => \init_wait_count[0]_i_1__0_n_0\
    );
\init_wait_count[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      O => \p_0_in__2\(1)
    );
\init_wait_count[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      O => \p_0_in__2\(2)
    );
\init_wait_count[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => init_wait_count_reg(1),
      I1 => init_wait_count_reg(2),
      I2 => init_wait_count_reg(0),
      I3 => init_wait_count_reg(3),
      O => \p_0_in__2\(3)
    );
\init_wait_count[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      O => \p_0_in__2\(4)
    );
\init_wait_count[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      I5 => init_wait_count_reg(5),
      O => \p_0_in__2\(5)
    );
\init_wait_count[6]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => \init_wait_count[6]_i_3__0_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \init_wait_count[6]_i_1__0_n_0\
    );
\init_wait_count[6]_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF40"
    )
        port map (
      I0 => \init_wait_count[6]_i_3__0_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \p_0_in__2\(6)
    );
\init_wait_count[6]_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => init_wait_count_reg(3),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      I3 => init_wait_count_reg(5),
      O => \init_wait_count[6]_i_3__0_n_0\
    );
\init_wait_count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \init_wait_count[0]_i_1__0_n_0\,
      Q => init_wait_count_reg(0)
    );
\init_wait_count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(1),
      Q => init_wait_count_reg(1)
    );
\init_wait_count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(2),
      Q => init_wait_count_reg(2)
    );
\init_wait_count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(3),
      Q => init_wait_count_reg(3)
    );
\init_wait_count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(4),
      Q => init_wait_count_reg(4)
    );
\init_wait_count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(5),
      Q => init_wait_count_reg(5)
    );
\init_wait_count_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(6),
      Q => init_wait_count_reg(6)
    );
\init_wait_done_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0010"
    )
        port map (
      I0 => \init_wait_count[6]_i_3__0_n_0\,
      I1 => init_wait_count_reg(4),
      I2 => init_wait_count_reg(6),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_done_reg_n_0,
      O => \init_wait_done_i_1__0_n_0\
    );
init_wait_done_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      CLR => \out\(0),
      D => \init_wait_done_i_1__0_n_0\,
      Q => init_wait_done_reg_n_0
    );
\mmcm_lock_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      O => \p_0_in__3\(0)
    );
\mmcm_lock_count[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      I1 => mmcm_lock_count_reg(1),
      O => \p_0_in__3\(1)
    );
\mmcm_lock_count[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => mmcm_lock_count_reg(1),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(2),
      O => \mmcm_lock_count[2]_i_1__0_n_0\
    );
\mmcm_lock_count[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => mmcm_lock_count_reg(2),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(1),
      I3 => mmcm_lock_count_reg(3),
      O => \mmcm_lock_count[3]_i_1__0_n_0\
    );
\mmcm_lock_count[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => mmcm_lock_count_reg(3),
      I1 => mmcm_lock_count_reg(1),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(2),
      I4 => mmcm_lock_count_reg(4),
      O => \mmcm_lock_count[4]_i_1__0_n_0\
    );
\mmcm_lock_count[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => \mmcm_lock_count[5]_i_1__0_n_0\
    );
\mmcm_lock_count[6]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I1 => mmcm_lock_count_reg(6),
      O => \mmcm_lock_count[6]_i_1__0_n_0\
    );
\mmcm_lock_count[7]_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I1 => mmcm_lock_count_reg(6),
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_2__0_n_0\
    );
\mmcm_lock_count[7]_i_3__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => mmcm_lock_count_reg(6),
      I1 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_3__0_n_0\
    );
\mmcm_lock_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \p_0_in__3\(0),
      Q => mmcm_lock_count_reg(0),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \p_0_in__3\(1),
      Q => mmcm_lock_count_reg(1),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[2]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(2),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[3]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(3),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[4]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(4),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[5]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(5),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[6]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(6),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[7]_i_3__0_n_0\,
      Q => mmcm_lock_count_reg(7),
      R => sync_mmcm_lock_reclocked_n_0
    );
mmcm_lock_reclocked_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEA0000"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => mmcm_lock_count_reg(7),
      I2 => mmcm_lock_count_reg(6),
      I3 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I4 => mmcm_lock_i,
      O => mmcm_lock_reclocked_i_1_n_0
    );
\mmcm_lock_reclocked_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => \mmcm_lock_reclocked_i_2__0_n_0\
    );
mmcm_lock_reclocked_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => mmcm_lock_reclocked_i_1_n_0,
      Q => mmcm_lock_reclocked,
      R => '0'
    );
reset_time_out_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(3),
      O => reset_time_out_i_3_n_0
    );
reset_time_out_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"34347674"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(3),
      I2 => rx_state(0),
      I3 => \FSM_sequential_rx_state_reg[0]_0\,
      I4 => rx_state(1),
      O => reset_time_out_i_4_n_0
    );
reset_time_out_reg: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => sync_data_valid_n_0,
      Q => reset_time_out_reg_n_0,
      S => \out\(0)
    );
\run_phase_alignment_int_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFF0010"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(1),
      I2 => rx_state(3),
      I3 => rx_state(0),
      I4 => run_phase_alignment_int_reg_n_0,
      O => \run_phase_alignment_int_i_1__0_n_0\
    );
run_phase_alignment_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => \run_phase_alignment_int_i_1__0_n_0\,
      Q => run_phase_alignment_int_reg_n_0,
      R => \out\(0)
    );
run_phase_alignment_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => '1',
      D => data_out_0,
      Q => run_phase_alignment_int_s3_reg_n_0,
      R => '0'
    );
rx_fsm_reset_done_int_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => rx_state(1),
      I1 => rx_state(0),
      O => rx_fsm_reset_done_int_i_5_n_0
    );
rx_fsm_reset_done_int_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(2),
      O => rx_fsm_reset_done_int_i_6_n_0
    );
rx_fsm_reset_done_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => sync_data_valid_n_1,
      Q => \^data_in\,
      R => \out\(0)
    );
rx_fsm_reset_done_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => '1',
      D => rx_fsm_reset_done_int_s2,
      Q => rx_fsm_reset_done_int_s3,
      R => '0'
    );
rxresetdone_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => rxresetdone_s2,
      Q => rxresetdone_s3,
      R => '0'
    );
sync_RXRESETDONE: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_10
     port map (
      data_out => rxresetdone_s2,
      data_sync_reg1_0 => data_sync_reg1,
      independent_clock_bufg => independent_clock_bufg
    );
sync_cplllock: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_11
     port map (
      \FSM_sequential_rx_state_reg[1]\ => sync_cplllock_n_0,
      Q(2 downto 0) => rx_state(3 downto 1),
      data_sync_reg1_0 => data_sync_reg1_1,
      independent_clock_bufg => independent_clock_bufg,
      rxresetdone_s3 => rxresetdone_s3
    );
sync_data_valid: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_12
     port map (
      D(2) => \rx_state__0\(3),
      D(1 downto 0) => \rx_state__0\(1 downto 0),
      E(0) => sync_data_valid_n_5,
      \FSM_sequential_rx_state_reg[0]\ => \FSM_sequential_rx_state[3]_i_3_n_0\,
      \FSM_sequential_rx_state_reg[0]_0\ => \FSM_sequential_rx_state[3]_i_7_n_0\,
      \FSM_sequential_rx_state_reg[0]_1\ => \FSM_sequential_rx_state_reg[0]_0\,
      \FSM_sequential_rx_state_reg[0]_2\ => \FSM_sequential_rx_state[0]_i_2_n_0\,
      \FSM_sequential_rx_state_reg[0]_3\ => init_wait_done_reg_n_0,
      \FSM_sequential_rx_state_reg[1]\ => sync_data_valid_n_0,
      \FSM_sequential_rx_state_reg[1]_0\ => \FSM_sequential_rx_state[1]_i_3_n_0\,
      \FSM_sequential_rx_state_reg[3]\ => \FSM_sequential_rx_state[3]_i_9_n_0\,
      Q(3 downto 0) => rx_state(3 downto 0),
      data_in => \^data_in\,
      data_out => data_out,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_lock_reclocked => mmcm_lock_reclocked,
      reset_time_out_reg => sync_cplllock_n_0,
      reset_time_out_reg_0 => reset_time_out_i_3_n_0,
      reset_time_out_reg_1 => reset_time_out_i_4_n_0,
      reset_time_out_reg_2 => reset_time_out_reg_n_0,
      rx_fsm_reset_done_int_reg => sync_data_valid_n_1,
      rx_fsm_reset_done_int_reg_0 => rx_fsm_reset_done_int_i_5_n_0,
      rx_fsm_reset_done_int_reg_1 => time_out_100us_reg_n_0,
      rx_fsm_reset_done_int_reg_2 => time_out_1us_reg_n_0,
      rx_fsm_reset_done_int_reg_3 => rx_fsm_reset_done_int_i_6_n_0,
      time_out_wait_bypass_s3 => time_out_wait_bypass_s3
    );
sync_mmcm_lock_reclocked: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_13
     port map (
      SR(0) => sync_mmcm_lock_reclocked_n_0,
      data_out => mmcm_lock_i,
      data_sync_reg1_0 => data_sync_reg1_0,
      independent_clock_bufg => independent_clock_bufg
    );
sync_run_phase_alignment_int: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_14
     port map (
      data_in => run_phase_alignment_int_reg_n_0,
      data_out => data_out_0,
      data_sync_reg1_0 => data_sync_reg6
    );
sync_time_out_wait_bypass: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_15
     port map (
      data_in => time_out_wait_bypass_reg_n_0,
      data_out => time_out_wait_bypass_s2,
      independent_clock_bufg => independent_clock_bufg
    );
sync_tx_fsm_reset_done_int: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_16
     port map (
      data_in => \^data_in\,
      data_out => rx_fsm_reset_done_int_s2,
      data_sync_reg6_0 => data_sync_reg6
    );
time_out_100us_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000100"
    )
        port map (
      I0 => time_out_2ms_i_4_n_0,
      I1 => time_out_counter_reg(17),
      I2 => time_out_counter_reg(16),
      I3 => time_out_100us_i_2_n_0,
      I4 => time_out_100us_i_3_n_0,
      I5 => time_out_100us_reg_n_0,
      O => time_out_100us_i_1_n_0
    );
time_out_100us_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000002000000000"
    )
        port map (
      I0 => time_out_counter_reg(10),
      I1 => time_out_counter_reg(12),
      I2 => time_out_counter_reg(5),
      I3 => time_out_counter_reg(7),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(14),
      O => time_out_100us_i_2_n_0
    );
time_out_100us_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => time_out_counter_reg(4),
      I1 => time_out_counter_reg(0),
      I2 => time_out_counter_reg(1),
      I3 => time_out_counter_reg(15),
      I4 => time_out_counter_reg(13),
      O => time_out_100us_i_3_n_0
    );
time_out_100us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_100us_i_1_n_0,
      Q => time_out_100us_reg_n_0,
      R => reset_time_out_reg_n_0
    );
time_out_1us_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00100000"
    )
        port map (
      I0 => time_out_2ms_i_2_n_0,
      I1 => time_out_1us_i_2_n_0,
      I2 => time_out_counter_reg(3),
      I3 => time_out_counter_reg(2),
      I4 => time_out_1us_i_3_n_0,
      I5 => time_out_1us_reg_n_0,
      O => time_out_1us_i_1_n_0
    );
time_out_1us_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(17),
      O => time_out_1us_i_2_n_0
    );
time_out_1us_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000010"
    )
        port map (
      I0 => time_out_counter_reg(9),
      I1 => time_out_counter_reg(11),
      I2 => time_out_counter_reg(6),
      I3 => time_out_counter_reg(8),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(12),
      O => time_out_1us_i_3_n_0
    );
time_out_1us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_1us_i_1_n_0,
      Q => time_out_1us_reg_n_0,
      R => reset_time_out_reg_n_0
    );
time_out_2ms_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF01"
    )
        port map (
      I0 => time_out_2ms_i_2_n_0,
      I1 => \time_out_2ms_i_3__0_n_0\,
      I2 => time_out_2ms_i_4_n_0,
      I3 => time_out_2ms_reg_n_0,
      O => time_out_2ms_i_1_n_0
    );
time_out_2ms_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFEFF"
    )
        port map (
      I0 => time_out_counter_reg(10),
      I1 => time_out_counter_reg(14),
      I2 => time_out_counter_reg(5),
      I3 => time_out_counter_reg(7),
      I4 => time_out_100us_i_3_n_0,
      O => time_out_2ms_i_2_n_0
    );
\time_out_2ms_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => time_out_counter_reg(12),
      I1 => time_out_counter_reg(16),
      I2 => time_out_counter_reg(18),
      I3 => time_out_counter_reg(17),
      O => \time_out_2ms_i_3__0_n_0\
    );
time_out_2ms_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFEFFFFFF"
    )
        port map (
      I0 => time_out_counter_reg(2),
      I1 => time_out_counter_reg(3),
      I2 => time_out_counter_reg(8),
      I3 => time_out_counter_reg(9),
      I4 => time_out_counter_reg(11),
      I5 => time_out_counter_reg(6),
      O => time_out_2ms_i_4_n_0
    );
time_out_2ms_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_2ms_i_1_n_0,
      Q => time_out_2ms_reg_n_0,
      R => reset_time_out_reg_n_0
    );
\time_out_counter[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFF7FF"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(18),
      I2 => time_out_counter_reg(16),
      I3 => time_out_counter_reg(12),
      I4 => time_out_2ms_i_2_n_0,
      I5 => time_out_2ms_i_4_n_0,
      O => time_out_counter
    );
\time_out_counter[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(0),
      O => \time_out_counter[0]_i_3_n_0\
    );
\time_out_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_7\,
      Q => time_out_counter_reg(0),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[0]_i_2__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \time_out_counter_reg[0]_i_2__0_n_0\,
      CO(2) => \time_out_counter_reg[0]_i_2__0_n_1\,
      CO(1) => \time_out_counter_reg[0]_i_2__0_n_2\,
      CO(0) => \time_out_counter_reg[0]_i_2__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \time_out_counter_reg[0]_i_2__0_n_4\,
      O(2) => \time_out_counter_reg[0]_i_2__0_n_5\,
      O(1) => \time_out_counter_reg[0]_i_2__0_n_6\,
      O(0) => \time_out_counter_reg[0]_i_2__0_n_7\,
      S(3 downto 1) => time_out_counter_reg(3 downto 1),
      S(0) => \time_out_counter[0]_i_3_n_0\
    );
\time_out_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_5\,
      Q => time_out_counter_reg(10),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_4\,
      Q => time_out_counter_reg(11),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_7\,
      Q => time_out_counter_reg(12),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[12]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[8]_i_1__0_n_0\,
      CO(3) => \time_out_counter_reg[12]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[12]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[12]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[12]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[12]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[12]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[12]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[12]_i_1__0_n_7\,
      S(3 downto 0) => time_out_counter_reg(15 downto 12)
    );
\time_out_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_6\,
      Q => time_out_counter_reg(13),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_5\,
      Q => time_out_counter_reg(14),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_4\,
      Q => time_out_counter_reg(15),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1__0_n_7\,
      Q => time_out_counter_reg(16),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[16]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[12]_i_1__0_n_0\,
      CO(3 downto 2) => \NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \time_out_counter_reg[16]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[16]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED\(3),
      O(2) => \time_out_counter_reg[16]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[16]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[16]_i_1__0_n_7\,
      S(3) => '0',
      S(2 downto 0) => time_out_counter_reg(18 downto 16)
    );
\time_out_counter_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1__0_n_6\,
      Q => time_out_counter_reg(17),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1__0_n_5\,
      Q => time_out_counter_reg(18),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_6\,
      Q => time_out_counter_reg(1),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_5\,
      Q => time_out_counter_reg(2),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_4\,
      Q => time_out_counter_reg(3),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_7\,
      Q => time_out_counter_reg(4),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[4]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[0]_i_2__0_n_0\,
      CO(3) => \time_out_counter_reg[4]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[4]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[4]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[4]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[4]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[4]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[4]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[4]_i_1__0_n_7\,
      S(3 downto 0) => time_out_counter_reg(7 downto 4)
    );
\time_out_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_6\,
      Q => time_out_counter_reg(5),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_5\,
      Q => time_out_counter_reg(6),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_4\,
      Q => time_out_counter_reg(7),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_7\,
      Q => time_out_counter_reg(8),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[8]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[4]_i_1__0_n_0\,
      CO(3) => \time_out_counter_reg[8]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[8]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[8]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[8]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[8]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[8]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[8]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[8]_i_1__0_n_7\,
      S(3 downto 0) => time_out_counter_reg(11 downto 8)
    );
\time_out_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_6\,
      Q => time_out_counter_reg(9),
      R => reset_time_out_reg_n_0
    );
time_out_wait_bypass_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AB00"
    )
        port map (
      I0 => time_out_wait_bypass_reg_n_0,
      I1 => rx_fsm_reset_done_int_s3,
      I2 => \time_out_wait_bypass_i_2__0_n_0\,
      I3 => run_phase_alignment_int_s3_reg_n_0,
      O => time_out_wait_bypass_i_1_n_0
    );
\time_out_wait_bypass_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FBFFFFFF"
    )
        port map (
      I0 => \time_out_wait_bypass_i_3__0_n_0\,
      I1 => wait_bypass_count_reg(1),
      I2 => wait_bypass_count_reg(11),
      I3 => wait_bypass_count_reg(0),
      I4 => \time_out_wait_bypass_i_4__0_n_0\,
      O => \time_out_wait_bypass_i_2__0_n_0\
    );
\time_out_wait_bypass_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => wait_bypass_count_reg(9),
      I1 => wait_bypass_count_reg(4),
      I2 => wait_bypass_count_reg(12),
      I3 => wait_bypass_count_reg(2),
      O => \time_out_wait_bypass_i_3__0_n_0\
    );
\time_out_wait_bypass_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000400000000"
    )
        port map (
      I0 => wait_bypass_count_reg(5),
      I1 => wait_bypass_count_reg(7),
      I2 => wait_bypass_count_reg(3),
      I3 => wait_bypass_count_reg(6),
      I4 => wait_bypass_count_reg(10),
      I5 => wait_bypass_count_reg(8),
      O => \time_out_wait_bypass_i_4__0_n_0\
    );
time_out_wait_bypass_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => '1',
      D => time_out_wait_bypass_i_1_n_0,
      Q => time_out_wait_bypass_reg_n_0,
      R => '0'
    );
time_out_wait_bypass_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_wait_bypass_s2,
      Q => time_out_wait_bypass_s3,
      R => '0'
    );
time_tlock_max1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => time_tlock_max1_carry_n_0,
      CO(2) => time_tlock_max1_carry_n_1,
      CO(1) => time_tlock_max1_carry_n_2,
      CO(0) => time_tlock_max1_carry_n_3,
      CYINIT => '0',
      DI(3) => time_tlock_max1_carry_i_1_n_0,
      DI(2) => time_tlock_max1_carry_i_2_n_0,
      DI(1) => time_tlock_max1_carry_i_3_n_0,
      DI(0) => time_tlock_max1_carry_i_4_n_0,
      O(3 downto 0) => NLW_time_tlock_max1_carry_O_UNCONNECTED(3 downto 0),
      S(3) => time_tlock_max1_carry_i_5_n_0,
      S(2) => time_tlock_max1_carry_i_6_n_0,
      S(1) => time_tlock_max1_carry_i_7_n_0,
      S(0) => time_tlock_max1_carry_i_8_n_0
    );
\time_tlock_max1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => time_tlock_max1_carry_n_0,
      CO(3) => \time_tlock_max1_carry__0_n_0\,
      CO(2) => \time_tlock_max1_carry__0_n_1\,
      CO(1) => \time_tlock_max1_carry__0_n_2\,
      CO(0) => \time_tlock_max1_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => time_out_counter_reg(15),
      DI(2) => \time_tlock_max1_carry__0_i_1_n_0\,
      DI(1) => '0',
      DI(0) => \time_tlock_max1_carry__0_i_2_n_0\,
      O(3 downto 0) => \NLW_time_tlock_max1_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \time_tlock_max1_carry__0_i_3_n_0\,
      S(2) => \time_tlock_max1_carry__0_i_4_n_0\,
      S(1) => \time_tlock_max1_carry__0_i_5_n_0\,
      S(0) => \time_tlock_max1_carry__0_i_6_n_0\
    );
\time_tlock_max1_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(12),
      I1 => time_out_counter_reg(13),
      O => \time_tlock_max1_carry__0_i_1_n_0\
    );
\time_tlock_max1_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(8),
      I1 => time_out_counter_reg(9),
      O => \time_tlock_max1_carry__0_i_2_n_0\
    );
\time_tlock_max1_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(14),
      I1 => time_out_counter_reg(15),
      O => \time_tlock_max1_carry__0_i_3_n_0\
    );
\time_tlock_max1_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(13),
      I1 => time_out_counter_reg(12),
      O => \time_tlock_max1_carry__0_i_4_n_0\
    );
\time_tlock_max1_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(10),
      I1 => time_out_counter_reg(11),
      O => \time_tlock_max1_carry__0_i_5_n_0\
    );
\time_tlock_max1_carry__0_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(9),
      I1 => time_out_counter_reg(8),
      O => \time_tlock_max1_carry__0_i_6_n_0\
    );
\time_tlock_max1_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_tlock_max1_carry__0_n_0\,
      CO(3 downto 2) => \NLW_time_tlock_max1_carry__1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => time_tlock_max1,
      CO(0) => \time_tlock_max1_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => time_out_counter_reg(18),
      DI(0) => \time_tlock_max1_carry__1_i_1_n_0\,
      O(3 downto 0) => \NLW_time_tlock_max1_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3 downto 2) => B"00",
      S(1) => \time_tlock_max1_carry__1_i_2_n_0\,
      S(0) => \time_tlock_max1_carry__1_i_3_n_0\
    );
\time_tlock_max1_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(17),
      O => \time_tlock_max1_carry__1_i_1_n_0\
    );
\time_tlock_max1_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(18),
      O => \time_tlock_max1_carry__1_i_2_n_0\
    );
\time_tlock_max1_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(16),
      O => \time_tlock_max1_carry__1_i_3_n_0\
    );
time_tlock_max1_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(6),
      I1 => time_out_counter_reg(7),
      O => time_tlock_max1_carry_i_1_n_0
    );
time_tlock_max1_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(4),
      I1 => time_out_counter_reg(5),
      O => time_tlock_max1_carry_i_2_n_0
    );
time_tlock_max1_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(2),
      I1 => time_out_counter_reg(3),
      O => time_tlock_max1_carry_i_3_n_0
    );
time_tlock_max1_carry_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(0),
      I1 => time_out_counter_reg(1),
      O => time_tlock_max1_carry_i_4_n_0
    );
time_tlock_max1_carry_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(7),
      I1 => time_out_counter_reg(6),
      O => time_tlock_max1_carry_i_5_n_0
    );
time_tlock_max1_carry_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(5),
      I1 => time_out_counter_reg(4),
      O => time_tlock_max1_carry_i_6_n_0
    );
time_tlock_max1_carry_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(3),
      I1 => time_out_counter_reg(2),
      O => time_tlock_max1_carry_i_7_n_0
    );
time_tlock_max1_carry_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(1),
      I1 => time_out_counter_reg(0),
      O => time_tlock_max1_carry_i_8_n_0
    );
time_tlock_max_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => check_tlock_max_reg_n_0,
      I1 => time_tlock_max1,
      I2 => time_tlock_max,
      O => time_tlock_max_i_1_n_0
    );
time_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_tlock_max_i_1_n_0,
      Q => time_tlock_max,
      R => reset_time_out_reg_n_0
    );
\wait_bypass_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => run_phase_alignment_int_s3_reg_n_0,
      O => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count[0]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \time_out_wait_bypass_i_2__0_n_0\,
      I1 => rx_fsm_reset_done_int_s3,
      O => \wait_bypass_count[0]_i_2__0_n_0\
    );
\wait_bypass_count[0]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_bypass_count_reg(0),
      O => \wait_bypass_count[0]_i_4_n_0\
    );
\wait_bypass_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_7\,
      Q => wait_bypass_count_reg(0),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[0]_i_3__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \wait_bypass_count_reg[0]_i_3__0_n_0\,
      CO(2) => \wait_bypass_count_reg[0]_i_3__0_n_1\,
      CO(1) => \wait_bypass_count_reg[0]_i_3__0_n_2\,
      CO(0) => \wait_bypass_count_reg[0]_i_3__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \wait_bypass_count_reg[0]_i_3__0_n_4\,
      O(2) => \wait_bypass_count_reg[0]_i_3__0_n_5\,
      O(1) => \wait_bypass_count_reg[0]_i_3__0_n_6\,
      O(0) => \wait_bypass_count_reg[0]_i_3__0_n_7\,
      S(3 downto 1) => wait_bypass_count_reg(3 downto 1),
      S(0) => \wait_bypass_count[0]_i_4_n_0\
    );
\wait_bypass_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_5\,
      Q => wait_bypass_count_reg(10),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_4\,
      Q => wait_bypass_count_reg(11),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[12]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(12),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[12]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[8]_i_1__0_n_0\,
      CO(3 downto 0) => \NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED\(3 downto 1),
      O(0) => \wait_bypass_count_reg[12]_i_1__0_n_7\,
      S(3 downto 1) => B"000",
      S(0) => wait_bypass_count_reg(12)
    );
\wait_bypass_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_6\,
      Q => wait_bypass_count_reg(1),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_5\,
      Q => wait_bypass_count_reg(2),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_4\,
      Q => wait_bypass_count_reg(3),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(4),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[4]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[0]_i_3__0_n_0\,
      CO(3) => \wait_bypass_count_reg[4]_i_1__0_n_0\,
      CO(2) => \wait_bypass_count_reg[4]_i_1__0_n_1\,
      CO(1) => \wait_bypass_count_reg[4]_i_1__0_n_2\,
      CO(0) => \wait_bypass_count_reg[4]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[4]_i_1__0_n_4\,
      O(2) => \wait_bypass_count_reg[4]_i_1__0_n_5\,
      O(1) => \wait_bypass_count_reg[4]_i_1__0_n_6\,
      O(0) => \wait_bypass_count_reg[4]_i_1__0_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(7 downto 4)
    );
\wait_bypass_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_6\,
      Q => wait_bypass_count_reg(5),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_5\,
      Q => wait_bypass_count_reg(6),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_4\,
      Q => wait_bypass_count_reg(7),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(8),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[8]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[4]_i_1__0_n_0\,
      CO(3) => \wait_bypass_count_reg[8]_i_1__0_n_0\,
      CO(2) => \wait_bypass_count_reg[8]_i_1__0_n_1\,
      CO(1) => \wait_bypass_count_reg[8]_i_1__0_n_2\,
      CO(0) => \wait_bypass_count_reg[8]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[8]_i_1__0_n_4\,
      O(2) => \wait_bypass_count_reg[8]_i_1__0_n_5\,
      O(1) => \wait_bypass_count_reg[8]_i_1__0_n_6\,
      O(0) => \wait_bypass_count_reg[8]_i_1__0_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(11 downto 8)
    );
\wait_bypass_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_6\,
      Q => wait_bypass_count_reg(9),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_time_cnt[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      O => \wait_time_cnt0__0\(0)
    );
\wait_time_cnt[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      I1 => wait_time_cnt_reg(1),
      O => \wait_time_cnt[1]_i_1__0_n_0\
    );
\wait_time_cnt[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => wait_time_cnt_reg(1),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(2),
      O => \wait_time_cnt[2]_i_1__0_n_0\
    );
\wait_time_cnt[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE01"
    )
        port map (
      I0 => wait_time_cnt_reg(2),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(1),
      I3 => wait_time_cnt_reg(3),
      O => \wait_time_cnt[3]_i_1__0_n_0\
    );
\wait_time_cnt[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0001"
    )
        port map (
      I0 => wait_time_cnt_reg(3),
      I1 => wait_time_cnt_reg(1),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(2),
      I4 => wait_time_cnt_reg(4),
      O => \wait_time_cnt[4]_i_1__0_n_0\
    );
\wait_time_cnt[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000001"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[5]_i_1__0_n_0\
    );
\wait_time_cnt[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => rx_state(0),
      I1 => rx_state(1),
      I2 => rx_state(3),
      O => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt[6]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4__0_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => \wait_time_cnt[6]_i_2__0_n_0\
    );
\wait_time_cnt[6]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4__0_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => \wait_time_cnt[6]_i_3__0_n_0\
    );
\wait_time_cnt[6]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[6]_i_4__0_n_0\
    );
\wait_time_cnt_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt0__0\(0),
      Q => wait_time_cnt_reg(0),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[1]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(1),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[2]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(2),
      S => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[3]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(3),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[4]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(4),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[5]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(5),
      S => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[6]_i_3__0_n_0\,
      Q => wait_time_cnt_reg(6),
      S => \wait_time_cnt[6]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_TX_STARTUP_FSM is
  port (
    mmcm_reset : out STD_LOGIC;
    gt0_cpllreset_t : out STD_LOGIC;
    data_in : out STD_LOGIC;
    gt0_txuserrdy_t : out STD_LOGIC;
    gt0_gttxreset_in0_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : in STD_LOGIC;
    gt0_cpllrefclklost_i : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    data_sync_reg1_1 : in STD_LOGIC;
    data_sync_reg1_2 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_TX_STARTUP_FSM;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_TX_STARTUP_FSM is
  signal CPLL_RESET_i_1_n_0 : STD_LOGIC;
  signal CPLL_RESET_i_2_n_0 : STD_LOGIC;
  signal \FSM_sequential_tx_state[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[0]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_8_n_0\ : STD_LOGIC;
  signal GTTXRESET : STD_LOGIC;
  signal MMCM_RESET_i_1_n_0 : STD_LOGIC;
  signal TXUSERRDY_i_1_n_0 : STD_LOGIC;
  signal clear : STD_LOGIC;
  signal \^data_in\ : STD_LOGIC;
  signal data_out : STD_LOGIC;
  signal \^gt0_cpllreset_t\ : STD_LOGIC;
  signal \^gt0_txuserrdy_t\ : STD_LOGIC;
  signal gttxreset_i_i_1_n_0 : STD_LOGIC;
  signal \init_wait_count[0]_i_1_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_1_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_3_n_0\ : STD_LOGIC;
  signal init_wait_count_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal init_wait_done_i_1_n_0 : STD_LOGIC;
  signal init_wait_done_reg_n_0 : STD_LOGIC;
  signal \mmcm_lock_count[2]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[3]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[4]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[5]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[6]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_2_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_3_n_0\ : STD_LOGIC;
  signal mmcm_lock_count_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal mmcm_lock_i : STD_LOGIC;
  signal mmcm_lock_reclocked : STD_LOGIC;
  signal mmcm_lock_reclocked_i_1_n_0 : STD_LOGIC;
  signal mmcm_lock_reclocked_i_2_n_0 : STD_LOGIC;
  signal \^mmcm_reset\ : STD_LOGIC;
  signal \p_0_in__0\ : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \p_0_in__1\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pll_reset_asserted_i_1_n_0 : STD_LOGIC;
  signal pll_reset_asserted_i_2_n_0 : STD_LOGIC;
  signal pll_reset_asserted_reg_n_0 : STD_LOGIC;
  signal refclk_stable_count : STD_LOGIC;
  signal \refclk_stable_count[0]_i_3_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_4_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_5_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_6_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_7_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_8_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_9_n_0\ : STD_LOGIC;
  signal refclk_stable_count_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \refclk_stable_count_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal refclk_stable_i_1_n_0 : STD_LOGIC;
  signal refclk_stable_i_2_n_0 : STD_LOGIC;
  signal refclk_stable_i_3_n_0 : STD_LOGIC;
  signal refclk_stable_i_4_n_0 : STD_LOGIC;
  signal refclk_stable_i_5_n_0 : STD_LOGIC;
  signal refclk_stable_i_6_n_0 : STD_LOGIC;
  signal refclk_stable_reg_n_0 : STD_LOGIC;
  signal reset_time_out : STD_LOGIC;
  signal \reset_time_out_i_2__0_n_0\ : STD_LOGIC;
  signal run_phase_alignment_int_i_1_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_reg_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_s3 : STD_LOGIC;
  signal sel : STD_LOGIC;
  signal sync_cplllock_n_0 : STD_LOGIC;
  signal sync_cplllock_n_1 : STD_LOGIC;
  signal sync_mmcm_lock_reclocked_n_0 : STD_LOGIC;
  signal time_out_2ms_i_1_n_0 : STD_LOGIC;
  signal \time_out_2ms_i_2__0_n_0\ : STD_LOGIC;
  signal time_out_2ms_i_3_n_0 : STD_LOGIC;
  signal \time_out_2ms_i_4__0_n_0\ : STD_LOGIC;
  signal time_out_2ms_i_5_n_0 : STD_LOGIC;
  signal time_out_2ms_reg_n_0 : STD_LOGIC;
  signal time_out_500us_i_1_n_0 : STD_LOGIC;
  signal time_out_500us_i_2_n_0 : STD_LOGIC;
  signal time_out_500us_reg_n_0 : STD_LOGIC;
  signal time_out_counter : STD_LOGIC;
  signal \time_out_counter[0]_i_3__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[0]_i_4_n_0\ : STD_LOGIC;
  signal time_out_counter_reg : STD_LOGIC_VECTOR ( 18 downto 0 );
  signal \time_out_counter_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal time_out_wait_bypass_i_1_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_2_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_3_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_4_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_5_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_reg_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_s2 : STD_LOGIC;
  signal time_out_wait_bypass_s3 : STD_LOGIC;
  signal time_tlock_max_i_1_n_0 : STD_LOGIC;
  signal time_tlock_max_i_2_n_0 : STD_LOGIC;
  signal time_tlock_max_i_3_n_0 : STD_LOGIC;
  signal time_tlock_max_i_4_n_0 : STD_LOGIC;
  signal time_tlock_max_reg_n_0 : STD_LOGIC;
  signal tx_fsm_reset_done_int_i_1_n_0 : STD_LOGIC;
  signal tx_fsm_reset_done_int_s2 : STD_LOGIC;
  signal tx_fsm_reset_done_int_s3 : STD_LOGIC;
  signal tx_state : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \tx_state__0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal txresetdone_s2 : STD_LOGIC;
  signal txresetdone_s3 : STD_LOGIC;
  signal \wait_bypass_count[0]_i_2_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_4__0_n_0\ : STD_LOGIC;
  signal wait_bypass_count_reg : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal \wait_bypass_count_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal wait_time_cnt0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal wait_time_cnt0_0 : STD_LOGIC;
  signal \wait_time_cnt[1]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[2]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[3]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[4]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[5]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_3_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_4_n_0\ : STD_LOGIC;
  signal wait_time_cnt_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of CPLL_RESET_i_2 : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[0]_i_2\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[0]_i_3\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[1]_i_1\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[3]_i_2\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[3]_i_7\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[3]_i_8\ : label is "soft_lutpair59";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[0]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[1]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[2]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[3]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute SOFT_HLUTNM of MMCM_RESET_i_1 : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of TXUSERRDY_i_1 : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of gttxreset_i_i_1 : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \init_wait_count[1]_i_1\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \init_wait_count[2]_i_1\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \init_wait_count[3]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \init_wait_count[4]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_2\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_3\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \mmcm_lock_count[1]_i_1\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \mmcm_lock_count[2]_i_1\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \mmcm_lock_count[3]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \mmcm_lock_count[4]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \mmcm_lock_count[6]_i_1\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \mmcm_lock_count[7]_i_3\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of pll_reset_asserted_i_2 : label is "soft_lutpair60";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[12]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[16]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[20]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[24]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[28]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[8]_i_1\ : label is 11;
  attribute SOFT_HLUTNM of run_phase_alignment_int_i_1 : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \time_out_2ms_i_4__0\ : label is "soft_lutpair56";
  attribute ADDER_THRESHOLD of \time_out_counter_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[12]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[16]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[8]_i_1\ : label is 11;
  attribute SOFT_HLUTNM of time_tlock_max_i_4 : label is "soft_lutpair56";
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[0]_i_3\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[12]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[16]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[8]_i_1\ : label is 11;
  attribute SOFT_HLUTNM of \wait_time_cnt[0]_i_1\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \wait_time_cnt[1]_i_1\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \wait_time_cnt[3]_i_1\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \wait_time_cnt[4]_i_1\ : label is "soft_lutpair63";
begin
  data_in <= \^data_in\;
  gt0_cpllreset_t <= \^gt0_cpllreset_t\;
  gt0_txuserrdy_t <= \^gt0_txuserrdy_t\;
  mmcm_reset <= \^mmcm_reset\;
CPLL_RESET_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF1F0000001F"
    )
        port map (
      I0 => pll_reset_asserted_reg_n_0,
      I1 => gt0_cpllrefclklost_i,
      I2 => refclk_stable_reg_n_0,
      I3 => CPLL_RESET_i_2_n_0,
      I4 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I5 => \^gt0_cpllreset_t\,
      O => CPLL_RESET_i_1_n_0
    );
CPLL_RESET_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      O => CPLL_RESET_i_2_n_0
    );
CPLL_RESET_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => CPLL_RESET_i_1_n_0,
      Q => \^gt0_cpllreset_t\,
      R => \out\(0)
    );
\FSM_sequential_tx_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F3FFF3F0F5F0F5F0"
    )
        port map (
      I0 => \FSM_sequential_tx_state[2]_i_2_n_0\,
      I1 => \FSM_sequential_tx_state[0]_i_2_n_0\,
      I2 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I3 => tx_state(2),
      I4 => time_out_2ms_reg_n_0,
      I5 => tx_state(1),
      O => \tx_state__0\(0)
    );
\FSM_sequential_tx_state[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => reset_time_out,
      I1 => time_out_500us_reg_n_0,
      O => \FSM_sequential_tx_state[0]_i_2_n_0\
    );
\FSM_sequential_tx_state[0]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      O => \FSM_sequential_tx_state[0]_i_3_n_0\
    );
\FSM_sequential_tx_state[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"005A001A"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(0),
      I3 => tx_state(3),
      I4 => \FSM_sequential_tx_state[2]_i_2_n_0\,
      O => \tx_state__0\(1)
    );
\FSM_sequential_tx_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"04000C0C06020C0C"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(3),
      I3 => \FSM_sequential_tx_state[2]_i_2_n_0\,
      I4 => tx_state(0),
      I5 => time_out_2ms_reg_n_0,
      O => \tx_state__0\(2)
    );
\FSM_sequential_tx_state[2]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => time_tlock_max_reg_n_0,
      I1 => reset_time_out,
      I2 => mmcm_lock_reclocked,
      O => \FSM_sequential_tx_state[2]_i_2_n_0\
    );
\FSM_sequential_tx_state[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F4FF4444"
    )
        port map (
      I0 => time_out_wait_bypass_s3,
      I1 => tx_state(3),
      I2 => reset_time_out,
      I3 => time_out_500us_reg_n_0,
      I4 => \FSM_sequential_tx_state[3]_i_8_n_0\,
      O => \tx_state__0\(3)
    );
\FSM_sequential_tx_state[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00BA000000000000"
    )
        port map (
      I0 => txresetdone_s3,
      I1 => reset_time_out,
      I2 => time_out_500us_reg_n_0,
      I3 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I4 => tx_state(2),
      I5 => tx_state(1),
      O => \FSM_sequential_tx_state[3]_i_3_n_0\
    );
\FSM_sequential_tx_state[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000300FF00AA"
    )
        port map (
      I0 => init_wait_done_reg_n_0,
      I1 => \wait_time_cnt[6]_i_4_n_0\,
      I2 => wait_time_cnt_reg(6),
      I3 => tx_state(0),
      I4 => tx_state(3),
      I5 => CPLL_RESET_i_2_n_0,
      O => \FSM_sequential_tx_state[3]_i_4_n_0\
    );
\FSM_sequential_tx_state[3]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0404040400040000"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I3 => reset_time_out,
      I4 => time_tlock_max_reg_n_0,
      I5 => mmcm_lock_reclocked,
      O => \FSM_sequential_tx_state[3]_i_6_n_0\
    );
\FSM_sequential_tx_state[3]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1000"
    )
        port map (
      I0 => tx_state(2),
      I1 => tx_state(3),
      I2 => tx_state(0),
      I3 => tx_state(1),
      O => \FSM_sequential_tx_state[3]_i_7_n_0\
    );
\FSM_sequential_tx_state[3]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      I2 => tx_state(2),
      I3 => tx_state(1),
      O => \FSM_sequential_tx_state[3]_i_8_n_0\
    );
\FSM_sequential_tx_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(0),
      Q => tx_state(0),
      R => \out\(0)
    );
\FSM_sequential_tx_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(1),
      Q => tx_state(1),
      R => \out\(0)
    );
\FSM_sequential_tx_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(2),
      Q => tx_state(2),
      R => \out\(0)
    );
\FSM_sequential_tx_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(3),
      Q => tx_state(3),
      R => \out\(0)
    );
MMCM_RESET_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70004"
    )
        port map (
      I0 => tx_state(2),
      I1 => tx_state(0),
      I2 => tx_state(3),
      I3 => tx_state(1),
      I4 => \^mmcm_reset\,
      O => MMCM_RESET_i_1_n_0
    );
MMCM_RESET_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => MMCM_RESET_i_1_n_0,
      Q => \^mmcm_reset\,
      R => \out\(0)
    );
TXUSERRDY_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFD2000"
    )
        port map (
      I0 => tx_state(0),
      I1 => tx_state(3),
      I2 => tx_state(2),
      I3 => tx_state(1),
      I4 => \^gt0_txuserrdy_t\,
      O => TXUSERRDY_i_1_n_0
    );
TXUSERRDY_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => TXUSERRDY_i_1_n_0,
      Q => \^gt0_txuserrdy_t\,
      R => \out\(0)
    );
gttxreset_i_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0100"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(1),
      I2 => tx_state(2),
      I3 => tx_state(0),
      I4 => GTTXRESET,
      O => gttxreset_i_i_1_n_0
    );
gttxreset_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gttxreset_i_i_1_n_0,
      Q => GTTXRESET,
      R => \out\(0)
    );
gtxe2_i_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => GTTXRESET,
      I1 => \^data_in\,
      I2 => gtxe2_i,
      O => gt0_gttxreset_in0_out
    );
\init_wait_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => init_wait_count_reg(0),
      O => \init_wait_count[0]_i_1_n_0\
    );
\init_wait_count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      O => \p_0_in__0\(1)
    );
\init_wait_count[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      O => \p_0_in__0\(2)
    );
\init_wait_count[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => init_wait_count_reg(1),
      I1 => init_wait_count_reg(2),
      I2 => init_wait_count_reg(0),
      I3 => init_wait_count_reg(3),
      O => \p_0_in__0\(3)
    );
\init_wait_count[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      O => \p_0_in__0\(4)
    );
\init_wait_count[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      I5 => init_wait_count_reg(5),
      O => \p_0_in__0\(5)
    );
\init_wait_count[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => \init_wait_count[6]_i_3_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \init_wait_count[6]_i_1_n_0\
    );
\init_wait_count[6]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF40"
    )
        port map (
      I0 => \init_wait_count[6]_i_3_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \p_0_in__0\(6)
    );
\init_wait_count[6]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => init_wait_count_reg(3),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      I3 => init_wait_count_reg(5),
      O => \init_wait_count[6]_i_3_n_0\
    );
\init_wait_count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \init_wait_count[0]_i_1_n_0\,
      Q => init_wait_count_reg(0)
    );
\init_wait_count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(1),
      Q => init_wait_count_reg(1)
    );
\init_wait_count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(2),
      Q => init_wait_count_reg(2)
    );
\init_wait_count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(3),
      Q => init_wait_count_reg(3)
    );
\init_wait_count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(4),
      Q => init_wait_count_reg(4)
    );
\init_wait_count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(5),
      Q => init_wait_count_reg(5)
    );
\init_wait_count_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(6),
      Q => init_wait_count_reg(6)
    );
init_wait_done_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0010"
    )
        port map (
      I0 => \init_wait_count[6]_i_3_n_0\,
      I1 => init_wait_count_reg(4),
      I2 => init_wait_count_reg(6),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_done_reg_n_0,
      O => init_wait_done_i_1_n_0
    );
init_wait_done_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      CLR => \out\(0),
      D => init_wait_done_i_1_n_0,
      Q => init_wait_done_reg_n_0
    );
\mmcm_lock_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      O => \p_0_in__1\(0)
    );
\mmcm_lock_count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      I1 => mmcm_lock_count_reg(1),
      O => \p_0_in__1\(1)
    );
\mmcm_lock_count[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => mmcm_lock_count_reg(1),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(2),
      O => \mmcm_lock_count[2]_i_1_n_0\
    );
\mmcm_lock_count[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => mmcm_lock_count_reg(2),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(1),
      I3 => mmcm_lock_count_reg(3),
      O => \mmcm_lock_count[3]_i_1_n_0\
    );
\mmcm_lock_count[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => mmcm_lock_count_reg(3),
      I1 => mmcm_lock_count_reg(1),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(2),
      I4 => mmcm_lock_count_reg(4),
      O => \mmcm_lock_count[4]_i_1_n_0\
    );
\mmcm_lock_count[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => \mmcm_lock_count[5]_i_1_n_0\
    );
\mmcm_lock_count[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => mmcm_lock_reclocked_i_2_n_0,
      I1 => mmcm_lock_count_reg(6),
      O => \mmcm_lock_count[6]_i_1_n_0\
    );
\mmcm_lock_count[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => mmcm_lock_reclocked_i_2_n_0,
      I1 => mmcm_lock_count_reg(6),
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_2_n_0\
    );
\mmcm_lock_count[7]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => mmcm_lock_count_reg(6),
      I1 => mmcm_lock_reclocked_i_2_n_0,
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_3_n_0\
    );
\mmcm_lock_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \p_0_in__1\(0),
      Q => mmcm_lock_count_reg(0),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \p_0_in__1\(1),
      Q => mmcm_lock_count_reg(1),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[2]_i_1_n_0\,
      Q => mmcm_lock_count_reg(2),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[3]_i_1_n_0\,
      Q => mmcm_lock_count_reg(3),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[4]_i_1_n_0\,
      Q => mmcm_lock_count_reg(4),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[5]_i_1_n_0\,
      Q => mmcm_lock_count_reg(5),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[6]_i_1_n_0\,
      Q => mmcm_lock_count_reg(6),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[7]_i_3_n_0\,
      Q => mmcm_lock_count_reg(7),
      R => sync_mmcm_lock_reclocked_n_0
    );
mmcm_lock_reclocked_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEA0000"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => mmcm_lock_count_reg(7),
      I2 => mmcm_lock_count_reg(6),
      I3 => mmcm_lock_reclocked_i_2_n_0,
      I4 => mmcm_lock_i,
      O => mmcm_lock_reclocked_i_1_n_0
    );
mmcm_lock_reclocked_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => mmcm_lock_reclocked_i_2_n_0
    );
mmcm_lock_reclocked_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => mmcm_lock_reclocked_i_1_n_0,
      Q => mmcm_lock_reclocked,
      R => '0'
    );
pll_reset_asserted_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000CD55CCCCCCCC"
    )
        port map (
      I0 => tx_state(3),
      I1 => pll_reset_asserted_reg_n_0,
      I2 => gt0_cpllrefclklost_i,
      I3 => refclk_stable_reg_n_0,
      I4 => tx_state(1),
      I5 => pll_reset_asserted_i_2_n_0,
      O => pll_reset_asserted_i_1_n_0
    );
pll_reset_asserted_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => tx_state(0),
      I1 => tx_state(3),
      I2 => tx_state(2),
      O => pll_reset_asserted_i_2_n_0
    );
pll_reset_asserted_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pll_reset_asserted_i_1_n_0,
      Q => pll_reset_asserted_reg_n_0,
      R => \out\(0)
    );
\refclk_stable_count[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFFFF"
    )
        port map (
      I0 => \refclk_stable_count[0]_i_3_n_0\,
      I1 => \refclk_stable_count[0]_i_4_n_0\,
      I2 => \refclk_stable_count[0]_i_5_n_0\,
      I3 => \refclk_stable_count[0]_i_6_n_0\,
      I4 => \refclk_stable_count[0]_i_7_n_0\,
      I5 => \refclk_stable_count[0]_i_8_n_0\,
      O => refclk_stable_count
    );
\refclk_stable_count[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFDFFFFFFFFFFFFF"
    )
        port map (
      I0 => refclk_stable_count_reg(13),
      I1 => refclk_stable_count_reg(12),
      I2 => refclk_stable_count_reg(10),
      I3 => refclk_stable_count_reg(11),
      I4 => refclk_stable_count_reg(9),
      I5 => refclk_stable_count_reg(8),
      O => \refclk_stable_count[0]_i_3_n_0\
    );
\refclk_stable_count[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFDF"
    )
        port map (
      I0 => refclk_stable_count_reg(19),
      I1 => refclk_stable_count_reg(18),
      I2 => refclk_stable_count_reg(16),
      I3 => refclk_stable_count_reg(17),
      I4 => refclk_stable_count_reg(15),
      I5 => refclk_stable_count_reg(14),
      O => \refclk_stable_count[0]_i_4_n_0\
    );
\refclk_stable_count[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => refclk_stable_count_reg(30),
      I1 => refclk_stable_count_reg(31),
      I2 => refclk_stable_count_reg(28),
      I3 => refclk_stable_count_reg(29),
      I4 => refclk_stable_count_reg(27),
      I5 => refclk_stable_count_reg(26),
      O => \refclk_stable_count[0]_i_5_n_0\
    );
\refclk_stable_count[0]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => refclk_stable_count_reg(24),
      I1 => refclk_stable_count_reg(25),
      I2 => refclk_stable_count_reg(22),
      I3 => refclk_stable_count_reg(23),
      I4 => refclk_stable_count_reg(21),
      I5 => refclk_stable_count_reg(20),
      O => \refclk_stable_count[0]_i_6_n_0\
    );
\refclk_stable_count[0]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => refclk_stable_count_reg(0),
      I1 => refclk_stable_count_reg(1),
      O => \refclk_stable_count[0]_i_7_n_0\
    );
\refclk_stable_count[0]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFF7"
    )
        port map (
      I0 => refclk_stable_count_reg(6),
      I1 => refclk_stable_count_reg(7),
      I2 => refclk_stable_count_reg(4),
      I3 => refclk_stable_count_reg(5),
      I4 => refclk_stable_count_reg(3),
      I5 => refclk_stable_count_reg(2),
      O => \refclk_stable_count[0]_i_8_n_0\
    );
\refclk_stable_count[0]_i_9\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => refclk_stable_count_reg(0),
      O => \refclk_stable_count[0]_i_9_n_0\
    );
\refclk_stable_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_7\,
      Q => refclk_stable_count_reg(0),
      R => '0'
    );
\refclk_stable_count_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \refclk_stable_count_reg[0]_i_2_n_0\,
      CO(2) => \refclk_stable_count_reg[0]_i_2_n_1\,
      CO(1) => \refclk_stable_count_reg[0]_i_2_n_2\,
      CO(0) => \refclk_stable_count_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \refclk_stable_count_reg[0]_i_2_n_4\,
      O(2) => \refclk_stable_count_reg[0]_i_2_n_5\,
      O(1) => \refclk_stable_count_reg[0]_i_2_n_6\,
      O(0) => \refclk_stable_count_reg[0]_i_2_n_7\,
      S(3 downto 1) => refclk_stable_count_reg(3 downto 1),
      S(0) => \refclk_stable_count[0]_i_9_n_0\
    );
\refclk_stable_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_5\,
      Q => refclk_stable_count_reg(10),
      R => '0'
    );
\refclk_stable_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_4\,
      Q => refclk_stable_count_reg(11),
      R => '0'
    );
\refclk_stable_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_7\,
      Q => refclk_stable_count_reg(12),
      R => '0'
    );
\refclk_stable_count_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[8]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[12]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[12]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[12]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[12]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[12]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[12]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[12]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(15 downto 12)
    );
\refclk_stable_count_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_6\,
      Q => refclk_stable_count_reg(13),
      R => '0'
    );
\refclk_stable_count_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_5\,
      Q => refclk_stable_count_reg(14),
      R => '0'
    );
\refclk_stable_count_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_4\,
      Q => refclk_stable_count_reg(15),
      R => '0'
    );
\refclk_stable_count_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_7\,
      Q => refclk_stable_count_reg(16),
      R => '0'
    );
\refclk_stable_count_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[12]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[16]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[16]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[16]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[16]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[16]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[16]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[16]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(19 downto 16)
    );
\refclk_stable_count_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_6\,
      Q => refclk_stable_count_reg(17),
      R => '0'
    );
\refclk_stable_count_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_5\,
      Q => refclk_stable_count_reg(18),
      R => '0'
    );
\refclk_stable_count_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_4\,
      Q => refclk_stable_count_reg(19),
      R => '0'
    );
\refclk_stable_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_6\,
      Q => refclk_stable_count_reg(1),
      R => '0'
    );
\refclk_stable_count_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_7\,
      Q => refclk_stable_count_reg(20),
      R => '0'
    );
\refclk_stable_count_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[16]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[20]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[20]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[20]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[20]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[20]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[20]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[20]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(23 downto 20)
    );
\refclk_stable_count_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_6\,
      Q => refclk_stable_count_reg(21),
      R => '0'
    );
\refclk_stable_count_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_5\,
      Q => refclk_stable_count_reg(22),
      R => '0'
    );
\refclk_stable_count_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_4\,
      Q => refclk_stable_count_reg(23),
      R => '0'
    );
\refclk_stable_count_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_7\,
      Q => refclk_stable_count_reg(24),
      R => '0'
    );
\refclk_stable_count_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[20]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[24]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[24]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[24]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[24]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[24]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[24]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[24]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(27 downto 24)
    );
\refclk_stable_count_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_6\,
      Q => refclk_stable_count_reg(25),
      R => '0'
    );
\refclk_stable_count_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_5\,
      Q => refclk_stable_count_reg(26),
      R => '0'
    );
\refclk_stable_count_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_4\,
      Q => refclk_stable_count_reg(27),
      R => '0'
    );
\refclk_stable_count_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_7\,
      Q => refclk_stable_count_reg(28),
      R => '0'
    );
\refclk_stable_count_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[24]_i_1_n_0\,
      CO(3) => \NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \refclk_stable_count_reg[28]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[28]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[28]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[28]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[28]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[28]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(31 downto 28)
    );
\refclk_stable_count_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_6\,
      Q => refclk_stable_count_reg(29),
      R => '0'
    );
\refclk_stable_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_5\,
      Q => refclk_stable_count_reg(2),
      R => '0'
    );
\refclk_stable_count_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_5\,
      Q => refclk_stable_count_reg(30),
      R => '0'
    );
\refclk_stable_count_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_4\,
      Q => refclk_stable_count_reg(31),
      R => '0'
    );
\refclk_stable_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_4\,
      Q => refclk_stable_count_reg(3),
      R => '0'
    );
\refclk_stable_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_7\,
      Q => refclk_stable_count_reg(4),
      R => '0'
    );
\refclk_stable_count_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[0]_i_2_n_0\,
      CO(3) => \refclk_stable_count_reg[4]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[4]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[4]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[4]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[4]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[4]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[4]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(7 downto 4)
    );
\refclk_stable_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_6\,
      Q => refclk_stable_count_reg(5),
      R => '0'
    );
\refclk_stable_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_5\,
      Q => refclk_stable_count_reg(6),
      R => '0'
    );
\refclk_stable_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_4\,
      Q => refclk_stable_count_reg(7),
      R => '0'
    );
\refclk_stable_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_7\,
      Q => refclk_stable_count_reg(8),
      R => '0'
    );
\refclk_stable_count_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[4]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[8]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[8]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[8]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[8]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[8]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[8]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[8]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(11 downto 8)
    );
\refclk_stable_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_6\,
      Q => refclk_stable_count_reg(9),
      R => '0'
    );
refclk_stable_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \refclk_stable_count[0]_i_7_n_0\,
      I1 => refclk_stable_i_2_n_0,
      I2 => refclk_stable_i_3_n_0,
      I3 => refclk_stable_i_4_n_0,
      I4 => refclk_stable_i_5_n_0,
      I5 => refclk_stable_i_6_n_0,
      O => refclk_stable_i_1_n_0
    );
refclk_stable_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001000000000000"
    )
        port map (
      I0 => refclk_stable_count_reg(4),
      I1 => refclk_stable_count_reg(5),
      I2 => refclk_stable_count_reg(2),
      I3 => refclk_stable_count_reg(3),
      I4 => refclk_stable_count_reg(7),
      I5 => refclk_stable_count_reg(6),
      O => refclk_stable_i_2_n_0
    );
refclk_stable_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000200000000000"
    )
        port map (
      I0 => refclk_stable_count_reg(10),
      I1 => refclk_stable_count_reg(11),
      I2 => refclk_stable_count_reg(8),
      I3 => refclk_stable_count_reg(9),
      I4 => refclk_stable_count_reg(12),
      I5 => refclk_stable_count_reg(13),
      O => refclk_stable_i_3_n_0
    );
refclk_stable_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => refclk_stable_count_reg(16),
      I1 => refclk_stable_count_reg(17),
      I2 => refclk_stable_count_reg(14),
      I3 => refclk_stable_count_reg(15),
      I4 => refclk_stable_count_reg(18),
      I5 => refclk_stable_count_reg(19),
      O => refclk_stable_i_4_n_0
    );
refclk_stable_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => refclk_stable_count_reg(22),
      I1 => refclk_stable_count_reg(23),
      I2 => refclk_stable_count_reg(20),
      I3 => refclk_stable_count_reg(21),
      I4 => refclk_stable_count_reg(25),
      I5 => refclk_stable_count_reg(24),
      O => refclk_stable_i_5_n_0
    );
refclk_stable_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => refclk_stable_count_reg(28),
      I1 => refclk_stable_count_reg(29),
      I2 => refclk_stable_count_reg(26),
      I3 => refclk_stable_count_reg(27),
      I4 => refclk_stable_count_reg(31),
      I5 => refclk_stable_count_reg(30),
      O => refclk_stable_i_6_n_0
    );
refclk_stable_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => refclk_stable_i_1_n_0,
      Q => refclk_stable_reg_n_0,
      R => '0'
    );
\reset_time_out_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"440000FF50505050"
    )
        port map (
      I0 => tx_state(3),
      I1 => txresetdone_s3,
      I2 => init_wait_done_reg_n_0,
      I3 => tx_state(1),
      I4 => tx_state(2),
      I5 => tx_state(0),
      O => \reset_time_out_i_2__0_n_0\
    );
reset_time_out_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => sync_cplllock_n_0,
      Q => reset_time_out,
      R => \out\(0)
    );
run_phase_alignment_int_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB0002"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      I2 => tx_state(2),
      I3 => tx_state(1),
      I4 => run_phase_alignment_int_reg_n_0,
      O => run_phase_alignment_int_i_1_n_0
    );
run_phase_alignment_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => run_phase_alignment_int_i_1_n_0,
      Q => run_phase_alignment_int_reg_n_0,
      R => \out\(0)
    );
run_phase_alignment_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => '1',
      D => data_out,
      Q => run_phase_alignment_int_s3,
      R => '0'
    );
sync_TXRESETDONE: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_4
     port map (
      data_out => txresetdone_s2,
      data_sync_reg1_0 => data_sync_reg1_0,
      independent_clock_bufg => independent_clock_bufg
    );
sync_cplllock: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_5
     port map (
      E(0) => sync_cplllock_n_1,
      \FSM_sequential_tx_state_reg[0]\ => \FSM_sequential_tx_state[3]_i_3_n_0\,
      \FSM_sequential_tx_state_reg[0]_0\ => \FSM_sequential_tx_state[3]_i_4_n_0\,
      \FSM_sequential_tx_state_reg[0]_1\ => \FSM_sequential_tx_state[3]_i_6_n_0\,
      \FSM_sequential_tx_state_reg[0]_2\ => time_out_2ms_reg_n_0,
      \FSM_sequential_tx_state_reg[0]_3\ => \FSM_sequential_tx_state[3]_i_7_n_0\,
      \FSM_sequential_tx_state_reg[0]_4\ => pll_reset_asserted_reg_n_0,
      \FSM_sequential_tx_state_reg[0]_5\ => refclk_stable_reg_n_0,
      \FSM_sequential_tx_state_reg[0]_6\ => \FSM_sequential_tx_state[0]_i_3_n_0\,
      Q(3 downto 0) => tx_state(3 downto 0),
      data_sync_reg1_0 => data_sync_reg1_2,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_lock_reclocked => mmcm_lock_reclocked,
      reset_time_out => reset_time_out,
      reset_time_out_reg => sync_cplllock_n_0,
      reset_time_out_reg_0 => \reset_time_out_i_2__0_n_0\,
      reset_time_out_reg_1 => init_wait_done_reg_n_0
    );
sync_mmcm_lock_reclocked: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_6
     port map (
      SR(0) => sync_mmcm_lock_reclocked_n_0,
      data_out => mmcm_lock_i,
      data_sync_reg1_0 => data_sync_reg1_1,
      independent_clock_bufg => independent_clock_bufg
    );
sync_run_phase_alignment_int: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_7
     port map (
      data_in => run_phase_alignment_int_reg_n_0,
      data_out => data_out,
      data_sync_reg6_0 => data_sync_reg1
    );
sync_time_out_wait_bypass: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_8
     port map (
      data_in => time_out_wait_bypass_reg_n_0,
      data_out => time_out_wait_bypass_s2,
      independent_clock_bufg => independent_clock_bufg
    );
sync_tx_fsm_reset_done_int: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_9
     port map (
      data_in => \^data_in\,
      data_out => tx_fsm_reset_done_int_s2,
      data_sync_reg1_0 => data_sync_reg1
    );
time_out_2ms_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AE"
    )
        port map (
      I0 => time_out_2ms_reg_n_0,
      I1 => \time_out_2ms_i_2__0_n_0\,
      I2 => time_out_2ms_i_3_n_0,
      I3 => reset_time_out,
      O => time_out_2ms_i_1_n_0
    );
\time_out_2ms_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000800"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(18),
      I2 => time_out_counter_reg(10),
      I3 => time_out_counter_reg(12),
      I4 => time_out_counter_reg(5),
      I5 => time_tlock_max_i_3_n_0,
      O => \time_out_2ms_i_2__0_n_0\
    );
time_out_2ms_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => time_out_counter_reg(7),
      I1 => time_out_counter_reg(14),
      I2 => \time_out_2ms_i_4__0_n_0\,
      I3 => time_out_2ms_i_5_n_0,
      O => time_out_2ms_i_3_n_0
    );
\time_out_2ms_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => time_out_counter_reg(4),
      I1 => time_out_counter_reg(3),
      I2 => time_out_counter_reg(8),
      I3 => time_out_counter_reg(6),
      O => \time_out_2ms_i_4__0_n_0\
    );
time_out_2ms_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEF"
    )
        port map (
      I0 => time_out_counter_reg(0),
      I1 => time_out_counter_reg(13),
      I2 => time_out_counter_reg(9),
      I3 => time_out_counter_reg(2),
      I4 => time_out_counter_reg(1),
      O => time_out_2ms_i_5_n_0
    );
time_out_2ms_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_2ms_i_1_n_0,
      Q => time_out_2ms_reg_n_0,
      R => '0'
    );
time_out_500us_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AAAAEAAA"
    )
        port map (
      I0 => time_out_500us_reg_n_0,
      I1 => time_out_500us_i_2_n_0,
      I2 => time_out_counter_reg(5),
      I3 => time_out_counter_reg(10),
      I4 => time_out_2ms_i_3_n_0,
      I5 => reset_time_out,
      O => time_out_500us_i_1_n_0
    );
time_out_500us_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000008"
    )
        port map (
      I0 => time_out_counter_reg(15),
      I1 => time_out_counter_reg(16),
      I2 => time_out_counter_reg(11),
      I3 => time_out_counter_reg(12),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(17),
      O => time_out_500us_i_2_n_0
    );
time_out_500us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_500us_i_1_n_0,
      Q => time_out_500us_reg_n_0,
      R => '0'
    );
\time_out_counter[0]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFBFFFF"
    )
        port map (
      I0 => time_tlock_max_i_3_n_0,
      I1 => \time_out_counter[0]_i_3__0_n_0\,
      I2 => time_out_2ms_i_3_n_0,
      I3 => time_out_counter_reg(10),
      I4 => time_out_counter_reg(12),
      I5 => time_out_counter_reg(5),
      O => time_out_counter
    );
\time_out_counter[0]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(18),
      O => \time_out_counter[0]_i_3__0_n_0\
    );
\time_out_counter[0]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(0),
      O => \time_out_counter[0]_i_4_n_0\
    );
\time_out_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_7\,
      Q => time_out_counter_reg(0),
      R => reset_time_out
    );
\time_out_counter_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \time_out_counter_reg[0]_i_2_n_0\,
      CO(2) => \time_out_counter_reg[0]_i_2_n_1\,
      CO(1) => \time_out_counter_reg[0]_i_2_n_2\,
      CO(0) => \time_out_counter_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \time_out_counter_reg[0]_i_2_n_4\,
      O(2) => \time_out_counter_reg[0]_i_2_n_5\,
      O(1) => \time_out_counter_reg[0]_i_2_n_6\,
      O(0) => \time_out_counter_reg[0]_i_2_n_7\,
      S(3 downto 1) => time_out_counter_reg(3 downto 1),
      S(0) => \time_out_counter[0]_i_4_n_0\
    );
\time_out_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_5\,
      Q => time_out_counter_reg(10),
      R => reset_time_out
    );
\time_out_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_4\,
      Q => time_out_counter_reg(11),
      R => reset_time_out
    );
\time_out_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_7\,
      Q => time_out_counter_reg(12),
      R => reset_time_out
    );
\time_out_counter_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[8]_i_1_n_0\,
      CO(3) => \time_out_counter_reg[12]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[12]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[12]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[12]_i_1_n_4\,
      O(2) => \time_out_counter_reg[12]_i_1_n_5\,
      O(1) => \time_out_counter_reg[12]_i_1_n_6\,
      O(0) => \time_out_counter_reg[12]_i_1_n_7\,
      S(3 downto 0) => time_out_counter_reg(15 downto 12)
    );
\time_out_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_6\,
      Q => time_out_counter_reg(13),
      R => reset_time_out
    );
\time_out_counter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_5\,
      Q => time_out_counter_reg(14),
      R => reset_time_out
    );
\time_out_counter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_4\,
      Q => time_out_counter_reg(15),
      R => reset_time_out
    );
\time_out_counter_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1_n_7\,
      Q => time_out_counter_reg(16),
      R => reset_time_out
    );
\time_out_counter_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[12]_i_1_n_0\,
      CO(3 downto 2) => \NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \time_out_counter_reg[16]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED\(3),
      O(2) => \time_out_counter_reg[16]_i_1_n_5\,
      O(1) => \time_out_counter_reg[16]_i_1_n_6\,
      O(0) => \time_out_counter_reg[16]_i_1_n_7\,
      S(3) => '0',
      S(2 downto 0) => time_out_counter_reg(18 downto 16)
    );
\time_out_counter_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1_n_6\,
      Q => time_out_counter_reg(17),
      R => reset_time_out
    );
\time_out_counter_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1_n_5\,
      Q => time_out_counter_reg(18),
      R => reset_time_out
    );
\time_out_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_6\,
      Q => time_out_counter_reg(1),
      R => reset_time_out
    );
\time_out_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_5\,
      Q => time_out_counter_reg(2),
      R => reset_time_out
    );
\time_out_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_4\,
      Q => time_out_counter_reg(3),
      R => reset_time_out
    );
\time_out_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_7\,
      Q => time_out_counter_reg(4),
      R => reset_time_out
    );
\time_out_counter_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[0]_i_2_n_0\,
      CO(3) => \time_out_counter_reg[4]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[4]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[4]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[4]_i_1_n_4\,
      O(2) => \time_out_counter_reg[4]_i_1_n_5\,
      O(1) => \time_out_counter_reg[4]_i_1_n_6\,
      O(0) => \time_out_counter_reg[4]_i_1_n_7\,
      S(3 downto 0) => time_out_counter_reg(7 downto 4)
    );
\time_out_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_6\,
      Q => time_out_counter_reg(5),
      R => reset_time_out
    );
\time_out_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_5\,
      Q => time_out_counter_reg(6),
      R => reset_time_out
    );
\time_out_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_4\,
      Q => time_out_counter_reg(7),
      R => reset_time_out
    );
\time_out_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_7\,
      Q => time_out_counter_reg(8),
      R => reset_time_out
    );
\time_out_counter_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[4]_i_1_n_0\,
      CO(3) => \time_out_counter_reg[8]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[8]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[8]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[8]_i_1_n_4\,
      O(2) => \time_out_counter_reg[8]_i_1_n_5\,
      O(1) => \time_out_counter_reg[8]_i_1_n_6\,
      O(0) => \time_out_counter_reg[8]_i_1_n_7\,
      S(3 downto 0) => time_out_counter_reg(11 downto 8)
    );
\time_out_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_6\,
      Q => time_out_counter_reg(9),
      R => reset_time_out
    );
time_out_wait_bypass_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AB00"
    )
        port map (
      I0 => time_out_wait_bypass_reg_n_0,
      I1 => tx_fsm_reset_done_int_s3,
      I2 => time_out_wait_bypass_i_2_n_0,
      I3 => run_phase_alignment_int_s3,
      O => time_out_wait_bypass_i_1_n_0
    );
time_out_wait_bypass_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEFFFFFFFFF"
    )
        port map (
      I0 => time_out_wait_bypass_i_3_n_0,
      I1 => time_out_wait_bypass_i_4_n_0,
      I2 => wait_bypass_count_reg(5),
      I3 => wait_bypass_count_reg(13),
      I4 => wait_bypass_count_reg(11),
      I5 => time_out_wait_bypass_i_5_n_0,
      O => time_out_wait_bypass_i_2_n_0
    );
time_out_wait_bypass_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => wait_bypass_count_reg(16),
      I1 => wait_bypass_count_reg(9),
      I2 => wait_bypass_count_reg(12),
      I3 => wait_bypass_count_reg(10),
      O => time_out_wait_bypass_i_3_n_0
    );
time_out_wait_bypass_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => wait_bypass_count_reg(4),
      I1 => wait_bypass_count_reg(15),
      I2 => wait_bypass_count_reg(6),
      I3 => wait_bypass_count_reg(0),
      O => time_out_wait_bypass_i_4_n_0
    );
time_out_wait_bypass_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4000000000000000"
    )
        port map (
      I0 => wait_bypass_count_reg(8),
      I1 => wait_bypass_count_reg(1),
      I2 => wait_bypass_count_reg(7),
      I3 => wait_bypass_count_reg(14),
      I4 => wait_bypass_count_reg(2),
      I5 => wait_bypass_count_reg(3),
      O => time_out_wait_bypass_i_5_n_0
    );
time_out_wait_bypass_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => '1',
      D => time_out_wait_bypass_i_1_n_0,
      Q => time_out_wait_bypass_reg_n_0,
      R => '0'
    );
time_out_wait_bypass_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_wait_bypass_s2,
      Q => time_out_wait_bypass_s3,
      R => '0'
    );
time_tlock_max_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AAAAAAEA"
    )
        port map (
      I0 => time_tlock_max_reg_n_0,
      I1 => time_tlock_max_i_2_n_0,
      I2 => time_out_counter_reg(5),
      I3 => time_tlock_max_i_3_n_0,
      I4 => time_tlock_max_i_4_n_0,
      I5 => reset_time_out,
      O => time_tlock_max_i_1_n_0
    );
time_tlock_max_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => time_out_counter_reg(14),
      I1 => time_out_counter_reg(12),
      I2 => time_out_counter_reg(10),
      I3 => time_out_counter_reg(7),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(17),
      O => time_tlock_max_i_2_n_0
    );
time_tlock_max_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(15),
      I2 => time_out_counter_reg(11),
      O => time_tlock_max_i_3_n_0
    );
time_tlock_max_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => time_out_2ms_i_5_n_0,
      I1 => time_out_counter_reg(6),
      I2 => time_out_counter_reg(8),
      I3 => time_out_counter_reg(3),
      I4 => time_out_counter_reg(4),
      O => time_tlock_max_i_4_n_0
    );
time_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_tlock_max_i_1_n_0,
      Q => time_tlock_max_reg_n_0,
      R => '0'
    );
tx_fsm_reset_done_int_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF1000"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(0),
      I3 => tx_state(3),
      I4 => \^data_in\,
      O => tx_fsm_reset_done_int_i_1_n_0
    );
tx_fsm_reset_done_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => tx_fsm_reset_done_int_i_1_n_0,
      Q => \^data_in\,
      R => \out\(0)
    );
tx_fsm_reset_done_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => '1',
      D => tx_fsm_reset_done_int_s2,
      Q => tx_fsm_reset_done_int_s3,
      R => '0'
    );
txresetdone_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => txresetdone_s2,
      Q => txresetdone_s3,
      R => '0'
    );
\wait_bypass_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => run_phase_alignment_int_s3,
      O => clear
    );
\wait_bypass_count[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_wait_bypass_i_2_n_0,
      I1 => tx_fsm_reset_done_int_s3,
      O => \wait_bypass_count[0]_i_2_n_0\
    );
\wait_bypass_count[0]_i_4__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_bypass_count_reg(0),
      O => \wait_bypass_count[0]_i_4__0_n_0\
    );
\wait_bypass_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_7\,
      Q => wait_bypass_count_reg(0),
      R => clear
    );
\wait_bypass_count_reg[0]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \wait_bypass_count_reg[0]_i_3_n_0\,
      CO(2) => \wait_bypass_count_reg[0]_i_3_n_1\,
      CO(1) => \wait_bypass_count_reg[0]_i_3_n_2\,
      CO(0) => \wait_bypass_count_reg[0]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \wait_bypass_count_reg[0]_i_3_n_4\,
      O(2) => \wait_bypass_count_reg[0]_i_3_n_5\,
      O(1) => \wait_bypass_count_reg[0]_i_3_n_6\,
      O(0) => \wait_bypass_count_reg[0]_i_3_n_7\,
      S(3 downto 1) => wait_bypass_count_reg(3 downto 1),
      S(0) => \wait_bypass_count[0]_i_4__0_n_0\
    );
\wait_bypass_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_5\,
      Q => wait_bypass_count_reg(10),
      R => clear
    );
\wait_bypass_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_4\,
      Q => wait_bypass_count_reg(11),
      R => clear
    );
\wait_bypass_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_7\,
      Q => wait_bypass_count_reg(12),
      R => clear
    );
\wait_bypass_count_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[8]_i_1_n_0\,
      CO(3) => \wait_bypass_count_reg[12]_i_1_n_0\,
      CO(2) => \wait_bypass_count_reg[12]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[12]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[12]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[12]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[12]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[12]_i_1_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(15 downto 12)
    );
\wait_bypass_count_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_6\,
      Q => wait_bypass_count_reg(13),
      R => clear
    );
\wait_bypass_count_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_5\,
      Q => wait_bypass_count_reg(14),
      R => clear
    );
\wait_bypass_count_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_4\,
      Q => wait_bypass_count_reg(15),
      R => clear
    );
\wait_bypass_count_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[16]_i_1_n_7\,
      Q => wait_bypass_count_reg(16),
      R => clear
    );
\wait_bypass_count_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[12]_i_1_n_0\,
      CO(3 downto 0) => \NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED\(3 downto 1),
      O(0) => \wait_bypass_count_reg[16]_i_1_n_7\,
      S(3 downto 1) => B"000",
      S(0) => wait_bypass_count_reg(16)
    );
\wait_bypass_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_6\,
      Q => wait_bypass_count_reg(1),
      R => clear
    );
\wait_bypass_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_5\,
      Q => wait_bypass_count_reg(2),
      R => clear
    );
\wait_bypass_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_4\,
      Q => wait_bypass_count_reg(3),
      R => clear
    );
\wait_bypass_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_7\,
      Q => wait_bypass_count_reg(4),
      R => clear
    );
\wait_bypass_count_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[0]_i_3_n_0\,
      CO(3) => \wait_bypass_count_reg[4]_i_1_n_0\,
      CO(2) => \wait_bypass_count_reg[4]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[4]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[4]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[4]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[4]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[4]_i_1_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(7 downto 4)
    );
\wait_bypass_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_6\,
      Q => wait_bypass_count_reg(5),
      R => clear
    );
\wait_bypass_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_5\,
      Q => wait_bypass_count_reg(6),
      R => clear
    );
\wait_bypass_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_4\,
      Q => wait_bypass_count_reg(7),
      R => clear
    );
\wait_bypass_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_7\,
      Q => wait_bypass_count_reg(8),
      R => clear
    );
\wait_bypass_count_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[4]_i_1_n_0\,
      CO(3) => \wait_bypass_count_reg[8]_i_1_n_0\,
      CO(2) => \wait_bypass_count_reg[8]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[8]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[8]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[8]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[8]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[8]_i_1_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(11 downto 8)
    );
\wait_bypass_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_6\,
      Q => wait_bypass_count_reg(9),
      R => clear
    );
\wait_time_cnt[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      O => wait_time_cnt0(0)
    );
\wait_time_cnt[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      I1 => wait_time_cnt_reg(1),
      O => \wait_time_cnt[1]_i_1_n_0\
    );
\wait_time_cnt[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => wait_time_cnt_reg(1),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(2),
      O => \wait_time_cnt[2]_i_1_n_0\
    );
\wait_time_cnt[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE01"
    )
        port map (
      I0 => wait_time_cnt_reg(2),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(1),
      I3 => wait_time_cnt_reg(3),
      O => \wait_time_cnt[3]_i_1_n_0\
    );
\wait_time_cnt[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0001"
    )
        port map (
      I0 => wait_time_cnt_reg(3),
      I1 => wait_time_cnt_reg(1),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(2),
      I4 => wait_time_cnt_reg(4),
      O => \wait_time_cnt[4]_i_1_n_0\
    );
\wait_time_cnt[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000001"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[5]_i_1_n_0\
    );
\wait_time_cnt[6]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0700"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(3),
      I3 => tx_state(0),
      O => wait_time_cnt0_0
    );
\wait_time_cnt[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => sel
    );
\wait_time_cnt[6]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => \wait_time_cnt[6]_i_3_n_0\
    );
\wait_time_cnt[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[6]_i_4_n_0\
    );
\wait_time_cnt_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => wait_time_cnt0(0),
      Q => wait_time_cnt_reg(0),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[1]_i_1_n_0\,
      Q => wait_time_cnt_reg(1),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[2]_i_1_n_0\,
      Q => wait_time_cnt_reg(2),
      S => wait_time_cnt0_0
    );
\wait_time_cnt_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[3]_i_1_n_0\,
      Q => wait_time_cnt_reg(3),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[4]_i_1_n_0\,
      Q => wait_time_cnt_reg(4),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[5]_i_1_n_0\,
      Q => wait_time_cnt_reg(5),
      S => wait_time_cnt0_0
    );
\wait_time_cnt_reg[6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[6]_i_3_n_0\,
      Q => wait_time_cnt_reg(6),
      S => wait_time_cnt0_0
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2024.1.1"
`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
GNlNeNP2E6l/J7IS2LF/ItWIQpPIQxmqiENbpRcUcYDs2zMQssAZwDKWmGxc99etc2e+6UESUZFU
1OoN93QWwGY5lwtShrTc9cE66KsLO3U82lRN16EHmi+RZOdYR9rf5z7OgjqfdL/T03/rfwU37+Qw
8V3wDE2i5hqr8BCHEwM=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
afRfkAd8cICiTJYylAj89MfRuyfOa9d/tbr4mvbd5mWPw07cLRhiyi8gIe4+Ayw0CiOk7/EjPgWg
Tg/1hR99BWRw4VbbDTWu69lZfP8CCWoBxtdt8WRuUGz7PLk6qQMaoDf1ogw0xyXTK96u8N6bef3z
7mR3YInM0ECnk0+8vQDLRo6ETSTf4oQYcckMpPfbPQ+NnHRvcHroYdddFGNYasbeUqwn7tkHnYO+
ADUKING4g3DhZO3UAIPZJggQnH3ayID9nf49Oa7bj//jtWD0qELIGFMQn48spke+RcfDkdFLHWhY
0esV8oRsRwIWfwXwqK6i7aIis3paozzKSWbiNg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
ZI9ECw+Xpdoykt5zjVXLh0QGbCnqQDaDo8xSAWsYltuhqetS6+j38MNsVABP/vySCUkxp7yLlCMg
506S40Li7Nmy7qD4YvDLHMe+fIgwi3Hb193aoDluxnxdVk/aJ7VOJWXd88+scqfUSpbbFv7rk9La
8woMpCSiDNVKi6NB7Qs=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
R1h0p6UTerhva5Yb3duDj5nR3CyyGXjeE4dSD+H9PUwvK6SElm7FlL9AjlrnVaE2OmUXbsHu+WYi
T1XMnV1ekw5xTRICAudvAhAJrZYMHEjujHOfgXPmKtXHivF59pFlTxnEtD+dnchOL4x/jMm/lCgH
k3/wzvZbfOaxaKFceauCooIRZkqZa5FA3KG53cEucbspEldbnPaAcHsiO04PEwklvjo+2XAiGTHB
YB9XEkRUOprwSG+TeOWFNvz4Yu2FYHffCsobvgsF/8dSJQ1SVv3rM1uHf3coB/s7NIgZ0ye03NO/
wLFRL+3OuPxWNTqHO9YYXL0XTd1rPaaWB/mCcw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
4l+Avk44zzaOVxRJDzFVTXMyJmyE6qVo8wAiHogr6M/NkYt3AgnEITBHDJ9dF4SVZQLiV7ZY6shr
asWzrbISsiBCAaZgyPZLr1zlBErx08H5Enf1NkLuocQa0x2Dc1DwG+mO+flli1++tKpmHM6ADCS0
3/8fREk+ga01VkXbOXY6B7Wt4G2O3LBdFCkEvd0/MEx5uYrJNrtpaeSd3h2CpYLsfPsxxbAzNJN1
3pvNKoS8QwUnTPy7oe/k8bFD0TOzZmLvT/dy3x7SFlB6oSYr7FtWMOOuH5B08Upu75qdujq2/6ox
I9BucH2FnuLsghHnPuWfjh4WTDPekeCy51G/7w==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2023_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
r1TCY1FBxgJgPdlaGrkCY3ZPZ26hJnsdsOKnUu75PgvjzuIkty6RkwizEzV4coW2cxRBcWf87Rmb
xbDdy2cDu4f2qP6QUVBsGtY+ITHBIqBPqY7IqmHvnmT8DQ7mta5QD9HpSpd8E0HAu+1hmp2y9R2T
j6cEj7KNzaIGnuiJLket69e1dx31L14fHppyx7EJTOKaIYVHOTGcZeHi6ozbitA95N9bUNiSCS93
sJcjWWTWEB9Cx8arBeHBMIflTsCrUzgwd6f7Lb59R/pt8Zgvgr/BoV20ZGDDy6BCwEWgGSw6GDBL
o5ppIN+dICJOxCzX0Ij4ziKkTNPa1OnlJpcXcA==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
U0o+hd53NUTkWW/pm++iDbvu9EzyJbrE8cgDdW35QL1go6ppv1cnjM7W9LNAUoYpay5lYpZFkUZP
qgnoVBG2BG64sAHeAjjMVH8aUuTYzhhM3n6pvSqx5Ez8rezjhdOlMLX17yWFSCq3x15rfV2si91N
p+FFtjXOD3BmuNjRLXeeRjrvt6sxfN9IwN0yEZYhwp2ukPLeIyrYpRXZ+xMErI/b7BKLZNIljMSV
Cx5wPeURNjIC7AKZOJmo8YQe2JGJEHps80MGm98CsBtbIXDa7uAxib3WWstVs/UbQGgqjiyNIOSu
11VEYQSI8nO9bNfJPsLpVaeo9V+FUKdFcBgKbA==

`protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`protect key_block
JlAVtvWu4RylemkeXAwQaOcauzsF85mjSoJqBa5/i7p1cOK0PM2Rfou44syNOWyUG43Mq5DdY2P2
hSdJ0BrVnIF/JY67xayAWorKfMTlvfZN3zVnvvFrLORWpe5MwPzwHomEMtQgK/WjTmMnFJzWxYti
gedoe+hxxEoZILnHN3ge9A2vnRikwcJETpOKLK13mQa1Td8zo9tsQuagX91Bw+AWLzMrHGR2bmQr
grI95HpSioNMY5CjomezDLSd80Rlkr+6SiaUQt3pqExFVEAsKNJdtppxNwugqojHWEqm1lF0wKuj
aLs/KfYF2Uv5wNjccSP2gHLWWaF5DKs5G5u4c4FjEiS/slCZgrt5Ymwc6XKQHNOHbPRsQcbbkPMD
V/YuoNzw1R2386n0pOd+vMD9nTtpB104VrwBNBxlGPPa0qiFM4+xr58vMzHKNT91irPu/EirEIjt
wEtLynnUeSKMKz3aAvXZPRYeEurD0fgZ6iczWVNZOsltSIQZjOp5ErOC

`protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GeMop+4rJGQJTsAts4IyQAkQMeKt+Fa026naAEt7J5/8nhAvWlIS4YQDNOLHMMUe8p+sUA4yE6+s
hf5STrRcmXzM45O/bYrkBzWHNqGcsEuAHfdUd9ANbBZmpncQ5ywZbXLX0oI4xVx55B8sn2gAj+p2
NQEPjYWPZXJNML4IilPsrW6TlgTi1zhp0JFBFT3O6yHY35dAseHXtMWYgjr0+6WBtVCCvC/z4bA0
UY58Skgytan1bUM557AlikG2LqNB6YHVUQihnx4ZProlAL61WY/j4joeXcDH8Tpm0cmYcUM5yUax
haoFS6FezWAqD4KpiUS3xpkUVET2QpN0NnxTvw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
1x9gXtJFU2nGQ9CWRMPqjLG5B7nyL7ZKhgWVKPrQTraMot+PY1MLqNEQ8371fSgOtWrUw7wBswaL
CBsGdy0dDZt13psueOntqYT0wcW3vUbYzfkMCsSjkk2yeYjnCYclwJKMNk3fGxfxpTLQsScOFING
ZyUHBaE+KeZ7yfdqvxwcpP+rUXdNX/2BGvcr+a0ow/nFsiCiOMdYIlAgllnBFoUV7lDr001O00Hk
1MDqVCRdpiO5q5FnggoIp1sVIq1C+JbqiuyPkv5jmlk3iIfZiHhSSYoxP3DJf7VPvlJ1Z/Hwz6Dw
/39nFCTotl6l9hqiLHTmISLBvSYZnjDEcOPtPA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
mxE8AqszO8jEcufcEIxsyswviid2I+vp0wiBEslkbc/wVr2vlpOaIaviDrRuTwIVx162O0AViWa2
3/lH2PLY1lcjenGETw7njsuJd6MQv4KihzbHhd1HB0TKpoL1n4E9u5PgNIGghcgNu7s2hE5rpffM
FAxMXxyhZQ3j4WBKTgRtbqcexvVSl/whSo87Oa/V48wwF7Z68uoZdaD4cdNihYwHMLOm0LYqYesQ
cScpoz43IdSFiQkBEM4YXuqn7BFIsJ7UAr+EkHAFxWYqt0IJ3+/OCuOYW+y/3p2tXT2YSGnEsOzZ
SLIsZpsw7wAiEfu9tVZJlTQ0OUTZqfQXK68i9A==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 59392)
`protect data_block
pPE/AeOZ9UPPOJP/gev29vz4vAy12ukSJ1lWuW+/trX3UqKXCb6mfQF7fZNZ/2mu8hjfCM/az8Xc
7eTzAdzRhNGu8HFD8sbbFbGrTb3cLp8zNp3ivYIquKrOP+Jo0pHAOy+QVpkmXOQbzZSg+J9D6TNd
hTf+4T8SWQ25AlzMsMlMqlkwxoKDVr/h2YtLFm1kY6H7HYYONJFZIdW60yKDS5+UYoxt5kmmpcrH
CSAo8XKdLudCb8w58U+X5HakmROZGbePqS5UNw8OhSlK46i873sCvFoA2AqJdxjiZ38HhIAutVBx
VfpQrSAGwJH2woVP0/JYvcgk9PqC0Hte9I4OK/VfbWm6FL0byqYFI3Z8ni4v9eXhgdDQlzmhVgwL
YyhkoGDsElcTC8GB42zhMBEswtyWx9IH9Giy+Yg76/3oWnrvyqC939jESiuv80vwdAz5yUWroSm6
1UFyBLXuNFMRHrYlJ0YQx2oK9rEu2K2sb/RGiKg6id9H5XtPywNBOgmmTdNIQXylnc2igbrZUK09
fUWpQmasXtprvDq7gS+xnIEDMhC9TFgGUyddpzpqKEkectg0q9yRCklHo2or7IWwJkytz0A/i+/i
yl5SWcjsIUA1Fn7Hv0yRtMMsYOeY7aYn8zXSKLehq0dYhQ0paaQQG1Ui5BvPLnUO/soC0ieUy3Yp
mEr3ngWD6qMQbzE6Q0G635TGqup9g2xW3DidLDJ6nDk0BrJZS1UCQFkrNhqGLVjGyD5RXo1Ivjpz
hg0iAXredYKMNZshB99l3jaFUZnxIg5GAMscFBDyzj5aOqp9j2SXv3TzdhRen5J3Aba/VDLaz3uK
8WmJFRqh53o4qg5xDcZQjfqdreuvmTKsrdUVNYSfaiee2mpb3l6SjEzW65bo1UBCJzZRCeEJAIO2
Hz67vWk7lgJoPydBlUso+sD1f9PH2rJw3+4Ged3IgA3r6uxlvk2ivwXPaeBqDlQOYUTpge6Z1N2N
hF7Lk3rkKkTQETZCiyaTAIPidnhoxThjVDuVMa/q0phvQbswgtSDAKCQv736Zan1qIJBg9lK38G+
k33bjZ71eaD7AEgLK5XsLOBV9m+mx+8tDJc/rUztg3tSfINOzq/HR5Lxasd9MWlATvJU3F7CIHFX
UIGz7Q9QZ8nOu899bqKKgLiKpjel0bGbGtiWQ+ncLGAb2KaP0TKs21XRWAJHyfHhnve7CSkyVmcT
63crSTeDNTB4TLg/dIPGNFE1Atv1hECei0JavDct07WY5q5v1ikc4qCtw66jFCVGbkfYoD14kQe9
8S4io4ySR1Nj2yD3tQtvD4irwTsiZnZXVlCzBH1Y+Rf9scpHaHQ7nO+5gOzR7DgCc8S/vgIVcqoS
Y5CqV5T/7xZFzhyg7v7z8gq7Zi2k58BVl4QzOWLvEQsv949o+ehgywKXbzrFIGyjJYDCEpVpFKTL
ACDt6fslQop5j7VNJceNwwImkfDbS7hQX9Voryc11TJB5Dah8qSIZnvI2QxxdQoue216keCXjdOB
1W5Q+9HXjE1EzWfwG3tv4P0ZlIZRecWq11+ZH0jidXbjplN4VugeVPzL4pUsU3MHR6CP/adpOfvy
enPNkUD7rJABDXW+iMNk/+Mj2faqSbJObJpiaaKY8dkJPX7znfOM65oGxgQqwuvxh3Xm4Jz8e/wb
q6ooqcBI2AQ1k8WNogTrFwCe3ARWZo7oKgupBxKTB4vrIo+zTh0dfrrcBNRqaJajnMK5khketdUM
zYXP7UYdioM/VkGibc/cYjcMjesDXqjYcoaSfNqh4GJh2nL3ZfwxnEVF+WIBpZJazXMxpsgrjWiU
monNv/Iu9FJv+v5vYqeMG2WVsAWOZmpb9GIRVfWn79rVq3zsd6V4lmC0Bsne7+8ZrqSX0a9zm6Os
lzHwDUySEFejleu0r1LCOWJ+pUJwckkCZxqz2mJTD1QcyH4aTuTMp+PqUy8cn3pV6Quw864hBXHG
d4rOS7Axsca7nrTDb/Z81p/kDRD6gfMI52OegjIazbwOzg20oddURY9lPGVsVqDfUaa6ojBwcQoI
P/IlaXCdtVioBSMbMSeHE4YmkFzCGTrXCNgLzrBUtgotaI8rX4UAY0maWp4RQQExY/aItjWpCT4a
PErHmyTJM5Fr8KvK+u2v7t+QVtAVze7C5t6q3dJHP7uerrk2QOi3zlu8Q3pV8F0UTLRmIjXxoS4n
N9PPFhCOwe/bg8eE+Ptphlwv9D4tUe/7gtn86rm78773nTBPXhOcop/D03VYQlZs1bybzhKND5+q
NkEt59odwlRXAl3egFcY83bJPJq6P/zH2X1SUJ2bgXeCDM8huklW3AS3+INmagpLr4hsIveiIpqx
pTLNm+/0dtbirUOyNG8MkLmf8TYSF5mRcFqlZgC4IG+dNpXtMiG70QyePjd+mIA2z1VuCqj9oP2w
GYz0Xm7MmnueE9bBYy3uwRqH8z7aeObkBdcBNz9Ur60Y+FBd6Nsv3+82QGLKpvmdIg+TfKcIZaiR
FP9aaEPrnBIH0rBMlBoyNJKwU+rp8I5ygS3MUwyhfQWFIuUXK+xbVmLpysrc8nQzKMm7O42hyI3r
dyQkPs4vzMkrDllFn12mdfjyt56WMUKoJ9u8pdu9D5uDv1R22X1qKv7C6PegLSVMk1dfhxyIGmKR
+12DkPhpOuGHNXlhXJJ84bXvqOlQyAZtCFj3Kvl4311ki6mfyFwF1Ubhhre+0WmW9F1x7xoHZl3P
/tw93U/QVpJM7U65Cwo8RLiiofnWwFnj75e5pWwrbbyOqS4YCASuicvm/ikYm0WODdptIv3MIMhZ
aiqNXyeXxA0NRJrPBbeot4NohdtXsWdAhCFxIv6hFAuxDmA7NF8q0jcI7qByL+lObTWzeWAtZl+R
YKhMxXrkrV7e5ql/JJ68th6Nr9eMAID0nNM2mzS9jwLpD+2oq1Pp5BZLouo9JCYKuk1uDDXmyfVg
lvtVQg/bNYejZdIE2rBvZlEHkBvKey9UiqRWblLbt1MYLE/RYFce3KFV3IaMQFkpY9+FOxpC8lld
4r0nXQJW9pkbKWbMTiBohydjEUUUaFmR+1fnwPJ+U7mz9jYO42LKizm0fXgN5gNhfEf3EH439p01
Fr/csVnI76HG1tgug4Mfm5u4KEc1b+KEWiyZDDLRYGRPFNt6n12mahB/P4/uMRAsyWpKzmXD4Rsf
+DtoF8/RdPoJK+jGoQIUqP5gbk9c6viqluGmYdoNzocwtojcf0LLWafRa9AzshQRA0PytvjscgJu
+iFJnV/VRaMreaCD8gtrconVbXxycbcnt/i2AIUxu8LHMWE47ITn7K+Fdxp55FtAmurDdfhExGDV
tUGJzUVHkijkGp/Ncweai4zVicKSdHR2Tpy+g7kwoxMiatiQxVnsSewZTI5jFBoOPEwBgqiRxJQw
uxJP9T5+dg0nA5eqf94/630Ty7oG9hCZ4wpKrzwqP/kmN+/nMtoYJmbc1cvQZq+XgOCoh87zUdWC
BisfrCUiF2zDTIg7/W7WWwDjhN0s9m0/cHsRaKM6yrL6mdrXMrjsOfqcLHNKHY7T/cyrTMmwaE+j
GHaPPNmx2c5u2Nr9vfFI3O/7T0rH+ii0aSq+qXrwFn/zRUpspFTSWbVzxyjAw6u6YIp5RvBRq3GY
Pp+DgZeCTDp1moSeGjEBG680yKF+DAA/pzfp/NhbTNM1nESBJnJdI7AAf8xksUvQ76uJvQe6mR38
BHYmMkZSdnj5yuFuYc6ZbZT5jIK3Nhv6fHI9qHiHQU5n7f2u14REAqHUAXNDaVbcSc8IfryZmPeT
dC+Esn2PpQT7hPrH0JOxx++X0bJlFz/TeOXP0ytYrphylihWBVyURVN28Q6X92TWVhR+0NaEsb4g
tuwfB57epyGaiR0ZovraMRJKqJQvhWuoQplcUv1iYnjRazS5s/08LA17FT1C5cK3zIlWsZ5If20i
RjP0eUXOiOAyTfTyeSxcM5RMyKebOh7cZIqryKC+doI6UJmNk6d1p1vn7ICb6sk1AouW+8UZpwEr
/RI3lZsad0TSyP0qzQK19giuoDZWYjx+SlSFFUX99KeMopFfFx6ZN50Nx7B1rOrc059EJmBLifVP
gsXa1JTfs5fE66IWuBsyJTsQfOK979LNejI0QjOSa7bkukPm3YRcYaqVyQIPkSLMqlV7Yp//EKhR
X8yRbBAXyQCmmf758RiHO89rDkN8sioQGfowOEcWZNhM5pB0C4IuCaXeTe2JHxQjBnnlOC5fyprh
wq587kFpFnmZNtSkCWvan4+T2547cxQN4FOtVBcuxXxFEDIzhM2lQ7CzivKoBteAm1fheggdaFTh
WPC/N0fbHD0/1jFFGAG+m30zMjZedPsenD6EIZFMUq0kI4Rnea0C7JP532C4M0U1m8//e4Svs/x9
EqAwoLrTnVGB4CkW2efZs9q33DW0PbdxOht2esdcaL9KQbCGsHQeA1fN4dmMd2W3G88RU1ltfwkE
56tlf8Goa0He7b55/8bjoKtLfm+a4V4D0yN7bTD9CChMRRlwtIUl5YVSAc4u6Wwx6n7R+MHnKP7z
KVQW+BR1Hw0pOOvaeE3pm6bKKdN0pry+GNiT857AMUiOdEU0SmZikQn+hk44C8s5UPwqhMyqT4Yr
stxCy0jEgCbxrws2ZP54o9UXKK7YxQitK2LREAPtFD9sZjBsyF/8KNRUtXcWzavaYY0rC6FDe9rD
xJz9htRX63KTXx5ePNmfIli9wef0MQSxgnAQqsqVObgtVrphoPSn3Qxnvpvk7ifIYWlIrCgejhXj
9NJPd45uP9DXUKkAdEVikm/8YWVstKnWcQMliGdHjDa6MkjUd3F4tbE3j/3grSU8zWth4PHS6rnJ
VL7bPqrVudTrg5yqW7Q+RW6q9KBFFj8FPuiSOF4MDx1dDJ+u4UvhfWtbHh9b7cTcS2dNj7RfGdud
03AE6GHPKLgh3+x/Dlq8tvPrVOkWRBNB3Gli7GZzHDmYgnCwz8gR+jcvZNjuup6zGQVX5MXlUc9A
IkhFawwTyxOzHv0m0OFdb02ZBkCRMd3rJIkyJ+mKAYzZzmdODp1OXdpOuVCqytXyDZPi43BU//Wy
FzgU1PqaullSTJXeSKRQMpZwYfEECrusx/T/CpHEJKPcoOrkr6nj4lJKWjYGyNl48a6f33iWGaSo
EjyiRUgnasmhC4qAuXMGPauwPVBwLyO1cijTpNpT3tI60+qLJLvLR3u59znODFWVa7II54Rzur5V
DNsvkGwgq71cjND55BG5PgoX+Zs6U4ehIOXxOxrsaY8vhqM8U0gZX94lohu3MShHhhKT+aThpk4I
AhMNTe7Uz9ARz/4WZbe/HiaIf4u7BqGTNHOCrktYW1bPcOsFNu4+euGd1jFSpk5Ro19+qX4M8b2g
Qbh/o3yfHoUwxtGyQgUwlnLwGgVbGfHcjr9TBYmg3GjW6JWc4uDHxYeUXEen2AW5iDGXveIGixo6
EpWfrLGCpxKo1c4Tk+S7rWV9qUAxWiKQqjujsOvkY2+7c8IsaogAidrM3vVdMhcJTIHhYhaaATdu
CL88NujpodkKMJZ2SSuk5/yM8kft2tN5YvWrDa9YP4XftxoSWnfTTKElOTHKIqlGvF5C9ULsoWlf
27NPVsCUSCNRecXju212Qyva7YANdVS+fX9nG2a5oiQi8rZuy1htKQMydwA9+lgZ6DEULPmcyn5C
beVaXvO2DidrgE+K1MwNMuALPzsYdKc/xqAGNohQeUWCexmHCLimHROGhXtxg0FFWUeIwqQXWQtj
n7+pNOKr0+Ib7n4hsDfYGcSK2Abl46aLNufEZ2NWBCXLU9JO9XmvLdqp4e9sowE+xvYk+JfvLdmN
Tc2824yFYs4q0EiMLigk2u0DiryfwSq2kO+5NbF6fCZIsoAkDpbN+t7/ghAwdd2kQvEFoheHGbKR
/56Qodc6n4Iy9tozX1pg0UwIMbVMieP3N+4dYuOz16EILRjWVaJCLn1bMAt+zqs+HJixjVOQNAna
cJMwbbCOWZCkTP9EkCWgr4QxEZ5Yv6+VFwd6LP7bHi0l2Jfzz3+MfuG8Zmm48/Cocfb4d05XRN3Z
8mzJJcNeLaosicTQHY23HbVj/45O9slHIU+I+YWHcC/ZyiIpdbBYh09v3KdtoKseKi6+UqSJOw61
kaY5Uu6FwalNC01XUQ3UNUFrTd15O/niUk+ioANaoFmeF4pWS5v0nTp/d/FitLWZg9xXK95ioWU2
b80hvU8PXROQ0bgkZa5DMtuPfgb5zkOcOhvLftN6DzJGT0FMar0r++fKkf+SrliYiJ+OGTozaI9u
zL1wCFCwiK4IhmBBCp0mRP/8AScDCOpBFQVFywECVCRhNBkVG/QLIV/7wg6dERObrnT7x8mIT8IQ
AmEKWwLGVLvyYd6qrGjDCeaQTJv5gWrD8q8wq3egMW8gLBiJiiDKj/WXApMnC0nw3OREh/XWwqq2
dECWsyleKGEhH1f+8JkdUnAro5lOsjFNZOsPIjiRu3BZqcv8rzGSGlAe6NA3LwZNtklodxuhfONg
0P5btwNqo/MbTGun0Ni0A42jQVk458EjeUkyXUIqfOeiPMvWUhatMmunPDbrsf/Z/XaFPGR/QXZB
VsNyOmgF+Jux8oC0tqa8Ohm5qPrHvdKXT4g5nRIwzAseZrsiGBb+nRZSdq3AlBGbz3z7EnWTegpt
7OTLRn00tQQUajvaw6VaTL8td8LZxYVmr8YWealbqLMhiEY1J4Oi9UuBQmSsgCXA1gBNiNcJqkU3
VMfKG2NFNW44QTdDxcopEH+TYS69czvdnBLfvRLiR+SDKaeUz3xwDU1kdTJmXF8fJBty9F/fdrfY
l3Q9v9fPCG/9TlKT2g1PqUupzEPDtqshgG4m3+SZeWaT629W5exlPl8KxmgO/V0SUetz72BwoUue
S6y3XY/FBN8IIV5GsXdwggxJeZY1v8vmCmIcSZlkkKQWyCc02Ze+zq9G0CdsTYrm+yNi66on4YaE
22Y3fFfCFt+SBcQZjFSklGtuhjEWEwd5PveHlCRdxm+Utti6DaTnLc5uipjvIHjqnaWXPXd6H0m0
Q10FreIEdttfs4b7iNCR9x6e60DWJiwl6ChhIGQhmSnFgsI0ef4Xz/iwceoETrr/+wHZsO8bHn7l
KojAuO0lpEzqaD0c95nwFifAUNmUJ8rGxTY7fRzu4iO0648KNfl/Ye0EtW7XTplU4aOheyAB5X8b
VdDtdQi2jqBMAp3+1tfH5MhSI0bTEUeAo8+eQUugPrvgVqzU7dnhE/fft2c25O2L31Bg6tnpAbAw
4sLvHs5z0/8ogimyTI0Jkl+idisvpNTlEzTCjgd/WPDoJziU8g+cVZ2/D4hjKAqvVNn7Kt9/n4gE
X0JFffMbkjHOnuJbq6WOnweoJEvnuBL2gB95Z4C2lrdRu6y+OM/WQK97W3R3iBi+eCTagtnZg+6n
jrKZP3nTsvjCHZQUgGJBS6Beid6tEntKF8yAasMG0vEDvmqAyUHr7M6syy7L00HI4cLiOM8WTJaz
j4UdMLxhjotfM+7EljwACdTAEOF2LPyzpDyG8qtmZICMqdzclJZtvOGx6JJoOzhu+ps0CKT2dd+i
UuE4JAv5x9hS0xp+31B/zW7yfjGbFrGBPANLv6TTd824Tm6G2eMe+Gb/wsjviLVtcRNIym0mLdhu
1iAICaeqh8PDmZrBFjnfojZjtPpBfMo+n8g9+eIbP5De+jNGi642URmZ5i/rmY8M3GPLQoVIE6tg
nWriQVN1PcIezT+FLCQoWNAE0xY6dqU/rlQGzjqxXaIjY0nBMN7PsAf7K8obS1zYYocrSpQVg0MZ
BlCMlqzOfn5FjxfXZRaBMfCjWTY+X+KudWuVd58pBznPBFx2vDsJ+vrfCK4mPHsIkLvE/lpnh/QT
meoO80LffTnmyIyVRHzZQpIMwtza4dF3f7YE4Obk3bBizCbgqiAaHiw3JMMBwjUqEG/Z4LXZj92P
gYIXdD7M6x/WwE2k4aB9e7vEKC7JqBJSnDvZZzzhJlPX5zCty2Y28WGzJI/g8oV3NaK0THdL2WQv
6RuhI0byfAyWhdEN4qxb4oHpwDIC7xzzmlPBclCMyBsRjbZ2YVPYiiHaaNyxMcAg2o5l7eIAGg6q
FngXKjT2fRWj2OrjIcDc/6IIM7mFlmsoNs+4HhlcaLbwyuIfWjsmRCIxc+hGmYdVJCQtME87N8pK
s6xhsxWteZ0uALFOCMDFpoF5wK91sryErPL0GAUgA0w1Ak32xz7ZXqnBN4ERPBkvYicO0xBdtMKQ
yBOJMRFD/H3BsG0gSk+EB7+RZhel6Qq9loxNjPktHAjlWziDmqDCq/HqNtgGhmQc3pzeEefxGuT9
u+/ghKgyJCYMUN1Xf06dRPNIz2MCaNMYh+I1QgeuLL6jfa5ZnZxl8L/rSXkoQAF4Rj/bTNO36I1g
pYEUqD+7YVd071wzZcV/wzOhduE3ZdQ26leBhw3niIXBMb9A4QjEydn+VRoHEhyCMaSGrv0Kl2ba
BjwrO/7+Q3zAp6wry4bYYU3bcDEz28ljTPJuIUwklvGmESCuxoQfbarCvk7VA41zyp6TRNomeSBx
Gk2SzCmFpulH/rG7sizHCY/ktTT+R7beKsoSN/IoywFhhRImt9Ftat9pbNp/t5Eq5r0klrIROS+9
+uXnA2uY7AeFO5HtXIRpM4o05EbJlJs8ZzfcngDr4gVxHuOZrFuCVRwmyO94ODE9xeSPw9vHeRMV
QnWqNYkN84crlEV1tejOhGW+YCg3/I69/UqOeAMxKujMkMZrFS1F1+YYfb8DyrC1CcGY8MtHEhCt
07RwAOD12G/yGzozr0lWQiw5OZ2p7d2otBQxuDqO9tyXCJmzefXg0AzN7Q5P0RCvkthtOUyQeZoh
Q4RM7tceVoGyIj4tSwLyOOUNAYx5BCCAhk0QWBeOcwZkhoNwRC8B9Y1pHAoZ9P5tQd7agsTZb85l
5gskQ41YVDFAbH/QLneP038zLxFH7v7yQUWtc/Tuf6APz4JG3BaGptSwQxhfzvzGs+2BK+q19Oqg
02PbvORnDBiy/1+7vpIM5NI5pyJeNCtVcMlgZsM59GTKO2lBSAjOsRdKSxUNEpstQTx8SHttVTsz
8P4DZC+huBDDwsmaxCCwOjeECA+JwLr11qlPuEAMrCAUFlLgbyPLSeRcKDHh3ieFg6x5G5l1q7zw
k7w0qJkRx4KLa6UJwe8S24M0Pvh274gEB7pMHTWdfgqFB6iRk2JpVZg5MVzaEUVd/fmhm0JnA4Sd
FObPYBBRIuqbLpSIrhmQ50uhEMiQsH+QYCheqbB639NFifHlMLublKAUHWp6+zNXjmhi/BnTA6A8
9JWF+Z5I4HyCJ1CDddlVAZYZpUqCbsWX4QUnh7ekgkuP41gOW2HIBw3x6vxRp7doThFZtRn7/CNO
ADIH4ePq6MCyqIGNivZWqS4Mdl2xjni+A6TRjQO42fBz6dHivB+zBLUpO9Dor0vwfCjx+tYCZaCm
CH21l2PYR5041MsUdy/gPsf6Wc8IFtHln1SqyT2HRBnZ2gzkfLa50uS4opFikAcYsEJ70QnBzKCZ
GXUWIHfc7N9RjkZIZnyZfrSShTB5VgoDlpWieVJbzDLJTh6TXGGOMqI1Ra30Oh8xEq/0m00elNae
eaol5EzPWRScrU7/2FJ7lMoUczS4JtzA6eIfrDjbBIxvpVaqaS/mzYuA4BZ8gIZobALImT6FCmdm
08RKmuik662i8yKTZmolb4qxmrYO1/T3JECfhTiFbFc/dYdV79X4ixbGY8X3of392AmSqNJycYnO
Ft2E5DhI342JroE+cw2BWkvvwnNC+k5CKY2DsbnbV8TNF4BxBSbIkMQM0rlGHjhyf3ckgqJxRlvV
dvX9Hx+dSsk1i4XRy9N0N6XP3odYDd1CzCE93iJjK9ebFfexSrAec2xfVcyT1q8+MH8KFR82M59Z
GK/NkYme/XAwb+n2jeZ/tJiry7abYxaG/IzTO3hk99Q+npGyxWnYjJNeDodMd1Ev8+Kcd7ISMd0T
JECKo9wh39fcxM1ianzY+dZ1Nie3JtD6gtuyqdXOnApnPD36mlccdW4j0Bpz2a7tMjxsdd/zuzpH
IE0ASSDbwsWgSpOxw9ecRA+XT92cspaplRYsBpDhM7vSw8OahEyNdq6X1IfwnmM0kLVrQJimOnxQ
Hi7SiXIg8BYac96GIAOJWmcDwS9I0ZeWzLzYMjImJJSWEk/D3rdZI7ozoOqayK5XeFGavb4IMNCe
aDvN5jBO28yPp7+Nvt0HpLuDBY2LP8WZEbFvE5wAaZ0AQNf90VEBNRsmRtLSE6Ytwf3lVsiCQhck
XDwI9y334LocQlOqMEjCtFIuVDOeM9+Jj9Wv2iddO2VCUbdOdO8NcxUQUby1by1SqHMx2UOVVM8+
1Y7Tqipfq0I/rltla5ELLH1cIjU9mfLHHyh40c/TEvOk5rDdz9xutMMce1QXf0N+/AlWuvSQKXqm
1/dkVLa6DtPynHUTB4CHYZd68cK7HoDo89SxE0foZ/eEluVcuv52gjT74ZK/o7ZdLezFymmQ51m6
yWOY7LsfNORwjA1Mj/xCkw4Dy1bO7f2sYPnrmKBWE1MjsvMbrIB2xxwOuzSHCnxVA9+3Tkc8XmNx
GnPnFqsTqDvBeqPa90OVfM7avWKb5zzL3dHWiDDSeLOyXIZESzmgd7M2RSZRlVbuCJTHTtWM8OAR
az4VrmZmKsva8GPCJKn0DQWw3u4DsXW4rbuTJfWP/0GZ7bOm1yu8MBkozOQpMu2WHI724V8uSFfS
xXvr1DfIB/p83yQx73kEat9oz83Pzefc2D8mlvTDvZoF/BjencbyYlRHYjHyecTY5mCiUshX5EFC
vKkVJqvW5/yxbGMMBsnH+ryCE5s5x1Rf/BVxTgd5HVU8Q7LkdhzA6DqDw6YhQbKTj1DTfzPhWlJy
DRCzmpATgtL05V/SOsaZeaUC4ukh9dJHuwz2IlXO2mvf7/ID3hxknVADvjciI8N15eJ3w5PA7JdA
Ojvmc+DBfWH5sEMTS2wuYxtpsXHxgcYqOnV16wD3IEasNSeOmy1tjZmdvVKL8UyJh7TOSqz9cKD7
eCXkC7qD1RkuWxPlqDYriZdG87C3dpVwprFZ2ws5gw3rKqhQt9vXc+uAmOjW3eTn59GQ2GEMty8o
yEMX4eK1fCWwi3XUJJt/Le1CwCmPIm3kHvp2Sx3lg3KYQ7hp6fLLdc2K5eUMv59Q1OsfX2i6xq72
r5vZTIHo9L+CNaeHmy0l3sV/OOWTKZs+BLvs5blRLEqBmuVCwlof/R/yBbhtlXywScejnXqr2vbD
7oUGSR3Bg5qY5HIlsuE9oQy/N9aIeACeF26/+Uam725TfAtCWomri/KuN1U3VUeSyTi+VykcPkpV
ufOWer+r6mJDrhwwOM8vHezj5TWJllyVIoJ7mqQzYpUniVTV63u2Bha28ipwz2FJrBAZuV0GQmfo
7bEaj7wLgcKvfm9un8hfd7stPqgzoKAVVyAw95ZAFarZn3Weqd3r77QwWLQR32iT6ePBXBta4bLo
tBONENjsCLeVOYn9LiFReRD1ojtcP2nquZUwBuhVpnmkzX7dzRKFCtVXAiu607rOjLVYC2nPA1kn
6YCuFh/SFPlSM5jCJ1vzycBGwhDwH3w/0ebGAMubk4CcbrAaSTZO5t3pSNHe4EH9/FMfTwr2uHl7
BXntScf9UQZZV/ZXW5uSCQeFdA3zP5RUCu/VecnrOdW5krGLpKM8ylJTJXdhdaZ/5LuScof3YoQ8
aHtsMd0pxtZUcHz40OY6mqNb+W4tka54GKH8aR+Tg5rdOrTLJWiGMCWPrpd2uwGFXRbQuBrJ04DR
1UQiMOlhnpocwg1o1qCxdYEsAMlttOtbHOXoP6jpKxjf4PWsxwpz8M7levvkOnVxpswXuMKysGv6
yr51nfk4yWNyivAKMTxzfOBwcdnaHf2imtF9S8V/uJ3vFH5p3/iUyNpdUCnBr10jObjgibr7y+qG
Iz0UR+U6vJWBG4tWAC5ZVkICD6W4/oOp5BgXlG4XuzUxnVluaIlzNlvsWMRzO1yKEgMqzawuw9Mg
3RxpduPSCpLz6/e+69fm+JNrWltBkJjd7KIMzdmUvzFxbKSXqiwz1RWYJzHxPn8Dyyy0ugt0UNDg
1cL8cvxkbnOmNiW0W/4cTLvd761YC5JamdbZhu0KsqEuctvKHUP62A0FbyKnHz/w5RS44XUL0SEX
QiEGd5/AYBsIeGuZxrNYJSxYMrk9gljvIyemlwZHRCqhXiN92PCIkIjQRVDT07X6MSrMHKdFHTQd
zenJd1521H9ZN4S7ny5hb+ZlgOtTFsYApua29r3wbUMBVgjdz9rQEnR1JyAifQ2rZKSZKsinHqRw
go3hHGDGfHKZV/YbEabPhPyOM4Zfb+W5Kol76ytLsI5ON0DmKlindjTt/Np8CG7iPtzTQvKazaaA
paPXU8j20h/5ywYyseFxnOw1cJFt7UtJcPohzLF7+ElkgFnq4FfokNYenkPKcXootSZpGD71Ed3I
PGJca5KPt1/OGmo3KcAfEzOMxcSvxeBbXr9FOnxq0lTprgHpRjJ9Fjvm3Hd6QQzVm6IpcTN9VkV5
bZ0uRgBjpkgklndplD/iuZF9bGblTNoKsa+ojyWrLIL+iwEyD4l2kA2jiDRUmRaK9/+iExcwixrJ
wcF0TAoUffhR1JZ/Y/cBCcE3cbXBUpY9sag6YmY8mbfF+Xou9UXd49tSgFbkew8S+h7r4PCsEjtI
mE2LqyrVUJgK8cNGF4Nk1AmNFVZTz3xRDvIbC2OHkaIt2yGksQ7QXwcMMXg2vzzSBcgMEWkfGcMn
6QBnHeBYF6ob8xp524fM7mivPUbwS1Bq9AuEJRn6peYGpG9xGKgI/IxgulxP9IvnZcfms5/9DOjs
rf5RzrHvCSKlkjcN2gG1hgHBfc9dY+Tub2aqrNjW6lyww9hEURPheNeEbqGF36CLOZbGgS4UQhwW
CTc4toVb0NykLGKY+HyhdrMUoCFDVbT6IGuTDr3AscUIJvbEdeIuMt8dvosAE6xzH8SmPQndHTAQ
xuJdnCHXCCouSdARO3zENA/1P28wGhvTiKSRRSQ18Y3kaHXoCVw8547q6acbhrrz1EL3Kmq6FEq5
nmaBv7IMeOgWWl+ImmzhuEpZRvRvQba0elHJIZhbfSJqTgOVrGN0A1cww1sLN18oxtRx3VJ5XHH7
YjLGcArMCQobSbPRqFDjw59XCzJqNNp8pKPszg6KOPVrSHfCDRXYtKC7FAo7EUOdyUoCb8g7mzAr
VKNfJw/c3CGZ7DTAH2NrdhuI4o8mAAw1pjRkL74KNO2f9ZWmBlqmm0V4goajjioxVpY+WScaf7MR
gp6f79oVoHBwzdMfJ7AwpSVc7yFARCeX7HcLlkYUda1ALcSzY6dmBSnChws/lUSwskWTYskRytQX
HrNEFjQr5qCDd9PKcjhAzwXUJFOfBG+0FOekukpBpXgce1s6dytWPKRy0XR3D33PMml5O1tjRJRr
ps0uKN9CbMBpB4j8v3lpfNukQmOJ9671wdqY9/on0Pf2fvfdNdYIqVh8ANNvduof3zqVrV8GdIbw
Y7mx+2S+bi5xsXt5YX07rKwgkYyEvRxwy3jGzVcYUOqVT62/r7LN7Z3picnaaizT87sz0GxJ0YQ0
utD7Z4+uQtXCtc1ZphqdwY0WwsusXzAFA4WEtgAwu8npoQbHV3PinJ835n22oNuMoS4n3nZ4nE9g
p0CGDsFHn7OCyuM3zy/QdJVZv9REWEsuHr8XdVg896tNww8DTQfq3c6WEBFdpXQ+whXw0UWnh3A/
j064xL2jtHOz6eOFAsjiAYE78hvHWrOOCF3AM9dOv3frxS0egBz5hM3gDwz9JApoQg639eT2E/CA
bDMS5xA6vaiZub5VTtGaJzuo3AXn65ZSRZHA/3hDPlZt67+G3Nv/NNofUsW9fzR32egC+ok19HeX
2RzJIPdLmErcAZZfDZA058wLOheE6p2Yx11kU9PNEIm7zeksoMIPx/YFim52951+zGDLxwqYoxAC
1nZBY2ccGeaSXT+384oO3gj0fKdVROvXSf8auTbLcKnTlgsKrIl3lPz9xor6k4Z3+F4ETn2tgwHS
saDDHvy5+Ok5HJqkgiXKHEYp92uiReCFSy1eDSUJ4QzauTRGgr+wwSxkEathCvr694Zdx8S4j47o
xcat7RjE301HoQEeK+khQXYdzkPaUEgXJH0upDmkSrCFvtglWpXRtgtLP4UcwZ6NirdaG9MQZCEV
WoI4KRFGBc6hgqJqk9KDY24bY7NtR2jzCBIJDR+XgfW3DrRUA41uqQ4c4h6a8cIdq8E78O0f0h+V
qqEVFQCdZjHkW70I0QyAYP+IDd/ouBW4AF4W9xhaXKtWetEzoACKL86phD069WorvdMVKeLiCsd5
L2xbKU53dgKIT71HAAlBA9h5xe6gUhzzEcep9hvzAKINuF6F0kSuI1XGkXi0pUjKKoWaYr9op7Lm
psc626Hu4MrL5vW6JrP3J+XNvTt8kvlFiIe/xK12tdeLB/C+6dzlGkmEtsp5fGK64rYBgmU+BOkR
3UNq8LQ0japNhYSujnOmqttY/jSm+rBT6OXYgEO50+TeRGIEIIM2beLSwvH29bccK1pAmtOda39P
W0U055zgnG34KJTv6/bk6Di4szv/yJ7xVZZ6cTuH2/UQi5B8Q8ju+RaJ1LyBqKjDN6wjn+6VrzzA
+BolACUWxG1tT5Rf8pIWShaLX3tXCaEYWU1+k3UgqVufqg40t9ZNgPjevzJBzIiNhC1IQlfQlIFL
Ao/CNx0bQmNDLcLA4lKIJ7x6pqZuQRFnFPBFKLNLp+k4kZv8ZpYlsjui87AarEW2muIpI7YN+H0f
Aj6calUcR38Aa9rargcOrSCdTOTsSZ6C4vKmALKyohiMtYHc9G1Ncc1u3VBNyY7AVRhQncOAMJIk
2Odt564ZuXyx14ovdyI5NKwizS0b8sBBxZnK/ANXdoLw2ORTwc5djnK9qJeswexJ5Y4EGxi7rfsE
+loBDjtTfNYkO6lbKr/PGBCWhM5SsEFin/TMhFAXfM6IbFxl1zXjUpEdI+hadbOq2VUKqC3P7hSF
CJeexoVxGmUCyeXGlSP6TT0z14jjiES0ai9Wi1N3kSC3rqSMhRFPkFvrfk4qSGudkkieVVozYPuw
gkeeMBbdZvogGsbrp52pxlQWUicr9SJBEEIhdEyxVcnuAXncpd5ZBLpEIq3AeSRfGMz0ZZ3xUs8r
aaw+051pp3LZRm8pCAD1Q4q/yvLY5qgZsDZ210z970p8qOlEy2LHg1r8je8Z33ZWhehnt1K2Ve7n
R01a8JZp5OkJJDpm5Q3u4ssMWpAR6J0kUFJ6Jla/BO4Ij24JhpxNvxm1vi7B9/wx+5tpb+j9JlFu
D2erMDNXztPHR8np80P4vpdds3BqaadOkLSYbnmqTLirsyf057whjLXRIVM0GloRgL2TsIHQq0Qa
yF1Pl1vmh12W4X9Tdiql/eUz+A3k8QXxzc2LTqwF3sVVPdDbedb8w7QaCOS8zu2FI7OOhYw2oWRW
emGlXgbjLtj01o7ulo6K+8HvVWEu/LD9/xpFvAy6bKcsIfjzIag0/93N+T8PruaAsdmC/BDyA5Rg
urt65+joL+9mkwsOa+XzNoVxeAVh2Y1QX+gGfzhG7GlDFDYyaYAQrMrfQVnSLfxIAvlegHCYTNEg
4rgoGjf1HHeLgUScLGSm9DSKyQkd3AUR5VAZ0bxaXqHWEaM6Zl0n5SFA2z3yCq4J7kdkfaiD0Y4O
jHjUgMZNnYbBYPf3mAv3pawt7lfIrvZsol4OvotuGjmPGh94V4sngIUyg7VWA51BAFfcSxBQIhB8
9zZ6ey/5Mdb5riV3yQ/+/nJe01oNz+47LN+9RnFs/xcK2fGCLaqoQs001VrnTrUCl0pyvjd7q0uR
5rwRoF3Lb5XQRhcE7YWAD6E1NBb/V/QwCN/ltyEKd37sMQf8nJRV2Dygnv1wLn3jgKxi57zAHX2Z
GWho+3ak8jNG7ZEt0QagaKu+EUEt1iYvdcA2Q/40CQPkTGnhhNAhxmJvcOVQzwuao3dz3yqqEe6O
O0NxHLC6lhs7xHnj7XMvg5VYE/ULDjE9MpAFNu3fFz60cKo2KEsE+1bbTpmEIUEx/+RHw+MLUYcP
2l1yitt1D+kZv34jqjWxXN6/kmg4tmtCY7ngI3ve0uwC3NRYT+RPELjzBS9xePk9SLkcAj7W+6bR
q2dHbuvWRhqYvx+zS5swsk8RX/By6OkaIuZ5eYk8e4Nl3Zk2wAzgl/V9VgRp7U4AfcFvRtK4fdRy
5MddbLegWkhfQ/JSArQYacOpFeLE6nl8YzcnWaaQY+HJhKyFrgOYcKebTBpnmaJltlih4r78F3LS
uXCTDpu1GdupJzhY5U6DYgmZpdkN6OUG/0hgQGRNniLNMShmuPTCe53jUsX39wgnu9vtmbT4D0Fy
Z1qd7vrjBSoTmNbDTtafKN9P3totvDSSaXAMHRSepK2ilxPplmYPtMoTkkQxAw/wr51+haZaOsat
R/J+WCE3pPDofetVNxF9vTH3tyRe7Po6ohbWYEgBx6tBq1Yr5eCm6bbKqYRp5p0BJ27JJjrA0ttH
ZUdFTc9+K66yeAzB+AwhlZRY81RMQ/6F9zDtKimHauqjJt6hFigVTZ8JwqjmsKJ+QQX/Wf/8VHE1
u5d+xMJ+dvhVsa/3yCxTOnN/PeMNvRk/pG56WEO1/a08nXT8SKNbYwq8bYAPu3sRC97i/dBMBUwP
l4AqGVwFeVr/v21A7PoarbDmWnKTmFHekROT/yHC0TRJlZpEyQp693Nkv+YJWyVoI2UWdr1Mbl0C
PLSn7ENYWI+yRmW9WosekuzfRV+E6einiWE0PBxqxh+hghjxD2fy8DmgxKfPtiN0AgA/bQoONGY0
wQzyoRgHgDS09kc3RKrgUR0NRLwXPH/cadAUccMcMkU6sIkAuT+yj8POorkUJtK1Kqde6QzmxWI0
aA52QwHOirlexxz5KymRmiTcd4WR3324k0TtxmNeGOGV5BD9+nnCtkY8ZevFm0BrhBV8X20bM5oX
ZPIz2WYrmWtR5HB0LcFqX43wJBG/YKfsaG9qsvNA2yhGlNRt+tw7g9zo69jBNvVHN5q6aYdIcHDq
zBCHjPYCXvTpCWkpFt3awYwHBB6unfxHztDF6yROB4/x/XR6AwGnu3VuIBX+3L6t3kceJEa35Ks3
CmfqBc0FV7LebYwgHK3q+u7bE8TSz2qaaDgXZ6Ik7LGbi9eoWLuO7bl/CvJeTSlMuqY9p2rA03d4
YlTRRL94fno+bvRD/UNgDJEnER5qliP85pB15nfLQg2koGL2t0axPfjmTIW4QB3+pQjCm7bT+7mY
6g7xE/UuKJBwo/xjN9MbP80x5ZRet/DmrPF6fBmN/adSzLnbOwcj7neseQvf7kpQf40kcjGbb8XF
yMRj8gFgSVYA/Mb+R98SocXAlORc9FQLbq+ksGarjrUVpo9Vvfc8gw+i5/2iPurHYSfsW/OPAOHH
v8IfsR66Nhw8Kd2V98aEy2JNYErF9/Ik6A5peBIF0Djh79CVT/CG/K0TAj07XU5OOWo91EDZQTRd
BBGiAhdbOibH8eE/Fw6Hx6PFE3v7vNx8pc8GqwgpJm5e039LlBOy7mGxEDuexjA1DXRQ1Ca5V15j
IbphJeDu7G+OOanC38s3Dc8E8jMqdMkGVgkBgTFgL87rpEW+v1Vx8rD3+WRH0oHhuR3ZPPPEicKY
q3NZrm81t4bPKUHblbEWjBNJ3raANbhZJH2uMpF4968CkoGVfVA2bxwGxd6Sv0ZctkcjmcOWEwFB
4pzqLKhqQyDas9u/4Q4t1t2IcMVfqK2F4e7n6Uq9SqjxToE2OjSEQaMWsZ6wzVrjM0YvgwH7oUbd
ZXcZBaIMuIaYqRNfMTplsN3+59PgzEcJX9Hnjm2cdV7qwSm3rVpAcQX6BZRaFxJQcVza0Q13fLgq
Ce1Ic3EvbEK5We1awmbsMoxcbm/dWZzBVxmXqlzRvgrude+sDug/LRw9jS4IqsFuhB+hVqTo7e8S
631W2bPaFdquFz6XAaOl9/ELeBai3yz4ugLMAN2APFPi1k5zn3+av2ddms2H+ph5/CVO6FK8xliO
U26CNl8eFIpoy6szaFzvtJ3hcT/Qii89U57Grg23NxaVhnoJLA6v10/H67kt9XdJnDMm7IEKxPIF
wAIUivhhF4dRn+tVHaSVeiUc2+lhvw4yN71yDx5iW40BeAjXkzLnuCxggvDqnRcTqzhgrJ/inq4g
PyQbXjcQMd1ItmmQCyxnS0Fgt930sBhC3PtPXDwSD+xiB/dskPZtCgqAuyTy9S+GtykSlXcXud4s
RrT0URbBdmNc4KwFLbw8wHZIpEeQwnRyKdaBnRzKkWZyQyxpc0DeaupRXBOHErqwLj0txLHTZeg2
mU3SpzYacVQyHu3GMU1wQXjutb8QWiYE8+TN5Q5wYlLVzRWHOkETyb7kxCwvLJPjjmuwEjfQmqJC
vyvNFEhW4pwqny8FxtDxHyv2iRXFoLCHG0ks23NMTJ78xqPGRsKI9e9356Ix5p2NrZz4scPujis0
mpUcOhwrS8uHRR8z+V6Wgz/OrmNsXKOxRgcnCtleAiJ30jBWxa2AN8x//nlNtpQV7Hycok9VbBDP
K5xLB8erFP4qu/eb2zR7NOcBrrf+n3K32bPDkjkkjibh9EOZ1u3nVdfXRta3t5uvejQbOXKlE/jn
HzN3sZj0I8QboKD0AazgGGc6drfZn0VuqSRJp3p2zkQ6Fc0Eu5Q7ze1n3ASeJ111IQPoiOSimNsA
xtqX1aFzRWucUk1OJWuAx5nmmU3pl9HL6r+DKmrzphUIUZHYGPv7UIWyaJrC2TiYcnzyQpxU5BiR
sgi9JvKdYkRdUiTwu8GKXUcepKUJkB7wnF4bmCW0fFH+rodARADszcHxT1pGQw44J6WLOb56qkvJ
Dg77qeLA686sOocF3UaBt/ynzi4MbEeRbZqkY2tiVpoblccH+bi2OM/I7uFUG15mSUCiof32szqi
eQ3jFja9jDKNH/sjE7Dks2PybYNp9lo+dekHzUsGLKRt7DpeOH5yrGXl4SpjPpGbhDIlMxcOoQr/
kUiQq2pX13hyl+WNwyo9QK367iF68Uu8nmcQTCY+vb67DsVYS8uS7SV7tBMsTo7uhQ9o8Dw4roQm
GGmW0Q6ebt5FIxZhciBeu2/Moc9m/j8430ZVPp5JLaySihA4nvH50axXni/VapOzTGw8Ogupfpot
i1ELhuRh1BFtsZmAgzKs5Xx31373ahiyhO7c2bvRcYDQ4s2gfkogCeZDFq0N/omXTgjIpGFkdHuw
EUwskZTGcyYHDkFn2YuF6Pn0xLRiRid/EQzEadBnHWXyDAMU/miBHRuK0nWhmDbBa5urKCYWHHQF
4T5tsDT4HmdTr9njXsqZdeZxCPsepmQFRCfhxpv51RBH21DyccZovC4rf4wYBf1H5OHen/M0NufU
aoLelkLdGcIIRAeGGRMQgQzOEvqrcaKb58kdV94tB2PNb+twkzYHmC3aBhlva20e+5DA+8E6cvTJ
/5fCapbYWunfgt6tGPsnz4HWyuvH/bJ9TA7nK89gFv8DzgZ16BqQZNIqhzEUoJW5hCss92AwGZpq
qHkzWISjy52bAOtcg/fw8RYNuT9F+kvwtmz2RzKJu0aeUHYW34GbncL+pq/Bq9NqFlxJhV63Gts5
zWXN9Et7OswhYwbGc/JVNuoj6nuabbRqT/DvBpC79PK6xdZR4n0jbzej+CS3ikOawZMLLttMGWZ8
35JGd9UK+ZacBPtC+JqxqQeKL7wIW/EGW+ltyYnvH9UeDhUTajzk0up2oWRRm9qc0WOO3zDz4Dur
gxrHI9RPZFVPS08/dP9Ux7FLor16FsADmOSvTd4DBGjIaW+uoZZGFWD4XiCVfzUSwgxw0Oj7hJt1
l45GLmf445iKmL4KP2zrlIpHt63dCo4/Cu7Q5F/sdfNh2PURr4x1mSiHycQSYESPKMQ87yUxzjwi
feR6mHL3r2vGQmNU248Bm0TAoy+G+tc/4fLlw9BvsNCgfza4XxaXpSy3MSW/coEyQEoNhlcBoQsB
snV6vTdjwOWswDKQi/r2ZHojy5ZTtYFuRMsl2p/WgsAu68hwQELdYApkxjiERcICbSxpz/kcfGgv
eajXlOGhitEQYDxma4nit8QjDk+9ismuSicwfE1wfzQDdXkc6gzOBJ5fgYmU+jb/mDASurBjIiK/
KolJVMGtFhg+MnNdqsWYMKAUEdcEbvblRLgmEfHQ7jaXacOKQMYfBEnbC7kRD1nP03LlopbGLzmJ
f2drwLk2sCON3LO8XR5q3d8Q86fO11JWeD7QVi3KZqbvhFcuvLKjiVSD5mL1SR+B+JylpB0WdBUk
TggrZNhm9WATuN8PnxLb3SvjAqsxWhvN4HHwt2kKFwGV45aZAX/5bKxfguifXqBoOQCs2PDbCwyZ
hM9qpAamVyqJf7XcIf2Vcnbk4nQvFtEIBGfs8TjPy6UJSolldFgNiMzTCJ/Wb+FfKjnpaCcEAWib
NbOQCUmYc2peQi65Dd+iD/efjsRzCUbcjyMxMxupPiSyw32s8hxjhK931VP1yV2uQcN6Of2IGFii
hxmwrW8fKFbOvw+XzO8qoQyO21iHPkai8DeoUYkxUoDOYY9BIpRWGqKuKJ4tgiFLqJiixfHgR6hi
aaI/6qx7gfLdG388rX9Ly/oh5ANN7XGkJcoVfq3kuc/N9yAh6U0nnPn+Mta75bIlFRVIBZx5gXLW
G6OTHC02WGOTra7lhEiWwZoYR62Ag70Ghk5ZmHE+fmoACWduo9/YVET9RorTzUNv11h0SFIqpgNd
A0NwuVqJX571Aucr64HQIkj8rCtec6yD/ovFAnm8CcyY6ypSOdOU8OKBeBlW3VuvQmIaD56Y1IbG
2BhtZiOsAagxizh1O38vVkRkCI76QXc+1FQRtrFT/dYe2MHqxdWCKbdQHxyUZ8vGxBozCgz0tlF8
k6N5enUejM1KLItT/Jv7LEmshI8aA3EV4kkocElxrrJCHMgURDnTuDKcB6w2FX29jVs067Vvyl/m
ffSzGuoVixwE4pj2mUKs619m+rBH+pONk8r3C1qxHYiqfZa0zFl7rMlBTfOzAvqx5WfKw51HkyQc
oBU+5H93C0F8vRVMGLLQJheAk1sgFXoSywJZMnFP6ZwVN1eVYg618et2GjJwbJgH8ZGGSnMLBjwc
aI0NAYyG5eeihbkrNEo9hUd5DPgOKDwltpThWnRYJblLbsc9QVK8pq5rwr/lftgk9Mw9fSTc3cc1
1YSWKB3LXapDKzbnadPOHOR0j3WGWmUj/i3uzLAAWwlxny1O8zh9mvEPfNBsMLoJ5IQfDzWOvKfQ
7RQ3gUEGinaamvIw41vjWcF2VnYEbU/G+zTgNr4wWecBWfFCs4+SdB779aDGqGGgljm30eMHvXUc
E/wuTADlPMZV6eKk8/Cpljbwmx6o+j65imB834OLwHCFZfzKmEp1L1DbfeBzPQbdVjLiNchG2EZM
pry+fWOrIUsk7KZAkRWILWE67aWFX+3vm5ivzhweMDGD+pNTAZGq31vxwfTbo84IdKSDZz7njme2
+K+IbVk1nU2/yko9xhar6QWkyWEptJX6pUzBFYxB74oLmhX8Qwpaeh+1En0ppFW+rEiZUCcL0bVM
wxNBBBLvbbs/2A/0SeXuMtIC9PW2M/C+oDC2ysunKSgT+SdsQhw520tTvp8s9gTqU5knVck94FQp
4STRH4ekkMfIuhFCb7OUp2+V4wY1NqDTEnLTaaEII4BK7Oom/fhTV27WQugFWEEyXg4Y89ub053d
4YXa5hc3+BLjdyqE0Zgz/i9MjxEqeB0X/ZxmfG6vPKCnVI/oLsvfI9WNuT4dgeBSYz1JgYE/6T0E
KpCLv73Jjf2dOzDUQQTUQ9rFRjPjjrRX+zQo6hg4oY2Ffq5tr2XuZwciO7JMkwrG6dy4vT3C3OmB
0pFCVeZJ8LzOnODNxjsXXgCWJe352YyMOy2ZfgbN6kj9f+aoIiTrIjU90x1SHwgJgrCK3utHonOj
kfBurYG+AK/WKlDNUwRwcle1QUri0nFePEec2PetZWyrHqhEIeOFBo+xfle4/mMTvWbxo/EwK4Bd
NAoURAW5oiDJ7YYl+vysgguL62B6ewcPb6lWKHAV8++8B1IUlJ+GV2X2h/nwpxbnNYl0RH/ArlT+
XYhat4KvPxEl6fTENlGMOYFs3TUS9hWUDQKqydRVTNwfhnCMPVkLuqejdCAXON/l4O2i9QxrimkV
CIUfuRqqF/ZwJGCZY8A6+5n4xDnbaO71ok/s2fKNq95uLkdSvfc01fXagmt2uDAOlOzeHt27IP7+
k59AagdjVkVyOuewo94jLCa0mOatGywLgbdu4qI57pByFV+FJriTg/WazhhfFPMpvruH8n3zOWGJ
VXrG0pzCoWA6w8IeQ63q5DvYxx01Ln0ztdx/0B8quWJSUhIWzqGourTMQPWa3s2SnW/kaOXx+sgN
QNJzbOhgRMDds8eMrVA14sK6stewuW3vWLfvv+XjWO0UI5lB2lluXeb5XhdIiMjzDpcnIBmU7gRh
TOtz4iMbRKs7hVHvMKwlEoRRVG92qDq0IYoQNFHRWsb9aYvzj9bLM8KPuCvAtZV+JpLx4+duiNZt
PoTCbq+i34dVGVXDBl7CtM2JMMiiC76SYhzgIx+1aE8awYSZ3DEZqrdT2fjVCfSCf3yXFflXGJqY
liqi/+Exchf25J+qGtQICUr63B4XHUaxnQMYgy6O83pZzz20kLkeLxg0Sa65CmLVuLKnDHN9YSZB
nzzpeXxhUSVH8x9TneP0B/67S8JinoWow6lYMze4rjmvfRNF8OBfZPfWKnVxh5h2kp+8cUzYjo16
HlHoQEe5rm4ywXD66E5wt8Q0kJ2NuGj3vOziNPy8ZKfxM1iG00Toa63MvTQl6norwmL3WWNgTpeE
E/tiDLB+zFe1BuLoVaM/mzvrKHoB1LKdVkzbbgaswVldzclIWra3jiPr8OXUB8vj7SfFLZ4kmm+L
tbCjq3n4bNhnXrAmMZMjrVXWrNKSIvnlZmPp4PGowbwST4QluNCL2js2TtSEcuZNoXkrDfaw2u7C
w3jKHkViyMO7+Oq1kAsvGaUbSdwYCQ2FbY9wA3guSTzQCubISfSZGBZCbqcs0SpBJyr224ISU6lv
R2u6kImBRe9FLyhz8Gp6UHg+2jYwLSG0AVCNSkUpvpYWUUKjlDWv7PSTFAhtv5fD6OW6LfzKGzHd
taVa7h8HNFuiyBo7vB/bOBXIL8ervm3prBu4iBLAAlsZvVe5ZykOo7UpfTWhaYNAmU0rpfS5hJSU
GVs4mD3rhxS4RXXo38N+rKPXZSxzsMrmuLKbTIaYFeOW6R6AunlLNwGj5UNEYbCpAhb9iCmjQKlL
Wn59H3vVZ1OYeePnt8tlOmB6UkjYyTc+EYhKd66bMlya+Kn6KqUm8Lz5r3Vwwl3iS/QsQMJio3Ts
QL4yv15UnWlD90kEy5DE1R40PUbcr/+Ltkd4WEAhUOibt5S+Qgty/MqDMcPxLPV0DquE4JNFPOdX
BB5uEfH3qzTIdQHKbY1yghXXaaKfqwzFKwKK+VZJugKFsolat5wMlq549lktgS3V7ifHhNxXVECi
Pa6WVwrYjL0XmOL9DPGG3qkkANy+Afxz7SNZMGrw3QFxwLYWEOXcbrTDHjqPEXV1ZCTQiROCIg6b
F3zeDeLlODvsvcGNofzo3FSVeCcNLJRJuqKcTqTx5iw+ZnzkwXLodP6MX3HLZANz+FKqoib50Yso
BnTwbjXNb18H8QIoxqWO04uP24KkYLac3zy4KsW96+ynS9qCaBPF6JkLeiADKJ9Fr6hD0iCX/qPS
THMG1DKX1XteswNauyyqF+A1AmbBTVOhI8IZocCVj0m2f92cbww54IHCoAL7F9JvbEEPYg3iaATn
nLsEC39Fe3rM+4okQzzDTaDw2AqG6sw7SYAe7XjdsUCLNIekeiwCfzKn4Fwedf3CHywEW0eC4910
xK7soOV/ubqb04enx+oZsElUmKLlM87LfmKNv+ASVkxmQFA/n2xu2l0hw1fk6n50Hg/H2oAVJCCL
ZUQMwWB1mfF7QIA7BfmnLM1mRFp0+5zLSfeHTdfyeCgYqSp1mrXU7tXup4l1o5doV8QOWT1ujlKJ
q2/5IJXt1mHcneefz+sk0f9ms1CAeYHRAiDayfNX35Gd2XJUqsIEU8FxWkeDXghmkiQOH7RdB4AZ
C9pjVPCTxCnnFGYhPv1TfsDFJwDVnu5zHlmLhJDFmffTfLw30XCCZkf9cxWd7e4FhWiP72ArU4fs
vATTPUyhXynbAHmaP30UrNy/j2kuHwMZ7JbmNoGigMwlQhM5j6p3qQ1xIpBW0IrQ6C7oYXWoLpKy
tMFqKKwaerqozgtFel76sd3VX682/gZd4D42LINBI4JmfwDXXYWPmC/SkmMqcuW+FYCzAOz64U3o
Luiwgg/1R/UqW18nRA0sbMQc1lgllAoXYQj05jiDJGl8mozoDmLt1y5MMJD1cvFW71PzmE3uhLPT
32klgwdx5o5T9cZq2OaRdLBwjffX8OKJ6uGKzHyrMqP87ROQGWSsm6pOCk60lyqmg5+mR2xEoE4j
YAEGfyUn44ZauDdV3txWYGDgFAhnCdqSYlFzElsbf67iFEmssyfVyOPdJ0LVnQJxh/sdSM44OEAE
lfHjKVxx1w6tlkKt9c2V06BJQvx/qqdKK4OpBFlSutZQ9/w8brNrWrBJc9aO8JNkII82OcDWs91n
bxfP0KQpYVDRvs2NZfvHMwTQryLfY6CiMUj0HXaws0wEZO8tJY0I9qmIAnUmWHGhTc6eTJZLMINY
iESakSuK98sW7rfkk62MKKTt9mkpBIhfGpZIDhImNTM/WbOJFvZ03g/24jfTuic7ta4u1uLxcJF3
f4CocEFSgKzSiu6ekSktBOJ2/z+Yx37QesyHE1GuhWSOXkrPd30kOZI/KqwhfYcptHzdQyWfAlqy
eBGSaUG5gMIjNzb7JcBJ9w5WKAfbcFTmMPvu90zOq2tNyzLYsumhRng2QxVUkzE0dWC1NNCS4VUs
V722zT3GQIxMAptQIDFliCBD5xHZWMOBi4m4mJPq2C0+mWG7aK9C9G2uoyLc6rC0wXp4l4M5EQxE
UzNo5VXz/KuxRDv6PWMjDjPR77qGsQZTSwdbOqrJlCYi6G5KRlrMlsVSp72i6FolbfJbLGUpd9L8
KH6x3bgMsKRTEifjS417rlSgUhL7cHlwAsbjcfVP2dloMLZKdRamO1bb2kybksfntRbQyB3opoSj
sXQ6C7Pk9d18J+99Lm3T7uhiaIGPNqXTO3QxhOjBhqEdnXjij+F3HxQ+HQx/UY3mA6QCmFGbagD3
niWs0fhQhA+veOZVxMWOaNdCITJ5P6IPW7sLxXnJPjgtsSV/Y8HHf9RIiBrU6up0dyFw5/4h3qJY
owYPu8Y1T/ZALrl6uV2PszHNUiDPcMTZuvj+8D6W4sS3xHbRuCt+qUTn/Pne7N41Zk/EYHjeS3SQ
Vo7qScAT/Sm4Me8CLmdTBFT23P9kkLlwVP/IhUpG3oy5NwJ2fa6KQAPhWcio/kXG4hYOYKj9l0hX
dkhinhdPUUAoz5KlmGviHv9HY4Fs91+kg7+KHczS5oOwr5n4xjR3pB1zM95WKw33zRXlKQH2IN/j
ny7bBgxjSSb0TzoIcWPnChU4GrWETMV9FQYzfdIUQPR8HlYwy2OSUL0+MHGdJh0hMH9oagSdllL1
Vr7JGMVRTDot5jXcpMpS48UqK2neVDOiMaEHk09Kw95+WvBdJyploGDb448utcCv/opq796f3O1N
WmkYZFGLxytFwI8AaWAuarEqVM4plWvX0WgK6/gSfjZ0jiBiMDIw+LsJCtPEOZRJGJO1fl07SPfJ
XpcIUc4AsCIdj4Aa0bC5F9k2ZysYoppGc3yPAUgTLce4j59PEJ5mUw1Jmwk4cLB3LFWNqQdXHi9L
wcaReQDnHVjxMvDMXYZQICIw8TvWDYsUJ4FGVlAJy3fDyj/yoT4tvrqG9GqmcaNmNY8fIW6nAuC/
zRFgrW+dZQlqc+K+PgtsZOduCTSjB/UAmsAK5VDMWv4wyQX7SbNPV4dXRfyUBriljBCLZB0ugjMt
sE51hrWhlCfQJZtgWfBEpsr49DHLiWzPTaxrgq9MQARuqrhToL801tQA9geBfMJx7ixc0uQelIRM
wnVVMQNuQpxYLB34+0LEck+8BJwv6bCnrWek0RRdfQWBQOaTb8vlOQ+mhcb2ozAoB3faKcO0zaK5
XUr/P2tF67tk7fnITxLv7o0t/RpcAcJvuti/+dG192VPQpkMpOGCtKB0ebhFbLu2JZKYdcAjIoKI
IIRso472qBf91mzkCwFp73uvUtbXn1rFQAGeFoSMkInrMe3H57qdLoVCcfesfu/X8YoIZmPze9QL
l7zNcjRzRVcmfP/6TlMy8yEU5KU4RHE2ZVaOLHk0kfeVh1c+1Lm9kd8Y1opEa4qDTdguohySjXwH
S9duXmiat9Qtk/Hvp+/bvriiy89SexCUAJq9Sbhpq+hOAnKDQyKXfUOs+c2A1pfuA0PVn29SY3NG
ToD3W6p9LU9HqyT7QS4TxekYzeSkzKzqsi1DKAgt868+fl7lkS27BboFBiyKsZrbx4q414y4ipyo
l75OcbXrA3YUUhIk8rvjRHX4w+Hf4+sE5yrIynWR2qrR1aPqWnv99/rc80pVx22hf0K0WfaQSyty
zc4qhW7K9WiJpcKueuTptrG3vizh7RmCXhbXLw2Q5DSRu3Ad5UKViA7Nz3Zp5RgH7KOxwi016uqH
J3uLzDSxLhmy42GoVdYasnm4KvkNE4s9c3eY0krMlPfUxKGZeZKLUVdcOoIeQaTncsZ3rae+Hc3B
4Dv/F48001t7+9ibNYTHqq6jUnN+3ucmmi1xA2o8XSyaQKPywmAFggiLSqNF1Re2U4VOFhkfqA7D
DYcJyOEGcqjGmz877cXc7WDXLyAecezTzZtxWZi/bjLVtir/tAW/GoCpbpUJl1/UCRMi2iMnKzUo
lKW3QPIH2suSEoi0ZItTmlwHMznLOclDA2rc+79AV4tm3VMS0Ng3GqZIjqdn4Hs7o0ZbcY2Fojwx
+3AiVS89enjawZSxNqq0sxfpC4cMAQ+RaL+Wdr7GxXVttDZczF/3AUql1XZ1JtVfL24F4NE47bej
HNU5VSqMK8h4xW9fz0OCFsPegQrp4zGMJpGG39coDUdlM1Y/V9fkSinJJt9JXF0gSJO1HiLw9UXM
uDswUmiho2UEvHV5BOG2YVPKy8vG61iIN+G9afO7qCyXJvBY7/pplWWObB8a2yESYVsfmBp82a8F
7gL8lAsPKPVH2RNfVw4fWuvHFloWmbRGoRgKK5uROUsEV7dviAdJRrd6tV9Fmie3Y5lxCYYwYrUL
T4voGltZKwGiI8r4fFMKnTtHR3H/gQ09c+5Mi4yI+YjO+i12Zl3cH/ZlXobZRrkP/IyUxU2sakLx
Y8vW6zmMnyF8xAb7R42OiuX70aScewJT9Zx4Hra7Z+ccBVsE2N6e0WJk/cdFvD0Ng9jxbHx331xn
2YjURrFMZxBGx4LMaNYaHsVZuUAehQ6KR8crVv+9dHIlk1ZsD5BTMhmQD6tnUOzAZ2FgaL0VMInd
kwg6gsMHln6zxc077uCqkuzbjdVaAM0/e0B9Vs+uP8KQogA9NF93/kOJXsrSgHZKWO7IZSrjknEp
hIU69okvNBVnH9cLkTeOnz5MUa3M/MOu4HHjzP3jgoTbd4qhobq9WSJVEhjp/Gn4rVYofdcPMOhj
/Fh9I8N5RPr4swwtZp2lbA+7k6bJmeHvLUzuDRgya+oF3EGNAUDylSKBflbHLxMnQk5O0lOJ+H0j
/uc6kPFOKGdneM9+gGPa8964z94d5rZSLx3P6/RciDYFnjugtx98x8VD3xchJGbLGrOuIaljShJC
XUfeBnCb59g71wa3AwQytvRQyMZLIKCx4wJd2FfW+pMPUXeJaXmgPKnCa9fU4rX39r3nRJKTQ8B4
nSLfsIZ+RCBZR7rwPOdWoneK5zCUPFeWYQfBA+ZfP4ieQK076BUoCeT8TE+bE6w0/MVIwo4NhqD/
n96o986qFK9irrTBoHHxnUYuh3bu83GzgZoHBdfFGXzDaV8SuRuOYZXWDEC0FtYcresmkDcdwGDf
qIITX0mtfcPcUW2gaj/3paVPBJ2AyO+pdP/TpRBPytkJbpo4M+BAFM1nehK90d0cLREUuQ9Bwhv5
/4jMZ1S9cso+z9ZHyzmG7O/NU7n6TBit2+wOqCDpbouIRtNNeFGNzTm74u0U5wsuVKgjYBT/ki4H
DFX6ciyqeSJFTGlJdLx5lbYBXLd4+SJ2gmgljiGdcZ14etYjMMC2ibIuIls+6ELhUAdHdw8N+VRy
PmfL+bRz2x5Zewp+Zdn6hcGrcNfie/R6kBY4n3KkBpSntx8i4odKFD1r54CcjoFssz+PvVInkN4z
WxK5m2PvO/XV2wODirmtZqtWHooGGeDA/yZo60fcO9fKQ2CRcCyl2mV9YcE07BzZeYbBRDxjxtaO
/0nwOWByI8GTYYKyhd61Kn9xuSPwG0VCqdzuk1PuHabsIyIQ3GSzCp5Aj51t6L4Vi2Vgfs6zlD/k
YAHN1hyZatMjVEEIqy2721s09kCDf0GJMv9xzan3umn8TXix5C6Ov4PSOzN9U4Tdsc9gw9yXvUgf
9AT5zezESo5aVv4wO4N7CERSyQonV+8R2dPFtXPQDlvQ4USg1CysqJGMaByLY2tTnO0Js1Puosnw
2vuvQ2Ep+ntOkoqg2CxciJkCnQ+eRHpXWTIlrgj3HBBBcXnCCKUGnw2SOx4mrTwdUceJ+w+LSXsD
F9myCM117HtU29jlmBYOBwXwrcCPh/55OHzDu3zLagogGcF/JbjzWz06igcGgbmIz/R8Hxj9fby8
1h6IWa7Nwcy6vUBCxMi70rq/0JM+ODaDgqTTQ/j+SRmQoGwu4kdKj0p7rbltqd7tiPyUgCRKFeP3
f+TDUIJKz8qMZS4OR+BWV51uLvfTeweWwARXC5ARTpMRRJHi655yYUJQ9X/vdlbnRdZtj3HRJBGq
XpVfQWl0t2FgP7lv7C3Nd1TfFy8RUH6kdDMBhaEducWqmt4NyHtFiF5PKwM20k3F+20Lu6vZN2OM
/K9tTG+xhKtFjOT+x6TtfIr+MpW6PcrQvzW1ypT2VlhRYXZxrsicQbHTxHfco355+HHxmO0N+mlu
0A+m6WDYL4IHhC0c9GLwtS4evB+Z98SfNTu8sIZfK/PSL2ssPg+vOzrtzgLA1FCMrAZOlMrdv9qv
UiamjnvySYjXjjtcgrGB3Cv5dnY4BH+ScBwRccfemqccma6lj7DGdcywhsiSx7+tOjOa+Na9M6eo
iYacva3HD/3hiNui8fS6JyTZhN/XbqSbma+zsp1Gh8C2v9ALZH/NkhI3QAF918wFaWEw9Cb4iN9O
zAn1O084f++MNwPV9lbfrZE9bJ/dFCa+g3QoQ/fD/tSPpxD5rfPJwfXGWGfgfDPklDzutmNa0B7i
BJObGHCcT3XQk4GAMLupbTU6m/PDCczsTE5oFRx0Suh5S3YAg6qtt+DaS0jVPgAUgPFcyHUpCj/P
wMPtMkloUs4+oJ9RpTt1VBXfNm7+Xgmxlsos4oQKKdZfHeCy7Q4beIfnE+kAsEnxtc7NUUUzhCoh
9qqtzrw47g4PdVH21KhNTJlliRlBEaDASGQXYniZyeckiYIlCpKphwpsw0JXht7og0OUIKG09/OA
5KSL/tJRrtA3K4K/GMC3FhdPDExXbdgypORBquqHb0kLqUA1Wafl+3VhyOZKNhUx5nW0j/OSCa74
LS6VPYfacmw1KFed0T3Wf2/3z4gXFFtxYmcTBYoBw1+pGOJMZmTaf2hcqa0hugsSw9uBEDybpmbz
YMtEFtvIPgSflFicE4P6oaVZmBJTBJUetQ/1yWtqLMIambu9RYFlpWKJRJqNHLBnDYKJBHH8XmtL
RjHFYJznWd7Mtsd/L/vvplMg+FvcwyzJ6saxBnnh98BY2cwpuDWVX2wGACx3ycJob9VwUbcMvudU
ay15geioXynpKSeP783Zb5F2hq2Ro9QjVXwPxQjzc7Oc547aTDQCqGQr0GDAMys8fdHIfFQkULTE
5dgormtuIivpz08remukkj/QMPQd1E7ZUnJMhcB94WIVbBGVbgxE7kG21+Dd0+Kk9Gu9T1Z7Kh2P
s12fQYM8J9JLPmQM82s9GV7+p9pw0srfXlpXx11xTPkw46qtx/pCuMRpN3s+lW2APCZxStjcBgb2
mPBc7qTcPPjXFZnj/fVkNq7jB0TT7A3Oa68PQ4kWb6t8bdIvacqpIyb6ZCPOEaR5Mo2s7tEbs7/A
icQ9cUCuPCHp4hw1LtVS4sFhdnY2DZmYxy0HC8ENTSxHaUVOd5ACx0jqtl5U2ru7PqfFFu7t4Q7/
b+xUOWOwsO2X0BQB0gYrHZIoRjfEyOQqAcssQKF4+CFB2g+rA2GaI36iUkvOSBNm7ggd9Nc3IF1o
Ov7zkrlyGPDqS/IBZGW6Yi86u+aCJsI+4eonJJ7oRk0jmgKm80e6IcMDVUum0z1qJpUs+WmWYtUE
xdrr90ua3yjoWQCPnAL6TByfkNneT9aoo60TUe+zwFrgGw6J1CDeYm30rSplAk/ffHt5SNuUng1w
MxMGn78OxzW3K9SlROlLAfjIVg9+7gWX+aS2l7Yrbu2LWdTwTmFg9ZevDdKMrdgyU4P6IMSvOZv1
YJahfUr6ZZjQImNk1fxTkDAFMkINADbddLLyL12AprLeXc8FtKEmoF1ubiiIDKsadWSmZYnqL+Cs
5gY29K+bDFtHpRb6jk4ErhZ9TPoRpvaoSo9MHBbUqZfwxkqM6qpSzbhGujmPS0rKPhPsi4twZU3/
hePBxcUfWJwlKloGvCcWK4vfRD8o8xo2nXjWLN2XRR059sV8hkPO3EAn41Kl6xGuGNu8kKbHCxxG
egPkhUQMU5AdMO9eUQ0CYxqTK9oURdqJGRBbQMjt9t/y0GW16YlTyOhHkNEVxogPOLIR8ZP3F8xI
VDda+bNY05vl6FWAcFOZXF8F2WVYJpLeYr2tZp8bb+PObDBM+xyx/VP5zzRZLTuIle5aLLHVlh9o
cqDgxQ1PVhJt0cypjUBzml37ljeoTgAQVoum7jZRG79biCNpHHFMuxdUKie6cFnyo6BXl3vqjP9N
kJq7RXBnPKCd1Xxp9cB4b9odQJvSvQguAO3PQlp0XKf+9UK33w5hQTilSlZMqPelS+QtpwwVpkn1
7PKzABN+iHQ2gBFqiDs30SxOVwZhZT1po2t8wQVX+pR8NIlC7Drdc9BBs9fSIGh3dTXUfZwqkGtP
ncAE14hiw43FSdLgKjSGYcxreKMoZgFr3LHiB8MCZ/mr192Qjf+VEKSGEeRlGocrmzOFi+Inbm3+
yEm2ncamJ2lr5IgPLWNdvdbrweYQJIoIIqPivKc0FD2Wf6xSy5BUiw20rKsTrRY3bF6dRMzC/Ki4
EvjGfTRTY2Nx+7kzEpSdbhq78RkfhQMnbL5xDxpVFABL8N/vgjLvdyfmzgr/VwVB3TW/ix5Bfjnu
UBwjtAZf637nDC7zrki4CsED9IK/oiZJoX2lzxqf+ZM3U83e/5HxGXteUioj3OiwltfYIpKx3G8C
RY1deS1PHZTBDOF7zyi5ihFFNo9mwQsVgwjK39r70bOqOIGX/hohWqGNhy3+6VIpDx1HWp05yBtB
oWVr8wKX6QZhhu3TnctcnJB3PtpLyT452VBIGjWWL9olf2L+6L3fxaq4pU4cOKlfcnAM14A6vH5H
IRa4ufvZAaPvKZN7EShO7Ah7zcEmkiPzqu0SyUlh5sEvhFrPWUWF0VEmwVM9rHgfUjGQUkS0FXeZ
YfQX+4TXLs3o75Nt7NBAuv1TNwo+HKfpVhYTqWj99P5Wsx+PWf/4Jg6TPlGMQfLz/bl2zOADEWcR
4f/YMORFQSyskdRUsyzlapeU59ku9bRMorcnsJ5RI/SahG570LqHZhoWjE7xzq32wugll5vyDz5v
Wh2OJc04y9jQkxLkgCxPv8QYmIevvxyawCNWSGELTfPUuo2L18pYVHiysoWeuUNM+1gJ+clmRdG6
FY3E8ouLbaUDViM0vDa9JKLvtNkyA7CBsxoxqbp3Tstn+8toCnLs436P/kea6z4PgIJAKjmbXdDb
gk37FmqrXaTjxYRZK7Wy+eoraav6M1HI7+AHJUxRzKcVStxBHHJLudZnbOO1eVramzoBHfe5sm5S
XaLbEosQUsnAHG+RuT02+F3uuI+c/uD7ZgWjwXQatFC0FFYky9Zsj9epBDGylSXfC1NppjGJ3vL9
HSJ+1zvqljFiJt124Ell853O10BWhCk8Spe0MC+wr5uK66eU2JJC3/SgTYfd+yNPgFVd3Dr0qbmc
lbWJZqVjxHxq6Se7cpzGcjZj6XjFbB4VDYccKzK7xZLiYLOJGo2MzZeF8hr/L6LWwTv/uY7uBzxa
/FhzN/IK9YWp59dwnVGbeTNPSEx20THUVVQEnNc3hBZanmQ1Q1oDrXBiNLS6pIKH8Ep6KLG5kQlW
NrZSvHuOZQdRMYYJL8+MN5bwjKBoLbrPAtfrgz0y25nNumS+NwLBYENHJY+irTrUg3N7SNhHnlVp
O5SnW2xY6df0wGb4N1XGKA5ohypo0Wx5JVOvo8+7xBa+4y0CmOAg0xODg9h09zGLYc/xoNU+A6Am
f+ZU8PdCp2kIKqgSIFgZoTZTUiyXGkAqtFvp7IyaB3QauOqDDlzxEF2Olzawo4mMV7iOBNdoUxW0
ulvtxK4q3rw1ieRrHB2IHfKapa3mk4D7kJKPRhEg3PT28Co5nq+VZpBe8Bz7Ez0zSEdq/Vow+ZuU
Ah/rOMXS3y/X0z5rhhf7UAR8xqO1bAMHUTAfClpXNuS537GLRkm5M4asRoXT6xXRH02631awXl/+
B/OwxxM4gT/Gfc13fXAm+nR2UQozwSs/e0bCRTKlZfzFIVT1rkgRBzfuoIwrW33fRSzKXMQtucM2
ckTXILAbl7hDtBie1q0Oi7g7gil8emICFcEoMRx88t3Ib9JhQXvMhMfovg0BKQ3K3/6HjeqZfeyU
HfGkWd8otOJQG6Ai6kRVgYvTYZph7nI9Sai5EfcaKiK73M2PI3AjlM6VHVLimC1bXDrN3Dv+z9tO
UgYLo28hw3b0OzlGzv3RpVKTVMSP3pScALMLTlXZ4v2HdYSKJ3c3NwpK5LeLnchxvcnJKQ0F1+tq
ky1zd17agvCLmR3BUSVLB/+hwcb4DXAhM3Kup9SeVLqAUO06B3MOmQxQLENWXWKkHzVpidPI4kIB
5SmDfgJF+3FizWsPbAmsI+Db4QCrc+FMS7X+meQsDHX96NW1va86GsOyiuEP2H8GQoZR/+5D0zKg
3/xElQo5eU5fmNTpucNOzOAjps6ygCe0BxamfubcbA3wa+qHVqHgIzusJfbyTYOAblVmeXuPWpKb
acj3x0j+S8N/wXLIX8G+FogLaAiLviq+Oz6obzpwJrUQE8Dc6K7hcFb9pdubj1jdP1cMFLi6YYhi
oVQru0Mg9qdLRqszGsbPoQUbusFucOAMq8/REBmcZC1Yl9DzfMp8P1hLlOwZ/FSztUiSpnCbuXyJ
8YqoSRJ9cZzs8QSIjrN0qKOCzOe1jMB6z3d4ph0OaOS4vhCAzMh4m9AG2UmHskPf5yLSccx7i5vG
cCSVsNZZ6NF0c8msi6qsbfrNvKjABirmYdpLChXW8L2Aim1SogC0Pt2wHHxvgsftPObr2WTTW2+v
RDoFoKFRgv48ia5HR3MyyidZVA+aWPmVNpMkg+OSjeTAOWAeKj9ETDSZiTuv0ONl+H4bGyZEntNU
wS6aI6S6bCf8EUu5pkE1dOq35zdYyFJLx06mVznuMzb/gkLcXLPfvVMNOfTAyCinjw/wDS72YOui
bsKzS8JtxcaJp12m3mKTR6FjBsM31cdKaUByo+sdD9T9mIlOlJ1jsGAspSxJSx69z+JFA3HAWZG0
221H+837XllRnXgC83EjSkXm3fZXmmrgUsc1fIaLlh8mDWcdOxvaIRrxXX0VuB1xvqil+2ufJN2W
Vw0Oh2JsY9FakuYPZjJBBYdsPNVFPITeA8c6o0FL22molz3fNEcP2+8Fm3uzHbJdxowzD7Sv3ntk
F7/p7mHiiXMV+cCMZOred3VKAYSzG4JejoQhiXgvekiArBEccOjQ55QOCndGWAym2z5qKR/X07gM
3s1lurtmEUmgbLrbV1guZKSoB5MSa8Xi0S0AM7INWfc1IfuckRd8ZZI5QPZ3U5UA6JNFntXj1L90
yyc9B8qTBAqTMt0W83CZjQyJH86IwNrpxdrB+gkDLUSC8PhUHganS2zhGC2aHBRcxuAV4RK7GKXJ
Xqvclbi4ttHSNrTLWiPpGe2DEMsJ8ueqxyHT3WwfKkLuggahBGjZJWZragne0pmGwD2y+Zx0R0W9
c3yYePuU2o54e2zz0VQqPTY9RrZi/moNBmJN4Byg+3CBoJNWq2rm99tpc4K+6edXtMONJ4H+TnV2
c/39cRIAB+2vK97ZPZleygqFofgIcx1I/w6MReLQzcSGXafrlRg/79f225mvjGsYQWs8KrD/B6sU
P8GXKRFaJ9CCG1LN9BOC7ZmXWoxzrcjouGB9Rc8RTNaL6ZoFe8u5AqsUFmgFZMwYVQRtKYIE9yK9
gotvxymZu8y2G198HE+fYu5bVm3a8ngMOWg588G36CQ4GIYOx2Fjfq3rRxRVhkkIzX2oj1sobg84
wcnN6EyFBUqpQ15tIwm/0URmrgMugoVxkxYo9iwRi+u9Xy2U63+iH63P3iea2TpE6FkD80FIAL5M
BKGrXWv4NEGGSuOgcJofA/4FO3J3NsEEBdGd4InNBwejeezxiFEriyIG/8kPOa3FL3QotvkCNzg/
iAaErwKXuqKp61b4kOC1AgAgM93FtLyG+mMDJeZYXgZD/DrEaXYT3TXoEWGpm8ivaktWB4/puChl
btZPAEuQtg9IBTVODxFSV/tPfa44YANyFLsCg3ct/8DtSP8k3I5+qbZbRXJihkHvM73x+WG4z+qI
9TgQNxMm8ov/3KXNKrMWCp7WpN3wTP7vrqWjkU2EIahspYWMAOhKr1umUs54aRE2tPQWBCxBdan/
gGXMo43aLnOSASdzonPbukxvIV/W4rmZa87v+R7cBzYYa8YQJjeu+wh6mcSD/YqTr86ND6h8A68R
8ZBcAFLvWQ//odYa9uA9c9UFk5MU5D2yRKqf8ghkfMyV4piSTWa/RvmwbHeW3XkON+C1xlqa0GEP
c9B3v7DJWxvBxFvCX8THUSt+jLpikk6ZNxhkQSmHZfgxPEos0jV/8yMoZeWyo5vjlmi0uq/tGeWZ
/z/uyWYKP51Iv+nUY0jt8u0DWXq2N2ggMJeOnkj3d7yCv+NIk6EBk5iMWXpJSjOng7CAO/2dOOey
quUCTRoKOeXEiquvRHYYW3SOGzi+Gb+GWNEhRixJytC7PxbOEtKruvBMvd9FTwCKkpzJgUxq1Qwi
rDl/ffLk0THRp9E4RCvX1TSwU8c1MHf4hfV9kdd34qNQWMyGCUgtKi+S8dYhFvISwDaDavzZS6D+
77hUgDufR1MMZVsom0GewVCB+GPCbbsXoe3bUc9jj4y37wadQeA4Q6T22sFnZVOqKucF4LViMvJJ
AdSwm4dcDqA3GaYyTvD7LnoHp06gs+1YPxjpBliVr1pX+RwXifyRx0XHAWuLvxT7zyze7OaHr1fB
ZNE9jLfHe0JY74b7G3mJqehZoP4XIvv1ogBoKWJS45zg281hRymBsdLtWbygbZxKlqIgl6qYILPu
8eWwjQSjufuHrg2q8S21UYIelf3rLLZYGYCirZ1I05K4mqstx6XYr6CgGmx8ixfOiOw5mecHxhQd
7MdEPHICbewvdjiDbJY6J41o3mP8RWJhCny1pxX/NBmCpz0roglCHyS3snamXNCGNKyf6qDtTv4I
6yPfXKy5DNWIyizq5kSl33QrJjMLjWDT3DFeFaCVQCTrg4XaEM1ZW2w4hI7KsnDTU0/tcmq6UQ4l
z+2GwNKvVZr6tvFVg3JLK6ygWSSNgOYw729DCk8lXcXEtNlnZ98gMY3a/WxI2fj8PbpfSrWXBN8Q
HyiPXhqj+9zobrxtIrzf4uknjO7DZ+EIWhvHsiu9rdKfFAjF5oXYH+WnrLw4B5a5UAN+FcYfebas
zoBTKMOkBJias1T5X9YmrSse6T91hH3ZH0sl5ld8uPon9DsLLEfw0RG9XnOohxYOb3wejiwI+mtm
IV6707BPcR8exhXWIqD8cZWJ3KvgE0bu6sQltbK18xeYwrKIVLzyGtEVnLW0jrDMSewqurP0Ddpd
gBAVj3f6WKdE/9CPVrJgb0XICRfv8cDQW2Xn2FKn9Wbbx/3+z8a7pPH390j0CqjFo8qTlWJHdHmD
me5duTUuiGMLBRAC8Nrm0DwBc0vFQZunJWDrNJoFSwrmANGj3OPbmv27uCCrksv1oCQ4rhEbvNkg
XA8nVmTfuYZF0VNpDTzvDjrwkAuY160YBuwHq2LYVuj6vUct9CsPugb+XrKAJFphL2cBtQT0GEoV
FVFI8KKD7Aafqp1WFilJ7+b2hZD8c1TkGTfB0ptWngxfDFx8/wP4eBjoOBA5loeBrdiDGU2mCeSc
0KrqSEPBELnj4hRZ3agQbXdIyo4DtIl/fvVDHaFDa18M9d7tcjBAZmjBZMynqIutM9L9pUp3y5Vk
sAyMaiUChXfbanBe8WiwP24JFs4bwGXyqu2hrGT6mPrc4fka90I7V2lkEOFsQwsUjqBUwarBlMj+
JLzIIZ9SPbNrX2Guyve/S8EG7iGYqvDwSf5Iw8DOyaN2aud0NKaWpero0lK1EyYFCUIhmEYGpTfN
5N0o8/ov3gKiWqUBz70CKt30jwsMeqtUJJy+1kESFmPzJTGl6rWny70/1wc3GC5ZjjEfcmdr38gy
m+CVhYLjyqQprZ4xjClSfINd6GCCc8w9iYTPAHpnw9nJ9Kn8Dyh9uwR6TDQmrkXpGHgjuu+4aILM
FKn/Zgr5h7YGYYRgVnEIDXqfgnrl+7Th61KcW8I/0odP73TLF9Bd43/lrso/Fx3/ILMQnYHnEpuo
PuyjX9JzGN22oNect02FYregymyrPlWSPdkip9Qu2HNwzIbt+gGZ7WT7x0wdV/Xzxx6NfCYDHDBe
ZNpyXZ8Z/KMVZxR1+zJGVtYTQhJ/8w/oOvP6S5H/aTzGt/zGZFBCZxhjEpZLPZiKmbseEuPtEUkA
XjILjFKNdHpWsdYi4AxF5hBY63NOmhaqD7vDWMOPa2FM12MqRBFtCAfUwWppBe94vjW58xadiADP
PLbNhSHAw10vW53msmLhaf40xOCan0/5dYdEgBsClG+mD/bKFWXFUZQo5ZhaWre8ldCSysUa2erV
uZb7zW1Xw8ZIDURGUruqa07qgWuTa6yQ4ESmotdAVcQy7ObV1v083DDp9Tezj8cLCZmOR+lbnb2c
HP01fVLFNe24I2FNOt8JRpAQDrXAv/q5H82AnRFmIZuau5/V5mSLfNdMBIlUlX7vsFw7hJzi9VXs
y5kkpz4Eazx6/oUVGybBitAi17jz/A8KQst9+96FMhxQqT6oO4oGOSiIoji78nDBAZYPubRzJrDk
1hptDwimMhGYW3FoVr4lD+yiOPbiB5SUGgO3/XqCyR+fXnu9dG8TgCLzztEfjAhYnOuP3/ZrjjDm
ds1sXLYJL3ZVbSsyIjztvP65Xahyc6KMtQJ5xzwuLd2MYbaQVNDZ79bPYPZSGE2AWggZyzNjWl4p
IItmFBxlwTsFm4u7r+g2fxrIgaqik006Q/kAjGyyJlW63vU0/xjb459wqCzPGOx9AiTWvsawmmtd
L4PnwdFQx/fHrPCYXEp6N8+0FP3Y+lBrd2jv4GlyeYcKW1TsYXRHxok8FP1wAI8Rs++laxhUgyef
P79Q2Qd2Upnn+txeF7ZtL9ogJZ2/TELZ/iHdMdjWC6TvNuHjIKm6qFfhlXqdIVktB1Pfd/xn0BLz
SPeN/bSkXXl7HlNgZPOxqX283SRRazdrFbOR5E01PWw6SVAwCPaYAcu/Q04bbF5CljyfY+8VNcQh
z88Gcmsbld0SXXJoVIepZQEvPv32/cNwt9m90Hr7AQoE5LYRVVgRdN+VCyIngyMfieMh3nh4Gl3q
K9nq09QtY49ZHlSFhh0j/RGUdHbgmimLAG7BJDvEMST6hhogKaZICDUaUa9mLnruITgYGP/isWWm
Sb3OzXbqYH3R5vur0no9zANgFwwwBGQZNQ+YwOITso7ddzTn+4HMUHu7c+r5s+TP6UFRcsTRw8u0
oQBhG+4mmCkQh7UmQhRNinZIlMqN9lkRUSlkiPG9MIi7gR1lJVrzkcgGJfhqAfkFtM0knudGNIpX
xs2QY1/rgsf+xvE34352gYrVwfRRiaYzYtppWc7TPf4Huefp49cWoKtgGvxU0nH+9G/ZTYzNjQM3
FWXT8YXQ8YkwC4XaSio4VzHb4zb00zFAaYVd+aPD436ZzViDzNxb/e97uBduiJkjhsCFrPJYzJpb
G0CUbFzMNPONkKeYwErxfF00HPFGy+UJKQWlyEm6jliywe96o4iWxdov9TAWWDxHxSL7quXinSKN
d9Z4WgMc9NlztHNzUWbfMH18f69JR0gntNRDCRKIvnr8i6uEOc9vNoR0BbBOSFY+bR6nfxBEBxVL
k9yW7uQ4dncCs6+ujdDtZO+uPpFMXdcfjkUb4lvrslzXXfHsJy+vUy9nltDYRnnwh/qYIcmIt8ng
/6sECPcu+XKp+U9Ylv9UUvtMWSMRoUZWdYHJg9eaiRlbBUvRPXHsRATrhAP+rdqhE7umO9qNU0PS
ir2WTqROXPKVAAqIeek8qBHce6h05BtA1ykbBWm6yi5qD9nffAENy6GjeQhQvAaCiN26xD1p7nIU
doBa9qmWtYLLieWFG5ptKIhM6DtvQCz9qir+kaiA4kS0hEyN4NBzsMeiAjqJbnA/jggptSVfpMYY
HbNvBrWC0YRqCrZFoDpiLtDAip7RGQijpczqqlOISvgkOztd+UdnliMKbKo5EVG4CPib26g+4pNZ
EG4FUTF3tCOwHXwxDEiajMO2/5YStSSqZHl+yS5B6dS7GZsKcyPzh7bPDNUZRVHoh/HyDCTBCJ/n
whfAU1FbLttlPZTyEl5MvjfosBxFf6dHfjM+8Jcf6mtkyhbAcKO12+hNpO/NMNE1CQGUkIScFDQ4
OSWQ+go0sUMP2yoJxQ+o555YjfA2buIpwliczjpTRYEptmQPdRMeN+hamNhHhXtdgZJoAxjHg/qa
/JVyoFMnLiECGHGyXiwR0QtbUnNL/IGmNB42nfJmGbxyowsZLSLFjRCNDYgtN4iMVt6Ft/o3JTBP
gbuBoKh0/8BTqnoLN99EO2ubSOymw4Kcy6CKydUWnzjSwPcNp+Bh0mRygkOy8ohkTp4HK+md1gSf
OH18wYnEyVZ0o1tVOXIWtGytoSRx02RqHGidfNfGNSD3NCGDLHxCmFd58BNHtlIHLe81GGHIRz+1
QrlxWJUpb5W0YHCBoQl3AUUNWC4nuq3uuhdkyM+ChnboN/j6JIKxfvbTbzTvti5pzKnR6YagE9U3
2C6qi9IeoRq04qlj8ENmjEgESqRqH5FHmrI4QWVSl7KR7uzwzNQPxf0NiQ27IgLYwcFidbqALvBZ
O0gxJY0n1nEjgrEFFVVzX2+3eqo31GfF+i8PSWAifkjQ5XTdV//dlE05yv0ivpASOcp/4HFOeYK/
m6y1o4ujGI4IN0XHe7SGU4FGLdSA2Y+ZSGBgbOqxXeB09ojsAhTnLk2ew1emvhKAjpnOVA/U58L4
O5QOZwY1ogHWanqO9WfPbVDEfvJaQcNjyyU3cKaP770pzK/2ewMpCGcUqHmr02DhAOcuzcm2y0R3
TC+uLRKpNrVFmaGBzmQ5AkK5DSeHcJOVNVmgc8ldVQLNGrKFbDBAG/aUG6WJxrFY2H/CVyewJ5Fg
SBOz342p5j2ojbhW2vhXR4wUrE7qyVOohS0x2O3rD+1Z06W36NkVtVmHSrxCehGTr4gEyTeHFmxX
24t/+qnPPVbHnEtLa5B15nP45GAeTh5wzCmOyi378456NgHS2+2HOLY9n8/t9yZFbXjCX4cRHMfS
QpNcB4m7mpUoJRXkbGppL/ItwVZcL1peStUlfsBQzXmBmgYupUqUXdilmGlOJj+s8UN2lWYAQlin
ej5oUW62Dwx6keI4Cel6z9ohrLjpXKtktHGUojHH3mopC0f0oxurpzuDgdnjkEHAox5xpbQ0uLPQ
+f6ZZSxFI4Q+mUP0vcACFxQVcanA+V7JERk85+0CtWNOfm8q1tyqrzye/rDp0bZQX1Ax0FXoPbww
nV8Emb2e2wjcRMgPbiiFGToak939bVPP18Coveb3uLmy5A+Zx+9WIl0sAc6z0dqxUMDHbruzShsO
hltBpNa48PUgd+ifdrj2VHPXgo2pwKo8lAPj30fObYUZc+hWXlevOv6226O3ESnQi2wK3T9wUZVU
d4kvs+YkJIbI37fFVxFi1GuArdPkwJVJSFyVV8pt3MaXWZxTRUDqeYGMql7XOK2aTntvUW+sGaWw
T5suuR0oGoN3amWPOgytJjPv5IRSBICVsYYW/98bGxJNk+HnbfZw5WYVJDnLlDAgdNWK/MGMyFTG
VOYXI/WntFPi/Wj6ed/AYKibIK4b5yFY/wareju6B0vm6DbHPRRkWUd+sjX0KDgKPc/BVcwZxMR2
HABvoTyFWVCAyT2Jz4VsXabQGu5EEH0pOYDJv/UYW93QGYw1BVvCO5phmatOk71GTrSG8p3/IPNq
MwvbSqLc0qENtd4IRlr7IIEGywOnNtWs7m2sn3nhOFn2kxOzn95zHoJYnWEEq/4LrLiKNLdXyMkq
cdybUP48/z3ppfOJb8oz6K5YTEB3OZsw7LIkByJSOsMNICHQK/bjjRKk/MucOLyzSSaV1X3l3sUM
MDGjJZt4HAuHOAb6m7KIVM1SulzIFG+iNOm2QStNvhqddR0YWrype4uk49/74IgRxfYp2AZKI47x
SFcLJS4ytPz/ZlQEP0F9uICp/dHsibR7bR2XPY3bq/o8tXjK+gAzSW1IxIjMe8ejUc84Jj/TphRA
uylT0h1zjxqEqko0hy3ssQf0W1BivBjxMMaxZpBo5ZJnOb360UhWcr/8bXnx9BxSyAHKTjIxMvZX
pQc9FhQ0KETisVxHVvPkVbGMDQL7HnbBo1IFxL8yTnHadH9BLJkgr6RsVs09AV99I6rsaGVRgT/g
X6nMiJGdLuGVs24gonM4fV7YKCce7omLznkiCIxYMwH2wzl6m9MMhw752rummaWAPVnXu31erPkI
ZeJOJAKHY63DRN7E6UdX3knAxkWtZlSKGoGcOL+xpWiupfKwMw4eoXQR9wyNOj4E4hkRZN9E+OcN
CoZqhXBf0xzX7N1RCen8Wu/9aX+1joXHvJz/SVum+2DVZWcsL//eTccXWXOIucIlFn7WVzcmqOqL
L/A9nXSHjMABzZainETQN+xafn3ZJADxvU+TeeWhEWT7s0R80mzhEK+Rqrs2EK5dDoyUlVICR2CZ
M+JC300Xp6tjwYNlfRKlcrb9/la0uK8/1mgcejGPz+4ACP+ImH/i8VLWneA8FCR177zjO66tJd7g
l1wi8mQ5fFI+lROK2hM0wsK1oxIRZZLn2hpfvl6ndC5m4ZW5wXrOW58NK5GCs/+iwHs2HkexiHeS
C0MSszWjletTrmiFN3C7KgQyfHuZ6PtUFseQb+UM6gpPL2hovwWvW/MsTMUPpC4GwJ7oTCirdb9y
LbJRrHbzkZ0UQEMKu8p3uwes2zsoLHKPM8+jkPu5fMOtFHlo9ailuk2+EbwkNeOyKr0IHAYdiOTT
bbpAhAke20qukFMv5x9gPQZ5inAHkvJJHnkDMvV4AYQIVBn/cgYSp3Anu3zqLynGjuSeXN3jONcp
WoZm7ft5egblzhEClZNjwqe9BTq7F0ZmLppRefB9WtqHHnisMzAXK69JxYmECaRQiiKs8vX6Ws7g
iHTAQ1Sfa6B49URpev9fQhz3UdfyQKYuptCk3EAk1aJSqahVYEvAZmi/5U25mVfpp84EB1o7+3IG
zTnZV48Hwz/REGfPplbdbJjIIX0fNoUOWqlGbL6kcbvot9n6wCa+cACfGxRp6BAXKYrNA9a21Cmd
j0L9pgirm2Fx3/eEQ3XI/8KqnvPe3o8WXmwT6gdp50qma/MTCZZlKWDe7SpC9g4iU9hvInfmL8ri
+cFn7J8JvHvxU6zmzQMdSi8e2dbzaNkRuwLFwFXdpr95a/QnqK4WPK0L2I9JtsJY+50PH4SPKEF9
EJx1mtedpldJa9kQQLWg4MNDZPu+IAJu6vax0tgpu0h1xlXvQZB7h9fhljgZYdD0P50f0qymYy2x
kH7WiBkmTngDfK+GDw2ATpOeDaybW16ba5jB9a24i6EDBRVVbub9VPx0H7o3nC3RD20OU/Lbqb1i
cExI7le7zZAclo+c3+XPgDdb0XIFq3DUkrgwijNMbAttc8QSjSKJVQ+S2/TNc9hYfAXV/NTnZrtb
1TSAhSEULfB9Kb4C+14j4tJqo+FelZFckCxDfPXfE041hdp3ujJhhc1uH8aizb6Aj2rUqWq1pGWK
JM3W5pIhpvBiKpE679VSb/pPIaSuhn5TL6XNaPkxXqpFrxFvgfl/19Xw4soKRT1GA6wRkQslV8rf
uetfDrfq8ffrY0pD4tmWatVtbQrIgSHiNiZBWrf4fsKgAdgd2BH2O4VlIallKPjLprVmWKVXnowq
HyMKejtLo1cMDMEMmqtKSyxdVDe8zPoM9fv855E/hP3tnCq+M3KYF1mOmNfzkVly8sPHM4umIe3p
OXpMq9CLV7hhAtQi2OaPedCoof3bfcCN4Jh7AAOvaNAOjqUWX/hMxIV2b3dMSTpJDDlZh3GwpKbF
yxAGX59wUgvtI0Zjw684aJUKRqgo71Sz6gdcEkTM5Pvanpcjex9ZgaxOyLMvGGwVc/wvlMdorF10
uTRsAf0wowcsfhW5ht4AkURRYyiEUVwe/HV6mImD/KRblnXRSeOf2INnt4ynOyUHp9k9y1Fb3jRU
Z8OHRFIAmeOSlJ2zAKXFjXfGGD0WBdrah/uEw1fqKXuLTKheDrDaXIzXK+1ItpgeEgoQQEYF1vh0
l913psjk/6oX6pfAu7bxl9dr2D//K0O9wf2PGQ2YggLcDqzFrp4JFGZLDIMjwlLozO+44gt44qDc
SvK0Subcoy0+giDtZ8kr9BsBU6g+VurnE+b+/MCkZw7srvh+G55eWUtua/Wrm+3x89u+kW0600xn
DB9XiLCd3JTqQfcj9zsZv4ZwHq+PP+az0LqA4MK3eKLTYwYYVP4Ce2hO352EfJ3LkEIy0TMwGhCa
dZRQQBqD+3z5eXgklVqGDNKpigg3X0RmagxlV7LeCxALhm/8fHwritmsY0cvgDAM3TWh1UfD9Peg
mvokOLldOFxoqME4KuR1TvnNYL/odegvAA6T4AaJR7+6rHaanUGZcoVGcKd1FAFVhmRASIfIczh8
DBXP1Fqrz9Zyi81ujol19e/RDlRm8LdE5JjSz0iNsfSGbh4JpEnZ4yrFaGw5pwa2+AGdvkpRUMJx
I2vUpYq8aRfT7yI3ZcbxNVlm5V3GwIfl5zJGkB8f7uPfVo4J7du2EMaIhdofb34Ik7DU2Jm++wwI
k0pDtVph81NzgB79PKhqs88kgs+/X4cUJrQMhVkQoPCcVwORckvV3d/m8IZMpLLemHDNXY2gHerQ
jxGvofWpBd7GpkFoGn6GlLIED+zJ8K0kqt7GryjFPlq1etTza+1ux7lx7zkj/EC322pfB59NmbqE
2lRRIMNCL7ZlmxY7QhlARDz98bDb47XzaMnPWsSBRVRvOtUz8FCktx0jaLw6CWWl5QMD8GL80sRt
pyite5uSFOe4pd3/kF/d8u6dbwW05LZDBMeVR+jNwDSeRN0kdowMH4EnXJfsc15OCiUR/ZobAP8Y
D+1PZiVdjocwUgFz+impgjXB7MRkxF9SE+00nRTurJIwk96QjJOFqgKVKh6Fl6XE/8r4hq8hOMur
ISKPgsDuMMB7HjZY22k4fi673FNU/+Fag3OJeDpC+Z71uV4xnG/pmii7E5LQg9xzEvp25P0nUn5E
zQHKgIAjFFIPqTXaq/rvRlwFRPO8f6PNpebBEVk3lu0TVhjl4Lwj9GtSA/NQuujDSAPHrpmNX5qw
NthtHHtRfnEQNOdrsfOv/mIZe+GVLR27BtaRLgn/7eAfzfUeumQ2A7DT8mCaoSmX8B9lbuk7KyM0
KlawtxiMq+AcnJpH28P7abM23bGHfRqky+GAcTZkGvseRPtSHWPKdPBcy+6mEWFuD8ydGgLsgD0n
rIAigbB2DwBGVjItyj93gIjjOGYlq3+K5rYN4+m6cgbDslXdGJvPLWhLj2kfzSGE+mMR4VXucrns
pwiKaQawlhUr50PqeuRCDhkzbEBf5fd3WXp1KWxYDOxkrtLKbI2oaN8B77jS6QQhgrljj0jdQ3gp
xoQVHjfYQW8D28YTf4XmS98HCDCUKCmNepliweKKTc6cV0lfSYmZ3K3RN3Dt2NRwpZBydnKg83T9
ACkMQmU+oLdvMgPkcnuWpTMEJaan3fNg9N+1lSmQE7yZt8gCFvZXBM/xgG5/djiuhPX9GQ9YjOAY
oGC9rP/sCCU/7Kj0EQzXuBsBce3IbmOLeWJLJ+k35BwGkaZ5oEjg6u9M3vQbD4dIXtB3J+1q4mAz
Ip/pHZQnqXZNYBhbt35GLIvbZerIryCLrTqQxIAZffukS5P2sh1pOHX36vKwachSOlMNjL8E7Rjq
LFieHuKV+S1wzVPgSSxxCSQ9qc6fGLuBJuaflci1x9/oI1qzMZfs5g5Z7kYy1aHQt2gmxSkKbJKk
/9Lf2hEP5Ecv83tcWhL3hMinv+ppXyK/xgwyPKhKfKPb1nmo3HGdjZtiHqLmJ0s7h7Vys7ciRS9j
qb4lb/GCLlDwYvWG5I2QDrWWvwk9v9TuWBTe+V4GVdARdgwwgtgtoLZc2qyHr/FVu6foEmQ9f5JB
0uuuuScU+kamIEd1qQyWPDnj+I/+/ScU3ipls4FGVQHs/WHZAx6mthSZ8cLfmeUeJD4PH/VG+w9a
ju8+4FLZMEf7a33MaEFPDk0HTnagcwYET3lZ6Xz4T0Se+aNbc0BqdGFGHY0uD9PsVe2Vh855NcZW
mJgBu/EwFKPADPEIJIubGkqjB9YoPGpicH6viLCtJM4vVRRMdGEAWw/5f9qImV9G30sAy57oh6go
l/SKw7lBiYCeKLb2LepMOG/UjrKmVPPAaQgZGUBO8xLFlvoyfZytadNWe8kmkwGtz0dfrrCP0J/C
y284SLeet8TU0LmQXrZR+FVEU7TDusffs+kpU2fTObS9eqTa5+Z05HBUluSscfv+/c5sAP6y7EYa
eklraEXGLBubBeu6EU3/C7g9bvoBPFn3EDOX57erZkKeG4ausapZsMKSlG/5KOjYePVxjXFFDap3
tCOcCELk5nS7QUxswwViStBRkFRyVM9c3TQ/bq2pRF161QvZYIwtIYhrAmDBLqNzcGW0rAHGYo8d
PZe/LlrULXSqMPWZHHB3gp9yAHlQ5QoTCoifCc5CQ3Rgt3+mPjz/N7Yn9WD/pM00RfZtd5Zk4ZQ+
8KbqhJ1Ol9bELwyU94skk9do+cObAknX3MsomoOGATSztHpzhmS3csr6RoVsgbmemFyiGEs4BEtp
2dV3Qjfht3z3fOM2GNvSTBytgJw6yXVlI40MeKQY6DLzKgwAPrE7qWeIsVoH0+yE/19OrraqkdRv
EY5AonN/eQp7gCbH4KgGfom1iWyoI+989aOIygJu/PmkSp3xXbybolHv3WkquXo1ZSZabveIUMNW
2CHAY+9+N4EA4ETiKoTo5ktYNo+/RB6Bf413fKRVfKe54XHCNCegUb7pAXSOCFmmWoYi+HcKelTI
/x7ym4THNtrCC7VSEWSgHV+RmLcEq9rmVkLBQGD6N+L0/0joiH716yzbUPCqTu6OrJj/LaBK7ros
hZsGGTI8KTPtw4fkUvp3EXWEWUpwQkAA9uk9EChkyyx0EwkbzSjuHLI3wkb1EhKrtqgXoAu9wuq1
i08OnsMICihliAkeJEnJJ7ZL9mCfwKV5OuZcnrYGmB0IZuoJpJhShXW8ZrwDiJKA9qITHBZ3dbYu
DCmS5wor76eMVd36DLYkH0Q/mi7qjjMSqOPWhHpDenyy3mq4JYT3R1Gvoewk2tTYmrAaH1ioQXHt
iNqb7wqjUE9YDcQcxdlO5CGyBVUVLPyezddBbNUOBBLyQ1OTY5D6LB13ir1MVwaI8eoCJjz8p/Wv
xPZ+TKVlOAT3m2sQ+HZjx0869+yMaZGhdpeC2feH+gMVfSn3rjfPL6rFprlqPyDBYhC1W80oKvK7
nrXVks2vEpqbN9crEEJVTk4ONq21gPWOJ6kwNeml4ut6RZus8Tny8SSBNOojkqMfgvazRGYIIj0i
kDzZdbuYa9grIUbtHwymoy/uQKPYbOP1q+v+P+SdzkgsErt18gMW5hWFuEL7kBJcGVO0XJ4SPqyP
fO3WgGX10che/EGi66ZkP6h/kBx790e5cK5s/ZhYF21ljKq0gfBgaX+/oGIHqlMyJWLcWDK14Z51
lc9GL0awPSV2ApRBa9tfZ9L4aZLU6wzH9gquQdCErwj5OOyWlKFRV1lLJjWKUzTFanlwl+t9+mA8
tpXD91Zhs/jmLjd1TBhTmFgwCLww3/e1PfOA3j0iFr/628NxZ/a7pzZXZAU3wbddEFzu/Mt3nqhm
ENqJtdayAoiww+P78htqZF+kw4UZTzq9A6MrTUuN8WYd2tloqB8z37Onvv6ngz0GTCg1iWkcTIlJ
PjYyuMNAPABxQA4j3F3+b1cfCZRerpX1NVppwtkAwfnMB92CFSjESBYJNa6XXzmkeBbeI6mQXg4e
K7NGJ2DSvDD2rutk4ZDZILHrh79Lx2L7BW8GkFz1VBN400SE2eU5yeZ4R9DMMNRzehcniv6nJcW0
CgdRpITdua5XIqzN0Y6kMxEGOpX2ro8X1Ic3IEFVZBDO8PaPC2sZdyHrterAryYz58W6n3oCkgsi
MndGWDxNbgQ2OIa4po0XPwbiRJp7/KZ52EKExsuDwirYn7wTafq7w6v/yVA6KkoGgeV+LDmDmX2k
QOoK5PI9QoxNFZtHbDIECI1vdSnK2z0ptE01I+RGnL/OvGJv0m8gmuhL1Q5gauXUVNe8mncI6DYA
2ffs1xDOsFVWoS0pH6ZpIyer4W4TUT2AYbTzv0LAsUbevQIUMw0Y4TwRPofIKLbV7EWbrmN9gLVj
JK+D2ZRNOZdLIPKllfth9R3qdLdy1n5UcR6wSNs8d2MoM6mvLn10oDnj3eGvZrJT7ysIb3pr0IoR
Wm8SHa+n3MbbvILqk+onyr+SS0JHXkJ1C7lgasOV2VGutPtV8aapKEVsUVmQ8V636qnjiZ6LA/A/
464vNXTnIBw4ZkYINFePZY/rI8jBZwK/fx0S/yKMmFZofc+rDwu4nOawnYB2RyVLlvhmbCjuhWi5
9KhMZIHie5ye3lNPJbomT1adJtitn4JJWsYZccCCdiWuI/1RSukFH+egVVv+CFFahx2qt6z68Ci9
hy8Ud5xU74AL2JY2fbP0rt38UhsGWWtqWkMsW9HpLtgqxwUuZV42wlekXvlzy5/NEFpVE3Uiz9Nv
pzXhsKwGuGEMqNZjku+Ekg8Dqqc84sbt8SuWtZAulaUPml/ANc2Uu/NAodnLu5fOWSv/VufA4UWD
qqL+VQkzPeqH7yF95pWl9EdRnBy40n95jRs3yXDLOI3udD9c3qUWTPLqI26qovFsNbSx21DaB1x6
DoivFgaMuTKHFG/YJiCsqPPX4Ki1//B4mRl65mW6zqzAb3PrUqwahwwT7kdyKpiLpZsAiXB94cyB
l4rm9UIqJYS9hV3hcgJVqbuHh/FPdKufHs6OikkjhgtLhfOpTEmJbMRg4hjMlsJmkXv+g6A6uQTg
/WhZqeld0nW4gTO2xQ+VL9Nqe6jVKjocoOxh6kuohajbUSSqOIICXy1tijY1brGppprjNYbni1e9
LHXpgth6Wqu8D1XC2SRxUVVdnTPMseMO8cS99614x//eNAhXFCSBGgKfr2G52miVAPHnfxESj9vu
RTBD8/xcuUSEKwPn0cjsfikR2ggHRFn2+7jXHlXgG5+pryW9GwNahEh17h8y3LXKt7loY7vzOptM
JIGkQKV+EN9d99yBimnA0IfTmW2bFCnmqG+cQG+zeCnyyJKuHXyF/0mnOkMaD+4c0a9p8W1on7N2
syJPm2glYfWwl4C4aIwE42C+jiQOZTLFhKvfOU3dUUeWBrj9yTLVlPcz39UX8MVY/5ZQXF4mxbsl
jeNMDcpuLgluzO7ayShhoytnasSP9CGqL+rZxQRhQjeoRf3jVFFxGgA1sggBoQLYp4geYdlGM3ts
3/GjpezlU+Ui4OgTnUWSE3W/dgA+tak9KWuceXGkz5yFJH+QAkpVasnJvPi3KGhgUHmgmvq27CE4
3e2zp5uewNx3ue441Y35YB2vjas+MuZTAbtyOGoRhmOFHAoR7+afygJbxjJm3tCNivGg6wLoWCdu
vINpcBUxc+w9tEFL9jl1HoUfYQ8jQX3ZsN4momaTr7+MbI/VKTVnQBBPw3YlERouLTVcNIZvGWtB
u/++nZbZnljbAm4xr0sjNIHzVD+pDEDgW5WVpzt3oGE8Kaj0rapZ/NkO/4g1TkziGL/PUU1n37wR
J1q5gUcEKhA2k9EGfDqOp3E2wptZnDiDlb0EurDCg4tVkZyHgAKPCNWDK+mmzcwMS1fyV3NCsmBf
kVrH5oblBFz+BZtXfh9VjM/DtG3OYSf+QFQHOFqmZ2kRMLT0FqNWlf6+XbcY5nWRNB64MBGpdBYs
5uoEir543uMAu80EbAgJ148ca62mMjyVkE2nLK/J1GI19+ztn0cOWeCDlvn/yKnWwYN1/2q67xdj
7l0cSqgp9Oi36jU/pq8Tf54ms8p2X8+5QoAKmww1ALFQfZZuUR2prs4z1LHPid4OGwxQ3nQ5Rh/4
29oIbs6NdhvfKOoF1YC7vZ9DDAUQ3/8B/hN3tclvTJXrfaSO0/slOge90tUFDh9C3grzFnah3BPT
TLwUynzHEPH6gw2yNizt5ipYIAXjOme8pM00tbHzpnlInQF9Er0Be5bb3hDpmnEtcoWqsdfg8x8w
OZQVcndsKSuXn2UL+sXMb8WSgBLW8lfVoSTNYX3nBeD0I9Qc2l/RBA8mvX+fOEKgoCG1c68hqWGY
edz0liK9fxdOKSOyLQktq6R/Zo3JRb03TrIKSceKa620zQXw9gc+7vdbpQn6P9NAAKH8qxAjenif
m7UyhmSmJwXGYrOBPtJ5+D4cZgdJ17ABak5PODwrUkkUGUnpoZ8veSb8ivaUjG5PpY7kG0DW3XZ5
cPyzBIEmMvSrhwceddEyTrSQ/Tnvnkm1/fnv3541G1qCEmSd4oY27uSL1ZXfoNfTG7ULNhyrjUaG
JMdNe1MImciKAsEqrEb5YyWc60KJm1gqoKDtXFOOf37NrxxrojKVFaRvwB51WofVZYT14aVPBJoS
B4xbwb7Ww8vGNnOTyZ5gd14fkfcvuTgTShrW66JIjduBrNVNgjJEgWxt1+pcNI2ETHMuSjj3Lbtp
K34Vhga+gXbMx8GDKnfryU5Wlf35GBydiEYQ5rDyJvLXq1S4n3idHG+CyyC6dZEvQfw6g22OJp2M
EIIquS5hSHmTvte3dxOZt4Pi325XP/UD2yGYXY8Ga6p57vQR179HtHESGNmj8epHHFdXUBiTUH2k
yKn5l9DT4WqHAGjPQ71Qm/dPPFQZtLD69GYo/9cjqnS9H9pAdd3d3XeUEDQjECQHqb1+iis4s9dF
vPqgmdJ/9+IPYcREKi3FWN1fR5/ILmux2pLKKATpQNCGBmh5L1H3UUlg8PtP0p4tTzMEW8JPK4/3
PeFA1mG971CGLepYIPmk0m+w7MPN5TwEeiEJNKRgk16ua5XkGj6AjcakNB9RO+3o4Fg3V5GHb3w5
X54O/a4NeKLLGBTqSmvOrdzlN0efCGSP31KOd5JTiKJp9+BibuvrkhUTu8JbEh7/+uKZSVVQEiHP
MhF4AU0MWqnwXfAMVjI54fyCP27RdnvlhQTY/5/lR+Ffxn1mkkbkGVJlt3abwz40jWhtK0IHWtdn
/xxR0FfbobfC3flVvLyGI9FGkskITxz/zSKx6jtpeICW/yTnuGVgUTIzbqHk4ydaIDUgIRb5U3Mh
I7cIibq0qsCwVwK8/Zn5d4TUeeGI2xWXJ/kk9WKz8E7NzQf3VISubWNn2xMPAUIO26E9QGMBIh87
MNrsyIj3XTOfSP/B2AGQBHVI5NzN989Y6/VQBovf0gFVxEq00st4ii/ZC3ywB+S3OMmqgBF0tk1v
rK9s/Ww+tfzHkifVaU11TDuwgZffE/RFZkAsb5gFdVAkeInPbTjxufaWI6gJgazalz08mutEzB92
1WXaPDnvvM3XIXygRjdKy4KvtZjKTZy6qeLunLX5+88+cFv6jJEmbaDeiB+vxVre9DC802CHr8fj
OHONyIwVfP6LmiuwrC71t1TePuRDJtQXLkekPDzRYXZjKpDNOB+qkmevLtgVpb+/A19/bl9hHkvK
tX9csPpJhbCI7Eh/jWZT1yr4jhZ2RPySOSjJq+k+vp+5subg35sYBePgukUeL51W2dkKvU7yH81+
+NT7sPug22fuyxTFH4e/DsSL3h7LKhA3k7loY6wCq/t4i30RPFNh45yKkyeX2GEk4Z4kWrkK3S7M
kK5iRxCXV+/49miBJGW2d2T2TR+4+id3FEenDs15UHB5T0ZVb2jY8NXEjcyBFg37PTwu3E55th4p
acAyk8vJEUnlR9BTIPrWTUMgdo0/SJpKkxqXWwa2ypemwsq/4E1kD/QPO92qVY51IdMpb3EdoPIl
lWSCazb1mNvyrEBH4OdrCZnNfaOwkYX2jqQbFOwrpobRQJf4ALWxPri3CfHGx7piITJmc+gjKOOn
60g9QJcqyxrs6yCfkmpaXhV7eeflSIfaAl5VxWfdDi+pmNxWrrE/aB6FNiLROHYmNwOhhS/V34Ro
2Vc+HejKqzjKGUfGjLncvB6BxGPjzpDAa5bf9QDUC+p6wr9WqhwlwgS9YDReCATzA6REeXR6EZp1
nnDrr9IM3UP/LAt2ghhkKc+KG59UlZVU8crBvqUGqoqgVNPyYNSHqAH1rVNASAfxWWr0SonOXezR
E13z3SBUNuY8GuIFV/u2UdVL+M38za+daMAldMmCXpCV51MYeTkewMIlbcGFq4pTohu/P2l1okEo
5FYSA7rb+RyivyrzaAYeTJWpmfX6EX59OG1PBdupf8DzyzDRTMtKmGaIBwNFksi8tLr0kLeO0dTm
NG15CSI+NQEOCMBjBh2K2LzhvpS45zVVuvZHr676l8Opl2V6HrxV8n5lSvk3M56rlmSkDJ/tjFcn
DKjKkoDlWbr3sQeubv7zsRXcQtzngXhPp76mtE3byHaEj7pcRTiWMI1+Z3SXQvmpc9UgWyIQDlRy
cd/4WJ+kMHxFO65zKjr6RGsp9YH6Qzjtk6JNZuF67rTcnSP9AVfA9otJiVwBW0Tt22MKKMmYKEJH
g3gxZyOHdi7n+0oJeQZP7JW5BhBy/Kt4jZT6VxZqEUzFKKFRe9uIy/e3uYyfMCyGjfresAJAaGRa
uG/T9fj33RFthzFYgYWp1VQ9igjARK5p1/3kF/pMuD1oawXfMT5mTYmtKxWZRJK8wWHtdTD5fN3b
2mfajbTSujW4W1qL5vxG/xvFzPOPpRZLbcKtvmge/p6dWxvsd/AsXVLS4fz9t/irX31x40K8Li5I
Vc1zeIuopjap3qf4hSsn5lyiGeqISJhRGOgZmbn2+TJ4O23Sf6QspJK6sBl2bWG91gBchGOU4Lye
vrIN4dzsv8CsMvXq1tnS/bAZTgrtGM6f1vyc9Do6Hv8f3Xa0Q7shntxEi0aYFEmHVoy8YcbcWEZK
lSYnRRXsVpbipw4QlbkcmRtcRgerBvEihbifmAuI292/QCBgac9/135E+yLcWXO4qv9xWzwJp8EV
+ac+IVJ5RC8Gnq9WpAbY/XP9Y1ClqwOnwrKTStsBj8P5yHIka8tL+n38NnVmqYkms7eOxhByqu59
TGDuAWkfqX1Zwu0me2PtK5ESiYO/cwa1njx025IAS2lMhhmdUy9C9UwB+KKy1lziXXAN0pED1+VZ
SGQDfB+MqolHef6w/sH1ZEMLu+n+iW9ZWwYAWIGS0e5b9ZbYg6ysvNJbevBWtdyMWxYF+/5tfVTd
+UXLonxfELSBRn6VIIAY23OQkTZKdTXM98gRChzdc7b0VYU/M7FROrdYtjR8ThmL5O8u5PQ+J4gJ
L24UCvlWClokaRhAwTfXxYbl5Hq8CdHFyUU4Wkku31CGvJ6fpc3meWByy0ufBdEmEC9taoZHm1CH
SdNWAj/Xlqev9ORkxkwD5YO3qElUyHV/GK2RaCDuecCP7GuzBv+ltdcWrt/NRSvN5a93pQPvFqJy
bGrK/WYaE/4boTcVFYgPzGIIuT3KE54gvFN69dXM2lKIiJSQNxiafCVV8Fyv4zXgRNh+J66G7EEF
i4ugT6qiBEQl1itKNM/BA45JeWUMceTAAJ6WXUPqoG8J5LIEEps8P75D6snDXYEDl3pk1tv++L6A
gtoJ1x5/gBvmZiPXSpqPox+NDzOJwIJs1OBG4s0NdFNVt/TtDDBnRpyAfkmAo9M44Y7jqiniOtNv
mkngDsEjxicDzWSNzqoSBVLW58cHGrgb/ZgJZA5GCe+hiYpYAbTduWfffsUA3ZkhHzKpOtMyq5lE
Gjcnv+NhD2ZD0vw7/g0FUN3yD6YQVMAzvwCUMu6mbL9t8mq0OcSYDr9hHHCjOv9PoTUcT87gsZCo
fGLxubQNkc/XTnG69vk7pc9hLIHGcPjafJx27ASnmdxjJ3mJE5fG6jmI08ZlENXQ/aadOxkBHzAw
pvxEyc8mYmKFU9NrgK+57qAVipmm/pOlwPWSgZc1WRYOZQtShGiPwWB6mJp0w3KK3yep0a4ZNEkn
N8TTAG4qaz3Vi2mBEpxfDK0YwKuV9PC/8nr5Lo8v0J6NaSYGWrQQM1N3p8VpMg1Wpj3u+DtvnpnX
+u83e2Uf51PH1hHZx6obbhFKWLdT3FLdEsHt7Aj6jnK76bpZRbqIiUUcaKGBYIqSZbPJAzoYYImN
l5YGDwVkH2Q6HchmqlPTpVN7j+aepfvC9sN6Yy/FkItdHKc8Ig9rifPBBu4zAF8KCq4ja9pO1+nQ
k40fkqqZunENcr8DyUiMnuQH5QVbXQqaEMN4lGavxsfDAt0xRnLsWnmtkCaU5qJkWKr+Ws8hqaQG
vE8wh4UWw5ZotYhl2PopdjbUYo7ty9a/M4i0NvuBk9Z6o7g3H+BSo3zL7TpdeHIK5VmMl0nmHIX6
sNhcDYHuWhn0j6YZM+Urs5g3UcTFQIrOj8QAwuHGsOsIqVPcg9t40rsBzWpawMXuOY09hZQIBy2h
9W+sI5TmDYhNkpwhDPw+hIzYmEvO5JX6OAxXH5RTbijU+75XnlZiKEe2JGQ3GAmu1IcgXP22h30O
qTMB86w1dCG+kBZUiszwwDz30tMZ+XHK/PxFt5JeYaC27znB0tsOhGy1d+n8ubsyQyk3/9li5NUU
z4ksz8snwniSsrNWatX6OAzsdrJfv77ke9IXJm+rNjOLQIEgJ9nTmWVx4X9q0mp7kk5zgw7XuPit
fCM61AOq6PeCppqTKlQanGAf98Drj5PEK3JVQ3gIhzsqoBBZaOoEmY/p8xjglTSrV0pQ21xJlMua
3THl9cRo7K26y0UgKnh6JAo94P8OwbDhvLsCGnoYvwgMNhotNhsvidfq103Zn8My/VoF4bktGaIX
ncOv0ZGxbVRX51f+gs3Bvv8+alW5y5C+8OwIt8owTW3dq0DQNAsjZ5FqNaDqbQj04E8HQrwvLoS6
/mi63dRv+SWQSJ40eM1yaTMSQ9OSNCCJmxaARupVZHAkZAkAG5QADRWA9fq/VPJ2Nl5YkZr2HS7G
fMgoa7keijyoWvNSqoUHoW15Tjh9v1crhR1t89bu1tWOnXc8BnQOD+xbbk2MtbXbcZt3dhafEKO9
XZ+Kfa8MfXrqPE5qUl2tft1CO5Zx92mVX1bfxUAwIGDMtjrQVHCoM5Z3HiXXt2bd4+rhwn6M/MXg
cbnmRZSZNHr2r/HeEww2r4XPFQFo0WPHvsF8Iy/eHKd63UejRn3zcLsc4qMhQKs2vAPBWlvhvMmH
5Yi70PATi69v2nHAKLqWyxB34h5vMl4BcdgRkfMyTeKYZVGb1FYFTDLEQ3TrtHo8mkTnZ+9/dEXd
u9LM8DdKJcgTQTX+ktzCAvKYWlZJ5FoQjkH25H+Sc9eoupOk5h2G+7PHEQEQjSVVPAvyQe1VJil5
bjAPDK4CnqShItpxCSmWUfCf9vU4J53Jr4Vv9qC1EOPjXfHb7a3r/wNsF7tktGgYzdZF1EwYtpJE
gwzR4wLkwsaiZol9AmAj/7KTLK48An+nOOwtKFAjdfXCcnPWJtl7iKhycq7lqpqX4tCTS8p8UQoE
mZtv6ZUrSJiUCfk/gfYoup3QzLxiqFnxIVLOtVMW6BbJ1rZVC7xCSHmlalV+NsrqchjvcGlhK+tM
Dx+XIczHAopaeCc+0kDfuNRc12yfZkzxcblqx/Px8pPdfOrHlKOa8S6NfGsqUm4M0vVfAJYqVg5L
tkD3Y8gQBr4/E9yJQnphH3WgENoiEYBKxtBhTdT62NuMYg1AKyalGu3r/O/CJRLRDRjsOx2/38Zz
rFblBIzteh+KdV/BUN24wK7CHTfvE8fMa7lwt2vYV1VZug52tTjxbff+QadrpXpYplMO6ONPiO2I
jgUBWa2ddvv/YLh3ZEkNfwXoXqpeUvhIS9ZkdrRVl77tFbhN2kf/zpM7FEKyjYYPD+WOZUeQzuT2
+d/bO2HwnfmgtYCgRRG+qYUbVE1B9ljq/Wh1Zx6QfpjtnjOrzn1t5xqQ4qBIp1kq/65w4Nq1oIb2
VHDKm3ctgocTrEOs8JG2tenGkvUm/j+CIRDkYoiailtkiLkdAb04KoS3vp4nNwpoFjKvurA1SD2R
PKxDQbDbinee4kWqk1bOZMATX+N6DLp3A1WFbXMTRb+Aom+qU+Ae4HEye9Nuc7gBQdZATyBdrPJE
+33b5OQlAsdmsKlMIQ0bI1YTFXodfHAHkctqb6lP8A0SKX2Y99Tex0OlLM7brOkEZbbCa1CULbEC
XIOvUqeXyfkAzbxtw928zvCvVNX7mJOeBvJw6H2zr0WFTV0MhtiyQblO5cCW3+Cj1RTPBj86X0XX
zw1eotiksuxHm9XLZOL53dbdhXKH7yK/gwjz78V5iO2rg7/ZfB5D7rxCHe4fdH2cbK5Q/8uh37WG
YyD3FAnp69ajDe9zV3i2xYfd/Dmk7Ucf+bMwqa75v/p62e+S0qFiZUX5AbkIzln95gEn0sg5RJOO
rqX+OnrGGGXqXNj1HAlQ514e7l40myulDNuTpxPxkpzfHEYGdRXJzODggtZ0rN0rBrQ/UA6HcRiX
VcerF/F8ThIDQsaWs1WpjfXriuxYMpmdR0Kc3sU8q1N24a8aP9eHjs94nYcIyQqpjRRD7Knx0Xcz
CKG1kpreRqj7+w5TeTLg4TNxLhN2AVAEVyDJLYXRtMbuMbM4pfUxuJ+XWTmv/E3iPFWjYHXBlWAy
pIatmUa5z2eXr1E85HDwYNtlNVCbcqJHUWMpsrZg9tJJGzSLIlH8YJ1xdhM9jFpK2EX6uQ0HX8RB
d5453T51xYSfx5Dsl6QcdEZEEa1ifKKSTc0ykXEydw/mA2RDFUk8BY6Ip9YyDwg4ZokSzacrn6Go
qbEXF/6quM233Gdp7BTTFvKPShOOmtdEsnMdYm4H3fGOJbZ+dTxwMQbuwGALkS33CDi+zMqbHDNF
ItAnCbpiNSgI9kbFFiN8d9D6wkNYi+PT/MREPIN+zjldaauwttwGjanti+9mthxYFq/YyD8tsp03
kLI3Ci6d5EdtTy29eiJciwdhdzBdD724aDWlKPO5jMFX0badqZIEeA/ENCosqQ+uTfhEaNw6LACk
Xu/EsWC3HulROYwCOTWG4KJG7ulN0P0b9365lCHAF7TjaYU0oZvOWXWxNn2isDtcmfBohGYydT0x
nxliVb9e7CnlBYaoSwZm65HEmuMOYy/rdPxjpcZ8jMYfF+7JHaTP79n8FsO/3GCoQ5hB7NgnTdqU
iuN5l8JMl4/jxbD0WpSdXZuG5faRfeCKWn61l6lI1S/sM3GxHJuzyF+i79T3plp/Tw0mKVqCDcTq
dj0F5k3/rG1mvk4pjRGYCHcSbWSFDQZYOE0Hjjy9nlPOPzt/IHCJBM50Gjpe79aTrBJJT88SdCY/
2N5fBm/4EeMC/j7xvemeqVBt3Z8LO6jhFO7oF1/71Bm4YdiaextvR61xIPyM6TJ0pXeQ4/xhawO1
20KRSIv/lw02e7iPyEq7I9K/5XfArXmBNt4qpYJcDUT5OBUxJyQ64E6JA80JVoe1ojU5W0uPaBz3
AK5nXEe11N8AEtABiZl4hPkZzOrG0hqcNw7qx15UQ54up9tzXgGiQBvT8RWIzP44efrtGet90Ks2
TvixT5t74sQYqL5kqQoxj6MLSVwX+q0jfw+EVb+/byLR/Vo9s/UAPYH1SqUBiVfViIPMSaIeKJh2
gkVsHj5AiIeKWNsKPgMOYZtM74DwKbL7W35wXhtVGeM+kETwJl/Hrs66DyYWuHNdzD2NBsCoqczM
8ux2jH2tixyvqaVfL83DJfRQMymV/d8S5sSa24ftELMjgiky3lt3YGU7YXK8xkiaIUX0aipD1EpS
0m2hJ8Mqm3RZbYVXVXzqYOIizfWmhFaS8vp8NeI/hHbDw2NmORe/LqjNMCbfMMS10AG+Eai+EHpY
W3YZXZzPModhPhvqlShjFtYpfvd0edHtbNp2O/QdbZnpDheiDW2eqVHX3XEOVlmMYj5L/9Fsw0hu
G2BI9e4V6Xx0ScM08CKdI1BOraqEqmoh/BwLwn+Gfnq8cBwxeKsf8XJTdx5MbU+s3CsPVC+lHSj+
rcbk/IJHiFKU2Dwu+HdjSmvvcLnT14zMV5D9gZPkciiWUwQluObRkN2fzmHX/j4UGKalAqD7VNI+
rUJsD6IVQNyQrZACabaOFhAcBaxqhexJZzAnFeZlOb0OYi4RsyTrqmpL0WXKLIRBs9vjH6JUJxu0
PB92jyAbe38Y4v62Qp3dAnbOk4DZKG1/ACCtiOuTkZj7GlmhyAJOyWZuGULRU6P97NEt5qse6VfH
WqYumuII7dpy5KO3dKXC4nVuQVPxvMwhzR78V8qNzp6kd3qzIHlVQSjYiTZX+q2uDt4KTxgx83ng
LKz1pLyRpWttlmp28dRcUblb69w0OzE10IlWqi6sqidA77sjFJaZNnx3hDRWuGY/NMEZGofvCZSm
AiEPx42yVnLr3VsvcIF6oOahsX5q++dqjoQGb1aX/1XLcuGNbpQgYYeUP8fQdZ9bdgEEe0FGE6Oq
C7HCRNvNzAMbHkW3qwqwI8rXUgSgswpJlEddWTjglwaF8TcY34kH8HqwvZmDkNfygNZKpugfFzcR
qMbyGSMcibIpYOGxKOGZWEUTtNo2as9VvlQKJYl9pu2KFmxBd2SQWVVL3fzxaSpCPIXG4RKZKmRG
jg5RurWfBJ1WVSaGo1ShM/I8G6p/saznItBB6QGqilebVnyMxsMtoONU3PKSyo/y+lyfYJI3cg1a
acjSsosKVep2ai7/8M9AzRbwoIKRG2sjkkVYxGByYf4j/lM7VgBIyJAA2j151fgOMGpOyZCLkPyX
TMczXBvihQOJzExTptnWRKw6tkvDfQvGRS738jeH+92vw/v2aH3xG2dBWhBEcXtWzPA7G3HrVOeQ
aOlKmvc0XRLjn2SStNR/K/GaMq0NiasYnw75B61TMSjftaJUolvHH6x82l8HpDOYpaiD3Y0yIxAN
qFI5pCGiG0tQF6a2FbfBhQQCrkTJosMyYPDmYEBTkXEyLzw234bhfL7hE700ZPQaigScpSOUcT/l
HYbJBr9fHwdEuMZvqUCYL/BX2jWiAk9afPKZLek8gLh9Qti9qwwggdSOOw+7hRQjwgzBTN9rTRyE
hZRtu7e4ZLgFnwF17BayVJO7Krcld4/ymrxb8RykS2HCYTBoAsE9yHC4HrWxw+UVp+ICmGgIw9fg
u+GKSBBrDRbl9GDgZGDq/lXCwNnmhjG5gagjzhrfhyRtVnS33v3/w8UiU5TRhEsPChAg0ziTbUfj
rnrILhEgnHsy0HzBb4JyXgE9+uaY36NkFLhdp4yn56nKxoU+RtFalSKCTREbGdX71S0V+Ajvi+vG
0Kp2DABp9gHYhhoLzn+gI7aj5SvvhTwAyoQ3RON8almoXH3c3tEeU+BDpsW+RDWTggp41TQoGc6D
sOQhsj4OePSF2qonCXG8Rut1ilXySJzElW2MMkoHPkP8wuEmM/fxaHm+L20biSSp6rG3IhZqclod
UoRmcoPZO3Qw5r7Zv6F4DIv8c4xPL0GtGJ76M/ZhOmXH7I0Wctb0HzUMtkQYVeUGFq5perHSpAI1
ur4Szz7dLjZ15htUjv1MKJ9U3MHNFxEoIvDcEV7BC93C1RDqZakXWaVyv9qFxWCC+yvdS82vsAp5
zjeLzBRLkBImeGiDKcZE+aFXPfnKKd5c/vGC+/94pNqtJ1KHR+CuX0UqwEwB5zBnqcmKThdtzYF+
nuYSganDDyu/dhMDlpsDg5XEqMQ7KZXT8zlS+pg+IkKq9rmMsPUPMPURKAYwsyqvVfgRUqOTHZ2v
72qVTMGkD2VfH87DliAtNC7VNlNzu3Uhhga08gNKWuvyd9LGAyBVI+VwXjMhv5EFYTgJvkbVfNxp
zJSqeRHFeqEmeTTLdiV/qqgBLiCsmVwAUEvDeSyJ+WDFVPpZvvb4be8ZEr/2TpmkbcMcKg04bKKr
Mp24eay0XIc/AW2nZNgWurD+umQCJayt/IxJZxq3FgZma2ROi/YaFyvauynm27ckI63dadOS9XL+
aMaoqeqNhh3+PQwfmprHlKptIJ7WCf2JGD+lgRleOu65XGcbMdkdEtfat7GYkxQSLkm0V/GpnJgB
ssmyZ9n/j89QXaq6oyx/Bw8yi3e/zzAkHdO9Z7I0lLLPZMsGaO3I6cZ5T31mjTJ0zsJ54Tz9+Ecv
4nM8D87OniQy8nrjGCyBRgTyCFIjReuP8NH11Ivs+Qqigf2LgblQNVIuRD9tb2ttH4JEAEw9qPpL
yBFgRlyMjhwVOIm4bpVhSRz0AQ+MKXLmYtmGYl3PDkPbyjp5UfM0bwebjZrsWVWegVe5ogeqUe01
DOZr4z4By4/3jTNDpO9xIxtAdyOz3uo/pBcFHjikpA/8nk7TwYnrQx/omrO2n+rQhRly2hJqVdGO
6oxTrW1RGR/4h9OywTEW78tofSQRKGY2y3TU1WVWytw6maKvblBnIL81ceyRUYwOkNHxzVLAj5qG
JxheMq1Uq9811d4k1z7twkWqGhNrQlNNLZaZc+apUO+pdUbW/83WozNJboXEtLJ/DNO+L66/JoNo
kqfjMONpv9NYR4g7awF2sNpEQPt9QTJPRGqbl1AaK4V48/5JMOJNoMEWou6WnAnI0JZy4YSMuH2L
/Heh6NezQBxuoIb2ahpLu3edawtdAEsRKlApvzz0KWpCZX3p29VR5mtkv323dPNbtxWVzbeHQXHv
dgzrlzLOKHvgpLM9cShC+Vcsf+TGf79EHPh3HZm8ez033kFuHfJ4riL2JHfng1Zn5BT4djWIR5om
D9/cJV9PUcKADg92FtX4iarUH8r1z8CvK2/7QZ0vcmtGj2RnUZfbmzOHvDNAsmVjoLCn66soWrom
7opYFoyotfP2Pcclc2JpqK3gc8n1bg9RAIR4VEilNri65dm0/croffVyWpEfZA2YGCsRfkvw3MHE
Vie97OnIZWve4aAFWNaQckY3cViKeO+LXDlB1Fh4QC4dIdeEBLMNnJE85/zfqlx/uwawOPbx8fbm
0WraR/y2dSG82hTSfMWoEfJayHjjo1duHiEayLEAur5av204+RPKK7UuVoslXYp9vnO4Cwf0Q7sy
oL0Ds8caFdDS4W5UGxdMMF8He2iV4lk1CEjGUWBU8ApvHsfOtyfG6U1u/Lin9soWrGF9V3RDWczP
OjGhO7cQLj9als8eR5u/jQ0ZZlyOX9nOPJRSn6A4UYJBUugh7nPDYSsZdIAcSOTzHxkNdBX6Ih8W
9rJC5CRiE6hu1QfSlgEigYelBQKbWBsTvZ/ME+bHR34Bu27RCJdwkY62/EjmPLOwmrdalcWln+C+
Ru4kfOIDXkQtOzKmoIl9fcCGzJu735qHraSGcyl0XaVdMwWLThptDmCSG+tO5MK8/TYkrK/o29oE
7BTBFc0ipp8dXBZ7Xuw+BC023a9FhnF4oDCl2c7z0oZxclnnxvmKn488QibJTXlLmY0c2lNIIGcE
xH6OPst7hAEQu7sCo2iakBLnG6h4vhCUs8MHxCVuEYadC8K2FX59Ixq6VvwzxTIiyW1arZhpbuVi
vYXIFDn5q51y3m0PMlhDbtG9FY7An+NYM/cqjh+avERSamMTUiX3Gf6QNIFl9mMD5/HvX9cCJtyl
U0X7Qlz5j2EX03A2wBbvC6cb/jritCl6iXFt4cGO/C0DNmR/B/PRacVPpsnDURAEa4kQqnM099Vp
W8fBdPD50aBnNEu7F1rQjIHRcEUaNz06zghlXSfNOZlsg4ZcQVo9SikIQyxKErTgA4SCGg39Nyox
RM3yQ/PnJcQV275WM/2nqrxSTdkQzqdo8EbbBYEONJhyu3wE8GyI629X8ks0YI13rl9G4P2cfDNi
h1ErEVVTiSJnjXagGXvQ/TC6C08gbZ/jzNBqc8VRqk8F2m2X482BPOEXmpDA3xD/v+qzgl/eZOU8
K7p+bAxXsLiBFnq0usVeC/TfrZWt2g/Vtd0AX/uuf3im7TQQ7YgO6lz+YfbMUuAWu1ONrBzeCzC0
H0XGFP5C9ASzP8yV314JdkgNNaQ+BuBH5EmeTuXxEcdrDjrn0eDLwBvybb5dqeoAYdkFIRtX5Anm
cceBsXdsVIkSJJVZ5ebDmr2BTBgnKVS+YU/EgUnCssUlFEhoPUApGOhlr8hxgozrO3WGP+/1G3Xs
Ral2lPjCpQzS5EpTiOVIYVgzM0m9c1EPJRgAZDJnTF7AG+3tFg3iOonQ6xAjqnhNz3opPeFqki74
mGo7QQ14PtIVJDcLy/Nh08zvqOgEta0slgcKhmXKfld51RTDVssMe8LE1xqx5ru6rEFcEHgmz3bq
ehZtcAuo5cB4oYpDbaRHEm4ttQjGHaYkmPS1CWK1+phAKIF7+VHDale85MYP0fuaj2bItdXcwhIW
O61TEzY3xEm83ytYXWS4utbZ0CrDX7hBub+dutb4+Hoytd7HzOUSRYyP75DiSn1A/Cjlj58ylB8T
ughIdNWxMO1gxkSSBzepDkCDCdKBSuUCI2oLkonErj/65b2AeTAEXKOXGXTf/ivbAG+mPMTrhfE0
KUlzDD8XZ/mphzZTl5UJ3RBEgp3+lcZvrua2J1HsEfSf81GC7mJvoqL5yOY/H+7EjQDNsWUdIIiM
3KwQ7uF80hqKN0Sf34oHtjN65nxVS7eG1ijnIdEpoE3hNByQP4Tu9RTjSAEk7kZt/uaLsr7V4vHu
BlRdKz8rgp5hBHf1Q4oYXh08cfai/0quO9fznf/ecxOPuK/cGJtpLNPvXeEDCZAUoECSfK5s9fkW
4GzUOik/AOfm2dG8IpbW8ryURzY0qE/324cnW2tuzQhzyBbJRwC9yBpbs5p9I+6HEUS1KZnLWF9s
XD3wS0VDsIUU7VIGxQf/nB/nmltp3aisJ4mWUkKrr9REBeMI36hHJjYFlBdueMx8+lxILfhnTugu
aVMKXoWp/cRlXElcQdQRGFonKPk0OjAKADnVCsK3SDbUC9uZICeUQ+KLzwZcMoY/t32+QhI/Vi/d
6Xrw+CzICXslQTYlo5CcdpoQOuZngLAZuXeHmRrDIAf5TwJ96m/95pMug3AaAs+tIer90DYriHgt
XaZWzXFf3R8iTV64UefgTiG1/rqdIQdo1F/M0CUIq+JTKf3qAlYRkLWP1+wfCt2xjQ9Y7J2sGLsI
7Cyo/HDTg8zaQbyr3pQ4I4imOmTPJA5BS8BthRlaheW6R3dpQHRWk9ZpKaD5pdJ3rks/oiqYtYSr
8CMuKT76CjpuGCH60k8TMSXYNCitLYnzkra4XIL9cIaa8rwNhH1ZfRDlcllWPjkwuBMGHhibtpJ/
hi6WdqgS+kGfQyc/N1tuDl6M/w7nj0k3JFN8+iGxNlhGeUjfGxT8PacFwdc6pSMZGllmPRF/fbIp
u9nlccXK4oNxHvAqPzpup6/6+E50kpi8TrbUMgoXaOTjREbclXwehwKLMXXOSpPr2cDtSHlX/rn7
ppHvN3hZWi14JKMtqyxcz2DFutQPUnDzSrFGlGjz4E5ch/SYPMQq220aMyUHHLxEfI8xocnw11Xi
iq2SPlDSh1EdRQEqXdbZheZ2qCQORgu7FqOomsbgq5qOvMeNarBVhoYqP++ryByeabRBtm/rMyz9
4NMz69pCElbp45mBRk+kKHp2tO+vNKbOUjqn8N9alLWRYQeCrtfHxglJxppyIqAy07Vf416CwRza
xCcnjtJtZAIsFbWJ242wdcIhPkWIAx6+0iiK+qqGC/6JXCFO7HntBAQIWYslijINuD4T8tyMWRHK
0M3iuWsnLTALtvX6SUAeAkdYhVgbPiU3lrdOmxiNdAop6nHe6gPLNy5S2wObrRtGqFi5SobBF6OL
fYFswteicc+ggEPr6Xb/jecUm4kYqROjW51gaZnZtkjucCHlk/7nIJ7q3/nN74uMQHmNbkYtATK/
EUYPwFW/cHoqZqG55wjt5tz3GG27sSAe5LEN1nIhvz7HYHhsdm6Lg1NSVajjES86VflXsZgrM71s
5XOug3/zSQM/UncaiXe2j9qMgBPE6ohVUkmfpYWsFFcuEbUFbJXRCFevASdycOWfVEigOb3IkORk
zM+w0HPjFt19Q9b6hn8S6elw/3jnwkZP4scaF05Odm1QoYEXljtN7bHeO8gynDi5X9ORnzgE3Gq6
me43Nlbtq9BfJuqNZ3aGV0L9CqWr5kFcYXbUw+5DH5EGCfJalTmYSLI31LBThvZ4+9R5hu9kfJZT
7oEvw3hou9vIeXr/8PFKBwZstAVM/nZ8Vi+dfRKB+eV1S57OfHFkmFkNZaRbejE2QPgFJOWVEvAj
lawzCPN01IPMGdtj4E8guaTrR56zCQ4P2jTmLDyIE8eaFb4Eq2wzpmivC4jtr+h8EvDgx1esua42
xfatSLGG5foLVRfNf8HCMSxXQoERbl0AURTp/+Adxv00oNZ3ME7MLykY3WTvdtaW3XEzmOvHiIbs
yLn8g/pMqscBYB7vgGFB8sCMMsifcFY2BmAgMkKaKQPNVjHGkwKRuVn+V9PYJEfjd/H7wEvtW/5M
qXn7T30ZytbY0/RPA2Z31Puk6anwefaytO2Ms+J4+JIJeeDhCfIIFiUsNWYWVE5OAc8PwTCxRXLu
NgET7U37q3i0jyQSQylzUuveRI2AWsQCV9XfZf0sshgg/zjnPNautJT392Cj6wx3icMw/mFerlGJ
56f6b3fP0IMx43P3IFlcK0GZGeYyPsSeOOxD42qSYYFmcHbrLJiB5p9iVt96h70QRKw6HJSP+2bh
p7VKX53cuEG1emyNyDnf8b1E59gisjBMjzzbDgnBqxmtjd8ltHFRZNomnAxwFg8u+kY+LbgJw3Ku
B1FtmZhih57keqKjQRRAM4xyi33mEnT1G83ln3C6q93kLXS8cVRRgFDN/jAQzj+99GhKJYqHGx+M
D/7pRt2LM9Vy2pdlr1GqAJfUzUmrbGGrIZN1mhc0uwRobpLIeMGbKwXGdfEgKeDWwS+r35xquHkD
R4eTb2ZYqCbXksXPV+hYa1Le6uyesPn7/XUMvcVkEYra50Vi7KN8HVbzzwey37QYsX+83S6IlbZk
GtdCzjAb3ROiB1/v/WLY7zrlwyMpi5UDLr5LpfHKfCH1niHWcfQ8q7/NzP2VJAKguQoNdkUYmFXh
r+6vkMvnxk5jSUswYep5VV5Pk4wAV3HoMP9U0MikCEZgVanwOXuVdcW8btYl7r6rvx5pRv3fTSPz
K4OCo468nV1p+mcz7n8TPu/yxWktXIlPbGJ5kiHdwkXhBSJarK7oDMyIcok8Vl4BUcfcQIYt1R0F
70HEqpYzBWqBZ/zm3rsw+fSsAwAFfn1zNv/q8FsnmlTRCtUFKfPme5nttS4by4suBFxF9+ce44mT
SFpoKdNJxEmAOWUHt63E3wqzsxEiOs9JlwN8ZiHhoHH3o8a1pN3n7pPrJq2QIkHUvQJXTsUShROM
fYELU4Q6ogNatbb61AfqzHqySteXQm6WHDQooyzWHLPru/Te26vA9dX/hEN1f5yQrrHTnpohQI/F
VXMXylxTRydNKSWetBjaJ2CbB3i1UEnbt/vi6zOZSTP9PBiuy2CoqU63HOO5u0iNfrUQsfxOfSCs
2SkCZ9r0rXom103ogQA6HGjG45eI8l1lrg+dUx6jg0c1LjhTKiOQ+FZh8vrdw0t+0sqG4EDMgIzo
HJha6l3QAYfkYJ/NpswlhcteO4Jb4R9hX1neNUTdOEGv/FAMbxj3r3MD7suLm/OcGPSFbUWDUej5
p111WG7XhB6JeaFlkTjpf2Ik5piFZh7R4t77VyL8tbTVMjgkr4Zusdl0Ks3YX//5RR6Qtm8YY/8z
W292Rw4Y1LEp/LMuYVQ3DNLA4oQThhz2MW8FNYolbwEtdle2193VNiBLt6231x5e8g73b6vx+SXd
Usx7+UNP1wkRrF0Yfh7EXF8EHyRdUfm60tTosoYjyHkk28W9kL44MzW0UW9yD0owYiL+4vzzAnTB
oOG+kHLiiZTTSfNIICiFpRtusR97yFl1i8Enx+qFhejaobv+cooKW8sbqqL+LjQTQ64zdU68SuCN
4UW6Z71FOuChnVWCX9LOs/lgFfKOlaDj7tcQGZ3DU5RK1hZSy2V/eZgxEW0Ly2jQ0N4r6pldQOOU
Rg9n/UVHaoJntuTDuG3Q88maSRq+3rZrgoz3ykiemZaa4IYtBLL0L9+csFV2WsH28WAJw6K8najZ
v7EpUOwXMSgT12y7FRxvv6tNjSVRFJ/Ll6JxYczHojn4IzAd7/prlP612UVo2V9uT9Ln1lDrRuAv
I46L7mXDxVMVhvt9EdKf+iK70X+dYktYWlNqRzV44gm9dqJqvqTioQfXuoH82x+ZYaHDzoO4JDIV
wgoa+m9xOWkr9ah1T2RAr1S+Nm/pyXRc6E/DR+jBjQ5epzObqIKsDYAhyuTtrWs3HtI9KYOlj5qr
eOveSwtBWthcS+QQ4comNUAmr/qYKsOUhN1VdstmcVIHDgP11B0SFTZ53lZZtPBjVnnWq5sBDsB+
zoeCAsgrsorHBndJ/f8CVNkrQs1FoBdEa4IcumntWQ2f0D+B8bX1SrVv9tpIhkqgL6zj/yYX7Mdp
KUWrOIYFy6aTp7m0V2qq8BTrg0d+SueexuMiN0L+OPoHDk0BDtHrLgjIGIXEwwqmHlJuX7lkR0Gd
9z0hRqPm+yxM5IoVeygxOo1QNRYlscBtPsZ6cLNXBEVhjUCLGOoMbQzWSSpClDmbQqZOtEhLauQO
x5R3oeJB9EBgb/KIa4Qp9JuTBQZDgvHVp963phIOs5JltLZVPXqmOtLU8jhJD7nyXpTmRw6ZCS3K
Z1hHpYNtSiQJJrqH46W8OmIKSPAUZJPkYGU+polCaxFegNRIw2ECAax0mlg95I+ItkWmksvHHhrj
O+Lnznaync2EfL914yaJQYsE0VTVfJ4DD2HKHxkTPmi61LBE7QSkHe5wCXcpfUE+jITL07dDL/2U
ZUxa6D+UssgwQBWj59r3TtrqHG9LzLkcAbz1GqBHEHqXlP6zqkL6WxNSCsvCMHs/g5ojKpZTG/d6
/D5NoPFCG4iCMwePDydSfNw6TeRMrB5XttHSW48Evn+Vun4VsrL6BRgMrbpa3Jp1w/KYQZftv5c9
oDcOXvgGIPMfxgUM+VJahJM+fnCWn+Bgybaz10b8leqGoXXKqrbVvOgZxPm862z1Dqxpx4Yof6Ja
lRNl3JqkX3EuxwfqfuaBTh12NLiSzbLNqHd+4K1sy7HdhsH5JrYQ7+gFUQLyzftjcQMmC2rmEjEl
23BKU8brFzTh5CZ1hnEJTH3/moSZQordUuEylLNZKW5bSlRNpimIg9e+HnS1CTDeS9/npc+xxyd4
S63lDwdSzXJzkmnjoww788lxRaOmluvms2gLS3QTtFdTtW29dTAbGahIwf5qpQZrUgShoafmjbiZ
06sNJ1DhSacODKO+yx4u1evmbMc4KGuZMtONoEo2S0IaowwakGpIi1SxXAbqx7IlK6evT2UJm7wL
3C9KyXj5dKSXpfryn3Ya4qAMuo+DOQ7bbU8Wn+uzsj2M/61znm/BXBF4LEyoGRWauoCEO8n0AW2V
NJ9mUeU90kLYO9o+K073gBFN9Fx5vucnXehNfa7ckarsUyhp/Y9c5tEyXeqfwi9gAEe6KLJjL8gA
mjU+eYvTNUTPmfj4cKlk5JFsnFp+6rCBPBBnwZIxtTIQpLTMoG2VMT2TA1GHQbvJy5IM9baP+Sax
DefsTLQl3JcW7pZXb+AUHu6KTS61ZxqaCFHK+7ky23Ur8Kuo0kV355Vk7nSXReoHs7ZNgSbVABgP
r8IiUw5TeibphBXiSGry9YuQFBbwCRLWx87k/MjycWL6O9YBQO1O3qKYr50NnqaLHj90gmVRCo3k
9IF64Hr82reh4I9E/EwcK/U0jwBRCR+yjBUo/gifzl+HbkBCh2HMPc6y3IIqNv9QARYUYp6SWuHw
yx2TjhNZSyqokZm3FOdwJV95mdc1vPMIN9D6/9YtS9Oo0r3QG1X3HEwlbTKRA8uJxxBcYKUdo9eA
icb6AxnQqqiZwSv5Ro8M6NLd9RQlQtVAJge7U5gF43x8rYtCwoDeVmvwkrzkZNEhAYPrmgxKTttE
5Z3v4J9nF0agOPwG31sPBA3mT91BwrA+C3Ba8ohKSAcw7J4mXSD5Ud1bi+jq7IHI3aM6606Nw8nB
NzmTEQnomtH5y2NrCimZU+IleTXh6vZxUNgPIRf1FnFMquC2ak45Hq5rQMrDwTXuC1eeDwlIsyPC
pWVnpVo9HMyRV5wF27BPw5x+7VQwYt74aW6m1FO187iQC6GrVWG21T7Vjd6y6VRPdh1l+/6tv9BE
cLQ4jdaJAEh9aClColPt5NaJOiX5MgefcochuF5uCo0Oy80snrZxPHu6sHl6aJb97NuzMs0ZBuUg
NZ7VCi6FgCF30WUN2EGk9IwNxJT4gGAN7jKe3IGavy5aoF2HGimRMPYjbyUn40q94GQ0eLI68XmD
hozF8s2IlYWfJBZVNTphezxh9qvwOE4AO8AK2FKWiwykafDOG8yKAohNpB/3ZmCjgPaaoN1/y+0O
Cul7adrTWzF+aVp5fCGwgwRvWqtpebkBlFC9v10VldubSV2vLFdT7hXxZeoALLs2VK8fQ/+HWDSt
qP2Lxx5RYLogiWx8++Cy/2Pu5Uv7P5dshbtX3eJHkCeDQp0d8TDHXb7+J9QGk+hAUineoMxWQzJh
zPU/9aeaWGpp25XLaqpS0/8nmumlXCz5uaiGWWMnpM0XU7jk0ggQtM0lIkOvpAKzaWTQX/BUc6HX
jd4muFiLeTkZQ6taAaakmPmuhPaE25uWNt086wyd5G3wsj+Co0YBLQ8addnkaRmgCLyjRM7+IsQt
Fj+qM43gg2MoikpaGYDb6oHbZ2cSFekK02famEvI+TOKJMZVRcwe3TBN9sQnUZD6+FI/y9muNwhc
aQL5Gph+GMIQow56y9TrcxR3GEquShf5QLKVOqX2hJzFDGV0xunt630+wKpUguqFI0/PkbKGJFKZ
eze8VP77SBWKIyk1pNmOHtVWINd/+lcg3+STZO14t2zCwRlwzExdORyeAV7H9tWyVzRS8AOt1jY9
pyBL4SoRH6qSllOB4pKM4BRd/UtmLWsJ6Dz/nCD/wCG9Btgiygyj5YQpeu917hANCVjP97ckev2O
MZjm/MmVuIStmMN+yhcWL/JiqQgQFpMrVq2R1lctLL3YmnPIc6VhebIDLMG7bP1lQtPZQeRIOIbw
5nfNHUDXZqCbW30s5mSKqTEtwLkFMBPPJaEOFToSJur4RQpZsa2EnfcwhHTy/8yIAxMaDAKUoseR
opkaDKdIZV/iK4bktAAHgCcHZyHh5VclRrl+LzuflkbQibHZK2B6ewjPQ8xzbU4IEArLyy/Xhikm
nubmzIQmM4o6rMQxiLGT1z196t3kMJCaEZAMQ38yn4uY7h268grGMcSEuzVwXOK8w8m23EMT9u5F
FNa3FrroUx/5biQKuBF60w10Okqd33fayT9t9HUQBHinCo3/jddXtHrdZ5i+Zt93PL6Ahb+lqBn8
s5pQIHdjzEh44PSzKPE3JyRm84mNs2wFv2DRuPnVYbdGva4NAVps05kfATdu2BkIGV5FLjKpHPbV
3Q2n1zpKCOA5Wsgh9g+9u397MVw30cqYDO0dbyUoGyY8G6wnlvNLRkW35PLXjgm2ay2zglGaYong
bgqpYev2Hirnxw5PwV7Ot0AqcmF40Gpg8D9hV1mMb7r21pG/3Aj9jAL8giBHf5CGEgifSFFtRnkz
oRwBjfn73DPI9gi7fhU2QuFDphIlJB+hCi6hl0Zpsk0RYbzv5+nYGQyj2BfEoZsisFsWBEBulq3D
yozZwG8aEaH1LRaZd2YzlVYRZCpSbsmKAUzUO4rGBcj+SUVwWff8NUwwfE/pzZ3vNnRQxO1HOYgV
+3a98gOcgNN7n+bcTKJ2rMI3Ia9D8eeOm55jj8Qe7Z3y7RpaA8PGVxPWjyfs6eSLIUGRx9Boq1m8
pmsb/Ksw7uX0Qo8whqonnefSrBn1du5+LqyeO2LYcudLbgUX1/wLs6I3NMwhUIQcAmQagmciPI0N
DWpMia5tAGU5AKDonLQiKhAelZ6cEoeoF6fci/oFzkSxXIy4NUjFNMZIgjCw9sQuQ9b74aXjg+Sx
2Z/oNgLHwWokfmBvlZI3ndyrA8zb97ItJGmiMTLDcYBP9p7y2PJayl3rmKrXfdmq6BQx06bsNaOv
0/5zRPnipLAWbbnJJGaRDVpol1zGyCCnH+b8OrTuoLr7/h8mEyWecOWosZz94GvP3iPyhuf5MGMW
6dLfCuZ/ys1MzJus2oT6dkGgcFdN8FgF0AYc/TFvbLHOQ/DZbwxkgqJF6juWcqKfrNOUeEbemXWP
PTsLmKah/H3gahcdS77+4C0lpfK9S5a3ViqDrTXyI2KH590uIk3jKWuODCGP7LVMm8bovPj26kd+
V/Du3D3Lf2Bk02epQQa9w29B58RJQuCqQh/JEARJdCZa4Sz1L8dhhCB53GEUI5A8mTKNXwgK5+2Z
pSNj54931Q/3NYetjj3VNGFS1RgHqD79O1HBm+DxC98g7yaXhQierfL0kMNWXeqRb7EA6sup7pAB
5xjL3q2mZ96hTWpr62hkBdt1m+CQXsIWuRg9IIbDFe1h7uj9hudXg0rFSFpLdq8JtU7ztyEdROLw
vKiYLZ5gnFBQ09rxVIXFVCKxJRK2JRosSp4vHuoborTt3wLhL0z3AY7XC6DKH96LUW4V+1jGPEai
llZToRmIWdTlDdXUoQuBBIzCkgpxrz1I8NrAHb6XRXnLsVkzanNQqHKAJygwFoV4y3KHFj3Rt/Mi
zxvVzq+P8fYnnEacaY3nOzknAUjBVbdNqCXOE7WGrGUfzi9rL3KElJIWcHnBSs+1IOTuZ4XZ2mG1
GusOJ4YjyFAgAId1cAs7H/12HfYJR419FRvsl1Lhy0RNL3he94KrkGZug2w55IvxuPg2vByzB+s1
BCMF8uyg1w6lYRmdJ87uV1EE7p+OzdElVybeoMy4XH7T3OKLj+/09hOTsROKVm3IMeEkWZVknl52
ipyFZx/Sq1geKp1O2i5kDipD/4M+kA55vgWHLGS0Tz6lGyPF4sKSftKYh08kMHh3XnSD8oKCilSk
QKSe/9WuKRbR1ZobJU9TMfjJqk0LUWufj+jFZkRbV7T94FqA9ATm9mt/MX/nHAgNdqaRWq9paD9T
8tcZMVg6cYAyX3rYVCzYJbeY2JwVUD6sgeL3NeAB9VU8qcBqAfN6KFmh/r9/AcojMi3Q9QjtSNmU
l+yz/Xm/cEgo4oPz/KR2sErJ8/MWKmY0pFC9oTssrwvIQ4OIpn0YQktXHhBQ/P15ZhyuMJY+T/Ta
/n9BgCVxa9lj5PVxxFIyQU1Yp/GpPzRpz3cdl55ncdC8lZcUwmACVx9thp3+w/AgY9aRF5XHAoMn
UWuS/tAgxe3Oc0glQWm7v3dzdfymF/ScBB8bAk0j1+9OESw3SdS8Ii7NXH56DqnvDlYdH2HRwVpk
+QrDT9X3zRMmta61mPgB36ucz83fpCB7W7XVnf/3JeiLMFxkiRU4r3/vJEk/uJ0ehNV0XfgzUp2u
NN2WK5xABy2gD7HWR4J/0trPv+YQ6odHmDXT5D/8phudkfF3f9/clpWarQg4JtdZHTNj/qREF4YK
cYkV/NYkVSB9Zzi/eYPucEycgwi0KP1RgsKie4DB8G4IpwTjCaCX1tMU6d7L5ocf0SwKoSm4CNFm
xPET+PosygN9ZAHwuHZQjK5I8UsdlFXnLGi9AClg/KPfhFA+FpRyvk2hSSN0Qh85mI24ChhiaE5I
D3w+h6gLxc0xZ9Y0Z8ImDdfXPwPGWqtCzEYYpv8xxikjTMjQvt6uW3x075yog0ykzUw+/94hLZNV
eCzrqvZIm+AF1vsOIHH39JvYF91TBHkdmIPXz7//ruBcj86Mbj5k2FQ1BHlYG8BIrrwadpOefKEH
4UU56VlfXVeC0pKmTvqijQu1kMG0iDsNWbMLzrZyP+150D5s+Iuv6MAZ9HPWFSPY+5A7S4QWyB54
ygNn+hU7SaZFdxMW3ihCSRveTs4U+NthXbfIpAismZ/X+XYTSnTIvVkocC6D21Vk8DjCTEraWObF
cFIpawDYz+leD5Ak5mLmDfqz9JW+hYqZxXdLb/TgrbFs9SCmQW5dIej1Uv3fhqCE0Z6ZASDymSNm
q2Yi/xw/ljiRujKobsze5Yw0G5+S4yHqTFwGD2FnfEyv/X+n2HQpcO5yxm8xojUkojrotMv9V4jI
qQnQILuwhspbL4IksOFUi776cKpJdV06E5NNHbi2FtZkfzssreI8gID1ifBLD0mm0LyWmcNwGYf0
y+ht4QqC5rFY9XIxlCyiQUVlfEDngM1kGa1y/3hbWdxIr+5xynqyansTfV/PmlmzK79GAqxtDMYj
6OFchSTSIUCObLBbme5UUwi2CUyPmbCt3xr0i3HqqGnZ4lDK/PgNNIAzZ0mzkcQeou08WawXSbFY
91ZwHa6ZKan/Nb84IPqpf+EnDvUKS2suo5n7psJBUe3P2MdC7evfriV99LjCZ4tQUNsDQM2vlHPp
25G+3k64PalWgznz5RQk04AlvR7GrpZX6cimmt9Xfstoa0JvVZPIo4AU/pITdN7FMmrwuT64wQMO
5FCZhPPgxehxdsbSXLk3b1pwapfzQiDi9C4Mfn6Br27IAek/kXnF1RoLjmKIuF0CSN6j5VEUqXZU
q3RBHGJJw+YfTvSauL26gz8Ci0lcB9PqONlwnYnJTiBTpWpRYTVZhW3Ll0taPfnukqtzOETUbyI3
ZMJ98xbC6Pc5hWE3y6oq8hPADpHmCeMtA3JPH6OlUoRXj+sWmWG2IyVoxjsiLv7Q8kYf/d97rSx4
iMFjAl+36VeNzxhLK/W+WTaKcfNnhiO79qkcfdmnxPWGbW2EbBF2B+emqcqqwiXGm4ShIXFxMpLv
YfJzh9i1hGsWNJhIwuUQ3//hfZXJhnQ665e1AsIgCZPZOYKfpu4TKOUX16ctPAupEmTgz6z6Xnuz
DP12ZtgqloO/mqWpcSQNfmlDboc/vacMU2ukAxsi4QQfi5P8jf+Zg3CtGWFcpp1aspZUmEHAHlNi
0JK+ryARKhQ7Y5WR+PJm5s8DXTxHHZ/7p1yShCwLTKTCTJ/8nyh5oHxdHheVJxj57xIheDhVJHwC
xmM9IIZcGdYp8JICnvUYKrEz/6fAdRXzeouDcj9GFlxBup6MPavVIGjT5Hgov9u2cHmjxTpkOXCa
MPm0IqcEjyrWLlSyLRdg9sO6ia0aup2Hj516u66szZpiMV+oXU85us2CQ27blhHeCFa0thUb6pBX
PaKJOIgNmuzMZUn2/XqS2drlDxWincEX+tg/X3C80jd8hUcIyUMmr2Mw9maBzz80YWAGXNFGaT5u
Svi0O2XpMPCRNafGYh5RLX0mRi9nuj+ncO4PecO1ft6KHS407+z+sAqIjHIwyShA8coUFmdwjLpr
VT4rWfZ3UgRihamUyzx2tDZBrSEutSVFg2nwAAo6HgHj4Fkf1iBxhGA22H2YKMZDavg/OhoiilTJ
OPNNLSjuMxHgVNHHUd4bSlZl+RUfX5SzBGvGOQbm/fIHnr6qA3nGsz73jAxTBA2BQW7dNHKvFtrT
BA+ULh8uQuHKdIS5TFHfHYvf+4gJQCtulJSp00T1c9qYJVLkfjpbkqc5Gl8d7v0aVLtgKg9atcYm
kXnIgjr0wt4yPbIbKDoMbRz/eGGpjF512lsqQGsyj3+9zKMt5jhbDL04IBcESjiAnrfr64fpGL9U
2XJj+ZM4Wbo6SXtN6yDocsqpgwwEl6kBIQV5C/9/fiYyRL8wz1mWZUQN03VepDdPW+UFCz42nN4S
7O2dz8tTPCY9EaR2GrSiSQSx5ZWM2rdjsI5W4tKjHSwVBRil0jtLwD8Kl0VNCYI5ekur31t4pdz8
2vLS6mYAvEnjiSFz2uehMn9eqE50qkRltATEkJRkXGKoWeW1gpAhD3/rDyBZyb2Ef0hxtPfHuGnF
Tv5zMIv7Z6vbz2Zo9JolHwzy9LVSGbTwKyq9dxP+VRTmH7xNF5TH9WV1jfSesLPNePn5/wpH0PPM
yje/SiCneyLYjJ/L9KaSDKbwBON7d3poBVlpWDIqCEZJwVaVXsVEhT5h+l01ISqNW4PcnDpKmN00
wKRZC8wmSDYM6+TZN62TBnxX7N1FoLUUwvSWcP2lbQmCemDjfucRmC+Hd51ws0UtA9ythxEFIZYv
6Bi5pFOo7JYzD2gwLu3h4Oep54Sl+TlqteUGA7uj9hxBnEovZD8gzu48nCsIWD7ROckiI1eQfm2X
9/uEqsHEyuZwwmpfrIQ8wp9uBaGXWmzzOyL8eFRmxs2y7Efpvnf7DkJTqCuX3xbZbx13Oi8NAELb
er4R4MNEABM6AunsdtC0nlZAg1uSTnRr9mdEjX1icw6oqv+ekRjvybRiyDD89iVu1pnAUcq+FD8M
A6BhRva5UDAjPjBjIUCB7DN81d6+dAJ+7Qslqxwzj295E36EhgDuN+UAuRI3FUijfHnExM2nbeAE
PmFvaZXiFbfIJOc0tcvNDnGnYCB3JOhUh9tKEVehlne73LuGFaKVZvc1DKxCzgw4XNWkt0RD4wP9
ZXVgLH2ERFZX3KpwR6RgxBnSvBmcOmn5LosCbvmvxeEtBjf49u+ngzMZnATxhhMQqoDqZIH5GjBA
jwhNt06ekLEhKCKIUu1R2u+Tbj7wJyYslB//a0qZgYEa3WP+ylFvxcexrA5H4Nkzudhk5ZPPZ+kn
8FVt4Akf0ts9OeXpLVQ0PtBLuFI0kaU1wlxILUB2yzfTDSbohguGvrc05buYWUr12K6ieD0zDMlm
4MqNZ+MAI11RUt00m4dQ1qYLIHd1k2xUMRMb3medwyxitfaq20KxpNSTIsMqEa5CM1gIvbitklM0
/i53NfCiJwgx3JOurOVMWu94uQzCtjXCZT12pGEhkeT+jGgbGDr9EMyxOCLdXpKS/j3w5yTc3Q/c
gaShk7OFrU4x3Z8eMGjPhCXXDvQRttIPhMj/iYOj4T+NtArK4fW7dzVwAKq4olCHCD1hzOlKqCer
k8hWsfRAp9qTm+WgpAjIvtpKwCxS+4/VjDVSn/6lkx3NErPzBoHxS1D7x+njLGuyDtd4KmtnECM0
FDLHNl5+YOn9GPht+AvekXPafBDzo8MiqOKA5JtOk08GiZS/z1LrukidX1M/eUf0zPsgQqkgT8ex
SJfFhEaaXfgBOQbAggaNoi1Z3oM5HOxx9EkO97NHBHZENUJM7XBFyp6SKEUSg+5PYHAc5NXA8Amm
BKzAM8VV459tCGMX2uu1VN9vAt6Ja7RyNiAQsNVto3PKVAdO+v9d9KDA33XF8CXjrgE6Ihy8ZQ4Z
Fm3j5YkcO0uQalM2fZQ3UDCd7Tkf63ZkT8OFE7pB9y3iNkY0vDzl44hzmQ7rr7DIEXtrQ83F42jd
wTExbrTRce63KYTqsEs35r4LjS199My9a13JqY5uXhSzorJ7eYbE9WXU+Fa/CsmQdFvHp9y9Zl9t
kYH5G4c5MT2KvZ+1h0ZYWUszHhezDOmgWAXHVX66y2aQ4h6n21I5mupLcEH/NgksbfC7+wWVtk3a
4L2J2umi89nW3C1wBMsv+B0dLLMteGA1N1llU1eBDtXypC7cHcCoFHW9JpILjt/eCNeY27NOppOk
g4JerK7Mu2dB9pK2ItChBCUxz+lM2hV1oj4lXL5eo2qqA6cKBUVy9oHeGArW2teMFkYuC8i4hfmX
Cdp1PJE1gvOOZmRmZ6cjDA4lGO5/5j499ovGdvHeY92Z3tciqeDfn5GECyKD9bgc2uoPvyUt2qUj
RMUMIfVZbfS6zbRWnntx0sEmLz+x+sgpBIkLcRHkg06dQ3RZHt8UoU4x4twY8g+12ZMnDsJSWI3r
ZhOrBnA0q5YKzwaxmk3+dLjSqESZwMXHIYAPTWObCF7FeK3Ae9rRk47Xj9VEUvN/si6jKJLrX8Wx
4PWKpiBUSYNKxb3jT8+QDDbMFip+GW2ay7TQ8KDbs7h09tMZcstAGAho16dmMzIYN/mbcEeGBYeK
IZ/bM/1WJjMzfHFWiRFNsyd8PWXz+aaZw/OELYeBwkfdd1NPjUfpRxVKcRSLUoK1fSlWf8wY7hF4
dyx+At6n7PtNBpBbwqAVAcV+yfVsULx813hv7deVQYEDOWMcGACOJY7AFB3+2OMA8L/K7uqYS7MH
NLuJwXNjyEYWpkpvpdhRdzbtqwH83NzrrBYknup3BUXafUqNDiFwBtnOUuSL2WR/JtVH+LZRp/O1
sP+dLviGz8OJNXje8KBOZijhEdYWnqVSZURIjbGwpWTFsbmdrXtVfU0bu+T/mUkKbShNVTFa6RY9
jRs/1Ib6WNkyUpYHQoWn7hPoIu7+NqzsWnULjKiO7TYDHmrqbLZ0d6/dF8v3xZ2EVQvvWevPCrYp
7KYLsxke/wK0Fs+97Vr6tiTKvKMsh1w/9Rbsp5rBjQjVlMiZN2xBskdjbIPuzxVwDq1bt5nnge9X
xvMIWG+UBLiD/3PnW7xTHeMFVF6BqL89/g+Ne1I4HYSeLXinvD7Snerb1gXsmvGgmiNA+fu+Pdd0
QbxLSuc5UsG5sD1K5IY/dm9s+WhMTOs93x3oFQ7xOfbE82ArmvIYuhUQ3jSrH7ge0cQnvWJbMMhi
NCEWR9rJWK7ox195Qa1DctkhAb83hKUaITsrazUSVtOAoCVKCiEw0z/ZzQeacaKmRf07GWAYT7cf
z5uLy0c3UQd7/Drc2O3fHdPP6xQsE8DZr93wabcKRwiIBOyDfeKTMUgAWtMqU8JRBusTQDDIpFCM
ZzT4bUGm3ytyMArMcVKNaJokLfrFJU1aZqGvkX97pttC617P4uvjBtP2H25pPKM6M0W/WWX/WCWZ
X2sCQm4vxu5NOMq+frijm66sdXfOQsKsFvxrX9bF0fxy1PBUHbgT6I0TbnFt38W91VOlj6yFVOAq
WONVQdIvHA+AXb2Saf/L+hI87MxftHlAwAI9GTYcUag4OvWsHsJJE+n7OloYeeEE+3S65PcBcURv
5zrXxks+OMydoFFOAnPE1i2Zuk+PkdvFt2dfzNlj+Tpx0iXGOAowajMgz9h7OV+uXFrwukPT0hYA
HDJlSbxxQGA31pd1YjpmQixwIKO4OSubVYJuu+r24hgqfDfyZlmeZ8vwBgx0xCpgsKeQAdzCZkuF
aLKWAhLJXh+HcJHpzGeYayxgcVYprqPumPf5jwU2HOUBT2RyWw9BTQmZjZOAKUzJLLQThUCDvLS7
7ej2fat+XnnxloQZH0g8cPY5ozqvBm3sHCVF+gdSpnZYGlXZhNsL/tWzSpRPaRqF02ei53qhAEV7
2HH4yC0wpBP0Zm/irRJRU1+0mj0SLMOU7fA3XYpvHOc1+1IG39BjnQtHlHuY9x2fqDcxZy9wV0av
oDwi61qdnFT/P2c4V5cFhnOu/79tda8nQx5VNEyan6FlZwsS1d4nEG3yj8QdY58Q4tsb5M8ei7iq
CqpWakUwT5u/tRzWF4O07YjkJz8NZ9AiqStRJN966lqjO2+HKn1dDKm0t3Mi/RIko436JGxhQsWl
zbhhoLT/2NhzBfJgt4HgOwUzpXiOhVBcUh+idlq19h/m039ZVVR5zteVdJ//XPhfERdXxEMLJLZb
ouIxWXWlChx6AkBsIBaQQBqQeQCwHrMR4pNUKYnpaHTtD/StGB4LuCqtoEnVLdSpmUgtLC5UG0Az
Ooc+ySkj3o7uaptG4luncSPdCZlLIoe4iXJD5u6X0ji8uyVm66dziPD6X54SF9JesUATnuFqzvix
o00+KTk+0VURl915CFe0NxMa22jya4bMVAhyFAHUNtIQZc6qASFQSaFKOcbXyvv9Ue1bXaAaJMJk
uGVXMWOdAd54Xo4O3YSj51paIb1VVaHHRwju5i5uv+JTIdoYymtl1Jzo8mJK37w/mO1pCERb7TIe
NNFbX49zqMTJTh8pEqb9v7CBNTRUZ6ViOLgc2TnZCuh48lD5L3pn4uo89zPgwjBSP2RUC0/3pTPn
EAH0DibUCzqYVLeyhfUN/xtvvuc09bm4oGus77nQEjzG9K9ZfUR+9+qUHthXzTYNJdvj9m15y5EO
MQAyuVlL3jUAsQwmfPWzO9qOlYZ9pvtgdrNSeESlnpXyyQ7aK/6aVLwGTtuIQXQUNJ9vabuHlWKQ
2EC71Q5Gxbw8c9krKxyp2XbK7DwRLeou7j0tzdomEcgy84Gv3LjTr/y+tlplF0uhgTPDzvu6AZZR
LltOnSJbhX5QMAFKhldSDzE6p35LRIcl7TFbCzY2im2+LxFVK318gL0pkQnUAhMtpz79semetaHc
+DCg4/K5RIWVhnKndCnpq0FBsxbjbsZ7i5NpPkc4BmN72QRFjZZY/pU/DjZQ38Fr63KQ0U6B8BZO
0rtNtyeKFpQsqV1ZHJFVwcF1dpIkxYiPbL1KO3uMpAkRyKqLLUtCiZ3pkLthUadEs5/2YdO4tr3E
rZKeeGbOO0lDCoqOAhDQt1PaUuAqn1LrO6rnc+c00At7S7BSX9U+HilhUIC124SLDu+akHG2AOAy
phuxPExKtikNe1dSdXt17RV3ZzPj/Nv0F07pj6r7o6HdNiUE+/rg7IdLUffwKn+NS20P09ph/Qss
9QzQwkEi3d7ZCMm6N7SUCWfDwrDxBxlvFSL/2/1juVwC0uJYcq531eJRG+5/fhwduIh+nFZ8jfy4
M7efYTSBo09oi/sEXCEs5TiKtftWhdLvUU+4Z23bmaKrrUQDeZSyz4H6IK+ff9hiAJX2a1BR9heR
M6KBhDOs5iSzuRsnAoePaJJCiPeH7KO1LlApiJo9+2peoo/R0UpnTD1MqfHaZrONRFEmCsyMy/Ia
hg9NtjQ7ZXTbfJUCjIP/OhiyNtNmoX24ecYDIwi5dkk4x3daKoOjy8g2p3YCdz8LYOJYPiLgpl0k
PuLS88gYlqGETqvftNN3neRW+zjw/q9JtXQHyejvkEyelhCbf8InVEpgKckWs8QX+hZoH0EBoZ//
Zz4hGoIUj7Uxjxsavj7MlixogFAvexXHvXulT9l6MoBtJ/4MPILjWA3io4E/tzLNYpdzT/bUgcpF
s8RV/uVo3LNK2g5ZaVXyPe8Ws7uJBlKs/8/oj/Ab6qZenwYgKwroqv7KeHqy2vo8rE8OsENmnLsE
1nGpvMr3bdYE8YpTWvxSIKj37auwO1dpwB1feZ7p6TKvlM6XvdvrM7djlCKcETyXi8F5ewCtRz3J
rIR8JqbRNTatUoOORv9UrL9muqurQd0vSO66LSz0JsnTd2LiPoFJ0tSkF0Fw7uGIBFtVhBzNShTN
dUH9vEkjGMKRhyRXe+A/9FcMCJIoNlnutg7dNtixLQj1auzi8KFA2regf0wXkn674SAbO1k323tB
WWRyKuLroOTIVI/gCrJUaNzhhDxKYO6vQesmxUpn6pauzdPUjX32ctMfQ/aKJI6spjgDqzCgZDHM
ZHqMgW4eUYuY/KvL9G4yMytT5/HTqrQd7gilxpnz5E5TnW66iCzENx/UWGv8AjF7qlWKxbfc9Tkp
vPf6nPk92mC6odLdGXgGFP6R3mt5RGFFOTKo+nnEBQ9ek7fI/+Oc8MV04CHBJujhiFlWW3kWQjtI
IvLYbRPPGj/5wkPoGzQPbt/bThcZWijba78VrJ23H4CDNZwU6L9r1NOYfJTCLoXvcKGh1I+AvBzx
K2amL7D05L41jHI7p/48hXLX/XapUZaKTC8NvB2BBpJ5n81oxVDn9N/VI1I8fBl/Nzl3WC/jdGMp
pclb4Sit0M/+JFVWGJLbI0KYC3Cva/EC0CdQU1hKHgP0uQjFeZ/yU7nWf54niSWpwZ4XauN/0NHh
bva7QV5Jhqsfx74sfbWl3+OTvNvjsOLGetOTBpvzu2w2dv4kI3A7bRV87qkOYhwx67WGqURMTQ==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_init is
  port (
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_1 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_2 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    mmcm_reset : out STD_LOGIC;
    data_in : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gtxe2_i_4 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_5 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_7 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_8 : in STD_LOGIC;
    gtxe2_i_9 : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    data_out : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_init;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_init is
  signal data0 : STD_LOGIC_VECTOR ( 13 downto 1 );
  signal gt0_cpllrefclklost_i : STD_LOGIC;
  signal gt0_cpllreset_t : STD_LOGIC;
  signal gt0_gtrxreset_in1_out : STD_LOGIC;
  signal gt0_gttxreset_in0_out : STD_LOGIC;
  signal gt0_rx_cdrlock_counter : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal \gt0_rx_cdrlock_counter0_carry__0_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__0_n_1\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__0_n_2\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__0_n_3\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_1\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_2\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_3\ : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_0 : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_1 : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_2 : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_3 : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[0]_i_2_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[13]_i_2_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[13]_i_3_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[13]_i_4_n_0\ : STD_LOGIC;
  signal gt0_rx_cdrlock_counter_0 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal gt0_rx_cdrlocked_i_1_n_0 : STD_LOGIC;
  signal gt0_rx_cdrlocked_reg_n_0 : STD_LOGIC;
  signal gt0_rxuserrdy_t : STD_LOGIC;
  signal gt0_txuserrdy_t : STD_LOGIC;
  signal gtwizard_i_n_0 : STD_LOGIC;
  signal gtwizard_i_n_5 : STD_LOGIC;
  signal gtwizard_i_n_7 : STD_LOGIC;
  signal \NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of gt0_rx_cdrlock_counter0_carry : label is 35;
  attribute ADDER_THRESHOLD of \gt0_rx_cdrlock_counter0_carry__0\ : label is 35;
  attribute ADDER_THRESHOLD of \gt0_rx_cdrlock_counter0_carry__1\ : label is 35;
  attribute ADDER_THRESHOLD of \gt0_rx_cdrlock_counter0_carry__2\ : label is 35;
begin
gt0_rx_cdrlock_counter0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => gt0_rx_cdrlock_counter0_carry_n_0,
      CO(2) => gt0_rx_cdrlock_counter0_carry_n_1,
      CO(1) => gt0_rx_cdrlock_counter0_carry_n_2,
      CO(0) => gt0_rx_cdrlock_counter0_carry_n_3,
      CYINIT => gt0_rx_cdrlock_counter(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(4 downto 1),
      S(3 downto 0) => gt0_rx_cdrlock_counter(4 downto 1)
    );
\gt0_rx_cdrlock_counter0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => gt0_rx_cdrlock_counter0_carry_n_0,
      CO(3) => \gt0_rx_cdrlock_counter0_carry__0_n_0\,
      CO(2) => \gt0_rx_cdrlock_counter0_carry__0_n_1\,
      CO(1) => \gt0_rx_cdrlock_counter0_carry__0_n_2\,
      CO(0) => \gt0_rx_cdrlock_counter0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(8 downto 5),
      S(3 downto 0) => gt0_rx_cdrlock_counter(8 downto 5)
    );
\gt0_rx_cdrlock_counter0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \gt0_rx_cdrlock_counter0_carry__0_n_0\,
      CO(3) => \gt0_rx_cdrlock_counter0_carry__1_n_0\,
      CO(2) => \gt0_rx_cdrlock_counter0_carry__1_n_1\,
      CO(1) => \gt0_rx_cdrlock_counter0_carry__1_n_2\,
      CO(0) => \gt0_rx_cdrlock_counter0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(12 downto 9),
      S(3 downto 0) => gt0_rx_cdrlock_counter(12 downto 9)
    );
\gt0_rx_cdrlock_counter0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \gt0_rx_cdrlock_counter0_carry__1_n_0\,
      CO(3 downto 0) => \NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED\(3 downto 1),
      O(0) => data0(13),
      S(3 downto 1) => B"000",
      S(0) => gt0_rx_cdrlock_counter(13)
    );
\gt0_rx_cdrlock_counter[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[0]_i_2_n_0\,
      I1 => gt0_rx_cdrlock_counter(0),
      O => gt0_rx_cdrlock_counter_0(0)
    );
\gt0_rx_cdrlock_counter[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFB"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I1 => gt0_rx_cdrlock_counter(4),
      I2 => gt0_rx_cdrlock_counter(5),
      I3 => gt0_rx_cdrlock_counter(7),
      I4 => gt0_rx_cdrlock_counter(6),
      I5 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      O => \gt0_rx_cdrlock_counter[0]_i_2_n_0\
    );
\gt0_rx_cdrlock_counter[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(10),
      O => gt0_rx_cdrlock_counter_0(10)
    );
\gt0_rx_cdrlock_counter[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(11),
      O => gt0_rx_cdrlock_counter_0(11)
    );
\gt0_rx_cdrlock_counter[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(12),
      O => gt0_rx_cdrlock_counter_0(12)
    );
\gt0_rx_cdrlock_counter[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(13),
      O => gt0_rx_cdrlock_counter_0(13)
    );
\gt0_rx_cdrlock_counter[13]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEF"
    )
        port map (
      I0 => gt0_rx_cdrlock_counter(1),
      I1 => gt0_rx_cdrlock_counter(12),
      I2 => gt0_rx_cdrlock_counter(13),
      I3 => gt0_rx_cdrlock_counter(3),
      I4 => gt0_rx_cdrlock_counter(2),
      O => \gt0_rx_cdrlock_counter[13]_i_2_n_0\
    );
\gt0_rx_cdrlock_counter[13]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => gt0_rx_cdrlock_counter(4),
      I1 => gt0_rx_cdrlock_counter(5),
      I2 => gt0_rx_cdrlock_counter(7),
      I3 => gt0_rx_cdrlock_counter(6),
      O => \gt0_rx_cdrlock_counter[13]_i_3_n_0\
    );
\gt0_rx_cdrlock_counter[13]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => gt0_rx_cdrlock_counter(9),
      I1 => gt0_rx_cdrlock_counter(8),
      I2 => gt0_rx_cdrlock_counter(10),
      I3 => gt0_rx_cdrlock_counter(11),
      O => \gt0_rx_cdrlock_counter[13]_i_4_n_0\
    );
\gt0_rx_cdrlock_counter[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(1),
      O => gt0_rx_cdrlock_counter_0(1)
    );
\gt0_rx_cdrlock_counter[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(2),
      O => gt0_rx_cdrlock_counter_0(2)
    );
\gt0_rx_cdrlock_counter[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(3),
      O => gt0_rx_cdrlock_counter_0(3)
    );
\gt0_rx_cdrlock_counter[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(4),
      O => gt0_rx_cdrlock_counter_0(4)
    );
\gt0_rx_cdrlock_counter[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(5),
      O => gt0_rx_cdrlock_counter_0(5)
    );
\gt0_rx_cdrlock_counter[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(6),
      O => gt0_rx_cdrlock_counter_0(6)
    );
\gt0_rx_cdrlock_counter[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(7),
      O => gt0_rx_cdrlock_counter_0(7)
    );
\gt0_rx_cdrlock_counter[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(8),
      O => gt0_rx_cdrlock_counter_0(8)
    );
\gt0_rx_cdrlock_counter[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(9),
      O => gt0_rx_cdrlock_counter_0(9)
    );
\gt0_rx_cdrlock_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(0),
      Q => gt0_rx_cdrlock_counter(0),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(10),
      Q => gt0_rx_cdrlock_counter(10),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(11),
      Q => gt0_rx_cdrlock_counter(11),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(12),
      Q => gt0_rx_cdrlock_counter(12),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(13),
      Q => gt0_rx_cdrlock_counter(13),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(1),
      Q => gt0_rx_cdrlock_counter(1),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(2),
      Q => gt0_rx_cdrlock_counter(2),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(3),
      Q => gt0_rx_cdrlock_counter(3),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(4),
      Q => gt0_rx_cdrlock_counter(4),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(5),
      Q => gt0_rx_cdrlock_counter(5),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(6),
      Q => gt0_rx_cdrlock_counter(6),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(7),
      Q => gt0_rx_cdrlock_counter(7),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(8),
      Q => gt0_rx_cdrlock_counter(8),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(9),
      Q => gt0_rx_cdrlock_counter(9),
      R => gt0_gtrxreset_in1_out
    );
gt0_rx_cdrlocked_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => gt0_rx_cdrlocked_reg_n_0,
      O => gt0_rx_cdrlocked_i_1_n_0
    );
gt0_rx_cdrlocked_reg: unisim.vcomponents.FDRE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlocked_i_1_n_0,
      Q => gt0_rx_cdrlocked_reg_n_0,
      R => gt0_gtrxreset_in1_out
    );
gt0_rxresetfsm_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_RX_STARTUP_FSM
     port map (
      \FSM_sequential_rx_state_reg[0]_0\ => gt0_rx_cdrlocked_reg_n_0,
      SR(0) => gt0_gtrxreset_in1_out,
      data_in => rx_fsm_reset_done_int_reg,
      data_out => data_out,
      data_sync_reg1 => gtwizard_i_n_5,
      data_sync_reg1_0 => data_sync_reg1,
      data_sync_reg1_1 => gtwizard_i_n_0,
      data_sync_reg6 => gtxe2_i_4,
      gt0_rxuserrdy_t => gt0_rxuserrdy_t,
      gtxe2_i => gtxe2_i_8,
      independent_clock_bufg => independent_clock_bufg,
      \out\(0) => \out\(0)
    );
gt0_txresetfsm_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_TX_STARTUP_FSM
     port map (
      data_in => data_in,
      data_sync_reg1 => gtxe2_i_4,
      data_sync_reg1_0 => gtwizard_i_n_7,
      data_sync_reg1_1 => data_sync_reg1,
      data_sync_reg1_2 => gtwizard_i_n_0,
      gt0_cpllrefclklost_i => gt0_cpllrefclklost_i,
      gt0_cpllreset_t => gt0_cpllreset_t,
      gt0_gttxreset_in0_out => gt0_gttxreset_in0_out,
      gt0_txuserrdy_t => gt0_txuserrdy_t,
      gtxe2_i => gtxe2_i_9,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0)
    );
gtwizard_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt
     port map (
      D(1 downto 0) => D(1 downto 0),
      Q(15 downto 0) => Q(15 downto 0),
      RXBUFSTATUS(0) => RXBUFSTATUS(0),
      RXPD(0) => RXPD(0),
      SR(0) => gt0_gtrxreset_in1_out,
      TXBUFSTATUS(0) => TXBUFSTATUS(0),
      TXPD(0) => TXPD(0),
      gt0_cpllrefclklost_i => gt0_cpllrefclklost_i,
      gt0_cpllreset_t => gt0_cpllreset_t,
      gt0_gttxreset_in0_out => gt0_gttxreset_in0_out,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gt0_rxuserrdy_t => gt0_rxuserrdy_t,
      gt0_txuserrdy_t => gt0_txuserrdy_t,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i => gtwizard_i_n_0,
      gtxe2_i_0 => gtwizard_i_n_5,
      gtxe2_i_1 => gtwizard_i_n_7,
      gtxe2_i_10(1 downto 0) => gtxe2_i_7(1 downto 0),
      gtxe2_i_2(15 downto 0) => gtxe2_i(15 downto 0),
      gtxe2_i_3(1 downto 0) => gtxe2_i_0(1 downto 0),
      gtxe2_i_4(1 downto 0) => gtxe2_i_1(1 downto 0),
      gtxe2_i_5(1 downto 0) => gtxe2_i_2(1 downto 0),
      gtxe2_i_6(1 downto 0) => gtxe2_i_3(1 downto 0),
      gtxe2_i_7 => gtxe2_i_4,
      gtxe2_i_8(1 downto 0) => gtxe2_i_5(1 downto 0),
      gtxe2_i_9(1 downto 0) => gtxe2_i_6(1 downto 0),
      independent_clock_bufg => independent_clock_bufg,
      reset => reset,
      reset_out => reset_out,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2024.1.1"
`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
GNlNeNP2E6l/J7IS2LF/ItWIQpPIQxmqiENbpRcUcYDs2zMQssAZwDKWmGxc99etc2e+6UESUZFU
1OoN93QWwGY5lwtShrTc9cE66KsLO3U82lRN16EHmi+RZOdYR9rf5z7OgjqfdL/T03/rfwU37+Qw
8V3wDE2i5hqr8BCHEwM=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
afRfkAd8cICiTJYylAj89MfRuyfOa9d/tbr4mvbd5mWPw07cLRhiyi8gIe4+Ayw0CiOk7/EjPgWg
Tg/1hR99BWRw4VbbDTWu69lZfP8CCWoBxtdt8WRuUGz7PLk6qQMaoDf1ogw0xyXTK96u8N6bef3z
7mR3YInM0ECnk0+8vQDLRo6ETSTf4oQYcckMpPfbPQ+NnHRvcHroYdddFGNYasbeUqwn7tkHnYO+
ADUKING4g3DhZO3UAIPZJggQnH3ayID9nf49Oa7bj//jtWD0qELIGFMQn48spke+RcfDkdFLHWhY
0esV8oRsRwIWfwXwqK6i7aIis3paozzKSWbiNg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
ZI9ECw+Xpdoykt5zjVXLh0QGbCnqQDaDo8xSAWsYltuhqetS6+j38MNsVABP/vySCUkxp7yLlCMg
506S40Li7Nmy7qD4YvDLHMe+fIgwi3Hb193aoDluxnxdVk/aJ7VOJWXd88+scqfUSpbbFv7rk9La
8woMpCSiDNVKi6NB7Qs=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
R1h0p6UTerhva5Yb3duDj5nR3CyyGXjeE4dSD+H9PUwvK6SElm7FlL9AjlrnVaE2OmUXbsHu+WYi
T1XMnV1ekw5xTRICAudvAhAJrZYMHEjujHOfgXPmKtXHivF59pFlTxnEtD+dnchOL4x/jMm/lCgH
k3/wzvZbfOaxaKFceauCooIRZkqZa5FA3KG53cEucbspEldbnPaAcHsiO04PEwklvjo+2XAiGTHB
YB9XEkRUOprwSG+TeOWFNvz4Yu2FYHffCsobvgsF/8dSJQ1SVv3rM1uHf3coB/s7NIgZ0ye03NO/
wLFRL+3OuPxWNTqHO9YYXL0XTd1rPaaWB/mCcw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
4l+Avk44zzaOVxRJDzFVTXMyJmyE6qVo8wAiHogr6M/NkYt3AgnEITBHDJ9dF4SVZQLiV7ZY6shr
asWzrbISsiBCAaZgyPZLr1zlBErx08H5Enf1NkLuocQa0x2Dc1DwG+mO+flli1++tKpmHM6ADCS0
3/8fREk+ga01VkXbOXY6B7Wt4G2O3LBdFCkEvd0/MEx5uYrJNrtpaeSd3h2CpYLsfPsxxbAzNJN1
3pvNKoS8QwUnTPy7oe/k8bFD0TOzZmLvT/dy3x7SFlB6oSYr7FtWMOOuH5B08Upu75qdujq2/6ox
I9BucH2FnuLsghHnPuWfjh4WTDPekeCy51G/7w==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2023_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
r1TCY1FBxgJgPdlaGrkCY3ZPZ26hJnsdsOKnUu75PgvjzuIkty6RkwizEzV4coW2cxRBcWf87Rmb
xbDdy2cDu4f2qP6QUVBsGtY+ITHBIqBPqY7IqmHvnmT8DQ7mta5QD9HpSpd8E0HAu+1hmp2y9R2T
j6cEj7KNzaIGnuiJLket69e1dx31L14fHppyx7EJTOKaIYVHOTGcZeHi6ozbitA95N9bUNiSCS93
sJcjWWTWEB9Cx8arBeHBMIflTsCrUzgwd6f7Lb59R/pt8Zgvgr/BoV20ZGDDy6BCwEWgGSw6GDBL
o5ppIN+dICJOxCzX0Ij4ziKkTNPa1OnlJpcXcA==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
U0o+hd53NUTkWW/pm++iDbvu9EzyJbrE8cgDdW35QL1go6ppv1cnjM7W9LNAUoYpay5lYpZFkUZP
qgnoVBG2BG64sAHeAjjMVH8aUuTYzhhM3n6pvSqx5Ez8rezjhdOlMLX17yWFSCq3x15rfV2si91N
p+FFtjXOD3BmuNjRLXeeRjrvt6sxfN9IwN0yEZYhwp2ukPLeIyrYpRXZ+xMErI/b7BKLZNIljMSV
Cx5wPeURNjIC7AKZOJmo8YQe2JGJEHps80MGm98CsBtbIXDa7uAxib3WWstVs/UbQGgqjiyNIOSu
11VEYQSI8nO9bNfJPsLpVaeo9V+FUKdFcBgKbA==

`protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`protect key_block
JlAVtvWu4RylemkeXAwQaOcauzsF85mjSoJqBa5/i7p1cOK0PM2Rfou44syNOWyUG43Mq5DdY2P2
hSdJ0BrVnIF/JY67xayAWorKfMTlvfZN3zVnvvFrLORWpe5MwPzwHomEMtQgK/WjTmMnFJzWxYti
gedoe+hxxEoZILnHN3ge9A2vnRikwcJETpOKLK13mQa1Td8zo9tsQuagX91Bw+AWLzMrHGR2bmQr
grI95HpSioNMY5CjomezDLSd80Rlkr+6SiaUQt3pqExFVEAsKNJdtppxNwugqojHWEqm1lF0wKuj
aLs/KfYF2Uv5wNjccSP2gHLWWaF5DKs5G5u4c4FjEiS/slCZgrt5Ymwc6XKQHNOHbPRsQcbbkPMD
V/YuoNzw1R2386n0pOd+vMD9nTtpB104VrwBNBxlGPPa0qiFM4+xr58vMzHKNT91irPu/EirEIjt
wEtLynnUeSKMKz3aAvXZPRYeEurD0fgZ6iczWVNZOsltSIQZjOp5ErOC

`protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GeMop+4rJGQJTsAts4IyQAkQMeKt+Fa026naAEt7J5/8nhAvWlIS4YQDNOLHMMUe8p+sUA4yE6+s
hf5STrRcmXzM45O/bYrkBzWHNqGcsEuAHfdUd9ANbBZmpncQ5ywZbXLX0oI4xVx55B8sn2gAj+p2
NQEPjYWPZXJNML4IilPsrW6TlgTi1zhp0JFBFT3O6yHY35dAseHXtMWYgjr0+6WBtVCCvC/z4bA0
UY58Skgytan1bUM557AlikG2LqNB6YHVUQihnx4ZProlAL61WY/j4joeXcDH8Tpm0cmYcUM5yUax
haoFS6FezWAqD4KpiUS3xpkUVET2QpN0NnxTvw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
1x9gXtJFU2nGQ9CWRMPqjLG5B7nyL7ZKhgWVKPrQTraMot+PY1MLqNEQ8371fSgOtWrUw7wBswaL
CBsGdy0dDZt13psueOntqYT0wcW3vUbYzfkMCsSjkk2yeYjnCYclwJKMNk3fGxfxpTLQsScOFING
ZyUHBaE+KeZ7yfdqvxwcpP+rUXdNX/2BGvcr+a0ow/nFsiCiOMdYIlAgllnBFoUV7lDr001O00Hk
1MDqVCRdpiO5q5FnggoIp1sVIq1C+JbqiuyPkv5jmlk3iIfZiHhSSYoxP3DJf7VPvlJ1Z/Hwz6Dw
/39nFCTotl6l9hqiLHTmISLBvSYZnjDEcOPtPA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
mxE8AqszO8jEcufcEIxsyswviid2I+vp0wiBEslkbc/wVr2vlpOaIaviDrRuTwIVx162O0AViWa2
3/lH2PLY1lcjenGETw7njsuJd6MQv4KihzbHhd1HB0TKpoL1n4E9u5PgNIGghcgNu7s2hE5rpffM
FAxMXxyhZQ3j4WBKTgRtbqcexvVSl/whSo87Oa/V48wwF7Z68uoZdaD4cdNihYwHMLOm0LYqYesQ
cScpoz43IdSFiQkBEM4YXuqn7BFIsJ7UAr+EkHAFxWYqt0IJ3+/OCuOYW+y/3p2tXT2YSGnEsOzZ
SLIsZpsw7wAiEfu9tVZJlTQ0OUTZqfQXK68i9A==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 18112)
`protect data_block
pPE/AeOZ9UPPOJP/gev29vz4vAy12ukSJ1lWuW+/trX3UqKXCb6mfQF7fZNZ/2mu8hjfCM/az8Xc
7eTzAdzRhNGu8HFD8sbbFbGrTb3cLp8zNp3ivYIquKrOP+Jo0pHAOy+QVpkmXOQbzZSg+J9D6TNd
hTf+4T8SWQ25AlzMsMlMqlkwxoKDVr/h2YtLFm1kxkw8zYqxSG3zVDl7NlOSQRK8rzGhYJncWGh5
K7EmwbQOShiLXm9PnKJTXLPB9l0qVggVhKucZ8t4v9Nd+LGWu0pUVM9lPcFZJwlA0wdd9cSSjSJ6
sJ75yQbx7MOi2jwonGDaIsqD/wEHFEaEn1JvoNvLgk9+9b83KoxkSYBMGOpBDoN+Ll5GHHwq0oY7
v+ATKtbSxwX7z/+4rc/nlC0qm+fxMa44wSIXEJeZ88TnesKq8BE7bdpS7hnIw0tPOHZXG6ax7syw
Jst4nuFh8FVtNTYhwAyO2WMeaPhVSLIV6rg0Vi5zmcffbPXA77zZA7Jrazn2gkDNIZV9OjFJeLfd
9fskkPjLDnZYwwARO8xC1DxSU1yJff/De/rQ95UOqgr5AxcLj+30jGbRj3wYkhrZ3EavwMTNhHLq
knS+fyv5zuQBsjI56Kg+Y2uzKZoqpzCemZDC5ty1QCXrN4+WDENov/IOXNN8T1xtP917d1Ihvfdc
alFyn+jh5ealOvB8nqwT8Ybw6GLRIFumcWaz7SFG1pMuVYjC59S23ySL/pcXe6dOFfSzv3jV05hl
uaE5cLPyUGQ3RVMAaxNC1ZFzwXDD4NbpxcJsSrI6HfPZcssnOAplTmVuKVKgryjXyX+sSKFvyWGe
CREmlTaNyYyC8BdOgQXiI820VXFZO9pycs+b7h2LZGTVUh7LpZ1t2NlIkg65eMJ7GoKShmPjFVRW
nop8di/5Vpx6L7To57WhWgQupO1T0IeTaHOZ88LrQVgZC6FENg0k6+1pjCYhQzZlYs4+zYqI181+
B82dmO0MeSKq+nglV4S235BlcisfpLhhWPqxog0fKPQ966KRosAe8Sml+EBdqyupnNatHA4koJKF
grZUt5hz7joHGdB5w7ZCwzJD90SZuN3wScOQrU3mlOy9WrNS2rFL6DYBC9/np8cFG6Ujp7RjaqjF
7WY90GrrxtZNVtM5qUPQCYyA+9oZV5376vykGTXFC6SCSc2y7lyqU+LPvEQGt0Hsy+j12Xt7EvAj
8sbgQ/eQPJb3+DHS1DAYujhK+oG/DlryiUIu2fGH00cdea65LXXrBMgKmlYCcYbIMRInaIxvcblM
hOkeeja0tWJnmurRXLc/7HbbxnNsgIJa9qnfhxFH/9xdpso2Djc+DqzcdtzmxJYR74xKFTPkpNvO
Cr8Bv+dOiaR1R5S7SDRhNzB3szPW38o1Bi6kEMsH+3BqXoGy8SScs1lsZNpoJc7aTgKUTUsDOTsu
+8RQIF7kzSA/mILhgH4dAms/fXVWUcj3s4m2BGAdL94AVCXHrGFbQwl44CBRcxlDLCn/1ELA8dLo
Sco1NE7/ZLg03aUmTN1CaKTbpGwBCHvUk5UNEtfJZ2uAqAfvtvBF2e47+8sdpXRVBkNd7/NmzXbq
bKnmHQq8hUsfeud/lVHXHmA7iVGZfPdyT6eeZT75b36a6gRzu5wC7BjydPhwvyhoors88jfWXCQb
2QpzqHFs5uyBxgDJyV8CJs7jGeF477hgyH+e1FtRjJD1PLwUIONIdz+M5aZ3eCNMPa2VVQb2hmUp
Hb2F5xgx7WMPyfio93jFG6bfLP1wN3ckypI+tcFheCi8prtRuxxjeqhFKq1WgRX6G5X4se3Af+zJ
m4w3o97sLbDFOz5nusvmp+gnXwkQKJKoGGjwhpXpWUAPxvrGFVJgRi0KyQOMDmyIS7vy4TUvVJZu
CwPQ9FBO2Ll1Inlm4gMFvEuYCqCde5y0KbDc5KRIThKuIlidBwC0abv2xx2FAEmHQiMIuUH1j4PB
fAJRNfQ2xRx6135dkOtie9YeMVbuxZli4ziDaiCPc+77JiHOZRF4TJrYUOEUfFmmyeDfcVud4PIp
PObSNSOW5ZKeobbDmWF1t1EgmBbG8yiUcaQFCtGB8DFrtL3D/ia8Jx3HvQJagip61sWjRQ6c8vjH
gQHww1OS0CacnAYOmXksgcgwj8+aFiL4YmVE5iQECuB1TQubaZz65wSOR41W23UoxxQWE/Z+dpjX
Xnl5n2XooopT2uQjag7hDd8yXUVS0gqxTSYaA9+kiy8MuioeeS9mkhkQ3ZOKi4rvuMnd7cz1HjFO
p0GqslY08PjlKZiqIISvKSqufKl6kUi+0NOXlTKf1CBmkA5l/O2qgdwNtM3M3Yh9k9EF9PwVHuLq
odW9y7kDw3bUhumKhoRbqGi02umqzryAtaXoZQ0aHSIgHKpqvfCRzkj7LuqKAtKhzjcCePryxWzZ
s3iYFigoZ65CNd1lEkd29jWVQ5otVkhj9W05flBaq/5NQyG/zLs9y56FyYKMhhUAUpLvS+PsIlFs
jp9OmSFMpgsjYoQKVbfRfUq8PToM1p421/HVOWv4a+du8VnhjzbT7ik89S2fcFZNohiHOj+QN0GB
142Ty2e27oY2TtPGiBSccDmZA3UdrwXAljQjUKuTeLMdp6GEPYRlWLCuBXxnhqpid2oVS3Z4r/GR
jowLGCs+3xRBO4xHkl9N7osqmq1Ko2n74k/czTy937yhn3vsDKNIjfZf8/I5jdyeg45UqH73xKea
ihLy/Y1atVmIyFXXqp2D13CI3qzZbZ1IZunISkku5QcbNMYDuZtI2rHg8392dY+hNkN2lS8L0n3R
JAog9CHg85Mphf6hVnp0MOryvLlxl740KPK4PHBWUKUJ2kOzbXYtrzp8b3qCpZ907zWLmvbZxc5Y
x+cdv3ywkjS2qfCjyrkIs0lzuDiBl7Pg3/u8EcCocVH4Dfs94MhM3PjuZR1mMnsZLCq3VubboE3n
Oi3F5NgMVEcyD3dtmQIgiGr0gELLi2KUXm0WChvTYhvmdqSW0HkY6Gfn9hN9tVYtASeTbcgMzfgm
ef7G+ReO8oH8t931uPZV3Y+uyZuCDd96nh2jgNKibPW+mGC7742C/YrGRF2fCMmViZrhk7M3+8D0
vRfKOE1pq9h5G7b2F8yGdST7RxT05nviLYjwv0upEzfwuJkcfK7kPZ87f4OxqDZe1rCJfNf+S/Ip
lP3UggBN9q4B8KvBF46n0+gmpJvraoeJlRBKnrNJjr/nD2lkAX487qrcWWBg4koYycBpoA8XielR
S5x7G/3x6P+54v+QgOQX9NrxdoWUpWyo9R5mEgnhgpBuJWjBX89kYHAmBb1YazozOj3+OoaZTjZn
BOSB6IcG4KaiCox3WxwHNcI5srtOBhW9FL1Ul/vTHxKacATb+xJEEdv39e2ilVIx0EXWpoIVO7jq
Nyk/+jiRJUCfp1v86qN++igpk1F1+Ojcjm7cOvbX4ZwJ7O5Hy1/y2kByK/7g9Go9pRIWxfT2vD5F
rJZVc0zJEILpe3ZjGDJ4WtyGBHfJ00/+WH7yhBTs9FF0FzhX7C/JBr1piF9ausokx4e1KQRXaUOt
ZO5AvE5PExo6ui3qwZz1Bs8tc/pi1D11I5Rf9GMXuJidK9cnq+01E5HPJXf6RbKFuhg1bdw0A9ww
PLSg9g7SZ93kcRcWT4SMjzCDsh2UIIcbLYNVqAK8CbhG11mlK2uloENtBzVr4mHQq1Kp+1zmTDPf
YpB3LB2FYnO4W7BtVbIlLox5c4VXTHefDkWoWj1SAkSqZZN7TJcaSe+UPJmXgCApavGL5S3uBK3e
Ptl5hyqBTmSl5o0UtD4IzcTMLXrulPso8NgFqY32G38h9ERX9nvwWzf1NqpakO9qolu3X0d7vF9T
UPx/10nujwWig5YXIUAVMow+ywT0QpYD15XLcSBRpRb2eGjP+i5OVuBsuCIqj+WJ/qi/DC2fpITo
gfKNaZP7kcwzB96CNIL1rPKAJ1A5JwpHqWIjuJMT3ZhYQWDztZqLo7sNSf/GPLqueX/XpE/KNn67
rg7lE/JIuZt4YQsA7dgZWIFmmJjodWFmlLddbsktll4F002t+oN/kgxDY+nLtcOmJtAlK17J6zVs
mXGJH9x5l4RVrvJCrk6DZpcyVrDXKvY4jFquxD1uPXTQkBU+JtQZQP8aOy1+qnWd4n/wPcpcFuO2
8AmHFIkFt9mH7HQWOGR2nee0KzqYcGAJgPSyR5saKqZXJ6B4u0Cc76xBGhKvSha9YK8G8cF3GmIz
z6oq4+qOc9kWYcKRID9hnrMJv4Z3mRSkClTAXiAK9DpbBd66TNGFrCa2Z1e3ZczbcwUFCZqdQfrh
pl0kTl3sUnwlA35l68B9dBP0hDYx/jqw0jlhOC4w0oKKrB3JRfZ5fbQMhfPHIScXCAAS09ycetpw
n/Fsr26K3MAiYhqzOauxBOtSff1SeKmutB2BBp6eRUSeDMpqAuLMaD1lRqrFL2XeiQ2NfJ7BjXD6
a5sOM8Hgm6iTwiJmUD8qr4YRBuk2Wr02bJRML42s3Tk3FftMJS04zLQLrYOz7JTJa+F9EOmiRi/J
ZtWAXvsWk9YX0mtqwzCskUboBcpsqZFU+pDGAFC00X9Meft+xTJRfudLem4HkAec4FgOg9jzJxq9
POnQR64OM5A1UtecEC9t5vbI65YWECJTWCFd2laBWtVUlriD7zjEwQ1sT5Cn+4LID+OdVR+sISVK
DfhBOroW26Nnzzfw0yRT+NBK5v3l+HxU00hFPcYSX6zG6VchsRynOh+KmnlZtobwYu8ZsdkLpSHJ
0gYH0DSz3x4Y5aYzk4EbLsQg7huFnI0FwhVCf4aBQbpbiHF3zNdk+GRTV6i5pFfenH/EYy651OQ+
1Lqz6FbQ46TEkfR6Eh2c6vxy4BIQVSMfQ4raiz+mQipFw7OJrIpV4kfcb6okreEh7rmFhxeTwtPu
qrHAT1rjTAJWDRKTVvbBk/vPCnQLhdx1afzKFYNF3sFQV7uVpwPi8FmC/SrV/xbf1Wz/wtPgTnfK
bNEhC+7fvONact1KVwHU88OZn6Nr1ZS85tWmilJ0Iex/HVRLE2JiE83mwdvDkzo115BvFKhOXp++
/kR3AFHSw8U0ksnF9M5mOIUo1DmhvB8tgSsW8Ggldm/cisSWL51Zj4AqGQ/JmQfz56kpGQkYTD8b
JzrE0cz16bZP5FmjS9ulqDxSuyL5qSDyfcV8qZO4cM4wkaMh070MHSBK4rtKumlcfYt2Xrza+SRs
VV1GAlz7qNxz+7b6KfVsAPzY2vVBhAG30U69Tc9ma1VgNvbOsYsEMO1EUijlArAfpGObDu8ID05y
hrFzmIfjsjGza9aYGlzTxVL7vCG6ORSbleJLadz9XPt2nfmOGMoA/pLKp6SUUJOMtwr+DKFbhitb
Vpupg8xqcIyY3dMi6mr0g1CadXRRbQm8Om+8Sh1rxmbtnSJ4KQgmazLOq8p1oj2nweiVVD1PaQDQ
JoMwAjZ7p3FcrTDq9H+MKPXxZxBOltfXniSPH3KVVRt2HFLGO++r7D/xufCLdsg+l9ZVTv82DzcQ
zEYhvTOSpjg45HbmVji2HHfhZKM9u2kmWza4hai+iHggjYca+YSAJbdrOHbubmyKXyAjwvDoo+Ai
6yDdNtr9WRiyzothnlSG2vnxjP7gciFD6vtbQDCrzL4yH3ppjWce26YGESlPH53OA0sdtFLQyoar
YyEA4WaG8MO9cZeXt42lqWhfwuib4V5xbAzdgn4w2bqIERJTqx8LtFpTCCjx/JIsWdn4LL/EcsGO
bN0IqCHS6+h9crmhAPT5WoOYfRKVEBeIVCslwnEphZQUIVmSdQcfHyRj70c+1J0Z0YJA+vDEK5kE
0Ftmq/lJ5ax5Oe3BVsRauZaG0XLdhFaCFc1ANL+XPiquV8kSB6tmCjXDLbZFHzzDpRy4LtkpEQ3y
3kd8HNrzHf6uJVsOWdLDzafSTCFw8wZ89in79W1QyNFuq+V9ZYwAEQUHjOoMY8n6tMG0dhlJWYKo
JeVsZnZzTJbRm7HHhljTOBeNEYoR4lKvI2528XVY6rzStEyqe4IY/tRVo700ZOWbhVALtdw8I4lC
JtaQktN7G64VQut/bgqaHRr4PtEhRG5lyQCP5hnkKn30K8Ky5nQPDsb64lvp5Xx2rv3QabLWKM7Y
ejYPX0KOLLNQkKVcnef4lRmnTt1e7W2f9eBsqrEEBbqfdp98gutGS2wmI2kps4mOF59yZP/UigfM
u/R90dKn6cR+2Iz0vM45Cq+WYhOprGgoqe+jCsbwkOOCuwBhaBqZYid/UMnwvUpmQivwHzNMbIN4
enKUMalQ56cXkyvLjm0YmAcrWaVEb8v0Q92BPr09aofWEFc7XBwcFqAjCMurmYPycMJDYeDbZT9u
h8wLSw3h9b8T77ibE8zcO4XpVQv6SdBD9ATJcYKkECxF6Z5Fqg0BBI3aPJStijypaW3FJt0JCKA/
FJFvCgQM1L+idzS6PUQLPo8Mm+oElPk6BFnX2FwhPWXdFowjDLfEGNaSH8UrCNpaC5LMkU3Pu3al
xF129TiLU5BbEuJaL5QQMAG/BqpiNvhtCJP74rFsthnAtX6JmG7MzmNcQgs5N2xwUmtQ/fIEicfc
tzT0+BNtODwhLI2ighysCzvXt5ZFsBVFR/17kFdG5v/Nxh8LQyAK4JuvvD6RY8/ZcHYswtI7zzkp
s6kI9e6Iai+y4Kf4+L2KMrudm0m18yvzPs+2PlkcsBKHWA8+DASiCe+ztSX/2/SwINkFlAltZuJT
wl1u6hy/OGXmm5sU/ktKITX09SQmahi/ST79ULDQPiLEK2+cYdWTmU3H/DHdKuUNY56JVong70EH
AXy6kTIstiRNLeeVgAdf1MGQUr1Tye/JlYB+IPyCItS85CUBJUqdvTAA23fMjOBromHdxQIMZg1D
G+6weU12cmx7Uu4SizmUf1OGYkKBPRstGHL3KX4wwdVJRPPxpp2w6ZuI6nvezmMIf1x3Vs/7k0gk
Rlm7i/rVrP3f62XaW5i227q1bkoDm6KJRbU7FZP6EAmEQGJDQ21T7x30b8U+VLNo02Oc2nWEuN8P
2bkSrKfo2S4/Tz6Gh6zXFF7eCn85iE/UK11iUolUrts4lnD2eT5nuU1D+6WFYL3qL5VCtww8e1U8
XNG1n5ENa+mYMtDCld5cZXTv3ig1pHfLIld4oy6IMyxrp3X/lIjAMymaUU0ksdInJA/rDJJDOSBF
8FM5TbTIBZazYD8U58pSWGe3+MQELG+Lfy66pCVIjLTgBoWARad8nadl27BU2w2upt386DfGGdqb
vOIDU6OL5Pw27Nmpvs/6N1mkEKWZayZ/ZGw80KkDL8ekbY7zG8j5Ycao0W5Aspu/xi56XpcIGE5O
Mm+FZ7Y33fVLyOWDa/f27WitrzKEVKyI+OQE69qQ88mA/DEIQd9cDcaAVZKd+fRilYsnKwUrRcHy
8630lZKL8XvANNbVHGGP3kims70LSjajn01CUBwuo2dsku0zRzY66mEwQjt9n0JSHI0IETBXg25X
ZR2nJFqymc+kkOFp59pBIgelTt12gKc0kU2M9L+I0LSFR5lcL5XgpaB5xdPcWVuYJvP32P7Kra/3
h61gW+r6NmAZlYTk5PuPxloykWC3E0Wz4u2p0zuqkvs59ugf5I9WRHYVeUjBtOBdO/0YHM4uh47x
9tIWZFJ2YqY0LlavBvm+FwieumOkbfsWgxGp/fku4Ge6SGYAYL+oP2aQarHVGWlWqVbVPBxofaDZ
/PgrU45/xJ36xOZ6w4aEve8GtWg0wgEvYNBEg00rsJxHK88DbSPmAMbLxQOGenVtx82MwcXkfWVF
XxwhWIbBI35MLjFGMHtVdB6fhpbsfBTwovJabtawqpvAWpcf0CUF2uGJDCE4vmyUT4vn5R6tHsgM
p5U6JB7p5aTocOmqnDGFlydWT5ZK+gzjdvI8gwBgudMIr1eNXokZq+ZxeVrvdIuq4U5sbcIv+yOl
DUy1g+UDtECe96qjZaqbg3X9oze3ZlMUBFweDre2Qh+QJg8ayfZik804pmjdK54DhnBftfsLJaG/
pXPCdi4fs5kcYynTgF/mRZrx6VlISo/fTOt3MvyrJ9dK6K3AGJu+RH7KRWgTkD+tqc2Bc2LQSs4Y
DPKPTYvKBuoLfTUdyQVMBg+av6h+WWcyvn5Pp33IbVV90iHZyGYP788nyHkHobCBI2O4l49NEuW1
YcHYHUtp9bMIHQP814jQo/nFvgYySCiR0nCXVOX15hxHND5bHjUDaobYAzfbVaBHmHlWDJsDqQBZ
YFM/+Ibct6kuJC3PjbxPj+0pqPK30YfZH8cekjPJUXtdpnh/QFOU2/GLmCoWO8Rn0a251hmzAEng
2nsYGB57VYnAv4MvzvhsQWEH66U2vXlx37MvYUlPzH7V/5CpIBggmTKdQiBWa+svHlE5Bum0myjj
79V3XMtyUBn8jEVIe5l7EW65BYTe494DONwaeGYJ6VbsXCbWqRnROQ+YRl7nw5QRCJufgHSBjuIF
j9n1HUbrozJnOlHpLQMyd/zO6dEb7LUWhGpG9bPxWU37TGruiWyUICQWIRn14tCf0hNwZKwh1Ffr
xvMfDZ8yDbLmytn0q7GU7p06gDoIabkym/0QCIHqsOh1d0i6kH5yocdA8dOyEFanmRZEqkHm1XrB
pc11rtQ6+IfxqcRkNYSTQxsxXinBwnKrlJ8DI+eqq+Z0GNdK3Pj8e+NgpPNnLPx7/yTtSa/d+2Sr
XZS8bZBVStVgiD3Ce5Mz5gzsWBSyUmH98dwJJ+a3vo5ifXv+Cm4LVC5cK1QmsOK8dLxSJbDVRH61
f4FqHnPzz1v1HQd9QavjDcvqSFGAnxXhkfoAJhXc7q3f59ZqaTWkC5DMorsq2jp8OcMb+n0RM+L9
8sSq3WVqFy/6NuiSOe4X5tRFsVeXzB2zXy2P6a7tNrqm2f6wazE2BP1ri7e/MHmFIizG9zzu4uOB
mc2AYr/HNn2BIQjAm60MvBtCqQkcQ9gXlQ9CGeuR932b9JUhPfU9OZuVH32uqVHarffD4lstSfKe
M8Dh/Ci6qGHokVqgQJAD4dSL5JLv383Fd5PuIXrXnes0Ux6Jf7STRsCi7Gsg6KtYD6NsLX78xh3u
PRKip7e+VFlZIK/BiVaymzarU5w67ueBXk2uua5W6BMZgmeGTP7iyHW6K+1FpwofcuIlk5PDye9+
O6Tt8KCFN1Jir5yone6LPhG18B36jxWipYQDDIz//J6Ul1i/kgoBGg2367o+60UuMW/kI1HMuCC8
HCOi+Eq4Fc6fAee+eh/loZKB/7u9HE2LvpQ1rmnZ90r4Br2ExbPNk9y8K+GqNR+U+X1yoOP8A+xO
yu3KobLOpXSa8kTEUkr69jPrxgddsf6Y5CK2e/WLXf/Xdxt5w6jU5fzwqPOD7wioQiCn6xFRrbFa
xyHBHdq7roz623moxMj789pff6EwDVSRWpvJG+S+jk5mL9DGegF32VuyOFhMpFq/GQ9724Usp3ln
D+WjBTPf6jvdIfUA3kpNpT7TMZBeZO1XdJX7KRGHGOifewkM3+xYFnPKEKM9rILWO+GucszVX3kT
KWv1vLs0KLZDRS0tKlLmkBpqXzI6pRK7e2clQvHdSpAlBiPOtrnxuCcYIzZOcrifBsqWvbwWLUoE
CPHdrNpjkP2+j9B3/UTuhcuL+acqoOx4UtOz5ppUlsOc+IYhThdZ5fyP9MkS9NSUBDWA1t3Kog7v
4rMc2ipPzXgWGekp2bQesvmGhaHjQuN5ZjhL4L4Y461zKLa5mKmoTduXZCUb+woGAo4Vyyp2D0gI
klRsv7NOXl3IJe2SS2wFhd6dRj/FU34S/5QeeOcsiLTdV+jUjFvH7K/1BfIUreztZo529ec7n8Sx
uqFEndT/blFC/QTzA1g31CNKfIddg/yHjRk4p8edE2/NoJEg9qVrqUFdXFiDvkodKx1yw1Xz4wfc
qfQpKB3CJY22anSeURx1+OO67xzHGGGEiEV3S8BeRHzipYKsOORTRpghy6hqgZuzx8jgAPpfxrih
xhzriQY6N5OOQkk7BlA0Xfci1liX7bm4cHQgmNqZ6JIiW0SWKp7mh9dJcHshZF1WAsX2Qp5tRAiU
W/P7bzv48C/iEKALgcWnwN1tbPdBwOk8zN0q32kKJ5Vpk8grz4nkds9pwjLmwp7EEsnE3JmQ+yQz
wcho9+XdnDIR0Wg638spLNEOawGixdSftl+x0VKoBnfzVYtdlM3wCt8mMwvqnToSROsN8mg+cLy8
gx483sYz1Zqo2iXGgBmOT6h+mkP+K4syWFqVaoMFGGWP/2VOlARXqNDBon/aBzM9Xk+afZbMCHm/
x9Ag2kWso3gpMHkf+B+4cq0K1GbI/OVdaz8Q66hhlyfvtgUaDXZ1ouFcSuOsxZarys8lLFZvTtJQ
pFjpYFTjPDEmXiMkyHJ76Z9Tia6GtJ5pf643zKqnExaPzggnbGOmXBbw4B/LhGEZzlxK9RY9MWLS
uuGLfeG0KLkNelyv4VADTYy8R798Gl7ztZexiKyWIdECpGCPsh6Lc7mKX98sdcQhtTbr2diuEt5l
pcCm/loMpEWFNB1T3Ds7csXtJXtC2+J04hzmsF1KWaBeXZAmqdE3ZgkRfPi7sTSF/6G+wVF89XsL
no/5vCxXK8UjY4nA+LQ7teS8sRtF8T1dI3aUUdrMXhifGNYmVGt6x4nWSWpYSl5ldt+B2+t7/vmw
lPIOo5rs5pv3usdildWg3Ybf9JQdYPQl0gfr7sbQQoKMpFexyXv7pb0nTYy4DX30DSHhmrVdRAS4
Qi/abwXynxgYHO2KDDPKj5RgSFmF5Uhj/4Enlzf+jxLbkWlKbpI6jyRWYQZnSyGa8wWf4IZQ+tBp
0/P2UK7AKPAYZ1YSu5YibxOPgRBz0C6bSwF2CdD3TAVILg2WNzQAvwTyA+kUa3Cd2g/rmdxLKgL4
v7o5Lp3I8QzwbdkkUV2nt8Bo7OSZJnkOLVnKZIM2WXJphkWMr6HFarC1L/Z5/pI+faVDVh3EGsWR
LmpGhjc2eJyxlIeo3FKC/jWtUIWlUs/1yMJhjzNmL66TCEN2aFTh44z9FKKTwW7Q495XtOB+WHzt
uzv+Z5q6rwyFILl44CYj/34NBpiX1ruglQA+RthvLP8GxOxhpTGCt7MuYUIkGpNXF/lXFLXuxb8A
ve7xbP9b13Tjqbm8WPwDZXTQXdIM2KkqhHSEBor6FdLex4PUYP01eSjwNaS8BvasLFPqigRGwryd
6bHEDllTNd78XRACrUoXlk/oxZvwg+Ti+LF1upun9DuRxmsI0TLELindhvcQ2RuXo4jlangBXx8c
z3ut67D7pL+HNFZZ0r8MQZMG9zXeASYB9y2l6GgasRiCKGUq/23mGnq4+hakYK3EX213BsGqBFNe
AOeUaibqxsAj4TJyBhpzqdqLH1JemLvRWrnz5ZT0AEm/BNq+BnoaNRXBakJHfBwjfOSgVB7PSKsi
Pz9MynB/GhaCO+BgB7wilZ1kxCOBMiVMGXZswLIXqBLd2hZFhIm79wZKE8n2Ypc5BMo91VeOeUKA
OPDw9E01m5tzidq0odhFk3HLHVc+AqGZwHc0wmRHkKgIjpjCrbbleoUsiQzoZNkn0AksmynrAIIJ
VuABCjVRLBEPgm03iLyQoiEATeRdahzeL7T+A2PtbO7dXrTLYNsYQtZ9MXs53TI86jvg7fLcf7Fu
EPlxRpkOtm90GsF5YQAzt6G57JBpqFOrXLCLlZFT385Re1zV2HiYAmD9CkVUP45yEu+Pc6b6SLwD
ThklBfzabcvEww1eBWPC1VnwL8H3Irn3xg+F9Hg5k5ht9B8X9c/DPlVEyrdPxxqNSwv3JNjqmM0I
5TWhC1PF7KHxjmO1Whnb8POp1V1G2jNHMKhonUlaBTQ4SdGpyNm5KVl7LQajyBQnwqir/dxHTqVn
bA4Pg5bRxB1yaJXOcCu3aVDMXkRySbIrUfRWbaFuwpb5uYnm8T05cKo/X9g1lSKYtenUstK0HW1s
i/lkrG8kFkD0tYfLGt6IJKHlkQbb4+2b2i22m44c5+6Vs5b3/N/LhfQG8AoR2MdeGsyvySGIBhc7
kegszplj+XfmwJrH27cOir5+cjY/R3K+sG3sDUv7N6qUXY5wM8AJwjPbCoFAtec6KZFaD6qKcYX4
CSC938xN0bx8ZYmHhM5qtTW2UqYW709nyZ84AvJogZ4Oc7K9HwxfYs4scKqcWrJSphbBClRVeO6o
6rTx6dVU/nLUVYIBNsEMmhOQbN+tB1va+iv6ndCscZs/RqIjp7dOLxCtlL/SgLrCn4E7+i7wgi0o
WfHZ8FY9ecUGLp5FAC3s2y2quv4VS0D9jysCtCLjHR+06BYkwIZ+7IL90GUBWdUCZU/AdkNbWOoA
OfDXVnO7Y3OQG7bFwAGWQoJuqOJwfu1ak4MABffYoL9DRueBGjnssQGEvyjJNyhcDnxfRko8IF7y
g9i8DiyyMLzHPO5/1dEvWd7fXgD0YtqzkhZb/fFzlmSpvcr0Rp43HIPuFYwW9nbWkSbhSpxaTlA4
ukVLXUfIATYZ+ZeX/azOEmYHErmHAxo7uSSvkRhibL44tDCytWb6snPF5PNh8Cm46+oo0x7+MYVO
0EZlZdVcycq9POSqAomHl+A9A9KsUD7KrT1h1PZn6AVP4oVvd/WMG1ZV0uJ1V3sS3uX0xQrSiliE
4wr6myyciKDChohGtZUeKRw35AYKzzrRRfgfdBD0obvtl5VcBgQ3XOeBOu03F6nWqDsCdlwrlhCl
GB1hwuO1Qt1b/8OwGB3t66V0k75k+1mOCW5sO3v2568Xagvje7aPKfjfYRD4GFaUnqDegR+jOUjb
tL8YdaBwEeFlXcWj67lHkjLmKotRcSdds8+8e9TdbGsOiOkEakrnu+yP1ND8jQ4Sbr3FP9riZbMn
xdGqSuan/PB8sdXqqT+jDyqufBJ34eiKbEimz/IhoYnGv/TJxAknCTOX7XvEm+GuoR+mDREjrxo5
QjZxqEy4Mil296ncf6t6XzN7Hqb0wdCfMtKzt5ybs5UDz9v1hfb2tbJvr4meHNIsyXZTzKv6iQZC
cxqjGLRbxACdzH4P/upUqMpWpN3ZtS4JR+3lxDDArNg92V//OhbNtNDXgcylCvGR1Aji86PUOC4A
EiPsHemfbrYmgywbtqYT8WJcMT9J3/+xCjcv/MCoTEoo9ZAPCJ3W2lvGm7QEW1ytXlgWHPDL7XUS
Car9eytvbrQs8zAtR8PEWcGxAmu5OtF0p57iE5ZeAfziiFdEy0tjFDUOUzYon5wYR4tm5QnATivJ
CoP71p95NuCSHhcH3nro9/Aiuz+mFJqhhg4ODBllsJwZxXWeXAH0Kr5BW4bVSkKuLbKSUSu2E5qD
CJ1vn8TBrylZ28Lv/FSGtSl2ilhRERWV3n5Uu8wBDuWoHgIFSSaMzuMzUfT4l0Vdf4ioOkEhEOkA
ETVzckdXuwIT8V7AXOxQlT69Hr5flarnJ4QFzUMSWxTOEpCMlAf8egbq+OVUh8lKL7Qf7MXbisdd
h9jC5sp1PCTb6yqzDu5fcdBUzDUypcLRF0YeBtLXqKcEt1HD5Ovw8xQ+Lpcl6C5IhOjOBILF8mEX
FnharMUlvFA0EUTFrefKuj9E2nmfT3bVB5atEhO6P8JO0i24sTkrR7Ev1nE19noCgPSVhIMEkShE
GBfuyfokx9Ihq/xZy5FVYUDbV2SZEaNusr/6XYYm30zZNjWWgoNMUPsEB8YNVFBA36EG4AewSYaX
A66oIknRy3QBBgvyj1h/QKoLZj6rinG7PyAFjSrqGNbLhh00GcfUjL/X7Wutnm2RAQy5XLxXGlXE
cf3n4INqxJYW9YEDxPyJ0boAavC9UJ2ZNeRaPbYbyOlXAggBfZIknPUr+1dFMxXkReoOg0oa9UJP
0wBHzF8p0BNnpdTgu5otviQGPvYGOzBCyIyiHFdfTpLkOHtn85JqqxP/JXwu7GEWlFtC68aYnwv5
OJsQI4QXjzS9R0D4bzwh55OyVM3mgrJV8nyUclMJf1qn9ieplAUlIjl+hw2TrmNLXfl2T4IOJKy9
BLwj+jrfRCPAAh+wx7CKlaWtwJKXpXLRHTdYGh+VN8zwum2WIxHroXPfQYu3xXVsR8IwIAr/5T7k
jF0KHwoDk5LitEcVU/7732r7yqFpaP0x4C9cHefqBXd93nb0fXCKAVUSLX0ijQ5DM2Vkeqi9oDHU
gII5iEeiMgZ1myYwrOkHAwGEdYvocZMLeCAoIvAETWcrtUJUQPobvBt85NDGowy1stzQRViN1mw0
AGirVjyvnW1CpHk/jIduybK0PrZ2U9Ea9vTmFKsAxLk4Q/SvjsRbNqhnvlhteFA38IpiGfESYpo7
8SpfbaKOgO5GBwDvEm0KQCfQWQnDvobnC4GVgkVwLwoxF11cCU6z74GHaw9UR7WUfp2nwyX4ItqG
Q7nbVHTkG970quqvullKdLOi/pcYYrN+YM598d0Z4R7SercgvMtS7qnMZrlBD8u4aiPV8aaHkjkn
D4SrTZHpg9vUJK4wsiD1Qo4gJzwXzBY18UB5yv7t94iCsz3IiF3/Xtgb6DcpgKoIsKFsVkHqn5k9
qBJIm5oM6ZAcvq56VwGxS/HpEZNwpWD0b/9fkEk8XRU/O1ZaBOCtADG8Wj+snC6cC5EhrNS7eH5L
7/dq7K/zLTF/W3j2Yyvuv45iuDGqdrNmdvEtfFCauRLqthRR4eb6AVdiHsdzE36mxTcxaH0Y9A+o
WPqoVgl3SHs8x2BZC9pMiB+lE5a1O9KkkqvHg9ZK3+fgnh92i7++UQKmzK42SvRPBM1YgxP3r4UE
YZ9UHha7HLh/K88qKJwWfAsjp9uLIXJZkdKDMhLASKj2HimCiU+SJjJB2m1FOXbSFxwwG8Tziqqe
OlX8G/f2c1m1BYFZAihR2+1CdFefuiXiBKQ+AR/YLvlles9YdoTk50cKeg6x2VJQYV89OsvHgkXs
GIm3Z156r4iDcF+iWU3X49kKmewHl4Y9xOefU/txyWZ46pIW5+HSLF8NE+hbjxvSsIFeofCe/Sv8
d37vE/oSndIEqKz1nO2UfPBhoEmspz3l84ILZsgPFJ/7wY9gq87ug3oRSs+cIFw3fjUlP0Q01e8W
pTmqdFNt5r+vPRKRl+MOFSLe110HmXggUwUm7GwWlIONYoIYy5qeOUB0/8e3TQui/Yg2jmFFquzt
V+62H6EtbMqedcJOn/I00MbP+rcaEw1A712mzqWW4qDSKISAw5PWIkSW4UqHiP8jaODJmy/EhtvI
XSe8xccpc9XgMyqsJht1XbXy29zTkWPl/tc9SjYeox3NXfcWuV2Id9uVgwOydz/B4F8yce8cusyv
34qThJnqbz2cqFGd5c9l3dVxgoHl7VuwJOb/x68EszjP7IrLEdA81qt3Tl4Tufat6V5LxH0F91nC
A9oS4Rsh8+rCIC/ryUnDyAJHwYeoTbd3JY/tInNYv/krMoVIdxVxDM051VrsXP2AS+jM+CIEZsav
EPvUsKCufxs7UB2lafrpjuBxoXSzYvsYyh7cE42EpcaUYZ11cO2Lbtm6gDvmL+un15y+5/TOheYi
EXllFB4pIRtP8aOBofOswe8J3PbBMccgmCYHgdLS4caiOhB0lkeDUj61rGsqlQoQSM9TH7CdqE29
Ojr/5VAe7IP8OzkHhm1N/8vgLWQDnQ8tutufE+bPd0r0dVO6TdOi99wKFF2gb8CKFPBKNhP0KjST
VKPyWK3kppDCpPVp4FUYpWCf9d3bh9qUEruLiDBlpbR2dSwCf71vQiSgjVkMlXPKbdUGjrAGbUTw
T3WBRGWpUSQyfgD10b7voCw3DtQuwZ3zVQNs+gGakJ5BgaXuF7XQfTZOZFOIjkZCq8lBbzlQXCBG
HnMqdPf7qC2pFF56DFRJIja7kNmYiO42y9743X999Iwi8fb7dZsJG0G5P/zpASoiyDAGZ3111Uz2
BhJuaFdy9BkObxYhIElUzkd6u6Cs4u8U+5ieQJVsrsMnenp8YHdeviB4c/tmKDRGw1qDdmgW48xt
xQpUEJRugf1kx1sUr2k67a3dRzKxkYboo+xLKpVntHd+ry3TZYyL52Jn6gh/T/UO8Jj9cdpX7KhW
OHH/oTi4/Vq2SMjvWr+fXU63beAhqOD6TOno+xSOZld6HyzYACkLrlx1F5+8m3B84c28QaV3fTz5
VwRrkVePFX/rrySh+QF34Q47TonrIyFEFeEr2vPelYyYCcApW+EDofO0NCRX1ZvGa4mzNl0GQC0M
KUDo+/O8qcDu9svAXVlgviAnRxdgMjwZqCKX/p2/SEkFcb1xeNof5Hq/RyI3CqUsoc73zkFOZBvX
MkU/GlT+PRGAzDq/c0oBUpkPXsrOxW4qJG6k6klVXT80ER3nUV+qrDK29rPZ6aLQN8xg53R8VQ7V
mOJrPwmeRNdvpNKaEQJRMPSRewsIOhyBzDV9tfpk1yIi1pwqU/MNeJWBJRwmAjbjXsWlb2QuexM0
hoUDbME24h2oDOMBLRyFJ9ZY9K8pS5HiUWU7t4qDxuMUgSD/3SyI1mU48Mo8ZQYU8gNNnsBvaM+9
NVj/FfB1LEeUUGMYAuyfgQ3xNe+jtqztBZlaQ0/ailUrokwEQDJu6S2J94ElRhTxkcZnRoOMd5n8
/6u++bOk7ISXckZhThh079Cjop5DK5cWe41w8BZlkN/zkNIDaS+5oayLBT94c0INo9viRqXYUvRD
brD4k85UsdLn47ZhZtRV5QzpZItknY1kzushI81qEyjdctWHYU+/ie4w5avRMkgBR8jr2AQQ8G5N
/bAhIkKjqdttEopTTWjqffbWNbnkgtU3Uob7ECGwGd7AyIyXI4lwq6XvGghwkDkNhATVEM69w2+d
uPsW+gPTZN8ZbNqB0BhlRzT96RqFMF/J3wlByTsuJ24OdZvuVacUzWsxrl/rW+rzxwihf9Y9nS3r
S3U/qIh2/uFmgYL89uko836OpJ+JUL2+5ienQDijtuoXWiMBU+Lvm9PaS4eapiYRk3LhLcp71bRw
WipmEuiGHKFT1r79j/j9PHWLN9GzIyfutULcCH+X/UYn7ynNA2ttgv++Dp+GgF5mqrUoR8Kthc9O
ILqPh5Pd4g41CEbJV+FVS9WNf1WlM7Z7h8dYY4+FgmInqPwtOAGPm76bNVVe3chz+g7NgbmMVVAQ
WduimNk2JNFGMf7+dgrpz0n5csFrct7Dxhq/dFQM7cXmq+mi0qa/BdwWT8/WbQJxfucbkkIHzB9u
n7tH2cnhE0BRW+uJAfITUs9JtPQl93FtpjzPZ26sovDz69kr3i9Xbyh5516VlrhPwMYwHYNam9CN
OCZHg1gwv10GWnRSEqnLlcoJA14q+av3NQnN3WvzrhtTQBlPvOEr+mEiSSQvN8yA15ZHdkRSu2VV
fJxAdz5zBpOeRfsJ6IGq1GRkibbU7rDR7sqJCaV36eCaY5fY5SbUBIHZlQ7gTW7+xJGwpdjkCRfv
vvWbH5CJDn+HM7lhfg4xa7Gno6/A/btXD4DSPNLJ69Xvx/x3zoBQZY57zBky/oxdweo8d/un5t2v
tUu6IpCdnlLTKm0ga2phqdm/fM4nr8eAp2e+Y7HlR8uQ1afKX5GIRvjdTQE/MTXIGylUVGahVqPW
7jHzre9p0J1G6H0+/2S7/lIUyM7fiBJQ5RO4lVKjdDOFt+hO6hBL3Wdog7q7AVg/Awbkh03m+78Z
zeK5ulJVOgtZpEgEnOYJmLeZQh4M811gdJW3dzlgq8MPsH2VrPc9RWCcHTTrgn9a/r2vRGOujA58
bh+PWlsyoRS20FkgWeCXY8ooN5QJCt8fBVT0rgRE2b3lbSki2Vc4lCelFJNGbGGaNQM2jMLsLj1K
XKYQ+BYBlxV1Lb28b907jttz+c6BOtWYqieR18U8nFcUBPJ0boV5WvHBa/aA4TeIBuYkPfkuYc6R
U8LBfl68czMqA6lgcDjIKpDDqK7WGpSEvhGq7qtvkCssmC1NflgAjcmK0DrhK0HZL0aVumOfgran
9ps9JC8625xNWeW15MgiMe3Hw6cDkLdm78iVbUzXN5QT3jya+wNfBwjUFzgG5KIxXWF3FC2P3m7P
fapPqSO0+NVi9lAdHDlM9n1XymHrH07BKMnhOnBatNSwC3qg9u6AwuettldRYzkC+i+shLfkit0e
AEYZaIfL/hl5tKOEoByLK6mMNahDuGUP+HsjT1wYPrge/GkmRwGerOcjyow+mPqBHxHkFFwxCVjf
Tvwhne72XTRLnbOdnhxHPS/A7YA7bCucZqVgnVu3AowoX2SoY5EAWNosEKiyQH8uVXIEEaO6MBbj
KWJXI0E/64ND95pY5+HGw7f42PRSA14Km2dZHVQOJkNlgr0ya18JCN3S8y0BEVydd4E3E4f+bDxJ
e2ATVVY9bm2jS24jD8pHG8wv7l41nvYrIq+q0NJUJ6eZ/HJ9Tio4fYCjJ5D9OIeERcuE1+TzRnpZ
3Gcy7zvovNglaMEVpb0jLjrRuHe5CLs5/awysz5qeQLX97GOdZcrRLC+NUpymYtWX5rk5xoKaM/H
8s/arYbsBuiTpubhu2ZI977Y581p6OxNis79EmMFpavDzZNdZIFdCIVRdWUB/TwTIB7n6F1ItKd1
12QBOobc7fwE5VEipBAadU4TnncTej1XQvYxzvlVs3TDg4wfjxU2i/t27bnp6iRLc6o2LtBy8NKY
3UQ6iEt2UILVBaMhq5e+0ya5ZchzlPVdTGJMepcClUWdRhc/klZhdVSFgqFoTKCLdaeQ7yVb/sxE
rDwvwz8Cq7FZTzqAMCQOTPeGacmJIw11V5dLkZCCZdktWU8FsQiLDhyCrF6Zajr40uzS01UISRHZ
UspaI8huIArHnOv0JIHx2XTrSxpgC9Hb8R9o2FOd3N9SfDZtis8I72AMVfh1ZZ6PTdU53XaxMnMo
rpiH2PEs1/wlk3CslnQhMw8QgxHgjLccjv08Fjyx7VZlYJQgGIuQkBXiMEP9GjZFx1DNw1gX1eX2
HQjhLbGdsXORcL6mB1TqSG9CYdNgU1lrr+kbjXzywNo2oTO8mpS2OS7X6x/5I3ln/7XMId769hWp
UPokNtgcUCOU/HqfW7BzcWlrSVQhgEvZ6KWzrmQ2us/Z3k/MK2yy26M92ZhAZrsIFVGLR+ePX0p5
cN61l09DJPpYsRU9o5EvVicUclx94hE9ehPjJawlkeePuRg44LixRqW9+QoQVelxTA8eHl1fvsc6
xPxLff2vvS3vtdB64juQp9VXdAeyuvw2EGo7sCwnVqSdUnHJWvGkfN6cXPq3Q50TZQZvmIKiGR3U
lBfx6R/rCIh7eqWISFrDixjFcnMNs4ouIUCkXFnkiXUKg2xyoIE6n4WCAZ/NQtVngf1C7u6SrPg7
/WoEdyx9y86L5jZfsanuejpCursGrAt4/TsJF90EcwSbNVo5BOKJ4qQf+T+6ODSBOVRHXCXIkdMl
Wq60Omd5wrv+g9y4oFFfe9aRb6UygbD1EQ7+MMl1C/WcQo0nKxD3rpt34QD3SIqkBdvxrXJGStOk
55aZEpcbfxYqq6+WldPtk6E8NSG2XD9sqI4OyCpXrluFUbxvIQNy510rlqCA1U2nU8f7erO22JC6
tYHzt8BQ6/nb3Wxrlye0o7HKbuk1GSNX9aTMk4TBTY5ALvvBSNhXEeNkf1y6gVNkrze9WjLMj6BA
Tk0vC8YQTGtJbi0/eBRpC4AJc8HhrKn6ACmSR/y72KV35t9Ml5JiJa9k049UeGNHKLLy87Gcextb
HDfU3HYiKfWMAcwmDpiKNUM/oTRY9YeYE6wIbA/SLZRSNAyJeQV77tPie+puI6uVg5c6ZwHaD9xu
yWfy1ruAswfUQanFOK2oWJ+xxd5Ihearv8Y4TGK8Wf2pmraUmgqVqJvL43NPv/1T78HCai8Ov3w6
euRJqKszRaCTugfGHgc5mLj6Z47Bu0YhsGx69SpTrGj2vYFeP+3lU3kMh/nwUqU26QHPaazGP5ql
COfqReEv8GMMDu7DKkMabRv3afveLqG7ClLNFwHtupD10OTFSg1AH2WZ2zNnjd0JSrRalewQTe/x
OWZKwk0ve/mU/Y+8OAQ+qzDKbd5yrqFj62b0QmHR3Ph/dfJ07EyKCIaRxFSSv00Be/25IGZKA72w
8MaQxcOqO1HOl1cuS8auguutXLXeMqoyMS0ifKFTsgSdlEMFcJnzx64Mzijdl9zEZ1ge8Tx+fMeB
oq0+upKyGl+DXLEvxCsHnhHLdncSXlL5CFY7u9PQTp0K62768NojqkXGoJeYo9ryN+J+INhd9yqE
WrefkTR15tWM7/D4KtRysJ4bjySjaWkYvOyqDnhE1t9gozyYCVo9gx7oWnF2srHnQzuy0JBT7cc/
hXf9SnDYU8TO8s66l15KHDFGQdJ1KKfi+ElZitKiwJo/jNiJxGClMtR1GhP3pynFX5omAfAO45Oi
+5mlXrH7+xJfKg59+1jtzLULO5V59VvMuoET+CWq4YwEdSnAE5GFsv+EWRXGhOlGaZKm6FFXOb7W
+ElpKOvpy4SxETM+VUD3uDxKOtMZy1qxeDmC+2n3mkuknB09jY3IEgY3tb99lSWEIPb67E33ywos
ZyEq9lUNceoAvr/RjCWw1SCbjiIwN62GtcaxFYxbYV7DV8zoDcQEmE8EM9mx3aYXtHhBKdU/npzY
j/EJTKfkKBQCIdIQSwYpKCovS8/+d12t4XDj/+iXpf7l80wDVN6nlogV/lDoZxkQETNeklrDZ2rz
k6BvJKn2XIev4u7gwPxjAdjdWQ9GilOGJlAjH6wvX5bFnte97xHC95CA36HjRpItoGfOMNEMc7pl
KUbs+AiSUgjc0IYMJe77ILh8hNjIjyqLLYPJhzihYX6QV191NpbLkLC+QiNIj/J1j8+B1WcrFFHQ
XXPf90DxYKWQCPw5yTnUiphsXy3GlMnvSu2zYnzHjnbVkSI6bHCOspeSDV6lzHl2Cw7L6AED/jUY
KRVpQxC/rEpIc7eoeUBmnJOmn2ZRb54RrWHoxSUKkoNN1ewydhYGTcjxkcApLinoR6d9pJyg+QlV
000zbhyA+KGnxYgOQdavjaO/wUbaRO2KVj66waKTjSyCfp/UFeYoZw49uuZCHf550xz4sxK80ef8
+2dqbU5QOPf+OnBNjEHHAPnCuaMXQYDK8B6gHqmS0pTnlftGORxhdPEGMJiTYrr4aXtGiZTkb4Fm
cgwRnycTvHyixHfhajYdF3pThEJ6DiuyrFXyRHmpoFBH4QjEPK2yQ7lRAOp3aqhlvJxgMaZDmhQC
CxQFpgLwkHJxijS6bLr4LtpswIUWoTJ7MJVLrJCuOB/hfj9u58JJzgi644B+eaEj2/w81JaQRZ73
VcRAbRZMv4tVFoAvOPW+YtXgx73O20ZyRqp4t0YXNdeuOYxJHh9Wlf0OuqNYypjvbgap6MLVji64
ZF+lQkzac0e1rTYDALc+w7zNqriToi45A5XW7xTL7nhEkqWuV+eqEyvhrmq5EnpVsdR5scQmbkz2
QYRxEsv+Y9tpVXwxfSyCTz60a1FrTtn3qgCNYxY/caaWnNRzIeHa83JuIiNazwneft03KC4kV88p
8NalLzN9U8h5O9rO2GikSK9kqv7AUnUGicUMguEevWhMMFosaYZc1sE/tJUjnIHFNInA4t4KIEcR
7RJbrHQeHvJlu4hwjefkwze1TyX/1tRtMu0A1IeBD1MfHr6Gzyt098Z2H9OJ1hcDDPZ/zjVRXX9Q
l/Ky/Pkrwnnfl+tFpwBYvU73rQOaeencjFYCRPESmDm26My9AxVtc3q/4gWA+8F4KqB0Z5pce9yG
2Q71ZnzPpru73iSIdcTBDP8XN5fpxKZ85Y0WvICcECM4Bl16wWSPuj9ggIIIfFxCtFMH7aBPoJGQ
mEVQiLXP3SP9BAO1p8iXL7c0wCOyFK05TEAkQcKhfv6yST/5RVuv1C3yjdEtrIAijFkKKAZab37O
OKDG9VvetuyeQjAPnx/7MLrEJvKqAoUqu+0x/cfRXvPYLWVtxGUeD7s32UJ0zDZsDNHDkuGmSvQz
aqT+5PiwdWJUlifGQny0R8BoPHO7rWwPgmNU09ogmBQgZZUbTa6xl7pYLpVJ3oSlEAemp09l6frw
tDXE81HJb83sY8/nVScLzONyaywn92a4FVfUCh1g/x1/FATQgV2KRdHGCb5xdGySofiR8bQI5J81
ElRVTztiFfzystWNNJ5BwD9dqNvo2if5rINPf+i34dIIhv3rc44CRc4Zlcx8Nt2j97Ikp72GZ9Hv
SerPBJrOQURPyqKY/ZCXY7SxpRIEJ6B3d2A20Yk4/2wpIoMAraCld2NvZ8mKic32HP/ZnMM/19H3
kjIJCKsUVPmHkCajaQZj7YqTGn4YLwN2pWNx9OqaXqhrnF2qs8JJklk5Y7QquCILEUmgS+3HWk6f
fzh9U6hMMmMGEBf2Z8wLB4LNeyRq94KBeUEzBVpwlKNFifXg5uccFU6/V+QVz+/CWrGxoiObE9dj
xE1xQsGZMy9dXaFd8UMnDighak/SbyaqsJuAF1Kb2q+6rUc5u/FlrzwclC73XRFpqQ0gAM6LgF0C
a778iZBhb5mQkkkyvo90ZDwslHQmTPmYo7UqOkfXPE1k/26mWiYGdeIeoeGzJr8Jb1LikQhlGOg3
R7XlS3caFbIMgm2NQA10PhcL/XdFmiV0eSDbz1VlDuv8LnHOGLBS0aWVW6LcVkm2Td6pmEeXSYfm
hvD7flpS7uTssY4T4V0qMMh+us96hEXVNHjjK6aK/7pIbjDhvxwbS7QAP0Y6ztnqne/yMaj7E+MX
uFjFBxkagq9DHROXBU80GBCbVluveA3ed2LZKphc7SOYxFwDdsWtT5yGAYLAadEugMUKZAt8+ri8
DxQU6Vxc+qRau8jRPVDQm6IESQ/aVgLZWS4dD+lcXd94xm5DhkYiUSRFKMshqZTH6wfdFiWr9U/9
ER230P2geW5ROU8vOzUKHCEmS8UmzVRjN9zb7DrmWBaadqIZEa/D/4VnY7eUOaWOhKvYN7VsyBbK
qAztG0wU/0j2XGE4U0utLU1u6sptI0Jrjr6TOh291xU6Zo6TObzhj+3jschAGhP19v6b2Zh/IFE7
OL0JkcvXhQMlx+GC7fxqKJ+11tCWrR5eZFxdVLn55zp22QjBEb8JVyY1gVb+MOJIQSMPYW6C2oZk
tOH9oz2gBkyjEGwWm1N6z2NzdAOf3Zb9a20SwSmOzAU/AW8qANqFHqilu/DK+AP4h0lkJyLpn/eU
TkAYrcj4KBz8wzNU7/pF0VCfqS+dUy8x+kOxRx4A6lzyFJ2WBugspMvEhlRt6Lv80+x09uKX15gW
UgfV/MvRnitLRitwCMHdkRrx5XkVIsivB2V3kooV3e0Xpr4KAssXybhsjS4SuHboW4E66H4NRCE8
tQzRepgBzyAq1HIqW2HqrkdPj4A4Juguifh6Z9KfqTYcRZZbKhjTxJf94QlHJlu+Jp7FG3VvXkvr
5GBVRo2jlFIhNOQHeQwnsArx+A1yVg5wUsx6mv88LmdwjLgK4mr3o1eTD5mtd2G/bMmf+PpSARW0
EXHzaWMPxSGPRI00mGFO56Mpj0fE8CkkB2IoSr8x3bSJ2PaQcIxiSy/bv8T3k7i1R+OyO703JUeT
/NJw6HnxrysQ1Frd4jTMLNEOOOlEbecrhOSK7j3avfWKV+BVmCaDlH6S3pqhQTVcE0xbOyd9F0Et
cXPbq/un04+80UNaNYwWhEXLD0t2L5yieJHnpD44H85gm4lBK9ZC7+G3QvqlB6ZCBDhEJJKWeqAv
eotF5RmMjkZdIJ1za+MKMukWCRjsKJwo8UamVTFh1YvFgvrNvoyUW4U1+g2CKIz3NHG7/k2221zb
hG3jJIvINhnFPeWkDbapSre9OdlBkBBbYPNBaluLGEm5PZNKQdFTjw1b7JId7r3CsW0ZZfnGUa1f
MQrvvCGz14rUYNasLS6FBUJ0H48J8u6sCqtrG/uwVx5462zukyTBobz/tk2YgrNoIXtyPYx8eZW/
OokwidD44dLo63Ng54zpAHu8YpukeoI9R3u6SpY2uUYkm7GqpaCipaeFzQ0xz6IHG328pGl6fgNg
gaq2WWrm/nYCQ6D//10kCp+4QRoQONMfevzQG6URK7RrsPrP4y5k+MsmRw==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD is
  port (
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_1 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_2 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    mmcm_reset : out STD_LOGIC;
    data_in : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gtxe2_i_4 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_5 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_7 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_8 : in STD_LOGIC;
    gtxe2_i_9 : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    data_out : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD is
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_init
     port map (
      D(1 downto 0) => D(1 downto 0),
      Q(15 downto 0) => Q(15 downto 0),
      RXBUFSTATUS(0) => RXBUFSTATUS(0),
      RXPD(0) => RXPD(0),
      TXBUFSTATUS(0) => TXBUFSTATUS(0),
      TXPD(0) => TXPD(0),
      data_in => data_in,
      data_out => data_out,
      data_sync_reg1 => data_sync_reg1,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i(15 downto 0) => gtxe2_i(15 downto 0),
      gtxe2_i_0(1 downto 0) => gtxe2_i_0(1 downto 0),
      gtxe2_i_1(1 downto 0) => gtxe2_i_1(1 downto 0),
      gtxe2_i_2(1 downto 0) => gtxe2_i_2(1 downto 0),
      gtxe2_i_3(1 downto 0) => gtxe2_i_3(1 downto 0),
      gtxe2_i_4 => gtxe2_i_4,
      gtxe2_i_5(1 downto 0) => gtxe2_i_5(1 downto 0),
      gtxe2_i_6(1 downto 0) => gtxe2_i_6(1 downto 0),
      gtxe2_i_7(1 downto 0) => gtxe2_i_7(1 downto 0),
      gtxe2_i_8 => gtxe2_i_8,
      gtxe2_i_9 => gtxe2_i_9,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0),
      reset => reset,
      reset_out => reset_out,
      rx_fsm_reset_done_int_reg => rx_fsm_reset_done_int_reg,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_transceiver is
  port (
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    rxchariscomma : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxcharisk : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxdisperr : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxnotintable : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxbufstatus : out STD_LOGIC_VECTOR ( 0 to 0 );
    txbuferr : out STD_LOGIC;
    mmcm_reset : out STD_LOGIC;
    data_in : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \rxdata_reg[7]_0\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    gtxe2_i : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    powerdown : in STD_LOGIC;
    reset_sync5 : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    txchardispval_reg_reg_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    txcharisk_reg_reg_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    status_vector : in STD_LOGIC_VECTOR ( 0 to 0 );
    enablealign : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    \txdata_reg_reg[7]_0\ : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_transceiver;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_transceiver is
  signal data_valid_reg2 : STD_LOGIC;
  signal encommaalign_int : STD_LOGIC;
  signal gtwizard_inst_n_6 : STD_LOGIC;
  signal gtwizard_inst_n_7 : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal reset : STD_LOGIC;
  signal rxchariscomma_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxchariscomma_i_1_n_0 : STD_LOGIC;
  signal rxchariscomma_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxchariscomma_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxcharisk_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxcharisk_i_1_n_0 : STD_LOGIC;
  signal rxcharisk_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxcharisk_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxclkcorcnt_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxclkcorcnt_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxclkcorcnt_reg : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxdata[0]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[1]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[2]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[3]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[4]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[5]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[6]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[7]_i_1_n_0\ : STD_LOGIC;
  signal rxdata_double : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rxdata_int : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rxdata_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rxdisperr_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxdisperr_i_1_n_0 : STD_LOGIC;
  signal rxdisperr_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxdisperr_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxnotintable_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxnotintable_i_1_n_0 : STD_LOGIC;
  signal rxnotintable_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxnotintable_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxpowerdown : STD_LOGIC;
  signal rxpowerdown_double : STD_LOGIC;
  signal \rxpowerdown_reg__0\ : STD_LOGIC;
  signal rxreset_int : STD_LOGIC;
  signal toggle : STD_LOGIC;
  signal toggle_i_1_n_0 : STD_LOGIC;
  signal toggle_rx : STD_LOGIC;
  signal toggle_rx_i_1_n_0 : STD_LOGIC;
  signal txbufstatus_reg : STD_LOGIC_VECTOR ( 1 to 1 );
  signal txchardispmode_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispmode_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispmode_reg : STD_LOGIC;
  signal txchardispval_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispval_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispval_reg : STD_LOGIC;
  signal txcharisk_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txcharisk_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txcharisk_reg : STD_LOGIC;
  signal txdata_double : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal txdata_int : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal txdata_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal txpowerdown : STD_LOGIC;
  signal txpowerdown_double : STD_LOGIC;
  signal \txpowerdown_reg__0\ : STD_LOGIC;
  signal txreset_int : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of rxchariscomma_i_1 : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of rxcharisk_i_1 : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \rxdata[0]_i_1\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \rxdata[1]_i_1\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \rxdata[2]_i_1\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \rxdata[3]_i_1\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \rxdata[4]_i_1\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \rxdata[5]_i_1\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \rxdata[6]_i_1\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \rxdata[7]_i_1\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of rxdisperr_i_1 : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of rxnotintable_i_1 : label is "soft_lutpair79";
begin
gtwizard_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD
     port map (
      D(1 downto 0) => rxclkcorcnt_int(1 downto 0),
      Q(15 downto 0) => txdata_int(15 downto 0),
      RXBUFSTATUS(0) => gtwizard_inst_n_7,
      RXPD(0) => rxpowerdown,
      TXBUFSTATUS(0) => gtwizard_inst_n_6,
      TXPD(0) => txpowerdown,
      data_in => data_in,
      data_out => data_valid_reg2,
      data_sync_reg1 => data_sync_reg1,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i(15 downto 0) => rxdata_int(15 downto 0),
      gtxe2_i_0(1 downto 0) => rxchariscomma_int(1 downto 0),
      gtxe2_i_1(1 downto 0) => rxcharisk_int(1 downto 0),
      gtxe2_i_2(1 downto 0) => rxdisperr_int(1 downto 0),
      gtxe2_i_3(1 downto 0) => rxnotintable_int(1 downto 0),
      gtxe2_i_4 => gtxe2_i,
      gtxe2_i_5(1 downto 0) => txchardispmode_int(1 downto 0),
      gtxe2_i_6(1 downto 0) => txchardispval_int(1 downto 0),
      gtxe2_i_7(1 downto 0) => txcharisk_int(1 downto 0),
      gtxe2_i_8 => rxreset_int,
      gtxe2_i_9 => txreset_int,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0),
      reset => reset,
      reset_out => encommaalign_int,
      rx_fsm_reset_done_int_reg => rx_fsm_reset_done_int_reg,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
reclock_encommaalign: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync
     port map (
      CLK => CLK,
      enablealign => enablealign,
      reset_out => encommaalign_int
    );
reclock_rxreset: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_1
     port map (
      SR(0) => SR(0),
      independent_clock_bufg => independent_clock_bufg,
      reset_out => rxreset_int
    );
reclock_txreset: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_2
     port map (
      independent_clock_bufg => independent_clock_bufg,
      reset_out => txreset_int,
      reset_sync5_0(0) => reset_sync5(0)
    );
reset_wtd_timer: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_wtd_timer
     port map (
      data_out => data_valid_reg2,
      independent_clock_bufg => independent_clock_bufg,
      reset => reset
    );
rxbuferr_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => p_0_in,
      Q => rxbufstatus(0),
      R => '0'
    );
\rxbufstatus_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => gtwizard_inst_n_7,
      Q => p_0_in,
      R => '0'
    );
\rxchariscomma_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxchariscomma_reg__0\(0),
      Q => rxchariscomma_double(0),
      R => SR(0)
    );
\rxchariscomma_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxchariscomma_reg__0\(1),
      Q => rxchariscomma_double(1),
      R => SR(0)
    );
rxchariscomma_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxchariscomma_double(1),
      I1 => toggle_rx,
      I2 => rxchariscomma_double(0),
      O => rxchariscomma_i_1_n_0
    );
rxchariscomma_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxchariscomma_i_1_n_0,
      Q => rxchariscomma(0),
      R => SR(0)
    );
\rxchariscomma_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxchariscomma_int(0),
      Q => \rxchariscomma_reg__0\(0),
      R => '0'
    );
\rxchariscomma_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxchariscomma_int(1),
      Q => \rxchariscomma_reg__0\(1),
      R => '0'
    );
\rxcharisk_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxcharisk_reg__0\(0),
      Q => rxcharisk_double(0),
      R => SR(0)
    );
\rxcharisk_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxcharisk_reg__0\(1),
      Q => rxcharisk_double(1),
      R => SR(0)
    );
rxcharisk_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxcharisk_double(1),
      I1 => toggle_rx,
      I2 => rxcharisk_double(0),
      O => rxcharisk_i_1_n_0
    );
rxcharisk_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxcharisk_i_1_n_0,
      Q => rxcharisk(0),
      R => SR(0)
    );
\rxcharisk_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxcharisk_int(0),
      Q => \rxcharisk_reg__0\(0),
      R => '0'
    );
\rxcharisk_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxcharisk_int(1),
      Q => \rxcharisk_reg__0\(1),
      R => '0'
    );
\rxclkcorcnt_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxclkcorcnt_reg(0),
      Q => rxclkcorcnt_double(0),
      R => SR(0)
    );
\rxclkcorcnt_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxclkcorcnt_reg(1),
      Q => rxclkcorcnt_double(1),
      R => SR(0)
    );
\rxclkcorcnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxclkcorcnt_double(0),
      Q => Q(0),
      R => SR(0)
    );
\rxclkcorcnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxclkcorcnt_double(1),
      Q => Q(1),
      R => SR(0)
    );
\rxclkcorcnt_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxclkcorcnt_int(0),
      Q => rxclkcorcnt_reg(0),
      R => '0'
    );
\rxclkcorcnt_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxclkcorcnt_int(1),
      Q => rxclkcorcnt_reg(1),
      R => '0'
    );
\rxdata[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(8),
      I1 => toggle_rx,
      I2 => rxdata_double(0),
      O => \rxdata[0]_i_1_n_0\
    );
\rxdata[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(9),
      I1 => toggle_rx,
      I2 => rxdata_double(1),
      O => \rxdata[1]_i_1_n_0\
    );
\rxdata[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(10),
      I1 => toggle_rx,
      I2 => rxdata_double(2),
      O => \rxdata[2]_i_1_n_0\
    );
\rxdata[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(11),
      I1 => toggle_rx,
      I2 => rxdata_double(3),
      O => \rxdata[3]_i_1_n_0\
    );
\rxdata[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(12),
      I1 => toggle_rx,
      I2 => rxdata_double(4),
      O => \rxdata[4]_i_1_n_0\
    );
\rxdata[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(13),
      I1 => toggle_rx,
      I2 => rxdata_double(5),
      O => \rxdata[5]_i_1_n_0\
    );
\rxdata[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(14),
      I1 => toggle_rx,
      I2 => rxdata_double(6),
      O => \rxdata[6]_i_1_n_0\
    );
\rxdata[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(15),
      I1 => toggle_rx,
      I2 => rxdata_double(7),
      O => \rxdata[7]_i_1_n_0\
    );
\rxdata_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(0),
      Q => rxdata_double(0),
      R => SR(0)
    );
\rxdata_double_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(10),
      Q => rxdata_double(10),
      R => SR(0)
    );
\rxdata_double_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(11),
      Q => rxdata_double(11),
      R => SR(0)
    );
\rxdata_double_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(12),
      Q => rxdata_double(12),
      R => SR(0)
    );
\rxdata_double_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(13),
      Q => rxdata_double(13),
      R => SR(0)
    );
\rxdata_double_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(14),
      Q => rxdata_double(14),
      R => SR(0)
    );
\rxdata_double_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(15),
      Q => rxdata_double(15),
      R => SR(0)
    );
\rxdata_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(1),
      Q => rxdata_double(1),
      R => SR(0)
    );
\rxdata_double_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(2),
      Q => rxdata_double(2),
      R => SR(0)
    );
\rxdata_double_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(3),
      Q => rxdata_double(3),
      R => SR(0)
    );
\rxdata_double_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(4),
      Q => rxdata_double(4),
      R => SR(0)
    );
\rxdata_double_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(5),
      Q => rxdata_double(5),
      R => SR(0)
    );
\rxdata_double_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(6),
      Q => rxdata_double(6),
      R => SR(0)
    );
\rxdata_double_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(7),
      Q => rxdata_double(7),
      R => SR(0)
    );
\rxdata_double_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(8),
      Q => rxdata_double(8),
      R => SR(0)
    );
\rxdata_double_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(9),
      Q => rxdata_double(9),
      R => SR(0)
    );
\rxdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[0]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(0),
      R => SR(0)
    );
\rxdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[1]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(1),
      R => SR(0)
    );
\rxdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[2]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(2),
      R => SR(0)
    );
\rxdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[3]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(3),
      R => SR(0)
    );
\rxdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[4]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(4),
      R => SR(0)
    );
\rxdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[5]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(5),
      R => SR(0)
    );
\rxdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[6]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(6),
      R => SR(0)
    );
\rxdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[7]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(7),
      R => SR(0)
    );
\rxdata_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(0),
      Q => rxdata_reg(0),
      R => '0'
    );
\rxdata_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(10),
      Q => rxdata_reg(10),
      R => '0'
    );
\rxdata_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(11),
      Q => rxdata_reg(11),
      R => '0'
    );
\rxdata_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(12),
      Q => rxdata_reg(12),
      R => '0'
    );
\rxdata_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(13),
      Q => rxdata_reg(13),
      R => '0'
    );
\rxdata_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(14),
      Q => rxdata_reg(14),
      R => '0'
    );
\rxdata_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(15),
      Q => rxdata_reg(15),
      R => '0'
    );
\rxdata_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(1),
      Q => rxdata_reg(1),
      R => '0'
    );
\rxdata_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(2),
      Q => rxdata_reg(2),
      R => '0'
    );
\rxdata_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(3),
      Q => rxdata_reg(3),
      R => '0'
    );
\rxdata_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(4),
      Q => rxdata_reg(4),
      R => '0'
    );
\rxdata_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(5),
      Q => rxdata_reg(5),
      R => '0'
    );
\rxdata_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(6),
      Q => rxdata_reg(6),
      R => '0'
    );
\rxdata_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(7),
      Q => rxdata_reg(7),
      R => '0'
    );
\rxdata_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(8),
      Q => rxdata_reg(8),
      R => '0'
    );
\rxdata_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(9),
      Q => rxdata_reg(9),
      R => '0'
    );
\rxdisperr_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxdisperr_reg__0\(0),
      Q => rxdisperr_double(0),
      R => SR(0)
    );
\rxdisperr_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxdisperr_reg__0\(1),
      Q => rxdisperr_double(1),
      R => SR(0)
    );
rxdisperr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdisperr_double(1),
      I1 => toggle_rx,
      I2 => rxdisperr_double(0),
      O => rxdisperr_i_1_n_0
    );
rxdisperr_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxdisperr_i_1_n_0,
      Q => rxdisperr(0),
      R => SR(0)
    );
\rxdisperr_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdisperr_int(0),
      Q => \rxdisperr_reg__0\(0),
      R => '0'
    );
\rxdisperr_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdisperr_int(1),
      Q => \rxdisperr_reg__0\(1),
      R => '0'
    );
\rxnotintable_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxnotintable_reg__0\(0),
      Q => rxnotintable_double(0),
      R => SR(0)
    );
\rxnotintable_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxnotintable_reg__0\(1),
      Q => rxnotintable_double(1),
      R => SR(0)
    );
rxnotintable_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxnotintable_double(1),
      I1 => toggle_rx,
      I2 => rxnotintable_double(0),
      O => rxnotintable_i_1_n_0
    );
rxnotintable_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxnotintable_i_1_n_0,
      Q => rxnotintable(0),
      R => SR(0)
    );
\rxnotintable_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxnotintable_int(0),
      Q => \rxnotintable_reg__0\(0),
      R => '0'
    );
\rxnotintable_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxnotintable_int(1),
      Q => \rxnotintable_reg__0\(1),
      R => '0'
    );
rxpowerdown_double_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxpowerdown_reg__0\,
      Q => rxpowerdown_double,
      R => SR(0)
    );
rxpowerdown_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gtxe2_i,
      CE => '1',
      D => rxpowerdown_double,
      Q => rxpowerdown,
      R => '0'
    );
rxpowerdown_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => powerdown,
      Q => \rxpowerdown_reg__0\,
      R => SR(0)
    );
sync_block_data_valid: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_3
     port map (
      data_out => data_valid_reg2,
      independent_clock_bufg => independent_clock_bufg,
      status_vector(0) => status_vector(0)
    );
toggle_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => toggle,
      O => toggle_i_1_n_0
    );
toggle_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => toggle_i_1_n_0,
      Q => toggle,
      R => reset_sync5(0)
    );
toggle_rx_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => toggle_rx,
      O => toggle_rx_i_1_n_0
    );
toggle_rx_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => toggle_rx_i_1_n_0,
      Q => toggle_rx,
      R => SR(0)
    );
txbuferr_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => txbufstatus_reg(1),
      Q => txbuferr,
      R => '0'
    );
\txbufstatus_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => gtwizard_inst_n_6,
      Q => txbufstatus_reg(1),
      R => '0'
    );
\txchardispmode_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txchardispmode_reg,
      Q => txchardispmode_double(0),
      R => reset_sync5(0)
    );
\txchardispmode_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => D(0),
      Q => txchardispmode_double(1),
      R => reset_sync5(0)
    );
\txchardispmode_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispmode_double(0),
      Q => txchardispmode_int(0),
      R => '0'
    );
\txchardispmode_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispmode_double(1),
      Q => txchardispmode_int(1),
      R => '0'
    );
txchardispmode_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => txchardispmode_reg,
      R => reset_sync5(0)
    );
\txchardispval_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txchardispval_reg,
      Q => txchardispval_double(0),
      R => reset_sync5(0)
    );
\txchardispval_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txchardispval_reg_reg_0(0),
      Q => txchardispval_double(1),
      R => reset_sync5(0)
    );
\txchardispval_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispval_double(0),
      Q => txchardispval_int(0),
      R => '0'
    );
\txchardispval_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispval_double(1),
      Q => txchardispval_int(1),
      R => '0'
    );
txchardispval_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => txchardispval_reg_reg_0(0),
      Q => txchardispval_reg,
      R => reset_sync5(0)
    );
\txcharisk_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txcharisk_reg,
      Q => txcharisk_double(0),
      R => reset_sync5(0)
    );
\txcharisk_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txcharisk_reg_reg_0(0),
      Q => txcharisk_double(1),
      R => reset_sync5(0)
    );
\txcharisk_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txcharisk_double(0),
      Q => txcharisk_int(0),
      R => '0'
    );
\txcharisk_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txcharisk_double(1),
      Q => txcharisk_int(1),
      R => '0'
    );
txcharisk_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => txcharisk_reg_reg_0(0),
      Q => txcharisk_reg,
      R => reset_sync5(0)
    );
\txdata_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(0),
      Q => txdata_double(0),
      R => reset_sync5(0)
    );
\txdata_double_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(2),
      Q => txdata_double(10),
      R => reset_sync5(0)
    );
\txdata_double_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(3),
      Q => txdata_double(11),
      R => reset_sync5(0)
    );
\txdata_double_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(4),
      Q => txdata_double(12),
      R => reset_sync5(0)
    );
\txdata_double_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(5),
      Q => txdata_double(13),
      R => reset_sync5(0)
    );
\txdata_double_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(6),
      Q => txdata_double(14),
      R => reset_sync5(0)
    );
\txdata_double_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(7),
      Q => txdata_double(15),
      R => reset_sync5(0)
    );
\txdata_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(1),
      Q => txdata_double(1),
      R => reset_sync5(0)
    );
\txdata_double_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(2),
      Q => txdata_double(2),
      R => reset_sync5(0)
    );
\txdata_double_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(3),
      Q => txdata_double(3),
      R => reset_sync5(0)
    );
\txdata_double_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(4),
      Q => txdata_double(4),
      R => reset_sync5(0)
    );
\txdata_double_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(5),
      Q => txdata_double(5),
      R => reset_sync5(0)
    );
\txdata_double_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(6),
      Q => txdata_double(6),
      R => reset_sync5(0)
    );
\txdata_double_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(7),
      Q => txdata_double(7),
      R => reset_sync5(0)
    );
\txdata_double_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(0),
      Q => txdata_double(8),
      R => reset_sync5(0)
    );
\txdata_double_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(1),
      Q => txdata_double(9),
      R => reset_sync5(0)
    );
\txdata_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(0),
      Q => txdata_int(0),
      R => '0'
    );
\txdata_int_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(10),
      Q => txdata_int(10),
      R => '0'
    );
\txdata_int_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(11),
      Q => txdata_int(11),
      R => '0'
    );
\txdata_int_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(12),
      Q => txdata_int(12),
      R => '0'
    );
\txdata_int_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(13),
      Q => txdata_int(13),
      R => '0'
    );
\txdata_int_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(14),
      Q => txdata_int(14),
      R => '0'
    );
\txdata_int_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(15),
      Q => txdata_int(15),
      R => '0'
    );
\txdata_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(1),
      Q => txdata_int(1),
      R => '0'
    );
\txdata_int_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(2),
      Q => txdata_int(2),
      R => '0'
    );
\txdata_int_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(3),
      Q => txdata_int(3),
      R => '0'
    );
\txdata_int_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(4),
      Q => txdata_int(4),
      R => '0'
    );
\txdata_int_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(5),
      Q => txdata_int(5),
      R => '0'
    );
\txdata_int_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(6),
      Q => txdata_int(6),
      R => '0'
    );
\txdata_int_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(7),
      Q => txdata_int(7),
      R => '0'
    );
\txdata_int_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(8),
      Q => txdata_int(8),
      R => '0'
    );
\txdata_int_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(9),
      Q => txdata_int(9),
      R => '0'
    );
\txdata_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(0),
      Q => txdata_reg(0),
      R => reset_sync5(0)
    );
\txdata_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(1),
      Q => txdata_reg(1),
      R => reset_sync5(0)
    );
\txdata_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(2),
      Q => txdata_reg(2),
      R => reset_sync5(0)
    );
\txdata_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(3),
      Q => txdata_reg(3),
      R => reset_sync5(0)
    );
\txdata_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(4),
      Q => txdata_reg(4),
      R => reset_sync5(0)
    );
\txdata_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(5),
      Q => txdata_reg(5),
      R => reset_sync5(0)
    );
\txdata_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(6),
      Q => txdata_reg(6),
      R => reset_sync5(0)
    );
\txdata_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(7),
      Q => txdata_reg(7),
      R => reset_sync5(0)
    );
txpowerdown_double_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \txpowerdown_reg__0\,
      Q => txpowerdown_double,
      R => reset_sync5(0)
    );
txpowerdown_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gtxe2_i,
      CE => '1',
      D => txpowerdown_double,
      Q => txpowerdown,
      R => '0'
    );
txpowerdown_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => powerdown,
      Q => \txpowerdown_reg__0\,
      R => reset_sync5(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_block is
  port (
    gmii_rxd : out STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_rx_dv : out STD_LOGIC;
    gmii_rx_er : out STD_LOGIC;
    gmii_isolate : out STD_LOGIC;
    status_vector : out STD_LOGIC_VECTOR ( 6 downto 0 );
    resetdone : out STD_LOGIC;
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    mmcm_reset : out STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    signal_detect : in STD_LOGIC;
    CLK : in STD_LOGIC;
    data_in : in STD_LOGIC;
    gmii_txd : in STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_tx_en : in STD_LOGIC;
    gmii_tx_er : in STD_LOGIC;
    configuration_vector : in STD_LOGIC_VECTOR ( 2 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    gtxe2_i : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_block;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_block is
  signal enablealign : STD_LOGIC;
  signal mgt_rx_reset : STD_LOGIC;
  signal mgt_tx_reset : STD_LOGIC;
  signal powerdown : STD_LOGIC;
  signal \^resetdone\ : STD_LOGIC;
  signal rx_reset_done_i : STD_LOGIC;
  signal rxbuferr : STD_LOGIC;
  signal rxchariscomma : STD_LOGIC;
  signal rxcharisk : STD_LOGIC;
  signal rxclkcorcnt : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxdata : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal rxdisperr : STD_LOGIC;
  signal rxnotintable : STD_LOGIC;
  signal \^status_vector\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal transceiver_inst_n_11 : STD_LOGIC;
  signal transceiver_inst_n_12 : STD_LOGIC;
  signal txbuferr : STD_LOGIC;
  signal txchardispmode : STD_LOGIC;
  signal txchardispval : STD_LOGIC;
  signal txcharisk : STD_LOGIC;
  signal txdata : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 7 );
  signal NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  attribute B_SHIFTER_ADDR : string;
  attribute B_SHIFTER_ADDR of gig_ethernet_pcs_pma_0_core : label is "10'b0101001110";
  attribute C_1588 : integer;
  attribute C_1588 of gig_ethernet_pcs_pma_0_core : label is 0;
  attribute C_2_5G : string;
  attribute C_2_5G of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_COMPONENT_NAME : string;
  attribute C_COMPONENT_NAME of gig_ethernet_pcs_pma_0_core : label is "gig_ethernet_pcs_pma_0";
  attribute C_DYNAMIC_SWITCHING : string;
  attribute C_DYNAMIC_SWITCHING of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_ELABORATION_TRANSIENT_DIR : string;
  attribute C_ELABORATION_TRANSIENT_DIR of gig_ethernet_pcs_pma_0_core : label is "BlankString";
  attribute C_FAMILY : string;
  attribute C_FAMILY of gig_ethernet_pcs_pma_0_core : label is "kintex7";
  attribute C_HAS_AN : string;
  attribute C_HAS_AN of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_HAS_AXIL : string;
  attribute C_HAS_AXIL of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_HAS_MDIO : string;
  attribute C_HAS_MDIO of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_HAS_TEMAC : string;
  attribute C_HAS_TEMAC of gig_ethernet_pcs_pma_0_core : label is "TRUE";
  attribute C_IS_SGMII : string;
  attribute C_IS_SGMII of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_RX_GMII_CLK : string;
  attribute C_RX_GMII_CLK of gig_ethernet_pcs_pma_0_core : label is "TXOUTCLK";
  attribute C_SGMII_FABRIC_BUFFER : string;
  attribute C_SGMII_FABRIC_BUFFER of gig_ethernet_pcs_pma_0_core : label is "TRUE";
  attribute C_SGMII_PHY_MODE : string;
  attribute C_SGMII_PHY_MODE of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_USE_LVDS : string;
  attribute C_USE_LVDS of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_USE_TBI : string;
  attribute C_USE_TBI of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_USE_TRANSCEIVER : string;
  attribute C_USE_TRANSCEIVER of gig_ethernet_pcs_pma_0_core : label is "TRUE";
  attribute GT_RX_BYTE_WIDTH : integer;
  attribute GT_RX_BYTE_WIDTH of gig_ethernet_pcs_pma_0_core : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of gig_ethernet_pcs_pma_0_core : label is "soft";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of gig_ethernet_pcs_pma_0_core : label is "yes";
  attribute is_du_within_envelope : string;
  attribute is_du_within_envelope of gig_ethernet_pcs_pma_0_core : label is "true";
begin
  resetdone <= \^resetdone\;
  status_vector(6 downto 0) <= \^status_vector\(6 downto 0);
gig_ethernet_pcs_pma_0_core: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_v16_2_19
     port map (
      an_adv_config_val => '0',
      an_adv_config_vector(15 downto 0) => B"0000000000000000",
      an_enable => NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED,
      an_interrupt => NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED,
      an_restart_config => '0',
      basex_or_sgmii => '0',
      configuration_valid => '0',
      configuration_vector(4) => '0',
      configuration_vector(3 downto 1) => configuration_vector(2 downto 0),
      configuration_vector(0) => '0',
      correction_timer(63 downto 0) => B"0000000000000000000000000000000000000000000000000000000000000000",
      dcm_locked => data_in,
      drp_daddr(9 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED(9 downto 0),
      drp_dclk => '0',
      drp_den => NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED,
      drp_di(15 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED(15 downto 0),
      drp_do(15 downto 0) => B"0000000000000000",
      drp_drdy => '0',
      drp_dwe => NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED,
      drp_gnt => '0',
      drp_req => NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED,
      en_cdet => NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED,
      enablealign => enablealign,
      ewrap => NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED,
      gmii_isolate => gmii_isolate,
      gmii_rx_dv => gmii_rx_dv,
      gmii_rx_er => gmii_rx_er,
      gmii_rxd(7 downto 0) => gmii_rxd(7 downto 0),
      gmii_tx_en => gmii_tx_en,
      gmii_tx_er => gmii_tx_er,
      gmii_txd(7 downto 0) => gmii_txd(7 downto 0),
      gtx_clk => '0',
      link_timer_basex(9 downto 0) => B"0000000000",
      link_timer_sgmii(9 downto 0) => B"0000000000",
      link_timer_value(9 downto 0) => B"0000000000",
      loc_ref => NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED,
      mdc => '0',
      mdio_in => '0',
      mdio_out => NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED,
      mdio_tri => NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED,
      mgt_rx_reset => mgt_rx_reset,
      mgt_tx_reset => mgt_tx_reset,
      phyad(4 downto 0) => B"00000",
      pma_rx_clk0 => '0',
      pma_rx_clk1 => '0',
      powerdown => powerdown,
      reset => \out\(0),
      reset_done => \^resetdone\,
      rx_code_group0(9 downto 0) => B"0000000000",
      rx_code_group1(9 downto 0) => B"0000000000",
      rx_gt_nominal_latency(15 downto 0) => B"0000000011111000",
      rxbufstatus(1) => rxbuferr,
      rxbufstatus(0) => '0',
      rxchariscomma(0) => rxchariscomma,
      rxcharisk(0) => rxcharisk,
      rxclkcorcnt(2) => '0',
      rxclkcorcnt(1 downto 0) => rxclkcorcnt(1 downto 0),
      rxdata(7 downto 0) => rxdata(7 downto 0),
      rxdisperr(0) => rxdisperr,
      rxnotintable(0) => rxnotintable,
      rxphy_correction_timer(63 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED(63 downto 0),
      rxphy_ns_field(31 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED(31 downto 0),
      rxphy_s_field(47 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED(47 downto 0),
      rxrecclk => '0',
      rxrundisp(0) => '0',
      s_axi_aclk => '0',
      s_axi_araddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_arready => NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED,
      s_axi_arvalid => '0',
      s_axi_awaddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_awready => NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED,
      s_axi_awvalid => '0',
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_bvalid => NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED,
      s_axi_rdata(31 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED(31 downto 0),
      s_axi_resetn => '0',
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_rvalid => NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED,
      s_axi_wdata(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_wready => NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED,
      s_axi_wvalid => '0',
      signal_detect => signal_detect,
      speed_is_100 => '0',
      speed_is_10_100 => '0',
      speed_selection(1 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED(1 downto 0),
      status_vector(15 downto 7) => NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED(15 downto 7),
      status_vector(6 downto 0) => \^status_vector\(6 downto 0),
      systemtimer_ns_field(31 downto 0) => B"00000000000000000000000000000000",
      systemtimer_s_field(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      tx_code_group(9 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED(9 downto 0),
      txbuferr => txbuferr,
      txchardispmode => txchardispmode,
      txchardispval => txchardispval,
      txcharisk => txcharisk,
      txdata(7 downto 0) => txdata(7 downto 0),
      userclk => '0',
      userclk2 => CLK
    );
sync_block_rx_reset_done: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block
     port map (
      CLK => CLK,
      data_in => transceiver_inst_n_12,
      data_out => rx_reset_done_i
    );
sync_block_tx_reset_done: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_0
     port map (
      CLK => CLK,
      data_in => transceiver_inst_n_11,
      resetdone => \^resetdone\,
      resetdone_0 => rx_reset_done_i
    );
transceiver_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_transceiver
     port map (
      CLK => CLK,
      D(0) => txchardispmode,
      Q(1 downto 0) => rxclkcorcnt(1 downto 0),
      SR(0) => mgt_rx_reset,
      data_in => transceiver_inst_n_11,
      data_sync_reg1 => data_in,
      enablealign => enablealign,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i => gtxe2_i,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0),
      powerdown => powerdown,
      reset_sync5(0) => mgt_tx_reset,
      rx_fsm_reset_done_int_reg => transceiver_inst_n_12,
      rxbufstatus(0) => rxbuferr,
      rxchariscomma(0) => rxchariscomma,
      rxcharisk(0) => rxcharisk,
      \rxdata_reg[7]_0\(7 downto 0) => rxdata(7 downto 0),
      rxdisperr(0) => rxdisperr,
      rxn => rxn,
      rxnotintable(0) => rxnotintable,
      rxoutclk => rxoutclk,
      rxp => rxp,
      status_vector(0) => \^status_vector\(1),
      txbuferr => txbuferr,
      txchardispval_reg_reg_0(0) => txchardispval,
      txcharisk_reg_reg_0(0) => txcharisk,
      \txdata_reg_reg[7]_0\(7 downto 0) => txdata(7 downto 0),
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support is
  port (
    gtrefclk_p : in STD_LOGIC;
    gtrefclk_n : in STD_LOGIC;
    gtrefclk_out : out STD_LOGIC;
    gtrefclk_bufg_out : out STD_LOGIC;
    txp : out STD_LOGIC;
    txn : out STD_LOGIC;
    rxp : in STD_LOGIC;
    rxn : in STD_LOGIC;
    userclk_out : out STD_LOGIC;
    userclk2_out : out STD_LOGIC;
    rxuserclk_out : out STD_LOGIC;
    rxuserclk2_out : out STD_LOGIC;
    pma_reset_out : out STD_LOGIC;
    mmcm_locked_out : out STD_LOGIC;
    resetdone : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gmii_txd : in STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_tx_en : in STD_LOGIC;
    gmii_tx_er : in STD_LOGIC;
    gmii_rxd : out STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_rx_dv : out STD_LOGIC;
    gmii_rx_er : out STD_LOGIC;
    gmii_isolate : out STD_LOGIC;
    configuration_vector : in STD_LOGIC_VECTOR ( 4 downto 0 );
    status_vector : out STD_LOGIC_VECTOR ( 15 downto 0 );
    reset : in STD_LOGIC;
    signal_detect : in STD_LOGIC;
    gt0_qplloutclk_out : out STD_LOGIC;
    gt0_qplloutrefclk_out : out STD_LOGIC
  );
  attribute EXAMPLE_SIMULATION : integer;
  attribute EXAMPLE_SIMULATION of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support : entity is 0;
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support : entity is "yes";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support is
  signal \<const0>\ : STD_LOGIC;
  signal \^gt0_qplloutclk_out\ : STD_LOGIC;
  signal \^gt0_qplloutrefclk_out\ : STD_LOGIC;
  signal \^gtrefclk_bufg_out\ : STD_LOGIC;
  signal \^gtrefclk_out\ : STD_LOGIC;
  signal \^mmcm_locked_out\ : STD_LOGIC;
  signal mmcm_reset : STD_LOGIC;
  signal \^pma_reset_out\ : STD_LOGIC;
  signal rxoutclk : STD_LOGIC;
  signal \^rxuserclk2_out\ : STD_LOGIC;
  signal \^status_vector\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal txoutclk : STD_LOGIC;
  signal \^userclk2_out\ : STD_LOGIC;
  signal \^userclk_out\ : STD_LOGIC;
begin
  gt0_qplloutclk_out <= \^gt0_qplloutclk_out\;
  gt0_qplloutrefclk_out <= \^gt0_qplloutrefclk_out\;
  gtrefclk_bufg_out <= \^gtrefclk_bufg_out\;
  gtrefclk_out <= \^gtrefclk_out\;
  mmcm_locked_out <= \^mmcm_locked_out\;
  pma_reset_out <= \^pma_reset_out\;
  rxuserclk2_out <= \^rxuserclk2_out\;
  rxuserclk_out <= \^rxuserclk2_out\;
  status_vector(15) <= \<const0>\;
  status_vector(14) <= \<const0>\;
  status_vector(13) <= \<const0>\;
  status_vector(12) <= \<const0>\;
  status_vector(11) <= \<const0>\;
  status_vector(10) <= \<const0>\;
  status_vector(9) <= \<const0>\;
  status_vector(8) <= \<const0>\;
  status_vector(7) <= \<const0>\;
  status_vector(6 downto 0) <= \^status_vector\(6 downto 0);
  userclk2_out <= \^userclk2_out\;
  userclk_out <= \^userclk_out\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
core_clocking_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_clocking
     port map (
      gtrefclk_bufg => \^gtrefclk_bufg_out\,
      gtrefclk_n => gtrefclk_n,
      gtrefclk_out => \^gtrefclk_out\,
      gtrefclk_p => gtrefclk_p,
      mmcm_locked => \^mmcm_locked_out\,
      mmcm_reset => mmcm_reset,
      rxoutclk => rxoutclk,
      rxuserclk2_out => \^rxuserclk2_out\,
      txoutclk => txoutclk,
      userclk => \^userclk_out\,
      userclk2 => \^userclk2_out\
    );
core_gt_common_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_gt_common
     port map (
      gt0_qplloutclk_out => \^gt0_qplloutclk_out\,
      gt0_qplloutrefclk_out => \^gt0_qplloutrefclk_out\,
      gtrefclk_out => \^gtrefclk_out\,
      independent_clock_bufg => independent_clock_bufg,
      \out\(0) => \^pma_reset_out\
    );
core_resets_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_resets
     port map (
      independent_clock_bufg => independent_clock_bufg,
      \out\(0) => \^pma_reset_out\,
      reset => reset
    );
pcs_pma_block_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_block
     port map (
      CLK => \^userclk2_out\,
      configuration_vector(2 downto 0) => configuration_vector(3 downto 1),
      data_in => \^mmcm_locked_out\,
      gmii_isolate => gmii_isolate,
      gmii_rx_dv => gmii_rx_dv,
      gmii_rx_er => gmii_rx_er,
      gmii_rxd(7 downto 0) => gmii_rxd(7 downto 0),
      gmii_tx_en => gmii_tx_en,
      gmii_tx_er => gmii_tx_er,
      gmii_txd(7 downto 0) => gmii_txd(7 downto 0),
      gt0_qplloutclk_out => \^gt0_qplloutclk_out\,
      gt0_qplloutrefclk_out => \^gt0_qplloutrefclk_out\,
      gtrefclk_bufg => \^gtrefclk_bufg_out\,
      gtrefclk_out => \^gtrefclk_out\,
      gtxe2_i => \^userclk_out\,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \^pma_reset_out\,
      resetdone => resetdone,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      signal_detect => signal_detect,
      status_vector(6 downto 0) => \^status_vector\(6 downto 0),
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    gtrefclk_p : in STD_LOGIC;
    gtrefclk_n : in STD_LOGIC;
    gtrefclk_out : out STD_LOGIC;
    gtrefclk_bufg_out : out STD_LOGIC;
    txp : out STD_LOGIC;
    txn : out STD_LOGIC;
    rxp : in STD_LOGIC;
    rxn : in STD_LOGIC;
    resetdone : out STD_LOGIC;
    userclk_out : out STD_LOGIC;
    userclk2_out : out STD_LOGIC;
    rxuserclk_out : out STD_LOGIC;
    rxuserclk2_out : out STD_LOGIC;
    pma_reset_out : out STD_LOGIC;
    mmcm_locked_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gmii_txd : in STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_tx_en : in STD_LOGIC;
    gmii_tx_er : in STD_LOGIC;
    gmii_rxd : out STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_rx_dv : out STD_LOGIC;
    gmii_rx_er : out STD_LOGIC;
    gmii_isolate : out STD_LOGIC;
    configuration_vector : in STD_LOGIC_VECTOR ( 4 downto 0 );
    status_vector : out STD_LOGIC_VECTOR ( 15 downto 0 );
    reset : in STD_LOGIC;
    signal_detect : in STD_LOGIC;
    gt0_qplloutclk_out : out STD_LOGIC;
    gt0_qplloutrefclk_out : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute EXAMPLE_SIMULATION : integer;
  attribute EXAMPLE_SIMULATION of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is 0;
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "gig_ethernet_pcs_pma_v16_2_19,Vivado 2024.1.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \^status_vector\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal NLW_U0_status_vector_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 7 );
  attribute EXAMPLE_SIMULATION of U0 : label is 0;
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
begin
  status_vector(15) <= \<const0>\;
  status_vector(14) <= \<const0>\;
  status_vector(13) <= \<const0>\;
  status_vector(12) <= \<const0>\;
  status_vector(11) <= \<const0>\;
  status_vector(10) <= \<const0>\;
  status_vector(9) <= \<const0>\;
  status_vector(8) <= \<const0>\;
  status_vector(7) <= \<const0>\;
  status_vector(6 downto 0) <= \^status_vector\(6 downto 0);
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support
     port map (
      configuration_vector(4) => '0',
      configuration_vector(3 downto 1) => configuration_vector(3 downto 1),
      configuration_vector(0) => '0',
      gmii_isolate => gmii_isolate,
      gmii_rx_dv => gmii_rx_dv,
      gmii_rx_er => gmii_rx_er,
      gmii_rxd(7 downto 0) => gmii_rxd(7 downto 0),
      gmii_tx_en => gmii_tx_en,
      gmii_tx_er => gmii_tx_er,
      gmii_txd(7 downto 0) => gmii_txd(7 downto 0),
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg_out => gtrefclk_bufg_out,
      gtrefclk_n => gtrefclk_n,
      gtrefclk_out => gtrefclk_out,
      gtrefclk_p => gtrefclk_p,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_locked_out => mmcm_locked_out,
      pma_reset_out => pma_reset_out,
      reset => reset,
      resetdone => resetdone,
      rxn => rxn,
      rxp => rxp,
      rxuserclk2_out => rxuserclk2_out,
      rxuserclk_out => rxuserclk_out,
      signal_detect => signal_detect,
      status_vector(15 downto 7) => NLW_U0_status_vector_UNCONNECTED(15 downto 7),
      status_vector(6 downto 0) => \^status_vector\(6 downto 0),
      txn => txn,
      txp => txp,
      userclk2_out => userclk2_out,
      userclk_out => userclk_out
    );
end STRUCTURE;
