// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// Copyright 2022-2024 Advanced Micro Devices, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2024.1.1 (lin64) Build 5094488 Fri Jun 14 08:57:50 MDT 2024
// Date        : Tue Jul 30 12:07:48 2024
// Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 38 (Thirty Eight)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ gig_ethernet_pcs_pma_0_sim_netlist.v
// Design      : gig_ethernet_pcs_pma_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7k70tfbg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* EXAMPLE_SIMULATION = "0" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "gig_ethernet_pcs_pma_v16_2_19,Vivado 2024.1.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (gtrefclk_p,
    gtrefclk_n,
    gtrefclk_out,
    gtrefclk_bufg_out,
    txp,
    txn,
    rxp,
    rxn,
    resetdone,
    userclk_out,
    userclk2_out,
    rxuserclk_out,
    rxuserclk2_out,
    pma_reset_out,
    mmcm_locked_out,
    independent_clock_bufg,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    configuration_vector,
    status_vector,
    reset,
    signal_detect,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out);
  input gtrefclk_p;
  input gtrefclk_n;
  output gtrefclk_out;
  output gtrefclk_bufg_out;
  output txp;
  output txn;
  input rxp;
  input rxn;
  output resetdone;
  output userclk_out;
  output userclk2_out;
  output rxuserclk_out;
  output rxuserclk2_out;
  output pma_reset_out;
  output mmcm_locked_out;
  input independent_clock_bufg;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  input [4:0]configuration_vector;
  output [15:0]status_vector;
  input reset;
  input signal_detect;
  output gt0_qplloutclk_out;
  output gt0_qplloutrefclk_out;

  wire \<const0> ;
  wire [4:0]configuration_vector;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg_out;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire independent_clock_bufg;
  wire mmcm_locked_out;
  wire pma_reset_out;
  wire reset;
  wire resetdone;
  wire rxn;
  wire rxp;
  wire rxuserclk2_out;
  wire rxuserclk_out;
  wire signal_detect;
  wire [6:0]\^status_vector ;
  wire txn;
  wire txp;
  wire userclk2_out;
  wire userclk_out;
  wire [15:7]NLW_U0_status_vector_UNCONNECTED;

  assign status_vector[15] = \<const0> ;
  assign status_vector[14] = \<const0> ;
  assign status_vector[13] = \<const0> ;
  assign status_vector[12] = \<const0> ;
  assign status_vector[11] = \<const0> ;
  assign status_vector[10] = \<const0> ;
  assign status_vector[9] = \<const0> ;
  assign status_vector[8] = \<const0> ;
  assign status_vector[7] = \<const0> ;
  assign status_vector[6:0] = \^status_vector [6:0];
  GND GND
       (.G(\<const0> ));
  (* EXAMPLE_SIMULATION = "0" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support U0
       (.configuration_vector({1'b0,configuration_vector[3:1],1'b0}),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg_out(gtrefclk_bufg_out),
        .gtrefclk_n(gtrefclk_n),
        .gtrefclk_out(gtrefclk_out),
        .gtrefclk_p(gtrefclk_p),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_locked_out(mmcm_locked_out),
        .pma_reset_out(pma_reset_out),
        .reset(reset),
        .resetdone(resetdone),
        .rxn(rxn),
        .rxp(rxp),
        .rxuserclk2_out(rxuserclk2_out),
        .rxuserclk_out(rxuserclk_out),
        .signal_detect(signal_detect),
        .status_vector({NLW_U0_status_vector_UNCONNECTED[15:7],\^status_vector }),
        .txn(txn),
        .txp(txp),
        .userclk2_out(userclk2_out),
        .userclk_out(userclk_out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD
   (txn,
    txp,
    rxoutclk,
    txoutclk,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i,
    gtxe2_i_0,
    gtxe2_i_1,
    gtxe2_i_2,
    gtxe2_i_3,
    mmcm_reset,
    data_in,
    rx_fsm_reset_done_int_reg,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gtxe2_i_4,
    TXPD,
    RXPD,
    Q,
    gtxe2_i_5,
    gtxe2_i_6,
    gtxe2_i_7,
    out,
    gtxe2_i_8,
    gtxe2_i_9,
    data_sync_reg1,
    data_out);
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i;
  output [1:0]gtxe2_i_0;
  output [1:0]gtxe2_i_1;
  output [1:0]gtxe2_i_2;
  output [1:0]gtxe2_i_3;
  output mmcm_reset;
  output data_in;
  output rx_fsm_reset_done_int_reg;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gtxe2_i_4;
  input [0:0]TXPD;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_5;
  input [1:0]gtxe2_i_6;
  input [1:0]gtxe2_i_7;
  input [0:0]out;
  input gtxe2_i_8;
  input gtxe2_i_9;
  input data_sync_reg1;
  input data_out;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire data_in;
  wire data_out;
  wire data_sync_reg1;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire [15:0]gtxe2_i;
  wire [1:0]gtxe2_i_0;
  wire [1:0]gtxe2_i_1;
  wire [1:0]gtxe2_i_2;
  wire [1:0]gtxe2_i_3;
  wire gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire [1:0]gtxe2_i_7;
  wire gtxe2_i_8;
  wire gtxe2_i_9;
  wire independent_clock_bufg;
  wire mmcm_reset;
  wire [0:0]out;
  wire reset;
  wire reset_out;
  wire rx_fsm_reset_done_int_reg;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_init U0
       (.D(D),
        .Q(Q),
        .RXBUFSTATUS(RXBUFSTATUS),
        .RXPD(RXPD),
        .TXBUFSTATUS(TXBUFSTATUS),
        .TXPD(TXPD),
        .data_in(data_in),
        .data_out(data_out),
        .data_sync_reg1(data_sync_reg1),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(gtxe2_i),
        .gtxe2_i_0(gtxe2_i_0),
        .gtxe2_i_1(gtxe2_i_1),
        .gtxe2_i_2(gtxe2_i_2),
        .gtxe2_i_3(gtxe2_i_3),
        .gtxe2_i_4(gtxe2_i_4),
        .gtxe2_i_5(gtxe2_i_5),
        .gtxe2_i_6(gtxe2_i_6),
        .gtxe2_i_7(gtxe2_i_7),
        .gtxe2_i_8(gtxe2_i_8),
        .gtxe2_i_9(gtxe2_i_9),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out),
        .reset(reset),
        .reset_out(reset_out),
        .rx_fsm_reset_done_int_reg(rx_fsm_reset_done_int_reg),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_GT
   (gtxe2_i_0,
    gt0_cpllrefclklost_i,
    txn,
    txp,
    rxoutclk,
    gtxe2_i_1,
    txoutclk,
    gtxe2_i_2,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i_3,
    gtxe2_i_4,
    gtxe2_i_5,
    gtxe2_i_6,
    gtxe2_i_7,
    independent_clock_bufg,
    cpll_pd0_i,
    cpllreset_in,
    gtrefclk_bufg,
    gtrefclk_out,
    SR,
    gt0_gttxreset_in0_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gt0_rxuserrdy_t,
    gtxe2_i_8,
    TXPD,
    gt0_txuserrdy_t,
    RXPD,
    Q,
    gtxe2_i_9,
    gtxe2_i_10,
    gtxe2_i_11);
  output gtxe2_i_0;
  output gt0_cpllrefclklost_i;
  output txn;
  output txp;
  output rxoutclk;
  output gtxe2_i_1;
  output txoutclk;
  output gtxe2_i_2;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i_3;
  output [1:0]gtxe2_i_4;
  output [1:0]gtxe2_i_5;
  output [1:0]gtxe2_i_6;
  output [1:0]gtxe2_i_7;
  input independent_clock_bufg;
  input cpll_pd0_i;
  input cpllreset_in;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input [0:0]SR;
  input gt0_gttxreset_in0_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gt0_rxuserrdy_t;
  input gtxe2_i_8;
  input [0:0]TXPD;
  input gt0_txuserrdy_t;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_9;
  input [1:0]gtxe2_i_10;
  input [1:0]gtxe2_i_11;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]SR;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire cpll_pd0_i;
  wire cpllreset_in;
  wire gt0_cpllrefclklost_i;
  wire gt0_gttxreset_in0_out;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gt0_rxuserrdy_t;
  wire gt0_txuserrdy_t;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtxe2_i_0;
  wire gtxe2_i_1;
  wire [1:0]gtxe2_i_10;
  wire [1:0]gtxe2_i_11;
  wire gtxe2_i_2;
  wire [15:0]gtxe2_i_3;
  wire [1:0]gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire [1:0]gtxe2_i_7;
  wire gtxe2_i_8;
  wire [1:0]gtxe2_i_9;
  wire gtxe2_i_n_0;
  wire gtxe2_i_n_10;
  wire gtxe2_i_n_16;
  wire gtxe2_i_n_170;
  wire gtxe2_i_n_171;
  wire gtxe2_i_n_172;
  wire gtxe2_i_n_173;
  wire gtxe2_i_n_174;
  wire gtxe2_i_n_175;
  wire gtxe2_i_n_176;
  wire gtxe2_i_n_177;
  wire gtxe2_i_n_178;
  wire gtxe2_i_n_179;
  wire gtxe2_i_n_180;
  wire gtxe2_i_n_181;
  wire gtxe2_i_n_182;
  wire gtxe2_i_n_183;
  wire gtxe2_i_n_184;
  wire gtxe2_i_n_27;
  wire gtxe2_i_n_3;
  wire gtxe2_i_n_38;
  wire gtxe2_i_n_39;
  wire gtxe2_i_n_4;
  wire gtxe2_i_n_46;
  wire gtxe2_i_n_47;
  wire gtxe2_i_n_48;
  wire gtxe2_i_n_49;
  wire gtxe2_i_n_50;
  wire gtxe2_i_n_51;
  wire gtxe2_i_n_52;
  wire gtxe2_i_n_53;
  wire gtxe2_i_n_54;
  wire gtxe2_i_n_55;
  wire gtxe2_i_n_56;
  wire gtxe2_i_n_57;
  wire gtxe2_i_n_58;
  wire gtxe2_i_n_59;
  wire gtxe2_i_n_60;
  wire gtxe2_i_n_61;
  wire gtxe2_i_n_81;
  wire gtxe2_i_n_83;
  wire gtxe2_i_n_84;
  wire gtxe2_i_n_9;
  wire independent_clock_bufg;
  wire reset;
  wire reset_out;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;
  wire NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED;
  wire NLW_gtxe2_i_PHYSTATUS_UNCONNECTED;
  wire NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED;
  wire NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED;
  wire NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED;
  wire NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXDATAVALID_UNCONNECTED;
  wire NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXELECIDLE_UNCONNECTED;
  wire NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED;
  wire NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED;
  wire NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED;
  wire NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXQPISENN_UNCONNECTED;
  wire NLW_gtxe2_i_RXQPISENP_UNCONNECTED;
  wire NLW_gtxe2_i_RXRATEDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED;
  wire NLW_gtxe2_i_RXVALID_UNCONNECTED;
  wire NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED;
  wire NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED;
  wire NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED;
  wire NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED;
  wire NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED;
  wire NLW_gtxe2_i_TXQPISENN_UNCONNECTED;
  wire NLW_gtxe2_i_TXQPISENP_UNCONNECTED;
  wire NLW_gtxe2_i_TXRATEDONE_UNCONNECTED;
  wire [15:0]NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXCHARISK_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXCHBONDO_UNCONNECTED;
  wire [63:16]NLW_gtxe2_i_RXDATA_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXDISPERR_UNCONNECTED;
  wire [2:0]NLW_gtxe2_i_RXHEADER_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED;
  wire [2:0]NLW_gtxe2_i_RXSTATUS_UNCONNECTED;
  wire [9:0]NLW_gtxe2_i_TSTOUT_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  GTXE2_CHANNEL #(
    .ALIGN_COMMA_DOUBLE("FALSE"),
    .ALIGN_COMMA_ENABLE(10'b0001111111),
    .ALIGN_COMMA_WORD(2),
    .ALIGN_MCOMMA_DET("TRUE"),
    .ALIGN_MCOMMA_VALUE(10'b1010000011),
    .ALIGN_PCOMMA_DET("TRUE"),
    .ALIGN_PCOMMA_VALUE(10'b0101111100),
    .CBCC_DATA_SOURCE_SEL("DECODED"),
    .CHAN_BOND_KEEP_ALIGN("FALSE"),
    .CHAN_BOND_MAX_SKEW(1),
    .CHAN_BOND_SEQ_1_1(10'b0000000000),
    .CHAN_BOND_SEQ_1_2(10'b0000000000),
    .CHAN_BOND_SEQ_1_3(10'b0000000000),
    .CHAN_BOND_SEQ_1_4(10'b0000000000),
    .CHAN_BOND_SEQ_1_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_1(10'b0000000000),
    .CHAN_BOND_SEQ_2_2(10'b0000000000),
    .CHAN_BOND_SEQ_2_3(10'b0000000000),
    .CHAN_BOND_SEQ_2_4(10'b0000000000),
    .CHAN_BOND_SEQ_2_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_USE("FALSE"),
    .CHAN_BOND_SEQ_LEN(1),
    .CLK_CORRECT_USE("TRUE"),
    .CLK_COR_KEEP_IDLE("FALSE"),
    .CLK_COR_MAX_LAT(36),
    .CLK_COR_MIN_LAT(33),
    .CLK_COR_PRECEDENCE("TRUE"),
    .CLK_COR_REPEAT_WAIT(0),
    .CLK_COR_SEQ_1_1(10'b0110111100),
    .CLK_COR_SEQ_1_2(10'b0001010000),
    .CLK_COR_SEQ_1_3(10'b0000000000),
    .CLK_COR_SEQ_1_4(10'b0000000000),
    .CLK_COR_SEQ_1_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_1(10'b0110111100),
    .CLK_COR_SEQ_2_2(10'b0010110101),
    .CLK_COR_SEQ_2_3(10'b0000000000),
    .CLK_COR_SEQ_2_4(10'b0000000000),
    .CLK_COR_SEQ_2_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_USE("TRUE"),
    .CLK_COR_SEQ_LEN(2),
    .CPLL_CFG(24'hBC07DC),
    .CPLL_FBDIV(4),
    .CPLL_FBDIV_45(5),
    .CPLL_INIT_CFG(24'h00001E),
    .CPLL_LOCK_CFG(16'h01E8),
    .CPLL_REFCLK_DIV(1),
    .DEC_MCOMMA_DETECT("TRUE"),
    .DEC_PCOMMA_DETECT("TRUE"),
    .DEC_VALID_COMMA_ONLY("FALSE"),
    .DMONITOR_CFG(24'h000A00),
    .ES_CONTROL(6'b000000),
    .ES_ERRDET_EN("FALSE"),
    .ES_EYE_SCAN_EN("TRUE"),
    .ES_HORZ_OFFSET(12'h000),
    .ES_PMA_CFG(10'b0000000000),
    .ES_PRESCALE(5'b00000),
    .ES_QUALIFIER(80'h00000000000000000000),
    .ES_QUAL_MASK(80'h00000000000000000000),
    .ES_SDATA_MASK(80'h00000000000000000000),
    .ES_VERT_OFFSET(9'b000000000),
    .FTS_DESKEW_SEQ_ENABLE(4'b1111),
    .FTS_LANE_DESKEW_CFG(4'b1111),
    .FTS_LANE_DESKEW_EN("FALSE"),
    .GEARBOX_MODE(3'b000),
    .IS_CPLLLOCKDETCLK_INVERTED(1'b0),
    .IS_DRPCLK_INVERTED(1'b0),
    .IS_GTGREFCLK_INVERTED(1'b0),
    .IS_RXUSRCLK2_INVERTED(1'b0),
    .IS_RXUSRCLK_INVERTED(1'b0),
    .IS_TXPHDLYTSTCLK_INVERTED(1'b0),
    .IS_TXUSRCLK2_INVERTED(1'b0),
    .IS_TXUSRCLK_INVERTED(1'b0),
    .OUTREFCLK_SEL_INV(2'b11),
    .PCS_PCIE_EN("FALSE"),
    .PCS_RSVD_ATTR(48'h000000000000),
    .PD_TRANS_TIME_FROM_P2(12'h03C),
    .PD_TRANS_TIME_NONE_P2(8'h19),
    .PD_TRANS_TIME_TO_P2(8'h64),
    .PMA_RSV(32'h00018480),
    .PMA_RSV2(16'h2050),
    .PMA_RSV3(2'b00),
    .PMA_RSV4(32'h00000000),
    .RXBUFRESET_TIME(5'b00001),
    .RXBUF_ADDR_MODE("FULL"),
    .RXBUF_EIDLE_HI_CNT(4'b1000),
    .RXBUF_EIDLE_LO_CNT(4'b0000),
    .RXBUF_EN("TRUE"),
    .RXBUF_RESET_ON_CB_CHANGE("TRUE"),
    .RXBUF_RESET_ON_COMMAALIGN("FALSE"),
    .RXBUF_RESET_ON_EIDLE("FALSE"),
    .RXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .RXBUF_THRESH_OVFLW(61),
    .RXBUF_THRESH_OVRD("FALSE"),
    .RXBUF_THRESH_UNDFLW(8),
    .RXCDRFREQRESET_TIME(5'b00001),
    .RXCDRPHRESET_TIME(5'b00001),
    .RXCDR_CFG(72'h03000023FF10100020),
    .RXCDR_FR_RESET_ON_EIDLE(1'b0),
    .RXCDR_HOLD_DURING_EIDLE(1'b0),
    .RXCDR_LOCK_CFG(6'b010101),
    .RXCDR_PH_RESET_ON_EIDLE(1'b0),
    .RXDFELPMRESET_TIME(7'b0001111),
    .RXDLY_CFG(16'h001F),
    .RXDLY_LCFG(9'h030),
    .RXDLY_TAP_CFG(16'h0000),
    .RXGEARBOX_EN("FALSE"),
    .RXISCANRESET_TIME(5'b00001),
    .RXLPM_HF_CFG(14'b00000011110000),
    .RXLPM_LF_CFG(14'b00000011110000),
    .RXOOB_CFG(7'b0000110),
    .RXOUT_DIV(4),
    .RXPCSRESET_TIME(5'b00001),
    .RXPHDLY_CFG(24'h084020),
    .RXPH_CFG(24'h000000),
    .RXPH_MONITOR_SEL(5'b00000),
    .RXPMARESET_TIME(5'b00011),
    .RXPRBS_ERR_LOOPBACK(1'b0),
    .RXSLIDE_AUTO_WAIT(7),
    .RXSLIDE_MODE("OFF"),
    .RX_BIAS_CFG(12'b000000000100),
    .RX_BUFFER_CFG(6'b000000),
    .RX_CLK25_DIV(5),
    .RX_CLKMUX_PD(1'b1),
    .RX_CM_SEL(2'b11),
    .RX_CM_TRIM(3'b010),
    .RX_DATA_WIDTH(20),
    .RX_DDI_SEL(6'b000000),
    .RX_DEBUG_CFG(12'b000000000000),
    .RX_DEFER_RESET_BUF_EN("TRUE"),
    .RX_DFE_GAIN_CFG(23'h020FEA),
    .RX_DFE_H2_CFG(12'b000000000000),
    .RX_DFE_H3_CFG(12'b000001000000),
    .RX_DFE_H4_CFG(11'b00011110000),
    .RX_DFE_H5_CFG(11'b00011100000),
    .RX_DFE_KL_CFG(13'b0000011111110),
    .RX_DFE_KL_CFG2(32'h301148AC),
    .RX_DFE_LPM_CFG(16'h0904),
    .RX_DFE_LPM_HOLD_DURING_EIDLE(1'b0),
    .RX_DFE_UT_CFG(17'b10001111000000000),
    .RX_DFE_VP_CFG(17'b00011111100000011),
    .RX_DFE_XYD_CFG(13'b0000000000000),
    .RX_DISPERR_SEQ_MATCH("TRUE"),
    .RX_INT_DATAWIDTH(0),
    .RX_OS_CFG(13'b0000010000000),
    .RX_SIG_VALID_DLY(10),
    .RX_XCLK_SEL("RXREC"),
    .SAS_MAX_COM(64),
    .SAS_MIN_COM(36),
    .SATA_BURST_SEQ_LEN(4'b0101),
    .SATA_BURST_VAL(3'b100),
    .SATA_CPLL_CFG("VCO_3000MHZ"),
    .SATA_EIDLE_VAL(3'b100),
    .SATA_MAX_BURST(8),
    .SATA_MAX_INIT(21),
    .SATA_MAX_WAKE(7),
    .SATA_MIN_BURST(4),
    .SATA_MIN_INIT(12),
    .SATA_MIN_WAKE(4),
    .SHOW_REALIGN_COMMA("TRUE"),
    .SIM_CPLLREFCLK_SEL(3'b001),
    .SIM_RECEIVER_DETECT_PASS("TRUE"),
    .SIM_RESET_SPEEDUP("TRUE"),
    .SIM_TX_EIDLE_DRIVE_LEVEL("X"),
    .SIM_VERSION("4.0"),
    .TERM_RCAL_CFG(5'b10000),
    .TERM_RCAL_OVRD(1'b0),
    .TRANS_TIME_RATE(8'h0E),
    .TST_RSV(32'h00000000),
    .TXBUF_EN("TRUE"),
    .TXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .TXDLY_CFG(16'h001F),
    .TXDLY_LCFG(9'h030),
    .TXDLY_TAP_CFG(16'h0000),
    .TXGEARBOX_EN("FALSE"),
    .TXOUT_DIV(4),
    .TXPCSRESET_TIME(5'b00001),
    .TXPHDLY_CFG(24'h084020),
    .TXPH_CFG(16'h0780),
    .TXPH_MONITOR_SEL(5'b00000),
    .TXPMARESET_TIME(5'b00001),
    .TX_CLK25_DIV(5),
    .TX_CLKMUX_PD(1'b1),
    .TX_DATA_WIDTH(20),
    .TX_DEEMPH0(5'b00000),
    .TX_DEEMPH1(5'b00000),
    .TX_DRIVE_MODE("DIRECT"),
    .TX_EIDLE_ASSERT_DELAY(3'b110),
    .TX_EIDLE_DEASSERT_DELAY(3'b100),
    .TX_INT_DATAWIDTH(0),
    .TX_LOOPBACK_DRIVE_HIZ("FALSE"),
    .TX_MAINCURSOR_SEL(1'b0),
    .TX_MARGIN_FULL_0(7'b1001110),
    .TX_MARGIN_FULL_1(7'b1001001),
    .TX_MARGIN_FULL_2(7'b1000101),
    .TX_MARGIN_FULL_3(7'b1000010),
    .TX_MARGIN_FULL_4(7'b1000000),
    .TX_MARGIN_LOW_0(7'b1000110),
    .TX_MARGIN_LOW_1(7'b1000100),
    .TX_MARGIN_LOW_2(7'b1000010),
    .TX_MARGIN_LOW_3(7'b1000000),
    .TX_MARGIN_LOW_4(7'b1000000),
    .TX_PREDRIVER_MODE(1'b0),
    .TX_QPI_STATUS_EN(1'b0),
    .TX_RXDETECT_CFG(14'h1832),
    .TX_RXDETECT_REF(3'b100),
    .TX_XCLK_SEL("TXOUT"),
    .UCODEER_CLR(1'b0)) 
    gtxe2_i
       (.CFGRESET(1'b0),
        .CLKRSVD({1'b0,1'b0,1'b0,1'b0}),
        .CPLLFBCLKLOST(gtxe2_i_n_0),
        .CPLLLOCK(gtxe2_i_0),
        .CPLLLOCKDETCLK(independent_clock_bufg),
        .CPLLLOCKEN(1'b1),
        .CPLLPD(cpll_pd0_i),
        .CPLLREFCLKLOST(gt0_cpllrefclklost_i),
        .CPLLREFCLKSEL({1'b0,1'b0,1'b1}),
        .CPLLRESET(cpllreset_in),
        .DMONITOROUT({gtxe2_i_n_177,gtxe2_i_n_178,gtxe2_i_n_179,gtxe2_i_n_180,gtxe2_i_n_181,gtxe2_i_n_182,gtxe2_i_n_183,gtxe2_i_n_184}),
        .DRPADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPCLK(gtrefclk_bufg),
        .DRPDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPDO({gtxe2_i_n_46,gtxe2_i_n_47,gtxe2_i_n_48,gtxe2_i_n_49,gtxe2_i_n_50,gtxe2_i_n_51,gtxe2_i_n_52,gtxe2_i_n_53,gtxe2_i_n_54,gtxe2_i_n_55,gtxe2_i_n_56,gtxe2_i_n_57,gtxe2_i_n_58,gtxe2_i_n_59,gtxe2_i_n_60,gtxe2_i_n_61}),
        .DRPEN(1'b0),
        .DRPRDY(gtxe2_i_n_3),
        .DRPWE(1'b0),
        .EYESCANDATAERROR(gtxe2_i_n_4),
        .EYESCANMODE(1'b0),
        .EYESCANRESET(1'b0),
        .EYESCANTRIGGER(1'b0),
        .GTGREFCLK(1'b0),
        .GTNORTHREFCLK0(1'b0),
        .GTNORTHREFCLK1(1'b0),
        .GTREFCLK0(gtrefclk_out),
        .GTREFCLK1(1'b0),
        .GTREFCLKMONITOR(NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED),
        .GTRESETSEL(1'b0),
        .GTRSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .GTRXRESET(SR),
        .GTSOUTHREFCLK0(1'b0),
        .GTSOUTHREFCLK1(1'b0),
        .GTTXRESET(gt0_gttxreset_in0_out),
        .GTXRXN(rxn),
        .GTXRXP(rxp),
        .GTXTXN(txn),
        .GTXTXP(txp),
        .LOOPBACK({1'b0,1'b0,1'b0}),
        .PCSRSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDIN2({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDOUT(NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED[15:0]),
        .PHYSTATUS(NLW_gtxe2_i_PHYSTATUS_UNCONNECTED),
        .PMARSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PMARSVDIN2({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLCLK(gt0_qplloutclk_out),
        .QPLLREFCLK(gt0_qplloutrefclk_out),
        .RESETOVRD(1'b0),
        .RX8B10BEN(1'b1),
        .RXBUFRESET(1'b0),
        .RXBUFSTATUS({RXBUFSTATUS,gtxe2_i_n_83,gtxe2_i_n_84}),
        .RXBYTEISALIGNED(gtxe2_i_n_9),
        .RXBYTEREALIGN(gtxe2_i_n_10),
        .RXCDRFREQRESET(1'b0),
        .RXCDRHOLD(1'b0),
        .RXCDRLOCK(NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED),
        .RXCDROVRDEN(1'b0),
        .RXCDRRESET(1'b0),
        .RXCDRRESETRSV(1'b0),
        .RXCHANBONDSEQ(NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED),
        .RXCHANISALIGNED(NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED),
        .RXCHANREALIGN(NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED),
        .RXCHARISCOMMA({NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED[7:2],gtxe2_i_4}),
        .RXCHARISK({NLW_gtxe2_i_RXCHARISK_UNCONNECTED[7:2],gtxe2_i_5}),
        .RXCHBONDEN(1'b0),
        .RXCHBONDI({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RXCHBONDLEVEL({1'b0,1'b0,1'b0}),
        .RXCHBONDMASTER(1'b0),
        .RXCHBONDO(NLW_gtxe2_i_RXCHBONDO_UNCONNECTED[4:0]),
        .RXCHBONDSLAVE(1'b0),
        .RXCLKCORCNT(D),
        .RXCOMINITDET(NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED),
        .RXCOMMADET(gtxe2_i_n_16),
        .RXCOMMADETEN(1'b1),
        .RXCOMSASDET(NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED),
        .RXCOMWAKEDET(NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED),
        .RXDATA({NLW_gtxe2_i_RXDATA_UNCONNECTED[63:16],gtxe2_i_3}),
        .RXDATAVALID(NLW_gtxe2_i_RXDATAVALID_UNCONNECTED),
        .RXDDIEN(1'b0),
        .RXDFEAGCHOLD(1'b0),
        .RXDFEAGCOVRDEN(1'b0),
        .RXDFECM1EN(1'b0),
        .RXDFELFHOLD(1'b0),
        .RXDFELFOVRDEN(1'b0),
        .RXDFELPMRESET(1'b0),
        .RXDFETAP2HOLD(1'b0),
        .RXDFETAP2OVRDEN(1'b0),
        .RXDFETAP3HOLD(1'b0),
        .RXDFETAP3OVRDEN(1'b0),
        .RXDFETAP4HOLD(1'b0),
        .RXDFETAP4OVRDEN(1'b0),
        .RXDFETAP5HOLD(1'b0),
        .RXDFETAP5OVRDEN(1'b0),
        .RXDFEUTHOLD(1'b0),
        .RXDFEUTOVRDEN(1'b0),
        .RXDFEVPHOLD(1'b0),
        .RXDFEVPOVRDEN(1'b0),
        .RXDFEVSEN(1'b0),
        .RXDFEXYDEN(1'b1),
        .RXDFEXYDHOLD(1'b0),
        .RXDFEXYDOVRDEN(1'b0),
        .RXDISPERR({NLW_gtxe2_i_RXDISPERR_UNCONNECTED[7:2],gtxe2_i_6}),
        .RXDLYBYPASS(1'b1),
        .RXDLYEN(1'b0),
        .RXDLYOVRDEN(1'b0),
        .RXDLYSRESET(1'b0),
        .RXDLYSRESETDONE(NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED),
        .RXELECIDLE(NLW_gtxe2_i_RXELECIDLE_UNCONNECTED),
        .RXELECIDLEMODE({1'b1,1'b1}),
        .RXGEARBOXSLIP(1'b0),
        .RXHEADER(NLW_gtxe2_i_RXHEADER_UNCONNECTED[2:0]),
        .RXHEADERVALID(NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED),
        .RXLPMEN(1'b1),
        .RXLPMHFHOLD(1'b0),
        .RXLPMHFOVRDEN(1'b0),
        .RXLPMLFHOLD(1'b0),
        .RXLPMLFKLOVRDEN(1'b0),
        .RXMCOMMAALIGNEN(reset_out),
        .RXMONITOROUT({gtxe2_i_n_170,gtxe2_i_n_171,gtxe2_i_n_172,gtxe2_i_n_173,gtxe2_i_n_174,gtxe2_i_n_175,gtxe2_i_n_176}),
        .RXMONITORSEL({1'b0,1'b0}),
        .RXNOTINTABLE({NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED[7:2],gtxe2_i_7}),
        .RXOOBRESET(1'b0),
        .RXOSHOLD(1'b0),
        .RXOSOVRDEN(1'b0),
        .RXOUTCLK(rxoutclk),
        .RXOUTCLKFABRIC(NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED),
        .RXOUTCLKPCS(NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED),
        .RXOUTCLKSEL({1'b0,1'b1,1'b0}),
        .RXPCOMMAALIGNEN(reset_out),
        .RXPCSRESET(reset),
        .RXPD({RXPD,RXPD}),
        .RXPHALIGN(1'b0),
        .RXPHALIGNDONE(NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED),
        .RXPHALIGNEN(1'b0),
        .RXPHDLYPD(1'b0),
        .RXPHDLYRESET(1'b0),
        .RXPHMONITOR(NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED[4:0]),
        .RXPHOVRDEN(1'b0),
        .RXPHSLIPMONITOR(NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED[4:0]),
        .RXPMARESET(1'b0),
        .RXPOLARITY(1'b0),
        .RXPRBSCNTRESET(1'b0),
        .RXPRBSERR(gtxe2_i_n_27),
        .RXPRBSSEL({1'b0,1'b0,1'b0}),
        .RXQPIEN(1'b0),
        .RXQPISENN(NLW_gtxe2_i_RXQPISENN_UNCONNECTED),
        .RXQPISENP(NLW_gtxe2_i_RXQPISENP_UNCONNECTED),
        .RXRATE({1'b0,1'b0,1'b0}),
        .RXRATEDONE(NLW_gtxe2_i_RXRATEDONE_UNCONNECTED),
        .RXRESETDONE(gtxe2_i_1),
        .RXSLIDE(1'b0),
        .RXSTARTOFSEQ(NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED),
        .RXSTATUS(NLW_gtxe2_i_RXSTATUS_UNCONNECTED[2:0]),
        .RXSYSCLKSEL({1'b0,1'b0}),
        .RXUSERRDY(gt0_rxuserrdy_t),
        .RXUSRCLK(gtxe2_i_8),
        .RXUSRCLK2(gtxe2_i_8),
        .RXVALID(NLW_gtxe2_i_RXVALID_UNCONNECTED),
        .SETERRSTATUS(1'b0),
        .TSTIN({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .TSTOUT(NLW_gtxe2_i_TSTOUT_UNCONNECTED[9:0]),
        .TX8B10BBYPASS({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX8B10BEN(1'b1),
        .TXBUFDIFFCTRL({1'b1,1'b0,1'b0}),
        .TXBUFSTATUS({TXBUFSTATUS,gtxe2_i_n_81}),
        .TXCHARDISPMODE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtxe2_i_9}),
        .TXCHARDISPVAL({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtxe2_i_10}),
        .TXCHARISK({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtxe2_i_11}),
        .TXCOMFINISH(NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED),
        .TXCOMINIT(1'b0),
        .TXCOMSAS(1'b0),
        .TXCOMWAKE(1'b0),
        .TXDATA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q}),
        .TXDEEMPH(1'b0),
        .TXDETECTRX(1'b0),
        .TXDIFFCTRL({1'b1,1'b0,1'b0,1'b0}),
        .TXDIFFPD(1'b0),
        .TXDLYBYPASS(1'b1),
        .TXDLYEN(1'b0),
        .TXDLYHOLD(1'b0),
        .TXDLYOVRDEN(1'b0),
        .TXDLYSRESET(1'b0),
        .TXDLYSRESETDONE(NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED),
        .TXDLYUPDOWN(1'b0),
        .TXELECIDLE(TXPD),
        .TXGEARBOXREADY(NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED),
        .TXHEADER({1'b0,1'b0,1'b0}),
        .TXINHIBIT(1'b0),
        .TXMAINCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXMARGIN({1'b0,1'b0,1'b0}),
        .TXOUTCLK(txoutclk),
        .TXOUTCLKFABRIC(gtxe2_i_n_38),
        .TXOUTCLKPCS(gtxe2_i_n_39),
        .TXOUTCLKSEL({1'b1,1'b0,1'b0}),
        .TXPCSRESET(1'b0),
        .TXPD({TXPD,TXPD}),
        .TXPDELECIDLEMODE(1'b0),
        .TXPHALIGN(1'b0),
        .TXPHALIGNDONE(NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED),
        .TXPHALIGNEN(1'b0),
        .TXPHDLYPD(1'b0),
        .TXPHDLYRESET(1'b0),
        .TXPHDLYTSTCLK(1'b0),
        .TXPHINIT(1'b0),
        .TXPHINITDONE(NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED),
        .TXPHOVRDEN(1'b0),
        .TXPISOPD(1'b0),
        .TXPMARESET(1'b0),
        .TXPOLARITY(1'b0),
        .TXPOSTCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPOSTCURSORINV(1'b0),
        .TXPRBSFORCEERR(1'b0),
        .TXPRBSSEL({1'b0,1'b0,1'b0}),
        .TXPRECURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPRECURSORINV(1'b0),
        .TXQPIBIASEN(1'b0),
        .TXQPISENN(NLW_gtxe2_i_TXQPISENN_UNCONNECTED),
        .TXQPISENP(NLW_gtxe2_i_TXQPISENP_UNCONNECTED),
        .TXQPISTRONGPDOWN(1'b0),
        .TXQPIWEAKPUP(1'b0),
        .TXRATE({1'b0,1'b0,1'b0}),
        .TXRATEDONE(NLW_gtxe2_i_TXRATEDONE_UNCONNECTED),
        .TXRESETDONE(gtxe2_i_2),
        .TXSEQUENCE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXSTARTSEQ(1'b0),
        .TXSWING(1'b0),
        .TXSYSCLKSEL({1'b0,1'b0}),
        .TXUSERRDY(gt0_txuserrdy_t),
        .TXUSRCLK(gtxe2_i_8),
        .TXUSRCLK2(gtxe2_i_8));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_init
   (txn,
    txp,
    rxoutclk,
    txoutclk,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i,
    gtxe2_i_0,
    gtxe2_i_1,
    gtxe2_i_2,
    gtxe2_i_3,
    mmcm_reset,
    data_in,
    rx_fsm_reset_done_int_reg,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gtxe2_i_4,
    TXPD,
    RXPD,
    Q,
    gtxe2_i_5,
    gtxe2_i_6,
    gtxe2_i_7,
    out,
    gtxe2_i_8,
    gtxe2_i_9,
    data_sync_reg1,
    data_out);
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i;
  output [1:0]gtxe2_i_0;
  output [1:0]gtxe2_i_1;
  output [1:0]gtxe2_i_2;
  output [1:0]gtxe2_i_3;
  output mmcm_reset;
  output data_in;
  output rx_fsm_reset_done_int_reg;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gtxe2_i_4;
  input [0:0]TXPD;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_5;
  input [1:0]gtxe2_i_6;
  input [1:0]gtxe2_i_7;
  input [0:0]out;
  input gtxe2_i_8;
  input gtxe2_i_9;
  input data_sync_reg1;
  input data_out;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire [13:1]data0;
  wire data_in;
  wire data_out;
  wire data_sync_reg1;
  wire gt0_cpllrefclklost_i;
  wire gt0_cpllreset_t;
  wire gt0_gtrxreset_in1_out;
  wire gt0_gttxreset_in0_out;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire [13:0]gt0_rx_cdrlock_counter;
  wire gt0_rx_cdrlock_counter0_carry__0_n_0;
  wire gt0_rx_cdrlock_counter0_carry__0_n_1;
  wire gt0_rx_cdrlock_counter0_carry__0_n_2;
  wire gt0_rx_cdrlock_counter0_carry__0_n_3;
  wire gt0_rx_cdrlock_counter0_carry__1_n_0;
  wire gt0_rx_cdrlock_counter0_carry__1_n_1;
  wire gt0_rx_cdrlock_counter0_carry__1_n_2;
  wire gt0_rx_cdrlock_counter0_carry__1_n_3;
  wire gt0_rx_cdrlock_counter0_carry_n_0;
  wire gt0_rx_cdrlock_counter0_carry_n_1;
  wire gt0_rx_cdrlock_counter0_carry_n_2;
  wire gt0_rx_cdrlock_counter0_carry_n_3;
  wire \gt0_rx_cdrlock_counter[0]_i_2_n_0 ;
  wire \gt0_rx_cdrlock_counter[13]_i_2_n_0 ;
  wire \gt0_rx_cdrlock_counter[13]_i_3_n_0 ;
  wire \gt0_rx_cdrlock_counter[13]_i_4_n_0 ;
  wire [13:0]gt0_rx_cdrlock_counter_0;
  wire gt0_rx_cdrlocked_i_1_n_0;
  wire gt0_rx_cdrlocked_reg_n_0;
  wire gt0_rxuserrdy_t;
  wire gt0_txuserrdy_t;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtwizard_i_n_0;
  wire gtwizard_i_n_5;
  wire gtwizard_i_n_7;
  wire [15:0]gtxe2_i;
  wire [1:0]gtxe2_i_0;
  wire [1:0]gtxe2_i_1;
  wire [1:0]gtxe2_i_2;
  wire [1:0]gtxe2_i_3;
  wire gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire [1:0]gtxe2_i_7;
  wire gtxe2_i_8;
  wire gtxe2_i_9;
  wire independent_clock_bufg;
  wire mmcm_reset;
  wire [0:0]out;
  wire reset;
  wire reset_out;
  wire rx_fsm_reset_done_int_reg;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;
  wire [3:0]NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED;
  wire [3:1]NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED;

  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry
       (.CI(1'b0),
        .CO({gt0_rx_cdrlock_counter0_carry_n_0,gt0_rx_cdrlock_counter0_carry_n_1,gt0_rx_cdrlock_counter0_carry_n_2,gt0_rx_cdrlock_counter0_carry_n_3}),
        .CYINIT(gt0_rx_cdrlock_counter[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[4:1]),
        .S(gt0_rx_cdrlock_counter[4:1]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry__0
       (.CI(gt0_rx_cdrlock_counter0_carry_n_0),
        .CO({gt0_rx_cdrlock_counter0_carry__0_n_0,gt0_rx_cdrlock_counter0_carry__0_n_1,gt0_rx_cdrlock_counter0_carry__0_n_2,gt0_rx_cdrlock_counter0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[8:5]),
        .S(gt0_rx_cdrlock_counter[8:5]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry__1
       (.CI(gt0_rx_cdrlock_counter0_carry__0_n_0),
        .CO({gt0_rx_cdrlock_counter0_carry__1_n_0,gt0_rx_cdrlock_counter0_carry__1_n_1,gt0_rx_cdrlock_counter0_carry__1_n_2,gt0_rx_cdrlock_counter0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[12:9]),
        .S(gt0_rx_cdrlock_counter[12:9]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry__2
       (.CI(gt0_rx_cdrlock_counter0_carry__1_n_0),
        .CO(NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED[3:1],data0[13]}),
        .S({1'b0,1'b0,1'b0,gt0_rx_cdrlock_counter[13]}));
  LUT2 #(
    .INIT(4'h2)) 
    \gt0_rx_cdrlock_counter[0]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[0]_i_2_n_0 ),
        .I1(gt0_rx_cdrlock_counter[0]),
        .O(gt0_rx_cdrlock_counter_0[0]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFB)) 
    \gt0_rx_cdrlock_counter[0]_i_2 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I1(gt0_rx_cdrlock_counter[4]),
        .I2(gt0_rx_cdrlock_counter[5]),
        .I3(gt0_rx_cdrlock_counter[7]),
        .I4(gt0_rx_cdrlock_counter[6]),
        .I5(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .O(\gt0_rx_cdrlock_counter[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[10]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[10]),
        .O(gt0_rx_cdrlock_counter_0[10]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[11]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[11]),
        .O(gt0_rx_cdrlock_counter_0[11]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[12]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[12]),
        .O(gt0_rx_cdrlock_counter_0[12]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[13]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[13]),
        .O(gt0_rx_cdrlock_counter_0[13]));
  LUT5 #(
    .INIT(32'hFFFFFFEF)) 
    \gt0_rx_cdrlock_counter[13]_i_2 
       (.I0(gt0_rx_cdrlock_counter[1]),
        .I1(gt0_rx_cdrlock_counter[12]),
        .I2(gt0_rx_cdrlock_counter[13]),
        .I3(gt0_rx_cdrlock_counter[3]),
        .I4(gt0_rx_cdrlock_counter[2]),
        .O(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \gt0_rx_cdrlock_counter[13]_i_3 
       (.I0(gt0_rx_cdrlock_counter[4]),
        .I1(gt0_rx_cdrlock_counter[5]),
        .I2(gt0_rx_cdrlock_counter[7]),
        .I3(gt0_rx_cdrlock_counter[6]),
        .O(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFF7F)) 
    \gt0_rx_cdrlock_counter[13]_i_4 
       (.I0(gt0_rx_cdrlock_counter[9]),
        .I1(gt0_rx_cdrlock_counter[8]),
        .I2(gt0_rx_cdrlock_counter[10]),
        .I3(gt0_rx_cdrlock_counter[11]),
        .O(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[1]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[1]),
        .O(gt0_rx_cdrlock_counter_0[1]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[2]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[2]),
        .O(gt0_rx_cdrlock_counter_0[2]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[3]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[3]),
        .O(gt0_rx_cdrlock_counter_0[3]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[4]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[4]),
        .O(gt0_rx_cdrlock_counter_0[4]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[5]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[5]),
        .O(gt0_rx_cdrlock_counter_0[5]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[6]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[6]),
        .O(gt0_rx_cdrlock_counter_0[6]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[7]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[7]),
        .O(gt0_rx_cdrlock_counter_0[7]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[8]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[8]),
        .O(gt0_rx_cdrlock_counter_0[8]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[9]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[9]),
        .O(gt0_rx_cdrlock_counter_0[9]));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[0]),
        .Q(gt0_rx_cdrlock_counter[0]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[10] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[10]),
        .Q(gt0_rx_cdrlock_counter[10]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[11] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[11]),
        .Q(gt0_rx_cdrlock_counter[11]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[12] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[12]),
        .Q(gt0_rx_cdrlock_counter[12]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[13] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[13]),
        .Q(gt0_rx_cdrlock_counter[13]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[1]),
        .Q(gt0_rx_cdrlock_counter[1]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[2]),
        .Q(gt0_rx_cdrlock_counter[2]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[3]),
        .Q(gt0_rx_cdrlock_counter[3]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[4] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[4]),
        .Q(gt0_rx_cdrlock_counter[4]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[5] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[5]),
        .Q(gt0_rx_cdrlock_counter[5]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[6] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[6]),
        .Q(gt0_rx_cdrlock_counter[6]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[7] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[7]),
        .Q(gt0_rx_cdrlock_counter[7]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[8] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[8]),
        .Q(gt0_rx_cdrlock_counter[8]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[9] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[9]),
        .Q(gt0_rx_cdrlock_counter[9]),
        .R(gt0_gtrxreset_in1_out));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    gt0_rx_cdrlocked_i_1
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(gt0_rx_cdrlocked_reg_n_0),
        .O(gt0_rx_cdrlocked_i_1_n_0));
  FDRE gt0_rx_cdrlocked_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlocked_i_1_n_0),
        .Q(gt0_rx_cdrlocked_reg_n_0),
        .R(gt0_gtrxreset_in1_out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_RX_STARTUP_FSM gt0_rxresetfsm_i
       (.\FSM_sequential_rx_state_reg[0]_0 (gt0_rx_cdrlocked_reg_n_0),
        .SR(gt0_gtrxreset_in1_out),
        .data_in(rx_fsm_reset_done_int_reg),
        .data_out(data_out),
        .data_sync_reg1(gtwizard_i_n_5),
        .data_sync_reg1_0(data_sync_reg1),
        .data_sync_reg1_1(gtwizard_i_n_0),
        .data_sync_reg6(gtxe2_i_4),
        .gt0_rxuserrdy_t(gt0_rxuserrdy_t),
        .gtxe2_i(gtxe2_i_8),
        .independent_clock_bufg(independent_clock_bufg),
        .out(out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_TX_STARTUP_FSM gt0_txresetfsm_i
       (.data_in(data_in),
        .data_sync_reg1(gtxe2_i_4),
        .data_sync_reg1_0(gtwizard_i_n_7),
        .data_sync_reg1_1(data_sync_reg1),
        .data_sync_reg1_2(gtwizard_i_n_0),
        .gt0_cpllrefclklost_i(gt0_cpllrefclklost_i),
        .gt0_cpllreset_t(gt0_cpllreset_t),
        .gt0_gttxreset_in0_out(gt0_gttxreset_in0_out),
        .gt0_txuserrdy_t(gt0_txuserrdy_t),
        .gtxe2_i(gtxe2_i_9),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt gtwizard_i
       (.D(D),
        .Q(Q),
        .RXBUFSTATUS(RXBUFSTATUS),
        .RXPD(RXPD),
        .SR(gt0_gtrxreset_in1_out),
        .TXBUFSTATUS(TXBUFSTATUS),
        .TXPD(TXPD),
        .gt0_cpllrefclklost_i(gt0_cpllrefclklost_i),
        .gt0_cpllreset_t(gt0_cpllreset_t),
        .gt0_gttxreset_in0_out(gt0_gttxreset_in0_out),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gt0_rxuserrdy_t(gt0_rxuserrdy_t),
        .gt0_txuserrdy_t(gt0_txuserrdy_t),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(gtwizard_i_n_0),
        .gtxe2_i_0(gtwizard_i_n_5),
        .gtxe2_i_1(gtwizard_i_n_7),
        .gtxe2_i_10(gtxe2_i_7),
        .gtxe2_i_2(gtxe2_i),
        .gtxe2_i_3(gtxe2_i_0),
        .gtxe2_i_4(gtxe2_i_1),
        .gtxe2_i_5(gtxe2_i_2),
        .gtxe2_i_6(gtxe2_i_3),
        .gtxe2_i_7(gtxe2_i_4),
        .gtxe2_i_8(gtxe2_i_5),
        .gtxe2_i_9(gtxe2_i_6),
        .independent_clock_bufg(independent_clock_bufg),
        .reset(reset),
        .reset_out(reset_out),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt
   (gtxe2_i,
    gt0_cpllrefclklost_i,
    txn,
    txp,
    rxoutclk,
    gtxe2_i_0,
    txoutclk,
    gtxe2_i_1,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i_2,
    gtxe2_i_3,
    gtxe2_i_4,
    gtxe2_i_5,
    gtxe2_i_6,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    SR,
    gt0_gttxreset_in0_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gt0_rxuserrdy_t,
    gtxe2_i_7,
    TXPD,
    gt0_txuserrdy_t,
    RXPD,
    Q,
    gtxe2_i_8,
    gtxe2_i_9,
    gtxe2_i_10,
    gt0_cpllreset_t);
  output gtxe2_i;
  output gt0_cpllrefclklost_i;
  output txn;
  output txp;
  output rxoutclk;
  output gtxe2_i_0;
  output txoutclk;
  output gtxe2_i_1;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i_2;
  output [1:0]gtxe2_i_3;
  output [1:0]gtxe2_i_4;
  output [1:0]gtxe2_i_5;
  output [1:0]gtxe2_i_6;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input [0:0]SR;
  input gt0_gttxreset_in0_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gt0_rxuserrdy_t;
  input gtxe2_i_7;
  input [0:0]TXPD;
  input gt0_txuserrdy_t;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_8;
  input [1:0]gtxe2_i_9;
  input [1:0]gtxe2_i_10;
  input gt0_cpllreset_t;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]SR;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire cpll_pd0_i;
  wire cpllreset_in;
  wire gt0_cpllrefclklost_i;
  wire gt0_cpllreset_t;
  wire gt0_gttxreset_in0_out;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gt0_rxuserrdy_t;
  wire gt0_txuserrdy_t;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtxe2_i;
  wire gtxe2_i_0;
  wire gtxe2_i_1;
  wire [1:0]gtxe2_i_10;
  wire [15:0]gtxe2_i_2;
  wire [1:0]gtxe2_i_3;
  wire [1:0]gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire gtxe2_i_7;
  wire [1:0]gtxe2_i_8;
  wire [1:0]gtxe2_i_9;
  wire independent_clock_bufg;
  wire reset;
  wire reset_out;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_cpll_railing cpll_railing0_i
       (.cpll_pd0_i(cpll_pd0_i),
        .cpllreset_in(cpllreset_in),
        .gt0_cpllreset_t(gt0_cpllreset_t),
        .gtrefclk_bufg(gtrefclk_bufg));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_GT gt0_GTWIZARD_i
       (.D(D),
        .Q(Q),
        .RXBUFSTATUS(RXBUFSTATUS),
        .RXPD(RXPD),
        .SR(SR),
        .TXBUFSTATUS(TXBUFSTATUS),
        .TXPD(TXPD),
        .cpll_pd0_i(cpll_pd0_i),
        .cpllreset_in(cpllreset_in),
        .gt0_cpllrefclklost_i(gt0_cpllrefclklost_i),
        .gt0_gttxreset_in0_out(gt0_gttxreset_in0_out),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gt0_rxuserrdy_t(gt0_rxuserrdy_t),
        .gt0_txuserrdy_t(gt0_txuserrdy_t),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i_0(gtxe2_i),
        .gtxe2_i_1(gtxe2_i_0),
        .gtxe2_i_10(gtxe2_i_9),
        .gtxe2_i_11(gtxe2_i_10),
        .gtxe2_i_2(gtxe2_i_1),
        .gtxe2_i_3(gtxe2_i_2),
        .gtxe2_i_4(gtxe2_i_3),
        .gtxe2_i_5(gtxe2_i_4),
        .gtxe2_i_6(gtxe2_i_5),
        .gtxe2_i_7(gtxe2_i_6),
        .gtxe2_i_8(gtxe2_i_7),
        .gtxe2_i_9(gtxe2_i_8),
        .independent_clock_bufg(independent_clock_bufg),
        .reset(reset),
        .reset_out(reset_out),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_RX_STARTUP_FSM
   (data_in,
    gt0_rxuserrdy_t,
    SR,
    independent_clock_bufg,
    data_sync_reg6,
    out,
    gtxe2_i,
    \FSM_sequential_rx_state_reg[0]_0 ,
    data_sync_reg1,
    data_sync_reg1_0,
    data_out,
    data_sync_reg1_1);
  output data_in;
  output gt0_rxuserrdy_t;
  output [0:0]SR;
  input independent_clock_bufg;
  input data_sync_reg6;
  input [0:0]out;
  input gtxe2_i;
  input \FSM_sequential_rx_state_reg[0]_0 ;
  input data_sync_reg1;
  input data_sync_reg1_0;
  input data_out;
  input data_sync_reg1_1;

  wire \FSM_sequential_rx_state[0]_i_2_n_0 ;
  wire \FSM_sequential_rx_state[1]_i_3_n_0 ;
  wire \FSM_sequential_rx_state[2]_i_2_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_10_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_3_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_7_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_9_n_0 ;
  wire \FSM_sequential_rx_state_reg[0]_0 ;
  wire GTRXRESET;
  wire RXUSERRDY_i_1_n_0;
  wire [0:0]SR;
  wire check_tlock_max_i_1_n_0;
  wire check_tlock_max_reg_n_0;
  wire data_in;
  wire data_out;
  wire data_out_0;
  wire data_sync_reg1;
  wire data_sync_reg1_0;
  wire data_sync_reg1_1;
  wire data_sync_reg6;
  wire gt0_rxuserrdy_t;
  wire gtrxreset_i_i_1_n_0;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire \init_wait_count[0]_i_1__0_n_0 ;
  wire \init_wait_count[6]_i_1__0_n_0 ;
  wire \init_wait_count[6]_i_3__0_n_0 ;
  wire [6:0]init_wait_count_reg;
  wire init_wait_done_i_1__0_n_0;
  wire init_wait_done_reg_n_0;
  wire \mmcm_lock_count[2]_i_1__0_n_0 ;
  wire \mmcm_lock_count[3]_i_1__0_n_0 ;
  wire \mmcm_lock_count[4]_i_1__0_n_0 ;
  wire \mmcm_lock_count[5]_i_1__0_n_0 ;
  wire \mmcm_lock_count[6]_i_1__0_n_0 ;
  wire \mmcm_lock_count[7]_i_2__0_n_0 ;
  wire \mmcm_lock_count[7]_i_3__0_n_0 ;
  wire [7:0]mmcm_lock_count_reg;
  wire mmcm_lock_i;
  wire mmcm_lock_reclocked;
  wire mmcm_lock_reclocked_i_1_n_0;
  wire mmcm_lock_reclocked_i_2__0_n_0;
  wire [0:0]out;
  wire [6:1]p_0_in__2;
  wire [1:0]p_0_in__3;
  wire reset_time_out_i_3_n_0;
  wire reset_time_out_i_4_n_0;
  wire reset_time_out_reg_n_0;
  wire run_phase_alignment_int_i_1__0_n_0;
  wire run_phase_alignment_int_reg_n_0;
  wire run_phase_alignment_int_s3_reg_n_0;
  wire rx_fsm_reset_done_int_i_5_n_0;
  wire rx_fsm_reset_done_int_i_6_n_0;
  wire rx_fsm_reset_done_int_s2;
  wire rx_fsm_reset_done_int_s3;
  wire [3:0]rx_state;
  wire [3:0]rx_state__0;
  wire rxresetdone_s2;
  wire rxresetdone_s3;
  wire sync_cplllock_n_0;
  wire sync_data_valid_n_0;
  wire sync_data_valid_n_1;
  wire sync_data_valid_n_5;
  wire sync_mmcm_lock_reclocked_n_0;
  wire time_out_100us_i_1_n_0;
  wire time_out_100us_i_2_n_0;
  wire time_out_100us_i_3_n_0;
  wire time_out_100us_reg_n_0;
  wire time_out_1us_i_1_n_0;
  wire time_out_1us_i_2_n_0;
  wire time_out_1us_i_3_n_0;
  wire time_out_1us_reg_n_0;
  wire time_out_2ms_i_1_n_0;
  wire time_out_2ms_i_2_n_0;
  wire time_out_2ms_i_3__0_n_0;
  wire time_out_2ms_i_4_n_0;
  wire time_out_2ms_reg_n_0;
  wire time_out_counter;
  wire \time_out_counter[0]_i_3_n_0 ;
  wire [18:0]time_out_counter_reg;
  wire \time_out_counter_reg[0]_i_2__0_n_0 ;
  wire \time_out_counter_reg[0]_i_2__0_n_1 ;
  wire \time_out_counter_reg[0]_i_2__0_n_2 ;
  wire \time_out_counter_reg[0]_i_2__0_n_3 ;
  wire \time_out_counter_reg[0]_i_2__0_n_4 ;
  wire \time_out_counter_reg[0]_i_2__0_n_5 ;
  wire \time_out_counter_reg[0]_i_2__0_n_6 ;
  wire \time_out_counter_reg[0]_i_2__0_n_7 ;
  wire \time_out_counter_reg[12]_i_1__0_n_0 ;
  wire \time_out_counter_reg[12]_i_1__0_n_1 ;
  wire \time_out_counter_reg[12]_i_1__0_n_2 ;
  wire \time_out_counter_reg[12]_i_1__0_n_3 ;
  wire \time_out_counter_reg[12]_i_1__0_n_4 ;
  wire \time_out_counter_reg[12]_i_1__0_n_5 ;
  wire \time_out_counter_reg[12]_i_1__0_n_6 ;
  wire \time_out_counter_reg[12]_i_1__0_n_7 ;
  wire \time_out_counter_reg[16]_i_1__0_n_2 ;
  wire \time_out_counter_reg[16]_i_1__0_n_3 ;
  wire \time_out_counter_reg[16]_i_1__0_n_5 ;
  wire \time_out_counter_reg[16]_i_1__0_n_6 ;
  wire \time_out_counter_reg[16]_i_1__0_n_7 ;
  wire \time_out_counter_reg[4]_i_1__0_n_0 ;
  wire \time_out_counter_reg[4]_i_1__0_n_1 ;
  wire \time_out_counter_reg[4]_i_1__0_n_2 ;
  wire \time_out_counter_reg[4]_i_1__0_n_3 ;
  wire \time_out_counter_reg[4]_i_1__0_n_4 ;
  wire \time_out_counter_reg[4]_i_1__0_n_5 ;
  wire \time_out_counter_reg[4]_i_1__0_n_6 ;
  wire \time_out_counter_reg[4]_i_1__0_n_7 ;
  wire \time_out_counter_reg[8]_i_1__0_n_0 ;
  wire \time_out_counter_reg[8]_i_1__0_n_1 ;
  wire \time_out_counter_reg[8]_i_1__0_n_2 ;
  wire \time_out_counter_reg[8]_i_1__0_n_3 ;
  wire \time_out_counter_reg[8]_i_1__0_n_4 ;
  wire \time_out_counter_reg[8]_i_1__0_n_5 ;
  wire \time_out_counter_reg[8]_i_1__0_n_6 ;
  wire \time_out_counter_reg[8]_i_1__0_n_7 ;
  wire time_out_wait_bypass_i_1_n_0;
  wire time_out_wait_bypass_i_2__0_n_0;
  wire time_out_wait_bypass_i_3__0_n_0;
  wire time_out_wait_bypass_i_4__0_n_0;
  wire time_out_wait_bypass_reg_n_0;
  wire time_out_wait_bypass_s2;
  wire time_out_wait_bypass_s3;
  wire time_tlock_max;
  wire time_tlock_max1;
  wire time_tlock_max1_carry__0_i_1_n_0;
  wire time_tlock_max1_carry__0_i_2_n_0;
  wire time_tlock_max1_carry__0_i_3_n_0;
  wire time_tlock_max1_carry__0_i_4_n_0;
  wire time_tlock_max1_carry__0_i_5_n_0;
  wire time_tlock_max1_carry__0_i_6_n_0;
  wire time_tlock_max1_carry__0_n_0;
  wire time_tlock_max1_carry__0_n_1;
  wire time_tlock_max1_carry__0_n_2;
  wire time_tlock_max1_carry__0_n_3;
  wire time_tlock_max1_carry__1_i_1_n_0;
  wire time_tlock_max1_carry__1_i_2_n_0;
  wire time_tlock_max1_carry__1_i_3_n_0;
  wire time_tlock_max1_carry__1_n_3;
  wire time_tlock_max1_carry_i_1_n_0;
  wire time_tlock_max1_carry_i_2_n_0;
  wire time_tlock_max1_carry_i_3_n_0;
  wire time_tlock_max1_carry_i_4_n_0;
  wire time_tlock_max1_carry_i_5_n_0;
  wire time_tlock_max1_carry_i_6_n_0;
  wire time_tlock_max1_carry_i_7_n_0;
  wire time_tlock_max1_carry_i_8_n_0;
  wire time_tlock_max1_carry_n_0;
  wire time_tlock_max1_carry_n_1;
  wire time_tlock_max1_carry_n_2;
  wire time_tlock_max1_carry_n_3;
  wire time_tlock_max_i_1_n_0;
  wire \wait_bypass_count[0]_i_1__0_n_0 ;
  wire \wait_bypass_count[0]_i_2__0_n_0 ;
  wire \wait_bypass_count[0]_i_4_n_0 ;
  wire [12:0]wait_bypass_count_reg;
  wire \wait_bypass_count_reg[0]_i_3__0_n_0 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_1 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_2 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_3 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_4 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_5 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_6 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_7 ;
  wire \wait_bypass_count_reg[12]_i_1__0_n_7 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_0 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_1 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_2 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_3 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_4 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_5 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_6 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_7 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_0 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_1 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_2 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_3 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_4 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_5 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_6 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_7 ;
  wire [0:0]wait_time_cnt0__0;
  wire \wait_time_cnt[1]_i_1__0_n_0 ;
  wire \wait_time_cnt[2]_i_1__0_n_0 ;
  wire \wait_time_cnt[3]_i_1__0_n_0 ;
  wire \wait_time_cnt[4]_i_1__0_n_0 ;
  wire \wait_time_cnt[5]_i_1__0_n_0 ;
  wire \wait_time_cnt[6]_i_1_n_0 ;
  wire \wait_time_cnt[6]_i_2__0_n_0 ;
  wire \wait_time_cnt[6]_i_3__0_n_0 ;
  wire \wait_time_cnt[6]_i_4__0_n_0 ;
  wire [6:0]wait_time_cnt_reg;
  wire [3:2]\NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED ;
  wire [3:3]\NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED ;
  wire [3:0]NLW_time_tlock_max1_carry_O_UNCONNECTED;
  wire [3:0]NLW_time_tlock_max1_carry__0_O_UNCONNECTED;
  wire [3:2]NLW_time_tlock_max1_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_time_tlock_max1_carry__1_O_UNCONNECTED;
  wire [3:0]\NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED ;
  wire [3:1]\NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h2222AAAA00000C00)) 
    \FSM_sequential_rx_state[0]_i_2 
       (.I0(time_out_2ms_reg_n_0),
        .I1(rx_state[2]),
        .I2(rx_state[3]),
        .I3(time_tlock_max),
        .I4(reset_time_out_reg_n_0),
        .I5(rx_state[1]),
        .O(\FSM_sequential_rx_state[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000AABF000F0000)) 
    \FSM_sequential_rx_state[1]_i_3 
       (.I0(reset_time_out_reg_n_0),
        .I1(time_tlock_max),
        .I2(rx_state[2]),
        .I3(rx_state[3]),
        .I4(rx_state[1]),
        .I5(rx_state[0]),
        .O(\FSM_sequential_rx_state[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000050FF2200)) 
    \FSM_sequential_rx_state[2]_i_1 
       (.I0(rx_state[1]),
        .I1(time_out_2ms_reg_n_0),
        .I2(\FSM_sequential_rx_state[2]_i_2_n_0 ),
        .I3(rx_state[0]),
        .I4(rx_state[2]),
        .I5(rx_state[3]),
        .O(rx_state__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_rx_state[2]_i_2 
       (.I0(reset_time_out_reg_n_0),
        .I1(time_tlock_max),
        .O(\FSM_sequential_rx_state[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_rx_state[3]_i_10 
       (.I0(reset_time_out_reg_n_0),
        .I1(time_out_2ms_reg_n_0),
        .O(\FSM_sequential_rx_state[3]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0000000050005300)) 
    \FSM_sequential_rx_state[3]_i_3 
       (.I0(\FSM_sequential_rx_state[3]_i_10_n_0 ),
        .I1(\wait_time_cnt[6]_i_4__0_n_0 ),
        .I2(rx_state[0]),
        .I3(rx_state[1]),
        .I4(wait_time_cnt_reg[6]),
        .I5(rx_state[3]),
        .O(\FSM_sequential_rx_state[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000023002F00)) 
    \FSM_sequential_rx_state[3]_i_7 
       (.I0(time_out_2ms_reg_n_0),
        .I1(rx_state[2]),
        .I2(rx_state[1]),
        .I3(rx_state[0]),
        .I4(\FSM_sequential_rx_state[2]_i_2_n_0 ),
        .I5(rx_state[3]),
        .O(\FSM_sequential_rx_state[3]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT5 #(
    .INIT(32'h80800080)) 
    \FSM_sequential_rx_state[3]_i_9 
       (.I0(rx_state[0]),
        .I1(rx_state[1]),
        .I2(rx_state[2]),
        .I3(time_out_2ms_reg_n_0),
        .I4(reset_time_out_reg_n_0),
        .O(\FSM_sequential_rx_state[3]_i_9_n_0 ));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[0] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[0]),
        .Q(rx_state[0]),
        .R(out));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[1] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[1]),
        .Q(rx_state[1]),
        .R(out));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[2] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[2]),
        .Q(rx_state[2]),
        .R(out));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[3] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[3]),
        .Q(rx_state[3]),
        .R(out));
  LUT5 #(
    .INIT(32'hFFFB4000)) 
    RXUSERRDY_i_1
       (.I0(rx_state[3]),
        .I1(rx_state[0]),
        .I2(rx_state[2]),
        .I3(rx_state[1]),
        .I4(gt0_rxuserrdy_t),
        .O(RXUSERRDY_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    RXUSERRDY_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(RXUSERRDY_i_1_n_0),
        .Q(gt0_rxuserrdy_t),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT5 #(
    .INIT(32'hFFFB0008)) 
    check_tlock_max_i_1
       (.I0(rx_state[2]),
        .I1(rx_state[0]),
        .I2(rx_state[1]),
        .I3(rx_state[3]),
        .I4(check_tlock_max_reg_n_0),
        .O(check_tlock_max_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    check_tlock_max_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(check_tlock_max_i_1_n_0),
        .Q(check_tlock_max_reg_n_0),
        .R(out));
  LUT5 #(
    .INIT(32'hFFEF0100)) 
    gtrxreset_i_i_1
       (.I0(rx_state[3]),
        .I1(rx_state[1]),
        .I2(rx_state[2]),
        .I3(rx_state[0]),
        .I4(GTRXRESET),
        .O(gtrxreset_i_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gtrxreset_i_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gtrxreset_i_i_1_n_0),
        .Q(GTRXRESET),
        .R(out));
  LUT3 #(
    .INIT(8'hEA)) 
    gtxe2_i_i_2
       (.I0(GTRXRESET),
        .I1(data_in),
        .I2(gtxe2_i),
        .O(SR));
  LUT1 #(
    .INIT(2'h1)) 
    \init_wait_count[0]_i_1__0 
       (.I0(init_wait_count_reg[0]),
        .O(\init_wait_count[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \init_wait_count[1]_i_1__0 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .O(p_0_in__2[1]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \init_wait_count[2]_i_1__0 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .O(p_0_in__2[2]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \init_wait_count[3]_i_1__0 
       (.I0(init_wait_count_reg[1]),
        .I1(init_wait_count_reg[2]),
        .I2(init_wait_count_reg[0]),
        .I3(init_wait_count_reg[3]),
        .O(p_0_in__2[3]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \init_wait_count[4]_i_1__0 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .O(p_0_in__2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \init_wait_count[5]_i_1__0 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .I5(init_wait_count_reg[5]),
        .O(p_0_in__2[5]));
  LUT4 #(
    .INIT(16'hFEFF)) 
    \init_wait_count[6]_i_1__0 
       (.I0(\init_wait_count[6]_i_3__0_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(\init_wait_count[6]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT4 #(
    .INIT(16'hBF40)) 
    \init_wait_count[6]_i_2__0 
       (.I0(\init_wait_count[6]_i_3__0_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(p_0_in__2[6]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \init_wait_count[6]_i_3__0 
       (.I0(init_wait_count_reg[3]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .I3(init_wait_count_reg[5]),
        .O(\init_wait_count[6]_i_3__0_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(\init_wait_count[0]_i_1__0_n_0 ),
        .Q(init_wait_count_reg[0]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[1]),
        .Q(init_wait_count_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[2]),
        .Q(init_wait_count_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[3]),
        .Q(init_wait_count_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[4]),
        .Q(init_wait_count_reg[4]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[5]),
        .Q(init_wait_count_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[6]),
        .Q(init_wait_count_reg[6]));
  LUT5 #(
    .INIT(32'hFFFF0010)) 
    init_wait_done_i_1__0
       (.I0(\init_wait_count[6]_i_3__0_n_0 ),
        .I1(init_wait_count_reg[4]),
        .I2(init_wait_count_reg[6]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_done_reg_n_0),
        .O(init_wait_done_i_1__0_n_0));
  FDCE #(
    .INIT(1'b0)) 
    init_wait_done_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .CLR(out),
        .D(init_wait_done_i_1__0_n_0),
        .Q(init_wait_done_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[0]_i_1__0 
       (.I0(mmcm_lock_count_reg[0]),
        .O(p_0_in__3[0]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \mmcm_lock_count[1]_i_1__0 
       (.I0(mmcm_lock_count_reg[0]),
        .I1(mmcm_lock_count_reg[1]),
        .O(p_0_in__3[1]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \mmcm_lock_count[2]_i_1__0 
       (.I0(mmcm_lock_count_reg[1]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[2]),
        .O(\mmcm_lock_count[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \mmcm_lock_count[3]_i_1__0 
       (.I0(mmcm_lock_count_reg[2]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[1]),
        .I3(mmcm_lock_count_reg[3]),
        .O(\mmcm_lock_count[3]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \mmcm_lock_count[4]_i_1__0 
       (.I0(mmcm_lock_count_reg[3]),
        .I1(mmcm_lock_count_reg[1]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[2]),
        .I4(mmcm_lock_count_reg[4]),
        .O(\mmcm_lock_count[4]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \mmcm_lock_count[5]_i_1__0 
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(\mmcm_lock_count[5]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \mmcm_lock_count[6]_i_1__0 
       (.I0(mmcm_lock_reclocked_i_2__0_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .O(\mmcm_lock_count[6]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'hBF)) 
    \mmcm_lock_count[7]_i_2__0 
       (.I0(mmcm_lock_reclocked_i_2__0_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \mmcm_lock_count[7]_i_3__0 
       (.I0(mmcm_lock_count_reg[6]),
        .I1(mmcm_lock_reclocked_i_2__0_n_0),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_3__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(p_0_in__3[0]),
        .Q(mmcm_lock_count_reg[0]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(p_0_in__3[1]),
        .Q(mmcm_lock_count_reg[1]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[2]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[2]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[3]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[3]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[4]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[4]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[5]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[5]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[6]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[6]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[7] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[7]_i_3__0_n_0 ),
        .Q(mmcm_lock_count_reg[7]),
        .R(sync_mmcm_lock_reclocked_n_0));
  LUT5 #(
    .INIT(32'hAAEA0000)) 
    mmcm_lock_reclocked_i_1
       (.I0(mmcm_lock_reclocked),
        .I1(mmcm_lock_count_reg[7]),
        .I2(mmcm_lock_count_reg[6]),
        .I3(mmcm_lock_reclocked_i_2__0_n_0),
        .I4(mmcm_lock_i),
        .O(mmcm_lock_reclocked_i_1_n_0));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    mmcm_lock_reclocked_i_2__0
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(mmcm_lock_reclocked_i_2__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    mmcm_lock_reclocked_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(mmcm_lock_reclocked_i_1_n_0),
        .Q(mmcm_lock_reclocked),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT2 #(
    .INIT(4'hE)) 
    reset_time_out_i_3
       (.I0(rx_state[2]),
        .I1(rx_state[3]),
        .O(reset_time_out_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT5 #(
    .INIT(32'h34347674)) 
    reset_time_out_i_4
       (.I0(rx_state[2]),
        .I1(rx_state[3]),
        .I2(rx_state[0]),
        .I3(\FSM_sequential_rx_state_reg[0]_0 ),
        .I4(rx_state[1]),
        .O(reset_time_out_i_4_n_0));
  FDSE #(
    .INIT(1'b0)) 
    reset_time_out_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(sync_data_valid_n_0),
        .Q(reset_time_out_reg_n_0),
        .S(out));
  LUT5 #(
    .INIT(32'hFEFF0010)) 
    run_phase_alignment_int_i_1__0
       (.I0(rx_state[2]),
        .I1(rx_state[1]),
        .I2(rx_state[3]),
        .I3(rx_state[0]),
        .I4(run_phase_alignment_int_reg_n_0),
        .O(run_phase_alignment_int_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(run_phase_alignment_int_i_1__0_n_0),
        .Q(run_phase_alignment_int_reg_n_0),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_s3_reg
       (.C(data_sync_reg6),
        .CE(1'b1),
        .D(data_out_0),
        .Q(run_phase_alignment_int_s3_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT2 #(
    .INIT(4'hB)) 
    rx_fsm_reset_done_int_i_5
       (.I0(rx_state[1]),
        .I1(rx_state[0]),
        .O(rx_fsm_reset_done_int_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT2 #(
    .INIT(4'h2)) 
    rx_fsm_reset_done_int_i_6
       (.I0(rx_state[3]),
        .I1(rx_state[2]),
        .O(rx_fsm_reset_done_int_i_6_n_0));
  FDRE #(
    .INIT(1'b0)) 
    rx_fsm_reset_done_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(sync_data_valid_n_1),
        .Q(data_in),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    rx_fsm_reset_done_int_s3_reg
       (.C(data_sync_reg6),
        .CE(1'b1),
        .D(rx_fsm_reset_done_int_s2),
        .Q(rx_fsm_reset_done_int_s3),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxresetdone_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(rxresetdone_s2),
        .Q(rxresetdone_s3),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_10 sync_RXRESETDONE
       (.data_out(rxresetdone_s2),
        .data_sync_reg1_0(data_sync_reg1),
        .independent_clock_bufg(independent_clock_bufg));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_11 sync_cplllock
       (.\FSM_sequential_rx_state_reg[1] (sync_cplllock_n_0),
        .Q(rx_state[3:1]),
        .data_sync_reg1_0(data_sync_reg1_1),
        .independent_clock_bufg(independent_clock_bufg),
        .rxresetdone_s3(rxresetdone_s3));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_12 sync_data_valid
       (.D({rx_state__0[3],rx_state__0[1:0]}),
        .E(sync_data_valid_n_5),
        .\FSM_sequential_rx_state_reg[0] (\FSM_sequential_rx_state[3]_i_3_n_0 ),
        .\FSM_sequential_rx_state_reg[0]_0 (\FSM_sequential_rx_state[3]_i_7_n_0 ),
        .\FSM_sequential_rx_state_reg[0]_1 (\FSM_sequential_rx_state_reg[0]_0 ),
        .\FSM_sequential_rx_state_reg[0]_2 (\FSM_sequential_rx_state[0]_i_2_n_0 ),
        .\FSM_sequential_rx_state_reg[0]_3 (init_wait_done_reg_n_0),
        .\FSM_sequential_rx_state_reg[1] (sync_data_valid_n_0),
        .\FSM_sequential_rx_state_reg[1]_0 (\FSM_sequential_rx_state[1]_i_3_n_0 ),
        .\FSM_sequential_rx_state_reg[3] (\FSM_sequential_rx_state[3]_i_9_n_0 ),
        .Q(rx_state),
        .data_in(data_in),
        .data_out(data_out),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_lock_reclocked(mmcm_lock_reclocked),
        .reset_time_out_reg(sync_cplllock_n_0),
        .reset_time_out_reg_0(reset_time_out_i_3_n_0),
        .reset_time_out_reg_1(reset_time_out_i_4_n_0),
        .reset_time_out_reg_2(reset_time_out_reg_n_0),
        .rx_fsm_reset_done_int_reg(sync_data_valid_n_1),
        .rx_fsm_reset_done_int_reg_0(rx_fsm_reset_done_int_i_5_n_0),
        .rx_fsm_reset_done_int_reg_1(time_out_100us_reg_n_0),
        .rx_fsm_reset_done_int_reg_2(time_out_1us_reg_n_0),
        .rx_fsm_reset_done_int_reg_3(rx_fsm_reset_done_int_i_6_n_0),
        .time_out_wait_bypass_s3(time_out_wait_bypass_s3));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_13 sync_mmcm_lock_reclocked
       (.SR(sync_mmcm_lock_reclocked_n_0),
        .data_out(mmcm_lock_i),
        .data_sync_reg1_0(data_sync_reg1_0),
        .independent_clock_bufg(independent_clock_bufg));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_14 sync_run_phase_alignment_int
       (.data_in(run_phase_alignment_int_reg_n_0),
        .data_out(data_out_0),
        .data_sync_reg1_0(data_sync_reg6));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_15 sync_time_out_wait_bypass
       (.data_in(time_out_wait_bypass_reg_n_0),
        .data_out(time_out_wait_bypass_s2),
        .independent_clock_bufg(independent_clock_bufg));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_16 sync_tx_fsm_reset_done_int
       (.data_in(data_in),
        .data_out(rx_fsm_reset_done_int_s2),
        .data_sync_reg6_0(data_sync_reg6));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000100)) 
    time_out_100us_i_1
       (.I0(time_out_2ms_i_4_n_0),
        .I1(time_out_counter_reg[17]),
        .I2(time_out_counter_reg[16]),
        .I3(time_out_100us_i_2_n_0),
        .I4(time_out_100us_i_3_n_0),
        .I5(time_out_100us_reg_n_0),
        .O(time_out_100us_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000002000000000)) 
    time_out_100us_i_2
       (.I0(time_out_counter_reg[10]),
        .I1(time_out_counter_reg[12]),
        .I2(time_out_counter_reg[5]),
        .I3(time_out_counter_reg[7]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[14]),
        .O(time_out_100us_i_2_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    time_out_100us_i_3
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[0]),
        .I2(time_out_counter_reg[1]),
        .I3(time_out_counter_reg[15]),
        .I4(time_out_counter_reg[13]),
        .O(time_out_100us_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_100us_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_100us_i_1_n_0),
        .Q(time_out_100us_reg_n_0),
        .R(reset_time_out_reg_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF00100000)) 
    time_out_1us_i_1
       (.I0(time_out_2ms_i_2_n_0),
        .I1(time_out_1us_i_2_n_0),
        .I2(time_out_counter_reg[3]),
        .I3(time_out_counter_reg[2]),
        .I4(time_out_1us_i_3_n_0),
        .I5(time_out_1us_reg_n_0),
        .O(time_out_1us_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'hE)) 
    time_out_1us_i_2
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[17]),
        .O(time_out_1us_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    time_out_1us_i_3
       (.I0(time_out_counter_reg[9]),
        .I1(time_out_counter_reg[11]),
        .I2(time_out_counter_reg[6]),
        .I3(time_out_counter_reg[8]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[12]),
        .O(time_out_1us_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_1us_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_1us_i_1_n_0),
        .Q(time_out_1us_reg_n_0),
        .R(reset_time_out_reg_n_0));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT4 #(
    .INIT(16'hFF01)) 
    time_out_2ms_i_1
       (.I0(time_out_2ms_i_2_n_0),
        .I1(time_out_2ms_i_3__0_n_0),
        .I2(time_out_2ms_i_4_n_0),
        .I3(time_out_2ms_reg_n_0),
        .O(time_out_2ms_i_1_n_0));
  LUT5 #(
    .INIT(32'hFFFFFEFF)) 
    time_out_2ms_i_2
       (.I0(time_out_counter_reg[10]),
        .I1(time_out_counter_reg[14]),
        .I2(time_out_counter_reg[5]),
        .I3(time_out_counter_reg[7]),
        .I4(time_out_100us_i_3_n_0),
        .O(time_out_2ms_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT4 #(
    .INIT(16'hDFFF)) 
    time_out_2ms_i_3__0
       (.I0(time_out_counter_reg[12]),
        .I1(time_out_counter_reg[16]),
        .I2(time_out_counter_reg[18]),
        .I3(time_out_counter_reg[17]),
        .O(time_out_2ms_i_3__0_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEFFFFFF)) 
    time_out_2ms_i_4
       (.I0(time_out_counter_reg[2]),
        .I1(time_out_counter_reg[3]),
        .I2(time_out_counter_reg[8]),
        .I3(time_out_counter_reg[9]),
        .I4(time_out_counter_reg[11]),
        .I5(time_out_counter_reg[6]),
        .O(time_out_2ms_i_4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_2ms_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_2ms_i_1_n_0),
        .Q(time_out_2ms_reg_n_0),
        .R(reset_time_out_reg_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFF7FF)) 
    \time_out_counter[0]_i_1 
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[18]),
        .I2(time_out_counter_reg[16]),
        .I3(time_out_counter_reg[12]),
        .I4(time_out_2ms_i_2_n_0),
        .I5(time_out_2ms_i_4_n_0),
        .O(time_out_counter));
  LUT1 #(
    .INIT(2'h1)) 
    \time_out_counter[0]_i_3 
       (.I0(time_out_counter_reg[0]),
        .O(\time_out_counter[0]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[0] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_7 ),
        .Q(time_out_counter_reg[0]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[0]_i_2__0 
       (.CI(1'b0),
        .CO({\time_out_counter_reg[0]_i_2__0_n_0 ,\time_out_counter_reg[0]_i_2__0_n_1 ,\time_out_counter_reg[0]_i_2__0_n_2 ,\time_out_counter_reg[0]_i_2__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\time_out_counter_reg[0]_i_2__0_n_4 ,\time_out_counter_reg[0]_i_2__0_n_5 ,\time_out_counter_reg[0]_i_2__0_n_6 ,\time_out_counter_reg[0]_i_2__0_n_7 }),
        .S({time_out_counter_reg[3:1],\time_out_counter[0]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[10] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[10]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[11] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[11]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[12] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[12]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[12]_i_1__0 
       (.CI(\time_out_counter_reg[8]_i_1__0_n_0 ),
        .CO({\time_out_counter_reg[12]_i_1__0_n_0 ,\time_out_counter_reg[12]_i_1__0_n_1 ,\time_out_counter_reg[12]_i_1__0_n_2 ,\time_out_counter_reg[12]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[12]_i_1__0_n_4 ,\time_out_counter_reg[12]_i_1__0_n_5 ,\time_out_counter_reg[12]_i_1__0_n_6 ,\time_out_counter_reg[12]_i_1__0_n_7 }),
        .S(time_out_counter_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[13] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[13]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[14] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[14]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[15] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[15]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[16] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[16]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[16]_i_1__0 
       (.CI(\time_out_counter_reg[12]_i_1__0_n_0 ),
        .CO({\NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED [3:2],\time_out_counter_reg[16]_i_1__0_n_2 ,\time_out_counter_reg[16]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED [3],\time_out_counter_reg[16]_i_1__0_n_5 ,\time_out_counter_reg[16]_i_1__0_n_6 ,\time_out_counter_reg[16]_i_1__0_n_7 }),
        .S({1'b0,time_out_counter_reg[18:16]}));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[17] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[17]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[18] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[18]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[1] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_6 ),
        .Q(time_out_counter_reg[1]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[2] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_5 ),
        .Q(time_out_counter_reg[2]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[3] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_4 ),
        .Q(time_out_counter_reg[3]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[4] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[4]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[4]_i_1__0 
       (.CI(\time_out_counter_reg[0]_i_2__0_n_0 ),
        .CO({\time_out_counter_reg[4]_i_1__0_n_0 ,\time_out_counter_reg[4]_i_1__0_n_1 ,\time_out_counter_reg[4]_i_1__0_n_2 ,\time_out_counter_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[4]_i_1__0_n_4 ,\time_out_counter_reg[4]_i_1__0_n_5 ,\time_out_counter_reg[4]_i_1__0_n_6 ,\time_out_counter_reg[4]_i_1__0_n_7 }),
        .S(time_out_counter_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[5] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[5]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[6] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[6]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[7] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[7]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[8] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[8]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[8]_i_1__0 
       (.CI(\time_out_counter_reg[4]_i_1__0_n_0 ),
        .CO({\time_out_counter_reg[8]_i_1__0_n_0 ,\time_out_counter_reg[8]_i_1__0_n_1 ,\time_out_counter_reg[8]_i_1__0_n_2 ,\time_out_counter_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[8]_i_1__0_n_4 ,\time_out_counter_reg[8]_i_1__0_n_5 ,\time_out_counter_reg[8]_i_1__0_n_6 ,\time_out_counter_reg[8]_i_1__0_n_7 }),
        .S(time_out_counter_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[9] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[9]),
        .R(reset_time_out_reg_n_0));
  LUT4 #(
    .INIT(16'hAB00)) 
    time_out_wait_bypass_i_1
       (.I0(time_out_wait_bypass_reg_n_0),
        .I1(rx_fsm_reset_done_int_s3),
        .I2(time_out_wait_bypass_i_2__0_n_0),
        .I3(run_phase_alignment_int_s3_reg_n_0),
        .O(time_out_wait_bypass_i_1_n_0));
  LUT5 #(
    .INIT(32'hFBFFFFFF)) 
    time_out_wait_bypass_i_2__0
       (.I0(time_out_wait_bypass_i_3__0_n_0),
        .I1(wait_bypass_count_reg[1]),
        .I2(wait_bypass_count_reg[11]),
        .I3(wait_bypass_count_reg[0]),
        .I4(time_out_wait_bypass_i_4__0_n_0),
        .O(time_out_wait_bypass_i_2__0_n_0));
  LUT4 #(
    .INIT(16'hDFFF)) 
    time_out_wait_bypass_i_3__0
       (.I0(wait_bypass_count_reg[9]),
        .I1(wait_bypass_count_reg[4]),
        .I2(wait_bypass_count_reg[12]),
        .I3(wait_bypass_count_reg[2]),
        .O(time_out_wait_bypass_i_3__0_n_0));
  LUT6 #(
    .INIT(64'h0000000400000000)) 
    time_out_wait_bypass_i_4__0
       (.I0(wait_bypass_count_reg[5]),
        .I1(wait_bypass_count_reg[7]),
        .I2(wait_bypass_count_reg[3]),
        .I3(wait_bypass_count_reg[6]),
        .I4(wait_bypass_count_reg[10]),
        .I5(wait_bypass_count_reg[8]),
        .O(time_out_wait_bypass_i_4__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_reg
       (.C(data_sync_reg6),
        .CE(1'b1),
        .D(time_out_wait_bypass_i_1_n_0),
        .Q(time_out_wait_bypass_reg_n_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_wait_bypass_s2),
        .Q(time_out_wait_bypass_s3),
        .R(1'b0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 time_tlock_max1_carry
       (.CI(1'b0),
        .CO({time_tlock_max1_carry_n_0,time_tlock_max1_carry_n_1,time_tlock_max1_carry_n_2,time_tlock_max1_carry_n_3}),
        .CYINIT(1'b0),
        .DI({time_tlock_max1_carry_i_1_n_0,time_tlock_max1_carry_i_2_n_0,time_tlock_max1_carry_i_3_n_0,time_tlock_max1_carry_i_4_n_0}),
        .O(NLW_time_tlock_max1_carry_O_UNCONNECTED[3:0]),
        .S({time_tlock_max1_carry_i_5_n_0,time_tlock_max1_carry_i_6_n_0,time_tlock_max1_carry_i_7_n_0,time_tlock_max1_carry_i_8_n_0}));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 time_tlock_max1_carry__0
       (.CI(time_tlock_max1_carry_n_0),
        .CO({time_tlock_max1_carry__0_n_0,time_tlock_max1_carry__0_n_1,time_tlock_max1_carry__0_n_2,time_tlock_max1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({time_out_counter_reg[15],time_tlock_max1_carry__0_i_1_n_0,1'b0,time_tlock_max1_carry__0_i_2_n_0}),
        .O(NLW_time_tlock_max1_carry__0_O_UNCONNECTED[3:0]),
        .S({time_tlock_max1_carry__0_i_3_n_0,time_tlock_max1_carry__0_i_4_n_0,time_tlock_max1_carry__0_i_5_n_0,time_tlock_max1_carry__0_i_6_n_0}));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry__0_i_1
       (.I0(time_out_counter_reg[12]),
        .I1(time_out_counter_reg[13]),
        .O(time_tlock_max1_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    time_tlock_max1_carry__0_i_2
       (.I0(time_out_counter_reg[8]),
        .I1(time_out_counter_reg[9]),
        .O(time_tlock_max1_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry__0_i_3
       (.I0(time_out_counter_reg[14]),
        .I1(time_out_counter_reg[15]),
        .O(time_tlock_max1_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry__0_i_4
       (.I0(time_out_counter_reg[13]),
        .I1(time_out_counter_reg[12]),
        .O(time_tlock_max1_carry__0_i_4_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    time_tlock_max1_carry__0_i_5
       (.I0(time_out_counter_reg[10]),
        .I1(time_out_counter_reg[11]),
        .O(time_tlock_max1_carry__0_i_5_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry__0_i_6
       (.I0(time_out_counter_reg[9]),
        .I1(time_out_counter_reg[8]),
        .O(time_tlock_max1_carry__0_i_6_n_0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 time_tlock_max1_carry__1
       (.CI(time_tlock_max1_carry__0_n_0),
        .CO({NLW_time_tlock_max1_carry__1_CO_UNCONNECTED[3:2],time_tlock_max1,time_tlock_max1_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,time_out_counter_reg[18],time_tlock_max1_carry__1_i_1_n_0}),
        .O(NLW_time_tlock_max1_carry__1_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,time_tlock_max1_carry__1_i_2_n_0,time_tlock_max1_carry__1_i_3_n_0}));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry__1_i_1
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[17]),
        .O(time_tlock_max1_carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    time_tlock_max1_carry__1_i_2
       (.I0(time_out_counter_reg[18]),
        .O(time_tlock_max1_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry__1_i_3
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[16]),
        .O(time_tlock_max1_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_1
       (.I0(time_out_counter_reg[6]),
        .I1(time_out_counter_reg[7]),
        .O(time_tlock_max1_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    time_tlock_max1_carry_i_2
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[5]),
        .O(time_tlock_max1_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_3
       (.I0(time_out_counter_reg[2]),
        .I1(time_out_counter_reg[3]),
        .O(time_tlock_max1_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_4
       (.I0(time_out_counter_reg[0]),
        .I1(time_out_counter_reg[1]),
        .O(time_tlock_max1_carry_i_4_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_5
       (.I0(time_out_counter_reg[7]),
        .I1(time_out_counter_reg[6]),
        .O(time_tlock_max1_carry_i_5_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry_i_6
       (.I0(time_out_counter_reg[5]),
        .I1(time_out_counter_reg[4]),
        .O(time_tlock_max1_carry_i_6_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_7
       (.I0(time_out_counter_reg[3]),
        .I1(time_out_counter_reg[2]),
        .O(time_tlock_max1_carry_i_7_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_8
       (.I0(time_out_counter_reg[1]),
        .I1(time_out_counter_reg[0]),
        .O(time_tlock_max1_carry_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    time_tlock_max_i_1
       (.I0(check_tlock_max_reg_n_0),
        .I1(time_tlock_max1),
        .I2(time_tlock_max),
        .O(time_tlock_max_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_tlock_max_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_tlock_max_i_1_n_0),
        .Q(time_tlock_max),
        .R(reset_time_out_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_1__0 
       (.I0(run_phase_alignment_int_s3_reg_n_0),
        .O(\wait_bypass_count[0]_i_1__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \wait_bypass_count[0]_i_2__0 
       (.I0(time_out_wait_bypass_i_2__0_n_0),
        .I1(rx_fsm_reset_done_int_s3),
        .O(\wait_bypass_count[0]_i_2__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_4 
       (.I0(wait_bypass_count_reg[0]),
        .O(\wait_bypass_count[0]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[0] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_7 ),
        .Q(wait_bypass_count_reg[0]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[0]_i_3__0 
       (.CI(1'b0),
        .CO({\wait_bypass_count_reg[0]_i_3__0_n_0 ,\wait_bypass_count_reg[0]_i_3__0_n_1 ,\wait_bypass_count_reg[0]_i_3__0_n_2 ,\wait_bypass_count_reg[0]_i_3__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\wait_bypass_count_reg[0]_i_3__0_n_4 ,\wait_bypass_count_reg[0]_i_3__0_n_5 ,\wait_bypass_count_reg[0]_i_3__0_n_6 ,\wait_bypass_count_reg[0]_i_3__0_n_7 }),
        .S({wait_bypass_count_reg[3:1],\wait_bypass_count[0]_i_4_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[10] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_5 ),
        .Q(wait_bypass_count_reg[10]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[11] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_4 ),
        .Q(wait_bypass_count_reg[11]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[12] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[12]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[12]_i_1__0 
       (.CI(\wait_bypass_count_reg[8]_i_1__0_n_0 ),
        .CO(\NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED [3:1],\wait_bypass_count_reg[12]_i_1__0_n_7 }),
        .S({1'b0,1'b0,1'b0,wait_bypass_count_reg[12]}));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[1] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_6 ),
        .Q(wait_bypass_count_reg[1]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[2] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_5 ),
        .Q(wait_bypass_count_reg[2]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[3] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_4 ),
        .Q(wait_bypass_count_reg[3]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[4] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[4]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[4]_i_1__0 
       (.CI(\wait_bypass_count_reg[0]_i_3__0_n_0 ),
        .CO({\wait_bypass_count_reg[4]_i_1__0_n_0 ,\wait_bypass_count_reg[4]_i_1__0_n_1 ,\wait_bypass_count_reg[4]_i_1__0_n_2 ,\wait_bypass_count_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[4]_i_1__0_n_4 ,\wait_bypass_count_reg[4]_i_1__0_n_5 ,\wait_bypass_count_reg[4]_i_1__0_n_6 ,\wait_bypass_count_reg[4]_i_1__0_n_7 }),
        .S(wait_bypass_count_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[5] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_6 ),
        .Q(wait_bypass_count_reg[5]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[6] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_5 ),
        .Q(wait_bypass_count_reg[6]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[7] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_4 ),
        .Q(wait_bypass_count_reg[7]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[8] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[8]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[8]_i_1__0 
       (.CI(\wait_bypass_count_reg[4]_i_1__0_n_0 ),
        .CO({\wait_bypass_count_reg[8]_i_1__0_n_0 ,\wait_bypass_count_reg[8]_i_1__0_n_1 ,\wait_bypass_count_reg[8]_i_1__0_n_2 ,\wait_bypass_count_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[8]_i_1__0_n_4 ,\wait_bypass_count_reg[8]_i_1__0_n_5 ,\wait_bypass_count_reg[8]_i_1__0_n_6 ,\wait_bypass_count_reg[8]_i_1__0_n_7 }),
        .S(wait_bypass_count_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[9] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_6 ),
        .Q(wait_bypass_count_reg[9]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \wait_time_cnt[0]_i_1__0 
       (.I0(wait_time_cnt_reg[0]),
        .O(wait_time_cnt0__0));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[1]_i_1__0 
       (.I0(wait_time_cnt_reg[0]),
        .I1(wait_time_cnt_reg[1]),
        .O(\wait_time_cnt[1]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'hE1)) 
    \wait_time_cnt[2]_i_1__0 
       (.I0(wait_time_cnt_reg[1]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[2]),
        .O(\wait_time_cnt[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT4 #(
    .INIT(16'hFE01)) 
    \wait_time_cnt[3]_i_1__0 
       (.I0(wait_time_cnt_reg[2]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[1]),
        .I3(wait_time_cnt_reg[3]),
        .O(\wait_time_cnt[3]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT5 #(
    .INIT(32'hFFFE0001)) 
    \wait_time_cnt[4]_i_1__0 
       (.I0(wait_time_cnt_reg[3]),
        .I1(wait_time_cnt_reg[1]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[2]),
        .I4(wait_time_cnt_reg[4]),
        .O(\wait_time_cnt[4]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000001)) 
    \wait_time_cnt[5]_i_1__0 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[5]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \wait_time_cnt[6]_i_1 
       (.I0(rx_state[0]),
        .I1(rx_state[1]),
        .I2(rx_state[3]),
        .O(\wait_time_cnt[6]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \wait_time_cnt[6]_i_2__0 
       (.I0(\wait_time_cnt[6]_i_4__0_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(\wait_time_cnt[6]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[6]_i_3__0 
       (.I0(\wait_time_cnt[6]_i_4__0_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(\wait_time_cnt[6]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \wait_time_cnt[6]_i_4__0 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[6]_i_4__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(wait_time_cnt0__0),
        .Q(wait_time_cnt_reg[0]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[1]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[1]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[2]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[2]),
        .S(\wait_time_cnt[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[3]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[3]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[4]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[4]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[5]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[5]),
        .S(\wait_time_cnt[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[6]_i_3__0_n_0 ),
        .Q(wait_time_cnt_reg[6]),
        .S(\wait_time_cnt[6]_i_1_n_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_TX_STARTUP_FSM
   (mmcm_reset,
    gt0_cpllreset_t,
    data_in,
    gt0_txuserrdy_t,
    gt0_gttxreset_in0_out,
    independent_clock_bufg,
    data_sync_reg1,
    out,
    gtxe2_i,
    gt0_cpllrefclklost_i,
    data_sync_reg1_0,
    data_sync_reg1_1,
    data_sync_reg1_2);
  output mmcm_reset;
  output gt0_cpllreset_t;
  output data_in;
  output gt0_txuserrdy_t;
  output gt0_gttxreset_in0_out;
  input independent_clock_bufg;
  input data_sync_reg1;
  input [0:0]out;
  input gtxe2_i;
  input gt0_cpllrefclklost_i;
  input data_sync_reg1_0;
  input data_sync_reg1_1;
  input data_sync_reg1_2;

  wire CPLL_RESET_i_1_n_0;
  wire CPLL_RESET_i_2_n_0;
  wire \FSM_sequential_tx_state[0]_i_2_n_0 ;
  wire \FSM_sequential_tx_state[0]_i_3_n_0 ;
  wire \FSM_sequential_tx_state[2]_i_2_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_3_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_4_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_6_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_7_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_8_n_0 ;
  wire GTTXRESET;
  wire MMCM_RESET_i_1_n_0;
  wire TXUSERRDY_i_1_n_0;
  wire clear;
  wire data_in;
  wire data_out;
  wire data_sync_reg1;
  wire data_sync_reg1_0;
  wire data_sync_reg1_1;
  wire data_sync_reg1_2;
  wire gt0_cpllrefclklost_i;
  wire gt0_cpllreset_t;
  wire gt0_gttxreset_in0_out;
  wire gt0_txuserrdy_t;
  wire gttxreset_i_i_1_n_0;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire \init_wait_count[0]_i_1_n_0 ;
  wire \init_wait_count[6]_i_1_n_0 ;
  wire \init_wait_count[6]_i_3_n_0 ;
  wire [6:0]init_wait_count_reg;
  wire init_wait_done_i_1_n_0;
  wire init_wait_done_reg_n_0;
  wire \mmcm_lock_count[2]_i_1_n_0 ;
  wire \mmcm_lock_count[3]_i_1_n_0 ;
  wire \mmcm_lock_count[4]_i_1_n_0 ;
  wire \mmcm_lock_count[5]_i_1_n_0 ;
  wire \mmcm_lock_count[6]_i_1_n_0 ;
  wire \mmcm_lock_count[7]_i_2_n_0 ;
  wire \mmcm_lock_count[7]_i_3_n_0 ;
  wire [7:0]mmcm_lock_count_reg;
  wire mmcm_lock_i;
  wire mmcm_lock_reclocked;
  wire mmcm_lock_reclocked_i_1_n_0;
  wire mmcm_lock_reclocked_i_2_n_0;
  wire mmcm_reset;
  wire [0:0]out;
  wire [6:1]p_0_in__0;
  wire [1:0]p_0_in__1;
  wire pll_reset_asserted_i_1_n_0;
  wire pll_reset_asserted_i_2_n_0;
  wire pll_reset_asserted_reg_n_0;
  wire refclk_stable_count;
  wire \refclk_stable_count[0]_i_3_n_0 ;
  wire \refclk_stable_count[0]_i_4_n_0 ;
  wire \refclk_stable_count[0]_i_5_n_0 ;
  wire \refclk_stable_count[0]_i_6_n_0 ;
  wire \refclk_stable_count[0]_i_7_n_0 ;
  wire \refclk_stable_count[0]_i_8_n_0 ;
  wire \refclk_stable_count[0]_i_9_n_0 ;
  wire [31:0]refclk_stable_count_reg;
  wire \refclk_stable_count_reg[0]_i_2_n_0 ;
  wire \refclk_stable_count_reg[0]_i_2_n_1 ;
  wire \refclk_stable_count_reg[0]_i_2_n_2 ;
  wire \refclk_stable_count_reg[0]_i_2_n_3 ;
  wire \refclk_stable_count_reg[0]_i_2_n_4 ;
  wire \refclk_stable_count_reg[0]_i_2_n_5 ;
  wire \refclk_stable_count_reg[0]_i_2_n_6 ;
  wire \refclk_stable_count_reg[0]_i_2_n_7 ;
  wire \refclk_stable_count_reg[12]_i_1_n_0 ;
  wire \refclk_stable_count_reg[12]_i_1_n_1 ;
  wire \refclk_stable_count_reg[12]_i_1_n_2 ;
  wire \refclk_stable_count_reg[12]_i_1_n_3 ;
  wire \refclk_stable_count_reg[12]_i_1_n_4 ;
  wire \refclk_stable_count_reg[12]_i_1_n_5 ;
  wire \refclk_stable_count_reg[12]_i_1_n_6 ;
  wire \refclk_stable_count_reg[12]_i_1_n_7 ;
  wire \refclk_stable_count_reg[16]_i_1_n_0 ;
  wire \refclk_stable_count_reg[16]_i_1_n_1 ;
  wire \refclk_stable_count_reg[16]_i_1_n_2 ;
  wire \refclk_stable_count_reg[16]_i_1_n_3 ;
  wire \refclk_stable_count_reg[16]_i_1_n_4 ;
  wire \refclk_stable_count_reg[16]_i_1_n_5 ;
  wire \refclk_stable_count_reg[16]_i_1_n_6 ;
  wire \refclk_stable_count_reg[16]_i_1_n_7 ;
  wire \refclk_stable_count_reg[20]_i_1_n_0 ;
  wire \refclk_stable_count_reg[20]_i_1_n_1 ;
  wire \refclk_stable_count_reg[20]_i_1_n_2 ;
  wire \refclk_stable_count_reg[20]_i_1_n_3 ;
  wire \refclk_stable_count_reg[20]_i_1_n_4 ;
  wire \refclk_stable_count_reg[20]_i_1_n_5 ;
  wire \refclk_stable_count_reg[20]_i_1_n_6 ;
  wire \refclk_stable_count_reg[20]_i_1_n_7 ;
  wire \refclk_stable_count_reg[24]_i_1_n_0 ;
  wire \refclk_stable_count_reg[24]_i_1_n_1 ;
  wire \refclk_stable_count_reg[24]_i_1_n_2 ;
  wire \refclk_stable_count_reg[24]_i_1_n_3 ;
  wire \refclk_stable_count_reg[24]_i_1_n_4 ;
  wire \refclk_stable_count_reg[24]_i_1_n_5 ;
  wire \refclk_stable_count_reg[24]_i_1_n_6 ;
  wire \refclk_stable_count_reg[24]_i_1_n_7 ;
  wire \refclk_stable_count_reg[28]_i_1_n_1 ;
  wire \refclk_stable_count_reg[28]_i_1_n_2 ;
  wire \refclk_stable_count_reg[28]_i_1_n_3 ;
  wire \refclk_stable_count_reg[28]_i_1_n_4 ;
  wire \refclk_stable_count_reg[28]_i_1_n_5 ;
  wire \refclk_stable_count_reg[28]_i_1_n_6 ;
  wire \refclk_stable_count_reg[28]_i_1_n_7 ;
  wire \refclk_stable_count_reg[4]_i_1_n_0 ;
  wire \refclk_stable_count_reg[4]_i_1_n_1 ;
  wire \refclk_stable_count_reg[4]_i_1_n_2 ;
  wire \refclk_stable_count_reg[4]_i_1_n_3 ;
  wire \refclk_stable_count_reg[4]_i_1_n_4 ;
  wire \refclk_stable_count_reg[4]_i_1_n_5 ;
  wire \refclk_stable_count_reg[4]_i_1_n_6 ;
  wire \refclk_stable_count_reg[4]_i_1_n_7 ;
  wire \refclk_stable_count_reg[8]_i_1_n_0 ;
  wire \refclk_stable_count_reg[8]_i_1_n_1 ;
  wire \refclk_stable_count_reg[8]_i_1_n_2 ;
  wire \refclk_stable_count_reg[8]_i_1_n_3 ;
  wire \refclk_stable_count_reg[8]_i_1_n_4 ;
  wire \refclk_stable_count_reg[8]_i_1_n_5 ;
  wire \refclk_stable_count_reg[8]_i_1_n_6 ;
  wire \refclk_stable_count_reg[8]_i_1_n_7 ;
  wire refclk_stable_i_1_n_0;
  wire refclk_stable_i_2_n_0;
  wire refclk_stable_i_3_n_0;
  wire refclk_stable_i_4_n_0;
  wire refclk_stable_i_5_n_0;
  wire refclk_stable_i_6_n_0;
  wire refclk_stable_reg_n_0;
  wire reset_time_out;
  wire reset_time_out_i_2__0_n_0;
  wire run_phase_alignment_int_i_1_n_0;
  wire run_phase_alignment_int_reg_n_0;
  wire run_phase_alignment_int_s3;
  wire sel;
  wire sync_cplllock_n_0;
  wire sync_cplllock_n_1;
  wire sync_mmcm_lock_reclocked_n_0;
  wire time_out_2ms_i_1_n_0;
  wire time_out_2ms_i_2__0_n_0;
  wire time_out_2ms_i_3_n_0;
  wire time_out_2ms_i_4__0_n_0;
  wire time_out_2ms_i_5_n_0;
  wire time_out_2ms_reg_n_0;
  wire time_out_500us_i_1_n_0;
  wire time_out_500us_i_2_n_0;
  wire time_out_500us_reg_n_0;
  wire time_out_counter;
  wire \time_out_counter[0]_i_3__0_n_0 ;
  wire \time_out_counter[0]_i_4_n_0 ;
  wire [18:0]time_out_counter_reg;
  wire \time_out_counter_reg[0]_i_2_n_0 ;
  wire \time_out_counter_reg[0]_i_2_n_1 ;
  wire \time_out_counter_reg[0]_i_2_n_2 ;
  wire \time_out_counter_reg[0]_i_2_n_3 ;
  wire \time_out_counter_reg[0]_i_2_n_4 ;
  wire \time_out_counter_reg[0]_i_2_n_5 ;
  wire \time_out_counter_reg[0]_i_2_n_6 ;
  wire \time_out_counter_reg[0]_i_2_n_7 ;
  wire \time_out_counter_reg[12]_i_1_n_0 ;
  wire \time_out_counter_reg[12]_i_1_n_1 ;
  wire \time_out_counter_reg[12]_i_1_n_2 ;
  wire \time_out_counter_reg[12]_i_1_n_3 ;
  wire \time_out_counter_reg[12]_i_1_n_4 ;
  wire \time_out_counter_reg[12]_i_1_n_5 ;
  wire \time_out_counter_reg[12]_i_1_n_6 ;
  wire \time_out_counter_reg[12]_i_1_n_7 ;
  wire \time_out_counter_reg[16]_i_1_n_2 ;
  wire \time_out_counter_reg[16]_i_1_n_3 ;
  wire \time_out_counter_reg[16]_i_1_n_5 ;
  wire \time_out_counter_reg[16]_i_1_n_6 ;
  wire \time_out_counter_reg[16]_i_1_n_7 ;
  wire \time_out_counter_reg[4]_i_1_n_0 ;
  wire \time_out_counter_reg[4]_i_1_n_1 ;
  wire \time_out_counter_reg[4]_i_1_n_2 ;
  wire \time_out_counter_reg[4]_i_1_n_3 ;
  wire \time_out_counter_reg[4]_i_1_n_4 ;
  wire \time_out_counter_reg[4]_i_1_n_5 ;
  wire \time_out_counter_reg[4]_i_1_n_6 ;
  wire \time_out_counter_reg[4]_i_1_n_7 ;
  wire \time_out_counter_reg[8]_i_1_n_0 ;
  wire \time_out_counter_reg[8]_i_1_n_1 ;
  wire \time_out_counter_reg[8]_i_1_n_2 ;
  wire \time_out_counter_reg[8]_i_1_n_3 ;
  wire \time_out_counter_reg[8]_i_1_n_4 ;
  wire \time_out_counter_reg[8]_i_1_n_5 ;
  wire \time_out_counter_reg[8]_i_1_n_6 ;
  wire \time_out_counter_reg[8]_i_1_n_7 ;
  wire time_out_wait_bypass_i_1_n_0;
  wire time_out_wait_bypass_i_2_n_0;
  wire time_out_wait_bypass_i_3_n_0;
  wire time_out_wait_bypass_i_4_n_0;
  wire time_out_wait_bypass_i_5_n_0;
  wire time_out_wait_bypass_reg_n_0;
  wire time_out_wait_bypass_s2;
  wire time_out_wait_bypass_s3;
  wire time_tlock_max_i_1_n_0;
  wire time_tlock_max_i_2_n_0;
  wire time_tlock_max_i_3_n_0;
  wire time_tlock_max_i_4_n_0;
  wire time_tlock_max_reg_n_0;
  wire tx_fsm_reset_done_int_i_1_n_0;
  wire tx_fsm_reset_done_int_s2;
  wire tx_fsm_reset_done_int_s3;
  wire [3:0]tx_state;
  wire [3:0]tx_state__0;
  wire txresetdone_s2;
  wire txresetdone_s3;
  wire \wait_bypass_count[0]_i_2_n_0 ;
  wire \wait_bypass_count[0]_i_4__0_n_0 ;
  wire [16:0]wait_bypass_count_reg;
  wire \wait_bypass_count_reg[0]_i_3_n_0 ;
  wire \wait_bypass_count_reg[0]_i_3_n_1 ;
  wire \wait_bypass_count_reg[0]_i_3_n_2 ;
  wire \wait_bypass_count_reg[0]_i_3_n_3 ;
  wire \wait_bypass_count_reg[0]_i_3_n_4 ;
  wire \wait_bypass_count_reg[0]_i_3_n_5 ;
  wire \wait_bypass_count_reg[0]_i_3_n_6 ;
  wire \wait_bypass_count_reg[0]_i_3_n_7 ;
  wire \wait_bypass_count_reg[12]_i_1_n_0 ;
  wire \wait_bypass_count_reg[12]_i_1_n_1 ;
  wire \wait_bypass_count_reg[12]_i_1_n_2 ;
  wire \wait_bypass_count_reg[12]_i_1_n_3 ;
  wire \wait_bypass_count_reg[12]_i_1_n_4 ;
  wire \wait_bypass_count_reg[12]_i_1_n_5 ;
  wire \wait_bypass_count_reg[12]_i_1_n_6 ;
  wire \wait_bypass_count_reg[12]_i_1_n_7 ;
  wire \wait_bypass_count_reg[16]_i_1_n_7 ;
  wire \wait_bypass_count_reg[4]_i_1_n_0 ;
  wire \wait_bypass_count_reg[4]_i_1_n_1 ;
  wire \wait_bypass_count_reg[4]_i_1_n_2 ;
  wire \wait_bypass_count_reg[4]_i_1_n_3 ;
  wire \wait_bypass_count_reg[4]_i_1_n_4 ;
  wire \wait_bypass_count_reg[4]_i_1_n_5 ;
  wire \wait_bypass_count_reg[4]_i_1_n_6 ;
  wire \wait_bypass_count_reg[4]_i_1_n_7 ;
  wire \wait_bypass_count_reg[8]_i_1_n_0 ;
  wire \wait_bypass_count_reg[8]_i_1_n_1 ;
  wire \wait_bypass_count_reg[8]_i_1_n_2 ;
  wire \wait_bypass_count_reg[8]_i_1_n_3 ;
  wire \wait_bypass_count_reg[8]_i_1_n_4 ;
  wire \wait_bypass_count_reg[8]_i_1_n_5 ;
  wire \wait_bypass_count_reg[8]_i_1_n_6 ;
  wire \wait_bypass_count_reg[8]_i_1_n_7 ;
  wire [0:0]wait_time_cnt0;
  wire wait_time_cnt0_0;
  wire \wait_time_cnt[1]_i_1_n_0 ;
  wire \wait_time_cnt[2]_i_1_n_0 ;
  wire \wait_time_cnt[3]_i_1_n_0 ;
  wire \wait_time_cnt[4]_i_1_n_0 ;
  wire \wait_time_cnt[5]_i_1_n_0 ;
  wire \wait_time_cnt[6]_i_3_n_0 ;
  wire \wait_time_cnt[6]_i_4_n_0 ;
  wire [6:0]wait_time_cnt_reg;
  wire [3:3]\NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED ;
  wire [3:2]\NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED ;
  wire [3:0]\NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:1]\NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'hFFFFFF1F0000001F)) 
    CPLL_RESET_i_1
       (.I0(pll_reset_asserted_reg_n_0),
        .I1(gt0_cpllrefclklost_i),
        .I2(refclk_stable_reg_n_0),
        .I3(CPLL_RESET_i_2_n_0),
        .I4(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I5(gt0_cpllreset_t),
        .O(CPLL_RESET_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT2 #(
    .INIT(4'hE)) 
    CPLL_RESET_i_2
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .O(CPLL_RESET_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    CPLL_RESET_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(CPLL_RESET_i_1_n_0),
        .Q(gt0_cpllreset_t),
        .R(out));
  LUT6 #(
    .INIT(64'hF3FFF3F0F5F0F5F0)) 
    \FSM_sequential_tx_state[0]_i_1 
       (.I0(\FSM_sequential_tx_state[2]_i_2_n_0 ),
        .I1(\FSM_sequential_tx_state[0]_i_2_n_0 ),
        .I2(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I3(tx_state[2]),
        .I4(time_out_2ms_reg_n_0),
        .I5(tx_state[1]),
        .O(tx_state__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_tx_state[0]_i_2 
       (.I0(reset_time_out),
        .I1(time_out_500us_reg_n_0),
        .O(\FSM_sequential_tx_state[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_tx_state[0]_i_3 
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .O(\FSM_sequential_tx_state[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT5 #(
    .INIT(32'h005A001A)) 
    \FSM_sequential_tx_state[1]_i_1 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[0]),
        .I3(tx_state[3]),
        .I4(\FSM_sequential_tx_state[2]_i_2_n_0 ),
        .O(tx_state__0[1]));
  LUT6 #(
    .INIT(64'h04000C0C06020C0C)) 
    \FSM_sequential_tx_state[2]_i_1 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[3]),
        .I3(\FSM_sequential_tx_state[2]_i_2_n_0 ),
        .I4(tx_state[0]),
        .I5(time_out_2ms_reg_n_0),
        .O(tx_state__0[2]));
  LUT3 #(
    .INIT(8'hFD)) 
    \FSM_sequential_tx_state[2]_i_2 
       (.I0(time_tlock_max_reg_n_0),
        .I1(reset_time_out),
        .I2(mmcm_lock_reclocked),
        .O(\FSM_sequential_tx_state[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT5 #(
    .INIT(32'hF4FF4444)) 
    \FSM_sequential_tx_state[3]_i_2 
       (.I0(time_out_wait_bypass_s3),
        .I1(tx_state[3]),
        .I2(reset_time_out),
        .I3(time_out_500us_reg_n_0),
        .I4(\FSM_sequential_tx_state[3]_i_8_n_0 ),
        .O(tx_state__0[3]));
  LUT6 #(
    .INIT(64'h00BA000000000000)) 
    \FSM_sequential_tx_state[3]_i_3 
       (.I0(txresetdone_s3),
        .I1(reset_time_out),
        .I2(time_out_500us_reg_n_0),
        .I3(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I4(tx_state[2]),
        .I5(tx_state[1]),
        .O(\FSM_sequential_tx_state[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000300FF00AA)) 
    \FSM_sequential_tx_state[3]_i_4 
       (.I0(init_wait_done_reg_n_0),
        .I1(\wait_time_cnt[6]_i_4_n_0 ),
        .I2(wait_time_cnt_reg[6]),
        .I3(tx_state[0]),
        .I4(tx_state[3]),
        .I5(CPLL_RESET_i_2_n_0),
        .O(\FSM_sequential_tx_state[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0404040400040000)) 
    \FSM_sequential_tx_state[3]_i_6 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I3(reset_time_out),
        .I4(time_tlock_max_reg_n_0),
        .I5(mmcm_lock_reclocked),
        .O(\FSM_sequential_tx_state[3]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT4 #(
    .INIT(16'h1000)) 
    \FSM_sequential_tx_state[3]_i_7 
       (.I0(tx_state[2]),
        .I1(tx_state[3]),
        .I2(tx_state[0]),
        .I3(tx_state[1]),
        .O(\FSM_sequential_tx_state[3]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    \FSM_sequential_tx_state[3]_i_8 
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .O(\FSM_sequential_tx_state[3]_i_8_n_0 ));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[0] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[0]),
        .Q(tx_state[0]),
        .R(out));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[1] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[1]),
        .Q(tx_state[1]),
        .R(out));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[2] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[2]),
        .Q(tx_state[2]),
        .R(out));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[3] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[3]),
        .Q(tx_state[3]),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT5 #(
    .INIT(32'hFFF70004)) 
    MMCM_RESET_i_1
       (.I0(tx_state[2]),
        .I1(tx_state[0]),
        .I2(tx_state[3]),
        .I3(tx_state[1]),
        .I4(mmcm_reset),
        .O(MMCM_RESET_i_1_n_0));
  FDRE #(
    .INIT(1'b1)) 
    MMCM_RESET_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(MMCM_RESET_i_1_n_0),
        .Q(mmcm_reset),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT5 #(
    .INIT(32'hFFFD2000)) 
    TXUSERRDY_i_1
       (.I0(tx_state[0]),
        .I1(tx_state[3]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .I4(gt0_txuserrdy_t),
        .O(TXUSERRDY_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    TXUSERRDY_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(TXUSERRDY_i_1_n_0),
        .Q(gt0_txuserrdy_t),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT5 #(
    .INIT(32'hFFEF0100)) 
    gttxreset_i_i_1
       (.I0(tx_state[3]),
        .I1(tx_state[1]),
        .I2(tx_state[2]),
        .I3(tx_state[0]),
        .I4(GTTXRESET),
        .O(gttxreset_i_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gttxreset_i_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gttxreset_i_i_1_n_0),
        .Q(GTTXRESET),
        .R(out));
  LUT3 #(
    .INIT(8'hEA)) 
    gtxe2_i_i_3
       (.I0(GTTXRESET),
        .I1(data_in),
        .I2(gtxe2_i),
        .O(gt0_gttxreset_in0_out));
  LUT1 #(
    .INIT(2'h1)) 
    \init_wait_count[0]_i_1 
       (.I0(init_wait_count_reg[0]),
        .O(\init_wait_count[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \init_wait_count[1]_i_1 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \init_wait_count[2]_i_1 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .O(p_0_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \init_wait_count[3]_i_1 
       (.I0(init_wait_count_reg[1]),
        .I1(init_wait_count_reg[2]),
        .I2(init_wait_count_reg[0]),
        .I3(init_wait_count_reg[3]),
        .O(p_0_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \init_wait_count[4]_i_1 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .O(p_0_in__0[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \init_wait_count[5]_i_1 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .I5(init_wait_count_reg[5]),
        .O(p_0_in__0[5]));
  LUT4 #(
    .INIT(16'hFEFF)) 
    \init_wait_count[6]_i_1 
       (.I0(\init_wait_count[6]_i_3_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(\init_wait_count[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT4 #(
    .INIT(16'hBF40)) 
    \init_wait_count[6]_i_2 
       (.I0(\init_wait_count[6]_i_3_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(p_0_in__0[6]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \init_wait_count[6]_i_3 
       (.I0(init_wait_count_reg[3]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .I3(init_wait_count_reg[5]),
        .O(\init_wait_count[6]_i_3_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(\init_wait_count[0]_i_1_n_0 ),
        .Q(init_wait_count_reg[0]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[1]),
        .Q(init_wait_count_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[2]),
        .Q(init_wait_count_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[3]),
        .Q(init_wait_count_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[4]),
        .Q(init_wait_count_reg[4]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[5]),
        .Q(init_wait_count_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[6]),
        .Q(init_wait_count_reg[6]));
  LUT5 #(
    .INIT(32'hFFFF0010)) 
    init_wait_done_i_1
       (.I0(\init_wait_count[6]_i_3_n_0 ),
        .I1(init_wait_count_reg[4]),
        .I2(init_wait_count_reg[6]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_done_reg_n_0),
        .O(init_wait_done_i_1_n_0));
  FDCE #(
    .INIT(1'b0)) 
    init_wait_done_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .CLR(out),
        .D(init_wait_done_i_1_n_0),
        .Q(init_wait_done_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[0]_i_1 
       (.I0(mmcm_lock_count_reg[0]),
        .O(p_0_in__1[0]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \mmcm_lock_count[1]_i_1 
       (.I0(mmcm_lock_count_reg[0]),
        .I1(mmcm_lock_count_reg[1]),
        .O(p_0_in__1[1]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \mmcm_lock_count[2]_i_1 
       (.I0(mmcm_lock_count_reg[1]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[2]),
        .O(\mmcm_lock_count[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \mmcm_lock_count[3]_i_1 
       (.I0(mmcm_lock_count_reg[2]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[1]),
        .I3(mmcm_lock_count_reg[3]),
        .O(\mmcm_lock_count[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \mmcm_lock_count[4]_i_1 
       (.I0(mmcm_lock_count_reg[3]),
        .I1(mmcm_lock_count_reg[1]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[2]),
        .I4(mmcm_lock_count_reg[4]),
        .O(\mmcm_lock_count[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \mmcm_lock_count[5]_i_1 
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(\mmcm_lock_count[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \mmcm_lock_count[6]_i_1 
       (.I0(mmcm_lock_reclocked_i_2_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .O(\mmcm_lock_count[6]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hBF)) 
    \mmcm_lock_count[7]_i_2 
       (.I0(mmcm_lock_reclocked_i_2_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \mmcm_lock_count[7]_i_3 
       (.I0(mmcm_lock_count_reg[6]),
        .I1(mmcm_lock_reclocked_i_2_n_0),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(p_0_in__1[0]),
        .Q(mmcm_lock_count_reg[0]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(p_0_in__1[1]),
        .Q(mmcm_lock_count_reg[1]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[2]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[2]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[3]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[3]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[4]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[4]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[5]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[5]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[6]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[6]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[7] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[7]_i_3_n_0 ),
        .Q(mmcm_lock_count_reg[7]),
        .R(sync_mmcm_lock_reclocked_n_0));
  LUT5 #(
    .INIT(32'hAAEA0000)) 
    mmcm_lock_reclocked_i_1
       (.I0(mmcm_lock_reclocked),
        .I1(mmcm_lock_count_reg[7]),
        .I2(mmcm_lock_count_reg[6]),
        .I3(mmcm_lock_reclocked_i_2_n_0),
        .I4(mmcm_lock_i),
        .O(mmcm_lock_reclocked_i_1_n_0));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    mmcm_lock_reclocked_i_2
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(mmcm_lock_reclocked_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    mmcm_lock_reclocked_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(mmcm_lock_reclocked_i_1_n_0),
        .Q(mmcm_lock_reclocked),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000CD55CCCCCCCC)) 
    pll_reset_asserted_i_1
       (.I0(tx_state[3]),
        .I1(pll_reset_asserted_reg_n_0),
        .I2(gt0_cpllrefclklost_i),
        .I3(refclk_stable_reg_n_0),
        .I4(tx_state[1]),
        .I5(pll_reset_asserted_i_2_n_0),
        .O(pll_reset_asserted_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'h02)) 
    pll_reset_asserted_i_2
       (.I0(tx_state[0]),
        .I1(tx_state[3]),
        .I2(tx_state[2]),
        .O(pll_reset_asserted_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    pll_reset_asserted_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pll_reset_asserted_i_1_n_0),
        .Q(pll_reset_asserted_reg_n_0),
        .R(out));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFFFF)) 
    \refclk_stable_count[0]_i_1 
       (.I0(\refclk_stable_count[0]_i_3_n_0 ),
        .I1(\refclk_stable_count[0]_i_4_n_0 ),
        .I2(\refclk_stable_count[0]_i_5_n_0 ),
        .I3(\refclk_stable_count[0]_i_6_n_0 ),
        .I4(\refclk_stable_count[0]_i_7_n_0 ),
        .I5(\refclk_stable_count[0]_i_8_n_0 ),
        .O(refclk_stable_count));
  LUT6 #(
    .INIT(64'hFFDFFFFFFFFFFFFF)) 
    \refclk_stable_count[0]_i_3 
       (.I0(refclk_stable_count_reg[13]),
        .I1(refclk_stable_count_reg[12]),
        .I2(refclk_stable_count_reg[10]),
        .I3(refclk_stable_count_reg[11]),
        .I4(refclk_stable_count_reg[9]),
        .I5(refclk_stable_count_reg[8]),
        .O(\refclk_stable_count[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFDF)) 
    \refclk_stable_count[0]_i_4 
       (.I0(refclk_stable_count_reg[19]),
        .I1(refclk_stable_count_reg[18]),
        .I2(refclk_stable_count_reg[16]),
        .I3(refclk_stable_count_reg[17]),
        .I4(refclk_stable_count_reg[15]),
        .I5(refclk_stable_count_reg[14]),
        .O(\refclk_stable_count[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \refclk_stable_count[0]_i_5 
       (.I0(refclk_stable_count_reg[30]),
        .I1(refclk_stable_count_reg[31]),
        .I2(refclk_stable_count_reg[28]),
        .I3(refclk_stable_count_reg[29]),
        .I4(refclk_stable_count_reg[27]),
        .I5(refclk_stable_count_reg[26]),
        .O(\refclk_stable_count[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \refclk_stable_count[0]_i_6 
       (.I0(refclk_stable_count_reg[24]),
        .I1(refclk_stable_count_reg[25]),
        .I2(refclk_stable_count_reg[22]),
        .I3(refclk_stable_count_reg[23]),
        .I4(refclk_stable_count_reg[21]),
        .I5(refclk_stable_count_reg[20]),
        .O(\refclk_stable_count[0]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \refclk_stable_count[0]_i_7 
       (.I0(refclk_stable_count_reg[0]),
        .I1(refclk_stable_count_reg[1]),
        .O(\refclk_stable_count[0]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF7)) 
    \refclk_stable_count[0]_i_8 
       (.I0(refclk_stable_count_reg[6]),
        .I1(refclk_stable_count_reg[7]),
        .I2(refclk_stable_count_reg[4]),
        .I3(refclk_stable_count_reg[5]),
        .I4(refclk_stable_count_reg[3]),
        .I5(refclk_stable_count_reg[2]),
        .O(\refclk_stable_count[0]_i_8_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \refclk_stable_count[0]_i_9 
       (.I0(refclk_stable_count_reg[0]),
        .O(\refclk_stable_count[0]_i_9_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_7 ),
        .Q(refclk_stable_count_reg[0]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\refclk_stable_count_reg[0]_i_2_n_0 ,\refclk_stable_count_reg[0]_i_2_n_1 ,\refclk_stable_count_reg[0]_i_2_n_2 ,\refclk_stable_count_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\refclk_stable_count_reg[0]_i_2_n_4 ,\refclk_stable_count_reg[0]_i_2_n_5 ,\refclk_stable_count_reg[0]_i_2_n_6 ,\refclk_stable_count_reg[0]_i_2_n_7 }),
        .S({refclk_stable_count_reg[3:1],\refclk_stable_count[0]_i_9_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[10] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[11] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[12] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[12]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[12]_i_1 
       (.CI(\refclk_stable_count_reg[8]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[12]_i_1_n_0 ,\refclk_stable_count_reg[12]_i_1_n_1 ,\refclk_stable_count_reg[12]_i_1_n_2 ,\refclk_stable_count_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[12]_i_1_n_4 ,\refclk_stable_count_reg[12]_i_1_n_5 ,\refclk_stable_count_reg[12]_i_1_n_6 ,\refclk_stable_count_reg[12]_i_1_n_7 }),
        .S(refclk_stable_count_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[13] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[14] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[15] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[16] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[16]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[16]_i_1 
       (.CI(\refclk_stable_count_reg[12]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[16]_i_1_n_0 ,\refclk_stable_count_reg[16]_i_1_n_1 ,\refclk_stable_count_reg[16]_i_1_n_2 ,\refclk_stable_count_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[16]_i_1_n_4 ,\refclk_stable_count_reg[16]_i_1_n_5 ,\refclk_stable_count_reg[16]_i_1_n_6 ,\refclk_stable_count_reg[16]_i_1_n_7 }),
        .S(refclk_stable_count_reg[19:16]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[17] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[18] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[19] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_6 ),
        .Q(refclk_stable_count_reg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[20] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[20]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[20]_i_1 
       (.CI(\refclk_stable_count_reg[16]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[20]_i_1_n_0 ,\refclk_stable_count_reg[20]_i_1_n_1 ,\refclk_stable_count_reg[20]_i_1_n_2 ,\refclk_stable_count_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[20]_i_1_n_4 ,\refclk_stable_count_reg[20]_i_1_n_5 ,\refclk_stable_count_reg[20]_i_1_n_6 ,\refclk_stable_count_reg[20]_i_1_n_7 }),
        .S(refclk_stable_count_reg[23:20]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[21] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[22] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[23] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[24] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[24]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[24]_i_1 
       (.CI(\refclk_stable_count_reg[20]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[24]_i_1_n_0 ,\refclk_stable_count_reg[24]_i_1_n_1 ,\refclk_stable_count_reg[24]_i_1_n_2 ,\refclk_stable_count_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[24]_i_1_n_4 ,\refclk_stable_count_reg[24]_i_1_n_5 ,\refclk_stable_count_reg[24]_i_1_n_6 ,\refclk_stable_count_reg[24]_i_1_n_7 }),
        .S(refclk_stable_count_reg[27:24]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[25] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[26] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[27] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[28] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[28]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[28]_i_1 
       (.CI(\refclk_stable_count_reg[24]_i_1_n_0 ),
        .CO({\NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED [3],\refclk_stable_count_reg[28]_i_1_n_1 ,\refclk_stable_count_reg[28]_i_1_n_2 ,\refclk_stable_count_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[28]_i_1_n_4 ,\refclk_stable_count_reg[28]_i_1_n_5 ,\refclk_stable_count_reg[28]_i_1_n_6 ,\refclk_stable_count_reg[28]_i_1_n_7 }),
        .S(refclk_stable_count_reg[31:28]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[29] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_5 ),
        .Q(refclk_stable_count_reg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[30] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[31] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_4 ),
        .Q(refclk_stable_count_reg[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[4]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[4]_i_1 
       (.CI(\refclk_stable_count_reg[0]_i_2_n_0 ),
        .CO({\refclk_stable_count_reg[4]_i_1_n_0 ,\refclk_stable_count_reg[4]_i_1_n_1 ,\refclk_stable_count_reg[4]_i_1_n_2 ,\refclk_stable_count_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[4]_i_1_n_4 ,\refclk_stable_count_reg[4]_i_1_n_5 ,\refclk_stable_count_reg[4]_i_1_n_6 ,\refclk_stable_count_reg[4]_i_1_n_7 }),
        .S(refclk_stable_count_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[7] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[8] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[8]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[8]_i_1 
       (.CI(\refclk_stable_count_reg[4]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[8]_i_1_n_0 ,\refclk_stable_count_reg[8]_i_1_n_1 ,\refclk_stable_count_reg[8]_i_1_n_2 ,\refclk_stable_count_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[8]_i_1_n_4 ,\refclk_stable_count_reg[8]_i_1_n_5 ,\refclk_stable_count_reg[8]_i_1_n_6 ,\refclk_stable_count_reg[8]_i_1_n_7 }),
        .S(refclk_stable_count_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[9] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    refclk_stable_i_1
       (.I0(\refclk_stable_count[0]_i_7_n_0 ),
        .I1(refclk_stable_i_2_n_0),
        .I2(refclk_stable_i_3_n_0),
        .I3(refclk_stable_i_4_n_0),
        .I4(refclk_stable_i_5_n_0),
        .I5(refclk_stable_i_6_n_0),
        .O(refclk_stable_i_1_n_0));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    refclk_stable_i_2
       (.I0(refclk_stable_count_reg[4]),
        .I1(refclk_stable_count_reg[5]),
        .I2(refclk_stable_count_reg[2]),
        .I3(refclk_stable_count_reg[3]),
        .I4(refclk_stable_count_reg[7]),
        .I5(refclk_stable_count_reg[6]),
        .O(refclk_stable_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000200000000000)) 
    refclk_stable_i_3
       (.I0(refclk_stable_count_reg[10]),
        .I1(refclk_stable_count_reg[11]),
        .I2(refclk_stable_count_reg[8]),
        .I3(refclk_stable_count_reg[9]),
        .I4(refclk_stable_count_reg[12]),
        .I5(refclk_stable_count_reg[13]),
        .O(refclk_stable_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    refclk_stable_i_4
       (.I0(refclk_stable_count_reg[16]),
        .I1(refclk_stable_count_reg[17]),
        .I2(refclk_stable_count_reg[14]),
        .I3(refclk_stable_count_reg[15]),
        .I4(refclk_stable_count_reg[18]),
        .I5(refclk_stable_count_reg[19]),
        .O(refclk_stable_i_4_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    refclk_stable_i_5
       (.I0(refclk_stable_count_reg[22]),
        .I1(refclk_stable_count_reg[23]),
        .I2(refclk_stable_count_reg[20]),
        .I3(refclk_stable_count_reg[21]),
        .I4(refclk_stable_count_reg[25]),
        .I5(refclk_stable_count_reg[24]),
        .O(refclk_stable_i_5_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    refclk_stable_i_6
       (.I0(refclk_stable_count_reg[28]),
        .I1(refclk_stable_count_reg[29]),
        .I2(refclk_stable_count_reg[26]),
        .I3(refclk_stable_count_reg[27]),
        .I4(refclk_stable_count_reg[31]),
        .I5(refclk_stable_count_reg[30]),
        .O(refclk_stable_i_6_n_0));
  FDRE #(
    .INIT(1'b0)) 
    refclk_stable_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(refclk_stable_i_1_n_0),
        .Q(refclk_stable_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h440000FF50505050)) 
    reset_time_out_i_2__0
       (.I0(tx_state[3]),
        .I1(txresetdone_s3),
        .I2(init_wait_done_reg_n_0),
        .I3(tx_state[1]),
        .I4(tx_state[2]),
        .I5(tx_state[0]),
        .O(reset_time_out_i_2__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    reset_time_out_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(sync_cplllock_n_0),
        .Q(reset_time_out),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT5 #(
    .INIT(32'hFFFB0002)) 
    run_phase_alignment_int_i_1
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .I4(run_phase_alignment_int_reg_n_0),
        .O(run_phase_alignment_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(run_phase_alignment_int_i_1_n_0),
        .Q(run_phase_alignment_int_reg_n_0),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_s3_reg
       (.C(data_sync_reg1),
        .CE(1'b1),
        .D(data_out),
        .Q(run_phase_alignment_int_s3),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_4 sync_TXRESETDONE
       (.data_out(txresetdone_s2),
        .data_sync_reg1_0(data_sync_reg1_0),
        .independent_clock_bufg(independent_clock_bufg));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_5 sync_cplllock
       (.E(sync_cplllock_n_1),
        .\FSM_sequential_tx_state_reg[0] (\FSM_sequential_tx_state[3]_i_3_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_0 (\FSM_sequential_tx_state[3]_i_4_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_1 (\FSM_sequential_tx_state[3]_i_6_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_2 (time_out_2ms_reg_n_0),
        .\FSM_sequential_tx_state_reg[0]_3 (\FSM_sequential_tx_state[3]_i_7_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_4 (pll_reset_asserted_reg_n_0),
        .\FSM_sequential_tx_state_reg[0]_5 (refclk_stable_reg_n_0),
        .\FSM_sequential_tx_state_reg[0]_6 (\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .Q(tx_state),
        .data_sync_reg1_0(data_sync_reg1_2),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_lock_reclocked(mmcm_lock_reclocked),
        .reset_time_out(reset_time_out),
        .reset_time_out_reg(sync_cplllock_n_0),
        .reset_time_out_reg_0(reset_time_out_i_2__0_n_0),
        .reset_time_out_reg_1(init_wait_done_reg_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_6 sync_mmcm_lock_reclocked
       (.SR(sync_mmcm_lock_reclocked_n_0),
        .data_out(mmcm_lock_i),
        .data_sync_reg1_0(data_sync_reg1_1),
        .independent_clock_bufg(independent_clock_bufg));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_7 sync_run_phase_alignment_int
       (.data_in(run_phase_alignment_int_reg_n_0),
        .data_out(data_out),
        .data_sync_reg6_0(data_sync_reg1));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_8 sync_time_out_wait_bypass
       (.data_in(time_out_wait_bypass_reg_n_0),
        .data_out(time_out_wait_bypass_s2),
        .independent_clock_bufg(independent_clock_bufg));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_9 sync_tx_fsm_reset_done_int
       (.data_in(data_in),
        .data_out(tx_fsm_reset_done_int_s2),
        .data_sync_reg1_0(data_sync_reg1));
  LUT4 #(
    .INIT(16'h00AE)) 
    time_out_2ms_i_1
       (.I0(time_out_2ms_reg_n_0),
        .I1(time_out_2ms_i_2__0_n_0),
        .I2(time_out_2ms_i_3_n_0),
        .I3(reset_time_out),
        .O(time_out_2ms_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000800)) 
    time_out_2ms_i_2__0
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[18]),
        .I2(time_out_counter_reg[10]),
        .I3(time_out_counter_reg[12]),
        .I4(time_out_counter_reg[5]),
        .I5(time_tlock_max_i_3_n_0),
        .O(time_out_2ms_i_2__0_n_0));
  LUT4 #(
    .INIT(16'hFFFD)) 
    time_out_2ms_i_3
       (.I0(time_out_counter_reg[7]),
        .I1(time_out_counter_reg[14]),
        .I2(time_out_2ms_i_4__0_n_0),
        .I3(time_out_2ms_i_5_n_0),
        .O(time_out_2ms_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    time_out_2ms_i_4__0
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[3]),
        .I2(time_out_counter_reg[8]),
        .I3(time_out_counter_reg[6]),
        .O(time_out_2ms_i_4__0_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFEF)) 
    time_out_2ms_i_5
       (.I0(time_out_counter_reg[0]),
        .I1(time_out_counter_reg[13]),
        .I2(time_out_counter_reg[9]),
        .I3(time_out_counter_reg[2]),
        .I4(time_out_counter_reg[1]),
        .O(time_out_2ms_i_5_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_2ms_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_2ms_i_1_n_0),
        .Q(time_out_2ms_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000AAAAEAAA)) 
    time_out_500us_i_1
       (.I0(time_out_500us_reg_n_0),
        .I1(time_out_500us_i_2_n_0),
        .I2(time_out_counter_reg[5]),
        .I3(time_out_counter_reg[10]),
        .I4(time_out_2ms_i_3_n_0),
        .I5(reset_time_out),
        .O(time_out_500us_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    time_out_500us_i_2
       (.I0(time_out_counter_reg[15]),
        .I1(time_out_counter_reg[16]),
        .I2(time_out_counter_reg[11]),
        .I3(time_out_counter_reg[12]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[17]),
        .O(time_out_500us_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_500us_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_500us_i_1_n_0),
        .Q(time_out_500us_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFBFFFF)) 
    \time_out_counter[0]_i_1__0 
       (.I0(time_tlock_max_i_3_n_0),
        .I1(\time_out_counter[0]_i_3__0_n_0 ),
        .I2(time_out_2ms_i_3_n_0),
        .I3(time_out_counter_reg[10]),
        .I4(time_out_counter_reg[12]),
        .I5(time_out_counter_reg[5]),
        .O(time_out_counter));
  LUT2 #(
    .INIT(4'h8)) 
    \time_out_counter[0]_i_3__0 
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[18]),
        .O(\time_out_counter[0]_i_3__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \time_out_counter[0]_i_4 
       (.I0(time_out_counter_reg[0]),
        .O(\time_out_counter[0]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[0] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_7 ),
        .Q(time_out_counter_reg[0]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\time_out_counter_reg[0]_i_2_n_0 ,\time_out_counter_reg[0]_i_2_n_1 ,\time_out_counter_reg[0]_i_2_n_2 ,\time_out_counter_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\time_out_counter_reg[0]_i_2_n_4 ,\time_out_counter_reg[0]_i_2_n_5 ,\time_out_counter_reg[0]_i_2_n_6 ,\time_out_counter_reg[0]_i_2_n_7 }),
        .S({time_out_counter_reg[3:1],\time_out_counter[0]_i_4_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[10] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_5 ),
        .Q(time_out_counter_reg[10]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[11] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_4 ),
        .Q(time_out_counter_reg[11]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[12] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_7 ),
        .Q(time_out_counter_reg[12]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[12]_i_1 
       (.CI(\time_out_counter_reg[8]_i_1_n_0 ),
        .CO({\time_out_counter_reg[12]_i_1_n_0 ,\time_out_counter_reg[12]_i_1_n_1 ,\time_out_counter_reg[12]_i_1_n_2 ,\time_out_counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[12]_i_1_n_4 ,\time_out_counter_reg[12]_i_1_n_5 ,\time_out_counter_reg[12]_i_1_n_6 ,\time_out_counter_reg[12]_i_1_n_7 }),
        .S(time_out_counter_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[13] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_6 ),
        .Q(time_out_counter_reg[13]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[14] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_5 ),
        .Q(time_out_counter_reg[14]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[15] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_4 ),
        .Q(time_out_counter_reg[15]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[16] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1_n_7 ),
        .Q(time_out_counter_reg[16]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[16]_i_1 
       (.CI(\time_out_counter_reg[12]_i_1_n_0 ),
        .CO({\NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED [3:2],\time_out_counter_reg[16]_i_1_n_2 ,\time_out_counter_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED [3],\time_out_counter_reg[16]_i_1_n_5 ,\time_out_counter_reg[16]_i_1_n_6 ,\time_out_counter_reg[16]_i_1_n_7 }),
        .S({1'b0,time_out_counter_reg[18:16]}));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[17] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1_n_6 ),
        .Q(time_out_counter_reg[17]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[18] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1_n_5 ),
        .Q(time_out_counter_reg[18]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[1] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_6 ),
        .Q(time_out_counter_reg[1]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[2] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_5 ),
        .Q(time_out_counter_reg[2]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[3] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_4 ),
        .Q(time_out_counter_reg[3]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[4] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_7 ),
        .Q(time_out_counter_reg[4]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[4]_i_1 
       (.CI(\time_out_counter_reg[0]_i_2_n_0 ),
        .CO({\time_out_counter_reg[4]_i_1_n_0 ,\time_out_counter_reg[4]_i_1_n_1 ,\time_out_counter_reg[4]_i_1_n_2 ,\time_out_counter_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[4]_i_1_n_4 ,\time_out_counter_reg[4]_i_1_n_5 ,\time_out_counter_reg[4]_i_1_n_6 ,\time_out_counter_reg[4]_i_1_n_7 }),
        .S(time_out_counter_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[5] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_6 ),
        .Q(time_out_counter_reg[5]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[6] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_5 ),
        .Q(time_out_counter_reg[6]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[7] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_4 ),
        .Q(time_out_counter_reg[7]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[8] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_7 ),
        .Q(time_out_counter_reg[8]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[8]_i_1 
       (.CI(\time_out_counter_reg[4]_i_1_n_0 ),
        .CO({\time_out_counter_reg[8]_i_1_n_0 ,\time_out_counter_reg[8]_i_1_n_1 ,\time_out_counter_reg[8]_i_1_n_2 ,\time_out_counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[8]_i_1_n_4 ,\time_out_counter_reg[8]_i_1_n_5 ,\time_out_counter_reg[8]_i_1_n_6 ,\time_out_counter_reg[8]_i_1_n_7 }),
        .S(time_out_counter_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[9] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_6 ),
        .Q(time_out_counter_reg[9]),
        .R(reset_time_out));
  LUT4 #(
    .INIT(16'hAB00)) 
    time_out_wait_bypass_i_1
       (.I0(time_out_wait_bypass_reg_n_0),
        .I1(tx_fsm_reset_done_int_s3),
        .I2(time_out_wait_bypass_i_2_n_0),
        .I3(run_phase_alignment_int_s3),
        .O(time_out_wait_bypass_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFEFFFFFFFFF)) 
    time_out_wait_bypass_i_2
       (.I0(time_out_wait_bypass_i_3_n_0),
        .I1(time_out_wait_bypass_i_4_n_0),
        .I2(wait_bypass_count_reg[5]),
        .I3(wait_bypass_count_reg[13]),
        .I4(wait_bypass_count_reg[11]),
        .I5(time_out_wait_bypass_i_5_n_0),
        .O(time_out_wait_bypass_i_2_n_0));
  LUT4 #(
    .INIT(16'hFF7F)) 
    time_out_wait_bypass_i_3
       (.I0(wait_bypass_count_reg[16]),
        .I1(wait_bypass_count_reg[9]),
        .I2(wait_bypass_count_reg[12]),
        .I3(wait_bypass_count_reg[10]),
        .O(time_out_wait_bypass_i_3_n_0));
  LUT4 #(
    .INIT(16'hDFFF)) 
    time_out_wait_bypass_i_4
       (.I0(wait_bypass_count_reg[4]),
        .I1(wait_bypass_count_reg[15]),
        .I2(wait_bypass_count_reg[6]),
        .I3(wait_bypass_count_reg[0]),
        .O(time_out_wait_bypass_i_4_n_0));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    time_out_wait_bypass_i_5
       (.I0(wait_bypass_count_reg[8]),
        .I1(wait_bypass_count_reg[1]),
        .I2(wait_bypass_count_reg[7]),
        .I3(wait_bypass_count_reg[14]),
        .I4(wait_bypass_count_reg[2]),
        .I5(wait_bypass_count_reg[3]),
        .O(time_out_wait_bypass_i_5_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_reg
       (.C(data_sync_reg1),
        .CE(1'b1),
        .D(time_out_wait_bypass_i_1_n_0),
        .Q(time_out_wait_bypass_reg_n_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_wait_bypass_s2),
        .Q(time_out_wait_bypass_s3),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000AAAAAAEA)) 
    time_tlock_max_i_1
       (.I0(time_tlock_max_reg_n_0),
        .I1(time_tlock_max_i_2_n_0),
        .I2(time_out_counter_reg[5]),
        .I3(time_tlock_max_i_3_n_0),
        .I4(time_tlock_max_i_4_n_0),
        .I5(reset_time_out),
        .O(time_tlock_max_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    time_tlock_max_i_2
       (.I0(time_out_counter_reg[14]),
        .I1(time_out_counter_reg[12]),
        .I2(time_out_counter_reg[10]),
        .I3(time_out_counter_reg[7]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[17]),
        .O(time_tlock_max_i_2_n_0));
  LUT3 #(
    .INIT(8'hEF)) 
    time_tlock_max_i_3
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[15]),
        .I2(time_out_counter_reg[11]),
        .O(time_tlock_max_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    time_tlock_max_i_4
       (.I0(time_out_2ms_i_5_n_0),
        .I1(time_out_counter_reg[6]),
        .I2(time_out_counter_reg[8]),
        .I3(time_out_counter_reg[3]),
        .I4(time_out_counter_reg[4]),
        .O(time_tlock_max_i_4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_tlock_max_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_tlock_max_i_1_n_0),
        .Q(time_tlock_max_reg_n_0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFFF1000)) 
    tx_fsm_reset_done_int_i_1
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[0]),
        .I3(tx_state[3]),
        .I4(data_in),
        .O(tx_fsm_reset_done_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    tx_fsm_reset_done_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(tx_fsm_reset_done_int_i_1_n_0),
        .Q(data_in),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    tx_fsm_reset_done_int_s3_reg
       (.C(data_sync_reg1),
        .CE(1'b1),
        .D(tx_fsm_reset_done_int_s2),
        .Q(tx_fsm_reset_done_int_s3),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    txresetdone_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(txresetdone_s2),
        .Q(txresetdone_s3),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_1 
       (.I0(run_phase_alignment_int_s3),
        .O(clear));
  LUT2 #(
    .INIT(4'h2)) 
    \wait_bypass_count[0]_i_2 
       (.I0(time_out_wait_bypass_i_2_n_0),
        .I1(tx_fsm_reset_done_int_s3),
        .O(\wait_bypass_count[0]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_4__0 
       (.I0(wait_bypass_count_reg[0]),
        .O(\wait_bypass_count[0]_i_4__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[0] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_7 ),
        .Q(wait_bypass_count_reg[0]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\wait_bypass_count_reg[0]_i_3_n_0 ,\wait_bypass_count_reg[0]_i_3_n_1 ,\wait_bypass_count_reg[0]_i_3_n_2 ,\wait_bypass_count_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\wait_bypass_count_reg[0]_i_3_n_4 ,\wait_bypass_count_reg[0]_i_3_n_5 ,\wait_bypass_count_reg[0]_i_3_n_6 ,\wait_bypass_count_reg[0]_i_3_n_7 }),
        .S({wait_bypass_count_reg[3:1],\wait_bypass_count[0]_i_4__0_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[10] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[10]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[11] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[11]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[12] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[12]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[12]_i_1 
       (.CI(\wait_bypass_count_reg[8]_i_1_n_0 ),
        .CO({\wait_bypass_count_reg[12]_i_1_n_0 ,\wait_bypass_count_reg[12]_i_1_n_1 ,\wait_bypass_count_reg[12]_i_1_n_2 ,\wait_bypass_count_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[12]_i_1_n_4 ,\wait_bypass_count_reg[12]_i_1_n_5 ,\wait_bypass_count_reg[12]_i_1_n_6 ,\wait_bypass_count_reg[12]_i_1_n_7 }),
        .S(wait_bypass_count_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[13] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[13]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[14] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[14]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[15] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[15]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[16] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[16]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[16]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[16]_i_1 
       (.CI(\wait_bypass_count_reg[12]_i_1_n_0 ),
        .CO(\NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED [3:1],\wait_bypass_count_reg[16]_i_1_n_7 }),
        .S({1'b0,1'b0,1'b0,wait_bypass_count_reg[16]}));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[1] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_6 ),
        .Q(wait_bypass_count_reg[1]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[2] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_5 ),
        .Q(wait_bypass_count_reg[2]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[3] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_4 ),
        .Q(wait_bypass_count_reg[3]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[4] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[4]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[4]_i_1 
       (.CI(\wait_bypass_count_reg[0]_i_3_n_0 ),
        .CO({\wait_bypass_count_reg[4]_i_1_n_0 ,\wait_bypass_count_reg[4]_i_1_n_1 ,\wait_bypass_count_reg[4]_i_1_n_2 ,\wait_bypass_count_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[4]_i_1_n_4 ,\wait_bypass_count_reg[4]_i_1_n_5 ,\wait_bypass_count_reg[4]_i_1_n_6 ,\wait_bypass_count_reg[4]_i_1_n_7 }),
        .S(wait_bypass_count_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[5] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[5]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[6] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[6]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[7] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[7]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[8] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[8]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[8]_i_1 
       (.CI(\wait_bypass_count_reg[4]_i_1_n_0 ),
        .CO({\wait_bypass_count_reg[8]_i_1_n_0 ,\wait_bypass_count_reg[8]_i_1_n_1 ,\wait_bypass_count_reg[8]_i_1_n_2 ,\wait_bypass_count_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[8]_i_1_n_4 ,\wait_bypass_count_reg[8]_i_1_n_5 ,\wait_bypass_count_reg[8]_i_1_n_6 ,\wait_bypass_count_reg[8]_i_1_n_7 }),
        .S(wait_bypass_count_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[9] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[9]),
        .R(clear));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \wait_time_cnt[0]_i_1 
       (.I0(wait_time_cnt_reg[0]),
        .O(wait_time_cnt0));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[1]_i_1 
       (.I0(wait_time_cnt_reg[0]),
        .I1(wait_time_cnt_reg[1]),
        .O(\wait_time_cnt[1]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hE1)) 
    \wait_time_cnt[2]_i_1 
       (.I0(wait_time_cnt_reg[1]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[2]),
        .O(\wait_time_cnt[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT4 #(
    .INIT(16'hFE01)) 
    \wait_time_cnt[3]_i_1 
       (.I0(wait_time_cnt_reg[2]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[1]),
        .I3(wait_time_cnt_reg[3]),
        .O(\wait_time_cnt[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT5 #(
    .INIT(32'hFFFE0001)) 
    \wait_time_cnt[4]_i_1 
       (.I0(wait_time_cnt_reg[3]),
        .I1(wait_time_cnt_reg[1]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[2]),
        .I4(wait_time_cnt_reg[4]),
        .O(\wait_time_cnt[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000001)) 
    \wait_time_cnt[5]_i_1 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0700)) 
    \wait_time_cnt[6]_i_1__0 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[3]),
        .I3(tx_state[0]),
        .O(wait_time_cnt0_0));
  LUT2 #(
    .INIT(4'hE)) 
    \wait_time_cnt[6]_i_2 
       (.I0(\wait_time_cnt[6]_i_4_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(sel));
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[6]_i_3 
       (.I0(\wait_time_cnt[6]_i_4_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(\wait_time_cnt[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \wait_time_cnt[6]_i_4 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[6]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[0] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(wait_time_cnt0),
        .Q(wait_time_cnt_reg[0]),
        .R(wait_time_cnt0_0));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[1] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[1]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[1]),
        .R(wait_time_cnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[2] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[2]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[2]),
        .S(wait_time_cnt0_0));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[3] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[3]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[3]),
        .R(wait_time_cnt0_0));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[4] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[4]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[4]),
        .R(wait_time_cnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[5] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[5]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[5]),
        .S(wait_time_cnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[6] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[6]_i_3_n_0 ),
        .Q(wait_time_cnt_reg[6]),
        .S(wait_time_cnt0_0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_block
   (gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    status_vector,
    resetdone,
    txn,
    txp,
    rxoutclk,
    txoutclk,
    mmcm_reset,
    out,
    signal_detect,
    CLK,
    data_in,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    configuration_vector,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    gtxe2_i);
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  output [6:0]status_vector;
  output resetdone;
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output mmcm_reset;
  input [0:0]out;
  input signal_detect;
  input CLK;
  input data_in;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  input [2:0]configuration_vector;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input gtxe2_i;

  wire CLK;
  wire [2:0]configuration_vector;
  wire data_in;
  wire enablealign;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire mgt_rx_reset;
  wire mgt_tx_reset;
  wire mmcm_reset;
  wire [0:0]out;
  wire powerdown;
  wire resetdone;
  wire rx_reset_done_i;
  wire rxbuferr;
  wire rxchariscomma;
  wire rxcharisk;
  wire [1:0]rxclkcorcnt;
  wire [7:0]rxdata;
  wire rxdisperr;
  wire rxn;
  wire rxnotintable;
  wire rxoutclk;
  wire rxp;
  wire signal_detect;
  wire [6:0]status_vector;
  wire transceiver_inst_n_11;
  wire transceiver_inst_n_12;
  wire txbuferr;
  wire txchardispmode;
  wire txchardispval;
  wire txcharisk;
  wire [7:0]txdata;
  wire txn;
  wire txoutclk;
  wire txp;
  wire NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED;
  wire [9:0]NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED;
  wire [15:0]NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED;
  wire [63:0]NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED;
  wire [31:0]NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED;
  wire [47:0]NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED;
  wire [1:0]NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED;
  wire [31:0]NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED;
  wire [1:0]NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED;
  wire [1:0]NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED;
  wire [15:7]NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED;
  wire [9:0]NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED;

  (* B_SHIFTER_ADDR = "10'b0101001110" *) 
  (* C_1588 = "0" *) 
  (* C_2_5G = "FALSE" *) 
  (* C_COMPONENT_NAME = "gig_ethernet_pcs_pma_0" *) 
  (* C_DYNAMIC_SWITCHING = "FALSE" *) 
  (* C_ELABORATION_TRANSIENT_DIR = "BlankString" *) 
  (* C_FAMILY = "kintex7" *) 
  (* C_HAS_AN = "FALSE" *) 
  (* C_HAS_AXIL = "FALSE" *) 
  (* C_HAS_MDIO = "FALSE" *) 
  (* C_HAS_TEMAC = "TRUE" *) 
  (* C_IS_SGMII = "FALSE" *) 
  (* C_RX_GMII_CLK = "TXOUTCLK" *) 
  (* C_SGMII_FABRIC_BUFFER = "TRUE" *) 
  (* C_SGMII_PHY_MODE = "FALSE" *) 
  (* C_USE_LVDS = "FALSE" *) 
  (* C_USE_TBI = "FALSE" *) 
  (* C_USE_TRANSCEIVER = "TRUE" *) 
  (* GT_RX_BYTE_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_v16_2_19 gig_ethernet_pcs_pma_0_core
       (.an_adv_config_val(1'b0),
        .an_adv_config_vector({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .an_enable(NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED),
        .an_interrupt(NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED),
        .an_restart_config(1'b0),
        .basex_or_sgmii(1'b0),
        .configuration_valid(1'b0),
        .configuration_vector({1'b0,configuration_vector,1'b0}),
        .correction_timer({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dcm_locked(data_in),
        .drp_daddr(NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED[9:0]),
        .drp_dclk(1'b0),
        .drp_den(NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED),
        .drp_di(NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED[15:0]),
        .drp_do({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drp_drdy(1'b0),
        .drp_dwe(NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED),
        .drp_gnt(1'b0),
        .drp_req(NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED),
        .en_cdet(NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED),
        .enablealign(enablealign),
        .ewrap(NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gtx_clk(1'b0),
        .link_timer_basex({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_sgmii({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_value({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .loc_ref(NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED),
        .mdc(1'b0),
        .mdio_in(1'b0),
        .mdio_out(NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED),
        .mdio_tri(NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED),
        .mgt_rx_reset(mgt_rx_reset),
        .mgt_tx_reset(mgt_tx_reset),
        .phyad({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pma_rx_clk0(1'b0),
        .pma_rx_clk1(1'b0),
        .powerdown(powerdown),
        .reset(out),
        .reset_done(resetdone),
        .rx_code_group0({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_code_group1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_gt_nominal_latency({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1,1'b1,1'b0,1'b0,1'b0}),
        .rxbufstatus({rxbuferr,1'b0}),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .rxclkcorcnt({1'b0,rxclkcorcnt}),
        .rxdata(rxdata),
        .rxdisperr(rxdisperr),
        .rxnotintable(rxnotintable),
        .rxphy_correction_timer(NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED[63:0]),
        .rxphy_ns_field(NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED[31:0]),
        .rxphy_s_field(NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED[47:0]),
        .rxrecclk(1'b0),
        .rxrundisp(1'b0),
        .s_axi_aclk(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED),
        .s_axi_awvalid(1'b0),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED),
        .s_axi_rdata(NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_resetn(1'b0),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wready(NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED),
        .s_axi_wvalid(1'b0),
        .signal_detect(signal_detect),
        .speed_is_100(1'b0),
        .speed_is_10_100(1'b0),
        .speed_selection(NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED[1:0]),
        .status_vector({NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED[15:7],status_vector}),
        .systemtimer_ns_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .systemtimer_s_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .tx_code_group(NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED[9:0]),
        .txbuferr(txbuferr),
        .txchardispmode(txchardispmode),
        .txchardispval(txchardispval),
        .txcharisk(txcharisk),
        .txdata(txdata),
        .userclk(1'b0),
        .userclk2(CLK));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block sync_block_rx_reset_done
       (.CLK(CLK),
        .data_in(transceiver_inst_n_12),
        .data_out(rx_reset_done_i));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_0 sync_block_tx_reset_done
       (.CLK(CLK),
        .data_in(transceiver_inst_n_11),
        .resetdone(resetdone),
        .resetdone_0(rx_reset_done_i));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_transceiver transceiver_inst
       (.CLK(CLK),
        .D(txchardispmode),
        .Q(rxclkcorcnt),
        .SR(mgt_rx_reset),
        .data_in(transceiver_inst_n_11),
        .data_sync_reg1(data_in),
        .enablealign(enablealign),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(gtxe2_i),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out),
        .powerdown(powerdown),
        .reset_sync5(mgt_tx_reset),
        .rx_fsm_reset_done_int_reg(transceiver_inst_n_12),
        .rxbufstatus(rxbuferr),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .\rxdata_reg[7]_0 (rxdata),
        .rxdisperr(rxdisperr),
        .rxn(rxn),
        .rxnotintable(rxnotintable),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .status_vector(status_vector[1]),
        .txbuferr(txbuferr),
        .txchardispval_reg_reg_0(txchardispval),
        .txcharisk_reg_reg_0(txcharisk),
        .\txdata_reg_reg[7]_0 (txdata),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_clocking
   (gtrefclk_out,
    gtrefclk_bufg,
    mmcm_locked,
    userclk,
    userclk2,
    rxuserclk2_out,
    gtrefclk_p,
    gtrefclk_n,
    txoutclk,
    mmcm_reset,
    rxoutclk);
  output gtrefclk_out;
  output gtrefclk_bufg;
  output mmcm_locked;
  output userclk;
  output userclk2;
  output rxuserclk2_out;
  input gtrefclk_p;
  input gtrefclk_n;
  input txoutclk;
  input mmcm_reset;
  input rxoutclk;

  wire clkfbout;
  wire clkout0;
  wire clkout1;
  wire gtrefclk_bufg;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire mmcm_locked;
  wire mmcm_reset;
  wire rxoutclk;
  wire rxuserclk2_out;
  wire txoutclk;
  wire txoutclk_bufg;
  wire userclk;
  wire userclk2;
  wire NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED;
  wire NLW_mmcm_adv_inst_DRDY_UNCONNECTED;
  wire NLW_mmcm_adv_inst_PSDONE_UNCONNECTED;
  wire [15:0]NLW_mmcm_adv_inst_DO_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_gtrefclk
       (.I(gtrefclk_out),
        .O(gtrefclk_bufg));
  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_txoutclk
       (.I(txoutclk),
        .O(txoutclk_bufg));
  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_userclk
       (.I(clkout1),
        .O(userclk));
  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_userclk2
       (.I(clkout0),
        .O(userclk2));
  (* box_type = "PRIMITIVE" *) 
  IBUFDS_GTE2 #(
    .CLKCM_CFG("TRUE"),
    .CLKRCV_TRST("TRUE"),
    .CLKSWING_CFG(2'b11)) 
    ibufds_gtrefclk
       (.CEB(1'b0),
        .I(gtrefclk_p),
        .IB(gtrefclk_n),
        .O(gtrefclk_out),
        .ODIV2(NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED));
  (* box_type = "PRIMITIVE" *) 
  MMCME2_ADV #(
    .BANDWIDTH("OPTIMIZED"),
    .CLKFBOUT_MULT_F(16.000000),
    .CLKFBOUT_PHASE(0.000000),
    .CLKFBOUT_USE_FINE_PS("FALSE"),
    .CLKIN1_PERIOD(16.000000),
    .CLKIN2_PERIOD(0.000000),
    .CLKOUT0_DIVIDE_F(8.000000),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT0_USE_FINE_PS("FALSE"),
    .CLKOUT1_DIVIDE(16),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUT1_USE_FINE_PS("FALSE"),
    .CLKOUT2_DIVIDE(1),
    .CLKOUT2_DUTY_CYCLE(0.500000),
    .CLKOUT2_PHASE(0.000000),
    .CLKOUT2_USE_FINE_PS("FALSE"),
    .CLKOUT3_DIVIDE(1),
    .CLKOUT3_DUTY_CYCLE(0.500000),
    .CLKOUT3_PHASE(0.000000),
    .CLKOUT3_USE_FINE_PS("FALSE"),
    .CLKOUT4_CASCADE("FALSE"),
    .CLKOUT4_DIVIDE(1),
    .CLKOUT4_DUTY_CYCLE(0.500000),
    .CLKOUT4_PHASE(0.000000),
    .CLKOUT4_USE_FINE_PS("FALSE"),
    .CLKOUT5_DIVIDE(1),
    .CLKOUT5_DUTY_CYCLE(0.500000),
    .CLKOUT5_PHASE(0.000000),
    .CLKOUT5_USE_FINE_PS("FALSE"),
    .CLKOUT6_DIVIDE(1),
    .CLKOUT6_DUTY_CYCLE(0.500000),
    .CLKOUT6_PHASE(0.000000),
    .CLKOUT6_USE_FINE_PS("FALSE"),
    .COMPENSATION("INTERNAL"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKINSEL_INVERTED(1'b0),
    .IS_PSEN_INVERTED(1'b0),
    .IS_PSINCDEC_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER1(0.010000),
    .REF_JITTER2(0.000000),
    .SS_EN("FALSE"),
    .SS_MODE("CENTER_HIGH"),
    .SS_MOD_PERIOD(10000),
    .STARTUP_WAIT("FALSE")) 
    mmcm_adv_inst
       (.CLKFBIN(clkfbout),
        .CLKFBOUT(clkfbout),
        .CLKFBOUTB(NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED),
        .CLKFBSTOPPED(NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED),
        .CLKIN1(txoutclk_bufg),
        .CLKIN2(1'b0),
        .CLKINSEL(1'b1),
        .CLKINSTOPPED(NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED),
        .CLKOUT0(clkout0),
        .CLKOUT0B(NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(clkout1),
        .CLKOUT1B(NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED),
        .CLKOUT2(NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED),
        .CLKOUT2B(NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED),
        .CLKOUT3(NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED),
        .CLKOUT3B(NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED),
        .CLKOUT4(NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED),
        .CLKOUT5(NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED),
        .CLKOUT6(NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_mmcm_adv_inst_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_mmcm_adv_inst_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(mmcm_locked),
        .PSCLK(1'b0),
        .PSDONE(NLW_mmcm_adv_inst_PSDONE_UNCONNECTED),
        .PSEN(1'b0),
        .PSINCDEC(1'b0),
        .PWRDWN(1'b0),
        .RST(mmcm_reset));
  (* box_type = "PRIMITIVE" *) 
  BUFG rxrecclkbufg
       (.I(rxoutclk),
        .O(rxuserclk2_out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_cpll_railing
   (cpll_pd0_i,
    cpllreset_in,
    gtrefclk_bufg,
    gt0_cpllreset_t);
  output cpll_pd0_i;
  output cpllreset_in;
  input gtrefclk_bufg;
  input gt0_cpllreset_t;

  wire cpll_pd0_i;
  wire cpll_reset_out;
  wire \cpllpd_wait_reg[31]_srl32_n_1 ;
  wire \cpllpd_wait_reg[63]_srl32_n_1 ;
  wire \cpllpd_wait_reg[94]_srl31_n_0 ;
  wire cpllreset_in;
  wire \cpllreset_wait_reg[126]_srl31_n_0 ;
  wire \cpllreset_wait_reg[31]_srl32_n_1 ;
  wire \cpllreset_wait_reg[63]_srl32_n_1 ;
  wire \cpllreset_wait_reg[95]_srl32_n_1 ;
  wire gt0_cpllreset_t;
  wire gtrefclk_bufg;
  wire \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED ;

  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[31]_srl32 " *) 
  SRLC32E #(
    .INIT(32'hFFFFFFFF)) 
    \cpllpd_wait_reg[31]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(1'b0),
        .Q(\NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllpd_wait_reg[31]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[63]_srl32 " *) 
  SRLC32E #(
    .INIT(32'hFFFFFFFF)) 
    \cpllpd_wait_reg[63]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllpd_wait_reg[31]_srl32_n_1 ),
        .Q(\NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllpd_wait_reg[63]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[94]_srl31 " *) 
  SRLC32E #(
    .INIT(32'h7FFFFFFF)) 
    \cpllpd_wait_reg[94]_srl31 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b0}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllpd_wait_reg[63]_srl32_n_1 ),
        .Q(\cpllpd_wait_reg[94]_srl31_n_0 ),
        .Q31(\NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED ));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b1)) 
    \cpllpd_wait_reg[95] 
       (.C(gtrefclk_bufg),
        .CE(1'b1),
        .D(\cpllpd_wait_reg[94]_srl31_n_0 ),
        .Q(cpll_pd0_i),
        .R(1'b0));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[126]_srl31 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[126]_srl31 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b0}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllreset_wait_reg[95]_srl32_n_1 ),
        .Q(\cpllreset_wait_reg[126]_srl31_n_0 ),
        .Q31(\NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED ));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \cpllreset_wait_reg[127] 
       (.C(gtrefclk_bufg),
        .CE(1'b1),
        .D(\cpllreset_wait_reg[126]_srl31_n_0 ),
        .Q(cpll_reset_out),
        .R(1'b0));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[31]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h000000FF)) 
    \cpllreset_wait_reg[31]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(1'b0),
        .Q(\NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[31]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[63]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[63]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllreset_wait_reg[31]_srl32_n_1 ),
        .Q(\NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[63]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[95]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[95]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllreset_wait_reg[63]_srl32_n_1 ),
        .Q(\NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[95]_srl32_n_1 ));
  LUT2 #(
    .INIT(4'hE)) 
    gtxe2_i_i_1
       (.I0(cpll_reset_out),
        .I1(gt0_cpllreset_t),
        .O(cpllreset_in));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_gt_common
   (gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    gtrefclk_out,
    independent_clock_bufg,
    out);
  output gt0_qplloutclk_out;
  output gt0_qplloutrefclk_out;
  input gtrefclk_out;
  input independent_clock_bufg;
  input [0:0]out;

  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_out;
  wire gtxe2_common_i_n_2;
  wire gtxe2_common_i_n_5;
  wire independent_clock_bufg;
  wire [0:0]out;
  wire NLW_gtxe2_common_i_DRPRDY_UNCONNECTED;
  wire NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED;
  wire NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED;
  wire [15:0]NLW_gtxe2_common_i_DRPDO_UNCONNECTED;
  wire [7:0]NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  GTXE2_COMMON #(
    .BIAS_CFG(64'h0000040000001000),
    .COMMON_CFG(32'h00000000),
    .IS_DRPCLK_INVERTED(1'b0),
    .IS_GTGREFCLK_INVERTED(1'b0),
    .IS_QPLLLOCKDETCLK_INVERTED(1'b0),
    .QPLL_CFG(27'h06801C1),
    .QPLL_CLKOUT_CFG(4'b0000),
    .QPLL_COARSE_FREQ_OVRD(6'b010000),
    .QPLL_COARSE_FREQ_OVRD_EN(1'b0),
    .QPLL_CP(10'b0000011111),
    .QPLL_CP_MONITOR_EN(1'b0),
    .QPLL_DMONITOR_SEL(1'b0),
    .QPLL_FBDIV(10'b0000100000),
    .QPLL_FBDIV_MONITOR_EN(1'b0),
    .QPLL_FBDIV_RATIO(1'b1),
    .QPLL_INIT_CFG(24'h000006),
    .QPLL_LOCK_CFG(16'h21E8),
    .QPLL_LPF(4'b1111),
    .QPLL_REFCLK_DIV(1),
    .SIM_QPLLREFCLK_SEL(3'b001),
    .SIM_RESET_SPEEDUP("FALSE"),
    .SIM_VERSION("4.0")) 
    gtxe2_common_i
       (.BGBYPASSB(1'b1),
        .BGMONITORENB(1'b1),
        .BGPDB(1'b1),
        .BGRCALOVRD({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DRPADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPCLK(1'b0),
        .DRPDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPDO(NLW_gtxe2_common_i_DRPDO_UNCONNECTED[15:0]),
        .DRPEN(1'b0),
        .DRPRDY(NLW_gtxe2_common_i_DRPRDY_UNCONNECTED),
        .DRPWE(1'b0),
        .GTGREFCLK(1'b0),
        .GTNORTHREFCLK0(1'b0),
        .GTNORTHREFCLK1(1'b0),
        .GTREFCLK0(gtrefclk_out),
        .GTREFCLK1(1'b0),
        .GTSOUTHREFCLK0(1'b0),
        .GTSOUTHREFCLK1(1'b0),
        .PMARSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLDMONITOR(NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED[7:0]),
        .QPLLFBCLKLOST(NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED),
        .QPLLLOCK(gtxe2_common_i_n_2),
        .QPLLLOCKDETCLK(independent_clock_bufg),
        .QPLLLOCKEN(1'b1),
        .QPLLOUTCLK(gt0_qplloutclk_out),
        .QPLLOUTREFCLK(gt0_qplloutrefclk_out),
        .QPLLOUTRESET(1'b0),
        .QPLLPD(1'b1),
        .QPLLREFCLKLOST(gtxe2_common_i_n_5),
        .QPLLREFCLKSEL({1'b0,1'b0,1'b1}),
        .QPLLRESET(out),
        .QPLLRSVD1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLRSVD2({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .RCALENB(1'b1),
        .REFCLKOUTMONITOR(NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync
   (reset_out,
    CLK,
    enablealign);
  output reset_out;
  input CLK;
  input enablealign;

  wire CLK;
  wire enablealign;
  wire reset_out;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(CLK),
        .CE(1'b1),
        .D(1'b0),
        .PRE(enablealign),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(enablealign),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(enablealign),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(enablealign),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(enablealign),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_reset_sync" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_1
   (reset_out,
    independent_clock_bufg,
    SR);
  output reset_out;
  input independent_clock_bufg;
  input [0:0]SR;

  wire [0:0]SR;
  wire independent_clock_bufg;
  wire reset_out;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(SR),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(SR),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(SR),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(SR),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(SR),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_reset_sync" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_2
   (reset_out,
    independent_clock_bufg,
    reset_sync5_0);
  output reset_out;
  input independent_clock_bufg;
  input [0:0]reset_sync5_0;

  wire independent_clock_bufg;
  wire reset_out;
  wire [0:0]reset_sync5_0;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_wtd_timer
   (reset,
    independent_clock_bufg,
    data_out);
  output reset;
  input independent_clock_bufg;
  input data_out;

  wire \counter_stg1[5]_i_1_n_0 ;
  wire \counter_stg1[5]_i_3_n_0 ;
  wire [5:5]counter_stg1_reg;
  wire [4:0]counter_stg1_reg__0;
  wire \counter_stg2[0]_i_3_n_0 ;
  wire [11:0]counter_stg2_reg;
  wire \counter_stg2_reg[0]_i_2_n_0 ;
  wire \counter_stg2_reg[0]_i_2_n_1 ;
  wire \counter_stg2_reg[0]_i_2_n_2 ;
  wire \counter_stg2_reg[0]_i_2_n_3 ;
  wire \counter_stg2_reg[0]_i_2_n_4 ;
  wire \counter_stg2_reg[0]_i_2_n_5 ;
  wire \counter_stg2_reg[0]_i_2_n_6 ;
  wire \counter_stg2_reg[0]_i_2_n_7 ;
  wire \counter_stg2_reg[4]_i_1_n_0 ;
  wire \counter_stg2_reg[4]_i_1_n_1 ;
  wire \counter_stg2_reg[4]_i_1_n_2 ;
  wire \counter_stg2_reg[4]_i_1_n_3 ;
  wire \counter_stg2_reg[4]_i_1_n_4 ;
  wire \counter_stg2_reg[4]_i_1_n_5 ;
  wire \counter_stg2_reg[4]_i_1_n_6 ;
  wire \counter_stg2_reg[4]_i_1_n_7 ;
  wire \counter_stg2_reg[8]_i_1_n_1 ;
  wire \counter_stg2_reg[8]_i_1_n_2 ;
  wire \counter_stg2_reg[8]_i_1_n_3 ;
  wire \counter_stg2_reg[8]_i_1_n_4 ;
  wire \counter_stg2_reg[8]_i_1_n_5 ;
  wire \counter_stg2_reg[8]_i_1_n_6 ;
  wire \counter_stg2_reg[8]_i_1_n_7 ;
  wire counter_stg30;
  wire \counter_stg3[0]_i_3_n_0 ;
  wire \counter_stg3[0]_i_4_n_0 ;
  wire \counter_stg3[0]_i_5_n_0 ;
  wire [11:0]counter_stg3_reg;
  wire \counter_stg3_reg[0]_i_2_n_0 ;
  wire \counter_stg3_reg[0]_i_2_n_1 ;
  wire \counter_stg3_reg[0]_i_2_n_2 ;
  wire \counter_stg3_reg[0]_i_2_n_3 ;
  wire \counter_stg3_reg[0]_i_2_n_4 ;
  wire \counter_stg3_reg[0]_i_2_n_5 ;
  wire \counter_stg3_reg[0]_i_2_n_6 ;
  wire \counter_stg3_reg[0]_i_2_n_7 ;
  wire \counter_stg3_reg[4]_i_1_n_0 ;
  wire \counter_stg3_reg[4]_i_1_n_1 ;
  wire \counter_stg3_reg[4]_i_1_n_2 ;
  wire \counter_stg3_reg[4]_i_1_n_3 ;
  wire \counter_stg3_reg[4]_i_1_n_4 ;
  wire \counter_stg3_reg[4]_i_1_n_5 ;
  wire \counter_stg3_reg[4]_i_1_n_6 ;
  wire \counter_stg3_reg[4]_i_1_n_7 ;
  wire \counter_stg3_reg[8]_i_1_n_1 ;
  wire \counter_stg3_reg[8]_i_1_n_2 ;
  wire \counter_stg3_reg[8]_i_1_n_3 ;
  wire \counter_stg3_reg[8]_i_1_n_4 ;
  wire \counter_stg3_reg[8]_i_1_n_5 ;
  wire \counter_stg3_reg[8]_i_1_n_6 ;
  wire \counter_stg3_reg[8]_i_1_n_7 ;
  wire data_out;
  wire eqOp;
  wire independent_clock_bufg;
  wire [5:0]plusOp;
  wire reset;
  wire reset0;
  wire reset_i_2_n_0;
  wire reset_i_3_n_0;
  wire reset_i_4_n_0;
  wire reset_i_5_n_0;
  wire reset_i_6_n_0;
  wire [3:3]\NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \counter_stg1[0]_i_1 
       (.I0(counter_stg1_reg__0[0]),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \counter_stg1[1]_i_1 
       (.I0(counter_stg1_reg__0[0]),
        .I1(counter_stg1_reg__0[1]),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \counter_stg1[2]_i_1 
       (.I0(counter_stg1_reg__0[1]),
        .I1(counter_stg1_reg__0[0]),
        .I2(counter_stg1_reg__0[2]),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \counter_stg1[3]_i_1 
       (.I0(counter_stg1_reg__0[2]),
        .I1(counter_stg1_reg__0[0]),
        .I2(counter_stg1_reg__0[1]),
        .I3(counter_stg1_reg__0[3]),
        .O(plusOp[3]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \counter_stg1[4]_i_1 
       (.I0(counter_stg1_reg__0[3]),
        .I1(counter_stg1_reg__0[1]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[2]),
        .I4(counter_stg1_reg__0[4]),
        .O(plusOp[4]));
  LUT5 #(
    .INIT(32'hFFFF2000)) 
    \counter_stg1[5]_i_1 
       (.I0(reset_i_2_n_0),
        .I1(counter_stg3_reg[0]),
        .I2(reset_i_3_n_0),
        .I3(\counter_stg1[5]_i_3_n_0 ),
        .I4(data_out),
        .O(\counter_stg1[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \counter_stg1[5]_i_2 
       (.I0(counter_stg1_reg__0[4]),
        .I1(counter_stg1_reg__0[2]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[1]),
        .I4(counter_stg1_reg__0[3]),
        .I5(counter_stg1_reg),
        .O(plusOp[5]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \counter_stg1[5]_i_3 
       (.I0(counter_stg1_reg__0[3]),
        .I1(counter_stg1_reg__0[1]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[2]),
        .I4(counter_stg1_reg__0[4]),
        .O(\counter_stg1[5]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[0]),
        .Q(counter_stg1_reg__0[0]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[1]),
        .Q(counter_stg1_reg__0[1]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[2]),
        .Q(counter_stg1_reg__0[2]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[3]),
        .Q(counter_stg1_reg__0[3]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[4] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[4]),
        .Q(counter_stg1_reg__0[4]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[5] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[5]),
        .Q(counter_stg1_reg),
        .R(\counter_stg1[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter_stg2[0]_i_1 
       (.I0(counter_stg1_reg__0[4]),
        .I1(counter_stg1_reg__0[2]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[1]),
        .I4(counter_stg1_reg__0[3]),
        .I5(counter_stg1_reg),
        .O(eqOp));
  LUT1 #(
    .INIT(2'h1)) 
    \counter_stg2[0]_i_3 
       (.I0(counter_stg2_reg[0]),
        .O(\counter_stg2[0]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[0] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_7 ),
        .Q(counter_stg2_reg[0]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg2_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\counter_stg2_reg[0]_i_2_n_0 ,\counter_stg2_reg[0]_i_2_n_1 ,\counter_stg2_reg[0]_i_2_n_2 ,\counter_stg2_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\counter_stg2_reg[0]_i_2_n_4 ,\counter_stg2_reg[0]_i_2_n_5 ,\counter_stg2_reg[0]_i_2_n_6 ,\counter_stg2_reg[0]_i_2_n_7 }),
        .S({counter_stg2_reg[3:1],\counter_stg2[0]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[10] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_5 ),
        .Q(counter_stg2_reg[10]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[11] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_4 ),
        .Q(counter_stg2_reg[11]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[1] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_6 ),
        .Q(counter_stg2_reg[1]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[2] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_5 ),
        .Q(counter_stg2_reg[2]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[3] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_4 ),
        .Q(counter_stg2_reg[3]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[4] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_7 ),
        .Q(counter_stg2_reg[4]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg2_reg[4]_i_1 
       (.CI(\counter_stg2_reg[0]_i_2_n_0 ),
        .CO({\counter_stg2_reg[4]_i_1_n_0 ,\counter_stg2_reg[4]_i_1_n_1 ,\counter_stg2_reg[4]_i_1_n_2 ,\counter_stg2_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg2_reg[4]_i_1_n_4 ,\counter_stg2_reg[4]_i_1_n_5 ,\counter_stg2_reg[4]_i_1_n_6 ,\counter_stg2_reg[4]_i_1_n_7 }),
        .S(counter_stg2_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[5] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_6 ),
        .Q(counter_stg2_reg[5]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[6] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_5 ),
        .Q(counter_stg2_reg[6]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[7] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_4 ),
        .Q(counter_stg2_reg[7]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[8] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_7 ),
        .Q(counter_stg2_reg[8]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg2_reg[8]_i_1 
       (.CI(\counter_stg2_reg[4]_i_1_n_0 ),
        .CO({\NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED [3],\counter_stg2_reg[8]_i_1_n_1 ,\counter_stg2_reg[8]_i_1_n_2 ,\counter_stg2_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg2_reg[8]_i_1_n_4 ,\counter_stg2_reg[8]_i_1_n_5 ,\counter_stg2_reg[8]_i_1_n_6 ,\counter_stg2_reg[8]_i_1_n_7 }),
        .S(counter_stg2_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[9] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_6 ),
        .Q(counter_stg2_reg[9]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \counter_stg3[0]_i_1 
       (.I0(\counter_stg3[0]_i_3_n_0 ),
        .I1(\counter_stg3[0]_i_4_n_0 ),
        .I2(counter_stg2_reg[0]),
        .I3(\counter_stg1[5]_i_3_n_0 ),
        .O(counter_stg30));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter_stg3[0]_i_3 
       (.I0(counter_stg2_reg[3]),
        .I1(counter_stg2_reg[4]),
        .I2(counter_stg2_reg[1]),
        .I3(counter_stg2_reg[2]),
        .I4(counter_stg2_reg[6]),
        .I5(counter_stg2_reg[5]),
        .O(\counter_stg3[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter_stg3[0]_i_4 
       (.I0(counter_stg2_reg[9]),
        .I1(counter_stg2_reg[10]),
        .I2(counter_stg2_reg[7]),
        .I3(counter_stg2_reg[8]),
        .I4(counter_stg1_reg),
        .I5(counter_stg2_reg[11]),
        .O(\counter_stg3[0]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \counter_stg3[0]_i_5 
       (.I0(counter_stg3_reg[0]),
        .O(\counter_stg3[0]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[0] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_7 ),
        .Q(counter_stg3_reg[0]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg3_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\counter_stg3_reg[0]_i_2_n_0 ,\counter_stg3_reg[0]_i_2_n_1 ,\counter_stg3_reg[0]_i_2_n_2 ,\counter_stg3_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\counter_stg3_reg[0]_i_2_n_4 ,\counter_stg3_reg[0]_i_2_n_5 ,\counter_stg3_reg[0]_i_2_n_6 ,\counter_stg3_reg[0]_i_2_n_7 }),
        .S({counter_stg3_reg[3:1],\counter_stg3[0]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[10] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_5 ),
        .Q(counter_stg3_reg[10]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[11] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_4 ),
        .Q(counter_stg3_reg[11]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[1] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_6 ),
        .Q(counter_stg3_reg[1]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[2] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_5 ),
        .Q(counter_stg3_reg[2]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[3] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_4 ),
        .Q(counter_stg3_reg[3]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[4] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_7 ),
        .Q(counter_stg3_reg[4]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg3_reg[4]_i_1 
       (.CI(\counter_stg3_reg[0]_i_2_n_0 ),
        .CO({\counter_stg3_reg[4]_i_1_n_0 ,\counter_stg3_reg[4]_i_1_n_1 ,\counter_stg3_reg[4]_i_1_n_2 ,\counter_stg3_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg3_reg[4]_i_1_n_4 ,\counter_stg3_reg[4]_i_1_n_5 ,\counter_stg3_reg[4]_i_1_n_6 ,\counter_stg3_reg[4]_i_1_n_7 }),
        .S(counter_stg3_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[5] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_6 ),
        .Q(counter_stg3_reg[5]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[6] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_5 ),
        .Q(counter_stg3_reg[6]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[7] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_4 ),
        .Q(counter_stg3_reg[7]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[8] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_7 ),
        .Q(counter_stg3_reg[8]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg3_reg[8]_i_1 
       (.CI(\counter_stg3_reg[4]_i_1_n_0 ),
        .CO({\NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED [3],\counter_stg3_reg[8]_i_1_n_1 ,\counter_stg3_reg[8]_i_1_n_2 ,\counter_stg3_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg3_reg[8]_i_1_n_4 ,\counter_stg3_reg[8]_i_1_n_5 ,\counter_stg3_reg[8]_i_1_n_6 ,\counter_stg3_reg[8]_i_1_n_7 }),
        .S(counter_stg3_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[9] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_6 ),
        .Q(counter_stg3_reg[9]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h20)) 
    reset_i_1
       (.I0(reset_i_2_n_0),
        .I1(counter_stg3_reg[0]),
        .I2(reset_i_3_n_0),
        .O(reset0));
  LUT6 #(
    .INIT(64'h0000001000000000)) 
    reset_i_2
       (.I0(counter_stg3_reg[9]),
        .I1(counter_stg3_reg[10]),
        .I2(counter_stg3_reg[7]),
        .I3(counter_stg3_reg[8]),
        .I4(counter_stg2_reg[0]),
        .I5(counter_stg3_reg[11]),
        .O(reset_i_2_n_0));
  LUT3 #(
    .INIT(8'h80)) 
    reset_i_3
       (.I0(reset_i_4_n_0),
        .I1(reset_i_5_n_0),
        .I2(reset_i_6_n_0),
        .O(reset_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    reset_i_4
       (.I0(counter_stg2_reg[3]),
        .I1(counter_stg2_reg[4]),
        .I2(counter_stg2_reg[1]),
        .I3(counter_stg2_reg[2]),
        .I4(counter_stg2_reg[6]),
        .I5(counter_stg2_reg[5]),
        .O(reset_i_4_n_0));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    reset_i_5
       (.I0(counter_stg2_reg[10]),
        .I1(counter_stg2_reg[9]),
        .I2(counter_stg2_reg[8]),
        .I3(counter_stg2_reg[7]),
        .I4(counter_stg1_reg),
        .I5(counter_stg2_reg[11]),
        .O(reset_i_5_n_0));
  LUT6 #(
    .INIT(64'h0002000000000000)) 
    reset_i_6
       (.I0(counter_stg3_reg[4]),
        .I1(counter_stg3_reg[3]),
        .I2(counter_stg3_reg[1]),
        .I3(counter_stg3_reg[2]),
        .I4(counter_stg3_reg[6]),
        .I5(counter_stg3_reg[5]),
        .O(reset_i_6_n_0));
  FDRE reset_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset0),
        .Q(reset),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_resets
   (out,
    independent_clock_bufg,
    reset);
  output [0:0]out;
  input independent_clock_bufg;
  input reset;

  wire independent_clock_bufg;
  (* async_reg = "true" *) wire [3:0]pma_reset_pipe;
  wire reset;

  assign out[0] = pma_reset_pipe[3];
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset),
        .Q(pma_reset_pipe[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[0]),
        .PRE(reset),
        .Q(pma_reset_pipe[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[1]),
        .PRE(reset),
        .Q(pma_reset_pipe[2]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[2]),
        .PRE(reset),
        .Q(pma_reset_pipe[3]));
endmodule

(* EXAMPLE_SIMULATION = "0" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support
   (gtrefclk_p,
    gtrefclk_n,
    gtrefclk_out,
    gtrefclk_bufg_out,
    txp,
    txn,
    rxp,
    rxn,
    userclk_out,
    userclk2_out,
    rxuserclk_out,
    rxuserclk2_out,
    pma_reset_out,
    mmcm_locked_out,
    resetdone,
    independent_clock_bufg,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    configuration_vector,
    status_vector,
    reset,
    signal_detect,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out);
  input gtrefclk_p;
  input gtrefclk_n;
  output gtrefclk_out;
  output gtrefclk_bufg_out;
  output txp;
  output txn;
  input rxp;
  input rxn;
  output userclk_out;
  output userclk2_out;
  output rxuserclk_out;
  output rxuserclk2_out;
  output pma_reset_out;
  output mmcm_locked_out;
  output resetdone;
  input independent_clock_bufg;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  input [4:0]configuration_vector;
  output [15:0]status_vector;
  input reset;
  input signal_detect;
  output gt0_qplloutclk_out;
  output gt0_qplloutrefclk_out;

  wire \<const0> ;
  wire [4:0]configuration_vector;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg_out;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire independent_clock_bufg;
  wire mmcm_locked_out;
  wire mmcm_reset;
  wire pma_reset_out;
  wire reset;
  wire resetdone;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire rxuserclk2_out;
  wire signal_detect;
  wire [6:0]\^status_vector ;
  wire txn;
  wire txoutclk;
  wire txp;
  wire userclk2_out;
  wire userclk_out;

  assign rxuserclk_out = rxuserclk2_out;
  assign status_vector[15] = \<const0> ;
  assign status_vector[14] = \<const0> ;
  assign status_vector[13] = \<const0> ;
  assign status_vector[12] = \<const0> ;
  assign status_vector[11] = \<const0> ;
  assign status_vector[10] = \<const0> ;
  assign status_vector[9] = \<const0> ;
  assign status_vector[8] = \<const0> ;
  assign status_vector[7] = \<const0> ;
  assign status_vector[6:0] = \^status_vector [6:0];
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_clocking core_clocking_i
       (.gtrefclk_bufg(gtrefclk_bufg_out),
        .gtrefclk_n(gtrefclk_n),
        .gtrefclk_out(gtrefclk_out),
        .gtrefclk_p(gtrefclk_p),
        .mmcm_locked(mmcm_locked_out),
        .mmcm_reset(mmcm_reset),
        .rxoutclk(rxoutclk),
        .rxuserclk2_out(rxuserclk2_out),
        .txoutclk(txoutclk),
        .userclk(userclk_out),
        .userclk2(userclk2_out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_gt_common core_gt_common_i
       (.gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_out(gtrefclk_out),
        .independent_clock_bufg(independent_clock_bufg),
        .out(pma_reset_out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_resets core_resets_i
       (.independent_clock_bufg(independent_clock_bufg),
        .out(pma_reset_out),
        .reset(reset));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_block pcs_pma_block_i
       (.CLK(userclk2_out),
        .configuration_vector(configuration_vector[3:1]),
        .data_in(mmcm_locked_out),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg_out),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(userclk_out),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(pma_reset_out),
        .resetdone(resetdone),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .signal_detect(signal_detect),
        .status_vector(\^status_vector ),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block
   (data_out,
    data_in,
    CLK);
  output data_out;
  input data_in;
  input CLK;

  wire CLK;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_0
   (resetdone,
    resetdone_0,
    data_in,
    CLK);
  output resetdone;
  input resetdone_0;
  input data_in;
  input CLK;

  wire CLK;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire resetdone;
  wire resetdone_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    resetdone_INST_0
       (.I0(data_out),
        .I1(resetdone_0),
        .O(resetdone));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_10
   (data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_11
   (\FSM_sequential_rx_state_reg[1] ,
    Q,
    rxresetdone_s3,
    data_sync_reg1_0,
    independent_clock_bufg);
  output \FSM_sequential_rx_state_reg[1] ;
  input [2:0]Q;
  input rxresetdone_s3;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire \FSM_sequential_rx_state_reg[1] ;
  wire [2:0]Q;
  wire cplllock_sync;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;
  wire rxresetdone_s3;

  LUT5 #(
    .INIT(32'h008F0080)) 
    \FSM_sequential_rx_state[3]_i_5 
       (.I0(Q[0]),
        .I1(rxresetdone_s3),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(cplllock_sync),
        .O(\FSM_sequential_rx_state_reg[1] ));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(cplllock_sync),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_12
   (\FSM_sequential_rx_state_reg[1] ,
    rx_fsm_reset_done_int_reg,
    D,
    E,
    reset_time_out_reg,
    reset_time_out_reg_0,
    Q,
    reset_time_out_reg_1,
    reset_time_out_reg_2,
    data_in,
    \FSM_sequential_rx_state_reg[1]_0 ,
    rx_fsm_reset_done_int_reg_0,
    rx_fsm_reset_done_int_reg_1,
    \FSM_sequential_rx_state_reg[0] ,
    \FSM_sequential_rx_state_reg[0]_0 ,
    \FSM_sequential_rx_state_reg[0]_1 ,
    mmcm_lock_reclocked,
    \FSM_sequential_rx_state_reg[0]_2 ,
    time_out_wait_bypass_s3,
    \FSM_sequential_rx_state_reg[3] ,
    \FSM_sequential_rx_state_reg[0]_3 ,
    rx_fsm_reset_done_int_reg_2,
    rx_fsm_reset_done_int_reg_3,
    data_out,
    independent_clock_bufg);
  output \FSM_sequential_rx_state_reg[1] ;
  output rx_fsm_reset_done_int_reg;
  output [2:0]D;
  output [0:0]E;
  input reset_time_out_reg;
  input reset_time_out_reg_0;
  input [3:0]Q;
  input reset_time_out_reg_1;
  input reset_time_out_reg_2;
  input data_in;
  input \FSM_sequential_rx_state_reg[1]_0 ;
  input rx_fsm_reset_done_int_reg_0;
  input rx_fsm_reset_done_int_reg_1;
  input \FSM_sequential_rx_state_reg[0] ;
  input \FSM_sequential_rx_state_reg[0]_0 ;
  input \FSM_sequential_rx_state_reg[0]_1 ;
  input mmcm_lock_reclocked;
  input \FSM_sequential_rx_state_reg[0]_2 ;
  input time_out_wait_bypass_s3;
  input \FSM_sequential_rx_state_reg[3] ;
  input \FSM_sequential_rx_state_reg[0]_3 ;
  input rx_fsm_reset_done_int_reg_2;
  input rx_fsm_reset_done_int_reg_3;
  input data_out;
  input independent_clock_bufg;

  wire [2:0]D;
  wire [0:0]E;
  wire \FSM_sequential_rx_state[0]_i_3_n_0 ;
  wire \FSM_sequential_rx_state[1]_i_2_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_4_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_6_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_8_n_0 ;
  wire \FSM_sequential_rx_state_reg[0] ;
  wire \FSM_sequential_rx_state_reg[0]_0 ;
  wire \FSM_sequential_rx_state_reg[0]_1 ;
  wire \FSM_sequential_rx_state_reg[0]_2 ;
  wire \FSM_sequential_rx_state_reg[0]_3 ;
  wire \FSM_sequential_rx_state_reg[1] ;
  wire \FSM_sequential_rx_state_reg[1]_0 ;
  wire \FSM_sequential_rx_state_reg[3] ;
  wire [3:0]Q;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_valid_sync;
  wire independent_clock_bufg;
  wire mmcm_lock_reclocked;
  wire reset_time_out_i_2_n_0;
  wire reset_time_out_reg;
  wire reset_time_out_reg_0;
  wire reset_time_out_reg_1;
  wire reset_time_out_reg_2;
  wire rx_fsm_reset_done_int;
  wire rx_fsm_reset_done_int_i_3_n_0;
  wire rx_fsm_reset_done_int_i_4_n_0;
  wire rx_fsm_reset_done_int_reg;
  wire rx_fsm_reset_done_int_reg_0;
  wire rx_fsm_reset_done_int_reg_1;
  wire rx_fsm_reset_done_int_reg_2;
  wire rx_fsm_reset_done_int_reg_3;
  wire time_out_wait_bypass_s3;

  LUT5 #(
    .INIT(32'hFFEFEFEF)) 
    \FSM_sequential_rx_state[0]_i_1 
       (.I0(\FSM_sequential_rx_state_reg[0]_2 ),
        .I1(\FSM_sequential_rx_state[0]_i_3_n_0 ),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[3]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT4 #(
    .INIT(16'h0200)) 
    \FSM_sequential_rx_state[0]_i_3 
       (.I0(Q[3]),
        .I1(reset_time_out_reg_2),
        .I2(data_valid_sync),
        .I3(rx_fsm_reset_done_int_reg_1),
        .O(\FSM_sequential_rx_state[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF24200400)) 
    \FSM_sequential_rx_state[1]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(\FSM_sequential_rx_state[1]_i_2_n_0 ),
        .I5(\FSM_sequential_rx_state_reg[1]_0 ),
        .O(D[1]));
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_rx_state[1]_i_2 
       (.I0(data_valid_sync),
        .I1(rx_fsm_reset_done_int_reg_1),
        .O(\FSM_sequential_rx_state[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEEE)) 
    \FSM_sequential_rx_state[3]_i_1 
       (.I0(\FSM_sequential_rx_state_reg[0] ),
        .I1(\FSM_sequential_rx_state[3]_i_4_n_0 ),
        .I2(Q[0]),
        .I3(reset_time_out_reg),
        .I4(\FSM_sequential_rx_state[3]_i_6_n_0 ),
        .I5(\FSM_sequential_rx_state_reg[0]_0 ),
        .O(E));
  LUT6 #(
    .INIT(64'hFFFFFFFFCCC0C4C4)) 
    \FSM_sequential_rx_state[3]_i_2 
       (.I0(time_out_wait_bypass_s3),
        .I1(Q[3]),
        .I2(Q[1]),
        .I3(\FSM_sequential_rx_state[3]_i_8_n_0 ),
        .I4(Q[0]),
        .I5(\FSM_sequential_rx_state_reg[3] ),
        .O(D[2]));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAEFEA)) 
    \FSM_sequential_rx_state[3]_i_4 
       (.I0(\FSM_sequential_rx_state[0]_i_3_n_0 ),
        .I1(\FSM_sequential_rx_state_reg[0]_1 ),
        .I2(Q[2]),
        .I3(\FSM_sequential_rx_state_reg[0]_3 ),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\FSM_sequential_rx_state[3]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h0CE20CCC)) 
    \FSM_sequential_rx_state[3]_i_6 
       (.I0(mmcm_lock_reclocked),
        .I1(Q[3]),
        .I2(data_valid_sync),
        .I3(Q[1]),
        .I4(Q[0]),
        .O(\FSM_sequential_rx_state[3]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hFD)) 
    \FSM_sequential_rx_state[3]_i_8 
       (.I0(rx_fsm_reset_done_int_reg_1),
        .I1(data_valid_sync),
        .I2(reset_time_out_reg_2),
        .O(\FSM_sequential_rx_state[3]_i_8_n_0 ));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_out),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_valid_sync),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hEEEFFFFFEEEF0000)) 
    reset_time_out_i_1__0
       (.I0(reset_time_out_i_2_n_0),
        .I1(reset_time_out_reg),
        .I2(reset_time_out_reg_0),
        .I3(Q[1]),
        .I4(reset_time_out_reg_1),
        .I5(reset_time_out_reg_2),
        .O(\FSM_sequential_rx_state_reg[1] ));
  LUT6 #(
    .INIT(64'h0FF30E0E0FF30202)) 
    reset_time_out_i_2
       (.I0(\FSM_sequential_rx_state_reg[0]_1 ),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(data_valid_sync),
        .I4(Q[3]),
        .I5(mmcm_lock_reclocked),
        .O(reset_time_out_i_2_n_0));
  LUT4 #(
    .INIT(16'hABA8)) 
    rx_fsm_reset_done_int_i_1
       (.I0(rx_fsm_reset_done_int),
        .I1(rx_fsm_reset_done_int_i_3_n_0),
        .I2(rx_fsm_reset_done_int_i_4_n_0),
        .I3(data_in),
        .O(rx_fsm_reset_done_int_reg));
  LUT5 #(
    .INIT(32'h00040000)) 
    rx_fsm_reset_done_int_i_2
       (.I0(Q[0]),
        .I1(data_valid_sync),
        .I2(Q[2]),
        .I3(reset_time_out_reg_2),
        .I4(rx_fsm_reset_done_int_reg_2),
        .O(rx_fsm_reset_done_int));
  LUT6 #(
    .INIT(64'h0400040004040400)) 
    rx_fsm_reset_done_int_i_3
       (.I0(rx_fsm_reset_done_int_reg_0),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(data_valid_sync),
        .I4(rx_fsm_reset_done_int_reg_1),
        .I5(reset_time_out_reg_2),
        .O(rx_fsm_reset_done_int_i_3_n_0));
  LUT6 #(
    .INIT(64'h0008000808080008)) 
    rx_fsm_reset_done_int_i_4
       (.I0(rx_fsm_reset_done_int_reg_3),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(data_valid_sync),
        .I4(rx_fsm_reset_done_int_reg_2),
        .I5(reset_time_out_reg_2),
        .O(rx_fsm_reset_done_int_i_4_n_0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_13
   (SR,
    data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output [0:0]SR;
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire [0:0]SR;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[7]_i_1__0 
       (.I0(data_out),
        .O(SR));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_14
   (data_out,
    data_in,
    data_sync_reg1_0);
  output data_out;
  input data_in;
  input data_sync_reg1_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_15
   (data_out,
    data_in,
    independent_clock_bufg);
  output data_out;
  input data_in;
  input independent_clock_bufg;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_16
   (data_out,
    data_in,
    data_sync_reg6_0);
  output data_out;
  input data_in;
  input data_sync_reg6_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg6_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_3
   (data_out,
    status_vector,
    independent_clock_bufg);
  output data_out;
  input [0:0]status_vector;
  input independent_clock_bufg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire independent_clock_bufg;
  wire [0:0]status_vector;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(status_vector),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_4
   (data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_5
   (reset_time_out_reg,
    E,
    reset_time_out_reg_0,
    reset_time_out,
    \FSM_sequential_tx_state_reg[0] ,
    \FSM_sequential_tx_state_reg[0]_0 ,
    \FSM_sequential_tx_state_reg[0]_1 ,
    \FSM_sequential_tx_state_reg[0]_2 ,
    \FSM_sequential_tx_state_reg[0]_3 ,
    Q,
    reset_time_out_reg_1,
    mmcm_lock_reclocked,
    \FSM_sequential_tx_state_reg[0]_4 ,
    \FSM_sequential_tx_state_reg[0]_5 ,
    \FSM_sequential_tx_state_reg[0]_6 ,
    data_sync_reg1_0,
    independent_clock_bufg);
  output reset_time_out_reg;
  output [0:0]E;
  input reset_time_out_reg_0;
  input reset_time_out;
  input \FSM_sequential_tx_state_reg[0] ;
  input \FSM_sequential_tx_state_reg[0]_0 ;
  input \FSM_sequential_tx_state_reg[0]_1 ;
  input \FSM_sequential_tx_state_reg[0]_2 ;
  input \FSM_sequential_tx_state_reg[0]_3 ;
  input [3:0]Q;
  input reset_time_out_reg_1;
  input mmcm_lock_reclocked;
  input \FSM_sequential_tx_state_reg[0]_4 ;
  input \FSM_sequential_tx_state_reg[0]_5 ;
  input \FSM_sequential_tx_state_reg[0]_6 ;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire [0:0]E;
  wire \FSM_sequential_tx_state[3]_i_5_n_0 ;
  wire \FSM_sequential_tx_state_reg[0] ;
  wire \FSM_sequential_tx_state_reg[0]_0 ;
  wire \FSM_sequential_tx_state_reg[0]_1 ;
  wire \FSM_sequential_tx_state_reg[0]_2 ;
  wire \FSM_sequential_tx_state_reg[0]_3 ;
  wire \FSM_sequential_tx_state_reg[0]_4 ;
  wire \FSM_sequential_tx_state_reg[0]_5 ;
  wire \FSM_sequential_tx_state_reg[0]_6 ;
  wire [3:0]Q;
  wire cplllock_sync;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;
  wire mmcm_lock_reclocked;
  wire reset_time_out;
  wire reset_time_out_i_3__0_n_0;
  wire reset_time_out_i_4__0_n_0;
  wire reset_time_out_reg;
  wire reset_time_out_reg_0;
  wire reset_time_out_reg_1;

  LUT6 #(
    .INIT(64'hFFFFFFFEFFFEFFFE)) 
    \FSM_sequential_tx_state[3]_i_1 
       (.I0(\FSM_sequential_tx_state_reg[0] ),
        .I1(\FSM_sequential_tx_state_reg[0]_0 ),
        .I2(\FSM_sequential_tx_state[3]_i_5_n_0 ),
        .I3(\FSM_sequential_tx_state_reg[0]_1 ),
        .I4(\FSM_sequential_tx_state_reg[0]_2 ),
        .I5(\FSM_sequential_tx_state_reg[0]_3 ),
        .O(E));
  LUT6 #(
    .INIT(64'h0000000000F00008)) 
    \FSM_sequential_tx_state[3]_i_5 
       (.I0(\FSM_sequential_tx_state_reg[0]_4 ),
        .I1(\FSM_sequential_tx_state_reg[0]_5 ),
        .I2(cplllock_sync),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(\FSM_sequential_tx_state_reg[0]_6 ),
        .O(\FSM_sequential_tx_state[3]_i_5_n_0 ));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(cplllock_sync),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hEFE0)) 
    reset_time_out_i_1
       (.I0(reset_time_out_reg_0),
        .I1(reset_time_out_i_3__0_n_0),
        .I2(reset_time_out_i_4__0_n_0),
        .I3(reset_time_out),
        .O(reset_time_out_reg));
  LUT6 #(
    .INIT(64'h020002000F000200)) 
    reset_time_out_i_3__0
       (.I0(cplllock_sync),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(Q[0]),
        .I4(mmcm_lock_reclocked),
        .I5(Q[1]),
        .O(reset_time_out_i_3__0_n_0));
  LUT6 #(
    .INIT(64'h0505FF040505F504)) 
    reset_time_out_i_4__0
       (.I0(Q[1]),
        .I1(reset_time_out_reg_1),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(Q[3]),
        .I5(cplllock_sync),
        .O(reset_time_out_i_4__0_n_0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_6
   (SR,
    data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output [0:0]SR;
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire [0:0]SR;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[7]_i_1 
       (.I0(data_out),
        .O(SR));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_7
   (data_out,
    data_in,
    data_sync_reg6_0);
  output data_out;
  input data_in;
  input data_sync_reg6_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg6_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_8
   (data_out,
    data_in,
    independent_clock_bufg);
  output data_out;
  input data_in;
  input independent_clock_bufg;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_9
   (data_out,
    data_in,
    data_sync_reg1_0);
  output data_out;
  input data_in;
  input data_sync_reg1_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_transceiver
   (txn,
    txp,
    rxoutclk,
    txoutclk,
    rxchariscomma,
    rxcharisk,
    rxdisperr,
    rxnotintable,
    rxbufstatus,
    txbuferr,
    mmcm_reset,
    data_in,
    rx_fsm_reset_done_int_reg,
    Q,
    \rxdata_reg[7]_0 ,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    gtxe2_i,
    SR,
    CLK,
    powerdown,
    reset_sync5,
    D,
    txchardispval_reg_reg_0,
    txcharisk_reg_reg_0,
    out,
    status_vector,
    enablealign,
    data_sync_reg1,
    \txdata_reg_reg[7]_0 );
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output [0:0]rxchariscomma;
  output [0:0]rxcharisk;
  output [0:0]rxdisperr;
  output [0:0]rxnotintable;
  output [0:0]rxbufstatus;
  output txbuferr;
  output mmcm_reset;
  output data_in;
  output rx_fsm_reset_done_int_reg;
  output [1:0]Q;
  output [7:0]\rxdata_reg[7]_0 ;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input gtxe2_i;
  input [0:0]SR;
  input CLK;
  input powerdown;
  input [0:0]reset_sync5;
  input [0:0]D;
  input [0:0]txchardispval_reg_reg_0;
  input [0:0]txcharisk_reg_reg_0;
  input [0:0]out;
  input [0:0]status_vector;
  input enablealign;
  input data_sync_reg1;
  input [7:0]\txdata_reg_reg[7]_0 ;

  wire CLK;
  wire [0:0]D;
  wire [1:0]Q;
  wire [0:0]SR;
  wire data_in;
  wire data_sync_reg1;
  wire data_valid_reg2;
  wire enablealign;
  wire encommaalign_int;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtwizard_inst_n_6;
  wire gtwizard_inst_n_7;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire mmcm_reset;
  wire [0:0]out;
  wire p_0_in;
  wire powerdown;
  wire reset;
  wire [0:0]reset_sync5;
  wire rx_fsm_reset_done_int_reg;
  wire [0:0]rxbufstatus;
  wire [0:0]rxchariscomma;
  wire [1:0]rxchariscomma_double;
  wire rxchariscomma_i_1_n_0;
  wire [1:0]rxchariscomma_int;
  wire [1:0]rxchariscomma_reg__0;
  wire [0:0]rxcharisk;
  wire [1:0]rxcharisk_double;
  wire rxcharisk_i_1_n_0;
  wire [1:0]rxcharisk_int;
  wire [1:0]rxcharisk_reg__0;
  wire [1:0]rxclkcorcnt_double;
  wire [1:0]rxclkcorcnt_int;
  wire [1:0]rxclkcorcnt_reg;
  wire \rxdata[0]_i_1_n_0 ;
  wire \rxdata[1]_i_1_n_0 ;
  wire \rxdata[2]_i_1_n_0 ;
  wire \rxdata[3]_i_1_n_0 ;
  wire \rxdata[4]_i_1_n_0 ;
  wire \rxdata[5]_i_1_n_0 ;
  wire \rxdata[6]_i_1_n_0 ;
  wire \rxdata[7]_i_1_n_0 ;
  wire [15:0]rxdata_double;
  wire [15:0]rxdata_int;
  wire [15:0]rxdata_reg;
  wire [7:0]\rxdata_reg[7]_0 ;
  wire [0:0]rxdisperr;
  wire [1:0]rxdisperr_double;
  wire rxdisperr_i_1_n_0;
  wire [1:0]rxdisperr_int;
  wire [1:0]rxdisperr_reg__0;
  wire rxn;
  wire [0:0]rxnotintable;
  wire [1:0]rxnotintable_double;
  wire rxnotintable_i_1_n_0;
  wire [1:0]rxnotintable_int;
  wire [1:0]rxnotintable_reg__0;
  wire rxoutclk;
  wire rxp;
  wire rxpowerdown;
  wire rxpowerdown_double;
  wire rxpowerdown_reg__0;
  wire rxreset_int;
  wire [0:0]status_vector;
  wire toggle;
  wire toggle_i_1_n_0;
  wire toggle_rx;
  wire toggle_rx_i_1_n_0;
  wire txbuferr;
  wire [1:1]txbufstatus_reg;
  wire [1:0]txchardispmode_double;
  wire [1:0]txchardispmode_int;
  wire txchardispmode_reg;
  wire [1:0]txchardispval_double;
  wire [1:0]txchardispval_int;
  wire txchardispval_reg;
  wire [0:0]txchardispval_reg_reg_0;
  wire [1:0]txcharisk_double;
  wire [1:0]txcharisk_int;
  wire txcharisk_reg;
  wire [0:0]txcharisk_reg_reg_0;
  wire [15:0]txdata_double;
  wire [15:0]txdata_int;
  wire [7:0]txdata_reg;
  wire [7:0]\txdata_reg_reg[7]_0 ;
  wire txn;
  wire txoutclk;
  wire txp;
  wire txpowerdown;
  wire txpowerdown_double;
  wire txpowerdown_reg__0;
  wire txreset_int;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD gtwizard_inst
       (.D(rxclkcorcnt_int),
        .Q(txdata_int),
        .RXBUFSTATUS(gtwizard_inst_n_7),
        .RXPD(rxpowerdown),
        .TXBUFSTATUS(gtwizard_inst_n_6),
        .TXPD(txpowerdown),
        .data_in(data_in),
        .data_out(data_valid_reg2),
        .data_sync_reg1(data_sync_reg1),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(rxdata_int),
        .gtxe2_i_0(rxchariscomma_int),
        .gtxe2_i_1(rxcharisk_int),
        .gtxe2_i_2(rxdisperr_int),
        .gtxe2_i_3(rxnotintable_int),
        .gtxe2_i_4(gtxe2_i),
        .gtxe2_i_5(txchardispmode_int),
        .gtxe2_i_6(txchardispval_int),
        .gtxe2_i_7(txcharisk_int),
        .gtxe2_i_8(rxreset_int),
        .gtxe2_i_9(txreset_int),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out),
        .reset(reset),
        .reset_out(encommaalign_int),
        .rx_fsm_reset_done_int_reg(rx_fsm_reset_done_int_reg),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync reclock_encommaalign
       (.CLK(CLK),
        .enablealign(enablealign),
        .reset_out(encommaalign_int));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_1 reclock_rxreset
       (.SR(SR),
        .independent_clock_bufg(independent_clock_bufg),
        .reset_out(rxreset_int));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_2 reclock_txreset
       (.independent_clock_bufg(independent_clock_bufg),
        .reset_out(txreset_int),
        .reset_sync5_0(reset_sync5));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_wtd_timer reset_wtd_timer
       (.data_out(data_valid_reg2),
        .independent_clock_bufg(independent_clock_bufg),
        .reset(reset));
  FDRE rxbuferr_reg
       (.C(CLK),
        .CE(1'b1),
        .D(p_0_in),
        .Q(rxbufstatus),
        .R(1'b0));
  FDRE \rxbufstatus_reg_reg[2] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(gtwizard_inst_n_7),
        .Q(p_0_in),
        .R(1'b0));
  FDRE \rxchariscomma_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxchariscomma_reg__0[0]),
        .Q(rxchariscomma_double[0]),
        .R(SR));
  FDRE \rxchariscomma_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxchariscomma_reg__0[1]),
        .Q(rxchariscomma_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxchariscomma_i_1
       (.I0(rxchariscomma_double[1]),
        .I1(toggle_rx),
        .I2(rxchariscomma_double[0]),
        .O(rxchariscomma_i_1_n_0));
  FDRE rxchariscomma_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxchariscomma_i_1_n_0),
        .Q(rxchariscomma),
        .R(SR));
  FDRE \rxchariscomma_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxchariscomma_int[0]),
        .Q(rxchariscomma_reg__0[0]),
        .R(1'b0));
  FDRE \rxchariscomma_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxchariscomma_int[1]),
        .Q(rxchariscomma_reg__0[1]),
        .R(1'b0));
  FDRE \rxcharisk_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxcharisk_reg__0[0]),
        .Q(rxcharisk_double[0]),
        .R(SR));
  FDRE \rxcharisk_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxcharisk_reg__0[1]),
        .Q(rxcharisk_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxcharisk_i_1
       (.I0(rxcharisk_double[1]),
        .I1(toggle_rx),
        .I2(rxcharisk_double[0]),
        .O(rxcharisk_i_1_n_0));
  FDRE rxcharisk_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxcharisk_i_1_n_0),
        .Q(rxcharisk),
        .R(SR));
  FDRE \rxcharisk_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxcharisk_int[0]),
        .Q(rxcharisk_reg__0[0]),
        .R(1'b0));
  FDRE \rxcharisk_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxcharisk_int[1]),
        .Q(rxcharisk_reg__0[1]),
        .R(1'b0));
  FDRE \rxclkcorcnt_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxclkcorcnt_reg[0]),
        .Q(rxclkcorcnt_double[0]),
        .R(SR));
  FDRE \rxclkcorcnt_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxclkcorcnt_reg[1]),
        .Q(rxclkcorcnt_double[1]),
        .R(SR));
  FDRE \rxclkcorcnt_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxclkcorcnt_double[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE \rxclkcorcnt_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxclkcorcnt_double[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE \rxclkcorcnt_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxclkcorcnt_int[0]),
        .Q(rxclkcorcnt_reg[0]),
        .R(1'b0));
  FDRE \rxclkcorcnt_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxclkcorcnt_int[1]),
        .Q(rxclkcorcnt_reg[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[0]_i_1 
       (.I0(rxdata_double[8]),
        .I1(toggle_rx),
        .I2(rxdata_double[0]),
        .O(\rxdata[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[1]_i_1 
       (.I0(rxdata_double[9]),
        .I1(toggle_rx),
        .I2(rxdata_double[1]),
        .O(\rxdata[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[2]_i_1 
       (.I0(rxdata_double[10]),
        .I1(toggle_rx),
        .I2(rxdata_double[2]),
        .O(\rxdata[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[3]_i_1 
       (.I0(rxdata_double[11]),
        .I1(toggle_rx),
        .I2(rxdata_double[3]),
        .O(\rxdata[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[4]_i_1 
       (.I0(rxdata_double[12]),
        .I1(toggle_rx),
        .I2(rxdata_double[4]),
        .O(\rxdata[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[5]_i_1 
       (.I0(rxdata_double[13]),
        .I1(toggle_rx),
        .I2(rxdata_double[5]),
        .O(\rxdata[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[6]_i_1 
       (.I0(rxdata_double[14]),
        .I1(toggle_rx),
        .I2(rxdata_double[6]),
        .O(\rxdata[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[7]_i_1 
       (.I0(rxdata_double[15]),
        .I1(toggle_rx),
        .I2(rxdata_double[7]),
        .O(\rxdata[7]_i_1_n_0 ));
  FDRE \rxdata_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[0]),
        .Q(rxdata_double[0]),
        .R(SR));
  FDRE \rxdata_double_reg[10] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[10]),
        .Q(rxdata_double[10]),
        .R(SR));
  FDRE \rxdata_double_reg[11] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[11]),
        .Q(rxdata_double[11]),
        .R(SR));
  FDRE \rxdata_double_reg[12] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[12]),
        .Q(rxdata_double[12]),
        .R(SR));
  FDRE \rxdata_double_reg[13] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[13]),
        .Q(rxdata_double[13]),
        .R(SR));
  FDRE \rxdata_double_reg[14] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[14]),
        .Q(rxdata_double[14]),
        .R(SR));
  FDRE \rxdata_double_reg[15] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[15]),
        .Q(rxdata_double[15]),
        .R(SR));
  FDRE \rxdata_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[1]),
        .Q(rxdata_double[1]),
        .R(SR));
  FDRE \rxdata_double_reg[2] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[2]),
        .Q(rxdata_double[2]),
        .R(SR));
  FDRE \rxdata_double_reg[3] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[3]),
        .Q(rxdata_double[3]),
        .R(SR));
  FDRE \rxdata_double_reg[4] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[4]),
        .Q(rxdata_double[4]),
        .R(SR));
  FDRE \rxdata_double_reg[5] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[5]),
        .Q(rxdata_double[5]),
        .R(SR));
  FDRE \rxdata_double_reg[6] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[6]),
        .Q(rxdata_double[6]),
        .R(SR));
  FDRE \rxdata_double_reg[7] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[7]),
        .Q(rxdata_double[7]),
        .R(SR));
  FDRE \rxdata_double_reg[8] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[8]),
        .Q(rxdata_double[8]),
        .R(SR));
  FDRE \rxdata_double_reg[9] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[9]),
        .Q(rxdata_double[9]),
        .R(SR));
  FDRE \rxdata_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[0]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [0]),
        .R(SR));
  FDRE \rxdata_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[1]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [1]),
        .R(SR));
  FDRE \rxdata_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[2]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [2]),
        .R(SR));
  FDRE \rxdata_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[3]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [3]),
        .R(SR));
  FDRE \rxdata_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[4]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [4]),
        .R(SR));
  FDRE \rxdata_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[5]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [5]),
        .R(SR));
  FDRE \rxdata_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[6]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [6]),
        .R(SR));
  FDRE \rxdata_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[7]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [7]),
        .R(SR));
  FDRE \rxdata_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[0]),
        .Q(rxdata_reg[0]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[10] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[10]),
        .Q(rxdata_reg[10]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[11] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[11]),
        .Q(rxdata_reg[11]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[12] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[12]),
        .Q(rxdata_reg[12]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[13] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[13]),
        .Q(rxdata_reg[13]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[14] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[14]),
        .Q(rxdata_reg[14]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[15] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[15]),
        .Q(rxdata_reg[15]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[1]),
        .Q(rxdata_reg[1]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[2] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[2]),
        .Q(rxdata_reg[2]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[3] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[3]),
        .Q(rxdata_reg[3]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[4] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[4]),
        .Q(rxdata_reg[4]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[5] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[5]),
        .Q(rxdata_reg[5]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[6] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[6]),
        .Q(rxdata_reg[6]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[7] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[7]),
        .Q(rxdata_reg[7]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[8] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[8]),
        .Q(rxdata_reg[8]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[9] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[9]),
        .Q(rxdata_reg[9]),
        .R(1'b0));
  FDRE \rxdisperr_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdisperr_reg__0[0]),
        .Q(rxdisperr_double[0]),
        .R(SR));
  FDRE \rxdisperr_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdisperr_reg__0[1]),
        .Q(rxdisperr_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxdisperr_i_1
       (.I0(rxdisperr_double[1]),
        .I1(toggle_rx),
        .I2(rxdisperr_double[0]),
        .O(rxdisperr_i_1_n_0));
  FDRE rxdisperr_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxdisperr_i_1_n_0),
        .Q(rxdisperr),
        .R(SR));
  FDRE \rxdisperr_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdisperr_int[0]),
        .Q(rxdisperr_reg__0[0]),
        .R(1'b0));
  FDRE \rxdisperr_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdisperr_int[1]),
        .Q(rxdisperr_reg__0[1]),
        .R(1'b0));
  FDRE \rxnotintable_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxnotintable_reg__0[0]),
        .Q(rxnotintable_double[0]),
        .R(SR));
  FDRE \rxnotintable_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxnotintable_reg__0[1]),
        .Q(rxnotintable_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxnotintable_i_1
       (.I0(rxnotintable_double[1]),
        .I1(toggle_rx),
        .I2(rxnotintable_double[0]),
        .O(rxnotintable_i_1_n_0));
  FDRE rxnotintable_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxnotintable_i_1_n_0),
        .Q(rxnotintable),
        .R(SR));
  FDRE \rxnotintable_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxnotintable_int[0]),
        .Q(rxnotintable_reg__0[0]),
        .R(1'b0));
  FDRE \rxnotintable_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxnotintable_int[1]),
        .Q(rxnotintable_reg__0[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_double_reg
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxpowerdown_reg__0),
        .Q(rxpowerdown_double),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_reg
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxpowerdown_double),
        .Q(rxpowerdown),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(powerdown),
        .Q(rxpowerdown_reg__0),
        .R(SR));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_3 sync_block_data_valid
       (.data_out(data_valid_reg2),
        .independent_clock_bufg(independent_clock_bufg),
        .status_vector(status_vector));
  LUT1 #(
    .INIT(2'h1)) 
    toggle_i_1
       (.I0(toggle),
        .O(toggle_i_1_n_0));
  FDRE toggle_reg
       (.C(CLK),
        .CE(1'b1),
        .D(toggle_i_1_n_0),
        .Q(toggle),
        .R(reset_sync5));
  LUT1 #(
    .INIT(2'h1)) 
    toggle_rx_i_1
       (.I0(toggle_rx),
        .O(toggle_rx_i_1_n_0));
  FDRE toggle_rx_reg
       (.C(CLK),
        .CE(1'b1),
        .D(toggle_rx_i_1_n_0),
        .Q(toggle_rx),
        .R(SR));
  FDRE txbuferr_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txbufstatus_reg),
        .Q(txbuferr),
        .R(1'b0));
  FDRE \txbufstatus_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(gtwizard_inst_n_6),
        .Q(txbufstatus_reg),
        .R(1'b0));
  FDRE \txchardispmode_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txchardispmode_reg),
        .Q(txchardispmode_double[0]),
        .R(reset_sync5));
  FDRE \txchardispmode_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(D),
        .Q(txchardispmode_double[1]),
        .R(reset_sync5));
  FDRE \txchardispmode_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispmode_double[0]),
        .Q(txchardispmode_int[0]),
        .R(1'b0));
  FDRE \txchardispmode_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispmode_double[1]),
        .Q(txchardispmode_int[1]),
        .R(1'b0));
  FDRE txchardispmode_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(D),
        .Q(txchardispmode_reg),
        .R(reset_sync5));
  FDRE \txchardispval_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txchardispval_reg),
        .Q(txchardispval_double[0]),
        .R(reset_sync5));
  FDRE \txchardispval_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txchardispval_reg_reg_0),
        .Q(txchardispval_double[1]),
        .R(reset_sync5));
  FDRE \txchardispval_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispval_double[0]),
        .Q(txchardispval_int[0]),
        .R(1'b0));
  FDRE \txchardispval_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispval_double[1]),
        .Q(txchardispval_int[1]),
        .R(1'b0));
  FDRE txchardispval_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txchardispval_reg_reg_0),
        .Q(txchardispval_reg),
        .R(reset_sync5));
  FDRE \txcharisk_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txcharisk_reg),
        .Q(txcharisk_double[0]),
        .R(reset_sync5));
  FDRE \txcharisk_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txcharisk_reg_reg_0),
        .Q(txcharisk_double[1]),
        .R(reset_sync5));
  FDRE \txcharisk_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txcharisk_double[0]),
        .Q(txcharisk_int[0]),
        .R(1'b0));
  FDRE \txcharisk_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txcharisk_double[1]),
        .Q(txcharisk_int[1]),
        .R(1'b0));
  FDRE txcharisk_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txcharisk_reg_reg_0),
        .Q(txcharisk_reg),
        .R(reset_sync5));
  FDRE \txdata_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[0]),
        .Q(txdata_double[0]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[10] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [2]),
        .Q(txdata_double[10]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[11] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [3]),
        .Q(txdata_double[11]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[12] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [4]),
        .Q(txdata_double[12]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[13] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [5]),
        .Q(txdata_double[13]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[14] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [6]),
        .Q(txdata_double[14]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[15] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [7]),
        .Q(txdata_double[15]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[1]),
        .Q(txdata_double[1]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[2] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[2]),
        .Q(txdata_double[2]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[3] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[3]),
        .Q(txdata_double[3]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[4] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[4]),
        .Q(txdata_double[4]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[5] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[5]),
        .Q(txdata_double[5]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[6] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[6]),
        .Q(txdata_double[6]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[7] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[7]),
        .Q(txdata_double[7]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[8] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [0]),
        .Q(txdata_double[8]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[9] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [1]),
        .Q(txdata_double[9]),
        .R(reset_sync5));
  FDRE \txdata_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[0]),
        .Q(txdata_int[0]),
        .R(1'b0));
  FDRE \txdata_int_reg[10] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[10]),
        .Q(txdata_int[10]),
        .R(1'b0));
  FDRE \txdata_int_reg[11] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[11]),
        .Q(txdata_int[11]),
        .R(1'b0));
  FDRE \txdata_int_reg[12] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[12]),
        .Q(txdata_int[12]),
        .R(1'b0));
  FDRE \txdata_int_reg[13] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[13]),
        .Q(txdata_int[13]),
        .R(1'b0));
  FDRE \txdata_int_reg[14] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[14]),
        .Q(txdata_int[14]),
        .R(1'b0));
  FDRE \txdata_int_reg[15] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[15]),
        .Q(txdata_int[15]),
        .R(1'b0));
  FDRE \txdata_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[1]),
        .Q(txdata_int[1]),
        .R(1'b0));
  FDRE \txdata_int_reg[2] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[2]),
        .Q(txdata_int[2]),
        .R(1'b0));
  FDRE \txdata_int_reg[3] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[3]),
        .Q(txdata_int[3]),
        .R(1'b0));
  FDRE \txdata_int_reg[4] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[4]),
        .Q(txdata_int[4]),
        .R(1'b0));
  FDRE \txdata_int_reg[5] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[5]),
        .Q(txdata_int[5]),
        .R(1'b0));
  FDRE \txdata_int_reg[6] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[6]),
        .Q(txdata_int[6]),
        .R(1'b0));
  FDRE \txdata_int_reg[7] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[7]),
        .Q(txdata_int[7]),
        .R(1'b0));
  FDRE \txdata_int_reg[8] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[8]),
        .Q(txdata_int[8]),
        .R(1'b0));
  FDRE \txdata_int_reg[9] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[9]),
        .Q(txdata_int[9]),
        .R(1'b0));
  FDRE \txdata_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [0]),
        .Q(txdata_reg[0]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [1]),
        .Q(txdata_reg[1]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [2]),
        .Q(txdata_reg[2]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [3]),
        .Q(txdata_reg[3]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [4]),
        .Q(txdata_reg[4]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [5]),
        .Q(txdata_reg[5]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [6]),
        .Q(txdata_reg[6]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [7]),
        .Q(txdata_reg[7]),
        .R(reset_sync5));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_double_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txpowerdown_reg__0),
        .Q(txpowerdown_double),
        .R(reset_sync5));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_reg
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txpowerdown_double),
        .Q(txpowerdown),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(powerdown),
        .Q(txpowerdown_reg__0),
        .R(reset_sync5));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2024.1.1"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
GNlNeNP2E6l/J7IS2LF/ItWIQpPIQxmqiENbpRcUcYDs2zMQssAZwDKWmGxc99etc2e+6UESUZFU
1OoN93QWwGY5lwtShrTc9cE66KsLO3U82lRN16EHmi+RZOdYR9rf5z7OgjqfdL/T03/rfwU37+Qw
8V3wDE2i5hqr8BCHEwM=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
afRfkAd8cICiTJYylAj89MfRuyfOa9d/tbr4mvbd5mWPw07cLRhiyi8gIe4+Ayw0CiOk7/EjPgWg
Tg/1hR99BWRw4VbbDTWu69lZfP8CCWoBxtdt8WRuUGz7PLk6qQMaoDf1ogw0xyXTK96u8N6bef3z
7mR3YInM0ECnk0+8vQDLRo6ETSTf4oQYcckMpPfbPQ+NnHRvcHroYdddFGNYasbeUqwn7tkHnYO+
ADUKING4g3DhZO3UAIPZJggQnH3ayID9nf49Oa7bj//jtWD0qELIGFMQn48spke+RcfDkdFLHWhY
0esV8oRsRwIWfwXwqK6i7aIis3paozzKSWbiNg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
ZI9ECw+Xpdoykt5zjVXLh0QGbCnqQDaDo8xSAWsYltuhqetS6+j38MNsVABP/vySCUkxp7yLlCMg
506S40Li7Nmy7qD4YvDLHMe+fIgwi3Hb193aoDluxnxdVk/aJ7VOJWXd88+scqfUSpbbFv7rk9La
8woMpCSiDNVKi6NB7Qs=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
R1h0p6UTerhva5Yb3duDj5nR3CyyGXjeE4dSD+H9PUwvK6SElm7FlL9AjlrnVaE2OmUXbsHu+WYi
T1XMnV1ekw5xTRICAudvAhAJrZYMHEjujHOfgXPmKtXHivF59pFlTxnEtD+dnchOL4x/jMm/lCgH
k3/wzvZbfOaxaKFceauCooIRZkqZa5FA3KG53cEucbspEldbnPaAcHsiO04PEwklvjo+2XAiGTHB
YB9XEkRUOprwSG+TeOWFNvz4Yu2FYHffCsobvgsF/8dSJQ1SVv3rM1uHf3coB/s7NIgZ0ye03NO/
wLFRL+3OuPxWNTqHO9YYXL0XTd1rPaaWB/mCcw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
4l+Avk44zzaOVxRJDzFVTXMyJmyE6qVo8wAiHogr6M/NkYt3AgnEITBHDJ9dF4SVZQLiV7ZY6shr
asWzrbISsiBCAaZgyPZLr1zlBErx08H5Enf1NkLuocQa0x2Dc1DwG+mO+flli1++tKpmHM6ADCS0
3/8fREk+ga01VkXbOXY6B7Wt4G2O3LBdFCkEvd0/MEx5uYrJNrtpaeSd3h2CpYLsfPsxxbAzNJN1
3pvNKoS8QwUnTPy7oe/k8bFD0TOzZmLvT/dy3x7SFlB6oSYr7FtWMOOuH5B08Upu75qdujq2/6ox
I9BucH2FnuLsghHnPuWfjh4WTDPekeCy51G/7w==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2023_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
r1TCY1FBxgJgPdlaGrkCY3ZPZ26hJnsdsOKnUu75PgvjzuIkty6RkwizEzV4coW2cxRBcWf87Rmb
xbDdy2cDu4f2qP6QUVBsGtY+ITHBIqBPqY7IqmHvnmT8DQ7mta5QD9HpSpd8E0HAu+1hmp2y9R2T
j6cEj7KNzaIGnuiJLket69e1dx31L14fHppyx7EJTOKaIYVHOTGcZeHi6ozbitA95N9bUNiSCS93
sJcjWWTWEB9Cx8arBeHBMIflTsCrUzgwd6f7Lb59R/pt8Zgvgr/BoV20ZGDDy6BCwEWgGSw6GDBL
o5ppIN+dICJOxCzX0Ij4ziKkTNPa1OnlJpcXcA==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
U0o+hd53NUTkWW/pm++iDbvu9EzyJbrE8cgDdW35QL1go6ppv1cnjM7W9LNAUoYpay5lYpZFkUZP
qgnoVBG2BG64sAHeAjjMVH8aUuTYzhhM3n6pvSqx5Ez8rezjhdOlMLX17yWFSCq3x15rfV2si91N
p+FFtjXOD3BmuNjRLXeeRjrvt6sxfN9IwN0yEZYhwp2ukPLeIyrYpRXZ+xMErI/b7BKLZNIljMSV
Cx5wPeURNjIC7AKZOJmo8YQe2JGJEHps80MGm98CsBtbIXDa7uAxib3WWstVs/UbQGgqjiyNIOSu
11VEYQSI8nO9bNfJPsLpVaeo9V+FUKdFcBgKbA==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
JlAVtvWu4RylemkeXAwQaOcauzsF85mjSoJqBa5/i7p1cOK0PM2Rfou44syNOWyUG43Mq5DdY2P2
hSdJ0BrVnIF/JY67xayAWorKfMTlvfZN3zVnvvFrLORWpe5MwPzwHomEMtQgK/WjTmMnFJzWxYti
gedoe+hxxEoZILnHN3ge9A2vnRikwcJETpOKLK13mQa1Td8zo9tsQuagX91Bw+AWLzMrHGR2bmQr
grI95HpSioNMY5CjomezDLSd80Rlkr+6SiaUQt3pqExFVEAsKNJdtppxNwugqojHWEqm1lF0wKuj
aLs/KfYF2Uv5wNjccSP2gHLWWaF5DKs5G5u4c4FjEiS/slCZgrt5Ymwc6XKQHNOHbPRsQcbbkPMD
V/YuoNzw1R2386n0pOd+vMD9nTtpB104VrwBNBxlGPPa0qiFM4+xr58vMzHKNT91irPu/EirEIjt
wEtLynnUeSKMKz3aAvXZPRYeEurD0fgZ6iczWVNZOsltSIQZjOp5ErOC

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GeMop+4rJGQJTsAts4IyQAkQMeKt+Fa026naAEt7J5/8nhAvWlIS4YQDNOLHMMUe8p+sUA4yE6+s
hf5STrRcmXzM45O/bYrkBzWHNqGcsEuAHfdUd9ANbBZmpncQ5ywZbXLX0oI4xVx55B8sn2gAj+p2
NQEPjYWPZXJNML4IilPsrW6TlgTi1zhp0JFBFT3O6yHY35dAseHXtMWYgjr0+6WBtVCCvC/z4bA0
UY58Skgytan1bUM557AlikG2LqNB6YHVUQihnx4ZProlAL61WY/j4joeXcDH8Tpm0cmYcUM5yUax
haoFS6FezWAqD4KpiUS3xpkUVET2QpN0NnxTvw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
1x9gXtJFU2nGQ9CWRMPqjLG5B7nyL7ZKhgWVKPrQTraMot+PY1MLqNEQ8371fSgOtWrUw7wBswaL
CBsGdy0dDZt13psueOntqYT0wcW3vUbYzfkMCsSjkk2yeYjnCYclwJKMNk3fGxfxpTLQsScOFING
ZyUHBaE+KeZ7yfdqvxwcpP+rUXdNX/2BGvcr+a0ow/nFsiCiOMdYIlAgllnBFoUV7lDr001O00Hk
1MDqVCRdpiO5q5FnggoIp1sVIq1C+JbqiuyPkv5jmlk3iIfZiHhSSYoxP3DJf7VPvlJ1Z/Hwz6Dw
/39nFCTotl6l9hqiLHTmISLBvSYZnjDEcOPtPA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
mxE8AqszO8jEcufcEIxsyswviid2I+vp0wiBEslkbc/wVr2vlpOaIaviDrRuTwIVx162O0AViWa2
3/lH2PLY1lcjenGETw7njsuJd6MQv4KihzbHhd1HB0TKpoL1n4E9u5PgNIGghcgNu7s2hE5rpffM
FAxMXxyhZQ3j4WBKTgRtbqcexvVSl/whSo87Oa/V48wwF7Z68uoZdaD4cdNihYwHMLOm0LYqYesQ
cScpoz43IdSFiQkBEM4YXuqn7BFIsJ7UAr+EkHAFxWYqt0IJ3+/OCuOYW+y/3p2tXT2YSGnEsOzZ
SLIsZpsw7wAiEfu9tVZJlTQ0OUTZqfQXK68i9A==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 153808)
`pragma protect data_block
j0lFNJ8qvza8Ezi1OrGI4wASbOlqL78bPiVT8UTmFCao45J1pvAjp+781+bGgNcwLvBPcnSyQ27n
7MgLxz8qlZwL4cATItW5TBB/6yt7I/fJZyn2taBcQ/Udn3oNQrptqqDpuf6zuGcT2C72fmzUl2Gr
x6Fnh6awztRLSIRydzDJs88wz5AbUZVDb6y+rLgmVpd3U6EaDg3XKrwCcRK0iiMBVtKKtNcQUtef
X3zoY2pmpeqcczpW/VeuX6piO3e6OFIts9d+VmYzoJQi2dFCc4/EB+8+ue7z6NYDHmkLlDEcYIAT
dwS150JjI3+yho2HTRLoLTD3Pb5A++n7LLKgC2UGi4626pLvpPmqw4TfxO4Kgqx0puHu1MGdvQ14
GAYtMlx/sZgYE9VFcy9PQxmH5eSVJpkTu0tliQrGHNm007ajZPOkaZWc6yY1p/M5o3a3/WQjFthr
KYacwN8TcVc/HhujBWBNe6+FrdeGPsFBocKWlzS3338yYgXI4OmduRlT9fngwWLDhfPpqzgzAGyb
qkYdpHBT8w8ARdu9HzVKvDNgtqIvxp7u/x/sQniD+ymfnAJ+s1uME0c4Tab8VNsWwGeAbLvY/OL0
2HlmIcmJuOKDYXBw5qLEE7Teq7rpk8KDkeP8So8V252N1G+hJYfxcJukTKevdSoWTWditCDhc1Y2
zUQ+fpS/5NmNS0WPP/PZPAgg5Z048oV84fcDQsRLWGoVhd50SDhX+uE/oLsN7rhH5I/1UvoQKojf
CC9tTEAvmZmF2QDONUQFAcmJDzHEHZptLpxgFmFdDLt+px4t7oPRulGe42tpHJhXzDuWyb54wabN
Yc8i7vEfmJ+oRZFAQZVZlfln886VjCVeMWhwLvWhQFjVlDsc8+ieuqU6NaZaXFF02BD5UYdkg7o4
qg1otVJARHfDzORQ0eo+NPSD/88vdhVJ1/h+30KBYslA7yLHucyStMqDCAA80joLTQCO2z0sskMz
hoVTAqSIDIo5peHr2Gef3IO0hv5U8LOD70S+7GZqMT/T93CBLKQjxWIfOdWWDb9Gt6WenWqW2DBq
a30CJUlTaK9ToX/K5tvTbnl7OhHdvL6MCk5IL8cxKjz94TCERXSZ2VYfGOVsGZtkRKjVwQM+w4nV
ILJ2gpvEAzDEG/fOs2z4R5gt5upmoUA8nUYBg4mAl43SB1RWJN4Md8V1lGjw0y8GaPx9v/adj83e
SK7Cc6VmS7hJUbtpiiOz46lXyscdwD+CWc/WvEXnHt0vlFqvS39hkeRUk/RTpGktL9fbatOvRrR4
JkjpxVqTNCuE0C9EB+gQ1Mz5nqn/73wMCGiSjczVwFM1pxfQF006NzOPmy1cBMfx4oIdmq4mEg5O
DgXZTXF9zSNdTY6oHV+yvXOKGBFUD7e0UUALBLOcGmPhl1b1EAonPKYuuXSF9pV/o+Mi1csJIE++
n7orANieHR0Hq3/oadDu7Nvr8x3LAENLHSE9CHKFCBq/TpJVdzq2tIwIEH+ag1RU18MYGRSx/0EC
UpJ3JuBg5uiWAacoAt4rAD9S8U85s7d2HrwtXtodME49r9bCCjmkblEAEdFlgVM0dGM8Ljw5uTmX
S7sOW7934zpBOZTOZJz8wuorkbfei4RET11pn9oaAaoRRxRn8i04C+W9jjPzNNAEFpNVrizY3Jbp
AOlmqYVMdrNPFtvIMIu+Frmquyfe2L4snn/glnqe+2+5RUBS7dUT6x7+nFVpl5IlVESCThO8FrsU
J2HYPuynJ+pdlVO73LaLYovfBRkYlFwUN94tYHGmUNIuZqQ94xN3ybKftr+in28ZB+/XNtcclhh/
k1Jaxtg+xuBw4pYJoGHHLHw9Hn5XMnT4fBGf+ayceK0Jacf+zviR571j6Fv/T2HucSI8vj5g+sOx
FWNI18yhF0zTujXmEPfcqw2srcKfyVFj+/zmmLb20Gr61gBPo2x3Me3MkptPTitUGYJ/CHnoTypA
/PGJ3avztGdkXTmVTm2mYPqRNtWHW9+clMJjZFny3aQa83/n3RXLE4yAoinvuGrM/ot5KdLbxrZ1
mCdsYHIg0MGRdgF3btO15PXHWBqeQuq2awHq7ce0/ttVQrU9146iqrJMe2e2dcb6koHtpMrxJiRu
cyZy0XU6qJE0lElq/Ip53XuZP8D12TrgMmB5ODM+oWGzrruLG1y7hFw3Akn4JmQyDBZF93CpVb9A
rqQHHFC87hO1CZSidqmFP6LyxR5mir6uE973t2iKSQCDCgQY6oQ1UJJh5HEWgt4V4wSXKgKuFn+p
SMngNbzf6i1y9BnWbJYe1Hr1tsKXDMJG+qrP/syugCCg4Hv7nYXg2oa8rfb2oxHLBwjX9R8J5ZHg
s/8eLEuIIwRvzj/Pd8lKvLM9ujyGpv044AypCbm0A+2Xi8BHokxEgnYzjgcM/0RqwUhXcRuYGATI
yBH0PogfZCextKhy3bxjkQLERWhRKRVMu8OBa6qfbam9tzaDqJo/o80j64Bac13ocjn4cNwHPnsT
DwDdOnAB5GI/4+ixEopY9P0blZ0kDAIviiyaZa35VCjTLlX5LXvR+BPaNDuPEwI3EQvl4Z4/0OOX
ePKGsRKYZ7T1yPs/6F0d1Wjxpx4PHG10Ju9mp+PXU6tQLwmlPy2li3ZN25hlyscgBRWbS60eqJcn
OiG1ouZt9wDyqAROA/nE8hpno7nl4v+N4H7vcA7LhveeWTnrYfC0L56AtdnBobpiXrQFZ3Ougiy0
xB4ai9+QZe3NiYlwwCyoyYYIYlrUKVOlNsBMMCyrA8yHO46FKbX8zZL1SsIAgozvKJ2jpB1yL7M2
art3pFwBXFc+IeBa4TH97z98c1YuiDwR+nQXeVroKfnufTeulT/oryztr81aQT7Ep6psRCW5g5Pu
9dWJA6j3Kp7+EDTWkV3hwqFHWPpsYzDgc28GDPBjMuMoyIt2b05igKcldHVZsngRV024pNBZ0fE6
RLoe71MjrGYs3rrL3nBr6GbdPknIwbxYDigOpzXsCZF27/zlDG34lE3MayyzUeYSUDcTeUqJh722
zvfp7t0ZirfeGQuZ+uYTHmVajMcdhq8CQZRwzewhFDBAaBfCRaQGVi5yLckw9v128HtvljoFKIm1
KWeABvAAd2rqNDMroG4iQ1FImOMMMpdUwJH4lgmMV+26eM5MXlUNf0hUrCx4ZsNeDZqHJHUMo4cB
I4IzwRKcKjrPNEjn/Mk+Ayp6brYc9i22dZWaFt47+a1y3Sq1aU2WtY6tkFx1Gy3jGQNr4rqHK3nN
cIVwLa61ndQ0W6KtqmH/3FPqf4CK3e33No72MK86NZmM/Ev7QmD+AREE6K5WgHp75ntvqIOOrVcb
sV13OxoOOwJuQYTAIRg4NLksKG7BeSEQD0E5YEOyHSoAUqbjZ9f/9Xt0Wlc1X7UFnVnaA03t+9ao
YvFPsbTBUck01+xgOT3bD9OB2b/XDMQfS3IJ4bc7jyJGNieRrg+UvhW6fMn2giY5wWY4NbwPhuxL
gjuhg+jiBumI+cysQMfbV8jAr+Z7xh+kxiXx9I/GeX2cIrnWDEN9K8q7huQoxsKcONmZjnQzU4mI
KBwPAYNmjCW/IFsIyp78B3rGLn3IHUMEwpBER71uuoHzY0ir8KPzVx8B45w/NEqsEcelH8CTyzr+
7wbiorz8CjeuTZRnE9lTC3X3NdQOcFkhXQRmJcjU2Utx4vU6OZ3DmT/cxYBiypekqdwNvqb9iYWa
YH16Ac4tbacI76YqqccCOpsOinHGmWVRpbKk8MOXqi/TIO/Nzsei0stx8f3I1HWGg3993U5rtHRM
SCQRC04LDGnmhl55l6fzTR8VFCWKLL6oVaBHTdESKfA3JuLHwNFBIRpMB986UKKT73cNjrst3eRR
g0hGfw2+oDogITBSIY/pcG3PPzbG801/eq4ZoMvwjjd8HjALkwrnO95AnIJmzP/SS7kA1L++0Zeg
FaanpRCJh+wSSQ5JBdiJz+TwmpPjsLn5BGg0e/jqOw6/9tL7b44IihuOM7YGEGhsxHCxdrLedlFh
+9ZcUtOHus1wXladdFSA92Xv/YtL2KJB3td33jzq6k9kF4UxNqOp+6GCcu6rYDfH/I23seRhDI8d
QQFLENvQRG0a8ASvDuFpVMSHzRkZfewZWb8/1TlS7FcmJYhfE397JcJZ3G/zX19LL22Kef3Cpi2i
R7nr8JMk02KE1GWhogqCLtuK8l5lYJ4GcZS6287PhFnXg+aAjNg3z+lMkI0SdffEg2uKjl+ZrRTa
HUsNcb+oS3Df17XsLGyRsfyairDCAmey9XFmtfX1yup7znENdSz6iAXMxlfDJOQNNS2dSlUC8NfG
bZJdI9SqAdNFvgtbTma+r6RWoXPsvT1Lfb9+pqhBaR57sKlajrA7GlpwjjqoJgFPqmTw1D9tSa2g
leIXa9GVA3Uuc8vpqE/Dn9Pmy0ROlbsAzf/D+8nyI2rPx81bTf2FQt6AynCTu5lnGRQkNW9Bc/PU
o+nMsSd4Jrt1CCD7zRk3D6Q/z/N5inKEj11S+0Ui4CRkZxPYp/FCEtBYRsXvzBglmLOvf8A8oeSf
5IuExmdhDYvjdp6sjzJ7N/RqoX2OZGqT50jokOJA4y0bIdrYDAdKxGVqFFXHiNWopjLhOcQYd3hH
s62S5vfo1ib/9+py0rhkOLGsHfyaAc0an9gtYrepVIH9sxHWMQ5SKB5uHuoDc3Qqc+XgoVPGtntt
1JmzBVyu4V48Ezu9Xgt1HqbdFVLa/yNmSvTk1SdOMw+E/1b8/9LA5lU/JrQhjKVyO2iLnX/O1Nj7
Gx/EdVLikofllGTVbL9Co8h+ankFi6zOQyIV/qo5e3T4zJaoeN78blt9hH2HSF/7FLAqGUzroEAe
WXB3/Eq+UO+xhgACuDZrysS7xbHToW+Wr/vrtkcnsOGBKlr4541UfG+xBNq6ejqFFhGFO20pxEvL
6P4/Ww1SGchgEtpTxbkdQ3YLvhZrNVpaDK2NPaIfB2W5sFVdZwVksQd6ZVnxtpJT4K+qLFnqL9Zs
afAg/Ns38lRtUP7pWK4ISr4g9clMSp+jrgEKeUgGx4IDUBNhzCAEgjJ2Ulwa2N7ebAwQp3a0cD2W
gXBtTBIBBqI0xDN+8Qj6fNsIxg15FQ2VJGL6sdw0KSg3NQCxdDePq+xsNvq+9J+MavEX/KRx8fzT
SRDK0/n3mJXQXB/oGfV8Gb+mcy/VYx87G3ygOycZxc0XALwD4jmaZtA+Fk5tJAKLeRPSo8OYa5x6
E+2A7yrqCP9Q4o0D8KlZM3HWhp2JEbd2Ww2wouqtTouRyAMbje6maxShrIA63Qd5qMEzGQHnehLj
aYp48M0YrTLMQNCPBos6NJjxXqaawUFjwfoe6kJmJki93iRxckJtsg2VOiKaBqc7sR7xEK0eX5ui
/aNphXT3TXt4y18lTsn0ek7wECGxNWWdIyNvClIH1BWwKq+09jUQGk4WSZ4gqflG8bGTVIC7rexN
TU0fOtcPOQ19NzXqMceU+iLtqhqoyBgISsYMFLcDcm6HE0IejOc9Fplduy1jJVBWigajX9CfGTP9
8REWzIamErmCjYY/DOEhWqYuC/TIYLqHJIS2lfOSxpk4gdsxVRHTGQZUGlMEoC3XGBVqSMrI6kfY
bCnbBdr0q2iuHuNQ25ybEyIBm22OgUQ0f9zz6NWz1OAbsw1d+fbMok6lt5zDa1z/t7nfUcMoCtjG
vByVq3qHc7O8or3FWdax2EPn7KYDT8pe4W64xX7n3NBXtVZpHFjD63D9gI007Ht+VorOBelOJdjW
41sAkSrXC03HkCbZuEp+ut69VZARzplQcYFCTbjh+aF6IDlQXji1u4rZX66pKASyt+ZDz5N5Ybw/
6SZegMhYZr6KRbkcT52cj1eAPmKRKzABYurnksQEn11HU8jSwvE9lbkYPhK9bnG5xOhV+1O90DXT
9+FqcVEewibeAcfVvxIFOKW+giRdkBPumBs4f+7ng3nvvzwlWDvlAQOgrsi9wCILY+EtUsAg491G
1b+K0TicH01LrfuaLv62ZpTxIfnXciqwA2pjVIr3IEJcevc80z3kzU8xmFaPHKBUC9wrBhiRBCBv
jW12CvbU3OXMpmyznXx7h6lhzB/zLyzqfI4RAUKm4IkgZucO5VTwLxaI7saMrgIAr9iA5k/nM0D5
bT9LKVwi7kb8LiB0EsxruL3/mY0UGxw2bZ0irVnz+CNTCnyAyXvIivUsMr/oXfp5NlQC/UlOwGi9
aAtlcFW5e4HSy+L89LqSIcc87CHvuzv2oJ31LrKSayRQvCjzRrPzP2LEO+5pYPjaffjlwV6nLVld
30IGQ97wP1y+973eMIucL9CGDwGtfv/d11KgMLRogv4rF+oJEUdrfyBt3lFPzRZRuKBa8lTGqIBm
aAAVrnatYI15KH3OPCfUTCwBNSNq8Y7FcbgXBSIFBurLrU7ZyDR9qphnWcq2FE1D4QpsiSNr6NYh
D5R+V65pUA7TSHYuVKqjF8cz9St+lYtYnf+mjoz1NejzrR/Ptp3Umh2KlDRAVW4izyQACwBXFsAc
lHejWeTYcNXk63WclCyVNBTMYBngwfZyOAORg0mWbXkE+cLj/9DUGbXkQpFOYkytJrcjd7GafQm+
LbGNJ1dwkLgfm1XOLn37AWJb5hKU3VXEwLQJuCpM+9ntobjxLB4gKmpfml4sLD+BnUCZTNA1Jjpg
JRptPYSjmc2WmbKdTnfWzLAinonfuVWDhj4laxAEMmz3J40kRBuk9pRqqmXy7DLL+T27sppYVA63
f0iOFXKxh9/TpY701B8aKzilhvhtEAH1vO+mb4a9aT085dQrveXcy3LvB+gbKbDyJ7O0XFxe4FdZ
ntVsuerj97gD6TQ8knZp6qOWWBSnTusHU6Dvj3LVL8XNkV+Yg5ewy8Le4vWR2CHIpj7nYuaVnnUF
1TwBnkTKbRZFdNXez+pbPhyzI0KN59ruPKL3Iim4fdFyYKdcEK1Juiwngz50nZdQyoXUFZHm+hOZ
0pLAFOv2pBFwQrSr12NVxiT3BouRKVGWTp2m9Rosb33H9pVPdfjq6dzaHM9mAaJLNfjDPvw8L3tN
TbnveYTIkjg88XU03Ue4yWJIWI/2dLmO+8r3GBW/Z41c/LJZ+Ix7yiB2Q5CP+KimuSmd4iPyhrhb
49ZAUQbmPUDgjn62do9QQYwRQNMfrg77ShxxtHMCXAmBdgTItqygvHUNj6w6CVvYE040tqoXgscc
JZZQ8i2UqhDmdTALGy/t/8v04jZ5lz0AsPyxtBCD+RJ159DilytOKFPgnvsynkVq6FHYHWHDeN4u
ancDKkR+oNZ5/oOBrpkBy/U7sYIoF8rhyhM/2KOH03JigM5t6VyflCmTJ0H1pFDxJbBNoPu2Q21f
C3poldq/zWSXFExc7uLZIvNO9+qqt2DmbnABWg0DgL9l+HqvK1uRAnV86kTSJhuSn5TsRI+hmnCv
Bb2veiEMglLRxKLBQY/tnbWZFrfWFbkGOSwH/kvNLGIiGUjXiCjnNVEkZQwTlljDwmkohAP/7ITP
uJchQfK1VllkzZNFgu5BDYjXmsfwWAaPQI7NhlwEi2YPO4SVwUNslhLWL9po6CDvHxadnCn3fYf1
NRkvWyge1ohEBDxYckeL6IuvIFiBHZ9VWOV+Oay30GSviWFt7MCCm8y0tqFuHjLRxoWpNEXdGvWU
Zh1uo9NyG4RQ6DN1nzj+FLpFvzFIVnNtjNCeZD3OEGmNihC6FgICBJidtfZZ4XAzMp2P42DBbG3H
boCOBhbPMiVHbHwJigA9lsjXsAnzxKtF7a9pMLE+NIjcmt3FziPaIlCOvxtn60NPEePlV9w6Ald8
FTFolSnV/THGQe80/peIqulwWPwrwOmaqM5+LczovzT2PZfM5M61dFXSCLf6VDCLywTYU899uLiC
3IUXb3VWnhGf9/t6OJ+V+FNB0m0xYGuv7EkMGpSBcxAypQMkQ5IBsgUAuQBJ5e0XorteAX4VH6Fl
2mk6qJjKYzSbK+iYAKwPA+4jGJo7ieT4bTSBFK71dLaSOH/OOwuQfqOE9d6Es+VTJj5Tubeiyjt2
J6oPVogHazp/OBdUSIdbCoOM9+Uso/mva4kDU9tqqHPLFF8gxqz6JISYlfpqLirH+S9Qg9AQCObU
0hgqtqsLcaBq40pv0beIbmFnM9Q596OcoqBw5K7RvSRahWFIek9hYQAsJtU9A25x7w276xC004f/
gZtddGpKhGboo5cjocI152D2kr6kIg3RBD+x6UF/mFZ/IdAYh9EXolBurAwhemQFsEFmJP0W8/mr
EREzrjPAp99Ftxif7a12QyeayZckHWfSzZXHuYwafTU5Zze5thYxkgRWgDdJYZfYOjj5h9IuNl7T
XwZd67eunYH+8asS5e2oNmwmZ93Rqozr+AwaJenEYwcATU7+p8z8Zlf8gqijkarGXCeTKF+UPBgj
YwJIHm/oprfVsuc7ahque0YQOMtJefK90qlk6i21HLC+Q1lSUqP3UUAZ0AiE/1KZjnwAUdVLO32E
Wj6OruwKtJVMdq8HoMHp9fFyAKYrY8vOmuX9M644b06W28gXFBCMsys0oryd/G4qMCb9nLwbl1LC
z+Nv4auNFOcFCfVFk4N3icRffpTfqrqw3TW4K4HgOCbmJgOPHV8uihyyRJqzNbkmfSyEf3j2U7QF
bO1Sg9odlPcCxT5fBwoxGMzwC22c13HBUtAMiSpSN1/uOH2qWyxIo6brWvY6tvDDBfdoYlFv2Z+8
sqc6umYO+3hm9OKPytCEu4GKTzAC3IZKGXkdThXLz1t140JzWW8dTpRPw4NWANHEfkkXMXJIeurq
e5dC8yIJE//dUS2Ggcwbcf4G3MH6ZQRIIyL2/Kmgjy47+XZzghtsAa/IZfZqV9T4omuAk5hpvQEz
CceU7ji3nrazb/G6QNdEOMJXo4RGuEN6fAakl6Vxbgsh0MN8BxUViXXqXtl23L8kFCXmqizXWrnH
kkkmmGlAKUwdKyrFCnkMsz13vQ4db328UXCaSeruyUzCDGDCzG/LPrnnO2WRNC6HC+OXv+O1k8N9
euQIp2nsSjR5VNe6Rg2FAlUUvxqZXkTqtJjP92bhk5KneMNqdbbtgQIwV55LBqAC776drT6JIJoA
TVEFxXWuJ+AANiDd3jJz72X/ZW7zu9n7XbHlW6ji2voF8t76dRFbyNNjXbPol9haxKH4DdoLr96D
VpteagPUZVLimZ8zs/4iiWdy/qPOtnraBUiQtI3B68LP1f5MUUfZPXrHPY8cKQCtHDK49iDJbgSJ
HDAvadtX6Dqoe0+5UJ9AZyebNW1ZvsXOUBhuagImD3BdliwnRGDBXFLmFAW3RZ9+XadSSrGxFafA
ThHu6eATBeSBAx8h6yWD+pbABZT7w/nmbk16VTiJoA77QjC3fqBejZdWzdkkrF70qmUI+0XQMf5Z
b7UicNUadVbGrzm+iiaD+THGm/gaf2UAoRdbDg+i3YNW2SWp6C6YcaPwxxTi3sN1FHCPMDYSELT1
LbodAKJmqnoXGwHGqYq1czCD8ntibApJdOpBn/sfoh4haW9FSR8glbkUynmuQKlaok5ba+Pj/Wov
JmvCnxTy5Wo4/LzqKBe2nei4zYfSezHlnY7pBGJjgTPBcEjF2p6pCr/XdPFsIlUklfP5U/hbhd97
crAc39tnOgCcDm5ul3dn/uStct9t9L5o0jBMONKA53fkTk2ddOfO1xnTIugp8DIYJnLJTI78PPRd
HiNdM0/bFA4MJP/tKio+YdI9GsOdyYZBh4omx76rvmvVkZbH7Dn1rOFTw340QZ00D4DfdrWwstce
pufjohz58V97x674u/dKpWlW1aYAddD6pK3LibILAJPtci4ht3RJtZ4/GLkcYctkI7wRWL0teNEz
eDFIaXArqndEJOiBu1jqPjzOVjTho0zIMRWWyXnCmJ1MBb6TJvGRyq4ZyM5QhX/qA5N5ZcxlaOSD
otsBN/PUykigXg4HAegzMXISdPSrUjmaJZ1FdXmFLUHaLZgTmI/vkuPCYhsYFlz0H/p53UT23PDf
va0980NN+WuaNZlCPY2c97mTXG6njv48JsDhBKJMGXjceHXHFJic1ap/XC9eJs5y9ntv1Ca/8esI
z6bjxOAxPCJhNlB8lZhwmW34xm5cN5/LuaWc3oYvINfTls0NNEfLYouHnncxoIC3a608E3s/PvZM
p/buu4UG3APw20MO54zGBXEcTFKoqaQ3cGZ2kXkuAwccS+Zy+EWMOyukZIAlWAicMl8fBDF6zuyE
7Fg5/ZTFzLHuYS8J6Dree2vWTvp0ihi0M6XTX/GRVDe3suOPDfyQML3YZ/NOFXEUfLiixPybBdE+
Phcaxx38VKJ9fwTxB3ugCdzZau1vU6DlYsvoFtfRVKHDHTanzyZSLS/22iXbsNq3sOJDeqiSD/I2
k8SGPV5wbaX3EBCOcE4HJktCIyP0wovpKNXbr2nsRhKhTn/hAhSLXYy9XKDlHPfvKrElqLts+iHx
689IhIGa3t5rUPZlzi3QVsWOnuEjc48v0DVyZnEcP3N60b2emb87ADIpo4uOF71dQircZDyoaPTR
Y7JzhsDlQKbLFWAW1d6ej8HJMA7wigwZgCls58i0l834dBtg+KzsEzfktzTVkI3HgXDjR03GSByW
gs1Z7xMlOanFTT0h6vavBUq/rOGoo9pzWBdr4Njwbp87y2Y+mtY15hRPJ0swvKkGE4tEZea2u3hN
Z5MTyPqtRWDGPM9HHOc+r+sJM4Mc+dCEiQFkgUx1xLeAb/DEQWvAhnKHFwLc9RPol3jCffbOXVab
KBXSejYF9SODIsb2J6vO2lIxMQUllp2BZVT9EZTBX+lXPOOBjXKH8oFyv3GzM8ZWoAvCG22ZtitL
iJAqs7iuIm/r78u/Sv5Hlyb9A+Gp7y8edlFOzZ9Jrd+jbXNlcA0HfN86ynafUiOvPzA307/3TAQl
hY9Fla+NwD8s+8vtgr/kMnt0R9YEj3LoykYjuU2+2JYFumuXLa2RKexQ8ICaeGMHp5kcbLe9e0Mh
cgfqklXfVkVL7bUIaC0FT7QC3f+YbSWe0FhtHMkZDQXTsKTvURc86gE6HHpl/JqOBGKkCmjxk4WA
f9xnurcDi+ymkN87TJ5zOckCq7UffU972IlkNR3J/R3qIIDJpc5o7Ub8mKhfAF9+31kZAOPbHz6n
IbPezq6aUr8xhg668UOdwiwQ2EwWEJ8/AaFwUt0IT+UExiVRhohgO8oUoCH3nFtVttytkdRhoCK5
30T25T0ISkCSKzupsWxjNA47KK8amq2waqyULycxtNK6YqwMZAOWt3uS4qRavqm/TWtWlhpbT1Wq
0Tx6Gxvi0WRElGf0VSNfM/yG4siveorJW8OPxrUDAJiDipJo7Jc58vCETtaMhVly+YgBExtqm07l
kfED1fE1SLvudYBI5QcZObTC6PLHWfyhycTA/t/HQqIyea4jpbtKIqDmSlVCJ/eQBm53e76/hi6y
15tM4hUAPrHBLEZJKJXkmK362Z7T+rX03Nat3ePylM1OXQtVIuRmDttYDD3Ifb2YWe7enKnMN6IE
oIgSn1RM5Qtlx62J+3yYiGa2vDe/kQZpKqhJO3Ye4T9VbDEsyR6WPgOxTHygcnyt+xB7y82uYNhs
1wLGRWzTvS1EvgqECJhRPAfGS611oP0ztBnWA7hLJ1U+BhCcMFFn6x10z1daAw6+RKUbvXFtE0ZD
ILKZAg+UPWmzlfWn9L7ni5keRcHO9U/INaxBt3jaIwHK0vyBQNEPnW07eCvwMMTsTWNPrSPF7u2V
jMtlbEyvYRCqXO7nwBeFuSroywnsi1crY4Jsr9T56SSLg7E2jKaNrgwt6+XY9/tWi/W0i1TOA75W
dy6JUgpo3ocr2rvL64euXrW13KmZjdYKPaTNqhT2PODF1F51YF44awAv3E9xI0OkFp4RK2awmPcW
OqP8RZ1fnA5xLg2U8a2CBnqeAuJs1FSaDwMxNO267gtSO2xd1igSpYaA0XsHJmj1fEbhDjFYrYXh
DH8oQVQVtLKWIMvSsbdQUKyrp+4VbIIqfxU2/XlhCfmHn+fgLH4uy77KqqD+hKZLU43dN6aKe2AP
w0DRWrSps6maC5xi6lIO7SGKF1Bgm6qHUiPKQqNKX63nUIZk7e1nWYBudildfiHFMhQlpf253HpB
kQos/u2g2eR8sTdOep902j5dcd1IuKFk1aVMOHawgjsytTa1o315CdQum/jO54jZ6acLtdT8/eBd
82RSeZwb2Xu36DC+n9oSu9CoDvye8KmVAB9rHjm7keVx5Zg2MpkkpxvDHSYf8GiO0m7GVUqLrawe
yeoRi0MBMLFM3WnNQfJ+5v5e1oRlhjn2C9rpDcXUcVa2brflz4aJFChnp8Tf8X5A8H1+oU40cl5M
xajFHQQxOJjalGrBnFBLhEDTQScE+j8hzIPq90rj2bYFWRRLS1wiI8wT8/aOuiwsZm/vggya49yg
OzTMGFwpU6LAmvZLirA4a8up0YnmcuIKMu7CFcz3dcRF+HYqcw1OSuDNGuZaXLHc6Y7uqtDPCrEX
hU3ftmI2DsaKAUlA4eRakmE33v+PASSrCLwYIgqhCX6T7vQ7/zEuEoJ8t+hHMsvBVrMK3oVoREZx
UR2oK7LYuEbJmkujVDP4nC75AFHe8zxoyLTeL6r/Mn36K9AnbuMz1qGrmqT1DuZwnQuRTOTcCO2/
Co2Zqoa6WSycIiJx4a+o+ZuPZCG4EJIsuRvUX6SE8Sarl6rInCAvCMQPPVSj27+yT4EdiObY3J8A
3LxYbVI3s6L+SGCOCyqLsJQ6kMaFTGxtEzJ1bLP+I9Y/kyAqTctA2aR5HRze1LMJ8/xva+0VuzJA
7X00XOWUeZPeimTdmFax+I9g0Vm6tJ9QUtOZ92YWo0B1vvzxLJjFuG6wFaokAcbL7a5Hx8+6/pOX
3d/f2Oig73HmtgJ/CSwi7Hf9L4ahSind3WeAlX6DaX/VNQYFctFxvui7kBn5VonUZB5VWxk1jU37
gbMh4vuQ29jcEpxrKHu8IvMqic3iI8WLuh0WP3wBG2mJThC5lUBufy7iBo9xZYFqttiapndgMacJ
IJzvcExE8OXmb/CLqjVnVbRm3QLUVZcXHRpMfumyJRYpyu/zjFN3LiUOVAhYywoLxhI3lRTUSZxM
/J4S0SBewJGF1ArEjdCfYp10aqURLzi1qXVe5GzcbmWp+PfcXxR/jP2Nq7hYnoHgoItTqzgaF8T4
CSItzdoCdt/NCLboWAOOfsv7yNIvXdHj07FCifkouGcaY1m2Dj4tW83SayJpofpIYiA2t+wu41EN
6uy3ssss30BMJtYLavyYGGXLla2IvOxA78gQAIMJwDaawuS0aSZwahfRvVv5r2xVijKJ+SxwwVyj
xtuDnk3zImOb0RI4fuZ7LJqsCm5CYjRheMwt9A1gtsXUIjnzgxKD6r0jh5mFsvrTIo94rNNWZjOx
AmxHIzwgcDmt71b8u+lrVPcqpLf0AabKBjbnf2h2Fo7KO2axzfH7E6Fll1vvRI+9CCXtFxhcNB0Z
kpNeTk+2rdieMV+kEXoxUoj8xq1MluTrhwammjCw1HY5Ys1vWv5gw0EL86OcrmlFHLVjU2HKNlK+
niTKZcxvdhWrOAmky2ZwK9tM5SxV/dGzl87IyCV4mRngFXiUlH5dfhHmMOpg1vgTqV1t+Lggcdvu
SyD0UHsB4adxsB9pTRuqn3UM7eu+E1mmELS3kut89y32or6tqr5KKiNp7QoO9QZyot2OXT/bT/G0
2YBiKIgs83uuYL0ddpsqD0U/YIdrE4mTk05ConwNm58PjbbtuwpxlEjp/VmhjC2eWL3BsCTpag+w
01b+MeqWRE7d52fxhX+sDk9hMb4+gU6CPJ1OkdZq2pxbvY32coIcNAGzAKIIv3ZpSXxOOzU2eyos
D2jY9UEgAXgRdM4fUYB/xDbPqYUmehWAZYypO6ILlvVgCjgSG80Yh40WI0tNWm4MCtEH0Qlkyuuo
yJPkWh2/139j8KlNR+geVeayUP5OB8ykVKq/VUb7u+wH+nAMwndV/30zDvuvOWRgO5qT3DBMc1eo
3wXlyFrV6yHndEk6PoGplq3qhzJ458K/AV20dfPlXQzj691kNXetvhH/+tWoYaDTBZw41J2p9KWc
kOEfVNsCRo0H7NXRcusxT36ryF8o1da2FRazzrUUdgTw5JOrujAo2nOuSMu87Y4QZm7hc54dEvG7
BYmbqyOWgLWy+XdKjk0aYRvpXIclQ55ESGdW9wXyrD3HKGtch8WCXKR0cIXSCKF1o8AUUOIgGfJP
A2cwaPlc9bc0BYjK683Rzd1oxAmgvi24Yj9NdZKyxqOVUU0cC09br/2S0Uh5f0pgs1EidaEMRvBT
S/mP/qDQepHMDnnuFIPyurb4iBjTTghdTyagLY7X3wC2HTjkCao1A4HtZ1YOCIEJV20Bo63A/PrU
/uSFusVgFgRsm/CkBsuZPZKbF2OqZt68VhCodru5lmW5sPiBYmKk6LPvUNK+vB8S6Jq22Jta0Zpv
1BNIxPv+WkbN6XuYEqDTxw0dSy2pL5NupTTDx73Pi204RhOGHH4kmgnKp3dP0fdnA4W7OfDOssa8
73rL6z/CXILjQ2HQJeDZbSLugdQYo8/VchOgUAmCP/bJrTlx4f4ys9JLlptITgh6KhLGrqqpV4Zk
97wS02sbKE15N992MAp0yDsLBCjvMSmIgb9Ei/AXoE5KEOQcciTq8ZzU5vYMg5YEeR3tbTq726eI
PKktJXrORvsde9tMZHPVMyv1CQZTEGc4JjsrcXnk0lvSGxFkWMWfR4R1plxyqXuLWZBKWNCaeGoI
l3ctsDRU3eVsLvEDiA2Nrbuzuy4L/LAC11nIMscFa4OdxQuUAnlj/CuLypKqG6K5lNZquEx8BFVV
uPowPa20ZHSHNzLrrjTX+aJ08fY73f9hm86f155VqdEQryxECpYao3FpLHJQdGGK2LieE7wVLXut
jhF4rhtlIRNy60fd1wmms54S7dSKDYEgdsurnB55yMKMSaYNxFcYDnlPM4xvGMAzvtDztukTWO3x
Gwmkonz3dvt5nOsM14msBkZsdBecZCWmi6tA5BoRvw3/bNUkQEgpd6Qw2Mf3hxr/nfJ5y1FYagKP
t5uI3r5+Uy3FrJFUMkgkLMthqz8oodk4Cbe3c3R5GqVBkRwLnakvgRjJu4r9nMhtwkyBjbqogV90
1yulDeJ60T5+tElSglYkpC77JqtBiO0dcA8s0HOMHCZecn+w1Ixgi516mebR2r0reUUk12qnTU37
1Z7hu0r7rbuMjAX2SbnppyBcXHFDkHuhpKAnKwQFUYV1perZ0qZxs+L3T9eZ1ntRN6raJ+ES7uBt
04lfYgMMlSiXoh7t9LdcKbmacJopaQTlqcKcFOERApRLGNcOCEh82xvQTpz5k1yeO3FNbji3PUT7
9JxLAVrTWPWDsp7+tS0Lh6HljhckMPrgJArOgx5jmC4lP3a8od4eD8g8GWoS0x2TY0Iz84cYRnF3
nMhNYR27jmvtBtnI731yazEIsVbiHkOCkQGC3GzPmWMbkvFWLpExotDwJXZKsBEdb6z2O7xgiEtL
WZHFt/pTTPtmugown1yXc6WZI9n0Cb+PWihNgfRh1Da/23AMFcDo5f5X6LHCuoIOzHTT53l9MHyK
pSRNmhg90qyc2ETqJjfL6r8PEYb6bzjONcPVYz8NGjw14zOWzD5N2BwG0ZRXwNupWlE0eFBzVr1C
IvS0oid4JKRhRhbK0VI+VCOlByUpIemg45Tm09HzLT2/U3qLCn0lf58B5LcysUoOr3NzC+6low0u
Si4sovRJa2abJ/8jp8KqZC68laxg/mhMujMnfYARGO7MkVZMeK50mov6BzkIbIsYQjJ5UWUZ/OgW
K3G+Xho+eWWef3YLp+hkYtFH13SzoJN0cF1Y2k9XrUl0lBjV7C143/rNk3FX24acKw8uklRKMUdL
qnG6CUW6VofN1fAzn7L/s8swNAhfguYvrb0DrUNvk8UcsOQIMfbpJ6SHsDo9rgZc5b5fBkgrXTs5
nx/SM5ObSUI77PHDoceKyUmk6cP/E4vgkREt5gEx/kqNPLTZ/LijYAD77CntF89I76JMubDncZ4Z
sjX81Sj6gwcwYDxl8pDadGZaBck6KAMcn1eCmRaywkHudFk17igRMxaiEavoEh+70OKpIhoy3PvQ
RXiCMSKOps+ncpo6qxwesNAHiQM46N6BfC1KuIn8S2+EOzWJSF06m7lUpWI5iTo2Z+SFnqsJDbsA
uQhEvqD8ZVBHICCvH05rAFfdJPheuHEV8roG73VnYK6mJB9wxmm9CXMCgvC0jbO18ScfA0855sZs
sf7zs0BpPXE4eLW6IHUk/4h4ZFhmRep9+XgtO/Q9FItvTJ+9v+zBQH49qYQog2pMUomz6DLRZ1/0
MRk+dzTVEX7dGHxrhTU/WiJxvFCezWVa2/I58b2shn0YHKA9wMwXYYOhzAdS0l5Iw1gn/YfJhQA1
9VQiNIs7A8/UYiyItjSSU3LrAbygEbygJquuHvFwUeS2BFfmzT3Z/2on7suhtvCDJgtvvXmj/Ry9
EMUsNqy74UEDZShTvhumSAPzp6zW+5WKWu9xye7MUkxlZKSa3a52H51lK2nPmFeRVIcV/wm4PfHN
cL0qNyQSxM39r64Yn1BIheAPJpt2AzA8B5kVGV5el3tOqvg1MMkRmv3kCu7111XhQ1IpMpel3Npg
3FzV9BUSWCmQ+kc4+Eh8leX8EuxEUJ1oGv+Dy0iXQDLYUb3/f9ksJcl77ME9a4+ytHxxMddtpl7a
9OrVQDGpkSQEmXWFGuc2uPRS2lCsAjaDRVRtEqLNxyKbXAqD4BeRCT3BRDquQGx/cmIetJ/u8AlP
B/hrT8vqeqNiuCfRsqP0QUiYewgPVn93GZWc6Ux/myro2WXFMYC8I08f3JhaibsZ4MYoAtQ2QURv
/FiSXgM0vijsmyJkaVM4G72RMPVUcKiNw25TYON2jx3W90Z5QA8duKCyxHDOoTAK+C8JUXD68YGj
5ywoP8tsBUzph3z/4hc+RugYUPepMhAf8l25Mo7ybpNCaHwnwTdYlYm686N871R7DNFm+Ncq2Avb
ipO+xsPrd6aSpfQCCgXKE1VFl3wnir/kBFsMXUySvKdeX4cqaNvdLHv6btuQ1nn3+D0r7ivoDMHZ
lyc+CPslJQC0RuyzQdlI0h8gRBzNLrUugRqO0aWcjkL5RsEBVdj+BxwD4Ijn2//6RBSEE65XvdJ2
YDDKG3SGxzhP/G7Oh+AFSaX2TURJ33NtkPVjb+fITW/7RD6DtrJKy708y+ziYQ6oA5gk8mb/wO+T
/+uYmm+uMEdtLKrcBxl4O9h8dkZ15hInvBP7SG7dYhOqdMtF8Ulw+wnp0w8XJG+xUpN1bdiY/mHk
JS+a9yhc91Dq1p4IjBqKsh2UFR9/LVWsHpSDuAkC+iemWUZvpcd04o41h4gMdGlwF6jCNd7GMGNg
EiV9gxdj8ieR84SuuJvaH9+HZoujh9GNNPcSSm8VTBHpeu24aewx+05DXsPMuoUDPJSH+S8/7/Nr
oSsNJCu5b9DlUI8rgu/F2Z2arhov9gi9RlLiW7KIE6GCNPnocTPdDP7NGdo3wuTQbKARVDCy7P8E
KdqiM+KKJWv07seIVtEK1evAoCkW3nYnngDsMOCIQBXLyvQ/Pw7/NJhYnA/pCY1Udv2OTdRYxH4z
wdGCYSdLugNgvnBlQod4e5OOdaNECzU+umXjQeonXGxqisx+KV2c0g5+/VOR9oLnAQz8Fb6c+uFI
sUY+omOthtfVr+UAbONuqRyuMleEoc/eXSCqalpX+XgbvIVVu12B+/kV0dW4f8GxG7as4EemEzwm
va8E573Lr6/ZJBL8jxiYXArCoVHyJOsL0uFcMA4HMW6kwxVDsXRBqLr8jA/zvtAva10wWcgPZ1Zk
4HNIIpgfJtYc9whvAeD8t64z0HQRyGHk7SlJlxI+ZGtwUlze6NhMWJmEPfCDUe0te9FGKIEabwwy
/JSO3HzNaRwv0rMeEAxS3ndN7fV1jEmt/B+jVy6wuvZTn/xtVH/KUNQsohsExA6W+sQrRP8j4YuQ
SWg+W91e8jnI1ZQDwlOZug2l0QGNeeb7aONc2om913n5U18JxtkUT4PE8hK6aUwEMp1n9g6ZuA4H
XZBkr82uGFt3UO98Lnnoc2/27iX1coneFq3gdGZSYen6ZW3ugWXxbgiIlmNUuLzK/4i97vFch2Rn
TFTQFRHIkI/ppR+yZYIZDbhHD/qGAqHdAICdSRRIokSdv6tbJ1bx13qwIaTbtz6lYsEn8FDSr5JR
gXgYs7yp1nuTzO1wOnJ4iP39E40mL8MFgAdKZLhG5huSVe3Md723nxpTr7mZGe0aC5a2vCYUMYCX
p0An2peftClA71QxdBhEDD6Yl4lw/6nLlztQFRhHbApeq4LvjSHDmdI3u8b5gwxKuddTHPWhU5pQ
hJ0I2jkVYKFoMc2CYQpy1VhJEUMBKu9nuRt+r0SRfNWeBFOOCsfbJ4FqOWkFL5tlKl5xXyDNsFsD
hRxDkhCE4pbAw2jUWsH9jqoT0hTOV06w4pDZ0t2vzJHw3gJCFheqcdTq9tIqWydB/BjyD6wKdXex
2LrdByHlqsRS6MkYO6fHEHYFdXHS6I5RxhnPpHmocoRlFWBELWMzu2fab7plyTLfKILJLv5X53VL
455KSvbu9SzxMbA1j1GXPdl241zFbQp+9PSwqnBewI+1O9QN8JsF6BBwToiLqgcbBD34BBLjX3RT
Akab3HvvdRtX0iYAL2FnAnCdGqkMYrAAK1e4pwWHfrilbk8t5lBRhRExA8mizteYQX+P6ezDDrED
v37Ncf9PQ5WqoOAKFbZJ8p6lDqftkIVke9GWdI6kNh1iG+517ritRnON21KFsjUmuVDfa63k8E9S
GW3LvEIgRGw36eO62QsEf3TdZVOugAAuqDYNkHPcIvoiQxG9xiQ3RTrfABrhubZYJSvHuda3SXc8
X2gYUBwi0dnwvyOpLolRpJfoCvjxPhGivfvXkLRyuzjG6vTHnergJ7q8rQ0QW3viGd5qb4TwYgPh
rGbb/UrmYDktEXemRYCbF886h79zceMUPHQAUGnWQYyiP3RoEGt4ebBlVanNA2oAg+U+k/I5kjRv
4SMfTTeAJw6Oc8NDt7RtChyb8Km7wWqQasup3wTWfLaLVJ/sbkHPEpWzCOURnMMNi46r1FuB6c7u
rYtkSev2jOH8vwzzAa1eYFSCd+tRMeSjY5cPydKW+NWPP0uBI5vQlZfcubojG7GCKwHddVRST9DT
35s3IECPn5HkZjQevPxVkwhjofZhtZPOLhXFgTst1jCE/54gNZuu7Me+SSDHZ0TuwXJtgOi0Wodn
nfVbdVbHMy+Mab36yNi/enE35aQUHurcGQXI0AdlwFYnjU399fhcapglQqpfOsN5cTkFBLpJXqcG
EfLXSp3cNm7fjUnwIvM7fKNIw8Z7MEWYNGSjJptXXq6/pbJkxUT7ZP2UVG3v3b+l+P+vmMcBp0J3
uQ8OG3UkMncl7WorHuYqslXeEgl/UDmjbbILvxwB3THVu41Gdr1aJDyReuhrwb8AV2V9gnvFsi8o
o/2Oi0CCz5nkNl7Jq7lDLUBX9UeBInj1iAMI1JpqecJFW7RiavZbMfiRMGKH5HdZ8CsX7jIsuGi2
U8JS6aAgVJj+dovp4h38PJSfiUBsqQmz6Bp9TOZoIkG9qJA0Uw2RKFL156FNngmwQDFZJsFeHFwO
CvE8ytM4bLvNwNG/ubajL476pFlXaZfdFR7uLBuXCgt9EQMQnLSoBjnUexcYoBBFsQQSOC4hFJ5W
AvQzymGi7i33/HcXUxBMPA4ayTo/btkx2mkV2i+T5G1hMZwUFBX7SBtyDvrjjOHqRuzTsCIUn4VA
i0ChcjGdV7RfDfSiO1kSX74XtAf5OWZ3KnFsK/s2y1pGZuKkHh+iRSPGTs33FefHFK3TY1ImcBP2
WJZi892YQ4QvfCSQC5H4z7RnjWBAu7UJQlfEFXC8GSctJOweZ9lMeNfmw0sObWeEn8M2MxtB9fKt
wzI1tRZNdA/p4N2+mobnZVN1iaOmnm7vHdkMVQPeI/nrj6zuHndbiBoIdl3Fa00MI6NcIrwyZGSF
dpKL8/uxxqi6q9QBDA20vPM/SnatchBEBNMJyy7yfI6Mu3xJ2/fY+1oCLytp8oBFRGdgYwdaJZuH
u+Yc2robj4Y1RmFpsp89x23oDhaT/9x21pqIcnkWq/9l0E/jS4sU8aSH20Pium9eP1MkVTRxeZhb
JB0/k2LVqJd7XwqAwAPOEwdgunUQI7Z5Hd91Y718Z1LhPuGdLI1BJqqajJguwO3HeAvYuXUGXjoi
rdf8MAL9GkTjVjUK9Q9pucYJndWTXQAuFIDxsS8p9uG/6aWLOBNP/3heXWyqgpHy+d8fEaSc4TMA
CVkUFF5LmQC+sJoXwBycGKGdzPNOdwvoQDPdj3HYHSw7JHS5ifmlIVuIPBLykU1euVqu5W6ehfMe
m8z8LsvkzA+yW6uw9R4VxdsGu4ey646qP0+UoNp67rY9bwYkZA0WWJdu30ITtGyM7hlKP+FZJCeC
SkRM0wZgcSsjeMpWjFr0GjA9VW7vyLVYBpHAOZX55/Ek2bOz7pO5s68zYK+1VferCRB6BZrmxYSW
i/KQ+pPJLgRXF5pZjbwhS9Hu2avKHiIHRuG+aEUbMXdtoI8YYXIaGW9yBkQbFIaAQieI0WgPlIsE
k9p4aCxSPBs2VI+OSrZXeEI5Mr1WkZ7V0X7iRabAzuxUShokqV+Vapfv7RIq4qlCZKgBV90JDf/4
H7W80L02whBeLql8HTQp69aDl4O8sB2S+C0CdaP8UKm9DztDnFkafYEE4gpni43i0AtOrlbVAsH6
MQkvYFncjBsZdrSOlxDRon+mIMdwZqBaXZojKcXVCNfoEL9LkFjHLQy8h20Wmng4xjnLCvYRaGmx
JsK7W28487GPm+Dl/cy4trOVFeRPtxo7VPtvJUwIms//g3c5DgwY3bdQBF9xVUXSGvSR/sxWqPNh
iu474svfE3MQ7RKSmwoFjxNsmrfGo3fPTcxvT6fj2e2rbC4JQ/q0ILklwWFLXaYL1D4C2/THNyiJ
9sr9wCtBvTAK4yXQc6XRhAIw3OuAKXHOZTMRVBp4VN/au2M9fA5GSNaETKc2/OcRdaTd7urwqrWf
TG/ttFQ94GiKUCvjlCbfB9ElN3bWfZ0PLCBXNGbs2y58Ukg4FE6ucwPqSfVVLGoUri2RHlFBeGtq
H4SCR4E44BrcLmR0GTHcoos+nj/jV7s1U1igrDXmnA2zgZeDILbazcVnYzGfFg5yzJZBjs54oH0M
ye57xe6S3bniOHkhUfdv/EboFIJ28vC/PxI8xXZd3ZpaP54LUn7ZXPZkVIIkR/I6vN5PkINRZ7hV
XPWGeye7ycfdOeKrJSx3cmgEQXKtnIKSPg3m2EGWu+vy0sUhGQtnSEU13CSeaOwR/9xlbDTA4tpn
P5Zr9Ued3Fh+obakJGU6MaRpjrZorvy2kuZ2y6sTDGgR0+IxInyFkM3GvVZNpmUKjAyGYJwL551c
hPbs8SDre49+uHgS7ML5XF8TqcBoOp8ooPf/Arcig8EG7uB8sS4FhhX3QKqgI2uvVvSRL815Ih/D
pbd2sLpXLddn/iwMQZBpTuAnvw837FMc5J4IOckAnkGfwAOsqjuURq5eLbaf3h06zZNb7yPSCqWC
Xfj7yDt6P6YPQJ6I11VMwYQFKw3REAh9kU7scnvcD02tAhpe18J6akFpDvQpygS+K1SmZ5O6kewm
AZevAzHvXSd+eVOKVxZErmRUetB9mfIfjZQ+fTxJQ6x1RKHNWTj8130vavxv82ACoAU2aIbXXqgX
BBGWjPDletmb2IvysN5ZzW+3h+vq00+HzZxykK0boBKxD/s0DWSdPbAseYf1/Aj5UstANscrVEK6
ZQoPz8gA4Iu0z/oand+jUAmpcjfBB2EmzEYBfmo58pwmZ2rtyNUHluyLoUKyEBxHGgspa/tH9C+k
WMxpD3hZ85+uQiFM6SHwNt6hfQM7d5kn3Yaqfm6ttkqu+kwnLk7n2ArhBVG0WZoX86dHwgKsU1mQ
AQ+5zOMU02pz/boBQ+9YPNdykDqfQOXhLZkuc43NXEp8tTnddXoqY4BD6/ru6bNNdggTFUnwy6NZ
YmTYYieHv58zyiaK52TCg1K76J2aYeVJ9OEHNIQpp8HlGc38APDbuBinZKywWRTnRdjAyAsTRjqC
96+L3UrIYZAOhad17NJcd6oSK2xunVWF6KHQ+OhV1x2sf4xrGqfMF1hNTSM5VjyPHGe5Tf7FlRDe
TD1w8w+oBGNY7JHssnpmHEAGvaxFHIxbh/LmMEsWYsM+HMCofj2wZDxKsFQpU+OcJYCq77rhzNqs
G4Oz0kNLsSgjIDUvUMD+x8f+s9ZwVO8+FvEmAwnA/ClTtUAmI+mkeRmxWdImx2OZSEpJrsBec/+Z
1gQbu58eg8kV905ze91132mEoEZLpULnihOHvyUQET6KqtHGtbGg8hZ/QOvxO56jGmJ2fkyd6E39
oZY/udhgnruq//4YbYhO04pFiilVGlQ6Aeg6rFASY060hpi0eENc7tdvekVhMDgXsX/rCwVkR1lt
OyDY8cdwSFGD5MWb/eNFK//l/LdnmdF85HPRWx4Sb90V3625BOPV1zwOUCpgrmfm3FjetGlGdhq5
eC3BrtrYWdEg4obcjyHj3ALNKLq7fHfJWuTuCTabVgg/s+DzZHXNaW8q4+dLZVAH45pnJsdvArGp
OFFp+elfmD+3ekvUFLlVy03eQjuyTMrZaEhH7Z4jrjbBKEUP4o/ylXBV/gsxV/wVJA5ABN1OYFdP
D3pN2Bt/6+g9sqW5eKE6Pr7DTaLBEpWgqTm3T9NBxZhAJcj//c+/8hU+7VFvfb/hENUyCf0Y1oX8
IMKjat4w9w/UeL9Up7cco8YOZ+HD+XFGGRktSP5aO/KNPBWoPdLAaFTIr03A1GLvIq+h8sw1zzZ4
eAwwSNFO0V8agBfNZFwuPOghxvt063k71wXKWjatcNhEIIT2c18C5vMzBIECa0N1bHw6UHYislYC
7TUATH1OYetreI7eHfj0que9XK9gVvH8C/C0xgZokA9hv/zPlEtm4oMxGdZNiaLqPj7gExsMEPKb
pml3CIcgiepoNpN8hRS+Cz4/pwxHnxzQvotpWPd5V7Kdg0DYa0L01kjGMEjcufaQHyAJgYdJ7YmK
YB5n9XkHy8rhvCvRB0wdymxdbqF1u+5d9Sby8nO0pRLAeT27Yro4cxb/UEJca2+HQZjVMnJxM1SX
7QjewvCnAtPPUQ2LrmI9+KzDmDffmGLv6V/eTFy+S5MJC1JgRRkYVU6vkzUGx2UZA/WSiDqTgTns
Fx4y2Ij20CNqgYM6NjhWOSLlmgsfzU/M8eyGLDH7r8ppMNiqiRjivkbM1jlAbmo0ZK7x7fMeZSzG
yUHGT6J/766foJbwxZvxFTv9KdeTFDqT2/P8v3cx1f1RWoGH8lU0MDvDKZTmXg5hKjyltgZxb+II
Uk60As391KGzrOHobiTiN3bCHbWmQ7IPzbB9U5MZu6KHzZtjXIOZXeaQO5jZCnkmp2/MEFBWip+g
cXos9ZnVT3w1Qhza43w/UGODsMNOPMudv32KvB1hMQWiXpmnwyjbBcdwdUL5Ez8+fMKgfvYC5k0J
InKJhzFCLMTcfe3I+t7K+0CepziWHhTC+nU/8eKu7LG37GSUVBPCK+WYRxICUzf+m69qIOVXcGiX
+7YFXlyoFkg8/rdxAxrgT7lSfYAFpcquy3nVoCAmjABGU9zRS9TSl3GXCb1hHX76DOtlUuqgjTbu
wQiESnl2eQ8cf9M1Pszbq7O2FBLa2oc8+xCdA9tB8oL5cnXJvCWeLPBLWO2izKGaMyo8Zkh2cyr2
kbHZiEV4CCpRtWuRlWLqs6+EDAoWi2MZgvVEAa78cq0UEbwJaJLSo5yiJhFagp03gw4v2SSTC3in
6qYFACsTwLOAKskv8HeejCKIaXxW9EuVnrZ6JWn71j3IgEWT8MlMIhTg/9fLRCSD5AED6VFHcTkE
y3xP8vHk9680yMyxCgqAwwGy0M1eTXA4+rP2au7F6pftg/Wf7uZIVzZEbzbaph/a5CEINpQiwwrF
bSk2fEYqsigexm+n3O+lt8QCNZrQIYB8idf8McKZ6C8LGq9N/dHcguv4J7ptXmPUyO/vWXIluJ6q
rksvUXkhoz5LwG7dw8uBJkceFQ9+wOWg7AXUQVmiTKP8wNV2VwVFxp2WvftQFcJ67ajaEleKLzIn
YrOpQuUBcrbdQiLicVlljG4DUMmSvP58TifVu6QUfPxANaT6TGZ6po11LQ63h6ROa3p9j3f5UaHO
b1Lwb3Arz+yN2VE95YbYztVx2iQdGv1q9XDDFow5fWpOOXopDKSzhTvuLNH+Bz9fYj9RcSDJSf6O
I/BZK5ChVkX71P8VI3yZNmeqvOZrm+87DokAaMV4qbyI4NfYyd949JgA6fIfw/frM+azIfJxjatY
ecjPsIDF7KvNDhWBWQ4IcL8ep5axiliuNUbsZwzJYA95T45MICyGalBappiPBLQfhDi7L/kpdhCY
up+ivC2qzPZ9vHw680gSKzecKRGOopzVBpVKUhjJvFfgbwz7LKErTNHrc22mxDpfiWw27ZxgK9lf
OJDAjaWon2LqHduazNjqFTrnr+w9mcfcHwAZjHTCQVaxYrEMNYa8c42+4NBcZOtA+GS6ifUUbQth
X5pqtvfvB4dNXLyMxDU7WDlOOfMdToRcJ9iQaoiVmRGOqhmJzoxqJX7Ezx7UL75cirCmBKqTnhka
2/JaZRHG9+g2D0Onu6T3dYfGS98J734eMuGJqR/JPTzHeIJ44tLVF5ZpH8hoeOfl5hOR6YPF7Hge
zc1PdyYMUlTviY4LtyoaW76RVFNCurdsL60m6URDKj9f6ewrrqlkGchdX2s54OSmkQVeB8wRrCFe
5lcThR1OoX50zIjzcnbtR7OLOcJT8EH9bZLKR7LSG9ANrQmXdY9Jag+LMED9FFBXxbYvIBsmo9V+
HZ0VIYqq0fvsjY3y0kFTDKDCXBKVetJxAIhEIZZ1Rpa6jHegmfM7jHquYnh5D+pkTzNz8xzoTPYy
2LNn/pMsDv04ZRSsJJZYFywa+zohDDG6+N4hGWXJxAcdI9LhITr4aEnXEh52ywMQKN2qR2wWp9wp
Qrv3xYV1VaHh6PnpfuwY9+cnk2P6OJZ1cvfDuvxndaOnQ65axBNZoGCAiGYhbjpYp4B4SufdmDst
QVy846fQgxkk47dRLo7MhZdtfAEghAocQ87hzdbCFwsCo0uhszNwKdFxmpd9nEDtr6g1sx+xsywL
GP72cxl2WgzCnPcAO8lPb+y9hA6dv4wSQS2YMY/IFNIuBIf28YkLwvoIP70d2gTKW0E2OpWlNgbM
Ejdb9p3RCchrJnJinloWtnnppdwAbFD5hC8kLRM4HPrK2RtwoFU7u0y4kOYK4nJgYTHT/HArT5qo
2qD8XBKZEGel/DcFCC/oiRg+GwTaKbWsElMesM04Ye4fRLvFHmUHwQwDKCrrfNCHtM29tA2Tglld
ImA0Tm63xQaaHNSjKsVmv6EPGcQzkjLYKOHwby8Jyat6jFnnmCV0r0ZQapBx6g4pFrMjS/b5MpmP
butXObVwhp63vnPqi6jJvoyueT5Eth2Zwy/wDAFmqxYRLsDUV9+FMPdqmGgrSRt3006giWopWwVB
eUXoTRDxIS1a8Nq6g1ORKT/qf75n9Vn5P6cK4yhaWhcoJ7MQhlXaXrgkwbB2H0j2DrSRfRpZ3Un4
og5ib3LGOrAjEskY/qJiYZjTYCm+6ap9ZT22x71hxagawnT2H3Ew/0YLALK0/pFTwpmiUWH26NLq
xISS34sbc9gmpFCEHcbtZCOszGe/rVNjvq+UrEPL3uyiQA0BE9Q77be6U6BsUOTBeFaqoepCTXVA
asuvkpj8dGqRH5jQJ8gg6KexAVElqqZONDwcZbESeFCNvn5RLEH+FI42HAqBQ93+zy1H+WdURAJ8
18P/RfXCOqFNoTG5NsXM8U5hEbEt3loWLhwYFySIksH6wx1sSAJUGABKUd75mNVAzFXPznNUfj9K
smBRiR+vnUPuyTirDAjnJInkv/m+lSPqATj6r3sSAqKAVgG0A/9YWqa6pRpBAah4JLBNzrpBKxWJ
mfW7ZLK94XT97+BfpHJ6wELJwmKewV3+qoxaUpwe5KoWKo7CYhQBklID/gPtBxPnQ6PcGshXURPM
MRAyFq7YylJX7lFcvGDprWis8/cs8almLbVGTA4lgLXw4S+eT+/8JyrgAVB9Binv2nei1ifGXIj8
Hl8pVez94oVlyXfnf8gkRhvYoo+5zCiTDb3IezhC2kOghaCJvJMPnMYkNVSNZFAjE0PhsD1ZhkNy
KbufbXhoT+tVJrz2eJwYEMvKgF/BJAqHRHU8RoMDHjezA+gEDi734VOq5bB03r3es91O+pnp9Fgg
/Ekuneydr/szxUpgT3tPGXka85UR332OPax2YlV/cZ+hIxqSzW5LbpfFfV7T5MrLyGmFZLOd0OTn
sjVsNlBPXq8yhX5eQ0PYARQYYx5U2/Jf77O0qm6QHT6QdSE6OMH0BFuuE0MCdJe5qD/cW2pdT6AS
ZtiC4kLlOYAB4xVRv2Wudb0mE4J5riQqV2MDlY+hd3bHHepHaMEvh/M6L6TrNPSX6Fi/fI8KnY8M
I67isFJLjuEsyi9FbxoEGiUPLFgusyCR9/1jcB8j0SF9EYVsu5TIehD32xOF3lIpePPprEkN1IJP
Lme2PJ1vkOp2VTuISapBwuQ+heFpE60oIf/P+NTCMyqetef/rQDTU2TKMoge88swkdKR3/qIM4Lf
2YV6LwTC92D00FNm2xWMT8ob/t0x1ec67W1HmMNVcTkSOQ4/FkrX29xaj2XjdiI5yDYJ4hcFtXFP
getkXpc0MHesuKd3BSnaq4edESwILxoCk0LtMS817gM/O9eBMXhZyfu2ZjsyojdQqayMLm2BFzuk
q9wA4PxJFjeQNhqCmnqfwmMMOWbdj518LETI2i670xr3rMSW2K+ipm5Ev6Frfo0HXT2stSSKwDIW
PZ2zgPTdACtKvwx5AiLWkkvDt7S5bfckNJrhE6c1dEDbOOomYwFpU5mua1swaGgWvlFF2/2Gy11x
9RzODjn11LEe8VbjDGzyVJTA8sBrS7yERqv4BXM/rL3cINb4YwbmHgwrx9ZHq9FF6JT18WI27H8O
9qaqEUQY1r7mPRv4kHjyUrVDdvB6Noij/wRxgt3qPNOtMAG9V2O4FctYgQ4vYo9pEgLP3z1xz/dW
Y9AjeRAimYGcRMBK9gQj0fdYZ4ELerFaHxrWVxacRIvs2C4RhkPorsk+0NLjdf4bfxs6TVFkY0UT
3SukUKhvTtFCLHwbqlJFO4Td54pVNGEvBn6dWkEucSXnkpyKgxa+6DQC6ZsdlXZSJfrCT8vD8lUA
10GettSkTMMFeR5nVX4FZJjBVUD3T2g6DkzWFD6nIbfHEpzHMYyLM9Fx9zvhaAqbCRdmGF75dXMO
QTOqgInWNtetghtiG1/1Q99dGlo5y+Pn6A5LwtYNPLWmWxuZm/BC+ZdeCY3kOfuKNcLwAMr0fokM
ucB1nMKpSeBYQKO1aoFQGGavEZ3Bk7pva1baUJa2/4o5xgKRlvqp2LA/V7C/QT/rsTQbtdrpIEU8
Mgx3aGj2tUVQFPO6S4ETg5Yam9CQvGWjDAJow0hcPqgZoTsOcOcX1R0ACPf229h++Kc6CMsKWmmq
UilA8EN7RwCyCwjCnHkDr66SkAnxoxGJg2SLjGKTM6yG5qZK6I3mtAzTLVZwIrTPEzttFvoyDOFt
E3J2VIUYVSG70a2YVUr5QWs7JTiOX5lhR/I5oJPMPRv8SQQO4+a/+R52CVHoH5aoe3AYsVitvKia
Gf39JYcZxtXtkxqUSn4ZGPtNkdbdi+QvbcBGGQGqDB9x0lOq253o/uzUm+WbtZsQIc0vVYbUWb0y
uTAH0xB8lN+uyBuJlFnQrF9XfRAQ4wqkmtjtbdpnMkSzdh7BbZA7Ch5KMSZkKg1yQ8KXWif4ahbx
QaNv3plw+Xpa4Yym3whnhi04GQLVMqc9dkKG+f+dGiKfEbV065e4JPkr2KRQBq57CfJGQGma9XfZ
tWyyeoNBs9TsEILq0qwFpuQyPrm4mvRnCxngDmANXfLg2t4wTc6neuD71v9NKfnWKpvlh/dHm5CC
Jy7P9r/DJmuskr+5ydgaz28GaKJVkvtgctOFRuwgtvR6zF84XsdPlH0+wTY47d7nJIXJ1XPFlEuM
HUbBnUhxMZUsVJsIxXbcEwrR2/Dy8l89W2CGGO443JNCuxMiMPiTT8Dpe67y/lhRidtR/nNOrcfI
CObaHDc8ieTOJo3vjXLvqqhlkDBtOI1dd3PGcj6LqZEtFDzSqIFKt3JFXK7xT7LE6DsK3eaEOT9G
BXp6hLq/2uuR0fydDsyuMalAMaPORtQfoqTR7JpZPDCv+TY4TW1U5qBr1+kYddqkHXyUu/5iri80
SMjWluOav2sq93wwMspov5HDbFN8Rh9d+iWGuCF/1vTpXsxiSngnUUg3roucFUP2QO0jIswYvumJ
F9iOB9zfqN9pH9YGJomrcwz5+LA+rgxnl9Zh/JOdgy6NgBL4vtoyHdIouUgJenxTL2xjq3dyQYkR
Rb8Gg/+jiQS4mXmbXmY6s5HN2dfCrb9rEMRSZIxdGX8b7tdsVR7+TDF4HOl0yNn3cu49N3X79sfm
rQQd//yvdx+o2tanSWe1uCtP4wrA2WDqoCn0Bf+uUyEIcaY/1bl42nRHByvQdAjLXYJARtHcFddC
ZwXQS7zVEytgID02SaHuCl95iZfIFN1cVUXkLTUthkjDoGh3vvdmpK50o4y0XTfPyRddQ9wjtmpf
tZGVknABVlch1UIdAMRoNHKOqnxLZjrazQyYRwg1nLrO9Qq2fT/Vdsc/nwMYm73/gtYslW+hlRZ6
qf5pBErKc+bj+RdMw6fToOU8fhoVgAxOeGvZi8QYLqbV9KdAYY10HePRQEIeEZtS7bR8Z1LP74YK
QVOwtdFHcuyZcuNkV3ut0xNFj/FLz5VFGrA0ER5ts3P6lF+4FDFc6H8/Trb19n1w5mhuX/VLAEGZ
yIFEeHoN6dA7qEQCfSuUC29pqmIy2M2xTLtdp7YZxOrMyHhd6u4QcFM7t8Xp1kTT8ycPTNFDaMMk
K1BiN7cxf/qQgASGuh63G5S3sK+hEMtpp/vtraldVgXTIKveHofK1uVViEKGOT79zWY2N+5AUI5a
TTtnOMuVd8eapswM3opMlAQ9FpQjTfhv3mcKKU0d6VQR6uYKKRlDFR/jNDngor6piC/UInizjMp5
qm6hA3sko+UXtlMZtRHzpaLbb/AI8v4S2OWd2gROMJ3ZbH9ZNtJc5vyFJy/OKTwKj3uBqVr3JMIF
3fSDZw0E+gdHFTPdikN3fAw528bkP2zuXL6zcMpBgPurW6MqDczWH4g2B8iJ6Ov4OWyX1dlsthhs
91M5HlCLfxE6pqcNcbu/xM3DTTTFWZU4x2fEjbmwhFD6ZohEifW3Wvpm2gw3XWbJ3+idP2lz2/JR
fV3BIvKtte7i08Yjh7OHGMkAHDXkLtCmQfbQ8svY9bE70AruG1wd38YBPY5zSmd40n+PpZZ4hGrb
tLcqKcRB0BFBR8u9tCirSElv2k59/DnEtUG5h5JXNemj4q+0JoHo0PrGxk1Zbc3mlkwZt3NvAd9k
Im8f3STl0zapNOcqVoQJ0VYT9G/y3Pakg+gtIb3fR1CALO1161JZh5fk9A2UhuliFSfwp3ZUh/lU
jKnFob86j4MbOnqm2BhQzn61VQp9SgBut7HTaOz4Sv6QdioMRCWAdVuI6NLOiTyZntLbJl9pD6UW
qR0NdqBcdAEx14WNydp3iETaEoWHTFbhLbyrQDLTe5Qydag+JX7D6Oy/bK6hga5bbrZSrw9a22i2
7ymFUR2vZv7GUwK9kzNAk8F+Uk/MRy5c8vaOVoN5bwh68NcqRuAcPWZGEZCvC6UdE3JP34/UEKqa
fShhf23Avd6damTKtlfHetEgb9W80NQpynQizpjJs9nUXBPtgYNYtFuMJJsVSxI2RW/wBhWlyouL
SkuL0r4i89bixC5MdW5LFykm3XEJNORWcbQvD7dXJHYPZF1XJBBHCvFxoIWyaQfWB0ilkPsUjq90
zXpzkPUEab7YaaG0mPCa2KDG1iiGBYoWSKHlaV7u7kuU1HrvOdEnVGWZ0WkpvTsAwuYkNF+grq/Y
26JqAHwFNt1viQFc/mixEgimDz6zBXuaxDNZNF6Gquv760ewaWEwuGoDi3Dms7YCfam4xAU56YSW
VU2AQP8zI6ZG2cB3CE5eAULi/gdFHcRx5TGgajdQEYUAxtuvRiQ9J15WcMzopivT1Kzh1G0GqECk
0z4Fq/f5vaydOoiuK6PsJW/l6b8mCFlka1YmC4vT4L5b3todMR3sbSFF1n0zo7lTOjlwrIR+n/Th
9h6spyOBAZ6PBTVKD6AgEl2NFIochKI2YAaHz82o+skL5NfYcZxWDvFx9uL/ph7H1xKoM9iF1z7q
2OtXSgCCR/mUbQMaJ983CDdE/uamfEb0tPFggGUp48okimcP3Ew4BxeqLlXy2//oszwSsO0Vo7ca
6PQPZm5qeirULxNz8BkoiItPs+nIOKIOm/14uLY31D9vNfFgCYssGRxO1SWTb+fi4F8oNpb/681A
lRJ6gtyoyrRNsUqoZ0SVEiKu9CiPzHHoq6pMCOkOos5X5kypNxfDAAWsCt4fOf+qNKYoK4/CzyPN
JTS6j4sfFfjLFhjn/kKTg9MxmsC92e5AyV2FfKJNDScrICjJRvlvJp8cW4PbNCxNPFafdWZSffp6
kIoYVgqxc6wPFFBzoqo3atHM4duvFCZVWcXwBrz5ID0Naassb72Q0u66eMZAPrsA5kbhqiL3h+Km
wAUrz+QiYlIWTWAYkrz7jReI3uwv10LMBBk6l1kSnFRw2MH9WduejUyqtomaoWbq4nRGUco/ee1K
TJQA7nDY1EFJWxK6K5wkIQrwYZLmZd/+t9VkZeByjTYZQgO0Yv2rWvM3ueKd7PeCBDjMoco/56Kv
5c5R2ayb0RGNUeVJ6fwUOgJw2QLAHAMFkZtgAZTtPa79TbRukdi2KS20TfIg8JTYmAr2xtd/z9aa
QGS3upaSNPxJ7PNkOf8DatDdbhBGKRey8B8YN1VVLYuzOGwHWhEOe5Rle8xm5lTfl4grGCTHdnks
l8pDxHOnj4PTRDg1wtqqdH5CLrqXHXb80Qb/OlLIvbv6L4Z55jTEGLSL05CTblW/F6JM9ygDNeyD
Iz+7qeV8pGah+01di92yM6b4feenCk/YfkINEY39el8y4R0mkyrV4Ocwu9YC4zrpugtSZXxkJqBp
yFTYD+b/J+NcuhpRRS3NkiPuCwPBe06FvA1uJQU6LzToPTSoaugjL6QQJGMWGmmJ55PmVP35IaOf
paXNWqtfu1cNP/zjPr6Hna/Anx5Wh2g98DQFRHPvil9v4wKowpzFaVlbz6FW9KEhFgvyAQMJ7+bg
UdqsRIkdlBaFCYsKofKwGVcKMaVsaqH4KEe0hODcnRu2fI2yCLh3eEQPKUoczuEzcgWrGJjHa1SW
xG3QOIPlyZR+zk5HQ2Gs2G0Vx5NCyXsY3rng7yTBEjrSkKz68jeXvbB8J6d13FrH+r91sfM5JkVy
AY3cPuaD3jg45KRKZF11uEcE0T7JINXh2iRA3yszb/+gWmffBKG+DKcaoZNGFN6XLptwbfISeJz3
JIStGYX3kE6JDU0WsEU6CJIPOrVKEObAgNzW/HS71PQAiYVQkbx6RzRDWU7B6Dq/tylHPxK0wQsv
qyODkMVnpqWRNhNTXCjVIw9f2TZtEsISj1OT1In4DtZ6ZiN2gdsyEcPDjAi7zRsD+LzHT468pAL7
FjOa6IftFEIuNiw6S+sLmbcrvAXQaPZBl/PqOnEUtXuGc98YFzWJzxqyFusdZbCZCmyth2tBq0aK
eBUStSCP7Gdd1n2fvOT4CylcAX9jpY0ZOrqauoIOsOSWeiFu54nYIXOWzumxz7kJUQ8jse3l6YHM
toklVL0aKI5RmXHVFv7AfwWTZCb/2fD9TW9NV4PBLtcFNCvJsbAY2Q4U2oKkZiroL23u6+kGQ+X8
s5bdtHH73r3gZjuHVXZTtafOBFTqfZ/opFlmtJMUstNzQpcvMPuft2lZ5ddW5t2NA8HA41/Dk1zv
7ZHUXNfVnOWK5ZR5luPmF9JTJv+04H3r8tNAHvwGqs6g7DqC2aKe6wVxRbjjPj4tpy55RJtI9292
VQGixk0TPaX0QrqBu28JlkVPIExXwUXS9vfHTXh0RM6PaTxOaUDCPmu0c2otSvlOk2u60APMdXZg
1vigQULhESF/VspxuXqANYmlv20o+nehYkSf5ADKPrGV96vyzOSndnXof6vgNGhOoLTVfTJ4m9gW
GtBfeoGbYJAMJtdThNQ2j613C9y9pyLzv/8If9JHH/B8X7bUrmRDONovR3jsGmDAf7ZLViSh2OhF
0DT4YikdJlJYzu3xYPm5FMlE3frt0vXbiRhrK21cFF6eZ/mrhj6in4BYOsNOc3KX6C3AjwBIXMxp
ZZWWHgyuG0s7n/XGSVWDZdeM3JwRsdlBlQc/oou8HxOtnyLab7dJEMzez8IUrFHocIjPrlhoK6Xo
7w9qkoiePXnVgAN5ZtOJpxfzWlskZpQJp/0pRvuidCmBRQhYIEs3kLAe6MPvjqTiW9D/n3Xqqg7R
ss9RwrTWpDmfo37yEONd+MUaeZDbFAY+nsR+bCqnsNlZ1sbNrGA4JoEVyL2UiwFd6aMaQgY5iG0I
GFB/fJrrVIrvOoWUaUr3qRtc9c9IjRGm3o7U7oq76TV7I+cJAcN/AbXRvxHBlIp7kNJhRxhmqTum
L1kBI+O5DOaWjpYQbasU1goUy/MasgxJHka+6GFjB7ApegyqD8sV9DJCG+N/im5gtFsrZONAeQBb
x8VNRfOpgtgCc6TcvgwN80UQRjnITL8SsPFw4uKujseOyGWNn0wYUK2qT0vZoiLZWdI6Z9nqBxlm
rDMYe3VlJT69nhioLtmEqHexBKmw9cz/DGyEQ2TPASy/SIMqO50Fx/7QLwyuYgst9DSrJQ0GieEK
ThoiweOXVym48CDS5GkJE1JBxcaGWe+vGj3A2EqsuO1dKC5QQsg/nRA26hplrlIs/fARD+Cdqr/F
V851MvK3/LLGoIC9gkXxHRvqhTPUoU4b4WFH1ZBxAxAxsxmTF+GptZ+1rPWh+SSYvmY75rBN6X4H
CKqHntHP69lgcwBT3ymiJsKQJ0/fnx7l+KRZRpK3z5cQqUhFw7Nibi41fg1bEF3yJmediBsSq3IK
c6ddbBUKPprPR24WStaWd2zgGYR2eTrBFsG2ymgW2Hf8q9hSHhcoZvJlwTALyCl5rOVCWL17o9c1
Ki/kMfim8n0t+gJMTGC/ry9EArZjjZ9alCtOZ2YxJOb51TTk+kV/iD6bdWIyV5S2W2tpzVEZpBs6
Fz8epmc0KQ9cbJCXM5qFwB7rOgr2UXs6VrCjglAenNUuDKDKKMLFTn7Zi+E9Vcg0LurGpHkY9kSu
N6at+/eu4SrFhPmUZrwNz06rMAW57OxuDcTbC8p/W2DXZ2CSKgwjSXH2PQLquyfHDsUL7p1QStFd
6Wf/ios4Vq4V2TIf68tMjK4d2bqoe+ESxwS2pW5EjozyAoplVDb7o7Qsmh/SGWuzVrEjFzAnBrRn
lb4DelS2rNvAcudIQDb75oC1zbH4wPmxn/W75/GFslH3o8m5Y3T6E1Ly200LtD7trByC9OyeVbAT
kGLfn+cLkza35ZOQNJqvuxFWv5ttfm6BUENuvPF31d8hdRuR7pvFTIX+hA37SbnAzx3MY9ZmfTov
0casX9R2s9n9wDjez5X+4qFf20JUS6MGhA2wkGop/8IoP6tsr/TnPorKXIGBC8TM3ipngxepDaBU
h3nz1H2i/OeqmBd5Q9pMOHLLqmOjQAoybBMeD8hrXCJB5WRJJZOVVTCr38OAVxq0m5ji6ZRGyS41
+66/w5eorkQYilAKfkxNulr6VvV7Z6IGyk591p1cgCmO/68Eoov1MB2RIcBG0er7cM3B7s5i2IES
NfrElIktTlN6m0dRX3LOA42+bXuPJbQl6kxpzK5rHFIEUqxwzq6jFMR7Pw/45h0iVmtJVWWLOa6P
y7Dxq76ta6SMM0099JOVURJ7BHGADR2+nSqZK2e+OaNkdbUmm9TsC8Hc/LYsXDdBt+ZDMQw/dcGt
Qltpd6x8lBWtQAkMzWTKhViF+qtojI3dD9dckS3tKPkM+2x3sgGdcbMY4+b982ym5pmfqm8CnXA5
Nq0rnKEorv+fJrwTytIeh50wE0HspH+vB371rTd6Wk4pXIKrg1BtnFedVDfy+6FArNTg5q1nccZB
/BQBHN0IiVOkZqHxLrqnH9aWkBqxYwlc5/A/W77Ma/IPHV7hR13W14YX7919vDG99s8MQk8MCKyg
40BOJecwnbK6jpgP1wYm1d+MBNkM5GVnMFv6EVPlQjkPZjQ2Vp0rIFqGQVS9wPbOVWq9Ce5Bt6BZ
nHNm3OMvY43FbVE6h8nY5bpHyYnxMphpkEKxxRNL3/7a1BTd5GVhMDVJPf4jB9sj1QuxtbpLG9sp
fyoG0YQgYJbc5fWRFpWECNS+g08xBz25zq0yXv8IkiEIRXB2PM0uwJlu8Z/sieOMoOER+NxJTq7W
3UzpHtqDRLG/7dKSsunGiPeJ/eQvjoAqXypLdSH3T7UfPJEIGpY/ZX1/9AUA6FNj3xZJzYROOd8v
U62qn/wXchrp5n4Q+Q/H7ZpeSeLQ4KmatXXtTxN5GZHpt5JppqeN+AHsKtsK0rubbxauQo2gMjDc
sFM0c0hFejza1YEQutzJHApBBEoLsafzTreCPSg3evA8fl4mL5xXaLc1mrrF8vPVb26Ykzpi25Pl
kQENdGb+bGwp+RUhBqaR9j0/fDlZooCVlY6udHOEWulfpxj9y1H8DW4cJRsqsH465JFNS7GPONyF
ZsbMu5RFT575ZkZlThI70dzatyr8z/XvkUzKzItE5yX1XYzjo8ssw+aCAU+/x6nZbvkvB+7cmyet
g0CJp87Ghd6bIQYdKLU4cqCfmOUQSlfJjQtVpO8Zn3oHLjAODJiePFPOTfO5zTzC3B7oz1UmNcms
jJI1DO1DZUR0Vev8RNRKQtbxTNZTDxh8KHSThzZ6KXuF9eBa3AWIPkjAdnjBOffLwXaK2YVgkgVr
hef8Pp4kXKM3aJcSta/ng+SFsASZw4yrzmg4Kn8LxQyca3MiM/ToMVlHM/PADnTla4QSS8YIkZzy
ZhXOMtMJrFf9zRzv2xmng8cRmhOhypxiKn279nFqdqIrBa6lNXWSBjUSOgZ6AGKc4RsmZ+0ClZcL
rrkHgrhL/QHWAyAbkB6sy9cz2vYjZeOt2zRDwOJP9XBB2nnp6AA5mCxGMZDB43cysvgr69O8Jvvz
B8g8Yt8J9WNmaFnSsabHI/Y6ET/2HhxJHI3xqDin16qpsBLYfL0nFZwHpDYUO7fxZMQpvk7KQTbF
FWtOULnWFDbCdY8TvZEM0bPYjNwaWtDAcyLgkqkJyjEaS1xJ0U4SHU99bOKIJBybVC7wajwdy1Ys
1+dWkodejsCqaPe1hbu6NGgua+GVDijTNmMM9S4f007M4llrgz0l9RrU0zNS/Q2e4GE0Lbe8a4Bq
89hy2FeEujexRBAVVVceEtCBYxX7hYIIM2Axmo0bHygbqXompM8GX5OgVjUlT3A/Zniq3Cv9jkrQ
AEaEH5vyBR0tdqEDhfL2ktLe3oWEi+lF/TTstqaOkmhaj/SGBX/v8pWYonDgNv1A1oIokiREoCKK
zwMIXsPSd2tXXnii1ZszBY//4bjF7+FuPdIQLhU28Vf/PHpP//BKHxNk2bzBdk9APmPflgJWgBr1
mM02HQUkiiEU0pUtr1U2eYsRtlv9BDUTczn3NqsXQFoRqxAcVAiTGqIYuiXApxyusUYrAKC2lInz
x1GhOBG5j277w6LWvg98SwXUPR4PWy6PdnqU9mn8EmNNNsbX/ONEqlYqd1rcs/FHq+QfBtqGqJgB
2JrbOmMwjhFnEEUDwnxtiiCzCPiGX/tPqrMo6dyebBN6BYcGsWztuCW96+LH5fiSjmPWUT6qslAp
ADFg/qHJkwuptkqXuQuk+BepQqV0wCv3mbRYJHV54+vK/ddsV8tBzT/SSIraKYfsFHN8QWyEH80e
WgdnXIJ3Vf/JVPgSHieCuq+VO+0CTPz2vQPf+/Wv58DYD4DpkSdvyillRAbW7Dv63qtxMXNQ9VsB
MhmSPhJk19L/WLSY0KSP3jBVo/Jh7jJqy9vRe3tOx0yS5m6OzcT7+RmzOTN+qgDRmOqLNrSB1xPr
ujVXoXTHZRWxhDpZn5BdJDZINH7z1HbgJFgEFVZM7LMeALZrGe12wsFg/SXCn4qv8lLWS7tPzzQV
hDnDEVMUh3A/e2dXnoFXISwLEsKPw9GzmBP8QdwJ0EMSGqMgEMUxbPaco+u+b/1rhAfujEJ/vTGr
k2y4NCEnCXHIpcM8qxFDXFYCrmUmd4A6o4UGB1mUevrrG5PqSUUp55y1q5wdBx6/8UTJrLi9+ZX0
523bEUZ83FT7Ri5IcmzCmwqfj6DO6t426k1AWQ6qshNNaXkkN2rKGOFqIamE3Cg8ZAJKh9VZBPSS
AAYrl/JopbwGnBtXUMiSUffR+qvDWelbp9AGpLZ9Samtenciio2NFy6tyrnF7lMc4sP9MnTMb4pf
L7r8J7G1XSBP7FN7fiZyYWso4KD3Yl+Wy8/AZM4nUJXn2E0jEkQtTqFWQx9TfZhIFrT46lzCLnKA
Ba2qGkke684RvZ2jntawzFcBku/QDkmkFRA+Tb0aflxWO11tfrlkxz3PvuleBZUdOaEjnuNWAfvf
DFhN4SZGhlkYEHvu1118qCZh+0CsGjEhXw4z+4NvW9rH5xnrg2QsiAgTeKqbvQxK7yv5J1G/5Kif
rI1r9VSM0Qal0IPllh3xKl6II7f1K6H50qRB5cd6xzwlvQAsoy9+0eNEMqN7ajeisvc+eZkzq0EO
JyPBh7S4EgQEdoB5dJkIQsd+H7Lw9mlnRDBIQty3sd7yPjoRwEgTHSFEQQyfq3evaoLcvosyCZ12
YiY+wh0xGQ/60ZKl/EvXC3nK/azpQ+bt99jop/fjIn6dncD0j7iXmZhoxL+fJQBbT9eGlre8b98X
mJ5isZzyHvZ/qhuFdrVv4iDoQ3ltlExG6Wx9PoT69sGr2p5/qt4CtRL+LRIKXpTMMJJOxFlka2CR
TjBnqOIQwUo7xvR6e0/oTs5GONlLTOu+nDATp1FSmnyW5Edn7m26XQ4aLzm5WevxxAInhGn08ZQ6
98XDDwvgy0IKW9tQSorbHYyauF4FmFG0xfuyEt48jlYht20pmaEqa6LwvIoy3cn7L57NTfA/rA5o
1/2oVdlOpp3lNcwbumBy98yWfPmj+j6cEgw0Kwz9gLtibHHyWXukkozCoWCoz++YG/cCIiHvVMld
Z7LYCIzNzZePPPyanZYHQvYI8Ewhx3fAFg7cfe57Ue5IzN3/TAc+OOo7CMI+j1zne95cxRAIhra+
mFTvMnmVh9AcbeHqmIWh/I2yA9t+QfCLCfPiJmUI90AiVhIWikei+6Y3VMqh/wAo9ANKAu0cqCxA
aYdx2QTkvkyeXjt/I47GiPly10a/Ej46Gcqcqr8yt/hWNo6Os2lTWyk9fCeOHg6RVr2eqMH2T4yc
k0vEjEPgjFhiHFVxtV13VvybppEHYUmXDfsF/BI8qmTgFZwkn+BMB7xsUA/XLbUUTajVJYst0xQV
XxDoNhiK4INZZo+rNMe73saGZBnpvDbXrC4Szn3rKWwXS/fQHGzjipxWhq/RjrRmr/uUz0b8hx07
IfPXnajmOeRv1rnBltqaLhXCe+2EfHOxAwVogtDS01JV8fBmetyZVNLlxqz8IRMWNmDpNxb5MI4L
HjDKoC8wfG03d9AfraQlX2clW66QDe0VbBa2k15tpNvHluHM/WkRZ4h4E1lgddjAv12ZM3SQVpe1
RNoKph8ZsDNUJWytwYEC9ILAWJX09A+SPZGZHczv9Gv736DrTBBwNdyTTOjshJqRYKNqHkvpcjd4
CaS1NI0zSa4wj7V9vqIjludwwt7gOtjAb47g0DNWCA1yHBuqZbrIRzOPaBMOMKVB13BEBZxO0vnv
CWM1e3rC3cDCRrcubjxI9yYZI+pqF5nXHFGJEOW7mJtSheQNyT3P4pmpysQNerYtEBuSgzxI+jo8
EYySbylZuCK8fxYkbUFSLlJ5RPjsHOpBlDQiqa1hHk5PqYAQ2upglAj/6H38yqoHZkL1DG4mrSK2
jLl+LIY7nnYkTnQjEVM9Y7nQOod4Gn1CNK9wlsapMVDkg7O4uiipJL5uVjszjU24d1t8k+r4q7OE
TlAKsk7D+OZVKj1juU+tSIpRYA/96MAfVA5qW3ewQUUm07TJ4uErUYPD+zOVlk6QcOe8f9H64LD/
jLzwjjPCDmzWf5IWuow9Hp6qzmgFHJFkI0RCFx98mvDErfqONKcKFqpjuRm3GjgC4VrzWtB4eTRc
g3GBHxPu321PCz4ojonDmSnr/pcEsn8at1bWerXx82PUOsTFTBeVz0GuPyIy2yOgE0PgGXF5QRXK
D7UTaukdJKHXOURVGV+LPYVqHTbctKuqVjL+WK0PAUUDk7nIIL/yON0XyV1t5JoNxDJbaQeGOJtA
O7estNSJMt3mc/rbmQ2cUKlpVsgL1jJTG6Jd0OEmqp+to4TxAn6zcbcYbsXI4wqdH4+V3EEKsXqm
rbZikkLH5Ivyr6WhYDXbZILzdzUKhP3/Tja3IR6jmQ3A8kxE+hNRPHlfzf2s2HSp1cRBrY/xWiLF
n3LRZ4RnROjF6g8NqQYHYWh9pFukIyWpiLJjZJ+uv8EcK07u3BsYMbXShlRr1BWLY6D1G+LkxI+B
Q3RXn2fSMZh0XRjA/Tm/70BDLTThFGYeIlGDVkCMRHutKpILVkSfomR9tSGdIeCov+JyokrKzBNh
WA8OuVI3iMn8GDkV4cIGGuodaXdnNQl+MAoX1ziHiZgFWPf1s7ZD+M1pEeLJfqKb7O+N96Ev1BHK
/R/0Xr+PbP1nmOWSQmDerX+rLGUDTkWO3msZp5yi6qArT6X1ugvljeM7jbeP1dF7KJB/Y3Ogo9CK
lQbY0ZeOsxMOrmv3lfVqQQm7gTMI+9HQfA5Bpped3DlVfv7rBGPOlsaTw1uGE8eqY3PmGmHhdUOM
riMJRiKPsWM1RY8jpSKKjDuUAaPt2FeyNj0y4lJSXOGIvtRB2DGWKDRP7k3kvZ4RnW8VjPWWWZvL
XT0oFVGt5nn9vEcj8TajA2tEBBC6vXrh2qx+eaKx1poAPGL7ylpR/TknDFs9cHVvOLgTxH2QhEEK
lqhmP+E8a56kek1PRghS8V/wyh83T/+CfCJoqrofrtFQ4BR9tzNj9EzQUn8f5LyihWmwFC9ZKi5M
FMpZc59PoJ6UT+2kTAPg9KIoOiptOluVGsTtbqFVLCo0xtPmFxALz/7oQz52clkXNGwBAGPb5k53
f2fg8EdD+34EB26SD57EK8IjrtNt0fncMXuejeLhRskkfRZ2c+UMqCtOyayehlw/Gg0cAGam2FfB
nIjnMPeASCsxGh+2Xep++zYquRO2CymM2Ki5TYf7IKc5HYK5qo6YYUiTtmHaRuHtNjPT6UK+vcFo
gmVfpnoDn6iPCRosMCF7kKqqwAN5shUReA+wWI5/M/cICWAInkN69IkTuVnaasQy8QVEnc6fLUZx
q5Y57rPjtts8DynXa6bTwT8sJ9fANoccVldhcVktXrYGUr4ZoTWCO3juQwah3uu4cItIrpmIGjhC
n19pLE5MlQ2E2AXQHi/dUYdd0TCw+r7L+3oGfW4vMtLBNFHuO+x/4T4PaYGhX8cIpp4rZXr3H+qq
myUq8C2ewmwcx7WuZlSG81PY2l4tEIdTt3vETleAX5GbCtu5jU9CcsNyzzNe+nE+8GYck0P4wsZ/
ydWCR55oODWGKaHNuPWXRCDzZVOuMk8pjiP1yTj1rSnnSCPClzq0eL0LhgKEkwqCjkHFapaiZxDr
qua2yTUJfamtONwxXxpWyTR77co4fZkAKxlhT3KyzyG0jyq2PoIoo5ovdtPnwxhVFxRqUW9akgHA
M1Zh11Y7e3ELQSTEzSJMH892Na9mor0aAmm86xN0M/hFfn0DG+8ynbULIicPp6KWuNZYS9Bj0G9V
ZvdUUm2wVWjpLmkrl6fP5gHRyCWSm7E7f39dFOY5PrYZDcQk/w4a6IepGCuMN9RO3z7FHJ3/adt/
iCynXMEY4pdSzMgLaSIxbbFfgp0tX92yfS/n45bL5/sCMPHZw+us+IaTVHMKFh3Fok1/iTSwuXdN
N/LUBwttg5Z8vQAWDpbXS/sDJ+a8g8Oatz/AL+Iydd4j1hRXtxs4Yk2c0ZUGKXd2PdEJmZ4SKy8S
a2wpN8mvKWrMF3Z6QLgD22Jn+t972eZEhWid7cyUzb4bgsr5P0K7gNzGvfhCMHY0LHHcXDKfC3kT
1x1d+ys/dNNd+7t1Fl8SyPmryzcSUJMXvLCcugwfeip4aucp1AN3xdUdRjFSshK9/zMLW+B3IE8D
5tQ0Oh7t+TeYO1whx4AWZizeG1huNQhCP0BjbKHM0li4s7HSvmzMn40ZOxy4AX15rHh4il3tfpN0
Xkkw2Mq6RYd2a7K1oDAL/1Hh72r7jTnb6/dWp3I52fU5BM5KFhe1XDRUJBvos4LnKg0C6PXaGQie
nfx9OQERu92cXVB6B5gYz3cbG4tRPQWLYmrbAtPyOWqFBQ4Pb9165ZTVrNP6MzpKNzv1v3X6G3Ka
GbIg57JvRjQZfsx1Ut0pItXevvd7BLdIW5njxxpGnE7czWaH9KhLNfsDHSgrKQ4RzZtJ3xlQK7CW
wGVWL0jlbPAvtfZAMWE7BIt64dcSY+FWO7U1yaTgJmYBUmp16YKep8pCCNpzc3AMAYnptsrWQ58J
5cW8yfcCGqHPJmiz+NSyQEtqCQ5xXodJRMBTnk/bTWmpkG6dx3159S93e7K1RBSS5xeuARsBZVPW
ydJ1WmzDKQv2Bpqa066dGL/ZhfwGelZKtDzunvILUDreSYd6W2xk0fDnCSp2U9K9bqGzHzB0Dtle
XxhlNiFoB+woTNDfjmcd9LB0Ct7FIMhpypLj5XrFLw7mcmCoOgdHVieGvqKb65eKRljINJ1l1h9E
bYZ7Y3pAcCLedjOPOJ9OaLuG1wsA9SqBL9Ww8SlaBqK18sHsP4J/KLjoCgZcKu3zX6LkI9BdiS2H
zUtt5cpInoX6tKQLiq0yO+IQ1GNVUIhzMVvbu3/znNOip0oD8iclPU2sGd4KmG1SickB/ixY7q1D
t+3VClDgLNEvYCUIy8TAaeRFf0igDC0qXQFQVy4hi4suRGhyzuDkFw6pU6+tV8ztoG9UjI7FWJ9Q
zZoz6MML+JVafnXXhN1ivD9P/NzfaIEURfJe255x3roCF0X52HQFXitIYYyoyGmxmkVSAweNldcp
Ypmpvy+fcJIJXuSj/Te3TZfGNHFdeVSREwd0Hj29A0HrlbRQFGQspnJXqz3FvsPRlORpYYM+pM2V
6RlS+VFWWT4P0X/6/hQq7HWosi2S/IyVOlH/HXVwi8upJOpLNrsUJ4GwW5ZpHIlXYJ42ytcn/M3l
flwN/TdvrtCms2NwS9cO4SHW4V4V3FeOmr9u1fGKrfOovZf+h+WQgrJ62L0eosSx9mz9ZYMnDsAo
CnSKsrkLD+vUKpw7aPCDE8BwcyXy6Gx9nS4xTXSnDWHAjT/NNLL2E65PB/Kbgn2seK0xif1XUBJC
ZPdFp+TFA/qfrJIAfgRDQPDNsXyIYFDzw2B1cQ6Jn1AB0kWF4jO8ASb9oZDxou6nZAAeHlM7ct5m
fFwpByIfp9ee+PwC3VTolyyV04Ls3QTHg9JJlIRi1QYjzwbiSv16QoKyb0daWrey7G/mR5NXmQ5z
mD+tUeGiHzDKGwWrTMMta5ziMg3A2Eg8u/VW8Abt1QmgQJuZW6P5N/lr3BG+HGz1uCILeJXJIEDj
jWExglcn0HRYvsaA3pPKnpLi+ZWs9fARI9L4QbUs3qwlvh+F0v0Rw2aMqTBaOczFa3rtZCEL6u30
hZBiMZ2bMb3kzjKKAkZC0+18OAuc/sQngKUQiSgo/UFZE1OQYdP5qFveaLupy44hNQkHd791zw1E
T1ymVYaf26/t7u8H9KpYJKtvUJzIrjR+siFVO8BjUezUROkzQNh+725QsYyY4VgVKb2w6FdmdA6Y
c/nmbnWJuKtRSx7yeDrSJGC9nT2o29666vRKn4THah2lpX2mp16gi/9kYFJcL27yDOJ22F49tjQa
UwgTl/7aT4T+wfyQLdaDWCAC0W6yxG8YBRGeW8ZJI94gbKufX3JuRJ4DPMAS4oTKIg3XZMxGPfAA
Eh5L6kSCKZtqjqOAHAxgLwtu0bmYJ/7ljX5U3169zFZwK+xrboX5NebkoYtoHSwtje9abskZbrwj
8n88AqjvAGH9eXdorxHmBQjQAKi1k0UdRxBBzMbnA3stCBJiUebo3MsZO1lOCc+aqvuZ4M6Bubb9
A2LE3OzGoV+z8l+XnzOlLLtTfSGBH5ZhzKdUBPHEF6AYBDZSrT2c1Z/dguLXfPTubOrMHQgFd6Nq
8Ic/YiUzjgrBy6hhaUSBiqHiA1s8GAtAyfRGPR+L8ymA+HNMH0QJPLL+qfWxnnpAgstuCHO8TnHg
K/FNGixqhVhifECHb6bAE6MBheYw8AVIlCCKkrwb30ewsLfEYc7KRmcVOxRQ4qTbgOhMcr3M8dcA
N+WVeCe42RHXEwBXmFYCfv2NeLenr5qk5oorF9oHfvaUrf0c54W5ndhCFk5ZPZN+8FzhQ+ds9pqx
PdbAO7yswXw+i7nZjg7rvfnnTBldiKsJffkcFbIFvyk4wNDFA4gWNxqPmkeg/stTqnaOX7b6dmqV
plbw+JV0gAV6U4kvW5iQnHuK78VD0YCoOpX/dgmAemUFRs72w6KAJ3BKtAtpuRbqkZ2Ir0vSPehc
sf8UJCHOUoFH483Tx0qBXgXwb0tNbstJhQMzIRFLmFF6fJBOU2crhv+GXZwHM1lc8RWKgBBOE6XN
ES/UD/9+2rXI/Kysi+qSGXFHLmi7Sfm0IeRhqBFOrc+zBmoFxLaPTKaSbobJ56Lp8uBHh0w4s/Yc
uhfJIl9Ya2QKQXF0zT99AthpnU7oiD8lDWnCEPFvgJh5ExTXMBhlTjmjNMLlvqEkL24Ooac+kO3V
NPn0U+XVljRxgRHE7KTIjCJFM2kjX/qfRvpIgl5Capd8ejzrmbueIiw7twtlM/BiTP5psZIGpXNO
goV2mi9LrK1j4R0kGYCw+K6BE5wSrfxHjdLCaYbJ1pIhS69oFM6nwnUPhVkWDbIzLgsldxaVCggn
r1Zwu0ygdxRVUlxOD07k5+DPoD2rRxBLj2JFTU6YagwZCyntOmY/1F8uUzNmGEq4ba6/JfujFDFl
50V8iLxo2vU6XRwdELcPNq6J6ZCrlfFYufagxJaLjr5Im+cq7NZcBNqzS1EhbTJyQ/XRWYOezPJL
I/GmZ174Z4cfjWU71WIte68L+MJbPDa9ecxvxXZv4ltWeUl7KPy5C/ewanr+mfPIf3CkTpMyNVGS
kMDyR0VAeS8cRMBEHkhd0Ie0NTWwwolmypttyWJOPEaOSZB75q7U0Igd6suqxNMCaEwDzT/YyCF4
CG9M71sZs58B8Cn58kkO1TaPsS6LR8bZcW/6RBcHLNHsBtP3zCoebv9D+cYpGFY2D4Y2MuRjeIuM
nHPyb/519wnNDiCSiFgebVPgGjbgqEamBnAiw3pTo62AUu9u+Nv1t2r5rszzfzXy/Elva0xhVmHa
H0PP4Qn1crk7HLh4/U2pj2jfCPQTdQQjvSY81LHj/zGC2Y+kR8oKPsI1HH4rhxeMMy0oMJBmaSnL
ZGb7tKjPGpl1bnqaJyb9nKJU9Jz3QMYnDFJMBqyCVayFs7LkWd2bx+l/C6x8LuipKTGUn5rNYif1
7s89y516i+Qhy5JiPFrPog7iXECHaOJs8TYJThA+e79HghSRjPuGluPZgXEpjGroysnfj7kbUcrq
++XZFLcHNFLdrIuNWMVypNlLTZTG6K1DjMwgYKTNZfp+JKej3gZbDRBbnbZJBtivGsXZQPG/SQIr
wkU1p4z9uZKAM+PUX6U1pQEJQipX1C1oJYGY4YbmSs6hrtKgY1/8ylQcgGjA8l/qshlh4Lqyvlcz
dw1d2hdC77xRziQXGaa4ZgWxGQnEhsBxvr8S9t7FqQ+4kv0CTju2hcGIIIUmw/TUO9NZNN0/vnvw
WLjCQaQRm9trUpbS+CKS+j2PdXheGWatkuVGyvEH6Pxh5bjq7eWXnmMs5BdBEN/XcsJ8xFHI4v4q
zO6jExPNP8NHMy0gcRmj2WtC/OXFZepBlfATBKeVvwSu9zh7dV2k4JsqQLcCz28yZRojAtTOjJRC
CXGqlkgTUsC+MxLrNxvEh6oRlYRYduE0rpwY9GaH9DceyJo7tvryFngmHOa0HC/iwYp1JVIc0IAO
ttICUnX9lIxGMmONX6qN/+Jy4Z0fvLI4RSf4cIdrshnDamSLCg3y/3SOqoO+GaCDhB4mCPvE2M30
z/k0ARFXKu1L8EkwxbMEWgrbpUWmYumTAgivbxvz8U17dvzeW1LxMhbnPKwgY1rYXGDBH4Hsp19f
KPTWE23voDMRJnZYXuT/Eso1er2Z42aLW+7f3KFdJVQ7GnrDM8iW/7YQgqK7dIsUoD/LoZjc4JLh
WGpeNJ+TBcT+ZPkEdYKxk0uOPXvjwx70LIZq4xqyxU8N0DqR+2Zgs5Z2dfp0jPvtXU4C+eV6vgbg
hyAXSky+F9JHTJ4FdCt2LeTRJzpXaQlKnDe0TZhbCHyYK9du9bdEIO3CIQvTP3Y7ArBCb9Q+FOmG
TsFSlqviTJUaTTrPtocGG31Oh+peLGo7k78JunsQqXHTJGtXeRIdqX7Liwgyjd7e7JrBF5h1g6D9
TlPp/umThLdS1JdYAxilZzN9/o7PhrHVvat0/7ThoWU/NkAoqsuXSKHo35z9QNOxKyvT8p3UGejQ
loQRlanj+Hw8W9BvFLe12PKkQRc78IWxUyvBYdFvIVUVtg5kyI4hj/vrFihAg6hDBLZOPi/0mLv5
xVj6zzCAs1Y2ZGHjYRddU+bBlm7mJZNkjoJlCE5zIz5j2uyhSrmJm5I7x+2+tUBDp9BMvr7U9ous
nUJKQ54D5/71sgSX66NjirxPOcMh7PCgLthHx4/Lu2zwPQyketNemFb/5/KrVJ43AeWyKXXHcDe5
vbQirDLxc/Do03cpxUOiRubYt8wnu1sDyG5kVFL0Q8lRegxOPqKGS2nZPyqTd8HQR+06zNze60Bu
9fvNsy2W50abxZtL0S0qh0snUJZN6RYcr3EfEdcZgM2ba8dSspXaDzuqz7U/npaa+rhO0Vc5L7r1
wP7IGV0e7cPvJxWDFT9A+ZZhMVa06bN2VtGF3OJ++cWww0Kswl7AiZKx7cM6G2VvVfbXIUV+7euV
b3mYKOCu9aRWqUQC+MyHXaEI0ITkhwdSkPa2TBUpcKao1CyRRpUXaW+VH2q0b1inrEXOer7lDAEY
96eyiy7qpaOdBAUn9N0G2cjIrP/PHjSs614K2mtToYHKMl8LsZ2jQ1US0Q3Qi5peOyGUjtGsbtq9
7me9Uuftfmls/+OPrikhkVrrNv4MNUyuxwKiOlqdUPtD4ruG0o09SEMtqkrbH/17JerKC2TCi+AQ
6K717uQm3Lg+lTgZ/U/GBrrQ3P9jKOPj6Myu751ChidUkPR86H7VxjGxoUsgtY3ka3F0DENSClhg
Qlg7+rFdqjiix7oaLrEmCv3Gg3QyzuhUlJBw44ZEHet8xmNU7KO+iUCcdLyy+AqZ0EUSbJIOAovB
wggxNctaHruJquT7weNJBwmXydwvGUkA4YhrYgab5GQmLYZQ71JB1w6nHaZAmjr/yTfSuUABWpyp
K3ln/vqinqgY48SHQVgowBeA/lYGVc6kj5BpyYA3rUzxw1jxZ8EUjUCq5wCFHtxre87PiT9bJ4AJ
oDeLpzWUiLGmRrLcpF+cxl1grHfGrm9jSvbSjvKeKC34q3/5TEbyUTm+GTzk1kT/dU1IMmeppzVz
J+jvqDpcpbYpJuUZdNtazFsP38LGEm8+MjJp5XjPQH7AFZFOCvSaoH7l9H0cgFQkSAfvxSPORcMt
C1KGn8n66JWoNuocre+ldRHih85OGhf2zcvAxLo1h3pbJbAy3IHRYXcyRkghXRrAodIhKZtyjnGp
Z/yFCRRRrz4lgM1xASXPMjZTpEIttKkqgfanINM+oKU4IyHLN959J1EKhQbqkClLsRf/euDGanFo
LQDTCy8mkxCx2VdAQPvZOlCwGWFM31RnCXB7SG29XOw0Ox+hdb69k6b82JdgWQepnw9+pi2TZM1m
RgfpSRWSeGjCUTaT2xioP2abAyRDYHrEz3M014ERy67GIydWfSydrKLRoN6TTmzjNTXZuIuf0xmT
TC+eyB1ZSkGa6FinbGouPzggi35nQ3huK3SaVscqt9peGhXSexQ4W9kXa9es3Scxe6MdTzjL6JOr
PR4poQb413oQiSrXSSW/jdKAVOgqhUIyMQ4D8oCSw4jRnzA/zIyxPRJiqOsVukmRVkogX6NnIfD2
pVL8niXZezCgJ0U7wV4Hj12dkIjIWD78Ba4zF+LsnX+wEvdKai7RdoFyPFIL6V9vnAU/bOyVHDQu
ZLjfDdiNV5zgRlVZWZdVdKYj34ZzppP1Nd6Xh5NTVwyUac1P51K095E0n7awsa1Km79wqugVcOw1
doUBzCDf4tnb5ZgcG6yP7Izy8H1jfhrjTVrBr6RiSZYWQsFXX9saG0rxXDtWCpQZCJg/oo+3Vc8G
pVSeyHKSZA1RBx9ebEA3Me7ijsfOhlTbVgnRwkBi6bdTEmhoo7u1mVV1U93AUIUZ7ZJvBrMOvl9V
Ml4/ESbIB6Q4CibWU5RARdJo0pjCUz02acJQzoPjMYpHOuAw9y1ykhIHBQxBX8U3cH2ytJod2+Bs
hf0nPAzUJqFc0m1SZOyX3pAFMRqWfqrvf0VdEU9QnpmKi9kdD4bKXaV7gWALX/8UF3x3TsGhCXjv
q97d4kheBoD1iTBxLmRBhgz4UOyIRfelqYpBiz17M8PXJ4PVVcBx9Py6sq9IZ0uA89jOTeLLN36Q
p/ufq/0C6fXGW7DqLG6FeLiyMHi7t8f1BuD/1QMB8ilZuWI0yBApeauTr4J8BkX0X/6eTp5w8UXp
tBPYLbz0vmnmzoKcfE2RGAfQPROFc6ayGmYdZAOVS4HFEwxxfXOA5OUxdfAIkhEFNXg+650AE4dY
7z2vwplvRpfLkPOBhyKJXiUz45OkknI8S+JJUfOTBs8zjM8CWldKo0jBfad9T7aoE3u4nT9Xzzk3
gmrPFm3p9TqhHXSXVeDvLe5xmR67nF0tZhxM/+bK4H5ugy570kNvEE675Ijj38Qlqej5sLMmyYNZ
M+56k3IuisVJepIKpm6WorICaCPUiSTlnApHiJ7IL+K9fG76X9M2h5KK0QTPhGJ/cXhU12Udx2ii
bwFqXFWL2pauSn8Urw3zZUIdtkK6BkL5RdmabO6qHdA+4F40yvTWhU13piaypIKpizFEsMVQoaqr
iKAYxoO1e/HLBzSI3gD9HivpADrYPAIty0bqt+98Lz6Z9HugHOqFmcqqI742JZSrh8NIrUF+fYv1
76oPPfHstC31p2gt1yp7fI49UG+PgBBX9giUYsX51KJwNXCFwZmuZYYDnnAIwO6/MNcHX1KGvTcb
MPZOUFR6/FpmFGGR2YvjYofIn17a4s9G+cM7eu5OliDh4K3+5RaiL2dl+LU9Frnnh0GPti/qjAS9
klU8pK4sXYm+I+ZnwxkE8sKz867gikBxW02WAABzGHZ3lf8E16SMyMbbQu38O+M9VXgklr3jv5pX
LcJILTwd1J+bIxkV6HTLjSznaoY0fYU46wuXOK0kST5yC+gzYjUGFW9O37l6g8bo9oKO7QTeVGgP
QKXc+UeGgzXF+XQ3ULhOOvw1AhjjkkFdGyXbkqQ6EZpBQntVo2iT1NRSjfzrgZgHi+UDMbJjJUvd
yWbYzdpNMaQRvGEbXFy8Pc8WbZ7NtGAa/DlM4LMPIoEKyYCW4pWYseqXwp7dXfqz3q4BANuPaOm4
UU0zxWzSJiMbH970Ht3N84qOBARI6U8WQ8uS81RkzlVGh9xMKaCltvy6h5eQP0O3RnpiInCKz71y
i370h/WzNA2dmi7k6eujONLAIwz2vnbnV2fjNOmt5vtW9d0n0WXt+2ORpzapgZW0AWehnoS+W+Of
rdBboXginJ+NA4KSDFJizqZKkUVe6L8VG/q/genuF95UrcH6Z0oOhR7BCFNhOS2YHDEceNI8VFxB
3S9B7R1NXZaC3HhSixdkvJ1Q6OhZtkB3sHWA4nnnQNPBmAQFuIaxfuEJGsq6VodCTWyHfW9W/tPQ
C873XKkcSGy01eC5JF4wHb6DFd2eMy+5kLt7Z61ghge0u9vk0iGgxuFhlQtQvwvfMs0tW6YqWiuD
VsideC1pYY2jBK9Dytx4lEffAWxLt/iwzXjqErIkSXWWlsboKyaIjMiRg+gOxpF/1/ue7L7Zwlo3
5i/m8xOYvIQd27naR40xmrg2iAW9f2By1JG25q2zKVlpZ28i/spVcwlVcFbZn5EaCUB+OWmXsp6c
HGZXXowdmjEtpl2sZUP/Whtx5vmjRsP7AGag0kLo4ZGDRzW/mKNqdTI1fdAZheNUVJsWenPgUDlR
uHYAurA2jM3I5C7tzFd5ere8OonsloBdbk8Ty+Sk8ZqtSavQ/4Ug55v0iPLkKlrGVJ5V0oXWLFPJ
WW5x4nfmmHci5WdHrw1Yrw/Ttf7I3K9aKMfTT6vZDQ4gYU6MQzZYL7DbVHh5GGDw04WHtoASn6Nq
ueG2v5p+Vdd6QdNVXHiyEPy6nIsvyZUe69748w1HGqrnz7e1svdGE78zfM3KgBUCnHsWjwqpURHa
fS3ANJF+p/6Lpf/leQU4ndNClCABWUF7aS3pquYRKNAK2mEFPpfoT5OhoC6PFf/TzbdbhgUgjpXb
oCnf9ngSoCQ9GS0hRBsg9BaucQOpjZgzKuikkDFgoJYaGW8hPWV+EwB2cgRnsosUi+E0d8jOY/lE
Y62Txpr0WlfTDqRSXJYNfmfgzuvJ8BFlWbSvr+LEMXuHzyRbnwZFbEh9U1sW2eiblj29wLY8Mi1U
OjtKDVP12/QYGtYn5l69T1dQNY2QzXUMhrDdfiJSqWVtHxAnoGWe6ZWz2AoATilJxyVxFq23G+T1
UvJNyrhR+v6DkUhbkLdHF+uqIaInxCGtbdnjTufNC1BkG+NgOrdgjCwa6GQX+kK3euwhZ/j8wpqI
Dg/FVErgV1WiQ9I81i0HZ1bgxRDFinqdYOlQQzILETNaeA/wy9eOKfcgYXOHhwLR02gFnaVN5Vvb
9P7WOibJrBqkC2mw2JiPPp5+CtYf4MFHYlgXoh9DIlyDH0cIg+Z2dcEOBY5XcN7H6Xy4GKq3lv+j
JlUzhhaviFRS/zDQbOiZzoSemiunq2y419E7qlrxRbVQg3wL+wM+sOXeF+YmWqqNpZIXERe+QRyU
oPm55xJK7zzfFt+xihZqJjZ569hjsGPh0/WRCd9oW1OI/KonvF3hIxzYZ5xo1PdISc6Zci8hVjSJ
opIci0fHZwqosLVlPGko/fuoQ2CriujVbL29AAUXbw/50PCstmIDlNDKtbEFzs8EaQbgqerZdvZT
OS01KFA/mxS7+kuwwVzSlUWTs5nuqhpM13R68En6BODn4fj6ikHRAk/r8ifp68pR8BFOOVdx17gH
k68/Mguli++FI3Hd13PfCRXlP01qcb4xnX0OYaACKWNdZdyGps2DmGXB8SzjIe2UvMYV/9Z0VVju
wQyYaUHFw2Qq7J9W6Cg/YvHN1F/jxK1pbHe8uJNmrWypx634Ep5LtPEMPBIKzjQCy1gHeJWslKne
rmho3LL7coG5uNmRPtmfTch4DdRV1luXVCWYZRByxNn4nBso9p9a3emjfm42+3nQXFuQ39KdleGs
mE7d1Wo17TW8XL7N1kMRvuiq5HyHo9GAl6rnBCpcNJf0jyHYcV64MPNyI1Lf8nvAQoXJHcEzmPm1
8rQPC+EZFG2AqOYB/e01oTaGprkXAjCdtnYNtBCh1lZA2iWOtf86RoRlk3Hv0PVLXldweKwiWiZ4
/dmAPDfDW3AXTFP5nW247bWfQE1v4z4N5HDvz/cmGFB1LoXuv+1dae0+peEO+Qt6fdyUKNQ+25w3
Twv9F43uUtKU/EvBKi2EWEKEl2GXhQK8WAfYR9RaXNFZf0pErxrNA9D6KCvPX8RawZa6fiA0zVhz
vEYQB6K+XdXbqYZkfGtwBze2ZpElc+P7pPwHZ9+8YH5azqmPs8QSQvQ7z2PvCwmTuf0eHoiXRRLY
7kZoLOPBjGpGNPW2eahl69mKywIn9RIVxsDlF/aFvgAhREwo519/KET8igFBVg6Kx8sCJNJB8043
jYonpjQRUQcdXIirYQpnmB/atqDjfKelj2352r+cBl/N1SGO3s87l1KrQhXQ8OtkEgGWdYy7w/2L
cyVjBFllZKpcEnalnshQ3ynsNBG4JMLgLaRsWdCPj1WRVnlo3UE0z7au+NVEZ+t9cJ0mf5Nnm9/C
Vu8pIpNZ6lTn//nqfcrnPKmpcT1/DA3Up8gyF6knOfiV3m5q3+wFQmiJiE5ydq6nquNmffY+Egcn
TlYEa3Xk4P43+FxXP/hDhOxWYVIqcavPwcBg/bB/GDjspRJXudb3qww0NBRKGSWokpjbMuMiqJsg
Gc0yORuwwWzOMpp5wKnIV43skew2CbEkVu1DyMXhg88hj7Xhu6D+51g6jKGxNi4fwoOLsajHmOG8
taxF93AJkkyB6glp0HbrR6aKfvFAYdGbuxPJsUc3rmU0fUj0e3ZamGEaA5Rvyze1N8QSxUuuuOrI
IniuaUp38W96rv0bJK/e43iGlTrIy+aGlTUi9NjKQcFW1yYTuOPbDWgoYQoa+6jKJmL539Rohgn9
iI+rP6UnN/YC6vIWwN7EMN5ptU1M0RiYfd6a7qXa1Yfl2lrcNshcKmmfg5H1FkmNp/Fct8mdUAg1
b0aU6taYi3/czcEayQiGBvDyL8W4uzX+wnUSvJxNhyT98z5Vzg654H+PzY70BnOgBqtQxXcWNhDa
o4I8XiHRnAB/0zdqXR7mTs2yGhcn5RDU39T47StGiP8ew1oikIr5Hy7v/PM3+Z+Jq4/X4y5l5dc7
o+v48gwG34mNGHRJSqzNmFc2QrtDzoCBHU8MVwZ5vP2UXV79gsasySulBB7GVer8W8q5+nIKXdjD
q5KdO8wwbUvmG01tepLApUAKglODtpDCCJ0JllYy4SjAkjX7AHCc1o4f0xqi6pSqdkSsWfwsd0CH
+wxV8+9MZtB/k1W3969OcWBbojHRVGubAteHXmBgZsgodtjBVLkjKhAemaMzB39NHTSd/FG05C6n
xfcJwMCeYrXywgAzG+rVvBb1B8uB874sp5v/kiBKan3MtaduHLcZHTdBuq+y9kbtESxj9xPqhzKe
eHs4z/klYBr10eMaoHsAKa5X7wwKPs3FL/0vwgLzqWrMQMVIlFMLCl3aKz98OmenyelwDAlOg1cP
9oyWlRBQVmU9El6+06XcQZwHl64CpIsoZ6gQlT4Rnm3TTu4Tga/BxM9fF0xfq37Ui0Qm4tEp9OTp
vYwSHf5UTbhD2CitV9aMa7WFcDTwm6cY5EQuRndrRgKwXx6peb7VpuPOv/XvI1F4nmY70n+94oUo
JTZaV1hkE7kLayrvUjX9YPu5yU9xc8ER/j90oZhdVvvoX++nHEBYbLmwAWzl0XY9g7YUp/gaTN9/
G3BEBv14Fl8OMd9vmUcu/TiR8eIAqnJr3esMw7P0ehO0Z9/jxf/uBLYAalupV4VuL5FtXpcFSuQE
zVU/1U/cuNnTfnINu9hxxLnopANRnDf9/VbLQ3B75zbF3ZYbXeTN3VF3iuFvLdyxdp8yta+FMl/7
GVdx8Ua/BBOX2atNvSZ/z+JB0oJYXwCYS/CSgLkg+LjE5kxzNpeXhcbHkHTnWvihSw8MtZAbcwOt
g1APbJs2LxAQOj+Uhefh5lixF2YUD0be7uP0TiFmIJ+PEuC04fx34/84Ov/K7egYUOSeiXoXFW78
kvy3AhQgYQVmT7A/FP30Cv+295dpCJb9yf1buUHxzSdxJsSeYTKpiZZ4F37IZX0697pt4V40iujV
DODwbKBgJMYprEOCQV6XYOWYWsL9NSiqog9+7pf7rIY1PwvSw6YXcSLTcFRQuLwl13WdEwrrG6ga
hzyX6A15hrlI/1M8pUn/RmkA814C7tv12tpxKVLUuJJgzQBnyox+lGsaZeyF+vo7jdCPbgFB7Jwb
s3/5TWbmK3zpTIxZmqug9LIpvkL3hBa6XNy2xAOnIu/DVb+QdP0BCoV5LMwma3dap94U525KT5Km
irELHNIbiyEZ8vFLeTGAg0TSXJBF6ju6XecTHAhWTHiwfQK4ezWqjKLY3nid6oznkJW9nj/K52Kp
okxcdDyk9/nybus0wpQKICThlF6uoYk5l6fxdbWHEjQCCg7V+ExDdhyYcODfNEVGyw+DFRS9bIzk
VYtlu2H7vFpokktzm4iiadCg14g9zmre0VWt2phEn4PVtiVo14gkjWxH0gmue5ZLpxDDOjlBNXCH
9A0wpRc1376nZa9AOqsDRRBGmFZrdmtWmxtswNr3HYZ14pe9Myy+ZCqObOkrozxNZvPO27EFcz3Q
Fx1l2obz5pV0WjLf/cAck3y1WgLj1b8zq/lrentZvQ7InaGz31GmVd36ZouJrsMcAsxNIQGPSX1W
9fr3uMVtLdfhQT4NxvRr/Vv6v0RbOsTEWpVjCCdS57pSE5sY1e2rWyuz9uX7gmfSXPkKiEXPGkx7
q3ECl+wnk54AjckGTQvtvdMsFQOd4j8CgcQo9LiHEOOsc2BTQzYosN86MbYydLb/R8pFRV3lQPIn
JofiriNtNR8gz7qLNN9w2cYMiS2oNuJoS53mKJgYwwzDT3OUh71XgPUmasrTsFkNOglH9+qrfuH+
GJlOXiZsJ9sl2/2U6iMSUVPrkZ1F76ReS/rV7vsvD5x6pwCJi3OnQCOBAJEWg9XS76770NfcPbF/
4JtSWRY2KByS2+eORtPYOqIVnn011x0Y5sDuPnEj98kRXyVBVHc5pK8qoCpQa1JYy4oP4iTI6xXT
wQeL5M29tnzUknLU++zgHAq//zivWYrgOqRDtn2TmwZNxPj8vaLKA0P5cM04FsVZjt65M5V2ZnEL
qGa2XoeqjPyJraRVV8JSRC4M8cVR4MdGyxXqWrpCBYSCQVR2LMv0TPlYUlPhK6xl+DwfTsEVxv7r
JGfXBA0nziM+vbFIX6xUTfkaLLqh8adwF9XmFTonRtuFbA6CLULc6moquEQyYMoSfodJq/ydZ5YY
PJ/wtVQXWofASddE5n/P3OX3Z3Z9XQIn68JRMZ9SVxQsdW909cqDg3I0LBXV2rUfQo/3AuP7HxNs
SVoeGUPx/33+me9IlPZ8BVXWzaAQSia2S8DpKWAogIdv4FhdRvpLlUlUGCKt85Sxq0nqA+jKPsUy
YbX743llwj1wo61dHlkIhe6T1kyg5VV1XP7rnHtyZr8gofZMs34L7k9oQIr2A1EpajJ6XtBvMsKk
BL8kqxRHkTL5fZZIdeperyyYS3b1QJPbep/QzKipHXjy3M7miRRM029RuffjDfrjrEkBGLPGGUfh
5yL028JgV9c8wUeNBLr16w5vLLG/2tXNk69LRjygz1mdOVdwBxTIZKJzC5X48tIxCw3FHGrEIo6S
VbqFUcyeACueYiY8jx27YgvxT6iwygti8IuU3mtLCKw86zVPBR2bjclWJkG6oZmOUYXG3wpaObXd
rh47TUCUN5ixFxuPsMuGEZNgfW6Egbb7OxaLLUSpYH9wpF9d0rbDsVyoEugvEnZlQheUhd+H6dLc
rTCmAwIRVEWQ8HQyxz46qIHPSorGtLcwhg1AWhFbLdrbASZ1osVOvOVsQTn2vEUQFnKkBkst/HRd
gVmUYGpTcVJlP8PlaWvyQxoTXJo3pBKt9KwMNSTozhMDe2u9+xdhJbC3NvpYhSveQVP2Va/MivKr
vSeWN41bCxQsHD2r6Tvn2l8G7LNS3ZERSz+684ZS4WQsTzDecN+cmLI+Fr1LAoL8vXWpivGJLx73
aINh0dC1SgbQTjrJqdtem6/o0J8ko6wiayKAK7T5j5Y70eqVgd2c/IwDH/SG7raDbYoePfSEY9Y6
B8g9NlyKgzARrAxa00arigTuFbUCh1tDYF/dIRPzGBr5sFRr1BZQh+2tLZERE/BFyJ0cXxO33zTs
9OEPNN0V9caw+EJCV/hqAvlzKehS0nqQ2mKyk9+mev8IfAK4PEmV4P72NbNtlw+zgZlKT3i8FPZ4
XhW4GjF8lZfoInEQ5H5Pupr1f4H3slY7fpQIDLM7bRGyyg3g8+qbG26r5GnfrSKrYlCTmFtb69Wy
uft8ud1dXOgqf+8h9BJcH0HHY/HFG5PkLWvIsil7T6i0gU+cePlos6OIShVxtTxDWSDNMKZghX5q
CcK2ehuhLNhlgCwaQ4XK22rx61TRoVQvmWZdnGfqD0Ug89vhNDu+fGEIQ5ReslCRtNAEvw8I/NQ0
atWbBUtKmjpYUXQfSAY04ZuC1DZ0HyfC/+kxtVejXnpSJ2fLJpS1PmikViIPpuL07kMUscGp9plE
NEd+0SEaHumJSraS2WT3gu78blXIVR4cAJfnN8r27oBTyszsZfbbVcoY0QRD5c5YIUeSzZSCK43D
YPSnI8CMZpzOypi1y+WlXU+FO6syCt1cymfQuTn7IXiz+IRf0gybjceao2C2Nv5M3ME8XhfWoqE9
UInSMzgub8dgDB5yrT/BtkUAUka1jFbO4yg4l8kecw4NnMDZLJvDLgyXgBVAy7+kUURzcIlHicSE
RNsLS4pX60J5CkOAot6cNscLcbi9HzftOYgHU8Q6wrXv/0xx9VXGL8ACHv3wg2jP1gIYRMNJT+UR
Xe9OxX+nuRdjlrClE5QR6UrTYAXkgeHXyemj0PFjxLJKajMMvl3u93FHiIb2rKLbrKbIE+WoTI46
CF+dwj+PVt+bO4bHO8dgGhuPJwlRFBx1W7dUpZmu5sMso3TP3fwM2vCbsoaJ/rYC3ot69wPqZTbR
kINuuX0QQaLyTJv/OQ7LGr8kioXUEv37yC0W6XFDAWxfPWq47plokmZqHdVi3qjoLjCIWz6MtGfU
pmPZKCNB48+NB99XszpdVVi1Uoo2/YeW3PIbYy9SekKmOYN2RAmqNMaI+Gc8qN4JP7Vi//UB4nKt
rj8mBm1tLHKij8cM7tDqNg1dgJvByW1ZH4DseOrGe59qKjju4Y9kpq9YI6pOWEWJrVyn559kwa9D
ZjuWpBIPe1SDuOzjyPruzwsDAGyK85fTimFLcrDFsijE1aPdqKr0vSnWUNAOSjCEOcf3mh3dVaal
kBBASZobKx9+wngoUdHPDQb7N94GSPkz/JUAaQS7b/sJyj+CyXp9kbyxBng90HUEjg4ERXq2V2BD
uQmWGw2ZlQorj5oXM5SVPz6VF2gihhVY9r/OWE6/aFlhjaE8QinDDDUdyyv1APhoLbJVTMP7FIdB
GWehnjVshrk/CGXDVTiKBWmgzWvxKDQPvk4cO9ku/zri/y8fqgb3eLc2bE7epVcVyxV25GfseG3T
My2V3F8mJzPODTMNhYhgnsC5Ydo6sbrzw1N1SFbt0J8LM5a8mrA+dxQp4cy/wctKUeXSIi7NZB2O
ZH0aulZhXmkDrSQWXFhAC5Y91tFWnA4PrMbhWT6WEVJFUu6pJPVr4sDpLJgN7zw6p7T7EQC9Gbkk
NVzrL5xqQ3h9r7GPZhkjP+KaG/tpk9v3FOmkocartVs2KIVe31vto9MFfZyNhZNGLPK2P4MnA4Z9
3RENRN0efMIWNcAL1YNqxo3uxC+luOsojq0fLwO4r7+7S3y/i5j2mlknUrX0HdBxO2rBZ8lTR+9q
H/weVzByKEbJfuoce3XHe4RL8y6O6mFZpHQTUFnZeUg6cePV6AMmaujFA0Br9d6l4DQMjEJoigH0
MQafvkxliDWiwApI4elgWne4HJlL+y2GDiVbBPHXUEtwbkWkIWKO9v1Kgq32V9xFA309EFfqgivp
2fK/hpOLE1FLf/KwAQ75SmDdx7Ww4TLQ8maFH8sESKxrcIrfhqox3L3AOtegs1IwrfuzlbkwnrRM
hjUwNHSn1V50xa9Z+DknxO6nrGHQQq2+t9EpB/UXUo/D3Kr4Ho0aReFr/BJpQGV1ZSAO8OkbgX3j
dGwj2Tjd0DLlA6oC7CBCOsgHZbzRVTktHGVK1OKTD6hbF5aXkRflhPqbk2g211VFLg75depGNlI4
SBZNQryEHx5FsgwEtduVX9DKRHWfORkQbkO8pnQO61hJCvzkXAZLMMJJKnXENp+58b1SWZIEl4fv
3+dFpG+wI5HYnrdcKxYUKpcQfNfRqBjEZ8d9cSnD5yqvfD0j07Vdljm+QZdXNjQpaY8K08310Kfg
lqWWi5ToitsXsGBoArlxQGXjehpcCLZ4a09GwtShIMSB/9l7+H3pndGm4dufbNrwug9Kb8y9iELJ
O5yzJ6VW1ADaU+PouKzSL/b+JcaD8Ez8lytXaY3IkR37hC8trWeGu9LwYJAs+0gUbHrGyWBb0cJq
RrEHhFjDSGD0mspaGxCt+jWhwIuSZMOQ5b/P5uFfifkokQCKOAdqPTX0/wy9MHgN4o9uJUH3BV5y
TLQcvsWizbvFbUlHf1sqz47ClVtxZJnPj18rmluGiZSSHCaOVtJ+o8FRhCeS5KeunIbU11b+ZmBY
qSVbYrn7po6XQSi69SXJSTReDqeD6adMhhcpy86H9jAA95Q8/9kK5JM9faU3wUpMVjywUzh0vLH+
vZsN2p745Vyn/DHIN9yyRtKjZt2kgv9LYluQ8ZVxTf19ne3P7Uno3mj7G50iifObDK6ecM7pRWV0
2nKJ6jW28B36AFUvXwn4vfbfe95UT9qb7guH53n4eiDdy55u7xxHQhI1O/waIP811L7U/gVRp4Lr
k6fe+skfxi1bn3kzilava7o3mrH2dAuLEVwhz3hWsdRdH1vriB7+BD/yr2nhEhIWO9rVsa742DVb
1xfAF8SbKeUDIMWzPPuwBGJbFVUjVi3UzQueZVh7JnSw13sXzhbfO5CLpShwf5oKb45FzODnUZen
wrQFqdaxB95svxX40+KbzDLfHecgct2RcwjL7xYVjDAtg7G85Gn0lwIIlQ2ejT7wWM0UXwB/t0Ql
0QuSIO3R+0tv/17Fen/Hzxs+4zmf/1YcWl+TG9OU/poM0kvxlLETcueb2YL/JK19zWJ3xR9ecmEV
PRClYI2tJDea4DutLXvXx05HLkUBQvcAl+m3csDCSMSCJEr6XMz6IfFCo3VqjHPG69pmk0KqJADD
HWn6NCLlsozd9X/XW+yh2O4e3DbGa+KENPwjxx4Bnbhq38+NAqczIIcH5T/H/LWBJsXpIzs4aPnc
fMqUWteYgHMra0YM3sxqVQq0zcM3Q1FQBDkkCclpCxEUF0cUUuJSZ5Oi+vmnjiO5wc5z7pNAgBdm
HiS0/QLo8orRLmUWRPQeR5i1u91X4HhCgLHgpahjWK4LluNu04X1peemkZxGApsQ/MqyoZeVxm78
tPf6slDi22METDxMxTnnAcjgaXb2NXt/5kqlw7aDesSDbGMic2z3YEXlZhK9LHQCDAWjMregreTF
UorM08jNSWmGMTcRgAQrHTtlIYN750BRu1ATzTGgmQs8z22kaJ2c22vuIV/h9Z7l1mBD6UwF8/+v
zysdD8ziaJzd+2yEAZtdSqsvtSo7Z61zjoIu9yreR4N46ZXkMmMWgxe6Pfbwmt9jGExb28YYJKWk
9dlzH80yA30foyF8KG1G4vlLFnqREepVyDslc+DrCdR7ssCa7L7uIjOwWzDflVIaISyjb/KDefZT
/g3TZgQVVRYPlWUhHuKMFJMtHAq7RwYweGf8t86jOUQ0PDSordn5CAGccHKT+A6WXas02X7mUWGq
Hjfc04ry2FHJ6e8GIxyphgO2x6VmLNaPVwKyQu3Z4ykR/jnzF4VobxcZFIRkfU/2fqvD7/l7rKSi
s1GXixbNoibE2zelmBxLciIsfW/1eW0fMDSxo8JoKdBTKe6JR9e8PJbirdlZQYb9YSMOchT90hRC
EOVhJJOzmP1+cJGYL1C5DjXUPRRpTovw45ScqQAZjesyyhk6/s+P/WsjI8fqmG1FHhloJ6YT+5SV
zFt2o7x/PUoxfLsbLhRqpY6sYD4sdY4uJ8xGU3jGkpBMOfhRKmAeMGWl4JTppHLpb7gyyclaq2Ox
u2e8yCpRZez+7kH4G+0WI9WrGZntZ/8eox3CFJbtS8zuQjqd4xSy2UtxUubJ4zoo9oboIOZvKJaS
YPJOpndbUH/j6nGHQJ6h//oOWN/76/OrwLhTUvrUd1L+QCSffDQmXJexNDykO/9X7CNxB+OdSDrs
NGC14LN8cKKXwrdc7b3w9RV6IlA4izP4OLX5ue30sfXK9u4I4QaHoS118iXrn2wMOBOj4GFhNvzp
3cd57cENjzFnu9lBZJLowDNdao7eQ5P/oQ8B0+2jWFaoHQl77kjnwiUSlQPPMjWvyUD0UyBa4FpT
cgu10EFlYhnjwzF6zEnzcvTcOtVrmfVeoKXkghpjFuulalYaGyT8D9BA0FseO9hjPV+KB/Ftoek2
3b+sBa/VcSd+DOUqalazMePc/H2i7UAuaSa+sj8Q4/hMX/WrIe8ztDVdbVNvXGrAI75ZkcmB2zx8
ysyUptAjE2f5OIR7hVJQA2+zTpIAtlPDLqyk+RRhrwd87bi0RC1akFAYjlVbAdly6bAEhbIGsETg
RnAcgHyOBAkKiJ2mnEHnC4IDN6rJEw2OXmP4Ys/p34H+IIJW5Ts1Foy4OCfkNgTX5+74Fkp1ufuM
F3cFwbQSe1+AGuma/4DBvuoLxdNh8e55xYCJdaiEvVH33WdS/Suc33npcGQ5FkhWjg055FBZYT9E
6LJdNYDe2ATiOEx64mLuTQfZLNGS3noP1hX95WhWIXHFwjJPaXCm6ogfRSB15hbkzol0SlkVVcFP
W8ZRUufk44e32UGnBh0Oq0naFJ98xKtj+nSEhvOHQoq+2gVYHh+U3Qlnt1YZrQ3oL9Qf8j3Qqk6Z
k2B55d2cK5JlHPw2lKphR+ZvWBwHMfWdF5xhDl9GhqqWotGihQKzIYGgz+iRgLIIPSXJ4tPsMqj/
I2KgPTHvgPJczffwAp/P7iOTqZZ7GUYEz22k8Se1b6N5MFh1DL5YLnobB5EkcTTz1HYUMSXtqtfz
BFz8vFm152so0Ol4iYQcMO/65yHAC6dgxF0QGYEE7vIrG7xmUZVQfo+xilJfQmVV6GhwABe8p9wb
rw2Zb3WrAyMieuAygWj+p4sFonCl8nfqeTTSCa1vDNZBSArwMWOUsiT4YESok0aOTU5Z5nMmYfUb
3oAxqXMHAbST4HI2E3POmI2WB+FrGOt9cQpm0deoqw7RwLhPDWB/gk/HNaEawvaE++He79GB0Evq
PYa0ns6AlDcRAWn0BVO9MyXxSQSlLq3ogEzyDxYHhUq30BQMPDooBYewH2Sdqij+POO89zPtlwLa
dMY85Aq9khHsluR1dYWLNGcjbVaR2XzS0VAsnoz93FEnV5bucStEkIr6ZgLjn2w8FykJlV5+PYTO
97YouwYL69WXPxJWer9RQmsXQ3cLMf5pAgQTB136CDyr+c5wycHD0OXBGhqo+6WmlDzhdijsgII7
N8JTXx7Hd6fmQFYEPxLa0KeKG/g8lWDtSy58rR/dydjjiM0EElKq4iDAP8YM3rhh8wr/ck3mfJwh
Fe4i9oS3l6pYQDEJGiVyNmkOPqlKDz+x39FsVfIJ5Htm6WRd4Le1H8NqQDUgEFWB8qlD3NhZpS6L
82ip5u7YSlN0GfUE+ekoIgDpJnLe2NYuQZT4UAw00xs873x78yZ2D4rHDX7Vg6EkamcPQlrkh0Mt
vPPmHQ1d8oL+usalbEoFPmd46peWs8sfqhv0iUWjIywd2VXQtvpW0mYDd6w7S6lNHpTlKlpOANaz
CmfSr7Y3Ja2OjONuUvzO5u/mh1ZkE8iL8rwk95fAbbD20G55VP5767YxSxTunOz2ncldSCAq6ZB8
kaYoMBzsE1qo86rOaKXsTMIzgN67Sls993ZkMM/IzK9LmfdqyiYwUQemCQ+NeCN51qg3v8GVfeYy
Oqz2C3PajTcy7tyES6J0Wszhd526oFienC7e3T55nFz6jOnH6OwXE2hcRfWX4jVpy306qxw7jr2N
q4aQrTIXkikyx/XLOUafSAlzvMLisO9fC/ZKg2V0j0uXEM3mWeoGsEa1NwcxTU+1ykwSMyOqOdPH
oPH4tILGqBbii94nz20EYTKTmSuuhXok0M/fwDMMwln8C4evTJTQ+0Dd7Jy8uTiXFHwWcQ7AIFlN
M1m5Qy8ENqsCuD6rRDoSUI9flHhFeXsQErPvJ6FG/JuN4ZOtO+Pw6gQ8XwsMl60GlnxjUg56OZ7n
HFdJL7sGleHg6VBIqdml8GMr5rOlUvSbiGPFO2UzWfxWVi5vScm5vfgHOoHZ6e2enRMwN60HJrep
omNKHKLpkGUWbBWw10JAlpNbGFcE+19NwbtyBRNX4NNEyoneUNrL/rOki1KD+Ecsi7RgGfhJL4In
s46i9vmvCMsBAucfmOp/MFuS7tBkUUoEQTGlTNyROiXJDU6bXwaN3rnU6n62tgVFaGWgLneTgVDb
4DLoegl+MDIWkyX2zG1bf+y9njW2R0mMQZAg5OSSVeO+bIOm5pQvoKn2qZZFnACGxTagGSqAQZsP
+59ApD87uyZt+T5h98cYPqPH2UUInrcuV7VvR8VxztBOjjsOipEkKlE+0gBlB25LWS6ei+W2OIxv
KOcRgPd/+0eXJCQeTECt4d7m/1Dg+t+FSV1gZTXetcsCTcKHi5fv6FEtSYR9/e2qeqduihHVuV89
dwWJHiMoCG64YBxPsPV7DypdaE6uOlWaHU+vIXMHVoZENw20t1cXFFgFgp3mlszvb+1bEJbIEkDM
dU/TQMzJf1xqEOSE4PBgTAJkMs3kR4F83sr/NY1hcQYhotjXt6SML3eQMSoVhDTUIwDjvl9PJ/Oj
03hX3jtNFoXzOwlQVh6mlYrL2XcgLtCtFjEj1CpX+XbG4J+EvMIHg9qxfT+EssmDktzPdDzOq59N
ar1NSREdt0ZHzhNI/sMMGCn0SnbQjcq0/yx+GOM3ivOVv0GAW1owVVIT93hmKh5b5PDlK2wUj//8
9C7TP76fdXtLoPlG0nM1A9sESzbBsXI5ZH6tFfJGSLQkuJc40On9ZWDiSa7H//9Fw7868xqISjhF
jFp38aDho8KdI4pZ1u83QIm0glqmLKcsl5RnXXzaM+TPDmAgMb/FS7y05k3o6X2/UNvgds173jK1
GEGTD+YqNI9kR/ZHI4Kmt98HSkaTEum4PxP1IsOs+0YHMETaer8TfePHUKy2EKoF1p61dWh0fV+5
UG2FX0zzHqg+8wxVPLwTCFAMbmJqY7vmMgfH1cTMxFG+Jm4tD8LiXxylbeGGnVkJXTJhn2tNftvj
lXODjloCDB4DR8v6rqef5te7rqm6HPqdscc9ow8mFQWJUTJJXuVHQSEsAx/At+2/EufaboVwNBxu
WSHdgCz8zENDsT8pNRG+MfSW3LB5YCrqj4aaINSn2fkrecL3GGZ18/2mXoBdQWORUfaQsUcti732
qOi12uym/u0X9Z/E1LwFN9Kt6MpchX4zSuT3DfviQYQhUBLXL8yr5X1F+1XWua3/QO7mr3zL5dtd
u9c7Jj7V22hNeBArYSOlU9a8pZ71s+zmSirxRZSyqSL1CeBhVw9lg96UWzJUBf9b7+g0SsQSb2bG
NweHaPQ/E2yFXvEQ11tZ8W5cnq0/7D+GE4RufzDFaqs+njNqySUM2bYm7S7cBFQRN6n9xVUhwhwZ
VyqqoDs0pee+/4c1KzN3ijIaUlUo7bwLYt1JT8hulyR8yX+ucOLCrRsw3PSX6fowaRrMnFSmqOm+
mD+KG4b5RMD7rPL34jkExTwm31w6uZv/6EG83sSr7TvXdRAzYZmuOsH4gzx2aybt9f684pI36HRi
WRGznZ/JG7W5iUMDJRe/ANt4pTn05SkztXzCgPLfm6AjPbpBey89EqdcsqMqVrZ7yPn/SGk54SRz
FGEjSXWB9IG3ZWA498HDhdjuC1V3xyIRbRQp5f5hsKyEL7V4fvz0dxKaROURvEx/ddbA+DChLwWN
zKT8674QgpEDzkldtGMFrHx3nIoF+ahSo/NdOm/ZRWDtyZywv/7JMtrFyBKKMyV9duF1k6Ho4mYg
DfAtstswUB8yypZPYhMbDZEVuA8gXJ0K0pkzozm2ifzs2NlY4JVK0n4ffLtfWI72UbrNAcF665aX
/Di6iy51I7yJq8/6vf75CsJOAJNhFY/jgW4DWXlZNg0nsyWDlJ6/RU/Sg4QRPizDFS4nphBuO1LL
ghJ6PAaNxPCW8VIUUrO1bF8oflF0G/iQkNnmUDFDxd5i1oCLWRRulK2hbuiC/nZMe81jwBJIpO9a
49ZsMAZoJx2TNF92Yf1wxio5+PCJCyASb2CZ/QAAK8vQPlcP2WT1Fwf5CxmB1NUOQ2p1Pf10RazX
T0kaw2RmKV3g8HWxDiJb4frkwAHMnt89MlDATiT+aP58b7QyqS4AV0o5KwrJcJilCKnfS8aysXEA
gtz04ual/O2zZaBSpXlMbOtaY9SNKGbzzlc3uKpHkDtYeA+HKFxniIQPQIwxuiWV8zmN0jySeQal
8BIkMElfuwlrFhd0zBPAvCFSxjFzsd1K8WCuwWbuv/HKk1yAnSHS6li28mkqbsS+MkZwCbLaawdH
M/uUskaF9BA29Mw0FQP4FNL+QR3O3oILOEklMTTz6cC26GhU5Qm/kDhvcQud7nM0RoDd5HLlCs85
tw3OFOO8s9GLqg6q7jT8KR7iuqXCzy70Vl8t1n3PZdUCWcVm+SMa/ER+7e1xd9Q+4o30PZofKcOr
QfRmSWd/9mPhOt0aekFpgYvMw+uq6+b4XIo89v4ihcb624n/v406iUQIQhViSgqXM9hlMHI3Y6qX
ywUQsAw+GcSDF6UhQvzUlI4Fx+TIBZ7CLchxlbYLyo5gCQ7x22Gc40rIfVw5oGBjLmaEENGIIkQV
jPNGz15U3iPli7YO90Xhp2579hhd+B/kJ8kXIfOQMw9MHKYFvo/6XHOSk1XHazHwuFYf1JIc+mCm
UE9dDjqk5hiXaNIVL9ao/rpwJKbqp+lwaaioj5gUmvEoZc7wx0L/Tw5RXVMILnYCG/I4sjZCcsoZ
kk0gaQ7o5NWpjcxa3VLsD0/WT4snLFxGwOVSIq6RAh/lH8Dfrf7rlMwX8ihZogZFg1hkSDR5+v49
6pqccg63niezhYSUTwrzX4q0KaSWHDtPfPp7z6EKJmjP45EhtUM2Dld0YgbbuJqbzo/G0yeHW2Gl
Xz9bWjumonObgvIWAFmxE83MUOCqqRT1AnCLbXGXj8xzoznc5DB+Pxjt5d/aFbMMPfTo1yqUNe2f
FRzBMekRFtEIuFipkMwWbtjAZX1NZ01+K72FqvpB4u7qVcVdx32r7R0z0QS6Pw3BZF5ZQi5Wgfzj
JNGTxosnQnvEXVuCqDpyASW9BComkB4uGAszURfQPVqzYJ6+7vCMV3t+WmHLd75ury/XtuboAfW8
WW0+FxvAxQ2mHP8NZZxJlJbyZkt56JpfyEson83FLE9NzDScJndu7fpamr1XTC5aY6OXIpcoV0Qi
bFIyVQY3P9RPit8ranIeJE8nuSYeHZMOyTqv+1ncEviQgtkewJcUbM8Y3ChAVBzV9byA1klyTpo4
nYM6o2Z6bfxOrOLp4dXdiB8VD5nYEF0fVoO4WTGu5v3sWAttF5PfFKu6mKOF0K+SbJkUk+5n3Euk
CIQ/Gs8HYwNbhGCS1ePhxTj2CA2dEwYUaCVsYEjafF++GcIP4JKXJrqHQwf5IhZnEx7SDepvlVBZ
60OAWvsdP6pV6m9pdIV/ZpY5zH9L+nGqloZkZKxGPRPG0UfgF0YUftRbgl/z6bCtE8mWekb8ebsw
7T52sCFOxAER8FzXnQEoKDPA1XujTCn61DzvCzTgeidzVyNw89ZOgdTiEQyoj2yqUBDCygIyu0+g
gg3JEA9UxVv0niBWgLx2uZc/ERP+u6iLQaGjZ0uv8E0UK82GgMJZ2kIWtpya8meves3jbqWrYyAJ
U9Oz0dKD6tr+ZwHzIozGb83hVjm5WqcFOkids57Tc/Io1UY95A0txtWDXjXcLU8RGHUmkQHk00kb
LWG1V8d6qfbrH9kHBySqDXCe01WoKo8T/3MPyvjiihzcqRBEBAYCuQ/dedtfv08Y3f8G367mj1pO
VM47fVBQaEsC/GGwJuvShyaHWsnZZj4HefxPTpLAG1OJsBUC6bXZME4LoM4etwrUroBdGZqRMfGF
F4I0FbpoLL5gfhDNT8jB9MEgVgfduv7nUYjD75dIczg9PJcpTP4ro0wsnBUXINZDKV5HIHCTO0Ff
VBeXRAX53etu7kBJpCVfY68zPDNiR/6vDZgX+CGxzIu4PBiTwHgW50Ee2LEXPyEABkmL0IEGX8hL
fJR/lZ7AbOaU1O7ijFpJaLUixosb2g8KaC1AI6DOtscAaJ+3xFscESMgzLjqdYu4su404lAWqAq6
lIszNTBw27rp5byVutxKf6WdUi6etdD1FHR8qBRuoLEufA1Hmmuoh1xcTSHt6poJaKJ0GWIWTYum
47Uxvrd59dCyT7zs/HqqSyuQbOM2u8hurAdZQU+c58U75rRqE5WUMvPoM9qw3D5t7Jas0T4px8yy
kvuM3MDIYWu334p/dgC5RRy5WMZETggYf3hIEAog0uiyKNhYyjk3cvmVwtuBAVaMT6L8im9/l/Ef
wi/pzpC+ZyQQMAR/WmN54RcC+VfC8wU709YW95YHAd/PGBAUs8xsmgHpxGTNQPsQN3P5n1zMOo+P
lcxBwK2ukGdbrudsipJQMdwjL8krm4L8x6SeFHvV+wENVYa7PLymIAp//sMSPlDEAYNf9kWSBQRy
lSJi0tS9ugJGf8VfhIRHd5rrZtuEsD2VFC/wbury6Yn1Ec6/i/89+dSwiadHWoHpSMwMxHE02l1x
wg7Pm3r4Nb0dPtg7iqtWK9XaAHC2vel38YhkENVFFdzPranyPKKLS/AcOVUlKrCVt7MyrDVKTicE
GoUgg6Cf5AQz+bpBClLOaoWHzDMt11+xOCCI1avUfTu/0Wx/SJgM3J7HOj5u3S43jTXvmQk5Qig9
mDR4fBkVzkmpoxkYqNBp+hHzJPe0crCGCw/UNpS5DtnTwTVme4Idd2iMMcwcaKZlyxOVsomn6cRx
lhHGWSWBtwrq4l4EM4SuH3E94dP5j5SGcekJv1zN67oxoJ876AcStLAVK5roQ0+EcwgpSrV++Rfi
RIAXh5Us/8AZ1naBkQKIbzfENmdGUmaMS6lw1jLj073+gq57dYMooi7Yzb5IGuIFjNRs/l1hVvtE
TXvyJ14AwJR2uwWQWTA8yPd6/3CUfJh0hpz237j9WL4l8yzI1i75l9ATVWfTQFUsifC+u1GRvGRX
i9Xb2MeRcmlXVua+V9qvXefCcjUIrNZAcrs6wo5Np1tB2I7Q0+/hdXMzG/u0MWrBZ3KtBwDzT98f
tqn0PoOTUBYTUQw/ZgzWAD+vdTh6fQYrfEBrOKSQBC+W3t14IJECuODcQIUxVHh0Vwz8ZJFtJwbo
OKKk42ePBVX0OqWbF0iJFrNx5sn1NL7cP1LUftPyCu4vl0sHZe3Bm5kx1o0irHvR3uzVLXjcbMxh
h03R4wrHo/O6qlReehvv+/lVK/C2i4INMyPxLjENgP0Zx/WW8xfgQ9xLLwsIrwUhZGYAbdGrLAXn
LoR/WJcwHFif6cw92mABP4N+v7dPh+Xl0fFP8mjt68/OyGkrFtLsOuHHCu75+u2pdYJ/shB56oWb
JCl1U4/YdezjKMtrwvo+haRHaXpbxkvpC6ad4Q1bN/Bg0TAxEEU9S4fdoUV4d68vOV5+X1o2EAKZ
OqRMwInX2QAkQuZHaqFLn6mES0LM0yJAzI9KavtJDroLBpyHzEEBblzR1I4emmIvhAhlqIKSktio
ZHeU5i/oYft+drJQEupyVGBa7/N0756zj3F5XZYyaJbyfJz5vEQfZUjJ26wiNRiPKJveSu5j6njn
Wth/fbLKmWOOoQK099LOhWjkaNUu6NtP6f5Xf4Fj3zuj6mMlPaym1pV5zVfi0pozfcvWPV4Ravpv
7JsCr2lGZZbzQ9Yrb8noZmbHPmZjR5sGiQSd560i/CILaBhLsL9X+yy+/uKIbVMlwmZAqgttANYI
QB4Ctm+6oFGXY/nQWa4J0fLVl7PZU0Rek5YKSIh98Gdn6v2nN5fAT4cptCHoZGenudJOaY3DA5v0
UzD6pDSo+2jTnVszxy21vBpd2PHJNmNWzePz0m/2vmwg/cg1tQfL6eMk4rHDoXhPXHr4Aq5ZPUEt
5uxTtNusFN/mpiFZ7FrcvC6DyrDA7vf+GlnuyjFbSDOjF8jgC2uajTfPdDFvjMWO1O3CjGodR2jB
4mni9mBJwPnMywVdHz6AzgBLt1/Qit0Uks6IwCMi6DyhJhX32I+olRnD6adDLcK3QfNYKJ0dp19+
ZSRrF0Za0iWHnKbJ7bbpaVsUXbjUBEkq60sBneE1aMX0lNtUH6u1Sg48iGcBP8oXu9FnQJ/v+b+Q
AnaTGuaDsy+MfpCqEhaGAUz/er0Fx1YM+mPMTOGa2eg/bqYaq7PYiQ8aVFX2OUUTuPtWoZV4c0MA
gTn6b5XD9IAlvIIk5NYEJTi0FtKeB2AdhdpaUqT5TgjroiKgcFtMTpHgLhavgXzvZ2dTo0lYMAnk
Un6xAwBKudT26qdpSgvi4jBbscGgzhg9DQf433jea6iuS/2miDnSCtmGsEgzGzOqfEO/far0JU4c
Xz2Qs56ONWT8ZMUky2+ijER4QBYH+GMwH4zrTss3YmVAvaWI+Vp1Vyq2cYvWobBk/U2g1b4kwcMC
97Z9pEDjsavhxSWhQ0Ia0dbdpfQoG534pWFiWqkxN2UxpvoDz/pdDwlUpgU32H1qy0mFed0RbHCq
WH9+J/G7ORXeTS+SKKkWrNpb8hdQWOV1h87EWDoWqGUTZAdvVEY4K5CuGmk9RxuHYlt0niMxxbKN
s55toM8lSrQfAy+hCGL8FHwJd3fqMgXCU7f/zyCwUnNaqTZTsvCV0TWtjPdS4QFqGGQYpqsanpFV
vn1krB37gY6mIVp328LyxgjpZOGbjvKo6shzoUTP//Kj96fqNL5iBUPk7kF7eO4rWCB9RxbXq8gO
mV/+hdLg/WIr82ya/N1KFrN78v8qBse0BgUrSJGT0o4BumiiuvsMPY0UQL962ycdo9b5Fw5NwoBe
aovezA+H9v7phE+i9SktrEJe4MHCy44SXniMgOaXPvjPETBqT/1YNWY8oTY/iRqZlQeLRGP5pZib
JiyGXHXY03299lPUnp2uBm2c1TfKI9Qwu/Jio9viIJcSTRU1ktotjtGPJqCvCCOlSVth9litwg2I
FE0vGtxhKQf/k4a0vj30mqV7c+C20xYSn2UUQmJYCeHGp69qe5uJjWYfhOz+aVGXeyNVmiCU4q7s
kHs6aBl10nhEu4hD2gyL6QOfzLNdqp+16qx10Pox1xaAAT4F8+2s/QcjHTfLMU88Yr1R2SLNe2Hl
qNK5gyx7x5pylNV3G3DOpfZcHi/2OM3rh6ERncQCYJ9+7JqwnLSFMSVbytTYMfEqE1Br+u9mlnn0
HQewiX6Dv812Tlikpvk1GD7fV+kqBX6P2fDOVufdvq9cMJAa65fvdUYpn4aaTTQsIrLqSki/Cmpa
+ZMaFEGmLn8fv3ON1biwZsbH2SNV5Vm5YKpa/fkIxBNWu2nnODcCPM0NU6Uic9uE0VnGR0I07mfY
pp0WcIKxqa56LtJgDQfpOPW8oNW1EeYcKYlOs0JqrLC60UvGwHtZHZ44T6rfemKNISSKTSzX+aYg
hd5OspRSZ34xREMF/mttUQuYJakBtc0JgOKYm0wrXdSHBO8TB8nc2V/s7xyjawQ9zKH4l2FXomwR
sSvo+vHVoc/RbYxRhDeKn4UK78rd84lOVBacUGVHUDI4RbYv8BLlN6ZyKP8QX4wtEl/vOREr6L6X
yZnyjwYlWzsZez6oqYMoW4H9CFG4pE+mgDgY9VRzZalcoB+9jlIDV7uGM0XPZZXlFkjKBDSu2POq
kWU58/gClrYV8Q8IGQxPwZpwypQWjUr/BQzeFjRgKnxqk2Kfq1PndISJx5Udr4AnQsSzIliLJuig
xLiPTe/1gWsslbi/rWryPHoiMPn1r2IwRXOnqY2iWIey5YXIlyECyBH6NTAi/VpVLT8f+DgTv4nF
CQQfGQQh5BEUbi09i6++aYR0Q2DEzqiIpNu36Za7QvJFNhzjWyroU37BL1Vy0/85136F6/gZDJDi
RNw1VyMDuIBFsQCWxpH1+Alf/tdoVBbvi9rlbp+PvypLHO0V/Zzhu9PO1iRcSgtW7eYrAntjWVPT
fg5GHYsb5Axv3GjVQCBWcKzOszedQVABbyhuxabQMmfKoNTjpp8N6lgPpi19LuTe5vB8ebzocKaq
vFCX4Fq4nb8LFBcsDN7EKmsZUe2rjxc5+Sf9OMEBr12pMt/hhjvaUOvROw1IlNiMtGS9k8y64uuC
yf6iFJG5UTW0Fso8PnzHCYFJwKOCagtUnGfgWnJtw/RNEBBVgEH2jYH+dGKMB1OAt3D6f/sdz7PM
qt8qkGJS5NzHIab11FzedsOEmsqrO1/GPCpSUOAW2hzy657P0XtjqeHfUgo76zNHK3YVYdvZ11sN
na2I8tcXQY9CVjmx45tpnzcG5Ly8FYaiEzKCyrM/D5D+mAAsIRePc4gibyCp00jXNSLp0lsg9aaC
0gcnEwM8UbyDu6kW6ZaLRv3ShI2SD4NzBipAgX3yKBih0z3K0OoJGnyLsbT0+ZKRY5xI93nOAU8E
hDrwRFdo+V8EI5TXZYEVGa0RQBvr7yl9rmM+lqlqA0KIiSIybyINcS2wF0z2pc/PEOn9PDF/tOK/
YnnPIfDtCN26zVKFFSpVvIOKlsqKZc/Zt0G8r3cEhxSsNoUOmJD1QjYrDrFD6Kl995VoxowrsFLq
Ne0lLjNBj6jmHrwZFwptl2i3r2DPkfjcOY0aUk7qqeMt5jYIw3bPTWizotZWLMuM5RXruFuVQvZ5
s9ggugXE+bCjg4G7ISlkY2jhjq9coi/SEcgL0Dmon2YJ3DkX2TbVYOUaf+EYpsBHPkhURfwPM7aq
taKpJGJQc2HMCd2jkf8hVpr67/sN2YR/iAzNgvheme0FA3Ou9+76jis8lMiyYZIckuoN0KBaoomc
imDuqCcVmRqvcYTzuUeGyzDnZBP1Nk19jFKDgUAD+KdfMzi1qQYEXi1Topv8D25I8FuDZcvbSfYg
EWRlOXc1lkpyWiP8lZ01yyE203xN1YhevsfoKbBqmHfz+8DTMugMejnLP/tReugMxBaur+szf2rS
+n7hz7wdPDngrjrEkYcMoSBpE5eUtARpB82/0N6lNdixV8ycw1aNjuuOzryOYLK7a9qA0EEmKDO9
lNQS5p9qxKXPcp9/QtdFxH2v2lcCLLKjHf1y1XgJW0Gkz+8mds4yLHOlQbNqMZQwkD9ZV4U0hoHL
0KVYs7PLVEExHznJ1AepTS27SAZKznZPRkH8PfCEMBWmIjOkycEib95qtZXJoheJfNQ2dH8i6diS
w9sJ0n7LrEgOEcU6TFNd0JheZyQKj92AP2WrHUZEoNePMD7941S2ygMI6gpxLvcxnmtjtgvR/O0K
D/ML+2oAU7HN3Jfo1tgESX5wV1ttlTAJskPd4pbtprFGIBrO6R9WzC6nPHh4rfsZ8vh/CqvfLBN/
O47ioYVWSt3iDIuFbvFvLpNAL+yg/An5MdRRM5FFfVHMf83LtUE4vzhIOdu2cBg+wmDK0hY8KIsZ
hkARY9XYnp+7dnF+YhBWL9a59Ua/DxC6VaX1sIdraCRVvpG00xp5erCDT3xrRIewWF/lAUJFhzZA
2ABgtSkFjfzrZsU6VKMbS64gpAT/nhBgFRV7TnzBPkWdjXsFh9l3hdBAocMbRNL5qAdFGOOLGivK
tl4fCilVwPOvaZmtYFUaL7nnCzM4uVLoN+gVrHpNnoKwco8FwSHpOg4hIgSWoWnPyTAKprYUPRb/
bm455P7ZWscTMhS8Q1E4KFs895CDPJZth96qWhS7dyT190pI9k2pe/750mVaLEA4BwnXuMCfKUSK
x+1Vs6Xs7G39va3DnpaL80RSSPKH7GDj8T4gnwgtcAx9F5PeQayZEi5bukGKbwqHzoTydWQkxK+w
MSMKdpt7L6aPzUIvKB/fLJBBe7LDh7elQ4Fqcfpnh/Zq4mOAVEP6byUvNaVSBV0lIsti6cYJBsHN
oY499Tm5v/tF1qc7O5z3lsT4bwtA7PmO06vULxvWJdzFM4fVkScBJtyH4z0RVBtldFGYRAO8iFlA
R4Ri6JKFjSQDCDgFwfpHBoz7A2BWM6zmvwnrcOeT7GQ1trdkNYLQz1sWrimxzHayVGu0GBHG4ZWE
qoA6W0xL6SuZSsTTI468xifhR4EG/TV2iKVKnReEx4CwnpevupeAGtUeSpTDlawdCPN8C0dcSgCI
p/T6NkOVEacR9r1PucdaFd8s1tgiRvtWtHvtXI4Mt7GGixrbPNochDV4ckRKn6eyGcrz3QYKOBIo
EgImFvZ5ebEj/e2g6TqeymtdTLoDuR+304MnOIoH9+Q2j/uIrX32khN0G/HjXSLkTmkCGx5Sd/jD
ajmi47nGEyFj4FIUgx9z/hVcAdROXCk90kTCkndBquuJG1b9yPCbydJrziL1C99G+1LTOBm9eUXc
idZJBTLZj1nT18grpAr42XEKCAdNOhEcFUauwCTDVhgy/jP7YgtaPE/qoWz9xsSJrdCfFb/cAeSD
AZF+wuF5g7XRTA5E7NZziIokfv27twQPhYK+oilaFaB5E77sbwppJ/WjCpkVzhxQPfU2u1yFWci2
09yOfionVYaXT0pX5VUAbvtyCu3FkLaHtU579ydopMvDfjKvfI5F80LVOzUoEh8qN0IGQhkBdxwV
xy+LiT/w/PsPGkFQ1yH96cHgFBVzptk99OmsYlVEcNyO4H1fJMZls3+qINgu/oa+dYgV4EvMca/D
aKbXTSCbb1/+PknF+EbjGB7XHSrfnEpJ8UjZMvMAhMiztrEBUieuujxVIhwN/A4yd1LXXUSWo2MX
hk6i3az6dq6JdtpxdJDimy6uMTxVKeSJjL6owD5ahN5IDc0ZqomCe3z5PTboJL7IsZY9KznBZ/Ar
b3g6O5BSFEhevIDq4/znOznd77Kfo/n0OO17VjvGrnBsMPe8IzibUC0o1Xkf3/vUg+FsmgkynI/V
EoV+3M8bLnRLSOOYP5JYi0/zFYJPT4T7DxbGPokuKpP4S4G1mqudSX9idgGLnY84budMjopViTBP
euVfntqP9tR91n+AostQxxA65yklGiiGMIgaT8DV5ZmmgXmCIk+EOz23uBJujgdvOn36etx1XRS4
9lb5q0j7+TqbguiK0B8ms2IeSIuipWEXpri4A2vuaxeqfpOw0cwkxXhYTjNlNSOHvQ3yhBRGIOzF
9t1CuKEsvJ1M2pnkEZx5WW7tSDd3YrdtnbwTN+Q7gtLqBeRoQISt7+JSXqDC86YKGHCQGWv6U99N
wF8xBpKsh5rm/AcvJjuCCQGfaBXv9RzdFwfbRR/79HV3YwTVOYYJqisv0MefcYfB+SwMT/ffHZ5e
yYbbOy0nRuzq8dJVbdpAypwl3eUY1KUUzopDvDcpGrkBOowogO4m78SjlBe5hsTGU3ZWHmLne8j0
l3rlIgnG3l1YMEZbIpa2YWFBTi2SmmdsjiRTpqdk613keu9PAol1XlWOgNUdy18NSFsPHp0Gr3p5
hUg60/7PUcMJLlbnymsVBx5nv3RQ8gub4r+lk/z9L0HYpX8OBJC6xztz3CYPcHPcxqRHGDLLSCJz
LN2TE+eiYnYPbXd1aJgLf+r8CIv+xn9OdBHtsnDp1Kri5qq5USXmEQ8MUZubLsyhpfov2BdJfOr8
YMTTFF0pFLoXOqnZQ/+RLf3949FMVX1v1FfsD9AcpTmX/sEoxAkBfv1GaRvvYs9IcRdeYxStLhxX
1kbDoJwslPoeoCwceBW3sDbiG7jXDkfSwFeA/rnbI4QPGYQ38iz+VNq5d/MKDTueEXQHZDkmu1gq
TSOp+JWv5ubwlsnlGo5eYkdZ/zGnXK8nvkBC5DSXB70fjZYWBV06A6T4mGvCMl4sh/xD+MFOnLhJ
1aJt69Pt0k4i8Av+Qxg6rd4Eu+4BqcLfdfZLPd3wvsOfl/iOB8K9nxJ6TS5Ub25XYgdHtRCoaewh
YVDQEsJmVQzQdzjBA49DgkCGV/YHECkqWATJy+TkWdgDk1NzMVfXINsPSjEz/ZAFQEqn9dzeCBq+
aK0iMG3jAYK4VhvRF6nJn4oJWkxA0haJTZgwUMua4U6ifq8Rxg+UUe4H/8j9svvn+qyCGi1POgOM
8zJV/62gMOkXZ0Fdy0jMFKbh5gMQw4rjWsezyQW1K/QvTGn3VInjS+OH7mb+ZbZpkDq2+uhGYwos
rBjoABJYNst0BBef4sO8eTDPrh/sU6QjsObiLiR2XCWoLjgyefSDxxGW7Kk35OtZ6TJ4nxuJ5nEw
3mtCNQZCiwJkoXLPOBgFusyM3vC0vu/VqbEI8+h/RVU5Jfqwoolt8a9AMub8y/+lpsIjbdbd30rv
gWfXG94pllUU5UYSS37X5Aw85SoEoE3s8wODcjRvB3EoPJ2/JRP9VzsUKCMEJz80Jdf+TYAKlr3A
nkOxDHyR6b7T1Ejf8BRHMfiOUK7lBIXReTHdtZvMUWQXVSaot7+KuXtkTOo+r6EMtIc60NtPUlBe
7tYH7EkykM0KR9357T/NUHuSrNm3eRvSE8wKfdcoWLmg9TpxSnjiTT0pSLprMAsI5QKALWlo1F7j
10TxLcJNQmqFQlD1KY7md6eBww+toHkxSV3E8GtrhG7GfvdvKFoDi8DYYDgySDZtUYMsk+CA0v//
A9Mab/W43wurGXgyVocLDDICx3eGG680LnDzowXoOSr2CTO9FxBUV9mt9kCv8n7jUwXwYUdC4S6X
PuEiV1wKwFB4TI6tr6fq9IHRSX8H0duBjWwoLZZJJj50fY2HV2sS0dafTks/ozv855U3ET/T6eLf
b39QZ5CYxpPVVaEuCQ0CbWUaqbcqVhgV8FmPNieDCbjtRPczbOybBiTugxmLSTunvTiPh+ONNphm
jRTZc1o1MNOpzLe2Cc1WuH0lZaaqFWkGui2Jd35Re18InsvQIZta0G5m4hM2L1AuaPw0QyQVgGAO
WMwlrygYBJigZ6NO0dSaVrUFVw5HDz/6CsuhrX+nd2eDJTvbv540r+Qlabif2TbBbvN4LisPUYA9
DzfnxnG8t7se+YY8TwqkP/97frkh3+r+gTkwtrQaf47ANKa3XzmECMUiouT2BgyubadhguDGNOmM
ZiQaciGlLIYIdfXW7GdFUnPVS17N72DYetPB31fGhra1SSXx2kCbgabxvjTJxM5wNEUSgHTt1ee/
bbsf9XfoNihUX4apEaaYoBV2O8vS82nGVM9cfplt8aUC4pFcZCieVVnfg5kZjf3v5lCVFrPm6ZKV
cHJmDKiDwRq/W6aCxN7tl5wwCSdtwJSPSv5vfoK/415qDa0g1wd/wFvPiJbLO4YmRNx/4sDh5Qbe
NtDe7bxSPQ9BUwG7n3Sp0I4QG6adBTvfspBs01EM32qZZFy+lXy0byn6hO/l/GOhtWNiGqiakoIK
QgBYCLnM5Fmr0F/dEuZgdRDmyvI1748TeMwCNbMkGdnkVMUbPEGeNKpJM80nhtrydYMqQrYlVzpw
wdHlJxvH7KxSjtQwZ0213+GrUI3rmCWUOQG0IW7THi8G7PiCAD2dEtgeQvsDSNnK/YLv6Ft136zq
Uguv2utXz+y1rkbye5TShM6h1gZy8ZXV5erab70K9H5zlHbE+Kc/n66iCyYh7Lrk8EwttLRzQQfz
aytztVMiXlC8oQwJ47DndGAnqoBuWhdmt/7PTZ4eIiNR+cqVy2lekIon5HX1vqFFRrlx/xW46iJt
bZDhasIH4M0pKMAL15RoKjJjRY17exRWRQMk9Hl9gOCWsWwTw4YKNKrJKrnOxn5BImBBIBFyRqWE
Vf/HiVHp++RK1OKf1U5502d0/52VE3dvI4Y1+Yiq6brqSd6YHn3h0wF+ezXPAmAAO/V6zEkZfkD5
o4bUjmycn60QZv6vhyXNWPwSpDZuBjHsuVSffWrUo4s7aB+KqPwoSSPoP0GdHYN4exTDtiKXzdIu
1Jeh/YYDlpSFmvjHU/6v3GlQ3eUoD9C4Y4ClmsElTWLgeAY+Yp2D/Vn2ojpEUpLa9+WsTMUYmngS
n3OWs4U5HhqY4R5sm2+GOhQM5F5N00UQuw+W4UvG8MUipQyHSdpXnJvqcjfO7xk5Q1qgc+/xtYY2
wmrZhohEjwuw0vd458vIqEVpnJLXvZtZCkviUEwG031Yfpiz3Vnd0vKjrsbrVTD8LgBQaj6IHeni
pHmNevOrNZCQDhE69TgGjDPHhgP4O24ETFEYO/qbZ8aX6l/2ay7Seopg43F9q30aI7+mGsCeoH12
WzKSL/Ga4LX4OeIV+FDvyczTpp9jcLApPJKFoCO1qCLgWipUAejI45Y1JWm98/N0MUCwflQFZMBs
O/akfA/mW92rjdeCx7Dh6xKimSXKiztBgyK1lr35c5dUUsLQB8KtQdObN6KePcxD8tMr6kYwsJO7
zzvrstmJCIpvI3WDuYbIKFT/S5YOy2BLCwKYAGzsHIhx1jusws2HkIeL2IFzD4uRZmq9h1lu0HK3
7taCsiy1hb8gXcwQSKvJFiwAZXl1YQX8PdW9nXGiKegFdZt46aBT0+5K6w6xmukAmuWTijTzZRfY
Kbj52uSM65aBu/JshPPBDnlzremxyKqEZUQn684GqdZ4/H+/T4IAj0Zi/BPfNVyaY2vVWN+XtEib
UyII4Y5rFHK/b4khgDariTSFDrGl2/tnSInC4fZjTs/UNZCmVXEmdwMcJSqQi4gccU1CQ8o08t0L
ZkMrN1UxFfBX/shwhWnAdXtn7v2awymaBAFst+7bM/3cCBR7Qy7lElMgyQUPr+J93+XJXfNq33QR
IKZyoOmTeBeujskqGxiHDLjKRfGjIfIfzJnD5KJEEe4x3qebq6/cysrus9Ny11XAVDY7SGsdYeWv
S+jLrXreVz006TDccvCU74DWsrQNoJrXfOFo/EayEtCy51aLMS+hSRXJPgf+pqHHtoCU/yYwp9BK
WnJQYbIRIK0cd3OJv41TmxkQIRXDLBgrm4eFRYoG8o//iDIqIxWYG5wJR7/RyWyz8SIeKiXD9XfJ
vZB4zatNZn2hwLYSQOeo6YMj9zSPMpcuXj4yNgEwgJCEbWA3r8dRteHY8Mxe+nXlRniD+L4n93im
vyXaS9Kwt/qR8/16atkJfCeZUjTjAOGoWypt9ASex1ABoHcwpUXytDcZ2IN/pqlndp2qjg3TignV
ERZ80JO7+KZoauCilBWZW6jQwCvRrEGuQUSxUvZGbWGEpmP3tGR44scGN22+fdQ/Y/3aAKp04jt/
MO2xUp1P3Ve0WvaeWu0HCQV+zYG/M9kLRHfMm1lTNGaHvZS1XQs02bFkc92z+MJ6EOpSbj7H7ld3
jtAwbPAsgwwHS+6IWRQQ9OCbzDN2tazyueRXWKJaFQYj/4f2dEavR0gq0Qb4xMtkN9zqCfIy6oKN
8gOXQ0kIB4dZJtkMm/+gn/shqWFvpv1lEFfclU7rcXfaPhmW/mb31YBzMo2+RCGLeyqD8UPUuJuY
bM+rAwHRHsTsLjSmd3gt9eCQrbFlMbgQgWA1NUG+nogw5ZVTS9wjN58e4lGjkhXwvQX5z1sUTd+A
6b2AsxJ2hrB4zR4OBjAkt5DNEb5hHpqI2RCcMRuJHScUW6hTI+J+RT8aIkiVl0pYzyaVRzCz6gIp
FGEk1lmOTVTGBgmhRpRJAifD6q6YuPKOaOjphhIiTZCytiozptn2v/DavQh3bu12QHm3nHp3knvO
qSH15INJWIb7K1DxsWv/hZMwGJvKsNFOq6/tfGpPzYcL7atRubegPorWdYxTHm0hVwwSNaiEv0q7
uXqBKFCouic64ykHf57GVohjRo8KhBv79J/dY2sMhL0LA/O5q9WnLVjyy9M7FoUe0c3rMbfLXlas
Cl1qgR5y3SZlUWGAU4ldyiJ/xPB6isBN6l/NY1gAWMvwPa9FnS0/sWt9a094RqwzIUMlnqtZEff6
pe86pP8F/KVULET3Zdyr1bnqabMeB5JJVqiiALxrbBqK3vbORBfpMY02gqs/Hxj+nL5UeLJis0vz
qyjfNxq87GEaVkA86iCKcXl4eiiWVih3YeHrSU1VwCW8IlUJlRfCBIICgKjCT+BIaVhyzWvpNqWp
TlbQeOUtFVpzmJGyrzY82L3J+9VqGbIVJuhp0HyRlVUbzGd9L7bAWKP4LH4vjxq5FiiARK85xkLU
ZgQ0FGWfoS+IM91hHi7txU9F0InFYYYP+3otsBHd9HTlzTOTlDfhwaHhnqHzBn3w/bn5uuhxPwrV
YAP+bgXaK92jqmxN1dbnqvS89H9tGAoYtdeFc1zj3nUhC9UX+EcAEM6fTZ0VJPWGphKUQnqviQv2
DY69RRpVsMvOOeAMDYcuOCsGfnICWjTAFHnyLJKAhbCywShRxnmkghTrlaGiPX3MACt34X2REMBJ
cQljbMmAdGXbN3mQ1sxVzXKGAG0p0khfeYiFZjgy1LuGZUupWpqKQ3jt/cZ6lFEzGWNYS1TbHDcQ
EQ6SxOQ/qG48k7GcdywBk8T1RtBur+wXs7OBNNfeY19RBCdWFbEKuBWbn4ngrpl37pP6/f2/GNlQ
tqyWCOg48x9MuTOetmjcMawb9u4gl7K6nP4wF/0erP6/zMEcVeY9zYPrvyQijoWY3f3IyNsijc55
J33g6oeUCmLZyHo4SbTlXXkgY3V97Qf8j9iusjVO9viP3BsvNwYuiR8L+kY8JPGbKo7ytbiHpZ3M
C5FVK0uxJ6qjk+Xr4G7Vv6VBiZJ7KIqVLv11oBoHDzus1oZCevUrJ7UDrzXA3I4J6LNxxvA6fTpU
iTTc1QUbzIRoQAX00UqqsfrnhhTrbB2P82EB/xa+UresKs2jwuGNlJaL+LrKC+ir3wqxjI8CMFGZ
Yq1crISOjgV8uPUPOKDgZcoB06Q+/VDyK2WjHJ+GQoBxIu4/m2dCkmI1TvYS3ZZIjkGXWeM2bgD3
2xKiy6UdIe/PxahPU98/r2AXUAdMG1UUvdiJBvVCkjgQXw4v6HCZUfmmPdxl9lfAe/z5indxHm7Z
ITHzFhjPvl3fM5sGGlaZfb31RzjAv8dGhbwX8ShYLM5JjouV31niQeW9VN8/pKslmcLPTdNHwsta
/y+aYa2cBRvfyLBOQzuVZW6o7IzzHtOfuU60SOA9wrNeh/QiSZn23knjsg5V7vtbwPSGKpPb6GsZ
pEFdNofdZQbIdF/OXTAM7Bfv6LcuXYCazSzUoFXYOGQuiXZa+z7rP6X33ZBl8I2ZDnWd/GpziIBS
32SIoFznl9yB2yIGtdsRkgRKHsUkbcTPZSJqnTkGvksgrko4Cr/JH+ywyaHWrQfc5vtlXmjTvzFK
I9RqrWxfVZoznYMkPKWTvy6oRycV6TVV9I+3i2aGhZcpPc0Kc1muidZajQCxIlRQTgriFrP4T26v
Zg3PPrf+2OKkI6OujPMw/VQzF8Y+QVUIyttZvVWT5vTD6xWoexMntBVbd/wDyqlSi3kG3wctIp14
cR0a4VNjMdeBiY9dwQsTJC3vN1SuMuLqLJCZLpKcTWynqPB3iL2bG/+3FnCVvIeB6ImLyYdfvP6+
9J6/TbqZ+dPfSDihnvIqW2LrOFtl5douyplG4AKk40IWkmeke+90B8ODDgfedGEWJlq/TW956I6P
JsN7aBNuDr2jJQmM2SbHmwha8xQXM7uIRBAIMvphdDGzClGDmBNQE2i3LPNoGstMORf2JNke2bTf
Qz6sckYzd0Me2t/dZUTYK6XYyK9M4ySvjV8qlZul/sHE+qwadLtw0nEd8VkRTKJSDsIp0FSu+3MF
GjiCZCI8I50PP6cs3Zcb4Hh/aDGx35C0U4Q+nBwxbFjONx1HxCS8G7rDCv5ARXnMM9bxhwv9ALBL
RKCfUJOuHiWlkc5W1hFfVwJZGs5YZ0L3HJb2sN28Svea5Bw4ZDFFBBRadPYx3umcrWHBkT7JLpV6
kOeqxrkwXMxxSCA2KNAztCAj0DNwru6vWwlB39SSBbQxlV+pvYdE3jwCgMm3W2aaMk1f66Uen4Vk
HoBeAuLr9XAcDhwdDFlF+Aj0ihRBIMRm6a4TFoLUZkq2j4YqVWhn/M5PQtDxdWsAvsUM/P5C5fRU
3p32ggRWxJVP922n2+mHFjEXb4td9PvLQ6D9nj5NTP24OGeIAnyXnoGST/MYWqNHiHKX5UrtwFNK
io8MiSRMSBa9WSSr/ZKHnfLQuJvQ23c52iWFLfHvmfmuP+HP/EGKmHPHn3HHLT4VVK3eBKYQyG55
BwLNoBqBTuNmWtWme15+bd10z+JIYz7esFO9cMmOPuBUsZV7Nt6I10eoLQhBC0KSXokrm/lzBtZ3
fR8j4OrjNoveUZdb/lqYOm43fRo4VgA/UpC/8Q4+GGCnisJBP/OeYEF3pVM0GNQaSmZc5vSA0O7a
zzeLvuh/H7+spGmSxJG5N5XfZ4/4WBEpqgoplhHZvakHmXL7ZTeTK7gJbxNVoA4jnB5n2A9SbkZJ
Y0Hz1+QGpmrzO1o7yPRDVc2rlaT2wgOCvt9cwlqtd9IX2Wj0VE9ZgausnkQh3vGVuuLzlK1X14SY
BId56nMVJ7ghRu4BSl/47nhXEIONFh/6XCDwxaBbScsvI/fvBuEoeHayTR9LNtgieRac7u04Gmwz
FP701ACFwLVSVxqa3cCV6CXPPGLmw6HDEw2XjXHtAtDDH4UR5L5sLJiqudJd7F7Y9RL+v/yhTxlN
9fjtHXoBE8anSpMgSoAF+Z0AFcV2ETXd9FwxETPGRg17vV+Ip70fyqofos7RkovTMk933XgVXXXy
9p3dAcTtRmHv42LkICM65ONHRkgSk4cF0n0s3scnNsHbPBGbSGaHhURM8herQCq10eFYcpHZG/EL
IyJgtG5YcQ8fyG/xRGALlAY1nPDFexV1u6YOQEiyG6yEWqns+1oqga0AE1cejwwceBR+JFlM8fj2
FykH7RALkZsKp6VPOadSdaI/DB1WB0NjUljYkCsZ/qLljZDk1XOe4OJvLudS0eodBz3Er120pS/w
dIByrnkJwQG16I/SVbsq2SGGRy8F3YMyKtXbsd9AcEil7xuuaWoDdisw8nSVTOROJ4p9vD6ZVFdH
GX8ASH5/udYgPN+24AH+zssJOHcH9eTpmWHH+YUcSOwyg0TrpC+G2aetkx6wNB7bNjuZLqVbq7Pg
Ueecs8iNJmN7v+1cnt1yZazV0B3rFtlucpFyWX/VCov/iJkzSZl2KKQcHANfb8fdHWoH/WCyvBBC
5/NmKLHD0lGgvKEl7f/cvHlyYNokqaCOgy4gK0WGo8jJgWb8H76mrA7Bz9x1OEImrOHFOhR6q4UK
EDWH//MAqVwS9nyj0qZ8w106Idzat0lMZu5cprmkmj6L1t8INHwqzvbBZnUYfOCu3L7MutrMMiKc
d8nSBcuzqDI667G173TPig3r531qkENokGBur1bFbhf/qVYXe7NUWx4wOmb7m3CvhDOjpHVytR1C
eHqYGegmqkrEnVbZachMucdCzkgPqQnmCBW9KoZVVI0UVN+aO1hDjoVThqjPLOELl4g68j5TI/Yi
rQHncGAK2TkXBlANG9pQoD+SuW3GEkQsB8qJNbhJ2VnkvM1JlYozm41Dl4FRhThLr0zkcGLjPkuB
OYItvU3nNLABr+aDzTfNZr/axbQdxj8xeprEaOaq9IQkxxZyxAPbFn+agHKuwrVLGCTnNxd1xDwW
5k+r8V22bpa9I90akqUL+Ha04KcwLNH4NGQ9tSS5g7ctd2zmW1FcMUEWsurYBDcWpcJlAErKLF87
duflAr5sBDvRf+azgIh+CHCmcl93bZUaUa2DnNW9WiPAsDMO+rUguWCz6seyQvGhio7dHjbDNeSo
1TTv6e6F8FnJ29V25xFdwQRDvzYWfBd8yt5WBQE7nJc542WN/stCSzUoivnN3z6x7yMKP07XBUQv
EheLr5LKm1QKIDT3XaN1SymrEWmT7MtxDL9Gv2cGojKbXXUAco+sDSv0JsW4P1fIQAHU8x6Y2+m5
eOJAgpZY+4vOvAjHJMb+87Ok+mBCp4KdrYHEc5KKGMce7uqdHwean8y5sh2BITeHGS/E4FSYEOyl
5uHMcPpyICxdG+1qv69jnijMBasoIB0B9gJCu28/prkkKCY0Bt9NC/QCwM0sI4aKkQyrmZTDKyww
a1cLprdT735j2ZiC6u/v1U8df0hQYGOy6Na3AZxBTmA8FgVDv9Z9kBnWiOlI0OHpvck8NQU1QOH3
iSfTTHgQAVZ/UZpYwp9uDllDrOWjut93RFhOSVwsJduOaOD3e4XyDAFOR3LAqnv2jAIIefrlc91j
JGwIy0lZKWvMriCV1GMh2aJm+7dLj3PzS7ZC0CRS0wmAZk3w4bWrBZvoV6dzYfyGI3PthVI10L5r
D/lYja/weSUz77O6Ld7JvihobfyNov9eda404V0s3TO/LFGhrNv7ts2RvJ/WeaMMf+ZFXebNwm16
jTHebJ5uOWKDhQDCdVD6oYZnAhrsPG3Ae/e7uK9HnGoiNnSXVJ6ThmrV6gVnXW1EoKA96f8YukIS
Vf8+nf6AbH55dPemqRAgPKQqp7TqKCVlefsmFTGkY40i335+SReXnGKbtBzf3u83DNGRt02GJ7bQ
0FRfuzlZIWGagvWj8aGWb15wSoGcDtoi5miuv4dZyDQsKWEyuJCZHwI4kO0Odu0ohZrj7qwUQwat
/vluSkEkPkKUy0MbaL7ix2YJoNqnm1scl33UZwu96S39uXyHl7yk6zGJntDNV1Sl3HHaMofg0pJ8
8LV3xiU7lWS2hw3nCr6EAP9Ps9Z0H5GqkhjTp3ckYTwvzLFdkxX6B4mg+VOl9SBqSZGInqw4a8iN
AGHcA64n/5tP2cKI1miJU5+9CaL/LkZR/7YifJVKikCfiJ+YGmToTXAu0M3+UZulii9Ij7fLB2Np
ATwb8SrHe+u5FshU9gkIw1xZunwX7xcunMN87BMzmUhD4jEFEDtyBPwn6MrNc5/LnSvUChdVx6km
lAfWuYJKR8bUCkKNBjDRaImheUZCmbC20BEHMMuzz8/99MdKETM/9uqg1bifHz0tndfHNTWnqxxj
Z2R8bFdNnYZnl3KHUJNiUqe9TSHnyyzjraQb1vcmzPa0qYcMEsPrFzpnova2yaYzR8BlzVKSEIZ+
wFU6M5jx8dd02YEo4RRDciTbVLxJTxg1cYxbNmmo4lStUpxa2glyA/OwGDEnD88SaDS/5sHRDmVI
xVFeXZ/ZTtwrz/Ra2rirAvfjDLJvNQLueHrjdueiB//0AyeJrNDFtyDoyq9cgN+ZkD0j7DB6TjiQ
5b+crlzd6ACC0WjQpCuJsWk1Jqvh+NMtjcdgh+bRXVWwNf3ypElx6Ru6/okC/kD4v0GWoYlqwhga
Wm0e6ukfJpDpdYVEoocCUTCurCEBVoALWHDwPs/afziXdqhw6otO/FxQFLyStGJ0wI0CEFyA65sJ
9btvn799honmrzMXWvyCGcG9GyxXqA6gLcvFYp8yuNWpTuXzmX406TWW4KXfVM3OHjpPSr0O8J4M
kHgVMmOgDFFDTXPnsHjcD5iO1CpmMUFvZdPSxUoy55Kjr5IyH6roJ2mTx3B839p+rVcU8zB1j2rK
i+WPPQqQkj7fF25eA0Kb5yJRRcr99zNxWVfG44I43H7EbtZdxTaiGOPLjWxRpTwPUB1gdzCXXx95
MHGAFGzbRxo1a6dUsfRtw7H/9A7zYgTjmDaPSp/HW7T5hdXfoXl60igDfzNQ7tw6FuH2l0YWx5Xu
GLuf8crHPGzVuZNw2xYLsdgjSG2NGPIKHXNOFYxMIvrPwDhvWeNFsTtJ1tl4BXpRgL3DN0zbu/iB
iI4k9OVJKFkvg13ypXGgm3Wua8d3RH2feweFDRCEdhpdxkiBP5F5ihrGhwEiLqE2S88P3nT7wQtP
2kArHY+TReJOsynY5glnJPDQu2EVTiTMV040PqPNvyj+4Y6IY8IVrBVtxSFIraRrM3XQxRJS/Jal
UP90k6PTkfde7rYCVuKvXaUFzPAXn28LzHsllrUfenJsVzn8nHOlmdmaJFny47ey0eSv2l0ft5Z/
BW5tKHwZp1Cakutno/Jzj6LzsekGkqmJpH05QMfYrSxRpcq7ZV+u08ZOZCfGQspcyeh4EvO4Q1Ov
fBHjfFdg6//+SqV3kGHZBbeL4qra0TYsMLWVo08Q5zzoqdmxS3aOnS+S3vQoOCVyrXEvbzjeV+nv
lmPLQ7DR9oJq9ADFEYoEcPFoYtYX2KsGqGpdP+Ux1S3CELcO0sb4LohJMf0YD/qaPCzbnuwlR/Tg
5+Ktj8Q1JdFUJ7LxGWC5Zac+kdFTqpi5J6442z+J80oNaT2wtG8/4342MRg2Oy5qyiAM7Cfy8ZJR
psWJcITDfYqtI42KTpkRyLwmwkq57HBIRvfh6nXm1xFa3T8cOIynXG+TECmca8MPqlMiIjBliB96
yuuWne8aZ9hz60AlPNwFmDJOHbmHbuquWWgVWDuTNdl/mYahBkeg/wRQzZPlEPuDgbiBnIr//AM2
VT0Wxvl/jLJ/dysiZzmCGLEEBh27QfDN4Bfea0l7otRKEOJlgq0kW2r04o4p5LO4K2hnjlqOLUs4
vr1wgESK177JsCMbZHYD/hidO9xLuI3Totf8HhHc9nzV+E9dY06KpNmKaCXNMCf2yfuQ0RlPjxHE
67j14CdxXdGRcm4+BLohE2d7vllZSVF2BlZ+xPYeo1qTok4AyMnFi7yKk/m4wJcxtoaa6+6++dj/
fIMHXCvsTyXwmaJtU9RZ3QuD04F2vbNMX6b/PKGjSRRciLrGlKlI4W+cIN+Y27yvmgxcDzSEIhle
53FPW2ekcz3Chs8IdoP4McIRz72BFLOGvq+xs2CztUSrIbFrvb8i8P4r5AMZDRJ+ma5ntvdJcJ0Z
QzZOopn6/GlyCiK2ZB4JTtw/WST0z3ytwZITpx5XN3noCjMkJwsCCcIvAe9BbL2G9Tjcd0AH3czj
GqjbdWWgf2j1PSN/Fo6+1sOOc0FChTH4zLCyXBs6ujqff+lgcIJKCXAdsrLdK9HltOSg7SJfzjiI
LkVGuPi/9TdcQTsxZmpKDYb60JbfVPqux3bhwRb014cEHI13G55WSsvjU2aJfBTXPM0KrL7w/RpS
CYE2+abgPnOJj3sjcC/2gJWK27Pk5cK/dperHStsFdMMn/vqsfrEu398LMRj0bU4Rwu5ktWf8Alk
hWIpkEL4fLkqAqCcdXJMrDYubqNpTbnIXywY6AwcGGHzLV3ymEOwxTouFKFwNmAHn1qTb57pgbtO
I4T2rkjkN8Lm2ydKWEI56e2nzXtJADhe8ubeJu1X2zjpVd4bKrdHQRdjxiXGRgH7J434SvVq7YQ6
+y6SKnjhnZkMb0nkB0FbjlTwaDXqF0Jw81QjZeLKg9lRqOJ/++9OThYHnjsNhBSmrF+0ME+GbLli
EEcZ9jWmhK7DQcwEMd+ki3ipikeMIhy5vvJWg2mq1Nlg/cDFMl+6Ol4bKRjT1IcUCUqWlRVck3Im
pITTjhKZi6oSeqwvN40eupI00Sm8ZXeF9elPTCyTqxvPsxcv9Zf8gU9qaHd36Jv80FdWv+bVPrKs
6IBDV4mu3wDZi/nYJ5YufZExQ0/e2oOKrn695CeQoKeVjPkRKAPbFkj59qgkZCIiL+GwOlwYifoC
oYyBy5utvzI3Twv4PMh3eqzz1wu6/bMlWiXUaXvPTGd5ZiZ12lOkYTBqkPQnUwKoK/E82tX18dTG
OVQYWEUhtPROILA/8Tr38hJatnQ7Gfs/UTn0H5qQFIuX7skB0C7Dddx/n5Xd+ZDl3jrprNypuSiZ
UdKTzSQ6HDvRk/N7E4BLPmwejQrMBQodDoTFOpUF5bdZCDJFwHrJEUsh0Xr/VZczXTZCJAd8OPeM
2WpDc+6or03EGKaDLy1E60gEf7ITas4Ggkl5O9RHOgRyI7e3EqS1RyktypypBptYvT6sf4b+tHkK
yekDAhzM/MX08UgTM0UuOtjrAwLjjbTMKbPsiLh89tSIiapId4YlhE0LmUjgW2rR92Tq8GEMaRdl
VjWlmD6tq3kpwh5yNXiv548Z6bU5nHS0ci2vS5EqTDj5pitvEC4bhIP+vthZ+J0RfcqP9WkVTGDq
Wia64QwLuk7rImEhJvZgs8OviA3macniJgOQ0EWY4DE2rgNu4jEc1dbTGPlMuzzFzKpovx1LXUNw
Vf2TcI287F8TGLXAVkKGsWEJe2YySR7au9P58LSwd79ncskl4W9PWh+fuk3NwlIaz9l1Zi6T6kG8
cYNxtz0bcdl8srIpNhyjMKecUeqcLXE+Khd+u8ONQLz6JKah8vzQptTgADDmyuS/qXsEkPrf645J
6O634IDioBUyFDv+yth0vRRYWaMGJW00tayNUenIrsa5n00WhORAId+vYEbWmZuGc8GMGvW7EPAX
5wTL9x+d/cr+/Z3YEg0KQjJdH8ZHLLjmxWNedQTFW6c0FeL2w67wo7PSM40FsCHu8xxlCv8giTUJ
l6RbJXmVfOIgulUuZ7CdSMOFRqHj5onvuJ6TPCT+8cUv2aFUDTe2a47EkIUOXUZ6MEGUXnV7DiWn
RoepD8k0663QDVCRehymikoRTuIlWxGVU3j78XTE8d1OqIdQL+psXfNkkddavdcTLiq5ZP7ielKd
rtYoELKtuPxbMwvXntispK7ZDJBCNfePqNFsMczTHmdZhS0YoFeFIl58vG/M6TkvkxindRTlTtBW
QmVgTM8QvBJoUAkczMRi45pcCpQJCRAxawesDh9bh3bFa3kyXdWIcCDdb8z1W/N5X7rBlYGZYmDx
PevGvEq7ccs5yGmP/BpYHSSUubjLGNYWclro6d0TeAaYXPRBT3/ryMgvaFRrWIFoCofkQzr4ODSx
w3g2OpZ+Q99R1MQgyWluNtGpKmR4mpHQAjf/ZBc4x7KGl2eVilVjShssAyw2zmbXtAiPqvkRNvhr
KL2WKyYTCkhFdravqACOqnaZCszcP9kYgn4OpsIHe44uO4NcdLTpXZLb3UDHenniMxUPNQOXgFit
GJKj2tHZym4aMFQ3qvJ4jDJMky9HOozPllPEPdF0i5k4+yFsKipSVqTAvrxvN/XkamYncNwJ96C0
3B4V0bLPyi8xoXc3l7ajjlYktDNkJPaHjn7suxLyBeruK+WMIGEelkNhsn67sLNE3vCK91OgETR/
SeCKT4rJb/cAUhbkao/BCD4QsAOarf3C+KoBJevYS8KDiu6jqoqhfMBx+B1NmuLSFjDzDIt3VPd/
hTUmEkWn+2PkO7zpQA9f91cf7A3QECFaFqv37TNWbH/mObrb+Ppv4lnFzOPvoeBu+nDHrj70O3Ld
irJk2UtNOHMPVhapgtbfhSXbCx1/5us12YLfEqamn3DtTK6V7CIlXQnvoV7e0CLPHJenje/+6+9P
EtxIsXIwkvubiu0z8UFvZAvobiQKm5npCp1uUXVZ3Tk70MqRKR8ATbfiEqbPqmz9sk/tgjDHASzZ
9+oW9w1xk1CAI+PfO0uo1I84yEaVE1Pw/EhuAZR2y2y2w61VBZqKvNU3T9qkqmNH8tCZmnaoeytC
QuClxitmp4z2EyUqpxVRK9pni0MDky04w7pWCgM0x1ZD1Rwt1AQv7JxAsuare7R4JnEYBwz4nnnm
pQ1YYzWgffm+8KIq+xk8JsxWaniGqtsyKjv8Ua8CM5PBsKyOtDIqMm93Su/Gqjy8nhbRrLnEM5tG
3mRWeg6U6AMjyW+1QZYMwMKx2U1wp0AOWe4yNmrxlXOAEevYmVzhHreHb3+/q/z09NUHPNozaAZu
Job9yvdEQtLaErWg1kMOQmOh2rMs72ZXA9QxRbks2d9vdEv1vR+pFyH1cAqI5xGDuj+u/Jyd9xxM
WSp8+Zu6LDQ1kYyorQMI/5xpWvGI3uViAt8IqG7He16OFCJ2fsREOD4VFfjw952nkii7RI72/2rj
8nYIc7nGzUWwiesgyDsMMwSdpezO4LW+j7TTueew2EIgSIEmO9afoyRXOsE4lhEFwV5m9JV1jz6H
Pt4y4Frfgos4sIFmdM9dOg929DptOUhT65LzTvNJfHogRqy79Paui5QcsSFe62HDTW6mgmDMlk5l
N8tlCDNOdYUc8BnMzPNJYNlVaPCbuMFafMHAacDc0BqZA0Ttk28CKEq1/nX021xyajmTZuDRD5Ha
Y6VhockfJOrodlI/4zQ+mbILDSug0OndUXJrqXPDydQ9m4u8nO6aM3xja9G8xoEOdAoCC0l/vJGw
7g+z8V0YgU1jjB+CFeU0qlSi4gKmbB/U4KtLXhk1J0g6wcy/WyIfcOKsKCUU0aVVNDeCF/mJUnLi
YvCCMO1LrDchg8YxxL6sdaSpBDk1aIx1KUEmkQO3DymT5H+cOSSvOeTQvpAZ809FVB9svEh9pst0
RQkFcMZTmcV7dIw9aOP4kZIwlKQJknqW934Ixxg3AWVnPPpVOfUTBZ6jI84051xEZMNCgz0P9CSv
qcXpSG3FTmtUpb0X1/LlM12scOr3ee3kZDopyYUFSZEWsEbET7M98kELcADXHxvWcKDKH+JGgDSm
ChKDWA3NBNlZBSjUWZl3CIAhjmi0kyzLFgdeTgIUKyHHb4FMmvIwR0e2EBhXGPxAJQjxe0wiktb1
j/jYORjDK5kKwjiwpEdR7NJGbZW0OdguSLWnc4arigFPRkkiT8gORs15VGf6X7k9wP9r4Tz7+rPr
yTPIR/FUptnwDVv0kI8HpfroxvahKQwAiNnEi+Kzi2uOSw5fWspZCeWY6YMitzzkcsW7+BUNCg81
dIjdFFUIuSUa+hoaFsMiifxjZS/3xjkrOJjVAT/W4Bt8WVsQUWkPPdDwU6N2cQ9/Uu8DikHfMYyb
M7PVAAjaYRO6jFpDuqc3cE38E7ypWKwYcGQ/ykxx7bNuisW0S+bP0t2A2IzyCDxObSk+fSMa8opU
8hAfi0AxRzXd1aq9ra9RyOnzT2TXKaSqHWNC1PMwJ6zbch8WG2r6bg+oaVHwlY1PXGPA7oLNB1w4
cL0EmX28hNZeAfWY69lbjZHIUhX3gAnqA+DYlFd+UtCKO4TkekI0iCXYaARoK3AdCccAKVh8XpMI
yQgIs0GWCVkgbJNyD9Oc691QgvDUXAkA7DK/Q5Fv8mlGKfNH9+owgXq4bM8Zqp8LiiS2xCCKpkPG
9B5K9vHLqgIDCcRzUKSNCl6xCQ6cr3+IPCFLAgzxpJo5uqXRG41W4AyH1ldL2qal5D3Lh+cWgAdO
mkAId/BHxu1wciWscI8o5rkG+vwW80IhYL9XlEdW8wh6pWJPKp2SzQyHv2qxYEEuPH4oLAj1qG2H
Vvas8ykIQc4tB0f5rqvP68SVkz4HpvFZeIbeLbLTjnpFQI3M5Ld0sLDO2qHd0C/mNUL/XSQCgnNW
YGlZWHLlV6673eyjQ42hZAy+CU3hM3pFRJajaxhVpWh/48JAkzxu2Uny6JJpNvMQyO7czGTWsD7S
5DjerpQ/+io5WUk0Jz3Dxo5tNTAiKZ530zWGLGuSVapupIUO0nJDDhCAiTpYmOzoHMIVdS5TvlGg
jorgkHPXk0AmgLZmDIwhoiSsJDB796k4GG0fOZ1rX7THjYSGZuT26ZneNFmT2ZpKkI1nsmcTQFwk
cJ4HjjxhCUky1nnAkR8dfpz/oLyPgKQbXqerl/nnRDfnAOGZ5udosblyY+K04NrVgaDkYGwP/pfE
Uy4MYuKsCuWwcHXXUj8dO+MFpfqHA79zk7dtHtrb/MCtcdhNzBP2SZZgDQ6yPd0Xx1WHl+zGL+ty
pAEFZ8ZHDucFnSyX7AzldjI89qVzkbf0j0I4OYGEWPoqqu9P+y0GbaQLTO+w5nzcGjAUtoQBcs8S
TVmmWoJhvZ6AgpiE44PNTaurzYR42M1e+psZBUyw9W7/droKxP04Wi4utLhp8lc03mgc1meWgC+X
bJ9Dpc7uoxYXuI+t4GbvuldmjEwAD4E+dyORzD2CY2ZSIoEbcdp5qZjIb04UIUDN9zszIOeXNi6U
8IzqXAFxUhFs68DnbgQhtp1klgpMe8S7t6qC2hbLCCm2W1H49vTQ45feNaXVf9+xXFZiX6+M37Og
PSC/Ghs2xt9mUN08f8DzLzgnImZQlHZJAIB9mugHusmegsMkJTUtkBbQtoTuDj2u5tYJ/iI3PwX1
iMG3E5lztjRPLrNnohDUEFKTOloruTgmHeGfoTWOvPagDrVbNUUlCZucfF5qPgtjmXhWEsJq+hrg
DG11uWHfVS20BRAcJoLQsFBtl1LDdi9i7NiXAqIyIGX/mrmooL3VhgYPKBN96g2tciSXoY1oSlm2
6nNxF2vinDGLwYkHdP6m6hELHLtPMLYihzKgxEunWWvySR6XM5jQoKJtHwGpJFqc5RP2vBXNME/m
3GeTPFlQ/pvPjS7IfbO+Oe9XRbupB4TRYVQO71E3YfG7XpUvr0ywbzuIPRmc3v30Y1IgmvsacysM
2cYgzsZ/Ck9vyXPD7KrH1arhH4goDyZdWuM3cLMGKZXZNPPegs9SV+F8/UGQuCEHlWl6a0YrcWA9
hix9VcN9Woj/Bf+9XYs8Jn2AVjcvMmSuw6nRVzktlv7Zn1YAt1M3P4D7KkFgg+nLZFDcRPSj9E50
DsK8OjaDXxMzxSD+/RvJ24fh8vcJK+jM7mi05NBhUqHH63Atg1jH0Ya/3n7tW30MA0jxVZel0J8N
BxYNFkI6INwWkzNMFfZm6mVEoxy6E5KTNSKi+JWbH0x5iLyjyPxKxfnLk6u4n02YsFNaIKOAOllF
MJbXr8Zg8zNLbevDvjXsY5kn2SIY6zqMqvF0Q+RoRZPd74TjMr0uED+xEtoxH40sfvDW5t5fUye4
S9fBG15pmGVRfOKaVFuaAUnGl+hRPhXY/JuJu1f/Ab/T/QPHdTsNX6iPhvkQIVh86AE7WD3+/oKH
ThF2532ZpvehCP5ZCTBHAg1+Ws2fhwdds8z50300Zm9kope2UjGA8qYQevb5Vz2yxkNsu2icRhit
/8QbXAfFsoJLssX0p7inSvtN5tdW1tcDDhZaaB3JR96HbX8t5YeG0MYNCyOxEo/BGXS6DNzY8hrD
6KqXFPoZY1L4goa1c6mQKHoXnboA20I2ZCeZxVxqRznEnl9okDP+aBQgqKJgS8M0cO8kFaMHXZLO
pgcfn4ShpajH96diRyhTVwUK/dMm51oSo6/ha9yCqpEggrhRrOorjRKQqUZeU7ZnmY1ZihKnOZsy
XuMZhegRY5a98ndL8ICE6rGvjX+TsjEUkmESo36VsYUEC2PlKKoSMYPlOdZb698WU0B5bwrl7Lyr
3qhk92zSqA84EDngUK5aeCJ7s+V8Ddp9zlzCG6h6zQgdrfcmtLGF1sJSIzL26HTVAuiPpy24T2tx
rfzpkSF18g/vHp4SlRH+0v3H0xHME6mTTOxWo5ncqeFJAvR1kD4gwrI561LrJLBbQO7KEsmbWsyh
K2GqdDHK937uhXYjz5il8Qg7hIP+LUw7JcvOyLJxQAfb/eaFf5HhWwYUFvw4bDpHasUqKkoqxcGG
OITWv56KrBOagTLpupjXrdddomvXTFJNJl1OXyaklLW2jeAzCLd44rpoeFyPPjKRNMFpwbwgC2iQ
MxIKHOuvkju/U9PtcUx6rmUEVgfslk69iIAQbyuS/7eM/QkV3VEanRF7JY5epAvljZMZH3IOj/sf
laQZVjxHOPTNWhFfDkLCyAOqI+RoouRw/oORfjfHG40XDRcR3h0QMNHZZyYQ6b9vK6VwVBHnSlQ4
CN7UUV2ZEJSwkqgevKQUWggIwH/Pm3PErlQ6NLPpfXWasCkSwT55Hh0M/7FdS0kSpG6ihxLgHaPA
5P/LmwGZpm5tI/DZsWDXIDduXBMLmIdZsXwEwWoK2M+dJLnrsHily90Mr4TCe6kicr2dsCklAI7Z
t4kLL3z+8NpZ+uFENzNMQvsqOC4UKMvmaacecex1g2JLYDBG6siXRjTHXuzF4rGTlIL4NIWwR+1n
QBBsoj44gKbNYnlv0JYtzak1manWVVzaEXGyzMxgvyaV6GiiMOl9bzb/zNDGI7t1pUrqj8OI/vxK
9+Xkcd0sGP6rDb7vePa9I6s5A0OwPfqpJX5GnIbXAUY1uvJE2NFRzSRLgGSDpaks3en2OelVKCCt
7Lkfd5IuoyMuAzA8wt95o78xsJcUS7iNCEPiZUaDNqFhC1UUs8IBicN87wH3RdW653cVlvt7LJg/
LG6NZtZj8l1Qpxgley/1YpC9VOazWpqbtspXPhZF1D0Gz5pusjuO4rqe1MRpuETW13xPEI4Y/bSJ
BgEWCFwd0HG2X0GDAXO9byweeVbDhENpcTCDcAncSZ2t6T+BRQ9udMu/EuJYWpiWE9qDPKWHwlea
xSElG3r4n/lH0IjBxUWBYuQvujdX96/tAoJW6oBQlEGjgqarRX8iQX3spZ46TUmdHOzFXz6bYI01
1gdytgbgBabr9Q0Ha4T40ghN3eqSE6O6e+tulunMKNk9ytDy31rxyfp1lwVLgFKpoM/GXDDd7QHi
hd4DCiaT3Ziyg3yCNRBu2Q+dAd9MsLD034aOuEoF62ED8c9UmdFdbSwdHoc6v6LbbKUcYNv5VWSX
T3uQjypzpCmra+CoBl2U3YEyVefiWCBfHlWcv1VpodJUbeAGzu+iwqcLBS+4NqCgKTNxeEOmodci
cJOMeerPC+LcfL19HB1+kuoFS/vEA2V66EwY7asgSVWfcUQIgJvUc3XPEAvQf70/Oe2X0I+HqlJs
GIWqDiqk7umPFFFwHnMWUdWwN+yhIRYD6qrpQBJRXsxEj7jAPGenDz/uf85K7wHZSx7PZkTtbC2M
9oALcBwcPiWcHWaFLhz+6FC0iUvY7ag1wE04qmk4fu4hjwqQg3QAn37j9KgyDbi73A3s9I8souS0
xWW50UDywoIRhYphi+NCetZMHtJmmyZBk2YLKVm2oQASp6aG7AAC5c5jalAsrRX0XtsV1b74pilg
MPKfaNICIhvcEauoyFTmrwwx2+7FgsdQSExjg3BynOqiTZqoaOQbnDWgv34pNBPW1reYtTneo5SG
mAbs+VyxUe1WFdCrZH9i+AS18R8jyHC/7m2vXy2UTqsUnGshVmwIVAwa9pzxhL66d5v8JFHwwYHP
4Y9Akmm/rHrsQAAb3Am7iRuhwr7UBgbYSL8Ge9laMc7lC2GQGneWPps0Z/baaI/0WCpv7Q745Nx2
n5Xog8kq1b86EPcrVzX2Hdk0KF/LpOw0WL9rLE9oq4WxHmYl6yMimFwwoYTU+rYu6vgq6dB3g/Xp
DvkcVUJlPD/V0gFLr5vZqC/K/MkoNcSkWaT42lnMmVgOZHBi8rhPYp7NYl01FgkJ9cCjWpeQZdhc
Y7KlZz5nNFUBmILhiYqe33Si9cjxMJgU82k+s8Ja3l+DhHzDdoSFcaZJlox4px/dSyUsJd5pMe1x
1gzmWXvFJgBMrFg9nHXTk/xxYplkUBUsRMfCVa4znHlL77TyeQxSe4LO1JpizB0ACn/KbtNNCUBF
4dFT+4u3iyPWao5TIwkoL6gQNpvcMPXkzFuF3RuqpCR4XnZoYcmRfiOK6UDDWW57OPt8xjQ9isD1
DSSjBNDg9CGqbiiyxx4nmaXW5tAkGfuq6xdSZid/2d4yc2h64gKi/tu6Iy9Mp1wmUvZPFPzifZJf
csUewcw59iX6gmq6S0/INRlHR6FleGHcBJJhbcOY4ftGSqCSIP7GtTXdCV8vN9GV4/RMwL7U7OsR
c+L7Ei1H0Xv9r0M2dxafbaPZW1niNetW7N/HnZPsJx2pdeA/lUWmMA8iLtUfqU6SLROIOD5I4C8R
fgwASJHvmTu1pQWmgZ/jkczwCFb5ZGulCNFiQF5dfnbkhVIhFZwDZdRF8DUk9cu5FYlHKXQooEs/
75QQcR4KMAV1MOffuWhk9nmrwSlGaWNgSRGgm8rKxJ78yS34Otqpn1mRuRNwAR3IPaHtu2VshrnE
xIYESeugXJeFTYwd/FJ8VOVevNcEjkuE9WJvswqmDWWPn8WR31MEVCSVcsTUtydBptZSOHHVaRxA
MMdf2nwxWaZ0ULP3KfGmC/+tbKxK+1u8JeoT8QQrI0xG3LowWUxwI2zJL2vuFKU7L3aKNmr2o6kQ
+dSMcG36ZSoKF3/D4jVgjgynXRcwMJplkqAY+B80AcHsQYOT6qL0QYX0Gl4Ic6H+ZAeeN+xlVhWb
aEJyc2GzMQO1+1ElFgt0eo3GhJitP1zGVXafEbI0lutfBvn6P4r2TC5Q01ZSEX6Egw6TcTo5H2AX
qCzAdEAraTsQWs19IGfYAnil4WSGyC1WJRPiTWAg1NFII8OWIvyQzyT8llHtigUuOOOcnDrOvViJ
xMWLJgn9y51OMNg5N7H6wH5xAlJGvX/d3gPTopLEc2OKJVj3aToQ+pG1shwyPP/lcgPpZuFqRVhv
AAz/9HyGGzXMHvXw92kvM4fSlLajJAlUiXUj7u8t4cb97u/oG+f4qX5lgXne6ZFV87Yrgem51ZJy
iJNbXyvAVwDVMMOX3aMlie3FsvIaqNJB5GpVZwpIiFvtxWVGYV0wAJCQTkoxqvgVOuedtR1dYaSR
6Bq8DGAM3M0Gan5/AQSKPy+NqsiCiUQ0VEWnMi12E4kMHReyoFaoKObhwpdixFbe6qPxCc9N8xcy
Om+yt/XhNOfSmNNUEjk0MvQbYNnYLX2Nw3dXsRHnj/tgTIM3fqY+0FJlT3WHJKwmW4CTaS1CZY7E
3mfRqstfIQExdffpY+6tlnUGy5h9MaCiOq8dF9C4HCk+ZflBITJx2YjLbluycss94bq9JI86xOOn
ZDNaPy3W4VHaxmnLczr7i/uQuboPlpngTgZS6YZI9OTus1Y0eqFbybWu6yh3zi5CB9ueJ2F2rg/7
FX2c3iwTNvq5OsP0Wcv0pOLrF9VOpL0wIvt+hnLZKilVEcFWX4lz98X9FPa8EaS6W1pyka84HmKQ
5CHC/WbRzZGJEv4wGH7Tu731+Q3Vw2gaThu3TjlDrKmVh4DmtRD0f9UaVKjuEj/eiDrpp2LTgT8h
iqwr3mt+PPuEs5thRy4xWtH+9dw0lSnU4eYjIj3JoColbSyHi6MriymVddlI15gaoiZ2n9bu2f9m
/mESqWGkfNwo8lcUePJ7TU6EUxNcmJ7MAQJb43y020vc3Z/M3U9ZV97PM1/ysPOKGqlbAFOWsnED
3klzdRaGG6xuy4njRevxTqLZoTbHEumup67N3gr7GTZFAUTWaGt9kubK5iK8dK7We8Ei/CFd309I
zhUh5xhK3dGzxS5KV/JexFvCp79OOfSokY4INK9TKz/v7AE6R4JXwyodgf6t38zz5iAFj/c/faf1
U05H28yskHSH5h7vw/74T8X61nAyjVp4aYEjrS6XIOGkjaGPivlj/e5Gx3O/EpgXe0IeuYdMzbsI
Rhk9oE6foyaF4Ce3a3mnXWXtOPRAKFM/u1Eba53jIRsT/UdSjjdubPg8TYNrxeZx/8jtCYoxjG29
94nh3v2KIgWMWWYC8+t8fdQwVVMxu8QVO8j9kLX3bYo48IKuiyXsQFLEtAexF4QWpwOyhr+pt8Qy
ybPunmcJ/lyCJuOihM3wHjpUj9Qtht7w54oJJOtq525H3WMTsOj+JlcE07Ws7GHP1CB6ea7B19Kl
OZKVif8Uo0Fal2wprrxGCPLBcJe9koi3QsqAlJ/OyRwBTdO6bTlz1lKq7mP6O0naHETygAfSCUmR
9mVfY4jtxMKMQIO1S4wfZjZt/R4imzFs48pMT3n3uYJLyAkWpnLrn80KFOgi9IRAGenbXy+WCOg/
M0D2gtFBrN3HiNCK0PjwwJYTFOMCom2DzzsOuT9pQGcVSvcMEbjEZm8lR+jV6JXZ13E5ZTPVFnK3
f6oauxmRXhs25dJSlsae3TCh4BFNPQf24r26Z9a42dT7AnuFwzUsijtrBIli4SZ4mOR6LKO4GGoU
DdbWZgv7GmiFH69XUw6KA/kCCOvh36d2p0rmJn2kxNejlB6ymILXudUV6iGuuH8HEZErqzJMTmrl
ecgADoAdLOgIsJLCInHqkICs86zmDw2Dkt4ZmJ6zCLtrxpLeMLGLgVX8Z3D+f3QJPMVbun3kKewD
7/8bQCTQYDZmHPhuamAacT7/stApnG9xpuw7g66fv/eaXK8Y6pdMMAGvIYW7zom+SEVdxh0FR4gy
fqTYncRcVQNM5tICLWbZEBqfoG1xm5Atp7x0Ch8syIKfqExRsiHkJSffhYuV4gl34XOOTIYIqrVx
o3CXarFysC7CZntb4Xcw3PKA5h71O6um0Vr84PhEpCcmCWzqijPRNf5+W8ZVh3poPeH1cgd6afuB
w1qhsf0DOM97xZqcg6c7Y8pbVudPNEIfTHesoTDtQOvz1wTrGHsk3FVVabP+4jNr3t7fJCMhrhax
HemJ59RTZ3gus6imzTAX2XUTyKy7HrTEZd72RJ0oMknKoy85z1pwwPDCTwV7rh6TBpyDCqQSLI+O
DHbwHOvuWrGOyz3R5a9CHYnWnWipeLdcx6Xh2+B1CKKC95gPDZt9mAMKGggSGi5dmsQJN+H+KBOX
gqfr7tJcgU7AOKjbszsL5hhiVAcACzKnxMC8/1QVu47SLRhWCpMXnE9IrE6U+o5FQOj9kLFbLVcn
a6wyufGyAWMOL2BtGd5OCCyIsQbh29vJ7/xuKItYBR+I9x8HqbIp9Y2Zn9Cuo0eyzEWF0LKAXG61
qtf/4XSjT4/odx08eQokiE/nMNFhdZH6A5qE7g/hm3CMhbWdofnkAbyTafWTYP4B8vas0KiS3fEN
KOYF6a6dUGVCQqsRoSFK77VRqwop8ELai+tL0uJMFK644czT1AaNtpAGGVYkj8vqHOdDtYVJyjTl
LxfnCT/fL/W+zuXu6n+Uq+kUiZoCXabMubnaqwylLuiF1vgzuv1Gj2PzOj8UYxNC+rnaq8HGkXtF
JRABfdq00b+UqhJuAHG3HHRH99aHdQ1IRPfP4WxyjlrWuWFRC0JldHq6nlGfWf2VTuJ57w6DQs9s
1Ojneza/ZTDtKQC3PHLY++p1xPsT4X65gPOxv8dm7VxbgcbyECimC5awWRF6whDGlA6rvHUSci6q
+5OCnaOKp8VKXde657iHwSwNvqqgVB2Pf6b7LV4FbnJ0B4B4ljWWu467w+1yuyLf+DnXlZr8K7bq
vcdQ6/L8oXRXnRoBas3lX33clUpcoW9WEpkki+ZitAoh8igvaMJVSAzc0Jf16HSB9i/zeyi7Ta1+
Z1+Xy65mPHRvsxxfkFDMYBF43SNg+XkIMdTYgISZAeszNSc1koerCR86KQEh7NopwpJFwaRs7wzZ
K/gbktMvafy8dHC5QuIipqZGrqiKdE6AKj0GabbU4opb3gTPeD+Rk/Ql3fAEuuUPoXUKtbxF6bqA
lVrAx29rUE892Om4XVgZCsdNxQL6n/C2EdsW8umrkHNoTlrpssDmuuLt5pjFzm5RHArhZBBueFcR
CcX40Kz8chSx1D9gLYkXo5B9wWM20Nc9ZlIEdt6yDoHG5s7EWJJxo4n1+h/hJFTiQ1hCj7e3+IzR
DzXVrVmUxPK4zIXnAX3imp5kAkQOO7R+7sPNrGJjDvFZu6lxlV9zeKDEMlhpOqHPBTFNOZ9I0IwU
/L0Ol+nqxVilL06RqJGISTernI6KlhhPneCPWfdsYY3q6/7LeNtGXPAYjAa33SkAZBy9V992ziu9
ARN9+ZkIpLCZZ9nK7+5paHEC6ABDq1Lx5VJN8qgEpFjHYwcj4pwT7KNmvUmwggjBWaTnR/kFhl89
zWDtAI2menlB/h/2DTAVYyiMBDzO9g0bq0Fe6MlHgy7leMAfJyarQdAeRiNErzo6UwqrMFOkCjHX
MzS1QWQ80QHeM7EaM6Uc9oUtZAcTOh6v+VXqrQoZ2wSl4f1B/aQn8w6WoQZoBw/9oT/mNdY3VNOQ
T/Mlz2mz/dm1Fd4TnjliD3h++O50pk7zHW5eIWwRxNr/toTI5SytuW5b7W5KILqv2WrCgG2/YsPe
1X7Z8ZOXMLWFgvHpx+ZdyXetNrgdLET9qJKmF2iqKiXHfFjUmH/h64Si+4OYipMlgWwzDhMiLZBq
y+oXfmt+xN7KsG9kUAkd8aTs/RY5358gmGCmL1xhnxumVJ3bwIJznXrHTh6hcS9+lAlMlAryiq8u
P6XZ+gGGOnqVskd6eav/6LgKbPSfPZ+4QWfcmFFYzkGguCO+PZZvWb8m5MwWAkUUWax2MtY6qj+4
VfM31Exto7Ux7xFrO11/E8kH5n/rBLjtYQTdvb1lo+f4DSD/voTPlfzwjj063snNfch8mzVxplg+
CijiKDeMd6rY32DkLE5O4IZlZGjOZMzzEmmX59YYHJNmuVcc0nHiY6Xr3zldZ8SaQxrLZrcL6R4E
mo3GvTbXiV1K+fCvSeKyUpKnlx7QEelslvFTyOIZefbfrjVJgQWHzEu9SxgSB+TIedYAuUxQdiyz
/SX2FEYfyKNVmjysb4lblXvNZlhzYOF0JLPd+xPIjKfecohFZwXgLs1aftT7eyzMc/KX3Q0hHMuO
hhTHwEEuZVgx9Pux0nzKddTZQLBQjAZ7SnP5oHpHjA2U0TRGX9fHYG2dURYHceTUgpL0NLYjWhWP
wvTlUj7rNeoeKzujF6BZ9uikf98KmHIDLGLRVqwuL3aVSk8iOgGD6yHIimW3dBDVHMvJ2Gwr6VKu
DKxCcuu7MqtAPdCET/BHcajUv6sVWo/B2xxeqWRYyv1J7HCMT9nHsNbYqzYY8YRrU7uf+nzFsPil
iLung0dEZl8i1UIli9VBaiuIOMzsqv/9bpPbN1dSqq1d5PcR4fJRHAaATr9M20JlG3VcDTDfgzUH
okCGPN9gGRRE71Ik+qEL+L0ZinlBqMeC0PZiFoi5V3ygbT+sK7mLJtplp9fvY9aPE0ItgL/OwsKX
1CvJoTYr/bZYWVCBKqDXXKxAdda5a4MYw5XLu3DZXBe9FgftpClN9JHK3K3Wk7USMHxLyqmCT8PX
W9+sTMrqmj3M4D2r+g4buxKwGwN/OTXmSpWNAGrQRCLtp/ptf7L8Zew3x2RXdE+OfSFOsBKjcVEa
j0f3L1HUdDche4UirnjqUazcu7f3X59onCVzXJ8MlqBsfGhT54ePtL+wnLdSdXohpn8HEYJZyJbq
Tv+VXE2FFQ0E5JhY7Aginl2c0Z5RMV3hKWtYwgg7kGp+4VcNfE4f4uT76pIsb7a5aCJRpYyHh7js
HGfgBbWCm3hI77xp3vOIISokY5i0083TiyBeCLshORk1meDhm1ASuNSz8nlIvqbig7wXn9fL/P7t
68K3S+z0NKxYiBtE4MAEytAIM2xT+FXhPwUS29wUYPUKx0D3/opMnthFcPhulebrgoGzCc3z6FBj
bHOMf60uOcSduThRkWrjj2RyfROAS0Ujnkqzg+g+rvQel/YohIACej69uK2/G+Cem9sJzhAE/eCR
qzt8PSOR/qsbJgZ3gWUdF/Uu4STwAswSm6yEUoBGz53GIyD9Xyeps4JbYycLP3vFChpnzPo1kiZN
IZ+L1Hqjo0Qu+EcDsV+RBVXl0Bqi80dcqmRe45waI62H9sLfh3fQ7Y6QsVIOH2YvxmCp5XmyztJF
fcunuLzw2IR4fNa+/whc+tvf6AOpvKBxHue8AK2q2EDtnIQ0xUg1fAjaM8Vz7G+eQ80rrPYsKJVo
xS6UGV2CE6V7pCSc6JyebTqdVfFcUHTNsiEiXJ4Yaa58aHndSekD2ZTEJyVVT1FQ3iT5BH5aJIx3
Po7CnKrvDUzXwpRddt8SALZ4mmiXhbu0NkRllcAjjTnpISTkUPceWgvSh+/VZ3utdEuAgwvc8HAD
1iXeSWOLCQK5AvgDyLxSmlYdHPCkHD4sIW4SjxQUZS58KlD5t5OiB2R/qn02fX1/cIOLgo5h2wvQ
rwyrtR67imuhqqhxnaf9JU+ASgmWmMC1WBRYK1zwANfXZkepds017UhDSCPggirV2TxNtaEPAEsn
lC/vIL6e7TUKF6gNE5YynO+/j1yYI9TVRej5y/frYHn4YAPt6rqIWNOs87G1G2TpF1CoX99SOcuI
Vvzmw+VR23NlAT+3d8nLfYat6Rl/WLzGKyFrM/rPTaCFr6iWNohqxavMHxve+uSvllbERG+40J5r
u3IiV51ZPsOyp8YI09H91+n8Ay3kw/6FK6W1qTwl+ahCU2cGj6FxMlWELNJjsj5SBOJwfzgtiwCk
rsOjURx4umpTKpnJaOfBehgB0EShdMZ43SiyULpTyhExvVzLq93o1sVlkxujLszl86/MnzxvaGGg
5nnvaWPaFkgGkTwPeAlextGqUWvwI+e/P+iSPBUpTnTVZIwWpFAD/9Fpkgo+jsGg5KkN01PJ4vwN
HwWNepDhw4051T4lYMXG4nokm/z/kYnHK0QqKbJjxpHhHTsNcyI8bzvhxjgzK+gQieMiGX2uIj5P
broBNU73mcqxQAF/U3p6GW1KsZNAwgu7FXxez/HcJunzciLY6oiS19HA+88hScdyYdGavXW79Dmq
sbERgFu0nFaU5fafjQ7+OFt/dVp6fhkFEVpoo+THPWUd2lilKU121xaqS7HohuDxhrCRPL2MdJZX
oM7ZbFVzDBFAeaT6rLjVbJdOx9FwKiWdq1uR1W4RjbVO911PWM3jRCuTvz+wGttsz8GhfTUPNT1K
rID3vD6Y6yPkcOrlpKZ5dlFHTwWGwgWOc2Qgqk/DMveVjIc0i5myycwuQlBFMFLTu8Ar4UVKZ6L/
R3aJDnAs7welXzKn3ikh2I8YXkDVRoKAbiyP5IENFvX9l1pwCmMy+nzWJw6oPsHCosmuLva6S8Gv
Is+g2GKPahLquJ6Lkc4wOr0DTlNlyZGaTfQDr0YQtYg6SnKJf1GW0ohfZKVxGV6cpqM/m234C5Zm
dd6x3CuNCzDYtEw1twAz2iQiz1qMf1rWYMFIkHSria6ryLQ5DgeQvZnPmMIz1uKPDM6J7gZvTgsd
uIEuBsswYRrJKCwh6BXQs6MIabfmf8v9nQJEh8R0+5kuIm1Ef5Wj3CgH84H6dHDI1yUPpKE6sI8u
xkYup1Audg/MYkEhOkWGxkTm3UsM+oPnmKnjLMwM2SDHE1g/CE7WFjjjsMjRFsv0NiiGc+Jp0Z8m
/1GzIC14FxOhPYvfZTdNYZ0zmu1Ky9NN7OAMg976tiVgJePjR53BhnoIVW9mbonSLbizm4QmzHfw
KnIfB9/DtAoJXRBFGmnlpd7AoMkkNiBn1cylC75p6JVXAnLy8SxHGIQC4l6NcFVngn1ylJ1KLpJK
mpW9a25IqqfcTrQENfDTDesHARBgsGY9YkFj5BQmcaBgc4URrz6xnCQG6idLftthtBYWBDAVtV8h
RbuPZ3tgVSwZ18mzPa/HVdMbPWB/xMdNPJF6nGVc6RLCxMF4OJHqI5t6Dhrny8CptNh2hQQB6269
R+jLge9W/amhJrRQDQdnGHRaHmZ+PHkR+C0Lrjl4usrf60XMlSQTlBJ8zJ0b1VBCkkluG0L/NRGo
JkqfbT8aqxrlJNqX6TPGL/lroNK1n/ycCpxo/hegCjifpop15D34XllkSoZDTwbp2ZynQ/5Nw8mP
0TOIvKD0LRbsBmCRk66Cw3nQM2YnCyyz6AcKh5dLH64As28nLCVHnuxI599FrklbtukzMo9ZgAxf
Xat/Y56MwzybEDtgUs6pIjTdyuESL5hq4vE1bFKr4NPrb2hcRjiGiAxNO0As6ykK6jOEMDKbfmHu
g48nqJjG67iGrtFt/QlX8quz5u393megzRLyC6Xdbt1yy/sI6EC6fvn4Cmt/7fq9obPNztcBV0aF
tKwk8UNwDNM70l0yS0bn3tEQzdFLMrzPG8pH4Olm/K2t+fPIXRQsFjmwUZhremPHrP34WUEh7gC4
JepbKYvC/4A/WSrYQSX3CDigUGDNetGdrUSYRbMCxmdB1uVh22O1iFUZOlF3KIRX91RJakWKlmTW
ltOV2zoCro6b/62k2ht4TzrGjFqkRPfFKSUh7esYOsdXaJhA6GF343V6dBuzRLeV1dB5NWORYgPV
hPtpksTR9IP1TP3kh37niv0wC4cJ7BUdKjbaljH5gAwREGz2cECnMyyK4t1BK6TploHGm/SGGuM0
EsJM9tGcKoPz/0KnTOWTtpPZXYlwU2nEsFhSdqrqDtzP163HRQxjtzFkKx5CcLJE4ASO04ihNW4J
RLJ7IFdYd9Mfy00Tf0Qsgf+Efucn+NfPpeQKvL0gNdEzUkJTIOwFU3DcEnCi078zosCnbBF+5Vrm
0b5/nfcKcZsBdZ5+ESsFadgps59PTY+mPV+gtQeB4Qjjfq6SFHGkzN7LIw7Dst8PMIpcMLpRUHZG
JEHxkc9gCxgtU4Mub24rRUK2310wMysZqZsQFSAfZ/HOnynRYpqdotwIv/z1W38VCuUJoHx3o6Hz
3bXkoYJhAQEz5fsLPTWS7FNLn5neJXeIiB2mJWGeY0nVM+ipfo/V7nKphNnkSXKqMrdVIbV87Ovq
A/wtCDHshq6jTMq0bK6ODWPhGSwJEDyfSYQujXcVjcxjqfoMiWhWubEtNpVPbOP4nqpS2JIY0I0l
a6h0E1Q4gjQ5jRmA5wrgdnjraKhJOv4Von9apHAJVIXiNWw1kiJBg7Ts2fonT2gDIwD7tLDAJYvN
01J+Tz+3cgykWNzbmaZhqRt6tNgZvXVFm1GDTXmq3xdLig7zTKy5wXza2CAyqTfwpkDvLAZ3+SXj
Z3aWS/huj2AuCJD+G3ivOetG4B6nuJWDw/5eF1cdy4WdX1Idhf8wGH9HO24yMEzw4MHAQL1LivPB
7pW1BEXKiQ6GKt+YrN8akdumQQDSaHWEt1aQjtVncGGV1CN/cHJx1b3cNCm47BTFK0K82eRQUjSS
HQMw+YG8u0NNeuqIBbveToCXBzkZnUqp7Y1oy/e4brQrbywpP3IWk4eytx7iALvUfOPgnD+6n00N
y5bEkQV6tnErvjGZDRUe/RxoOHdKZ2jL7AUHySwIWvt4mPWbbc5ug9Dp6VdJw+0cb3pXeiKrsfqH
F+mrm+o06CLphoJ2j4ogcI1A7jXNfAOlHVkFpMS1cF/9VaripUyaOwhnb1CrlWAuevDiKgU8pnPd
MEeUNPcAKoJTQeuNHFpkA/DTYMQFV6YG5ijRjo1tNXkzGZWm/nU/XdHHUgHFOSq98GLzE+VFzmqB
9d0C+5HaVbtmCcUCi6JKmLSi4Imnd1UE1VsMRqrWB+UYamhsGAghebhOZSfuIMpQjvTBdY/6P87w
/2j/EfAxO6LIwzJ/aOCMS9dk3Pw0/JDztK6NPsKyXpDz6mqpP4MaNFCcA/Ri6m9K8QkU9kVHXDMc
onDXRy5vVKA8yH2k18T/pbf3Ii1wqB6CwLGPovN++g0IwG8RxoRX5GH/MMz5L3syJAB1lrHEmXFa
ckYGn3jvokBTEuXtw5Sg7nrJrtfhQoLa0rR0YdUkLnGtEDUt9ra/0Qx2MN/VjXIe2zw+eY9GtFDI
V3wyTHeftcrFr0Usr64cBEZIOzJgxa7iCM0N3Ug2d/YRAPzz/8l94TQ3wkLRsRvut42eo2kJ0RAc
nv3mBBR/lKbBWu5lMmKR8p3d0GnA5/WJXOoRtsWNywl0H1spuCPicZeEMlCpm40vS/iaUpO1fHOJ
Yofo/qkVTTLZXo9PVWHMJS2Xpkp/1kIzUsee9D49am0TCgucXo7xPAQ9dgX4aWeLI+AGIgWdcAbk
PxAyu8AZLkdLO+0aorBn5c/CUvb7MSxDh58rKBMu0mPmww92Vs6mMGTYvqHK7/sTewzRATs5MzqW
ibJ4aeSg4vL2DyJ3htH2hHF/73re2qrmRbv+MIoxj5Vd4pt3imAIGsRov/UIRmVZlfzjmFjC1Yxc
ns84/pfOQfs5898i6jm8O/7cyv5V1pX11vLA4kezdbWKMVxJCQ81EXaZ1CbWC/JaIidXHjJq+99i
TseuXwGYrE59I+xZo2pTvZvwnsHTvv2LoDBtjggNs+rCiCjHR4Ie9+TvwS0Qf4Lgw/jdvl6FG6Yg
hL0MIY99274EtaYC6o7UaRaE7eFGfePL1C+NYhjiHtyetZgW4jNMT90M6ibxEKGxE9QCelwgFghi
G/+GABaZgjD+/j9X8wNTYOTKQCiDNYZUwCL3x1ks7RHmJ3bHRYY5Xt59MigIUOIyy50Z5O1Cv3Kz
GhvulZYrlEvmFkfCvQ3QyDqLKZdjot2HE2Vk9Ot6IwD8f4txdUu3ZNvUXAHdkm3oTkAwoJLuRwN2
w89AF+N5kvOs20kxOKyca65hF829ZgKE2NrgKqISLN2NvDFk3+wHkxgYRDqTjtudi0iOrPdN+JOt
RSvPGHEsCzazIAZ7OGKvE7/fgPI2T2WqKtlBSGu28vUUkMupK6snrJGCP3APIytihB4+4n7L7H9v
tITg32UGJTcPa6YdYk8fWCh/F2fDh3B0ExbSOfV/cwgdQDPNnf2Kt8MrNRax+dM6X5vq2ZDzsyk3
CDi9CK+a4ANu5LeHK5QAZKZ8fiyEERPLbyjVwHuIV04nwcFq0qjUfv3d9UvJAAZr+KlsMLG2Tinc
iwrOWzeN4Z9Z5vxpaC1r8RfR+dvIcK8pvbspHpT1OChj+EoXho9yUcFLuqFZQPMBqk9JhGo5w32X
T4lXLJnEa7I0XrHQTZfJ0D39GV4AePjjjSs7InYmSrOE27v0y/zsnytg5Mr+hD9Swrsf0V6+wcx0
MSoJDoy/48jxiNAxduFiOa08y5gBDAZcb4RGUQDxPHZkM/Gv6/djbmnXUBSik+d7uq87oO0ICQYU
5WPxf0dKC+hK0gJ2LarHqf+t/+AKjZFsuFf009pgp6Z3jXaP17s7nH6xMk6oL48bVNUQYu53UR5j
OkGcGj+cv/0ZLBDckAxdFoCh8qWFevuQ2IdDUBkvUOrHCnK+Q/IHMytfhpiFg6Cqd6WvHnNWpQss
T8GYdGBGHS11WgcQ1UpVhQtd3YVTxnhNDqFBuHWyIni2qXQw+b4PWz6yL71dDtr8tTSkcR/lts6g
CGvKvKKJLnNxH/oz8LTDUUYs02FCXHBe4xzHbexEpbod1kDHwIIPIDvEtVI5DmVDVXSr4uSHzNBN
fAvjOQIWgVGkreiIV63rQYknISTzhp7mSVAMaz0KEFwxb0rf6uliPPWo9N1hWGm2DviL3X2BpAUx
1RqVIDFEdod3szRyuh7gCi84j5B4WtXpH13emGD/gSAsWSzcY3kZDKzX4cSLwsf3TdUBbZTr5Ju6
M34Wk1tDB7lf9uM5RJyQhV347SD0UbZiEiwUWPaAtpqWVvtb2mJDFS2bdpobRGWlxSSLRlHXkdyo
TN3ZIXyxLlAq60VX6QVjurm+q4nqcgbcwxEnaRG0V4wdsQVQ/gmFk9cARYiNUxX/nNg0m5EgiS0o
SH9tNazA4gkD7Zk8vOfSp/RhNx8psNIE3/ZPylfxeUbKP3RvU14u5qfNSIb5dSQ+fsk3lHyeEzEy
HeNTKtSUuFTVbJMa+kk3XSx+n1CFjtE+uH2fjzXm0Wk/WIv0gkoav/V6wljSHnGtZUouCOqX99I/
k/hrlkTZLsrT3oCQ7Q8Huw6H1fVkoDc5oZSHra4jHMMQ4Xoa+sspg3I9Y2YFRZpSGXehIE5teY3L
bYNvPfdVsSCR4hsFP493ubg2EwpvgWYIJJRdfQnvRZk2MbHE+2hF/XQm8sgHzqvV1+Ba3DrO8WLG
Ad9JzVbrtSi3aythWUdfEExyH59Fu67kT8DYY0Urq7TaBgoJcx9XVYbfIl73GV/L2HrrgPItfFJT
Y6E7iGx3MBkPNsKu6ypnw2ZoX/tnzatQ8VBH+nJH9CzfCV32Ug/OKfA+kjG9SM1xdC421DE1UV8X
qXvrSfS3HJRJN5xQwe80IFxkJlsI1sdjXNfwntJxujkXNTps1X2mAP2Uu+8u1CxsPTFORpm3rzGi
4+xXZVnYAmLOXwYlJonXiVELbUm25RE+K4bWvxesDL8+GD+WCY+pulgCe+mmCkROeNO0KMkZ8EW0
VPZdpYr6nhvm8IUAiXOhupluL651zb3bh7KpGZXY9QP26LxYPa3HTyc2XwJ5CcdMAXxA+yXVkvGN
CUfXlXNeM11Bg5H529uHTRh+cTPLcfC0y+Sn9Ft3XxiWoCEdJm5+RZLZV5jq4qlf6FcIpp0K5V2c
HH7V9Av/ewHd/gX4rA7MbAn7olV7p/7X1ykbwVLqoslKFWl6Z81kThXMdQQdwsMS2thjFzPcHuOH
FXemNRyRGlht9rPpZXVlOKd64uNXxe1qpUznehqwgQF7kBIWQB7bLhdto4BW27ww9DsnffUACjPp
LRVyHOYOMBN8LWUbIpzxQhRMczsbEUbJ0A8Wm3lbUHC8SEBTLF5wZ8zymyk7M0Pgl6Xm6MDBaXjE
CP6GzIuHcenNOsPnGifAsnui5rxWYvlCJHs3nAk16Uo3+NmxX6uHDrtiAI+8Pag1Tmhdpwhn/vV/
5oI185JpgrmorZA84wKigtf/yK/Ftq6ueORfjKWIzXpArq+hl72VLREefGjcgv48Vww/RZATE3nu
m0vsgSKZngFYSdtYyAoOP6lpKz5ObZB2PUunE893GoofyapbeW7rHJAAZz7R2/I7mQl/e+lDH4EF
AWdGz9kzsbY2i1oWxMxjr8uAuHFF6k5CnEjjZ5EJM2vVMRkDURdVZZiRrOSsg/A4ZAMv7OsG/HH/
cHu2xYxZ2eEC19/pjpbaPjEvV6LFXsVOsJLnZiNv6vfiLAWSojGpMCafkjUgMAL6G8xbRaPyuOw8
ga1XXQBEAIeoGmt+5G8IzdZNyjtjZuqeZKdYDhafM87O3K4uteBzJmjfF/r6Y3JWGkLJsxu55OsP
TukqWOr8d4OA2zNL2ZwFkT2mDvzVfQHgy8kwoSaGwPndGhN0DfQgkvwOTsVpFyqo19tPKFWXs6rB
TugJU3a3DP0no4gPBWl/SkiqJNGGUQEiyGeC+glMVtaRL66zT6GOPXh01DQCo6jCzUZXsc7hzQMe
siNZf5B/kuDaI/HBS/poK6HraD/6SeMUcZ+zLB0CjTPfnRGBnHINsiXO8U4dIrLtcaZc1+qe9zK9
5XPnlxH9IxX5QjWZl9Upl07FPguItjr02R1kXr/xmh9FMaLJeEaVgGoDl98oaL8MuBO74l+MKMa/
cQBCYJzEQkOfn4VxtgkI/qHH0whdBtqsUSHSksIAv/SHCAg1DZqP7Zb5OV40XBWd+FIR6N2EsNEK
7hK2uhKvuBHdMeKqNR9jur8bJM6Kvgo5MCp7+MjB2BqJTAvoyfIIPPU2o0/3d68fv1BqR8CPo2SN
dV0gBDS0R4nYnELevazVPpipI+1mjJqpyI8uul86PdBDEsNX1vfD78rIykrD5mowWbvUwetJ+48w
BtoifXzdOs+DII5/4OL5IvGfWaomtjrFlxIv4Hsvjc7w9rl0abIX8v+xLM5UtY11Zm3trRdvfUsw
2ShQ+b8dyqtQ/oIWJ+hs/sF2ELjlDR3rDsD21F+doXdVYvSTUsAAHBzGnEKMfP6p9n5vI1olALnz
Hd64iDYox5twfhflenimO2atDQgzV3ZEG46nIqGdt9YvmaxvwrGJHKu5yOR7gquJkHZ6DOIfbaOd
2pLuwKdG4gmYpQVVzkwKxL6DHh/TZY8fIweZwrDwGQTx1BI8jYI2BxAiloFcGnpGyJfdmeIBNtbT
ols7Axqodetlief7gb7V+AcJUudSXiVaLPhVKlFSqYkcNeODBjKO+Rwb7EadP8IYTk5mqUvhx1wY
bM8+B1c0ixn33N4Ot7iplFJ8uE/uXro7ah4mIH5ghBKH39NEO8nDLGUe/9CIapvwWUwl2ardEq1x
GUew1uFXZi1gcXKZRwV24XR8HyqVYEfI+sj11QptFBg++J/FdJLlHw88zEgLzBD7dH9EGvHwD7L9
FCQ44M4oeeyot3AGjTDE3DT8YdCi1ILo/iVJ2huJJtE96YnsUkj8i9ur/0eNvrZkPNH4Nifua06w
YwtbKO0FMJ0qe3ZWn9N+7REuTmYkJ5WiYZDCJ8bcntfgDwsamZBI1Ko8b96WygDwJhQfdxK/kJ+m
k0mJl99BtcwYB1aew2BN3F+Qsq/qW5XDcxvD0EU2m5gcnMTUXC647XFW0OdeZCpsUJ0Vb5h5VpDX
95r/xb0HQQm7MYsc/t+7s0Nh8ZgrV3WZATCmijcYfgRLyLF61G9pqeCNYA9veZ/L4LmHraqwkhui
geUSWZpaJb/hMDjvAgW1lTsCNeTCG4TZsANN3Uhs4TtaQWX0XBsxxRmJEkbIsEs5o/V9Q+QDcq7u
1wk6ktWSZmZPZgijcrnyRg2yDNcxMWu9R008EZBWQU2EtRzU1ikhGfJWXrE/9CMylE1LxL6O45Lc
fsoXKDCIg1LIye0a8vIu8CRwTyk1fMgP3Fn6CwbqtPbm/olxB6/pO+W4pz8/zs+fAg2z3a1y0bbm
y4YKoX+nLWeo/l8crwPhvHaxCs1hJ1Ad4et9AN0WSoZkM8KJzR7rnfvHGpiKYeTMuWedAnZnuNDM
WgUn2oVjEL2hcWTEc7aTsMJvQpozCdaNL56FBXxuH0kXsgY7aWAcPCrcqxPmXu/ml+y9lfwcPg6w
MFAqnKhDnE1SxQ2if0YAWyLdYN1SK1kxw/TjydLMJwHqMI63503KbEbJrx28XW6wOqe8jgX2i5/r
sXxbzCsveN9+EBgIij8Y4J+5pQruei8JoxyuTce0tfmkU1Bx6VHuGCdA11lR1Vvcku8U4i5kyqT2
Gl89/xiLbKRjsluxlIz4JJtY+twUB664S0T1mh4hDEV0JQ2L4dARLBiFRoHaGUkh28UKAZO98GG8
07AB+XT2aYA1YZPm7Xc2XZoJdBhqpxU45APUMMk8dPLqpqAx+2Vjn/6czKbvXT8jZJs06AB9C0XD
wRf+f5E3OjhVVFhqNlKT01G7WJBm+H9xskM6N7R5Pte1vxKO47ohj/Oqc3eR6LIQAqex/0Vid5tP
eCtenUfhwiQyZ4RumKX8KlFCiUmORtQcWYQV6rUqBs5PlhvH/UbELtJ0PVhLR24BJ4kjIUCC7ggB
E3v1LK2JHwIh807W2ONFaFIThJi2V1b9l48A1j4v3F1TqnOpomgi3NhTruQqCXymablsYqejmZpP
5L+WJ7fLpl0cybGKDUZ7zl/rLe6PL0fe3Yjs7hb1FiUuqe0fGsEj9Qf3pzy/1JR13z2FqRxNQ3JN
EtYpC0ywc7WE3ysoTmGOG/W9ec9eO0irBORqUiHcDw9/0Ajssq+0c5u+HfhvhXuBMedLFt9vE66I
tuNG1Z/hgGJ85EaPIRu1EGfvmlGIGfD0/p/2Rzvtm4KgVTDJ7+dV+CL5nFUoFqlqcdYK87LJx/u/
X7VLB0+a0F2L4e3yZ5idxiiYwUB+OQszESeGP2XaA7dawTjGyF/ZGdBMZqUDyssZc7LshXzy7wQw
gcNEjX8lZvFOFUPhkykKCsFbB/aKAuZDerw+k+D4W7YoBf9aVYfx05tgf+jgwZ82YkYowjNvUhks
I33KvJOfFAc+RFIEqDTMHhpaSQ/N6oSgAnShWdWX0AFSy5dFT2lSjqwOUbQ12KMBtfhhdUnWTT8U
iYXAO2/3LVjduscpidjnYtnAZhLIn99inueE75gHxn168PrgNQySm5uXhL6xBLAdWH7G1Ef8kxws
VlWbME1XHJoH15mDRqnHC6Zlc+NyjCipEfFStU6VmBgQoHgylu9aK0A/4F/gUl9V1uNlwj8G7qh9
LsU2LbovtQXMiFEBCb68QBjJ9hR6kFSCJsPZqyJhgtUOeB2QuiqecBgqtDf2IPEsCsA3oiir7nOP
2szLC0fOlXKYCysquJfw4v3pxMqtenOH8RgZwSycDvI8du76RQ7IOiAMxN2gRn0/6ktzAt1wf/0p
W/4nuX7zODqm2uasfccop7A8TdOtRseTKX3RT+LfAzdWyfgmgkpt1FbtWmGnppPaz6PBkGNhCva1
wdGeOK7dfB8+AherwiKWlGJQCnhiP0iWFC2PQcZ8pJVaRo1SVmijBKsHCp5Imu922igIbuaVgX2Z
0BkPRQ4Tevl4wm5P7Sgi1PJay4BZZgWnBnL7BfNGdbrceUsoDPKA8SFg3wBM+0qxf5D6URS4TgLh
UCGteZpHHTtm88wU6QzWAxUeCh1wIicd9WXcRX6pv+mFVCk6qAtA2JMrnGf+3oy768iMXFIwaX9Y
m3o8FQ/Q7oYWhCAMUCIGg8b1hMD9uoSuojU0O5hvG7bZRy8pd2/C80dLXpFGzXCsLhvFnyo1EXLg
iAMArddWxxwK3O28cks3uHWJCxxC+e/1wZaMNfbAswbNwsyYh81xTv1vgyEcqiIBKY7DDHmkjqR/
yaQW8rlaB4deOoSV7/8q34bEkVUhs/ghMx3HVmkNcD4j+/UMjHRnrKwdwgZNb6MAQ+ouRy2pucrt
CgX9L13TDw1G6dt/Y4VPIIHv+uc95+DF/lrOaZIenvhuvwMY3L8qrgOL5rbT8L3Bp2cGzqg0F+Ba
MqwWM8jvL+2jBRtz8265cXN60OuGTbbreOiu5A7ZLYOTKgx4ojn5a4ULcre0A3sEJu+FMZ6DwAkt
IidUXESdR1r0v+Poyp7iDTpaxcJ21zREcWgrZMjxP2J/Ws1Pw8dcFkOGuft5UFmIZ7eGVKUx1u70
IN/aUtXAQLGFSiwkRodkyZ7HawVl75chqaPyxHOPAgTkk/whdJAI0yoQrYAe/bnLICnjq1+t1qTs
BmXMdcUeV4fdT157JyADHm3F5GkixCemuMxG4eXwzqzj4y9w1pUEFpZbl012jytrklQ0xqKgSzyF
NoID7+4FUaseFNKC6DlNP7IyOcLMTO7Dfbwew3LhBF2R1cFz50xdKES8kvpuDnLtxhNo3bIki4pi
mvhGG8SiNcMSJjy4pGshfPhVMM4A+4n/zIV31wey3R0EB6u6SLxuQxL8R14/HSI8Xf6epsPAHoBA
El7Q/2xEz129AozzSu3q59JxsPw/SZwrRrsz/b7BLc50eJsSauZHVFCxNCNh4hKzJe4m/IAZFDsM
vdymp5ljESQ0/RYWxBXoYwEP0bqG3dJCHS/xwsfu7qrC3f27fzn4iqTlfD0HQ96zL4scnRBwaQyK
v4Te1hBZRPfsIOnFSNLPdHapOZ+R2lL3Jgp1xIuYcC1hwljBoSDBpFoz27slrPdRXgiTr/gayU4y
6+v/z3G/GG1GELYSFRIjMf7wjw2TVi+7p/9AtrcPFgnRNwRafCN/4/Tek3fNMVIiayKBN4+ChPrW
nK4ISq1g7mmkpuyYJKl9RRsGbgv3LvjaqKDRChVxfUW80J2oE9sP2YYPUCo6qGq7JQGCBydC7ko2
Ug3FxkpMXfpJzz0YDSKGsWC7N6iL6zOdRkyjSLq4ahiaPPN5PUt+UfVWgiwb7N3qOGaa419SBLj0
So5UoT0YETel3uJGC31ePx4FgSeiqpxfCsotNY6ngYm8gPWsy/dPivXhQkQh7SzKV9WxQgGQ5miV
Lr1f3RYXOpJ8l+X2/q7kb0JEmFKCuJia6NTSOhuvWn+JD2kylXUzPwdkzSY2bk8aP04JjbOwoKki
Adspg5S0xn2Z0BCACCCosmh2FP5ATFNCaxhH/3pFn0UmYzIKfnpNkkLk9JRaK70XaGX+j8Um6Gwc
Ept+ACuQnGlk0+IY8Do/pRWZWMGPWi4CFJsX1JPprsOv5r2WvRIxYjriFvlhb31l0vMAVi6qK0fG
0V7aMenjxqx2RPtNujn+BzIlVjcUorAdzyyQXp1orMouFRRrDHxCSYvDD58IdRn7tQ2yPS8VdV5k
Y+S6gvgPriU/UpwJVXUDGUccH8uVpnSrAyXleaC5hlfMfD6XLefsZ/Mtk2Uq9ZOhbLLej/vSoP4P
npUAM++UkTD1d+sp9DmdqBRhyb/nmZa6iTpo+qkow5RzFbHmEbqPg4JMxtjEMzSs0woFmsoMpChC
QbNJgHqhRTqNotp+kcslXj8MJA0lNkQgKv72MbwWTDM13eMXoDCUcubOAluiXZOlo92tTBiRdru3
wS5i284kpG0KAKvSUL/1G1y1XGkjdSj9okXDk6ikgq25X8X0Ok2JOaMg/YdluwJSsTf51J1j0SQl
cSEaFyY5iuqoIqs4vNrejpqzLS7MbwjiCWgd1ZHo8+pRhtoQPNc2PdJZkevNxeXC/JPA+YLrBHnq
zZXShhxW61sH5zTlEdTN+zrO9KibhmDWqdIR/dXnar411tpVRn3HVQw7kb1CCczDXcuvNypegTYn
LWJwqqhGJppnEAZSwPQRUTedi1vlE/d9K5e3MF93LqRA90Q/6cjCCjd+pz+9I4Z6Sbw+L0+ftYRy
pIhpuNcVYOCsBXK0nKnBCesMLKaESp+b2Vo8hwYwhV/KkkyffWYJYz7K+O0JXe2W/3RQrf3ww7JG
abAioEGZd1qtmLWgTMwBMZ4915Taw0j951c87xy0T4x0AVP529uuGj/ezQhzCJ/cQ+Y9B7NU1Ohl
aiV0iwJKos4g0K73QsXtrmiwl+HlkKqPl9zl8MjVzW/ETb+ZeFyg5uzhFjMa/buaa70FNCtaJ55o
HXDrGWiRgSMb7ASgT8uh2k3CrITmYQUqU/tK8syDCNSyFQX+3j3x8E/UfPJaOHtZhlXzDKpQwYa2
pnEy/MsPlvkf7wXV+PqfJ9FW13e1ljfkxihx8wxiXEYBRalo7FkIJlXFjVcyrrZpsadU0JBaR7u4
tumdRQRzQHYnCjCOGVpdHCiYs4C8ONQJa8wWg4lHCJDUszTr/FYtkcawZWua0EC0EyqEsF9q6vdH
UD/r1AOK0GmB+tl+HhyjQTs/xVH9SICH5X2LczrpCNdJdCHtu9sz82xWvF2JipyrCYtMYxTsvMKd
YWrtjER/pC1DQElreJ96bltkERG1391WCGNBLWMEs+u3PrFvKDBGlZAssxexX4rFWwHZmAX1odwd
aGA/4/VH+rKxXnPz1rTiBH8gqc5WkJB9bSvx0sJVxBgK2+cbumEJmD17L/aWTj12gmi8j/lhi+HC
yAPJp0N9HCYsaRH7SdB66MS6P6X9i+Shibd2lbUNObLQ8wFJbwbWpgeiiHpfxqjm6GZ4k4ktRwOs
TIACvX2TgydMAYkTWjkouYFPJbjcTXW6sF3gxP2q23d7DzdebE4Fz+pObXjRIE8FZSbLo8YSwyZp
LvMwF1f8pd6uAEqJfrYTg2mG7xKsXaBKaRc5Ncq7J/0d8+wmqidGqi3ZYfnjaPGmPF9D+j820Trm
SVX3UPBdA30uwXCUxOY1HwNsCvDvGUnFGhejAflFd2b4V+CCuKFCapHLjb348vvOdNbG1cd9F/zZ
D3kJ6ciig8M1FTVOqsix0fOSb9fGEN63iGgv+AEYE0lPYwR85rSX1+erDPejjV2a7+OMafJTXfLI
QZrPmWo1wETXoE0jg6FvVlXq5EUGhEN6R1fSewslBNm203SdAGS1dfPBYseH63Hp4MHeFP5pOppe
VHzyk54aZQ60b8STFEgY8a8wL+E7SBrWz95oiNTl9qmOQYmZ3MX09ihPAvHUsiT7Y3ybMGcwAQ/D
mI84CgnX2tBU1EFI8SjYDIzudlaw8GI/hOY9lA4w7WO4X2YdZZBzvhJHatoowp1MkGS83jU6lKup
PiVkUwHFKOj1gCSReuwwBPCo5SCL8PEBSRApxhVrDaRhCIjk18GfWlzs1UvN78gKPzUfej4pe4z0
0pNLigpyIAT8ALtlokG/lJx0pAB+OjsSg211IJyogDM03RtnLgsQc8f7f+XH5hggbQC/VSC9nzzY
bW/87A9YfNIADyrsoD8Y55Rf9LvWqYaDWJ68G61/KdRXhXdks2NeLlCGP71AKHAzuYVwMwvUw/yy
e8MJNAvOaUbx9GwLdn5PHmG9FKP0k0yN5HTiEZiXtfOT3lxpw7YjN8cXOplLnaLuSeL5iz1YVyuu
/j6vj+wMJok8ZUbb0l73/AqSyUzzF6ZjjkccwfgaubOqcar93sefZH8GZ37NC50icdJQ2vx3ZhkR
mXOuFZxDxn+4aBE2Yfugtj53+a+2YgvveIgicp3jiu+S+Jn0z3FmMJwHyhlaIhcNr6UNYTDc5CbT
rUi61t6stbvJO6VcxS+6EzlQypBKr4HjMyaBUlfPrz49ECXtVupqr4xJpkTUDMQ2ETQm9vojQY/y
ueyaG++ueVNWe8C+5xOBEmZJXFrvDWEaKM8qHCsf01nncyhpy/IYLLoxTMJAbwcUb+I7uzv9MO0w
ho8gTdbTUh1OmqDqgEoKuCH/gOWMRiQY2rOEB1Qc8a7MC0xIgMj0RGze4xttn0HGxe2s08j7ioVu
1az0RUKelVIWF5vs5HhQEyxXZgqekPkZ+0wV23l9X+WLpTWTrAiYiThDpQMD+SK9udLyx9hG+2YJ
4iA/xyP8BiX+OAedsw3Dh+Ndz+ykv4GMf7tuW5SF3KFoIdS2TiQrixyyV6eMPufuQHg80pYshLVA
i5hmxm94x1cvJaVzf47pVpb+DbZ4+3EkSrYp7qPthFvyvxBGjudlMpYJCaa3oaN4E9mSHoPKzeWz
zarSeQfX0VBqKlw+JPr1f0WNiYmnzEWbzKM696zTFsxe/YhaFy93co61LGFEvXN3vpl4HT3AdjWH
X1FX3nAhqsdiwEC7LEyAIgBMsxsUZ/Ze6iljpgcF/GAryMC/Uq61xz4Fma8YULFi/lydlTOj5EKF
8fod8z7f5+JtNxMtjNENY6jrdEleXU1zwFQhq8i0e5R0aFIKf6fNYbrz0uozhEHyPZpO2hcPsQMG
wYzBB2LglrlLtlx2nhZ9UbxXcroOmdSN5XsG7IwUcr3M8icrF8l+/S8gtN+QUAsceXJItnNaGvpD
Q/gC/UvnFS8LROD8I5sD2JAuG2ZzvZARC7lnmOVT1DLJVBC+TS2aF3eu+HM6LcUOQ5CS2DKlCSRr
hb4i3A1Uoe0q7iEY73MvSi8L/2uOki/7wOxSWJPJS1G0Ig3pZ0n+4wB4WGYC2v7iwOSDcr6wnNHm
bgYOq6wikYrEIfQnlWUvP5Hq0v4/jQMDIarSgVFJo7hzTIyESRGq9ThOdOuOIh1fu6aSA/FIbmMD
mhMf/UVaPhXpErClSbfOSaN39mS3I8iiAra79B/g6KEKa++wlMEq1E3MLPAFqpMQRZw15/Ro3M2w
JGGC0xp35aFEx2t0icStdE+KzitR9Jb1UsiMciWeMXCGzee70UiPiz2kr6zlM+mGW1MGgbIyOfjy
gR/UWURh+VGYc7SSDhPOVc8U0nop6+NELlMkMzW+33OXuk9bnVOG9lIOSSZJO/PYXNoTqTujY0Yg
VZlyXFeI7WFsV4Cb0VEXP0l61zG8+8ahMHNzRTTyg+u7eK4hvLcuwmQkGp3hO5kwnhuhHW+IuI5Q
zhv5+SRUbw6r/UT56p10l8k0gmR42GtJZRre7nrAO4SVNU0Oileu8DHEvjE6JcepWSAKB9VzZlTv
xll9kNpD30MUQvtG7f9NU5sb9z4ZVS67RqhE1slNATcM7LcJdHiZFNzBF5ousCe238RNsH8mYVHO
60g2VKfAN91F1tZYsIGiTBtm+MfO2z4cFWRqfFhqyn/CNK6KQSyFNRIutXSi++oVd7T2bjYmcN6I
7bKda51VjWbrpim0ZZ413MdxyDnGt0TdpA+Elw/vjpeOEcgI0mFEXbx0w1QGXimuc1H1EjG7YL0M
qCgf/ZSiCDcknjZDhwpYcrEqBHv7/SYmarOFksrMdpP6R9GTOt/adBQov1YvvgHoWDLK1D1j6PCB
Ks/Ga/2XE2GY53e6GmHSJV2ytecSobngankjbZRQjHBcFksbQkFV6HCVSO51tNlqvUUOxu2yQdP+
npMw56TdTv4zJQdz4IbZm3rbEfFunoawjlmZFibmiDM6rOr/ThHLtvZfN91tmx/gjrSzW19aVY3z
oQTd2G7WdLJD06QSY0Q3816mxR9B78N2rw8pmIfpdzg36At2BIO5AptlgfkUAZbT4Mq0w21NPs3t
r80wSCUUHowImd2bJSK5Tg1iIFqKlvrzpAn25jkzj2kZx5RDsPDEwo1xwRqJyZS6Bpzl16493gIA
XQKBTejkfk13gwgVEvXATq9UYkunW9vDJif+dNkp++hHWlfKRlNaZ0qIsHJL/yH0MDCCFwryABUK
x4B45OtVCUduf7vlAm7qcswXwApcQtOAKGoAE+p3W+YKcNItI3cXXDnoFOmDT4oXzJP7zsGFbBkr
bf3CU3ox2E2145msVCMVVIxmhmKhRNXVFMMO6IpyvWsPfv2NxQX15tdu8yxY+PwazI+8wTp75gdB
f3ydVMEQUm8FPlIbNDNgd73hZjg0TJj7Y5Tn5dEY8gyNCWjZ+BBkyLnZXKFuPrGtuV4fXj9zGucA
mOho+eiJ745Hw3PVzXVBeOXUceQhSUD8S7/Sc7XK9Xm52O/YMiqWg0su/Huw+47vRnqxlyFSjLAe
2L/zpP+bMCGLdtuxYQgRslxLIpXIMrVDwqN2TBZLmGfMPGKRIm7OPMhKpO539KFp30qttgZrj9jk
JdiY/rbFZhknSiz7i/7PIBzxvTYE8AgTgEnICC13oqbPn2ytMctLFwiyP/GcQBwXxvyW6mltiEBh
hOzv4Twrnu9nUoX3zk4ulMuT3m6/ddDP+M7aC30O1Lc1Up6LmJrrP1D6gF8mvuhOYukNVRadfGve
g2GmmaQB077urTU8RTajV5loIWnKxCrM4rc45MRMePvo5jTrc9obPhIxBtLCJKvbPryJqtgejd2N
rt2QSciafmmpGEHD+aVIsRNW0Y0mj1pnfyImZpkHsEExlpxzc+n34tev4zAEw2AfpxnAy8WQFnkf
F/r+uOlid5UkdGMsWRGIsKFM2bjNjJkafSbWwuqRfm6C4DYy1XmL4qLQJ/MmaPP5ZgCNx8Ah9Hk/
oGy2fLJE2wa54W09Zj9ShviozsjieQL9B8Sr7dDr2JK0tajkXpgiKU6x3i2CFMfGuLk2k5iE5piw
JBJeI1slB/4FyVmzNUQvD+yds+z1RYHhTf9JzbgGZhy6XbwUbUR6uJFnhJrA9SQQXRa+20CtTW0/
8fcZhRvNuxDSp82nrelIMC1wv771Yaf1evxuA7N2xRXAZPMUsxd1Ck3M+sUIXSiRDKpM1XI7b4tP
hpKij5Tbs0c0ZaftY3i4nTCj2YPFYhLHn9QRtiJiWeNkqFZ8alsg0jOE57hg5NHGge+PSzFMYwnK
+q2hScozvfpnnN5grsq0URjJaQJBpuJ5U117KDv3OwIKyP1iksizwqIAp3IR+VzpXiQlKgz9qhgy
jwX6j+tVh4CFwhJK04MUxHr6YNneNQLgS/m7oSBXk+/nKHFGzztHps/hduW0rzkTOe9B5i8zZWZa
bBMdBTmh4BhV/surOWmX9izXbHdYZvQw/JsulApT6ClOZ8MQu6VCgqnZJ82jf3RuOfOkgzr0i5ru
/qbVysHbBIhp/PoqEPLwwwFpeRXHCR4U/dLzEBSKHVBjjb7jI3OAs97mqI88oSMFvyjisD5lCpt2
08HG0NwtQoA/xOJw5wJq+phgz2KPYUdLpSsH3S2/ehmqPTmDyuIAdidJMfiMXPq5LmlrmdXJEnYy
/dQABrgO3u1VHihCy09nLoHIqZGBwkCn2JvL6WCNTbTSpQi5NNzvW9yk7s7b5ZjVyPeFeNThpHnn
Oxqb9IodfHAX0+lMACF3rrETirGu8J0G7uOL6yYMdr3tZUChb4QYFKjgihjwRQ5w7NsTIPYKphT1
tZXAzXsFNZlpSTIYTlkD1EY729EwKQe49+pnQdm945XP56IRf4fcKoStHjXhMzHe7oNuSTc3Ra52
uJyrQCZLZtewISIY/tUsga+tU0aq76GxuA+XnXl5o4o8WiLjhdm0aF2ElcRvJZDGXSB4sWfYTUfa
laqkIHGsCcQh6XxVhPMs3C1CD9FWVGE0d28qklHFaGZHOIDAr0Aja8nkZl+uSOMSLwGh8isBwaDM
95Ym3uldEr7BhWaEO/iEoLrZQTIAsMo5RiOje2ErDwGMdEDxEOfS+vZ3KR9nBtve/cO1yCcHYFpC
vm7qHwrLEF6x4QX5A/VOSj+ISVstFMhLptIBW0Lu/MfCCEh7IsLWwGQzJfpX0hfcTzJoyEkXZezq
6iSkOA9P0mwW3wZ9FLhEap6n91J7QrrmWrO3t03Lg0bXCAPGXAYKOnm0B0YW6aem4zsiuXiu2Cbv
y0M2X0iSSqLTltm3Z5qO+ylUwk9klzuRpTDwcZ2e81TXT8kRdAICfNrhl/tbZu4ksMpA4vH6p83i
VlAZWCzYtRHotjYaYJvkO7+wAU35xTXATQ+wPKteNLE3a01ZaFrTVRxul3AbZQrmwM3/eirj3Ys5
dN6s7MgIh5ezVVb9rwHQIJAlXyZH+C0HirA2nrHxaqVtjqRbZ23a2kPcEGsvfyxGOCFpTnZBiCB0
Skk3faEAaK0wuoH0JXuV3ZnBB7aXRIyNtKvIf8Tk3pNqcekuWFmFXQrdfoTvMG0OG/RaqUNXj/IT
FQY1Jx6uXi4pMIQo18q6PgtGzAVqVbaP54BhwbwekvPdr4gqpW0sZhieXA4dFGCX049QR9t8pOxI
4N9wCQT/nWus60lDrUzcmWG38RdmAbEkOvT5bHkp+P09fHWhCLA76WbcePkqj+xZAf55a05RCa3F
A7YWLyONu6TSPWN6s7LuEvOq5vBQ38kGEzpNqNSxS656D4IgKVb5fKC1lKoo3DLX+UQFUM9rYdb2
ZSJM9nrpCzEtVHLSmA7vyoctIxyvSCl/h7VryY/OHkEup20w1hI3Pv18qfo9jci3Ba2dQ24XVPKr
gVQGPPYvCKeV214wRHQzVW8SDQ1IZ02Qx3x1VktsM65dLy0K7AzKVYWwoT7nwo4vPDqcMsvU1B2C
fui15Yw3aE0bvYTXOHV1jnMIn/9QUfSTan3FbBAU2vztARRMy8mGul4oRmvOhnjbqY+gy++MHTZ5
nBt1WQ5OsY1riIiCgYDhiEXdbMJq1OH3QVGyC0PcU0H9ZqRrTG72WqqEWnuKb0/dp9r9NUG28wc8
RXvtW+DyzfR5Yg7amGbhAA5r9tovvR/Ss8u8jIfHrfCLcH+PW3sGAGFw5esuem2sqh5FgGQZ+l16
N/1OvUSmeA6TmSQGlkusaAMbFJauyjg4Docca76aY3q25ygxzvGd3PzmE2g93iwhk5WVGhLNslmR
A+Gk8rMin7nO2IBgq34ZSLNCpzVhH0uluOWDnOjXbW3sKSeTHD//MH8PHZFbctY5OyCJcXY46f8B
6c4ROS/H7zxYP42mEpB4coRtBgxsWPD+3GMHeQTuGmVcgfpexWX2P8rSpG+auQlPy5tAjYvkRBGT
ORo3ZrDwSG+D1Bqd45p1oUgX+rfi6XNgfuL9a/wF7XU0fRxTX3j5OznzblGDoBd3v9dXuk28cN+0
qUgFhANK7TI+zosepvkWWg19gqXoB+QAV9nZIW7KCMpMHYnyMlwGZqxioAMDNL/sqYI/lfawQmQf
Lgz1My0RD6DpJA85A/FqbsuznYQ2v2waZErhFfi+cVSq8wqjV6yCUkHxCw/w37mBvofYAuBl+fG5
WIPBogGeidgvHDo5BuROFwFLsURF3ViIZ3aGfDLTPPBs0mIcDRl/f87g0fYqhuWODT+dgnQWdm8R
u/uyl1UiRU3jx+htD5anTDvUl7yCOKWd+/aHQFDBY2Hdh85IcA+/sby/DfvmYpWOL6zICvctDARq
DUKLFApc7Y/ZAhEhxbA06pb0h5JmYOWSJyCjh5B3FmPpfiAXLlhWmEdXuWrxQZ6vnK7vckjRUKTa
dvOpWYiwgploN1eFjHDSpQUD+4/HLadTv9FA+iFoR5wktFLPnkEFtIozQwgvNL5UHK8StP2bhc6Q
skNAsDftETDhQQIJ9GSV2cYU2SKWzxt6leYGi/LeU9D2JhxqEn1+wfv+dD75tiCJjnK3sM2n7YzE
sJJu+eqhxutsxYskvJNSQXFLU07cdsOfnQGD2JqSFkurJ9ig7v4yicDxcR2xPCnmuOqpAy0O8k3M
iYA+8Mjh7KJ6W2PSEPgnMAPhMwqukuI1k30sG5M+u9mYV6fIdVTFEOpIEP8DS6b9dizBcUrDjmVh
SbT2DtpIeIOXqdDT91RBEIPpgYtiHWAcNionm9EEsTKQa+Dh83VM1QRD6PpfOi+Tr1f16fXYjbmT
b2F+gvCsmujG01BZbyQ/61oIQ9vjHygNVjQdZjmtklfr+3X/NCuSQKdNGno/qJpxby8xxOrfuQ8a
wSBu5J4oOWwDcHt6J32Yy4Pvr9JWObEHoO2kIfVmIiG7ub0Hc7PRnWk9BfrDCcJTrE+dWnJytLf+
bxxhdg8mNU1OUhVdJ/U74OmHkQMDc4vtVyThR2AI4QkQ+ipzNGxPQTz3ccUEX/YYSVWrr6yqvOgK
dowpd6FUmRQR1js3YEncX7wHCsWF0u0+YDqRusJ0yFQpLvSwDaFDyKZJKArltO1MASJBRKJN/a8e
OmxanRNy4ubUqFd3BYf2u8d+YiehE8ub2iEOeimg/xN4kElHjVbwJhuibuHSfVNeZrn78Mh+dP+J
zVvZDR5fcqDRQJRDxTPESGAvdonWAjVN8omJWSFVqrgiwf9Lo4qSUPqupCUZ+or75QZa8OQfQKz/
Q00x3dP6TDyOOXTabmji/WXqdXkM3kTMfdAQZc9/j8pxZSUbIbf8L5UMPvGcdF2YKv9ACRrY39YH
SbypRZAQDJ0ywrKMvVSwAJQ1tcoaXYTUUHjzhSC68wp2ToZUwGE5Ex8vT/dL57wD5hNW8C49tbzb
VbL+6ZYWOmh1T6SdcWE5jIvWUn+bx7M50wCkCOZN+pSAuj/Vu0OOBIhYiMjPqtuaacHJOP/TuXD1
Z7VyTW/FrJYa1q38UUDpSz/eVFQDY+ps/OTy5re/W8WUZbCVtNmT86xk3axIv8iIYqaWT8j+knRi
oivqa83SAyPnCLXkcozUn1wKJMHThItpSPfcuo3agNXA66SM261fgK2rlXXMt366llUxs+7ausIn
wN8mbetT20iNFYHne+MpCWk7Ulb/NoYcAUaMO5pFBC2CF1+isGk8EGilpK3GMW+0zMTrOhtRXvlG
UT9xRVmmQZ+ueXO7gGH+gQ0qQXhQ17ryhhOBsEu1WxPSMjSV++Wf0ySY913S0wxWJVr/nIVjvA/5
TjDPcEH6ghu2Uu2bcHKhSz4RjUk4ndvJ+2zDv0QdFbhjePKRrpkOOHLhSEay323XIHcCbOV+NZOk
P9t9vUf0ESExL9q1MnZc6YRJSw49RPvtYM/ZH7bvQS3xxchTqMdBjO2qEm58/icHydSt4ST669E3
cl7+iBI05ovaoMTO19hzzCNuDZqy86JWikM7CAPfBPZmStnCWSZatPBsv7i4KXJaIv8cOR2Ac6HU
yMpZASrvP5HX6I4GXvTPlGDgNRkfxhD7I1Rm8AAxSS9Li0xZut37z9U3c/PRxGOhhqHrWgm8XQiD
GpnSIH2bPWEr9ZABf1iOo7wBwBUKBJm8T2p/zUOQaQmemQHBQxgxd6HUKzE7hq6t2PFacCuwzC4p
PPBnR+ieGU2y3pWoyHQj7Imt9Fhdh3PH/yf7nN116joPKwCTi+TbOivO8k6GNZYlrKC1OqeB/70J
iRaV2zzRjjWnDH96M6AnOq5S2fiw3McuRzt7LalAD4G0+zPtguFIveDL9aTIEFn/cMi7PwYz9nvb
t/1fxWtSL5U2qleOyy2Ts1cZ3og/a/kmtlbCrO/umP0DGRmFhwDLQN646p7oFEqhYy23EQ0mFEdL
nQkru+ZFxdfS24c7BcdrVidHvqjcMQb3wUZXTX6hwlsufyfBXb2iA6Dj3/cn/eQSRwA+CflV1NzL
ipoPZu0+kzcLnVMFYkrDZV4RMIKHcmwy4Ymxztqz/7ifob0W9mhWQ7hVNuo3vCVqRHA1d5EUqeA3
EoozLLdDBx9Nz2PU+wrVITPqL5j1GLdbgzX4cso4UDP+X9s1Pa+S9bfhcA9LyUIi5Thlw4Sr8Cvk
IH47LjIa0Ps2JFMMhsC1uxUIl/96M1S4Bx9/lthfeX09AsmWbjW6dV0C20yq6gwQt3duX7N8f9V1
AGYCw9zjvBVmcd1oc0M8vU73Wmt/qXj1fgmcVEjD6eL9jYzhLPQWx+EhFELOorIeF5Ebws2gViSU
/lpL+GPiCG5LFZULEgoM4xQ5sGdB092aGjWUATUG1FRUjqZhdqT81N7UTG+FMZTd2G6nK7fGDtSM
IIqphl5OzQd9eGlU2Y6tfqNj9LS7Xd0dD0CGFF6Z4xn8rba22CjYKYquQBlSzVfARHeWDYAmhKgc
SyZocwkSdTiBccUSv1i8y/1IiliOTZCcwIQrXCKHY0HWWyNja2Xibrxs2GTGG0Q5wB4LX6tWk0zg
GnL1r+2WhGIMwjiUCPRz4zg07l1tyxCc2lCLuPr5LJ0HCZJJPJnjTT+aDuUf8w+zMxsWIPKlBsQF
V0Z3u017M3ibkaBIiIO91atpLqxFVf1fLj5qMtPyoSEqfN+eWuZiP2Ruu5TeLlbPEXLvTvEzq5bh
/hY8fgIq/0z1oPFKKORRwTFHsM7b4hdX5Oae35b87++G+FnDuaRCmnxoXIxfUtXoIq97obRWVGDC
Et0v5u/kYoDMM1WQSOP5PTRCLF1k6OkzH670jvSEwlQ8QX6yXe2s7I6B/JLmDqNLgrkaLxxD4beH
NeDE1tRH/dwR3mOquQeMEkL2NJj5p8Lc6iZN83/aHdhoMt/IpJzgxwvkurvxuXcrvo/B3N3TnkyY
Q4yv3onGNgMtIQ6PEqU9cJtCO/buqGm80Syvva5JJYEopwht+/5l9m3vD8kHbYfKAIM5R2l74Dd5
Kpv0ywmQwAwACKpgA3KSR7YWD4QLosUnixdnqCJWQuGGsu75yvhxc0FRNhrjzP1W1Ur4g+sVjElK
L5i/5K6Oqmtzj884vAiHZSEbeD3FE6ft+1lPgYq45Uh8sWbZoFTL0bRqU6xTdyKeee0gyB+Ruy+I
Q4DGsXc8zNxr8AV/lWNKsFPe2IgNsXPQ5VrxToHAJdeB8szguW8Xr6QtJjNRH9ogLkoI9PxiGyxR
5qmG0qcIzGiykoL0ZXef+CnolpLqTIiECFnM/LSzy32SzBI2qnfAqq2FerFHRMEXbMZuNOXzWGmz
AfLxb4xIRYSV9ovZKzrPySA9JNB1lzATityyKU91rzjnRflboTyJDu3izG7coia71MyhMvBSW9aV
e/xkP04qy7N0LVNK5002bLlX/J9bGkhptvzS/fNGLnNzzom6ad3HlDbUqjlC3uztJc2D5HSbDp4D
wknke5CBajNSsQ8ec7c0CbuaBMO+Y9PbjFZdzsYH+kykWp2M1tww8iZwNK+XpwJ3dCxQho8tM54P
5BG4lANK3NzkFxGbfhVAUA87iBMvRu4I2DcKMhDmLwJFuAnmYO0Z7P+e7p/2OfDDanU1J0sou5Wx
qt6V5nQjVRSU+wF/zAJPUNNaQExgjpH6rdTRFajGzt3JAgJhzSBPKpg0gFSdpgPb73xQ9ZcQPfqY
EUXkb+HE0jg/EBzq1p8hIHxA58BFDZigsZT2Vdwdnxp0kl533DDCKiw8MzbcN/FvsKqv3JsXGls/
Qo/NdW6BTcUO2YlnC8FMEODTMccvImarIxugZXLDrd2XGFcg8Q/fbGjY9bpQxHBDuDNgS8qpsKyb
bqxZyvXuSLWVJj1BvkiGjcaaXUHz16vjIxHVtWjjIni4JVQHAxUw1fhU7U/J2SSy/Ka5xoWle/NF
OQ/1L8wETaTeDDHrt2edHdN4GNPCXq63O+e0ZZ0jb4dlCIndr8N7YqyBX4BgGZqB1XDuYYqSGwPC
U97e3p2jyHkiuEBO/9VFQ4HYG0pVWzFHYd6Mte+3jOmnE9Z0CulVUT1BxHtqS3kqmsDZBB9KICSb
O5MqgsCZThZgfIqb2uIAbytUjP7NqSjHl81cL7zr6Hqz6J0a7CovOvgcyQ65zAYZ8oxwmmu7xERk
wsfcQaw4gd2mBJ+IUQU/wSacBbLNCBiaq+l8QQ3H4DWvhEli7C4H48z9KAp5scKzhqgJcDG0Zl8/
ftqZ6wMoGALArqozqIRzE7T95FP0fkbnuGe9poTSFrcEhjHLPNAL2dpFggwCwC/IwuFWanbFNZCU
U+zvEMMlrQxicND5dWnXdaarLphAh82yyZiTAlFOQB0rQU5/yrltKarmIBwIlgjITdMkCuNSrgsA
5eTd5zbowMwu52vUK94KwPJQuOgp3B250qlChR4QszfhB6H7QNm5667HybXR66owNTh1P4TwMurr
qzTMNcZJh8ZQM9BwXBQO0rDsPjEmb+BzmPYYgh1PlVDHjEL9Mp4V+mi9PJ2cI9ym/pKwwSWNMeds
CQLma5I0/ltsFMi4vXYzOOGGRuiJa7b/17+nXp0b4iYJEXCw1tFWp4qsvh+CyQ6IvWJw+a4+K/D/
dSZwsccOeDxYrO9WyjjejChjKFMdTlasgcEQ2gA1PB6mxlfziIaXKH2RhgoCZ3tJ0gaGpXALdQmo
WVTuEWsvBKEiD3d2gb8Jb0peD8v8TF+MmpSVkJ4OUbM+ooXGslj5lUZcehayiEpFu0eCJAuvAFNg
TB3xv73Umr7hThYd9tqBjksMh9a2XXNzF+XB2aIBpP88nV7llAy2nbwvKfeL0QkDT3+J3ajHcdpv
Wr4rzCPrHxEFlti0srGg6Pqmaqa/6eQqc79ykqI9xzPtVcYxuLPOVCgilmimZSWcFlcKlqsX5ijJ
b8PdqnGE0tbSenkyRACe1+zfTNvM3RDQNTgV5E5alo/UaXHztYgzxHvyDatsjF21joCKeDZ8Br9j
/zYzECukrX39Z3aMTQVLs+V+lJXG/fN6MYAlZzS+dGlNSrtYCcAtHMXiglKhnifLtZC0ybLUdHdG
/ohJHiuBQcpSqp7JiSzsUPI6SFzjiKz0IhYSsmg+f9ttJQ0LTcIa3LJbjfhfIAgQBfBH177TgrF2
qcQ9/ANFBlN3XG75SpzANB8PnAiD0dAZ4a3dN9ds8lDfVXIv35bjhwj46qhaOsCftl344x+uocFT
6GGKyv1FCZqxRy1BPuLYfJkfp5HoMq9rb30I8xKs2ZOyXnm5ASSXK7RF9wb2X1I6m33LQHUQOw+6
GBE353CtyuCuSauUa43U4VBMUFBZxY0KrX5dbFKQbFP4r8GcNzJ3WxACR/55SVRBpDPjL8INdlI4
91CXDnV63RuATXQ53V9AcWM7G6l08OQyyK6tArBONcI8FNVwtPBO6iNySG9AC7/uQKvgDZJHFhCQ
B1j5YDSjWM2zBSVlbeteMXlMurDyGMoiEbWHye//qZsOyo6K76YPsDhVQnPd5LjgqfujSkcmxSy/
pUEUe0orW5UduM1GSDWHkgHg7+G9FmRc1xhMXD4a8Z5B6G+cbNJly77pAuGzXcMIDp52m8r0I5Oo
O2f/ovj5yE+dZmY3LfC/WsGm/KLt4ksY5Jt78F2w0xHUbyzlKk/kafMRdpat9tU/nX1FPa1QANmf
gZHmy0eINsBbCcDhXpScuMOFjQuoGZ5nIAS+GmrwT9a2C/LjZ+VpUVFfUGEurtXo1c/6frW7zrTl
6sHvX89mpV5pGDwItRqfhM5dr7dplDyTOHEp+BGUUWR8M7wSCE8QVUkQ/hBeG1RfXbDlhSZctzVJ
mk7Vka1kf1AaHy5+kQdtjauayuZEM2BM2ukbEfb10oLERiJv2mwzMOYve3iqdPoVW9341wHAYkkM
xEreYh9ROcAVxoNcsvsI8slGO/a4xLQZM6vMd8qpCPlRXDtVsoHsaPPk3CAyrywnlSxwL5locyri
BN7i8UrESr6fT0x2YTtMD2VNrU4iZsafAJqtrKvL+dERIWOUE/9Ztoxl5yx0cR0VTzolmS/GcUA5
zEO7v9TCA2odX8zR+vtczwaFRd4fS/atp5UTFl5iAg5TzZhreS/uQXUbFF++8XKhhPuiQ87PaFON
Wn2Jtc/aNAWmTYjdIGEzcyh8H6id7jvPhIcyYl8hHd31G0JkmIDinmXmy3vlfH0NaUMYdFLRL3ty
+bxE6KXyJ9IeAZnPsD/xS14bdKBKV78Evj+EnMiJcTlfMYKRt9ZLQPn3vHfO6EXT3TTiLGjFI/MA
K5J7uS2vdZrDwrgSgwzcLZVCGIjRIgZbD+Zc7KbkdlOWzDkXuFqRPBZ0J909vVPrV5Wp5858Hs4V
TD/WKiJnn1dGaxcS4op9XFRugeeyyv5ATNAktlWmJ11GepYpGHxtK0ydcGhzzL+/r8viFJPbujRz
nKn8vrd5sLS3RLDLamgGkvbhzu7EF6Ob4Mu9YNNW0PHSM3X5FnHM2NbrNfCwgQsb8dz7qRQa43jd
5MXd2F4yBg1ObCWettVALYU+JacLpHtL4J5eU7ks8HNrFeQ9C4XhsDm3nXrH8OtIajHCK17/14lg
Fy00k/L22rfdW2yj/xriZZdzMw+izOPQe49mtLZeqR/hxcTcJPUq37QEUwLaBi0khWVK1bxO4V3i
csxEu6iuTuLlA2gyUoaunRymuvwdJE3yTpQqk2qvzvDqyIhuaAe1CSCkovuJop9hjnwKnXQHzaoQ
qCTBC3CHL+g5M4/wo+r4HZuRf26NjoDVPSOAeJ6yl+6Uk0uVRVXoDbsX+x0rRrs2pAzzLbzEcN4X
sarYIGM+Jp0KgmfzkpCR2z0Qaplaj3mWFfUJEi+qhv/+ZwuawPZ6LNtoSS4IdZ8TVOHxALACf2gQ
eg4wxOxy511+arR+eNm3os0VqAXhRZv42D5sCEB8V4TxE43PpaPXBtV0d1ATjG784a5g1I7J5ZLr
suO0c+UcuL4aHa7Ljzzp9HpD8eR8tbN2Q43znwn/ixH+936gnTTETdyQTw4vZwlj9CrtGvk8KZ4r
oBSgHVCc7aHGfTaIsvsq4gXgXIqer3Qvu/P8RINHSrxKkLki9Ii2OhM1qSBhc10KrnQ6ofelTrCf
II9SMBQdu75x5n2LWM6cvJ0JTATwt5M2UXhDAaHKPI3eaUQWcpPu66kOF2OZfOgtcgw2KA3BLlbn
sBZVwj6eKHR9Gh6wxKW41vBj4yUquwE4R3pZ56AWQSiTUR0kzUGxeqCHSQgmeIoyl869JW2H/FWB
uhZ93c2MzfaZBC8ltzKScULMEMqjmVpPKA8kKevpaFO3GrI4m+XRIqXN0IlJvijWl9hFgoWXPanW
ZsB9onPUzNMD+9Wpidy/DOjahPjXQel4AeElyGT6CvgSQ+m/10OfFy5DJVPZMD91a9NuDRfanjKD
AxhipFkNjIORzS8ueqwD+5/af/kHRCXAUAedvvbj3az+1msb76QOd7qGZAImbk212YbT9WtVzF7p
aBknPHm4sPCRHhGZaWfPSvWUwqpDBqORGBaCXXXh1SSfMYv6ulpO/0Jxck6hMvfr4e+daXrrCMiQ
7ljB/ZL5ijbikFlHYrY9kttkvEGBt33kRhD1x430qJmV0TjT3aFcJO3L+ucp8IKEK++vxSTH8YmP
3zcT7Zd/0eNEf3Z2oY9H0/ebTIWZQEsrnUf5O2IpGQSq82RsfmcvM2Rwvyxo+TQWxVvMLuNFq5vw
MiGUQIMt7mKtecznWXf6UpsGJrsRrxyUPkXrc1ZBUOjJVlsfqhBLZoCegnULd5YRn40CvrXP0KR7
POy+uFDZfLzGgcAjTtL6tDRbf9yo3v1SPjHD7VLyvrI6Vk3T5jKb4jlfG5SDx6tJTEjAFVXN/HNH
IWQj6hI1ayJt+JVARsyrRtF1A8St9hpehpg80xu98zENLxdPtDoq9Tzy6PkR7Yd5A4vk8jfDIde+
RLYF/MDMg0qKz1nhbqj47aGdUy+4TwGMace0+RElb81CQI/QX/81Q4ZztZ4bTqx3lkGfUFHWF8pZ
Rg7Pr92Dq8ZRoZ40EyShC0nTdkvv3pyQ0O5EKAPEfG/eI1AIGzGonMTaccQxVD373X+87glg0NqM
Jyb7WtgWUq9+rxk9saLPKjNVixRZrrxJe65jr0+vWRzqZjbCScYFwzaLrMX3rkpeq8bhuAC/aio0
6umzpYFJUBpXPFgImOjV1SuwEkpUEuX1BR6hm5ADcGLCVlSqvDxSkdR4Cml631a8yZWozxDqevnB
PSiYzhfXFBs2z7jv4k8PdXBJnnJuT8Fa0sG4v6fwbKHkV79QKBhQeH1a/M5XOt+P1OIXX5ifQbeo
AGQgvS2zozQpiYFe8ija5/saM4VrKB5jo+kYMEoTICar24LqcJlocyZ61Mvfdixwd48DvNxUnheK
LJB0rUFjq9oiWbn25zBu0X9Vp3Zog2wHob1+7mKIXzsoGJJsqSJKEjHly+jjoDdmpAKHJCo+97oH
JdD1sxKaAnJ+/tprEokt5UW/d+fFjWpI4OGPzg1k1fWKxHzh0M+bVHQudjfk73+2oiAd2GtdFy5O
3BFT5Z2GFCOE8wQ39XUzMKUkjt0xXpJZMBRqDmD0kwFKeFEshVpRbxDNXrZmm3WHYHhYq0sIHkM2
zlhEkjRL21kK01S+vhjJLTlh+yisF56H8RODrYHjS1eKy115ZVlsWZBNq16S5dIazYQHuLOXMKYc
ZRy2jJIanGyXnb7BsZ2SAQlo5/sIWfhKGtmAqmWxh2Onmk45+NF0oy5vHEa7TJk0Sq4somucFui3
yVRc1br7BLXX4spQpz+LP9C514Jor2qKY5wuuvwurqeq1ynH+6NanyFQvWimRK9oKc1ZPM85EOoM
kLc9uO6lfBjndxQYSKdq/4MWCpZAskAF5WnvgQmjDP1KKcKEKdJWISkXmOFoiA35bGf8+aukoc7Z
h4WkAWO+P4bjZiXm2pvPZDfk+N5NSJ1YOTX0DYJrXaZImmSJ7NV9QvCblR9fndCsJPn9PuNufyVa
8zWGA/x7Q3j6Y/EqMKbv2S4vX6iplY51ZSGm/jlr3p3dJzMG3aqYcUgk9NrHKFNCdO+FjWiZMOL3
7yS6dM4nRXhxN3Qe32W1o5VVz6G2hDqI4rnN0zNGjjM7/sEp2bPbQ6+xdLVbz4ffEEdpf3Tjhdah
51KFOF+LyFyvL97iaZqmkF6N+WJ82OKZBhZcX2YDX5kbl++LulI7Am4LNDT+vdLEqHVpi7yWwfF8
uMlwZUYriM5WK2nuX9x4wjulziGBg1eA79lf/nOM5QzwiSD7qnBl2qbLC3i1EcUQ8Focs/ln/YuN
M/wSSpOPg3ZfnSg2pEgkgJ6lyebQ7A55NV8zMZoKSc1FhWkYoj9BaMZqsB66S7I9UpfAVd7N9zen
3iTC6L+Y/rwISJ20+agFdwrxK/pEQZO6Sil3u2F3Bd8xs3V0vNBKeKeXGNfuridA5FF7YbqDNXwE
ozPgN9+ZCt8HJv+vju/nB/NUHKHhO1t8SYm4CPj7A3/xx762hQA2s1Q5J1w8yKbyTBWB7LU3c6kK
5qYaENTFmq0K6yNvD317bRf4FuuaTUbZ+A+9L8MKihCpOyClrUJVEQulOW6G5MfGxGjflAuiDFRr
aLkZhKrv2tTSRKscr0nsgi+HiDZpZIFVYA2NQp9xkvwsyBgZLONT+VVTxdIB4c8adNmc3Afq9Nby
LAZkC/imRNX4Y+EL+4C/PuPt5rzasWHSkt9UNRrk25QFViJV4QFMpxYz7f7eh3T1rjvSgP+pdfJ8
oGyChPEoQiWJ1Ld8rjv4bqhA+DWGSJdz5BXGH3mq+SoDSKA+IgbSo7sIR7K5Vh2iLapeCvoMUdy4
zPhjO0x6rnVxfcmdT3ghigvNp3yHut36QTpRfprD0JdDiiFtxZMiYoztEbEKYik7522ZR5Gt0Gem
9dTSJcf0x/GkR9E030RlnRX1oBcWcItJC1qPhdx87my3cUVFEkd/Ewjm7KW+UKtcM9+QK4FlOAql
EfyT4Y9AL1LllBm/FSseLMRBMBBiTYrzrT140oo4s3DGc3EsNqoEcmlBbUXi8jF2wN+6iW+k7RBB
5KadlK+1KRZ7cYN87dhcfvjja2R0mFRRFrMiFTR/DuAnEX0WTk6+vRcnVTOtV8ugUE+DeKoxbzmn
HgiOfd2ri0zBpF0GwICqxE7CSLlAIlMVPVdOhN3/j7yi+B45WL9TVoMfCb9Nw9VW0urt6CWeQ5bF
WZIvDV/X2gASKBK8svTntxk87xLC4fU05fvtylnD9bLN3QaUKZG5nJxBgEe+iHYaEkHiHtoShsjN
OMVL+AybZry3WlTzBWGS+Bl2dDF5LKFsXiirWz2J1tceLBe6BnI1w9in59CERo8dvz/A6TLhakiu
vtTNndCU8FD3FjROZN+qui1JKqiV69REkxOCeC6ZZoNwbXYB1z6xbvm31AQopHGEH7VdvCNSlWS/
rAXfwiNqxfrMJpBuefXmCcuSlIFWjmcRO6nfNs4PWK3eSB9rSPNOevzugl/FddW4A2Xi8ttlQmhP
/hrrhC+7y3rDNIizHS5uHGQHdDtzooRQzj3o/UjbKo8kbdk/CO1iuf2siBGxRocXJnR3yGAKNRas
NLvc8tKM+GOV76EogZ60iuOAKbXrKcP0clk0qQDlHshJxF+CB2uKjvUNDvThKysZvnnzNSuf1j/E
47uyMSktP/ae1+G5C8sChzsPNJWdTEXd3iwX+8yTr0oNYxkdbeocifcoonNnRe+hDmWUH2yE9gCY
z7qLHWVsxFTx339Dn5aPjXpNU76NuaScHemlTmCyJH8IUh+5l2BkCGZdUMBgO6a6peoNalUZZuZZ
l2OLKFtU2AI8SZX7qMtLUeY37BmwJm195bzG0/K9w1UO5gqgvnl9fmBPTY06YsCw59NMWK/mDWdy
CXndkmNDUudOvYhWbk6H/+jY4eDW3o89KlMxsg4JbtraZ8CpsSF2iLcM7JTnn9jGitHFCoXJBMpk
JXIqyyx0i/JFnDrWi1s8Yzgsv+hhUX9seYDOWyhpDLxPwS90Uyy9FD5p4BP7ipcSuLmN/XgrwOv3
KUkF0NcclbpQAVoubtJPm6SLQedl1pERMgX/azzjVyCR8Kwx5TlerE83vyFWGtNVzGDqoEZt3/q2
SURbBPH+ISVw2ln3XWA+nxM77lUFGx3vvbp2G4McZjaAe9dcAbbvzvNm5nw2yUX2VkCNdwdikRY1
QOu0EOg2ANTCwccwwmd+eK3FATEo4EF1LCJtBDARN0beUxb0lLQOKEEx/cK5cCJK4dFkNLphm0Gv
edAs1rfc4v3PBpVQjnyBvTj8Wk+VB+L3KuwSgAjqMYX/4t+5Dw1UoYKY1rAiPIH0CI2uu2k2TyC8
iE/dP5dENCdCRYm+37RpaKLgJ4IwiBbzolyfpSnJJggWPU8Ey0XkLTjXO09XTgo5JUsbrHtkdn5C
el17njolOW3OQ3biKD73TNqXGJ+rxFUpH7xNexcGdrRaCwUl28C/LzaxZLIUeLqj5HvfYxdTztS+
Nsu4e2sbI/a3QNv8P6ZSnq1C0/45lhgIkxhgSMky2ASATwqGUKvyVuhvQp9OcTAIcLMcak7aTAsr
bY+639AWPClBbFmKIvrMiBN5E0g1cjEwMiuv3cGRbirKCVu2G9OuPqkmqSP8YNnQztZ9JXPBiPFj
NO3WmP6ul50UcE5oh74ZlqH4Htlyp0BfSuu345YIBoLDtPuq1PwBwWfOTeQAM8n+gJQHvA3PE3JD
SMKr9iLWX/su4USmEqn5WvCDseVCXc9FkImPVp5Vw2UM3+c/6MgslknFnlnirDQ3N4QHAHJ/wRg8
fihYMsK4M8Gd8ccso+l2IVizdUfMEnAIpyIPn9mVnycKhaIm+cjSny3kqww0EDYn1VEEKWyIBDii
ymP2WjClx7u8E0R+Yo7MN3S6aPvS2aejexSM9hRx8YwktD6eEpuUvVWEIzOTjXrrvN2XoIfmPDQ5
qcox225HNkDYcSed2b62HdSrmnMyPZlLR1oeAITz23B0vEJXMCC55gS6sYp9T6+6o8pfqJz+x53S
AkMksARmHS1dEgizo9xCyukhyC48yhGObYVe2ygzO6AH/X2qqHhs2jffEc4oLe6E0cO9ZijVp9LD
rDxcvgJp7wO4dViHF4+4Gihpsn+davc+2FCJX537f+NvXntV4M8Razs/60MFWebRqounEPTJFkZf
BhrhMHdYmwQOhHGLyNmZCpD51/AgGEA4qi0YHUVt3r2PAttrOWmQnrhAe51THxWuSsVudD3CuJWi
KN6y/vCyXRfJ//BB4NbIAGbkjmsx/cQEYV7skwW8J7VuTAp6ncCEpdwF7swwj6VyfBpE3hIBe4iF
MaPsW3MxZyHDDR1xu+2moEfzTLdre57xtxRK3FvIbGzstrUej8UV28IxlSEU16ynrkPbmn1+mBPi
DlIjURA5fEL9d/C9WZtedkmOOJvV2R3+XVqNQHnPjaE14enT4ky8ndABFuAC0dOvlt7318EDLata
eu1YWk4mDWhVRZfhOgUYe6kIdoGxidMudONm5p70ma0HhzSwxfP7ttWUXTV0V0WuTgRJQwxOQjLE
B48Ga7ayVvGQ96Mq/o9yl8tXerIW8DtOpJMryvf1VNVpdIS9dzgS9NqJzHQ5Qg5ZfG6SUgv2zSd1
BFYihbwdwkNsJRWZo92Mtvk5rtbaoKhT5re0plSdERyYu8aQVSv+yab6GB0gFNWc3K658IXqBtrd
dxqqacIXjCUusVrNm7r8o79X7+1NpZWWffwfN3uAmXpUasXB0r3jIWHrDNY6yJQHXg7M4D2ShHxc
kqvkRLanMtwVrtjUynetEFiir6kiM+3zJwTULsuQllz3+Fsor/rHs7XJqW8EZOUYsp+LdxEODER9
rZGYr9KWhjYNHxg0QqLNhj0n/TwEU6WrHC/emEyI3tMCIuHNYZZyuVAjS9OQrJHT+yKHyLC6l4et
33MrUe4ZU+d8fKwh36UHvWoaex/+sc796m52WHOa4/pyJzhsW/k+LRuuQVP6+FQv6FuvG8YNtSWc
o7zYYYsuDV+yDOsehEE9tueYfSiSOkqT9viI55LDKV2rtUjp1IINLKePoPytCF1g8cHtWVpig5Zf
kz+GSTgJPKZ3BEaBnWET113JE4+/7rakEhAb5jitfjiTfoI+EMmWEECTabI4840N9OjWb6POHM9U
1vrF7z63w3hMT/afl7xP7L45/374ap82Yc6b1zFePlZGLXOWsaKAP13UMm/FlqvSnABq/uCMm8SF
7yQlAdvDrFPCvfYj/kEp5c7KWbOZSQpH5kDP56pdL+2tg8380U2sAqU0d67HZ+4E+6f429S3Gc4s
4GTrXJ/2NJlCBX0Bq/gVih5sYlEhCm669DEJBctiNt3mf77t3ZLuJlK4VTu8jGog0NQIcciLBpOf
QrPNCPw/CY0mgGPk1+iz2goCqu2zX9a/2oCcBbt7dCbTmb5mJ4Xcfd5tDG3u2Yf+cmSspEIxkCSr
x2d90bEUOI71JiVRadGc6VXbJBn+dcKxyF6s05Ozk9KzQJzzBpV+DHQ3bvGiwZJSNkKBlBKZ+Sn+
wYa85++CEyHQqBkXDpc59tH8wWn4V8GM4n48/qrjlEC5MUHN/x2LB8wHN1mhysR5vZ6Xwpz/1s/a
5s4AcU/0xmzWMUvWRZTjBg8ehapbuf/n0AcJynzDdaCHe/Xa/xuraFwSFjPD+qII+o8kTpRE/bW0
otktpg8BKxRsGTIef7MOjS/ouOOkEOmWRSg2yAMf6YU5JnX19eyVCIiVbsxtMiEzmjfMgb3W18xp
9MSwaHjK1HUsIwB438AQNa+N9VzbCWWzpZjVtGpwIaiu98tcE/GEJSjuaWV08FfZqiZhIt2EIUNg
Aij+LTvHqFoZ+2iwGwdJbGTkYriPwLCd7Iz8MFVRuu8Mk0Wb08YWxARXrdfPb0ND33RMV5z5gwMU
scaBSDGiJXJhSyWYGZX2Mgws+hvbyFp1CGzidfjhxVENaFDbnxTVVQINRJwCCOKpCElZZAoPxfTr
sCgjrn9uckZg9WPMPABhzpZhIF1wuY8X0XL47KaaX6hOihT4wzKSpf2+qwS/ITAbzteXshcgNUse
dJ054Mx6NmBDEw/G4pduYwk19tMD6nYYvIOkqGbHENlhtP47njD3VqF3nAjIQbysCVVSm5PVjgsf
jOEf1H8JxG7t1NsAUFG6ri5Qzq9t7r0pSsg7E2VHaSA/Cg1FHGblmUgdBNpqcyGoPbg8KA8XkCll
GZyFa4GLF5+i06gkj72ulp/UB7LpPs9lHQMeIziQDH85/b4oStBSv4Ko/gPamHFa/Ar5+c+w/iGL
8NIHU9e2iWKSa7B8BIgr83nGbEJqoIoK6aANrQoToKqrCwvBdz2nT6MrmAKLx0t5rY/9ghH5gOtc
oAza7hxMGiirKbHmN+PzoNW026/W9Zc2xX5UIoNFNfBebN0626Y0jdchSFw3Pv2ZJkpcJQ9BQr/v
D0cT7OZ2ITompBvadW2O5/G0QUnr7bk/TjakFcar31DDHCY9DcsGZcTxr5e6+ipT5Y8SEl3Wjzdu
PiFlNpiDNXZEV8J9iik/CQUQ17JNM1yO6ya1CyBrCM8rpxl7GIULORSY/hXsH2b/4ll1FUHdj84Y
k/3On8xm9+FEc/LQzbvWo8WuOPcQF/5eFluXeD0pXCKo4bkeUWZ0lPd+RV8MXLrQsJTMIbsUAqum
KzCEdNyYrsvBD8rUd1btjZLja+g7FeYeH+oRXv067cmht1ZP9yorN4SDyHenHOfRXy9oGZ1pyU/r
fdZEmknXqdAZs3A5JzuwYfHTgfnHuqxinbyDBvhKBE8ngjGBSne+tHiSIKMS8PWfInlEd5CbKsqJ
6UEPQfnzAsUB+9dvYLYfvuwWaoHfVXv0jBdPzsbjKL1XsX7kIWHVSdkq6AEMacHhMRQYLs+teFpO
OKRqX3OI8R0UxiQtZmBzUqQLyrz2NosS99lR76BIZ5UoO/gCmM5I4nQXl/xk/1r4JuuITFty3Sxh
yTM8dz3FcN1oFH7ekCZbRoV+j+nXs3+KQyZ/2fXOtLwAtewuf1dXdtPjQjM/mufwAPrWctzwyS9k
uKACVaoqWdQjN2qk4YYWOPfIdMgQOQtKjXGf1sHh5x2Wxn5b3CuVVlrUVdDvdoz8rXRqjiJrVzfT
hrw4FxoVtJ2Lmorj3Na7ZkbVf77rJUQcZOjCaARFuLIL2pwojk0hh8lgYUO/txvlvo5Qadwm+E89
DTzdl6f0Ax8RqpBNPdYBjwX7r9FRSFI+IDYSBXfJH55y5+n2ibk381JevOR0pkkL9ktBLkshoBFC
9KdsZpS2zC5HfFSfBwj5BCb9mbfFWE6ioUzLfsat3uDfFIfDmrKX5lSBEjIRysr78tU0XrTCv34U
7psV2txUwHO3ADz+MlAr8I1M4G/6XJODnsMqMVD3JS2MXdilOQOMooRPjjl8bvRDMLQ9BFVuNGpo
xo+35GalCdnvL0WQYM7vrgbbOcnM9mY7Yn4PuSt2NDRC8YdE21lyVOhgsw0TGKbhnuTKo9F8tht0
F/e1BZl+UtbvyKQz4Fg5Hr016glnr4dGx6shuBJuESCHhsgkb9XEg9iBv4b+UPe2Irwklk8cDjRj
dMau8jCmqWzUGDLVPbTPuGNJ6+h5FxDLOnW3VbtSd1on7B6MhToJfdcoz8BvyAfFweXyq7fmycHf
PJMEF+RbinO8tUuS0a00//3vv75mRQmP8lUB0Gq1rU7BYwg3oHGOhECGIiMyLSoVhTEaHgLW7m9v
V/MwQ1OqisgBiN890KcJ8Yr3ZP2bK1/PqK3MVEnn4QeTFQ4C7Bb+cA9jiWpPmGMrvSR3+ft17HCD
vb7auMw2h22pgvRBaCDh8KKqYG1SS5vzbq3gXW+GHEQTDzysOb1ZBAe6O/cGRcTkPL6FGrVyuV10
LP6GzMWUJ0Sz0vqH7TQjBbOSMsj5dZ9CdwVxSyMp4unHO0hd2T/7JB1f/ZRVO4I+1PUlb7C10JaD
tiaBCUDxuTMEoZlkeXTsE1nvgL5ytua5OTQ3VDSN/gYU5/Jddhcm4pdQEJu/MXiGSQTpLBblwDwH
R8pYGblJbzISsH+xoLy1pUa2yXhu3xP2+gDlhzojXKCqucPAznW8JLmNzsinomwm7D7n0oHXzFaC
ULxK4vdhtuF6VPuCu6PYtEadISzxcGok0nsDN6KIm/EqexwgU9FaYZicBt5dinZ6V8B+bkjUQSdp
Ii0Wr1nrHhUogP0xiEsfoHYQBMljEokfnhL5O4ARzKOhNSVGF26EduOThGjD32FMTdTbVZPzzc6D
wOHtCCZIHO4tNDiBnw5jX3r/wSb8GdyHLPRPkTY6mz6M1TtCarQ8kSYNpuiNBjEm/ec1T4xMdrHl
06N7w1T6EEQUi4pRcayGGajRfYWURcOe2hScaiQ9LHVmsLh2iOkGrzUdMkqaJe5/FdGHchvBcUDT
w8rsmYAG1FG4NuZOKuh5+U9KOuBqSnrhFaYuz+zQSjrsvfzBfoxD/Rj/G4iT1qSGE1aVBigxCdw7
pDnhSnS9qK1K3YBz9zgsOl/jHUQIUsQnbjFJPjmz5feK4h/PaxpTaj0wzzTr3A5sZI+2QaCEccZT
ivzE8wdLk8Y0AROZvGDVRHO6IWcwgMy20Dt6VDQ70JSD79f+D0ZayXWHMfkE0Zf6NlmxRgSGgMnN
RaBtQFXDJS0sVbXTkJmb8j/6W3TysySUv6AURolvL1+9YkXx9bUux8xC1FVdM2g+f+VgzMdeunFF
ERwRP/tm+Nnu5GUxthOPvhfFTHvoX+hmgwCjUR1ffEY+8ZNMj6tVSmIt0GHXldUGZL2hLnP3z2pm
e3d+HcPmMECmTSJS0QENIhjUE+hYs+3nkHut1kaX49e02eyVogMXqg90I8wAdnEDB8TNEoKmz06w
RSFpBFDb5B5Rt+rBvt969iG2EiGPjMesqezztAz1Qhu1FVb0w6nr0M25xQvCqfJqt7BORXbYExvH
pmgQ/36BNrSw3Yfs6Vt14RLqI81RoYKhx8MTHIixKfr378VK+5re+lhGNnxWf3QNN3PuJaPwq6m4
/j9v2qI/aSXEfgGTzuZpOVx1y9OErRc+rV2ccC4QQhe2CmOfixYsCP34/ConwBSgC8mz0Wdx16YQ
xKbSo0Zq8HPj7fF7Fo3oPSxjD7J4YOvXBbx4Iw5kbKFvfNEVykZ7HS6IYKtervj/shN8M2wCaJbP
Jg4ZDnooF7RdmOWWL3t+TbOmrNBddsYPMtb2nwr726j8onBHlz/wJlnCApdalBvpat+qrs5RnEVf
6IVccsYrSfik7eL45qnYsHnR3Pg+kmX1Y238UsPJzGGgviiIa6ZZJG8sUogYrPUC7Vh1kSOxt1gp
oqUo3zv1durwBxoH9hK5DYAelyLjTVTJdfQmxKvx14h3ZOIBq8mcLveXevI1kbKiRpqTtO0Ju25/
Bu4v5oweWPGhFxYJGQdBzCIHSqGOv0YHZc/uztaYNKMZc7as94OZvpKZiPnciU2uQ4x05XIwVfY4
xJg/y46YOVGrH2aJq/e+FpBkgo1jfYUe/Mo6Ryx0L4qbd8bAyjeDM+voJh41Y2mTFxpxX0USjPXn
PqXrxMATWG2jwfaANi4jHVD2KHJfD8pxXMRYZLFsps/70SSyGTN/GWAzAu/AUKQkdwMen6b9S+tB
C9ozd2CQFjoZ28giFVdmMWkY+WvflLRE+vyj6gHEWZdv26Y9gIY5WSjEftjhnLflY/1bWKyaI+x9
Zc5h/jkXvOiRw7nOtMWPdJnO0lphXe47tu5DxDa3E2TDzs1vN5+Cvbag5By38ErmRaL4gI92HC2j
GPybF62jbdtuEgZePC+IKWlhLUcAl6vqqbt6Pk5f577SlXSzjLe5OQVEw//87jFaysLTCIIf4P0c
cDNYWmQAPNHRL7+XfcGP0FQTa7yYtaqluwgCf5KtoepzdY2mTfuiID/UYca2an5+Z2fGKCrW/MTm
NQE6G5EBsyCsLQh89HrslleFMqgG2ZK58YpmsOxQL54KILlFJJsiEHxirSjDhNjPhQ+BCOHxIjKv
bIypv1hSzbzjZ2PLs6/qMNaO3LMaxaFZsYS46VY+Wc8Hagf8mXnf8Uq8l8bCWkuMZKRcMONpcElS
Pm5nwGaETeec1GazBzAy0K30wbNLoEfqKJ0ROkLnt/yrkLwIAyQGdHk5QvXCjTq2ymnUZXoHLA8o
CzN0kJ7Sbz1EWQ6eSie9fkVdQeiw7BtZq8/e74i89MLI8ENtk8tjU7MAyiR8M/gF0p0Qd03NueIa
B8H2I3sC2N0JlkTYTKTTu76MoO4OrWwpjZmr9buo+dJ8QmxxST3oiamheBRe3s/gT4Y4I+3SBqk0
jAlO26aWFkqNnY48dDhjiTOR+7vMWc4YJD0bZwunVq3kcti347B9uNDHQdON1gTXnHK9P5YByF1V
3y3hr3T6hPYSV8myObV+9GF7Q699Cujn3+48nsyo3m6C3w7E1WpdQ3oUGhbkGp7QRFoDh3muMQrt
fDfTNL/xrO5sW1HwamzCBjlETOINONLUjeGGLjRXJV/gbYy2CeKostqvcoZAM26y1KZGUOuSsU/h
sqvYsSdjhbf0BJPl9si/nbhvADQAAkrbJU4Yi3GvqH3sm8gzzysY2xx/p3djrhDDgFbF0xTNnZ6U
4K2Xqivxza0JYhyqyZGgqreL80/dH53MllChflVNGYqapHFJX9//kG58AsE6PPoJDAFqSeDSDubF
zJriK/77hWP+e1hJuxtzIXXS4wkc+qjSfYxW580s6J2OaNvRWJqVWyhw3+H8AnrylyjkZVyOSRek
gapGfSUla+rxLIFW90ZDtV7T54SfYHHwXUAVxwyAAcrx/+YaLIVG9HD11IOhmPk04cNawNTCzP//
Oi22z2WAeJ0z0fxbd1Om4uVPrWfDocwi4YMf0pq/Xk3bixo5Dn8vXVAN1Fthp7f0tFPgqLj3XsZz
HWm4DkLiY31Alj4insaBkdSTwpcAfkaieGyVafWxztpEwZ3bOnVEOgtdilXx4sR6SXn+SNuDl2Zk
r6txAKu4asDQQe6nspr4TCndhlHz1WponJVCYMVZioZsZUrcug9lXqdDXDW1MdP+VQQPVr3Mkidq
nOku4yAhR5PRWI1Vsdkg8zU7LK7IKKJhu6Vpb9AwohccH4TX9InxuVBaVJUsajZx6Gnr/2o/u+T0
mOMqVHDrgsFdJF8lVf5oeo6WY9bHpsAsf7V9LPK56WvecH454sfmAZ0HgLD0yieWy4rEH5rMtEyR
DuFOPTkddmHjkObFAlSjfX5dPuNhVQt340WwxApC9FQglGy7Qd4RsF+rGt4MPXIDidL6awLKBgpO
Q6TrrLM+66M+prnTX5+bw+Lqym8K+mrfhysINUYTsATXhPBFloKmERwMK6//Z3fkpBVAHhJ8fD/k
njA2yDsLWNZqevm2eEhPeTmLPqgjd4NInNOnK5iWUoebOmOh+M2Cn2R8hzi1+uL+mvnAUhTdRWds
f6UPmQlDTwysqb9ujbq0Ei5RldpghJA/naF56Uitd0rYno+RJ8ENgmzqs80pebKAR9OlzP4c4s3N
q2NIRfM6f93kznFobx9gAyeoh4GdLiiU74FsJbTdc8ixmImCX86nUsn5rk/xqVP5wV5HNqvJOs5y
WFiryDmd6K/oiCijUwrSNpwdOi1VlDmXnhPCSEv/o6/RwZizwSuomNAKWzmtY/LlBcHF0nZmzMir
xHsXdLnC0RG9UOqoHYP8WIS1dBhUxU/JF78mvw1GJVaxQ/VFCcQ5aOMrXawGFPFNMMbfYlsYbmd5
W3SrHoE9YqL7NaG+/Trriyonz9t0BUoZGGrUej2IgYqlh3BXQKDzVcr+0/FuL3hC/TRw+d8fiRGv
KyNmmHhEeitvWqQqYX4FjZuYdCje5BdnmWqUVEtB76QT2lhZzepeGqR7IvL6EY1EPOXl3Rur/w0m
T9OwVtMqBpopgHSul514i4KjvggFSrNZABlO40vtzqb8AKezKVraUuwkMdyQxpgYRhZxKhjrrERe
fzHwt+C6kcnXKmv/zRGYhqppzWiAmB9n4MCTW84Qd0t+/HaFOw/B97kMPEn71J2qITmvvNWYYaBY
6tHX689j3A9ZE7CB++nWLb9NAECUuIByQdUBGepFWQNg5yQRiqYG7VY78I9TDYck1ac0ufcRIJ4A
odlyJT0fvo9cAdw7+ipKsG+5ZXd67z5JiTySYZL3aa9JsNmNGGNawBtduXkk4gKTYTcvwN3Y2wDl
wCNMtgMVcZhsQCiCXtNAS/jUiQ40b4vCkh/a0KAR3xR4JfYkLR4LGJs4/qpZBe+A8DUEJknwm1wg
J9Jz5X+iZY750HMnN06uGL9pQFswM9Jq/wkJcQEDsWUDZAzxTyRA88GMEJbNc33EkQ8InW0RYPSq
vSp4rHloIh1Zmo+Qek1VObEF1pe0UukbMJRA4z9x/gSg4JEnMJ9k+t9toZG969npm+5iK3za1Tkv
gsTIYo3h/XlRWKUSBg7MRflcENhyipVIOxR+xirLnBT2E4hzEu/st5Z7cmzJaeH3le0BgCgqssrE
PzSRuWOJA8PxwY8i3wnKa32a7BA6txz1+FWCuE/76R+eNIYp1Y/2sW8yH5BEAG9kpZfV1WVijzpq
TUiwpi6MBysoqJoLIT8QxOQEcYE9CIH9Xr4ak6TxnAWLkfoJRupU2QD9zJU3vFsK6lOJmufW8oPO
typXXE4swo7y3af4jWN538/knvwEi5w9ErO01zRqp1b+8AaUHOK/1N+0+kJsKDVfQ9duqaj1xJex
F7Jsvd18601zbglTojwy6jgreFKQLBfqCtCfMAgeSWHQOfV6CzKmjqbDgBvqnLyr8kTQyq0w3oVJ
sJIgWJYCjdD4yhfOJiZ1kF98Q4JSVDLRqKUpEsKOTwYEb0WlZG17ryO7jX2WbSTgkTUXUneGvuc2
NreoLUYQBgkTqPg+fKRzTS6lEgf6eT1IH0TCb6BnG6vwaHkT9pI8Bly39m85svpeE3jXQnhbBJRD
pri1UeaAc93N9fvKCv7KhQpQfopjSgxytdDiTWj4CztqorUH9O7NPMUj7HX7zO7jXCyzQPEgm5w9
a/zH8UCKhQPyTs9uKFzV6A1G7d5UU8vR9V1Y2aOVvI+o4Ku6LtW5IAVnZxnNufse9nD0SX0R7Qfh
vxt/pU0VatNXVVjSAYqvkbN46SdlAR32KdxjuNOdSx67Wah+XhB8Ng4FpmjicWye1P+O54XTCmQg
XafGhjnsK82Ub3+53r12Qxvx6LwJA1fFxJGx1EA5n9NSpJ2EM2xy/tquXO9WXKuyfhDBzDcvd66W
pPpENputc9dNtRYwl44qoWKjGRQEYLWqs/LCvk8Op47UH0APpOP6wwDtAzJpBeB5U+MMvt9AAl87
JPxr4LOYZMpK8bmUEtQ2zfYD1javEAWVryHjr3T8G2SwMrfgg2HZ8zg916gafz7/4/rRTX3WaS7e
PLuYiwkY1cxOj/2hvlahK18GCYnn5ritHSPMtj+V+Qq9OLQ3X3L4Kiqir1EduBHwokYYnYT0TSmM
8TwSUM0G8fkjjw34NCq2XaUgJWO7cnmiL9N87cHEMcszZRA/KaPtBSwcT8vYSiz5KT6iZRDMM3Yb
IKCIRpp+Bq5CgreaNxOQIrHNmDEUNGzGtppZe9bSfTFe45v63DfjZdkEfyZqP58XcUS6pMNepTIb
9IUmmw+qtitvyBN3kh0B3qDfX3ilCboTRDnmR5dZJynO3N7tMMWMoK7HGYV+kSvcbS6yOZopxma5
VTnhIfrUHCyXG1/XykSzPh8dch7qQUZfXDOzfBduNGw1yYgJI9Gt5r5wFXc7cSwkb+ocHv94M7wI
V7X+xdJHgSivquLRAN8uaahEvJpbccdaqba93buxYKdqkuToyhiEZxnw2xoYwsDcWr4w+5wzjtQe
bm9gJzNR/S9xpdPEb6dlZKRxmT9GxA/6TLO93zAMel76EDIzciZK0W7SF8YfMAcLOq44QLEc4UTv
Rcme2z4udOhG1ngJCZknPxMs4EHLz6NhAL6y19JSdGg9n5peEJutTGmyo6zaMo+4pNT9P7orbxPL
SlsQcMjPEVGp8RvEHmEq9ORNhU+ZxiYWOVOjEocjejIbCHdiggYrIXnkbWFzXX/ffkvDhblmEuqK
fCLhnarA/aXc4kD0qft5CypZ34L5Y09phuE1xJnLD90o3ZKwAQXZFic8wGw8uKdiQRO9qZZszdUM
bCv7jse17O+0r9LuL5sVzm0A+EyulSb73aPu+Mmu1a4/VB/9ALM/GhRjs1Wkz22IfuvY3TiOZ2Pb
tenJ8ITfbwJpFHuO3ofHCxk4BtDSzmuSeUSXwSeyCX33cx9myd2W4KZ/aF00Ily8VaMVwkm0XYuw
28ynbcMY3sPoPo8nKGheKD+sLD0FLB5v3tHqfIniY1dAof2qer1AQgpBzcwUI9tc2Is5mm5JpJYP
ozsDwxVoh2qKVXWtHhbdonNy/IEkYLFc4gmsgG/uIygDwtb0ytk0CNoLZxXX+Bv/ghXhj9pbNl6o
qqpjoIrmjrwUzCZEB3E3xLIolqTJfe3ZRki220E9iDrq/SGU6pGJc5odafxGP3/ecDzVClDeYzgM
Qd6VSWUDAUlwcNHFxs19SR8leMtbOR13WACr6qCEQVKt3dqZitpcx+XNCIA+og1wJyq/MBsCVncW
fKQL6BJoZkdXAWfZ2UaQi/MPZXfK/FGAvCpCuW02my3SmADQGOskPbrt+Xoo9jfnQfrIl6AKTC/4
JfrlNLeYTdLRAXyyqWa43BG+Q6fP3R1eN1NWZweZt/No3skWZ370g5HzdYItSRH9aTW75HfdxuuJ
i6qx2xTnCwH0PEy2tLIHRfm26I+EA6Law0cJa8TkVmvVl1a7eWpEmQEgX3FxxEpA9gXS96UuQgII
9J1jaw+r3TXXHvBAqb5q2u70n+SLseNn5WlNg9oKeLExggkkeJW62bhRG8Q4D+P5tTOnEJzY8tMq
T/uS42SVarFJhIAmSqzjMaRhO6pQLnNpM9L9dKgqIIUq6YiBomzsb6BMnZpIvWr3sU2zxpf7Qgb0
0e9hTdHMbyI/3G+JMmFrHpVjds2WFV/yNIDwfVm24pL71KwngmNuomWiPTafsbfzMRPPzeEhNBEN
a1vqnqux5AthYPgCOCH+nY9s8e9+LljB2dXnOq2oyDW/UQnIG6/7PCrSt1QGdvBAVoukDVYXrCEO
epsTmEB5WMrSnSclXrnQNIBPU42+ZbG8M3myS2qHTSU7SpWf6XZJVCuD92nNt56kbO2CHCUUg1rs
XGM5cNuV0wzVOVy5IPsuql9CNKSv1vzTUozPGMixJ35hI/9krkjF3XeW+gEGyuq+k9e4QnwnlW4W
+zFgpx8OossBUbQbVrbVfbcr8JpoA4GoM2FWtpvfWjv4/Vm8op5I1qOtF3VTPvHpacjgkimWIFnB
nxiFriSA16sMDM0MhjWXfncV3ITvsoAnCc5g4lpIU1vzu2icDalOQHIXn0IBuZe/nydAB8TJQ9s1
uuJgSKoLeRHHiBd4RFI8VPVJCQl8Wg+hWZYkjR9LQp3bdWmZXHI1AI69MryNvnCzbyQo3RePR3Ij
FoTlztJatfPLWhPWCpq9ApwGUx88SVkp//E2ZDshZ6/ZzLfE8y24OvNuHnMIUhSCmEVHz9hDJDTr
O1Me9pcK5gl/XkTFK4rWzcIEjxW4ljEY6pLpirMUbYuoLubN5CDM75TR0j7omikQkO/yRVqI64UF
Y3GS/eMJBhahKTFa6V5Zgrp2zEonoLx1Wh+WNgqY6KPP5OYoZp/nQ52SpkEJKY317b6x5x0XiY3M
1VNcsoY445nZUSD0KDaDZKeFKU7MeH5JGbXK4Jz9OY8WA+aNSz3Is4spcrpmBW69kDhNj3ezeGyY
EVD+xs047tCnU3rkCFOTMj3fRarpgq8NpudHzPg+RWUm/X3ZBC3SQMbrzdj59sSMhVvy526rXPOd
2u+MHOvt5OkjeJoYbBhChDXrdTJB0C4nkXSxDhNqCoj7aT6jebwVusuBCG/lOLpuHd4WybeiyWDz
/WYRabv7tODf7g5BcSzA2tayw3toGe8DA6loD15q1eCz7DnQyFXxE60jOisCy20KEXG8Jxz2UxHd
dk7ZXRVzledl0uvGkbriFH9pd/4Qdjy+UHDTdaTTJwa5xFIzEosn+NEWw4TNeJHsHWq0+LCIFhzu
0Lb/ZfvR6HpLy4LgV8qwQw9uQi+O5qwBrulCk1XLhqClQrw6buLcyGJ+EfjEgc5vZbk0PiKBpfX9
5yZUDIxm9XJFUaLRdbzsAHEtAL9ldzGhj9aea560pz8lf3d62XKFlNppS3karZMUY7iTMTbOnQw7
qoYxbHD2IZepNErou4E0X25PhKushEEsmJAfCDaXPP5N7M3eiv8/Xg/miZNiyvXXuKBLWLK3Igig
ZRqqFLrjSO1+NkJXJrSkSYhV67YH3Pi1VG9zyhHOSZSE7nLm2pREaMWLdGpZbJ2/1nWpmLui5c6d
2TSIln3VNbp+2NJ6BW+AziVZd1cGt7tRjrOEP8WryN6XtE+8SSEB+OuZPL+8rHCswmAWB11cFf7U
04ZeqGbCGqXqblykDFr+5On4GwIFNtpvyFNe/lVRGxCu699SdU47CsR/Z7+sqZ1V6Z7rqUG8w1Rl
CpCJHTlmat6AlQO8gsl/gDWHoe7QUxWLGhodfU98WO8CDHcnMWdv/EwMraaLjdrry9u69e0YQmr7
kmYEoUfisYvMeSs9vs+jQjSptMZkY71aPzUrYJjWi8weSZ1i/7SjuwpbdhUstSkBivKFttW8JKSq
lPgfFQaF06ifHyJtOB4KkmAmHePBoNCPMqBGa2bT3zeQOLf3Tk0YfVu+pXfrN5wRBcy9CdyxvJwd
p0gD/2ykjBanApLQnU5Dw+TxPbz4ts48F4yZkd+9fvgu0f4Xaqk/EunZXKYqj+vR6PibFTRTo6Io
vCPAK6N5K2nx4VzGTpTg/o4irEV7fL+veanWk9ZpKegoFHu1i+CSMnVoWSuSwfo2ZLC1gdAYe8tH
OmTlA3lp9f03ielHYh/9jc3wWIk+yLc/aSz36rdDvCCLLDXsA8mfqQ1n6uskmIKCyX0KMWHVSEqQ
tFgvudZYVJI4gpNomRtzVa/2ojyfu8kPyVrGBIJf7uQBeGI9pK0rkTuFwIZoZET67c+UNYFw1/c+
kCRruyE6rw9lpV7ebXq4hCZ5JrjN4U2lQ6cog67LUma6Tmvz7wosd07u/UjLz7IYmaJna1zdS4Jv
KtL2ubKAWPxoCvYkNW43LGGOaakPHQ7vxIefM5Lnp5JDOgJfjCi5e/1mFD/iP6Tor3WUP2P8mIFg
jzEx7KQ3iMMTSDFCKW8G33A81UQeI5mhYBYoNBCs9KN+rDg+M9qGmQKBrGg20PAIz3+1KR6Rn3m7
AgRvtD5q6LjYcZMIZ4MwGd3GMf2tQguI9P7i68fIxWdSzABmCc3+cIyid0U5zDEpNN6E2wflvc0L
cWTfcmTgL8U9N0+oMbH9+byGiiH/JL3BTIVengtbQ7DtFccSCeVlzuQ8UFbK6urVrIafmyDy9Ppe
tcLyxulLQzoC56rDl/MT/bRl3+QqUD69Yl3xCiBW2nwOTYPqtnHRP/62EzFBcQM6U/8nHzlbHbyg
26kHv/aupnAKzORCRFmevAXJktfQcmZRL3k75X6uAwDr9c+emFolaUBL1SdnKFSQ3EArLB7+KisV
MsMQJTssr/t56IMOeHX92Twnp+4G4uJex+GaLxHzlBbQZ2lLZLm3WFdOpIVOrFGgWyArDkp3fBVR
EPJ7mpZjooxB2MVJLlaewfWSSTUHDdRxIByf0PCTt5mXBDeOgB0EnbZfbbPgns8EXfhahnWN8Cws
b0XafBkqGExUvgmjyNCSjXK2/baTAT7NzlBz9wbxCv61aSdjWjb5GgLZbozSz+Aw0zei89ptRgoY
sg1fVJgz0Ru5PtIwFbPM5uTBYjOkOG8C2fGK8biHgm2/VeRS3zjozfV/tEIgxhRn7ZZFbEgIxFs1
qR68x+vrQEiWKJaGmDzFocF3/6JBrZV/ENJ/fwSHfdoqM2E7nZzGtlsLXlHRiLlX9TdkGrzAKkaa
sISnghvuMiUiKOHn6gK0Ak2UB+WQ8psKExgjTloTtY+oqDkHAjBbSCBhJ4U4bkp1be8g0xRMDtqi
KTxfLDJBSNTTirD/P+mCrEzzZhCfqbEot81eAgYKGmXpj02I7t3hej5X0ae6nqfPhcaJPty+LUhr
kcsSgQUVHSdSaXAzn8OHuA4O/IqJKxhdCPwQNkMqAuooFxPE3uKeGSEadeaeATraRIMA4Y7Z5Ixn
j1UGIat4URSDYnd3xEY1loSZwEqnxgZBWZMkJT0hM8DO6w0SenCcpEBC7RlgQ74CMGZclufzJhWR
arx5m0/eWMopaPHk+IhK9Ie8pjZI1WI5iGQRC+GAGvC6kQM4gjeHZIHsm8Yar4aYehZtgR++62te
edAi8BTBAMTXUIDWfUVHe8XWU6NC3mMXL0D2RC5pErdS++j0HnSQdnImM9JynZ0omIcXqNkw9xqT
DU0PZEBiIdrzEt1DS/PKmtoqRK689o5xhtQvoGlumyay5fAEfUay1Z3rtipBYJXIcP1EBKPa3GRN
kAlxOsl8Dfd0sFwy2qVKs7ETk3CAMdrnFfx9TKYCtzOO1FZiPUsWecFUPYgXIcGSPkXBweyAaEx9
+kVX/3EngqLRasgSLIQCL6ado1T6+rlnhYUsh/Yml2lKnsk03WFPB8QxWXOw6iZGxoIGZVee85/1
YCqJvP9Wb9AdyHMG8LslPYz0PVBYeEljMm7LU4RfnnAoSMs4Rfvx11QjDHezCc/sG32xaOnkQEJq
oA6EsQRoui3fDDI5wP+h4JxpQ5XZeFuR/FnluRgGCdmT5UAhxJ2kjwLsfBoSwbsNYZIlzg2yGjoz
LiKDazIdVEhywXP773VETFpaXcayd7ZMg1BoRagwiyw/LGa5gRagXIp6d+RalRqsPyHEgGsJmjrA
3WqJX9pMO53qoP4XQNc4gzWe6+glUI9ZIiVcbEGBU8Sg3OVUlBlWQ4+3N2e1WGE53+Uo3rYtOCY8
gBNkfkYRc24/gQ/35E+g8UbVL9Yb/WKafmRb84Qg7O3No3fTxepdQB0PMa6A1DaBXV+8lLqeLCHM
1RUyoSTd1olodr3iOhkFs9yLCa7PQHGduEDlo2f0CQ4yLiOAVji08efIohdnrmhws9B/lfrd178y
X65LzoZEBZjdkEAwvNfi9P5lCNuE7sPgfnu4TbRCDGs5t8Jnm7bcu+uEipGi++Pkn8CxbUMfrXIQ
IE/j5azUlGURuXBBxjIKTOliy8H7uR+dm60Q+aMM7CISmEHQLGh1McFdDNEJj6K4OFlT9m6NPNdU
ITz/YzLBXmjqFm/Lx1cPtb/Y/drGWqXfotR8jx4h655asKG4VO1Fq5iQ/eTCUVgzk+uJOEL7nrF6
1XtuC5hm6n0EIWGGGNwcCkDbB8ZWclCt5/HMe+rAjQFsoFaKJcKKN8G4D0o8ClXLYd8uRwa3kX5z
NuW+8RET/S2YTaLekqN/EtL1ZwidqKH1Fa6i8P7n5datnK4nW9Lrg02VUkdHEzZ91iLCX07MlOB3
MQMSXPA6NmdLSJz2JpwNqcACV1KDot2fPYo2xAY+tMQOQWv3uoQvof5U7vJckp0c1tU3bDpNhjch
I5KU/EyvE7pqEoE5rKhrgY728VbsFUrl0uZ3nHwJfwiumvzTOdHHvEJuD71nUogLfk2Yo0qqn+EL
wIaltfsAyk9Bn2rEJV5FtmFK0Rt9euNaYg5Fa2zXFduC7jTLPPZ9CF+3M3AhAvtYBimqHw/Sujld
snGwoGVB6z0+gscYmr+A6xfdHB944y7p6v6UJrU8YWNbEnbIshw4KGm5o7nQHYbT6TaHuEdRuoN3
D3JWkBDfUetjCXRhmx4hKjaDmIvVcvq6Zyd/KFYOHYgmEJidH91mhzTmE93NNVvULfWUbarfCPIr
t45zGaxlsumBpSW8bfczb1YDcpQJPUIMXtlpAI/scqWLNQG9Zz3qeNDlizHESnK+u04I2SuJP4Hw
1eo0EixeUhBGbWZ7K4TE8aoaF+lFXbbtTP++GEJptqG8lg9KaYux+0BYoxwULcFbgn5FVCzIl5XD
BKaL0MZGdZaIg9unpgHPRJv5+Iu1yBQcNi9gmC/pLG4Q9GsQdAzfOMi/lOFmWobN0RTiWMQHUTSV
wKuDddm2WQZ5O06dWmCJzyDb+F66dH59BKPGk0Qy12NwLVxnhO8SkipW8PmEREqxKWQRT4SbCt0v
i7h9/XjjJpRD+PfIi2i6ZkuVXzvUAErsJXZNNMrWKb4KTIW9vehWmCEzeioUWhVKEcGeJvJkVi8t
FpKp9CKP+DJC88/9+IUs/0RwyJJ6IT8s1avoO/NeTsPebgjFYBxsk+8JrFbFdlBePHt/pBp4SUKZ
S0MiOIogScf7wRIbi/TxFGwttXu19ZXYct2dShMkrpqTy4YH1RMvJ8tm2jTdZf9gzyG0+sgAQSN3
okbP9DC733HSe9NTazmE4Wd7j2WbITjWaoNSGZQy+LwSWWGcP1Uv0Y6732zm98jlWLt0UbF2cL33
ahZs6L4lIZf7wdfS11VcFrbOArHQVsxYjwYrmP7HblJGF85q9qXgMy7PkQE+eT3vYS6z8lzX632G
hVMSc5jACACi6UiFFTX/szOJVk/dY2UDeU2a2ubtkg4PHbdqchecKhe7m0F9JB1KvV4wo7IDBgy3
Hzp+aFeDXUggZHxGBbQNIHuwYi81/C0n3mt6YqMnlnvGw5kz1kLQDDzzGyohOkEh6W9kiPKN1CVb
5JJJwI9ePlbLX5z3OlFWcf0k3SfCK6nSs2HAJlmGOgHvZgcu4VHW8by2oJQ2qe+8Dxsn79P4gsF9
l5sfRP5HRB5x9c5+MINYr3v011ldw+UbsNv32wJOOn2nItLxbCHXZLDw2JD7g55EtYnzd5viemve
MFRrBn4BEyC2Ak5F/q6tKKDOYNTXBSb9Y4W5MoksOTKjxz8256UwH2rLAtgTnzbVwBxIeDt6Ur7t
MvbN19aAVo3XmPaIIoqjNtVNjIDqnG2wGFJRd+F/0bRKTTPNWkUuuGjDj36VPmoOdRQBnruZPxjM
KgItLhFWG3jlkZwwBOeUJMfT7JM5P/mU77V/i/gNZtbrz28DUVHn7yeYd4WdBfdIwIUGnl0XuS83
n9Nek+7Y/PKRNDep9vTs9+xODLCwUv8fbSIma15Sw7kJT1zLJ97z0H3X99AD8IxHTgxttSYPLO9R
75rmVKsAF2jX0JAQ9f/YFl3ECksusV3KhnWBPQN+CxhkOhDepziAR3WXFrGA80K13WoHENl2Ki/l
+FE/iqicJD9s36KvTi6vmTotkkY83isG5dEwx8rVv4JARbOjWJKNy1Nlo4hBfMTFL4VLQyN9Gd6U
XgXwQ50ebJP+pWuij2VOEl2XaeGSZUYDODPX9fb8LaqlfzKGjrKlwmYxJ0VvdIV5cre384waT/s3
sFwq4+xe9aH2lSxejOpUuk3B+HlX/7gxcJzLrXOyuu/xKl9pEVQNN5xBIhupZ6Djat/cnERM2vNc
xjqPFfNx6QEg5EBsN124PyD09UxXnQTKEzd9EUIZSRoKMmb5rnaiLbgjkwtMQdN+4qZ+KbyvGMB6
pxb1vPqOKhE8DkUSPxam2EnCqSfgwUO57mggJzZ/aD10SVou1IkRQyPFfqYz7KWNd8sIZO3rdiQ3
Kl1OIT6pwLxLsZRHIN+qD5yeqNKk7+7jvq/Sir6nL4yR5E7SoG9AiLy6orEkqC4zPCg7v+2WRGLF
y89HCGnBIVpUfrmXJnexejYvfapZbs6RJrO/XjbeCDs+iU7lpbukFtqyUgz/MvzFJLUZIhRX+o8K
CkCcmw+opQ3UBYxXtOTxOiLGtJzF26NjJ9h2HBtELEPO9mGwNSq4Bg7lR3g1d68GCzcCF5Qj+y2E
vdhb0YgdxyxYo2xausf75858pW/oOSTX/ozTzNi1lZhQwaHPRnz97HMJmGTjwctWq8St4YxKvXjz
5TanDlkbNF+BNc3TW9XTxBMXxzNUsJGSRMFutbIfgJHQ6xocMYLGLeztDuljbWnb25qu7upsDJQn
ECNBu1AXARWvAYvktw1zCvscgwQFL4AGHm2V5e8tHGOkuMMnykkEPKyh3P+mjTzWSg55zSckkSZc
N4Zi0vJapeQ6mS8DSFdOSocgaPe12cF87in8pLS2D6w5HnmgOkOtd/66/qEvZ1ial8BILlqJeoCS
EsOxbLlXmtvYxwL2jDMvnm0SimE52Y16+2sHKbBQUPTU/fbx734sr1TVpB+e/q7Fd0p3BDFxcioc
skRtWZYNzYXSaI690Ish7hRlgv4ngdKo6QK0jldX8OmzFVf1b/1CbpcvoWeARd5jhJkkyQIzyAvC
3jM4wnkA76AjPCH0d1zHm/mf2ytziXULXj0A6MsOEzUFevg36DK6gmUtKt2aGF8fds0BXCPreHhN
JVyGMu3fwITK6tNg5NEhk0wTJgtl83pQ4czJ8emqufkcsvoXgQpfw97/jY5gq04vy29B83vhvfrO
jWCC3F8jHzhRBbRCaLhEOT634glNmy+PO/hDzpVwTlFw9u6Q8Gz0sr74z8L+IvsKOXr/z9v5Amyo
gU8qRe3UOZGOnPBQgfdUlN/g4+VoBmMHD5utVgHUaJEvDFGBnpZqmKvsc2hJaKhRoco742v4tUdr
BAkJ6Qup3uyIUq3xMH9A9QRZklfMhu3cm/3MR2ZpKuXypxheBMA2RJJkQpt2AbEh7L3MAdTG7wDi
3nFhYDgbe8hYHg2TMLtUtzrvYv5/iXEetbsH7ilAPcX1jxVP5OEpMkfPUJg1hR14y0cqZGQzW/TE
3QoeEgRmiqumBNxj5iT420af0UO+WeN5uNbA8h77GBvKEZKEimCpbmZpuitK9gJrh2v1Wd5WXFp1
5lbW67w2xltS3MXrmB/TIzgJgHwMUX7f/xN2zHpFwAnTjSHqxurQfhqX1YITkIRFAD9p93S4Hcrm
EJ/HpDYWs0e2LqG94Q8LieZ/FSXr/ZfbkKttBlcvXjVAfNkH7OYr4QD7Cub+08up0p6yPms04+hF
Eoi0e0334JS2adQCY9KAvPQKIzDgB/NaDBtnm4ZaVxu/eOz0ixKLyxNZq2VOCaVcZg0GZznxX0lu
ZM9lgmY021TnKwaS9nfkZ/OoDpTTQFVEX3ZGmhehuFFfoGeIyMKFbcVGJ0STkybpXuBpttnbbzLu
7FoE1mDCGDBVCkIn0jnQ8SrO4E6YUoHvy4DZ7U7QOG3fSZKuU0U7QfzghkFARdUzz/sZJf1Y4ILa
hvvvl1d45321IrEO49GDpAg7UTOPLn9sCeO/6tcTv4xDJ5cndKr+jDlasUAWBYKAi8M2tlhtzNlc
VudgkWkmn5FWvvMoh1eLqA5XNzaxnsRssZxCRvfKccxeE23Kspc0QZxfA4uSrGX+09TIbQoqlo5v
vkOdPU0UFHU50kUydxCrC8E3FnGDUY+D9ush4MMiQXTuYw4PczLzcpfSfnhvPa3DDR+WvOBtdofl
Ro0WBDRjbBpV0f9kngJCfkSrzFVz6nWc/Lww8vbWZfsYI3LS9P+6y9E8cEKQ4YFFD69flP/dw2/3
zF8P1kqrwUu6bRmgD0aBM/VmYb0exgjPl2lZWL/1wqNlJq4pTycFHvwzJIDLL8ZhZChB0lUhvN5Q
wriC9j+dVehWryGflmeP+tGsVZ83zF5TPAKWRw6pTQkNC5xjjr8Vnrtn+X7ry/3S1NRmLmXaQ2Qd
Lzr7DtEuWfc4vhfUuKecyy2y+ODpuuRcXkI71IqlRzWpzJQQz3s1unupD0myxHpdhA22uLitWm/Z
B3eshII5rR7KOg7RLCy0tBvJNxk/b6UkjC4/w7ie2J9lzXcebIl/b6JgSSAHSOSBDEFt4j8G6STf
Rpy00as0EoJN3wAWrits34R3P+oD+MMRFjR3Eq1eJfpTkwd8mG2vfBHRS2TIpAvctQy4yCWSj/Il
ZxZmqd9laCvdepH+HmNGcFLcSKt7aCmESS8I1A5GtSajpLvaLJCjz5cLXaYvcfjnVzjvT9i1dOIF
1sv48jJ/WRwQ80UGGWyJm+qeaPamF9dBjXW6AXcK2VHG9JqTUR4i5Gb5r6LveDAztbx777h9DeCD
v1TV8X5Vepd+b0GlmHMIytyxm9DgXq1xwGhVL0ei7IrLENn3+G2Hch3CBKWfAOSvJdL5VKpZe5BY
Pn+DBVy9SWUKEUA44CEjirZjP9b2Pr+DJsrXB1STsZkEj3BGdH+kdD5LrmiDa/KDgv7bEhOQxhne
GkHzOYOkKc4F0PK8jCsoRrQabf+pu8jXn+lLpsxAtlkVUCRGDypo1UddM2FbJqd8YKBIlvWodjeo
KDZCIGDzdGmrGlV1MI2r/YWHx6wUvBT1slhLP6exxMNWwSf9dG73jxGm7Wp4o3G7ZTi8SbztYK7U
AdPV7sQs7/uO+5Sc4uk7yxdBTKg9q+Y50fwXM1l76XtP1Fjf4NRkYyjDeGx/mTyj37xYLa9guBzg
EFCqmBZwQDfcekfruKvrZdO1hQ4+VgNe2JbJI6YTcJqGA9BSa8Jn6MfaMakaBdP5TrrHQeolvZHB
C5Gs+aki8CtsgegFn7hDnJ/ZtzSmMmjofqjve3x2paTv/Rno+P9iIDUOKCxxj/G0JQxQGrRJXt5i
1Lw4+mfqtpLmX0+WxErHDzAlJxUG2GJwjME9mZsulKtMrxREHLzMKESL8sqhpOVIJHscqtdW0laD
9tSJNPz9zBXKhcnSP0gYr6ay4PkXH3euDONNaGYQmt2cD+Lo/ICcR7A/7e9mWcmrhQQTSA6UVAjE
UQLlhH4iWGXWAto2rvZeYF07tWxn2aXcER3bD4hzO8ho691pkaCT1iqX82RM65+QSoVbVqtCCals
eoaqopdXvv04s60hq2xGfi5BDfDb27+acw7RYRIw/m3cDwj8CN8FzWHM+no3Tz9NAjYLGr5KkBm2
6aQPt2MkeO0OPja/ahl2J4dih3MKVshrrF0AoB8Rd1tvjrK7Cz51W2a77nsx6H9J4FQpYJAeOIeJ
T2lQM8aMWtwkN/vgCYJc0lCXgP6OLLi/eez+ZSZFnHYNlx7u2KnYSOogmse4McEJeAFetcOCAWAT
1J4Dg1CKMM0gs+ZP5awvghGZIb9roS+CSTC/dsNpD970PEPjhhFxZkOUp8ZOKrqk4HaEpxOARi4m
s00m8KtCs0ROJpnll+W3OeVJ/H4Ne558deerBZxRy2/dmGr4KMQkLo9l8ryytMTxWNsFJ2yaGy8R
7D2mOBl5VWS4hFH3SFzTJzehh7ZlriyoZdEiuqjCt9V5VZVBegYv7AfVf1av0RNTKtieg7O3z3Ch
KkTLghZqJ9V9XvaorERubvfE0YPvWYlJK/Xi77Q+oFrRv2WCYe03fJZTcfT50pw5CzOFdqqHAzAS
03LuFrYVP5zZ8H91hmTgjFK/lt0LRQM8s9bba99N7FqGG4v+5BUGJl72VxD7LBi3bnKiS0sKh+N1
ml4HW4r6V+1PazMZLvG/SMwiHUAsyRBUoN3fIt6Cndqp+iuWy+6Q4+OIwnS/NaUrA4BtO0k5u2uN
jPepp0o1F7Ln2SZHGmuM3n+ESMp2rT5HDNTrojbdfh4nA/HYyIE18XoCRYOfuk6Ptefx9K1PIS8o
jqkz+aVdHIU0GQFViogQHLQoLo8aD23XyKAdAbaiq3SndUOy1Mceb4wcvHgXatnRdeF+CGj550+B
qoZZo0pLxCQ7RwcHEzmM0wVBuCgajicbVB1Zdau8KDysHxcD6ieZbixI4XrWsu0qISi6D9mWiH4r
3wdgf/zoITxKKF6lJoz+grDVLukZgVhBzEp1MYmMa2fu8ekPrflwNT+AEdPaY1vRGaBJWmaPp3gY
AKzZXKpQehhfM/p1vRDkk5flL56U90dR90MQkrRKyDQEv7EoTBXl3ne+RnrViY9ZjOUWghaispw0
f5fPeYrP6VHFWuiTBLo5MNjRRJ6ebwaJgOrAhgxRxp4hE6u1lzZ+FlWPnIPit0zGAPXg4n2RAOiY
lm5nfFPCZtIcmVgsR3lxpvGJe8yLDZF1D3Uc68OYvvilzfPKUwxPzrJ6cjE2pKFkMMRknaQbGmEW
H7an7w9cI6ZIaxW20tPZW5YDjjhb0sjui9uUxCfggwOvshMNUKcHYtVgGCUXPXSxKdMOX2LABbWk
OzeBG5LT4aS1yCc/CUubwoMWU2ZK+e8Q/O48aFDc2RpTuiqPRCg0nr3jHU1fDAbwm/N7M8DVe4dn
fq2CkMDxc5ujJVmXmsl81rO+ZkRd0u0Hckf5l0rREUUVGnPNNd7ok9zaGS/TnXeeYxwTGiu0AwDk
2wqRkjrdrELxe/I3HVWYN9qEtvx6ptVbplEPiCTwDBAaCYHWDnMzxaht/7omAPmwFfzULcO2b43s
RR3err3fHmvpjz4I0YymC7Sg1CR/PAUtj97omw0p89pdtLicxhs4cTwOC6nk+uYt/HBhJHdNXCwq
QwMk7LgkWhy41yjlG1VE/Zj5bxsUnd2DDcgcxgQjmkThKCRGKJ0YpdZ07D30tuCT1yJct7MBGCNy
5bll3VPXmeMPADajqw1OY2JNINc1SGGPHioAavPAlTdrGaf6J5VMc1VhqU9Rrnu1sG4vQoNclSAc
gEsXE2+msYPbLb0mtjuRlHxyrxHufjiaoc+5XWl1le24IJx2VrVlR2x5aO9jk0dMX0ZNCY03vk0y
t42D0pU7ZgC5M3quiYPCd2Q6Gh/2Y+mNGr39J++UOZFTOAwimd3MyR/DG3um8R6UomFrCZL/9rXX
zFS1wyvvSKOXEKwLjr4XkQUuCiTyWiHA64lY20tBcDMccQJ5N1zpcr+woqLO1RgezNj4MxJiVaiN
LmEMqANSuDGxEPp4KRutevtpXLFlqY6ZdtzA6olcb7muUCTRQFl9xLQMqQ6hrpN2/DpQBwuHv6jZ
7OTbB4It2DajM5Rq8i5myu3M7cK4k/aUK1GtVMYA3PHUcu80DF59VO48XoLYg86v+k5OKaMoa0QJ
5N7pk+UOl5VtC89NnuH0Sfn9tVWu7S9AixVpMGdmYvIOSts3bZ05OTZuIaBqOUG3s61jeb046LJe
6hQq5APbSPNSntLXE+RlAy1amVRxIhO8J0TXedQCGvEKsxrRQa8ckVErxXsPoEWfTFI1hcZ9hP7F
nRepHqNBclCNlTHSqmyOvwynFAabCNsl4E0P1HV78Th0be82xQYNGQnFAx++aV1uwuPxkoghhBMv
5OQwfXIcTH9lNerFCJHmbOHKFPOnUYbamxRqqe2beO+Og7kZhC4xirkdrZeCqhJyAMpL8zAY4ddw
hbldMEmiut4IkUwTYmhVtAnz6eFmvQcyMLYUykewm75RwkAvSYTc+rzEnnaINUW23u8nm7vjer/u
jqYy0uy7Mg28DXtdgdXnjCzpT3WLWQBsacTu0bl8LgCI25XUP6IILEuNS5Q5Xd3aOmmr+8HGfNZr
4M+JhbJTgq5r6PPxCldbNoyzkoqbrZkJRgZpga2YcxQ3e0Vkf6VmWpfZVVllIXJfqlbBSflg/4vY
+Hgc9z7zkLpw0KFdrAzbLKgZ9Y+bWd/HAXI2Vi8vUNyGL6wUKPG2/pyG3EpzLMu3gOEnFtqiJBu4
S7ciwCvtfRA0U85ocAP9CmrMlHTkzUfhXn3VBCv+rUZaVVRTEPfHknZPkjLSCHeEcqeodqfFeohs
mlNlboR9cdY4tP59qP0m+pKvSqowquXGMMB7qhj/ocUYMQXdQ050i5O4nL7MDtGOq9TRRr1fL+3F
FJgxOthk9+zUFMnuXD1aRPkgXzo8TpM9X5P+Omch/a995FF9sjOea7SAmloMVaDTbybofAbNJG06
Rf71RnkbLg/wZOGhbJ96xxA/Qs0hcHZng0ejYJnRYMmZgXlGP6JCs+xPbM7pvsE6Cxn1xNXk/aJw
vF0ueDlZZnIbzgTTGLNxXJbSgG9PVJq8h4YuhOHnOMDEJpwZvLVGVHIZ00ThsA/nVmQNaGyuofDL
dYrAgjaPgpgnlnMCjCaA3yv+khN3IUmHVxdYEXJos1WjsicgtG/jyj/DbusCWFTtO36IGoQd5Spm
EhwYpZIpULTJFtWBTt2HzJsugK2SX7Kb/6t+LubMAfMorLIJ9dk+EPDHagRQuTIwAGhPZhy7jmvu
HFY57/oQLgV7pSn+lc/Jpr0SG3WAwkpRVkSE5obv8ZjtoUUMiI652Upe8mldeJSct0HW879MIRlJ
obq96q55n3/u+YQvUp9L6muNCtfocMteY01mAEX8JgnYEazTq8zaNe/hAXv+TI53xjbpxBBMItAf
5RB6+QpjLuTmlI/stKYRYAJgsO3BAbJDZ9169NHXAO0cWRtnF+t9qBJpGKVHPUm0uwfhjFT6stKK
SdlSjQPMAXMo+W/Szthk8mkZ3KrN/UDUR+c2JHB+sCh1TxndGEWiYE/8ZfABK4Z0nh2oZk99rdf0
eJx14wQCniYlW9xsY9oiBU1dbT0IDdt9zl2jg8O5a7sJpdr9akXcA/arqt4MYZwkSszfxo6P5Mro
CWj3Ili27PlIIy+Yj10ptLZAkaXcZP/E8VtmdXhQBjR/5tPZq1+eCMu23dpsURtvv6P0GwIYn9tq
DLsL1R8whtDGNRv2PCVW1Ud33Pacpfd98Y2KB5yzey5t7RsOaCy5A8IvdacAOUdsnHHBU41nTJ1o
ncsf0CxMNIWa7aGAgK71aSptvIGHq+25SoK8w15dsAH+dDKSShXPDX2ZLAGh0IzWadMkieSoN2h/
yDELo/S3Uc23gtaGPGWyrBVR3NvPh+9tcIXmF7e7mbxMq5iYa7ezAxpDOgThRODKBupzJMr7nhRP
/U4e5/BUeRvoWiWXTaS27iR4EMrgi3nolmPX5uShxvS7By1akbhi8o0neq0M06zyF6ojzS0gMlit
4bw4UYMsS6uJ7gW3QVfuoZfnxwmYo1/8NpFmnSBnuFwrscwz1dG+CqsaycsNsYcsWTjLixn6azwg
wO2fKDOOx99vg/P7vkZfU245aI7S/ds3JZ7z1lhiz4GZBkfQ3A/LnquiJqzbvaExyk/NhfDHC0v3
TSS1qux3wGXsdbT1jCzMlpJ2xu8G1LzgmHtow/JhCb3Ue1LXtrOwJHRL1kqhDEzPyyixpf8CaFx+
gVmx1zJFxMCzd+FZB9PB1zvc4Pe+rJWNNc+ErpeIVlTOhPa8vW5532bjtG8X9tK5AAmBpVtW83Lh
T4m3AD+A5cvc4JfbodBy141LzXaqQERukkpBX8UXwQzf872IEqTA4EIcow236s/qGV5IkyAbZzmc
J28eKWcJhyAzt+f+iOuB7f7SNtmyNbu1pGVrylglv2ur5mJZgHb2ftBxPnRDJ3y0FZckv0P2DOX3
gB2oX0BJ/Zvy9ufERMN3VMr7443iXUKKmfxfUds98fkvQcr6jo2hAZL/uyvpYZCPBPqYEl1bUARY
PPQHBob31lJ3M4G1kedaC1JeIHHwqF7sHvPzbRUPpfelNU2WNd13SnmB60NihKRCDoonbn6oPqI3
w9w2pvDjM6eAfTG251evzLAQphEex3mkFMx9YNTAL8SXkO4s12BgmNoWGXZpXZavQ3p0jEbSd+dy
WA2i++YceCM2nNiNOsdnH0ka7L/0+7eynSCYGHOpJB4/rco6MNDsujHka9nOWiK/CmRHXAQemBRV
aRG8ZwwC80C/4/d9IS2DXCaevFohacAf7ivcRaGpwmONBlze5f3wbj3rW2glabbIXxLl2Sfxil0k
E58/AN6JROB2NGkZjx4W6lxqHruRttetuv/uh2thYQtImlapkpM+Wa+Dc8jlQXRg+jlFv0wj3XuP
GCoY6ADmZMiLEpJ7PNurMMhgI8Sti5DUsSxBxE+LGvZ3EXqgeUwXm9m6W2/pU7hZbGjrD6EEyg15
1Fx4CnCnAwuZ0lZajli7nLasfDmFTaynlVcDPcu2KDI2c7El9daqW+V+JMObtf0ksb1lekwKl8up
iC3MjqF0/W8a2tosZVxusvIoYaMnYQAAXvVm/61S9GElBg2amf/k5qDgrE3Ga31lKVlRQC894Bc0
7+P8QUW0AWso+b7LOdcHVb4zgeKdA+617sqlnZPSHm7pW1ocom42s9IjJLHXlwtORuLsO7Lt6D5y
2jWQ6RY90zmzBHbuPEr2Qr+Hi6WDFHFFJPzXYdWtPiQN1BhBgrN7xAy41RD6gwXJbfiT5nYUy1z8
k6IpXdLHAXREfkgLCLhOkBLYiwi9+Gb98rx6U8iWUcb7yWoSpgPYwJGFgH+RZhwp8YYSDgubyRB4
v7Wqp2M86hjxYLrRhyI/0ClkxnZXxVNTvMlMOr1mLHApFPzHAYso21gSNOlRAaR2pPRI0Mtp9GCS
ceU5AXdi7Gp1/yfFBySSUQe3qV7MViaebRq2ZJ+IrGheDANAOhRzmhuwvmkfBVgKPGg0K7bOqhqY
W5JSgZw26olqVoKKOXhcr1mYiGa5yi9sVIr41353PCu3dPKDHhJpMfJ9Xwu2coxanRCGG45ZY9MX
b9na3f1ni6e19ZR27FTW0dixEzWpRBbaFKKBg/9zQbjQub9+kyUGQZsA9qDezJNm8hv/RbUhuiNL
AX89U9NfT+/QCqh5rfrpVB0qp+mRdH6Q6MvCPKmE/GRFOHjxHnWrdx+xMnzSYnJqLw6KWpQH1/+W
ZV4hMeIVW62Fa7bVA03EdNagbh6bPMhwL+uqEmGFBGji+o2tKSq6HLdvhVK4hmKS3mk6/WDuosgw
pJk0BgExDBWls+QzvJ2xry/wol8lodOT+lztoG/VY87WwNh5gnDMd6FoMiNeeUDLivWrAHbcdhm8
OkU8k2kZmpYMAqDMnLgV4I7TGABWMwqcZrwuCIebsBF0Ej1Ncua0sKAjGs19Nke+MFEZgiKgriUH
76QZq/3ctt/8WZQWD26ozgVGyU2MBGyfq0b07v7NSWySkR8Zs3znCJPPmD9ikTDavvb767yj62fo
Rksl/NbCHKZGjFEyS0iH6WpVgUSyCZHnoMdFU2h5w5cwVsGCFuNChoO+FooS3u6IEBRzkqu4Js3Z
P3uip/KyDhoS9qCGUKxF9MUiiMv03uNk4yIQJ/rdxvr9kq7Rs3dtHf+wqQtMbKTAvCM7OwEp3ULO
0T9ZTYaXyPuJRmQ3AFQ+f5KMsu+uSz/jTR6dShMwUr4ALZ2doDFgGPlPwsYRuxTnqDI5FYr3SZA2
0Mc3QgBFtIQ2NkuH8adYHe9IMrQRsGJN4gZn15JJJescszo53XbmQx6tLxHg2EjMdoQ+JRPv+7oD
Km8y1gDdEj1AmfgPtAi7HyVggDBTtB9OcAOuHDeOL1LOm+usUQd7SpU0tvG9TMwF8xIkzD/XDBoS
eb0EdfcBalXM0TmfceyJDQ+mzRaO1sHYXnoE53aGpQ+5vH5CRXa1AlFrn16xQpdL0QoYBVEL/1zn
w+OZ8iKD+AVMtbsA7Cfev0y4Y6LcizZYnx6uQbtKtLtgMRHFai+dAlVvmEzirW4PnLS4Bsk9F233
rz4ut8PFxVniLK0KnQFQmD40rXb5R3KqUl4GA1pwEYWlvBhpNMWwrAXvj8uuDSCK3iW4Fe7Kk78t
fwEXDn29fiONQZAZm182k4PC5ds36kF/ulpZhebnVJI6WJk5Ig6vEfcpbidcODW5wJj+q04Ejtf3
3lgiaWLjWVlQshPkJ4CAEGdvZapZbTOTkRmtc7C3rApixo+WXyuaaBEZpvmZMrXyOBpn5p4ObxG/
N7cHtnENlc2o2PrA9JGwfjZ2OcdTCLGNJ6LTmLyCJeiLYY3tOEozqHG1wfmBYUNLsOP1puKKPVSb
oLZ676ZIzb4InT1UJQGWL/lvkcguCg6t5eY717euVY6XhdLpodAx6u2j3MatkaWyZWQjr5bATHtz
0btq50k40cgsRGjY49cLksCGZyaViv9yswkgyCMBhbhT3BhabinhpZGjvhg9R02bFNpU6TqiXfNa
Z4M4Zfpzpu35lgoAAjgrwY7aMCI/xeHE4tWNsFRghuHF8atdx2qnWK0701Mki0hU4wAm/ELZuR4y
VXT3zSv1FbZERqSd4polWHtYNHyOpwnWwlBHMVWtZ/bULsyGCVL/65TfwO2CpNvptJvKK5wpBxVp
4q52vHtrqxNaSUBRFTb+Np0u/S5LA4dy+NVrbxjsb8zrLaOfLNuUED02jepNWRXDmgraJDrR39xh
fhw5WW4Pp3FMjaVv/4S12tD4yj4VdAmuJwReiUE3oqUhd2J/VNZnVZqW+qwvW4Vd7g5c/1IGglkT
1HBmxt9etKKiSMf9hEhk8Cy6q+EeDFZpuHMQclAHCUXq6DOgsIlbgP4vNG4VoXliwfiXiBuXQMTj
ImtMRHNMsbkeVrcmBaUFeOF3CyoComLet9/1Ng1wfTIn2He/4qt9SF50TLZ1mD8XwvUtiAGD7XAl
ZJTV4bSgPc7Rw+tt3OwthTuCm3JQdUnixCPtAM1WKPSPkrP0/xKXPs1FIN1Rz4IlwKscsWUOlUJf
Do0Y/mHHN8OO56abxp8nF+3NzmS/ChDHembEtXJRI7jCaM7zX48vO4AZNGbrb02aPTTV4BVGMwgd
E/ltvqwh3TtjdKywMIpX/Tn9Z5hUY/0L7FgPAet3+17kU9MUdBbpNxUaZd7tQh9Ltz3z85EZuE2B
hYZnZ8i1Ye+UeseR01tFC0GE5LdGmU7DIWFDpce8zRP/DCE5m8dpli6XN3hXIifin+HiwqKTstme
HjH+kK7fSMFo57VSxaQ1qUkF8H8YcT0G0byKdj7SjPGsYNY4+VKMmMKt3JGcPSxEcugLZTOBZBI5
kKZWBloUWHXRQEIOQVRQ1SXZ0rZpTXXyMw/szZIvHGFUz9QvCQbx3HCu8A1mODw1CbJZ3JaVVLBc
I3RTljyUdfy7VUdBBeTayju3CLnvUOpS4yuFkl4RjNqNAL3g77ohD7M2Lx0ypeIqjncsYjbRrXrV
eJfFuDlN1dfANgPh48451DDcYVYIapiLPgIxriHkhs6+U+0rdlIzNnjTjbVw5zkWcgmv7miKfxV1
4QEVu4HRclTq9AN7MgSArTFhX9t5KGBqanuY24oRsy7g5hgpcCWrbv8xaeX96kTmFp0Zjis79ZE6
q67UG8ZY5zt6gDNF8sSq+s0rLHCmhh+sAzATKSP0ZqqD1TdO9yr40SSF3WW7uqm16j0NPZ482ZeN
ph7tMetIzcNGhZ3jd0oS/dlIarQNH1zGjW2QxOHbn0H/0sZC28F7AFh5JRPwLmM4xGwsk08SnNGd
SERbu4s4GoLqQPfq1t6nLLPVborWZpnk2TFJ63lHFJLZ3qTnadJrctNsphNioJ0hhu+RpRq+tHH8
aUJlcGQWRWw6+Zkx2tNZJdr9G3Ha3A04RaVkSbDXsqaENw5+Gor1Qc57mspZwRpA6rxN5HMeoNPb
UwoqXTibbFcNV2/WMGL8tgCkdws+ti5Zr2C711JRZCDQAMjCtd4JtrTuZeyemjf+LK6t1gLTLZFU
FUBpKhY62sGPf7tzCS+ozXIQkp3mkjalpA6eJAgGolfqFeOXDF0tcczPmmzhBZOYZA/0p3MYcuLM
zHBCCVr3cUYVJny8rHSflXrDbrxN+ReKZLBi4XCbs3JK83BQxaJtRD2cGJkdaMVX8eY0J0M8cMA7
g9BbzYl2JrswME3haKc8p0ZR57ZPbtwrKBffe4nNEz5fngol2SuJdH+WZRobi8RUbJMS8RgqBtdU
YCc7z+98fNR+WBBDrls4EDjMRfn9Kdzoa0KCKs4OK+Z6CIyC43YqvMihDIyYzI6xNE9sukmCXx+x
Q3VozpL/608EnS57xlGdCdRWZ386P52Yy6CmUt0jomZc+UyZuXP/QK03B13O+RMnnWQjM0ENnuea
AmAZPgOxFJADku8Li3nn+DE2uxlQu5yQ8dW+eP02SUnuSoSr3uUBZBPXeW9LETcx050hXXSQaclv
LRVLGZcEOvO2Y/v13bT6r8S5+WTlH/RuoA7Wx1M7gR/FCE2HV0sTMveR5DB4TgkAyJCdf/oAJfnJ
Swdn+r4nRMvpOaqJlrMZywcLhNFdqE6M7w1fNi5TpF4uJirVX2PKk+GnguMMqakwWqdGukgiKdEi
2Dcj0jLjTzNiXxQxcWhoGwNjIp9wwnsyVCevsMMeS7PQGJWWUfvqFYY4gQbzq0VkXxCU7sMZ0W2K
GuAoo/pe7x707hmXdN4beldAsgMeseeSpbGKSpb+d2CR89yR85NtF/XnjBRfzXYB2CK0zL9PQ56y
KjYet67dXHKSL7xj4N9r6MkW25aOgdQyig0BmcwPzl2YO9sdaU5MHXq3XwEtFoNr9cJmusiWZPBw
NL5MyYKdQzzOKjh2LLrEOZ2Ap/PfBzd/Ss0idQdQm3puDkKE05PPB5PmRCSWCENmOiw6gKwW8REX
N7VSAhgIz4bum2rFnSHNlsVhhz2tH47z28Ibq8JS5LYx9znsG4R+Tlc4g+AQ9kZ8hPleif3JNy6+
w1ZXVLA7nn2z/kBJKD0QScKy58tIDaL3lwItwAzN0Y5lZ8t7IX2S9w7VAu7fkhd70djTpoHEsB0D
LhNRCk36ZbSNUObOjtVEw9ihPuJKo1++R5FHLNB7Fpcc1QwmMZp5W0XgSAi1VLqdx5pRHeVOhwDX
9WcINECg1VMmEr5mGYA44gUIVk/ZNnfzir5s44cCjaFTjDMjxMvDXknT773dSNmA6U8m2CTbUsHk
7w5rKSFNWWaLx8m7oCDI4MDN5+ehAijlUXYbHBWpck/vp4kxrhVacPAP2QbXu6vZgy1LqoDem+LU
pfQT+Uf7A2teaefqjuccMHFf/Sb4QfwW2aVDpgvHkDQUbNEIDKby+qzUzXeN1xQqunvLyPgyOJUw
6xoEHMaIdfs0VDkIciYxTYp8kx2akI4Nl0z+r+ydr6c32lm9TAyiKi014AzChhAukzVk/ZnXCbDu
8CDTXWXxiyUFj+q6VkL5odgkqJg7GYIMeN9WiwXCg8hfu536CTBMtcxUX/0hxFBEU6IKNPmVCN99
+a3ljjI7JaeSDXZHv3Qb236hdGiG9T/+5KX0i8wPXmYpwVB6ok3zRz4Uu9RaWNJfNAT5JVWu0S0B
YZ7yKPAxWOPtSH+dtryzCQUp+ArGADGCDmzSd/334FIzQzi5lJtnf9jYFczd+KnveidikwKunviI
0BwX0o0DG/nkVeLpCBS297lTEEzRCAUMCkiel5/NyTXbujyXKTf0Nsns2HuFY37SMEHxbXA1992g
lWs1qaryOdWY5wUL67vCD4dAUuDo+Mv9PbBKPE1JAQJb282Iq8JwiPAKztsVd3ChgxYNFjUCgNcA
OjCR6KhHt3Xy/MKSimOuo6KTS4Z4f9SDBo4wjIIEfvEiqGbrCqk2uBQE9usgZQX1nhOlnshss+6q
jC8g5LXxNsEE7xpzkvYGH7swqdUjOlhbzf91CUZ04j9aZyb31kFBnbVb2tN69V1ejqG5g3TIuCsZ
K1/VbDeSr+hQubRuop30DZhR/4T3zGcdtTnIEJLPb9v1EQErSB4Wwg1gxGpqbpIHZLp7WA0H0/Oc
G5Yx91LX9O8Gd3dWm6DNiMk3fpwrP8rnUQ+KONs2gG7maQx7RDd8ipWnn02bQ613THiry0acgk6S
pP3TQ0B4IGuWqwwIXFBQAN9KDb9E22uxOfqrX3STbfWp26M2JwxOdwq17zkrcLGNKJjCDSBOY0On
tQCZYa7iRYwMaG7NeSh7K5y4pK6BIjQbg1OmZfgoNbLQ3NgQOguUoIiFrlc9XYboTurLX5ZMQcbn
bx2nsRcHxV2mYc+ybTsMuL+cUiwdDknwPekmDvs8UebAlmhqrpjBUQR6eQ17kh+CMjbG8IxTXhfP
QxNs4mf3JA3QsWNz2zET/TeavlmpmkRPTHuBPBCtwz4LNMsvgfzfIj9ChC4g/3lSPprYNE4j4hSw
pZ7sDODcCv6M1CAJ7lwMfAUcJbuvcbcGyWMDkcmCpj624qDOv1+8XbzhqNThjaiVNNNQV3qr3cBY
MaqMeiaZzG+U+DpDGYVCdAI3bseJpx2Xdgbio6vutrxxBhJeRDlUo8P6eu0TCAqjTnA5QsqGH6n/
+uPSkpvEdYMLnVJDT8RhJQrOOpGlnpXNlGBClMAR1/Iu5MN4nWms68g3Gn75p2ZYsULwpRZOTs+5
EM5q+vJCqpx+a0xae4H7wajoL/6+lKnr/KW080WZtKCMxsmaqrjwPo8osN7yw1eKs6/zM4xKcSQg
r5x54Veozzxt/pJB2ENmRhGOCqNiUXb4EPjkVQjZIKxI5wOKQNBHkdquA6+di5hoTL8F4nV6Ogch
pKt0WHHsOC2wj8ZHhume0tSTzsh6IaZSoCUdY1ZL59zeM8Ye4/zaP1dAEtUl62vp28FZcbVWJ2tL
rXZYKfMkpb04+4kSq0GrEaU43P/Djlf5PKG/OXLZteEHHhGsQIvIMOQUUOnmkAP9xLo4ec3KIuK7
2qPn9m7qoNNKLeikcY1irSWHNcFSnp5W4R+pGxpM2nvTRqFt4nePBNoNkjR8ioKnjJpFHoTsq1uV
CbpD0VD28obKgUky81p9u2e6NShi6EyJuya6ADyfMmvoetCp/xIEzZ30nMVw6khPjyuDbX5wX6z0
PBZDu2RjFKYz/xq7cS4iyG75Mq/T+yLi2LQDBZlfXbyPZQENTB6acF1ottkzi2LYpxDxGJCCNYRd
pV1E43mO+LQWnYoIAcfvkwGALvEznqtPd9DVxCIG4I4o6qYFxNnKH2EKlQghlzrbY86vA/uoIku4
ugbSuhpv1UxIUEZc4W375XXh8PtCHQRDO5UJxuN1FVhtohm9CYmC80Un/PK39vEAxvmC1UB0XPIm
KrU+MDRin60g1+AElAKR3PuI6hUS5q3l6PfpqQ0xlOg0D9ZOzaBfhvssaNlBpHf5sKUuyOoLXN+0
5+twpf5LRfxJg/71pwKh41YPbZKBBvoGbGP7ZOpjNhB5NuNpFjezwscoraDkAGxTO1ImHBxzZJFG
4bHAFiQZ+sfxHb2lYCYgAwbWXaTrJdoroAA/5XBMOEMWicMTpAUdXTjxUpBPrduNGC1w2ki6hvfA
WIj6B22IxtjMp3S8odWelSn4GsXOe5tYH1mTJhCaoN4GuUmCu3XaIrFXHGa8B6yIdPpaPkACk6az
IkQxyOUr1xj3MfS6aNygprlKGreUb1279bKaxoifh2Z1cV02PY6mJyOGg/T4TG9vh/fZkVWjeS3r
Jb96ky1X/+s2XWeaYOj/KxTzbeUjQXiM1H1kgAy8v0zHAwo+VImX8FGl8S3c5htpA+T0ayrhwJf7
o0qMNqC8Vb/Sp1i2aZxmm10ThJbpXnJ7+EF9XDjab4iSkHdyx3a4ljRdOPHvR22TkaQlcgIXtw0d
bbZaP1rlaNaevAeWnnG+6iHSdKXHMgJbqVOvRCbswSx9cHwh+arwDZEc6G+iTKs8Slu2V2gsRQpO
iXXQajDUnF/v7V0iaZ2FM433TayuMaI/9yFAPJ8wsPjI20AMeliDoOc+5XS8aReP65f2nt5mV+0V
TgMmDhFu1aYJc4FguKy8nHoMV2x5t2Ln46iRSLqDdUn38bp1PHV7/BjcDAAk8LZ0QzJ1G4Zss/1e
GcCsj7Tniy1lAuXwBSHk4F9N5qj2Mvd7DoXqtleK/7QKrTBXKSGgs/T6McIF0XJllf+z2EtVtysC
iegBmNCb9KDV0JN+1FbfSSA6/sqzW7RnM3YVGvJqUOSYcNjjoiRFpf5MvDVQTSBED2BQrnHvus2d
luRhG97bmKQtDbOIikWBIIG27b5sfKkXxusejam61LXco7izHq3S6vh1j83TFiFknuD9W1cuOsz0
ZN1EywxPjh92T3I/MdYx10jKGkO9Z1KKJ16FqTnPKKSLcP8VLL3OLDnr1YDUcMdkENdXLGWHspTJ
SSUP+GJyb5b7L788PnmniIH3hP5Bg9nu88V9mipCantrZJIwxjAfMfKge93b8hV8aY9DSenYRZIT
D1X5uuToIqApGN/njJV1tr4Eqz8iD5W2zJpSGvclRc2tSGH2Sxp47YaZv+x8Ajv2yhFqf80wjI8a
IjjGiCsTjhgNeuvL+CkSnFqZ8O7Pn9vqlm+493jYwLOcVkTMwg2ItVLbo7BtV0lYz4coJ4EqMCHQ
75ja/GBVjmdw/d2W4Lu8SuWENcMEjaebd7Z5yvfUew7ZgX1By96UA6jOPqMlGrpp5OdtZidGnl/A
lDUSbmquJ1FeA6V3G3I8F5AolXOQ+E62dfZsP68qVfKGon4oLgQYKlvfzeB8Ly/T6UwnaUHttLvx
xNCUIqsTtdjyQ2tXS4EThPY3aLUFbRocGF71FXyBKfualwGxamgbmFJC3hnsKBdTTKPG8cZJWk6P
jvR7ujNm6WYAhRIeSvtaFZ8XyBiTd2dEEOIxpVlFj48bMMJjLIaFh4++KUV7StiaxYN7Y7zW+D4f
rFrYwdOrEeGzxnU0+sKBxr1cQoXz+lmqRD/9k7ZsA0YgSTepTS7H7Xsm2R09D+R7my8swk0UrVkW
GfPoqWY7XmcPkd3+fRn4OLakLh1z8h8d2dXOSMlwr1dKJblCadLGDpLDRJ8KNFZDCM08jSQRaM3a
D89Q891/bBKivmbzuT1Hga6Mq/qpPnOl63Uy4F57xLcLnkHxO20AN6kEq4EpH82/vf6MbjMfQWes
0sqD1RUqAZSqHhDFbhxFInkANs4GoNn/gSxM/Y988opyEELOq+Lq6n/w9mZDnnskfC0wMZIErXe/
TmVuozO8A14SNQoo5sKdUaKX0hrPDNVsxI2fYuRKKZU/1J/PYyFEehe8TOpz6u56CYtyO4Q/pXD8
WoftA3vBaCwKLHBxgugxAqQ2XPNfovuQyaz8LD1GGdFFK5C7T5TV+kHULwrttw+Dap4OyZ8QOa4B
38Amhmg/AcjAkacdMVXmHjhSeKvuJIMZwNDa4risS31+euXxHSgX3o+qYAHcOVoqoQLD41GCgzbC
mtQNVNYeT4EahDzsvMED1xQpiQ1O5OA5n4+nJg/zmoUplZkk1r3BCpq80mD7IqN0bbTwPHjYs/hV
1PRNZca/xKkEjtBT96x91LsZx4ZwHsqYtmUaPo2U/loMf+v/wdjz7Cf55hrZ+htXJonBltrgUKgd
8rqIGuQ/8MbjpU4Rh9aoMP2P3gznUOBGQIVG1WMn2F98bUbzjr9VGmD5jNcEw8LCWIgGmj6y5Obh
iFFxgeG6sgqJDJB4jzZh2La+r6qElcoL6F3gCI3q7Y9PU61f+qBysWh8BAWPrlFRiRq7wX/UqQXB
yd4C1DEXDAuLaeCFfgKHicyBvgOaBF+FFdqPOMWmLIx9+6CegbmwD0q9JGhzcCQiVSCEl26OTBfO
kZbpy7wqeBViCIbCaLtDB+OcZZYmxDifT2u4wMDQWIzAv8Ht+jxmpyfTgX8ZKpSwTQWCa4kgZqr+
Wv8S+1OTREjlyaZJtKekzR5i/HgcLN6CFzBcaHKtV8xtVQSBN6I8+qpdQCPWPHcjuS6zdz6xxaLC
wgg3N67YjMURpiWOEGv+I2RlW88/kyUzdMHzaMV0LVGyMbmcWaEuamaPeZ/0oKnwX9QDcWkkSXL2
WJcGfGL5HUIe0PJFqAxMgygGvRohgONSXFH410r7KXjzITndULyNhbg7BdzjXNO4Htl52TdiQ4Wb
ex+7fDgQSj2Enbn01OgdMRxkp04Ol1ouDyYpD/5PzmIw5cDaFExXQ7INpA5y4D32v3GMRvmpMlFS
s5QsyYvp6LnBK55a9leuaUjxUXtkcuq/MZhV3AzKgwfbGkaPBeAEt5vBbr8AFs4fIDELyixfHoQF
sMAOu05hbrA0fr4hu2zUZ2Bfo4wPGgKM8YEWz7j2wyCkQ2e0TVgWny/yRl/TAJAupQ0JnMWCiSQn
5ZKcT2hXPxDbt49sizAnfG983oXtm57eKreOvZpUSH8pR1bTqc6rF7qhyfGOUBKgqQQDDzu0y9gd
Nfwb5GkmxjQEWc0Ina3Y8Jzqkogx+35MB7FD9iBMbnq/estEO2QWSHqqKcAmdsvswQ75bqJh+tYe
MPyhYwIIbypTRXKd5nkxi3X+7fCnrPcjpQwbxBUeJ0S/GwymAn3spjGOBT4jNJncHcByNpR5krZ2
m632D/PxkwxvUA3ug5iPvFLnkASlAZqcBZNrHVbiKe/yxVr0Ba1l00R74SWDUUJf+sxbE2kgCWU0
TEV2jDXKxa9R9cD0aHLmC38vgcD6QXdlgwDn6NEaB23Mt6iGuZe3gXMnWoYWQGoLE8DttsuL9ENG
ypr52WvBSzoCe81sIEpBF3F39EkKqRjwuOhJN3U5Q9SztuqkwVen1QGKNE9lQ0I8C9Rv/tniaxzx
Qi9rkRYBpEQ3/skTzku04FIiu08exgcZYUpvK1y7CO7KLp1BAvqxNa/CuC0w4aosQ6e6HYag3Re8
Wa33sM4plZKD8gKF3o3BdB72218VTg1mCWwQBPadjo2vH8sVjR6Y3x22qYF7wTACflxWGeIB8sI2
uI0GI9OS2SMuq3yfuE10fHB4doF50YNtoWfPFS+cbHS6kMnUk5C8baphbOIYeRU87re+tMvuS9m+
C+U9mw7acVS4sAGujwb9+I+1j3dVK8SYjwvw3Q0GV4xc/Iku5kyVw/HwImJGcG2em2Kd1fG8Qxgl
OyZTgSwlvzlnGKB75HHO/wn7q+/KhDhUKLfeuPm1nwWwZF54TkEneNPbZGRR62AdLVd1+i9OLoWb
WHlbMWwOkejajxp4DBppbQ/JYTn0Nr0BjQUpHLwUq0WonpOBZvetT90xwjBglCF+LGR3EEITZsk7
8gc1HPl6V/bMKQj00FRWZ36WeWCJ5e/xXWhHJXxW3bqNxFu2wHQyclyjMjlKHqiIQiSBExspmNQw
lTzzMPODGAzHyAT9l8RFOpYHWtXovf5WF83Deht464ZFEbPcdzhvs9ndO4YUOPEoVnPTUGVm1v/Z
oOvPe5yr7wSFVl0DiSmkVw+4GH1ft8zSbgHxpT2PTvLyWAo3LGLiIrg/DeeFiA//2f9H78a29ljH
PcI480nvaGDk6hrglzqWnA5WsCxAewaZpNMz19CtDa0wzfGhh96Yzed0N3nC9HpAhg0w1VWXI7Rb
hfy9DV3SRgd00L8nlv/kbftYiSzV6EaPdO1LZzcdTFOlRTe9TooGo+pGnnlypQINIQeNDRxOxfpb
RB0MPhiXsSL1myoN4bluSOvkknTaTAvSvNvJqeQM0B8dcLNxwTWYH6VnVGds0vr4OcjvCJ+MwJdO
p70lMS/38oi0rIFwciwpCtn7PpVps5XWotjeuifdsddMGTG0WAsh3CV6WbZPk1wTahKNMb/+dSiK
e9vjBiyRC92OF1ZPaIrmRqLBQxzXq245f7CBR0TUj4rwZ2IN7gaqord303kxpEL721E8VhaWy+p+
g+FBroazcIyseKYD+lJUBRaWzxpMuq3fLPtJ0ja4tzqRW74xSDwHN617/+Y1Hk254744xIY0fbVl
ptYwJ6H9ahUIurN6i/uq3TmX5OVVbnpS28ASavjYaFSCRyso+Almjy8GSUFg5cVe8kS+Ne4wuv4Z
L6JC729HDT9WtUo44jARjuN4vE3dkACKIVsf22xMLzHFdFbfcmKJHPUI+wkPeBjD3wVv5uTDi630
r51mEE7UmAqQvvw827+On99w1h16T6F0qaRHhjHfHBWZX8u9/XagjfXSSipxic4GM163nusccdao
h9Wt5bJVSBIrp+FVgGr6k2hMqAB4TPAWGAEaRfqghMzOD/KyeU7Jzzr+mIxfAmmvv9CZ6t7t07/Z
FpQTSGEsTOKduykWLabGOPckB/dlVyiytGlgvTKgSDTWarLDW1VCTgR+3q7jb7pRkjaHymAfAHhu
9uKKobI4Nl07ujWKq3oIat0CDXLx4Gi7xbrSBup3KMKW74+tpLKIHXxhcZFnUrYUpJjebF3edgBx
lNjGN1MbdZ/YZgNyT3lzRQA6hCYHDxpvEfSWoMTlk+Wr40z9sLnurst+NJTzVlkfL2pAbLtYOE/d
QzHaC3XfKo2ACVOttMMYbIk9qnVdkTZ0K+vQbPmF30N5rL03eA4ls0NwHuBz5aJ/MiwJJt2oDe2l
GMaLctR7bG/MhqL62hodknQVut3CGJv1Xq85zkFBoAQ0N64bNJxHFaO4btiyG2zBeC3HXGpMHg14
09AS/+QfOj0Jgwdgk6bFYCn9Dgw0c/7V6jVZ/2uejq2vHIf7Pju9L1W52djfqZaMQew2N09UXhsz
ohsI9EUSiaUn8RCx+UnKu8EX/KDlCQhrWiu63/JYnTdfxoVLiEtRsbVcmLH4/Fp9mHxD1d3CwbZo
LfCBjEJVrOc/6Fpo+8+k4WVBqNaKovmCnoBK9fcyvwEB+AydPU3QGv/WfOOnyIeZYS000280Pziv
3IG4jViuwFwbc1sFnRlQNhRVoNsVJyF2XDczIHmGs3F/PTRDHSP029Lmhvu/oednZz0Pm60GaBh2
+bhzZLNpp4APCxWbwfl80Nfx7TyFicLoJA+VrT/+YtzOuSrBOsuAshV0JmkAoG56FH6kvb2PYPeS
itzWMJBcpyEPLBgZqmZPFN6J7peVcdpsnqiL6IqXyRtC3sbg7a7l3fci7By11O50kDkHJnXhxnyb
b4UvrLMUcWXeZFiUrlf9vBakyhDwzf1E/9t1VbAlNrTqSMzRMqsqtg+05ux7sAvEAY+iEjlTqlCu
SMHb9AWoG+PNspNj51Z17TQ6lf4X1Y7CFgsvg4q7qam0yyytGpxZPNB/lX5pKMv9Ydma0BOq4Csu
fDvkWBfL/nU4X0RAPX0ZDZV/fm4KI38u+nLSiXdYVzFBVJsm2g5RpC+l9z9KZ+R+KXruBmb+EMpu
Jg4Ab4Lw+Z0GMqoMM0CAf3eVlLtYl47V62mnsrK/gZNXTTYC3wcy3H4zILjGC0xKCORyaXyEt1Yi
zwAmlHRKhIw9WI8HD5OsauwkKXo3PEhZWkmCRKNobce+XTH28hX/AfY7XGR4YZt7tHsEmr0jck/N
TxjRAUXz/aYbXoDNh08TmBM37+RrEetLJBf56fVD0PnsnE2IQUM4nnp6t10Yw2EkweLbjUjxFOcW
ca+nI7MCD9o6t7BDqjUexBppDFGE/mcp8a3l+KYRWXatdQjv/J1nb1J+p+qVWh/1MFbRRSlmuJnt
9PURQtyK5NPMEjqi6tuYKndfyRce1s0tOlCovKrXVPUGVkg6H5lynC7aYAsU0Vk0JI8C5HDLyB4f
PGxWJgIWUU/d44RVuqL/J+cf3qEoB4Lo14+VTRmlBsyxZjQksq/w8mFNW9iVhIwvuFgT03c/8rek
eFW/lCJwbpTBbWBnb3c9P198xiTOLSz6M2aoUjzWwodKBtaCZvpCqq64przPl4keFS31IzEzIce/
Fa0NaqoDDKV/HhYIk2hPGfFyDad1rbtC1PAoSqGlCPlGdlHYIUi9DbgLe69EhOvOF1E3Au6NC5lE
Ouc/RaLBEpfINkwBdGbcpLrXJMdWDESsN2giz22DO4+osBpQ6rvqk0n6+Yn1eO7StNgXzzcju0Ob
G++7F3i/xX66jkCfCbT/Gf+YeoRxeJs/KQoWfXFbrkLRSg1mWXRJwk11b4b2HtZ8Egg9LQiql9zB
jasoI9QBxWEkwpeu32+aJuXPbSpSkdBGRkl++Fsyt/ZQgpfDPWAxOPHc/sghbX5OfuJcqrEjQNlG
YHus59Lc0fWtym0KE9zoVbBXecrgSgMT0LVKOplFKX8mxZSHd1SR+rmjFp85ZbdAIjFabvinZGLw
td6tj15I0hVSjq0Cpvfu+VWgN1wXUE/xhYZsxTyml7lCsItEwsYANn4xNP4vWu3ykNpGFz55uqFy
9Z7rphEaWzE3wxNoqEp8y2TqdJ1WcYD4F1VsVPAZx6AOdcJtBG6kpN5DR99MPLe0dvGC8iJb8N2+
sLLQagB/SRyVBx3Y9FLELd8y0eI5HuiZHQPdEdZnPtHhGxFaQtNIyejCWfF4PzM6IdHjPG9wbxFl
aGdm520FNq6I/+cjT2uvNOYWNh7yNJfPQPkbsdYpAq6ao5u7Xw4qDz7Xp3zVAzlqXXAHm2/FtS9c
giOoEYLFzURlud3+fof+L8dbutceEiKUVTccv6Pl2vIws/CkavsBP0ey5CsDHobHOPcwqlv81iD+
aE4pbW/5nsGw1Ae4GMlE6oaCjCF9gSkNPJ00fUCKusuQ7W8tNTvZuRaOYscj6cr4owk01woG8HZl
UOK67MrMf9pLkRRD9sb/7rp2OwbnPdXNoXm5P5LVBXiFl7L3dfdiyfR6vjOALPL0G7tz/w39kVf2
JGTNTU2oYFJGYjBagrSUySs6nUDCNIsVqOSRcl96SJbKJMdIORVCjjgtvoDLZPJ/iWEQowUVlwCV
4CV148teZldV5dKCVc1iT6LeCezJ4tg0L+L8Da7Uh/drmPCw8o+DQ2gs6LyBoMw/gJNC14xXFps6
dbFZ76kxyRUzPjnZm+tpQ8I9cKtAmrysyztupVIsjmRVuTQpI2l3a6X1A72W6FE+lk9CwiqBTaT4
06Q0dtjBJA95x2ClspGOnm+cBo0of/HnDGlLfh3D1OQru3A+CprThF6uANYnvZT/oBVflrmH/r0U
jdxcrLjtHM8qrL12zkbntZ2Y1ZiW2ieeFt+SFEZQTZxsWt/VKiAYZNnc4pjFWC74RFK9td7CCWfo
4bLzJ8TCVPXTyJY+ymUIMNuoPqX9NcjoJSapdXZnloRuLsa4obgpm0z7l89cyhHYL4QR4Ag3F2BW
OViTmuYLWMqJm1U22t2nzSjAf8MfiffSEku/Ra5a713SXRyX0UlXQx6OQ7d1FVNOJ6dLa6KVdRPo
Vu1W3TPBmJB/EddJLMcgQnI8GJxnVjS7ctE3L7aYL3Yi1d8P7t0FpO15Owzm0UASpinQs2q6lDSQ
AENKFydFFjN6b/dhla4HW+fzGqgiAzTd4RdDBLB/i9gug43X1ubM+WdtpRK7H1JsOPlLmJpjbIdw
0DyES6GnlwOtrWDLvKt7Hgj6SlmlTCgDQQN/o9u33U+yAhIT53wF3kj3Ly7DfgeAOJbRq8cdf3QD
EMRTqPdkcliisDx2mNAm9RnbpIKz/B7UXyvlfPHETFnWIMMzomIfOKq3FsUzKpmrKQVUEIlxYYvk
HNpzryvVsDJ4krNosKra2OpfSdizdU/9GUF5Wk5P/Ed5vet4R9Tk6uK4w2q8hBVCsBniI10ZI7/6
KoCzUEKhVIEBLkvbjjnhGHJBzJIOGikjiEmyGCUKTO47YZ/wDtFockOW0T0Q44Lmpb0TygRj21DU
nfQKlyZBPOOQ/bwwdLEKTWwoAfPS9sEgtBYKiw4Qab9nKmnEmAnTIqvtKln0QV4XklTL6AM1qn7q
ua5GVOBOm5GE09Yka3OLXF2zqyIFUyg4wMFW9s1KDuDJdHMQ3JAVpeBBqQFFOYVRj6XoKYlyZlnd
GiOuEBWVI/HIowdrUSV/62DslrFwu4geGvh+BBXAf0InGzs16/cjh9ceZ5lrqb4zlHuHDzBLhvrK
dPOCCuG/UaooRdm+IebB4IC8kGCuK69a7qtOurrcVkzKIgE1hq4sQcz0Jku4HNyurBMSxy7j5oyr
XMU0hofUe93uHFQ6oOWfxD5CSLgqqjfkn/WJaEePhq9xVxL3t1RyHyXnvrMH575IKRhbJ/Qb2SVv
k6I1oSqrYITJD1d2qYK05OJyb00S6VmiNIBxiLrSqcw7qMfeT0vm042lm/2T9pf3ojZJNWBqb7h9
lJBNvKeLckEF2EMAuS37TgylFJhl0EL7ucvHfRrUnU5EMxklzi5tU7y7D+mFhNruKaEVISOSQ7pm
KNFfnUmeToynDNVwHVbFw88D6IkehAFSbPEowzM0FGdvRCYCid747NoNqsGR2VsPmNcapx7SdYCh
xmfLxkc1xCAOzXW3DeRfI5C5EgVCpWw/rfbV/agc18FHdSL4BT8ryyBn7NJDVVU7a0PAj7gs5j4Y
ZBFOuQZMf4JrCfQDkMSQj5z664Ecuui4ADq/nXoOcPXU6kfC+wcn0zDvbi855LVRLGADBD5scoZZ
lKiAH/aYlePAnPzUJTS1hLNKYoSVEq7ZyDmbrV/Pex7izSllJyv1M+z59JgbfHiSA4ItDDJ/Yv8d
r6OfYC1kPfaWbHVEQBMupmnjROa68OZ/JkQFx0HX38YTt625mJrDD6xCnn+L83oOu02spb5SArf3
TsuQf+vRFi78xvrwYDQe0dt34ivhVhhrYAfNnYZzNHdf6ywcHJ0kwxiTPZo2aeJzTEFS6/rgMLFM
wrTpB4HSaupBbr0/hfDtUGbOIsLzrBOblFFYMqBdTdbh9+b4cLLrNvLWfMYLv411hm1H1KMOi08b
SsVYrdIbkl1ZnLK+Jdw5NI5woBCXM2OaODqd/0R7jUi18DwFyBSxUL9uYq/sEk8qwVA86FulPV8O
o/ue2KcsjP9Nour3zkCZy2Pd8hno0VwiSiLpME84yki4t9e06pxvOGEt0pfTPGrrLw7gOSGJpUa8
QM90kjHLucaITHcpJeOb/4YOspmJWUj1zu9KgzM7FZKmtlnTGBAZOU2B2ePw5sj8B6gemPlvUiZ0
NRGdvGldhI2hNoVKBXAJQfReqfbFp0pFb6Tc13ZcW0EM9pgjN144+sraexMj/+U54cuZE1miija6
QiS0Dg9KXCsHZBc1kiu7f3zDvB5+FUuP60FmXc4EM3GolCG3swP2J7jjNv70RGuCLQAC3UJqm7Z8
teqq0tghBFLj4ybEMC96zEzbhXg6hqeA8vc0dA8gLTU2C7H7EfF2ZNNkdT8NxfPdj0QfpTG7X5KK
9I9PYFZZrKtDGqQkbaQRImSKcD4YqJ5SQP3zKwB8Wjb+wJMXdEHJE7Q3lHFILLrq9Rr9nCDfwrX8
u+eF1LSsn/a3qyVmxdAoelnOcBZSQ6f40ucKSoDs/RvYsavufHciiY6GqgsukCxSyrC6DX5aGk7Z
Q/4uykrOK46FlODv7Eiqf7VRUxPWYZFPBxSzryLQNHrCJq7lb6QKlS5m7y8PKzug4GfrJ0y60FR8
QhB6Bo7WmKmiAhv1yg9ScPTM6xIhm13vS7IXPj2tJik5YwQef6f/MfrVhGkNcDuY+bfuqmX47FES
isQjpFCCoMdEcLHFiSNNfdOZwzyRK54HmLU/GfNqs74SsqqeJ4QZq+gww6SGv7cMX9jT/iy5Hm4A
EjVf21lvJFgv6rnDzo7EyZI54J9ghwxy/WDI3r/MUkTKOZnv97khrdjAUZqSrpC9gek33eQC2Uct
4TNeDSuZ24m9gRdKv5f6uj2hUuXV6GYPmD/BUpfczuNLfFkNXmSU/cLGW9mg/rM61hIyLTX1g1lh
BTP4qx9MePZbvWnmFipCzWKcNQmkUG7OfQdQFLGAS1odNVLcy+wt3SR/aMW9JoIdrpuHzOZW5D5R
bO3h5Y2HKoerulU3C4ZK4GecVswALEYx09HsE6nUAT2jBNWBLAfgfW9jnSf2qYCCT+WsBZVRAsGo
2XkUuuZ+nk7KxDylyYoXn/M/XCWNgMVd83hHSnZxmXjukujcdJlQswJny3YPsBuI+B1L3YnD5ka1
hkX97rsDpA8kbG12BPMPMUO0YradsGQpJeWZTzXEH7xPdZCDrspfPhLwLl3Cn5qoQyYvTYnkJln7
vPCzmdET/gfax7Qblx0XEawGioGkl5icOaWG/YgsUGlK9ziSYiy262CwZXC+fXhzsRwKw87nlZw1
exYDjW8VjY5czGbGVfvE887Gsxxj1UogBAJwg6cVAziYO3s/mFqQjEk23RGb7kjilSX9tCDPPSsO
FFuMT64MgUpSHTe9tqcGaQo0SxFzZLwPZcGQ2sB20iv3HJL/PaYS1cNlR+d+iR8vrZqmfoayiYuC
nzpNgYK1qxVX1VVAQo3UGLTnWG0PuvMeCgimiNOBEDpjNoFHLvluSR1n2qy6rqmfsdtPojtKEHWa
Yynms+/XNIXU71ntcFKScIERQi+wLwWAgp1E96V4DV1tLkS4YAgepzhyUEEeT20xL7PwxYGx9X7U
Xjgjkq4TLPZujJIIkP4e5sks7aySZ4ftKm6Hah/gMrZ0YDSojoSA0cM5+0i6rDYFiwCYqu3G7np/
Gox1W/JAyQ8oy5TNq1myMKzKdGDfpF6vRFvsHKOin8Z3/+Hdde4/Y/qd0dYSiuyxNeFxXTDQA41a
lzp/E8jsgxyfj31Ripjj/oJ/cOb03tPY6ZkDhAT+6Rm7n0d3IFYKfVA8fu1Hvnq3vblRU0ytPO8X
uXK2slOSmZ9+n2sVsP7DgnyXLvKYrL72NU+WM7ZHqrUMn8GVQyqzrM1yJ/nfOTwIux16NtOcwOBg
INOAVyLQKSLHM7A2mm+Co/8Wy2f3ilG5+2pgh0RGxaVaVDZZje5jMg7Yg38Rod0F13gZfrMT6MAD
g9OE5yLxV7ZXImxQHnhCJZwE4wmqtgBQP/I230Q0qX0vClITqOB8kv8eDrM7R3x4xDhsZ/Ay0zq0
bU+MZlmAqi+Ut4w29iTaagZdoJ8vdTFxcINGrTYQ23OuyuOw2FforZomokPolUreRNE6MMbol2ng
1oSR/b8++4yS1sD/ADzCduFu3KdBiYRHBuJOcFL5gkD7gd2NZFxXLTNkJnvUYYxf8FdJUUmvPOE9
xISJOayLG11UvXCfepAEDUfzSTvXr7+ft9xRPF9wW8xAWpWp59fyMg/2AJ1E2wfDBgiU/dUXiWVX
88wGsB7j/neLtyDVHtJ95vTP/akX1/4tqYFzg0ITtP8O7g+JhxbxOZuU1/+A+ksQTeVVi2+XixvB
C6MFekFX2OWU+uY5bFRtfHKvgfw0JEBWq76igE2DEeN7KpcnLBwlM+quGLKuGN4O99zNmnGfYHl5
LsgInENWGMy/VJEhC7zovRBlvlVFSA74WhN7vsfEHRX2cyrQd8pI/fvmGu+yAzWOFbjVali6gkTV
BB+3odWWEwLyD8o6GJhew7urtVYSZEC9K05JkVEucYno9leGIQM9zsNeeMj3UIwRMiE1pwDcR7Je
YCgSA0jLlvYkH878DY7q4Jk+cR9Hvfk2HFKlW2dIwQPtDUuKY/riSkMyNkpJy/96h93Trl93/ZVr
00vguK8ZuK2IhTqWx01Gb1pDiPhgsOgNKA7x5Gv977DP559cDLcf1NnQps/pfxmASJEC/pJmYGM3
ZXKjAiQcYp0CIOarSSI1SkT9m4XdD+BO9b2xN7RkhV/lpSpN00uvnqFgs/kaikQMPjctcoYNSBPT
38mYbqNZDMlqdcr+IPs8Bo7arKTVtSiAhyWPdshs34srKOxjupcxSaDp5TAca8gz4+0/GYzxvLYG
FU1UtDmQtoz05M0hwYfgjfdg3pxYDqjB5wVLrRsuAZ/3xHN7uUnyC8K0KGWER0wxpRtnSG9Ht/ze
uKQvGIPkibWImQ/aBfSzM8x+WWV784LmjefwfGWYETP3OcC3EDKWbDV1qRcQ60zR8fe65InxAfjP
b/GwEqoPX2oQ4oPOpfRAsahVUbHOsoMnXKcZgPf3NZdtEwFGFZfIwzJV7yv5whS4RJs72TRsX2eG
mx4fG05N79adB5PpTDcyPczlrnO09+ubcgAwCa9WZURWZEtJul5aVULrk37++o5XqKLMmYnwy9vA
+Cfn/GJ8J/a/4YH7QRG7pSYDqsQFmXNxT25hGmYiKHTIriYiNDTw5g21wJWZAhEb7Nni738oBorU
RXZRJyCPj2A2c2Ht0bhajbQfAAKNuo9gj8eUp8d8U19sLtkm4BS2aWtA3KiNDfuzz6Y5SDYVPjS7
xhOQQMUh2VcPCwY/WxZiCTyHhsHuOsrBOG539XDdSIOa1x2xy2+9WaPf5bcYGYccrMo7D5q1foHM
ARH7g7E6OMoBoPOn8/DiONXJHUJ3IWXkqeri8IRNR9f1ic1EC/mLaEcBg3OdsAdKX+QWg0qOUkfW
1GFOYyq8gqmMusoKvhuLRTYNuaqQUAn7sC6VDP7CTcC7T8RqBC5v/MzkhxUwtNITbrc/hq5QcmpN
5g5HXOezmWC02fVAaS+ElwEdEYEAQkjThDvYSN+ReP9LzONbFn2cva8wsgDIJfxEYIjfZIJFQZJd
RwWuLPvLlBtcFQyyF4zSzHGlzFnB4Z7naSb/aLfEZYjYltxvA4sG1iN90lazrv2SyZ1EQJK8bvQP
VNx+76Y5pCVqafTPvNZiQmZKfSCCNynpgy1a6mFl1YR1kS6RSs3QWtJ8RVfrCMawMnslMzfn9Y3l
LKV4z81MSH3zw9f5xdt7SScTLtVLepZMpzpDguUV1CXkC1ANpv7X4lXwKJN988NnkW1woZZugwKT
PGvWfjLoBVKv1Ed539Hma2Um1eMIx7hLJdhpzhXkP00WLZccGJBNeSH+L1P3kPFfkuqxNavOm7+Z
zednLdLOVqU8zlhkJxBcpkYSd9JOg5WVezBRcOTQHM6HsPsmgQjGZc2oi3LMiJ/YsklcewD/v2pJ
MhWQerHU0xMG3kKSn5dQXn9UkGx+RUnchbJJjnAjB/ckCO46e88WCaJY5h50w+1YdKqWB89kyR17
WlVoX6CfTzNOj+SHxhppWKbJQ618OWoyf/1VH0B9Tm9aYlE0FbOpfy6+EuWPpKEYxvAscdrm7an2
1IyJSECWll4kSRQqsEJLDLFlBaHXQV0o+PIHlLAMrbnxTu5UyZGNyUmJJq4Z/3enZvvsz9frBeeX
vh+xMhgZCqYtcxctxGxXvOISwcReRGwgN8oOQ4UtlbrOSRksff+HI4+PnCZCvCn7QzWCZAvnZPA1
c8W0co87/N3TO1GSwjL2qf5rKNkkADlzV33FLsPxFBVOLDVGJc4We8h2iV1SKnmqi93tSn7sHit6
YO79tzinwiuNs9bdrWKWn7+rLWmSXaLcb+iL6Uy+fjq8DQdL0aLIy3KaWNSYrX6BJ0SxS40ggtaz
cf00qrvwXbkHDghPWZh15ggr9gzWqBqORAy0+sGCY8/bV+B255xeEbpd5MtPJ7bOrNOm2auw9bs8
TvDdTzSFvhTaY6kJDFuRU5C1GBAFhKVunL7W5edm59HflqfQGWZ8dakAHMDZnbgPx5hDQTgZpVD+
9AvxJYLzTN+BnUGSlj4l0xQ8fQHJG9hcFUUBOy7dziv/4gaz4fee4juFykWzmnp/veRs7NmOyTf4
dvhy/X5iQ5tSXEZ31cmolqYsFRZWWFbMmeA1A9ZfVEDogfxj9vijaTgOt415ntnoIHnBdh7tYt9Q
MZzoLb/dRHmNTQoTsH3Z5EoT/wulZqKJiDokbNIv0Y3imzZob2oXM/3JGq6k0SlP64b4Lw6s5NBV
a96MRq1NzNyX3JTF73habhg/HpYmTTsM3bshF9+Y+6/5sF5Fx87KniBw+wpknC0IUHJiI9c5BR1b
Svn1gQehfoEYfd14Dmi2ZxRMZvQJLWKvZivlyvvLsMcDfSveExm6T7M7cF9+/21jbZwtcON7sDZD
MfebqeXnQlxueBLRvjZonOQrPPogD4p96VXsY70MC36bZQlW8o4lGGSiOPUmN49uateQ17cSST5I
tz9fAxVk+O+KcveGNFyaleEB9cVHRXkdpd39mKskmQWQZxH4cJP1zt/Z/Hf0r+1blHZCtRWCDfOZ
0eXhe10EWeu0fAvENEJQMwuBvGgSb5JeNhI0LeMTSxhNB42XIr7gDTwZr09+6jD/6g7Bp5JzAcLd
Qom27+SYddppZThi7+rC5+rsjQn8vcECvXAq/lctH2blegAIJ/wiuzbdpa8GnxrsHedZIcJ88RGR
b86JQ+ktZ9fk9DJapJuZm1i8nz0sHqh2rS+c1+sGM7D/nmXbSN6Gs4uTCTb/OJ+zwuIgIXOp2RmS
tI1Vamo1bPtEZBV564aOP9rvKBs9oUWEyqXkUq5gbgmeDDIVXsvMfC30pZuiQIsFMgmKekvIqqAb
HjWALw+KAPlOPZ5aPAO1KJh/6rkNu9unqCzhVLD/10I6LIB7voxB35onJ2w0Ld65iUr4YQQ/kXpK
8SJL6YEx2CrDIKIB4mbNv/ZT1lxSqvMceS8SbyIeRHjY6su5vzXQUTcNoaWvN7ak2gFMH022zipo
3kSpelC6BstcIP5mqSf+WnmkugVVGNwZNs3HJOf6Ysi/2oIyRO5g2TDpGlW575udkWvUvzMf+mlC
sDVCV/1M0s7yFcwLmTtelqMXtQAa/ZPITJj7hRUGwL/UJWABRqeg8aSweAvkQqrkhfgepZFaGjsx
V/BbiLOaWxo3y8mYevRiiCZpyG0YsEhgMUs+3+chCiJoRfuyGtQ2A7+AQEYSS4CgUR8fIbf0Zwex
7nUO2xu01u+Bws16Q69/XOatj40DHOx/qyL8ZinaVwD9LzupIjKjpk+HpmAx21v8ZUFR4UVmeMZr
mR7OV1wtMnWJ7kcWeeKDwl+tNcqTa8FR3BTPvJlDUWA0zzazlxUhp5leXLjPCTTD5hAPOdoIB0MJ
ahL7ODHzaHb2o1uZz4Bdrgu7CpguTeZbjtS8MS3g99dm7qftSowYcnKsXwj2g2//u13WxmP6ULUU
+y7WopEnMDBvvKq1bLmG3fMIWpsMO978bmv3UdwmgAsCAHRlpM7nsCdKVRPM9ec+5nRPMOC3xxED
up6IYfM0CHmSioYWd4aRTynTynt5fem353/jbWqM9s1dMTw4cusyvTePj7VxX6B/FWfCm4XnNr51
USoFlxrDZvVrhYRvsB9bu8scTxzdZpgifiRIw2f44KNcF9cJSLF4E6fYjNPWm9/P8JSprw0cRSLR
tvTsnayl/SwVBAUChQIuNFlMOh82s0Cd3qDxrvK6LJFaFU0CGe/2ezmmb55U7cXZgDQ+mJbHwtnx
T3J/muyNPNrBUaKhnlvvnBXpwyGEm7bRPfCca9FaCTysoh0uM2PyNlNKlEMGrAzO2pwzrS96R4M4
R8UR0ljaUfTY5EtIRdmA6zuBAXafRQQdtOxS53R0JZHS8HL7xNbzA9BMs6ztdYDIm+URcAJcGjl7
zM21Ppapffea/6LpXphTXGydRCx/ldhZIBVou5EScxTxZecQjy2PsmIs1RqkRFwHWevWVWTni9wZ
zAwDzOyI2FewYNcEcKR0Tl3q7HzkiH5kblh1ECQNNRnOXAkDzdiwrPyGMDeXATwVS2q1/jHkPAtv
8vqsuUEhgGjAafYhYvriRYWH6g3sNgMMxhdWoIS53PV7E+l+PqrpfigrmBHCDSjHJiZrZG2LmzAL
cyxlPWax0nkz+ctirkUUQT5zl31SGH3okkoxGaY0ALVKuO5PhzbVPLyGl9CrxWIppPl00ug/errZ
sHxKiIyS2QApxzlVxVFI8jUZKtgZJi3ZMrQUXc4L/b/co0SZQGTp27eVTR3AYBoJhIhVs4Zs19zr
H9B1iPQZnsdOZNw8SoVLgtOfSbAJNQO5lgFoBRePtQ4V2/XKI6xuWqQq5wETWY4snUtiQFot9mY3
M0bYmFHiIUzNyKn5cqBa6BCYdmU8SoUy/9PJ3WI2x6P82KUkp1p6+wbDQ2/cy85Vs3g4Fp4Ame0K
IaynQWIhAC6d/kgU/Ldui/4GmMnC24o0zbmPNPYmJWvMyo/wLgtT26xLxkmGAPJkoqq7bss8950U
eBZBb4gqD7Jgf6QGACYU9OEciEyNZSwESdg9bwawEs3ICE4CwCWzKHaqME3YXztGljIX8lFQIVGT
0uvoLVxPg9DZHSvN/7LsAK4GQpStKnwlObq7a/dbXI5Kr4EDgJ6Bh4ehDLjMaNkB6NkXgrFQigDQ
zX0+7Pvlht7uWATe4ftqS0fttlpRW1ss4FeHAUqyesZhh+dqv53whJxLrXiJ8AQ3GdhR+4Om3k15
bWqdjHYIhKR6p+g53SXfhwWot2cVAlqHlxhn+/ustHLwB3HG31HfSebhsmBkkq60Ks9sE9Rmu/oi
YdzzOLA2ZpfVtORHodohq59bgPDIGg421OiFObd7QNIDkZa6BxmuB02T0JH57uKmoCEWy2cuY+ZW
3ZO2xFvwNQILwjprb5722KJ7VCg+75KgrzgTTh8o1wNcIVneFC3VfVUqe6ZGNHdt1ftFvCJxAI7o
zrDtQXaf/Qa9vJISiqSu9+FAgJsC/0kBK5WYsYrjF8WBRbu1YogA+/PRpYmLANLvhudnVXLYqu1G
T2QLXZeUoIy9VMDKFKZwIC0AdsYOxHTFhu9TWZiaEikT/rbG8nb+7XWBRQYeKTVhPMkPwaDQcs4v
qYc/ZtOeZp23uzI+3tCk5eYr6n/CMDwmHdUOzgYOwoJllgUcglpQLk94OBkdDITwDHa+8iYO//FJ
qwBKVxjrDV8m5fFb4vZQaAb5CUimkWMZkN3xLzPqY2mcrkQgkcylCiYcXn6sstdKuUHWq4JF4ZcX
PL1Zyx1nNm+B5PtedNlVr+gmOkPpmSxfHjlwEWhtEzue7Jm0nQ0ly8uhF205sH28rJ+dPxkQslAi
FxPWvr3ilKzxcl5hSxMJ3pG+wtJ1lOwJlB6kVknGDGXULKaGX77UdGIdVYIpi+5ElviLoo8sDglF
Wmf5St4z2L00jWWZ2lzNeXDgeV2EgOFmKudktn1/YVA6/jOwxejOMejHy8381fawpAe5YY4ArGAL
lWgS7fKM4ybqiqY2FnXqNjFd23aVBVBNmUaIgW1JtWEhhZNyxmvMVJjgUp1RtRTaTOu5zdDCYPXl
AcTu09h/GX1VkrQAW2O8Yziy8XGZcfD4XEGyJWtTOWAjGHPLic7pyUT6AurmsB+V6+UtaghNYfIO
E+98xDP2x5d6Tj54suhH30RXepA9/TVxQGEg5MpnZEvsLlXlu2bchXeePySoIsddHorjkZWQHvN8
YAG1UljGcQ6br4TrpUqKaHDHT9XInYDvuoIsQ70UPFFukVYNf1aFng0ltoWh0fZj7x2M13wwX6dG
8BVBW9oWdyek9kYdh30eqbgXc7dTZcQ7OHSLY3eKIVT8oPKWxhhjXBR385SqqfJ1SMuvF+aqu8Kt
EZoPzL0igWtCQ7Ru93BC57JBaWh5/utlHoiCQOwT/iyOHzIPtdr7KezGwf3YSEgvqslF70odw8sr
Nf4UmhP8zsGGgy3My5A4yAK7yPYnew7F2aO2p1QSJgEppbnNGaca/D+TGryC8I6PCSWZu0zNqqt8
k/QiQpy1p9wwdeAVFJRIRhFLrcJ8HyKuUCOcLb8e+mgy6BoyLC1MD8Y4tttvNNvPlUPSMmVyvP0p
NZc214ZXuN/Zb/tLchmZBLbn1T3lNbmTk1yWmj/6k37/9BBr82M9GLK51Xst+fUUZubycCEr/Ka6
jOnKtDWzoTryc1W/fmLiGvsbFtF+FiXHJxedRsicyWtVC3i4t1wQjARWdB82vzt2FhcSYqMQRtpT
GQtxovoZmf694CdVHKP2HT+E6QD9LfV5poOX3YRs6/ObMqOr4exSx00iyDngUgvpbpXHc07aSrKh
MY6nYUE1iPE98NAOLlSsAw8T0U8y2tyxOlfmv92rcgILB6P2S/8gTZprB26K8n9ljpIMQ5eXZrvg
5v00BH78X76QMNE1GG2uDvOCacI0EMcmbso/mV528RHrEC6bAeqzrpzIBf12OxRYm/r0Pl5uOQHr
amZn3QgguCo0HvEgJR8bmc34Sqsw/EbO7NpXdPs2uatfld2PuHXALk/zQ8Hqh8X1aiuwLItiyNfI
+PqOOlm/4GFIx85jKVtfN7rVGJzsw2yf1BDUqN8Y7uRILtbl/on/wIeMHFtThIayc82j+lOs7+t7
5HHRauSlvBfcDUCRSeeQJymLwA8mLyls0F1Ib7nZqYi6tKLLuZZTJAJS/Q3jEN1Vidh0soIJ4/np
o/m+np3mgW8f+S5P3VrxdnICmMtnb19z0yJXwib+XcsqGqjAfOv4hGC81vXZN2AZzdqPQNbp+3c2
vVKsZh54L+JP6TK9zWoj9XhbF+3fGU96rsJmy/QmDCuvrcH3xzx8Aqyv7PKKZ4MtFQxS7Bb74jQW
vsZ6pQPMBpjle3lcXONSbSHCSTs3FFgQ4O7vgyh6hY8iAdtwZGQ6AplA1o+cw3woXszEMUbuTEvJ
EoUxEJ09uP0lXiVtrGmaPFR+99o9zvVJyAqfnGpUBBhaPBSmgOsJgI2egr4dn6bmlKG7K0AbRH6S
LVgm1htuJCWTSl+mYwQjOeBY8VsZSQpFpD0L88J5Hav/BwdL7KpKGu8Qf9Y/HprEWHYragSqS6we
Nqz+35n3t3O1cgFKuNbeuELCVlBX6Gfzn0eaXxnoEAvgGsM8iFRG+HUSicdpCCBYPfdK9bmFaCxR
CekkKig5uF91pUbaKOJRPGV4mCiN1LxffNaSH6p9ajK1bKYLJDo3Y1x1t3hH02ww5H6UnpQu8oQS
OCMTT8SxmTOcAPFxjbLuhsmuTRny1FZhss+5xImXZ1Wn0DE6xAy2xe0SZru1MS2/o/0PHembANns
3nPqf0aV1FA0gPYhYJWjVw2t6gAznUUbqxbW02uPkEYCWkTc4b8eo9NYCv4/f6OxKylyh5Z/oZoo
cWVUOxwtrVwq2cyYY6I+Kpj4RAwXAzyFsL0WIIoOb70boFCpTcv5Dr5j/xtYYSI/2vdvCBQ8OwlH
e65eMcCEzNuVV0Ko3JKMR+y/FNPjztIIgq7tiAIXj5KkLgPHMqM3IjEyRaXMSKiVK1oywu6oMqdE
z58xjFoBTButphbReXgvDYNednwyuGr3/LqZZIldIa8z6oSwughrjZWnguiaKQW7ZuZBFMqeUuoe
QoV4916mpvLswoq7odQ2afYAd1au8eoSDc0x7tXT/cynHPGTkINHNZMu9PvLRqLimP2845iGUHgk
nB8P34b9H70e3eb8FJzNzvHI/CWx5s67prOBpzFJpEZEHqvbO2HKVN9uJ7DVSUnEi161FtRNqV0K
DGr3XSL3wjm9knw39NFSH2iQa99IbkMBRpmTbpfjDzm7PfJjdWOUCrBKEKWoFMOUJX+XnJgcOPWZ
r3vt4dKBc/hM9msKBHeZPVD8zV7ChqPHyKyU7GI9jFrTeG340/xWfRhU0aFcxY1EHRE+9Moh0PM5
leoXrnG5ZbBbOvsl1tDHWzn5PzDfs/F+U4N7or8YhlXrJ30ppabuwnvWpiwEgNQgQiXvsbaEHNVt
YOXL4xqaYVFO7JemfZ0HxUFIFVnLIEHyBCP9zZyBuwU3Uc2LVVAFk31kRKKg4C3IRTEhKIOUH0RH
/w7vYt+K0V4E53LgQTC4Dc/Ko0e5wnTq/qZ606BjYdubCPSc1CwiV2bG8vsOG2nTdxAV+tRwXNKC
SnUSnxLDHqD2h3TVEXuYhipOZeI8hjzYrKKrTxJ1LjrnbeP6vo8bGY/vgE5npZJIwnpCBuvLutga
lgSfFYlXFsL8TjqOW2BM5vb6e9+JX5YEWtcnVWKQxAyqsWFu9b7GxdkjCUK4fn9QkU00TBUThrGH
dAe2VgqCFoeUhmnARCS9fXVvywCwobypLSJu2Fu2tTMz9KtEENz9ubF+BcvDNuhvtqIpUZtFkeBn
z6ChpO+NLcgKNzsA8jNULXa39JG6ElOnEiFkQeeWIFtnrL6kpJ16Qg7bljvIQq2ubegH1oLP02fu
dLYLs5FQdCA56UEmiT7vRbRdRjUSKh+03E1ux9uSHHYsG6GNeMbjyIKA1Wn4wTpPZhLEPu/Awc2Q
V/aG3dxTzwKFl1GzJspT2u4Md2CmgTyv+SW5iZxhh6SBngez/ykAaMWCJOMxbnZikrLoOJHtyL7a
eklJrLl4wcCQ7KqZb7ZmiLRUciKnpftrCMzM7TOzwSVFXcvztuyOoCBHoEjwFvyStMMAXjKPmb7r
QPZaVz2NR3o+Bua/kjvw0DAiOF4uhOiACVh8cMHnGI1nKGl5yXYwRwr/PFb9LkL/+gxQfeCWhGNE
XWaKdwE/VY9reLf7nfZA9kYy8oXEaIgbKDq5lBSAWT+nSezr3AdHIaR1gYCrH5zFPknEqco+Pwrp
fPdjTDhtLAi1ByUwCJ4bZlSXJG1XdU2FpVZ4wW3ibJxxmtG1vsI7ywMNHYhwfJmkADfjNgu26N6z
B63ftDyUJsoUrhQaAwcMqLGMWOU0bWF6x6/rEO1BAifi6qEKfTlxer3JFAdezUUCzsslSbZLze68
dr6cFzVRSRbhB+TSXfquMj6HUCmD2Q1JEMXXLqGPNtlXlb816vXKFNzKgBVOFghnvu4hXn/PoeOM
XHGEJR5CvE0kqQISqbO0ILRF7BE2j6VIX2UXB9f6TDy9aaI//lS+MhHNO/7QJiR1cWsyDNPYW5dP
sEoFCgwcdadeYYE3YmFa3aYDg9lgLKPfTkRkFIugqRgU5E3bkTvsJqHDaBkSypo/RpbqyF/WT5a8
gnOQ5fW8dGB8TaOdst4/1NQkpwe+vIoB7jsNrjg7Wbqot/Rij7TrorttxpVoNUHX8rbxU405xwvg
/y2sjXiGjZ6GOiGGITt6ZzkoonYXcKofkf73sgz1yS2/7EIorp/LMPOr6kDbT+Go+AqJpHdpvx3W
2kW6HZF07trjSNX7VcEX0+YIf9A8hFJFNBTD3SOGuURI5qRTnKiK2eYFrAeN8GVbghpyxgxp7i+s
AT6fz5iDpWrLzBGKuGwBgiXB4f5WkYbQKByIgoQi1L1FziZZ0eDYBVXf4WzcG1EMCksXN/axnR+r
NtZ/cT6LD9DUBv0RIlsLPky/5y1pnG+sh4uBC9s3mgvhr78iHl8ZeYDjNjRGphgsaz6cCXSetBg6
ZHRA9wQJnsq8H+IQTX5pQLRVsmNZTYGU1t4Wz5WvPPHoGofRlvI0uYdBZEQHJoiNuB+eJgCJNHvA
q8+Zzgl8s24pqvXzF+AcQ2cucF+E8Idl19PhLUsuaR4zATqNoRBM/tEt/2zoSJbkTYkGsFyEqJGK
gPP4In/T18yQi4KScw5h7NdU6AnqUGt/sjqV26vS4b/HO1xB/jAoyMTjybbqOMt0euSVdTrJ1NgP
fqeEbbD+Y3OBCpgTC6mFF1lgxm0oHiRR96jD0bBYRa90DiM96oiSPobOBVY7kCcbQEaPaThtc19u
mue+r1M/LBYOXiQ34LNFSFG1XEttNgZfOS3YKiN4vf8rLhoDvLizosX2TMY4DxwQrxHDDmpfJhaE
c9JMYgQMZpAyUbfJiWvWj44oImZXCk+xw0t3bqCYnXSrEI5TF6qhMjg74mAOd3XYVJ+E7qqjayxs
J6Jqs4baPXxkkGpdr5SzkD4AjMkIeb7HTrutqTg2xtmP6uGoGIuO7Wb/lPlM4KW/xE4I/S7GZ/jb
SDqwMEAOCaIsXKbVrO9D/6kkgTO0YQn07ZRL0HesRU1w52y40rxzzUkqtQK01CZzT3liRhGvEyea
qLNrTcAb/aJ5GZzXD6lU54B0XBVDJT7AbwGE9nbylEhZFi2fvkv/5+1HeC+syB9AbUU9wKfGzRhQ
1f/H4bVjhcepsr2ki4iJ2BTpU2RgRbiaUPbOr5BBFD8OWn7ZrkAaLvK4Jb3gPxaa923GPeoP7ldq
3Rtt844Z1gbe3FPc9QVyC6b6vllqwmDlEkWfqmDqAya+gtwjojguMq9+b9GnEabWg0uNCn6h/vwn
STsA5a8rsuPREkjC3EyS/TA/0FVtwF5Gcx4xfIiAdwIy/wSc+h6p1ZlpjW672eD/tj0JacF7kiU6
LCHGeoL8jnVHIvewjtmdkzvDpr9rAi8YNd3JangTxcIHC2zCQkcAiMGeJXbB/kMfz+/9fI8dCf2M
ySwwfDW5cABm75kPywSsXNbmL28aq91T8Ykvad9FONZb/CYznYq0HWlPfSP/ccf1jH8RovRUnJld
5siB45j54UpIIije1pQ5S9vs76i5Ngp0iU6Y3X+gXZ/woZHVbnV6ZkbtoDsckERZ0YMTmbdbFhgi
wvcEwtNwcIKfNmQ/64CBZmI/wo3ypX0dHF6zNNBJ+aLK8a9NjjT0IpfYJ7WlY8b2nlsgcVB3Egan
T67SGE3FZeXdRMb3DTi/cPyAL4FWk8JV1TeIDTXDiXoOY9w4QodkIzF6+1KwyQ7c4ecsfbXe9Wvj
geY7pxKp/4LAhd9Pb/JIuJYjXXW7fm+61qYYe/JW4NfCAZkO0VwDJJLoMtKZBT5TuDZrTEQbr3cM
G4BIc4zSq5LZVRU0d6ymHZE17WLzKauQ/DFAK61Kh+yah3n8ievm613jvsXmXpXjqk2sI7IJ/UJX
TtTchRFMBTCcF3lkGJ7tnW6SJ9dixwitJGgflL5JqYPt8cqWOcM5ijktdmxvr7DPdmh+q9zbNvx9
ILVnyjnrej3gINd5+Cv0ID7da8Bea7gEUmEUha0wZl9pjPVIbYWs5W1cExSaLQMwKJ2fjbsko71Y
UdN4rqffXaKnJ7WCn7+Zqsf0J4Gb9fAvpPmXgLoenL+2Z2NwEMSR02l5/qymNSjJnMNN/hZJaxfw
NDdxAQDvl2V+aDYK+XmGbrT6ozb0HOakC6Myh54LY8T0IccvaQE40vnvBdePjTSGmOcb1Ud5aDM1
sQ6+JoKyYOSApew9r4IV6miTfMIQdlJbQaVBdzZL2jnCRrFzcEown1SzkG/0J9xixgC429ErJ6wj
5FAZwdp511bYyQ3CWdpasYm8qaOhmq5wxrFEu6kIVW5uP7yXUj9IIWpntij0gl2SiAEq6CDDwzbQ
nN5ozGLUkoUHmiqEZBJadwEkqyZUOz7RUksQ4eaPOMck0Ji9dagVq/rWgyHfU3keKNi8dHqSYcIU
kWKreXJNx3qaDbDO5nVUcjgEEt/3AXrBBzLH8Pl+5i2UMNUdlTuqpdXwXPV1XqCHNvJCoVO7e+Qb
xF02HLVckhwWPupBRU27QgOBeiR75I8udwUA2327bMJcK4tlVpCrNZKqadE+V0O/00ou9EfoG2Sg
hjp05UUsy6ZDTfExg/qMaY6iXZiRfci4krb3Mgd8yzqXK+YPW2CNljvidPZ9TRVkxg1cLGWC1SLd
8rHEM1L/i5z8eLJ7FfbKK2sjpNdrxYNJLxrT89LtiHtToNScd6khixF0R6lSnWxr79XVhgLK5Hj8
OyiPlS2bQCiUGF8qpLJ/gTLX7TALKcNfD8LEMAbiA3sH4eVEBkBH6dDkNv0xSw7m2kFZNoiq0UAZ
ldQ0LxJNrKF9mBOx9SLj3egr4oFyZA8vcmNn2nHpn4RUT8Psuz9ExRhnzMc9pkCnrFeieaDbgssO
QmQTeiJFXUsA2k/G1ovUd1VprvLNAkpLgydqvBik2qArqIf+iMPJk/FQNVM5BW74nvcDpo+GthOk
PAgnjXJHWieVBdSsvJUHc9xf8TcsO9JB571in23uuMN03cgxYYFO25RBYviJcRObWeuZbUimeLoX
3tGx8lfBxG/baacXaY9uGSQJ86maNE8FjeqLls4djCF0wB/+QOyL9wh1YLmsc9FghZ77lqwkxtrC
jy7xPQ9npeb4/IUob6y5gTfHT3+sNTzbffdzCyZ3CoK4B1cMjiQHLYn6TwhjwQlVPxc1ZhRz6SW2
HGPZ+81iMj3b/XQ3xx+SIjYIfgbBeyTybio15eoR5sqOdyo6VyxL0yh91v4aCRgaXmcu1sDONeGO
S8tO8vEo+Q3jKQUJ94Sz1iwXo6rx9OttbxH3gnxEusIHiLA372acxRL8qvY3bFB1+UrWwVnn11cA
+Y8Sg4G0iwPFbYLnFWDrRIuquJuT8RCjhGEj/VJh30B4CPp3so7OxNkGjwqPCKG3Sxh0HEVNiD9L
CwQUP9r2+xuiUWa+Si1R1Oedu8EUiY0xa0nBHF2yXRmNysmYN3qJ6OOB3Po5+i/xgDpO88lKmLmQ
yUw08zhHidVV/9o4qFzzzKHW1JIcUazI4gp+GCnx0FlBAMitHWXrR3q/hjTz+IDNqNnI3EeezSg9
JXq9PDcFT0+v0lVvKGunIbdLcNvGQwZ2G8c8wFUXAyVbmTicUNymd3RW98p+waYAJozmsZhQkkrm
Dlwuvj78rKrKBuuoaZemDY94a7+Qe5Vn+ea822SSJIzDR0l4TJOMsibqll8Wwv8/BO1VzJMtzu2P
FQkZwogPmYTeEyJz4uquyVoLWapkOzCAOn9KH230qGe748yMgJEJeuAC9xryapobGZP6MNB+ZLQf
wIDjolR9dYay1MjH/f8jBmO5deVK5+nlxl+5IELc4k+qApjOKOTNfsM/FWJtD7x4MfWuZ6MMELiB
UZyJIBKxiS4kSujt5nyZZx1JOysTMZZVXvz5zoP92isLgUIdKAKsvvnTNrS4rXi+9q0LehW1cffq
cJ+xV431tORnID1BqMA7QOWHVBZcZiOLeAKmIJ/dlyF/u60lFajD0aPWuKUXYlwtmPG1/NDssdea
CnyOkb3CyBl7k27nCguG3e+GLt6/PSVjkCLM4VbRVPgLaJ6ddQZEElnpqmFQzQtavvUctJ4qN/i8
j2EHsQy3LXLAY/KFn9GbPepoAeBaeqYwad8lNOuP6+cbgSYjIjcmSaQCzOkx6mtApMvwtNy9NYIx
40O3TzHFKOut65V2dTKLTMg2YhlqjNkGN5HdFj4nqFHNik2nFW+/qFBc6LqoBhj0N9b6Ffzu1JAI
WzP9pjvc/zOFKio9jcKz6SDNgaRb+C0wc+mhq0XE7bvxliowPGOd5eEjHmRKFO+8db5xxmAppE3y
yJ14g7Wrs7nxci0Gs9Ic+ieXJZFW+tzSjBc7LGXhRp1y7QOXqZFmB0Kfb5Lt7uXCWlMVHaYwy5cC
aKXmfrpLsmxpBhqtx5PmWj0iF8g8JExjhHWPKJMrOt83zOaRx1xV49g+H3fWMzbrIT6TrbgcrEDm
3OOLBQ7XWJ41xyhfzJ3xsyZTIH6Pjs6UUz3DjWVhC05Jw9OpHrO/aqMaExxko0Im6/e2q88wztrF
T9P1nKvtZ4//DIuJ4OIns9DH54tyj47qMjgCSWwZehFoH3JPbNV37XF2MKdYR8SvslDHIrroRdro
FhieHT0Zskce00YK1ynaQA7FF4gKgOH2LluIbdLm3WYoTYq+gwHYQ9DQi4lLLj7GSxU41dcqwoLn
rEDJFwCTk8xcGValmNsS2HJFZVg7LE0BaWGV37sdSfMRpFWWn94aJyli07k/I4rzNn5012XtTj2i
dsuegcq2Xy2yEaA3g5Dq6XwhQJytOHfdla5vAxP+D/Tffkrpdp4WHwNNQXd0jsspg+VLm8SFIdEN
tRtFm1UmDqzTuKx/xc7KLqBx7GbbecNWgcF2i69O8hK167gKlSEvc6yo6wdgfy7c/BJcMZKtY7Ul
FtKpTVYGTB1CsbT8xebbG7EIBkfsPcsUn8M6mC/E8+lyUZC6XlaZg/TvmC4gvUpfiRiQgJZOsOP0
ZesG1zpXVuUCowVDMnLZ4S+bPXXML5PTM2MKQQPZzuGJLETiAnrx2jPq0t2G0ZPlnIqI2RUGwj1v
HeD6xqNCXzmJ2spni2PNDpgqJl6hCACYQtkpR6hghq/+3uMzEk5Ii1h6m3xrpZKtUdDM8kB8PZyD
QWpvwUTqu6vtSv+Xc1yGcYIEc1sBLqy9tTM22LBOgTBox6Bp9/Rxd11wsreVIh+CGik34mYyJBtN
9+h3DIJ+tAS+ClRC6OACrq1VMJlYjiaO+Mvmq5IJLkasHn05BSsmcduhs0fz1IW1/eicyQrKhhrb
wl0Br3/0sAtMSszhUI6l6kVdTKfsNgQqJ7ZpYzMkfVeMBf0UJdOU2+fI0uF9F4BRKNDTOmGtBdIo
9dcFyoatHWRAgqaPw8jyoUDbENCPLOFqt+kZs9uYmVyeWjvtpzRpzRFMBnDaV56EuS86rLPBIkOF
ze1DgV5QdcdUanp0ltIFeotMRzptfqBH20jfdnYM7muGh5dwAO1sxIB82Pbuh+iOTzuo5y6I4mzh
IuGuz7Bw71uj01VHGPR6eKpU4JbLLrSOxAjfS6tm5LHpuxK0D9Ji/PJHigGXzKZxvpJ3MMbwUpq/
w7UB5pjM0arhiX9m1mADg9CwUf0qXHoXofj0BY6eV4CynPkY3IwxomaHISkwa71vtJ9ZBKGsAkkX
Gbqc5NRbqXwSrRb6WIHVv7BoGKVxKwVqRpRqDO4aUXrKLR9t9miKlm2o19u7zX5HZ7S9P8GX8x1w
/gS8MeszEH34l0M+XYJv17OulYJRxN/pAw/5R+e9G5IZ6RP6toF6WC/sDbtm/58pkMyU8pM74/aa
jvsK/qzVMh2dwaQkfJCuG0o3RYAjjSkrDOhOHMTy2nx9Z8W9WfOBqviCa/qqckS4WbXyCPOSkKIF
uyFxjDD+Vw8faxRYV5oOJmzfZmmGz9SJeGVwoEs9j+0RupIdfi7mD45m1SVACPwuWQK6w4kYZsHG
5lpOIT4Cyx37WOksVjCltCHikHnl/E1Z0PEYIN5susUu+f5ml+ZMNGos+38oXMyIivswvoyQdr8F
VOv4MjvVDBsRcBKBNfX2sVL4j3Z9lAeD8HXEmf4WWd98dZ44J4CWhjGeLIUi4aM9tMvhGneCXNe6
2TCq3oBlzIa4UeEpqfdKGBhiZ5QeHtV8tgcNJdr+KP0Asu+2dz6LN2e7ezhb6DcjLSO9n5192Wkf
NVy9zY79h9GdXebAB7hNJOBE6PfyoBimfkcx0p4tS4QRhVP11p4+QoduxW4oev74e6uBCG7GtGS3
Yhi/Chf5aNrH1i6suIZhgFdfBqCYIqv20BNuBp/bU/Q/bAmnjN0OcnyuDtl5BRNNZsZcBWtrmiFp
Zdx8Udhbe/MttKFNkAqE0bA9kTijr9EGYFcwhz0HA9rYGXfM4MZPj7b3BlDuIPa7cUa82TQMf45a
8UHirQq8EPfn97cZc6aKrZbjAtwjoijPN1RMNHOiNJOAaP3/wf6RDR81gAyIZt6OXI4My4ioaXeb
YsoUCmC8otGb97LOGs4J2ky2nLCgSBbF6/5CVkm6KsI2NqH/qUpu7ggqzbXHv7eII4AeXDqADPQ2
WdwwE2Yp2Nt8LRpULQcUfAiBPZ8DxQAFJlJ6F7v34GS3D/EkmWpUVMEX3xP8WRQ6oSBxaTbeWG4n
jGyojjuuZNP5PcYTN4ryhJEYHCpCyBnk8Cz9FBDURRoFVHgjbHn7lULAHCICRJp91D+3aOg0IQ5e
CjV9kmZRQTstGpjh6k3dduLuAH2WAft3WsNCF2ovdlNYoLqJim6GTPfGt2MQ2lDpOg0OccKaTLtw
boZJ6sB544QjtntDgMwD7gxhfyict4BFfJTobLNcfePWFujg+te5flFtrTeZI4xFThSlTXSdkDJA
mFTl3q07z5gUIW6p0UB1pPEy8dDvoyQVTGzbAFuEo+kN78MyesU29ftZEqXL8zXJawJWRNFRaBXq
RQo2WpB3/DdDygfYGlsKJ4doDhbeDUDDN4iObw6TOcqmO1ytPWGx+wXQI9/4CXAAu+39zRFX/BTy
k3B43mnYqotbmXaIcV/hjqoNJ3qm0kfZhFrOYsylPdjoFFFh9mLcsXpIAyuEmBLf4N8W+IclRmRd
5r9ecEJ4/QPv1lWOKJYmqlW4KiDqJPggr6B91C0AycpxazT1Vg06DXriaEhyMVj/jF8peCCa/GnX
gm/FfqOiPjlAkA9fYJEEPg0xj/DBGsgtzLxJqlzJvqilrqChmmnuA85YzcgZN3wmihYsMeKlsXLk
s83hM5x/3PyJgMh3hIz+P8QnkhIy9ch3RY7otKg5RDV2Fzk7st1EiMdwnHvgCXLzpJ5kMDcOGjuu
3EhKsOGLXkuZ3AYauEIo4yCS72e0r8NiVPNGFUC8w+mQ3VNFaDElcDxtP24H7KzwshCLsVn5GtRc
72CNDP2hFAby9sxuH2dtfQuvorrZhsaZdFhqyF+k2V+5uqNa/TqSiiJBh+is8UivVrNLo9eNKDoH
IqpQ/PuOV2LIjpDbJ/zKvA4FKUBsab42JKiwdqBwZ7zbIWAMFYvm9Wm6w0XqQ+mUTbjTDI51UMWZ
+4sfunoFULI8LGTDTnvJ147zTVFCnPZA1pcRxGjFFhP8zBSIzM8m3y3Mhke3x5ocjpKtixqJE9Jq
++ffGBAKRe7WQx1lQnVI8XQEb/hEjdtquxCNoN3eUWlWIuYh0nzyE4cnLDwG9I4VFHT/la7eXiPS
83oFqvpDXKY7XdnJUh7OKUwzUrCr9DBJH/z+CiJu0Nu4+D3Pi6KjuhZtLHRti4kVOaH3LnbQFnza
dsFxRfIdGQom0b4H5zC1Gz7kBxMTt+Yeed4ZxRRsR4aarRsEnKpWHzGyV79B13ZwrKB0eAycVjvv
UcC4OSbSoKvGqHy5KOhe2WFTcAwwmtbdTsMg2mOTsqR9MJtH09cJQDiwg9OjmeSgQ0ccAWsPEUMQ
z6eLMdN8p/aDbP4H0uEJ1aTzjI6REEnpCuvsLh8TKf7G14pyFx+HcAnYCNaK3KPp02Aycx29yY27
/o88EMZukRhz+M/aTtSbDLDlgyxlOi92DjHS4/Vh2MbiK0IZBxCrevgcUfypbcwCsr3zVMfmUztb
LpGGkTLGQojGVVSmL5LF/QdeuwoysxiyB3+3vGjkaZE8CleovrymAiy/754Aky+7NmyvVH4JN0wX
YqgcEI3oFhN97HrbOAm+xvlloMgqXjV0w+KDjQmgBmNAwIhU6EI3MlAYAYmEO6h9bKLi8fsWVt6K
Gp5LsNLge/qlgjXZDu9+hYmxvPYjNIIvFtteYfqOiZaD8tr07LddADMyiWj0/XxXn7uCiWwVdup2
JxFUqnbTYKJmrmTuLl8iaMlj+puhR6mUhLcVgZ6wIRZtohZ2e3oYWymZaukQuJyxLV5EOmZ8BM3l
Fk4943qXtIeNVvR7Ly/50D7q/QCy9TaDaKRdxEDr1zoG3bc8RP4Y5KMp0D5EuecUWtmJmzQ6Mntf
8l6godsS2tY1IHCWYd9j1hdRGbI44IMNPCdJ4SaZPbQ4t2IuPtbvbpt1yJP/n5SzOz/cv/FlFLwb
NhhQUHBiuVxsXwAaLkP0dX4qRGHgsoyOjEZoU97e8UuSnbpqaCqtyiyO/Fimp31bpBkV9bmh7TEw
9p72HUxvz21hoE8UM+Xte+3mAUgDGs6q+52Q6sMOQBKA0iIuW1pZ9MSlfo2eiDgBEVlSeXFQ2GLE
+AhvfehdfS8zk/D4SCQKWQ9jwGjPc8jfaUDTbbns47+ZrqaOPtatTFkrIB1wK7Y3R8qSjSl7ISMJ
4NQ1Sl2fLEoWJodzZPRO+TIMM4lbp+BwPmWso4+2DlKA6+4RS9rh2XXwd6ZYtgmpcrNVSzGYIVLT
STIRqo5k+59X9O6GKc1vj2qR4K7KMksHljNwsqngP3hZMrWJDbWjmSzHbzrfLU+KHMWEzVa11YqU
mcKTEe5/intz0f8m8SIg+XYoYzE4uMJx3cJZeyK5qfib1HPCtp9aJYO3PWPotimrZWv5lKEg7sLw
of/vksc1Wa4FPvmvsXXn4eJtu2S4Yp2oLa41qJkixbAkcQzZC5o7D0+xcpeM2yb+/sPoqpqNOsPN
+MLkjNhnaeTLDZVDGVC+5zxgFHWpfaV2/NwfJu2yafgj2Y/0J8f8p1tCzUOOohflCjPRuU2a8XOX
kgrzL1rWd0/L7gxReosHTzgk20wOfTUgGI4pUJXj+TzgMowFjfkC2i1oTZlYM2tRm+hdmSu9b0Fy
iOcICr9J3f9D4Z+SrGeHGa4FbLwuj8iL9zON+AEAwDfgbIxenzYlXWbH3vmAD0GPb7V5UkmYYgOU
9c4STPucD9sF+tWC2qqrDiKW4IaV6tOgXOyJWzKmDJc6AAJxDlA30rZ89MWHL1LGyYDfGGWdRM55
DxYdVaMjw/nM3xriUojCy9qdkjyeedMJQEddxy3ldLs1vdJU9XF/k6fWI5oGLD3aMkSzrsz/mljr
macKdurSjbKdNvNOSea1Xr+2ojih2FdPoe9aNTWR/0K1ZFig2djB2j5Cb1QU0IurgYWb7+r4nz8r
I5rQdfBBAwNvlX4Nv1wZ1pkDCc4leqY0m5wOq3BiE0TWzglfK8fSMaieuSugfZWK3kRsyHJ0K//2
FMmgGnWBX2QFvxrl0aWXK7UgDZJtfE2yM77nnRrMq4zHhmTtZq1z87kN6o34znleTE27Zagfz14Z
+Ynz964lgQ2b95BzDA0BSAT5DesjRIRy97YrwxfO0ScjzQbE6fMyFdfgEqtLpjmM5csySDECwN9M
qLkWOX2UYjGtGNNu5+cwlydvFhWida0sS5U9sQ5AHXk1eonJEvKWe0x7WjSop0fUSrXk6W02LyTc
2Fm1Et4pILgvcQFlVQabW55Tk1hG19VzQxfPiyc60ne2777mQfksJ+3KdY6l900b9e4Xa6OiGZuF
1qOluGgM9X69rrS/iWqQe2BqZagNtUlxUzEWsOK22gxMyfKZpQfqR5oHv9ka5HdR1Jh5REWHUTyo
ejQ4wOi/8dB9snAW0g8tUpEZZxDhOH0smQl1Rp0icuctdERFT7B/OlbJqGEFCFVHi0b2SoW1nD12
yFOvoPwTasqgML9ho5MuN0KJFk0I+HvQ1o7yFty0hup3j4Vo4GMCW56g+yAn+TbW5hY64XYdQdK0
IiUpEw37GT4vrlxDTcC0B9mTm5og/q8LonkW5zu96dK1FZl03tsjGOPaSkN277xz9bAHSr6ShNnW
45V1jJyWyLeZyOt6n5egY+Iiuga9Z2k6cXOPRvpxgSyS4yMfTAiEB0b3LIeF2jNc9dcwlK6JKUCO
JA0AN+U806+PC9tNFDaO0GC+/+iNldBIx7ptWCDyIooOfX3AuhCcO/aH/TEBPrQvNY9W55qjtQwo
F0XcoDOJYMYJSPinenuPoWPA25M8llyYI5eZi85VlRmVYtT5OD0dB9WoCgzfVgXT1OTZ4LgIG7jb
+JxG+XnJ2ea4Bp6uCZS7RQkGBt9i/JK3AEPh9dkuBATYr1830zmHY4p12G+zvXX5zGHy75G+9eAD
Jab2nmd8SA/iP7WKwYfXMTY1iaj/YHijFaonxx6cYnXVcn2z1hptpl2v9maacKwDTfyV+bYW/OLo
9rNrC590lnowKdAl2hUN4SvHtyd4BMWeYnHUzrUyFreVWklAH2KD5ZhV2yhJGGtpfN/d4cMh5NIJ
23bnepv6QSULsiYnrusnRx0RkZVJTSqUTDqQIK/0KvLngwkhQ0CabH8/XB4UChbwcJIxFPZCT0jO
4dymeRlmNciHAMBl555yF/F+4Cm+adciv5zqK2jp/PqVL+Lvqa9EKx2s5ubG552eFAWldx+IieJX
EuAw5aUEgpceNSAOjZGlmQDh/oCuLasomzJLOc4erp16n7U4lCUvJEnblIdAlyODNS08lmNxRVbD
WkMJJa+c+0DQKDfC1YZfTpVDe9WllW+lrShjnit0Za894mvI9GjE7hv3uSFdD6la4beSVd4lyaUy
0j8MZZrbn0MccpypoVUFP7Yhbt35hQjbMYf1Uc1BYnABw2vQm8dEFR6218Jjsa5niAgo2fYI2bb3
HHuSmFtFiXO8AecIxgIp+DhiIWI7CyRK8QGG3dolpYhDYgO6VgpAxFHPSYTQUDDn5QzrYtpHzy0j
kjLgCrojWOlOvJLwhrVacUIhXDBgPc4hA4PA9h8LAEusDpjgyHwGLIskOOCqcuEkMhpr7A87IvBN
/l3iv/nNfQ+kaHx0Hi7LGayd6dglJKNHOUnIHJ+AtKBKRs/V3BWrKVJwTl2XCSHjrcAolEuNwIai
flNWgj40DYbX1sqH9VTmVLfaYsY6o6+TTRlyj3L8LUCe3H5FpTvqjbhbLjg9pDFgI9fw0Y3SGeBU
yeJMPH9CuvQxh9hEwxSjMPmARN9r6YrOP1Iz97/BbTf40xS481VgAHbTSMHoErrh0M7t5lisycFY
wB6e3Dt8YLSrG77zru23eSv7o0hGWeuYza1fcaL+chUOLbUGlKMI7iISUy64BeHdJxqARjFLOXDF
PR3wjVPqS/LMfdd1a8U4nY4lGvATUyfVLfjpdLbwmbrkqNQWbVwPBBYtCKy9KI0FLKeqhBkOK/9S
aOZff05xpSR172R3cSuRvRHYhUK1rX0U1SzO17BVxDPV0C1ArV9EgJA3QtlkxXx8RSULCu0ZrGUU
qYY8nA8KM1lOqU3NnbnjJQJ1zJk2tEthEuim8JSj5yTjrURlX13VpZkXoeR5jiikKWtIDTUI2Iwe
SC5yqjPCeyuLXG/1axxf+T8S5+0dbk2kMsUcryYN5Fbm4pKrJ6zv8X1ZckdnzC/62ngImubmP9+d
yDshYvQWGl5Ndm9nRhMiG7dhocRjSI71CT9vPIiI84DIcmyyS2srWzh084TFWTmPL5Xd0fcLLMmd
weNmqRUbJVGk6mfXrdQpiG1jxd3tV6RYya0sNkGDt4aws1nWLGmJlUQ5IuIA4QSwP7JiklO7xRER
pHHKUkveprpyxBKwDFYHHClpCp1C39gNAGS0BoAo0+mTrk4QQ4MmfQG0dLmecygGTiQV6WvVVVg1
tWNzwdR2IkgABAWEaICOY/ze6vbI4uJqXf0kfL0VhQdNwGrS+CF6IhY2QzHtAQY2x1y0FzZJnV8V
KaeHCpLA7yatM/W3iuUvFtloUqRip387qqUfelZRp9n6e8wrjMH+h1hMZYYpThNhC7Vn/85lbmEt
qzhFJhDiafv6qI3CW1pHf7M7rbUnOkQKbuntY895IWY9gUEDyMcINu/VPQbHiY0TPifkFz6BQoQn
4yG0B1ooMBSHyq1bfaZ6YBif+v/TQDz7tOQ/1Nv5tzDwrOzpeZqh0qfi1AmpoiMK0XDlkhFHdDY4
JXiBIcD6lsI+dhAoSlqtzF0REOG/pYP56GYIy2+p2hXGo6jxnmSZQNoeDZJrqtvSFZLj7uLaLOBI
wCu6FsMC6y9rjP5oQd/3oR2Hl+54vmaTwvHWZ9vucawSYPfuNFnjVIDmgZUAm14Z7HPkBVR1oPtm
Dq9nBF9Cs7Q2IeO8Ocgcc41pOn7Le1PziB/JfqitxXMLSt40SDxmjtDzPQuRPqHjO//BRlMOz6qN
KOLn9ZZYjTQQGhEgfgYiuRySWltXLegvA8sMOQqAk7+B0WHcMaIl3a3mvWPv+/9hnoj/glU3FMjR
A63pWOF0TFnQVaQNW1MJ0pUhHuSBOqnrsTVW1we4feJIPz8KcCuQnR4Ce9QnM05EJeKT76wcqWjr
TLgR9a7BL5S0sZgvtXv2LReOFuVk4QvMJn78P4mfq0mnaZPE6is2K32Op6rUCPn8YOEW59GxjZCE
S6oLEzlsFyuR2zrXyDbV2JrIn+hCjKK3pDIop3lTYed98hDyE3laNX9uuBbfHn9Zl1JyhWkk/9x3
pQoG0s1Omzy7zfP/2/+Hn04GSB1VfXkFJsXOTopOJ3oLeNG2c7lYEv+MOa9BhLRS4Y+N39xmNy/w
+owq6IkqcmxMJXIcRA1htz2z67odgjJ9OtU/n+6b/ifONvLXyerwmT+mBAVuXXdcw/tfTCjHGjfb
crmj7cXqxY2aeego+jwabzvAh4NAv+6Lw2ZYxCKoxQa1HQKGVMWq5/o8J8L3ls/ztkZ833QnJ+eT
pl073pI62+2JuvoLfYLSt4JySh2rrpRpyykQ2rVkGnH4jEuP1VqTL7KWuIr0e4NbYanVQz6HRLso
8ZNj5GSSQusaHe4i93z8dOvsD+KZI4mMRrsJm+gMASjtkd1XI5DP/5fGgqxv5Axsdy4uVBaB8txz
iRzR1052jEegM7YJrcj413jqcPyXSUfy9xepO3LHgwF9adN+g3ugUrtcvv3XLIJC5FWJu0etnxzW
4ivu1kZetedhTYVy6l7i9CTrH8lUnMrZ1M9TRGKCoolfYkVK3jHZ2x401q3GO01ffd90b6FFiMeP
5xDS/P1Kdwk8v+IsO6SwtQyNy1naMwysDO3zzNAx+51DsXPiceEDZhtQ0l/fIxLE90G7Q/BiwZU3
0grJoCkmaGCHgzNXWgCyN7LtMT7ud8AaiaCxFkPJ8zyuDkj+Fv0lFKHZQLiK0h+LvYl+Pp044LpL
ji8WBEttbsk/WYdClgHA3KjuTkkF6LUcxGdsuvryOihWjHyMXvwhiKhKOjMlgpXQQb2sRh3/CWch
A6EFwppHuzTGilEcyrvxBJS5Z+fTs0+hIG0u0ZrluY+YELbWKQPqdi6lz24Oe33jDqnnzf+hw3mT
a0UKkfBdI0vqUEE02YNTrfd/cq/DNtE6du86nqxk8mhAGLCLgnJo44LRLT4L5Ann+Tlz5NszRtnz
F4NhYfTYdtbkF7CUznrb6pSkmOnOZfxaUgUByHkdkrjS6ULXSaNiJrRCW5zPMmQpMs+JZTYklBFc
Umut4ji1p28tN8bQ3Ax6pSgcDV7SJZKk8eIkilOusHMlVf+OWLCROCc0S/BmhV3zKFyiH0rSW0gl
7LodDOJMRtgrhAXg2j86Zg4eLyeqWq7lgj881SixDeTBBPw4xdMt1k6e++osfrmyXM2yC3m9zYzc
1fXr0S91ahFNpgSgHcMRdbzrBDBsEe6gqKDO4Oq5BEosUbv61S0jqVyYoHMn3tdxKs08RXIWYqOl
V0JQ8vY98jyK2V6cea31quQK6zfTYbxyrm7fVnQOiE7fHvP02NoJYSpHZorruSY7CrBLYlQ5Z1f2
swNdND7yultjJtmHgCb+fIT9sBRDabP3Bq6ZS9yIgZAyf0UP80elkek9rsaohkJhFJlop3JPw7n2
+Rnuofe7vGAVA8bGPXClI15Wj7f4IqZ+AEbQFPYmWxVmipYvQFug8KMwVRKBNJf8uhqnROjzlkbm
9f8M8RKJi0W5xZbsywTwirg49DWNMq8BTH7tYRC/FfFYejEt+RsdN1RHbeSMmAh+FECe5Bo3lThD
bJLunPze9qL+E/ZfsqGHeyf/2wqjj7VOsDmXLjVLWeucdI+g0Cq6c9gm9I5tRVOd/Gu+dcUTTJ5N
M8D2tSRSt3fkNfw63Os1blZBb4hXC1mjoTxKersjvfj/mNX9mjCwM+wmu9A3/eytqncXOy/r9Q5m
eCWaR8+ObqaodzOhpoaSTmfFOCvt2MF/BE+89J2e3wY9vwVUa00xcGO4vi4PTyK/cnDWqAjIXowb
+SvS5yIjbuX4QdDZGkYVWjjd3A1MNt3zxxn22+GhAAFEXV+AqUwh3l2+f1LrRiFv+8VUNwAf3xcx
xevyzGoMU+4UODdEsBiQkw/WMvja8G/TC/NDvk4WVhP/ZJfQQjcQ20U2OqfkiGk6hk8LPfq5IEyf
Wf+2W2/sE/xwpBI5vidi+mbic7Gd2Uf4ej8ymU9O8YyQaf6oWdacIMjEl8rq99BGMZnX2lhs13m7
l/FlVBHXQg0E+4lLCoRqpW7AZ9umNqAU8jJYlXh9J5CoQAt9G3mAtk7LxEYrRwWSBAe4Ikg9H42e
pgZWsSd7UcH90krAXerC2j9umLbpNjCtDQ0yhdCUXsPFI7evNPT2pXsR1ECA4xOU1wrbJkOwsKce
bM4CWAiMctNA17pefJ5dpauEVb0Nf3e9Us1Y+pV5RCEMZiu8wJI+BtiHZGXlJ2F6+JT+lXc8FDUK
ERlfUxlC3IHKVtd8BydH7WrJpP+8Hu5MWIAi/b6ueMU4DjrVs9k8sRhLzC1G6N9BtG0de61bvQPt
yGo30C4W0XNCk3c+RDuGUDZJXxIlkzkbsjz0Aspi+MSR5ZG55YyybgxLYCUkhH8Wvpj34XDg5c9h
ejXFnL8iLWbfETP5tdgxUDrAoLAtVRKKiu+ggaxsr1kQnoxhJvVlaAHe9fw2+YsxZO7buKP/x2Aw
1U2CZW8huzCqQkXEbJByZaS0xp+C3zO4zGuqZTFRg347RhW1P5NnwneffjHqFG9PLk6l89YZlsFa
D45v34IevgXQKDS3kR36hMsMuxgpaUdvBM/xB51M3QJMB7GO+AaSG3h481d/YHOMXF94HNozSzXD
PObIB4lIUDrLHVbftUwfGPcCqsbWrp/5tXR5shCnXm1WzqkMlvWgNiLVdTP6qld8iD86cNZY9kPw
QcYuc3Ya+1FgSRQibI1XGsVNfcSM0Z7rxrXV8/Hfryv4+y0jtTyGWcIjG4RUl3MQQ2zS3LVaXXYg
NKaFnJD+iKO5PlOtoU2gZ6IcqBxADoRy/JorGSSZ7p54rA4NppQFnFUZJX1Qh4PKcOMEB/QH5HtX
kdPNR0m0unR/YB/EQlLSHHQKzxnnwRRuOb9ZLdYjDTATbWC84AQnP/FsuVRcb50OgYkCLzAG+ZDt
vYajeBferHoD+T6RJrgBe905K4ctpOnWl0dns3QGwgEMw/J/4pY9vWXeb717APumBMy8Oz0u+jFM
sRR3Q+PU2P1r2/0s/Q9KBXwG2bNnJdDCvRvSdB1NEGQLk+Fbt+FWVx23RZ/JBHObGTidUlVaBEE6
MrnwJws5yEQJuLOIr8u9nvqBfnHPa5zwGbqywpK/unClAbogoadsyPEouyMn+MPq2kESdkxV7i8E
hl2gZH1nzhHDx7PkhD0RHOiLWr+QItjJyEUVdJ6t6aSSNnCinbwjtIDcOGmm1hPpmXozrj88cj5h
SqNGT7Qi+0LAwh3vFqjRcae39NaQB2NksPNX7VgbmcxP+w3/nLpkteHC64TK2nsci2GVx6qHmaUT
Hph1Iz4zszVXbnzI+Dlr6HLYPU8HRujF86fdOgld1lDi3ZTzcCUTx0jfjL9y1Y1I8i46nN/Qea75
a5pygDijcW/U7qdfSF6wAYG24ZxiD0mHdl70jt7+U+iwUft3G7Z3c9ABSnl+ugWc5/uLPjsjyJKQ
l4wleSorEh5Dpq+zkQg+231NBBQcVOrgCwAQseRlYx0Rg+R/KUwHAn9fYecVe1QjLBCYUNKBMe9/
X86TPAIMLlZoJc+As42C4Ak+rXgxmwGjG2t2mrftb7RHq/sRmAPkluRMFKgNw83U7My5xQ9Ls4iV
/3WpoHOSf1iqqtDaoNACGFuLSkXVgy6xydFq7iSAmijd6MDnYuWkJYaKg8LC4+EYV+FcwhASCg0l
8XCJ4f+Er7Tjkl4zKzb3slZQMjeEDfDrN09n2+n+D5GGIaAHvTcftG8m3v4G72ltJ8jqBTatDOrh
tR/CqTp2y0MZlg6lpa+5OIGZYVrYH8JtR13f0Rk/wuc0AWpjSa5GOAOk0/B3sV1VHrnmP9zSNXDv
b6H3g6557cSF6zCugCzvLB3uWGTKwXi9Binm8Y6gil3mFBrd+X573GyIs2+TopmunfzK0PkHMrWc
SorTR/ls9aRvhJ3QDmbXiwWMuFJJ9u9JQv/wxYamQTdwtT7BNc1pn0MrVwGoT8jnSw9rhxdgWRr8
GBTIIgXKEhFkP0YiEFBYRW96k4XBHv0apbj+VtpPZ1bP/VNGqnTU8kiKiXuEw7c7JN06k2In5JCt
fXMzD7q7ESBgDpEFgU5VveSmTC+CPS/+LIQeD022ZT48QMRiDBTIJxwV+aQrXrT2EelMw6FVHLVB
O3fvbngk6bOHPp1sGz7t+6f5ZAO/UpM7FkiTDBY9S4NzQZGE7AE2Xtbg6BJOqV7zkWW4fU/kPIYJ
3c6InJvEsbOGFpkOGSrVY8iNcrFUJviNQrREhP0I7jr/fD0K7lg1vyI/o0YdLmTP5Y0EsFaCwDe/
C3aZ6l0szKdUXQtpImUvCQy5yYkgUSlx+kcSt32gL3NVzREGcUqSAjpfWEzsu9KZxP8ONonJn2Ed
IXyWxl3fKwOorQssjTSnh8byQZNAA8j9YQpy2HlELW7SjCC4JS4aGMvPUJtDrlHivo+fxKB/GiYd
0COwcwpX5SvG0cscDi6o9IV7NW89drpo+0t+9oHuQyqSnOHQ3bQYGvSRYtwvLLHEDo/L/+ocRDj/
girEn+mej8IW4NysLem2t6034FDGNW6HeG2TWMILj58vcowjdGAYkVuZ0tF4XWq5qrI+L7uciEOM
d0tuqbsctdqauWmibxv5T4wyaScmAXWtMhrPqaQY3xP0FSnpqAbmOarO6OfwmO5dTUDHfCVfItl7
N6HsGCEqTTz5i6uACBg2Hzl5Vy8qrxD3WztnfslI4H7xPSY2aoO/2BcU6nOVochMJ1rxz0pyzvLl
tOXjWs6Sc6BoyklXNMhmiuOZQ8bMisFOcRiJN7uFyFSdSvh2G77mOu/3un9nuiKR/yyI5S2qutl2
UTrnq1qAeWMmxSg47d2MqaeAx+VE7xmSjwt2V+fHPx2Vj23nhNfDyegxo9Eg8uaKU8RdyajbYJ/k
lkNnKk/90c83PJEd98T8g4v1LMQO1ZspR7K2qhsk00Ox28q/wIG/ltdlFK4XD33csl94XHKxzT2x
9B8xfkmZgQRAN8B7JAhfqMltzMxB1vMXPtGuls+IwLJD+fV+ixM6aEtuIoVXldVuLq98pdfjvKLf
nDoMvTbfPX7aY9ziGJi44Se0bINRWZyUDmpS1SN7TGceHf5XOJ1w4+pkh9sDcCHzQxEc6kRiGPHx
dcccqlA02xNmmdfGJkwUoNZwhGTDaqSvVtOxBRF62Y4LZzpbIx+3KepD6pwEt2ESiNGEG9cB+61h
ErAVz8eKBXYf5IwdDpHNgX3fbFOeOBHIcrqeVf7+ZMafezNlS28l9xUl3ikNNBDD1ClluFhIG1R9
XWZmAWMLiEjwh42tTzL54OK0g8Ll7Ef0goRIiuUtDPv67TH2pRZltWJDOw1pzBaBG7hWGtmCu1T8
TpY44OHR4VZXxcJn4FOkvqZZnRfMMYG4j0xBqR2WpAbc7uv44fvNuw6j/gIjUmyoMJzqQQ8Mqq2W
ZeGMl3ehPmo/UvdVSmgn8HUAkSWjS0ph0390uuqw6+I0JKrsfSldrcrK2BkFZ28egnzs8GLvdY9O
JPGAGZnc3JVnon+CoUfpcvJNOat4Y+uSBjJF5XgurXAb6wmToPUoT0z2md5EZ1O9uHB4FEoaQnB9
WkvznGh2/ang3yvYzqymrLgX3yOsFf1rXoC5z/O41aRqBR+HRZq02hmcc6/U82O0QJTGkmc959x1
ZPpVO67OrJiMOw8RV/fpkVju1n7j/Ux2DPysNG/3mJPYZJFEVMI3nuJwD2UFFRV3LOUpGRwHWc+o
5LzTe6lELuJgkI+iv8k3Z4hlefW52GjeWBuugHBrLcpQy/yUw/t5tnrHu4siQl6Xd1XWNhBHk36Q
LOlPHh8hK1z892H/IMGdY3k+QSgY3mqRBUxongq+otwhViAXszETNaF3M3nMkXac1aSoLcUB+J4n
SqKKHkw0qgoDE3xIIu2O8e8zLXBzlGR7Uu5ATZtbuteOt8tEBWA0eYBy0FyH4gtn9VbdLcAKRrNx
Wos8dPTk3/kYNUjwnbWbjnefNFsmKOufTxaB777t8k3knQ5zuz4QsA/6wbRjxTix4J8n4HDzyLnI
v3tCixe335EbTcghZh/KxJnkY+mL5rmJ91Eg1a7o9PTDucx+XsrvapMmIapImmClkLrIpsnSu7Nj
huXHM8dHbn+oRqVABlrJsX/JY2bs2Gqox6d888hL7Q4yy0BXJd6drxSs1CdeNTOx9gVnJFyNHvjw
EpmGPCXD7SANjvjsgbbwEmsGJ9EeMNPDe40aC9VDkdoav4JQRNLFKR6nEISv447vefuq+tPIiGQv
1By5bQOMqkO1uwdSN8GF6xal97MSOSiXzQGzT7NvT5UG2pZy0/Y4YRDPXla66LNeyDVo0JnZFCPL
hP9QxWm7Pc0H5FH7t31qj+NinjIrkpkVGVwkmxxSIIMgB2Crpm+Tv7exQu+akrVbzcJ7RcfSbRZZ
tk3x5crfCDcM332S/wdDORG1i44KYPZQJzc7AtkTp03ycBZUABzAr2oWauRIQMbcAODwXVyJC7/G
L5wV8as2/hfKcHQRBDGr+VY5Z2Z9MXmbpxvrE1nMlyU4mxDFy9H4Z74J3PB1V3sndQgYsPPh4Qyd
9N2jA8AIzE+m44d5gNtZnUGyRDI9Jn+I6nAMieH+u1EWdN7KCLw/Wz0HBfB45aETtLaik5I3zoZq
GFXKd9/dHdBiEZkAtS6MJDFQ4k/7VjkcNEQeiqXQc/AknzmUWfEOZEFxGUJVU0lfkZHn2n01C9HB
G/3x4DgKE1akeyqPwgfnPISWhOjJrr4NxNh0W/ovtS50P9gyEgMqiRxFXY7AzDjzUuQFxXlv5NiV
j+mH3hXcueoF0bXKRMzLcVC3MM69DpyAzOSkZ/XrYX+GEXyewVEYlBsiSnDraThLPkXsbYxPCkkj
O2eWMY98+bUS2tr3zO3PcAN+nB/l7vxRfL73m1ifGDQl20qeW7XesrKcyHcTgXz2pRWlOZlv3lnV
+magN863D/f8Q+MWzCFYL7RJBQGj7agZxM/xPXab9K60zwaZvScFeSBMcf89MkgiWeZYfc5zHRNE
VAKguaFuXT+Dti9xqMAXnkezE20z55dBfPqYvcrDmisg817py3wkytioZSCOn7Uuj6TO3tLAQFE9
+xmzXYbQZKnOxRip6udnBawziklowUbPHmIDhCvK63Ir5IgLzrv3MDVIDrHf2LzJ3mZ9Md5IzhBr
2B4Vb2nBDwgszMjXjBD2rB0r506M3GDT7SHLE1TgzbIjMEnMc6X33mUXskX/6pC9TyCd6QjbcaJu
NpDIKNbVSE5jNvkMFAzJYPuOtw94LkUVyrHPvOYzLzHBXjNdcOhj9h4sdF14m7xeODYbjAQ218Ut
kEeMQNLWPrgymGaE3g88stUrGWWmQ52kxV8bzOOay86Oulu/rXfQDIPqAUIwUcWohdALbjwP2gMr
KFVvEfywib4s3ik6b2cXDp9mF5ZrV4TtuJa6gR7foHSlk44CRmDZtYJ5ZWc1AUhL2ap8vrcNfYug
SyzIPRXubWhTkMq2Aib/aH3DaokXvw==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
