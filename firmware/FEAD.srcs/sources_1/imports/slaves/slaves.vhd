-- The ipbus slaves live in this entity - modify according to requirements
--
-- Ports can be added to give ipbus slaves access to the chip top level.
--
-- Dave Newbold, February 2011

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;

use work.ipbus.ALL;
use work.FEAD_IO.all;

entity slaves is
  port(
    pwup_rst      : in  std_logic;
    DAQ_busy      : out std_logic;
    ipb_clk       : in  std_logic;
    ipb_in        : in  ipb_wbus;
    ipb_out       : out ipb_rbus;
    FEAD_Monitor  : in  FEAD_Monitor_t;
    FEAD_Control  : out FEAD_Control_t;
    VFE_Monitor   : inout  VFE_Monitor_t;
    VFE_Control   : out VFE_Control_t;
    clk_40       : in  std_logic;
    clk_160       : in  std_logic;
    clk_shift_reg : in  std_logic;
    clk_memory    : in  std_logic;
    BC0           : in  std_logic;
    clk_counter   : in  unsigned(13 downto 0);
    orbit_counter : in  unsigned(31 downto 0);
    ch_in         : in  Word_t(5 downto 1);
    ch_out        : out Word_t(5 downto 1);
    trig_local    : out std_logic;
    trig_signal   : in  std_logic;
    CRC_reset     : in  std_logic;
    CRC_error     : out UInt32_t(5 downto 1);
    APD_temp_in   : in  std_logic;
    APD_temp_ref  : in  std_logic;
    Vdummy_in     : in  std_logic;
    Vdummy_ref    : in  std_logic;
    CATIA_temp_in : in  std_logic;
    CATIA_temp_ref: in  std_logic;
    V2P5_in       : in  std_logic;
    V2P5_ref      : in  std_logic;
    V1P2_in       : in  std_logic;
    V1P2_ref      : in  std_logic;
    Switch        : in  std_logic_vector(3 downto 0);
    nsample_orbit : out UInt16_t(5 downto 1)
    );
end slaves;

architecture rtl of slaves is

  constant NSLV: positive := 3;
  signal ipbw: ipb_wbus_array(NSLV-1 downto 0);
  signal ipbr, ipbr_d: ipb_rbus_array(NSLV-1 downto 0);
  signal ctrl_reg: std_logic_vector(31 downto 0);
  signal inj_ctrl, inj_stat: std_logic_vector(63 downto 0);

begin

  fabric: entity work.ipbus_fabric
    generic map(NSLV => NSLV)
    port map(
      ipb_in          => ipb_in,
      ipb_out         => ipb_out,
      ipb_to_slaves   => ipbw,
      ipb_from_slaves => ipbr
      );

-- Slave 0: id / rst reg
  slave0: entity work.FEAD_ctrlreg
    port map (
      clk            => ipb_clk,
      reset          => pwup_rst,
      ipbus_in       => ipbw(0),
      ipbus_out      => ipbr(0),
      FEAD_monitor   => FEAD_Monitor,
      FEAD_control   => FEAD_Control,
      VFE_monitor    => VFE_Monitor,
      VFE_control    => VFE_Control,
      APD_temp_in    => APD_temp_in,
      APD_temp_ref   => APD_temp_ref,
      Vdummy_in      => Vdummy_in,
      Vdummy_ref     => Vdummy_ref,
      CATIA_temp_in  => CATIA_temp_in,
      CATIA_temp_ref => CATIA_temp_ref,
      V2P5_in        => V2P5_in,
      V2P5_ref       => V2P5_ref,
      V1P2_in        => V1P2_in,
      V1P2_ref       => V1P2_ref,
      Switch         => Switch
      );
  
-- Slave 1: VFE capture
  slave1: entity work.VFE_capture
    port map(
      DAQ_busy              => DAQ_busy,
      clk_ipbus             => ipb_clk,
      reset                 => pwup_rst,
      ipbus_in              => ipbw(1),
      ipbus_out             => ipbr(1),
      clk_40                => clk_40,
      clk_160               => clk_160,
      clk_shift_reg         => clk_shift_reg,
      clk_memory            => clk_memory,
      BC0                   => BC0,
      running_clk_counter   => clk_counter,
      running_orbit_counter => orbit_counter,
      ch_in                 => ch_in,
      trig_local            => trig_local,
      trig_self             => FEAD_Monitor.trig_self,
      trig_self_mode        => FEAD_Monitor.trig_self_mode,
      trig_self_mask        => FEAD_Monitor.trig_self_mask,
      trig_self_thres       => FEAD_Monitor.trig_self_thres,
      FIFO_mode             => FEAD_Monitor.FIFO_mode,
      ADC_test_mode         => VFE_Monitor.ADC_test_mode,
      ADC_MEM_mode          => VFE_Monitor.ADC_MEM_mode,
      trig_signal           => trig_signal,
      CRC_reset             => CRC_reset,
      CRC_error             => CRC_error,
      nsample_orbit         => nsample_orbit
      );

-- Slave 2: FE capture
  slave2: entity work.FE_data
    port map (
      clk_ipbus    => ipb_clk,
      reset        => pwup_rst,
      ipbus_in     => ipbw(2),
      ipbus_out    => ipbr(2),
      clk          => clk_160,
      ch1_captured => ch_in(1),
      ch2_captured => ch_in(2),
      ch3_captured => ch_in(3),
      ch4_captured => ch_in(4),
      ch5_captured => ch_in(5),
      ch1_out      => ch_out(1),
      ch2_out      => ch_out(2),
      ch3_out      => ch_out(3),
      ch4_out      => ch_out(4),
      ch5_out      => ch_out(5)
      );

end rtl;