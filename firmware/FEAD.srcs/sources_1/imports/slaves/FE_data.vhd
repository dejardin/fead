library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.FEAD_IO.all;

entity FE_data is
port(
  clk_ipbus: in STD_LOGIC;
  reset: in STD_LOGIC;
  ipbus_in: in ipb_wbus;
  ipbus_out: out ipb_rbus;

  clk : in std_logic;
  ch1_captured : in  std_logic_vector(31 downto 0);
  ch2_captured : in  std_logic_vector(31 downto 0);
  ch3_captured : in  std_logic_vector(31 downto 0);
  ch4_captured : in  std_logic_vector(31 downto 0);
  ch5_captured : in  std_logic_vector(31 downto 0);
  ch1_out      : out std_logic_vector(31 downto 0);
  ch2_out      : out std_logic_vector(31 downto 0);
  ch3_out      : out std_logic_vector(31 downto 0);
  ch4_out      : out std_logic_vector(31 downto 0);
  ch5_out      : out std_logic_vector(31 downto 0)
);
	
end FE_data;

architecture rtl of FE_data is

component pacd is
port (
  iPulseA : IN  std_logic;
  iClkA   : IN  std_logic;
  iRSTAn  : IN  std_logic;
  iClkB   : IN  std_logic;
  iRSTBn  : IN  std_logic;
  oPulseB : OUT std_logic
);
end component pacd;
  
signal sel: integer;
signal ack: std_logic;

type slv16_array_t is array (integer range <>) of std_logic_vector(31 downto 0);
type slv4_array_t is array (integer range <>) of std_logic_vector(3 downto 0);

signal ch_fake_gen : std_logic_vector(31 downto 0) := (others => '0');
signal ch_VFE      : slv16_array_t(5 downto 1)     := (others => (others => '0'));
signal ch_val      : slv16_array_t(5 downto 1)     := (others => (others => '0'));
signal ch_out      : slv16_array_t(5 downto 1)     := (others => (others => '0'));
signal output_mode : slv4_array_t(5 downto 1)      := (others => (others => '0'));
-- To transmit 32 bits, we need 4 clock (160 MHz)
-- But for base line, we transmit 5 samples
-- So, either we send frame_deliliter+baeline_data1+basline_data1+baseline_data1+baseline_data1 = 20 samples in 5*4=20 clocks -> OK
--     or we send baseline_data2 with 4 samples = 4 samples in 4 clocks
constant BASELINE_DATA_1      : std_logic_vector(31 downto 0) := x"45103081"; -- 01 000101 000100 000011 000010 000001
constant BASELINE_DATA_2      : std_logic_vector(31 downto 0) := x"8100003f"; -- 10 000001 000000 000000 000000 111111

-- For signal_data1, we send 2 samples in 4 clocks
constant SIGNAL_DATA_1        : std_logic_vector(31 downto 0) := x"281fe07f"; -- 001010 0000011111111 0000001111111
-- For signal_data2, we send 1 samples in 4 clocks
constant SIGNAL_DATA_2        : std_logic_vector(31 downto 0) := x"2b5541ff"; -- 001011 0101010101010 0000111111111
constant FRAME_DELIMITER      : std_logic_vector(31 downto 0) := x"D14AAA00"; -- 1101 00010100 101010101010 00000000
constant CHANNEL_IDLE_PATTERN : std_logic_vector(31 downto 0) := x"EAAAAAAA"; -- 1110 1010101010101010101010101010
  
begin

 Inst_FE_data : if not FOR_SEU generate -- Reduce power consumption for SEU tests
 
--Generate fake data for channels
  FE_Fake_Channel_1: entity work.FE_Fake_Channel
  port map (
    clk       => clk,
    reset     => reset,
    fake_mode => 1,
    channel   => ch_fake_gen
  );

--remap module I/Os to arrays
  ch_VFE(1) <= ch1_captured;
  ch_VFE(2) <= ch2_captured;
  ch_VFE(3) <= ch3_captured;
  ch_VFE(4) <= ch4_captured;
  ch_VFE(5) <= ch5_captured;
  ch1_out   <= ch_out(1);
  ch2_out   <= ch_out(2);
  ch3_out   <= ch_out(3);
  ch4_out   <= ch_out(4);
  ch5_out   <= ch_out(5);       

  --Mux for mapping data sources to the output channels to FE
  FE_output_mux: process (output_mode,ch_fake_gen,ch_val,ch_VFE,ch_out) is
  begin  -- process FE_output_mux
    for iChannel in 5 downto 1 loop
      case output_mode(iChannel) is
        when x"0" =>
          ch_out(iChannel) <= CHANNEL_IDLE_PATTERN;
        when x"1" =>
          ch_out(iChannel) <= ch_VFE(iChannel);
        when x"2" =>
          ch_out(iChannel) <= ch_fake_gen;
        when x"3" =>
          ch_out(iChannel) <= ch_val(iChannel);
        when others =>
          ch_out(iChannel) <= ch_fake_gen;
--          ch_out(iChannel) <= ch_VFE(iChannel);
--          ch_out(iChannel) <= CHANNEL_IDLE_PATTERN;
      end case;      
    end loop;  -- iChannel
  end process FE_output_mux;
  
  end generate;
  
  --This process processes ipbus read/writes
  address_decode_proc: process(clk_ipbus,reset)
  begin
    if reset='1' then
      output_mode <= (others => (others => '0'));
      ack <= '0';
    elsif rising_edge(clk_ipbus) then
      --write register
      if ipbus_in.ipb_strobe = '1' and ipbus_in.ipb_write = '1' then
        case ipbus_in.ipb_addr(15 downto 13) is
          when  "000" =>
            --control registers
            case ipbus_in.ipb_addr(12 downto 0) is
              when "0"&x"000" =>
                output_mode(1) <= ipbus_in.ipb_wdata( 3 downto  0);
                output_mode(2) <= ipbus_in.ipb_wdata( 7 downto  4);
                output_mode(3) <= ipbus_in.ipb_wdata(11 downto  8);
                output_mode(4) <= ipbus_in.ipb_wdata(15 downto 12);
                output_mode(5) <= ipbus_in.ipb_wdata(19 downto 16);
              when "0"&x"001" =>
                ch_val(1) <= ipbus_in.ipb_wdata(31 downto 0);
              when "0"&x"002" =>
                ch_val(2) <= ipbus_in.ipb_wdata(31 downto 0);
              when "0"&x"003" =>
                ch_val(3) <= ipbus_in.ipb_wdata(31 downto 0);
              when "0"&x"004" =>
                ch_val(4) <= ipbus_in.ipb_wdata(31 downto 0);
              when "0"&x"005" =>
                ch_val(5) <= ipbus_in.ipb_wdata(31 downto 0);
              when others => null;                       
            end case;
          when others => null;
        end case;        
      end if;

      --read register
      ipbus_out.ipb_rdata <= (others => '0'); 

      case ipbus_in.ipb_addr(15 downto 13) is
        --Addresses for reading of data
        when  "000" =>
          --control registers
          case ipbus_in.ipb_addr(12 downto 0) is
            when "0"&x"000" =>
              ipbus_out.ipb_rdata <= (others => '0');
              ipbus_out.ipb_rdata( 3 downto  0) <= output_mode(1);
              ipbus_out.ipb_rdata( 7 downto  4) <= output_mode(2);
              ipbus_out.ipb_rdata(11 downto  8) <= output_mode(3);
              ipbus_out.ipb_rdata(15 downto 12) <= output_mode(4);
              ipbus_out.ipb_rdata(19 downto 16) <= output_mode(5);              
            when "0"&x"001" =>
              ipbus_out.ipb_rdata(31 downto 0) <= ch_val(1);
            when "0"&x"002" =>
              ipbus_out.ipb_rdata(31 downto 0) <= ch_val(2);
            when "0"&x"003" =>
              ipbus_out.ipb_rdata(31 downto 0) <= ch_val(3);
            when "0"&x"004" =>
              ipbus_out.ipb_rdata(31 downto 0) <= ch_val(4);
            when "0"&x"005" =>
              ipbus_out.ipb_rdata(31 downto 0) <= ch_val(5);

            when others => ipbus_out.ipb_rdata <= x"DEADBEEF";                       
          end case;
        when others => ipbus_out.ipb_rdata <= x"DEADBEEF";
      end case;
                           
      --ipbus reply acknowledge
      ack <= ipbus_in.ipb_strobe and not ack;                     
    end if;
  end process address_decode_proc;
  
  ipbus_out.ipb_ack <= ack;
  ipbus_out.ipb_err <= '0';
  
end rtl;
