library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity FE_Fake_Channel is
port (
  clk       : in  std_logic;
  reset     : in  std_logic;
  fake_mode : in  integer range 1 downto 0 := 0;
  channel   : out std_logic_vector(31 downto 0)
);

end entity FE_Fake_Channel;

architecture BEHAVIORAL of FE_Fake_Channel is
  signal counter : unsigned(31 downto 0) := (others => '0');
--  signal cylon : std_logic_vector(13 downto 0) :=  "00"&x"001";
--  signal cylon_move_right : std_logic := '0';
begin

  -- Counter for testing purposes. 
  counter_proc: process (clk, reset) is
  begin  -- process counter_proc
    if reset = '1' then                 -- asynchronous reset (active high)
      counter <= (others => '0');                 
    elsif clk'event and clk = '1' then  -- rising clock edge
      counter <= counter + 1;
    end if;
  end process counter_proc;

--  Additional "cylon" ping-pong '1' pattern
--  cylon_proc: process (clk, reset) is
--  begin  -- process cylon_proc
--    if reset = '1' then                 -- asynchronous reset (active high)
--      cylon <= "00"&x"001";
--    elsif clk'event and clk = '1' then  -- rising clock edge
--      if cylon(15) = '1' then
--        cylon_move_right <= '1';
--      elsif cylon(0) = '1' then
--        cylon_move_right <= '0';
--      else
--        
--      end if;
--      
--      if cylon_move_right = '1' then
--        for iBit in 14 downto 0 loop
--          cylon(iBit) <= cylon(iBit+1);          
--        end loop;  -- iBit
--        cylon(15) <= '0';
--      else
--        for iBit in 15 downto 1 loop
--          cylon(iBit-1) <= cylon(iBit);          
--        end loop;  -- iBit
--        cylon(15) <= '0';
--      end if;
--    end if;
--  end process cylon_proc;

  -- output test patterns set by fake_mode
  fake_data: process (clk, reset) is
  begin  -- process fake_data
    if reset = '1' then                 -- asynchronous reset (active high)
      channel <= (others => '0');                 
    elsif clk'event and clk = '1' then  -- rising clock edge
      case fake_mode is
        when 1 =>
          --Simple counter
          channel <= std_logic_vector(counter);
        when 0 =>
          --zero 
          channel <= (others => '0');
        when others =>
          --zero
          channel <= (others => '0');
      end case;
    end if;
  end process fake_data;
end BEHAVIORAL;
  
