library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.FEAD_IO.all;

library UNISIM;
use UNISIM.VComponents.all;

entity CATIA_ctrl is
  port (
    I2C_Access        : in     std_logic;
    I2C_R_Wb          : in     std_logic;
    I2C_Device_number : in     std_logic_vector(6 downto 0);
    I2C_Reg_number    : in     std_logic_vector(6 downto 0);
    I2C_long_transfer : in     std_logic;
    I2C_Bulky         : in     std_logic;
    I2C_Bulk_length   : in     natural range 0 to 19;
    I2C_Bulk_data_in  : in     Byte_t(18 downto 0);

    busy_out          : out    std_logic;
    I2C_error         : out    std_logic;
    I2C_Reg_data_out  : out    std_logic_vector(15 downto 0);
    I2C_n_ack         : out    unsigned(7 downto 0);
    I2C_Bulk_data_out : out    Byte_t(18 downto 0);
    
    reset             : in     std_logic;
    I2C_scl           : inout  std_logic;
    I2C_sda           : inout  std_logic;
    I2C_clk           : in     std_logic
--    I2C_ack_spy       : buffer std_logic_vector(N_I2C_spy_bits-1 downto 0)
  );
end entity CATIA_ctrl;

architecture rtl of CATIA_ctrl is

  component I2C_master is
  port(
    clk       : IN     STD_LOGIC;                    --system clock
    reset_n   : IN     STD_LOGIC;                    --active low reset
    ena       : IN     STD_LOGIC;                    --latch in command
    addr      : IN     STD_LOGIC_VECTOR(6 DOWNTO 0); --address of target slave
    R_Wb      : IN     STD_LOGIC;                    --'0' is write, '1' is read
    data_w    : IN     STD_LOGIC_VECTOR(7 DOWNTO 0); --data to write to slave
    busy      : OUT    STD_LOGIC;                    --indicates transaction in progress
    data_r    : OUT    STD_LOGIC_VECTOR(7 DOWNTO 0); --data read from slave
    ack_error : BUFFER STD_LOGIC;                    --flag if improper acknowledge from slave
    slave_ack : OUT    STD_LOGIC;                    --acknowledge from slave
    sda       : INOUT  STD_LOGIC;                    --serial data output of i2c bus
    scl       : INOUT  STD_LOGIC;                     --serial clock output of i2c bus
    scl_int   : OUT    STD_LOGIC                     --Internal serial clock output of i2c bus
  );
  end component I2C_master;

  signal I2C_long               : STD_LOGIC := '0';             -- short (8 bits = 0) or long (16 bits = 1) transfer
  signal I2C_last_transfer      : STD_LOGIC := '1';             -- last transfer (1) or not (0) of data (1 or 2 byte transfers)
  signal I2C_reset_n            : STD_LOGIC;                    -- active low reset
  signal I2C_scl_int            : STD_LOGIC;                    -- 
  signal I2C_scl_int_del        : STD_LOGIC;
  signal I2C_ena                : STD_LOGIC;                    -- 
  signal I2C_ena_del            : STD_LOGIC;
  signal I2C_addr               : STD_LOGIC_VECTOR(6 DOWNTO 0); -- address of target slave
  signal I2C_R_Wb_loc           : STD_LOGIC;                    -- '0' is write, '1' is read
  signal I2C_data_w             : STD_LOGIC_VECTOR(7 DOWNTO 0); -- data to write to slave
  signal I2C_data_r             : STD_LOGIC_VECTOR(15 DOWNTO 0); -- data read from slave
  signal I2C_loc_busy           : STD_LOGIC;                    -- indicates transaction in progress
  signal I2C_loc_busy_prev      : STD_LOGIC;                    -- mandatory to detect busy transiton
  signal I2C_loc_data_r         : STD_LOGIC_VECTOR(7 DOWNTO 0); -- data read from slave
  signal I2C_ack_error          : STD_LOGIC;                    -- flag if improper acknowledge from slave
  signal I2C_slave_ack          : STD_LOGIC;                    -- flag if improper acknowledge from slave
--  signal loc_I2C_ack_spy        : std_logic_vector(N_I2C_spy_bits-1 downto 0);
  signal loc_I2C_n_ack          : unsigned(7 downto 0)          := (others => '0');
  signal CATIA_reset            : std_logic;
  signal I2C_max_Bytes          : natural range 0 to 19 := 1;  -- Number of bytes to transfer (1 I2C_long=0, 2 I2Clong=1,or 17 I2C_bulky_DTU)
  signal I2C_n_bytes            : natural range 0 to 19 := 0;  -- Running counter of transfered bytes for short/long/bulky tranfer

  type   I2C_state_type is (idle,
                            write_reg_address, wait_for_busy_reg,
                            read_reg_data,     wait_for_busy_read,
                            write_reg_data,    wait_for_busy_write);
  signal I2C_state     : I2C_state_type := idle;

begin  -- architecture behavioral

  CATIA_reset                    <= reset;
  I2C_reset_n                    <= not CATIA_reset;
  I2C_error                      <= I2C_ack_error;
  I2C_Reg_data_out               <= x"00"&I2C_data_r(7 downto 0) when I2C_long='0' else
                                    I2C_data_r(7 downto 0)&I2C_data_r(15 downto 8); -- With long transfer, read msB first then lsB

  Inst_I2C_master : I2C_master
  port map(
    clk             => I2C_clk,             --system clock
    reset_n         => I2C_reset_n,         --active low reset
    ena             => I2C_ena,             --latch in command
    addr            => I2C_addr,            --address of target slave
    R_Wb            => I2C_R_Wb_loc,        --'0' is write, '1' is read
    data_w          => I2C_data_w,          --data to write to slave
    busy            => I2C_loc_busy,        --indicates transaction in progress
    data_r          => I2C_loc_data_r,      --data read from slave
    ack_error       => I2C_ack_error,       --flag if improper acknowledge from slave
    slave_ack       => I2C_slave_ack,       -- acknowledge from slave
    sda             => I2C_sda,             --serial data output of i2c bus
    scl             => I2C_scl,             --serial clock output of i2c bus
    scl_int         => I2C_scl_int          --internal serial clock output of i2c bus
  );

  program_I2c : process(CATIA_reset, I2C_Access, I2C_clk, I2C_loc_busy)
  begin
    if CATIA_reset = '1' then
      I2C_state                      <= idle;
      I2C_long                       <= '0';
      I2C_ena                        <= '0';
      busy_out                       <= '0';
      I2C_loc_busy_prev              <= '0';
      I2C_n_bytes                    <= 0;
      I2C_max_bytes                  <= 1;
    elsif Rising_Edge(I2C_clk) then
      case I2C_state is
      when idle =>
--        I2C_ack_spy                  <= loc_I2C_ack_spy;
        I2C_n_ack                    <= loc_I2C_n_ack;
        busy_out                     <= '0';
        I2C_ena                      <= '0';
        if I2C_Access = '1' then
          I2C_long                   <= I2C_long_transfer;
          I2C_R_Wb_loc               <= '0';                                 -- first write register address
          I2C_addr                   <= I2C_Device_number;                   -- Device number
          I2C_data_w                 <= "0"&I2C_Reg_number;      -- Register address
          I2C_state                  <= wait_for_busy_reg;                   -- Synchronize with I2C clock (slower)
          I2C_data_r                 <= (others => '0');
          I2C_ena                    <= '1';                                 -- Initiate the transaction (I2C master latch address and data)
          I2C_n_bytes                <=  0;
          if I2C_Bulky = '1' then
            I2C_max_bytes            <= I2C_Bulk_length;
          elsif I2C_long_transfer = '1' then
            I2C_max_bytes            <= 2;
          else
            I2C_max_bytes            <= 1;
          end if;
        end if;
      when wait_for_busy_reg =>                                              -- Wait for I2C master to become busy since its clock is slower
        busy_out                     <= '1';                                 -- Stay busy during all the transaction
        if I2C_loc_busy = '1' then
          I2C_state                  <= write_reg_address;
        end if;
      when write_reg_address =>                                              -- Chip/reg addresses have been latched. Prepare next transaction during first write
        I2C_addr                     <= I2C_Device_number;                   -- Put back Device address and add transaction type (read/write)
        I2C_R_Wb_loc                 <= I2C_R_Wb;
        I2C_data_w                   <= I2C_Bulk_data_in(I2C_n_bytes);
--        if I2C_long = '1' then                                               -- Prepare data on bus in case of write (by he way, we do it always...)
--          I2C_last_transfer          <= '0';                                 -- In case of long transfer, start with MSB
--          I2C_data_w                 <= I2C_Reg_data(15 downto 8);      
--        else
--          I2C_last_transfer          <= '1';
--          I2C_data_w                 <= I2C_Reg_data(7 downto 0);-- In case of short ransfer, start with LSB
--        end if;
        if I2C_loc_busy = '0' then                                           -- Chip/reg adress write is finished
          if I2C_R_Wb = '0' then
            I2C_state                <= wait_for_busy_write;
          else
            I2C_state                <= wait_for_busy_read;
          end if;
        end if;
      when wait_for_busy_read =>                                             -- Wait for I2C master to become busy since its clock is slower
        if I2C_loc_busy = '1' then
          I2C_state                  <= read_reg_data;
        end if;
      when read_reg_data =>
        if I2C_n_bytes = I2C_max_bytes-1 then
          I2C_ena                                  <= '0';                            -- Deassert enable to stop transaction after this read
        else
          I2C_ena                                  <= '1';                            -- Continue transaction for bulky read
        end if;
--        if I2C_last_transfer = '1' then
--           I2C_ena                   <= '0';                                 -- Deassert enable to stop transaction after this read
--        end if;
        if I2C_loc_busy = '0' and I2C_loc_busy_prev = '1' then               -- byte read finished
          I2C_Bulk_data_out(I2C_n_bytes)                 <= I2C_loc_data_r;
          if I2C_n_bytes = 0 then
            I2C_data_r(7 downto 0)                       <= I2C_loc_data_r;
          elsif I2C_n_bytes = 1 then
            I2C_data_r(15 downto 8)                      <= I2C_loc_data_r;
          end if;
          if I2C_n_bytes = I2C_max_bytes-1 then
            I2C_state                                    <= idle;                 -- End of transaction : 1 or 2 bytes read
          end if;
          I2C_n_bytes                                    <= I2C_n_bytes+1;
--          if I2C_last_transfer = '0' then
--            I2C_data_r(15 downto 8)  <= I2C_loc_data_r;
--            I2C_last_transfer        <= '1';
--          else
--            I2C_data_r(7 downto 0)   <= I2C_loc_data_r;
--            I2C_state                <= idle;                                -- End of transaction : 1 or 2 bytes written
--          end if;
        end if;
      when wait_for_busy_write =>                                            -- Wait for I2C master to become busy since its clock is slower
        if I2C_loc_busy = '1' then
          I2C_state                               <= write_reg_data;
        end if;
      when write_reg_data =>
        if I2C_n_bytes = I2C_max_bytes-1 then
          I2C_ena                                  <= '0';                            -- Deassert enable to stop transaction after this write
        else
          I2C_ena                                  <= '1';                            -- Continue transaction for bulky write
          I2C_data_w                               <= I2C_Bulk_data_in(I2C_n_bytes+1);-- prepare data for I2C master for next write at same address
        end if;
--        if I2C_last_transfer = '1' then
--          I2C_ena                    <= '0';                                 -- Deassert enable to stop transaction after this last write
--        else
--          I2C_data_w                 <= I2C_Reg_data(7 downto 0);-- prepare data for I2C master for second write during first write (if needed)
--        end if;
        if I2C_loc_busy = '0' and I2C_loc_busy_prev = '1' then               -- byte write finished
          if I2C_n_bytes = I2C_max_bytes-1 then                              -- We have finished the bulk transaction
            I2C_state                              <= idle;                  -- End of transaction : 1 or 2 bytes written         
          end if;
          I2C_n_bytes                              <= I2C_n_bytes+1;
--          if I2C_last_transfer = '0' then
--            I2C_last_transfer        <= '1';
--          else
--            I2C_state                <= idle;                                -- End of transaction : 1 or 2 bytes written         
--          end if;
        end if;
      end case;
      I2C_loc_busy_prev <= I2C_loc_busy;
    end if;
  end process program_I2C;
  
  I2C_scl_int_del <= I2C_scl_int when rising_edge(I2C_clk) else I2C_scl_int_del;
  proc_I2C_spy : process (reset, I2C_clk, I2C_scl_int, I2C_scl_int_del, I2C_state) is
  begin
    if reset = '1' or I2C_state = wait_for_busy_reg then
      loc_I2C_n_ack   <= (others => '0');
    elsif rising_edge(I2C_clk) then
      if I2C_SCL_int='1' and I2C_scl_int_del='0' and I2C_slave_ack = '1' then
        loc_I2C_n_ack <= loc_I2C_n_ack+1;
      end if;

    end if; 
  end process proc_I2C_spy;

end architecture rtl;
