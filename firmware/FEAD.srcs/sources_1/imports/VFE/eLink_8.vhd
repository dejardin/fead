library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.FEAD_IO.all;

library UNISIM;
use UNISIM.VComponents.all;

entity eLink_8 is
generic
(
  DEV_W : integer := 8  -- width of the data for the device
);
port
(
  data_in_from_pins_p : in  std_logic;
  data_in_from_pins_n : in  std_logic;
  data_in_to_device   : out std_logic_vector(DEV_W-1 downto 0);
  in_delay_reset      : in  std_logic;                           -- Active high synchronous reset for input delay
  in_delay_data_ce    : in  std_logic;                           -- Enable signal for delay
  in_delay_data_inc   : in  std_logic;                           -- Delay increment (high), decrement (low) signal
  bitslip             : in  std_logic;                           -- Bitslip module is enabled in NETWORKING mode
                                                                 -- User should tie it to '0' if not needed
  clk_in              : in  std_logic;                           -- Fast clock input from PLL/MMCM
  clk_inb             : in  std_logic;                           -- Fast clock input from PLL/MMCM
  clk_div_in          : in  std_logic;                           -- Slow clock input from PLL/MMCM
  io_reset            : in  std_logic
);
end entity eLink_8;

architecture rtl of eLink_8 is

--  component selectio_eLink_8_selectio_wiz is
--  port (
--    data_in_from_pins_p : in STD_LOGIC;
--    data_in_from_pins_n : in STD_LOGIC;
--    data_in_to_device   : out STD_LOGIC_VECTOR(7 downto 0);
--    in_delay_reset      : in STD_LOGIC;                        -- Active high synchronous reset for input delay
--    in_delay_data_ce    : in STD_LOGIC;                        -- Enable signal for delay
--    in_delay_data_inc   : in STD_LOGIC;                        -- Delay increment (high), decrement (low) signal
--    bitslip             : in STD_LOGIC;                        -- Bitslip module is enabled in NETWORKING mode. User should tie it to '0' if not needed
--    --ibufdisable         : in STD_LOGIC;
--    clk_in              : in STD_LOGIC;                        -- Fast clock input from PLL/MMCM
--    clk_inb             : in STD_LOGIC;                        -- Fast clock input from PLL/MMCM
--    clk_div_in          : in STD_LOGIC;                        -- Slow clock input from PLL/MMCM
--    io_reset            : in STD_LOGIC
--  );
--  end component selectio_eLink_8_selectio_wiz;

  signal data_in_from_pins_int   : std_logic;
  signal data_in_from_pins_delay : std_logic;
  signal iserdes_q               : std_logic_vector(7 downto 0);
  signal clock_enable            : std_logic := '1';
  
  begin

--  inst_eLink_8 : selectio_eLink_8_selectio_wiz 
--  port map(
--    data_in_from_pins_p    => data_in_from_pins_p,
--    data_in_from_pins_n    => data_in_from_pins_n,
--    data_in_to_device      => data_in_to_device,
--    in_delay_reset         => in_delay_reset,
--    in_delay_data_ce       => in_delay_data_ce,
--    in_delay_data_inc      => in_delay_data_inc,
--    bitslip                => bitslip,
--    clk_in                 => clk_in,
--    clk_inb                => clk_inb,
--    clk_div_in             => clk_div_in,
--    io_reset               => io_reset
--  );

  inst_ibufds : IBUFDS
  generic map 
  (
    DIFF_TERM  => FALSE,             -- Differential termination
    IOSTANDARD => "DIFF_SSTL12_DCI"
  )
  port map
  (
    I          => data_in_from_pins_p,
    IB         => data_in_from_pins_n,
    O          => data_in_from_pins_int
  );

  inst_idelay : IDELAYE2
  generic map
  (
    CINVCTRL_SEL           => "FALSE",    -- TRUE, FALSE
    DELAY_SRC              => "IDATAIN",  -- IDATAIN, DATAIN
    HIGH_PERFORMANCE_MODE  => "FALSE",    -- TRUE, FALSE
    IDELAY_TYPE            => "VARIABLE", -- FIXED, VARIABLE, or VAR_LOADABLE
    IDELAY_VALUE           => 0,          -- 0 to 31
    REFCLK_FREQUENCY       => 200.0,
    PIPE_SEL               => "FALSE",
    SIGNAL_PATTERN         => "DATA"      -- CLOCK, DATA
  )
  port map
  (
    DATAOUT                => data_in_from_pins_delay,
    DATAIN                 => '0',                     -- Data from FPGA logic
    C                      => clk_div_in,
    CE                     => in_delay_data_ce,        -- in_delay_data_ce),
    INC                    => in_delay_data_inc,       -- in_delay_data_inc),
    IDATAIN                => data_in_from_pins_int,   -- Driven by IOB
    LD                     => in_delay_reset,
    REGRST                 => io_reset,
    LDPIPEEN               => '0',
    CNTVALUEIN             => "00000",
    CNTVALUEOUT            => open,
    CINVCTRL               => '0'
  );

  inst_iserdes :  ISERDESE2
  generic map
  (
    DATA_RATE         => "DDR",
    DATA_WIDTH        => 8,
    INTERFACE_TYPE    => "NETWORKING", 
    DYN_CLKDIV_INV_EN => "FALSE",
    DYN_CLK_INV_EN    => "FALSE",
    NUM_CE            => 2,
    OFB_USED          => "FALSE",
    IOBDELAY          => "IFD",                    -- Use input at DDLY to output the data on Q
    SERDES_MODE       => "MASTER"
  )
  port map
  (
    Q1                => iserdes_q(0),
    Q2                => iserdes_q(1),
    Q3                => iserdes_q(2),
    Q4                => iserdes_q(3),
    Q5                => iserdes_q(4),
    Q6                => iserdes_q(5),
    Q7                => iserdes_q(6),
    Q8                => iserdes_q(7),
    SHIFTOUT1         => open,
    SHIFTOUT2         => open,
    BITSLIP           => bitslip,                  -- 1-bit Invoke Bitslip. This can be used with any DATA_WIDTH, cascaded or not.
                                                   -- The amount of BITSLIP is fixed by the DATA_WIDTH selection.
    CE1               => clock_enable,             -- 1-bit Clock enable input
    CE2               => clock_enable,             -- 1-bit Clock enable input
    CLK               => clk_in,                   -- Fast clock driven by MMCM
    CLKB              => clk_inb,                  -- Locally inverted fast 
    CLKDIV            => clk_div_in,               -- Slow clock from MMCM
    CLKDIVP           => '0',
    D                 => '0',                      -- 1-bit Input signal from IOB
    DDLY              => data_in_from_pins_delay,  -- 1-bit Input from Input Delay component 
    RST               => io_reset,                 -- 1-bit Asynchronous reset only.
    SHIFTIN1          => '0',
    SHIFTIN2          => '0',
-- unused connections
    DYNCLKDIVSEL      => '0',
    DYNCLKSEL         => '0',
    OFB               => '0',
    OCLK              => '0',
    OCLKB             => '0',
    O                 => open                      -- unregistered output of ISERDESE1
  );
  data_in_to_device(0) <=  iserdes_q(7);
  data_in_to_device(1) <=  iserdes_q(6);
  data_in_to_device(2) <=  iserdes_q(5);
  data_in_to_device(3) <=  iserdes_q(4);
  data_in_to_device(4) <=  iserdes_q(3);
  data_in_to_device(5) <=  iserdes_q(2);
  data_in_to_device(6) <=  iserdes_q(1);
  data_in_to_device(7) <=  iserdes_q(0);
end architecture rtl; 

