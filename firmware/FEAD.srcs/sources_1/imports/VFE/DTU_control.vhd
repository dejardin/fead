library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use work.FEAD_IO.all;

library UNISIM;
use UNISIM.VComponents.all;

entity DTU_ctrl is
  port (
    I2C_Access        : in     std_logic;
    I2C_R_Wb          : in     std_logic;
    I2C_Device_number : in     std_logic_vector(6 downto 0);
    I2C_Reg_number    : in     std_logic_vector(6 downto 0);
    I2C_long_transfer : in     std_logic;
    I2C_max_retry     : in     unsigned(13 downto 0);
    I2C_toggle_SDA    : in     std_logic;
    I2C_Bulky         : in     std_logic;
    I2C_Bulk_length   : in     natural range 0 to 19;
    I2C_Bulk_data_in  : in     Byte_t(18 downto 0);

    busy_out          : out    std_logic;
    I2C_error         : out    std_logic;
    I2C_n_ack         : out    unsigned(7 downto 0);
    I2C_retry_counter : out    unsigned(13 downto 0);
    I2C_Reg_data_out  : out    std_logic_vector(15 downto 0);
    I2C_Bulk_data_out : out    Byte_t(18 downto 0); 
    
    ADC_start_Resync  : in     std_logic;
    ADC_invert_Resync : in     std_logic;
    ADC_ReSync_idle   : in     std_logic_vector(7 downto 0);
    ADC_ReSync_data   : in     std_logic_vector(7 downto 0);
    ADC_Hamming_data  : in     std_logic_vector(23 downto 0);

    reset             : in     std_logic;
    I2C_scl           : inout  std_logic;
    I2C_sda           : inout  std_logic;
    I2C_clk           : in     std_logic;
--    I2C_ack_spy       : buffer std_logic_vector(N_I2C_spy_bits-1 downto 0);
    clk_160           : in     std_logic;
    ReSync_DTU        : out    std_logic
  );
end entity DTU_ctrl;

architecture rtl of DTU_ctrl is

  component I2C_master_SDAb is
  port(
    toggle_SDA: IN     STD_LOGIC;                    -- Toggle SDA line during idle time
    clk       : IN     STD_LOGIC;                    --system clock
    reset_n   : IN     STD_LOGIC;                    --active low reset
    ena       : IN     STD_LOGIC;                    --latch in command
    addr      : IN     STD_LOGIC_VECTOR(6 DOWNTO 0); --address of target slave
    R_Wb      : IN     STD_LOGIC;                    --'0' is write, '1' is read
    data_w    : IN     STD_LOGIC_VECTOR(7 DOWNTO 0); --data to write to slave
    busy      : OUT    STD_LOGIC;                    --indicates transaction in progress
    data_r    : OUT    STD_LOGIC_VECTOR(7 DOWNTO 0); --data read from slave
    ack_error : BUFFER STD_LOGIC;                    --flag if improper acknowledge from slave
    slave_ack : OUT    STD_LOGIC;                    --acknowledge from slave
    sda       : INOUT  STD_LOGIC;                    --serial data output of i2c bus
    scl       : INOUT  STD_LOGIC);                   --serial clock output of i2c bus
  end component I2C_master_SDAb;

  signal I2C_long               : STD_LOGIC := '0';             -- short (8 bits = 0) or long (16 bits = 1) transfer
  signal I2C_last_transfer      : STD_LOGIC := '1';             -- last transfer (1) or not (0) of data (1 or 2 byte transfers)
--  signal I2C_max_Bytes          : unsigned(7 downto 0) := x"01";-- Number of bytes to transfer (1 I2C_long=0, 2 I2Clong=1,or 17 I2C_bulky_DTU)
--  signal I2C_n_bytes            : unsigned(7 downto 0) := x"00";-- Running counter of transfered bytes for short/long/bulky tranfer
  signal I2C_max_Bytes          : natural range 0 to 19 := 1;  -- Number of bytes to transfer (1 I2C_long=0, 2 I2Clong=1,or 17 I2C_bulky_DTU)
  signal I2C_n_bytes            : natural range 0 to 19 := 0;-- Running counter of transfered bytes for short/long/bulky tranfer
  signal I2C_reset_n            : STD_LOGIC;                    -- active low reset
  signal I2C_ena                : STD_LOGIC;                    -- 
  signal I2C_addr               : STD_LOGIC_VECTOR(6 DOWNTO 0); -- address of target slave
  signal I2C_R_Wb_loc           : STD_LOGIC;                    -- '0' is write, '1' is read
  signal I2C_data_w             : STD_LOGIC_VECTOR(7 DOWNTO 0); -- data to write to slave
  signal I2C_loc_busy           : STD_LOGIC;                    -- indicates transaction in progress
  signal I2C_loc_busy_prev      : STD_LOGIC;                    -- mandatory to detect busy transiton
  signal I2C_loc_data_r         : STD_LOGIC_VECTOR(7 DOWNTO 0); -- data read from slave
  signal I2C_ack_error          : STD_LOGIC;                    -- flag if improper acknowledge from slave
  signal I2C_slave_ack          : STD_LOGIC;                    -- flag if improper acknowledge from slave
  signal DTU_reset              : std_logic;
  signal ReSync_busy            : std_logic := '0';
  signal I2C_busy               : std_logic := '0';
  signal I2C_ack_seen           : std_logic := '0';
  signal I2C_scl_del            : std_logic := '0';
  signal I2C_n_ack_loc          : unsigned(7 downto 0)          := (others =>'0');
--  signal loc_I2C_ack_spy        : std_logic_vector(N_I2C_spy_bits-1 downto 0);
  signal retry_counter          : unsigned(13 downto 0)         := (others => '0');
  signal retry_delay            : unsigned(15 downto 0)         := (others => '0'); -- Delay between 2 I2C retries

  signal ADC_Start_ReSync_loc   : std_logic := '0';
  signal ADC_Start_ReSync_del   : std_logic := '0';
  signal ADC_ReSync_transfer    : unsigned(5 downto 0)          := (others => '0');
  signal ADC_ReSync_stream      : std_logic_vector(39 downto 0) := (others => '0');
  signal Idle_ReSync_data       : std_logic_vector(7 downto 0)  := x"00";
  signal ReSync_DTU_loc         : std_logic := '0';
  type   I2C_state_type is (idle,retry,
                            write_reg_address, wait_for_busy_reg,
                            read_reg_data,     wait_for_busy_read,
                            write_reg_data,    wait_for_busy_write);
  signal I2C_state     : I2C_state_type := idle;

  type   ReSync_state_type is (ReSync_idle, ReSync_start);
  signal ReSync_state     : ReSync_state_type := ReSync_idle;

begin  -- architecture behavioral
  I2C_n_ack                      <= I2C_n_ack_loc;
  busy_out                       <= I2C_busy or Resync_Busy;
--  VFE_monitor.I2C_reg_data_DTU   <= VFE_monitor.I2C_Bulk_data(1)&VFE_monitor.I2C_Bulk_data(0);
  ReSync_DTU                     <= ReSync_DTU_loc;
  DTU_reset                      <= reset;
  I2C_reset_n                    <= not DTU_reset;

  Inst_I2C_master_DTU : I2C_master_SDAb
--  Inst_I2C_master_DTU : I2C_master
  port map(
    toggle_SDA      => I2C_toggle_SDA,      -- Toggle SDA line during idle time 
    clk             => I2C_clk,             --system clock
    reset_n         => I2C_reset_n,         --active low reset
    ena             => I2C_ena,             --latch in command
    addr            => I2C_addr,            --address of target slave
    R_Wb            => I2C_R_Wb_loc,        --'0' is write, '1' is read
    data_w          => I2C_data_w,          --data to write to slave
    busy            => I2C_loc_busy,        --indicates transaction in progress
    data_r          => I2C_loc_data_r,      --data read from slave
    ack_error       => I2C_ack_error,       --flag if improper acknowledge from slave
    slave_ack       => I2C_slave_ack,       -- acknowledge from slave
    sda             => I2C_sda,             --serial data output of i2c bus
    scl             => I2C_scl              --serial clock output of i2c bus
  );

  program_I2c : process(DTU_reset, I2C_Access, I2C_clk)
  begin
    if DTU_reset = '1' then
      I2C_state                                    <= idle;
      I2C_long                                     <= '0';
      I2C_ena                                      <= '0';
      I2C_busy                                     <= '0';
      I2C_loc_busy_prev                            <= '0';
      retry_counter                                <= (others => '0');
      retry_delay                                  <= (others => '0');
      I2C_retry_counter                            <= (others => '0');
      I2C_n_bytes                                  <= 0;
      I2C_max_bytes                                <= 1;
    elsif Rising_Edge(I2C_clk) then
      case I2C_state is
      when idle =>
--        I2C_ack_spy                                <= loc_I2C_ack_spy;
        I2C_busy                                   <= '0';
        I2C_ena                                    <= '0';
        if I2C_Access='1' or I2C_ack_seen='0' then                   -- Loop until we have a I2C ack signal !
--        if I2C_Access='1' then                                         -- Loop until we have a I2C ack signal !
          I2C_long                                 <= I2C_long_transfer;
          I2C_R_Wb_loc                             <= '0';                            -- first write register address
          I2C_addr                                 <= I2C_Device_number;  -- Device number ("1xxx000")
          I2C_data_w                               <= "0"&I2C_Reg_number; -- Register address
          I2C_state                                <= wait_for_busy_reg;              -- Synchronize with I2C clock (slower)
          I2C_ena                                  <= '1';                            -- Initiate the transaction (I2C master latch address and data)
          retry_counter                            <= retry_counter + 1;
          retry_delay                              <= x"00ff";                        -- Nb of clock to wait between 2 I2C retries
--          I2C_n_bytes                              <= x"00";
          I2C_n_bytes                              <=  0;
          I2C_error                                <= '0';
          if I2C_Bulky = '1' then
            I2C_max_bytes                          <= I2C_Bulk_length;
--            I2C_max_bytes                          <= 17;
          elsif I2C_long_transfer = '1' then
            I2C_max_bytes                          <= 2;
          else
            I2C_max_bytes                          <= 1;
          end if;
        end if;
        if I2C_ack_seen='1' then
          retry_counter <= (others => '0');
        end if;
      when wait_for_busy_reg =>                                                       -- Wait for I2C master to become busy since its clock is slower
        I2C_busy                                   <= '1';                            -- FEAD stays busy during all the transaction
        if I2C_loc_busy = '1' then
          I2C_state                                <= write_reg_address;
        end if;
      when write_reg_address =>                                                       -- Chip/reg addresses have been latched. Prepare next transaction during first write
        I2C_addr                                   <= I2C_Device_number;  -- Put back Device address and add transaction type (read/write)
        I2C_R_Wb_loc                               <= I2C_R_Wb;
--        I2C_data_w                                 <= I2C_Bulk_data_in(to_integer(I2C_n_bytes));
        I2C_data_w                                 <= I2C_Bulk_data_in(I2C_n_bytes);
        if I2C_loc_busy = '0' then                                                    -- Chip/reg adress write is finished
          if I2C_R_Wb = '0' then
            I2C_state                              <= wait_for_busy_write;
          else
            I2C_state                              <= wait_for_busy_read;
          end if;
        end if;
      when wait_for_busy_read =>                                                      -- Wait for I2C master to become busy since its clock is slower
        if I2C_loc_busy = '1' then
          I2C_state                                <= read_reg_data;
        end if;
      when read_reg_data =>
        if I2C_n_bytes = I2C_max_bytes-1 then
          I2C_ena                                  <= '0';                            -- Deassert enable to stop transaction after this read
        else
          I2C_ena                                  <= '1';                            -- Continue transaction for bulky read
        end if;
        if I2C_loc_busy = '0' and I2C_loc_busy_prev = '1' then                        -- byte read finished
--          VFE_Monitor.I2C_Bulk_data(to_integer(I2C_n_bytes))   <= I2C_loc_data_r;
          I2C_Bulk_data_out(I2C_n_bytes)           <= I2C_loc_data_r;
          if I2C_n_bytes = I2C_max_bytes-1 then
            I2C_state                              <= retry;                          -- End of transaction : 1 or 2 bytes read
          end if;
          I2C_n_bytes                              <= I2C_n_bytes+1;
        end if;
      when wait_for_busy_write =>                                                     -- Wait for I2C master to become busy since its clock is slower
        if I2C_loc_busy = '1' then
          I2C_state                                <= write_reg_data;
        end if;
      when write_reg_data =>
        if I2C_n_bytes = I2C_max_bytes-1 then
          I2C_ena                                  <= '0';                            -- Deassert enable to stop transaction after this write
        else
          I2C_ena                                  <= '1';                            -- Continue transaction for bulky write
--          I2C_data_w                               <= I2C_Bulk_data_in(to_integer(I2C_n_bytes+1));-- prepare data for I2C master for next write at same address
          I2C_data_w                               <= I2C_Bulk_data_in(I2C_n_bytes+1);-- prepare data for I2C master for next write at same address
        end if;
        if I2C_loc_busy = '0' and I2C_loc_busy_prev = '1' then                        -- byte write finished
          if I2C_n_bytes = I2C_max_bytes-1 then                                       -- We have finished the bulk transaction
            I2C_state                              <= retry;                          -- End of transaction : 1 or 2 bytes written         
          end if;
          I2C_n_bytes                              <= I2C_n_bytes+1;
        end if;
      when retry =>
        I2C_error                                  <= I2C_ack_error;
        I2C_retry_counter                          <= retry_counter;
        if I2C_ack_seen = '0' and retry_delay /= x"0000" then
          retry_delay                              <= retry_delay-1;
        else
          I2C_state                                <= idle;                           -- End of transaction : 1 or 2 bytes written
        end if;         
      end case;
      I2C_loc_busy_prev <= I2C_loc_busy;
    end if;
  end process program_I2C;
  
  I2C_scl_del <= I2C_scl when rising_edge(I2C_clk) else I2C_scl_del; 
  proc_I2C_spy : process (DTU_reset, I2C_state, I2C_scl, I2C_scl_del, I2C_slave_ack, retry_counter) is
  begin
    if DTU_reset = '1' or I2C_state = wait_for_busy_reg then
--      I2C_ack_spy    <= (others => '1');
      I2C_n_ack_loc  <= (others => '0');
      if DTU_reset = '1' then
        I2C_ack_seen <= '1';
      else 
        I2C_ack_seen <= '0';
      end if;
    elsif rising_edge(clk_160) then
      if I2C_scl='1'and I2C_scl_del='0' then
        if I2C_slave_ack = '1' or retry_counter >= I2C_max_retry then
          I2C_ack_seen <= '1';
        end if;
--      I2C_ack_spy    <= I2C_ack_spy(n_I2C_spy_bits-2 downto 0)  & I2C_slave_ack;
        if I2C_slave_ack = '1' then
          I2C_n_ack_loc<= I2C_n_ack_loc+1;
        end if;
      end if;
    end if; 
  end process proc_I2C_spy;
  
-- LiTE-DTU ReSync codes :
-- Code / Hamming encoding / Action
-- 0x0    0x00               stop 
-- 0x1    0x07               start 
-- 0x2    0x19               DTU reset 
-- 0x3    0x1E               I2C interface reset 
-- 0x4    0x2A               ADC TestUnit reset 
-- 0x8    0x4B               ADCH reset 
-- 0x9    0x4C               ADCH calibration 
-- 0xA    0x52               ADCL reset 
-- 0xB    0x55               ADCL calibration
  ADC_Start_ReSync_loc   <= ADC_Start_ReSync             when Rising_Edge(clk_160);
  ADC_Start_ReSync_del   <= ADC_Start_ReSync_loc         when Rising_Edge(clk_160);
  proc_ReSync : process (DTU_reset, ADC_Start_ReSync_loc,ADC_Start_ReSync_del, clk_160) is
  begin
    if DTU_reset='1' then
      ReSync_DTU_loc              <= '0';
      ADC_ReSync_stream           <= x"0000000000";
      Idle_ReSync_data            <= x"00";
      ReSync_state                <= ReSync_Idle; -- Send stop code after reset
      ADC_ReSync_transfer         <= (others => '1');
    elsif falling_edge(clk_160) then
      case ReSync_state is
        when ReSync_idle =>
          if ADC_invert_ReSync = '1' then
            ReSync_DTU_loc        <= not Idle_ReSync_data(7);
          else
            ReSync_DTU_loc        <= Idle_ReSync_data(7);
          end if;
          Idle_ReSync_data        <= Idle_ReSync_data(6 downto 0) & Idle_Resync_data(7);
--          ReSync_DTU_loc          <= '0';
          ReSync_busy             <= '0';
          if ADC_Start_ReSync_loc='1' and ADC_Start_ReSync_del='0' then
            ReSync_busy           <= '1';
            ReSync_state          <= ReSync_start;
            Idle_ReSync_data      <= ADC_ReSync_idle;
            ADC_ReSync_transfer   <= (others => '1');
            case ADC_ReSync_data is
            when x"00" =>  -- stop
              ADC_ReSync_stream   <= x"0000000000";
            when x"01" =>  -- start
              ADC_ReSync_stream   <= x"0007191E2A";
            when x"02" =>  -- DTU_reset
              ADC_ReSync_stream   <= x"000719"&ADC_ReSync_idle&ADC_ReSync_idle;
            when x"03" =>  -- I2C_reset
              ADC_ReSync_stream   <= x"00071E"&ADC_ReSync_idle&ADC_ReSync_idle;
            when x"04" =>  -- ADCTestUnit_reset
              ADC_ReSync_stream   <= x"00072A"&ADC_ReSync_idle&ADC_ReSync_idle;
            when x"08" =>  -- ADCH_reset
              ADC_ReSync_stream   <= x"00074B"&ADC_ReSync_idle&ADC_ReSync_idle;
            when x"09" =>  -- ADCH_calib
              ADC_ReSync_stream   <= x"00074C"&ADC_ReSync_idle&ADC_ReSync_idle;
            when x"0A" =>  -- ADCL_reset
              ADC_ReSync_stream   <= x"000752"&ADC_ReSync_idle&ADC_ReSync_idle;
            when x"0B" =>  -- ADCL_calib
              ADC_ReSync_stream   <= x"000755"&ADC_ReSync_idle&ADC_ReSync_idle;
            when x"0C" =>  -- ADCH and ADCL reset
              ADC_ReSync_stream   <= x"00074B52"&ADC_ReSync_idle;
            when x"0D" =>  -- ADCH and ADCL calib
              ADC_ReSync_stream   <= x"00074C55"&ADC_ReSync_idle;
            when x"0F" =>  -- ADCH and ADCL calib
              ADC_ReSync_stream   <= x"0007"&ADC_Hamming_data;
            when others =>
              ADC_ReSync_stream   <= ADC_ReSync_idle&ADC_ReSync_idle&ADC_ReSync_idle&ADC_ReSync_idle&ADC_ReSync_idle;
            end case;
          end if;
        when ReSync_start =>
          if ADC_invert_ReSync = '1' then
            ReSync_DTU_loc        <= not ADC_ReSync_stream(39);
          else
            ReSync_DTU_loc        <= ADC_ReSync_stream(39);
          end if;
--          ADC_ReSync_stream       <= ADC_ReSync_stream(46 downto 0) & '0';
          ADC_ReSync_stream       <= ADC_ReSync_stream(38 downto 0) & Idle_ReSync_data(7);
          Idle_ReSync_data        <= Idle_ReSync_data(6 downto 0) & Idle_Resync_data(7);
          if ADC_ReSync_transfer = "000000" then
            ReSync_state          <= ReSync_idle;
            ReSync_busy           <= '0';
          else
            ADC_ReSync_transfer <= ADC_ReSync_transfer - 1;
          end if;
      end case;
    end if;
  end process proc_ReSync;

end architecture rtl;
