--------------------------------------------------------------------------------
--
--   FileName:         i2c_masterSDAb.vhd
--   Dependencies:     none
--   Design Software:  Quartus II 64-bit Version 13.1 Build 162 SJ Full Version
--
--   HDL CODE IS PROVIDED "AS IS."  DIGI-KEY EXPRESSLY DISCLAIMS ANY
--   WARRANTY OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING BUT NOT
--   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
--   PARTICULAR PURPOSE, OR NON-INFRINGEMENT. IN NO EVENT SHALL DIGI-KEY
--   BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR CONSEQUENTIAL
--   DAMAGES, LOST PROFITS OR LOST DATA, HARM TO YOUR EQUIPMENT, COST OF
--   PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR SERVICES, ANY CLAIMS
--   BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF),
--   ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER SIMILAR COSTS.
--
--   Version History
--   Version 1.0 11/01/2012 Scott Larson
--     Initial Public Release
--   Version 2.0 06/20/2014 Scott Larson
--     Added ability to interface with different slaves in the same transaction
--     Corrected ack_error bug where ack_error went 'Z' instead of '1' on error
--     Corrected timing of when ack_error signal clears
--   Version 2.1 10/21/2014 Scott Larson
--     Replaced gated clock with clock enable
--     Adjusted timing of SCL during start and stop conditions
--   Version 2.2 02/05/2015 Scott Larson
--     Corrected small SDA glitch introduced in version 2.1
--   Version 3.0 06/21/2019 Marc Dejardin
--     Reverse SDA signal to cope with LiTE-DTU bug
--------------------------------------------------------------------------------
Library UNISIM;
use UNISIM.vcomponents.all;

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE IEEE.NUMERIC_STD.all;

ENTITY i2c_master_SDAb IS
  GENERIC(
    input_clk : INTEGER := 160_000_000; --input clock speed from user logic in Hz
--    bus_clk   : INTEGER := 312_500);   --speed the i2c bus (scl) will run at in Hz
--    bus_clk   : INTEGER := 500_000);   --speed the i2c bus (scl) will run at in Hz
    bus_clk   : INTEGER := 400_000);   --speed the i2c bus (scl) will run at in Hz
  PORT(
    toggle_SDA: IN     STD_LOGIC;                    -- Toggle SDA during idle time
    clk       : IN     STD_LOGIC;                    --system clock
    reset_n   : IN     STD_LOGIC;                    --active low reset
    ena       : IN     STD_LOGIC;                    --latch in command
    addr      : IN     STD_LOGIC_VECTOR(6 DOWNTO 0); --address of target slave
    R_Wb      : IN     STD_LOGIC;                    --'0' is write, '1' is read
    data_w    : IN     STD_LOGIC_VECTOR(7 DOWNTO 0); --data to write to slave
    busy      : OUT    STD_LOGIC;                    --indicates transaction in progress
    data_r    : OUT    STD_LOGIC_VECTOR(7 DOWNTO 0); --data read from slave
    slave_ack : OUT    STD_LOGIC;                    --indicates slave has aknowledged
    ack_error : BUFFER STD_LOGIC;                    --flag if improper acknowledge from slave
    sda       : INOUT  STD_LOGIC;                    --serial data output of i2c bus
    scl       : INOUT  STD_LOGIC);                   --serial clock output of i2c bus
END i2c_master_SDAb;

ARCHITECTURE rtl OF i2c_master_SDAb IS
  CONSTANT divider  :  INTEGER := (input_clk/bus_clk)/4; --number of clocks in 1/4 cycle of scl
  TYPE machine IS(ready, start, command, slv_ack1, wr, rd, slv_ack2, mstr_ack, stop); --needed states
  SIGNAL state         : machine;                        --state machine
  SIGNAL data_clk      : STD_LOGIC;                      --data clock for sda
  SIGNAL data_clk_prev : STD_LOGIC;                      --data clock during previous system clock
  SIGNAL scl_clk       : STD_LOGIC;                      --constantly running internal scl
  SIGNAL scl_clk_prev  : STD_LOGIC;                      --constantly running internal scl
  SIGNAL scl_ena       : STD_LOGIC := '0';               --enables internal scl to output
  SIGNAL sda_int       : STD_LOGIC := '1';               --internal sda
  SIGNAL sda_ena_n     : STD_LOGIC;                      --enables internal sda to output
  SIGNAL addr_rw       : STD_LOGIC_VECTOR(7 DOWNTO 0);   --latched in address and read/write
  SIGNAL data_tx       : STD_LOGIC_VECTOR(7 DOWNTO 0);   --latched in data to write to slave
  SIGNAL data_rx       : STD_LOGIC_VECTOR(7 DOWNTO 0);   --data received from slave
  SIGNAL bit_cnt       : INTEGER RANGE 0 TO 7 := 7;      --tracks bit number in transaction
  SIGNAL stretch       : STD_LOGIC := '0';               --identifies if slave is stretching scl
  SIGNAL sda_highZ     : STD_LOGIC := '0';               -- Put SDA in High-Z for ACk or incoming data
BEGIN

  --generate the timing for the bus clock (scl_clk) and the data clock (data_clk)
  PROCESS(clk, reset_n)
    VARIABLE count  :  INTEGER RANGE 0 TO divider*4;  --timing for clock generation
  BEGIN
    IF(reset_n = '0') THEN                --reset asserted
      stretch  <= '0';
      count    := 0;
    ELSIF(clk'EVENT AND clk = '1') THEN
      data_clk_prev <= data_clk;          --store previous value of data clock
      scl_clk_prev  <= scl_clk;          --store previous value of data clock
      IF(count = divider*4-1) THEN        --end of timing cycle
        count := 0;                       --reset timer
      ELSIF(stretch = '0') THEN           --clock stretching from slave not detected
        count := count + 1;               --continue clock generation timing
      END IF;
      CASE count IS
        WHEN 0 TO divider-1 =>            --first 1/4 cycle of clocking
          scl_clk <= '0';
          data_clk <= '0';
        WHEN divider TO divider*2-1 =>    --second 1/4 cycle of clocking
          scl_clk <= '0';
          data_clk <= '1';
        WHEN divider*2 TO divider*3-1 =>  --third 1/4 cycle of clocking
          scl_clk <= '1';                 --release scl
          IF(scl = '0') THEN              --detect if slave is stretching clock
            stretch <= '1';
          ELSE
            stretch <= '0';
          END IF;
          data_clk <= '1';
        WHEN OTHERS =>                    --last 1/4 cycle of clocking
          scl_clk <= '1';
          data_clk <= '0';
      END CASE;
    END IF;
  END PROCESS;

  --state machine and writing to sda during scl low (data_clk rising edge)
  PROCESS(clk, reset_n)
  BEGIN
    IF(reset_n = '0') THEN                   --reset asserted
      state     <= ready;                    --return to initial state
      busy      <= '1';                      --indicate not available
      scl_ena   <= '0';                      --sets scl high impedance (1)
      sda_int   <= '1';                      --sets sda at 1 in idle state
--      sda_int   <= '0';                      --sets sda high impedance
      ack_error <= '0';                      --clear acknowledge error flag
      bit_cnt   <= 7;                        --restarts data bit counter
      data_r    <= "00000000";               --clear data read port
      sda_HighZ <= '0';                      --force sda data
    ELSIF(clk'EVENT AND clk = '1') THEN
      IF(data_clk = '1' AND data_clk_prev = '0') THEN  --data clock rising edge : prepare data on SDA
        CASE state IS
          WHEN ready =>                        --idle state
            sda_highZ   <= '0';                --master on SDA bus
            sda_int   <= '1';                  --Should be at 1 to generate start
            IF(ena = '1') THEN                 --transaction requested
              busy      <= '1';                --flag busy
              addr_rw   <= addr & R_Wb;        --collect requested slave address and command
              data_tx   <= data_w;             --collect requested data to write
              state     <= start;              --go to start bit
            ELSE                               --remain idle
              busy      <= '0';                --unflag busy
              state     <= ready;              --remain idle
              if toggle_SDA = '1' then
                sda_int   <= not sda_int;        --Idle patterns for tests
              end if;
            END IF;
          WHEN start =>                        --start bit of transaction
            sda_highZ   <= '0';                --master on SDA bus
            busy        <= '1';                --resume busy if continuous mode
            sda_int     <= addr_rw(bit_cnt);   --set first address bit to bus
            state       <= command;            --go to command
          WHEN command =>                      --address and command byte of transaction
            IF(bit_cnt = 0) THEN               --command transmit finished
              sda_highZ <= '1';                --release SDA bus for slave ack
--              sda_int   <= '1';                --release sda for slave acknowledge
              sda_int   <= '0';                --release sda for slave acknowledge
              bit_cnt   <= 7;                  --reset bit counter for "byte" states
              state     <= slv_ack1;           --go to slave acknowledge (command)
            ELSE                               --next clock cycle of command state
              sda_highZ   <= '0';              --stay master on SDA bus
              bit_cnt   <= bit_cnt - 1;        --keep track of transaction bits
              sda_int   <= addr_rw(bit_cnt-1); --write address/command bit to bus
              state     <= command;            --continue with command
            END IF;
          WHEN slv_ack1 =>                     --slave acknowledge bit (command)
            IF(addr_rw(0) = '0') THEN          --write command
              sda_highZ <= '0';                --take back control on sda bus
              sda_int   <= data_tx(bit_cnt);   --write first bit of data
              state     <= wr;                 --go to write byte
            ELSE                               --read command
              sda_highZ <= '1';                --release SDA bus for data reading
--              sda_int   <= '1';                --release sda for incoming data
              sda_int   <= '0';                --release sda for incoming data
              state     <= rd;                 --go to read byte
            END IF;
          WHEN wr =>                           --write byte of transaction
            busy <= '1';                       --resume busy if continuous mode
            IF(bit_cnt = 0) THEN               --write byte transmit finished
              sda_highZ <= '1';                --realeas SDA bus for slave ack
--              sda_int   <= '1';                --release sda for slave acknowledge
              sda_int   <= '0';                --release sda for slave acknowledge
              bit_cnt   <= 7;                  --reset bit counter for "byte" states
              state     <= slv_ack2;           --go to slave acknowledge (write)
            ELSE                               --next clock cycle of write state
              sda_highZ <= '0';                --stay master on SDA bus
              bit_cnt   <= bit_cnt - 1;        --keep track of transaction bits
              sda_int   <= data_tx(bit_cnt-1); --write next bit to bus
              state     <= wr;                 --continue writing
            END IF;
          WHEN rd =>                         --read byte of transaction
            busy <= '1';                     --resume busy if continuous mode
            IF(bit_cnt = 0) THEN             --read byte receive finished
              sda_highZ <= '0';              --bring back SDA bus control for master ack
              IF(ena = '1' AND addr_rw = addr & R_Wb) THEN  --continuing with another read at same address
                sda_int <= '0';              --acknowledge the byte has been received
              ELSE                           --stopping or continuing with a write
                sda_int <= '1';              --send a no-acknowledge (before stop or repeated start)
              END IF;
              bit_cnt   <= 7;                --reset bit counter for "byte" states
              data_r    <= data_rx;          --output received data
              state     <= mstr_ack;         --go to master acknowledge
            ELSE                             --next clock cycle of read state
              sda_highZ <= '1';              --keep SDA bus free for incoming data
              bit_cnt   <= bit_cnt - 1;      --keep track of transaction bits
              state     <= rd;               --continue reading
            END IF;
          WHEN slv_ack2 =>                   --slave acknowledge bit (write)
            sda_highZ     <= '0';            -- take SDA bus control for next transaction or stop
            IF(ena = '1') THEN               --continue transaction
              busy        <= '0';            --continue is accepted
              addr_rw     <= addr & R_Wb;    --collect requested slave address and command
              data_tx     <= data_w;         --collect requested data to write
              IF(addr_rw = addr & R_Wb) THEN --continue transaction with another write
                sda_int   <= data_w(bit_cnt);--write first bit of data
                state     <= wr;             --go to write byte
              ELSE                           --continue transaction with a read or new slave
                state     <= start;          --go to repeated start
              END IF;
            ELSE                             --complete transaction
              state       <= stop;           --go to stop bit
            END IF;
          WHEN mstr_ack =>                   --master acknowledge bit after a read
            IF(ena = '1') THEN               --continue transaction
              busy        <= '0';            --continue is accepted and data received is available on bus
              addr_rw     <= addr & R_Wb;    --collect requested slave address and command
              data_tx     <= data_w;         --collect requested data to write
              IF(addr_rw = addr & R_Wb) THEN --continue transaction with another read
--                sda_int <= '1';            --release sda from incoming data
                state     <= rd;             --go to read byte
                sda_int   <= '0';            --release sda from incoming data
                sda_highZ <= '1';            --release SDA bus for incoming data
              ELSE                           --continue transaction with a write or new slave
                sda_highZ <= '0';            --bring back control on SDA bus for next start
                state     <= start;          --repeated start
              END IF;    
            ELSE                             --complete transaction
              sda_highZ   <= '0';            --bring back control on SDA bus for next stop
              state       <= stop;           --go to stop bit
            END IF;
          WHEN stop =>                       --stop bit of transaction
            sda_highZ     <= '0';            --keep control on SDA bus
            sda_int       <= '1';            --prepare sda value for idle patterns
            busy          <= '0';            --unflag busy
            state         <= ready;          --go to idle state
        END CASE;    
      ELSIF(data_clk = '0' AND data_clk_prev = '1') THEN  --data clock falling edge
        slave_ack <= '0';                          --Acknowledge from slave
        CASE state IS
          WHEN start =>                  
            IF(scl_ena = '0') THEN                  --starting new transaction
              scl_ena   <= '1';                     --enable scl output
              ack_error <= '0';                     --reset acknowledge error output
            END IF;
--            sda_int     <= '0';                     -- sda goes down when scl is high
          WHEN slv_ack1 =>                          --receiving slave acknowledge (command)
-- LiTE-DTU bug :
--            if sda = '0' then
            if sda /= '0' then
              slave_ack <= '1';
            end if;
-- LiTE-DTU bug :
--            IF(sda /= '0' OR ack_error = '1') THEN  --no-acknowledge or previous no-acknowledge
            IF(sda /= '1' OR ack_error = '1') THEN  --no-acknowledge or previous no-acknowledge
              ack_error <= '1';                     --set error output if no-acknowledge
            END IF;
          WHEN rd =>                                --receiving slave data
-- LiTE-DTU bug :
--            data_rx(bit_cnt) <= sda;                --receive current slave data bit
            data_rx(bit_cnt) <= not sda;            --receive current slave data bit
          WHEN slv_ack2 =>                          --receiving slave acknowledge (write)
-- LiTE-DTU bug :
--            if sda = '0' then
            if sda /= '0' then
              slave_ack <= '1';
            end if;
-- LiTE-DTU bug :
--            IF(sda /= '0' OR ack_error = '1') THEN  --no-acknowledge or previous no-acknowledge
            IF(sda /= '1' OR ack_error = '1') THEN  --no-acknowledge or previous no-acknowledge
              ack_error <= '1';                     --set error output if no-acknowledge
            END IF;
          WHEN stop =>
--            sda_int     <= '1';                     -- sda goes up when scl is high
            scl_ena     <= '0';                     --disable scl
          WHEN OTHERS =>
            NULL;
        END CASE;
      END IF;
    END IF;
   END PROCESS;  

  --set sda output
  WITH state SELECT
    sda_ena_n <= data_clk_prev     WHEN start,  --generate start condition
                 NOT data_clk_prev WHEN stop,   --generate stop condition
                 sda_int           WHEN OTHERS; --set to internal sda signal    
      
  scl <= '0' WHEN (scl_ena = '1' AND scl_clk = '0') ELSE '1';
--  scl <= '0' WHEN (scl_ena = '1' AND scl_clk = '0') ELSE 'Z';

-- Bug in LiTE-DTU, try reversing SDA bit :
-- LiTE-DTU SDA output driver :
--      drives a logic 1 when the output should be 0
--      High-Z           when output should be 1
-- With pull down :
--      sda=1 when output should be 0
--      sda=0 when output should be 1
-- So received SDA from LiTE-DTU is reversed
-- But we have to send the normal data to write in registers
-- From I2C master :
-- high-Z when output should be 0
-- drives logic 1  when output should be 1

--  sda <= '0' WHEN sda_ena_n = '0' ELSE 'Z';
  sda <= '1' WHEN sda_ena_n = '1' ELSE 'Z';
--  sda <= '1' WHEN sda_ena_n = '1' ELSE '0';
--  sda <= sda_int WHEN sda_highZ = '0' ELSE 'Z';
--  sda <= sda_int;
  
 END rtl;

