library ieee;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
--use IEEE.std_logic_misc.all;
use work.FEAD_IO.all;

library UNISIM;
use UNISIM.VComponents.all;

entity DAC_ctrl is
  generic
  (
    input_clk   : INTEGER := 200_000_000; --input clock speed from user logic in Hz
    bus_clk     : INTEGER := 1_000_000    --speed the SPI bus (SPI_clk) will run at in Hz
  );

  port (
    SPI_Access_DAC    : in    std_logic;
    SPI_DAC_data      : in    std_logic_vector(15 downto 0);
    SPI_DAC_number    : in    std_logic_vector(3 downto 0);
    SPI_DAC_action    : in    std_logic_vector(3 downto 0);
    reset             : in    std_logic;
    SPI_clk           : in    std_logic;
    SPI_strobe        : out   std_logic;
    SPI_data          : out   std_logic;
    SPI_csb           : out   std_logic
  );

end entity DAC_ctrl;

architecture rtl of DAC_ctrl is

  component SPI_master is
  port (
    SPI_clk               : in    STD_LOGIC;
    reset                 : in    STD_LOGIC;
    transmit              : in    std_logic_vector(1 downto 0);
    SPI_user_data         : in    std_logic_vector(23 downto 0);
    R_Wb                  : in    std_logic;
    wait_for_transfer     : out   STD_LOGIC;
    SPI_csb               : out   STD_LOGIC;
    SPI_data              : out   STD_LOGIC;
    SPI_strobe            : out   STD_LOGIC
  );
  end component SPI_master;

  CONSTANT divider               : INTEGER := (input_clk/bus_clk)/2; --number of clocks in 1/2 cycle of SPI_clk
  signal SPI_clk_loc             : std_logic := '0';
  signal SPI_user_data           : std_logic_vector(23 downto 0) := (others => '0');
  signal SPI_transmit            : std_logic_vector(1 downto 0) := "00";
  signal SPI_dir                 : std_logic := '1';
  signal SPI_init                : std_logic := '1';
  signal wait_for_SPI_transfer   : std_logic := '0';

  type   SPI_state_type is (wait_for_DAC_registers, wait_for_SPI_busy, idle);
  signal SPI_state     : SPI_state_type := idle;

begin  -- architecture behavioral

--generate SPI_clk from input clock
  PROCESS(SPI_clk, reset)
    VARIABLE count  :  INTEGER RANGE 0 TO divider*2;  --timing for clock generation
  BEGIN
    IF(reset = '1') THEN                --reset asserted
      count := 0;
    ELSIF rising_edge(SPI_clk) THEN
      IF(count = divider*2-1) THEN          --end of timing cycle
        count := 0;                       --reset timer
      ELSE
        count := count + 1;               --continue clock generation timing
      END IF;
      CASE count IS
        WHEN 0 TO divider-1 =>            --first 1/2 cycle of clocking
          SPI_clk_loc <= '0';
        WHEN divider TO divider*2-1 =>    --second 1/2 cycle of clocking
          SPI_clk_loc <= '1';
        WHEN OTHERS =>
          SPI_clk_loc <= '0';
      END CASE;
    END IF;
  END PROCESS;

  Inst_SPI_master : SPI_master
  port map(
    SPI_clk               => SPI_clk_loc,
    reset                 => reset,
    R_Wb                  => '0',
    transmit              => SPI_transmit,
    SPI_user_data         => SPI_user_data,
    wait_for_transfer     => wait_for_SPI_transfer,
    SPI_csb               => SPI_csb,
    SPI_strobe            => SPI_strobe,
    SPI_data              => SPI_data
  );

-- Program default ADC and DAC configuration
-- ADC in normal binary mode, no test mode
-- DAC in normal binary mode, 0-5V output range, 0mV default value
-- Default mode = standard running setup 
  program_SPI : process(reset, SPI_clk, SPI_Access_DAC)
  begin
    if reset = '1' then
      SPI_state                          <= idle;
      SPI_init                           <= '1';
      SPI_transmit                       <= "00";
    elsif Rising_Edge(SPI_clk) then
      case SPI_state is
      when idle => -- We are in the default configuration mode, wait for user request
        SPI_init                         <= '0';
        if SPI_access_DAC = '1' then
          SPI_user_data(23 downto 20)    <= SPI_DAC_action;
          SPI_user_data(19 downto 16)    <= SPI_DAC_number;
          SPI_user_data(15 downto 0)     <= SPI_DAC_data;
          SPI_dir                        <= '0';
          SPI_transmit                   <= "01";
          SPI_state                      <= wait_for_SPI_busy;
        end if;
      when wait_for_SPI_busy => -- Allow some time (1 clk) to initiate SPI protocol
        if wait_for_SPI_transfer = '1' then
          SPI_state                      <= wait_for_DAC_registers;
        end if;
      when wait_for_DAC_registers => -- Now wait for the end of transfer
        if wait_for_SPI_transfer = '0' then
          SPI_transmit                   <= "00";
          SPI_state                      <= idle;
        end if;
      end case;
    end if;
  end process program_SPI;
end architecture rtl;
