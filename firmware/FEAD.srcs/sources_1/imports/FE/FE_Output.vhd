library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.FEAD_IO.all;

entity FE_Output is

  port (
    synchronized    : buffer std_logic;
    clk_160         : in  std_logic;
    clk_640         : in  std_logic;
    reset           : in  std_logic;
    ch1_out_P       : out std_logic;
    ch1_out_N       : out std_logic;
    ch2_out_P       : out std_logic;
    ch2_out_N       : out std_logic;
    ch3_out_P       : out std_logic;
    ch3_out_N       : out std_logic;
    ch4_out_P       : out std_logic;
    ch4_out_N       : out std_logic;
    ch5_out_P       : out std_logic;
    ch5_out_N       : out std_logic;

    ch1_output_data : in std_logic_vector(31 downto 0);
    ch2_output_data : in std_logic_vector(31 downto 0);
    ch3_output_data : in std_logic_vector(31 downto 0);
    ch4_output_data : in std_logic_vector(31 downto 0);
    ch5_output_data : in std_logic_vector(31 downto 0)
    );

end entity FE_Output;

architecture rtl of FE_Output is

component selectio_output_eLink_8 is
port (
  data_out_from_device : in  std_logic_vector (7 downto 0);
  data_out_to_pins_p   : out std_logic;
  data_out_to_pins_n   : out std_logic;
  clk_in               : in  std_logic;
  clk_div_in           : in  std_logic;
  io_reset             : in  std_logic);
end component selectio_output_eLink_8;

signal ch1_DDR_data : std_logic_vector(7 downto 0) := (others => '0');
signal ch2_DDR_data : std_logic_vector(7 downto 0) := (others => '0');
signal ch3_DDR_data : std_logic_vector(7 downto 0) := (others => '0');
signal ch4_DDR_data : std_logic_vector(7 downto 0) := (others => '0');
signal ch5_DDR_data : std_logic_vector(7 downto 0) := (others => '0');
signal ch1_swap_data : std_logic_vector(7 downto 0) := (others => '0');
signal ch2_swap_data : std_logic_vector(7 downto 0) := (others => '0');
signal ch3_swap_data : std_logic_vector(7 downto 0) := (others => '0');
signal ch4_swap_data : std_logic_vector(7 downto 0) := (others => '0');
signal ch5_swap_data : std_logic_vector(7 downto 0) := (others => '0');
signal clk_40_pos    : unsigned(1 downto 0) := "00";
signal cycle_pos     : unsigned(1 downto 0) := "00";

begin  -- architecture behavioral
  
inst_OSERDES8_ch1 : selectio_output_eLink_8
port map (
  data_out_from_device => ch1_DDR_data,
  data_out_to_pins_p   => ch1_out_P,
  data_out_to_pins_n   => ch1_out_N,
  clk_in               => clk_640,
  clk_div_in           => clk_160,
  io_reset             => reset
);

inst_OSERDES8_ch2 : selectio_output_eLink_8
port map (
  data_out_from_device => ch2_DDR_data,
  data_out_to_pins_p   => ch2_out_P,
  data_out_to_pins_n   => ch2_out_N,
  clk_in               => clk_640,
  clk_div_in           => clk_160,
  io_reset             => reset
);

inst_OSERDES8_ch3 : selectio_output_eLink_8
port map (
  data_out_from_device => ch3_DDR_data,
  data_out_to_pins_p   => ch3_out_P,
  data_out_to_pins_n   => ch3_out_N,
  clk_in               => clk_640,
  clk_div_in           => clk_160,
  io_reset             => reset
);

inst_OSERDES8_ch4 : selectio_output_eLink_8
port map (
  data_out_from_device => ch4_DDR_data,
  data_out_to_pins_p   => ch4_out_P,
  data_out_to_pins_n   => ch4_out_N,
  clk_in               => clk_640,
  clk_div_in           => clk_160,
  io_reset             => reset
);

inst_OSERDES8_ch5 : selectio_output_eLink_8
port map (
  data_out_from_device => ch5_DDR_data,
  data_out_to_pins_p   => ch5_out_P,
  data_out_to_pins_n   => ch5_out_N,
  clk_in               => clk_640,
  clk_div_in           => clk_160,
  io_reset             => reset
);

-- Output signals to device
-- Prepare for big endian transfer : MSByte first
  ISERDES_cycle : process (clk_160, reset) is
  begin
    if reset = '1' then
      ch1_swap_data <= (others => '0');
      ch2_swap_data <= (others => '0');
      ch3_swap_data <= (others => '0');
      ch4_swap_data <= (others => '0');
      ch5_swap_data <= (others => '0');
      cycle_pos     <= "00";
      clk_40_pos    <= "00";
      synchronized        <= '0';
    elsif rising_edge(clk_160) then
      clk_40_pos          <= clk_40_pos+1;
      if cycle_pos = "00" then
        ch1_swap_data     <= ch1_output_data(31 downto 24);
        ch2_swap_data     <= ch2_output_data(31 downto 24);
        ch3_swap_data     <= ch3_output_data(31 downto 24);
        ch4_swap_data     <= ch4_output_data(31 downto 24);
        ch5_swap_data     <= ch5_output_data(31 downto 24);
      elsif cycle_pos = "01" then
        ch1_swap_data     <= ch1_output_data(23 downto 16);
        ch2_swap_data     <= ch2_output_data(23 downto 16);
        ch3_swap_data     <= ch3_output_data(23 downto 16);
        ch4_swap_data     <= ch4_output_data(23 downto 16);
        ch5_swap_data     <= ch5_output_data(23 downto 16);
      elsif cycle_pos = "10" then
        ch1_swap_data     <= ch1_output_data(15 downto 8);
        ch2_swap_data     <= ch2_output_data(15 downto 8);
        ch3_swap_data     <= ch3_output_data(15 downto 8);
        ch4_swap_data     <= ch4_output_data(15 downto 8);
        ch5_swap_data     <= ch5_output_data(15 downto 8);
      elsif cycle_pos = "11" then
        ch1_swap_data     <= ch1_output_data(7 downto 0);
        ch2_swap_data     <= ch2_output_data(7 downto 0);
        ch3_swap_data     <= ch3_output_data(7 downto 0);
        ch4_swap_data     <= ch4_output_data(7 downto 0);
        ch5_swap_data     <= ch5_output_data(7 downto 0);
      end if;

      if synchronized='0' and clk_40_pos="00" then
        synchronized <= '1';
        cycle_pos    <= "01";
      elsif synchronized='1' then
        cycle_pos    <= cycle_pos + 1;
      else
        cycle_pos    <= "00";
      end if;
    end if;
  end process ISERDES_cycle;

-- But OSERDES is little endian. So swap bits before sending
  ch1_DDR_data(0)     <= ch1_swap_data(7);
  ch1_DDR_data(1)     <= ch1_swap_data(6);
  ch1_DDR_data(2)     <= ch1_swap_data(5);
  ch1_DDR_data(3)     <= ch1_swap_data(4);
  ch1_DDR_data(4)     <= ch1_swap_data(3);
  ch1_DDR_data(5)     <= ch1_swap_data(2);
  ch1_DDR_data(6)     <= ch1_swap_data(1);
  ch1_DDR_data(7)     <= ch1_swap_data(0);
  ch2_DDR_data(0)     <= ch2_swap_data(7);
  ch2_DDR_data(1)     <= ch2_swap_data(6);
  ch2_DDR_data(2)     <= ch2_swap_data(5);
  ch2_DDR_data(3)     <= ch2_swap_data(4);
  ch2_DDR_data(4)     <= ch2_swap_data(3);
  ch2_DDR_data(5)     <= ch2_swap_data(2);
  ch2_DDR_data(6)     <= ch2_swap_data(1);
  ch2_DDR_data(7)     <= ch2_swap_data(0);
  ch3_DDR_data(0)     <= ch3_swap_data(7);
  ch3_DDR_data(1)     <= ch3_swap_data(6);
  ch3_DDR_data(2)     <= ch3_swap_data(5);
  ch3_DDR_data(3)     <= ch3_swap_data(4);
  ch3_DDR_data(4)     <= ch3_swap_data(3);
  ch3_DDR_data(5)     <= ch3_swap_data(2);
  ch3_DDR_data(6)     <= ch3_swap_data(1);
  ch3_DDR_data(7)     <= ch3_swap_data(0);
  ch4_DDR_data(0)     <= ch4_swap_data(7);
  ch4_DDR_data(1)     <= ch4_swap_data(6);
  ch4_DDR_data(2)     <= ch4_swap_data(5);
  ch4_DDR_data(3)     <= ch4_swap_data(4);
  ch4_DDR_data(4)     <= ch4_swap_data(3);
  ch4_DDR_data(5)     <= ch4_swap_data(2);
  ch4_DDR_data(6)     <= ch4_swap_data(1);
  ch4_DDR_data(7)     <= ch5_swap_data(0);
  ch5_DDR_data(0)     <= ch5_swap_data(7);
  ch5_DDR_data(1)     <= ch5_swap_data(6);
  ch5_DDR_data(2)     <= ch5_swap_data(5);
  ch5_DDR_data(3)     <= ch5_swap_data(4);
  ch5_DDR_data(4)     <= ch5_swap_data(3);
  ch5_DDR_data(5)     <= ch5_swap_data(2);
  ch5_DDR_data(6)     <= ch5_swap_data(1);
  ch5_DDR_data(7)     <= ch5_swap_data(0);

end architecture rtl;