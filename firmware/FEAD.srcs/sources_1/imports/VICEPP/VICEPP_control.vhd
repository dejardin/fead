library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.FEAD_IO.all;

library UNISIM;
use UNISIM.VComponents.all;

entity VICEPP_Clk_Ctrl is
  port (
    reset             : in  std_logic;
    clk               : in  std_logic;
    Xpoint_ready      : out std_logic;
-- b1b0 = "00" = Internal clock to all outputs,
--        "01" = Clock from FE to all outputs,
--        "10" = User setting (b15b14, b13b12, b11b10, b9b8, b7b6, b5b4, b3b2
-- b31  =  "1" = launch config
    XPS_default       : in  std_logic;
    XPS_Define_in     : in  std_logic_vector(31 downto 0);
    XPS_Define_out    : out std_logic_vector(31 downto 0);
    XPS_Sin           : out std_logic_vector(1 downto 0);
    XPS_Sout          : out std_logic_vector(1 downto 0);
    XPS_Config        : out std_logic_vector(1 downto 0);
    XPS_Load          : out std_logic_vector(1 downto 0)
  );
end entity VICEPP_Clk_Ctrl;

architecture rtl of VICEPP_Clk_Ctrl is

  type   XPS_state_type is (XPS_idle, XPS_set_input_output, XPS_wait, XPS_load_config, XPS_execute_config, XPS_next);
  signal XPS_state     : XPS_state_type := XPS_idle;
  signal load_default  : std_logic := '0';
  type config_t is array (integer range <>) of std_logic_vector(1 downto 0);
  constant config_loc_osc      : config_t(7 downto 0)         := ("00", "00", "00", "00", "00", "00", "00", "00");
  constant config_from_FE      : config_t(7 downto 0)         := ("10", "11", "10", "01", "10", "01", "01", "01");
  signal loc_config            : config_t(7 downto 0)         := (others => (others => '0'));
  signal channel_num           : unsigned(2 downto 0)         := (others => '0'); 
  signal dyn_load_prev         : std_logic                    := '0';
  signal load_type             : std_logic_vector(1 downto 0) := "00";

begin  -- architecture behavioral

  XPS_prog : process (reset, clk) is
-- Config 0 : put input 0 (internal oscillator) to all outputs on both mux
--            mux 0 : 0 to 0, 1, 2 and 3 (MGT_ref_clk, HP_clock, VFE-ch1, VFE-ch2)
--            mux 1 : 0 to 0, 1, 2 (VFE-ch3, VFE-ch4, VFE-ch5)
-- Config 1 : send clock from FE to VFE : 
--            mux 0 : 1 to 0, 1, 2 (FE-ch1 to MGT_ref_clk, HP_clock, VFE-ch1)
--                    2 to 3       (FE-ch2 to VFE-ch2)
--            mux 1 : 1 to 0       (FE-ch3 to VFE-ch3)
--                    2 to 1       (FE-ch4 to VFE-ch4)
--                    3 to 2       (FE-ch5 to VFE-ch5)
-- Sequence : Set Sin()  = Clock to distribute (0= loc oscillator, 1,2,3 = clock fro FE)
--            Set Sout() = Output to program (0..3)
--            then set config with XPS_Config pulse
--            then load config with XPS_Load pulse
  begin  -- process reg
    if reset = '1' then
      XPS_state         <= XPS_Idle;
      load_default      <= '1';
      channel_num       <= (others => '0');
      XPS_Sin           <= (others => '0');
      XPS_Sout          <= (others => '0');
      XPS_config        <= (others => '0');
      XPS_load          <= (others => '0');
      Xpoint_ready      <= '0';
      loc_config        <= (others => (others => '0'));
    elsif rising_edge(clk) then
      XPS_Config        <= (others => '0');
      XPS_Load          <= (others => '0');
      dyn_load_prev     <= XPS_define_in(31);
      case XPS_state is
      when XPS_idle =>
        Xpoint_ready      <= '1';
        channel_num   <= (Others => '0');
        if load_default = '1' then
          if XPS_default = '0' then
            loc_config     <= config_loc_osc;
            load_type      <= "00";
          else
            loc_config     <= config_from_FE;
            load_type      <= "01";
          end if;
        elsif XPS_define_in(31)='1' and dyn_load_prev ='0' then
          if XPS_define_in(1 downto 0) = "00" then
            loc_config    <= config_loc_osc;
            load_type      <= "00";
          elsif XPS_define_in(1 downto 0) = "01" then
            loc_config     <= config_from_FE;
            load_type      <= "01";
          else
            loc_config(0)  <= XPS_define_in(3 downto 2);
            loc_config(1)  <= XPS_define_in(5 downto 4);
            loc_config(2)  <= XPS_define_in(7 downto 6);
            loc_config(3)  <= XPS_define_in(9 downto 8);
            loc_config(4)  <= XPS_define_in(11 downto 10);
            loc_config(5)  <= XPS_define_in(13 downto 12);
            loc_config(6)  <= XPS_define_in(15 downto 14);
            loc_config(7)  <= XPS_define_in(17 downto 16);
            load_type      <= "10";
          end if;
        end if;
        if load_default = '1' or (XPS_define_in(31)='1' and dyn_load_prev ='0') then
          XPS_state       <= XPS_wait;
          XPoint_ready    <= '0';
        end if;
      when XPS_wait =>
        XPS_define_out <= x"000"&"00"&loc_config(7)&loc_config(6)&loc_config(5)&loc_config(4)&loc_config(3)&
                                      loc_config(2)&loc_config(1)&loc_config(0)&load_type;
        load_default  <= '0';
        XPS_state     <= XPS_set_input_output;
      when XPS_set_input_output =>
        XPS_Sin         <= loc_config(to_integer(channel_num));
        XPS_Sout        <= std_logic_vector(channel_num(1 downto 0));
        XPS_state       <= XPS_load_config;
      when XPS_load_config =>
        if channel_num(2) = '0' then
          XPS_load    <= "01";
        else
          XPS_load    <= "10";
        end if;
        XPS_state       <= XPS_execute_config;
      when XPS_execute_config =>
        if channel_num(2) = '0' then
          XPS_config      <= "01";
        else
          XPS_config      <= "10";
        end if;
        XPS_state       <= XPS_next;
      when XPS_next =>
        if channel_num = "111" then
          XPS_state     <= XPS_idle;
        else
          channel_num   <= channel_num+1;
          XPS_state     <= XPS_set_input_output;
        end if;
      end case;
    end if;
  end process;

end architecture rtl;
