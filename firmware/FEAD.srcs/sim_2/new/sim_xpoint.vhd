library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
---- The following library declaration should be present if 
---- instantiating any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

use work.ipbus.all;
use work.ipbus_trans_decl.all;
use work.FEAD_IO.all;

entity xpoint_tb is
end xpoint_tb;

architecture simulate of xpoint_tb is

component VICEPP_Clk_Ctrl is
  port (
    reset               : in    std_logic;
    clk                 : in    std_logic;
    XPS_Define          : in    std_logic_vector(31 downto 0);
    XPS_Sout            : out   std_logic_vector(1 downto 0);
    XPS_Sin             : out   std_logic_vector(1 downto 0);
    XPS_Config          : out   std_logic_vector(1 downto 0);
    XPS_Load            : out   std_logic_vector(1 downto 0)
  );
end component VICEPP_Clk_Ctrl;
signal hard_reset            : std_logic := '1';
signal system_clk            : std_logic := '0';
signal VICEPP_Clk_Config     : std_logic_vector(31 downto 0);
signal XPS_Sout              : std_logic_vector(1 downto 0);
signal XPS_Sin               : std_logic_vector(1 downto 0);
signal XPS_Config            : std_logic_vector(1 downto 0);
signal XPS_Load              : std_logic_vector(1 downto 0);

begin

Inst_CLK_Ctrl : VICEPP_Clk_Ctrl
port map (
  reset               => hard_reset,
  clk                 => system_clk,
  XPS_define          => VICEPP_Clk_config,
  XPS_SOUT            => XPS_Sout,
  XPS_SIN             => XPS_Sin,
  XPS_Config          => XPS_Config,
  XPS_Load            => XPS_Load
);

hard_reset <= '0' after 25 ns;
system_clk <= not system_clk after  3125 ps;
VICEPP_clk_config <= x"00000000", x"80000001" after 499 ns, x"00000000" after 549 ns; 

end;