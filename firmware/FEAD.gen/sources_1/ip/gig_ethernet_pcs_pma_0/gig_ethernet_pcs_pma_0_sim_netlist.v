// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// Copyright 2022-2024 Advanced Micro Devices, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2024.1.1 (lin64) Build 5094488 Fri Jun 14 08:57:50 MDT 2024
// Date        : Tue Jul 30 12:07:50 2024
// Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 38 (Thirty Eight)
// Command     : write_verilog -force -mode funcsim
//               /data/cms/ecal/fe/fead_v2/firmware.2023.2/FEAD.gen/sources_1/ip/gig_ethernet_pcs_pma_0/gig_ethernet_pcs_pma_0_sim_netlist.v
// Design      : gig_ethernet_pcs_pma_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7k70tfbg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* EXAMPLE_SIMULATION = "0" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "gig_ethernet_pcs_pma_v16_2_19,Vivado 2024.1.1" *) 
(* NotValidForBitStream *)
module gig_ethernet_pcs_pma_0
   (gtrefclk_p,
    gtrefclk_n,
    gtrefclk_out,
    gtrefclk_bufg_out,
    txp,
    txn,
    rxp,
    rxn,
    resetdone,
    userclk_out,
    userclk2_out,
    rxuserclk_out,
    rxuserclk2_out,
    pma_reset_out,
    mmcm_locked_out,
    independent_clock_bufg,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    configuration_vector,
    status_vector,
    reset,
    signal_detect,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out);
  input gtrefclk_p;
  input gtrefclk_n;
  output gtrefclk_out;
  output gtrefclk_bufg_out;
  output txp;
  output txn;
  input rxp;
  input rxn;
  output resetdone;
  output userclk_out;
  output userclk2_out;
  output rxuserclk_out;
  output rxuserclk2_out;
  output pma_reset_out;
  output mmcm_locked_out;
  input independent_clock_bufg;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  input [4:0]configuration_vector;
  output [15:0]status_vector;
  input reset;
  input signal_detect;
  output gt0_qplloutclk_out;
  output gt0_qplloutrefclk_out;

  wire \<const0> ;
  wire [4:0]configuration_vector;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg_out;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire independent_clock_bufg;
  wire mmcm_locked_out;
  wire pma_reset_out;
  wire reset;
  wire resetdone;
  wire rxn;
  wire rxp;
  wire rxuserclk2_out;
  wire rxuserclk_out;
  wire signal_detect;
  wire [6:0]\^status_vector ;
  wire txn;
  wire txp;
  wire userclk2_out;
  wire userclk_out;
  wire [15:7]NLW_U0_status_vector_UNCONNECTED;

  assign status_vector[15] = \<const0> ;
  assign status_vector[14] = \<const0> ;
  assign status_vector[13] = \<const0> ;
  assign status_vector[12] = \<const0> ;
  assign status_vector[11] = \<const0> ;
  assign status_vector[10] = \<const0> ;
  assign status_vector[9] = \<const0> ;
  assign status_vector[8] = \<const0> ;
  assign status_vector[7] = \<const0> ;
  assign status_vector[6:0] = \^status_vector [6:0];
  GND GND
       (.G(\<const0> ));
  (* EXAMPLE_SIMULATION = "0" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  gig_ethernet_pcs_pma_0_support U0
       (.configuration_vector({1'b0,configuration_vector[3:1],1'b0}),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg_out(gtrefclk_bufg_out),
        .gtrefclk_n(gtrefclk_n),
        .gtrefclk_out(gtrefclk_out),
        .gtrefclk_p(gtrefclk_p),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_locked_out(mmcm_locked_out),
        .pma_reset_out(pma_reset_out),
        .reset(reset),
        .resetdone(resetdone),
        .rxn(rxn),
        .rxp(rxp),
        .rxuserclk2_out(rxuserclk2_out),
        .rxuserclk_out(rxuserclk_out),
        .signal_detect(signal_detect),
        .status_vector({NLW_U0_status_vector_UNCONNECTED[15:7],\^status_vector }),
        .txn(txn),
        .txp(txp),
        .userclk2_out(userclk2_out),
        .userclk_out(userclk_out));
endmodule

module gig_ethernet_pcs_pma_0_GTWIZARD
   (txn,
    txp,
    rxoutclk,
    txoutclk,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i,
    gtxe2_i_0,
    gtxe2_i_1,
    gtxe2_i_2,
    gtxe2_i_3,
    mmcm_reset,
    data_in,
    rx_fsm_reset_done_int_reg,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gtxe2_i_4,
    TXPD,
    RXPD,
    Q,
    gtxe2_i_5,
    gtxe2_i_6,
    gtxe2_i_7,
    out,
    gtxe2_i_8,
    gtxe2_i_9,
    data_sync_reg1,
    data_out);
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i;
  output [1:0]gtxe2_i_0;
  output [1:0]gtxe2_i_1;
  output [1:0]gtxe2_i_2;
  output [1:0]gtxe2_i_3;
  output mmcm_reset;
  output data_in;
  output rx_fsm_reset_done_int_reg;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gtxe2_i_4;
  input [0:0]TXPD;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_5;
  input [1:0]gtxe2_i_6;
  input [1:0]gtxe2_i_7;
  input [0:0]out;
  input gtxe2_i_8;
  input gtxe2_i_9;
  input data_sync_reg1;
  input data_out;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire data_in;
  wire data_out;
  wire data_sync_reg1;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire [15:0]gtxe2_i;
  wire [1:0]gtxe2_i_0;
  wire [1:0]gtxe2_i_1;
  wire [1:0]gtxe2_i_2;
  wire [1:0]gtxe2_i_3;
  wire gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire [1:0]gtxe2_i_7;
  wire gtxe2_i_8;
  wire gtxe2_i_9;
  wire independent_clock_bufg;
  wire mmcm_reset;
  wire [0:0]out;
  wire reset;
  wire reset_out;
  wire rx_fsm_reset_done_int_reg;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;

  gig_ethernet_pcs_pma_0_GTWIZARD_init U0
       (.D(D),
        .Q(Q),
        .RXBUFSTATUS(RXBUFSTATUS),
        .RXPD(RXPD),
        .TXBUFSTATUS(TXBUFSTATUS),
        .TXPD(TXPD),
        .data_in(data_in),
        .data_out(data_out),
        .data_sync_reg1(data_sync_reg1),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(gtxe2_i),
        .gtxe2_i_0(gtxe2_i_0),
        .gtxe2_i_1(gtxe2_i_1),
        .gtxe2_i_2(gtxe2_i_2),
        .gtxe2_i_3(gtxe2_i_3),
        .gtxe2_i_4(gtxe2_i_4),
        .gtxe2_i_5(gtxe2_i_5),
        .gtxe2_i_6(gtxe2_i_6),
        .gtxe2_i_7(gtxe2_i_7),
        .gtxe2_i_8(gtxe2_i_8),
        .gtxe2_i_9(gtxe2_i_9),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out),
        .reset(reset),
        .reset_out(reset_out),
        .rx_fsm_reset_done_int_reg(rx_fsm_reset_done_int_reg),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module gig_ethernet_pcs_pma_0_GTWIZARD_GT
   (gtxe2_i_0,
    gt0_cpllrefclklost_i,
    txn,
    txp,
    rxoutclk,
    gtxe2_i_1,
    txoutclk,
    gtxe2_i_2,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i_3,
    gtxe2_i_4,
    gtxe2_i_5,
    gtxe2_i_6,
    gtxe2_i_7,
    independent_clock_bufg,
    cpll_pd0_i,
    cpllreset_in,
    gtrefclk_bufg,
    gtrefclk_out,
    SR,
    gt0_gttxreset_in0_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gt0_rxuserrdy_t,
    gtxe2_i_8,
    TXPD,
    gt0_txuserrdy_t,
    RXPD,
    Q,
    gtxe2_i_9,
    gtxe2_i_10,
    gtxe2_i_11);
  output gtxe2_i_0;
  output gt0_cpllrefclklost_i;
  output txn;
  output txp;
  output rxoutclk;
  output gtxe2_i_1;
  output txoutclk;
  output gtxe2_i_2;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i_3;
  output [1:0]gtxe2_i_4;
  output [1:0]gtxe2_i_5;
  output [1:0]gtxe2_i_6;
  output [1:0]gtxe2_i_7;
  input independent_clock_bufg;
  input cpll_pd0_i;
  input cpllreset_in;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input [0:0]SR;
  input gt0_gttxreset_in0_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gt0_rxuserrdy_t;
  input gtxe2_i_8;
  input [0:0]TXPD;
  input gt0_txuserrdy_t;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_9;
  input [1:0]gtxe2_i_10;
  input [1:0]gtxe2_i_11;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]SR;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire cpll_pd0_i;
  wire cpllreset_in;
  wire gt0_cpllrefclklost_i;
  wire gt0_gttxreset_in0_out;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gt0_rxuserrdy_t;
  wire gt0_txuserrdy_t;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtxe2_i_0;
  wire gtxe2_i_1;
  wire [1:0]gtxe2_i_10;
  wire [1:0]gtxe2_i_11;
  wire gtxe2_i_2;
  wire [15:0]gtxe2_i_3;
  wire [1:0]gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire [1:0]gtxe2_i_7;
  wire gtxe2_i_8;
  wire [1:0]gtxe2_i_9;
  wire gtxe2_i_n_0;
  wire gtxe2_i_n_10;
  wire gtxe2_i_n_16;
  wire gtxe2_i_n_170;
  wire gtxe2_i_n_171;
  wire gtxe2_i_n_172;
  wire gtxe2_i_n_173;
  wire gtxe2_i_n_174;
  wire gtxe2_i_n_175;
  wire gtxe2_i_n_176;
  wire gtxe2_i_n_177;
  wire gtxe2_i_n_178;
  wire gtxe2_i_n_179;
  wire gtxe2_i_n_180;
  wire gtxe2_i_n_181;
  wire gtxe2_i_n_182;
  wire gtxe2_i_n_183;
  wire gtxe2_i_n_184;
  wire gtxe2_i_n_27;
  wire gtxe2_i_n_3;
  wire gtxe2_i_n_38;
  wire gtxe2_i_n_39;
  wire gtxe2_i_n_4;
  wire gtxe2_i_n_46;
  wire gtxe2_i_n_47;
  wire gtxe2_i_n_48;
  wire gtxe2_i_n_49;
  wire gtxe2_i_n_50;
  wire gtxe2_i_n_51;
  wire gtxe2_i_n_52;
  wire gtxe2_i_n_53;
  wire gtxe2_i_n_54;
  wire gtxe2_i_n_55;
  wire gtxe2_i_n_56;
  wire gtxe2_i_n_57;
  wire gtxe2_i_n_58;
  wire gtxe2_i_n_59;
  wire gtxe2_i_n_60;
  wire gtxe2_i_n_61;
  wire gtxe2_i_n_81;
  wire gtxe2_i_n_83;
  wire gtxe2_i_n_84;
  wire gtxe2_i_n_9;
  wire independent_clock_bufg;
  wire reset;
  wire reset_out;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;
  wire NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED;
  wire NLW_gtxe2_i_PHYSTATUS_UNCONNECTED;
  wire NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED;
  wire NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED;
  wire NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED;
  wire NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXDATAVALID_UNCONNECTED;
  wire NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXELECIDLE_UNCONNECTED;
  wire NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED;
  wire NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED;
  wire NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED;
  wire NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXQPISENN_UNCONNECTED;
  wire NLW_gtxe2_i_RXQPISENP_UNCONNECTED;
  wire NLW_gtxe2_i_RXRATEDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED;
  wire NLW_gtxe2_i_RXVALID_UNCONNECTED;
  wire NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED;
  wire NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED;
  wire NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED;
  wire NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED;
  wire NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED;
  wire NLW_gtxe2_i_TXQPISENN_UNCONNECTED;
  wire NLW_gtxe2_i_TXQPISENP_UNCONNECTED;
  wire NLW_gtxe2_i_TXRATEDONE_UNCONNECTED;
  wire [15:0]NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXCHARISK_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXCHBONDO_UNCONNECTED;
  wire [63:16]NLW_gtxe2_i_RXDATA_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXDISPERR_UNCONNECTED;
  wire [2:0]NLW_gtxe2_i_RXHEADER_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED;
  wire [2:0]NLW_gtxe2_i_RXSTATUS_UNCONNECTED;
  wire [9:0]NLW_gtxe2_i_TSTOUT_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  GTXE2_CHANNEL #(
    .ALIGN_COMMA_DOUBLE("FALSE"),
    .ALIGN_COMMA_ENABLE(10'b0001111111),
    .ALIGN_COMMA_WORD(2),
    .ALIGN_MCOMMA_DET("TRUE"),
    .ALIGN_MCOMMA_VALUE(10'b1010000011),
    .ALIGN_PCOMMA_DET("TRUE"),
    .ALIGN_PCOMMA_VALUE(10'b0101111100),
    .CBCC_DATA_SOURCE_SEL("DECODED"),
    .CHAN_BOND_KEEP_ALIGN("FALSE"),
    .CHAN_BOND_MAX_SKEW(1),
    .CHAN_BOND_SEQ_1_1(10'b0000000000),
    .CHAN_BOND_SEQ_1_2(10'b0000000000),
    .CHAN_BOND_SEQ_1_3(10'b0000000000),
    .CHAN_BOND_SEQ_1_4(10'b0000000000),
    .CHAN_BOND_SEQ_1_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_1(10'b0000000000),
    .CHAN_BOND_SEQ_2_2(10'b0000000000),
    .CHAN_BOND_SEQ_2_3(10'b0000000000),
    .CHAN_BOND_SEQ_2_4(10'b0000000000),
    .CHAN_BOND_SEQ_2_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_USE("FALSE"),
    .CHAN_BOND_SEQ_LEN(1),
    .CLK_CORRECT_USE("TRUE"),
    .CLK_COR_KEEP_IDLE("FALSE"),
    .CLK_COR_MAX_LAT(36),
    .CLK_COR_MIN_LAT(33),
    .CLK_COR_PRECEDENCE("TRUE"),
    .CLK_COR_REPEAT_WAIT(0),
    .CLK_COR_SEQ_1_1(10'b0110111100),
    .CLK_COR_SEQ_1_2(10'b0001010000),
    .CLK_COR_SEQ_1_3(10'b0000000000),
    .CLK_COR_SEQ_1_4(10'b0000000000),
    .CLK_COR_SEQ_1_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_1(10'b0110111100),
    .CLK_COR_SEQ_2_2(10'b0010110101),
    .CLK_COR_SEQ_2_3(10'b0000000000),
    .CLK_COR_SEQ_2_4(10'b0000000000),
    .CLK_COR_SEQ_2_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_USE("TRUE"),
    .CLK_COR_SEQ_LEN(2),
    .CPLL_CFG(24'hBC07DC),
    .CPLL_FBDIV(4),
    .CPLL_FBDIV_45(5),
    .CPLL_INIT_CFG(24'h00001E),
    .CPLL_LOCK_CFG(16'h01E8),
    .CPLL_REFCLK_DIV(1),
    .DEC_MCOMMA_DETECT("TRUE"),
    .DEC_PCOMMA_DETECT("TRUE"),
    .DEC_VALID_COMMA_ONLY("FALSE"),
    .DMONITOR_CFG(24'h000A00),
    .ES_CONTROL(6'b000000),
    .ES_ERRDET_EN("FALSE"),
    .ES_EYE_SCAN_EN("TRUE"),
    .ES_HORZ_OFFSET(12'h000),
    .ES_PMA_CFG(10'b0000000000),
    .ES_PRESCALE(5'b00000),
    .ES_QUALIFIER(80'h00000000000000000000),
    .ES_QUAL_MASK(80'h00000000000000000000),
    .ES_SDATA_MASK(80'h00000000000000000000),
    .ES_VERT_OFFSET(9'b000000000),
    .FTS_DESKEW_SEQ_ENABLE(4'b1111),
    .FTS_LANE_DESKEW_CFG(4'b1111),
    .FTS_LANE_DESKEW_EN("FALSE"),
    .GEARBOX_MODE(3'b000),
    .IS_CPLLLOCKDETCLK_INVERTED(1'b0),
    .IS_DRPCLK_INVERTED(1'b0),
    .IS_GTGREFCLK_INVERTED(1'b0),
    .IS_RXUSRCLK2_INVERTED(1'b0),
    .IS_RXUSRCLK_INVERTED(1'b0),
    .IS_TXPHDLYTSTCLK_INVERTED(1'b0),
    .IS_TXUSRCLK2_INVERTED(1'b0),
    .IS_TXUSRCLK_INVERTED(1'b0),
    .OUTREFCLK_SEL_INV(2'b11),
    .PCS_PCIE_EN("FALSE"),
    .PCS_RSVD_ATTR(48'h000000000000),
    .PD_TRANS_TIME_FROM_P2(12'h03C),
    .PD_TRANS_TIME_NONE_P2(8'h19),
    .PD_TRANS_TIME_TO_P2(8'h64),
    .PMA_RSV(32'h00018480),
    .PMA_RSV2(16'h2050),
    .PMA_RSV3(2'b00),
    .PMA_RSV4(32'h00000000),
    .RXBUFRESET_TIME(5'b00001),
    .RXBUF_ADDR_MODE("FULL"),
    .RXBUF_EIDLE_HI_CNT(4'b1000),
    .RXBUF_EIDLE_LO_CNT(4'b0000),
    .RXBUF_EN("TRUE"),
    .RXBUF_RESET_ON_CB_CHANGE("TRUE"),
    .RXBUF_RESET_ON_COMMAALIGN("FALSE"),
    .RXBUF_RESET_ON_EIDLE("FALSE"),
    .RXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .RXBUF_THRESH_OVFLW(61),
    .RXBUF_THRESH_OVRD("FALSE"),
    .RXBUF_THRESH_UNDFLW(8),
    .RXCDRFREQRESET_TIME(5'b00001),
    .RXCDRPHRESET_TIME(5'b00001),
    .RXCDR_CFG(72'h03000023FF10100020),
    .RXCDR_FR_RESET_ON_EIDLE(1'b0),
    .RXCDR_HOLD_DURING_EIDLE(1'b0),
    .RXCDR_LOCK_CFG(6'b010101),
    .RXCDR_PH_RESET_ON_EIDLE(1'b0),
    .RXDFELPMRESET_TIME(7'b0001111),
    .RXDLY_CFG(16'h001F),
    .RXDLY_LCFG(9'h030),
    .RXDLY_TAP_CFG(16'h0000),
    .RXGEARBOX_EN("FALSE"),
    .RXISCANRESET_TIME(5'b00001),
    .RXLPM_HF_CFG(14'b00000011110000),
    .RXLPM_LF_CFG(14'b00000011110000),
    .RXOOB_CFG(7'b0000110),
    .RXOUT_DIV(4),
    .RXPCSRESET_TIME(5'b00001),
    .RXPHDLY_CFG(24'h084020),
    .RXPH_CFG(24'h000000),
    .RXPH_MONITOR_SEL(5'b00000),
    .RXPMARESET_TIME(5'b00011),
    .RXPRBS_ERR_LOOPBACK(1'b0),
    .RXSLIDE_AUTO_WAIT(7),
    .RXSLIDE_MODE("OFF"),
    .RX_BIAS_CFG(12'b000000000100),
    .RX_BUFFER_CFG(6'b000000),
    .RX_CLK25_DIV(5),
    .RX_CLKMUX_PD(1'b1),
    .RX_CM_SEL(2'b11),
    .RX_CM_TRIM(3'b010),
    .RX_DATA_WIDTH(20),
    .RX_DDI_SEL(6'b000000),
    .RX_DEBUG_CFG(12'b000000000000),
    .RX_DEFER_RESET_BUF_EN("TRUE"),
    .RX_DFE_GAIN_CFG(23'h020FEA),
    .RX_DFE_H2_CFG(12'b000000000000),
    .RX_DFE_H3_CFG(12'b000001000000),
    .RX_DFE_H4_CFG(11'b00011110000),
    .RX_DFE_H5_CFG(11'b00011100000),
    .RX_DFE_KL_CFG(13'b0000011111110),
    .RX_DFE_KL_CFG2(32'h301148AC),
    .RX_DFE_LPM_CFG(16'h0904),
    .RX_DFE_LPM_HOLD_DURING_EIDLE(1'b0),
    .RX_DFE_UT_CFG(17'b10001111000000000),
    .RX_DFE_VP_CFG(17'b00011111100000011),
    .RX_DFE_XYD_CFG(13'b0000000000000),
    .RX_DISPERR_SEQ_MATCH("TRUE"),
    .RX_INT_DATAWIDTH(0),
    .RX_OS_CFG(13'b0000010000000),
    .RX_SIG_VALID_DLY(10),
    .RX_XCLK_SEL("RXREC"),
    .SAS_MAX_COM(64),
    .SAS_MIN_COM(36),
    .SATA_BURST_SEQ_LEN(4'b0101),
    .SATA_BURST_VAL(3'b100),
    .SATA_CPLL_CFG("VCO_3000MHZ"),
    .SATA_EIDLE_VAL(3'b100),
    .SATA_MAX_BURST(8),
    .SATA_MAX_INIT(21),
    .SATA_MAX_WAKE(7),
    .SATA_MIN_BURST(4),
    .SATA_MIN_INIT(12),
    .SATA_MIN_WAKE(4),
    .SHOW_REALIGN_COMMA("TRUE"),
    .SIM_CPLLREFCLK_SEL(3'b001),
    .SIM_RECEIVER_DETECT_PASS("TRUE"),
    .SIM_RESET_SPEEDUP("TRUE"),
    .SIM_TX_EIDLE_DRIVE_LEVEL("X"),
    .SIM_VERSION("4.0"),
    .TERM_RCAL_CFG(5'b10000),
    .TERM_RCAL_OVRD(1'b0),
    .TRANS_TIME_RATE(8'h0E),
    .TST_RSV(32'h00000000),
    .TXBUF_EN("TRUE"),
    .TXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .TXDLY_CFG(16'h001F),
    .TXDLY_LCFG(9'h030),
    .TXDLY_TAP_CFG(16'h0000),
    .TXGEARBOX_EN("FALSE"),
    .TXOUT_DIV(4),
    .TXPCSRESET_TIME(5'b00001),
    .TXPHDLY_CFG(24'h084020),
    .TXPH_CFG(16'h0780),
    .TXPH_MONITOR_SEL(5'b00000),
    .TXPMARESET_TIME(5'b00001),
    .TX_CLK25_DIV(5),
    .TX_CLKMUX_PD(1'b1),
    .TX_DATA_WIDTH(20),
    .TX_DEEMPH0(5'b00000),
    .TX_DEEMPH1(5'b00000),
    .TX_DRIVE_MODE("DIRECT"),
    .TX_EIDLE_ASSERT_DELAY(3'b110),
    .TX_EIDLE_DEASSERT_DELAY(3'b100),
    .TX_INT_DATAWIDTH(0),
    .TX_LOOPBACK_DRIVE_HIZ("FALSE"),
    .TX_MAINCURSOR_SEL(1'b0),
    .TX_MARGIN_FULL_0(7'b1001110),
    .TX_MARGIN_FULL_1(7'b1001001),
    .TX_MARGIN_FULL_2(7'b1000101),
    .TX_MARGIN_FULL_3(7'b1000010),
    .TX_MARGIN_FULL_4(7'b1000000),
    .TX_MARGIN_LOW_0(7'b1000110),
    .TX_MARGIN_LOW_1(7'b1000100),
    .TX_MARGIN_LOW_2(7'b1000010),
    .TX_MARGIN_LOW_3(7'b1000000),
    .TX_MARGIN_LOW_4(7'b1000000),
    .TX_PREDRIVER_MODE(1'b0),
    .TX_QPI_STATUS_EN(1'b0),
    .TX_RXDETECT_CFG(14'h1832),
    .TX_RXDETECT_REF(3'b100),
    .TX_XCLK_SEL("TXOUT"),
    .UCODEER_CLR(1'b0)) 
    gtxe2_i
       (.CFGRESET(1'b0),
        .CLKRSVD({1'b0,1'b0,1'b0,1'b0}),
        .CPLLFBCLKLOST(gtxe2_i_n_0),
        .CPLLLOCK(gtxe2_i_0),
        .CPLLLOCKDETCLK(independent_clock_bufg),
        .CPLLLOCKEN(1'b1),
        .CPLLPD(cpll_pd0_i),
        .CPLLREFCLKLOST(gt0_cpllrefclklost_i),
        .CPLLREFCLKSEL({1'b0,1'b0,1'b1}),
        .CPLLRESET(cpllreset_in),
        .DMONITOROUT({gtxe2_i_n_177,gtxe2_i_n_178,gtxe2_i_n_179,gtxe2_i_n_180,gtxe2_i_n_181,gtxe2_i_n_182,gtxe2_i_n_183,gtxe2_i_n_184}),
        .DRPADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPCLK(gtrefclk_bufg),
        .DRPDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPDO({gtxe2_i_n_46,gtxe2_i_n_47,gtxe2_i_n_48,gtxe2_i_n_49,gtxe2_i_n_50,gtxe2_i_n_51,gtxe2_i_n_52,gtxe2_i_n_53,gtxe2_i_n_54,gtxe2_i_n_55,gtxe2_i_n_56,gtxe2_i_n_57,gtxe2_i_n_58,gtxe2_i_n_59,gtxe2_i_n_60,gtxe2_i_n_61}),
        .DRPEN(1'b0),
        .DRPRDY(gtxe2_i_n_3),
        .DRPWE(1'b0),
        .EYESCANDATAERROR(gtxe2_i_n_4),
        .EYESCANMODE(1'b0),
        .EYESCANRESET(1'b0),
        .EYESCANTRIGGER(1'b0),
        .GTGREFCLK(1'b0),
        .GTNORTHREFCLK0(1'b0),
        .GTNORTHREFCLK1(1'b0),
        .GTREFCLK0(gtrefclk_out),
        .GTREFCLK1(1'b0),
        .GTREFCLKMONITOR(NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED),
        .GTRESETSEL(1'b0),
        .GTRSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .GTRXRESET(SR),
        .GTSOUTHREFCLK0(1'b0),
        .GTSOUTHREFCLK1(1'b0),
        .GTTXRESET(gt0_gttxreset_in0_out),
        .GTXRXN(rxn),
        .GTXRXP(rxp),
        .GTXTXN(txn),
        .GTXTXP(txp),
        .LOOPBACK({1'b0,1'b0,1'b0}),
        .PCSRSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDIN2({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDOUT(NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED[15:0]),
        .PHYSTATUS(NLW_gtxe2_i_PHYSTATUS_UNCONNECTED),
        .PMARSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PMARSVDIN2({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLCLK(gt0_qplloutclk_out),
        .QPLLREFCLK(gt0_qplloutrefclk_out),
        .RESETOVRD(1'b0),
        .RX8B10BEN(1'b1),
        .RXBUFRESET(1'b0),
        .RXBUFSTATUS({RXBUFSTATUS,gtxe2_i_n_83,gtxe2_i_n_84}),
        .RXBYTEISALIGNED(gtxe2_i_n_9),
        .RXBYTEREALIGN(gtxe2_i_n_10),
        .RXCDRFREQRESET(1'b0),
        .RXCDRHOLD(1'b0),
        .RXCDRLOCK(NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED),
        .RXCDROVRDEN(1'b0),
        .RXCDRRESET(1'b0),
        .RXCDRRESETRSV(1'b0),
        .RXCHANBONDSEQ(NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED),
        .RXCHANISALIGNED(NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED),
        .RXCHANREALIGN(NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED),
        .RXCHARISCOMMA({NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED[7:2],gtxe2_i_4}),
        .RXCHARISK({NLW_gtxe2_i_RXCHARISK_UNCONNECTED[7:2],gtxe2_i_5}),
        .RXCHBONDEN(1'b0),
        .RXCHBONDI({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RXCHBONDLEVEL({1'b0,1'b0,1'b0}),
        .RXCHBONDMASTER(1'b0),
        .RXCHBONDO(NLW_gtxe2_i_RXCHBONDO_UNCONNECTED[4:0]),
        .RXCHBONDSLAVE(1'b0),
        .RXCLKCORCNT(D),
        .RXCOMINITDET(NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED),
        .RXCOMMADET(gtxe2_i_n_16),
        .RXCOMMADETEN(1'b1),
        .RXCOMSASDET(NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED),
        .RXCOMWAKEDET(NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED),
        .RXDATA({NLW_gtxe2_i_RXDATA_UNCONNECTED[63:16],gtxe2_i_3}),
        .RXDATAVALID(NLW_gtxe2_i_RXDATAVALID_UNCONNECTED),
        .RXDDIEN(1'b0),
        .RXDFEAGCHOLD(1'b0),
        .RXDFEAGCOVRDEN(1'b0),
        .RXDFECM1EN(1'b0),
        .RXDFELFHOLD(1'b0),
        .RXDFELFOVRDEN(1'b0),
        .RXDFELPMRESET(1'b0),
        .RXDFETAP2HOLD(1'b0),
        .RXDFETAP2OVRDEN(1'b0),
        .RXDFETAP3HOLD(1'b0),
        .RXDFETAP3OVRDEN(1'b0),
        .RXDFETAP4HOLD(1'b0),
        .RXDFETAP4OVRDEN(1'b0),
        .RXDFETAP5HOLD(1'b0),
        .RXDFETAP5OVRDEN(1'b0),
        .RXDFEUTHOLD(1'b0),
        .RXDFEUTOVRDEN(1'b0),
        .RXDFEVPHOLD(1'b0),
        .RXDFEVPOVRDEN(1'b0),
        .RXDFEVSEN(1'b0),
        .RXDFEXYDEN(1'b1),
        .RXDFEXYDHOLD(1'b0),
        .RXDFEXYDOVRDEN(1'b0),
        .RXDISPERR({NLW_gtxe2_i_RXDISPERR_UNCONNECTED[7:2],gtxe2_i_6}),
        .RXDLYBYPASS(1'b1),
        .RXDLYEN(1'b0),
        .RXDLYOVRDEN(1'b0),
        .RXDLYSRESET(1'b0),
        .RXDLYSRESETDONE(NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED),
        .RXELECIDLE(NLW_gtxe2_i_RXELECIDLE_UNCONNECTED),
        .RXELECIDLEMODE({1'b1,1'b1}),
        .RXGEARBOXSLIP(1'b0),
        .RXHEADER(NLW_gtxe2_i_RXHEADER_UNCONNECTED[2:0]),
        .RXHEADERVALID(NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED),
        .RXLPMEN(1'b1),
        .RXLPMHFHOLD(1'b0),
        .RXLPMHFOVRDEN(1'b0),
        .RXLPMLFHOLD(1'b0),
        .RXLPMLFKLOVRDEN(1'b0),
        .RXMCOMMAALIGNEN(reset_out),
        .RXMONITOROUT({gtxe2_i_n_170,gtxe2_i_n_171,gtxe2_i_n_172,gtxe2_i_n_173,gtxe2_i_n_174,gtxe2_i_n_175,gtxe2_i_n_176}),
        .RXMONITORSEL({1'b0,1'b0}),
        .RXNOTINTABLE({NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED[7:2],gtxe2_i_7}),
        .RXOOBRESET(1'b0),
        .RXOSHOLD(1'b0),
        .RXOSOVRDEN(1'b0),
        .RXOUTCLK(rxoutclk),
        .RXOUTCLKFABRIC(NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED),
        .RXOUTCLKPCS(NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED),
        .RXOUTCLKSEL({1'b0,1'b1,1'b0}),
        .RXPCOMMAALIGNEN(reset_out),
        .RXPCSRESET(reset),
        .RXPD({RXPD,RXPD}),
        .RXPHALIGN(1'b0),
        .RXPHALIGNDONE(NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED),
        .RXPHALIGNEN(1'b0),
        .RXPHDLYPD(1'b0),
        .RXPHDLYRESET(1'b0),
        .RXPHMONITOR(NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED[4:0]),
        .RXPHOVRDEN(1'b0),
        .RXPHSLIPMONITOR(NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED[4:0]),
        .RXPMARESET(1'b0),
        .RXPOLARITY(1'b0),
        .RXPRBSCNTRESET(1'b0),
        .RXPRBSERR(gtxe2_i_n_27),
        .RXPRBSSEL({1'b0,1'b0,1'b0}),
        .RXQPIEN(1'b0),
        .RXQPISENN(NLW_gtxe2_i_RXQPISENN_UNCONNECTED),
        .RXQPISENP(NLW_gtxe2_i_RXQPISENP_UNCONNECTED),
        .RXRATE({1'b0,1'b0,1'b0}),
        .RXRATEDONE(NLW_gtxe2_i_RXRATEDONE_UNCONNECTED),
        .RXRESETDONE(gtxe2_i_1),
        .RXSLIDE(1'b0),
        .RXSTARTOFSEQ(NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED),
        .RXSTATUS(NLW_gtxe2_i_RXSTATUS_UNCONNECTED[2:0]),
        .RXSYSCLKSEL({1'b0,1'b0}),
        .RXUSERRDY(gt0_rxuserrdy_t),
        .RXUSRCLK(gtxe2_i_8),
        .RXUSRCLK2(gtxe2_i_8),
        .RXVALID(NLW_gtxe2_i_RXVALID_UNCONNECTED),
        .SETERRSTATUS(1'b0),
        .TSTIN({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .TSTOUT(NLW_gtxe2_i_TSTOUT_UNCONNECTED[9:0]),
        .TX8B10BBYPASS({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX8B10BEN(1'b1),
        .TXBUFDIFFCTRL({1'b1,1'b0,1'b0}),
        .TXBUFSTATUS({TXBUFSTATUS,gtxe2_i_n_81}),
        .TXCHARDISPMODE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtxe2_i_9}),
        .TXCHARDISPVAL({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtxe2_i_10}),
        .TXCHARISK({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtxe2_i_11}),
        .TXCOMFINISH(NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED),
        .TXCOMINIT(1'b0),
        .TXCOMSAS(1'b0),
        .TXCOMWAKE(1'b0),
        .TXDATA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q}),
        .TXDEEMPH(1'b0),
        .TXDETECTRX(1'b0),
        .TXDIFFCTRL({1'b1,1'b0,1'b0,1'b0}),
        .TXDIFFPD(1'b0),
        .TXDLYBYPASS(1'b1),
        .TXDLYEN(1'b0),
        .TXDLYHOLD(1'b0),
        .TXDLYOVRDEN(1'b0),
        .TXDLYSRESET(1'b0),
        .TXDLYSRESETDONE(NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED),
        .TXDLYUPDOWN(1'b0),
        .TXELECIDLE(TXPD),
        .TXGEARBOXREADY(NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED),
        .TXHEADER({1'b0,1'b0,1'b0}),
        .TXINHIBIT(1'b0),
        .TXMAINCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXMARGIN({1'b0,1'b0,1'b0}),
        .TXOUTCLK(txoutclk),
        .TXOUTCLKFABRIC(gtxe2_i_n_38),
        .TXOUTCLKPCS(gtxe2_i_n_39),
        .TXOUTCLKSEL({1'b1,1'b0,1'b0}),
        .TXPCSRESET(1'b0),
        .TXPD({TXPD,TXPD}),
        .TXPDELECIDLEMODE(1'b0),
        .TXPHALIGN(1'b0),
        .TXPHALIGNDONE(NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED),
        .TXPHALIGNEN(1'b0),
        .TXPHDLYPD(1'b0),
        .TXPHDLYRESET(1'b0),
        .TXPHDLYTSTCLK(1'b0),
        .TXPHINIT(1'b0),
        .TXPHINITDONE(NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED),
        .TXPHOVRDEN(1'b0),
        .TXPISOPD(1'b0),
        .TXPMARESET(1'b0),
        .TXPOLARITY(1'b0),
        .TXPOSTCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPOSTCURSORINV(1'b0),
        .TXPRBSFORCEERR(1'b0),
        .TXPRBSSEL({1'b0,1'b0,1'b0}),
        .TXPRECURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPRECURSORINV(1'b0),
        .TXQPIBIASEN(1'b0),
        .TXQPISENN(NLW_gtxe2_i_TXQPISENN_UNCONNECTED),
        .TXQPISENP(NLW_gtxe2_i_TXQPISENP_UNCONNECTED),
        .TXQPISTRONGPDOWN(1'b0),
        .TXQPIWEAKPUP(1'b0),
        .TXRATE({1'b0,1'b0,1'b0}),
        .TXRATEDONE(NLW_gtxe2_i_TXRATEDONE_UNCONNECTED),
        .TXRESETDONE(gtxe2_i_2),
        .TXSEQUENCE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXSTARTSEQ(1'b0),
        .TXSWING(1'b0),
        .TXSYSCLKSEL({1'b0,1'b0}),
        .TXUSERRDY(gt0_txuserrdy_t),
        .TXUSRCLK(gtxe2_i_8),
        .TXUSRCLK2(gtxe2_i_8));
endmodule

module gig_ethernet_pcs_pma_0_GTWIZARD_init
   (txn,
    txp,
    rxoutclk,
    txoutclk,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i,
    gtxe2_i_0,
    gtxe2_i_1,
    gtxe2_i_2,
    gtxe2_i_3,
    mmcm_reset,
    data_in,
    rx_fsm_reset_done_int_reg,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gtxe2_i_4,
    TXPD,
    RXPD,
    Q,
    gtxe2_i_5,
    gtxe2_i_6,
    gtxe2_i_7,
    out,
    gtxe2_i_8,
    gtxe2_i_9,
    data_sync_reg1,
    data_out);
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i;
  output [1:0]gtxe2_i_0;
  output [1:0]gtxe2_i_1;
  output [1:0]gtxe2_i_2;
  output [1:0]gtxe2_i_3;
  output mmcm_reset;
  output data_in;
  output rx_fsm_reset_done_int_reg;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gtxe2_i_4;
  input [0:0]TXPD;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_5;
  input [1:0]gtxe2_i_6;
  input [1:0]gtxe2_i_7;
  input [0:0]out;
  input gtxe2_i_8;
  input gtxe2_i_9;
  input data_sync_reg1;
  input data_out;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire [13:1]data0;
  wire data_in;
  wire data_out;
  wire data_sync_reg1;
  wire gt0_cpllrefclklost_i;
  wire gt0_cpllreset_t;
  wire gt0_gtrxreset_in1_out;
  wire gt0_gttxreset_in0_out;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire [13:0]gt0_rx_cdrlock_counter;
  wire gt0_rx_cdrlock_counter0_carry__0_n_0;
  wire gt0_rx_cdrlock_counter0_carry__0_n_1;
  wire gt0_rx_cdrlock_counter0_carry__0_n_2;
  wire gt0_rx_cdrlock_counter0_carry__0_n_3;
  wire gt0_rx_cdrlock_counter0_carry__1_n_0;
  wire gt0_rx_cdrlock_counter0_carry__1_n_1;
  wire gt0_rx_cdrlock_counter0_carry__1_n_2;
  wire gt0_rx_cdrlock_counter0_carry__1_n_3;
  wire gt0_rx_cdrlock_counter0_carry_n_0;
  wire gt0_rx_cdrlock_counter0_carry_n_1;
  wire gt0_rx_cdrlock_counter0_carry_n_2;
  wire gt0_rx_cdrlock_counter0_carry_n_3;
  wire \gt0_rx_cdrlock_counter[0]_i_2_n_0 ;
  wire \gt0_rx_cdrlock_counter[13]_i_2_n_0 ;
  wire \gt0_rx_cdrlock_counter[13]_i_3_n_0 ;
  wire \gt0_rx_cdrlock_counter[13]_i_4_n_0 ;
  wire [13:0]gt0_rx_cdrlock_counter_0;
  wire gt0_rx_cdrlocked_i_1_n_0;
  wire gt0_rx_cdrlocked_reg_n_0;
  wire gt0_rxuserrdy_t;
  wire gt0_txuserrdy_t;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtwizard_i_n_0;
  wire gtwizard_i_n_5;
  wire gtwizard_i_n_7;
  wire [15:0]gtxe2_i;
  wire [1:0]gtxe2_i_0;
  wire [1:0]gtxe2_i_1;
  wire [1:0]gtxe2_i_2;
  wire [1:0]gtxe2_i_3;
  wire gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire [1:0]gtxe2_i_7;
  wire gtxe2_i_8;
  wire gtxe2_i_9;
  wire independent_clock_bufg;
  wire mmcm_reset;
  wire [0:0]out;
  wire reset;
  wire reset_out;
  wire rx_fsm_reset_done_int_reg;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;
  wire [3:0]NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED;
  wire [3:1]NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED;

  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry
       (.CI(1'b0),
        .CO({gt0_rx_cdrlock_counter0_carry_n_0,gt0_rx_cdrlock_counter0_carry_n_1,gt0_rx_cdrlock_counter0_carry_n_2,gt0_rx_cdrlock_counter0_carry_n_3}),
        .CYINIT(gt0_rx_cdrlock_counter[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[4:1]),
        .S(gt0_rx_cdrlock_counter[4:1]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry__0
       (.CI(gt0_rx_cdrlock_counter0_carry_n_0),
        .CO({gt0_rx_cdrlock_counter0_carry__0_n_0,gt0_rx_cdrlock_counter0_carry__0_n_1,gt0_rx_cdrlock_counter0_carry__0_n_2,gt0_rx_cdrlock_counter0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[8:5]),
        .S(gt0_rx_cdrlock_counter[8:5]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry__1
       (.CI(gt0_rx_cdrlock_counter0_carry__0_n_0),
        .CO({gt0_rx_cdrlock_counter0_carry__1_n_0,gt0_rx_cdrlock_counter0_carry__1_n_1,gt0_rx_cdrlock_counter0_carry__1_n_2,gt0_rx_cdrlock_counter0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[12:9]),
        .S(gt0_rx_cdrlock_counter[12:9]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry__2
       (.CI(gt0_rx_cdrlock_counter0_carry__1_n_0),
        .CO(NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED[3:1],data0[13]}),
        .S({1'b0,1'b0,1'b0,gt0_rx_cdrlock_counter[13]}));
  LUT2 #(
    .INIT(4'h2)) 
    \gt0_rx_cdrlock_counter[0]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[0]_i_2_n_0 ),
        .I1(gt0_rx_cdrlock_counter[0]),
        .O(gt0_rx_cdrlock_counter_0[0]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFB)) 
    \gt0_rx_cdrlock_counter[0]_i_2 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I1(gt0_rx_cdrlock_counter[4]),
        .I2(gt0_rx_cdrlock_counter[5]),
        .I3(gt0_rx_cdrlock_counter[7]),
        .I4(gt0_rx_cdrlock_counter[6]),
        .I5(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .O(\gt0_rx_cdrlock_counter[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[10]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[10]),
        .O(gt0_rx_cdrlock_counter_0[10]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[11]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[11]),
        .O(gt0_rx_cdrlock_counter_0[11]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[12]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[12]),
        .O(gt0_rx_cdrlock_counter_0[12]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[13]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[13]),
        .O(gt0_rx_cdrlock_counter_0[13]));
  LUT5 #(
    .INIT(32'hFFFFFFEF)) 
    \gt0_rx_cdrlock_counter[13]_i_2 
       (.I0(gt0_rx_cdrlock_counter[1]),
        .I1(gt0_rx_cdrlock_counter[12]),
        .I2(gt0_rx_cdrlock_counter[13]),
        .I3(gt0_rx_cdrlock_counter[3]),
        .I4(gt0_rx_cdrlock_counter[2]),
        .O(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \gt0_rx_cdrlock_counter[13]_i_3 
       (.I0(gt0_rx_cdrlock_counter[4]),
        .I1(gt0_rx_cdrlock_counter[5]),
        .I2(gt0_rx_cdrlock_counter[7]),
        .I3(gt0_rx_cdrlock_counter[6]),
        .O(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFF7F)) 
    \gt0_rx_cdrlock_counter[13]_i_4 
       (.I0(gt0_rx_cdrlock_counter[9]),
        .I1(gt0_rx_cdrlock_counter[8]),
        .I2(gt0_rx_cdrlock_counter[10]),
        .I3(gt0_rx_cdrlock_counter[11]),
        .O(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[1]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[1]),
        .O(gt0_rx_cdrlock_counter_0[1]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[2]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[2]),
        .O(gt0_rx_cdrlock_counter_0[2]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[3]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[3]),
        .O(gt0_rx_cdrlock_counter_0[3]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[4]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[4]),
        .O(gt0_rx_cdrlock_counter_0[4]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[5]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[5]),
        .O(gt0_rx_cdrlock_counter_0[5]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[6]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[6]),
        .O(gt0_rx_cdrlock_counter_0[6]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[7]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[7]),
        .O(gt0_rx_cdrlock_counter_0[7]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[8]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[8]),
        .O(gt0_rx_cdrlock_counter_0[8]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[9]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[9]),
        .O(gt0_rx_cdrlock_counter_0[9]));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[0]),
        .Q(gt0_rx_cdrlock_counter[0]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[10] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[10]),
        .Q(gt0_rx_cdrlock_counter[10]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[11] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[11]),
        .Q(gt0_rx_cdrlock_counter[11]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[12] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[12]),
        .Q(gt0_rx_cdrlock_counter[12]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[13] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[13]),
        .Q(gt0_rx_cdrlock_counter[13]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[1]),
        .Q(gt0_rx_cdrlock_counter[1]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[2]),
        .Q(gt0_rx_cdrlock_counter[2]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[3]),
        .Q(gt0_rx_cdrlock_counter[3]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[4] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[4]),
        .Q(gt0_rx_cdrlock_counter[4]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[5] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[5]),
        .Q(gt0_rx_cdrlock_counter[5]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[6] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[6]),
        .Q(gt0_rx_cdrlock_counter[6]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[7] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[7]),
        .Q(gt0_rx_cdrlock_counter[7]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[8] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[8]),
        .Q(gt0_rx_cdrlock_counter[8]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[9] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[9]),
        .Q(gt0_rx_cdrlock_counter[9]),
        .R(gt0_gtrxreset_in1_out));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    gt0_rx_cdrlocked_i_1
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(gt0_rx_cdrlocked_reg_n_0),
        .O(gt0_rx_cdrlocked_i_1_n_0));
  FDRE gt0_rx_cdrlocked_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlocked_i_1_n_0),
        .Q(gt0_rx_cdrlocked_reg_n_0),
        .R(gt0_gtrxreset_in1_out));
  gig_ethernet_pcs_pma_0_RX_STARTUP_FSM gt0_rxresetfsm_i
       (.\FSM_sequential_rx_state_reg[0]_0 (gt0_rx_cdrlocked_reg_n_0),
        .SR(gt0_gtrxreset_in1_out),
        .data_in(rx_fsm_reset_done_int_reg),
        .data_out(data_out),
        .data_sync_reg1(gtwizard_i_n_5),
        .data_sync_reg1_0(data_sync_reg1),
        .data_sync_reg1_1(gtwizard_i_n_0),
        .data_sync_reg6(gtxe2_i_4),
        .gt0_rxuserrdy_t(gt0_rxuserrdy_t),
        .gtxe2_i(gtxe2_i_8),
        .independent_clock_bufg(independent_clock_bufg),
        .out(out));
  gig_ethernet_pcs_pma_0_TX_STARTUP_FSM gt0_txresetfsm_i
       (.data_in(data_in),
        .data_sync_reg1(gtxe2_i_4),
        .data_sync_reg1_0(gtwizard_i_n_7),
        .data_sync_reg1_1(data_sync_reg1),
        .data_sync_reg1_2(gtwizard_i_n_0),
        .gt0_cpllrefclklost_i(gt0_cpllrefclklost_i),
        .gt0_cpllreset_t(gt0_cpllreset_t),
        .gt0_gttxreset_in0_out(gt0_gttxreset_in0_out),
        .gt0_txuserrdy_t(gt0_txuserrdy_t),
        .gtxe2_i(gtxe2_i_9),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out));
  gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt gtwizard_i
       (.D(D),
        .Q(Q),
        .RXBUFSTATUS(RXBUFSTATUS),
        .RXPD(RXPD),
        .SR(gt0_gtrxreset_in1_out),
        .TXBUFSTATUS(TXBUFSTATUS),
        .TXPD(TXPD),
        .gt0_cpllrefclklost_i(gt0_cpllrefclklost_i),
        .gt0_cpllreset_t(gt0_cpllreset_t),
        .gt0_gttxreset_in0_out(gt0_gttxreset_in0_out),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gt0_rxuserrdy_t(gt0_rxuserrdy_t),
        .gt0_txuserrdy_t(gt0_txuserrdy_t),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(gtwizard_i_n_0),
        .gtxe2_i_0(gtwizard_i_n_5),
        .gtxe2_i_1(gtwizard_i_n_7),
        .gtxe2_i_10(gtxe2_i_7),
        .gtxe2_i_2(gtxe2_i),
        .gtxe2_i_3(gtxe2_i_0),
        .gtxe2_i_4(gtxe2_i_1),
        .gtxe2_i_5(gtxe2_i_2),
        .gtxe2_i_6(gtxe2_i_3),
        .gtxe2_i_7(gtxe2_i_4),
        .gtxe2_i_8(gtxe2_i_5),
        .gtxe2_i_9(gtxe2_i_6),
        .independent_clock_bufg(independent_clock_bufg),
        .reset(reset),
        .reset_out(reset_out),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt
   (gtxe2_i,
    gt0_cpllrefclklost_i,
    txn,
    txp,
    rxoutclk,
    gtxe2_i_0,
    txoutclk,
    gtxe2_i_1,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i_2,
    gtxe2_i_3,
    gtxe2_i_4,
    gtxe2_i_5,
    gtxe2_i_6,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    SR,
    gt0_gttxreset_in0_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gt0_rxuserrdy_t,
    gtxe2_i_7,
    TXPD,
    gt0_txuserrdy_t,
    RXPD,
    Q,
    gtxe2_i_8,
    gtxe2_i_9,
    gtxe2_i_10,
    gt0_cpllreset_t);
  output gtxe2_i;
  output gt0_cpllrefclklost_i;
  output txn;
  output txp;
  output rxoutclk;
  output gtxe2_i_0;
  output txoutclk;
  output gtxe2_i_1;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i_2;
  output [1:0]gtxe2_i_3;
  output [1:0]gtxe2_i_4;
  output [1:0]gtxe2_i_5;
  output [1:0]gtxe2_i_6;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input [0:0]SR;
  input gt0_gttxreset_in0_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gt0_rxuserrdy_t;
  input gtxe2_i_7;
  input [0:0]TXPD;
  input gt0_txuserrdy_t;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_8;
  input [1:0]gtxe2_i_9;
  input [1:0]gtxe2_i_10;
  input gt0_cpllreset_t;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]SR;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire cpll_pd0_i;
  wire cpllreset_in;
  wire gt0_cpllrefclklost_i;
  wire gt0_cpllreset_t;
  wire gt0_gttxreset_in0_out;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gt0_rxuserrdy_t;
  wire gt0_txuserrdy_t;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtxe2_i;
  wire gtxe2_i_0;
  wire gtxe2_i_1;
  wire [1:0]gtxe2_i_10;
  wire [15:0]gtxe2_i_2;
  wire [1:0]gtxe2_i_3;
  wire [1:0]gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire gtxe2_i_7;
  wire [1:0]gtxe2_i_8;
  wire [1:0]gtxe2_i_9;
  wire independent_clock_bufg;
  wire reset;
  wire reset_out;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;

  gig_ethernet_pcs_pma_0_cpll_railing cpll_railing0_i
       (.cpll_pd0_i(cpll_pd0_i),
        .cpllreset_in(cpllreset_in),
        .gt0_cpllreset_t(gt0_cpllreset_t),
        .gtrefclk_bufg(gtrefclk_bufg));
  gig_ethernet_pcs_pma_0_GTWIZARD_GT gt0_GTWIZARD_i
       (.D(D),
        .Q(Q),
        .RXBUFSTATUS(RXBUFSTATUS),
        .RXPD(RXPD),
        .SR(SR),
        .TXBUFSTATUS(TXBUFSTATUS),
        .TXPD(TXPD),
        .cpll_pd0_i(cpll_pd0_i),
        .cpllreset_in(cpllreset_in),
        .gt0_cpllrefclklost_i(gt0_cpllrefclklost_i),
        .gt0_gttxreset_in0_out(gt0_gttxreset_in0_out),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gt0_rxuserrdy_t(gt0_rxuserrdy_t),
        .gt0_txuserrdy_t(gt0_txuserrdy_t),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i_0(gtxe2_i),
        .gtxe2_i_1(gtxe2_i_0),
        .gtxe2_i_10(gtxe2_i_9),
        .gtxe2_i_11(gtxe2_i_10),
        .gtxe2_i_2(gtxe2_i_1),
        .gtxe2_i_3(gtxe2_i_2),
        .gtxe2_i_4(gtxe2_i_3),
        .gtxe2_i_5(gtxe2_i_4),
        .gtxe2_i_6(gtxe2_i_5),
        .gtxe2_i_7(gtxe2_i_6),
        .gtxe2_i_8(gtxe2_i_7),
        .gtxe2_i_9(gtxe2_i_8),
        .independent_clock_bufg(independent_clock_bufg),
        .reset(reset),
        .reset_out(reset_out),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module gig_ethernet_pcs_pma_0_RX_STARTUP_FSM
   (data_in,
    gt0_rxuserrdy_t,
    SR,
    independent_clock_bufg,
    data_sync_reg6,
    out,
    gtxe2_i,
    \FSM_sequential_rx_state_reg[0]_0 ,
    data_sync_reg1,
    data_sync_reg1_0,
    data_out,
    data_sync_reg1_1);
  output data_in;
  output gt0_rxuserrdy_t;
  output [0:0]SR;
  input independent_clock_bufg;
  input data_sync_reg6;
  input [0:0]out;
  input gtxe2_i;
  input \FSM_sequential_rx_state_reg[0]_0 ;
  input data_sync_reg1;
  input data_sync_reg1_0;
  input data_out;
  input data_sync_reg1_1;

  wire \FSM_sequential_rx_state[0]_i_2_n_0 ;
  wire \FSM_sequential_rx_state[1]_i_3_n_0 ;
  wire \FSM_sequential_rx_state[2]_i_2_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_10_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_3_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_7_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_9_n_0 ;
  wire \FSM_sequential_rx_state_reg[0]_0 ;
  wire GTRXRESET;
  wire RXUSERRDY_i_1_n_0;
  wire [0:0]SR;
  wire check_tlock_max_i_1_n_0;
  wire check_tlock_max_reg_n_0;
  wire data_in;
  wire data_out;
  wire data_out_0;
  wire data_sync_reg1;
  wire data_sync_reg1_0;
  wire data_sync_reg1_1;
  wire data_sync_reg6;
  wire gt0_rxuserrdy_t;
  wire gtrxreset_i_i_1_n_0;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire \init_wait_count[0]_i_1__0_n_0 ;
  wire \init_wait_count[6]_i_1__0_n_0 ;
  wire \init_wait_count[6]_i_3__0_n_0 ;
  wire [6:0]init_wait_count_reg;
  wire init_wait_done_i_1__0_n_0;
  wire init_wait_done_reg_n_0;
  wire \mmcm_lock_count[2]_i_1__0_n_0 ;
  wire \mmcm_lock_count[3]_i_1__0_n_0 ;
  wire \mmcm_lock_count[4]_i_1__0_n_0 ;
  wire \mmcm_lock_count[5]_i_1__0_n_0 ;
  wire \mmcm_lock_count[6]_i_1__0_n_0 ;
  wire \mmcm_lock_count[7]_i_2__0_n_0 ;
  wire \mmcm_lock_count[7]_i_3__0_n_0 ;
  wire [7:0]mmcm_lock_count_reg;
  wire mmcm_lock_i;
  wire mmcm_lock_reclocked;
  wire mmcm_lock_reclocked_i_1_n_0;
  wire mmcm_lock_reclocked_i_2__0_n_0;
  wire [0:0]out;
  wire [6:1]p_0_in__2;
  wire [1:0]p_0_in__3;
  wire reset_time_out_i_3_n_0;
  wire reset_time_out_i_4_n_0;
  wire reset_time_out_reg_n_0;
  wire run_phase_alignment_int_i_1__0_n_0;
  wire run_phase_alignment_int_reg_n_0;
  wire run_phase_alignment_int_s3_reg_n_0;
  wire rx_fsm_reset_done_int_i_5_n_0;
  wire rx_fsm_reset_done_int_i_6_n_0;
  wire rx_fsm_reset_done_int_s2;
  wire rx_fsm_reset_done_int_s3;
  wire [3:0]rx_state;
  wire [3:0]rx_state__0;
  wire rxresetdone_s2;
  wire rxresetdone_s3;
  wire sync_cplllock_n_0;
  wire sync_data_valid_n_0;
  wire sync_data_valid_n_1;
  wire sync_data_valid_n_5;
  wire sync_mmcm_lock_reclocked_n_0;
  wire time_out_100us_i_1_n_0;
  wire time_out_100us_i_2_n_0;
  wire time_out_100us_i_3_n_0;
  wire time_out_100us_reg_n_0;
  wire time_out_1us_i_1_n_0;
  wire time_out_1us_i_2_n_0;
  wire time_out_1us_i_3_n_0;
  wire time_out_1us_reg_n_0;
  wire time_out_2ms_i_1_n_0;
  wire time_out_2ms_i_2_n_0;
  wire time_out_2ms_i_3__0_n_0;
  wire time_out_2ms_i_4_n_0;
  wire time_out_2ms_reg_n_0;
  wire time_out_counter;
  wire \time_out_counter[0]_i_3_n_0 ;
  wire [18:0]time_out_counter_reg;
  wire \time_out_counter_reg[0]_i_2__0_n_0 ;
  wire \time_out_counter_reg[0]_i_2__0_n_1 ;
  wire \time_out_counter_reg[0]_i_2__0_n_2 ;
  wire \time_out_counter_reg[0]_i_2__0_n_3 ;
  wire \time_out_counter_reg[0]_i_2__0_n_4 ;
  wire \time_out_counter_reg[0]_i_2__0_n_5 ;
  wire \time_out_counter_reg[0]_i_2__0_n_6 ;
  wire \time_out_counter_reg[0]_i_2__0_n_7 ;
  wire \time_out_counter_reg[12]_i_1__0_n_0 ;
  wire \time_out_counter_reg[12]_i_1__0_n_1 ;
  wire \time_out_counter_reg[12]_i_1__0_n_2 ;
  wire \time_out_counter_reg[12]_i_1__0_n_3 ;
  wire \time_out_counter_reg[12]_i_1__0_n_4 ;
  wire \time_out_counter_reg[12]_i_1__0_n_5 ;
  wire \time_out_counter_reg[12]_i_1__0_n_6 ;
  wire \time_out_counter_reg[12]_i_1__0_n_7 ;
  wire \time_out_counter_reg[16]_i_1__0_n_2 ;
  wire \time_out_counter_reg[16]_i_1__0_n_3 ;
  wire \time_out_counter_reg[16]_i_1__0_n_5 ;
  wire \time_out_counter_reg[16]_i_1__0_n_6 ;
  wire \time_out_counter_reg[16]_i_1__0_n_7 ;
  wire \time_out_counter_reg[4]_i_1__0_n_0 ;
  wire \time_out_counter_reg[4]_i_1__0_n_1 ;
  wire \time_out_counter_reg[4]_i_1__0_n_2 ;
  wire \time_out_counter_reg[4]_i_1__0_n_3 ;
  wire \time_out_counter_reg[4]_i_1__0_n_4 ;
  wire \time_out_counter_reg[4]_i_1__0_n_5 ;
  wire \time_out_counter_reg[4]_i_1__0_n_6 ;
  wire \time_out_counter_reg[4]_i_1__0_n_7 ;
  wire \time_out_counter_reg[8]_i_1__0_n_0 ;
  wire \time_out_counter_reg[8]_i_1__0_n_1 ;
  wire \time_out_counter_reg[8]_i_1__0_n_2 ;
  wire \time_out_counter_reg[8]_i_1__0_n_3 ;
  wire \time_out_counter_reg[8]_i_1__0_n_4 ;
  wire \time_out_counter_reg[8]_i_1__0_n_5 ;
  wire \time_out_counter_reg[8]_i_1__0_n_6 ;
  wire \time_out_counter_reg[8]_i_1__0_n_7 ;
  wire time_out_wait_bypass_i_1_n_0;
  wire time_out_wait_bypass_i_2__0_n_0;
  wire time_out_wait_bypass_i_3__0_n_0;
  wire time_out_wait_bypass_i_4__0_n_0;
  wire time_out_wait_bypass_reg_n_0;
  wire time_out_wait_bypass_s2;
  wire time_out_wait_bypass_s3;
  wire time_tlock_max;
  wire time_tlock_max1;
  wire time_tlock_max1_carry__0_i_1_n_0;
  wire time_tlock_max1_carry__0_i_2_n_0;
  wire time_tlock_max1_carry__0_i_3_n_0;
  wire time_tlock_max1_carry__0_i_4_n_0;
  wire time_tlock_max1_carry__0_i_5_n_0;
  wire time_tlock_max1_carry__0_i_6_n_0;
  wire time_tlock_max1_carry__0_n_0;
  wire time_tlock_max1_carry__0_n_1;
  wire time_tlock_max1_carry__0_n_2;
  wire time_tlock_max1_carry__0_n_3;
  wire time_tlock_max1_carry__1_i_1_n_0;
  wire time_tlock_max1_carry__1_i_2_n_0;
  wire time_tlock_max1_carry__1_i_3_n_0;
  wire time_tlock_max1_carry__1_n_3;
  wire time_tlock_max1_carry_i_1_n_0;
  wire time_tlock_max1_carry_i_2_n_0;
  wire time_tlock_max1_carry_i_3_n_0;
  wire time_tlock_max1_carry_i_4_n_0;
  wire time_tlock_max1_carry_i_5_n_0;
  wire time_tlock_max1_carry_i_6_n_0;
  wire time_tlock_max1_carry_i_7_n_0;
  wire time_tlock_max1_carry_i_8_n_0;
  wire time_tlock_max1_carry_n_0;
  wire time_tlock_max1_carry_n_1;
  wire time_tlock_max1_carry_n_2;
  wire time_tlock_max1_carry_n_3;
  wire time_tlock_max_i_1_n_0;
  wire \wait_bypass_count[0]_i_1__0_n_0 ;
  wire \wait_bypass_count[0]_i_2__0_n_0 ;
  wire \wait_bypass_count[0]_i_4_n_0 ;
  wire [12:0]wait_bypass_count_reg;
  wire \wait_bypass_count_reg[0]_i_3__0_n_0 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_1 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_2 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_3 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_4 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_5 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_6 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_7 ;
  wire \wait_bypass_count_reg[12]_i_1__0_n_7 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_0 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_1 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_2 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_3 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_4 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_5 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_6 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_7 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_0 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_1 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_2 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_3 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_4 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_5 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_6 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_7 ;
  wire [0:0]wait_time_cnt0__0;
  wire \wait_time_cnt[1]_i_1__0_n_0 ;
  wire \wait_time_cnt[2]_i_1__0_n_0 ;
  wire \wait_time_cnt[3]_i_1__0_n_0 ;
  wire \wait_time_cnt[4]_i_1__0_n_0 ;
  wire \wait_time_cnt[5]_i_1__0_n_0 ;
  wire \wait_time_cnt[6]_i_1_n_0 ;
  wire \wait_time_cnt[6]_i_2__0_n_0 ;
  wire \wait_time_cnt[6]_i_3__0_n_0 ;
  wire \wait_time_cnt[6]_i_4__0_n_0 ;
  wire [6:0]wait_time_cnt_reg;
  wire [3:2]\NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED ;
  wire [3:3]\NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED ;
  wire [3:0]NLW_time_tlock_max1_carry_O_UNCONNECTED;
  wire [3:0]NLW_time_tlock_max1_carry__0_O_UNCONNECTED;
  wire [3:2]NLW_time_tlock_max1_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_time_tlock_max1_carry__1_O_UNCONNECTED;
  wire [3:0]\NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED ;
  wire [3:1]\NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h2222AAAA00000C00)) 
    \FSM_sequential_rx_state[0]_i_2 
       (.I0(time_out_2ms_reg_n_0),
        .I1(rx_state[2]),
        .I2(rx_state[3]),
        .I3(time_tlock_max),
        .I4(reset_time_out_reg_n_0),
        .I5(rx_state[1]),
        .O(\FSM_sequential_rx_state[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000AABF000F0000)) 
    \FSM_sequential_rx_state[1]_i_3 
       (.I0(reset_time_out_reg_n_0),
        .I1(time_tlock_max),
        .I2(rx_state[2]),
        .I3(rx_state[3]),
        .I4(rx_state[1]),
        .I5(rx_state[0]),
        .O(\FSM_sequential_rx_state[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000050FF2200)) 
    \FSM_sequential_rx_state[2]_i_1 
       (.I0(rx_state[1]),
        .I1(time_out_2ms_reg_n_0),
        .I2(\FSM_sequential_rx_state[2]_i_2_n_0 ),
        .I3(rx_state[0]),
        .I4(rx_state[2]),
        .I5(rx_state[3]),
        .O(rx_state__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_rx_state[2]_i_2 
       (.I0(reset_time_out_reg_n_0),
        .I1(time_tlock_max),
        .O(\FSM_sequential_rx_state[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_rx_state[3]_i_10 
       (.I0(reset_time_out_reg_n_0),
        .I1(time_out_2ms_reg_n_0),
        .O(\FSM_sequential_rx_state[3]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0000000050005300)) 
    \FSM_sequential_rx_state[3]_i_3 
       (.I0(\FSM_sequential_rx_state[3]_i_10_n_0 ),
        .I1(\wait_time_cnt[6]_i_4__0_n_0 ),
        .I2(rx_state[0]),
        .I3(rx_state[1]),
        .I4(wait_time_cnt_reg[6]),
        .I5(rx_state[3]),
        .O(\FSM_sequential_rx_state[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000023002F00)) 
    \FSM_sequential_rx_state[3]_i_7 
       (.I0(time_out_2ms_reg_n_0),
        .I1(rx_state[2]),
        .I2(rx_state[1]),
        .I3(rx_state[0]),
        .I4(\FSM_sequential_rx_state[2]_i_2_n_0 ),
        .I5(rx_state[3]),
        .O(\FSM_sequential_rx_state[3]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT5 #(
    .INIT(32'h80800080)) 
    \FSM_sequential_rx_state[3]_i_9 
       (.I0(rx_state[0]),
        .I1(rx_state[1]),
        .I2(rx_state[2]),
        .I3(time_out_2ms_reg_n_0),
        .I4(reset_time_out_reg_n_0),
        .O(\FSM_sequential_rx_state[3]_i_9_n_0 ));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[0] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[0]),
        .Q(rx_state[0]),
        .R(out));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[1] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[1]),
        .Q(rx_state[1]),
        .R(out));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[2] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[2]),
        .Q(rx_state[2]),
        .R(out));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[3] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[3]),
        .Q(rx_state[3]),
        .R(out));
  LUT5 #(
    .INIT(32'hFFFB4000)) 
    RXUSERRDY_i_1
       (.I0(rx_state[3]),
        .I1(rx_state[0]),
        .I2(rx_state[2]),
        .I3(rx_state[1]),
        .I4(gt0_rxuserrdy_t),
        .O(RXUSERRDY_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    RXUSERRDY_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(RXUSERRDY_i_1_n_0),
        .Q(gt0_rxuserrdy_t),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT5 #(
    .INIT(32'hFFFB0008)) 
    check_tlock_max_i_1
       (.I0(rx_state[2]),
        .I1(rx_state[0]),
        .I2(rx_state[1]),
        .I3(rx_state[3]),
        .I4(check_tlock_max_reg_n_0),
        .O(check_tlock_max_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    check_tlock_max_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(check_tlock_max_i_1_n_0),
        .Q(check_tlock_max_reg_n_0),
        .R(out));
  LUT5 #(
    .INIT(32'hFFEF0100)) 
    gtrxreset_i_i_1
       (.I0(rx_state[3]),
        .I1(rx_state[1]),
        .I2(rx_state[2]),
        .I3(rx_state[0]),
        .I4(GTRXRESET),
        .O(gtrxreset_i_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gtrxreset_i_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gtrxreset_i_i_1_n_0),
        .Q(GTRXRESET),
        .R(out));
  LUT3 #(
    .INIT(8'hEA)) 
    gtxe2_i_i_2
       (.I0(GTRXRESET),
        .I1(data_in),
        .I2(gtxe2_i),
        .O(SR));
  LUT1 #(
    .INIT(2'h1)) 
    \init_wait_count[0]_i_1__0 
       (.I0(init_wait_count_reg[0]),
        .O(\init_wait_count[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \init_wait_count[1]_i_1__0 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .O(p_0_in__2[1]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \init_wait_count[2]_i_1__0 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .O(p_0_in__2[2]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \init_wait_count[3]_i_1__0 
       (.I0(init_wait_count_reg[1]),
        .I1(init_wait_count_reg[2]),
        .I2(init_wait_count_reg[0]),
        .I3(init_wait_count_reg[3]),
        .O(p_0_in__2[3]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \init_wait_count[4]_i_1__0 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .O(p_0_in__2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \init_wait_count[5]_i_1__0 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .I5(init_wait_count_reg[5]),
        .O(p_0_in__2[5]));
  LUT4 #(
    .INIT(16'hFEFF)) 
    \init_wait_count[6]_i_1__0 
       (.I0(\init_wait_count[6]_i_3__0_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(\init_wait_count[6]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT4 #(
    .INIT(16'hBF40)) 
    \init_wait_count[6]_i_2__0 
       (.I0(\init_wait_count[6]_i_3__0_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(p_0_in__2[6]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \init_wait_count[6]_i_3__0 
       (.I0(init_wait_count_reg[3]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .I3(init_wait_count_reg[5]),
        .O(\init_wait_count[6]_i_3__0_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(\init_wait_count[0]_i_1__0_n_0 ),
        .Q(init_wait_count_reg[0]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[1]),
        .Q(init_wait_count_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[2]),
        .Q(init_wait_count_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[3]),
        .Q(init_wait_count_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[4]),
        .Q(init_wait_count_reg[4]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[5]),
        .Q(init_wait_count_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[6]),
        .Q(init_wait_count_reg[6]));
  LUT5 #(
    .INIT(32'hFFFF0010)) 
    init_wait_done_i_1__0
       (.I0(\init_wait_count[6]_i_3__0_n_0 ),
        .I1(init_wait_count_reg[4]),
        .I2(init_wait_count_reg[6]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_done_reg_n_0),
        .O(init_wait_done_i_1__0_n_0));
  FDCE #(
    .INIT(1'b0)) 
    init_wait_done_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .CLR(out),
        .D(init_wait_done_i_1__0_n_0),
        .Q(init_wait_done_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[0]_i_1__0 
       (.I0(mmcm_lock_count_reg[0]),
        .O(p_0_in__3[0]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \mmcm_lock_count[1]_i_1__0 
       (.I0(mmcm_lock_count_reg[0]),
        .I1(mmcm_lock_count_reg[1]),
        .O(p_0_in__3[1]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \mmcm_lock_count[2]_i_1__0 
       (.I0(mmcm_lock_count_reg[1]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[2]),
        .O(\mmcm_lock_count[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \mmcm_lock_count[3]_i_1__0 
       (.I0(mmcm_lock_count_reg[2]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[1]),
        .I3(mmcm_lock_count_reg[3]),
        .O(\mmcm_lock_count[3]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \mmcm_lock_count[4]_i_1__0 
       (.I0(mmcm_lock_count_reg[3]),
        .I1(mmcm_lock_count_reg[1]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[2]),
        .I4(mmcm_lock_count_reg[4]),
        .O(\mmcm_lock_count[4]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \mmcm_lock_count[5]_i_1__0 
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(\mmcm_lock_count[5]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \mmcm_lock_count[6]_i_1__0 
       (.I0(mmcm_lock_reclocked_i_2__0_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .O(\mmcm_lock_count[6]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'hBF)) 
    \mmcm_lock_count[7]_i_2__0 
       (.I0(mmcm_lock_reclocked_i_2__0_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \mmcm_lock_count[7]_i_3__0 
       (.I0(mmcm_lock_count_reg[6]),
        .I1(mmcm_lock_reclocked_i_2__0_n_0),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_3__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(p_0_in__3[0]),
        .Q(mmcm_lock_count_reg[0]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(p_0_in__3[1]),
        .Q(mmcm_lock_count_reg[1]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[2]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[2]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[3]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[3]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[4]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[4]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[5]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[5]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[6]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[6]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[7] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[7]_i_3__0_n_0 ),
        .Q(mmcm_lock_count_reg[7]),
        .R(sync_mmcm_lock_reclocked_n_0));
  LUT5 #(
    .INIT(32'hAAEA0000)) 
    mmcm_lock_reclocked_i_1
       (.I0(mmcm_lock_reclocked),
        .I1(mmcm_lock_count_reg[7]),
        .I2(mmcm_lock_count_reg[6]),
        .I3(mmcm_lock_reclocked_i_2__0_n_0),
        .I4(mmcm_lock_i),
        .O(mmcm_lock_reclocked_i_1_n_0));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    mmcm_lock_reclocked_i_2__0
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(mmcm_lock_reclocked_i_2__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    mmcm_lock_reclocked_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(mmcm_lock_reclocked_i_1_n_0),
        .Q(mmcm_lock_reclocked),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT2 #(
    .INIT(4'hE)) 
    reset_time_out_i_3
       (.I0(rx_state[2]),
        .I1(rx_state[3]),
        .O(reset_time_out_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT5 #(
    .INIT(32'h34347674)) 
    reset_time_out_i_4
       (.I0(rx_state[2]),
        .I1(rx_state[3]),
        .I2(rx_state[0]),
        .I3(\FSM_sequential_rx_state_reg[0]_0 ),
        .I4(rx_state[1]),
        .O(reset_time_out_i_4_n_0));
  FDSE #(
    .INIT(1'b0)) 
    reset_time_out_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(sync_data_valid_n_0),
        .Q(reset_time_out_reg_n_0),
        .S(out));
  LUT5 #(
    .INIT(32'hFEFF0010)) 
    run_phase_alignment_int_i_1__0
       (.I0(rx_state[2]),
        .I1(rx_state[1]),
        .I2(rx_state[3]),
        .I3(rx_state[0]),
        .I4(run_phase_alignment_int_reg_n_0),
        .O(run_phase_alignment_int_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(run_phase_alignment_int_i_1__0_n_0),
        .Q(run_phase_alignment_int_reg_n_0),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_s3_reg
       (.C(data_sync_reg6),
        .CE(1'b1),
        .D(data_out_0),
        .Q(run_phase_alignment_int_s3_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT2 #(
    .INIT(4'hB)) 
    rx_fsm_reset_done_int_i_5
       (.I0(rx_state[1]),
        .I1(rx_state[0]),
        .O(rx_fsm_reset_done_int_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT2 #(
    .INIT(4'h2)) 
    rx_fsm_reset_done_int_i_6
       (.I0(rx_state[3]),
        .I1(rx_state[2]),
        .O(rx_fsm_reset_done_int_i_6_n_0));
  FDRE #(
    .INIT(1'b0)) 
    rx_fsm_reset_done_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(sync_data_valid_n_1),
        .Q(data_in),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    rx_fsm_reset_done_int_s3_reg
       (.C(data_sync_reg6),
        .CE(1'b1),
        .D(rx_fsm_reset_done_int_s2),
        .Q(rx_fsm_reset_done_int_s3),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxresetdone_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(rxresetdone_s2),
        .Q(rxresetdone_s3),
        .R(1'b0));
  gig_ethernet_pcs_pma_0_sync_block_10 sync_RXRESETDONE
       (.data_out(rxresetdone_s2),
        .data_sync_reg1_0(data_sync_reg1),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_11 sync_cplllock
       (.\FSM_sequential_rx_state_reg[1] (sync_cplllock_n_0),
        .Q(rx_state[3:1]),
        .data_sync_reg1_0(data_sync_reg1_1),
        .independent_clock_bufg(independent_clock_bufg),
        .rxresetdone_s3(rxresetdone_s3));
  gig_ethernet_pcs_pma_0_sync_block_12 sync_data_valid
       (.D({rx_state__0[3],rx_state__0[1:0]}),
        .E(sync_data_valid_n_5),
        .\FSM_sequential_rx_state_reg[0] (\FSM_sequential_rx_state[3]_i_3_n_0 ),
        .\FSM_sequential_rx_state_reg[0]_0 (\FSM_sequential_rx_state[3]_i_7_n_0 ),
        .\FSM_sequential_rx_state_reg[0]_1 (\FSM_sequential_rx_state_reg[0]_0 ),
        .\FSM_sequential_rx_state_reg[0]_2 (\FSM_sequential_rx_state[0]_i_2_n_0 ),
        .\FSM_sequential_rx_state_reg[0]_3 (init_wait_done_reg_n_0),
        .\FSM_sequential_rx_state_reg[1] (sync_data_valid_n_0),
        .\FSM_sequential_rx_state_reg[1]_0 (\FSM_sequential_rx_state[1]_i_3_n_0 ),
        .\FSM_sequential_rx_state_reg[3] (\FSM_sequential_rx_state[3]_i_9_n_0 ),
        .Q(rx_state),
        .data_in(data_in),
        .data_out(data_out),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_lock_reclocked(mmcm_lock_reclocked),
        .reset_time_out_reg(sync_cplllock_n_0),
        .reset_time_out_reg_0(reset_time_out_i_3_n_0),
        .reset_time_out_reg_1(reset_time_out_i_4_n_0),
        .reset_time_out_reg_2(reset_time_out_reg_n_0),
        .rx_fsm_reset_done_int_reg(sync_data_valid_n_1),
        .rx_fsm_reset_done_int_reg_0(rx_fsm_reset_done_int_i_5_n_0),
        .rx_fsm_reset_done_int_reg_1(time_out_100us_reg_n_0),
        .rx_fsm_reset_done_int_reg_2(time_out_1us_reg_n_0),
        .rx_fsm_reset_done_int_reg_3(rx_fsm_reset_done_int_i_6_n_0),
        .time_out_wait_bypass_s3(time_out_wait_bypass_s3));
  gig_ethernet_pcs_pma_0_sync_block_13 sync_mmcm_lock_reclocked
       (.SR(sync_mmcm_lock_reclocked_n_0),
        .data_out(mmcm_lock_i),
        .data_sync_reg1_0(data_sync_reg1_0),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_14 sync_run_phase_alignment_int
       (.data_in(run_phase_alignment_int_reg_n_0),
        .data_out(data_out_0),
        .data_sync_reg1_0(data_sync_reg6));
  gig_ethernet_pcs_pma_0_sync_block_15 sync_time_out_wait_bypass
       (.data_in(time_out_wait_bypass_reg_n_0),
        .data_out(time_out_wait_bypass_s2),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_16 sync_tx_fsm_reset_done_int
       (.data_in(data_in),
        .data_out(rx_fsm_reset_done_int_s2),
        .data_sync_reg6_0(data_sync_reg6));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000100)) 
    time_out_100us_i_1
       (.I0(time_out_2ms_i_4_n_0),
        .I1(time_out_counter_reg[17]),
        .I2(time_out_counter_reg[16]),
        .I3(time_out_100us_i_2_n_0),
        .I4(time_out_100us_i_3_n_0),
        .I5(time_out_100us_reg_n_0),
        .O(time_out_100us_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000002000000000)) 
    time_out_100us_i_2
       (.I0(time_out_counter_reg[10]),
        .I1(time_out_counter_reg[12]),
        .I2(time_out_counter_reg[5]),
        .I3(time_out_counter_reg[7]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[14]),
        .O(time_out_100us_i_2_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    time_out_100us_i_3
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[0]),
        .I2(time_out_counter_reg[1]),
        .I3(time_out_counter_reg[15]),
        .I4(time_out_counter_reg[13]),
        .O(time_out_100us_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_100us_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_100us_i_1_n_0),
        .Q(time_out_100us_reg_n_0),
        .R(reset_time_out_reg_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF00100000)) 
    time_out_1us_i_1
       (.I0(time_out_2ms_i_2_n_0),
        .I1(time_out_1us_i_2_n_0),
        .I2(time_out_counter_reg[3]),
        .I3(time_out_counter_reg[2]),
        .I4(time_out_1us_i_3_n_0),
        .I5(time_out_1us_reg_n_0),
        .O(time_out_1us_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'hE)) 
    time_out_1us_i_2
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[17]),
        .O(time_out_1us_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    time_out_1us_i_3
       (.I0(time_out_counter_reg[9]),
        .I1(time_out_counter_reg[11]),
        .I2(time_out_counter_reg[6]),
        .I3(time_out_counter_reg[8]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[12]),
        .O(time_out_1us_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_1us_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_1us_i_1_n_0),
        .Q(time_out_1us_reg_n_0),
        .R(reset_time_out_reg_n_0));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT4 #(
    .INIT(16'hFF01)) 
    time_out_2ms_i_1
       (.I0(time_out_2ms_i_2_n_0),
        .I1(time_out_2ms_i_3__0_n_0),
        .I2(time_out_2ms_i_4_n_0),
        .I3(time_out_2ms_reg_n_0),
        .O(time_out_2ms_i_1_n_0));
  LUT5 #(
    .INIT(32'hFFFFFEFF)) 
    time_out_2ms_i_2
       (.I0(time_out_counter_reg[10]),
        .I1(time_out_counter_reg[14]),
        .I2(time_out_counter_reg[5]),
        .I3(time_out_counter_reg[7]),
        .I4(time_out_100us_i_3_n_0),
        .O(time_out_2ms_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT4 #(
    .INIT(16'hDFFF)) 
    time_out_2ms_i_3__0
       (.I0(time_out_counter_reg[12]),
        .I1(time_out_counter_reg[16]),
        .I2(time_out_counter_reg[18]),
        .I3(time_out_counter_reg[17]),
        .O(time_out_2ms_i_3__0_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEFFFFFF)) 
    time_out_2ms_i_4
       (.I0(time_out_counter_reg[2]),
        .I1(time_out_counter_reg[3]),
        .I2(time_out_counter_reg[8]),
        .I3(time_out_counter_reg[9]),
        .I4(time_out_counter_reg[11]),
        .I5(time_out_counter_reg[6]),
        .O(time_out_2ms_i_4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_2ms_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_2ms_i_1_n_0),
        .Q(time_out_2ms_reg_n_0),
        .R(reset_time_out_reg_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFF7FF)) 
    \time_out_counter[0]_i_1 
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[18]),
        .I2(time_out_counter_reg[16]),
        .I3(time_out_counter_reg[12]),
        .I4(time_out_2ms_i_2_n_0),
        .I5(time_out_2ms_i_4_n_0),
        .O(time_out_counter));
  LUT1 #(
    .INIT(2'h1)) 
    \time_out_counter[0]_i_3 
       (.I0(time_out_counter_reg[0]),
        .O(\time_out_counter[0]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[0] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_7 ),
        .Q(time_out_counter_reg[0]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[0]_i_2__0 
       (.CI(1'b0),
        .CO({\time_out_counter_reg[0]_i_2__0_n_0 ,\time_out_counter_reg[0]_i_2__0_n_1 ,\time_out_counter_reg[0]_i_2__0_n_2 ,\time_out_counter_reg[0]_i_2__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\time_out_counter_reg[0]_i_2__0_n_4 ,\time_out_counter_reg[0]_i_2__0_n_5 ,\time_out_counter_reg[0]_i_2__0_n_6 ,\time_out_counter_reg[0]_i_2__0_n_7 }),
        .S({time_out_counter_reg[3:1],\time_out_counter[0]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[10] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[10]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[11] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[11]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[12] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[12]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[12]_i_1__0 
       (.CI(\time_out_counter_reg[8]_i_1__0_n_0 ),
        .CO({\time_out_counter_reg[12]_i_1__0_n_0 ,\time_out_counter_reg[12]_i_1__0_n_1 ,\time_out_counter_reg[12]_i_1__0_n_2 ,\time_out_counter_reg[12]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[12]_i_1__0_n_4 ,\time_out_counter_reg[12]_i_1__0_n_5 ,\time_out_counter_reg[12]_i_1__0_n_6 ,\time_out_counter_reg[12]_i_1__0_n_7 }),
        .S(time_out_counter_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[13] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[13]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[14] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[14]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[15] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[15]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[16] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[16]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[16]_i_1__0 
       (.CI(\time_out_counter_reg[12]_i_1__0_n_0 ),
        .CO({\NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED [3:2],\time_out_counter_reg[16]_i_1__0_n_2 ,\time_out_counter_reg[16]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED [3],\time_out_counter_reg[16]_i_1__0_n_5 ,\time_out_counter_reg[16]_i_1__0_n_6 ,\time_out_counter_reg[16]_i_1__0_n_7 }),
        .S({1'b0,time_out_counter_reg[18:16]}));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[17] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[17]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[18] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[18]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[1] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_6 ),
        .Q(time_out_counter_reg[1]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[2] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_5 ),
        .Q(time_out_counter_reg[2]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[3] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_4 ),
        .Q(time_out_counter_reg[3]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[4] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[4]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[4]_i_1__0 
       (.CI(\time_out_counter_reg[0]_i_2__0_n_0 ),
        .CO({\time_out_counter_reg[4]_i_1__0_n_0 ,\time_out_counter_reg[4]_i_1__0_n_1 ,\time_out_counter_reg[4]_i_1__0_n_2 ,\time_out_counter_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[4]_i_1__0_n_4 ,\time_out_counter_reg[4]_i_1__0_n_5 ,\time_out_counter_reg[4]_i_1__0_n_6 ,\time_out_counter_reg[4]_i_1__0_n_7 }),
        .S(time_out_counter_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[5] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[5]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[6] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[6]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[7] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[7]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[8] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[8]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[8]_i_1__0 
       (.CI(\time_out_counter_reg[4]_i_1__0_n_0 ),
        .CO({\time_out_counter_reg[8]_i_1__0_n_0 ,\time_out_counter_reg[8]_i_1__0_n_1 ,\time_out_counter_reg[8]_i_1__0_n_2 ,\time_out_counter_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[8]_i_1__0_n_4 ,\time_out_counter_reg[8]_i_1__0_n_5 ,\time_out_counter_reg[8]_i_1__0_n_6 ,\time_out_counter_reg[8]_i_1__0_n_7 }),
        .S(time_out_counter_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[9] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[9]),
        .R(reset_time_out_reg_n_0));
  LUT4 #(
    .INIT(16'hAB00)) 
    time_out_wait_bypass_i_1
       (.I0(time_out_wait_bypass_reg_n_0),
        .I1(rx_fsm_reset_done_int_s3),
        .I2(time_out_wait_bypass_i_2__0_n_0),
        .I3(run_phase_alignment_int_s3_reg_n_0),
        .O(time_out_wait_bypass_i_1_n_0));
  LUT5 #(
    .INIT(32'hFBFFFFFF)) 
    time_out_wait_bypass_i_2__0
       (.I0(time_out_wait_bypass_i_3__0_n_0),
        .I1(wait_bypass_count_reg[1]),
        .I2(wait_bypass_count_reg[11]),
        .I3(wait_bypass_count_reg[0]),
        .I4(time_out_wait_bypass_i_4__0_n_0),
        .O(time_out_wait_bypass_i_2__0_n_0));
  LUT4 #(
    .INIT(16'hDFFF)) 
    time_out_wait_bypass_i_3__0
       (.I0(wait_bypass_count_reg[9]),
        .I1(wait_bypass_count_reg[4]),
        .I2(wait_bypass_count_reg[12]),
        .I3(wait_bypass_count_reg[2]),
        .O(time_out_wait_bypass_i_3__0_n_0));
  LUT6 #(
    .INIT(64'h0000000400000000)) 
    time_out_wait_bypass_i_4__0
       (.I0(wait_bypass_count_reg[5]),
        .I1(wait_bypass_count_reg[7]),
        .I2(wait_bypass_count_reg[3]),
        .I3(wait_bypass_count_reg[6]),
        .I4(wait_bypass_count_reg[10]),
        .I5(wait_bypass_count_reg[8]),
        .O(time_out_wait_bypass_i_4__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_reg
       (.C(data_sync_reg6),
        .CE(1'b1),
        .D(time_out_wait_bypass_i_1_n_0),
        .Q(time_out_wait_bypass_reg_n_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_wait_bypass_s2),
        .Q(time_out_wait_bypass_s3),
        .R(1'b0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 time_tlock_max1_carry
       (.CI(1'b0),
        .CO({time_tlock_max1_carry_n_0,time_tlock_max1_carry_n_1,time_tlock_max1_carry_n_2,time_tlock_max1_carry_n_3}),
        .CYINIT(1'b0),
        .DI({time_tlock_max1_carry_i_1_n_0,time_tlock_max1_carry_i_2_n_0,time_tlock_max1_carry_i_3_n_0,time_tlock_max1_carry_i_4_n_0}),
        .O(NLW_time_tlock_max1_carry_O_UNCONNECTED[3:0]),
        .S({time_tlock_max1_carry_i_5_n_0,time_tlock_max1_carry_i_6_n_0,time_tlock_max1_carry_i_7_n_0,time_tlock_max1_carry_i_8_n_0}));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 time_tlock_max1_carry__0
       (.CI(time_tlock_max1_carry_n_0),
        .CO({time_tlock_max1_carry__0_n_0,time_tlock_max1_carry__0_n_1,time_tlock_max1_carry__0_n_2,time_tlock_max1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({time_out_counter_reg[15],time_tlock_max1_carry__0_i_1_n_0,1'b0,time_tlock_max1_carry__0_i_2_n_0}),
        .O(NLW_time_tlock_max1_carry__0_O_UNCONNECTED[3:0]),
        .S({time_tlock_max1_carry__0_i_3_n_0,time_tlock_max1_carry__0_i_4_n_0,time_tlock_max1_carry__0_i_5_n_0,time_tlock_max1_carry__0_i_6_n_0}));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry__0_i_1
       (.I0(time_out_counter_reg[12]),
        .I1(time_out_counter_reg[13]),
        .O(time_tlock_max1_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    time_tlock_max1_carry__0_i_2
       (.I0(time_out_counter_reg[8]),
        .I1(time_out_counter_reg[9]),
        .O(time_tlock_max1_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry__0_i_3
       (.I0(time_out_counter_reg[14]),
        .I1(time_out_counter_reg[15]),
        .O(time_tlock_max1_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry__0_i_4
       (.I0(time_out_counter_reg[13]),
        .I1(time_out_counter_reg[12]),
        .O(time_tlock_max1_carry__0_i_4_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    time_tlock_max1_carry__0_i_5
       (.I0(time_out_counter_reg[10]),
        .I1(time_out_counter_reg[11]),
        .O(time_tlock_max1_carry__0_i_5_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry__0_i_6
       (.I0(time_out_counter_reg[9]),
        .I1(time_out_counter_reg[8]),
        .O(time_tlock_max1_carry__0_i_6_n_0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 time_tlock_max1_carry__1
       (.CI(time_tlock_max1_carry__0_n_0),
        .CO({NLW_time_tlock_max1_carry__1_CO_UNCONNECTED[3:2],time_tlock_max1,time_tlock_max1_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,time_out_counter_reg[18],time_tlock_max1_carry__1_i_1_n_0}),
        .O(NLW_time_tlock_max1_carry__1_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,time_tlock_max1_carry__1_i_2_n_0,time_tlock_max1_carry__1_i_3_n_0}));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry__1_i_1
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[17]),
        .O(time_tlock_max1_carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    time_tlock_max1_carry__1_i_2
       (.I0(time_out_counter_reg[18]),
        .O(time_tlock_max1_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry__1_i_3
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[16]),
        .O(time_tlock_max1_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_1
       (.I0(time_out_counter_reg[6]),
        .I1(time_out_counter_reg[7]),
        .O(time_tlock_max1_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    time_tlock_max1_carry_i_2
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[5]),
        .O(time_tlock_max1_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_3
       (.I0(time_out_counter_reg[2]),
        .I1(time_out_counter_reg[3]),
        .O(time_tlock_max1_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_4
       (.I0(time_out_counter_reg[0]),
        .I1(time_out_counter_reg[1]),
        .O(time_tlock_max1_carry_i_4_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_5
       (.I0(time_out_counter_reg[7]),
        .I1(time_out_counter_reg[6]),
        .O(time_tlock_max1_carry_i_5_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry_i_6
       (.I0(time_out_counter_reg[5]),
        .I1(time_out_counter_reg[4]),
        .O(time_tlock_max1_carry_i_6_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_7
       (.I0(time_out_counter_reg[3]),
        .I1(time_out_counter_reg[2]),
        .O(time_tlock_max1_carry_i_7_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_8
       (.I0(time_out_counter_reg[1]),
        .I1(time_out_counter_reg[0]),
        .O(time_tlock_max1_carry_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    time_tlock_max_i_1
       (.I0(check_tlock_max_reg_n_0),
        .I1(time_tlock_max1),
        .I2(time_tlock_max),
        .O(time_tlock_max_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_tlock_max_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_tlock_max_i_1_n_0),
        .Q(time_tlock_max),
        .R(reset_time_out_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_1__0 
       (.I0(run_phase_alignment_int_s3_reg_n_0),
        .O(\wait_bypass_count[0]_i_1__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \wait_bypass_count[0]_i_2__0 
       (.I0(time_out_wait_bypass_i_2__0_n_0),
        .I1(rx_fsm_reset_done_int_s3),
        .O(\wait_bypass_count[0]_i_2__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_4 
       (.I0(wait_bypass_count_reg[0]),
        .O(\wait_bypass_count[0]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[0] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_7 ),
        .Q(wait_bypass_count_reg[0]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[0]_i_3__0 
       (.CI(1'b0),
        .CO({\wait_bypass_count_reg[0]_i_3__0_n_0 ,\wait_bypass_count_reg[0]_i_3__0_n_1 ,\wait_bypass_count_reg[0]_i_3__0_n_2 ,\wait_bypass_count_reg[0]_i_3__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\wait_bypass_count_reg[0]_i_3__0_n_4 ,\wait_bypass_count_reg[0]_i_3__0_n_5 ,\wait_bypass_count_reg[0]_i_3__0_n_6 ,\wait_bypass_count_reg[0]_i_3__0_n_7 }),
        .S({wait_bypass_count_reg[3:1],\wait_bypass_count[0]_i_4_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[10] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_5 ),
        .Q(wait_bypass_count_reg[10]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[11] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_4 ),
        .Q(wait_bypass_count_reg[11]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[12] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[12]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[12]_i_1__0 
       (.CI(\wait_bypass_count_reg[8]_i_1__0_n_0 ),
        .CO(\NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED [3:1],\wait_bypass_count_reg[12]_i_1__0_n_7 }),
        .S({1'b0,1'b0,1'b0,wait_bypass_count_reg[12]}));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[1] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_6 ),
        .Q(wait_bypass_count_reg[1]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[2] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_5 ),
        .Q(wait_bypass_count_reg[2]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[3] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_4 ),
        .Q(wait_bypass_count_reg[3]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[4] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[4]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[4]_i_1__0 
       (.CI(\wait_bypass_count_reg[0]_i_3__0_n_0 ),
        .CO({\wait_bypass_count_reg[4]_i_1__0_n_0 ,\wait_bypass_count_reg[4]_i_1__0_n_1 ,\wait_bypass_count_reg[4]_i_1__0_n_2 ,\wait_bypass_count_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[4]_i_1__0_n_4 ,\wait_bypass_count_reg[4]_i_1__0_n_5 ,\wait_bypass_count_reg[4]_i_1__0_n_6 ,\wait_bypass_count_reg[4]_i_1__0_n_7 }),
        .S(wait_bypass_count_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[5] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_6 ),
        .Q(wait_bypass_count_reg[5]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[6] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_5 ),
        .Q(wait_bypass_count_reg[6]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[7] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_4 ),
        .Q(wait_bypass_count_reg[7]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[8] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[8]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[8]_i_1__0 
       (.CI(\wait_bypass_count_reg[4]_i_1__0_n_0 ),
        .CO({\wait_bypass_count_reg[8]_i_1__0_n_0 ,\wait_bypass_count_reg[8]_i_1__0_n_1 ,\wait_bypass_count_reg[8]_i_1__0_n_2 ,\wait_bypass_count_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[8]_i_1__0_n_4 ,\wait_bypass_count_reg[8]_i_1__0_n_5 ,\wait_bypass_count_reg[8]_i_1__0_n_6 ,\wait_bypass_count_reg[8]_i_1__0_n_7 }),
        .S(wait_bypass_count_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[9] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_6 ),
        .Q(wait_bypass_count_reg[9]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \wait_time_cnt[0]_i_1__0 
       (.I0(wait_time_cnt_reg[0]),
        .O(wait_time_cnt0__0));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[1]_i_1__0 
       (.I0(wait_time_cnt_reg[0]),
        .I1(wait_time_cnt_reg[1]),
        .O(\wait_time_cnt[1]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'hE1)) 
    \wait_time_cnt[2]_i_1__0 
       (.I0(wait_time_cnt_reg[1]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[2]),
        .O(\wait_time_cnt[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT4 #(
    .INIT(16'hFE01)) 
    \wait_time_cnt[3]_i_1__0 
       (.I0(wait_time_cnt_reg[2]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[1]),
        .I3(wait_time_cnt_reg[3]),
        .O(\wait_time_cnt[3]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT5 #(
    .INIT(32'hFFFE0001)) 
    \wait_time_cnt[4]_i_1__0 
       (.I0(wait_time_cnt_reg[3]),
        .I1(wait_time_cnt_reg[1]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[2]),
        .I4(wait_time_cnt_reg[4]),
        .O(\wait_time_cnt[4]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000001)) 
    \wait_time_cnt[5]_i_1__0 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[5]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \wait_time_cnt[6]_i_1 
       (.I0(rx_state[0]),
        .I1(rx_state[1]),
        .I2(rx_state[3]),
        .O(\wait_time_cnt[6]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \wait_time_cnt[6]_i_2__0 
       (.I0(\wait_time_cnt[6]_i_4__0_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(\wait_time_cnt[6]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[6]_i_3__0 
       (.I0(\wait_time_cnt[6]_i_4__0_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(\wait_time_cnt[6]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \wait_time_cnt[6]_i_4__0 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[6]_i_4__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(wait_time_cnt0__0),
        .Q(wait_time_cnt_reg[0]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[1]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[1]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[2]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[2]),
        .S(\wait_time_cnt[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[3]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[3]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[4]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[4]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[5]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[5]),
        .S(\wait_time_cnt[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[6]_i_3__0_n_0 ),
        .Q(wait_time_cnt_reg[6]),
        .S(\wait_time_cnt[6]_i_1_n_0 ));
endmodule

module gig_ethernet_pcs_pma_0_TX_STARTUP_FSM
   (mmcm_reset,
    gt0_cpllreset_t,
    data_in,
    gt0_txuserrdy_t,
    gt0_gttxreset_in0_out,
    independent_clock_bufg,
    data_sync_reg1,
    out,
    gtxe2_i,
    gt0_cpllrefclklost_i,
    data_sync_reg1_0,
    data_sync_reg1_1,
    data_sync_reg1_2);
  output mmcm_reset;
  output gt0_cpllreset_t;
  output data_in;
  output gt0_txuserrdy_t;
  output gt0_gttxreset_in0_out;
  input independent_clock_bufg;
  input data_sync_reg1;
  input [0:0]out;
  input gtxe2_i;
  input gt0_cpllrefclklost_i;
  input data_sync_reg1_0;
  input data_sync_reg1_1;
  input data_sync_reg1_2;

  wire CPLL_RESET_i_1_n_0;
  wire CPLL_RESET_i_2_n_0;
  wire \FSM_sequential_tx_state[0]_i_2_n_0 ;
  wire \FSM_sequential_tx_state[0]_i_3_n_0 ;
  wire \FSM_sequential_tx_state[2]_i_2_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_3_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_4_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_6_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_7_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_8_n_0 ;
  wire GTTXRESET;
  wire MMCM_RESET_i_1_n_0;
  wire TXUSERRDY_i_1_n_0;
  wire clear;
  wire data_in;
  wire data_out;
  wire data_sync_reg1;
  wire data_sync_reg1_0;
  wire data_sync_reg1_1;
  wire data_sync_reg1_2;
  wire gt0_cpllrefclklost_i;
  wire gt0_cpllreset_t;
  wire gt0_gttxreset_in0_out;
  wire gt0_txuserrdy_t;
  wire gttxreset_i_i_1_n_0;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire \init_wait_count[0]_i_1_n_0 ;
  wire \init_wait_count[6]_i_1_n_0 ;
  wire \init_wait_count[6]_i_3_n_0 ;
  wire [6:0]init_wait_count_reg;
  wire init_wait_done_i_1_n_0;
  wire init_wait_done_reg_n_0;
  wire \mmcm_lock_count[2]_i_1_n_0 ;
  wire \mmcm_lock_count[3]_i_1_n_0 ;
  wire \mmcm_lock_count[4]_i_1_n_0 ;
  wire \mmcm_lock_count[5]_i_1_n_0 ;
  wire \mmcm_lock_count[6]_i_1_n_0 ;
  wire \mmcm_lock_count[7]_i_2_n_0 ;
  wire \mmcm_lock_count[7]_i_3_n_0 ;
  wire [7:0]mmcm_lock_count_reg;
  wire mmcm_lock_i;
  wire mmcm_lock_reclocked;
  wire mmcm_lock_reclocked_i_1_n_0;
  wire mmcm_lock_reclocked_i_2_n_0;
  wire mmcm_reset;
  wire [0:0]out;
  wire [6:1]p_0_in__0;
  wire [1:0]p_0_in__1;
  wire pll_reset_asserted_i_1_n_0;
  wire pll_reset_asserted_i_2_n_0;
  wire pll_reset_asserted_reg_n_0;
  wire refclk_stable_count;
  wire \refclk_stable_count[0]_i_3_n_0 ;
  wire \refclk_stable_count[0]_i_4_n_0 ;
  wire \refclk_stable_count[0]_i_5_n_0 ;
  wire \refclk_stable_count[0]_i_6_n_0 ;
  wire \refclk_stable_count[0]_i_7_n_0 ;
  wire \refclk_stable_count[0]_i_8_n_0 ;
  wire \refclk_stable_count[0]_i_9_n_0 ;
  wire [31:0]refclk_stable_count_reg;
  wire \refclk_stable_count_reg[0]_i_2_n_0 ;
  wire \refclk_stable_count_reg[0]_i_2_n_1 ;
  wire \refclk_stable_count_reg[0]_i_2_n_2 ;
  wire \refclk_stable_count_reg[0]_i_2_n_3 ;
  wire \refclk_stable_count_reg[0]_i_2_n_4 ;
  wire \refclk_stable_count_reg[0]_i_2_n_5 ;
  wire \refclk_stable_count_reg[0]_i_2_n_6 ;
  wire \refclk_stable_count_reg[0]_i_2_n_7 ;
  wire \refclk_stable_count_reg[12]_i_1_n_0 ;
  wire \refclk_stable_count_reg[12]_i_1_n_1 ;
  wire \refclk_stable_count_reg[12]_i_1_n_2 ;
  wire \refclk_stable_count_reg[12]_i_1_n_3 ;
  wire \refclk_stable_count_reg[12]_i_1_n_4 ;
  wire \refclk_stable_count_reg[12]_i_1_n_5 ;
  wire \refclk_stable_count_reg[12]_i_1_n_6 ;
  wire \refclk_stable_count_reg[12]_i_1_n_7 ;
  wire \refclk_stable_count_reg[16]_i_1_n_0 ;
  wire \refclk_stable_count_reg[16]_i_1_n_1 ;
  wire \refclk_stable_count_reg[16]_i_1_n_2 ;
  wire \refclk_stable_count_reg[16]_i_1_n_3 ;
  wire \refclk_stable_count_reg[16]_i_1_n_4 ;
  wire \refclk_stable_count_reg[16]_i_1_n_5 ;
  wire \refclk_stable_count_reg[16]_i_1_n_6 ;
  wire \refclk_stable_count_reg[16]_i_1_n_7 ;
  wire \refclk_stable_count_reg[20]_i_1_n_0 ;
  wire \refclk_stable_count_reg[20]_i_1_n_1 ;
  wire \refclk_stable_count_reg[20]_i_1_n_2 ;
  wire \refclk_stable_count_reg[20]_i_1_n_3 ;
  wire \refclk_stable_count_reg[20]_i_1_n_4 ;
  wire \refclk_stable_count_reg[20]_i_1_n_5 ;
  wire \refclk_stable_count_reg[20]_i_1_n_6 ;
  wire \refclk_stable_count_reg[20]_i_1_n_7 ;
  wire \refclk_stable_count_reg[24]_i_1_n_0 ;
  wire \refclk_stable_count_reg[24]_i_1_n_1 ;
  wire \refclk_stable_count_reg[24]_i_1_n_2 ;
  wire \refclk_stable_count_reg[24]_i_1_n_3 ;
  wire \refclk_stable_count_reg[24]_i_1_n_4 ;
  wire \refclk_stable_count_reg[24]_i_1_n_5 ;
  wire \refclk_stable_count_reg[24]_i_1_n_6 ;
  wire \refclk_stable_count_reg[24]_i_1_n_7 ;
  wire \refclk_stable_count_reg[28]_i_1_n_1 ;
  wire \refclk_stable_count_reg[28]_i_1_n_2 ;
  wire \refclk_stable_count_reg[28]_i_1_n_3 ;
  wire \refclk_stable_count_reg[28]_i_1_n_4 ;
  wire \refclk_stable_count_reg[28]_i_1_n_5 ;
  wire \refclk_stable_count_reg[28]_i_1_n_6 ;
  wire \refclk_stable_count_reg[28]_i_1_n_7 ;
  wire \refclk_stable_count_reg[4]_i_1_n_0 ;
  wire \refclk_stable_count_reg[4]_i_1_n_1 ;
  wire \refclk_stable_count_reg[4]_i_1_n_2 ;
  wire \refclk_stable_count_reg[4]_i_1_n_3 ;
  wire \refclk_stable_count_reg[4]_i_1_n_4 ;
  wire \refclk_stable_count_reg[4]_i_1_n_5 ;
  wire \refclk_stable_count_reg[4]_i_1_n_6 ;
  wire \refclk_stable_count_reg[4]_i_1_n_7 ;
  wire \refclk_stable_count_reg[8]_i_1_n_0 ;
  wire \refclk_stable_count_reg[8]_i_1_n_1 ;
  wire \refclk_stable_count_reg[8]_i_1_n_2 ;
  wire \refclk_stable_count_reg[8]_i_1_n_3 ;
  wire \refclk_stable_count_reg[8]_i_1_n_4 ;
  wire \refclk_stable_count_reg[8]_i_1_n_5 ;
  wire \refclk_stable_count_reg[8]_i_1_n_6 ;
  wire \refclk_stable_count_reg[8]_i_1_n_7 ;
  wire refclk_stable_i_1_n_0;
  wire refclk_stable_i_2_n_0;
  wire refclk_stable_i_3_n_0;
  wire refclk_stable_i_4_n_0;
  wire refclk_stable_i_5_n_0;
  wire refclk_stable_i_6_n_0;
  wire refclk_stable_reg_n_0;
  wire reset_time_out;
  wire reset_time_out_i_2__0_n_0;
  wire run_phase_alignment_int_i_1_n_0;
  wire run_phase_alignment_int_reg_n_0;
  wire run_phase_alignment_int_s3;
  wire sel;
  wire sync_cplllock_n_0;
  wire sync_cplllock_n_1;
  wire sync_mmcm_lock_reclocked_n_0;
  wire time_out_2ms_i_1_n_0;
  wire time_out_2ms_i_2__0_n_0;
  wire time_out_2ms_i_3_n_0;
  wire time_out_2ms_i_4__0_n_0;
  wire time_out_2ms_i_5_n_0;
  wire time_out_2ms_reg_n_0;
  wire time_out_500us_i_1_n_0;
  wire time_out_500us_i_2_n_0;
  wire time_out_500us_reg_n_0;
  wire time_out_counter;
  wire \time_out_counter[0]_i_3__0_n_0 ;
  wire \time_out_counter[0]_i_4_n_0 ;
  wire [18:0]time_out_counter_reg;
  wire \time_out_counter_reg[0]_i_2_n_0 ;
  wire \time_out_counter_reg[0]_i_2_n_1 ;
  wire \time_out_counter_reg[0]_i_2_n_2 ;
  wire \time_out_counter_reg[0]_i_2_n_3 ;
  wire \time_out_counter_reg[0]_i_2_n_4 ;
  wire \time_out_counter_reg[0]_i_2_n_5 ;
  wire \time_out_counter_reg[0]_i_2_n_6 ;
  wire \time_out_counter_reg[0]_i_2_n_7 ;
  wire \time_out_counter_reg[12]_i_1_n_0 ;
  wire \time_out_counter_reg[12]_i_1_n_1 ;
  wire \time_out_counter_reg[12]_i_1_n_2 ;
  wire \time_out_counter_reg[12]_i_1_n_3 ;
  wire \time_out_counter_reg[12]_i_1_n_4 ;
  wire \time_out_counter_reg[12]_i_1_n_5 ;
  wire \time_out_counter_reg[12]_i_1_n_6 ;
  wire \time_out_counter_reg[12]_i_1_n_7 ;
  wire \time_out_counter_reg[16]_i_1_n_2 ;
  wire \time_out_counter_reg[16]_i_1_n_3 ;
  wire \time_out_counter_reg[16]_i_1_n_5 ;
  wire \time_out_counter_reg[16]_i_1_n_6 ;
  wire \time_out_counter_reg[16]_i_1_n_7 ;
  wire \time_out_counter_reg[4]_i_1_n_0 ;
  wire \time_out_counter_reg[4]_i_1_n_1 ;
  wire \time_out_counter_reg[4]_i_1_n_2 ;
  wire \time_out_counter_reg[4]_i_1_n_3 ;
  wire \time_out_counter_reg[4]_i_1_n_4 ;
  wire \time_out_counter_reg[4]_i_1_n_5 ;
  wire \time_out_counter_reg[4]_i_1_n_6 ;
  wire \time_out_counter_reg[4]_i_1_n_7 ;
  wire \time_out_counter_reg[8]_i_1_n_0 ;
  wire \time_out_counter_reg[8]_i_1_n_1 ;
  wire \time_out_counter_reg[8]_i_1_n_2 ;
  wire \time_out_counter_reg[8]_i_1_n_3 ;
  wire \time_out_counter_reg[8]_i_1_n_4 ;
  wire \time_out_counter_reg[8]_i_1_n_5 ;
  wire \time_out_counter_reg[8]_i_1_n_6 ;
  wire \time_out_counter_reg[8]_i_1_n_7 ;
  wire time_out_wait_bypass_i_1_n_0;
  wire time_out_wait_bypass_i_2_n_0;
  wire time_out_wait_bypass_i_3_n_0;
  wire time_out_wait_bypass_i_4_n_0;
  wire time_out_wait_bypass_i_5_n_0;
  wire time_out_wait_bypass_reg_n_0;
  wire time_out_wait_bypass_s2;
  wire time_out_wait_bypass_s3;
  wire time_tlock_max_i_1_n_0;
  wire time_tlock_max_i_2_n_0;
  wire time_tlock_max_i_3_n_0;
  wire time_tlock_max_i_4_n_0;
  wire time_tlock_max_reg_n_0;
  wire tx_fsm_reset_done_int_i_1_n_0;
  wire tx_fsm_reset_done_int_s2;
  wire tx_fsm_reset_done_int_s3;
  wire [3:0]tx_state;
  wire [3:0]tx_state__0;
  wire txresetdone_s2;
  wire txresetdone_s3;
  wire \wait_bypass_count[0]_i_2_n_0 ;
  wire \wait_bypass_count[0]_i_4__0_n_0 ;
  wire [16:0]wait_bypass_count_reg;
  wire \wait_bypass_count_reg[0]_i_3_n_0 ;
  wire \wait_bypass_count_reg[0]_i_3_n_1 ;
  wire \wait_bypass_count_reg[0]_i_3_n_2 ;
  wire \wait_bypass_count_reg[0]_i_3_n_3 ;
  wire \wait_bypass_count_reg[0]_i_3_n_4 ;
  wire \wait_bypass_count_reg[0]_i_3_n_5 ;
  wire \wait_bypass_count_reg[0]_i_3_n_6 ;
  wire \wait_bypass_count_reg[0]_i_3_n_7 ;
  wire \wait_bypass_count_reg[12]_i_1_n_0 ;
  wire \wait_bypass_count_reg[12]_i_1_n_1 ;
  wire \wait_bypass_count_reg[12]_i_1_n_2 ;
  wire \wait_bypass_count_reg[12]_i_1_n_3 ;
  wire \wait_bypass_count_reg[12]_i_1_n_4 ;
  wire \wait_bypass_count_reg[12]_i_1_n_5 ;
  wire \wait_bypass_count_reg[12]_i_1_n_6 ;
  wire \wait_bypass_count_reg[12]_i_1_n_7 ;
  wire \wait_bypass_count_reg[16]_i_1_n_7 ;
  wire \wait_bypass_count_reg[4]_i_1_n_0 ;
  wire \wait_bypass_count_reg[4]_i_1_n_1 ;
  wire \wait_bypass_count_reg[4]_i_1_n_2 ;
  wire \wait_bypass_count_reg[4]_i_1_n_3 ;
  wire \wait_bypass_count_reg[4]_i_1_n_4 ;
  wire \wait_bypass_count_reg[4]_i_1_n_5 ;
  wire \wait_bypass_count_reg[4]_i_1_n_6 ;
  wire \wait_bypass_count_reg[4]_i_1_n_7 ;
  wire \wait_bypass_count_reg[8]_i_1_n_0 ;
  wire \wait_bypass_count_reg[8]_i_1_n_1 ;
  wire \wait_bypass_count_reg[8]_i_1_n_2 ;
  wire \wait_bypass_count_reg[8]_i_1_n_3 ;
  wire \wait_bypass_count_reg[8]_i_1_n_4 ;
  wire \wait_bypass_count_reg[8]_i_1_n_5 ;
  wire \wait_bypass_count_reg[8]_i_1_n_6 ;
  wire \wait_bypass_count_reg[8]_i_1_n_7 ;
  wire [0:0]wait_time_cnt0;
  wire wait_time_cnt0_0;
  wire \wait_time_cnt[1]_i_1_n_0 ;
  wire \wait_time_cnt[2]_i_1_n_0 ;
  wire \wait_time_cnt[3]_i_1_n_0 ;
  wire \wait_time_cnt[4]_i_1_n_0 ;
  wire \wait_time_cnt[5]_i_1_n_0 ;
  wire \wait_time_cnt[6]_i_3_n_0 ;
  wire \wait_time_cnt[6]_i_4_n_0 ;
  wire [6:0]wait_time_cnt_reg;
  wire [3:3]\NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED ;
  wire [3:2]\NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED ;
  wire [3:0]\NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:1]\NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'hFFFFFF1F0000001F)) 
    CPLL_RESET_i_1
       (.I0(pll_reset_asserted_reg_n_0),
        .I1(gt0_cpllrefclklost_i),
        .I2(refclk_stable_reg_n_0),
        .I3(CPLL_RESET_i_2_n_0),
        .I4(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I5(gt0_cpllreset_t),
        .O(CPLL_RESET_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT2 #(
    .INIT(4'hE)) 
    CPLL_RESET_i_2
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .O(CPLL_RESET_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    CPLL_RESET_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(CPLL_RESET_i_1_n_0),
        .Q(gt0_cpllreset_t),
        .R(out));
  LUT6 #(
    .INIT(64'hF3FFF3F0F5F0F5F0)) 
    \FSM_sequential_tx_state[0]_i_1 
       (.I0(\FSM_sequential_tx_state[2]_i_2_n_0 ),
        .I1(\FSM_sequential_tx_state[0]_i_2_n_0 ),
        .I2(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I3(tx_state[2]),
        .I4(time_out_2ms_reg_n_0),
        .I5(tx_state[1]),
        .O(tx_state__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_tx_state[0]_i_2 
       (.I0(reset_time_out),
        .I1(time_out_500us_reg_n_0),
        .O(\FSM_sequential_tx_state[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_tx_state[0]_i_3 
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .O(\FSM_sequential_tx_state[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT5 #(
    .INIT(32'h005A001A)) 
    \FSM_sequential_tx_state[1]_i_1 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[0]),
        .I3(tx_state[3]),
        .I4(\FSM_sequential_tx_state[2]_i_2_n_0 ),
        .O(tx_state__0[1]));
  LUT6 #(
    .INIT(64'h04000C0C06020C0C)) 
    \FSM_sequential_tx_state[2]_i_1 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[3]),
        .I3(\FSM_sequential_tx_state[2]_i_2_n_0 ),
        .I4(tx_state[0]),
        .I5(time_out_2ms_reg_n_0),
        .O(tx_state__0[2]));
  LUT3 #(
    .INIT(8'hFD)) 
    \FSM_sequential_tx_state[2]_i_2 
       (.I0(time_tlock_max_reg_n_0),
        .I1(reset_time_out),
        .I2(mmcm_lock_reclocked),
        .O(\FSM_sequential_tx_state[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT5 #(
    .INIT(32'hF4FF4444)) 
    \FSM_sequential_tx_state[3]_i_2 
       (.I0(time_out_wait_bypass_s3),
        .I1(tx_state[3]),
        .I2(reset_time_out),
        .I3(time_out_500us_reg_n_0),
        .I4(\FSM_sequential_tx_state[3]_i_8_n_0 ),
        .O(tx_state__0[3]));
  LUT6 #(
    .INIT(64'h00BA000000000000)) 
    \FSM_sequential_tx_state[3]_i_3 
       (.I0(txresetdone_s3),
        .I1(reset_time_out),
        .I2(time_out_500us_reg_n_0),
        .I3(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I4(tx_state[2]),
        .I5(tx_state[1]),
        .O(\FSM_sequential_tx_state[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000300FF00AA)) 
    \FSM_sequential_tx_state[3]_i_4 
       (.I0(init_wait_done_reg_n_0),
        .I1(\wait_time_cnt[6]_i_4_n_0 ),
        .I2(wait_time_cnt_reg[6]),
        .I3(tx_state[0]),
        .I4(tx_state[3]),
        .I5(CPLL_RESET_i_2_n_0),
        .O(\FSM_sequential_tx_state[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0404040400040000)) 
    \FSM_sequential_tx_state[3]_i_6 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I3(reset_time_out),
        .I4(time_tlock_max_reg_n_0),
        .I5(mmcm_lock_reclocked),
        .O(\FSM_sequential_tx_state[3]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT4 #(
    .INIT(16'h1000)) 
    \FSM_sequential_tx_state[3]_i_7 
       (.I0(tx_state[2]),
        .I1(tx_state[3]),
        .I2(tx_state[0]),
        .I3(tx_state[1]),
        .O(\FSM_sequential_tx_state[3]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    \FSM_sequential_tx_state[3]_i_8 
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .O(\FSM_sequential_tx_state[3]_i_8_n_0 ));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[0] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[0]),
        .Q(tx_state[0]),
        .R(out));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[1] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[1]),
        .Q(tx_state[1]),
        .R(out));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[2] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[2]),
        .Q(tx_state[2]),
        .R(out));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[3] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[3]),
        .Q(tx_state[3]),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT5 #(
    .INIT(32'hFFF70004)) 
    MMCM_RESET_i_1
       (.I0(tx_state[2]),
        .I1(tx_state[0]),
        .I2(tx_state[3]),
        .I3(tx_state[1]),
        .I4(mmcm_reset),
        .O(MMCM_RESET_i_1_n_0));
  FDRE #(
    .INIT(1'b1)) 
    MMCM_RESET_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(MMCM_RESET_i_1_n_0),
        .Q(mmcm_reset),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT5 #(
    .INIT(32'hFFFD2000)) 
    TXUSERRDY_i_1
       (.I0(tx_state[0]),
        .I1(tx_state[3]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .I4(gt0_txuserrdy_t),
        .O(TXUSERRDY_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    TXUSERRDY_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(TXUSERRDY_i_1_n_0),
        .Q(gt0_txuserrdy_t),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT5 #(
    .INIT(32'hFFEF0100)) 
    gttxreset_i_i_1
       (.I0(tx_state[3]),
        .I1(tx_state[1]),
        .I2(tx_state[2]),
        .I3(tx_state[0]),
        .I4(GTTXRESET),
        .O(gttxreset_i_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gttxreset_i_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gttxreset_i_i_1_n_0),
        .Q(GTTXRESET),
        .R(out));
  LUT3 #(
    .INIT(8'hEA)) 
    gtxe2_i_i_3
       (.I0(GTTXRESET),
        .I1(data_in),
        .I2(gtxe2_i),
        .O(gt0_gttxreset_in0_out));
  LUT1 #(
    .INIT(2'h1)) 
    \init_wait_count[0]_i_1 
       (.I0(init_wait_count_reg[0]),
        .O(\init_wait_count[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \init_wait_count[1]_i_1 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \init_wait_count[2]_i_1 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .O(p_0_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \init_wait_count[3]_i_1 
       (.I0(init_wait_count_reg[1]),
        .I1(init_wait_count_reg[2]),
        .I2(init_wait_count_reg[0]),
        .I3(init_wait_count_reg[3]),
        .O(p_0_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \init_wait_count[4]_i_1 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .O(p_0_in__0[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \init_wait_count[5]_i_1 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .I5(init_wait_count_reg[5]),
        .O(p_0_in__0[5]));
  LUT4 #(
    .INIT(16'hFEFF)) 
    \init_wait_count[6]_i_1 
       (.I0(\init_wait_count[6]_i_3_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(\init_wait_count[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT4 #(
    .INIT(16'hBF40)) 
    \init_wait_count[6]_i_2 
       (.I0(\init_wait_count[6]_i_3_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(p_0_in__0[6]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \init_wait_count[6]_i_3 
       (.I0(init_wait_count_reg[3]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .I3(init_wait_count_reg[5]),
        .O(\init_wait_count[6]_i_3_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(\init_wait_count[0]_i_1_n_0 ),
        .Q(init_wait_count_reg[0]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[1]),
        .Q(init_wait_count_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[2]),
        .Q(init_wait_count_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[3]),
        .Q(init_wait_count_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[4]),
        .Q(init_wait_count_reg[4]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[5]),
        .Q(init_wait_count_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[6]),
        .Q(init_wait_count_reg[6]));
  LUT5 #(
    .INIT(32'hFFFF0010)) 
    init_wait_done_i_1
       (.I0(\init_wait_count[6]_i_3_n_0 ),
        .I1(init_wait_count_reg[4]),
        .I2(init_wait_count_reg[6]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_done_reg_n_0),
        .O(init_wait_done_i_1_n_0));
  FDCE #(
    .INIT(1'b0)) 
    init_wait_done_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .CLR(out),
        .D(init_wait_done_i_1_n_0),
        .Q(init_wait_done_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[0]_i_1 
       (.I0(mmcm_lock_count_reg[0]),
        .O(p_0_in__1[0]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \mmcm_lock_count[1]_i_1 
       (.I0(mmcm_lock_count_reg[0]),
        .I1(mmcm_lock_count_reg[1]),
        .O(p_0_in__1[1]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \mmcm_lock_count[2]_i_1 
       (.I0(mmcm_lock_count_reg[1]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[2]),
        .O(\mmcm_lock_count[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \mmcm_lock_count[3]_i_1 
       (.I0(mmcm_lock_count_reg[2]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[1]),
        .I3(mmcm_lock_count_reg[3]),
        .O(\mmcm_lock_count[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \mmcm_lock_count[4]_i_1 
       (.I0(mmcm_lock_count_reg[3]),
        .I1(mmcm_lock_count_reg[1]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[2]),
        .I4(mmcm_lock_count_reg[4]),
        .O(\mmcm_lock_count[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \mmcm_lock_count[5]_i_1 
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(\mmcm_lock_count[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \mmcm_lock_count[6]_i_1 
       (.I0(mmcm_lock_reclocked_i_2_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .O(\mmcm_lock_count[6]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hBF)) 
    \mmcm_lock_count[7]_i_2 
       (.I0(mmcm_lock_reclocked_i_2_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \mmcm_lock_count[7]_i_3 
       (.I0(mmcm_lock_count_reg[6]),
        .I1(mmcm_lock_reclocked_i_2_n_0),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(p_0_in__1[0]),
        .Q(mmcm_lock_count_reg[0]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(p_0_in__1[1]),
        .Q(mmcm_lock_count_reg[1]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[2]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[2]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[3]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[3]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[4]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[4]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[5]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[5]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[6]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[6]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[7] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[7]_i_3_n_0 ),
        .Q(mmcm_lock_count_reg[7]),
        .R(sync_mmcm_lock_reclocked_n_0));
  LUT5 #(
    .INIT(32'hAAEA0000)) 
    mmcm_lock_reclocked_i_1
       (.I0(mmcm_lock_reclocked),
        .I1(mmcm_lock_count_reg[7]),
        .I2(mmcm_lock_count_reg[6]),
        .I3(mmcm_lock_reclocked_i_2_n_0),
        .I4(mmcm_lock_i),
        .O(mmcm_lock_reclocked_i_1_n_0));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    mmcm_lock_reclocked_i_2
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(mmcm_lock_reclocked_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    mmcm_lock_reclocked_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(mmcm_lock_reclocked_i_1_n_0),
        .Q(mmcm_lock_reclocked),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000CD55CCCCCCCC)) 
    pll_reset_asserted_i_1
       (.I0(tx_state[3]),
        .I1(pll_reset_asserted_reg_n_0),
        .I2(gt0_cpllrefclklost_i),
        .I3(refclk_stable_reg_n_0),
        .I4(tx_state[1]),
        .I5(pll_reset_asserted_i_2_n_0),
        .O(pll_reset_asserted_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'h02)) 
    pll_reset_asserted_i_2
       (.I0(tx_state[0]),
        .I1(tx_state[3]),
        .I2(tx_state[2]),
        .O(pll_reset_asserted_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    pll_reset_asserted_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pll_reset_asserted_i_1_n_0),
        .Q(pll_reset_asserted_reg_n_0),
        .R(out));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFFFF)) 
    \refclk_stable_count[0]_i_1 
       (.I0(\refclk_stable_count[0]_i_3_n_0 ),
        .I1(\refclk_stable_count[0]_i_4_n_0 ),
        .I2(\refclk_stable_count[0]_i_5_n_0 ),
        .I3(\refclk_stable_count[0]_i_6_n_0 ),
        .I4(\refclk_stable_count[0]_i_7_n_0 ),
        .I5(\refclk_stable_count[0]_i_8_n_0 ),
        .O(refclk_stable_count));
  LUT6 #(
    .INIT(64'hFFDFFFFFFFFFFFFF)) 
    \refclk_stable_count[0]_i_3 
       (.I0(refclk_stable_count_reg[13]),
        .I1(refclk_stable_count_reg[12]),
        .I2(refclk_stable_count_reg[10]),
        .I3(refclk_stable_count_reg[11]),
        .I4(refclk_stable_count_reg[9]),
        .I5(refclk_stable_count_reg[8]),
        .O(\refclk_stable_count[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFDF)) 
    \refclk_stable_count[0]_i_4 
       (.I0(refclk_stable_count_reg[19]),
        .I1(refclk_stable_count_reg[18]),
        .I2(refclk_stable_count_reg[16]),
        .I3(refclk_stable_count_reg[17]),
        .I4(refclk_stable_count_reg[15]),
        .I5(refclk_stable_count_reg[14]),
        .O(\refclk_stable_count[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \refclk_stable_count[0]_i_5 
       (.I0(refclk_stable_count_reg[30]),
        .I1(refclk_stable_count_reg[31]),
        .I2(refclk_stable_count_reg[28]),
        .I3(refclk_stable_count_reg[29]),
        .I4(refclk_stable_count_reg[27]),
        .I5(refclk_stable_count_reg[26]),
        .O(\refclk_stable_count[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \refclk_stable_count[0]_i_6 
       (.I0(refclk_stable_count_reg[24]),
        .I1(refclk_stable_count_reg[25]),
        .I2(refclk_stable_count_reg[22]),
        .I3(refclk_stable_count_reg[23]),
        .I4(refclk_stable_count_reg[21]),
        .I5(refclk_stable_count_reg[20]),
        .O(\refclk_stable_count[0]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \refclk_stable_count[0]_i_7 
       (.I0(refclk_stable_count_reg[0]),
        .I1(refclk_stable_count_reg[1]),
        .O(\refclk_stable_count[0]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF7)) 
    \refclk_stable_count[0]_i_8 
       (.I0(refclk_stable_count_reg[6]),
        .I1(refclk_stable_count_reg[7]),
        .I2(refclk_stable_count_reg[4]),
        .I3(refclk_stable_count_reg[5]),
        .I4(refclk_stable_count_reg[3]),
        .I5(refclk_stable_count_reg[2]),
        .O(\refclk_stable_count[0]_i_8_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \refclk_stable_count[0]_i_9 
       (.I0(refclk_stable_count_reg[0]),
        .O(\refclk_stable_count[0]_i_9_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_7 ),
        .Q(refclk_stable_count_reg[0]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\refclk_stable_count_reg[0]_i_2_n_0 ,\refclk_stable_count_reg[0]_i_2_n_1 ,\refclk_stable_count_reg[0]_i_2_n_2 ,\refclk_stable_count_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\refclk_stable_count_reg[0]_i_2_n_4 ,\refclk_stable_count_reg[0]_i_2_n_5 ,\refclk_stable_count_reg[0]_i_2_n_6 ,\refclk_stable_count_reg[0]_i_2_n_7 }),
        .S({refclk_stable_count_reg[3:1],\refclk_stable_count[0]_i_9_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[10] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[11] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[12] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[12]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[12]_i_1 
       (.CI(\refclk_stable_count_reg[8]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[12]_i_1_n_0 ,\refclk_stable_count_reg[12]_i_1_n_1 ,\refclk_stable_count_reg[12]_i_1_n_2 ,\refclk_stable_count_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[12]_i_1_n_4 ,\refclk_stable_count_reg[12]_i_1_n_5 ,\refclk_stable_count_reg[12]_i_1_n_6 ,\refclk_stable_count_reg[12]_i_1_n_7 }),
        .S(refclk_stable_count_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[13] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[14] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[15] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[16] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[16]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[16]_i_1 
       (.CI(\refclk_stable_count_reg[12]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[16]_i_1_n_0 ,\refclk_stable_count_reg[16]_i_1_n_1 ,\refclk_stable_count_reg[16]_i_1_n_2 ,\refclk_stable_count_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[16]_i_1_n_4 ,\refclk_stable_count_reg[16]_i_1_n_5 ,\refclk_stable_count_reg[16]_i_1_n_6 ,\refclk_stable_count_reg[16]_i_1_n_7 }),
        .S(refclk_stable_count_reg[19:16]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[17] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[18] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[19] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_6 ),
        .Q(refclk_stable_count_reg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[20] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[20]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[20]_i_1 
       (.CI(\refclk_stable_count_reg[16]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[20]_i_1_n_0 ,\refclk_stable_count_reg[20]_i_1_n_1 ,\refclk_stable_count_reg[20]_i_1_n_2 ,\refclk_stable_count_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[20]_i_1_n_4 ,\refclk_stable_count_reg[20]_i_1_n_5 ,\refclk_stable_count_reg[20]_i_1_n_6 ,\refclk_stable_count_reg[20]_i_1_n_7 }),
        .S(refclk_stable_count_reg[23:20]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[21] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[22] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[23] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[24] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[24]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[24]_i_1 
       (.CI(\refclk_stable_count_reg[20]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[24]_i_1_n_0 ,\refclk_stable_count_reg[24]_i_1_n_1 ,\refclk_stable_count_reg[24]_i_1_n_2 ,\refclk_stable_count_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[24]_i_1_n_4 ,\refclk_stable_count_reg[24]_i_1_n_5 ,\refclk_stable_count_reg[24]_i_1_n_6 ,\refclk_stable_count_reg[24]_i_1_n_7 }),
        .S(refclk_stable_count_reg[27:24]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[25] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[26] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[27] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[28] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[28]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[28]_i_1 
       (.CI(\refclk_stable_count_reg[24]_i_1_n_0 ),
        .CO({\NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED [3],\refclk_stable_count_reg[28]_i_1_n_1 ,\refclk_stable_count_reg[28]_i_1_n_2 ,\refclk_stable_count_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[28]_i_1_n_4 ,\refclk_stable_count_reg[28]_i_1_n_5 ,\refclk_stable_count_reg[28]_i_1_n_6 ,\refclk_stable_count_reg[28]_i_1_n_7 }),
        .S(refclk_stable_count_reg[31:28]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[29] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_5 ),
        .Q(refclk_stable_count_reg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[30] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[31] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_4 ),
        .Q(refclk_stable_count_reg[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[4]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[4]_i_1 
       (.CI(\refclk_stable_count_reg[0]_i_2_n_0 ),
        .CO({\refclk_stable_count_reg[4]_i_1_n_0 ,\refclk_stable_count_reg[4]_i_1_n_1 ,\refclk_stable_count_reg[4]_i_1_n_2 ,\refclk_stable_count_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[4]_i_1_n_4 ,\refclk_stable_count_reg[4]_i_1_n_5 ,\refclk_stable_count_reg[4]_i_1_n_6 ,\refclk_stable_count_reg[4]_i_1_n_7 }),
        .S(refclk_stable_count_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[7] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[8] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[8]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[8]_i_1 
       (.CI(\refclk_stable_count_reg[4]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[8]_i_1_n_0 ,\refclk_stable_count_reg[8]_i_1_n_1 ,\refclk_stable_count_reg[8]_i_1_n_2 ,\refclk_stable_count_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[8]_i_1_n_4 ,\refclk_stable_count_reg[8]_i_1_n_5 ,\refclk_stable_count_reg[8]_i_1_n_6 ,\refclk_stable_count_reg[8]_i_1_n_7 }),
        .S(refclk_stable_count_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[9] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    refclk_stable_i_1
       (.I0(\refclk_stable_count[0]_i_7_n_0 ),
        .I1(refclk_stable_i_2_n_0),
        .I2(refclk_stable_i_3_n_0),
        .I3(refclk_stable_i_4_n_0),
        .I4(refclk_stable_i_5_n_0),
        .I5(refclk_stable_i_6_n_0),
        .O(refclk_stable_i_1_n_0));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    refclk_stable_i_2
       (.I0(refclk_stable_count_reg[4]),
        .I1(refclk_stable_count_reg[5]),
        .I2(refclk_stable_count_reg[2]),
        .I3(refclk_stable_count_reg[3]),
        .I4(refclk_stable_count_reg[7]),
        .I5(refclk_stable_count_reg[6]),
        .O(refclk_stable_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000200000000000)) 
    refclk_stable_i_3
       (.I0(refclk_stable_count_reg[10]),
        .I1(refclk_stable_count_reg[11]),
        .I2(refclk_stable_count_reg[8]),
        .I3(refclk_stable_count_reg[9]),
        .I4(refclk_stable_count_reg[12]),
        .I5(refclk_stable_count_reg[13]),
        .O(refclk_stable_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    refclk_stable_i_4
       (.I0(refclk_stable_count_reg[16]),
        .I1(refclk_stable_count_reg[17]),
        .I2(refclk_stable_count_reg[14]),
        .I3(refclk_stable_count_reg[15]),
        .I4(refclk_stable_count_reg[18]),
        .I5(refclk_stable_count_reg[19]),
        .O(refclk_stable_i_4_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    refclk_stable_i_5
       (.I0(refclk_stable_count_reg[22]),
        .I1(refclk_stable_count_reg[23]),
        .I2(refclk_stable_count_reg[20]),
        .I3(refclk_stable_count_reg[21]),
        .I4(refclk_stable_count_reg[25]),
        .I5(refclk_stable_count_reg[24]),
        .O(refclk_stable_i_5_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    refclk_stable_i_6
       (.I0(refclk_stable_count_reg[28]),
        .I1(refclk_stable_count_reg[29]),
        .I2(refclk_stable_count_reg[26]),
        .I3(refclk_stable_count_reg[27]),
        .I4(refclk_stable_count_reg[31]),
        .I5(refclk_stable_count_reg[30]),
        .O(refclk_stable_i_6_n_0));
  FDRE #(
    .INIT(1'b0)) 
    refclk_stable_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(refclk_stable_i_1_n_0),
        .Q(refclk_stable_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h440000FF50505050)) 
    reset_time_out_i_2__0
       (.I0(tx_state[3]),
        .I1(txresetdone_s3),
        .I2(init_wait_done_reg_n_0),
        .I3(tx_state[1]),
        .I4(tx_state[2]),
        .I5(tx_state[0]),
        .O(reset_time_out_i_2__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    reset_time_out_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(sync_cplllock_n_0),
        .Q(reset_time_out),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT5 #(
    .INIT(32'hFFFB0002)) 
    run_phase_alignment_int_i_1
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .I4(run_phase_alignment_int_reg_n_0),
        .O(run_phase_alignment_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(run_phase_alignment_int_i_1_n_0),
        .Q(run_phase_alignment_int_reg_n_0),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_s3_reg
       (.C(data_sync_reg1),
        .CE(1'b1),
        .D(data_out),
        .Q(run_phase_alignment_int_s3),
        .R(1'b0));
  gig_ethernet_pcs_pma_0_sync_block_4 sync_TXRESETDONE
       (.data_out(txresetdone_s2),
        .data_sync_reg1_0(data_sync_reg1_0),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_5 sync_cplllock
       (.E(sync_cplllock_n_1),
        .\FSM_sequential_tx_state_reg[0] (\FSM_sequential_tx_state[3]_i_3_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_0 (\FSM_sequential_tx_state[3]_i_4_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_1 (\FSM_sequential_tx_state[3]_i_6_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_2 (time_out_2ms_reg_n_0),
        .\FSM_sequential_tx_state_reg[0]_3 (\FSM_sequential_tx_state[3]_i_7_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_4 (pll_reset_asserted_reg_n_0),
        .\FSM_sequential_tx_state_reg[0]_5 (refclk_stable_reg_n_0),
        .\FSM_sequential_tx_state_reg[0]_6 (\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .Q(tx_state),
        .data_sync_reg1_0(data_sync_reg1_2),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_lock_reclocked(mmcm_lock_reclocked),
        .reset_time_out(reset_time_out),
        .reset_time_out_reg(sync_cplllock_n_0),
        .reset_time_out_reg_0(reset_time_out_i_2__0_n_0),
        .reset_time_out_reg_1(init_wait_done_reg_n_0));
  gig_ethernet_pcs_pma_0_sync_block_6 sync_mmcm_lock_reclocked
       (.SR(sync_mmcm_lock_reclocked_n_0),
        .data_out(mmcm_lock_i),
        .data_sync_reg1_0(data_sync_reg1_1),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_7 sync_run_phase_alignment_int
       (.data_in(run_phase_alignment_int_reg_n_0),
        .data_out(data_out),
        .data_sync_reg6_0(data_sync_reg1));
  gig_ethernet_pcs_pma_0_sync_block_8 sync_time_out_wait_bypass
       (.data_in(time_out_wait_bypass_reg_n_0),
        .data_out(time_out_wait_bypass_s2),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_9 sync_tx_fsm_reset_done_int
       (.data_in(data_in),
        .data_out(tx_fsm_reset_done_int_s2),
        .data_sync_reg1_0(data_sync_reg1));
  LUT4 #(
    .INIT(16'h00AE)) 
    time_out_2ms_i_1
       (.I0(time_out_2ms_reg_n_0),
        .I1(time_out_2ms_i_2__0_n_0),
        .I2(time_out_2ms_i_3_n_0),
        .I3(reset_time_out),
        .O(time_out_2ms_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000800)) 
    time_out_2ms_i_2__0
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[18]),
        .I2(time_out_counter_reg[10]),
        .I3(time_out_counter_reg[12]),
        .I4(time_out_counter_reg[5]),
        .I5(time_tlock_max_i_3_n_0),
        .O(time_out_2ms_i_2__0_n_0));
  LUT4 #(
    .INIT(16'hFFFD)) 
    time_out_2ms_i_3
       (.I0(time_out_counter_reg[7]),
        .I1(time_out_counter_reg[14]),
        .I2(time_out_2ms_i_4__0_n_0),
        .I3(time_out_2ms_i_5_n_0),
        .O(time_out_2ms_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    time_out_2ms_i_4__0
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[3]),
        .I2(time_out_counter_reg[8]),
        .I3(time_out_counter_reg[6]),
        .O(time_out_2ms_i_4__0_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFEF)) 
    time_out_2ms_i_5
       (.I0(time_out_counter_reg[0]),
        .I1(time_out_counter_reg[13]),
        .I2(time_out_counter_reg[9]),
        .I3(time_out_counter_reg[2]),
        .I4(time_out_counter_reg[1]),
        .O(time_out_2ms_i_5_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_2ms_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_2ms_i_1_n_0),
        .Q(time_out_2ms_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000AAAAEAAA)) 
    time_out_500us_i_1
       (.I0(time_out_500us_reg_n_0),
        .I1(time_out_500us_i_2_n_0),
        .I2(time_out_counter_reg[5]),
        .I3(time_out_counter_reg[10]),
        .I4(time_out_2ms_i_3_n_0),
        .I5(reset_time_out),
        .O(time_out_500us_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    time_out_500us_i_2
       (.I0(time_out_counter_reg[15]),
        .I1(time_out_counter_reg[16]),
        .I2(time_out_counter_reg[11]),
        .I3(time_out_counter_reg[12]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[17]),
        .O(time_out_500us_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_500us_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_500us_i_1_n_0),
        .Q(time_out_500us_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFBFFFF)) 
    \time_out_counter[0]_i_1__0 
       (.I0(time_tlock_max_i_3_n_0),
        .I1(\time_out_counter[0]_i_3__0_n_0 ),
        .I2(time_out_2ms_i_3_n_0),
        .I3(time_out_counter_reg[10]),
        .I4(time_out_counter_reg[12]),
        .I5(time_out_counter_reg[5]),
        .O(time_out_counter));
  LUT2 #(
    .INIT(4'h8)) 
    \time_out_counter[0]_i_3__0 
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[18]),
        .O(\time_out_counter[0]_i_3__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \time_out_counter[0]_i_4 
       (.I0(time_out_counter_reg[0]),
        .O(\time_out_counter[0]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[0] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_7 ),
        .Q(time_out_counter_reg[0]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\time_out_counter_reg[0]_i_2_n_0 ,\time_out_counter_reg[0]_i_2_n_1 ,\time_out_counter_reg[0]_i_2_n_2 ,\time_out_counter_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\time_out_counter_reg[0]_i_2_n_4 ,\time_out_counter_reg[0]_i_2_n_5 ,\time_out_counter_reg[0]_i_2_n_6 ,\time_out_counter_reg[0]_i_2_n_7 }),
        .S({time_out_counter_reg[3:1],\time_out_counter[0]_i_4_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[10] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_5 ),
        .Q(time_out_counter_reg[10]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[11] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_4 ),
        .Q(time_out_counter_reg[11]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[12] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_7 ),
        .Q(time_out_counter_reg[12]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[12]_i_1 
       (.CI(\time_out_counter_reg[8]_i_1_n_0 ),
        .CO({\time_out_counter_reg[12]_i_1_n_0 ,\time_out_counter_reg[12]_i_1_n_1 ,\time_out_counter_reg[12]_i_1_n_2 ,\time_out_counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[12]_i_1_n_4 ,\time_out_counter_reg[12]_i_1_n_5 ,\time_out_counter_reg[12]_i_1_n_6 ,\time_out_counter_reg[12]_i_1_n_7 }),
        .S(time_out_counter_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[13] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_6 ),
        .Q(time_out_counter_reg[13]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[14] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_5 ),
        .Q(time_out_counter_reg[14]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[15] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_4 ),
        .Q(time_out_counter_reg[15]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[16] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1_n_7 ),
        .Q(time_out_counter_reg[16]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[16]_i_1 
       (.CI(\time_out_counter_reg[12]_i_1_n_0 ),
        .CO({\NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED [3:2],\time_out_counter_reg[16]_i_1_n_2 ,\time_out_counter_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED [3],\time_out_counter_reg[16]_i_1_n_5 ,\time_out_counter_reg[16]_i_1_n_6 ,\time_out_counter_reg[16]_i_1_n_7 }),
        .S({1'b0,time_out_counter_reg[18:16]}));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[17] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1_n_6 ),
        .Q(time_out_counter_reg[17]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[18] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1_n_5 ),
        .Q(time_out_counter_reg[18]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[1] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_6 ),
        .Q(time_out_counter_reg[1]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[2] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_5 ),
        .Q(time_out_counter_reg[2]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[3] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_4 ),
        .Q(time_out_counter_reg[3]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[4] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_7 ),
        .Q(time_out_counter_reg[4]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[4]_i_1 
       (.CI(\time_out_counter_reg[0]_i_2_n_0 ),
        .CO({\time_out_counter_reg[4]_i_1_n_0 ,\time_out_counter_reg[4]_i_1_n_1 ,\time_out_counter_reg[4]_i_1_n_2 ,\time_out_counter_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[4]_i_1_n_4 ,\time_out_counter_reg[4]_i_1_n_5 ,\time_out_counter_reg[4]_i_1_n_6 ,\time_out_counter_reg[4]_i_1_n_7 }),
        .S(time_out_counter_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[5] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_6 ),
        .Q(time_out_counter_reg[5]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[6] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_5 ),
        .Q(time_out_counter_reg[6]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[7] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_4 ),
        .Q(time_out_counter_reg[7]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[8] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_7 ),
        .Q(time_out_counter_reg[8]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[8]_i_1 
       (.CI(\time_out_counter_reg[4]_i_1_n_0 ),
        .CO({\time_out_counter_reg[8]_i_1_n_0 ,\time_out_counter_reg[8]_i_1_n_1 ,\time_out_counter_reg[8]_i_1_n_2 ,\time_out_counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[8]_i_1_n_4 ,\time_out_counter_reg[8]_i_1_n_5 ,\time_out_counter_reg[8]_i_1_n_6 ,\time_out_counter_reg[8]_i_1_n_7 }),
        .S(time_out_counter_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[9] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_6 ),
        .Q(time_out_counter_reg[9]),
        .R(reset_time_out));
  LUT4 #(
    .INIT(16'hAB00)) 
    time_out_wait_bypass_i_1
       (.I0(time_out_wait_bypass_reg_n_0),
        .I1(tx_fsm_reset_done_int_s3),
        .I2(time_out_wait_bypass_i_2_n_0),
        .I3(run_phase_alignment_int_s3),
        .O(time_out_wait_bypass_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFEFFFFFFFFF)) 
    time_out_wait_bypass_i_2
       (.I0(time_out_wait_bypass_i_3_n_0),
        .I1(time_out_wait_bypass_i_4_n_0),
        .I2(wait_bypass_count_reg[5]),
        .I3(wait_bypass_count_reg[13]),
        .I4(wait_bypass_count_reg[11]),
        .I5(time_out_wait_bypass_i_5_n_0),
        .O(time_out_wait_bypass_i_2_n_0));
  LUT4 #(
    .INIT(16'hFF7F)) 
    time_out_wait_bypass_i_3
       (.I0(wait_bypass_count_reg[16]),
        .I1(wait_bypass_count_reg[9]),
        .I2(wait_bypass_count_reg[12]),
        .I3(wait_bypass_count_reg[10]),
        .O(time_out_wait_bypass_i_3_n_0));
  LUT4 #(
    .INIT(16'hDFFF)) 
    time_out_wait_bypass_i_4
       (.I0(wait_bypass_count_reg[4]),
        .I1(wait_bypass_count_reg[15]),
        .I2(wait_bypass_count_reg[6]),
        .I3(wait_bypass_count_reg[0]),
        .O(time_out_wait_bypass_i_4_n_0));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    time_out_wait_bypass_i_5
       (.I0(wait_bypass_count_reg[8]),
        .I1(wait_bypass_count_reg[1]),
        .I2(wait_bypass_count_reg[7]),
        .I3(wait_bypass_count_reg[14]),
        .I4(wait_bypass_count_reg[2]),
        .I5(wait_bypass_count_reg[3]),
        .O(time_out_wait_bypass_i_5_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_reg
       (.C(data_sync_reg1),
        .CE(1'b1),
        .D(time_out_wait_bypass_i_1_n_0),
        .Q(time_out_wait_bypass_reg_n_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_wait_bypass_s2),
        .Q(time_out_wait_bypass_s3),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000AAAAAAEA)) 
    time_tlock_max_i_1
       (.I0(time_tlock_max_reg_n_0),
        .I1(time_tlock_max_i_2_n_0),
        .I2(time_out_counter_reg[5]),
        .I3(time_tlock_max_i_3_n_0),
        .I4(time_tlock_max_i_4_n_0),
        .I5(reset_time_out),
        .O(time_tlock_max_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    time_tlock_max_i_2
       (.I0(time_out_counter_reg[14]),
        .I1(time_out_counter_reg[12]),
        .I2(time_out_counter_reg[10]),
        .I3(time_out_counter_reg[7]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[17]),
        .O(time_tlock_max_i_2_n_0));
  LUT3 #(
    .INIT(8'hEF)) 
    time_tlock_max_i_3
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[15]),
        .I2(time_out_counter_reg[11]),
        .O(time_tlock_max_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    time_tlock_max_i_4
       (.I0(time_out_2ms_i_5_n_0),
        .I1(time_out_counter_reg[6]),
        .I2(time_out_counter_reg[8]),
        .I3(time_out_counter_reg[3]),
        .I4(time_out_counter_reg[4]),
        .O(time_tlock_max_i_4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_tlock_max_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_tlock_max_i_1_n_0),
        .Q(time_tlock_max_reg_n_0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFFF1000)) 
    tx_fsm_reset_done_int_i_1
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[0]),
        .I3(tx_state[3]),
        .I4(data_in),
        .O(tx_fsm_reset_done_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    tx_fsm_reset_done_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(tx_fsm_reset_done_int_i_1_n_0),
        .Q(data_in),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    tx_fsm_reset_done_int_s3_reg
       (.C(data_sync_reg1),
        .CE(1'b1),
        .D(tx_fsm_reset_done_int_s2),
        .Q(tx_fsm_reset_done_int_s3),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    txresetdone_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(txresetdone_s2),
        .Q(txresetdone_s3),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_1 
       (.I0(run_phase_alignment_int_s3),
        .O(clear));
  LUT2 #(
    .INIT(4'h2)) 
    \wait_bypass_count[0]_i_2 
       (.I0(time_out_wait_bypass_i_2_n_0),
        .I1(tx_fsm_reset_done_int_s3),
        .O(\wait_bypass_count[0]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_4__0 
       (.I0(wait_bypass_count_reg[0]),
        .O(\wait_bypass_count[0]_i_4__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[0] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_7 ),
        .Q(wait_bypass_count_reg[0]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\wait_bypass_count_reg[0]_i_3_n_0 ,\wait_bypass_count_reg[0]_i_3_n_1 ,\wait_bypass_count_reg[0]_i_3_n_2 ,\wait_bypass_count_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\wait_bypass_count_reg[0]_i_3_n_4 ,\wait_bypass_count_reg[0]_i_3_n_5 ,\wait_bypass_count_reg[0]_i_3_n_6 ,\wait_bypass_count_reg[0]_i_3_n_7 }),
        .S({wait_bypass_count_reg[3:1],\wait_bypass_count[0]_i_4__0_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[10] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[10]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[11] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[11]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[12] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[12]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[12]_i_1 
       (.CI(\wait_bypass_count_reg[8]_i_1_n_0 ),
        .CO({\wait_bypass_count_reg[12]_i_1_n_0 ,\wait_bypass_count_reg[12]_i_1_n_1 ,\wait_bypass_count_reg[12]_i_1_n_2 ,\wait_bypass_count_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[12]_i_1_n_4 ,\wait_bypass_count_reg[12]_i_1_n_5 ,\wait_bypass_count_reg[12]_i_1_n_6 ,\wait_bypass_count_reg[12]_i_1_n_7 }),
        .S(wait_bypass_count_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[13] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[13]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[14] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[14]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[15] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[15]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[16] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[16]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[16]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[16]_i_1 
       (.CI(\wait_bypass_count_reg[12]_i_1_n_0 ),
        .CO(\NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED [3:1],\wait_bypass_count_reg[16]_i_1_n_7 }),
        .S({1'b0,1'b0,1'b0,wait_bypass_count_reg[16]}));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[1] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_6 ),
        .Q(wait_bypass_count_reg[1]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[2] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_5 ),
        .Q(wait_bypass_count_reg[2]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[3] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_4 ),
        .Q(wait_bypass_count_reg[3]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[4] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[4]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[4]_i_1 
       (.CI(\wait_bypass_count_reg[0]_i_3_n_0 ),
        .CO({\wait_bypass_count_reg[4]_i_1_n_0 ,\wait_bypass_count_reg[4]_i_1_n_1 ,\wait_bypass_count_reg[4]_i_1_n_2 ,\wait_bypass_count_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[4]_i_1_n_4 ,\wait_bypass_count_reg[4]_i_1_n_5 ,\wait_bypass_count_reg[4]_i_1_n_6 ,\wait_bypass_count_reg[4]_i_1_n_7 }),
        .S(wait_bypass_count_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[5] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[5]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[6] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[6]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[7] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[7]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[8] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[8]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[8]_i_1 
       (.CI(\wait_bypass_count_reg[4]_i_1_n_0 ),
        .CO({\wait_bypass_count_reg[8]_i_1_n_0 ,\wait_bypass_count_reg[8]_i_1_n_1 ,\wait_bypass_count_reg[8]_i_1_n_2 ,\wait_bypass_count_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[8]_i_1_n_4 ,\wait_bypass_count_reg[8]_i_1_n_5 ,\wait_bypass_count_reg[8]_i_1_n_6 ,\wait_bypass_count_reg[8]_i_1_n_7 }),
        .S(wait_bypass_count_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[9] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[9]),
        .R(clear));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \wait_time_cnt[0]_i_1 
       (.I0(wait_time_cnt_reg[0]),
        .O(wait_time_cnt0));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[1]_i_1 
       (.I0(wait_time_cnt_reg[0]),
        .I1(wait_time_cnt_reg[1]),
        .O(\wait_time_cnt[1]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hE1)) 
    \wait_time_cnt[2]_i_1 
       (.I0(wait_time_cnt_reg[1]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[2]),
        .O(\wait_time_cnt[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT4 #(
    .INIT(16'hFE01)) 
    \wait_time_cnt[3]_i_1 
       (.I0(wait_time_cnt_reg[2]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[1]),
        .I3(wait_time_cnt_reg[3]),
        .O(\wait_time_cnt[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT5 #(
    .INIT(32'hFFFE0001)) 
    \wait_time_cnt[4]_i_1 
       (.I0(wait_time_cnt_reg[3]),
        .I1(wait_time_cnt_reg[1]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[2]),
        .I4(wait_time_cnt_reg[4]),
        .O(\wait_time_cnt[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000001)) 
    \wait_time_cnt[5]_i_1 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0700)) 
    \wait_time_cnt[6]_i_1__0 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[3]),
        .I3(tx_state[0]),
        .O(wait_time_cnt0_0));
  LUT2 #(
    .INIT(4'hE)) 
    \wait_time_cnt[6]_i_2 
       (.I0(\wait_time_cnt[6]_i_4_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(sel));
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[6]_i_3 
       (.I0(\wait_time_cnt[6]_i_4_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(\wait_time_cnt[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \wait_time_cnt[6]_i_4 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[6]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[0] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(wait_time_cnt0),
        .Q(wait_time_cnt_reg[0]),
        .R(wait_time_cnt0_0));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[1] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[1]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[1]),
        .R(wait_time_cnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[2] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[2]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[2]),
        .S(wait_time_cnt0_0));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[3] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[3]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[3]),
        .R(wait_time_cnt0_0));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[4] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[4]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[4]),
        .R(wait_time_cnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[5] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[5]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[5]),
        .S(wait_time_cnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[6] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[6]_i_3_n_0 ),
        .Q(wait_time_cnt_reg[6]),
        .S(wait_time_cnt0_0));
endmodule

module gig_ethernet_pcs_pma_0_block
   (gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    status_vector,
    resetdone,
    txn,
    txp,
    rxoutclk,
    txoutclk,
    mmcm_reset,
    out,
    signal_detect,
    CLK,
    data_in,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    configuration_vector,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    gtxe2_i);
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  output [6:0]status_vector;
  output resetdone;
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output mmcm_reset;
  input [0:0]out;
  input signal_detect;
  input CLK;
  input data_in;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  input [2:0]configuration_vector;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input gtxe2_i;

  wire CLK;
  wire [2:0]configuration_vector;
  wire data_in;
  wire enablealign;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire mgt_rx_reset;
  wire mgt_tx_reset;
  wire mmcm_reset;
  wire [0:0]out;
  wire powerdown;
  wire resetdone;
  wire rx_reset_done_i;
  wire rxbuferr;
  wire rxchariscomma;
  wire rxcharisk;
  wire [1:0]rxclkcorcnt;
  wire [7:0]rxdata;
  wire rxdisperr;
  wire rxn;
  wire rxnotintable;
  wire rxoutclk;
  wire rxp;
  wire signal_detect;
  wire [6:0]status_vector;
  wire transceiver_inst_n_11;
  wire transceiver_inst_n_12;
  wire txbuferr;
  wire txchardispmode;
  wire txchardispval;
  wire txcharisk;
  wire [7:0]txdata;
  wire txn;
  wire txoutclk;
  wire txp;
  wire NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED;
  wire [9:0]NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED;
  wire [15:0]NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED;
  wire [63:0]NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED;
  wire [31:0]NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED;
  wire [47:0]NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED;
  wire [1:0]NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED;
  wire [31:0]NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED;
  wire [1:0]NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED;
  wire [1:0]NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED;
  wire [15:7]NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED;
  wire [9:0]NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED;

  (* B_SHIFTER_ADDR = "10'b0101001110" *) 
  (* C_1588 = "0" *) 
  (* C_2_5G = "FALSE" *) 
  (* C_COMPONENT_NAME = "gig_ethernet_pcs_pma_0" *) 
  (* C_DYNAMIC_SWITCHING = "FALSE" *) 
  (* C_ELABORATION_TRANSIENT_DIR = "BlankString" *) 
  (* C_FAMILY = "kintex7" *) 
  (* C_HAS_AN = "FALSE" *) 
  (* C_HAS_AXIL = "FALSE" *) 
  (* C_HAS_MDIO = "FALSE" *) 
  (* C_HAS_TEMAC = "TRUE" *) 
  (* C_IS_SGMII = "FALSE" *) 
  (* C_RX_GMII_CLK = "TXOUTCLK" *) 
  (* C_SGMII_FABRIC_BUFFER = "TRUE" *) 
  (* C_SGMII_PHY_MODE = "FALSE" *) 
  (* C_USE_LVDS = "FALSE" *) 
  (* C_USE_TBI = "FALSE" *) 
  (* C_USE_TRANSCEIVER = "TRUE" *) 
  (* GT_RX_BYTE_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  gig_ethernet_pcs_pma_0_gig_ethernet_pcs_pma_v16_2_19 gig_ethernet_pcs_pma_0_core
       (.an_adv_config_val(1'b0),
        .an_adv_config_vector({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .an_enable(NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED),
        .an_interrupt(NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED),
        .an_restart_config(1'b0),
        .basex_or_sgmii(1'b0),
        .configuration_valid(1'b0),
        .configuration_vector({1'b0,configuration_vector,1'b0}),
        .correction_timer({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dcm_locked(data_in),
        .drp_daddr(NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED[9:0]),
        .drp_dclk(1'b0),
        .drp_den(NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED),
        .drp_di(NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED[15:0]),
        .drp_do({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drp_drdy(1'b0),
        .drp_dwe(NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED),
        .drp_gnt(1'b0),
        .drp_req(NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED),
        .en_cdet(NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED),
        .enablealign(enablealign),
        .ewrap(NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gtx_clk(1'b0),
        .link_timer_basex({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_sgmii({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_value({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .loc_ref(NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED),
        .mdc(1'b0),
        .mdio_in(1'b0),
        .mdio_out(NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED),
        .mdio_tri(NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED),
        .mgt_rx_reset(mgt_rx_reset),
        .mgt_tx_reset(mgt_tx_reset),
        .phyad({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pma_rx_clk0(1'b0),
        .pma_rx_clk1(1'b0),
        .powerdown(powerdown),
        .reset(out),
        .reset_done(resetdone),
        .rx_code_group0({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_code_group1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_gt_nominal_latency({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1,1'b1,1'b0,1'b0,1'b0}),
        .rxbufstatus({rxbuferr,1'b0}),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .rxclkcorcnt({1'b0,rxclkcorcnt}),
        .rxdata(rxdata),
        .rxdisperr(rxdisperr),
        .rxnotintable(rxnotintable),
        .rxphy_correction_timer(NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED[63:0]),
        .rxphy_ns_field(NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED[31:0]),
        .rxphy_s_field(NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED[47:0]),
        .rxrecclk(1'b0),
        .rxrundisp(1'b0),
        .s_axi_aclk(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED),
        .s_axi_awvalid(1'b0),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED),
        .s_axi_rdata(NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_resetn(1'b0),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wready(NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED),
        .s_axi_wvalid(1'b0),
        .signal_detect(signal_detect),
        .speed_is_100(1'b0),
        .speed_is_10_100(1'b0),
        .speed_selection(NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED[1:0]),
        .status_vector({NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED[15:7],status_vector}),
        .systemtimer_ns_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .systemtimer_s_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .tx_code_group(NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED[9:0]),
        .txbuferr(txbuferr),
        .txchardispmode(txchardispmode),
        .txchardispval(txchardispval),
        .txcharisk(txcharisk),
        .txdata(txdata),
        .userclk(1'b0),
        .userclk2(CLK));
  gig_ethernet_pcs_pma_0_sync_block sync_block_rx_reset_done
       (.CLK(CLK),
        .data_in(transceiver_inst_n_12),
        .data_out(rx_reset_done_i));
  gig_ethernet_pcs_pma_0_sync_block_0 sync_block_tx_reset_done
       (.CLK(CLK),
        .data_in(transceiver_inst_n_11),
        .resetdone(resetdone),
        .resetdone_0(rx_reset_done_i));
  gig_ethernet_pcs_pma_0_transceiver transceiver_inst
       (.CLK(CLK),
        .D(txchardispmode),
        .Q(rxclkcorcnt),
        .SR(mgt_rx_reset),
        .data_in(transceiver_inst_n_11),
        .data_sync_reg1(data_in),
        .enablealign(enablealign),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(gtxe2_i),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out),
        .powerdown(powerdown),
        .reset_sync5(mgt_tx_reset),
        .rx_fsm_reset_done_int_reg(transceiver_inst_n_12),
        .rxbufstatus(rxbuferr),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .\rxdata_reg[7]_0 (rxdata),
        .rxdisperr(rxdisperr),
        .rxn(rxn),
        .rxnotintable(rxnotintable),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .status_vector(status_vector[1]),
        .txbuferr(txbuferr),
        .txchardispval_reg_reg_0(txchardispval),
        .txcharisk_reg_reg_0(txcharisk),
        .\txdata_reg_reg[7]_0 (txdata),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module gig_ethernet_pcs_pma_0_clocking
   (gtrefclk_out,
    gtrefclk_bufg,
    mmcm_locked,
    userclk,
    userclk2,
    rxuserclk2_out,
    gtrefclk_p,
    gtrefclk_n,
    txoutclk,
    mmcm_reset,
    rxoutclk);
  output gtrefclk_out;
  output gtrefclk_bufg;
  output mmcm_locked;
  output userclk;
  output userclk2;
  output rxuserclk2_out;
  input gtrefclk_p;
  input gtrefclk_n;
  input txoutclk;
  input mmcm_reset;
  input rxoutclk;

  wire clkfbout;
  wire clkout0;
  wire clkout1;
  wire gtrefclk_bufg;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire mmcm_locked;
  wire mmcm_reset;
  wire rxoutclk;
  wire rxuserclk2_out;
  wire txoutclk;
  wire txoutclk_bufg;
  wire userclk;
  wire userclk2;
  wire NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED;
  wire NLW_mmcm_adv_inst_DRDY_UNCONNECTED;
  wire NLW_mmcm_adv_inst_PSDONE_UNCONNECTED;
  wire [15:0]NLW_mmcm_adv_inst_DO_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_gtrefclk
       (.I(gtrefclk_out),
        .O(gtrefclk_bufg));
  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_txoutclk
       (.I(txoutclk),
        .O(txoutclk_bufg));
  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_userclk
       (.I(clkout1),
        .O(userclk));
  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_userclk2
       (.I(clkout0),
        .O(userclk2));
  (* box_type = "PRIMITIVE" *) 
  IBUFDS_GTE2 #(
    .CLKCM_CFG("TRUE"),
    .CLKRCV_TRST("TRUE"),
    .CLKSWING_CFG(2'b11)) 
    ibufds_gtrefclk
       (.CEB(1'b0),
        .I(gtrefclk_p),
        .IB(gtrefclk_n),
        .O(gtrefclk_out),
        .ODIV2(NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED));
  (* box_type = "PRIMITIVE" *) 
  MMCME2_ADV #(
    .BANDWIDTH("OPTIMIZED"),
    .CLKFBOUT_MULT_F(16.000000),
    .CLKFBOUT_PHASE(0.000000),
    .CLKFBOUT_USE_FINE_PS("FALSE"),
    .CLKIN1_PERIOD(16.000000),
    .CLKIN2_PERIOD(0.000000),
    .CLKOUT0_DIVIDE_F(8.000000),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT0_USE_FINE_PS("FALSE"),
    .CLKOUT1_DIVIDE(16),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUT1_USE_FINE_PS("FALSE"),
    .CLKOUT2_DIVIDE(1),
    .CLKOUT2_DUTY_CYCLE(0.500000),
    .CLKOUT2_PHASE(0.000000),
    .CLKOUT2_USE_FINE_PS("FALSE"),
    .CLKOUT3_DIVIDE(1),
    .CLKOUT3_DUTY_CYCLE(0.500000),
    .CLKOUT3_PHASE(0.000000),
    .CLKOUT3_USE_FINE_PS("FALSE"),
    .CLKOUT4_CASCADE("FALSE"),
    .CLKOUT4_DIVIDE(1),
    .CLKOUT4_DUTY_CYCLE(0.500000),
    .CLKOUT4_PHASE(0.000000),
    .CLKOUT4_USE_FINE_PS("FALSE"),
    .CLKOUT5_DIVIDE(1),
    .CLKOUT5_DUTY_CYCLE(0.500000),
    .CLKOUT5_PHASE(0.000000),
    .CLKOUT5_USE_FINE_PS("FALSE"),
    .CLKOUT6_DIVIDE(1),
    .CLKOUT6_DUTY_CYCLE(0.500000),
    .CLKOUT6_PHASE(0.000000),
    .CLKOUT6_USE_FINE_PS("FALSE"),
    .COMPENSATION("INTERNAL"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKINSEL_INVERTED(1'b0),
    .IS_PSEN_INVERTED(1'b0),
    .IS_PSINCDEC_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER1(0.010000),
    .REF_JITTER2(0.000000),
    .SS_EN("FALSE"),
    .SS_MODE("CENTER_HIGH"),
    .SS_MOD_PERIOD(10000),
    .STARTUP_WAIT("FALSE")) 
    mmcm_adv_inst
       (.CLKFBIN(clkfbout),
        .CLKFBOUT(clkfbout),
        .CLKFBOUTB(NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED),
        .CLKFBSTOPPED(NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED),
        .CLKIN1(txoutclk_bufg),
        .CLKIN2(1'b0),
        .CLKINSEL(1'b1),
        .CLKINSTOPPED(NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED),
        .CLKOUT0(clkout0),
        .CLKOUT0B(NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(clkout1),
        .CLKOUT1B(NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED),
        .CLKOUT2(NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED),
        .CLKOUT2B(NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED),
        .CLKOUT3(NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED),
        .CLKOUT3B(NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED),
        .CLKOUT4(NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED),
        .CLKOUT5(NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED),
        .CLKOUT6(NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_mmcm_adv_inst_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_mmcm_adv_inst_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(mmcm_locked),
        .PSCLK(1'b0),
        .PSDONE(NLW_mmcm_adv_inst_PSDONE_UNCONNECTED),
        .PSEN(1'b0),
        .PSINCDEC(1'b0),
        .PWRDWN(1'b0),
        .RST(mmcm_reset));
  (* box_type = "PRIMITIVE" *) 
  BUFG rxrecclkbufg
       (.I(rxoutclk),
        .O(rxuserclk2_out));
endmodule

module gig_ethernet_pcs_pma_0_cpll_railing
   (cpll_pd0_i,
    cpllreset_in,
    gtrefclk_bufg,
    gt0_cpllreset_t);
  output cpll_pd0_i;
  output cpllreset_in;
  input gtrefclk_bufg;
  input gt0_cpllreset_t;

  wire cpll_pd0_i;
  wire cpll_reset_out;
  wire \cpllpd_wait_reg[31]_srl32_n_1 ;
  wire \cpllpd_wait_reg[63]_srl32_n_1 ;
  wire \cpllpd_wait_reg[94]_srl31_n_0 ;
  wire cpllreset_in;
  wire \cpllreset_wait_reg[126]_srl31_n_0 ;
  wire \cpllreset_wait_reg[31]_srl32_n_1 ;
  wire \cpllreset_wait_reg[63]_srl32_n_1 ;
  wire \cpllreset_wait_reg[95]_srl32_n_1 ;
  wire gt0_cpllreset_t;
  wire gtrefclk_bufg;
  wire \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED ;

  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[31]_srl32 " *) 
  SRLC32E #(
    .INIT(32'hFFFFFFFF)) 
    \cpllpd_wait_reg[31]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(1'b0),
        .Q(\NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllpd_wait_reg[31]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[63]_srl32 " *) 
  SRLC32E #(
    .INIT(32'hFFFFFFFF)) 
    \cpllpd_wait_reg[63]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllpd_wait_reg[31]_srl32_n_1 ),
        .Q(\NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllpd_wait_reg[63]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[94]_srl31 " *) 
  SRLC32E #(
    .INIT(32'h7FFFFFFF)) 
    \cpllpd_wait_reg[94]_srl31 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b0}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllpd_wait_reg[63]_srl32_n_1 ),
        .Q(\cpllpd_wait_reg[94]_srl31_n_0 ),
        .Q31(\NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED ));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b1)) 
    \cpllpd_wait_reg[95] 
       (.C(gtrefclk_bufg),
        .CE(1'b1),
        .D(\cpllpd_wait_reg[94]_srl31_n_0 ),
        .Q(cpll_pd0_i),
        .R(1'b0));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[126]_srl31 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[126]_srl31 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b0}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllreset_wait_reg[95]_srl32_n_1 ),
        .Q(\cpllreset_wait_reg[126]_srl31_n_0 ),
        .Q31(\NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED ));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \cpllreset_wait_reg[127] 
       (.C(gtrefclk_bufg),
        .CE(1'b1),
        .D(\cpllreset_wait_reg[126]_srl31_n_0 ),
        .Q(cpll_reset_out),
        .R(1'b0));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[31]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h000000FF)) 
    \cpllreset_wait_reg[31]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(1'b0),
        .Q(\NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[31]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[63]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[63]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllreset_wait_reg[31]_srl32_n_1 ),
        .Q(\NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[63]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[95]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[95]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllreset_wait_reg[63]_srl32_n_1 ),
        .Q(\NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[95]_srl32_n_1 ));
  LUT2 #(
    .INIT(4'hE)) 
    gtxe2_i_i_1
       (.I0(cpll_reset_out),
        .I1(gt0_cpllreset_t),
        .O(cpllreset_in));
endmodule

module gig_ethernet_pcs_pma_0_gt_common
   (gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    gtrefclk_out,
    independent_clock_bufg,
    out);
  output gt0_qplloutclk_out;
  output gt0_qplloutrefclk_out;
  input gtrefclk_out;
  input independent_clock_bufg;
  input [0:0]out;

  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_out;
  wire gtxe2_common_i_n_2;
  wire gtxe2_common_i_n_5;
  wire independent_clock_bufg;
  wire [0:0]out;
  wire NLW_gtxe2_common_i_DRPRDY_UNCONNECTED;
  wire NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED;
  wire NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED;
  wire [15:0]NLW_gtxe2_common_i_DRPDO_UNCONNECTED;
  wire [7:0]NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  GTXE2_COMMON #(
    .BIAS_CFG(64'h0000040000001000),
    .COMMON_CFG(32'h00000000),
    .IS_DRPCLK_INVERTED(1'b0),
    .IS_GTGREFCLK_INVERTED(1'b0),
    .IS_QPLLLOCKDETCLK_INVERTED(1'b0),
    .QPLL_CFG(27'h06801C1),
    .QPLL_CLKOUT_CFG(4'b0000),
    .QPLL_COARSE_FREQ_OVRD(6'b010000),
    .QPLL_COARSE_FREQ_OVRD_EN(1'b0),
    .QPLL_CP(10'b0000011111),
    .QPLL_CP_MONITOR_EN(1'b0),
    .QPLL_DMONITOR_SEL(1'b0),
    .QPLL_FBDIV(10'b0000100000),
    .QPLL_FBDIV_MONITOR_EN(1'b0),
    .QPLL_FBDIV_RATIO(1'b1),
    .QPLL_INIT_CFG(24'h000006),
    .QPLL_LOCK_CFG(16'h21E8),
    .QPLL_LPF(4'b1111),
    .QPLL_REFCLK_DIV(1),
    .SIM_QPLLREFCLK_SEL(3'b001),
    .SIM_RESET_SPEEDUP("FALSE"),
    .SIM_VERSION("4.0")) 
    gtxe2_common_i
       (.BGBYPASSB(1'b1),
        .BGMONITORENB(1'b1),
        .BGPDB(1'b1),
        .BGRCALOVRD({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DRPADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPCLK(1'b0),
        .DRPDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPDO(NLW_gtxe2_common_i_DRPDO_UNCONNECTED[15:0]),
        .DRPEN(1'b0),
        .DRPRDY(NLW_gtxe2_common_i_DRPRDY_UNCONNECTED),
        .DRPWE(1'b0),
        .GTGREFCLK(1'b0),
        .GTNORTHREFCLK0(1'b0),
        .GTNORTHREFCLK1(1'b0),
        .GTREFCLK0(gtrefclk_out),
        .GTREFCLK1(1'b0),
        .GTSOUTHREFCLK0(1'b0),
        .GTSOUTHREFCLK1(1'b0),
        .PMARSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLDMONITOR(NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED[7:0]),
        .QPLLFBCLKLOST(NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED),
        .QPLLLOCK(gtxe2_common_i_n_2),
        .QPLLLOCKDETCLK(independent_clock_bufg),
        .QPLLLOCKEN(1'b1),
        .QPLLOUTCLK(gt0_qplloutclk_out),
        .QPLLOUTREFCLK(gt0_qplloutrefclk_out),
        .QPLLOUTRESET(1'b0),
        .QPLLPD(1'b1),
        .QPLLREFCLKLOST(gtxe2_common_i_n_5),
        .QPLLREFCLKSEL({1'b0,1'b0,1'b1}),
        .QPLLRESET(out),
        .QPLLRSVD1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLRSVD2({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .RCALENB(1'b1),
        .REFCLKOUTMONITOR(NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED));
endmodule

module gig_ethernet_pcs_pma_0_reset_sync
   (reset_out,
    CLK,
    enablealign);
  output reset_out;
  input CLK;
  input enablealign;

  wire CLK;
  wire enablealign;
  wire reset_out;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(CLK),
        .CE(1'b1),
        .D(1'b0),
        .PRE(enablealign),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(enablealign),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(enablealign),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(enablealign),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(enablealign),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_reset_sync" *) 
module gig_ethernet_pcs_pma_0_reset_sync_1
   (reset_out,
    independent_clock_bufg,
    SR);
  output reset_out;
  input independent_clock_bufg;
  input [0:0]SR;

  wire [0:0]SR;
  wire independent_clock_bufg;
  wire reset_out;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(SR),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(SR),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(SR),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(SR),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(SR),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_reset_sync" *) 
module gig_ethernet_pcs_pma_0_reset_sync_2
   (reset_out,
    independent_clock_bufg,
    reset_sync5_0);
  output reset_out;
  input independent_clock_bufg;
  input [0:0]reset_sync5_0;

  wire independent_clock_bufg;
  wire reset_out;
  wire [0:0]reset_sync5_0;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

module gig_ethernet_pcs_pma_0_reset_wtd_timer
   (reset,
    independent_clock_bufg,
    data_out);
  output reset;
  input independent_clock_bufg;
  input data_out;

  wire \counter_stg1[5]_i_1_n_0 ;
  wire \counter_stg1[5]_i_3_n_0 ;
  wire [5:5]counter_stg1_reg;
  wire [4:0]counter_stg1_reg__0;
  wire \counter_stg2[0]_i_3_n_0 ;
  wire [11:0]counter_stg2_reg;
  wire \counter_stg2_reg[0]_i_2_n_0 ;
  wire \counter_stg2_reg[0]_i_2_n_1 ;
  wire \counter_stg2_reg[0]_i_2_n_2 ;
  wire \counter_stg2_reg[0]_i_2_n_3 ;
  wire \counter_stg2_reg[0]_i_2_n_4 ;
  wire \counter_stg2_reg[0]_i_2_n_5 ;
  wire \counter_stg2_reg[0]_i_2_n_6 ;
  wire \counter_stg2_reg[0]_i_2_n_7 ;
  wire \counter_stg2_reg[4]_i_1_n_0 ;
  wire \counter_stg2_reg[4]_i_1_n_1 ;
  wire \counter_stg2_reg[4]_i_1_n_2 ;
  wire \counter_stg2_reg[4]_i_1_n_3 ;
  wire \counter_stg2_reg[4]_i_1_n_4 ;
  wire \counter_stg2_reg[4]_i_1_n_5 ;
  wire \counter_stg2_reg[4]_i_1_n_6 ;
  wire \counter_stg2_reg[4]_i_1_n_7 ;
  wire \counter_stg2_reg[8]_i_1_n_1 ;
  wire \counter_stg2_reg[8]_i_1_n_2 ;
  wire \counter_stg2_reg[8]_i_1_n_3 ;
  wire \counter_stg2_reg[8]_i_1_n_4 ;
  wire \counter_stg2_reg[8]_i_1_n_5 ;
  wire \counter_stg2_reg[8]_i_1_n_6 ;
  wire \counter_stg2_reg[8]_i_1_n_7 ;
  wire counter_stg30;
  wire \counter_stg3[0]_i_3_n_0 ;
  wire \counter_stg3[0]_i_4_n_0 ;
  wire \counter_stg3[0]_i_5_n_0 ;
  wire [11:0]counter_stg3_reg;
  wire \counter_stg3_reg[0]_i_2_n_0 ;
  wire \counter_stg3_reg[0]_i_2_n_1 ;
  wire \counter_stg3_reg[0]_i_2_n_2 ;
  wire \counter_stg3_reg[0]_i_2_n_3 ;
  wire \counter_stg3_reg[0]_i_2_n_4 ;
  wire \counter_stg3_reg[0]_i_2_n_5 ;
  wire \counter_stg3_reg[0]_i_2_n_6 ;
  wire \counter_stg3_reg[0]_i_2_n_7 ;
  wire \counter_stg3_reg[4]_i_1_n_0 ;
  wire \counter_stg3_reg[4]_i_1_n_1 ;
  wire \counter_stg3_reg[4]_i_1_n_2 ;
  wire \counter_stg3_reg[4]_i_1_n_3 ;
  wire \counter_stg3_reg[4]_i_1_n_4 ;
  wire \counter_stg3_reg[4]_i_1_n_5 ;
  wire \counter_stg3_reg[4]_i_1_n_6 ;
  wire \counter_stg3_reg[4]_i_1_n_7 ;
  wire \counter_stg3_reg[8]_i_1_n_1 ;
  wire \counter_stg3_reg[8]_i_1_n_2 ;
  wire \counter_stg3_reg[8]_i_1_n_3 ;
  wire \counter_stg3_reg[8]_i_1_n_4 ;
  wire \counter_stg3_reg[8]_i_1_n_5 ;
  wire \counter_stg3_reg[8]_i_1_n_6 ;
  wire \counter_stg3_reg[8]_i_1_n_7 ;
  wire data_out;
  wire eqOp;
  wire independent_clock_bufg;
  wire [5:0]plusOp;
  wire reset;
  wire reset0;
  wire reset_i_2_n_0;
  wire reset_i_3_n_0;
  wire reset_i_4_n_0;
  wire reset_i_5_n_0;
  wire reset_i_6_n_0;
  wire [3:3]\NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \counter_stg1[0]_i_1 
       (.I0(counter_stg1_reg__0[0]),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \counter_stg1[1]_i_1 
       (.I0(counter_stg1_reg__0[0]),
        .I1(counter_stg1_reg__0[1]),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \counter_stg1[2]_i_1 
       (.I0(counter_stg1_reg__0[1]),
        .I1(counter_stg1_reg__0[0]),
        .I2(counter_stg1_reg__0[2]),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \counter_stg1[3]_i_1 
       (.I0(counter_stg1_reg__0[2]),
        .I1(counter_stg1_reg__0[0]),
        .I2(counter_stg1_reg__0[1]),
        .I3(counter_stg1_reg__0[3]),
        .O(plusOp[3]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \counter_stg1[4]_i_1 
       (.I0(counter_stg1_reg__0[3]),
        .I1(counter_stg1_reg__0[1]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[2]),
        .I4(counter_stg1_reg__0[4]),
        .O(plusOp[4]));
  LUT5 #(
    .INIT(32'hFFFF2000)) 
    \counter_stg1[5]_i_1 
       (.I0(reset_i_2_n_0),
        .I1(counter_stg3_reg[0]),
        .I2(reset_i_3_n_0),
        .I3(\counter_stg1[5]_i_3_n_0 ),
        .I4(data_out),
        .O(\counter_stg1[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \counter_stg1[5]_i_2 
       (.I0(counter_stg1_reg__0[4]),
        .I1(counter_stg1_reg__0[2]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[1]),
        .I4(counter_stg1_reg__0[3]),
        .I5(counter_stg1_reg),
        .O(plusOp[5]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \counter_stg1[5]_i_3 
       (.I0(counter_stg1_reg__0[3]),
        .I1(counter_stg1_reg__0[1]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[2]),
        .I4(counter_stg1_reg__0[4]),
        .O(\counter_stg1[5]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[0]),
        .Q(counter_stg1_reg__0[0]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[1]),
        .Q(counter_stg1_reg__0[1]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[2]),
        .Q(counter_stg1_reg__0[2]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[3]),
        .Q(counter_stg1_reg__0[3]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[4] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[4]),
        .Q(counter_stg1_reg__0[4]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[5] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[5]),
        .Q(counter_stg1_reg),
        .R(\counter_stg1[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter_stg2[0]_i_1 
       (.I0(counter_stg1_reg__0[4]),
        .I1(counter_stg1_reg__0[2]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[1]),
        .I4(counter_stg1_reg__0[3]),
        .I5(counter_stg1_reg),
        .O(eqOp));
  LUT1 #(
    .INIT(2'h1)) 
    \counter_stg2[0]_i_3 
       (.I0(counter_stg2_reg[0]),
        .O(\counter_stg2[0]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[0] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_7 ),
        .Q(counter_stg2_reg[0]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg2_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\counter_stg2_reg[0]_i_2_n_0 ,\counter_stg2_reg[0]_i_2_n_1 ,\counter_stg2_reg[0]_i_2_n_2 ,\counter_stg2_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\counter_stg2_reg[0]_i_2_n_4 ,\counter_stg2_reg[0]_i_2_n_5 ,\counter_stg2_reg[0]_i_2_n_6 ,\counter_stg2_reg[0]_i_2_n_7 }),
        .S({counter_stg2_reg[3:1],\counter_stg2[0]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[10] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_5 ),
        .Q(counter_stg2_reg[10]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[11] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_4 ),
        .Q(counter_stg2_reg[11]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[1] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_6 ),
        .Q(counter_stg2_reg[1]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[2] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_5 ),
        .Q(counter_stg2_reg[2]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[3] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_4 ),
        .Q(counter_stg2_reg[3]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[4] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_7 ),
        .Q(counter_stg2_reg[4]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg2_reg[4]_i_1 
       (.CI(\counter_stg2_reg[0]_i_2_n_0 ),
        .CO({\counter_stg2_reg[4]_i_1_n_0 ,\counter_stg2_reg[4]_i_1_n_1 ,\counter_stg2_reg[4]_i_1_n_2 ,\counter_stg2_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg2_reg[4]_i_1_n_4 ,\counter_stg2_reg[4]_i_1_n_5 ,\counter_stg2_reg[4]_i_1_n_6 ,\counter_stg2_reg[4]_i_1_n_7 }),
        .S(counter_stg2_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[5] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_6 ),
        .Q(counter_stg2_reg[5]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[6] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_5 ),
        .Q(counter_stg2_reg[6]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[7] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_4 ),
        .Q(counter_stg2_reg[7]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[8] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_7 ),
        .Q(counter_stg2_reg[8]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg2_reg[8]_i_1 
       (.CI(\counter_stg2_reg[4]_i_1_n_0 ),
        .CO({\NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED [3],\counter_stg2_reg[8]_i_1_n_1 ,\counter_stg2_reg[8]_i_1_n_2 ,\counter_stg2_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg2_reg[8]_i_1_n_4 ,\counter_stg2_reg[8]_i_1_n_5 ,\counter_stg2_reg[8]_i_1_n_6 ,\counter_stg2_reg[8]_i_1_n_7 }),
        .S(counter_stg2_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[9] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_6 ),
        .Q(counter_stg2_reg[9]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \counter_stg3[0]_i_1 
       (.I0(\counter_stg3[0]_i_3_n_0 ),
        .I1(\counter_stg3[0]_i_4_n_0 ),
        .I2(counter_stg2_reg[0]),
        .I3(\counter_stg1[5]_i_3_n_0 ),
        .O(counter_stg30));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter_stg3[0]_i_3 
       (.I0(counter_stg2_reg[3]),
        .I1(counter_stg2_reg[4]),
        .I2(counter_stg2_reg[1]),
        .I3(counter_stg2_reg[2]),
        .I4(counter_stg2_reg[6]),
        .I5(counter_stg2_reg[5]),
        .O(\counter_stg3[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter_stg3[0]_i_4 
       (.I0(counter_stg2_reg[9]),
        .I1(counter_stg2_reg[10]),
        .I2(counter_stg2_reg[7]),
        .I3(counter_stg2_reg[8]),
        .I4(counter_stg1_reg),
        .I5(counter_stg2_reg[11]),
        .O(\counter_stg3[0]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \counter_stg3[0]_i_5 
       (.I0(counter_stg3_reg[0]),
        .O(\counter_stg3[0]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[0] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_7 ),
        .Q(counter_stg3_reg[0]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg3_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\counter_stg3_reg[0]_i_2_n_0 ,\counter_stg3_reg[0]_i_2_n_1 ,\counter_stg3_reg[0]_i_2_n_2 ,\counter_stg3_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\counter_stg3_reg[0]_i_2_n_4 ,\counter_stg3_reg[0]_i_2_n_5 ,\counter_stg3_reg[0]_i_2_n_6 ,\counter_stg3_reg[0]_i_2_n_7 }),
        .S({counter_stg3_reg[3:1],\counter_stg3[0]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[10] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_5 ),
        .Q(counter_stg3_reg[10]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[11] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_4 ),
        .Q(counter_stg3_reg[11]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[1] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_6 ),
        .Q(counter_stg3_reg[1]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[2] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_5 ),
        .Q(counter_stg3_reg[2]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[3] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_4 ),
        .Q(counter_stg3_reg[3]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[4] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_7 ),
        .Q(counter_stg3_reg[4]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg3_reg[4]_i_1 
       (.CI(\counter_stg3_reg[0]_i_2_n_0 ),
        .CO({\counter_stg3_reg[4]_i_1_n_0 ,\counter_stg3_reg[4]_i_1_n_1 ,\counter_stg3_reg[4]_i_1_n_2 ,\counter_stg3_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg3_reg[4]_i_1_n_4 ,\counter_stg3_reg[4]_i_1_n_5 ,\counter_stg3_reg[4]_i_1_n_6 ,\counter_stg3_reg[4]_i_1_n_7 }),
        .S(counter_stg3_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[5] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_6 ),
        .Q(counter_stg3_reg[5]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[6] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_5 ),
        .Q(counter_stg3_reg[6]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[7] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_4 ),
        .Q(counter_stg3_reg[7]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[8] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_7 ),
        .Q(counter_stg3_reg[8]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg3_reg[8]_i_1 
       (.CI(\counter_stg3_reg[4]_i_1_n_0 ),
        .CO({\NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED [3],\counter_stg3_reg[8]_i_1_n_1 ,\counter_stg3_reg[8]_i_1_n_2 ,\counter_stg3_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg3_reg[8]_i_1_n_4 ,\counter_stg3_reg[8]_i_1_n_5 ,\counter_stg3_reg[8]_i_1_n_6 ,\counter_stg3_reg[8]_i_1_n_7 }),
        .S(counter_stg3_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[9] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_6 ),
        .Q(counter_stg3_reg[9]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h20)) 
    reset_i_1
       (.I0(reset_i_2_n_0),
        .I1(counter_stg3_reg[0]),
        .I2(reset_i_3_n_0),
        .O(reset0));
  LUT6 #(
    .INIT(64'h0000001000000000)) 
    reset_i_2
       (.I0(counter_stg3_reg[9]),
        .I1(counter_stg3_reg[10]),
        .I2(counter_stg3_reg[7]),
        .I3(counter_stg3_reg[8]),
        .I4(counter_stg2_reg[0]),
        .I5(counter_stg3_reg[11]),
        .O(reset_i_2_n_0));
  LUT3 #(
    .INIT(8'h80)) 
    reset_i_3
       (.I0(reset_i_4_n_0),
        .I1(reset_i_5_n_0),
        .I2(reset_i_6_n_0),
        .O(reset_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    reset_i_4
       (.I0(counter_stg2_reg[3]),
        .I1(counter_stg2_reg[4]),
        .I2(counter_stg2_reg[1]),
        .I3(counter_stg2_reg[2]),
        .I4(counter_stg2_reg[6]),
        .I5(counter_stg2_reg[5]),
        .O(reset_i_4_n_0));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    reset_i_5
       (.I0(counter_stg2_reg[10]),
        .I1(counter_stg2_reg[9]),
        .I2(counter_stg2_reg[8]),
        .I3(counter_stg2_reg[7]),
        .I4(counter_stg1_reg),
        .I5(counter_stg2_reg[11]),
        .O(reset_i_5_n_0));
  LUT6 #(
    .INIT(64'h0002000000000000)) 
    reset_i_6
       (.I0(counter_stg3_reg[4]),
        .I1(counter_stg3_reg[3]),
        .I2(counter_stg3_reg[1]),
        .I3(counter_stg3_reg[2]),
        .I4(counter_stg3_reg[6]),
        .I5(counter_stg3_reg[5]),
        .O(reset_i_6_n_0));
  FDRE reset_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset0),
        .Q(reset),
        .R(1'b0));
endmodule

module gig_ethernet_pcs_pma_0_resets
   (out,
    independent_clock_bufg,
    reset);
  output [0:0]out;
  input independent_clock_bufg;
  input reset;

  wire independent_clock_bufg;
  (* async_reg = "true" *) wire [3:0]pma_reset_pipe;
  wire reset;

  assign out[0] = pma_reset_pipe[3];
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset),
        .Q(pma_reset_pipe[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[0]),
        .PRE(reset),
        .Q(pma_reset_pipe[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[1]),
        .PRE(reset),
        .Q(pma_reset_pipe[2]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[2]),
        .PRE(reset),
        .Q(pma_reset_pipe[3]));
endmodule

(* EXAMPLE_SIMULATION = "0" *) (* downgradeipidentifiedwarnings = "yes" *) 
module gig_ethernet_pcs_pma_0_support
   (gtrefclk_p,
    gtrefclk_n,
    gtrefclk_out,
    gtrefclk_bufg_out,
    txp,
    txn,
    rxp,
    rxn,
    userclk_out,
    userclk2_out,
    rxuserclk_out,
    rxuserclk2_out,
    pma_reset_out,
    mmcm_locked_out,
    resetdone,
    independent_clock_bufg,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    configuration_vector,
    status_vector,
    reset,
    signal_detect,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out);
  input gtrefclk_p;
  input gtrefclk_n;
  output gtrefclk_out;
  output gtrefclk_bufg_out;
  output txp;
  output txn;
  input rxp;
  input rxn;
  output userclk_out;
  output userclk2_out;
  output rxuserclk_out;
  output rxuserclk2_out;
  output pma_reset_out;
  output mmcm_locked_out;
  output resetdone;
  input independent_clock_bufg;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  input [4:0]configuration_vector;
  output [15:0]status_vector;
  input reset;
  input signal_detect;
  output gt0_qplloutclk_out;
  output gt0_qplloutrefclk_out;

  wire \<const0> ;
  wire [4:0]configuration_vector;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg_out;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire independent_clock_bufg;
  wire mmcm_locked_out;
  wire mmcm_reset;
  wire pma_reset_out;
  wire reset;
  wire resetdone;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire rxuserclk2_out;
  wire signal_detect;
  wire [6:0]\^status_vector ;
  wire txn;
  wire txoutclk;
  wire txp;
  wire userclk2_out;
  wire userclk_out;

  assign rxuserclk_out = rxuserclk2_out;
  assign status_vector[15] = \<const0> ;
  assign status_vector[14] = \<const0> ;
  assign status_vector[13] = \<const0> ;
  assign status_vector[12] = \<const0> ;
  assign status_vector[11] = \<const0> ;
  assign status_vector[10] = \<const0> ;
  assign status_vector[9] = \<const0> ;
  assign status_vector[8] = \<const0> ;
  assign status_vector[7] = \<const0> ;
  assign status_vector[6:0] = \^status_vector [6:0];
  GND GND
       (.G(\<const0> ));
  gig_ethernet_pcs_pma_0_clocking core_clocking_i
       (.gtrefclk_bufg(gtrefclk_bufg_out),
        .gtrefclk_n(gtrefclk_n),
        .gtrefclk_out(gtrefclk_out),
        .gtrefclk_p(gtrefclk_p),
        .mmcm_locked(mmcm_locked_out),
        .mmcm_reset(mmcm_reset),
        .rxoutclk(rxoutclk),
        .rxuserclk2_out(rxuserclk2_out),
        .txoutclk(txoutclk),
        .userclk(userclk_out),
        .userclk2(userclk2_out));
  gig_ethernet_pcs_pma_0_gt_common core_gt_common_i
       (.gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_out(gtrefclk_out),
        .independent_clock_bufg(independent_clock_bufg),
        .out(pma_reset_out));
  gig_ethernet_pcs_pma_0_resets core_resets_i
       (.independent_clock_bufg(independent_clock_bufg),
        .out(pma_reset_out),
        .reset(reset));
  gig_ethernet_pcs_pma_0_block pcs_pma_block_i
       (.CLK(userclk2_out),
        .configuration_vector(configuration_vector[3:1]),
        .data_in(mmcm_locked_out),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg_out),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(userclk_out),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(pma_reset_out),
        .resetdone(resetdone),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .signal_detect(signal_detect),
        .status_vector(\^status_vector ),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module gig_ethernet_pcs_pma_0_sync_block
   (data_out,
    data_in,
    CLK);
  output data_out;
  input data_in;
  input CLK;

  wire CLK;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_0
   (resetdone,
    resetdone_0,
    data_in,
    CLK);
  output resetdone;
  input resetdone_0;
  input data_in;
  input CLK;

  wire CLK;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire resetdone;
  wire resetdone_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    resetdone_INST_0
       (.I0(data_out),
        .I1(resetdone_0),
        .O(resetdone));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_10
   (data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_11
   (\FSM_sequential_rx_state_reg[1] ,
    Q,
    rxresetdone_s3,
    data_sync_reg1_0,
    independent_clock_bufg);
  output \FSM_sequential_rx_state_reg[1] ;
  input [2:0]Q;
  input rxresetdone_s3;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire \FSM_sequential_rx_state_reg[1] ;
  wire [2:0]Q;
  wire cplllock_sync;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;
  wire rxresetdone_s3;

  LUT5 #(
    .INIT(32'h008F0080)) 
    \FSM_sequential_rx_state[3]_i_5 
       (.I0(Q[0]),
        .I1(rxresetdone_s3),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(cplllock_sync),
        .O(\FSM_sequential_rx_state_reg[1] ));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(cplllock_sync),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_12
   (\FSM_sequential_rx_state_reg[1] ,
    rx_fsm_reset_done_int_reg,
    D,
    E,
    reset_time_out_reg,
    reset_time_out_reg_0,
    Q,
    reset_time_out_reg_1,
    reset_time_out_reg_2,
    data_in,
    \FSM_sequential_rx_state_reg[1]_0 ,
    rx_fsm_reset_done_int_reg_0,
    rx_fsm_reset_done_int_reg_1,
    \FSM_sequential_rx_state_reg[0] ,
    \FSM_sequential_rx_state_reg[0]_0 ,
    \FSM_sequential_rx_state_reg[0]_1 ,
    mmcm_lock_reclocked,
    \FSM_sequential_rx_state_reg[0]_2 ,
    time_out_wait_bypass_s3,
    \FSM_sequential_rx_state_reg[3] ,
    \FSM_sequential_rx_state_reg[0]_3 ,
    rx_fsm_reset_done_int_reg_2,
    rx_fsm_reset_done_int_reg_3,
    data_out,
    independent_clock_bufg);
  output \FSM_sequential_rx_state_reg[1] ;
  output rx_fsm_reset_done_int_reg;
  output [2:0]D;
  output [0:0]E;
  input reset_time_out_reg;
  input reset_time_out_reg_0;
  input [3:0]Q;
  input reset_time_out_reg_1;
  input reset_time_out_reg_2;
  input data_in;
  input \FSM_sequential_rx_state_reg[1]_0 ;
  input rx_fsm_reset_done_int_reg_0;
  input rx_fsm_reset_done_int_reg_1;
  input \FSM_sequential_rx_state_reg[0] ;
  input \FSM_sequential_rx_state_reg[0]_0 ;
  input \FSM_sequential_rx_state_reg[0]_1 ;
  input mmcm_lock_reclocked;
  input \FSM_sequential_rx_state_reg[0]_2 ;
  input time_out_wait_bypass_s3;
  input \FSM_sequential_rx_state_reg[3] ;
  input \FSM_sequential_rx_state_reg[0]_3 ;
  input rx_fsm_reset_done_int_reg_2;
  input rx_fsm_reset_done_int_reg_3;
  input data_out;
  input independent_clock_bufg;

  wire [2:0]D;
  wire [0:0]E;
  wire \FSM_sequential_rx_state[0]_i_3_n_0 ;
  wire \FSM_sequential_rx_state[1]_i_2_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_4_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_6_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_8_n_0 ;
  wire \FSM_sequential_rx_state_reg[0] ;
  wire \FSM_sequential_rx_state_reg[0]_0 ;
  wire \FSM_sequential_rx_state_reg[0]_1 ;
  wire \FSM_sequential_rx_state_reg[0]_2 ;
  wire \FSM_sequential_rx_state_reg[0]_3 ;
  wire \FSM_sequential_rx_state_reg[1] ;
  wire \FSM_sequential_rx_state_reg[1]_0 ;
  wire \FSM_sequential_rx_state_reg[3] ;
  wire [3:0]Q;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_valid_sync;
  wire independent_clock_bufg;
  wire mmcm_lock_reclocked;
  wire reset_time_out_i_2_n_0;
  wire reset_time_out_reg;
  wire reset_time_out_reg_0;
  wire reset_time_out_reg_1;
  wire reset_time_out_reg_2;
  wire rx_fsm_reset_done_int;
  wire rx_fsm_reset_done_int_i_3_n_0;
  wire rx_fsm_reset_done_int_i_4_n_0;
  wire rx_fsm_reset_done_int_reg;
  wire rx_fsm_reset_done_int_reg_0;
  wire rx_fsm_reset_done_int_reg_1;
  wire rx_fsm_reset_done_int_reg_2;
  wire rx_fsm_reset_done_int_reg_3;
  wire time_out_wait_bypass_s3;

  LUT5 #(
    .INIT(32'hFFEFEFEF)) 
    \FSM_sequential_rx_state[0]_i_1 
       (.I0(\FSM_sequential_rx_state_reg[0]_2 ),
        .I1(\FSM_sequential_rx_state[0]_i_3_n_0 ),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[3]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT4 #(
    .INIT(16'h0200)) 
    \FSM_sequential_rx_state[0]_i_3 
       (.I0(Q[3]),
        .I1(reset_time_out_reg_2),
        .I2(data_valid_sync),
        .I3(rx_fsm_reset_done_int_reg_1),
        .O(\FSM_sequential_rx_state[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF24200400)) 
    \FSM_sequential_rx_state[1]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(\FSM_sequential_rx_state[1]_i_2_n_0 ),
        .I5(\FSM_sequential_rx_state_reg[1]_0 ),
        .O(D[1]));
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_rx_state[1]_i_2 
       (.I0(data_valid_sync),
        .I1(rx_fsm_reset_done_int_reg_1),
        .O(\FSM_sequential_rx_state[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEEE)) 
    \FSM_sequential_rx_state[3]_i_1 
       (.I0(\FSM_sequential_rx_state_reg[0] ),
        .I1(\FSM_sequential_rx_state[3]_i_4_n_0 ),
        .I2(Q[0]),
        .I3(reset_time_out_reg),
        .I4(\FSM_sequential_rx_state[3]_i_6_n_0 ),
        .I5(\FSM_sequential_rx_state_reg[0]_0 ),
        .O(E));
  LUT6 #(
    .INIT(64'hFFFFFFFFCCC0C4C4)) 
    \FSM_sequential_rx_state[3]_i_2 
       (.I0(time_out_wait_bypass_s3),
        .I1(Q[3]),
        .I2(Q[1]),
        .I3(\FSM_sequential_rx_state[3]_i_8_n_0 ),
        .I4(Q[0]),
        .I5(\FSM_sequential_rx_state_reg[3] ),
        .O(D[2]));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAEFEA)) 
    \FSM_sequential_rx_state[3]_i_4 
       (.I0(\FSM_sequential_rx_state[0]_i_3_n_0 ),
        .I1(\FSM_sequential_rx_state_reg[0]_1 ),
        .I2(Q[2]),
        .I3(\FSM_sequential_rx_state_reg[0]_3 ),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\FSM_sequential_rx_state[3]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h0CE20CCC)) 
    \FSM_sequential_rx_state[3]_i_6 
       (.I0(mmcm_lock_reclocked),
        .I1(Q[3]),
        .I2(data_valid_sync),
        .I3(Q[1]),
        .I4(Q[0]),
        .O(\FSM_sequential_rx_state[3]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hFD)) 
    \FSM_sequential_rx_state[3]_i_8 
       (.I0(rx_fsm_reset_done_int_reg_1),
        .I1(data_valid_sync),
        .I2(reset_time_out_reg_2),
        .O(\FSM_sequential_rx_state[3]_i_8_n_0 ));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_out),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_valid_sync),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hEEEFFFFFEEEF0000)) 
    reset_time_out_i_1__0
       (.I0(reset_time_out_i_2_n_0),
        .I1(reset_time_out_reg),
        .I2(reset_time_out_reg_0),
        .I3(Q[1]),
        .I4(reset_time_out_reg_1),
        .I5(reset_time_out_reg_2),
        .O(\FSM_sequential_rx_state_reg[1] ));
  LUT6 #(
    .INIT(64'h0FF30E0E0FF30202)) 
    reset_time_out_i_2
       (.I0(\FSM_sequential_rx_state_reg[0]_1 ),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(data_valid_sync),
        .I4(Q[3]),
        .I5(mmcm_lock_reclocked),
        .O(reset_time_out_i_2_n_0));
  LUT4 #(
    .INIT(16'hABA8)) 
    rx_fsm_reset_done_int_i_1
       (.I0(rx_fsm_reset_done_int),
        .I1(rx_fsm_reset_done_int_i_3_n_0),
        .I2(rx_fsm_reset_done_int_i_4_n_0),
        .I3(data_in),
        .O(rx_fsm_reset_done_int_reg));
  LUT5 #(
    .INIT(32'h00040000)) 
    rx_fsm_reset_done_int_i_2
       (.I0(Q[0]),
        .I1(data_valid_sync),
        .I2(Q[2]),
        .I3(reset_time_out_reg_2),
        .I4(rx_fsm_reset_done_int_reg_2),
        .O(rx_fsm_reset_done_int));
  LUT6 #(
    .INIT(64'h0400040004040400)) 
    rx_fsm_reset_done_int_i_3
       (.I0(rx_fsm_reset_done_int_reg_0),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(data_valid_sync),
        .I4(rx_fsm_reset_done_int_reg_1),
        .I5(reset_time_out_reg_2),
        .O(rx_fsm_reset_done_int_i_3_n_0));
  LUT6 #(
    .INIT(64'h0008000808080008)) 
    rx_fsm_reset_done_int_i_4
       (.I0(rx_fsm_reset_done_int_reg_3),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(data_valid_sync),
        .I4(rx_fsm_reset_done_int_reg_2),
        .I5(reset_time_out_reg_2),
        .O(rx_fsm_reset_done_int_i_4_n_0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_13
   (SR,
    data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output [0:0]SR;
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire [0:0]SR;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[7]_i_1__0 
       (.I0(data_out),
        .O(SR));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_14
   (data_out,
    data_in,
    data_sync_reg1_0);
  output data_out;
  input data_in;
  input data_sync_reg1_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_15
   (data_out,
    data_in,
    independent_clock_bufg);
  output data_out;
  input data_in;
  input independent_clock_bufg;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_16
   (data_out,
    data_in,
    data_sync_reg6_0);
  output data_out;
  input data_in;
  input data_sync_reg6_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg6_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_3
   (data_out,
    status_vector,
    independent_clock_bufg);
  output data_out;
  input [0:0]status_vector;
  input independent_clock_bufg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire independent_clock_bufg;
  wire [0:0]status_vector;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(status_vector),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_4
   (data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_5
   (reset_time_out_reg,
    E,
    reset_time_out_reg_0,
    reset_time_out,
    \FSM_sequential_tx_state_reg[0] ,
    \FSM_sequential_tx_state_reg[0]_0 ,
    \FSM_sequential_tx_state_reg[0]_1 ,
    \FSM_sequential_tx_state_reg[0]_2 ,
    \FSM_sequential_tx_state_reg[0]_3 ,
    Q,
    reset_time_out_reg_1,
    mmcm_lock_reclocked,
    \FSM_sequential_tx_state_reg[0]_4 ,
    \FSM_sequential_tx_state_reg[0]_5 ,
    \FSM_sequential_tx_state_reg[0]_6 ,
    data_sync_reg1_0,
    independent_clock_bufg);
  output reset_time_out_reg;
  output [0:0]E;
  input reset_time_out_reg_0;
  input reset_time_out;
  input \FSM_sequential_tx_state_reg[0] ;
  input \FSM_sequential_tx_state_reg[0]_0 ;
  input \FSM_sequential_tx_state_reg[0]_1 ;
  input \FSM_sequential_tx_state_reg[0]_2 ;
  input \FSM_sequential_tx_state_reg[0]_3 ;
  input [3:0]Q;
  input reset_time_out_reg_1;
  input mmcm_lock_reclocked;
  input \FSM_sequential_tx_state_reg[0]_4 ;
  input \FSM_sequential_tx_state_reg[0]_5 ;
  input \FSM_sequential_tx_state_reg[0]_6 ;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire [0:0]E;
  wire \FSM_sequential_tx_state[3]_i_5_n_0 ;
  wire \FSM_sequential_tx_state_reg[0] ;
  wire \FSM_sequential_tx_state_reg[0]_0 ;
  wire \FSM_sequential_tx_state_reg[0]_1 ;
  wire \FSM_sequential_tx_state_reg[0]_2 ;
  wire \FSM_sequential_tx_state_reg[0]_3 ;
  wire \FSM_sequential_tx_state_reg[0]_4 ;
  wire \FSM_sequential_tx_state_reg[0]_5 ;
  wire \FSM_sequential_tx_state_reg[0]_6 ;
  wire [3:0]Q;
  wire cplllock_sync;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;
  wire mmcm_lock_reclocked;
  wire reset_time_out;
  wire reset_time_out_i_3__0_n_0;
  wire reset_time_out_i_4__0_n_0;
  wire reset_time_out_reg;
  wire reset_time_out_reg_0;
  wire reset_time_out_reg_1;

  LUT6 #(
    .INIT(64'hFFFFFFFEFFFEFFFE)) 
    \FSM_sequential_tx_state[3]_i_1 
       (.I0(\FSM_sequential_tx_state_reg[0] ),
        .I1(\FSM_sequential_tx_state_reg[0]_0 ),
        .I2(\FSM_sequential_tx_state[3]_i_5_n_0 ),
        .I3(\FSM_sequential_tx_state_reg[0]_1 ),
        .I4(\FSM_sequential_tx_state_reg[0]_2 ),
        .I5(\FSM_sequential_tx_state_reg[0]_3 ),
        .O(E));
  LUT6 #(
    .INIT(64'h0000000000F00008)) 
    \FSM_sequential_tx_state[3]_i_5 
       (.I0(\FSM_sequential_tx_state_reg[0]_4 ),
        .I1(\FSM_sequential_tx_state_reg[0]_5 ),
        .I2(cplllock_sync),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(\FSM_sequential_tx_state_reg[0]_6 ),
        .O(\FSM_sequential_tx_state[3]_i_5_n_0 ));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(cplllock_sync),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hEFE0)) 
    reset_time_out_i_1
       (.I0(reset_time_out_reg_0),
        .I1(reset_time_out_i_3__0_n_0),
        .I2(reset_time_out_i_4__0_n_0),
        .I3(reset_time_out),
        .O(reset_time_out_reg));
  LUT6 #(
    .INIT(64'h020002000F000200)) 
    reset_time_out_i_3__0
       (.I0(cplllock_sync),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(Q[0]),
        .I4(mmcm_lock_reclocked),
        .I5(Q[1]),
        .O(reset_time_out_i_3__0_n_0));
  LUT6 #(
    .INIT(64'h0505FF040505F504)) 
    reset_time_out_i_4__0
       (.I0(Q[1]),
        .I1(reset_time_out_reg_1),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(Q[3]),
        .I5(cplllock_sync),
        .O(reset_time_out_i_4__0_n_0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_6
   (SR,
    data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output [0:0]SR;
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire [0:0]SR;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[7]_i_1 
       (.I0(data_out),
        .O(SR));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_7
   (data_out,
    data_in,
    data_sync_reg6_0);
  output data_out;
  input data_in;
  input data_sync_reg6_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg6_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_8
   (data_out,
    data_in,
    independent_clock_bufg);
  output data_out;
  input data_in;
  input independent_clock_bufg;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_9
   (data_out,
    data_in,
    data_sync_reg1_0);
  output data_out;
  input data_in;
  input data_sync_reg1_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

module gig_ethernet_pcs_pma_0_transceiver
   (txn,
    txp,
    rxoutclk,
    txoutclk,
    rxchariscomma,
    rxcharisk,
    rxdisperr,
    rxnotintable,
    rxbufstatus,
    txbuferr,
    mmcm_reset,
    data_in,
    rx_fsm_reset_done_int_reg,
    Q,
    \rxdata_reg[7]_0 ,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    gtxe2_i,
    SR,
    CLK,
    powerdown,
    reset_sync5,
    D,
    txchardispval_reg_reg_0,
    txcharisk_reg_reg_0,
    out,
    status_vector,
    enablealign,
    data_sync_reg1,
    \txdata_reg_reg[7]_0 );
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output [0:0]rxchariscomma;
  output [0:0]rxcharisk;
  output [0:0]rxdisperr;
  output [0:0]rxnotintable;
  output [0:0]rxbufstatus;
  output txbuferr;
  output mmcm_reset;
  output data_in;
  output rx_fsm_reset_done_int_reg;
  output [1:0]Q;
  output [7:0]\rxdata_reg[7]_0 ;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input gtxe2_i;
  input [0:0]SR;
  input CLK;
  input powerdown;
  input [0:0]reset_sync5;
  input [0:0]D;
  input [0:0]txchardispval_reg_reg_0;
  input [0:0]txcharisk_reg_reg_0;
  input [0:0]out;
  input [0:0]status_vector;
  input enablealign;
  input data_sync_reg1;
  input [7:0]\txdata_reg_reg[7]_0 ;

  wire CLK;
  wire [0:0]D;
  wire [1:0]Q;
  wire [0:0]SR;
  wire data_in;
  wire data_sync_reg1;
  wire data_valid_reg2;
  wire enablealign;
  wire encommaalign_int;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtwizard_inst_n_6;
  wire gtwizard_inst_n_7;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire mmcm_reset;
  wire [0:0]out;
  wire p_0_in;
  wire powerdown;
  wire reset;
  wire [0:0]reset_sync5;
  wire rx_fsm_reset_done_int_reg;
  wire [0:0]rxbufstatus;
  wire [0:0]rxchariscomma;
  wire [1:0]rxchariscomma_double;
  wire rxchariscomma_i_1_n_0;
  wire [1:0]rxchariscomma_int;
  wire [1:0]rxchariscomma_reg__0;
  wire [0:0]rxcharisk;
  wire [1:0]rxcharisk_double;
  wire rxcharisk_i_1_n_0;
  wire [1:0]rxcharisk_int;
  wire [1:0]rxcharisk_reg__0;
  wire [1:0]rxclkcorcnt_double;
  wire [1:0]rxclkcorcnt_int;
  wire [1:0]rxclkcorcnt_reg;
  wire \rxdata[0]_i_1_n_0 ;
  wire \rxdata[1]_i_1_n_0 ;
  wire \rxdata[2]_i_1_n_0 ;
  wire \rxdata[3]_i_1_n_0 ;
  wire \rxdata[4]_i_1_n_0 ;
  wire \rxdata[5]_i_1_n_0 ;
  wire \rxdata[6]_i_1_n_0 ;
  wire \rxdata[7]_i_1_n_0 ;
  wire [15:0]rxdata_double;
  wire [15:0]rxdata_int;
  wire [15:0]rxdata_reg;
  wire [7:0]\rxdata_reg[7]_0 ;
  wire [0:0]rxdisperr;
  wire [1:0]rxdisperr_double;
  wire rxdisperr_i_1_n_0;
  wire [1:0]rxdisperr_int;
  wire [1:0]rxdisperr_reg__0;
  wire rxn;
  wire [0:0]rxnotintable;
  wire [1:0]rxnotintable_double;
  wire rxnotintable_i_1_n_0;
  wire [1:0]rxnotintable_int;
  wire [1:0]rxnotintable_reg__0;
  wire rxoutclk;
  wire rxp;
  wire rxpowerdown;
  wire rxpowerdown_double;
  wire rxpowerdown_reg__0;
  wire rxreset_int;
  wire [0:0]status_vector;
  wire toggle;
  wire toggle_i_1_n_0;
  wire toggle_rx;
  wire toggle_rx_i_1_n_0;
  wire txbuferr;
  wire [1:1]txbufstatus_reg;
  wire [1:0]txchardispmode_double;
  wire [1:0]txchardispmode_int;
  wire txchardispmode_reg;
  wire [1:0]txchardispval_double;
  wire [1:0]txchardispval_int;
  wire txchardispval_reg;
  wire [0:0]txchardispval_reg_reg_0;
  wire [1:0]txcharisk_double;
  wire [1:0]txcharisk_int;
  wire txcharisk_reg;
  wire [0:0]txcharisk_reg_reg_0;
  wire [15:0]txdata_double;
  wire [15:0]txdata_int;
  wire [7:0]txdata_reg;
  wire [7:0]\txdata_reg_reg[7]_0 ;
  wire txn;
  wire txoutclk;
  wire txp;
  wire txpowerdown;
  wire txpowerdown_double;
  wire txpowerdown_reg__0;
  wire txreset_int;

  gig_ethernet_pcs_pma_0_GTWIZARD gtwizard_inst
       (.D(rxclkcorcnt_int),
        .Q(txdata_int),
        .RXBUFSTATUS(gtwizard_inst_n_7),
        .RXPD(rxpowerdown),
        .TXBUFSTATUS(gtwizard_inst_n_6),
        .TXPD(txpowerdown),
        .data_in(data_in),
        .data_out(data_valid_reg2),
        .data_sync_reg1(data_sync_reg1),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(rxdata_int),
        .gtxe2_i_0(rxchariscomma_int),
        .gtxe2_i_1(rxcharisk_int),
        .gtxe2_i_2(rxdisperr_int),
        .gtxe2_i_3(rxnotintable_int),
        .gtxe2_i_4(gtxe2_i),
        .gtxe2_i_5(txchardispmode_int),
        .gtxe2_i_6(txchardispval_int),
        .gtxe2_i_7(txcharisk_int),
        .gtxe2_i_8(rxreset_int),
        .gtxe2_i_9(txreset_int),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out),
        .reset(reset),
        .reset_out(encommaalign_int),
        .rx_fsm_reset_done_int_reg(rx_fsm_reset_done_int_reg),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
  gig_ethernet_pcs_pma_0_reset_sync reclock_encommaalign
       (.CLK(CLK),
        .enablealign(enablealign),
        .reset_out(encommaalign_int));
  gig_ethernet_pcs_pma_0_reset_sync_1 reclock_rxreset
       (.SR(SR),
        .independent_clock_bufg(independent_clock_bufg),
        .reset_out(rxreset_int));
  gig_ethernet_pcs_pma_0_reset_sync_2 reclock_txreset
       (.independent_clock_bufg(independent_clock_bufg),
        .reset_out(txreset_int),
        .reset_sync5_0(reset_sync5));
  gig_ethernet_pcs_pma_0_reset_wtd_timer reset_wtd_timer
       (.data_out(data_valid_reg2),
        .independent_clock_bufg(independent_clock_bufg),
        .reset(reset));
  FDRE rxbuferr_reg
       (.C(CLK),
        .CE(1'b1),
        .D(p_0_in),
        .Q(rxbufstatus),
        .R(1'b0));
  FDRE \rxbufstatus_reg_reg[2] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(gtwizard_inst_n_7),
        .Q(p_0_in),
        .R(1'b0));
  FDRE \rxchariscomma_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxchariscomma_reg__0[0]),
        .Q(rxchariscomma_double[0]),
        .R(SR));
  FDRE \rxchariscomma_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxchariscomma_reg__0[1]),
        .Q(rxchariscomma_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxchariscomma_i_1
       (.I0(rxchariscomma_double[1]),
        .I1(toggle_rx),
        .I2(rxchariscomma_double[0]),
        .O(rxchariscomma_i_1_n_0));
  FDRE rxchariscomma_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxchariscomma_i_1_n_0),
        .Q(rxchariscomma),
        .R(SR));
  FDRE \rxchariscomma_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxchariscomma_int[0]),
        .Q(rxchariscomma_reg__0[0]),
        .R(1'b0));
  FDRE \rxchariscomma_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxchariscomma_int[1]),
        .Q(rxchariscomma_reg__0[1]),
        .R(1'b0));
  FDRE \rxcharisk_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxcharisk_reg__0[0]),
        .Q(rxcharisk_double[0]),
        .R(SR));
  FDRE \rxcharisk_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxcharisk_reg__0[1]),
        .Q(rxcharisk_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxcharisk_i_1
       (.I0(rxcharisk_double[1]),
        .I1(toggle_rx),
        .I2(rxcharisk_double[0]),
        .O(rxcharisk_i_1_n_0));
  FDRE rxcharisk_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxcharisk_i_1_n_0),
        .Q(rxcharisk),
        .R(SR));
  FDRE \rxcharisk_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxcharisk_int[0]),
        .Q(rxcharisk_reg__0[0]),
        .R(1'b0));
  FDRE \rxcharisk_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxcharisk_int[1]),
        .Q(rxcharisk_reg__0[1]),
        .R(1'b0));
  FDRE \rxclkcorcnt_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxclkcorcnt_reg[0]),
        .Q(rxclkcorcnt_double[0]),
        .R(SR));
  FDRE \rxclkcorcnt_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxclkcorcnt_reg[1]),
        .Q(rxclkcorcnt_double[1]),
        .R(SR));
  FDRE \rxclkcorcnt_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxclkcorcnt_double[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE \rxclkcorcnt_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxclkcorcnt_double[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE \rxclkcorcnt_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxclkcorcnt_int[0]),
        .Q(rxclkcorcnt_reg[0]),
        .R(1'b0));
  FDRE \rxclkcorcnt_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxclkcorcnt_int[1]),
        .Q(rxclkcorcnt_reg[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[0]_i_1 
       (.I0(rxdata_double[8]),
        .I1(toggle_rx),
        .I2(rxdata_double[0]),
        .O(\rxdata[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[1]_i_1 
       (.I0(rxdata_double[9]),
        .I1(toggle_rx),
        .I2(rxdata_double[1]),
        .O(\rxdata[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[2]_i_1 
       (.I0(rxdata_double[10]),
        .I1(toggle_rx),
        .I2(rxdata_double[2]),
        .O(\rxdata[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[3]_i_1 
       (.I0(rxdata_double[11]),
        .I1(toggle_rx),
        .I2(rxdata_double[3]),
        .O(\rxdata[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[4]_i_1 
       (.I0(rxdata_double[12]),
        .I1(toggle_rx),
        .I2(rxdata_double[4]),
        .O(\rxdata[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[5]_i_1 
       (.I0(rxdata_double[13]),
        .I1(toggle_rx),
        .I2(rxdata_double[5]),
        .O(\rxdata[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[6]_i_1 
       (.I0(rxdata_double[14]),
        .I1(toggle_rx),
        .I2(rxdata_double[6]),
        .O(\rxdata[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[7]_i_1 
       (.I0(rxdata_double[15]),
        .I1(toggle_rx),
        .I2(rxdata_double[7]),
        .O(\rxdata[7]_i_1_n_0 ));
  FDRE \rxdata_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[0]),
        .Q(rxdata_double[0]),
        .R(SR));
  FDRE \rxdata_double_reg[10] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[10]),
        .Q(rxdata_double[10]),
        .R(SR));
  FDRE \rxdata_double_reg[11] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[11]),
        .Q(rxdata_double[11]),
        .R(SR));
  FDRE \rxdata_double_reg[12] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[12]),
        .Q(rxdata_double[12]),
        .R(SR));
  FDRE \rxdata_double_reg[13] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[13]),
        .Q(rxdata_double[13]),
        .R(SR));
  FDRE \rxdata_double_reg[14] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[14]),
        .Q(rxdata_double[14]),
        .R(SR));
  FDRE \rxdata_double_reg[15] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[15]),
        .Q(rxdata_double[15]),
        .R(SR));
  FDRE \rxdata_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[1]),
        .Q(rxdata_double[1]),
        .R(SR));
  FDRE \rxdata_double_reg[2] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[2]),
        .Q(rxdata_double[2]),
        .R(SR));
  FDRE \rxdata_double_reg[3] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[3]),
        .Q(rxdata_double[3]),
        .R(SR));
  FDRE \rxdata_double_reg[4] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[4]),
        .Q(rxdata_double[4]),
        .R(SR));
  FDRE \rxdata_double_reg[5] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[5]),
        .Q(rxdata_double[5]),
        .R(SR));
  FDRE \rxdata_double_reg[6] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[6]),
        .Q(rxdata_double[6]),
        .R(SR));
  FDRE \rxdata_double_reg[7] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[7]),
        .Q(rxdata_double[7]),
        .R(SR));
  FDRE \rxdata_double_reg[8] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[8]),
        .Q(rxdata_double[8]),
        .R(SR));
  FDRE \rxdata_double_reg[9] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[9]),
        .Q(rxdata_double[9]),
        .R(SR));
  FDRE \rxdata_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[0]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [0]),
        .R(SR));
  FDRE \rxdata_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[1]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [1]),
        .R(SR));
  FDRE \rxdata_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[2]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [2]),
        .R(SR));
  FDRE \rxdata_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[3]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [3]),
        .R(SR));
  FDRE \rxdata_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[4]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [4]),
        .R(SR));
  FDRE \rxdata_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[5]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [5]),
        .R(SR));
  FDRE \rxdata_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[6]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [6]),
        .R(SR));
  FDRE \rxdata_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[7]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [7]),
        .R(SR));
  FDRE \rxdata_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[0]),
        .Q(rxdata_reg[0]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[10] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[10]),
        .Q(rxdata_reg[10]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[11] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[11]),
        .Q(rxdata_reg[11]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[12] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[12]),
        .Q(rxdata_reg[12]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[13] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[13]),
        .Q(rxdata_reg[13]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[14] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[14]),
        .Q(rxdata_reg[14]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[15] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[15]),
        .Q(rxdata_reg[15]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[1]),
        .Q(rxdata_reg[1]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[2] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[2]),
        .Q(rxdata_reg[2]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[3] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[3]),
        .Q(rxdata_reg[3]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[4] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[4]),
        .Q(rxdata_reg[4]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[5] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[5]),
        .Q(rxdata_reg[5]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[6] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[6]),
        .Q(rxdata_reg[6]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[7] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[7]),
        .Q(rxdata_reg[7]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[8] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[8]),
        .Q(rxdata_reg[8]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[9] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[9]),
        .Q(rxdata_reg[9]),
        .R(1'b0));
  FDRE \rxdisperr_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdisperr_reg__0[0]),
        .Q(rxdisperr_double[0]),
        .R(SR));
  FDRE \rxdisperr_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdisperr_reg__0[1]),
        .Q(rxdisperr_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxdisperr_i_1
       (.I0(rxdisperr_double[1]),
        .I1(toggle_rx),
        .I2(rxdisperr_double[0]),
        .O(rxdisperr_i_1_n_0));
  FDRE rxdisperr_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxdisperr_i_1_n_0),
        .Q(rxdisperr),
        .R(SR));
  FDRE \rxdisperr_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdisperr_int[0]),
        .Q(rxdisperr_reg__0[0]),
        .R(1'b0));
  FDRE \rxdisperr_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdisperr_int[1]),
        .Q(rxdisperr_reg__0[1]),
        .R(1'b0));
  FDRE \rxnotintable_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxnotintable_reg__0[0]),
        .Q(rxnotintable_double[0]),
        .R(SR));
  FDRE \rxnotintable_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxnotintable_reg__0[1]),
        .Q(rxnotintable_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxnotintable_i_1
       (.I0(rxnotintable_double[1]),
        .I1(toggle_rx),
        .I2(rxnotintable_double[0]),
        .O(rxnotintable_i_1_n_0));
  FDRE rxnotintable_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxnotintable_i_1_n_0),
        .Q(rxnotintable),
        .R(SR));
  FDRE \rxnotintable_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxnotintable_int[0]),
        .Q(rxnotintable_reg__0[0]),
        .R(1'b0));
  FDRE \rxnotintable_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxnotintable_int[1]),
        .Q(rxnotintable_reg__0[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_double_reg
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxpowerdown_reg__0),
        .Q(rxpowerdown_double),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_reg
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxpowerdown_double),
        .Q(rxpowerdown),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(powerdown),
        .Q(rxpowerdown_reg__0),
        .R(SR));
  gig_ethernet_pcs_pma_0_sync_block_3 sync_block_data_valid
       (.data_out(data_valid_reg2),
        .independent_clock_bufg(independent_clock_bufg),
        .status_vector(status_vector));
  LUT1 #(
    .INIT(2'h1)) 
    toggle_i_1
       (.I0(toggle),
        .O(toggle_i_1_n_0));
  FDRE toggle_reg
       (.C(CLK),
        .CE(1'b1),
        .D(toggle_i_1_n_0),
        .Q(toggle),
        .R(reset_sync5));
  LUT1 #(
    .INIT(2'h1)) 
    toggle_rx_i_1
       (.I0(toggle_rx),
        .O(toggle_rx_i_1_n_0));
  FDRE toggle_rx_reg
       (.C(CLK),
        .CE(1'b1),
        .D(toggle_rx_i_1_n_0),
        .Q(toggle_rx),
        .R(SR));
  FDRE txbuferr_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txbufstatus_reg),
        .Q(txbuferr),
        .R(1'b0));
  FDRE \txbufstatus_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(gtwizard_inst_n_6),
        .Q(txbufstatus_reg),
        .R(1'b0));
  FDRE \txchardispmode_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txchardispmode_reg),
        .Q(txchardispmode_double[0]),
        .R(reset_sync5));
  FDRE \txchardispmode_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(D),
        .Q(txchardispmode_double[1]),
        .R(reset_sync5));
  FDRE \txchardispmode_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispmode_double[0]),
        .Q(txchardispmode_int[0]),
        .R(1'b0));
  FDRE \txchardispmode_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispmode_double[1]),
        .Q(txchardispmode_int[1]),
        .R(1'b0));
  FDRE txchardispmode_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(D),
        .Q(txchardispmode_reg),
        .R(reset_sync5));
  FDRE \txchardispval_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txchardispval_reg),
        .Q(txchardispval_double[0]),
        .R(reset_sync5));
  FDRE \txchardispval_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txchardispval_reg_reg_0),
        .Q(txchardispval_double[1]),
        .R(reset_sync5));
  FDRE \txchardispval_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispval_double[0]),
        .Q(txchardispval_int[0]),
        .R(1'b0));
  FDRE \txchardispval_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispval_double[1]),
        .Q(txchardispval_int[1]),
        .R(1'b0));
  FDRE txchardispval_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txchardispval_reg_reg_0),
        .Q(txchardispval_reg),
        .R(reset_sync5));
  FDRE \txcharisk_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txcharisk_reg),
        .Q(txcharisk_double[0]),
        .R(reset_sync5));
  FDRE \txcharisk_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txcharisk_reg_reg_0),
        .Q(txcharisk_double[1]),
        .R(reset_sync5));
  FDRE \txcharisk_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txcharisk_double[0]),
        .Q(txcharisk_int[0]),
        .R(1'b0));
  FDRE \txcharisk_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txcharisk_double[1]),
        .Q(txcharisk_int[1]),
        .R(1'b0));
  FDRE txcharisk_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txcharisk_reg_reg_0),
        .Q(txcharisk_reg),
        .R(reset_sync5));
  FDRE \txdata_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[0]),
        .Q(txdata_double[0]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[10] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [2]),
        .Q(txdata_double[10]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[11] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [3]),
        .Q(txdata_double[11]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[12] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [4]),
        .Q(txdata_double[12]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[13] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [5]),
        .Q(txdata_double[13]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[14] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [6]),
        .Q(txdata_double[14]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[15] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [7]),
        .Q(txdata_double[15]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[1]),
        .Q(txdata_double[1]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[2] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[2]),
        .Q(txdata_double[2]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[3] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[3]),
        .Q(txdata_double[3]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[4] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[4]),
        .Q(txdata_double[4]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[5] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[5]),
        .Q(txdata_double[5]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[6] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[6]),
        .Q(txdata_double[6]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[7] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[7]),
        .Q(txdata_double[7]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[8] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [0]),
        .Q(txdata_double[8]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[9] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [1]),
        .Q(txdata_double[9]),
        .R(reset_sync5));
  FDRE \txdata_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[0]),
        .Q(txdata_int[0]),
        .R(1'b0));
  FDRE \txdata_int_reg[10] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[10]),
        .Q(txdata_int[10]),
        .R(1'b0));
  FDRE \txdata_int_reg[11] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[11]),
        .Q(txdata_int[11]),
        .R(1'b0));
  FDRE \txdata_int_reg[12] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[12]),
        .Q(txdata_int[12]),
        .R(1'b0));
  FDRE \txdata_int_reg[13] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[13]),
        .Q(txdata_int[13]),
        .R(1'b0));
  FDRE \txdata_int_reg[14] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[14]),
        .Q(txdata_int[14]),
        .R(1'b0));
  FDRE \txdata_int_reg[15] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[15]),
        .Q(txdata_int[15]),
        .R(1'b0));
  FDRE \txdata_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[1]),
        .Q(txdata_int[1]),
        .R(1'b0));
  FDRE \txdata_int_reg[2] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[2]),
        .Q(txdata_int[2]),
        .R(1'b0));
  FDRE \txdata_int_reg[3] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[3]),
        .Q(txdata_int[3]),
        .R(1'b0));
  FDRE \txdata_int_reg[4] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[4]),
        .Q(txdata_int[4]),
        .R(1'b0));
  FDRE \txdata_int_reg[5] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[5]),
        .Q(txdata_int[5]),
        .R(1'b0));
  FDRE \txdata_int_reg[6] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[6]),
        .Q(txdata_int[6]),
        .R(1'b0));
  FDRE \txdata_int_reg[7] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[7]),
        .Q(txdata_int[7]),
        .R(1'b0));
  FDRE \txdata_int_reg[8] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[8]),
        .Q(txdata_int[8]),
        .R(1'b0));
  FDRE \txdata_int_reg[9] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[9]),
        .Q(txdata_int[9]),
        .R(1'b0));
  FDRE \txdata_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [0]),
        .Q(txdata_reg[0]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [1]),
        .Q(txdata_reg[1]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [2]),
        .Q(txdata_reg[2]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [3]),
        .Q(txdata_reg[3]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [4]),
        .Q(txdata_reg[4]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [5]),
        .Q(txdata_reg[5]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [6]),
        .Q(txdata_reg[6]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [7]),
        .Q(txdata_reg[7]),
        .R(reset_sync5));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_double_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txpowerdown_reg__0),
        .Q(txpowerdown_double),
        .R(reset_sync5));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_reg
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txpowerdown_double),
        .Q(txpowerdown),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(powerdown),
        .Q(txpowerdown_reg__0),
        .R(reset_sync5));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2024.1.1"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
GNlNeNP2E6l/J7IS2LF/ItWIQpPIQxmqiENbpRcUcYDs2zMQssAZwDKWmGxc99etc2e+6UESUZFU
1OoN93QWwGY5lwtShrTc9cE66KsLO3U82lRN16EHmi+RZOdYR9rf5z7OgjqfdL/T03/rfwU37+Qw
8V3wDE2i5hqr8BCHEwM=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
afRfkAd8cICiTJYylAj89MfRuyfOa9d/tbr4mvbd5mWPw07cLRhiyi8gIe4+Ayw0CiOk7/EjPgWg
Tg/1hR99BWRw4VbbDTWu69lZfP8CCWoBxtdt8WRuUGz7PLk6qQMaoDf1ogw0xyXTK96u8N6bef3z
7mR3YInM0ECnk0+8vQDLRo6ETSTf4oQYcckMpPfbPQ+NnHRvcHroYdddFGNYasbeUqwn7tkHnYO+
ADUKING4g3DhZO3UAIPZJggQnH3ayID9nf49Oa7bj//jtWD0qELIGFMQn48spke+RcfDkdFLHWhY
0esV8oRsRwIWfwXwqK6i7aIis3paozzKSWbiNg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
ZI9ECw+Xpdoykt5zjVXLh0QGbCnqQDaDo8xSAWsYltuhqetS6+j38MNsVABP/vySCUkxp7yLlCMg
506S40Li7Nmy7qD4YvDLHMe+fIgwi3Hb193aoDluxnxdVk/aJ7VOJWXd88+scqfUSpbbFv7rk9La
8woMpCSiDNVKi6NB7Qs=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
R1h0p6UTerhva5Yb3duDj5nR3CyyGXjeE4dSD+H9PUwvK6SElm7FlL9AjlrnVaE2OmUXbsHu+WYi
T1XMnV1ekw5xTRICAudvAhAJrZYMHEjujHOfgXPmKtXHivF59pFlTxnEtD+dnchOL4x/jMm/lCgH
k3/wzvZbfOaxaKFceauCooIRZkqZa5FA3KG53cEucbspEldbnPaAcHsiO04PEwklvjo+2XAiGTHB
YB9XEkRUOprwSG+TeOWFNvz4Yu2FYHffCsobvgsF/8dSJQ1SVv3rM1uHf3coB/s7NIgZ0ye03NO/
wLFRL+3OuPxWNTqHO9YYXL0XTd1rPaaWB/mCcw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
4l+Avk44zzaOVxRJDzFVTXMyJmyE6qVo8wAiHogr6M/NkYt3AgnEITBHDJ9dF4SVZQLiV7ZY6shr
asWzrbISsiBCAaZgyPZLr1zlBErx08H5Enf1NkLuocQa0x2Dc1DwG+mO+flli1++tKpmHM6ADCS0
3/8fREk+ga01VkXbOXY6B7Wt4G2O3LBdFCkEvd0/MEx5uYrJNrtpaeSd3h2CpYLsfPsxxbAzNJN1
3pvNKoS8QwUnTPy7oe/k8bFD0TOzZmLvT/dy3x7SFlB6oSYr7FtWMOOuH5B08Upu75qdujq2/6ox
I9BucH2FnuLsghHnPuWfjh4WTDPekeCy51G/7w==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2023_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
r1TCY1FBxgJgPdlaGrkCY3ZPZ26hJnsdsOKnUu75PgvjzuIkty6RkwizEzV4coW2cxRBcWf87Rmb
xbDdy2cDu4f2qP6QUVBsGtY+ITHBIqBPqY7IqmHvnmT8DQ7mta5QD9HpSpd8E0HAu+1hmp2y9R2T
j6cEj7KNzaIGnuiJLket69e1dx31L14fHppyx7EJTOKaIYVHOTGcZeHi6ozbitA95N9bUNiSCS93
sJcjWWTWEB9Cx8arBeHBMIflTsCrUzgwd6f7Lb59R/pt8Zgvgr/BoV20ZGDDy6BCwEWgGSw6GDBL
o5ppIN+dICJOxCzX0Ij4ziKkTNPa1OnlJpcXcA==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
U0o+hd53NUTkWW/pm++iDbvu9EzyJbrE8cgDdW35QL1go6ppv1cnjM7W9LNAUoYpay5lYpZFkUZP
qgnoVBG2BG64sAHeAjjMVH8aUuTYzhhM3n6pvSqx5Ez8rezjhdOlMLX17yWFSCq3x15rfV2si91N
p+FFtjXOD3BmuNjRLXeeRjrvt6sxfN9IwN0yEZYhwp2ukPLeIyrYpRXZ+xMErI/b7BKLZNIljMSV
Cx5wPeURNjIC7AKZOJmo8YQe2JGJEHps80MGm98CsBtbIXDa7uAxib3WWstVs/UbQGgqjiyNIOSu
11VEYQSI8nO9bNfJPsLpVaeo9V+FUKdFcBgKbA==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
JlAVtvWu4RylemkeXAwQaOcauzsF85mjSoJqBa5/i7p1cOK0PM2Rfou44syNOWyUG43Mq5DdY2P2
hSdJ0BrVnIF/JY67xayAWorKfMTlvfZN3zVnvvFrLORWpe5MwPzwHomEMtQgK/WjTmMnFJzWxYti
gedoe+hxxEoZILnHN3ge9A2vnRikwcJETpOKLK13mQa1Td8zo9tsQuagX91Bw+AWLzMrHGR2bmQr
grI95HpSioNMY5CjomezDLSd80Rlkr+6SiaUQt3pqExFVEAsKNJdtppxNwugqojHWEqm1lF0wKuj
aLs/KfYF2Uv5wNjccSP2gHLWWaF5DKs5G5u4c4FjEiS/slCZgrt5Ymwc6XKQHNOHbPRsQcbbkPMD
V/YuoNzw1R2386n0pOd+vMD9nTtpB104VrwBNBxlGPPa0qiFM4+xr58vMzHKNT91irPu/EirEIjt
wEtLynnUeSKMKz3aAvXZPRYeEurD0fgZ6iczWVNZOsltSIQZjOp5ErOC

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GeMop+4rJGQJTsAts4IyQAkQMeKt+Fa026naAEt7J5/8nhAvWlIS4YQDNOLHMMUe8p+sUA4yE6+s
hf5STrRcmXzM45O/bYrkBzWHNqGcsEuAHfdUd9ANbBZmpncQ5ywZbXLX0oI4xVx55B8sn2gAj+p2
NQEPjYWPZXJNML4IilPsrW6TlgTi1zhp0JFBFT3O6yHY35dAseHXtMWYgjr0+6WBtVCCvC/z4bA0
UY58Skgytan1bUM557AlikG2LqNB6YHVUQihnx4ZProlAL61WY/j4joeXcDH8Tpm0cmYcUM5yUax
haoFS6FezWAqD4KpiUS3xpkUVET2QpN0NnxTvw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
1x9gXtJFU2nGQ9CWRMPqjLG5B7nyL7ZKhgWVKPrQTraMot+PY1MLqNEQ8371fSgOtWrUw7wBswaL
CBsGdy0dDZt13psueOntqYT0wcW3vUbYzfkMCsSjkk2yeYjnCYclwJKMNk3fGxfxpTLQsScOFING
ZyUHBaE+KeZ7yfdqvxwcpP+rUXdNX/2BGvcr+a0ow/nFsiCiOMdYIlAgllnBFoUV7lDr001O00Hk
1MDqVCRdpiO5q5FnggoIp1sVIq1C+JbqiuyPkv5jmlk3iIfZiHhSSYoxP3DJf7VPvlJ1Z/Hwz6Dw
/39nFCTotl6l9hqiLHTmISLBvSYZnjDEcOPtPA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
mxE8AqszO8jEcufcEIxsyswviid2I+vp0wiBEslkbc/wVr2vlpOaIaviDrRuTwIVx162O0AViWa2
3/lH2PLY1lcjenGETw7njsuJd6MQv4KihzbHhd1HB0TKpoL1n4E9u5PgNIGghcgNu7s2hE5rpffM
FAxMXxyhZQ3j4WBKTgRtbqcexvVSl/whSo87Oa/V48wwF7Z68uoZdaD4cdNihYwHMLOm0LYqYesQ
cScpoz43IdSFiQkBEM4YXuqn7BFIsJ7UAr+EkHAFxWYqt0IJ3+/OCuOYW+y/3p2tXT2YSGnEsOzZ
SLIsZpsw7wAiEfu9tVZJlTQ0OUTZqfQXK68i9A==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 153744)
`pragma protect data_block
uKVYwWffV3RuqqLQYt4mpFjGHNDIKf/27OKqt8JzU2PS1qiLGMEN1/Y4Y5dU7VAuoQIwP1uj7z9u
HPcm2Lcvdiqq+zmhiiJbVlrViQoQSZpymKHR83nw5ONBCDuHchYwATluZ6ZbeVbqk01ccH8Vjp5W
AgOqaSzsspLLsZmRc7NDRWfsFRl7dGP6j1nAiMkePcJ8mNEdmFrCwrkpTgyydE+EimeKk0HEddkF
rNZaqyTuvB7wxhZfrtkurSDfoF3fVVc2Z1R91MPhLWfgKkrJ8jt5+qkehvEcdWQ2ER+3YAhOp5Yw
H5DGb+Yw+FxEdCobkXrottBWzFgOwdnNRmQIPIMHKJHcnSUOolNMGIEbEGOs272DzF/v4PKlNKUk
Fm7JHjo0ZIsR3AD1ocRRQ5AsqURqDKAgC9Y+cJPrFCOt2lm1ENO6dVsTljT2y3mrR9VqZdTId6Mx
7ROMqolFM18fozvUopVdudgiZ4Oprfy+QCfL8Bm4DqZdutn2c3jB7zkGQCWiITsnE9cwnmLeN4t9
OUKsg7gKK1RW+zDwVi4Rc1sM/O1t4aUAq2gTiPmvLBWC1iXH3DwgVOn60RbiQSbPddoimXCyzDC2
V5Wny7dynMe+lnza8Ar12Ln1JOdZ6iJpjjhOPL8O9cwSDVn9wcQ6EFw9Wu+Zh2gfW70R103wnI5e
Oboa1ABJv7F2bOtKdpG1pnI2ihgQnfPgck9SDklQqacCPdkAO5bwUBMvyGJ19sbnbJVruA4Bpz0B
+3LUCzo4osJBdkZI9YovcErOs4Z1I5H8IrVLJnGFn3fngLF4vAA9yv0hJW9xB1TcQjs/DHHUr73p
/wnkYlNBWNf6zs6KEKadwjtoeBFRLqGJ7TZOWQ7qUmbBKXcARCzh3Pso832mrKUhXQmqtrXqFcHf
xlWP039GjMa3+sZ/sTKxjjtneqXWwWM/26f8p4leOV6qGJBbAFaL4SA5HHtq4/FIhUgMwYfNmu3A
RveLPJ2OQAQwaS5iHxJfy4CXZz2uz9Zui6r4Jpkojp24HGMxU4eZXannlfsJ7gzOJZX0VjvuDvFF
aB7Dyd+PxOtqR7YhFm3DgZNCBuLTX9z4Udfcsligwg/u/7kbkaLQj6uJ4BAcYJG35y/0xGrfhENd
n1/lpL1SZYsSHseLbWBo3l/WI03C+jfw5MeCp9wPjxnezy3lSwgR3bButNgbh2IviH+zF6uEUwqh
+umeY1jNXC4m5GckVn80x8PJoOkNZQ8ANh68KbZHudPgAUOQDTB9PsXueYECoek4QUPDJ/2+ltBT
TFsh+Ldv7PSiW192Dy1XIyU8vs8QaTTbO5KUwEY0/KOEx17oMYWU+zTXlgnDyirys6ji0RpYYSEV
E311Ha7RPhsraSmndU2VtFJ431CrBigNP84LwBD1XQAIAoJ7OAtVFNP8n96TVlZGSL5irnZRruPV
9fEocxwk8q622LyYp8YEyEhkwr4swRFWfevArVmIw+rj0pr/uGy5CGe4XxXKyMFnRauImEnwc3EQ
vMR25/g2TN2taU5LKYcC7YLGQFBrpyxzzrvVty2aOk5eVJ13vRNBQAQc9OrlEz79wDntrNUWcRk4
HB+w7loggQpCPDwrZaFOlXRjwjZHHbC31hQel2MXwp2KCWcBPDxaGWDNiYNqsdWG9UNN5i3QR0E8
5DhtDw2R7emXfjM8sKbqIlg2T39IuEtxLxQ4SEXy4clZnrUauPJ41XkMDEmSNcSrRNWXUbgL3clS
eOEb+WLRp8w3xXl4jwDepwZKurBC4t//COVZcA6RTHWcakTu9aCh2DwU2cYbKvhGDCVLST6ICdSg
kFggq+RLD7cSnlIJxG64Rl4VTVA+dQbyJaBRgstYhxzh8XcIGqdFjvNq2eaTmtO2Ezpd6As4KOrX
fD1oBjSLvgZSq9xiE8s0rKxS2CZ0h65WOhAmOzPvO+MHCB/Rcw0dp8pwlQvUJ36Yic9yYW+/ENWt
PHdRvISFlj4yOkAFZIdm1nMoOpkN0L7fB6IW8MIbx6YDp7FeyKurvCgGUAXT1PUw1lBZR9xVcjWc
XCjThcgnmEVBOc5eB6ckbBccTCw6O8vBk3/05W2Q51xGLqeGs2I4OYGIbO8+u3jxMHaAW1NPGIUj
tTkZVhHKtsRB6iEneI2Pv/jIkcSmkvP9DBGZF6GyXf6kP35Gpa2UfnztxdylPFCHUk8dkmcflAt/
QPGetkXlsOCjQm01aZbnx9ha4NEDieSXAKJEurWd0YJ66+q5TWXqL8hmx6fUAtGS+l12+z2E25Yj
wUaXZWFAxmKH57tVed3/ilBY6lDFKHRrVoRP2MoAHQWwpFNKb2l7lSQWpnQUhowuA0cWFLkg+Ey6
AUc7xjaSI9V1vcp0XKwjnI7upuc0UGIn2A5qAvRyPPqgGx7629ZCQvrr0a6DfA+zwWdb/NYBew2T
+lVJem8ZrjKh5St8pqJhXXrgSDKeiCfllaqNka5hSKZTy7HcfJEDNbrieBcCK/IQCL8IaKTCdW77
24wvwr9UieX1dskIyfqZWltpfnYMc05Hn0tbqFEZ/qDXKFRryxiNKxmCc2KbaIcAjFv24A2oDgPb
HC7BVPzBqgfmky1Y7qghT8csGNYQ/DjbL6rqcC8NF3/s+FFlZi6FSznMQNV4x0KhqEbNasbUfUha
KwiZiYPakrL2BUmMaeVGv9DZ3VT8gJkcjEcNFHHOObTJ90FJfawYV6IW6wPLiFBBBHdYxs7DrMmr
wFGPSbICbrfORsoP6bTOnNIpqnf0f7kSoF99FI1gbOs22oGqqHP4Iyrr6AvhIkJOY7nJXd8EjlPZ
CDk2QDkMOuzC64hsVvglivVNqSTV0Anx+Aq/Zc2WNaD6NZLF1KztZvFpl9cL4jc0K4Oe5CWH4KD/
qrgddKJa+SrTNmruYelxtRnkkmTih245KBUSR5IcQbA9XYlJYf/x6RehbaKlpoKPMlFMzslT27OU
2eAXDGO+NUNJkbq06rgdUYSBwXPMD001SpaSTiXEwQofYT9BcESMt/quzv3KsQdwoC7AboZlES2a
Y314L0Ex8SJAI1KaLlcFdZ6ESfc6eK6kDgP1OgMINWAuHJ6rQs2uCGQNr1K6MbniLslYmVCTqzw+
hCN1ngHTZVJWngAHIwCL22p0NlRxP+VpZZSPsdr6O9JiqZKeHJCyTggi4br48hyscqtV6J29ykz6
3nBv9ZslaGAqmi6MF53lpOEJPCjTPYsvww4n4fuQABJNAcjRK9YWpxBa9p8m1rYK0oYEzL/jj4g7
BcEIb4/lYV9wKU05ViHvwWzexSZsocGl4OCg3xD4y/X0r/pdoIDZ9K5B40HFnVrAm2YIIKCdrxeG
WR31veTiJVAZaT9Vl1evQCjs/iCic54vM2/fswol2zeYRMNkBAvmYnBIGBpN6NzAHwfyYEaV9aS/
YqC2X4kBnsgTxrLlr8nqVYXIwbYxSc1bajaF+3sxIhMQ4M65n1jlNG7H+/yeNWgEMCKUlQzj97S8
YZnrV7EIYX6vMySCA1Ue1sdwufIMlYA7VR39PI5b0bXjyqCON0OlapD8F0bJvwvu2EDV4NuQGs0O
fc+n1xL06kd/zU8kqYM6TWP2iILtkqPBsHRBZQubfYFe0HPvTM0n0I6ykDxtqjfNGyW69RFDCO3B
IXzATSSMDR18F36mkCMvK+amn15Mr6uRJ8zhDTWmR7P4DCk04xLQ6mmYvKfx3n2oSxOa2HJeVKAn
XsG8l09+RONbDL82JZHxZfezLjyrpxI5TRdoiRF5C0iBeJ3fmM45+0a2CpgXC31lKES/dJ+8nNTH
+RYKWf7EY/JzQkJefvORWzmykqy5JPAMKrUyc6IxtL+/kV92fCuTnN4MtMVUqUnuLaG6rLUC52y4
sjUG3CrQchIsG9LTDJphvUxbGCgQU09uj3fodbm6E5TwJ8VExp1ZCG0rWMxvDqjTiw6mMZw5TfjH
BdU7Fj4vGvBEzfArnSnHLAXyP5zxvYY5Llr/cyZ19QiHOTCxMNPg/S26p4Rx5SiR26s1vKC5nEzZ
oDJB2YD5OfaHZ3KP1ONwbnqfodCOiMIFX61vpnezp06iqNJ7opu7RJojBAt8d+np2yzYBkx5bhbF
ZBBKCIBnk0m8FkN/8TtqN3y1FgomTiQXV4Csy2rxFbo8VVnDT6VMcCB0o32xtnwuUQJzersAPbDe
UEJuC2StrBiL0WNWmFk5KhU9A5fLk11jf9SNjF1meEh+EAJh8Z9JKw6kx1NsSWyMrgF0JsYGRKgx
qw/d/g5lINFdEMswWDXQlIOYQf/UTcSaC1dJJ+hrwr6wbs95I7Vs+tlFvZk8qPDs19yZ0SPaeseP
nWT2Q3EHlLkZlsKZNK1yY5cuMgVpA19JBq+RCg8CGyVj2QmoASzCKzYZ70m/cOQdHzayPzzkBN+Y
w5H9ozDwydFojAmIiXC/icuJD7trs3VFj8erVlLgLcza6p83vSo/uKTTpXe1A30wi+sem6yhxROy
VrsVyr+lkj4gH+AQiNGfSZOkV1/diUX8GW4krOafyxDrLFFhZGM3Hi+fMNINmuMiIna67UdFRi8T
P9AvswyP8Fqc6/0NMBuEk/N9Z+vlFe/KyBeTPWxkPh9gDSYf9pFsaEOaknQYhJfzGygey4dUIedD
sDL3yt6gXR0wrY7v2uN/0x+Rtt4RNmcIl3rSJMfsBuDcBRoROIdK0VN8RKe9tXT00srbuh2U8m+k
uLKOv3ihn6MdgGAzXoUZ7HL+5xzX0vCXADSgznf3fj0+WTSCKRU77KgWje8wzvJvPCASimkQh7VJ
fqldTNWyhXpoFcedFR88oDe2DR8PNbVU2aOgdgJIDNiLSWHdd4baJbXcKlQDitc/+DzxwNGwp9nd
iXo2Acul4w0q31ZYFxxDZz8fwn7fNfdKEcIu57A6TM6hAl7viuF70fe7kAwPXusyF2VeAcd1412U
qTPSRcQfwn+Mou5ImCO5zw99v3V7bimbAfjb/FIxZmWz93UVKihdi6S4a4v2iqN3qrQMCok/FLS3
LxoyS7EzL0R2fhUjHdlelbn/T+K+VD/gCj2fpE6hdE99GSABI6ztpiy/30ekCPdyd7apQXgZS5Uv
XRrqqg3xtBA4kUk8LZuIvQ9bsyDwSVwVzpl8FHPaj9EEkON9JhQbuULoRbXz8V2lC4B/O2RHV6QY
o7EdCQhXb0nsdQGvHZvi+uiiXE7cFE5XWUyWpfN65BCurQ/JLxBKe0cJySGMUxSh9EvdxLcS4BfV
AENlhycCc20Ye4lFDOV0aH4FGaCy2kobGuZxwGWgc/7TaRADI26HOKlD923OEFn7RpdIukwJfwEN
J979ZDmPCWOdjhG85p8yTk9Zp5aXL9uvN4uuAJR2WE8UjegonI+ffVls0kkS8NubAegweJOOreL1
B/CaPrZvAOl/LeRn9QMwfc7Vu7MAYb7F012MKFSoezJqvYbUSybpK8aK6Pk3uiJWxSkRqXTQMc1v
ydMIF+MxYz29ZfJfTMltAtiDatoPC2fimOGhvPhM1LchvQbTUGW5xigKWj9IEbIsIFZOylIWQmIk
8yblNiIPDHUhHRVlje7wRaykl889KNJmEa3OE2itnzJlct88QpmiUwQyUzvoycddeB0SDXJeOqxI
kd8fr/mtCl/c8NSE6yDSTJOss4K487zLG/qWMRQjpStsK4jrsPrffT6LzbyJume3cV68STUmetJ1
9T5KNCxd4esGMrQDHSnJtPpkoa/dRjBzV/FriUsEV0kTu5ZJ8KeTsKtqur4/3mZtAOV3UilRSzPv
qDOCvx8f5rhD5mPHQGaYlnx+AAAu3w2SPhLEOJkZcGqFJreespigu3kO7sKUdnpi85UEA+0/jdfQ
Llw2BGWRD1QcAZWMnsQ0TvLiV/QVef9tZcrax69HwKaOBrgEp27iO1xjfPCcYc5r6s2bhiD/0Cov
kwFUgjXpCkB5xe0wFTPb1sxsf9otMilP3K2sEnL8qGdk4kivMY3U6LjbsONWoxe9nkzhaBe4MiBD
HNr+FS6lIowXjVVqPK4g3HVtGhKZT3bIjJNRCW8N+z1N+DGileo/zmm7mL1R93wtUo11nUi4RAf0
XfybtQxitwNVydHBYHOCdnCoX75XAl/73njVw/jisIPSD8HeYlq57oKE65YtpLC0U/jt/IyJqhiM
fffenuCOvKIyo/0kHvatQefzyW5oAEvrSmtL5RFzU36FC28q/aN3OO9UERH6PWcmdSfYd9BX629h
wbrd+aV6LendAjndnT8qkeSgI339kiAZ0m2z6g6YOLHBxMH5AoCvhSsqKtnYUm/qKcO5VG9t9AeJ
DULkPqzP7OMFT1znmGBJvWyzYWGyJzzdhfVBqwVTfxWec0yY6/Px8c+qbIfgOR6LWduzNe9opt6X
ZzRB49ZO5ng9B2F4BQTgdv5Hpl6Ll6bN2tesslMFYXP8BKUEQK+XpJaeZFGkSMGoDhAIzwb70ww/
VyX/k+xlIAMZtT4nEHfR/+l/lDJunbSkqcuexnPg/pMzoyFu3+Fsbtrbv9DBmSI5uMgGZ9ttgctx
SJdUV88ItTUYQnWPRQ1lDTSQwWRP7fGS4yBCl3b5PRJyVn90kSO5wm9ykf9m02td1JSzUTjeFiSH
v3tCLHui1wXxY+BaGREPR57OF/X7J9uC/5d7xcITi2mk4lEE1MjGihNJBSiZiCHSLKOYM2LyFUkx
jMUBcssyjzNZzLaiEXdflbtbn2RGr+p6NIeoboQ4iM9tthOsDg5O7+3MKk/IGv6dl05+S5IWLaEj
GMysJmmwzojjDmiglJs0ehSoNOYiT9AM21dNuo7k9iiqPHKQNvdUBlf7q0bboVcxkxkue4bkHN0y
ybhvhCyK3g4bJYU9GUPWrEmfdmfU0Fp0wahss0hA40KfCENqDu8jL50HGX7vBILOFdA4Nn1kXivp
F+/K6d4Sb7IoPVCrFkcseSpEFJcPOthcmNyqHk9QOh5UtjrDPsUPc5fKxHGIUGt74VAVhCp8J2fE
pNrer8yi+XSFT1EVwQ7uRuXwnKRPetAvFoA1J0VzzbrrHC653vcmRj68/0ufDsdHlFoNpp0yDO6k
nEP3YCZBuoyiJ1eKV+T42eTHhUBKQ78IwuqSv0KRwZPxl55GTFkTVJrEVLz4wNBhlGoUuPwyaxFb
do19pidSqAUCOZry2g+75t/jNuF1t2Ns2Ewo1zSyuBRQ4USVSu5v+pWLQY/8rpJ2LTXD9BMCNwkl
PJFsGMq4PMERtZuanjvQZVSnUdEDW2BFCCT5gE6WWc6ZbK2fBAe5vncv5HEJzBcyujlruesJ0Wgv
v+z2btS1emws22UiQhLWxboB0oUJykLHARWlPZXw8gunXopAOJTWHTo8j+Io5tviDU2956hN2xC4
HvW8Tx8RYj8QarhhKE83U2gsiKnLEh9M+lVOX8gVZTSnw5Bwj2Gw+tNCftbwkl5hYOM6zXh9UaRy
SdeBLH6DJ7py7de9uf76ufnu65JrNmRnAP4Op7Y1E00MgJGg4b8uPbCg7WmCylxiTnaM5W7n5nTP
fj9BltqwllvHuxMu/kdhkE/iohqsMkhPdBouRPxWIVe+YIIE1pKPWYdMqUMUF8zzKnPe77wkc9D7
9FKxbe1whzv/AVlKJs1YWDtZxobkYNPVvXFXDRbCTsLTK3mWpOABKWc3Vu+hZ2A5+Hhqx21aksk/
mB/33Lsg2ABmPNwQCgzPhNr+WKJkxIyTUTMoL13fuQ5Q9Z0tiX9GAsgDVNQMjR1/Uxrf8Lpz/fTX
MbS+7H8ugUmoPAyK6uXC6xs4I5vfed3t9uEKNW0jASwiLc+WDM+kFCQQvODrsIRy0SfJnMaqmLmP
C8ukB/8fuDro3C9fk7Ox86js6V/up5J21Fas3mk/zGWEuAR5S7oWB1wQADwVz8abt6qx2ucmNj4M
b/f2+6O4r6VtRI7hSKsoh3IlF0E4WV0v85Qmh9X1hvoISiVNowi/saU0QOHlaQPJRHiaG9jzPnEG
xvZ/STsMC885tvW2Cjx1juhWFWojTsTsz/CfUF+WRDyx3DNXYuKGxJKIHIdhZmCB+FZuhmjlQPo6
N1qRZiOKFyd+lkZk1pxTVsGq2A9uzyhKEhRr6mJR+vrFERwQ5+3Nxqf0iA99TjzzfyN018D3lRzX
aJ7wNIgL/Qow7EQgDI7fKntW6qo1jqAc7KxPQi+rb+NGj43iOJ8EzXDIQOg1kRfDtLSXWhCNnKav
NxpT4pbb3N9bs9uoucNvgl8Xp/vf6qp5wWFdDnYyn8zg0AhLVi2M1Y4WdPm3Ee7QSc4Uvi7K82zO
0lv0RBLrQZdYoZh86GRvI/JVT1+IzoQwR0HIRY9hv3iiWFp/8bjj0JVUw33wNYZV9Asc80ShArOQ
0gbL/bXsBsR2HhBRPXJ9Mil6w3k90T5ZKOxXzPsQOZC9r3tUcQip4yZI4eRlTrEZ2ccqO5WeZnIa
xQgpMrHdn6PW64dNbNaAq5kSAyHFMSMbU3LaxYaKUbJ/GXwYUWjy6HY7FcQpZsQdnX7ZgkhvaU3x
a63cPqeBsttKbgan9NFa5uYP8MHFfK3jfl6pPTs/SD1eIpuuEJYt+QMuCIm3VBHsBEIBfAa63v4Q
5+Pb0ecj83I/ORW+RxVplUb/KchsQYogBczE3lROJV6mSeBLpaGymI6ia056k1QbKr8gOJv/UOYh
t1r0/ozzMrIJ8UNylQfTO1lmxHNPEhn0bEKPe6Ur6szQj5YVHCuyv2hzBmB32ZTGXo4N2aWejVU5
IWojU7thWjDaG//alB1bOLiaa3CKot0oG0NzdnD3lExhDDL/NbqsuTi87+ced/L9QMR1uBPzx1jG
CGVPFb6eRSu0sqi2QYqz1/XIpH2yBTE4cNOeIf18rZCwfh/N8Fruz5qQvedCOCqBvlSCul2TrUyA
xi9tWT70yCVKXP0JEUqvCZGQD0fTcYQGfILPa3RguoEumYE4Qc5xDEQAf5a5lENo1K5MlEu4+ScJ
h9BuhbOxdcfRIaSaATPOvZopJ9e+IsLc8AcCRC03TgK/vRFtCb+iDtK2kmxUFkUbCET4UnMUB+Av
w4AjJrNz1+hW2sEYELZJNEiiEKEhf8pR8MI+O5quLNp0Zq3ChbqzwJdGNcaOPGpuVULQwPxYtPWw
7u5vFLS3JWEScUGTOnGp13Ra++/b406r85DhehcU6YpWV1FucHabIZbq8EVQRh8GlvC0nttTYpa7
0mZwtKLsG30IumFvFMM7x3Q/lGSN0OywpzpwPEcvuf0OI8EF4EwpoapeiOpdAZeJtBgk5fe7fT74
KYpCn60zi8Eu8tU9V8rS2syFwSjmv3QtG/vMT1Mf94QOH902xbSd8rATYJsR+rMp5reW3gPuR6Cl
pyVSyeTj06PDBTq+8tbU1xV5FTwwF4zvYNlTrNaGybxAVzBwjuj0Sp8skBVfITuuzz8J68bPcR4h
JgVO+BKEc0BW3/Ms/dS5AKQfsXCrqGNuyg3+rfWTqG/IVror54UBhtCR9cgsWY9iSQ5JmQ8KL/z+
WnRZ0PRBcs4mXCCHut3iXyisU1PI+Cof5IBwdf8M+zXXF6DtXInRyJFYZH3QU/kqREdwsHcY3ZSN
bi+DFBHjE0S2QFBDS6lda8AL5da2c3wZ5MwqFnzUax7SYynUGGKfL/B/xvGoxc4nRFaVaVxr5HWx
+BPC4MYxvWDtiQZFS1Lo2dAmlO1yG2SnuMoOmFenjESU02ck9iwMsxLZ5CU5chjAkqxX9uX4HiQK
5P4TSCH8jMtsrUuXdLhYMbRmFDyVxaIPcDmsLgsTnqCq2V9bmIVP1VIeDI50ovIt+llEDsTAqEdz
f+znjj+5155PMHRzIAkEGToMV7nPQ1gPRSPMyfWQfLu/cwW/8N4n2h1gJMA7ZtsfuYFht0kix96R
qyuyR1auz9QLi8tfnivFEah39vRK4MFrQq8602R0GRVtKCZS7QvABKpCt/+0W8g9pIWWEM86P4BH
hhiIOiJAqWxJjwVxKP1jjmBbErRY/MnJHB+U9B4hii/M1ZMyP3Qsv+6nX7o8mi2Gs5PU7a908lVw
WNHW46PtFKE6Zf2pukvTM78cE4RZYxkv+Ktz4YXuyDZ84uzz01tfDUO52Ygi2d+cLPXeWkNZk+EH
NYsZVi5ZV/L+L+EQuhf7LI2uE0yAFldAQjHeKSq5NAFMwYh9kAZ6XQ2gfNscWHA90GlWlRrVDUxF
g3gMGr8REyfVcCG0e9cfUK+MNQo7vqo1r2Lk6vFlh5QtCYN4fIOfCRNpUQ4EeUS9o7HeQxCzUgLH
xodaJ8VmYw+scaM8OxDPA8ACS2kn7w/b9Mf3Q3Yd9Cr2m2BCONH14os2hJKsH3CBpMsOM0nF2Wo7
MTw4DfMI/DVGitq5alQZiXWhdnRAFxsEiKm37KsVYcivwoYav30guNVty4nt4ZYMS0d68DgumUf+
W4G+SWC7Qa7g4vyIp+L2LO2ByCgRoMMD5pUgDHzqCyg0tbJxPwxYaIZbjPQkETAdcjAPgHZ4eJmn
BMexWs5ETWWqCFXSBbC0pZ4Iz1ZdqsPT2dDJYCDpTuVj9jF6bl4YXtRBFoij79GqZ6KGYqKrHvYX
bBp+bPnEIhw5fp6/RbeAwSAz3CExiRAqsPO/dJKEfX4MLa9fWxRHxGQRzL4a3w9kQVlFdHP+y6lJ
eUOAnkY8TqFnMpGHxYNfuZblFwPglv1DKmT1r83VmPNtowsbILtltmkXFQCAPXJbL9GAilmAwVio
p5fBE4hk2zv5kfSTnwbmdCFiWqLaaBxcZ/HHaBKD8DJOB49r0Zn3vLCkPqPM3/YrTY8fnvV8VjO/
LOFGjnKtGrWtZUkNVT0KCkoJpp90dYq05z1HJIiwzTaeA2WoKQYkXRaZlTy1B+5dZqeyO6ms9Es1
nHTJraXSoGJMRPoxPPdZFO7TJvCkAdpLLt0u41s8gthNq662DxE6z9qTFtfZIfrD/UrA6PIAXeLE
ZQ2upCpaEpJ9lb3EbR9gSTYwchO8OUWAYDv9h8QAi2Y9WD1M4esNmtkEq1/x1wumY0+eN+u45btc
KLDQe5BPHh6WAZZgS+2JK+da86nWSomU+0bypCdWzeLDEBMd0SvvuK6RuRVi+z44+VpoMJChlbtI
I7s8wN0ZCt3kQ0NiLHHYs30/w+N5EEiGa4XHimxbWdrms4h/jScii+Njjs3D2E7nYWoQFPai1M8P
oAtVnI7IerztpKcW9dFIU/stJ+w/LLM+PTNn+8uVdIDA5eFUOrGQoqf+SC/7hZEMv8u0084z9Xjw
YwrChH6tqujFMGvfieT+OSjxzmGB7jr6Ww4UZntdGx+bnMZuu1Cc29SoT3zyzv7f6/Z69U9QdPzj
KMnFo/NdOpMt7uDhwnhh93Jbc0g2s+S4d80vze1gz4TJyqRjr0YKMooLZi+sbfpYwbL1SDtwUj59
uvCpcbwNPgHp1X+yZA5nWbC5h5vcbMN8KAK49vEloJDUh+Z1CQQLSxCAR3SyA9ZFHxXErfYAcB+K
s7Y6T98MrM0MSxyM/60uZIu9bZytRa3ZwdD9dabU+v7biueT7uIibrB0KTfZADjkaX8AGoUDguq2
3VQzmFg7RK/xPqnr8JPEorRu2w/i/VPJercMd2FuRU6IWrioYDaslPcoGkKGJGYFhdy+2I3sNVV9
gwlfvqxmdSGadj4KFBuiPmgZbR82Yx3GIttkGngV0nIVrYdLtdFi0+bDD6Dq6WsrghHlItlYLrjK
P30nYpZYoMf6oF5q7sjMfwJOK1LvOPjPlX2BjSOCU8lTtpjZo/WK9gcQtRO7QrmQm9mqMzxqLfSO
7ItuI1xd1snyjl92XeGrWaxIbzcldqKMYYEdqi49RA11vruKCgJtJOPnKqMXdsDbaE570TVpjeMO
+UqYhvc1kZItoc1E3dEJT/pVhm1KTtN1luwPhoOtFiiURNUhDuzbZkFmijCPvhcTMdS8xODc9LRG
my+whKQvI9NQtR/OElfF94cjPsCcs24oLaupYnvHGPJ4E0cn6JAWwyude5DCUePS1Ro/ERTtSRbH
ex9lA/rc21A2a+h8UdtFcF0mVm/nWYSwb+UQnetmcg3FbdY+dpJBFDKfsvZOWaGoQYMnoy051V0e
Y/pyTauxqyR+YlXcXcEfdI78gYJc+ourRjOp4DLCAwUhUdiYhHbVSk0JZaHTB6CD4ow7QrLS1l6e
sZ8qL+SQhj4TCPxRYQVaFGpr5m9kXI/kqZL+gStU5zMQcGBQp7xfB7UrEX2FV6tfrqJw0f5v/bxl
LN8lQXBAOhRHfed7+y5Swj2mZMLE+f9vWue4o8rt7mCYIckHL5Uw+429b+nZA8iOONQq6Lwm5xS0
wBbL2KWe2BDNrZOlFvEb+UIvRltLLsLu/uCI+DLjr/5bIO3oweTGIvOeB0zxytxQ6osIiG1iGsuH
1t4ECRxgfa7/razw5oqda1/GpzWlHtabDHQujrs35aDI+8WOtRUpbZEDEUeEIMJ7omYIn2P2K3LC
p6VCF/yEcrdXBYgRN/ks1RUXm4SzT+IcKTzSrSLGHRwN7poYhnAzBxDPy+2okqyY4xKXbireEO+o
lGIK3r5rHHdFIOUjiAZwDBlZtjuzGN997gt7uC7qfYTLvsT/DDOtwYxNz80PVcEJwjWJsqgU9+aQ
X8+wOG2arjgj4bc4/8b4yGHcZdFVYhGUVt/iFAw4w+ZoK5ZDaPt9naDAWpran77/lAlEkF8cIXTz
g3q7WuZoVsF/YCMtnY4UkDP04tq1E1tRXQFaqqBwyyV7tfF8ByuLvbMJDn+BpVLE9Yrdv1Q9dsG8
8FJZFiqoMHZj3byyAznXf0p7OCr/uZ5wD2aDMakNhjoi34aFP0Oay1/bFcDmU4eFh6G6FLvjgfx2
mtD2sboiag/q7gbOuTnCSN77gXZ8LogjwMihW5ehxuQV19uo4/PDyaygayh31QvAqEOG1VEofWPD
Mb8U8Oeu3rDGO5vCtmlHQR2NQXpSx0UOFWscezr8l9KiOoOv+ay1LhaAJQqJyS811a/nJ7fPkcq7
aYhAeBHJDdnfaLFaMJirBnnq/rICIh5VoFZxsniIk/cyr/mC62pUBnxcbOX9c7+GYYFWJQjVF1jG
EBNSze5U83Lr3BLIHOJMJ4PmROeRPnaiwoiBjxJsVXkKxKzoCMQ2DJ/IRh9rsgiObUtjOtJebNV6
fDWGhIiJNyicKGIhM2FKX/FpF2/A4/eJS8OYZCtLFT41rJf5t0Ag/jX1WTVwHLOS079EsC8lqON+
qj+MZpUpTP6SbL4l0f+2EuoeiaO0w+K/vtspoHYEo3+5Fcyo9d+BT1+kQtwbn+0gym+i5NR+YNDF
qNcIBCdT2auVAVzBqKJweGXM/HnoA3dnMs8ibsLXHUFgd5ZvJMhHC9+IhGZaNVu+GH3hU7bNNf+9
peeUx5BJNuobeo6Qxwcb2HF3cHtDtDis5OaqWY5MMWlu/TJvptI9ebFdj03mOrS1TO5CLxKN+0WE
ahPv7D/NRB9u8Atr8dZ7uV5ik8VMuKh8UEwSJ9PXv5VZvp7zY0V9QMNyHr6lYUWM7bKMEKiPP5Lm
tnD+sLKoMCFFaoxfTZwO+hp65rypsVUqRS5+sPQtbzL9T64A7eK4Fk+Ll5756utWpUlhoO770ObA
xqClGLkyVBHJEy7YKEyEFw4i7oAugwOUsYwXmaLTBWXu/EoqimPI0dIuuc/xHhQB68WBpbzJCeVV
3p5eX+LObi608bcQwlzlSPQmEKzvgGLTmRdhEH7ijSafqZ0V3+X4s9WduxUU4M+9tH7HMyvonuKX
cc5szMYgMz6uNK8fj/W/H+0j+rvc7thoucVwQu0U0WrbDQkwKUJbk6Hpm+hoICYD7xhpm1fLDkeV
FDFqwJd3id/OEWqUwU5H9C50mBAYVdNEGb3Le6CfQ8Xku5wRwqwXwwUQdP8llMsqkLODKO0kxEoI
Jr3kwKK8aKyvaqRjsYFFvZvnYc1PVjy5bYVKLzpLxo0xljWWripMrNoENiXZJGflLv0Hwzj7WsrN
RiVLmRkrgJU12mCnacwfAQHPhV1wdVTELdNKH/PtpVrDuaKltHCpqE8Hs5xzM7LW/MXSKggn701I
QgYshIr/ychMe542/nc/fFgHnwrufs/ZrEZhcvM5Qw+GgttqWn1a7vdvcuN5x3HrBn8KH98QrDVK
JqSoVPLLqd9UZ/cx/VdNjSdNKwtKMmNco23KazDokn7VyPDzne1d+EVjoDzXbr7dCqM500BUE25/
WoWFHSrVnqLzXIRlssPQcDHD5Ap3Dyt8Bn+7nPgpK1tUZ7aU1nRC7d0NoQPJoY+e79PRTeP0rZ4d
CcjUMO3dc+6Pt3stcKqWOPeBCpMyrQVV8GSi0ILG3Bg/VVDxEDZVH+5ar2x21v5Vepf8HI22dVBb
Nmt4QGM1W7+7x7u6s8pqER7LR29G3haoaGpGsbs66nniIYeOkotZ+RJwDRP9m1PS5M7R18RevmS4
sx8Ci8XghV3ee+UOwyyg5TPrNWUCPZRGAqeHiLdYhlg4CbhP/l+cIqQeIijEhK3owpXMwpH4xuvQ
OUzLw+XkgS5dSpsbJbx4UxXKMlKycUwxVlh5eByWJkTFPom+cVLqJJoZtqDEOjmF45TA5AN1fTMG
QkAcRNB0QrSZjbAmmk6zBXy4O40/tfqzmZcUb3/iZ4Zk7fXFI7QscjKlC1lewsBcd4ATe+si5nqT
YG7lAK+ruPSRrlQ7Wv+2h/98ndAmambsN0jMSm79AtgDd0UprilDNhAkJnIHacxj50ew8A3Ua9js
DSFbqkXz6szg2OVJqJI2cibuGmlOH3dL8PTOFoWeefe89b/IRFLc3wiHjc1Jx9TMDcQV3jl5555E
Jhc+HsP3EODbnQhbcEe7s6vnQHVXlH51lr0eT/iCqGMHu9w0ecKf04nsXkWOrvOaQdN0eo3d0F31
sbIOhrb1BgXVCIuEl3cCkv0dQwWDTeg8n9av2tp9pLQwDaldDm2SJgRg6KTX20Ayh0aW+HysdWZK
hLxYNEo6eH8jMc7A6incArvaXEFBlWqWqUW/0wZb2MefAGyvwWTbCZgc1jQoBfwuf4Odu/4IyJ8m
7tmTPn6OiyuWkbBRBfdJ4rDz79oy+IbUn8EK27KUaKz28PNQQhskNWNLJfl/oq/3SdqWQVAuRvga
b37HdF5n89SLoUS3m0DTSeIACPAlLyFNnLGGG7SmEp+orbcldDipKiysOIgUtJ543+oxXwVM7R7f
JnY03BBVtoGYQwTJlZMYHZ0eNKYGyd+a3KQkkJkB3brjhq13GZaQhv9Dk4idwb9a8D/6/xCSEKJP
8mCXQh2Nev43VW9tpZb9iVeJ//s5y1qjH1hMQ9mUj4YM/TaEWdIh54PbH60mf70kYVid4xxwIog/
xZ1AHOmMXJY96g+MalkSCDWCpcCH8WyEYOWSsssTGGVo5mejP4Yl57zpKzFs30LFKh2tYp/LfAKa
xy03I3bKCUGYOz8x2Pf26pbNTxQVPkCJIr6c029lB6ku4dPgm8iyC/PEwF1bhK3pPDX677CobErk
+X0TxkUz8vETwlL8XK4SON7XrrTHRgbftooaB+6vDXLlqVF4hJEUoGfp9/ORSGgjFG+LXg+Bv/Aa
YxQqMo6xhHGBqhtdhm8+S6sAHQOOigXgPFdC4I/TiQ5vL5M3gZMa5XOCvrKOVCfcfHbpFtPMjRGT
Gfk1bi6FWUdN3Hzl57ALqF/UP6NbW0ukJOg92nOi/ZAmx6avs7s7BKNdvJFBCIZakwptFVPVo53T
SpojBHixhc+3CTKoMKrmgvRG916exv06C6HacrGf7cTGJAU1+5JxnKPz6VDOmsnK7JAU+5UucH3n
afJwkKJSpVrLdgMp/Bb5LWfn0wLWrSy+xoxL7HPYsEXpIxd0DsGxIDWL6MklGNVQTqccVktqB86N
K5JOCVecIMdti43t1quxqDFBxtnYymCNoYSBDQYnc0a0u4V8kDQvmpb2kyE1NplDU0ZMIVOLG0KD
yE1sX8rp2/5rcMA+17EWtyInm07T76H613GgLiianQPjVPIFN6FbNFxBxiaDxQcsgEuwLP2kfdjl
HyMveUxV0x140x+hF2BfM42j0O57XUvn3i3MnGmHXc3pWtTI6mL/bVPaA1V0QUHc9lldU/7hFA2y
4s7ewZT8KYgTUPL+j2Cccsz3D9QCsKFYoHFCrlOT1BZwLQRzmq+mePoDW6+Q/Ik3h5C9vkP2+tGQ
weiZVrTZHxDgzR9rqyHQblH+vnWRaJqTD7dc9++ZC8yqs0bwYWgnQy5WS0xU05YObVXMX+DIoN8d
dGj2JJVjroyXPILtLUc5QgTHHrr+XrkGhJzy02TDZe5zKY2T9yzk4uNf4Us/hBxEffs7TA6/zn6y
k94MEl5P65qPaXivBdKH4QaNKRIEUPfnELAv8u4Yq2yV/gKAJT1BO27HotzaEVM/sniFAIEljEZO
p2F0oJscP7n3mBZ8ocVLA9wYQop1Zi52Ssn/fmVeuxDe6nMGaaujjudMXY/D6mJK8FZLCwcBcXp1
AR0JMAeTCARuH7eyne3vNP3VHG2WX9+ZhxH5NEGnIqG8P1F9PcAXAj/oMCcB+0QM/6HoIawIcMqU
o3mLp9fttaBJMZOp/zrCnYwvnGwginsmm25zrXHBsBMBLoxVpKv8849NrL3700RqW6a7LLwTLP9L
vey42Sm10n+HGangLmjf/8mDeyJvseYpMUPPe3crIEkF27COKXoTfvJ/tDgAgp1AhOcwd3dSCvVO
01jHG2NPWZ5RhYhH+CiVRNdihceW1783jLP+1u8M7IU79eKE4cc9zqsqDbk/7I1zt1a0/EnRwm9o
+7+S5UJtaMKxB9/xA8DUKN3NJDotFAaO+5T/K0ynYTtF5p3iqWcaZYNyqYmJ6+1pVQYVlByPI8pg
c249Efe/q60F6Ozxb5gxMN8jZplQxURWVKFuPwqFvmyErpUfqoE+QzR6an6FUVBY25OEAU2Y4qeT
b3+vDT1IGG/sl6sDaiqmfW1H3xZWX6xhs+WZwkuMGt4xHy54VjMWjFEyQHe5IMQOEiztwUk6rNFo
xbWMm7J5kSAcs9/8j/wV/uPuVwjxjPYF2WhDxynVTIQpbjrLCDTlCYjyV+CGI+FIWRapwmEZmoM0
IVL7HQkFp0rjczwE0xNJuO1SMj2fyj97OfSDMT8DO3F+6tTfjOO8qRuWESY4mAJ6GMzSCXmdtFU7
KXGpvfrlRm3/scRi5IIYq8CeyckJeIoZREb2+47+rmrI7DtnJH2waulkeW0pnrqy1TLnzprgMcIo
wgVZKYmXKwbWSiTUn5Wb9KRjwyX4/XOio2z9kcuZCeev7yjWLnQi7am4iITpWKHTJeU07Shs+v4b
zA6Sln890Lhh7fXeCfNO47waWVXRu4Gumq2uSoZiKhOCgU7jABmwwCTfN3XA4mF90ApYLetpPsH8
O5wMtRc30JoHgAe8GNbPCh9qb/drrqoa0S9SO2/F1Z9eiPRFbT1AKNYWmqHiX06CZoJuoetLHZDR
ue8lNalnlDLjIMbWDn0PEW8KVw0RG8yT/1knPeo4nxdBQD2VQU8dP4jT9zfG4urj02XXQ+jjxgBX
HexC9J0iBSASOYmMn0Arewxfwk8PAT9Q+vQzY/JCCmgMBHle3wQHOyw2UX7ElzKKekkS1cRbq3Td
Rxu6qjK3r1sZ9GvSgEjlvc//feHb7hRnDSZRJZ6ttrmxpROgRFmqpKvQO3gXMvy9U+3zFQl3TTX9
Rd83E+GwG9guY3vXqzkDsHVe7e7spcrRc0ASsqkaYknBIG59fgg29/jVkdmmNj0sq5TikxJtqXkN
xm9tek2G7zXAHqZy1vT3DHF1ojfgCQ2gU4vKJpqDqZZMKkykoKoMGGriBtJU9iOOTYJKWuBLbogp
CIppGlRfrI8vwBJn5vZdaKIoOWsc38ezHBodV1wjeFDDyINnJKFF0GfJHZScsnprF64o0NP2P1C5
I3sKvGrZCpScRsZVV46NXE/nm8UZY1Yy5gbggyTTGIqVC9eKxPs5E1oJFLvSoUjntR8rskQ2b0oK
Fiq4h79mRyGalSlwMAowcGaB/I11hKH+2ON+w3KsiPgRKkIeXoq0KqUj82RmmJbq76hvbTcGkZFn
Zk+owVu+IY33vAkTIDk6M8FIj/ZTEDKczj6LVLWKCYygOw6Dmk6qBIlim21cUgDCqOItndhUFEUI
jz0Cs+sg0dC40+0uUiYhK1ZIE//yIPzcVans7lCSFOqFbvQXqGM2UfslyyDwN4Bc2jV1CMmR2B6a
ZxZ+D5D27syabPNZ6lRfl1D+lj0/s7jyql7uAmbWfF2GjiVjh3IqgmJAfnOTeNOIdZ9Zq1eZ2e9B
8VlFnOBPN63P5eYR3C16xbkKzaFebNsuxjagFAHYKN5DuDzGARqmB+0BYd71VeWRrnosrdjBEj3l
8zVomy3tXTlAGw/b8OcyiIx746IJd5005mN8VfzclT77JUrmR0W82z4Rkb8KjZMZgWYCnyO0nHfg
nniiWctftG/cMuJK/AP8g9d9uUH4nXfaGJ4UCm/cg8Il1WV6GRKZU46WpteYwHv21/0MtoOanf1J
z5/GJ1oqIccpZmWXEGtFPbnGT8rRcPlU3E9yVUQbsoiLcZf24jI6I0K5331+stC7g40MUD31k9w+
GiVT9YvDxuvbrhnVBCvGuwYDJkdgUMz1We+cgE7M/r+3mh8xWUIts5yAgEZY4jji+tN0M87VJQAa
955gYES+C4qsBXxUPG2oUKjB3iy84BWIGLzMx/ZKb1QoO1jbcIT+m69SHMtJtIXe28pnby9PcGjG
4Im9wfx0WAbxlb0FXb4mtKa9oXy9MuzDDNLDPhlJvu7OWCzkC5ub1FAVB7wR1gV4fhDMayjwsrpT
ptRiTc1KgygDCI/1z1g5DS9x0Y//vCUUVHJQB2sC51h+8DUWIeiOG/TU8kxFMF7X34ptJ3mIxmPF
18q14dqH4zabSXG832TDvsCFongeuKsbVLDxRs5cvDczGBBySkQ9bu4EKCZ4BembuS+3J1jIrRgX
VffbOCPnxSBCC3glAlIZQpGrJtqMTxoprcPkdRRcZmrcHlbpeiHJmpo7M0bqyJlp0Zhw7P64/r8f
+ioNQCrB+iAntFgL3jhULBbZD4GaUoouheXissSsC0tKBWSvOu0chwHxhp40gsDqhq0ZpVbGNbhl
V0UJngdfugx28U+Mbt50ITCU+BxpvMjU+LsqO9/H8H2DYtAa02xi6deNQvi2usxd63xjbavjOam/
tXgS2Md0DmDIt5LQov/3k0fPUbsLgKo7rhc7bvP346oyRKScBWtAnaaeOVSi1Ga3X40LGBbvnaWz
+BYRJyKqf2dcswVWXcIiLlSZpvRirpO1C3qXDnS1IBvp/xVbgwFhPojW3OmLtmGV2Yruwmag3dzh
VX3+v8b1uFDg7AsqBtLwyEq9k5k1N78ctuKSbNyITrbnkSzXRpaLj/g5t86Dl/Yfa+zrvmZk3DOx
P+cMMJMeDWELMl6fcVULGf1bvIcVzpc91o+YgJoA0nmczcQkyUF4ARtthx+BenboINZ7TCDxkdEq
rfNMpafYkRV+hRaiFjvxYDpf2QksttMqHWck1m6/8OMX6nOXAULg+vQe6N/BT4JxDPLnHxw2gFL3
PvROdH2oEVWCiF8/tCjeB2RxEP60u5DpAHq7cRy7JN48tXMqkONF5MkgPKjBOX4Da/+g3OyiWLO5
eRlMTMEoDNfzZ4ZHMmhGaghUeqKZ8BLWVaW3orTPD1EFiKwGw88L7nyIktVn0LNfURPHuXGlmBBg
dbswyfX/N8kklFlYtRBVbLBG173lFj29hzNkPdDKrvoYYiKQST5Tv2DUUrAmWiAqQMpLyIfT2AJq
kgUXPhd6yEcF4Bm0CCbWQZm8AowNyrN6/7osqocq5YcN9nfI1ppxmuYOhE7Ael3SEVviGdy+WpKO
rtgaCttv8w9p+5ZULK43UivsWqtpzDmxPoFLjQV6N5W2JEf6G0jU6gPcj0AjAVnfuUHOp3V1n6aC
QBbtvG2BDqA/PedyQAUZ+Lsz1C0YrnDhxBr3IDVpU2oQgyWXaNv9q/l17a7OpxTOb/n9Rfx+EPpE
HQ+WwP8r1UEQE3ViknLAUsYtxnomcRVIH5efJLIhTJVLC2JT93gb/dxO1d+xwjkzWJvo0LXP1cs/
6lRM8DGmTT8DDTQcUFRRHRmoHGWwBLMYD6xOGMfibNq5k/6QQdhFOjLbrOBILC+z1yTEkJ4foCxi
D0cJA+gOpdXXeIfWLns2TkZdEbTaZSp05KSoLz5y9s26sGV78pRkaCtRVkkndiky9TxBKxwEu0dW
xZcUDBKFBDmjtrkN8Gv4VAo3ieV9YJ91jXAq5p4QYFLesjjMsvEasOT+I1e9KDQz2UZLMJgQtXlx
JinsBYH8Ka3UUOxBFY6KvFEsQ9w+cWR1hZBSMhYOestOHdp1wBAhNRiqU0LcsV7MGL4jnS5u36to
+byO20ppg9bbDgnar1+9f2C69r10U7ikTQjc0EfuyUJRb4+7/2AFidBx/VwQiWNoQEC5UzdLVNGX
KLn3a2N/dhCXJh85BbpqN/z0mKRjVW57U0xcI5LJC4v2E+is6czbX2ms14Fb4MYQfNEzzRW5hAmL
FY/q+gnsjCPy2cMjnJfGQZzezQUWdjXJ5J6GII3DsQrhrBYnN8VkTUQwFibCFzhElVCwWCz0ttCY
pD3Yj2kiuFnKDVtCSS9PeoR1AoDjJWf5vImJSRIinjJrirVGNhVTv4ogZgfZroLtt/JLb4cn6gN1
UyQ1qD3/k1MCOKef3vEC0atVpeHrzTX5zfNt0q+n6TY5pzT/u79QIIqNAyrVJ1aEzKGEcBXw+RC/
OjoxC5jbxFEwlk5TplDDkRbQZgSqgIFJyp/TuG9R57ZCk1JEE8ZcnVQUZusUKsv8wLWlPwlZSUCm
gil0vE2hxoDl6cnB7pqJVpQTVR+OoVDZTF6iU/2DtKOslgnuumgqLsD1i5sYk9dugaDmwKSfqH36
ZhnaPM5dI03Tfr/Fgg9b3ofmW1cXsYX+Yzx9CWEZJ+ntn8WfPOMF5rW54iuV4OCh06yGAsbLJ2KZ
QeT3V2jAaXij6mO/RujtMRVJWj2VLYdie9ZkRseloeBw7IG+vK/cnLNStS6+M9JJjOCrVNN+VSJv
cHrSNSzfQCuf7MvqPE6OlbCq7YLwzpDM6jRdB7nIeFBRa2lhgA6Im03zRthw/oktsxVfPewsmw+O
n6eYi36MYb+NmK8Nc3QElm7Q5K7fiD5lkzrYiW0XVecdpu9SSOUykKvFsUZo2rorPSS8toGTeWNr
lRjw5PLakjHvE5CeD9FBlXYPmd6OVav2Wy0/yw/fsCeWbojkscs0jA9QWLFEVZPKuWgaFNGqEpBP
FCTKnVVSHHVRa2GOZsurpEV+M8LW7GZt6zbpX3vbaGBzA70yxY22dnUTJc8o0PQDjUf4E/R33d+I
ZAYSY2pTdcanIHRNGL0SJYAkGfmtOFlLrcjJ4YU5Yy0QMZOyHU9fIQ3roW+yAl24bOHyBd1YJwNT
K2WMveWFh4NviIFYAD3LhVMCNynFJ4n2MNYspw5o5oHFO3o9GA3JISCXv8ALz5oXXpVbJhfgw/Zc
qT0nrfDd4tpQBWi44r0UJGST1h8j0GIc0M3JkQRTnz81WqvzSsuBcuI39p+yvx94gLILe5ZMnrrT
WlAguXkEHMpb2HkUv5UQ1XF1zxEYZR9cvd3ms39hpUJjnpk7J1hlNR7TDF02v4hxsGYLT/Po/c2E
//IHbhy9a7RSbZUQctokLHtgykBxLGnkdEcFWxOTnsxjwGFBpjouRW4/7eo/pLlUgx+gY9cGU2qs
Z8LiVR6mHMVdUfMSpaOA76W9zpfvvypU06Gz+SGnSrz1D5oAnmUf1oLcrQ5+A0Tzk33OjkneEqSu
Lzyh7rul/R6U5pVcSh3q94/ou3C77XrZgrDBEIFaxqlhO8ci04Ns4Du9j1myCNw0n5iZPgK3KVCL
8sLlDFJjW5zj7lRBkfb91pZu185lSpTJrChUmeSNqLHkb28d/LbGy9CPXVRYl+HtB4ckKfnOUU37
0efo6uK5Qb6zPr5WQOmslxLmCyQgJiyafFbS6nrxqR/OQ7y4m/2yP6fwYbpP2IkyqTMei3+asuUK
5JBYOZiNtGjOGYNBMex3NILy4ElExTzsmdQMFfjBiuy1d3B4z+ocbl7773f9IKZSHoPFR+UJ9Hz+
+trOd0c6M4ZLIygCxBK2BI022/lSVwnYFl9WoW8wsdSR5HWCG0db8CRTkDMSR/tPMRZwwWx89uKT
KutuvaFqeqZO6dN82wyTyg02lgvADmBbH1bl06HWpZZ3E/eigtvZmhtD0/DIH5d089OdNvWjcDE4
X3Uvd7x/BCCZcH27P58UwvsP9lLA+xIJnYrRUKU9K9PpU1md4u1lFYbmyLQkrTPHJUsr10JO4lv3
EYBkKN1gZCzFt58828QlQho7IQ9KHnPGuIB9vmJRmIN7cOyzU1oi4FNYBCIZazLh9joQgcKdAQ1t
OoYJ0emECe9TLUBFt7oaL/Q1cHfAZO47EXAxGxxVHQz+iT31CAg5kU9zOX6vybvjZJyC3a99SeOY
TJojnbpHk228zd9YOvqt4Bq1N5GUPAiixcweWj7ZCBdpC2DLsHwO5ClRr0uyA9m5G8R8YSrrXK3d
V2qnaL4qCT+AHuu4rb6FVru3hCN6H5/aQANFBmD0u6rvNwFIOaVqHndJXy4gxFl6hLC4T45l+NNx
T6tEUjotXcFVGyyt95iOlgbjXO+tJQ3GF/Hy3LAZI7QFAokXF0gp8xy7Rkb0jKxP04dy3WAVz5nP
bnSXO3PWRT03JDg9V0YyB49D+5AB6tElL1OtKLbgSRCgIWWA/P8XEtehHWZ4YKeHkLwWiiATajeo
YKSVWqORbEH/NkyR+4tLKNXsSWJn9tsqVPHJrfnFf7hnYFnPuhGJe+uAlJPNvNqG6IXX9xFVPw2/
wBS0ysb0WTe7vow0LtCu0k9NIvYQnbqNX/DEbzUddN0omtvk7Px6WU7v0ry7S0h5r1zTsB4/WLTp
maCFpQr4g73INwZYtGmyfPmspnA//SBrwgigwp3se6DuQCikS1JzlFVmpNtwIj/JfEJrdWm1pF5S
XxbidctW8RLvdnS96rebE9kXDStB3ItWbH6GTv2peqNrhir1VPnGIIeNWqCcvyFW/sLjKyfjXDNr
KoDWUlftvBjB1+5FnvVgLe9ErY0Bg2x3M78Dyb5dIPq2hSE64izpjADdKNPo7qLqn02zYmZgbZPz
VLmKGVQWadNQXiXicYs1ox6dPJ4TzoII0B3mveGEuE0jE3hvRoOoIkJL9CL/W/LZ3EjFg7Yt33OD
MMAbTojRQLvcQohLC8OAlKIi4Vu5vj2Ltv28l5JgrDPtwXUYqEm7ecL7JbcTMe1WvmiRMPckIk+F
Mzm1sf+bk3IcaNSsRB2/UX2c5/3ViyL1G3ytvRVsb8PVEdRoq9F44Al0pNWmjYQZeepGmvQFMGQj
0aAs0vJwEYKyxThRue0JIo2tSal574/yd4QASo6PP00dPmXYD3q5e68vzeIwkjd9mjFtXx9GcyIk
RA2jT2M5FjjrVjsbq0T9PPP9QhUgvBXeY7W4kQ3q6Gad0NE5enOtqj2BIu8pywIKsSANP5f1dWuf
NVj6LA/QSimUal0cNmOcj9nfv/rnfeQdxWmixaW/4FYIgVsUaZYP8gKgQI7hS2665jtli8O+X0Jn
J3WXPv3AdoxooqIm6UwluXgJhBqtLouVAZnG8KYkWLKGA+eQlufiRAssq9xT4GCxTC2RGOMlp2HO
vsfJmsPXC4k6M7hgSrLaQbm2FjvXqyU1V/Vfxxe3aumJ4hYXi+SaCK2R0OkKozHprcwJGjqYeI1I
+e05XZsOxGPTXQwVVF98WtrWE5KosbOYc3LcSTeN6Mba6tGqv9NHmDOThANFvOlcviWN4nsWM6ax
x7IGbkdM5hi7k4lVChBl7isepBLbI1uoZnhzeTNK8OhU6larDaSQ7JYNhRW49UkeZluQKK5xSg+s
JKjiFjjhpc9xCNSTmT4iDrIh3N04yWnIxxVFid5ZxFt+AnKSQvdBJUve16FTZX4jjBj0g/zr2MMJ
a9oeADtf2xWL3JyG/+ddiAiKsfTXnoSex5EY2AZEv9blxKuP4u7Vr2efngAGqVpuwhcmVF2ABGkV
tojw4MzV0zHn8SnIuImqf1MKTp0pKl7W9kn2FX+3/15XLqMgbD3l3L94jtEKxF0o+QkZsWdpKrY8
SWBU+a/DM467QVXb8F2AiIKzoxG1svECbtaaSUrJ3mNJ3d6rslT9fsCV0RHj9VH9MtbD5vI1CUay
0eQy6erpr7o7BoDnHHmNHLJnDXsZKhAwap0i9NxPO+UYeTj5XQIEELigcqrPpMm8tXkugHlZH+j/
WQE5PaMoWzhIDXRA8Ae87wBvF2G9n5sbmriN6Iff5cIfLOEVE9HaEq2MO2SEOEmfmTyB8VubqhRD
4CQNMtC4Q8hMbHdccMwh/tlLln3cYDk4t+0f9uQIhDbOVtDQx3cr+oSq1c15J8ggchmzG36GwJzm
CBlKaP2WMACqa4v+pl0KhmLX7GH2yXH0yvZuE0kLRr82SH4crhsQweXtRsv0FwFEaEyYwxqKrJH7
t97WskOnbWiaygpubkuUHsZVHN6z6UpZHTx8hmSh4vB+Xi8mOB5tpY/kCBrGqKMo416b0FzZMbNR
H2Tvp0aZHdIqcXK47y+OuIofGYI04h1NEQE8QWUsoVClse7tuP0quPGSJ551PkNPyYcRiYqxrwcr
n+zDSm23EiiGI2FbRwWEC7vBgNAd/1Oqwo65ROsjRNlYcmTVdO8lKKZT9icvECyvfxjHIB9ojCCW
iWqL705dwv2c867CykY89tbObmpKQ4At1Jn8rCPtevW0YlPa17jy8O1kcXi2zxPgMObCVxsHfY5J
GNnkI4dFxuwqUsdf6oG+SihyNFnUVOD6dOyAUrg9vOEppUNDZtzOTDZ0TdHeUOGvexeuhYBjguJn
9VCFvMdN7EDBeC9rB0X3BNUnPIGtp5SRcGX5Ri+anNY8er+E85hw9apODG8GFvCT837ydcUmDRVQ
YjFSRtTFNOfhSOEjb6TdkSX3eplDQ2wZbV24DGauWmatd7r8vIdSTKyBepN+dgwaOv51qz+Fa7b1
QiZqAYmBrnlomCqHMUiIh0uPZzFQldDhaJK2fSYikNk1v2S35OhhSbccVcrj6fc5Ly18uAMfxHWG
eGmhVQBj5ezdmACLocaWGd5Bzn7zcTU+yjIdlZz71V5FMEa0sTIm+3yvkyB8p47sWOtFeN3JuRfZ
xonzCE1U8fm/hAlX/VyEHSjiYFcxtsYZtJ3ZCibeqhFVnLYrCJUj7KRxQxIzvPMsioPuAGakCC2l
EXfx1MwooZpThtmgW32GBnD4+f/L4sWeF4GGrJLeWy62qN2kHT8+8R1ssytF0Z5WnfoiMAaQybpd
HYvCXMfXzDs/Ol/Ihcv9EubIsELZg0dymiqwPE4lizbSaeTznpXljaUMTtcOJOE1DYmGs6dHQvek
Ar03+4NWP6QomBghQSFiYd0SPzt05X5Z/3stYaSFBE6KefQRFBFQEIuejh+64EAGFwPEWmCsin/e
tG9xjcLoMyqaaUrkMeIuxr/UOFiMkYJVgInQtnVsKb552Tz/3vNLai2NRqI7ndLSJKw5pSzHRX2L
XMuKND0Uxpnbg8S3HcUmcv2F/m477FyUJXKttxDsjrEPWZoHzfKHet3fcD6e4Xz4r8tmoRaxQNEm
0gm/fb8KVsLRAd24vauCC2D0PcZovyq1F0ADhYIu0sRUQn9Tg/4KskdEsgKLueuAvmG38KGZnMdV
DdKbZT0Y3zykB6KebtX+/9vnM8fWOnaiC6ltUzfqGNrCVUgyNRt9LuPnsLzfs3PYR37s3ekp6DEi
YYksMHap/wpe409TXlhHaulIj+DZca+4S326VVumuH1dM7uKviHTmzcHQRqr5VeQ0Atgl0i4La17
hhwOoQP7XBq8K+4YuZ7klp/k99lfNV2vEY/IS6Xvqv5luuaC8IvjroU9Sdxq6DEN+YyWaxfxwS1E
oSZfW9kZc+DN+fGngcvS/8+/nnEtyBFyTwufKLK3N7OC5VEErYrK2+IZ7YmQv3xzvB0Aj1q1qdko
d/ONeMzNBb0L0rvPTiHR025NARYQwP/MX7vbyoxxtW7d1TgDOmAVOj6eyucMlc43tEZ+u9QIq9mq
gzWFzTWqrA0VHH4/k8ZoVmK/CyselE3KGsw0sReppvYmafMOeTK3GQqs2JtJIPvoovZX3LKGwsdI
z6T2el6O/5D03hatp8kLTcU0eaTzqHlhEW9OZ1uoLJxqXTgE2F6srWBBm0EH+mKGKTFwaHvrb2p1
Uu5eCOVDM+lAGJiC7m8UFj8krUY+ctMsDmjvWEBB1azIElE1Pm9VdUFdTgCtjm5anpYswGe9EWRw
iK+nGbraoyrPvWIk92qztuXwoHc/RZAdQyCtnGac0K4R9+JzkY2AEomq+mNyc2X+h7lNUFbiQaNV
YEQ/9nzwIkk2KFgOd41ubT49Ln8yL3n0W3n2im4IJv95T7dbnKmFyDHEIe+pLLU+9ciorqDQ7QUg
cEdgHkLeMloeL82xjQYEBBzzzWyYKahA0HQiPfsgNrjkYr1v38mow1HaqWO94TW7mVgSbRH7lDJW
nlmMTBX1I9xmN6n+opc6bRI3q0WZRDAcRljTtw6MRgeiUK+oYn+7AIQ7Znc7YhODLQqCujF3seVc
tVABvq9oaiySGV8Bwv2+a11Bdt+Vo0Pj+e/JHQ9QreEI2s7gJRMqcLdgsnsUol+UsrIiH05PjjKC
WcZBYYLH+XU1TqjAmBcbFrSbB8FDQqmUpaCliR4V57kFtOKA7mQpy7RuOSWXn1bQ5akxIQt5PhBp
6EgWDRgsiTR2OjNuBMLPdyH1kqOFi5KLMzzKal71LTqzJ+oXsJ5/N8rDyxewcUe9nXPs6xLX2ClE
q5qL65GDljvdcNhMM57t/hPxW2iMPJq329avExouMCPAYVPr395/hpVMJeumtOjItppFE1FQkKVJ
2JYDUawhPG2Drw3F+oJ2OsfW/CDSbdBs/dpH/ORri32q0xoWoSmT8JS1fCKZzuH/SaVwzi3Nc9ba
QBn9s36diC438vHGxz2JI9RsUlYXVPTFEKOQuIpUz5RaJgX7Sk4vENQAgUSB+FKR7x/rmJJhCjTK
sdk8unEqeMIY6aKgoS5p2kbtRF1bj/VeuMR9g7DGCHLhJzx1DSCmcshceFup9T4Mm/WT6PFSrR5K
rkFQNj3lEZI4BO2DdO0JgaF3bF2hUDk7RoiXV0xYiT+gKklPgLFm2+r+B7WxmbEstBDuFaT9eeV1
AUx6k2N48TowsDfIz6LDVAQhHq3OMr21aAEQRDov7NOX8+6GkjLg0mV7pitBu3VIrgFMq4mCzpPL
q35yk9HE2hyO9Zy0tQ2GrVSOL+zh/JC3/sb1wgZ8P9bJAHEGrUVPcY3X4vucDIqnBuMxOQXtYDHW
nRIJ6HAnY0J64gmTbXHMOEFwdOEyUgX73O8qP9t/9ifbHx3LoGAJx32vmmUnK+FhGvAc1mAh+7Y0
eP8oGr7OhcBeW9rjTRyQ+BXwnphq3D84klK6yqIDWUC4VFYE4FTpFHhYUDnWyreLqwCKLOFlt/by
d57fR9d4+t1DSJ6FQqHKvpLe2yISU3TmK+SXtDeGP3kzFnIMNPQQABYxwSkrIDIHj7uU59X4hcBy
vBH7GmqxADk8KKgA3yVbHOxPaThgFu48IHTOi8ONniPIm5wk4MjwZSHQ9VXttxnXaZZhH3RARlm6
pRaQIWNvEXf0A2vENliFiQ+kre814w42UvZNWnRak5GZhgxMW/khOOPbWCpiY508JJUra+sAKC3m
pIji37RoUSXNgXSij43K3hnnDGUzj7H7h1Ar2qLWZclZ8LqLU9G7m9Onuam9asLU6VBcU9jY4HWi
9eC8vlvnOLAWfNZDjpy2OAmCiaFYha8uAcNm56Xv1HJeOAvevH5lpr25Ibb7zXetBeNPVlqJ2QNS
6bql6Ww1qz2DRn7W12XyKezkV6xxoYTbYEWCq612EMBsHXkDPrApT2LBsGtDnytzEcHrC2EaA1gM
psqLd0+0McmO4IamSd6vDi5PjaD7RButDyogqQsTcXvKBEADczUVbmvbsc6camKdVKmkyOjNvXhN
M2qnbJZZmHwKm+3iNcT74/VRkEHV1hm0VGIVbdkh3+WJX8Q35Jb5B9nKiJCUEhyNqxhHELWzwTFO
zWNqwQqA2x+Frp2pLUH4nJXDSSofmT1A5uSWw5dOdv6F5n+/DCzSg5Gq0NT1XWG2icq5Mk6i/wZU
3Yjtpl++f8mOGjfnD497D/PsoSA6mqoovaDB+A1+VSlnNP9ee6v4JynHflaOf2VuDC2J4gLIB1eN
ipDumibOYka6vEs/1jF+E03RPrwlm7TkwGa+ZkrDqK2zHiZWrvbTLZGkzEn44prh6npk5G42vMrt
431Bc5KcgoXSICgE6iU/vqwTOaec7wOUrDv/VsxPaP2t9b8P+MIPWTMbzrbFYvRuTKRHU/tuq6BH
Q9ZpchW3WgOqSjoI6OpRgE2rxcF1Qhd//PJGtw/EoMhm7JZOOhVT2XINHZZGxGPWIRabHImLgtS/
VzJbqsH4bc3pUV9nwA4Mf5YlkeBiXmq+hmqPBb3B5gonDmoVwqFbJWp5wwThfAT7/6Utfjsg3rnu
IgATbISBE97pSm6QsZJ82vH8ge4AUOVmqwUlR5GCkf2yur+fomMNnbqV/VLe0TEbokqBQPJhCh1V
qhg1F99vj5SEH0x7bz3oCfHylHGM81FJnsQW94VodGCSf6XxmfI/J6dU06HDzyFkMFtcokAIEl5H
Hrb04Zh/VPcHCAAJ0K97ArMOHQCHjdovQCZHzZbKByFxqkMEJEPDIMhbZ0jhJ+vAKYdlOpMiRifc
KJdXFdKfBYD4/fkRaHQomFAzaXdtqu2odf23DVg9MJRjAbeiNmKGeqDHpuntWT1UUQvOWi9jr+hb
SDxDPMqnizfkCK7vqE0buFeSPCk2Z5fgIBTrA7evHyQM9SgZF3EDpOX4O3nhYHaxPzR7ZVpVfLRa
kfKlOy1s+HmQ+6kZF8fjvOtpysBv2w5jQeey/mlQwfPIg0yQZEn/6GgsmMA61bLK2AWuBlbdlp6+
ywDdm11B80xr00Hnugy7/lcwGxA0pgJb3fUPF7gVtXYu2QKmR9rlqKXox3fV9M+PsxteSazU3RKa
hFw4bFEfsO8EzudhTJ2Y2BRpYHr3EQH0JwYqU+pRmT3YpoyaM3kxkvlqocNr4xyY6KQ/4Bwf2v17
5p7hbyBPu97yr0vK1U3gjfKKSx+dui9iyfURScphaBZFNE26UofZFABDJMTmogWCm5wsipFYeWLH
gKZzVArEj2aTI7XR8BwpOjqVrx+wj3UbdeGE6NQRan4SEzl0XsqNkYwfONrf3jKdjEY+4Kj/YYEm
6oZlk7/SPMtl4JwYHjR9nz90ZX/2GeV/UQ6Di/Q+qtgyC7thW4brYnsxcZPsRYJc+U/Y+BjuUd+q
IZQ3Tu4IN6IYi7ktcuLTpTC/J3JDHA6EKGFLqDx6FKDAI/lx59fvJjjFsHDAsjX/nCgJmNI3LabS
11l4JfJ4JjRp26dFgQVYhhBOYn0vt+2Dhv7z9c1oewxZlz73dFidqroDLTBQlNp6VbYaYlgZqxPO
cQR8f7awx2RNpJlgmfuTfNiX5F50PbUzQR1hfBJ+gWjkqSt3pMbm9ytrq70uYgDqflGBpJ2kLuWL
2poZiswG4gZL1bBYcA4ixhVbCve6jCI+RS/L1fecbwfHDQUwrlRYA+jXiQBXOtXEqp4WvUZf7wI+
uUDttiyqg1/ZCgUnTXpBp7zCqyipIS4T/qkfsHa71/PbECrn8hhnaxrFEaaO/M4BYHndBwAZwurB
xU8tSfqhWUKGR3k0t4UejW9Jdu5jTPXZWAkEnAw1dQt3Vwr5+HYxIWgnBuNIPUaRA5wpHortWRO2
83HhY+FAGGSj4JMbQC4vM+KynpWDPiR8y1u56N+4tPVXG3adbozy8a3oso9t6HQYbP4aatpO/FWd
97sUAVerW9XTlm3I9vIh37ZcMbHg3mAhkiliu73IaXYa4mh9b1nenPCgTcFX7X0txOMUJ2K+K4a2
tNhmbSkF1H+F9/LxefoYUbY6Q9qCea4uTWA0BoQ/Zr3xGHi+iyF6qs+MFBPFjZQwFjQaar/jZ08H
ot5VrXetFzkzaAuMoGlHVdJRVUu6W5ikcc0elzjUOOSsz2TUrhIdDTFXQO8I/pIo4RyvapNYvPkF
QkFsm3ep5owCdfC4bgj18Qzz07qB46eoE2PG8BaVMHkHYJ1qYuJ2Np68EJPBcpAtHFna569kOHeq
jo7q0O31eA+Ravx66Usxg5a2iEwUpcJq964gXbNmewQlTfLLjLQwl+2GzTnjH1jdBaC8CfCWKWgB
CU2GCJWKe5FSBTaD8aGRj3kF9dlAmjIt7pQTBp1dwB/bbj4WdWS/N65ctFW9gPuxjjRgZoYeXGa+
os0//6T8zx14xiXlYO86hGWdc7oPN/x29ENDq4Ki7lwgpcAyz8uwtW2uXbSLAAsCTRsTlsCDwTqi
6g01yHzLx7oFTrs30le1Opo1cqM9zcoCcTRB5wd/60jHEknZEamEKGOkoaXnl/BuoIijPGsZ1s1Y
YFIF3zrI74FY1EVj9hMy94yq16+J0EhpmEFr7Jm0wt9KC72BgEw4H80IBawu8BAZ3o1+aKwWLrJ8
w4pWgzlR14v6xc/fVeensn1ym7cQb2jb5P/2q7qVu0H/re9wzHWSsdKpcSPJPZrfyxutERDJNmTJ
S/FPmzoAF6SypOrGaSZTw94Ox/H9xu12fMQAG2VGsP/F1DdRQnvJTT0xsOGafdtiTSggDK81qPBy
rwJ3JMbUWNkzAIVl05mLslMAbli+q4m47uBDnMBBJFJp3gsf8hCOlcwG8bh0D9MEK3eizxUW1oR2
HZ7wst86ObjH/np3eQV6eyYN4vr2915M+IdBu9wuu3cA0vxC1f9q+D7++smtuaEV/oa0tfHf+q6a
WZNapfxskfvuKXEORWKlxe8oKF2JvJ7QAp349XMyybf9lWHmZGozHVVBnbUzp27CVAjrnOlKRbIl
HNySioKm+krXoeuIJWzbqCyy2dtNk9RYYV/D7YWAAq31P6Mv6LCeimEQN778u5Ds0zz1E7GrPZDZ
t6Uc70axvpuuER2KB61s6Bf8GxZl3Xq48Xr5SzogxCiZ8b76DbywZv7YVP+aFNxrp/W8UisspMeh
TvFWQeynOBhLWqgqkZKSF7snunAi2JeN+YTbRyTtv0cDCyIJQfDgL2C09vSyMRUtxFeRlDFZpWz+
AFgQIfyl9XS6XgEFnAW9zoTAhfJrmOCwaKOJsrv8MapFXGuGtoGQ6G5ykdpd8fLzWOBCzP4Vrdcb
viG7Lu/iptURDtVQg4aDVR5SagOHYnLW0fnL5CqHlXB0BkE9vrg638aT62IjwklnrhyblguP4V2a
0cqGiXbUC7Z4LuHKer0fIPg48lLsnPT23vJphDA4i6SSmzv0KaezTA8aOs6zwZf2zCkl5NyFbHK6
WXvhFxgnRTXBy8tuYSu797kbPj/tZsm998jLZPJNdJP8LnS7jwBinbn9q0hvVlF1Z3lm+ozU3hhg
ETB4xAWtLT9XBsf/U6fXQMzcCTlQYwfA4pKHcQ3EdZ1mNefIaYWFAl248QmwPMcdLDv8PZrPaYHe
kgdDmpWH6y8kyHVwXs7WNV2swm2/4LqvqPXbp/0oe2P760OYWcSGSyGFIjdJORvC1DgwXHr55VaL
+zDMdeEHgTBcEF+JY/P+UJGf3npICb3T8/cRQU7/LjSOVunLFZ2i8mmUc0q3gJKa9WnyfyEbYqxx
J37Th+SVbu1SCETqfvGsVG4zqvS3uVMtq72/d1VRCsQ+n3zfcpIMnPNb6hxJ9piIp818Qjqlfv3N
gLbAcswegmYHXzx0R+mBAjn/0myjaabRdHO4RJKAQzg0p/LDM7S9f8eWvFhi4r7C1GqPMqIpc+Wf
pJyLCvQPu0LrQibmNMLNQ/rgShL94G8MoCdurqi+ZKhCapzQOsTLtT20gvwliZmhWIZ8vATGS6yr
k8kjNGc3RHDAIFrknKd2LtfN06KDg/ZrlLMVhzZTm72RK4xnQ6SUv6vffPe7Rv3rqNM2ZfPTqSjY
l9Eh7Yz1mqAg3Rl7EGZMeuF2eiPxOY8FR/f/YvIXCU8UDfBijNnqNgy7hYGF8I13lepAWROcyCB2
xrfc7qeLMZVyhuMpoIJJGfj4m8SCxP7mSTE44Ffns7sJsg8qypzg4w8enuW5bGUAmGx3dzhmU90D
qfHhQ3NsFi3hbB3lWlt1KOYI3vp2KmNPKk7KdicSrZ4RM7rEHd0i7oefE2Dk8T9WXU1s5S4O3vFw
6tKp6FOSRu/w20ntz+ZbtIPei6NBzyrCZHd2sh+Nz0SRxcLjc5nsYIZKRCSe/zF5bjX89TpKXo7V
cgAXteOXG5CCGPniF9rBfiEq+0BWttHAtcIM8Rp1rdPpqY3UkAeiPJjZ6k3xO5K4VvwDJc26BpmK
0CZNS9tJJNz43K1kgqKFVLbAmzzJwK40c0E1eF9n7KB93uk6swzYL64jDt22uiuSGRco8fr8xKr0
LYSkmA5xFcEueOmi9YhxEp6CzdPw9bASnWXRlTRKuCFhJdNDJgw4SrePpCrGHL/C1V9PCUHH8UTa
FzG716ONSM7+xOQo5fRaDZvOmJK1BA22ZU1Elbe8pdqOVBW9RT16GjO3WYp9mrn4EEj95xx+qgKL
Pt8ljHoOImNECEWj3rUkgDHRbGcZWqLBLazMTav7JWbpESQq9EADDVgtCokWp33ARBqN+W9wLCzM
Hj+yVBuHMWBmdZge/TkF521RVogSzJQsMiaETjjBJHenG7pvOLNbPFY/TjHqd14lceWapQco5hLv
k8gMOQG+KB++HgzCpJA3S63pDMV9FHC8POV7XRar73jseX5RH+ah554OxXe9uqRiy6BMtNhfETPr
bUr/94JI3UrFY85atgE1tk8kkk3TpNjEEkXQADPrEXPT+hK95aul6SyWM1iH9IJvPe/FDfJpTyGk
xZ3P9XSbsXy3NzRBtfit5fRJ1ticnGnW1l3hG2LnzmEmhIetab3tJuiYRL44IpBCAzGyuhU2ld6i
tItkXEyVRH/82nS8oc8UjazVzusvAxLolFdCgjXLWZj0eNVstl4MuHupEl22kAErkGWlItTHW7Ll
GINRTi4KKbjyWaCj4UMvqOzifZJHHoIEHcyjNrr7OZDqp8QsPjrh7LNfrdwldNh7Mb11u0huWdz1
mQL8dkinN3fhyx6+lzFQCoikIySxZhmnoGAbyMJFhkb7Q2R1NaV7fZoTmVLdW0n0paKrB4vEF7Vp
Z3ZQg1EUWBnQ4a+irMTFd5an+dq3yoWP0pa2N34OzPAvKlZF/jpCkHWG4sz21tBmXVDH9gKFy5c5
J7JdFwvJgnOXJ9Ferx507K4EIaYk8SYJfzIqHqoUJRwA2R2xBvghN3Sms0wgSlYxRwHMOAZAyoUz
NqLUTIge7ZKsW74Y3dOGiyN6rMS6wkUiLVKnImxxZjhPDmUvSKuFlaDZ/gy8WSTSclOEmqYqTQsm
C0f3+HhxLJUEkSARpFqo120hxGKvdWZf9df5rGk05PFfgO4ux/HGodMzl3fPy1r8ocCRUkdz2vHY
qIONYJt9k1hz5xIlWvHE1jsHgiTl8vseT6QNJpOgljZUkbYGs7peGQYNs8R1HgPVzqHFAvJxhJND
m9I8lmk2x3uZ3BRMrwkeBT8YuiwjhKTbySXQdFoZ2k6YJaNm8xKRcmta0vfwdGfxCmh6LEsjc4ng
qKHXDWBjRmYEtGR7/yQfWJaxN9FSfCTgRFjkrCpWHxuBN/q3CT7OjiH3GbnlD8rXnpMbQLll1FFZ
LeXTiO8V2+XXDkcmPHzMOMusYHwcyGikT6I/kOxzDR6FQoN16xAsGLwQpPqFqu6EM1YXPRxe+Mvn
56m6ZNw32PybhibmaVagfNVXa5slkwAVTWfnZ51YuYJv7XIgc7aNxMeyVJocIj70ovzlaN/KyE00
yLE1ANog4Uf78WBhGMFuYPc3lzx+K20j37tm64MzbtTcQE7uJcAt/Sgeno6jiK3lBQw5vU96JtdS
QOKh8dchy+iIL/9jhmgR+NpHmXGxNGrNT39cK8Wh0D61uiM0YnhVqHaJCZzz7gq5LRmqL3KNDVY/
TDIAVp07BZUCuI2/XbCCfGLSkHx/b3kqEXvljKERduczdMrBa68RxaDlpCOYNT9a7LgUuTcCSa9L
/egPMchAnJBbD0ZnXCDf3jLKTdF6ttPcC95HPLvnbN8+c8RXPVGlwLhBkvj/j8/hs9Op+gqPqsvr
akLRgRObSPRO/WosZUekJk7iF6br1I9nS4CV4RvDlgKMsXDod/HDLO9YnoGNO53CChJoBGNB+iRH
BdXfstuFhdyattTtXmP7Ugwa31l/Vmz09iW7ZcDGU6SeEcGTEMMmjXZ4EHz89SUhgC/sZ9+/Q08C
KDo43n8oHnpAtZKqv8Z9di66/gYfG6QEFHnUEa0s9jaCXfFAIFM2V3Si8X7M6odq4mRu5/tdrQWc
ReSa5v9x5I6K7xDdGrQeZ0XeX7yYK367BXVf6u5yXmH2q5l8cQZpwPSf+QEZPr3MPEmPVupm5tUC
4m5UY8OalknBe4okfhkVK5UPyEsvk68B9/qgO1uiTe/CZt4erWl4prvaV4Kjm/rxsaMxtlcRcRMP
28djBO02zVDS3JITujr4UWgcANdsEtGYjIjtMWczPDyhmOM8nQWcOwCirTzzoe5EQO9yEP4ese6/
ZPatqQWevoP815L7z3J+NMdLtBCGoO4giZSXTPoefrhkYtwRs0JclkOsK1wvKYvlV3/aqfdFAclU
xD4XyEuA0RmKjfrXTzLlPFaFsIm6x/uGjXK1gJAEWpxmI272PfxAKvz0L4lSW6Mg15FUr0Ikf1gL
29IXbd3qspqWhP2Zj5efnJDA0PX0Gt870bWXQA+JjbMGGEcE//aemidWzKHFS8zUF1m9aIhmjMyT
dyTvGQuN9rptybDL2/7MMW2R9t/1n8DKba7LsSCQk99U9mSFli/UgeOaIn9yQbSwVObKn8TFABlI
ZRxsp75q57zRccyjs79s1g/3eypBnzwcMWTISo47b3w82rRQ3mKe9hPJv5vCE8MvsjhIAC4ssYg6
xNncb2nndXD7e9F724cOnWpKC9w8ccrXNu6IRugzEQIqrLmJxICVObwC8cVZF29PCcaaRGVSQOwC
uYkmskFSY8K6rLKgKeN8TVAmV+o9cBdXBZZNV8NtcD5AFwTl6Xf5rhFEZf+Em6hUzJf4Tvf7V37r
j2oM3ioap7l8DuWhMoJidgJmS7hXfHjVp99+YnIHvjBImLSKMuKVa1hK/0MOrv8NYc6wYNwEII6w
YCUiwOV/jZzYXB+d9nus4DynbdGV+ow48LozMo+IjYi1bP5wVGKzCpDdPz93BQY9sVccVnTbq8PM
AhJAQ5/c6xIr+pmWlKlJ5Eu6rUV+POWMOIxDQvdgc/Io0HlHwfwiTa2ucGpuux5dNUBcI38fXkyb
nHrLMnaCqGisE3bFG0qGD9Ta5moZAe2rpQ/tzNc08fsyFDL7kCfmnQxWMxsW+4n2QA5L1j4ci5xI
Wf+HfL9a69WyqAO1Q3PB24wKtOKSPdmOA9wbwhma74AqD4Pc5v3PvHw35nAmzmSJe4nSiNNo7V70
1mrmDPcCE1mDavtVChkxwafSG8U66SyDks5bbd9lv1OOuQ3xDrLWVuw7QJMaJNak2KAYSSm73D4b
EVn1ex4AeQJ8b+bo9JAO1L0oepEDHvViWqzNHT1Eirxwwo8jmaC4kheVt/z1cRxKOFJunGJHppCt
qSwaXEJUbMhvIgUdXLgy+OvcPlTJyM+OkPEwKj3c0jTav9QmioluPvo79wilM2VMNJloreemhdvW
8SMebSsQKZO5pX6mwzzG1FwsfFNRcS5kSxvRZ0Udc32boR5FvqRgE0SpybAThl9naiGmr8N7HlZj
rg6/9Ty7HYZeGOqYq9gg0VdpDcRu7wtFLfBedO70GERuiQB6OxFTJzn0uz/Zk43Jkkf/h9ID2hur
8G7Ys07sPyyxj8MC0pY9D0KibB+k0nfWQbPRRvGaI+dd48Rr1DPTNGFOdK42fM5EZUBTIbDvHR29
3/xxR8YqHpRe493b9otsvf7Xv0Iy1A2gSMogO5BgH42MjYMiTEECWJ7yiB1OCJVOWDUOsnM9E+rL
VqIMFxH6qQjMU0heCa7Kg36AtkAo2y3KLiO28QdFtOhfE3OzGkGWpZhh5m6jhZlB4XCS7floBpud
OpdYXJ1V0T+DM+ie1/k0TED/xZ/wlIZiCERpkBIR57HYpUWMtqsdy4rUHpvENq8JWObD9Mi1EFU2
V3FG2CWLw4aJoC56HgZ3TWUZkoHh24iOGKU7mQ+wwkZQOvILX3g24s252DpnOCelm2DuXkBoQ+qM
+X9/ubZrR/qZkZ1VXsD2gvhKDTUIbbxJA8SCpBjIFi49RBqv8agZdMsO6QG7PVAsplJEEAh/v5z6
l7JK2Q6I4Bc9g86GhCgHCCZWV0ocSb5Hs8GaXpBk5rA6BFqUbCErk4mFuYZhNkMkMW2ab6+KVnLQ
JGmR7zK22bZaPjEqtgbDNj4R7onquV9gHKk0drUSwXliSDTU7NvwGnDJiOE6gwpEC2WwrJ3L5o/6
/U3yAAwZe+OO5vFg0iAw8vuqWz9Pb0bIkUl63zHv6MvcID9nfsywtXReWgWhFCoH6viPhyPmsfon
lcjFHxsee6aH1U5KP/unCREHtHdxsrf9LeBXtxW+RFtXh4PXcR0vepbC+MwrPZVmtLLhDA3J5wCz
TMZQcycG0OvkOAFJv8oOqdMX/R23OK18baJCFJbbj+TiozNmDDAXPmSwSZxVWhpTdcRNamWZtUcG
XNCTmQbwh0mSX2p1HGtVypMHGNijPD1q190BAA+/ybZhDPo/7iLdLdxx6vfDlwrCRzlqUjsKY3ub
1fyTf4DPGNNdCaBnY42fpQoeSUIrsEE0iqEMmtjTaH+HBfCl8BlKw6lBkBPBRZx8XGNFxAyYqdPi
nrglh7KO6pXpL6h/jgIYkgQXO5fnL2ACpB1tpa+7MP1aLF8ianPUl9vTAHes0sdCtji/zy4ao2cE
W/Z5KN+LLj/lwFtDY22dK6pbujzM1r7G9vIeyXHIxj1quho/ZdC0+mpvByNiowLXcQobV3pL7+s0
cDPmnZUfSEOMCYnBIkNDbesisgA88+3DBhSGvDulNxKxWvzaugXpm6u2VBioos6Z+p33D32Nj9XY
IjV5UFzUwXFAoVF5pJXkfCDnA9Pcam0/Eo/YwHMXBoQkQZIAfCn6VgLW4WT1plQovCRunWjM2pFA
kjaBTRA7HX/L8UpGzUyKNqyNaQJBcm9TsW0cIRwx85IAz8RiTdxToiFgJ2fgIyC4/DvlTHlWgzyE
GraLA9c+m1rURuDMf8/DMHl25X+ZuaVvfyNyTMkoza9gyx+QaXdcRao0h5hSVVVoVb5R20Nxt36F
5ffMJk5XXoDvxR9Ep8pNSmGLyAV5wm7ckUvj+JiLtL15zrNtkrW+e8oMa/2wYKr7SR0u+6T5DWec
c4TQ3/iYvmOYpG3Z8oftFafbUg4G4eucCeOWp2rMSKxNinP8DKF8ibedHZpgj0ndmv2eQy8UDGIU
dJS7wL9852M2Zk3oS6omVIBr1g0Z/lQnGqY8/f1CPSNs+dYYoPMRcJiJtx5kJ6E2bi1LFQ3ZJbKc
/3lMf/UTB0xIzcOr1qwrbcx0eoqTUsftyMYNSmW/B+HGn4ggYhY03HXmPyt2pkVt4fkzitYw7Hjs
76byDr3W0cQq6Tfntlr5GaM6Un1xI3X5p963M39EXh3LTMomi/N0kDf5nAXo7vAV7UroUIeMPH1+
R3eFwDEqcjV3jL7wLGKkWhHyXmlp4g3S9xwz4hHgYJgUFiM83mcU3vLGBtIk4GqQ/At0KP+j6PgQ
wOk6ALyqGTVXsIjsmc3uIlmhoBe4I3B3iqBdFOY2wmDoTpJadtMkf6thKApelIlFCGFwysrBhHve
dKoH+SvN1UJyssmNvjekRhj5tW1W7VDQYW/ha6yHe+WkUans8whpK33F/3nor7mj3ao2q5045+PQ
uFoFJxe8Dr4X7mLJbHxOsjz5gkGQr1mDz40b2vwXVSJAdppuPvmqXw4nzmEpPmznWzSTrn0r4Oqv
19mofQTd13neaYcz/pr/j1UP/g28BKr1Tv/6xVHCu+A9wAq2GL6fIs11U+JpmGsSmgG1nRFjzG1c
ARBCsQcinRjHOojEUaBmcrd5sjZ13TN/i/wuyD9v3FkyzFzJS3GpyNEBor50h73iIIbMiZ09F1MM
RqG9rA4NsxbZogAPfWKf660QWrd2grUHd6aCniCe9UjIJoB+uw9kdeLBRB2j8ZQ3VkmXnkfzpZMa
QL+XzEctmjQikQscfkziN83JWp8Vb0Da78wuLyb5ccgR81i+jDTa6BUMSrav7+JnKpFxygBIsUx+
svoLKVDzjgfUthxPncw1NFOA6qL6sZltJGXzqdTQaKJEA/Q2mTQXNeKTIzHw+4IqeeLKm2+SnhQ8
MQwhzz3Z2HVp/F9Z6BBORPuJeXtGidGHRUL6qrSwnqqW8RG0JHuGH0KlHzTmhK/GWd6jdBZo3Ia0
032Plv6nBUw1iE6kLMtVtv+vVavjsGgHxCsAEaoxRJxXrpim5JAIWDSS7yBlYj/FMZtQmepms+3p
dFPHIf5QZRz1l++PFNFuuM6CWjiexy8saTamqYsT8E8im8cS2llKMBOVNR6g0+9e97GPmozNXRWo
JaZ0nyTow5+gg7Bz9bw++0/5bDa503olP0Y1rdMJ4XJ4zT320GwQbH4R0T1uYMdcNjDRn9D+EhDT
5VyWiN4t7Erzkf7iVG1QGke4Rd/X8JqtYXa5IZCp9inutelGlQKe0DBsOwZOHN3nQJT7s/hyBBrH
fsZjVTnrT5DiF5QgzyPRMHK7j2Xiv6lcex4AsVEcgp+XA6WAob4TSywy1tAx+Murvkgmy+zIcukm
4P0UKQqx1ZBhmE5rQ1fzamWFNLPCws7ZUeR3XelpeehO/PEntlabpwy5eES8bt2gDBHV2kIPlS1Q
H9UgqtP5KLmqVzTgGDRyBitZtiEaCgZLIpt79QhYM9MGc+LeZ4ISuTRh3gGy08fR4XvcgloUGtRC
1Y/kp9Wbf4K4SXnoadsNF0fOX7yRjgOyxfoQU+rPFaORhccYAbh9oc1ntjyTN0mmGhwFMAZyFdc0
XEXDEfuwqY9NJvkyW0l+pd0g35vmaFGmbRkFTcqZSBCQ5dEVBQG/4awtQIYnASBPfCSd5Dp5AEQ8
2gUg8d2so0Qaqw7Pa+kkuJMHEm5MM2A1kGQ352XKTodGjNdYmojA4XedSZbNv72DhHpNBzSg0nnC
crb/cX2Vn2sGQ0nwt5/qfrJ1BpUJCQ7rzay01H5NFCkri7lG5DE1jTBxdabxDYIxA7P8QyZN9Clw
XlQg65e/LtMjdNsAk65zj+Slif/gi7+3fTwNKbnFwyET/5FDIEWh0qUPomWMs00yZpfTK1W+iXA0
R8ACxgOe7OW3VmvNuf7fpGVl0p3XYtWFs0u2UPHJhx3WetJZKwJ/6d2UIyPxk8zDcTWFjXOyE1OC
MOliVTQERAakmV8RhKEiRQr/nphttzohTUOcVN4+bjIJ/PkqXoIXShxFb0p7FHL5/LKMHfaxkxWb
hz+6J8UaJE5yBXZF4aYrEcBsaYc4P84avMW3DNz5oAPOyumUB9Yjs2NmVq0RGSB+QcPnodsyGVSB
slFPsu/B/hZ8E9t3M+tmZqvCan9EbC9hpJfP6aaCY9Yf6GHlL2zscMxXh0k4oSaaOnrupkXZNVXT
MGr7x4HOGgk0y21ukNuKGcxKHm7VSHA/5VHr543/8Egmn6Yc1iAUpo51jF4K6C/bZ3f6aaPPWlw5
lQ8bx/UoFBdN+ugnOEupLweoRyLoI18AzXzxiXYp2nET4Nam8+GpmonpmDdM6DbYju8Y7jlipD6O
vXePwdC2KuENpWi1maMC/5hrU3jJ/T56d8rXXEQ4ycN1fbvrTIzLkGsww5l+3YZ59vhO8vu6TUdJ
Ll8ThjZ/KPPucRcBvpDMZYw2idEUgA+9Vtf9iYkmSKyqVpmhcn57DrgKBqFz6IabelgYxnXRIaex
jFkJFc6eV5lMnQhEMmBRc67Cdn4pOV19lJ4qWbgKkIJ8rZzOsfkFrJwM8s/6UfDY/1Z2roRfUHEw
qFURbGEUD6bodixgfayWwnsxdVkZx4bhxm2K6NScO/CF2z9NhMVsv+lyhqONtwqiWVhHR/rq5oEz
Bqu8/t07ht82/rDF5b9SPGTWnygK5lrBZGaMjO7WdPE6Bn3Y6XULSfZCEHhwEEytg76UBcvJU0jR
C8EQu0MeKsEWxyYXpvysUk2GRlMS6iaX/jKmagiQmN0vDC0Saj63SH2/VmrZISHTHyu8NsSq/Z9P
S0fTd4NoOEFwR4s+Fq9WLRTFtlFEurflZb9kqrK61HJAph0yasFgPJwXiDgCkFn7tLdDmE9MEbLW
ZDnehpQ4fCXZ3zL+Ze5kqdWSGZFCNCq1WWxL3Goh52mkOCZnIxSZ9HmKcsjQaTJsdv4o0Kh58CoK
XrA0amYPQC/Dlt5iAAx9TuHRPdNiAANBh9Bq3ddH9YFVDkp8Aupf8dvFRp183ySUQQ09rngmi8Qw
RJjH2BH5CqHxxam87pJvZhq++s6KIM+pKvpYIg0BGYfmA61vhrcAefURwYzdgQAELw10mBqALtYy
ZqUXxzMWAPss+ozUCisBymzdEVTBlKi7J89+0IA52gunTkPzgoypvl4Fe14HtJ81nbRkZwpyup39
srIB04EF14Vv3qD9mQqu6CbH+i5NtwoAvnCGSBmD/nJGKnisrGcMlI9sx2fkDaI15yoM7cJoI8GX
5lXC2GuxI3XwGDDmYFEBGcELMzYKhn1oM0TlIRaVn0FTjLfvGW/DQDhQNKtvNhq4gzHyupqja+pv
4vPi3SoIh2Wintlhfbt5jE38/PsSHYnglsevT77gIVc3wQwGwYFKzdMKUcDP8DlbRrrvQ1WSGAn9
tV5R2lAs3Y06AP1KyaftoyTMi9PyI3oMdd+OLs9GI4qXGAjHVwAho+LQ3GKBJx48uAFbrg/ddmX7
M/aM/gmxwyDr0YGvjEUSyhyeSviGqaAGMnVTj5VVGa5fv3CMG1mMfl4Bo0Rt/9jPu9WcFAMJ8IWE
/gFtTT6CSTRsK7j8X3kSQoKgcVxpWg4qCignrOoDG6Qt4H6rpQ5kyWKty7ZpKbCLk76xxpzuhm6/
bNWqd8DK6Do5xyxi7N4uxkGm9ecg1iE6tyU8YQ3/KGYEuSSuS2BDu+KX0Pk4l7N7NNARVRjTXquE
Hh7+2tYabT8lwvaKZaiOorHQs9rji3Pdmsc6S9ZSqCS8zv4Btp0EvTAiui7zlW52k5HC4IL14uSH
sO4kKx3haE24hx5MK5adv7ieQux3+HDMmphQTzTGxPxqfjP1GOQuln2JDjAWIu0j0FUUuQMr2vWx
0RrcwygojINtV55evmfgfKNClfgOXbHhUeU3+fIb7HzjS8aSVAg864D36o5mZQuh+QQOTpmlsyOV
e2UvTGaeQDKMk9fEYIxHYfoNLGrwApNkclCLC+ONVvXL+/EVDicVEUA+y5nqbM531V64OEaxXQw1
0GBeWFT7xPhHZX47l81NALGdVXSKCTJ8jR4QrZFkSY8ZuiQC04YVq5N/0zYbPI2L3CkCPEC3x32A
WOqcpwcQnL6rcMtc4dNr/bEHghsHnGRqlWZnmAjMQ/X800kQSZT7+lFtrT78us28SN21T86MaCHj
OtsdXj1crxxOixONCzyRVv+9945nCKLQewuBO3BRQh8SKPRZbFrno2sKpzUuFhMJ9PowO2maf9Cy
EWSCAUp8S/sasJ8vQI4VVHcqO1a1aF40Mi6DHn0P03+8ZozioNwYMRYj9EglkC6sal7y1lSafFwL
LSxet7IIEwThcu2DDk+k00rDntoIsDitBtx2Qi8KAMoJ0KqEuSohXDj7BWRmeh93sWz2M/UXqzbk
rOmmMTedXedx28/5RYnM1xP+p11UuGP+vj9VA6B/e4ByhBgT4nuTuB5CD+Alosa2nFTF0a+9hlKu
Xz/J3CKN3pLBFLwMBED0MAz7B5tAF52BcyEC4QwEhUkQ5b8fsyxXeFtzlakRsJy5XmB/ooWxpFXg
FnVWNEt6pmt0L4nDoVdMsnNiWUByh7H9qUTvRJxH5F0EKB7ZUAwi+IWmxk2HwQX0Ag/emkY8ih/z
okT4g+9EuH1VeoQmeYy1IXaDNfkbjXmao+j/UCW8ZQyvgD3jvp7Z/b/IG8GAjw55v9t1SK4b4mv6
XnzcoCd5UEf5N0HM0VoEnIGknEvdWx/eFwMOQSOKj3fvkJmWRiCSn9iC45TYODWNypr0jZvpuGGt
y4LvvQzEnihJj9YP7fufO93mKFbnvgH/Q3WUidevWX2I7dZZI/C7KXlnRlKQTo4p74cN2Ih21W9+
YcdlNNyErsaxhOxA/w0LnZ+GCDpDUn0+r4OzgwHZn13G9Pnu0bJESZHdYyJdvgIfQr2iBjgumu4t
aMtZqzdBTy5/pMP6cUveQ5z24b8xPenFprq68SOs/UnXznS3zuogJrlIl1jgu6/ArpPrIuhWUgaq
4S04i83j8NWoLmGKXcZht/hXCX9AD6c6oZu517na6X8Rmnkuu0NgCvnm6m9ENyjAYydSL5dEcECM
PSAzzoZzmPxdNMYIUUzU90yYxI028zs0wUysrohaHtrowTGPaDJU8A8HT81NGn/uIGP0NRa4J5Tc
nlHC58t6tDn9c+h7GD9W0IhrqOWa+cnhW5wJuLINzUDEV/RWmx2OZy2nngkG9WD9ITpU7QkhzB9M
n+mnFl5m6344m0ixi/b67KLYZr3dpslQCy1o/CqjOnnPjx2kS32j/qWvPyNmXKgKzhgUWofKI5J4
05op3A0lEBMIYW7TxLMZICMn+si/zsLzYC7xnP1S9BcNkyEWEOBAoASTFiBnqGNWEvzZZw+/0VFL
09XIQXAb4HkDMqB708/w80PRVAdhh5sAhEoxgHEoWvxKReczVgQKLIk3nNv4FiV1FGQea0hwPvOK
4ita5kP9lTurHxUNO5LbPrDfDa7h6Uvjg0iq46+mqFTj64VNanaN7j2OlrZrh8byzoIMsG8OUwwJ
Rip6rq9v8EpxWs2/QoP4SdEdQAv0lhQYdKXWWrJsHh9mED0AqgBeUQvULIrXMdjyLe0cVrlZs+oN
0qih4Zstg94d3JpezuxX9Dnk4Am5gS3qqZlk1GAAmlcqOx/JTKs+XabmdaUOWZoBGjS46geNO5GE
SBXLuklXuqvVHw7JmbF1b+5lu3TJReUuxf2oY9j4lsI6G+2mn77Fy3Fw87F1HD5uDoeUuYiSCHrB
fV+RpSyan8ycz5sRJ+D8cknBFHyWNsD5nkCHg4RMQhYHDJhwn0bFJ2+kzRWuoyGfYllgU6ONcOjV
FVQ0XT1100i0ZsL/33LB6sfRiLQ073fkqxAyLriDMGDFknUEmQKAuSFLlh4dsbaJ+obXvv+wEeV/
o71Cb+BNwXdjcUXAQmo96/UJLtTh+PBfeRYm+X/AKu1CAY213fhMOio1tJGw2YhUCBvYGi8fS5UH
WJzb4//fWgGEFE038UVGmqRwfj/bc4qyOnxKbaU1se9aloUrJSgsO08Uq2OVdVwC5QL0/Ry/u0II
81Rgmkosmm5cfaxetLUZ8A7DNhTtVyo0LVRE5wa+mPnlMrZKmq2Ynf2J/CDgLDLBXmB6PrYw8Ehh
ve8BY31vE6bV0S7y/vhY1uDXcIcbXM3dlcNo2o4xTsQ24CYLQWIjQAMOsL4hGRrhObR6VsaRoj96
ZEUvLt8TvXnagqFUuOM6748ZJ185DVU9Vi+xLKmP6u1YXxkJtBMkQZUiBougUv1U892gKOkru0q/
B+Qlixmni1CopBj6JQvlhz10lE3myvlrWomO0B81LZ8PrftsaX+78RtARRJbEtCH4hd09yuzdR4F
hKTbevBStkCwFmbsJcVRMB6RCcjad27bdZ3EtYyW5P5CottdurPZq8lDTaemTS2w7AxcOqgEfqUq
KMD66zXhedl2dwHkvg8TpK54n4LOaLvS2J+VSqaG8Roy4aWnVmsXTPrLILGoNnb4x0HX1jliBVUk
7o2JNiRoze9RUP+xldNrlugctt0VqGxPgbW1lUyzUrYMgTiuCVfMFu99vEglBWhnxo+iQY32DZde
TJcdWRkvVlaavEcDQjFYIEaHvzOtZIgjs6THUahqzDz/UYo7vvDJxan5ecuIdxGJrnDDdW5uIDPy
m3wYOnwIglb4rzBN5lWMKVI3o94VaLEjqTn/3lLXTqRx0K2in9uDFdrd1wLCIXNoTIb81ci9NKh4
QaR3Q9gigs4sQGnYY6/31A6jh94PE12UPy7ocPgP76O/K72D8V6Nr9uksmH28utHixi3Sosh7HsY
113Pn51QNk56jtLKi3Nb4wOQcBjSadF2k/ghBw16Qy/3VeXofRVgSi6C4AqYoQIK1991jy/xuWq1
q/BxsbXOWi7rlOVWClPkZThMxz3BkPARdZ+uXRY8kdejDf7FmBmHvhWmQhJ7+b9feS44LhkZcKWX
kGHycv7nlntGRQNnzihX2XaWkh42+HtlIqqb6gfsy3PWlIJ2kUXM1lF4qHT5ITEwnKO1QjHKFJOj
dWAGA3NVStzDDofMZWPPF26ugfMearOe6+hYYthVImbAubm0n3qGJVuXW2nhkkwDB8G98C2iiQk7
FV0sgLj4ELVQIm4q/L5HEoKPEr7t8VgQwJnViKAr1zHACn292M25Gw0yWcqQ6CSJR++r3m6yU7aG
HFMbMV+x1h7Iq/hElH8YmibBGwDucpzZLmXYuG310Kmturtval57WM64sNElT6OoJB+oGwD9tpvu
By6Tauf9+yK8P7BtrzN1kGZlMBfc2bugZyJr/TTtvht2Crhb74pcrcDDah5yt+OPmpqFfY7errum
3+oxk0RxVAjUYIr/W8DO0IlqXnDIvND7NpxZsiFpi8ap4DTXv6R+aG2gVV3JGvCmajdn51FxMQHg
/pXhyFmsd++LXKuD41r7tmEdvPoQtporHiDJkPneDP0tBYEt4lgq8ueD1AbP53krOuxr2jZz6wj0
8MtK+r3Jg3xrIUM2MZI7kkmihWzVPurskTv/sgwI8JTfY5KVGidnFyW3cjBbE/QdQT2PQ94r+RtS
Rh+QluhBS56+hB7qjUG3Em+DCB4UgmVmEunJ5SKrqMteUkWFYa90gaD66m5PBSFaHdiHiDIazNNu
jTyZNL8QUNMOMCXc+JNTPQz2dp8RKia6Wj24uzfzoDUaadYi9VoP0QoifDLTjnDac1jup4YfZ1Ew
nzoxYzGHmXi0HKo9iWi/OfIPsM/S3oZNVsrXCMg9mpmsedkv2UjhPZ9deOVZbyAXQzBplfG2uJ2/
HsP6grK/ZRH0KctwXUegJLzyq2gaxXq1nS4Y1ade3qTh8YImx16wUaZUeUdZk0cYhklUSAA51a6B
e7wyssvUjUbqmHl2weQcdauV5iUq3taDA1EAIx0Fplg4F8C7UL2QLKg8pwvuH+D/q4DAzlnzA/Ik
feAP6/p6bH/WWVfgwwvDdRpYfZyMDaaKlVMTBu9g5PrsgZT0CpUICKsPBPkRDG3QmbCZyT/Wvkw8
9s49XCi8TMbRGQamiGLwxsa2nOUJripv/G+SNvh71mfv6WobykM3RRP/BlNEuwvdqAGw36fu0UrJ
sIWyWHg1n8+hdcqX++mDZA/onf5cWkaNq+ksnidRmeRCx2kupxV1QG7dX5B7isAQkEt/AiL4B1gN
0F1PtWxwfejSp489amSTCsuHoY66Hdp07tNx02oBXk80s3gIg0G6z6zcarBWjUN06K5MSTfOjZB1
uUw03U0bT2E7ihuIXOoa5Un93VWLGUGBj937iyhKT5Dz+eMdlKts/bM3nBxTliOgUhGxF6RXeMOc
o3bN2lBgwirRpqmeF7blhMlhw0bMuhFoduQfrxGgkMWRQIAMcYI4Kn/F4fQzzgZlOdb/McApldol
0p7k8dNuzb9GRrNvIyLPs6+6jUNaaHLOAfKLpBF1sSMH6kgk3faNY3rfNpfy/zpZvTteHEJvppKA
qtUaOkkZ9fHmJM7PDVxWujqVFN+1N/H65ZTX9PIhAUg/OhPD0FWsoomupRyHYYgsCafzex0j17En
pBHzMlzvK4l1GxTp2ECkP14WM9379rIBLhrFUPF8T27Jrd29qBSF96L46DgtIkIRZy8lo7O4wy6H
2Um8sQ5qChFnrLOc2BJw8tcNA0UGJ+Z3GgC7zvj/VI+brZewvDV3VTxM5Xph66ZWpDw/mDFEDHgb
8v5EFCSdN50EFa/rEjIVQcnE7G+J7QIsCsl48jaXJMBQLTr8S6Ik48IjqlnMQ32TrpH/LGAfdJuC
OKGfq0Kvu/xDaQCIx2TPddnFKoaEDvNE47W+3V+NktZEX8V2aRqRrhbxragZdAis6mdN3NWzmAd5
mtGqY9PLOSieyQGibTGdjRtd20vebieCCiZ/71VR+oc2xDvUMD3hTLsn82u3iQPJLMVaiia/0Kvk
OQgBpPU8CcwKmIizDVqjfi5ObE7Se/BzbeNxhlLN2MOf7483kzIzYLPzBKru0+t4jxVoniwSHxkZ
3hkMya3mlfdjFjXIuhvsgcAYdzzY702x0pr6n+J4lzo1T7ashJM/7c8/M05f+XxHONoUScPJ7paj
zo0Qm41/RxQ6FLJIhkL2/2hukgq11Ek6J+spzC2uPIuk+yKJDgZ/xjX8SirS+3nDZrcfXdsTufg6
jTQpH4b/w0nYQppNlJvXbcLspV9y/DK4gjPWo14U+Bbiv2RWIabhJ8BluMGBect6c83dfFCZ3v69
mQdcqKCxhI1W5iCvWGYR9RaIgQe5/Xs/NteK8G3UuLEq2p0JUo/dov9eL8TI+EpXkuuoTl1bxKth
2ygCVXLwyA1SQ23QB84RQ31S3LCjTlhwGI/SPoXh5mw9X87Xvp9w6myl+kUcflx/ImeQzgPeTKYg
bKmvvqe4pw9smmRJP3Yx8TT9YrnKTAJGqW6nYjt8K7Povvpd2ye8vRCVVPAUirOcucRQSjo5IAhl
E4TopqZWh3pETpmupFh+x58+SEXFtzFtY77CP/Xx5TCkpJnkF8KE8M3jU5fPYvQT9Ge+fA+1/tAz
+Yp4AC6RbH2Gs7+exFvp7Qai4h+PEl168pImKhVTHUhp6b4CRM/qhXrxsbmt06xoPSlZaUzk2ck7
XwY1ng9Toj6D7O3/EuCvjWQmtIsHywSRFiHxbx6AewK9Uw3hlso3I2uufsYB/UgVerXsqH2FKNVd
zr8faRMRrzvpW5bAaJRcuq7TfdIEbxN4Tvfy45DzZiXMBRRYTMje8YDKwHzric11as085UIKfL7r
ePB0+RO7FYEHBMKpJISJPm20PoKZYGQefLNto+cxE9FdRyLXCHx/Uv4WrtxWKB/TReldBOhtb1qP
3P5YX1bCN6/x/jr/NsTLGW0ZHbDk0f72joO/yl8KdO22+OrNbNFb0B7BF+Uf8Q4y6SRGTMBVGFCr
49+27ChJyRt8V9NpDVmN6FkV7guDp7fgJKINBon3ytnGU/wtaqU4GJUmFTR7cIFB4/080uix5n+X
hLlMjxcWP2UkVoFkFOdknx9RoRgmObEn2QxmCSinthByK0Jhdczr6TZF+diu/60KbGsUfvAOARma
v5P4Qd3g9gjj9Y5/N2rNZgL9Z2mkfZfbfC2Kuxgs8lDPVjq5vtqD/Jbjyql71ye3vqD0FSXlwwaH
K4TQd6SUmV6q93cgy0R/X12CeHU09DPNSo0sHaSflvDZJzUAL93gb9AWjbrTOiBeFCl/1pLpiFZM
Bx6bIDe9TF0GdPP3dt61uXN3d6yLltQi3CfX9YTEIkEckFqG4m7RzHqQi6lMPToWVbIw2z8avG4R
O7IiJV5lVIIVcAvwRo+7Y+LweUZWqKl/C/C1lgJet45uaaenfuYi5adKahhQ8S5hN0kk4Gw7Ph4t
HouR1evt5n0QnGvigLAHTci/WD+ntxMABEpohg9JcPr7NROHs4TyvLLoAVUD6DP3umHwQ7FvOCME
678yNL3Ujj+hdhKCUxS8HQIg2gZ99vUHdL0tL9YzfPVS4pXWQCz2LHoaymq/mHEjL91q0oXDpWLA
ylHX7+DkVAKD9hPwgtDNLUpyXY1yNs8p/qAWQ/O9mdCYr5UABCFQKGow0S5moLg65hDH0RBfQOVS
CKxEdU9322FO90sa6XV7m+Mbx1pCpn2ttWqus4FbiT8+h7gLyTP0yu2LLdjvwCGs9BleSi7bq2u8
TKfMfQNau68Ti7vfnombAdO0gwLuExRZd7bXihx8mEHQeNKh1YHPLNh3OtT9uu3W1NGSHIJNn8Ae
vKrvre+rsHTUzcN+19rftIrNUxVASkckGZCIe00JX01ya6gNQelfs7acUwSTDS0r38DGDI/tQCLf
Yna9TplouakNedsjp2aJWFiLtW6hVGDBs8iF89utihZqW9Y9O8d2bgk0Xgok7Dy6SyefEqJCDhuN
SM+IWiWHsk84JMGw7uNFCXpeLB7sbgL92yLt+jsILSDoZFZMXWizo4HnundrEifV7NHuSPEniq20
JsY2z9cN1tzdQOwOS7i1maUCC+T+p14T4Is3kGhr8qwsAldVcFUzP5E7P6+rCPsj4MAF0AaM9nFU
mcZKB2hAHHLnH745cjS5EC+PwvAq9BNwgRCGF6jCGFJfeVaHpX5xSqPJSAVfbmOhuTqsZomlTC7/
kN826ziCIQbN4YZieKtxxRu7kylzLDUbdzAM+EVBdL+w8axsKJTvu7b8by4LqNC0ZiR2LLwWXzOU
2lTj/a/Q0/QeggY2v/566gBxbP4B8OUaztF34N50pg4P7bqWx/8uIU8lld2oq2WBZAhIYo9xEL9B
yTXkqfNysK4RRPArC+9eMw2lzL12DA/Sr9aUsOaLfyT9dnl8ildb/MbK/cqDhSD244Db4RVUD+kG
2yaqEj2lrXVs+Y/doWG6lt07zH9BPwQI0J+hEDcGmnwXsg+/A5lN2pw7Sa7E0BghlefkD9SVSae/
Q/3HANgagqv7sIh10fggTxq/OXc8WUCgE9AegwRIxpNxlAsoYrV2uJ0EiYPE7GpuwMVkpb4pKsXQ
nY/r0zkb3lLTmPXuox1EGPDZDNOJF3GTd9G+rlV5aMHfvBNseL06YY49fdhJdHO+z7ZA4M6uwa0M
/pLeY4gRLCFKDV1FA/yae8c2U9Wo9n+7zrOt1g2sYmZ6LJTvbAx+NGxqJV+0eVLlE25KwLnej2cq
OABIA8V4/ghwfgar/mvWgIiccMUEVzeHtRsbGacR+nKbFktW+gL78AywSdC8nG9sJ3BTZsAaKZ1T
dq2aBQBB68VyuwgPHf2wT/hPqR1jnfDAPnTfrsAMuRaXafyMlDrCvW6sOX/pb8qvVuFLOLtWcEyC
uS6fPaNUah1RyNWLyr3wDQ3/rKE3iYC0EO3LwbjjcgQUQP7VIpdmWQBTfXz0a8+OLPbrKk9fLEIg
qzvZKa7LTs/CxeuIzdLkC4nX5EpN89lgGfCSmiF2yMDw6/uRlpFB1Qs0RtZpIHxnF38Z7+TLluEX
642dyRXfm6h3lWr8j7hK7MiRN9NOo/4ujbpj7jdJZOysO4k3UojVYGNaczT5HX+u1YHV5P8U8X2N
qCamJ0Cxg68dy2VmkonzA/s1RLmz5jSG/exqRPnWOyE1FGCs5iCfSIFQf7QXqSk9PZYI7mM/Rk9T
wjDCGKZCXcogh3S8meluOwAxv10AbkkNOlAOWhBA8kb+cNEBf29DFIjzAJ0nyWQCgwRVTn9YXM7Y
FzoeV7HA8W7gk1TjB3GSwRZxv7XbAddAshYslMZAERePIIP3hzwpInK3pBCuH0N3tWfTTh5zirY0
9+F6Mg7Ycg+l3oMqVd5X8a1xAZSuZoenl4CfZ3uJWlWRyWvOqG79z2T3leJehE1oi9IDvsX0oqkj
D49YhWNTQ7UDdeHpMAjPN1f088+mpaP+aJSSUL0sOMOW/H0rKQotzo8yg3vbzsg26WWi4MMJO7qW
0WyUE9g4zSc4elFZDwRcgWAlqZJLxtqG7Ls6hWTuY9moQFvRmlFrQCf/Fwu7u+uy5G+YHIBui8Ug
VTBp8vJOIbPfA4SDIEMAs6lKte+2Q/VIIqsNZ/RiNIwOUE2HOucBghUlbk20zfOh1NMaRjpILp2b
9FWI35uIbsJxBPnNXzJKwcPe0aZ7+jrgVst2aricAJ4il+Dhod0N8AJ/c5uP2kPIjpjVDgxcN0xX
eNZg8wr+2LhtNOMU01/vQKn1XAyrc9FZr4jXRnEhJ9ZPXSDNpUiLyuYbgAWSEGL7fTzoH3zDTd2B
909e6cZHB3Lk/RciGsGWWLuLIJTvn6qHdOM/Vxg3VR5Dq1C7PXq78d4JQ9VAgMs4Uqv+9eZkBDUa
0JZhyWIm7A6uW3Ob7d5fhoU+cYapRZWmV9n8vxeNn9lGAi9LArJTOrE47CGgYSz1sCwJ80F4Bx89
+HhoyTyvVlZTx+ZsJGlz9+/59XBKfR3c0TYJqmkgc03OOy21L/UoQxX8BuI4n9aA2kOkacCLFECG
zXRoyjBTpFHK0PazKSLzOKbrfnJvQM4wEvVPO7WDDGyEPDO3+EAttgVKVuc7DbtZ54KjR9m2H0P2
uLoRAevmmQEOej9MXwvCCO+7rq4SPzJgAh31D4Qicj/UP3IdOB2M86tK78FJBkElWzawvElOGPWF
C1u9swge4xq2Ij/+VepDnu9M4yJzbR270pTLwRfE3gSOsHD+/8C65eqTWs+Ci3DgbTD+ajh4QGN6
1HSezm5yit69raNRAs8ULPSaBYqhHHZEb843w1gUIbnMTu22Kw7ZJGsz04EPEPrkSZuizguuerV+
DO5hdCpXLJh2MqBFETa7jKZ2QkjpFI6tYoEtnwSCMl2RlmvL518dVm9Cip1LwwiwvGCwDPsRwkXd
CrRkEFRqiqMCrl88F+O3lPNPKQM765bjFHjawrVMpqUnc8557ohDT4A8LE+a/KEb/2RQ797Z71n3
sTg3H1pj/0E3uCCx1jQ2YKQhpAvu3RUWk1I1xgcJdncmSlzm6Sl4Ywj5uZyx09Tb9x2QYLFjpC3f
GR0rpIjy8tCN69AIkNPwOiuHVh0iO9TNdT7G6N/soZIllvOdsFqFMd++cHmPKsEs9/Kv4lx0pFj6
bKkd1qM90JWdg+ptRpA/0nO34PvmIccawJPqkVNtQvL+j9h6d3E9cPoTTy9wVm4g0/z7jH0dV8/E
MxxnTYZtYrxX0HrNCH+bBNnfuQpGSB25Zo02rezmiqz3FkybQZr+XoFMRD+T9tV/6ESJeQmczWY6
TKlqKZHs7WZVssL1AwBbkMznf/7KI9lMppiLFp9rodnl3KVBz+5rIaxu7J18vZQYwhNuoN8Mrt4q
QcIMkf2mfFAn0+3VbQQAeEVYMAQq0Sd7EeSlgpT8SBGbqA/1T2ARNarDxw/5ikuXooPXXqtAWWEN
Ikic+AXEAqGaq2iX7zl1clu2JFPV3RCtYiupndUjJ7R1W4YkPqUrZ5znUZg+dvLeCnw01o8HI83h
WXUFQLrbboLeH3WtkJVN7htBos16cgTNGTn7fWUIt3J08IF6CHqcYlsimAI1tfnxHgcE+TUSGOok
GksIalcdgi9aK+pn2BRhXoND9SN0YpGWjNDEppIE79kRXrE5Fzn+QN5buC7LISjUvSEA0V52XF7F
3dbEvYKV00BC0ZwKf1WYQ4m57PYqGKEtQ/BH7bZ4un51IVq3b+XuBJme68Fqvx2VprFhyZ7z0Kaq
owCsEI2HGKoNhiZ282kj+6KYa5wlcRhzVkLu4/0EzsvWBJm7IgxeDsUItzHUnbNNlZppC/RwrtFp
hvZPplh1hdrXy42aPVmYHVKfEz4x3r0S0E9aavsDhT9SxjK69wfLH4z4+ZGtyDkvYv20tDzxCbpn
8YXDGzhIhZN/BQty9kLwgtbUdZyDVncUdNShelaPCCSbXyn8AwQ2Elfcdbn3aq3G9GLr2OaDVhPN
TIQpIP+pMzHt9Fkbl0OLaxph6DQ8Lp48H/4idqz/pzCFHNbsVGycAWOSm+mUfAE1rYYOT3m9TOM8
DjOmoqrVSoQCvSaEBeIghfFpZ4fIusjbH6uVXrfAQI4nJQehvmprYCfbGiEIMF6bfC2VhKJAlyAE
s3xZU1wUvNxxEe6I8wVyzp4dsPshOIW7AinRWO6+JiyYoQQN5TRSOM/2uPSY0lBRa9I+QB1AdogI
p5gTv4Tud8ihOIREHNMV4UBr8QwjuDw42A+aLhfCjEEnYokqAMvxq9Lezth04NOpLmO1meRAO9NQ
YQ22Wst5pKlKPYOXyFlbn9P04hbj4qZRmgFdH8/oiBLlB8Vv7USNBswNu8b7lPSB5r4MgTJX36i1
LGNfGL88GgEgLuhL+Sj2GkHiq9iM/ayxrsWfanmT4zRgPDtR2zQx0+7gvMi7q4JQnntlMpJ5YLzB
aoZ4TazbDZMqF8LYtGtsEAvMItWN3dCnIMf0n2rCOPZzNLBfhRk5xil5KSIOhp5tBRherKtu2KrQ
fkMAI15V3IZ4/cRYwVp8G5gJU3pnqH1TLe/2qJw1UJc+1EQEPgJWfWpyV/PpkVtj97BY72xvjkdl
3twhAZrZBgG4qKGuRzwjaX8vsNTCUDebHiMdUUQJzirKyQ26alChoRHNQsCyhTV6Fd2V1DN49JfS
151MgXHCDdb6UZTjLCCETSk0qT89Hg1PmaWP/NYPqGPE+w/n80XY9dJWOHQE2npOGUSwcXuclSjO
p3/HkaJh2LSy5KvPQYlpqPb/SxcU0TGeS8JL280JxfqZOxJHI80edlbDoV+sa3+XcyapnW12Vdpb
8ndJ0IVCP2JxZYefhEJpwfgQWkg740NX/3FCQJCXu/ijbE3+6lxDeszFwdYc2DpRv7Etx37Cn9cd
vMMDOEpGG2CT5D+7E9wSDhJStyJhifXHJmW7xFZdsJaqMuOpALdlMrwFEmc1cEUjXyDKNXU6HSiI
XuKWV0zo7wwP+Agg43OOXP9rP1L43ApFVpb10ozTzF2W+/F74KgSzXbBSTBV0AVjztZINFDPW0Ge
So2B+P+fUTY8vtN+Dp4q6FUHJrOJSS3K9/3zTtap0STI4sBwDeGbF8J3E+kIsRO6aHOFOC1tLhBx
jWs+rnA15E8zQZLRyA0VXwKU9mvw1iDcyC9Y2wqoXwzA5JQPEr2Wn3imWZcYo9dt4uuQRklzlowB
N5ZCT4uqAHClIwFfrEgoiZa+07vuycX9cuog8xZJR4qfer9q4ZV9v2Q7N8zxdcyXhZrFDW19Svps
gAaowCJWWzwGuoU3W8j4+cC15SvXxENHqX2fr8ru3AUj1RqZPl2rPV8MXgq3gHLUGd+PFY2aPncD
asdijzSLujHbzxQTUOe9lvD0izMf2Mjcqxt8ryzWe3OVqqlucglCO6gJNpAQ5v1x21h8bFQNTLKz
7wHPH59w9xc9IGjFWT8T7/RSxdrQWvWacVk+j4QTU0CQ0hfxKaQwojGtuzClvoZT0gWFU3Sr+Xp/
nouGR0xb9+nlhai/2XFut/Y6CdT1uQq696o8ekevV851hsAA1TbXP3FqjUGq0ekfw3jq7PmApiFW
WlrxRGGLhD6XwOV6d/+4rYIulAB3qnPl5QwtWED2FYgGE3VLVa4wsrcKfubB617zRDXSSYpQkoLS
iDknPPfM+QwvoEETg1ioCkpN+IQtdlfefFAnY5YjDD/GfVfPZPXg4tbiJzEFScsi723/TexmcIa/
gqbBHRi1ujZl/o/Bxq5pYOOlSwj2KZGzmKPwTnavOzkQrq/a1FXoI9evIv3N4QmY4yTK1ixJ6YWX
9BWsZrtcnnk5G2j+WMBW2q3clyVH58OkTBQP49AXJ2HS0XlED3isWcMnqAuHLL7+3+TGBEtm2JmH
Ff2Ul6q9BSKlxvkLqr9WGQz5HMFXc9Gknxj/EIzZ/dMd64T+gBLdMN5/YK+zLG9TK691kmcbfg9T
SACAonWSH2eSYTYoftfBOKr4VAaYv5f/yZseLC1NOAxepixZEpT9AeXAFvMAQV/8fU62bEB8NOc9
C1mln5e2av3R8O7Fawdr3IsU6KEGiTUar136HjIeKCibsOA/ffXSF25saXoMTWjFofNI6qSLsPV4
I+NAh9uzr8ARgAyksw+AvnGpdxLTzvwBOMc8w0dbX8fL3SuVzNkBSPygR8EPeDvkh9JJwRixDCXs
ONIr4R4RL5+EAngXWTYB8PRuefc9SwtiPG7NY03vM4oUIFogBb79oE7+kbt3REz+3UyVCXDKsnpR
i8fC/TWOqqjSfmx09hAr0cd83TyWkdbQX2SOBPU/oZL6qQ8SBj9Vl2dIG8Zz5tTN1KngFbIEUiY7
2QxLweSXgQDYyz1cMK3N33j0vRyRs+5jbuie67uecfEk6/BTn75QH1TMaQ9pT9OMnQDtsUzdyoLg
huFdcekFlTRRT3R59uGtucVzHbbbJJmssNtqYX09yhmZiguh/myN6O1ayTvGpEfr9mLXHP2lRED0
dfy89MdlUroM/SDHvQTjsxUrbgdZd84pI4YR1dD3yBozi7Mvu6/SFh2g3JFFv4Zan+VW3ymL1sFK
ZVFP8I3uDxyyAYQC66R1kxiSrpOkih2znf3RCtOv0JqboEms2yekXC1c/bzW0z03XVIdXQJu2vUt
8NDezOOcCEeKnKxnP7bigcMMJrdSDi47MPmvwizR67VB2GgJiGLub7YmA7eS0+2xfXPBzG8P/8of
tfWUNK366CcwHY4WRg4/03wNZ99QA7Xy6aQ76SijkVGPVqnZKvPB9o+jb9T9Y2ibxcv9xWAD0HY2
zNnbwgiNLbpRQrLwKU8b4qJeM4431OiT+ZtqFbsnUJPFJcVoQPhAcI58e6KiHz13l4DXPfG3XvwD
L4p3K/lPIsKOAj1Aor8LVzTnF1Yzq8ZTbBidOOHSEP6akrQv+0ExQPRu8E43i6+0yXqOC7DIVUiI
1M/a7rGzDuC1OaiKO0X0RUrpsu2mJcdfPBK3vuSI2gqrNAgwZtWYVQx1PDQqvdNvdFi3nwdmwpOo
XufADqnG7PyFaUXLqaKiB5pB2ceMmxgZMoTtQ0aJbRUr4N3K9O9iE99ANLdJI9qBAy5BvT/nIuVH
+FnoK+GpA/Ta2aDpEbsJ0diJNXUmHdCeoyj2gVKpz+krJr3YSiisfiAwHYGRRC/BDA6l0kKpFOQw
eV946wefTLT/Hg3f8QqgF+IzNzBeyKkhyvL7xBhl9xdJacZSeEePghnriAK/PJCdsqyzY4GiqY0S
KfIOBkDOjsmj1G6RUKCT/+C4D2rNlUxK1Ln+Slf+Yiz7kLXoyUmBpax+/SGn3ENgVJ259BjQfwPK
l0DykgcEgSjz6wHHaXYOshYmq2nHni5ItrtdwjoK8R6lA6VgZV6vjokzOcvpjLQ1lNAN8UIv/2tW
00BUiV2fhYpIWhxQj9/2P3MuTAmFFLT6v1tC/tAiMNVlJUeShav90lTO+O5HImDnYZ4e6x9yEoVz
vmf5ObDCi5Nj3ZkY6bYugkZqpP5iA5JtMpKP0xu69wMgjSpWZTYK7fpyi1IwP1zxdgqy3KCdojW3
7E0393UsuKquAmwR6lYWaAx3Ku1jLmdq1IOGQSMIkxUeXerb9tOQ3x13r4zM9uUmRmYWMkEbf3dI
iSLOc5qtKi1nIClAzKiS5uo58/IONwCEDO6YeeNgfIvHBXjhqUISR917R/pb755zGg17lzIFvCER
0B+Hm6eLosQOMXW0TzozSavxT54XO0SDX+RRwXMoUM8pBHROThvp1xuIslhJtnoatujZSrAergUe
prf/Y3ZSJxl51ZN9UV6WgFg4nl1TxoDQ74r3O5Az2dhCe3+3UolraiT9a4u3EgsO4+IkNxOmtyJm
EdTVnhK+zE7lAFkdYcvJiwP81A1gzKcDqi6i7T5WdfbuMAjJBbJkswDHnjBz6AkRHYfKpi+/UQ5q
yxI3/YbBjf4ha6vaoMS7Feq1maApdB30kfPkXdBZh5OALtY3Kw7GAO/imgEUTfHMXyvbx0Dbtfeg
CNPum0bRVnv5eRl/DX3lEUBUd+5g2VwuQ3yoYz6RVkEIU9ld4VwV8B5pyVkkWYI/SAAXF3mgyRHi
N0IelIwOeCceHmI4zhEF6BG7XNDXf63Nsf7c6DIh3oaHSJ0H4vyJXyfmJWvYS0WdSD8zFJWEwQsZ
Wz4BN8njHcXxfXbarMf9BNMR5A6HUSZjvenyvb84rO3Z+Y9WPRdND6VTJKxTIzxJnt0+yF0DkAjL
zrJvPw/bAqEvz6KT/7v9rCePg9YMRizEEb+7CNSLFQaIpSfU5Lf/t9z53TCq321mkCi1mJqL/dZx
ZeLiAKQNUCASByqAjDzcUaWdxpBbRELnYhzj85BfjAs3A9TWCvgaz6bO+mZ66bkHOF1spKQALbxs
nHktzT7oUb84rlRk/cdaUq91g9kIxgkG5z+xWPZQYsDz2vGqDTr1hY/Ocm0h7ZdlbS6YZhoTj4wm
p6/jTGm7gmyhLUWu4ytvrf96imvUZmRyKMOjRvWyT9ugL06ckILrZy5UMVo8qGNx3/6PlPSK5gHn
Bv4l9quLo5R5HPXnIBsqh5CagGNvkWZL2ifgnYFJsFQT+Z3sYYlZMFW5dgb+uAM2TnrUXX0uTxqj
Nc1l5UuJsIYyfC/jI28FB6BHDFn6678gwbyPLsiwkaskZefGK1H/AB/+WlxyjVPuuf849ZAvHBj/
tdMnih7LvZnSxbVRR/1l6PIpj9CkIuCzjvz2b5Ijchsdrx9vYyfkS15xoDL2FSZ8pP1ejgWTeDwU
rp8GpQlU3k4yOCHA3971A6QeH0DNuf47s9kvm7qQPM2rVNyz7Y5GNBB4ZcMU2qDnzEJREHhm3FC/
MZ251+4FnRqdgSV/jMLG9hz9L95voVZ2QaTNqUdRY4qeVIO4+wmBoS74BfI+6VmcdO4z9kQ7kMQt
CI+zr97wft6O1M4c5lqA40UR420BVi3IzP/h09pTMPEZX/+zNLZ4X/acvmIkw7qu8hz6miJU3ItY
fWvBCt136UVESovKDa9Kl0IXxOtx0oxfwqNR3PuGrnOXFqjzfJZp8pFI0iiEdCMCSFT5ldDtp/0y
ATj2JB4pz7AK+X3/l+MR5m40qid7f5ec0utxG0T9tZKfOVY16nADQ9/RE3dn1GeTKaolbmeKwHWp
oTDgBcLY3facTw2jE7esn902MosrqAQWAqHx/K9kSbxDkDZHd/Enleq48+XitwWvvAqQ1kt+73Aq
XAZUc6WvM4EKZkqMwKenMHwtvllUxosHmvKvJ66DgyvfW0EqQvyQlCda2GX3rwHcC/PH6mvhcXX5
RRoZ70sx0GtK5OiSpkojmWH1G5wJtj1hHCjr+T1uIiNubsucjBO+wBG/XyCOBKFTliO+cqhPqElI
7nZM7lKIpiqW/uuGGBD/Xe34RqRA52jH7Z5ebYt8h0p1r/C20XbaKgwumjWSZw7t9+p+AMakOauu
UF8vkzUiX003cp9GmOUaaNYYXq5He0bpmF2Z9PO0daa0WKSzh13IrX89qkVRuYQLugn0HjkigQg2
94Zg4IE6eC+JJ/Q04UfQW0Z+qkYxCoTeuWxr2R8fOo0uHg9qxpQMviPCDzDoiL9IVlVdk13Mx3c7
k6ZoZSc3OeyoC5R+Ej9EONvXWcbCB095mZgPxuf7caXfuoHC28+SpsBD5CGSjWXax7GjK5k225DC
x5BpVTe6eGrD0f/ThEHBxVplIpbQW4duxHyIp2kilB0he5rfjegz1HenJnbjZSj7b7dxyV3JLIsC
0BYNu/FDrhDTweLE1+SzDFwIyQLRz6l9WuA2E90e+iVxrGqOMAI+IF/aWp7X0rQNcUwE1zjl0e5+
iup0neL9xmtn6Es6zdl4fn9Pixd+rcbKQIz9WcLB09b+QtcMXGUP9DdP9x/PUHXlR9Klyb2ZgXuY
LReHV9KsXtMdIfKiRfFafxwykUuyCHyRWttqzHDG9FNrfjYqOpa2z0V0An6BPC1w006bN9RN5OBV
JvazPxAc4j9tfnd2r218eq214r+z2c6KSCTObOGVpB7pA/A1w/ShVEU8zjgxWdx2tHGx97IXCpSW
MM8bsOpmJbn2vQFsj2oFH3rTcwAITOCsShG5Va8Dr5PksL6c7/g6jtYmW67/VL/tHIbYEkk/QVls
d3N4768QinxAJP2G1ZRW6tfyicn3MfvsJNugWZ9l/Ht67GMCdDPl0fVP55VWYgNMhmU6zCMXJr8k
Y3RE1mbibFulR0cEspTWVCTTWl6nGs5DMrP/clojuGETeHnrXgXLaxO08jvkYfJM0lbe10/NptWF
LQ65fYCdr4jLpEzdsUv6mICRCLvx91sB+yPPFtHniDQIDqK1o1Dyn7kIoE5Lm/0oqEXrECaFK+j7
y7MG+HNCCVVU5zL5c6aDoA3MT2WRu2cFJol/iQ/OWOlRgjyhutqy0WoxfWMsKX3CJY+PHwfsKT5P
OUaI41P/puU2me6VWjuZE2/kz79hX+P+i7ofdOD4zJY0mP8F2hMRGa7IwYn8DIsiDDAH4Mh1mkIo
bz9069CI//Wola8FjyTDu6lBVrxYxFw5m1UHJAtxo5kdgdEGI3lgkwrKX29GAj5TpBYaRCrxe4am
lImq6RzoFzkRgIoyM1xu+ytELM0GK6E2pKeAty/Ea6yyUGJ8K3IgyE7WbBlTjFuZy6ceQJsjYP8g
I2qBqd7yTpp9LraUFXClaTx6mFhA6zAIcIDSXvVYYc0qw4s0J51hQVZwrizt7gdrewlQHgqEn/oZ
nOtdcSv3KMO1Mwx9bqTaUyY9G1WIvIIfzOq/ZSP558K+cPtdQe9bIbOw+B9qsbw5yqwB/62O3012
RAWE8hEvVf3G27i/gfuAKpoOoV4uFEFfLO3jB5BYAcJHxf5mAembODOt67IFyt/fa7cdiNccIUwM
Busskym9FKWBNMOZIQLdVDNEX6viKJiQil3L1bQkVV0Px3zB11PuwwAiqbOovl1fUdWGWDDLcPiQ
l/P2oQzM6J+HkARHzzudgGqoj+JsPgtyfuv0mYKczTQ/lQ9JAK9VIClGwaDE9zmnPD+1kv0/yEyc
CUCSFszupmgMj3OkrsppPEBoHz5gZte93GxHnlo+0ODvYa/Y44JwyTarqUN3AkPBGvinQQPexwsq
E6OTmMt4lJn0Th413ZxhJ6M27Mc4M+djHfELu5YUTCtRi++zdhDZZLXG2YBOAA+dzjQelkagDYN6
bfCa0EJvi9JMeJPBZ+v+XIKNlyaphY5gZaCzmtZI7hBXGFQidTzXVCfg02Tq6Y00g9bvmuifMb3+
1mHwj57kf8CNYSkA8iPo28toeayYbYf/es9xuNgD4vVy8tS3MlZqcIKAf0BhBq49g48nMiSF9PSZ
CHsWE5/haebf5c2yiAcTMpNQ+qfX9spw7qBrLvX/6r38M049vjQLD0hOOs268Y/8YSvqjXWKfi0Y
sqJE79TkHr1y8m37DQn/IfN1fkzQC5RHT6RDSruLsuJ+/yZnOf6a1Sa9tg8HIbLo1MNXsCUm6j/s
gZs4kvbdWjwN15AB1MOsSCMwXHNbWjp6J7ntPqjGtGkdnlI1d+E1poidjVa/ePSE0FmulWKP3X9g
VyDsPOdv22+xFdBi109VqBKtS0JjUf8UrdeOTLuIcBidWXTjsbhJt2/wa36evtdpe/61c2/X8JP5
v3SHkiqp6AWXic8VSBvbhKRv2CwZ/JVuVbIV5TS2BY4L4VxpfcfFUpH0VwrP+evEKiQamrcf5966
f552WfMzIuVz/CH0e2oYeoBNpMmthuBmpIko3mY6l0gCKCqFiQC5pe8Yzp5amdQaa4362PgqLNFl
dL73Ysa4dBTub2yDqcbxh8wnSIvLtykIyG/VdqQq8x1cRvRpHABKpe7rJcmFZwcb0jxQsA5b9xZC
0c/LP5MyesD5A4KVw86laUBkBy8VQoi0SiEn7hZGD4ShmYo/WpBAeaIv6U7voUEkSeDkECw4FUGX
ChaKnEeOxuX7tVutzMG4hlfAqgsAViyAsxRbeq/qt5p9PAo2O28UN5y8rpq2R8hvWxoNTuXEj+0t
sJQIwQHlUsKtkZXWkVLjnz1/kvwmYSINYCPAEkI1d6T6ffiB7+PXP4s7T7B5bEoxexfxN8quU+As
yz4Sh3g0DhrBlYZrJxOATUZiU3O28ybwbLul4LBzTO4QIxz+71CTLK4EJ4j8yL8+gQIOslOGD7Mu
KkMoBDplVmxLTKqsCjsgkEZhn1CaMOKZyD7IYY/vi99jqjiClvEltuwARsBsq+gi9RwcGNJ3R2be
E/e6LUL4AJqXcaVm6vTmUEHMoD1VvheUqJRtKzDF4UM88CAyePRGRJlkPrgZ6KxP5ui5e7Dpsrg7
IR1FQTFeo983D5C1BqGnqHIVslQiqXF8S2cXwH/uWPQ6S5XIFxhSgI75VnifdCyC/mdY0aaWWMyU
O1pR7B9m4LDNzOItZ913VBrHacKutQs2WaDFXTXvxyhPJp2KgF6ANAfTECi4RG+EWkwtscgsvRTZ
9OQ2RqYkTvLWLLdCZFFgpIBhMGVEnsLzwVRpUiIatH0lUcHMn9VBWj9V/5thSPauY6Q9/1M7E01V
qgT6LHnrStEeWkXEWxpjQC1oHz1dXX3j8iFt2z1HAUYPbA2jX02wme+6iVJKYySuiTFmh13D/mfm
nSmyWCX51PST6QFUI5qmORhBLV3+Pt3H/eomYqhsZPOu8mr1M6Z9fG6xOAqbNQwbyQF2JV0PFbn0
b/leGQSafO3nRIrxfHN84yDwiedYgonhyWPf0L8/JZeYdHNtPuR0fikYvN/H0DZx+FnW2raMS5HO
WB0UQB5pzRUlJROKQwBjHDrms0p2n7K9p8PZl2ElhGL7m5pXf0d0Xf7kHooGIQp3hdhyCq03DbQT
dRFBcO5C3UhgCYEhzOtDoPIhBxIdhcGwNTc6CcxGeVOuWaNxbx2K3Cgk5NZ+xS63XOUECgslxEBT
yq/vDXrCJNZ3f0hlQb4qUqHqM/nOEthw0htuokt9QAx2bNTCnPw62U54v6XpWskov53kFyfjTcuX
rxzgtYtR/KpIrWRQOlUwyh3/7Gm2yCe5dvgdUBBBZaXOhJIlhSUG0t2S8ofxzhC+Tf3QV7r+4ycb
2VCFpnV1AM+nva5wi3UVdx9KZtsUi+j9GWau9mKuwjg68+U8y1T5LtsUJjtFwfxEx5dlqMmmn4W+
Keb0qT2viALzW66gXLOJ5osfC0YuhrGqBqhWtq9oJ/1zi9sFumyPEzRlsj3i7H6XXup/Zp7U/uve
MEjpyZeCbNBUMcjzXZZfrCJsln0o3mz+CXFsi+CdIYj6yg2O1KsO1OO9xYT48H3tlOH+Z1QsuuTz
lC8fQzYpkJHAn5wPX94kVXFbbyqQ6t4K8zVuTD2A2kBb7uyndD6xHmT5nn3NwgavJP5dhSX4Je4+
52ge6jnrv6sMkQ5JeZLX3Bseh5r6ZAufv4+XDcPrgFgqRRtkhubMwGi80aV7AhmsyH0V6A7ygfD2
zH2i2V+TqDk4mpF79NpykH9wFKqh5Lo5KrDItQTVKv5eno83XP0w3K35fFwdgB1463Ba7T5Mfkxs
NJ3Cb5bZJ01RD/gKBY/b7dsLsPaJTK5Wt5aHiNUSbaAlBn389NXNh5NNqXdbUsbr0qt/vM0RVKAk
UxWBHhDEUgWA/0uA089to5tO4KlRMibPQFBTyocPO37V91mo+DqhRAuquFOt7+75se47xw9MT5+/
ofnNr+Vda7R+nbpMulet797f32LS4r2Z42DIohSEtMpgYAGQNUDYOWmca39r2gvP8J2ybxdDP2bD
ebc3H/Wxa5hxbEoUi8P2WnhnDHqR7nWC+Zuo96r++R0ehu1rc05Rj72dvoL9LOERC9xZlag5btxY
iSl1SeMLUorH237zxxnGLo6gbjF6k+wFP1spGwCHFqFpzNlVkfRvpSOEyZdTSShUxdzuz+GHW9EA
VE5DcQXW4xHWQ7frHTRGn//30clKaUWh5xPT9aBi3+FnoTzOzZEfD1MpiSb3IWb+mGNd9bX1scLG
RQEoyy+1F+sCy8uOsHAcpZM1RFgy0oBHgUJ85gx2y0gyvoFKjwF4IrVup/7aRx9qboPLHN4YeNOI
5N6zXJmd2NQNZgnt83UXrnM7HAyy8mxdcb/SyMiJQAcg+7GkbfRunxhzv4BfQWtz5WrvchCzqGK5
IZhxqGXz7qgAl+yCM/MuZMV+1HayiFGRVqDOhUbVgovXhCOiogxLaJUClsW6M2IXdkdySMiBJ53c
u3LLtoPcvMzg0O+kKwWuvpDcQrRttFOXfy+/DOD2OMPeWcC8jIOS4yi+NJ1wBDs59f/1J2jQYIG/
xdRIOSfw2FtWGMIacYnQVHa9ckRf5r2jAQZN6VR9ya6Vnv14LDqJ3RO+vfqoqeqIHVUimNUTDcmC
sVqJTvrfvC9h/H0jkZBmAFctls99dY1GIUwpYu89v2BNlG9aT6La2XPsx+JYERX1Klq4oSLwbhpb
ocePC90Nl7QyG+V3LF/8q8HvXFTAY46uWlKHiirnSsrpZ46wB7rwkPJ2xmHa2Ay3QcmM0rz2T9tj
w4UryBoPYTyfVLiT1dcHMt+aH3aGYJUBOrPDeyL+EMiHvZHol35YIYX+olJHJnw34sIwqtdZr52X
SGh+Q6dXhR8RHJcCisrVIuFLcodKAMG95BwOdH315bf63aBJVM1jZC3vvHxIVEj+OPJGlYq3N5rv
+XqkhnJSPABniO39bcAHbC4ll0IeFz7ru8y+czQPGkcfkQLsnypElih/WuHOd7i+ot99ATm0IvnX
SpaetKTH4Pbsr1x58Oo3S+KlN3VXBJOaeo/FLZ37kjhnmoz0gdry/GCLaT3FAC5OC63K90uLyAdK
EguChDCDq7TgNdMdZfYRlFIvWJ5J0RzKDHd5nNe2ZYvArKOTyTfMy+7UAD3vYVvyTCEQsbHjSvp4
bd0efJNQXHIhV0Hf5TNd6AADIgsIJCb65xlhX9FHWv5AGgGVq/y4dcPDBCO1XR/TpQPB5diePkWJ
/6dnSY2vq3HQ+Y1iqsIfz0/0fkx0ucRLIOcy4uduGry/V/y6pUnViWX6aIPGO7g0g+oEj/oQPvJu
c/oM4IpaIZ69otY88xg7krDiHAQF+Z0iPQe75ZwDiEIs9rMGEGuGUSiKUCuualy74cdd/06TxkSO
W4VdcV7r1AfRMumVoZjpYOisxE2PnjIj6G3e8SMCMXJOBF2gTHjIYgiwwk2wrFi2BdLGHNskmgvh
/i+ioTfE70fAyJX7G4Go1lxmlrsDXLd3ysTj3piKu5w3UkbYHK4UMUM8O0mXQ/1y3ZWbymZlcwyG
JhpGqdDTibS9FBFLoEJji+0mB/11wjoGJKFIROBYSMnBwSUYEtGetXU38iOND72H85nGKofpOdg7
EDdYl0jMTCMGhrPCGL2uz939LhkRSl1hx/dxNGm5C9hh9KfgQeaF5g8+13VGuk2+aZFd0VSZM9sz
6xQbOkyaThbXQZMSZ03kHfAIREO0ExQP1HD+h0ulv8oa3RNv9438STwnbOIEyz4VfWBWrcYUCmPs
i3VKd80EycnQtgYLvKdPQouAiyA5FB5w6scNl5eGhtgDujQU5KpPBSUFRKXJODp2un7QYp/U+mi0
GA0UgZCftBeeUKJ8KC6oiYCrbpIZbyQGYaidXnYVz8oeDq9n3ZLQILRLxCFTOo9iAwf/OzFq0HNF
RqYPq1l9wPSyagVwlu6ZTZC6Wm19dULnPnoS4H7NBR4HN3V3YfrBt082KXL+huKn6cRqGZ4SjBlz
DYBI6/NxvN+OgBGqooUePdzOMpnZOMqtXkystNbfXbYeicog6jsD+p8byWri16IKlSHfbtmO6bze
GHpoM5JBd/3RKbB4/yC5rzH538jlOLHQ0ZWiINqSiPh2NJhmVH1D4TY41GaAYtAuRaUr3LrqhEs/
3kyRTFBOOVJNjCfuyQMX5uU7Ce47YeGtE2JaO32tZ3jS79jGMkIFw7whBO8W/HeYiAOZPrizopxQ
uWb4nhkxzCbSUyBKq/UHX2jV6BXhI+13p2DAzOgEPflnCvgEyPR04mb0ETkkJXmSDA2n/+dspuSC
nI80xUvYDMGn2Njwy+CcCIrRSgjclP80GqzRk3MQlefRNNNXJm58LGqTaNlMaBRGCXyPvCexsuXM
nuThsRU9S/zeOtjuldS0HPxescAohyBMdH7m0ohR1DQvv9lt7nU+E2ZItAjh337OGUTjYhiiaY8y
I8PiCdjoO7lQGo2KfoCRpY4gJogzPITa9i4UkLwqJJKBcr8IYcpF77EnRrVCPZmVGwNlNyxDGEkZ
FETyBGdHOEsG77MJ3CYWFo6tpGegSSkSZvXsQsJpZrfPfOwXInMrNZVtF/HUekGIQdk2jgfCJP7E
96AhirMakzyjnScQYnaKBYK3W3aE7Z05WxusWL2TeXIQRzmnc3eP8ISsyoqY7VmzG1ea33R+0FPV
GQMTqM6yBwbwsHpXxtOYLCF7z0Wbnp5KuJ9BFznjakCFtxwe0+xWoD1SywGVW4jGk8h6hlpOI/h/
I11HuNVS/e8MMdmFSWfieny89/Q9nQKiEwn8G6rB7MBadG2JgDL/s3xeT/zgLoqTNlkNLIWbpajO
NbJ+AgYUPxrqOBvNRy7uP8hwQ8h9xW1QwkifY9BU8pVJh1Vv7TV8JAz1UX9uddd2XNVR9RqII13Y
iR1v6uWtO05HVQAgHrOtu7mEBTjoCdsBneDdQOhQsXm1nAqhHcMLEsKZlH3PrUatu++/cZT9qA3s
528FtdufiDzNWzDwwBcVg9oC/3ou/RnrPGZ7etzVhqfXzssw5ADvcgTVp1A7lwXfscZQ+V9mIscR
jxH4S4Q8ovZOWherXiof/hkOb45qy6gbAjR8wn0w+xiBQKrFGH9hWMq9WHkDTI0OWMcO3kEdLtyu
wdE/OKwolx0mxSq4NOrvbFJWFxjBHGj/0HpuIsyQ23zCE3Up29fwPQMFaUtA6/SOt0vWqZe0S3s5
mT4iW1AFGSSZjv0Cgm6Sh6DA2+w6QbWGbY5KN5Za9rCGPsavYLQAoygtpdOgC5eNzVhARf0u5Ek4
JMI4hnn+dERgUo1MtJ8xSTcZHLzZiIrHmqsYJ3I6+V8/Krf4H+PWVA6xQeLAeAqt9V7E1FaqXTuu
9U3+qv0TFYCF0GKv+rHdiBNqVwOw7FVs2rZMAgzBfOELZH3V8G+ls46oYBNNAEkyhDu1MjR2sO1S
2UW12JmnpcG3SBugfBXB8X0nAOP9x1krhfCGHYBAxAoM8XNl4CFMkNehAuWgg1WSr6SOhpKgUS7e
s9GLxbURMbLHziXKFv9f7MNM6THB7hamdGhaqE5rHfd+UXeX/kQrqc7oPGatnyVZ5n3L3WlRDcdz
fTuYtO8Vh0DgGCpZ5/LcZbiH6YfgwfPyg9iG6/mF4Fx0juarm5E4Hav4eAdqhFEeAeDiJ/07fFpG
nPaSA66cNl+kcKSrqAg78qQmSZ3Q6dPy5QOfCPyjKoqYh8R57HPP4sXRZteD5Orxe33upYiaJEXM
tZB4+kIYzGoCCsMNGoPivN6tlpEJUlsNMNUtr2iA3X4CmRECnrtkv+Crf/XVg40yRP2cdPT0XMhd
CcUeuaTVKfXy0k3WvVovJNMu/hO9HZqF7pLO57u+xBZRtVMelzO25ORFaYbwco06C2pK1UVtmaHW
uQu97Cn5fjxFEdnkCSen7dEsoeaWP3ak7beJl3dWnQ2Wz/e5D/q3tXafGDjaeyaKx1Bup9Qn8md1
L/K7hANFRUOYx41zUbbLOf1UKa2H05C3cP6b7AQnMJBl1IrpTNl8Rcg0EV47pFjgKonk/dtnjs5l
6j7xgoUtoDUf2Ifw/JjzZI311I9DLelROMXasQPXjYtM1ThFkxR0kPkSTZkzxGOf4H5/8FueQqVj
CTCX/ADdtfZvfX8v9NIuzDyqVobDmks5Hax5EG+gAsEhK788hPtGX0Oa237VAd+hxP6qvjbkE4j9
huE5szNASezIQgHW7HZ7AdE4MKDPh8LWTvzQeyeO4lYtRXprbyQDdK09VzBnGdcUywLk5uTdo2og
/ribJnpyzx8NokMxs2qL0osMVrJVCIN4Dl09+gQzWfKMIwrZPQXOMsed3nUC/M9yG9/4LRtQ20XG
ZC05+uA8A1soyV15VC79TU1pQZ/wRr431H0pg+dzs8v8evNJ3sbta4kQpwWvUGYuln4h4ZP6DPbG
iePy6lC5PSHcFncvfcB8JD5Z4aCiLy50Amos1E8Q+rEsG2IlKJgHUn8L+D/wG7B3sQRpe/xB9rKr
Rewzu8j1sXTgXIriyix9wVX5OQFznFzjQkzOvLnbtQ4i36vFh7TDfWZq0h876jSxWBcg/xE7i05S
K6nPF7koAIF+z2ID4HyS1Oyi9tYwIzjZoy/A60Dn4KHiLPHNH2OaaqNnP0fuSxO6dasX2Fmgw06b
MXtVHMEV1a9nbd0Uf07qmJQEHY9y41LH4z3fCbfGCer3iTB503tOfyOwhzCOBBgKc+iVHXu9zKMR
I4B+/iQu4BRsRGN2vesEY2e/QWlAu1LkYXQZ4iCIbIhx8J7tBQ2Y5Skixal0IfeBp6AmmO8Horvn
DDoCOG4dKep8wk7hhd508aFwSZmKSBPKUff7FrUPsogTlRVomoNyNcC4D/745Zx234SKTWykeQfg
wuDma4s5KA7OLWM9PMeZR38PioDfR9J+EhiXiORYOn4JWw63g4OK9qYUxpHLg0SXHKAbvefiVcKU
kjKkFdwT/kopIUUjdrxEX7sbJ4LNrrZ1KKC4juHiO1e82/TxxsHDmcRi6U0/N0wBNH4dID3S/XQQ
rBuEIN9DfMvq1earLor7uz86jaYAB1QChuairv2l06iDVoBo5m20R2tmbLs7xtmkeB0A86M5Iwa8
zYj0E8xLC0RDAL22Ck5rOUNH/Uk7QW82kKoDSE14kRHiLau8Xnzg+WclVLio4rRlDaXeg56lHqL+
Q/HWjB1KmxIzNlBLon9Y517Itit74DBhAqbGry7KPWucUl2lwRK36dAPsz85uqRQahVln58Mol/h
KxiXmWlROR6Zq8T55hg4ergi841CiIFS/Ict7i4Od8WLtQ/Un5XolIMm9J7PS17imQbwSjYXIivX
C0jPXFKI4IYrluBDmIOiIwIcUqk6joumvA12tt1Y3vz98Vjw9xpSWgXnfq17Uyr8LsQ3ZE5lUC4V
IMjSFaWpghiseTFH7eSfl2A7/D1TCSc0NkgXSrn48SvWDv5dwz1kv6QDSXxTkIwrE7DIq7c5NNzj
YxusTJFd0f8skEWcJu2TZh2er7Wt9fiC7mIE9Vhw8ts9aKKJRSbE7XHlJ7/BbToUQsan7IW+s+/O
J4JozLkP7ipqgxAOX5rAP9TR2JuKGs+Dh29goVwkdk9lb4e37htV4I9vLyQRptsFNaK+JmIW79eX
lkpx5nSuTdYZBqJDy4YBi9ReIMV9T9DdpHvk92i1zd+8+HoX4x5Xr1+JUeJ2LB3JAwC/lpxTN+wy
RyWxAgGNeG9KDp54eS2avTBEsSXMi2dIJgsmwV5wYep12/gmHcgHmKxzQCS+tRSaf+CichtAtkUH
M1oPFwTQxfvRymnbS064rretvgRejuUmLQMJ2JSoP281rFRMWVFk5tAlyjWrDDXhBxvwiarcoX0z
73ozMIdwpe+zR+GNXXJjpyvJeHgeLkTVEWz0stcNzqfaGfWcxIZk7TwV7P2LkNgnIgdf/ZixxAUD
nKwN2yyGXjgGLbmuhcvaJX3/xIP1Z5aW6CeX7gZyikt8ailqNM5HI9ImpSLN5faWFecXXx9nsd+C
7xx6rIz+GWzzAw1iiob0lJlCw8d9Hyei32tbyvqGLVB+CFNjR7kxyu665y7cw5YT4J2f1HsPSr1v
AOTK1oj9mlp/kxDxuH3XhDKv5XI/HKWanMRI+FJ4ZhZ1ARDC3YDq3VXoT83hXPllJnntM8xXZ4R9
i/LGSbKZDZ9UtG2u0UPYFvr6OFnZ7JHLPOqhAWph9HahOOWI/IqWVrmHA1OCuhtNErfVJ4YKNvdK
Tin2vkUGDrLkjc0dFSYqatWACi3mXMBrMi9JhZdwu82PDRHYoTKrdkh9LSdfq95fAlRz3knlikhN
yX6tO1cmu1QR/aSUQvA0FZL/VTTZsSs2ov0lg6A6jlgDPEME/yxOOlf7dV7sl6J7LcTxa97schEb
bYL23kb1rPL3tsG2VvF2aPGJR3VvYfVMFzcfSN1U7mHXCyimxoeHW30KkLR3OyXiQzlGZFpRICUU
ByKIJeigh9aOu/o+A+/6nGHAk1gF4kzhguryVsoaUYxA6HxcIKrd7fDPlYrXA/gR3xa0H4gbN3C9
Sj/1Om6XJ/b0Ha0obHxO3ARplTiYkKZUYz4UVIr50B7w/USTJ6R9+QmmCz2jcKLqmsdBCruYrel/
3j1QWf11ZwZgW8ftNzJhd2QWSNuV57YWKdYDIGU4nNR6h9EDrfmwCBcDfs6zyGtRvjHwGjR49fqC
d2tXtdNG/k3cPBH8TKI4eQF6OOif5ruVhQlNE4Ybt4MHTkKBIvy2gmf2XNNZvCejnxm6LMoTU467
sqBNlPJeI5DYukQ3Ym8MWrRMVmtQWSsVcdOVJioW8tT6tJknFc3y8nFUCNCJi6i2CB9QALRiB1Ok
tAE1hfHwf/eYTXw0nsNLnGtPVnpK8yZJ9MwIEFZLv9wYP1rkecfCyD4hHriDxVgSqSYVwfNRKmIW
dDKV8VfXIrNeYbqZDxBcGVWSr5S6sFTYq4QnRSwe1X9SU2cTTVeIEcHDp5YrPnMQsDJjdhnytY8K
48D3lluP3MN4rKSGki9Fw0ZfI2fAYFt644WxFKHpa72EbyAPc1HrkaeCpQgu+LxrItkAQugdlmKV
vbhjsInhBFNjOmY1lMIdsbFADVt1ovH2JP2pC4Ir8Uqw5IkL3R4yns1F77nOeL2vQjyZ+41L34r4
IGh2qrXkFyWP8AUVWSFJAve2QfSSMhMPjGDaCk4B2MkV/ubXGC7vRO9wcxbMVTxUZZpOJa6xmlD0
L1QnzRrwTSU0ZvoiMbLeka2YOe/qwyH99YhzYqEXhT8v+RaHVeBAvE4bpcHz8n1/Ch/81gHF4kRU
SqHJQdTDR+FarO4ZacGT8aEc7kDkxn06qLAOtKPfC38t4MDlt14W0qhtCunIxnLJP2cS8ggh4PjP
q0+qdarPr+1NRO6ewXjKEFB9vvVoWU6wXHOZ0/iD7tAgQJXCBys9CckGo8jHGBVOBadAU5/Xl4lO
H1cpnbvrTn7KX+XvY2aU6USQ4S0h93ysLoQ0QpqkV1E2Xa49bcfUlDhxaO/PpTTkzbUZPHuSgELa
8Mox+uvgq6jvlWyO+4qQWLf9R/DwJNc4bA7OhJIGHDmOrDssLtMVUx8ZkPDUe1p75OOM5u0iUp8Z
twKFnoPkE/ZdDeEDsafuHi44CRT7bBVtLBczckJNhCu+0/vu2A2Dxzs0YqcV/FO6vPWzDwfDuUIT
EZpq7PcEoeARfQ8qPnnspHnm4bjwDVMkLdmviih0mL2HkIMrEl6Gd/28c1nk0R96eii+dlYjjkTj
AG6kldiz128u6OtP2eIK8YqdUbAIYH5wA6wcskMrTIwH7yAoHxMTiMjV0gmVohFFEg7oBRDtoSLi
tzAZ9LtKhRDiV+uiHfBeEQWuoi9uTZNp5Cce709gSuwCsI2c/piFoH8yvyFRnuiwDo2YYuui093f
l2O/ttQi33trM+iZ4ucMs3KJ55d0NejtzEXrLvdEklFhnEd+4zhrL9PNEcc+/B063AgH9RPm7qdK
BAMg9tgVjJWzSh1QTG7lVJ2BCUA8pF2cwlv6lYV3VqrLjzS7JQZP6L2BQe5DQc9liWe1sS9x6pz7
Ud10Y/19ms8CxCAJLNw5K1NtTwH7oQCMb2hEzYw6uscPiSGwJXZSgPrsziJc8MrhindVKgqRlZL4
7tTgxCsdXyQq8aNZYbpMTBab25dSy/8ysEkTdWW6LKxbM9TvwlciFJOkdPfX8vzCwc+j0f6jLb3L
/HrUhNbS287VW92oKlxlw2sK7+3MwlEgQVTu4kbTSn45K3OGS2lUY6T0IAkpxwSz24P8eWoNhKL/
bPDsO/o6ndC6ZoCB2o1vAkRTA7CvIZUuZ2W3HqqhK96dbe6yc5zfv27KlrvZNlHZJqpcyYjcFrrQ
qujzsC7m4nSKC1l424sbkiIu6+QAapRFtbD/G1UjcdA8GqqJEGdLimeE2UM4Jc5PsToIvoF4fV4y
PfqpZeNKn7/aLRf3NgNGbuWV8camsbzg0TQLpFptgzXnfJiQPFoce5PXejfXFcQtnDRgOmR/I4vc
T8gf/a/HsQT4arXDU4ioLdw0o96jYPKcKaCLYf2EUwG0LWKZaW/+jGP+wfPS0BfpNv1JeaH08PW4
tpX2QHRyra/Ivq46MWqYXUr/CYQfUbWZOgd7zRwsZVlU4GzlObC8a1SoDkc3HvaUd5Xs3/mMg4FF
aI2aDCp1JqRpBEZ/UD717iSICJvLYQVrYghqL2OolegGRYWyYm7HWW6l5S5+9/139VtN4UmOofs6
YjrPUztw3ZJ4Wp2McnJibmiHjKG8kdJNk8Usqrh6hfqlVilmXFpH54fM3laCmuNC1X+vN22MbXXY
qBcLOVXOMSdEfGf1CV950YwSjRARhtx4PecWC9YnNVwkocPPzflqU0XT3ikzNJkTo6bX0eCZtmfQ
/WeYrOotPDwgkmjev92aieiIPR79+A2loG06wVtJJFnE6sG5m+OfZGEqaneTZzkzK2iossf2Ombu
jV4/RlLM7jrxNcWp4bwsuuCrIXBnS6qIeSwXrMM8ARmIsthrT5Eopl2K43JmztR7Sc1Y22uYD/ZE
1haj0z4KDdeiTcEkXyRUQxV2uZSJK5RbquuNrvUVqV4+ucXnUguynUjO76eLtSwAPlbNoC4z4SLK
rjttiWrUfHFOSaQrFpK03oz/AIoEndwIEEHnjDi8p+vbYXU92iCQTrlsDCy5Zdi66zVgDXITRHOh
3atbBmevb834gedO4SsyMv1DdNX1GO61m5kHcTXxhl2bp9MzL6WwFalPN2xAtTQkvisM8dtSq86z
HJGCGtcl8J1zmSQZOQrbPk2k0Q2Zqu2DAK7GwDkpMtUoSoid8BXMkMjHHaXJmcbDeLKWuQAH4Mot
QxkdjV3cdWWUB/I/Bm1ULQws7ejK3uCxxozccFCY4JTw+DS5xH+R4+f0+XRF0D3smZFfrwJky3/y
9J7hVLppejhNu2xBfKJQZ4dpuWwz57O+uZ3uHZqJwSTRW+2loyDOJASO3PwvCyvIcx3x659wNtsW
fgW5VHqVvnSKzc1ZcDRwwi1ouoSzSzmQ21ZXoTgxJIWYQTh+gp7bjlS/bLapxk70QT6l1jl5Dbxe
uIn743h/cxsaOpUOLjol5DR8QU8jkNARkU44IXhd9RqbLm7YQHSzKP4QftnD8t0wdQfgxn3sU+zV
bxp1QZzzO2nYxaHURa79y7gMH+QhYNzB3M12foOaOz3YFv0CWC6oS02d5qnYN8HSiXFWEP69yqxB
irSpVh1Yl9HBogC+dF3uC226tXrm7V7muimjO2cxPdHc8dGKvTUkfaDb35XynGt1r/iTqDW+j7Yc
ZZSmsZKcuv9n8LiB7kVvJFhusuxVmu8xMikvN7N2X7xTmFT8+EmnsB0f9qQf9/I63pAEoORKNBIa
asZntdGu6MGNN/I3azQ9iY+EZ8RMeCEPLN4vrh2z5HI4rxWX0zOCkbgliCzwCH+cavjIpTxzUCd8
Cji/5EiszU0z8BP6+PbM993EaEFz58w3dvOj92XH9oiotbGNvJq8nL38botc8hbpuTfEC7BRjuZj
8tFBxd/m9sCdn/k31/qf8eMEsA6A+2iTclGEDIYP99pPPuRlGP/4dsy3gbarcVRctgDn1mpZb+sZ
7BCQI9PK0AcCAG9kXlIy07pAaAVlqoaJP5LaVifTTDZVXYeR7ahhbEbT16MKSERSJ0UxeTnjjFoM
IJWvpjurEa5aogjfT06s6b9ELRY9H2LnAqvOazz7IP2eB0CERdRC8P03UAmuwVKsNit2j4DukyFM
FXLgsAvrtoHfVu68KgH6PaCjprpwxEAeEGU56AYGSxbkhTxMlWDa+Wy3v0gYiWLXHrDOBO8Gn3UI
emvuYiL/et67bgymKOufwgDbiXVoXAMxV/GIJyeXSWGfuU7rw5X179dROrstPqH21PcUR8CiKZkF
gxrMaATI+8xFZP/kdtxWzbvXAVo1K9n8K3VS+ZQFwWwRDNq/bZCJ9rN0EzhzMhX3DzGTejRhjkbX
nnNNosBk3CQMqQMIbHs2vpMXOCGLEsEEX6Zfb/uufsJKTwFf7owNDJ+9ivURICiKPDhQGXqdrXBd
DNW/03A5MpYyNRxepUIDXLt0oJVoIEznz5DDmc/Wwt2NRJHtc6FA+QZ3w2V6byBbSgUHawHMd8do
GVOrDtfYyggUTAUO2NpIe1jOTWSH8ndwdq4Hf7QOJ5sH6bXRKW/lgJhoUWvqZ578NjQzmO3GAwHx
dQVgDNCc7QILXZjBPV9g35RM2WqvLXndcR58kto1aMCfImVJy1qj9cIygAhZY9hjHMcNvZzA34bK
AUnYA2pFwJgmKg2+lQjfXWXU8kEQoHzrgmIkyg6fvqqCPtIFjg0h5ghuk9jm+sF/35lHPaBgDUzd
vqobOmCE/dImcCkQrLUpYlaUlPFmjlwXWbBP4hQ3kD1yN7mufknH+4kJlm9QOLeWm1HsbSw3sRt7
iqTsd1ZPSAFQ0ZsvXf7/Vspgd7XIZorguZtJ76XkM2w7U/oLPEejqKQ5tY8OeX4GIREebZBvFCah
I0H9f13xBHBOuJVtQcz3q/jhCUHhk5lBm3qvVV/glyKVWHwD5uV+lrnzzh6GjhIMk9Q5E9DjsFir
c8WSfOnUlA0vwYEoUMgDgum0lyx0vjIiuUkxfa76tOYjx3Myx/hWQujljYepXWul3YZN4LRt7tAM
gMiPR9H57q0QcK7IU4rgzOyjIEsLMjrSBXrEqKD+gFPCe5S5k4LJ2h4Ks9RQij61i0C526l4618c
TAFSME/HozKEGh3xi9oeeZBQKOLbPZ+/pdiX0yFUGSruxsY8BOJqAGmaZMftyVt+bOujld362i7G
hQ9wE4keuMv8RpYUuiFzEu1BlC8zCDu46MgKT3a1MxYIypxyHqm7UCtEdNepam1B7fCStdFgx4lg
k/RFwp7JkhZFyzXAj/n0xZZ5IcnT7w3arvJiB7jCleA9F5XfLPG5d19fg4AXo7GnsQ7QaI05/ZyD
RYTLK/GTVffcmf3cI2578MPZlzBfXxWhCgMrygWJuNBHib4wLxU9suLub/9c3/9q48RvD2y42LQe
cI5UHMThvVy0AXBMP6kQcFN5jgcCwkn45a0A21HYo9bb/+JwLx4hQOKJEQyOB09zX04GjKECC7FJ
PCC/vcPIIyvxlpflmpQFhuhcsURANerWnrgPQ6kSUzbNDFhGFoDFZyX5jC1OqOdTxom3Z0AJWOof
OlGaEpnSEs2xOhenA4RAtsv6SwwwCKYdhsLc+IQ3cj14rZopyYhNcVb5hDt2V5QT52rRPMhPDsdv
rahvAWo4kyG69CWvTinLTPjr9h1QgtLwTEY75G79P0DQwdBFoBSDk1QdFw8hHd4qw/ajMt/c13sm
pUPDsG6brg/2r43dDAKDRwoAdoDBWWKL2ySYtoZNP6LSmy9vFaJ4A9n/GRcabWfURp/KiNXOD12p
JKsLl3yvydhlMtxIYlHhjBiLmCdaqTrXt63vkqBlQX0l3+uA/+bSNKUcTj9xxW0r2g7GVjzfzTbP
eGGBHc4hzRF1D/kYYnCiX+rdxnYbLGbK2NbKQWWVXUl6YKGXSNYzbLb7lweuGPhzvH//jH7+XYJW
VCC1/iFYJdMSqXcXvYOm61v26O3CdIepL45VLap4XFzNX5B+PhJQzkKUFEGwIi17dqvw/EcOX7ej
lQvaczL4X0QkWU7KzVP4vjmHzvgw+bz6tHA5J/Oo7Y/r5QanmUaE5IfAQ9YLDa++n+CToIDGSN0R
D+PJrrd2KO/eJAIlmZQwkFg7quG/hYWh6ZeblpLk4W3ByUTTwJVDA6MfsZy0Am6KqfrWNrjVvL5M
ii+AAiw8/nSA/yej8ehiGJPRNa4HagCpTFXarboSLE00OPllqw5GdQQkXBq1/7cfEO8WSKu7DOJS
RZlmUbSGWaABr1fzMMAx/bO/S0D/aA718914nDOesqyY3BQ7+P2VM/QA+eqnVj7G7HFC2jejAyQO
4MUy8Iur1/W/uy7QhYlEj7HyPHPcO73itXIzjfy7LPCfxVqq/yLBi4XMDbg/ttew9QLkqDCi3XSu
ytooYRiQGjDLjJ4vnr94Sjeu/qTsYyIX1H5v1K8sC/kPRgb6+EKJIBXpdligEBJfQTuUXNHIYwPh
wbFGofQUKIPFYgGnFWKmKbgFInkMVqNOahvioC1GB0RNutuvnL3X/AioY/ufU7SQlNmEZXeQrhc2
sndjm4tKWdd8PUraYk9miEtehaer8im5wHMKUZ6KGd/totyE6jk9XOVAkTuUNmq7W31zr/8ryqYY
G/pctZH+JPR5WGrnOMLtBfFOgQG4qGAHGsS5F9h7+6FmpPIUYuqw6ThzP4YunxQSMp0EYCIjVly0
dK32hfAQLn4H4h2hBkjBsJADaIi2X6pNbmJoJC9q+kdTCdoA3t2CSqFCuhegQxZFdZAL0ULEQas8
9LegYGjiACPThAtbwjwHTamebnxKBzmtmvOP69qPP9bx+fRBi+Y9SasmWXo6qEhp7DoRPwPhvj+b
8es6El+N/pvQjpzHDJDdQkznLPlF1HxXQY+iTEgyAkTKqNWPc7LZZUZkO+a0xDc+TbLUpGNfgA6i
z3/KC8vBmUOjSINscrglNRew+57m6oS6nwpKHGsiKpGEmQQGaEHE8NT4svq+QTTfYwBUix2q5akX
ouvAjba1nj0elbGiLgdTefrJDzhyeX6MyA4AIhK0mOpb62nsAssnbazU9ppobC3QeVqjURv/++68
1VkQ81/FcfwluLWgbgNbA69MfOW5RD6f/0zUOghPJ5D0K/YY64vmzNRB/gnBvBwBjv7rl5A9pFUi
8etqyZqS9znDeBcNNuhiXVSQ6fE5pbqb+mdabDvBEV/LMoH23GHLcAAGJYyATLVU97m2G2IY9iOm
dosUwL1Yu2CDdPLUq/YslH78IUjUrMzlot8SVRL0WiDmYUmAx4SAhI7CyLZcjZfibup/vevFFAXW
I4ZvKWiCqvDLJCxHrlDDPgrbf2NA/lACTrO/OOHEbGOWyjoj57kkoc/0UG48X47Ox4/rO+csZ3M3
lMi8DK0dEP+f5zZ/8G9Hdu/sII1ymU+DKW14/75U3iY6gnn6thJBWXNkTwgeRBsfo2ZLjcu5nX0P
jW2cZEN4azNBfdxblZNWbWoU9suEcEPlgRJmlodewSYD1Yr/txSj/QfpkqHjSJoKD/u1WR9Un5BD
VJmBaRpRnxneyxi+NImj4n2IRMQLcrGAEzsCW/fSAEnk6I9O9i+nf6+GyQBhRkJXem+jCMOgJVxu
jiKNhGU+ZgOqE4x/EaK0VyF7juJ6Hdlwh1IambWRcOq76ghPUxcLttUXWzomcNxH/UewFrxlPd1a
24X1RWneLZT318CJ0IPyb7/TCqfm5RRsM149o7CC0Ob6uZmvLSA9NIbdqZvg56x2ZiSUJ87Zgn6W
n2+iPYa1adoR5JSh4L6J5m4cmhe+gnBgAdLMZXtgDByTEsHWiJpUXcmx01j/UbX20VOv469rFxid
IBdwP7LyIHHRNUZWWj1SZ2UioGPZ1ebjpwBISr0ax466kvDYCRyFRZ5GkJMa+8qojTxmoZl8Okl9
CuELEHo2zXONJVPNwFubIij5y/bpYAtguqVzGLO21bHtLOQzNkvmpOOtMdaQUW+AiRpv3sjps9j3
Lg1VgIlAmYCViaM8+KIRnvBQMvDMQC5yrHdd0NtrWf8+TUMim3tflq/qVaxpEf5x/gxgyKfHHZHK
YS/AJod/wBmue3trCwNBNRRwAlJ2fH+aLdPzM5ep/uvU036xps8ijCvk2WOktmk+hv2CnPR3MTiN
8jsIa8LwzMfqLNOGCOGCoSMBDAmrNbUqfG3uNFRy9ppjF1Oj4OmKvevRi5rF4vEL9F42fwZ2m+fj
p5LmzVrHHgNAqy28igGs8tN2R1qQahuWSwoUHpX7rzgMnbG+RQeletoFhvImNPijAfDjLE1yjE69
JXh824ul0noD1cpV7aDzwDspK8y6MJISG7RvdYGvI5b9G2mheBqNE59Ap2mSDnLuQES0AxJfNAu7
A6aSqAw+mtvsBbIzGeVsFcl23Bvl2R0QPJEphyh2iJrbxfiGlCGOq/bkiC3kfVHfQH4TTSoHq3Qb
Ss90C59LlVU4oXv+XTDceX8HJ/6GWUW24w79XApf3/uS2HPXWjR9AQy9qvJPz1CQ/oQGLPvlyBsu
fPtgqzZ/mKagBXSobXgtXSpd80h6gtnK8lMm8tpL1rwKAxVnuhZqtluT37QafEI/iD1OB0YS8vf6
OSng1XGQBBivJ1WULRGwfmEhHXUa1t6R6pj0pxd6jvSAOolNZyPOiXucPz38OZLxb5Yx2Aon7vWn
2K20Dy8McN8iDBLUeyzgQIcbdGKirTHwkTXF7HAm9wrJMDhVh6m3SsdME4TFZcjViQwq1qY08Z1c
lzXkFjWD/J2fYapp2QEugDZpBxR953gGl6OTZ/xUJAxVbQ6O7f7mzqgSP+brNeViWR1DV6FTKySi
PPL36J3RegCvRJXFcIJQAR5y61UC3HTKCZgEiA1hfJvRSrZOq9oAmPPygmdvhXCN+lwd0toVydDu
5MdPCrJG/89WvAeLjCrmHjmAJ8nuxp3HW0A2A2TGZe2bm0o5iN2C71q4Iw6+oX4Haxt/8DABCX/j
RU/5Z0/5K+iW8dOz67I7fWjcHZkRh50y1YbMNG9cUAztTXXB7oPPYrndSU8l8bUeW7UmPstnRbUH
EDjPble8D59LVAulL16xIQfB2OkGQh3QlDSSZDtyuGHfrhDDS2z7YvBZX4THJsg+OX7GiWjRc1Ts
SGZDf1XtxXCer2JXN9WYclB9pPf1VutYfgiky42u4ZtFVnppp08DVtWqmmFU6iM8B7aLCGQa19TA
lLKBMj6cznSUhLLG7fropxkLffQaDUkDtPtbONphWa9oGQEJZXERSKKzQmMDMbG3Ojj9+sD4LXMb
RzzsOyRAEi4Q0ZweWKpEmfBhI2ZJODWcMRPIYmTAqbA68NFIC78bMHELWy0RdjHPZBn9bUI7z7al
Nw+xF+tk4/DxUu6RMrNGiIXJ/9j+GpH+SutJc4cEDiQHot/bbwQYNgczWsIaLf/t6zMnfORkfZgj
O24G4DvuD1L38e8DTYRabLHh3e+BQRPJxi43ahPVYm8kt1KFwM1Fj4jKwX/toNEK8y5i9ZsYwuul
weWDiI+2PDxUMyMYWjvj7GYg7XGgqcSNHkt36t1vC+kgeqik5ggtOSUGeSN8/1MInekoHtNAZw1g
gIYAY317Z8dcNNCSpH8a0GcRC1MOteIPM5ccbH869O/6BHkPK87nZ6cIsJvTwOuWL+co6zAZ8HTt
mYh1UjGZNaykSuKAg+p0pU4krQbwq8muEwt8R1eNciVA4GCRp30lFT4gi8L86M0zBuEA48CJD4uH
mgL9kHQ73R5RR0Ko1eElUzOYrqOgseSPUZsMeynCfu4yg06qeXMufUFz2h49BZtlExaMS1AjI/dh
26FgrBAlcV7OlzL8i2Y8DzCAdzjbjKyS8P7cmeW1ycnkcy7PU0F8TCXrQHPxOISJYnncVhkot6rP
WpbH/8tYNaM9wJn0tTekrO64u3Ng+ygnSYQVrq8yObgcM7stGdREvZEhhKE+9tXHg1hUbvfGtmBV
kmfVmdqwRx6koWYWn8RG0uSBrtborJSHcj8RS4VQwoWa+pn8SVp9afDwTxd+CDWa6Fw45QdpO8oM
5JX6kO3i/Oxn8h1r0OU5ShSp0DOhE1UE/ZPY64y47TyCnVzf9lTOTMesHggPkxZucCBhuSinYw9d
NvQhUKTR2XQUIJ1FTqRvVE5J7KMr1u97cPp2PaVc2Z6q1ybpKu0oBqSARHLuvjFh8miYJvdWTHoj
f4+Wf0df8WJXzqGwC0tiqOF5xPR30uPYi5BsctrwBpg8ZAIIkVXGNFx8Jzl/GufnMuFgpQupAJhc
iKtcPmnZEEvhcgKHfe7sfTLzbVD4yXRij3dRCxtmFkv1IGsyAE5/No74sgv36Q7gumN0mnNNzzwv
3D/LWemt1tcyXZ10XdRSuDmrNiFrtxlQU3Wtm8mvEplYsdyvsfIzPPmUc2b0A2j+uxsEYETIn7S5
dE7n7aeTFPBO57EAMz35q3r6OGdb9b0kBQLVA93Kh4V4rG/SjYu5T54ZX7rCuioB59qZu7y7YV6a
mnqp4FaQ9/lASBHEH7wtutgh11ns2RWfoC9HCo8/RdmQcQChoePRZIt8cpDW+ITd9tF79ZleQDNb
8Xz36nZlBcGedaZyopopGCk50j7c0UseMmlUoVanE3IIVT0bIPt2WHTrXypCqfx0pUEWz+SNI3gX
Qmz2soVthEuRVoAU3EiG3wFSpowrDQ89NdEqp095ZnYU3da7c8aEvAPL4rEcOCpeD2Da4PxoqYdY
QPTEWjQvD5EmUgd5IDYerGB9/sJ/dk7AQzNtSCehqcHkGWyysl7uSyOGMFzl2p2RHez1Tl2d4Zkn
mRB+Z4b2TvMV7Wz2JsHZVeQwiOJJxbaPpm69cQ0icZIZgo7Wsc8lpQM9xnU3kKot82DvX+4Tgdra
NgwxRLaNPue4oe7RQMGyWIVJeJAsVCM+KBBBiWf63pa3mvvT1co7Rjh/xOaw23Ew95zVdYCM4lck
6NklA7vypr2C5X79c5oEi6aFAMN6cy/1k4IeB3wJowQ6CNysJS4gq/DPNIkNBwCZqtrbtXmwFISx
o2XablfNCXXj4HMcPtokDfe055eGYDzOk6OyYhrY/ud18OxIoglX0S6TSt4kNys5Tb1IDnUebwq5
J7alOgjvb9JeoYDMPfgjorNRdOCq45+h2jAId3j6ND8B0mWz7LuYfhaLT1y8RUtYaS+Dh9pw+jj2
z6QC9NM8e6oy8uY5Vlq0L5Je8iBRBDASxlYXcDi24A97iPfbLELWpY6s8HaWEdLfsnV8GkhxwmH+
2LR53lfS1T1Jcu6e/4juLRuMzgJOFSEeCq//Ap+Ds7eClyRzxFQag8eZ8MLp8A/YigYZAuKlQcWZ
nkWyZj9a2JCNRyrYQg5VNZV6eIFkCaZ5sMV5Qhh+4THKbnY83CsRhZ3QfGXUw3SrPPti1MNaYMZm
QbRSR5C9SNwyrgkvX01HNIYfw3CwasaLggPrhSP+lH6qWOLHR0/9opv05nCWwZAKo+dG+sZuE6US
KkYo87iV8m+Aav0zkWshUYPGHTevo1MGaTqDa+rQVwErGFJLuG5ZKieIneH/ubMzGcE5zDJUskxB
pYgfvK57e6A/12KNmU63VPoJIm6uPe8E4lJEQjDsA+ymceoRbrDgd4TlALHb/0e9vSFnNHmR1Swz
a51nPmD4kNh0rzObzjpqC6f0VSh/p79G9F1IsamlJdgWvpoTgsULyXWRsgA+Ob9zydTD57p1O2MX
+GPrJ4uKXQdfDsiQ+D2pdK/zP5UtCmK0J6vKxk+tGT1e2sRG5c87nqlYPzI2fJdaEFYB/+2WHIxj
mtDaBIeEKN593d3d4/vaEsRmwEs73K1hNG+ZvQH+88Td/0P7jGcC5mme9BOpMb4G4Spgl7GYVZoL
oWdBLJfioddKZnaogYt4U3WAEB3/hA1xHFEq3XKMlBTZ2rff8VJMS2L00GthPqOYzTfE7F2CViKy
gGeEJ1Z1s9RrOUM5ZTXsFC+FIOiKpHk/XYbJZ0YCfbTMUA77f+97hF3YEN6kt/z+UJGYyi5vvEo8
qks8aZOV/qKojIbTDoQhOp9TkZlQ9QiaXjJCvBPu1hYylx3haa5LqDTWJ8OTp2nyVjgiwQAuJ3aC
u5QZCCclHFCZttZOPZdU5VuDN+Eq9JKY9pMnEkG2Qq/1+mKybTJnicuvqSOUZgOcOmJAD5D111nt
YUwQe4QK1eX3yZlLs5qOUrX8cJeulx+5iFryEEQqlx3C77lNXRIezQ9zJz1TTh/WVzcWdqCKEJHC
tahiouJNTLyEGi9ajPhTII6SiVnewmYxn16S4heGWPKvCKWJLm7iNKD8MrnwKjyVA2uhZ97zIaZP
ccF2pOSOQo/GnvAYuPaRvnd4osNWf1ggzVoZxev9Dy/oEsHVZzQGcHO3nx0IY1XAerYzyojtMhBe
pBzlCd3opweRPK8axR6YNujC61B7uDjTNNO9iBAPQ+N4yiNEistU0EE1fxUn4VUaerTKMi/2QneD
92Es4O7+cP/e2JELEoi0Ct1IwJGDWZCXsR++9zHf2Tqu5jJJeB6Z4/1KJMX3cHPZwd+T/OP3ENA3
+iklJxI6L6ebnHhQ+N/tha0TNoUDoqTR8GNCfIzGDIq29L8lvaud/bbeZAck1EqGhSFBtx80iFEd
qbNFWqMoxbNnvBMzowIUe0ko2bYix55G3V73uhM8oE98jYMbLLXLculZ2t7yugU+9m1/iPGwxTUl
uyNpKqr/v+nRHptNFMiHBXKmkYxe7atvHRiLR+741ryo0H2zwHsMWo7Qbk35d8S0m7us5Nn3i9kj
jRY8rArTPp87c2ByZrYzIDm7Op0+6+kLJ8p5m5u+lXmlvCpv6GPzBi1JJAU9xLV1XXyYPRZOmXRQ
3LhKGzxdzpeQVffdhvAA2ZnyFamNKTTJJXDvH88VcZEDa559Fb2t6pdUi6OhAngoVgk3lzHrxZh7
d7sQPM6iQn18eyTzd8JYIRvhIbPZbgZz+zIVdhpYEq2Z/vSd5EAY9cdV/V+nwatt5SWTYE9j2CuR
nikYS71cjzh7nin+weiY7BMeXYbeukakrz7S/S1iPPbOHp1i3BBIPSbzlKGJ4gevdwLBGWYwxnkz
1Az0VcGW0lsaWeH0lB80dHBOi7MYo1+SSjltgDpJbSY3uhuokBXNNz9Ho97eDQeS5051QfGnbDVd
vrgGMmL5XKvHZT2idDrvMQLKuExKhjzCWChG+2j1SqjLzGr0LRjjjPWv5ZmRoXXsR6B26z1s3hjy
oPniBzM9dL/9ofJI+cq9Jbddrf/UIRWUiTlCyBrey30Ho9MsfvN2Dv5EcT1sNUXbF48FK4CGzfVl
I9HSroMeA2TlfoPHWnFtVKJDwXgLM5kpUuOakOQbvSwV04xMnKORjS50yXmonmbTuZTm1mueXcfP
MTCjt7LBYjgP5KbNGjNxCnRyBa40XdnEUQwGWf6IfecOzuX516IQEgcGiCNTuBgwa7MtTQNvZc0x
2OBCf7vQNGMBSJ/VaISC216XSbo0TExzYkApqifGdkGn1Vdj1TQkEX/hMoSE8R4C7k9gM9B1lFHV
l0pKzzakfuAwYcZ8pz3HIa27aM+Yspxrg8BnxdUzyoqfHAezgsU3lWs/X+zegNx8hv7YLMuA7XGs
L1n35oJlTxcUFY8PgoszUWl/ab+9+wNaPZ37+CIeDp9S97fCCF6j9aApk8m4QHqVi48ogFLGEsnY
FGAVt4+2eUO6XxxLwrXAsrv8AVAcNoGJThhSobl4DOpdVH1gzjvYt4vEmcDhc41EL+l/y2kUVknh
ZdgMR+bA/syrKP/v6DBlhKa96pZNRqODGRIk4eaQZUrBA/RzDvftcBeXcyCMkt+2p0ogd3G/sFBt
4tTFrgwjSQI6kGS1lwgPfoSr3/uyIUheoDn2zS63o7o0dyfRMROXQQiV52YaOC3PE7Y6ZJZDy1MN
fwc0E0e1vokUcwc60E/T6pJxx1lbI29rm7cCoxcic88HnuQjr/Yf8RzCcPu6yiihNytm5f/w3qQY
nRCNBjlJF7z0esiesp2BFx3nqLRTEedYiMn7K8o7BPXQV76v2wwLNv8F8rIvQWa7pi//pqW9Xgr3
8hJtc+0APK5Goe95/cHs9z+1yJnGVjUvDgMios1C/w0ZopMOWO/HWTcg6G13rB/xTpUHsyIWlFZ8
CHpJj0zSrtZUMfKlr6T233Bg9dGAIv5MublbBOw2Wf1oYTpw1gdHDc+EHDFA6vQZQAfPOO78UY7N
JNZQPFeJfCWXZ3V7x3JCWX/DUXST3g2/bE0cIAF6ySbejYB9r6s6LTC394Yfrkw5q7oowAlKTEfb
U8+4sLcOhsfMNx4vYfx0st42GqOgHDD0kQpNJ6WJzGte2rLe0geJ9S7FPiThO39kNEc8jjd6gnkS
wepbrAodcMoZSI46puotVe45vQGzIWhFMH9ypFnJ+muVbNUhfm2Y+24uwcGi4rwUohiODOXJNVQP
WfulAqqvzN83HBaN8l19HODN8fvWhKGyHkDjjsrsmtlsDoNMtSc/UwxNVQ+5+RdUNw2p/c4eeqnF
wBhYmwoYYjwHXIqAppPa3Vl6/iKt4kB9oaIJsygQNeFWd03MKWcaydkc9nrUOIB8gcJp8Kk8sasf
DCNBMKy4svIwB9EzdysemCqrWqV4s6qJowan5NEK5ELsc7cr5QUTJMwkDsVbvXe7xDbPQAghNlF9
GGyxI9E6FJFNm3ZVI/MNnzVX499jByMlBcQsbrQzSs4cjRNtRRTmCF2XdnZpbdSSGRVx/Mork+ZG
k1R0PJM7/BrT2Uag7PbGLdAMk30fEFa5W30jMwIb1ChKwVWl55g+rUV0bVuB1B/hWHW0Mtx9h5mo
MXocclnWlAMSiFr+VmpLPPhdaj1OVn1H+4vdOuqWWRAk55Qn1oCvH9sTsa/7glDXvCAcpPdBqaxY
JmnzFipYpGM+rbw8EGbeQ8hqL0v0cmm3WWE2KOnUvnkFaRXoswgqAmrBdG+5WjJNQZ/ePycCPHw/
5gXWx/PAHFVDNHEC0J6sYqx2fMpUhgvnzH/fj0z7BOOtmCJIadUpn8r2i+KWFsHhl7jUHoFDvyfp
HoP06MixNDYnctbTtDEpw4uKJeXEAEx6TDPW5poa4vsFdE5VYXSuQ43WMCybPuWIDb7ktJTS2Jw+
hpqiVq8umFhvWJxXnhoaf05UPovigOc7ako7b9l3e58g+6hIrDFa9ZjLNQ1/ZtaQ8H3IiypqU6YE
tcA+pvF7leLHAmQVtCqkFjJmJ42kg0bF+tX8/4NDhydor/fCzPiVDniBUEq0xWLqbm+CXdQzZoOP
M6w9MSjmWJSStfLHGRq8kcl6zd9sTq54Bw9ywxqMkja1FLwVBnZ6D0q0pRdtSQQPAVxfWdIt+TlH
k0MGZU+GlumtCNN8u7t8KuvqhOyTukPXnntovx+f41Qd6nJ4Xbs/RuEQ1mOzOf3/WbnCEM4y2RGr
TNEEblJ9hdITnX5LRkqecbS/NSrQsrnM+D1uL3w6HrT8S2pEOHAurNuvBDTjahDvn1vYPqenwsQg
40pmbGxE3yzPADf8xioc6RVCeTeFsSiHrJC6UzpZ0HAMZwT6VdGF5dKKAOU89q8KtmPFe8lSFKEk
QgDKIkndEXK0OiU/LRyKR5jrHhUolBfExC5p+Pzw9d8SF1T1hc5vO+AKXgXxjlXvZKVG3/zP9wt6
NdvOWutx91LOfQpUg9DltwDHU0Bx70moUiCDCRpPQQXDMCASNk+b68bR1vI8D48AvWA7a+3IzuYM
2YNr3rJRddxkzyuQIzc4vs2h+G+R1rut3YxKRs7t5EnV2M5OdKkn0AZh984lhOIkWOckJ7P/Qui/
266xaFMcwFPhr50DIheng3XyhaJXrBieVOIf+vTbaRl0aDVZmUofY4tdhTVGMJLRPICy5jliKr2T
BNq269zL/pD2WRCvCZGLRA+0sq47eH0TvNv9wCa71qos0rDBA6EYZ4STeO4gpy0d87FGgLDG/SEw
WGHCA0/C50CXfDrHSSM/r1RLG3fEhhMlg+mEDaRx5UcnNTYYAQ+POLpLjFsrATwiFdwlfbgjgxsq
XeH905xA/6kRmKGO9LehnFMXjmMauEMDmLJrHWZjFrcBtIoesVV9q58VXt+ymgM2kUoYhf1SAnqm
B+zRgPbtCG1C7nx270e88DGld+2cP+Hb+ZN6NAERVZSWi4xn3RUnym5OGS395pJNIFOX50ZbO6O/
/a2Q++9w3erhr4Ldot6LipR8KFPbMXxzT50Vx4ytTyGdBbHl4qO8VM4xwByb/PLoDySWeo4vbtI/
klQxn3btwix0c6QZiRNg+c3KMOHfNhmZKGdaWYvCd2sJ/voHg02iqE7npf3uacPSMjFAdqlfKb3t
RycK7d7SBcaB5BKv4xJUqFJFY8d101ElibQc25j8/XkbgnCluu4YHiq40d7nSFYEJVirAVR+5bgU
g1mjhCUfto0luxXXYKBkuQ2/yyEaOHHahkkqTKqDhcQi62ab6m7wtyrOgyWtjT4nIyEqLZ3IVtbZ
lFCEtu/9MMb48gdSnwemNpoucXmaYayutYa7bIJ2H9ruIWdz53VicKdzwkXvQr/NslbQ/SfeTu+h
ZEv4ME8jjKeAJH9Xj4my8jdcDGoQRU3LD9okUNIynfmpMd6y/x9hieS1rpoxRbdUSpFsxfjo8nR5
WhN7zjfBF5uFPEnGK2Ggz1Y05Zhftq4VxeQmHh40qJq16afrp0k3Z3IEliEY4XMRBmk5pyD+nvYV
Y6NXTnbYEff/BNJn3DGgxjtx5cCz4CnHUNKo9cMEdCARA56tSYlc0o3iVnIGwBE6SwjHOdl+bWDn
O5rlHgyPLhyzr3JsBMGl4c5AvJIjal2IpEsTpcz+vbKJxY571GZepM7kfNPRL0mklcja+GcQrtsj
cbBIiIK1hcfnH0R4iCpWsaW4NSbcozyGUA/Ukso29qJIa2/U7jrf8yKNmsRyYCS7lkgBlEA4DZ/N
vR/yy9jhZwbYA4fyqQq2GRg6oN3kgUfvGG48oLXjewbAr6X8IDKunVLstDuutTlhr1lAsVdByAeM
yKK8tqmztC3eZHw9ErKDtzeiynKMhTKSO9PCrwk4INaIs7XU6k8+sway81NgVrX1mw4BioJphOoT
ULYScg1iHP5Br6SY43TgZfbeEM5dDnRAS7jx41sQ1Ea7qs2i89u+Uhlcnhp3p3h6ieyNIgEwTl1x
Nnt86Kkqq1japuzlZStozOScTja5HfRDGBpPZ0enHCs3wREfX0DKcB2PUWmtp2uougjFDeQQWZvb
ijCuJ61zKtc9kVsj4o6FSr+br2kMUQWKlS/Dz2n5ExNg6v4NgDMvtiuDP4Rl3GE8Ibp+dZ37DhhM
dUQny8q97A+DKjL3O0SIcYyopvRGuyinzO7reMt5p9b1S3YiG9Z6KvpMCcDow5yRM+DMcfqpK2rh
Um1U/dKpZZsahk64HOVKwJeqO2B/EcAxEEwzd/9lxhV2YB5BDVMRIkpYuwTi/oQKIrJ8LM1YD9aT
0rb4gZBIRSfby8aJC07plRvSiZoqG4TCGZ93CCriKcMoUfziwtL79+aagdpk2BofsRYHY4YZHZTm
km5AOe8kArfXU+VY6UENAqImuv8Z2qbx8gdobAkMgkKt79UsGPJRH+Md0JrGi5pjX/raRF16yvOy
XxyFSanID0elt6UADvbC99B5dc3r9HGr6EhrwawgxMlfxqaIN8YrFsr6nh1U4R/OzvrL07wHwz+E
rXAypCzem+ysQ3EK3oh8P8z8O/kKx1BZDTfg9r0MvZD95pVuasrmF4IyfslOoALy7KJ1nTPaMpxd
N+WVwdURnb3VnItBUH0XcB6T8ySeRWfCegyGT4nxiaNbij4R8H713ybEJvAx73oMWtq0osRoWKIL
vjPPykol2WiyIn97AvqdtrR+zStEE/jU1X2BxqD82SEGiur8LYTBxAybV8KMQ926ZQaJT6dmKUbr
e4VVF87lGbXXRs6xEKGQBLDmQw5vSd8Zg+toP3ikgdWxIdWL5ot6JRzS7L1y8+k7GmsOOiDOxPSu
j96hN2y77VKyUddEcVouoY/AHsX8wOOOiWmnue5VWNcUb4orJtGTv8IWjg9ZtiodSprpLyTYHePA
Lv/GFb2oNvjUrGHmpf2tKJBoGHMc4ZlDkEhfQ488FKrIYPzA5gmW65608SbPKVXtsNufN57qHcUn
pQ6fKC+Rt26Wmu/u/Q3fKsTfMuJywDPFr1HnTfoaeexjsZd7lTSGQnfu9DSGzD618/mdEo0cSYZJ
Wtty6iVFQSbYe76qdkVAM7aJWMt6F54beOIuM2xZRZrIRcdq8WLM6GUFvzdwUMSX89NXXk8t4c7u
Ts5QqLxMT5lzFC8OdW3R3++aODp5QrPkQ6xP1o3uI1YUjC97GnhE8dhSbPsE7vqeFq1xAZ/SZr8q
GUkyWuqFoqrqOmbupgsgIQyRvZj1yvbu+K1jX1W4bjh3y8f68kNS3mEGHJXuZ0Uv/T+aCah4MV0O
DKVZpkWDwp+Jv1XME/MWSS7LNzihvqLqc7xu6NLYM7OFhU+7KrehIePFQR1Uq4RewnFMRve1ckD3
P1EYaLxjLF9SjcRJ6T1ylBThFZtsFo9giwrhO7Sangl7lMPSBCBiPesTs3HAEU3Gs1pHvS2Nf7cY
Wj3hPhq2gYRzyRAJ4q9ZQGrG140djK46R+fH3HAoyAMAD5Mi4jeYjNL+HC1fYZ1ZqNCrtPbKPPHb
COR4fkQlDXH8TzRo6R4XQUm/DDoNdg/fRtOWxNJ1pyY/ITKM6GF0lQCgo0Is2bLMmxEDekWLr4mk
Pd8zgzoMIJnLrmXQmv8pDLFTpWZPOhGqvCsTXmAn6yhTNoJKgxu8FOoI1QhOj7AFIYG7lZiiNg4J
gGY6xC3j3SWmgwC14NNkF8IqTc75gImWqUa1oR0tqLemn5S1I/6l5kbhVG5p3O2zNQD3Vh18Qdna
XZJ1oSKHOuqSuVwoDPJDBZwTRxE0uLVc6UKZ87E0UIhyf2kQS/HSdvHt3lOrc598dyD5PxBVexyc
AcJKOwY+FP7PbOzeA3lUJUb6IQiQJ6ypiCdXgVnKNyLeioS3NNIvmX4eBGiCw57pnWaFiaUyWqb8
d5nrlKqCxEW7lDPZZlJQMbidhiuhYkPZjjEE45PKvULOc7kIyQlHNrTmlw/aiI4tZu4vScmGu64o
iahOg4npQ5O2ucDsKs3qMObwoHXcRa06+GcwXeBIs5h+qlAQ5hqwQiKqDM2pUoSZRbXN4FDsh7k9
ExOPs/Tj8myz35yjZsNV/A0M3WyGG8j+FUEkM7YGkNrFvGBsq3RnAAmf4ycYE22GUrQAhd3ysyp3
VMU72kT1yBzrKQVXkQMTcwClLwamZFa64aUlM2/oxkKlw+n6BWhjEW0pBP/SRhLbF/buxBDuj/+L
vGLmDERreYfenht+Sg6WpMGQD2Hhnp1qPCMklAXP6kvKi53mCIaGTN6Ci3DYxW29BqD72SNl/xD6
L1b9F/OmWJcVK5a9ObCirMaDLArphReiuSsnWUdr+vEzP1MNL8REbm+URIQvAKj+Rz6jaiV1pAwQ
sfJGvxTVjiJ3jCQ4L4jV8NKhs7szLzDxMmvX3Lym1rd8cpZCLz2++XZ7FpcbVMbrTLO5w7CPjFqx
Nn3VBcbn3WbI/zyV+vleSo4BVv9K+0TnZed6/AiHJvPw1RJJ8KLRmdxKjB0Wdum/0lwseJPwh5wW
o4A8aVpxbh3HZgLal5cN7s7L2irCkwu8LEbdUvFV1qq09x+8b4XBlGvhvWeDbgEPM6gNhoY+FeOJ
BMtRYX1AIJRY09wicl8BVx1Snhw3lKTtpvoc2lM31YYVUueFjn2fc9LVs8qT+RBDAiLZVxZyrYvr
f15xo+b7vp8LdZ8nGhaXF+33ZYvbht+J8a64C80aLRsPKTzLWq+q5D41lINX75t0D9vgprjg20N6
kK1sGhtiDJKhxR7rXRnt/Gxwj+POcFUluDL6R/k8xWB8Lsn0/zE8G1IcbvX8oTmuAq4pEZCcWmsH
4uhQZIjmCYumU3xYaAQWaCo14UE6I0GeE0ziMIqhhV4hR2S5ZqHNPftkl8cz+nCXlK9lLE3pDkgs
z39VwanlN578kSMR+fxd0nS4hFyH2XM0EbYhzSNXUoJnZDBV5aE6t7WWeRSE6B1CH9DRi8c+2OkY
92VZmshI3wGCUFf6cwu533z4UfPRsOwAuhD/pc70tTyaBWt2c8x1HYxLEO74pfs4UCmsVb5ne5ph
s5axZE6XSoTqVozdqtUqa+YlcqA9Na2GYjOouv0Z+kqW1XmycI/NZAS0DpnO5UjBxbqqzP1nbOYf
TDWLNomlSoGxlvJHRktUO2xz5/sWS13VmLndiqQ2FnmHxS5GdrPv0Q7jBivYgdaPiJmyvPCRD2bz
YSOPRXpocE4f/YZPkpiSXriwHZ2zXUXqfrD6Q7TNYLkA09i+ZdP4EKrU9ApJWHYFYo1qTwVzbn6o
IhKvQ07FjvVX4j4xKwN2eljMkKjhRIffO5btn0374avVI/ZSnE0FjEGRbPicCncfiZ4mv85JCNfj
lMlCENoxYfI4+T1QV9voRSvOezYz0nazXSP+jSL5pFUS5LFMpd3U3tLXfOVdDJgpq5+vnkCw0o6n
fdauWju6cExFHZJwjp16l0qFcVnhspZF0x5byJ+/64Dh8cz3BxXu9J64Uj140y9zb3v0tTfLLB+b
qHMgGWxr4AzZHa7EJ/W9dk43tDN/GrEOwWbWE6hFNGSBds539yB14i3fNzsWIDRKU3uFe6jPmBqP
oeB4G0McjbhFVL5FAwoLcrjaTAu+zC0WdG6PsHSPCNrqD03TCDnE4pzvhr9Y9f0EUUywdaVb++uA
5PMLnMU42389LC6wxKUZYwm0iQXiIcD0ktAaSQcKYVa8kIhNElN/2Fq9z/3sKcNh4g33Lk7fUPVE
1Mx3BsQ8gHza1Gp6Z/X8JpQJJYrUQfvE6xz7pLyus4gXG/qJvlIQWxEymiMpvlT+ILCx165YWaM8
M3bG2Z+oJbPe1GYJZORToDxAdmKerzqXIJDfseJbMe+hUAkciNlTRXsJsEqD8vL/CVeOyTcA4uDW
QyAybOahoJefyezP1HSbJ9kMdGkfr4Csg1Tikc2tSgB9X4mEKXFsS3T3+YguAUbd2qDi7hNYo6PB
qLtYrS3C2cBIU2SN/AKSs+hTtNxyAljnxtTIdJVy2NWBu+QPkGDwZUdzomYuBBMN77p/esS9ps4A
KVhXS/TH48+NkqFb+yrldQnNcrBvV21NrnbzwVSduJMKm12LDWnQF9JiBe/hHY+cwjw9yjBq7cxY
8WX7RFnUqhSb4gNebAQKSKjVWzAU5IJzMNhjOzX0akvqh7XKi5RCVNkNIK4o8GB0IP84zTlVaxoR
q5aMPqwrtFZW0r2L9C5b9TUoXen9g2/LMiqN8jwE1JaOdd0yRRga5wCZ7doPfbjwfNogbK60N/Sa
Esg2uOWE99oas2Eo0q+npjv5geKinLj4o4VkJhFACwTjh+p4OwCsyhF2GITwuFFzVR+KSmbQFi95
5U2bcLtubU50Cu0iF63o7yGXRni81SdZmJlK8r+yIY1t30wu6gh4I0y8UGUO5PldPita3OHyH7R2
2a6IMLwQmqlrEvHpW/JfKjo2LH5cki3sDOmuHsYOqBTyuMSEe37s7kvmqMC2V7r47fB2t9mndRpb
STlN2b7IgmcByOphPKNdASe1ogwuTkjyxypK4eOKq3wmE4cm7kqjELkw9BzYKJ8piwYJ0cVx0YAT
YO4+CMUWgSkP2RyysI2UFQx1tBkRV33zwmpqCD/YPXZfh5/UkRIvGl7h1oa8lI0GjK6n/dV/w/Km
gP8PHdq1IkJfQrKbqeeL6N4y3znSIthT9zKiFBmyRl6GEysz4XEkmyNWMx2fjLlpBQtCIkXnD0LE
LDFV0NY4R5WQXDHjXlOdFq6I6omSiXeOQTQBQCHyeWuowf90TJDGcClXHSbVjhLLCAHWD9vrc60v
g4tZPHAjOOQAboLMUfH4bw8iRUR6LBuvJVSe/Ou/QvTHHBEHTdHZ3sUzi+YGPVoffLTFpOosDaqH
vXcoUwN5SX9i6X0xnd95U81ip8ZiPXSR0S5xQZAbs/T4rOkh6Y+clLxfiw7XtEE0+rqNKl26N48k
OGNiiYpyFZuYjsjCG+REn7seeNRvugjCHvGBWu2fh/SJ7UMpmy5+vl8t9Njk0GypZy6B1C0uzOG2
nDRIwbfKL6F2PTjllz/mGHYJLrs76kiy+Laay3vxF9RRZPDijEfdVy0ufu8IPdY+PBD5EIqeWphv
73doLeh0beoFKeZ60a36Z1C/lwaT22kzdBY+OCTLInYxChQhW8/qhrbW7tEyeU2k78Z+4T6C7to6
00QveD4WLfAJ/qnvMQsLb84dms5fmsYDfIKWDNT7vAxGWqalHo0TNGsXk2Eq69+/ERPFwEl0zUx+
/lHzSFMmxBbezO3NiDv1Bca//Z9hvhHADCJPg6sN0xh2KXUq4RWhHY8/qAivGvL0f7TdBdjzgY+7
t5Efvwtlk/mvh8RdxJe4bCmO+nm/fOY3CeQF2BmXaL+4FyvcqMBfEUlZ6NKsqlbOmTFW+Rm9V/pr
vbXwawoFnKxI1fTBoBBKVwta2RFYH3CMbaaAbcSbgGhYHJlwZRSUTOWAngB6h6rXzqkmFM1LIXZB
nyLBsB5bVnjIDE5hSzydkB99BuBbRvz68tcUD/Qeey1rLS5HWOOt+VA6qhjXsn8yyZjizVWwdEkO
+NcNsIVbJTtyysXuVR/Wrqmd+VQlQt+QdiIgDgCCRDwUL/yD42B3bsyNX8M71+YD1NA7G3MK5h41
GmwTpo6T6AeqfIM1iTxZaT/Aj0js+4UDp0sK3+cR5x+gY5Amw/JMDtoaQ+DHTnYm+WDGRkMAD1vb
xwPhQ4TVIuCVJhpQ08x1wUK7FDjNr5ZouQHhftk535aVg/BfLihMTcLE8F/frK2zDhcUGOOB8FnB
zuZXPJaR+/xCyZ2gcw0GEUvInC6JHkoXg3gOUDhSBSBWKr4JUNv/Vhj7VNhVB4LudMjt5FffXOhd
pEeEoTsgg29gTCLPksjJX5eQ9wXqyrStgCO8GL6bT7zPPIEn+hb75hHj0dD8opBXYibNaN6Beoxu
P/9LkpZNg6xjgm7/IWQlFR8IEO/Gs/Z1ywWqetRWOuh1uGS9R/fpqjJFykJVoSrjmU3ykpFbJ/Rb
nKAXdgNPS94R+YQJJg25HmcKLl4xjgKujfb8FUXIgrYzfeLKYSqYIuDy7D8UeB+YMnAabBNmBi7F
PShlXqUvfdhMz+Omh/yO8/OnELwIGveZzIOjonq4IRWDtwdrIry8O8UOFfPSfREnyL7gbG6e8Xp5
Zy/MfK7nVA2U7GhlaQ0g+VxYV/nykxZC3nhKztnCXAdO/3+NVeiD653O0obHX/IsJLSW9CJM5Rms
zeX2Em8ikozc8sFrEMOy7ZOLZ8p/OIfr7QG8GMpAdmEdRZK+MrWNQVXr58aYjnhkZId3GQJLU5EJ
vs640ilXj1EiaRjDfYSQ+Cnn5n6Etd3C2yYqNyRJWM15UHeUjqghPGqKU/ac9pzG6ZjxJNsITZk1
tgVyq3W5EPUUGy41cTXdmYR3Vp25rI0onUtjGLcwXLKV6mAyGMZf17Ody0iZUjIFokh5BU/n0dgS
R4au75iFPx9E9P5/+q9cU8ip1pwRquQ3qbxboUhEcbaWpeRuVEEs0j3kLevoLrUG6E1k+xXEutcU
cboSDNA93DQ8bNvQKH5jSlZja+zNlXPCBBiyVonPCent9kDhWwhPCSiddkrcSW7Ta2geBWQgJXYF
ZmIgUW4kIGaoKZOxYRIk++jD2hXxD5ehmlsvjN0ikMmbZKB2+OazEixJORNnsj3JXTLiZZfvdCDh
+w7fgf66GsqIBQSm9UvZxG9BHk59kR1UB/qMfaOa4nTl0PLBGZM2eQ4avKJkRBGM6qqvTgl/chYk
ZpZKINN/aEQ3YFbXdtfiW3ttATUg+D5X58Fo4ILFcGtkgJl22c1zw7v9LOJ0gjqivxaN+D2gGocn
Ky71/wHgZTo50HsfFvVbPVprfW7z9T+xMwFsSr72dkn/nT1BsZDNTuDPjgOSw6wJSTldZXFx20Hr
vCQZKhNQtRYhe4jXlZ1sWOSXcJCJ0R6JZ1kHOqwhr71wvthyf83s467RGK5v8ZwfcL52bhWDUOqV
w4lxV5sa0CfzMLBHVU/Zg2EqlzBASD02SC7eZgDhepctJ+lKLRXZPqHPmQnRAXkTDET/mT1NVSm/
NpcWvtaNYg0kFCR3qRRD0dPeMm73UXn1Ghi6q1SJ9ul2lECa4Ju6e4KIDMIa9Aate2fLgvvRxI7a
ngOXaW3HcFfLpFuZNnjGAeNQ1sgoxc5cq0XrGZ/8MxzyIN63hn7EqlH6/MI4+MXzJsH+HhF0o1sP
To07f8IUOhJXU/Fve784rmqoSyxdLoXWUioU8niGj/Ff3P4KHOeuNOwcm/jAPrMuff5bhE2GJ+5+
BMH4cUyZ/BEHJ7rCUB079SwAS3V3Es3+8ZWtdaGTBbptqpMt62RsqMVvoSrwJ94sWal+NCli/6GQ
41MQDhlLV4wJw/ztSUSCU7s1RXO2wOAuxwdJDZt6khCcoH8i78hXT1TMlea2d2NO844lSnCBPTTh
nvihP7XKr2C0lPOCfyedr0g+CXJpiQIlx3vrYIiQ6SLPgRFxs7cDnt9HZUq/dKmZ7hJZoFnIzfpA
ncQVDIWAAkXzVJ7elFEpG6vKMWu49VIJtnZklGSdLvN7j5YWhTFISYT+h+ILLWEwge46NWt5f6+D
E89qnJL3EgxfxG/ln0z5mnOx0di1TLmAtTEBgD8Q6QLQHhXjRa3Sxu00AGaRn5NviYh5xqJocl6V
ABuOCTADSZJskbqkqfqXhP3aED46pfrP7mmkz/3o+6IkLOb3I3ju86EoaUXGHegFcrS4l1WTS/HV
R1NukUXIjVn2+8A59dSA+Gj+xl0+IpPHoa/Il94Ccpetar26SUQ3o/aZRdo2wdMma3zwKuflrVDi
UeEaj0K+00Rh5sE5eVCn/WUh03mSZ/UdAMKx5GGakRmGdbAwtd0Kx/3ysUbkkGt76wHqbpmGtn76
GPzyR1GSG0uaO9sj5xOCrMo9C3la2wk8/n9Xey4TI/8f4kBQC5ai8EnxdOrJ+kuBEPdLZNskArQz
vv0V8LUqpZWHk7jEfFEVTxPlQ9xn/WWJQUQA5sIyG/FwGMFxjwsbA6NSypIe8IgY4herKdDMdVmJ
/q9IQ8vLOm+ofm8K5gnDPVd9S62wL9ThunYeD4uHkE7/f3IJH4TT3wG/UW3JGwPcYN6FyR6baFpn
hWryhqtHOC5HV05nTR/Rd7m43/M/Vo9rsoANEguxZGf1Apud+OrZ5qFb+7I6jXjJVrvb11jJMwJb
AQbm7TkTIhzm8VZhPbwA7/F80zK/lVqNCqstjjjO6/t9wDLM/vKKYGkYwYIMBuN8+Hp4LngDwn6+
PVOXZNliEIZvHCa9p/leal0dIojHy/5dysTBW+7ZRXw2Zcmap61Un69V5d7rNemBavV3YxijSAuY
MVoufLsirTIwiOXGdT6nbCMRHTMdm+HpLV+gj6y/uOUEpEeD/4V1VYULcwMZIsOS6WUsJStulD9y
YaBjPSvXpGmuy14sX7FEqsg2988twCZoMOXGS8XxtcolLHz5lLBS3+AippDhzjT53toFhlbg2lz3
9KN69UdK4Bit6aW2zmE4CI7SKgFWJFISVxhvQWYfttHyQhNSNKeeQaiF9ZsvTxyD/ll5HXTLLFij
L+BGHATXQNA5HCtwLBLiO0pPQ5dExXLuWEzfFhZpHosICM4auk1YbSKXkD8orNqonXHQtZN5KWxU
oSf+X6dfokBWugmam5aOcBuTNLILJAVNpmgD/yEsn/PwvVg94ywZT3eUxnqse/m5CjyhuGpDHsmZ
cxEJHym6cxliRM5SotEWbvFC4Wc/2gDbXSLJjs0egD3RDhYcIx34o476yRQoR0LJbmC0gxbtuBcP
dMc/GXJ7xPmLats30Q//2Clvcq5RJAWMfzoG04+SOJsui+bZpxND6MWSOGNar65ql6y1Ri0bdmf8
ojLhS5BagbRO96k6lZ2cTtNLDoiCAP8SLvtTbegDpLKNq/pvAiJ0kUzd9kXUACEY4azdHf1+nfk4
4e1l984D+PP+00muy0xGBroWC5J0cX90Or0XUkuvXtMlsEN6AOL1Zws6Ikyw7/VR4GAe59iK9a/Z
h0BGNveQ27IY7P1ZQJ72c/scZBk/cUIdIz5zdKn1C96rgVhUYTjFd+tHMmoVBsg9A4tqOkCFpJ36
hi+a1u7wVx3nOLBwZFZcSaLyjkU+Ssa+jrDv+2YCKx5i0oWhl33XP+bLYLtDwauOLR8bXyruPSKx
n7PqgSJcvUMJT95AM/YXvskM2wD3uawl6enrJSCLD0SEbkacDUgK0+DWeMjbAnX0giMwFoVZvMiZ
+iIPWg+o42tHUvor8xuX5iqf4NfGG9yTIYNPJLuP5FEK98Ri3mfavkcEkjku/JBeMe1zPg6fap48
rIgkYIQcYAo/c/1KoxFCKxNO2QHMUG/FCIMVG/AIqSr1yY8fv9e87qKHCJmZRVpuRw+sNOvd5ICX
CeksMwrQqvszRY7aedAFh7ZNzIi0oodfLPuwtlaCIwHXa8XNG4MVW6WiHtr/01ufiEoQEBowTEXw
lVi/WeuWCGTFVy02kBNhmiJ6qEX7KvTKRCBaN314H+oVsOeTzEnjBy5SAYVldzIZurj42vrDbFYf
RsaZuLjR3ZjM52LbtBMIWzB89znn3gRfSfCI+3+MukrYpktIy33c/XUuG63rx2mgl9IX+gKbcJw1
mmQ87EnSwAweCNBFP6URHHkIp4N/HPa8CAatESp5kbyhlncu2T/lhJnm5AmYWXOoBzYl2JdXE4Tj
noE/FHDMGgevcrTipYkvmagFMPAnwRWSm5/Kz9IA8FrYfgYwsc2Xke7BU3sMZ+lZwbItWaCaTxJd
GbJ41M0hFhx6mGBtzpu48YxbRVid5jDTILIQ2k641zUMEsd+Muij1/MLgxNG1365Eu4AtFqwXkeZ
79HhBu3wNpGU1tMnJhZBERCaOa23K7gUV9OXLd2pymcmxAbGecEOJ4fzS+g9IPwLWwyv7fK9Uhp2
J6b6s1h24y+1hsxl/RFptbOTR4tA0Bi3Z2Ry+kdMOt6ObwJHeQM8gc7EwbuqipUAyisY3V7fUcq5
DD8pjTDhdFSkBTHLLdGMAMmOjI5f2+8AGL+laCxKDV8KwsD9nRTENDzm70D9scgDSxNU7oEHedqN
H5D/s5CQu5dQ7dJ/F5DwF3ddsEipevTZwLHgAx5a9hTeIDjSiLZxww93JhPYczxWjk2EREKLnMYj
iONCHWeapCOfvwTrZQpmQFydq5R4lET9piPxYWIgDmwFfKR68Y3+l9iYURxOORlccbO0P4W6LF8d
SLNMo3dCfbnIRmYusgMJ0PVAgXSU30Z0o9pkA2aQFDduFN4GGOu8ybVnLRnqk/jeezIabAJNSEQQ
oGC9hF0P/cjRvyJH0eKuL1Y0j63bsvq4HbdaHUeHKfubwMZaftfitLyAs4NqkbYDSNzrErp4IrrD
FHgnEigmlgJIekIduUcOPGc5DHZf8RTs1F1xEokz2/hwLkrkt7Ecff5lqppBkZc6Abo+fnC2H+p1
+lWzhgyIcW639+o6w9pyEywcR0j+PSglL/JYzRsidm7ONQasV8NPpCvsgdzRdLWmkh7wAjMKA+aJ
yZphHbEFqicvpgMb/cgTcIilxz0V4DbLnNlTEnqILltZgU6d1IPjwslojlCjByviSo4mTZCv7CYC
OZmfwIIjD2xq3xZsbxLRf1+uEFjyLX/tPvCUz/TKrys3PyFQ86g9fsi2c8+vB9WkiKFFHYjS3MTw
CtTmNoRw5jeKKFJPeZZJV4Wt+QjIX5eOcNvQPEWfLur+R07yJC9zYvJHtTHidu2sTw+3DxSc4/ca
qiXu9Fr+wbT4B70ERXEJuX6z5NKu7T8LfORk6A14Lnod7P3pb+QrUllktBX3wFvA0C5QwT0R8aUj
i4W9S1qcRkIPMMSealM7RJJSR0HPjcv4WRzug3Klb+ky55gs6HD/SetHuZGvrX77JEXHva1yvywY
J1tIlL6WvVDwMl7uwocxmqBgzjrBkKNljRyE0zlqc+WXLFE7uq6On9BJswcP7rudJGhXHtCfA1D7
Uh+dprxy8qprhavVTfsXuuPQGk2MFO1BQUodzLYaacTveOlTS/Qe7nqdmWGibd2TM/vJx55T4Pai
FvrJEJunlaGcw1MZBevwahhd535/pGyR0/0I/jvlyyTikpnrEG0/CEmrPM/8CwhKYndVn940Q8Rz
6LCCs/nQvfRi0TZ3coxea2/2Bk6vECbYQJPV/Gu5lWka6J+VAolgFiW9UebgPfaLNAN2XUbl6qgY
TSHT0Wa8y6GF6aGRmOEOaQPGITZ4DvgaWE/VeIQ/rUQhIJ6OGuqbB/EPqzqYplrLGE54DojJqUYc
lK+T4I9tpHZB0YiJjkX6hcbyn+PPYbaL4I81B6w9kB5E7JNr7tfGsc0OKMR2cLZEmQ60C7ZYXqZL
0i4DL7aOIJ6V6GkkVjrWTkf40sVrOEHtSYkhMbvVqHeebI8tZfeHGntc5IZgCwAY3s3EZaWt0Rna
LPRe3LXDNK7RcV8zouCJPczr4+6RGfais/z8hUamOxHMadOMWVvPvfbJNPBzQA8/i5hw7I3PAo1R
jUdYf4tmCPXEV1L1udyHuFiVFjcO4TS992aj9uyiKNrELuL1SbuAtTuj4tBNQtuWlWH7B+Q1FmRu
RfLJUrkuM79BnUnvHBZE6JrsOSFg7yJ4Fd52wK/HsX3Yrq20SIV9q1f/bz4luj7/Aszz6MmTywW5
crc0lyoDDBpNm6ofimM9/tVqeyrorrPaJ94LN2Ay4w1Ic6JjN5mLm/m8xlf5NyapD5H2nI8ryhi+
Poov8uiF4HdZwMp2UWQ1f2/3JpSWmu4r1L3kUHAJRrOioeNI5RYD05fi6BkWRCgi3zEw2JmeQIYf
sar0/LNqE2v+i8CVfI647HBVEMGwQs4D/yKyUHZ25HvMCY3C9sWI46Q+nK8EVOolSpJdZ9pGR01r
vzIMK6mpHu1v4jmcYRX39XevVx6WminodkApO28nLbyIJ/pLtfBERe3DLFsKgmZnpOfh+YO7NJhg
J3UsdEym7ajPTbEfuqzHTezUWirQ+HvLLV0MDkWQ5dkZhimCAZpeonz+BjVm+mKAzw7sIyrFaD8/
5qb6v+8I0PB/z0IqUZhBMWDnWI3awMxjziCidGpl/4Vq825gpKdTkcwGgiR6YhYY6NexbXUvKMGa
pI5OT9SKyg6h4QUXE+FKSeEVZjwcH33AXWz/D5ASH09qbOlnMNedX8CtiwgE1pERHYquDLN/VPHc
efR23DES8+qLV7NyVjxZE/x8EPIEp6qffZ3tORvp7Os7voF/lldoO9+DnvVWS6aKW2BxU/M0AW73
aTNgUzdFXPb80etGEhGtpg5Uw50++aKeatchU3ohWcWE5Vhkgx8fEpbrywiaMs+2B1VO4xoAf9yv
eWSX8ByL0EkPkzYWp7N4aINCHmCRIHiY0zj0Zmz73DBAfGO1BKXxvuZTT7WMCsYmF4trllgMEauX
ju2guVCrTw9pVFGUETvuACGUwFzKZNp0hRjchFDg1rhU/x2VHwLGEIiwid+hUtiXohObRxPIyUo1
V7EcvjdYy/2d+KbCHpOaCV0Xw+vcbexSvQFijxHcZJfv3pkd6bNJ/xs3i+kpFGd+h1QB9d3uPSdJ
jl1H3PfU3Qd0AGLu9BYJbJuaHJ5AGVICmD3GIKhrTfnOz27cHst9dO2dR62NfCKw/yuIJZQWNsnt
Jr76w1fyaMJH3Hb+GBDOMLwfqAHOAkHErQevYKa/nbKyfVR/BWwq/tM5Lyexl0w9FCyPGl7VMvRN
gbhn8e2//73S4uBdRTJ/LqD9XLXzEUvQtdnAjDz9g569xvpgpOvVY7npXUVf1SA+Sp50X7Ytaoaq
jwMhqGfxHlpDETkb0BPbsGUAdqS9PIw0NJNxUVKZZFzwQ6CZ4y2xlm+pMntDSis7zu+KiCgjYcIP
KwktESWtXpszR3htz7DmVD28b14AGRNSW7yjU1vmle8L37MEDplIBJOO4yRa7SobzNLcX35MXy1b
ukN7NQdPNVBUT4R1IwPde9wWvDy0076Lem+BIw+OZ4zSnIFt11Enht6wQfQmnarcsHj2NRHpkjyv
NfDqwEKuytHEXjn0W4N6IhVq4NkGdnR1hbYQejl9j4RZBU0+JavtnaQes/LZDuXZk+G/YccX+p+2
KX6Mut1GvTJgryISdC+NwnWWvVNKlInphllkn6QjokTFwiNf8ghsu6AteRSk057pgzAG0HYiX+3O
r8qtEUrKcUnWwapG8g1KZ3kHq8oT4QU7sPfGc5YQHhr2EWzAVkgqhKdc/mOb283GT7Gst9+tl60a
M1rN5D7rxP18YCSC7Xn8yAnnd7LbVeKAdjO07h++732h+ee/o70UpCxcvlRwFiSksebnNEEn88nn
7eTngQylk/xR2+/g91yURsQWJFeV3q/+7YE95l89x88ZEywYhh+YTPAeiT/OFjD0On65j0zyzaRK
/BA0XuuSsSl4BZZD/9++/XL3bAtJdNOYFX3chjaX3cP5g1bEgEy77282+cdpyD9traUasK22jOac
I4MWh4kF/s/sSw+iocycSh7N8oDTxda06Tjw2jXhmZlv+rwmo5qJ/1aWBpeRl02ZvFlioegLsUXu
KdDBuTfxz0IqCMTM6kwjOUkilq4vVn5dg5fUws1iTuM06jgKTgLVOK1xkcRdKSatBrhsHZ5z4Z43
e69w0T49QTTwj90xKDJ5zLUlTkfucb0lHMiRAuEdzFNCzE2+qCUXipe/PvBVjzmmSxg7dDHMqFK4
08f7fuY5rQAOT+DdJoJwpTKnfj3iiHI2u3SsHQcoEjY48LfnOtbEE+j95nTGnETBbL496aKU1/BU
Bc3ENUeemrXZkj7ybMNLm0sBjWO1PPoEEAeoRgDl2G+aS+9Ze0+rLzl+ndqxehziCVrVA3hFIE41
QoTZrXIRvlFRHvymtGMMzLVyeboQArW2LgfqH+KJTX+Lh3qqN12c5YmWKXzQJAX9B/Y76LCdHxHp
hUQ91C/YqZfx4mKWBgJ1xbBjHfhnhwyU12bbuvC476uk3atQr49vlIIVko2VDil8xlfP0KqFwG49
0IucxNm3lR6B5GGTSinmAUH0f5f5ubSyLAIfpnAKsht5qAQCMQNHknZwgNMir40rFIXi1Mqz/+iu
v2uGSWbOyO0Jsij/dsbyaWdTjn9HGEevS4nNCuYvyXNkV4uJHrxOpuw0MGIwq0VRdTWVJoZ0OO9w
i1G7Jz6ozAcHGpEVbzA/8cYmHmkn8PEoJqFJH4CuXhHmwkqJt7Px23pwJaaG0w3VDeo6avDRXMC+
3Lhyu0fKNZCys6kvCxLu96BT1TBqxFHe+SAmwOSbT45EjyT4GBGsd6WGbebcQhxKoAtpnNZbhry9
tln/lS3sY/RLo/VHU+JqtqnvAG9Z/rqEJrIkqYb6mN9YEb/S3mJ8F4Ar39L2nfCt/tnlwBuuymEu
wxT58bxybO1qsNnWeH/SWo8rpIcJbcHJa+FaxDLnkfRCPmSumJKTHAQ+BEin2jBBDcz+4NKNNgpo
OkwRQHhdStho4FKFVZa62c5K5/v19JK2mEeKy1UEvWEoEV3hfTyRNrEfX+XhPXCS6H6vBZ3c0RYq
So4PF709GGOpNssWTGp7QmmHDOoOBve6SrR1bDFx6MLZ2hSRtZ6SgcPbeabr6WxIvA6WeqMCu9YJ
DxL87RffJhBeQUG59QQtbSCGRwqJOr6RtZcUVvsGUm8CkQw75f5CTnxejcxxmwAaiHKrXsNd1N85
rntqahPqpaqUdmAJPVyX79CyhM16RAQAp8Qf+3XxmLiP2+S8rzB8gIuZa+EDsRUtfaB/FiDVten5
AYhfJaVYsK17zH4eGniectxU0udI8o8qT6l0SnGEAgYhDH1GgvtfHrD44R8fAUFLZP3uVaENrnk4
eDq25u0HYNWwI+qHWsZbebtCfzvjOgruefs+/JQB07cyrjVb1hkrfCxxp3VCSGbORSMZ1Ol9V2hH
ovIm2yldF0CDhrLvjl3hNPI6tPs4jP3p+7+L68OalqMBkLVR9CvlORQHSIk/1jeAzoC33bQv+61K
t2mMgzoG4Ezn3Q9lEY1TRcKWw19faICa+IWWA0OE7QwiXeNVW0stHgMv7ikGlajPw6klpw4R7Kh/
5jeymONT8KBvEE0NoBmtOQ7pCyRJNCJ4upezSRricubGs97Sfnhdtq0lDDJjWruPwx9CCqEIsjBs
nooCNxVQonywL+kpQfguzz4T8IWSf8ysfyuyuwXSwXtugJZfnt9WmUd7XpYw2H9faKJMBqY5P5bI
XLH1rAES+T+zlLtS6rmsrxBcKewYVOSNca7uyhSm7fBAIvvgDIFCPwItjkMKm+AWHEItW8uCJVlB
zauVWhkzqgMVF0pa0pP5haZjL1IKtrs4IGG9eiMJFW4tidxBNp/wpVHQOZ8uIEUm0xxny6pgPHp1
XesImOFDh/RBFZKc0R4Cl1BjL3iBok56FKp4+sXZqI3uNbaO03nQMnLLd8+cQNZABqoIcmIlg3aj
vxZJYL7BVKy0RZfu08Xeb5PRhaogDZ0oEuE8UwUFxsS39GfQUklt6wKd+uzZfWKCn3vmi+IFYy2O
0m5qMJ6f2YI1Qb37nKJShDhlZVelAhwjrzwJtkF32lymzo9K6PfrHQcYZolg1Dtlg/R/coczE2zl
s7DiB3ZpiZ9KlT+5hTqE4DnfapVLzmys25qdb6TPPcvD3v2+4mBijxvgPMmmQNA3oAH5sTKP9JIW
5PKBgquJAh71EleSS5MneFw5MYKbvLQpdepq22HtrcfeuyRTmD+fV/A0Zm3bTeWVSnb7/nq/phd9
Ptk0ZBjp8U7Toe/Q7IBwgByT4RGJz40lBajlTXMjvj6k/sND8yu0ZnrfYVZiR8iMOY5Wb/QXtswc
od9+9n+naiTSy7xiA7VWaOMQndYLmEaugXTxOPsGLn5CGAS3gRITqYcAwe1U+UHdka/EE1icSGPS
JMcid7hkn5nZyLUAxNNa6HXtpao5F1yJ6/jn6XpTG6bctzQg+IQsZliYWzugI1w8rsmhjOR4/4kO
tit8sF2TzSZSGx5qf60rY3bry8+uZIbd+5WDZ6pANNS6Xjq8rTnoPJVIuGufb08Oq+CTzMYj/MmV
k3Wkqeu14/fc8+m2fWADbZ9+B8QFNrt91CMv3E5A/F8+g/7HOhB0Sllx2K2JJaYAtDS1l/yRI/iV
yavnKWibldEPkOhuPzak7UoAj4bAM8J//stedmBnNg1rfyo7C3+KfvWCn+ahcDAgrWAw3EHmtq7K
LFxN4Sdioe7rlC+Uu8wWKAEFchvfH7mOGAi7rHaLGr+coDxEXQCGRxnbsMJDAplujNXjIg1K8mfI
wbjLjxe5pSag8IFwGOMPNwbmSFKpRvkLfHIpURj1AKHSPM89S4VFRf28GZKfOaJxOO+qD6/vAAPZ
j6gX89KyfgpZCTTfhq46z+yfGmWf6W7ymhz0lcQwuXJWHR8PCz+ZZIUR+nffotv6DUbza877bbAX
9azBdvCzpT321SovCD2h7Etw6Y1FvNNTxCRo1UEIrOB+LjnaOVe50z6W3krFICwr1YoJMNxhHc6x
P2cNjdTs4b/nUd4A3sOncitcmlo2hOzuwBZ2kx3XgCokuWDote+1wRZ6RIx7ARE03eGWJUfi6Msd
qGDT5lQxL7zL817bp20hJ0cXze7awMXjFbCz8dgUCqFGkLzbh0G3rzM6yjxDIVQ5FgH9b8zwhMui
dCm7F8X71tLKaF9UIFcbWPXH/bbNy9cS1jUtK9jl5TCq2LBUyK7d+8faDAHRjyRrMmzHQtN2wfD4
YyMythYn48rkshrVGKcW1S5QKd/dhbUba7LH027D0PxUcwAfTBPd8fRUvJgiogOG+vP+wwrhxUnm
zsCiWKVHXrg6IxvXNOT0pDQH9/lR5kH85h4a/jNCs63hpHCGKlkhhxuDMpcnISC8hwjJlbCEdJab
h0i7ZCm5/AFK/9ed3BN+t2DHzZiau+ReUeCVXCvTA4xQR7JvW/RIMyjxccz//XPR7bzUqg0O1yKn
AQUGeOoq0+DUUgNdrC0Z25kKnJRflybC8QTVvVJseJdUBYvaq/gCTNKvYWTujziNnEWgVSM5wt4Z
6NHa3LGa43gfqLRMPDmRe3VUOXpplmtUKnF4CfUv27r1UDU8L82lOn6KdxwIOTkPXdB1O6R6ZwQj
p2zCMhIryIRFuZjG2lIOeDrwtiIXzVQK3AkG4wtJkLEqxFKzUJ8tfg+zndf1vcBw+QhplRVyDqka
WfZpO0a498q6xmXgbzbSKmTKOWLrzlPQuD6usR008ffoGLv6bQFXI/GSuczOXpBUcc5I4HI3A+Zr
0kS002/fF0yVKuo35GTkH0bWCv4IkXOauz81vrlj7DonX0GKnkIeKghYOiLluXMc4rIKxFgz6ScD
NOcTrOyb0KgGpcwgeRWA37OdBFB9/rLKmQ0dUGDywrfzZPFatp7rgasxEDEx2ZawzeRmM6+g6yj6
y0rfV6je0l0UgVZ2Ivj5V+hz5GFIlr6FJLKTUIcfUI3zAYQv7yiGa3kd8bD4XhBTMeJtRRuX0nKA
O11YsWWSiU1T2dC8c90bbeWvvWVqBZ96ZRz+75SOR4tNZVeiwRbXPVcwN37ulehEZ9MMp9JnyRCn
Rb3PnTS6V1NT/zvMBRFwrbuPMjr8nCJa9dDjQUc8DobxGq1WPKniQQF6WH2zzvqEwDXRTGnDabjm
fZbkx7YFYr4oc9QY0OklrgbJcLt/d5W+dmGbnLikZaBJVzKCXNmLacXfbAHGTl64mIUIktXzW6N1
Cn3D0YMRCYQ6fsiKP0Y15k1TW5NfXpo9Zxq75X4HffXca1VkfhvlSCP29ijmkFp27mj3xQa8RrSR
dNjdYXZ96EAJvFM6dXZxINRoIbv7xjyLPGipIWwcDGvQT4m2x3NSVcmfJ5dKQlgN6JMQdTNQsb/1
Y1ptjhT32dbqMl2o66Vr1XSYEGX01QfnjaUvWN9oC31l0Sy1pyFXqR3OVVt2x0gcEB4ML+AAbwxd
/SqtIyIIYDu3sWjO7I00b0BuHRCtUTbbzjTrFUjbEP8UKHj6nHUnbCaeqrEKGWwnmXrHDS5ZHXRa
2vJ8oseg1o27E6+JFdAn1LMscRgI/MOTHi3vOwC85I9PPSlBZ71DVjG04eP+NwE0GKWssvX8lncx
riZ5w1o+s1i8RVkkDbQpMvvf9vGJDDlBLna8R/LCoEqdv1oYQoSB6D6Q00bB1Mr7agMoLLp6+F0F
3UTcEZymqWjjhsMF50S1NTSxMmZR5QDmCDvRAdhUDAZE7z4t5WMyT5Wg2A1cZUT45xILXPYTBlck
2DMGWKKRDAG7CRgUDhNFbaFSeqF3e83BW0qBjoM6B+oD1upu4J/emXS41o+AEIdPzr3dMVdSK3Pg
hIXJpUp3qp3Z9kQiFvo/KYDd8RWQTG83ExUA0V6HKj9LbADtHcM+Iz3HHfFjyAr3sR/O0yzqKelo
Cf2j+CgvQcraYkIijBGpn9CzV2//IApsUGzqM5tBflPHhVYzpDsR6ZxRpPJi0F156Ew0IVt19Uqt
V5CRUCSZyZgUJCxrDhq+FXdN1kf2wSp++4cvvQQ9wzA87iL0Gjg9Ud4k1bJsTtVf6R66M2LDBKJz
CRcgTcbZSB35G5ncg8FlOT+XghWdG1r3QUEUCkHisgHUhI7lMlRItlGSOZneD3nbnqZuaK6CoWtC
o8edKvaEmKkHuYECVJnnhvEedfFQMHY/WTQOdX4JF+YkceQE77/pdK88tlXMFLD0USLAL0JMOzlB
wMixVX+ckFgEMut0CB8jVUhMqT6/EiEbZSvyqV4h8jvZoelm+7l0sd5sYfQaX6F3c/KkWftgPntA
vx0VrvokwQbGfpEaUV2pKl9020/2RFIPY0/LmUELTxe3iP3ptDEEg+8SfXRYYel064UBCyrjbd3k
cr41e0yPOWT0i1Vq5gz3cyGQne76WN2FMfOiBe8ejp9YxJc918iytB3myIpi3ZsDw+LBaOiawTaP
Yjb9hy4ALnSnqJQbzCTTT1106KGZGxkKosZbqHb7K9g8+KULfs2G5WTrMoYB65zUYTd04E2O9bns
jZW7DVeRGrxaphAOsFJJX8V7XiWzgdWdVcW8I9kqXsVOJSo/APbnmCkNSbaSwke8dMSs3i0cQBAd
WYDKT7Cg/xzFP/ftWIUa02fTJ7GWrkS/QaD4/fLRm/bTbbKUcv/ENkNJeqlc6CezX5FEQSgowLoO
REv0ZIJMBOjfN0HiBcbtbRLeVTMr62Tc24RcKZ82kzeWm+SZe4PxhVf0S4kKjiI5bN0cC4oEV86O
B3jsAilU6EQ+rgxgFxO8d1lj+j05ArmUwOu95m7JZDtH3cOWeS0KOs0Zyo2DlM/B49HyZnPRekMj
aEZ+8onsCxyiRFj51EGgv3pVNPwPuPgeG0uPZZBvVG14QSIKBU1XzHqqC3SP0B1haI+MQ3Lcon6R
LqO7jVq9f1G+Jwnz19AA1vFLzNuzUbpA+RFt/DQapnvfJDHDf3xcBQEUZuv9b/2zdQ9CFfGvJHOD
+tNFyjbesSlgnT6rZh8DlfCadnAmz65OQZF8ji8n1adFfkyv6dsDbCjvkypDyRjM+7at0naekbgj
IwZQAbwtTEhgt66AxrMYjyN4DwUL3qShPWOIocf8OF7evwXw+levXDWc1xb90tsmU4k3DArjpAAQ
o6IGdP8Id7h3EPtv8BDP9Dva4CNmH0tgsMQEA0eaIpquVIKqWF4aNb2Q0U4WEMN9rEph5zj+YEps
Wcvhon5zUmtjUpB5oPD3hOU1DmywsZ6WU9uTRVLTdZymYB2fSi15eKVC5xfFApM5OvDWI8PQC8Ic
ZB0Cku+Ho0Pj79lC6w5EMAzh4cgautNS5DnmR2bq7NOHvzdnHyWCDXlwtU8BrvMwfxbz1u57e4DD
3tL4aHb9SpGK/jI2Fv9QO1k2oHFE62oL9Ol2xP+8nBmhkbDM/XSXyWunuNuPXMcPskvf2FOh3L/l
YCHxzqRsXpSTJS3YE6oqqFiMrFf/+ImXfYX/gjmloJ6IpV1z7RhtJzA/o7dVIMabyulDFUaLorxd
pZcOytBXDGdiRhSckKxxwbV/D9QP1bCDrOOcIdSUSFAfI9t+uIPLaytG3edMIFjXay42cnz/bPEy
Cs3tis+Wlc4yat3gmEgatm3TpyCVBrTP3g3LUXLGFImRgQ1wgNtR0osopB9FNU+RYJsRtsx/rRgl
22PAVfimz7cWeUg3OfbrzsSMtVRDjhtHmblOpnfOt8URMSdYQrSJtKM7SHRUX4biF2CniBsJUWLL
rA06FO0bg/bWPU4alBHMkYbt32LvucaIocnwsni8aLWgGFG3QIHryEXUYiTWJ3O1nWmzFx8CDsvj
UasnsEvLuvEqr0Rk3GDCLGBEWhuwbYsazhwPOd5252+Q1hwhmid83prkerpkpt+k53vZvzy8jIz0
YQZZPnoz30a5oJH9hnHYMYwGDP27O48ln64icyGlenMyUos1C1+4xOkANVbTUt8di5jd3iC/qfaK
jlqwan6Emgw4R1idDV+dtmrFrRo5IvK15kKNod2/jfEEdOlSgG+To7OjqEIi3fXL7I+FQ+MHfBpB
2K0wQgGUZuGsZ8iOw4UyGSYGx4d7nD7JDQWHgibqL6K/QrvDHgyKNy49jtcJV4j/ZELji24HcC22
I6cluqjGhhAx9vQNRF0R6nsG3TQUP8WVJ8SvjfgZloJ3AmKq0yOTyiOlCDqoK9kGqjXbV2it3ffB
hVbm7tfo8fmLiHRnhiC9Gvsqj0xD6oHygAWvDwYfdUtvPg7zFlpJBM4Unqgi/b8kvWcFS9Pht0oj
GYnzN6OEO6qu8xTxMnxcw8MMycx60J3lq+zZfwiwejBTGx3wlLG5P8e0ZmIbCJQtjMXxPeX8dHnB
K6p04ia9JCHp8Uv178A6l73Zjvej+0ejIOU5Edbc/caUScUtZard6l8qv6c0PS7lJukZ/2Px9pdy
Hn8nILNUoDhbhxxAlHUWfdGF00tmo5BMRT2K8TRG+X8RuL+EZq4lRAR4IQ/6qh+V5kbxsrrNvqRI
4T4dck3JjmEwFGpbwz2KTMlmOJq1vswX+VQawBamkoZvfXOJiBdDo0A2XkrtrMuEnk3yZHnhoxzi
z5pKgSsY2Skn5C1TElAAHirOAJs2MHyh9+1cCkU6udytf/LW7yzITO0H2ndSNkCr9aIX4VWUtLEh
iAxHSxi9KsvMBCDAX7UqFWcOwddDyqkEvPKMvqDVfIoI66Pjuf3cAu8Dq1Dwft2p738iixlDkIAL
8sr1mHxMz+my9MglFNKe73Ta6HPWBolzN0vhycvvjrCeBswDf7qONDRksaqGPOtzWVg3G1zARC+D
f7P5KiwjAkpCOOFebwHZU0Ob08Aux+W/T5FlfGnFGWD7fe2N4vuQTjXuqsKw6mVp6doag/rVdGvu
l7v+kWIwW5GBAWQ3R4GQ3h3cfi363O6t1bra+5Wn+dIlfxC81Bx2COtE9YWXGoQxGiMsnsPRrVjW
cKbmEMVscess3dbflgZEpGQaWd5N2+GTl990LsY5krXfUrW4JojECak8dXivIhrYwhDYWUQuGmq2
gUDbLNbM39AO9g7vWZTKnytfF34AU4h13gURzIUzzdStUA5a9JolAKSFB1ICm8/tSVbeSiPFJyXR
V5taZJDGLFoehOqnCvhTqiC0VXof1R739jkIkN/P93S/Xt6+noae6yC85DKoCAtbVyM2WbF2LUED
slfpo8me2bsD9Yu0xiEALI1xW0bMmvK0DLagifFmVoAzTZ0qMhTjV6bCGFQ2hDRYyezIHPzWfbjL
O7qigwy9A9qMJjHtHwWiU4GA3DgfbgYp+TQBDXvzoiD51ogSR+reULX9+KDo69e25IxH7x79hlXR
h8izuaYm8GtZCPtIBG1EQVw2M5YN6KiaVHdyCXkPSIy+9kO8AUho6xFsVIsjNWMzkbwgLcowmhBK
tWan0EyyL9qeiiOMuodLJEYyfurcwDcXyPffa/M/dINirT0pvQyVKZT3jHvDwk4Jv6ZNdU3tyJae
oQDzQDHCY6M9hqUu6XUw6eCpBMpVmPv3zbWpkayDSp1yd/LJTPjkpJD0DXSHEiZeE6GgqTjNHTTB
AWVIrhZh/JrRgMXnXnwNK/wckkLaqWQFJYwimbl4T2GT7TGX4ymAZh7fjLbpmot2/7mskJYvPsqZ
/XVnBAFLKLa+CxcrDhhhDNpLCoV9JKAVftoImlg18iInaceMasyA7di+78Xc74PjP8TW21UmdbHn
Ogdg8a7NTipOu3P8ri8Izd+4EKAszNYlUGrxWgFEgKFnc9GJMXRKW0JMUNeoJxUNohchMU9AOmg+
S8HEQMuMusgPwiAzc0zY5arRT+oYXmdIbBSplbaMkFRRlesvXpY+B2UBKFKkma38AAzdv62ZTPRP
N6Th/Tg1KsxwzUmq/XXzoYXaO2VTyoKpBGP4Dz7J6NyhcJwkyXpkFlBegsMmj4BqgEo0H+JbuPax
rMpvkkvIihfryhsA0xUHfi8qyW6KS4BPtlTajV52K3J5pO8Nekena18HZalOeMTWPJriYai0F+Na
veRrIPXbS++PNVHzVNjyjDgd2xz6oKXMNK/61owJR5e7UkBSglyKeHazRutNmu/skY/CNR+/iPJc
CtlWsplJiqHO4UUoUyURssCCvBmSPjctBt5eDN3DPQKTUZzy57ZvU3BVe8U4a5v1HJHDyrXW/twW
3VfX/crAPfcYsE/tCoPtUZXCWvxi3fDdBaxm8ox9+XBW6L7p3/7XBz8rkySRdpQevGa1u0hfnR6f
LffEn4Z1DylpP0mCgMvgUHOc13BiHFLRuYuZgDvcYa23KOsr85ShIvCWe5rNdYN5rqWOrW/gTH67
QSaWJYQ0owCDF16ID89642IUgTts6zR5+8M4gzu82i5+beMf/KNwe3YUoLAfROThMBcebRdQ6gwM
DcDTobZna3cWk251iSV5QKs4QvhcpOBvZcTBuV8wvM94LoyQuEPPaj1TD5HJqNCIY8kfUk43OmTA
tgZDrYprwdmhwdygMBewwscKCEZzzHxkU9SN2ZFM3iVpfdtX9HogzFOnWFuNQtU+UNKmvCwaoVlR
UkVjE1blCeidVj8Zc40HGRnyQwgYgygMYhkHYgjo2Lnzh1ZLlmuhcUS3VoHJsEzJElOBzdL0ibQz
eCwUTECPUlziS314Ub1qFkrIPKVpO6Euja4AZRwbgigVgVbEaYdCvuSE17QL+l7ClLfvYQ16ZKMM
RaNfG00GxtwkZbDC70aBpNq4q/CK7sh643BngESO8zqAg6i/ldb++b4e6e4qnpn2XZzHBsouW6g7
Ia3P+zyjBlg+VtdIv7okRSm2M7x3+jO6z6TZjQ7MBlSGmG+nO521pC5Fu+x682iJ+WZtrfQhEBoI
iwSGUZjD2XIPsfRb0ekEjZ5JQuPhoNoxzigE4acpjGGlxywmNxanh0FuGcIvGGpm2dDVbl2qsZ1i
n706ukH68XQvqBORvw0MxrixscsMIThdDGZfVNFNXrRg4IGKF0akAzKkENL9hPGFUkXrCKD6giK/
9Tfsn9fc54KlkT8efm5c32V8dvCd3cEeNM0ReehnLIWm6Q3NpS4sdS6g2PAzS6DN03G0I8NCAIbJ
sC/GcooIGd1ZXU7yjqHeDA9fZKdVy+DkRmfLpR2w8PSNH3Ye0x4ppw4zfwASeA+SFFQqy+nczWjd
Ov3qG0UCApRQ53IydO0ONmPOlQpM8jVGUty4n7OTshWQtzV3bl29jvt23pR/EgiQ7o/2f366q9RO
xXsVEZQj8KgmCPO1xTeXTZ2KUDAKAvj4Iuj+yL9QqgyopHAmd4MP0FXNrM+KiByP2Ph/1wKJwjkc
ZFW6zacPDTxe+yyq62yTYS0q81rq2qrYK+C8h5vDaVNUid6eoLh5eZ863eUDt8yNnXu66+f8dBZo
9vQk1jfKoQsZTMhMl7fEm6PqMo2bJ9eCZdJEpALUtgnIXMUqjbM/NywP1L3kWWw+GNzLZ7aC/s0i
pV7FwLJCJlu1vD72ZnJzsdCzCFSscFR9vg0cjws12iidxeKCWnBeytsVk2mCZwCdTVmPYT02K9Zy
r7dRihcxMaMiF1C6OVSD1FWzJcHFHk2ZJIjHOpYpQg1bnMrHhFmwGDgufKggVusIL7A98nvj9lWF
XnuGbiXpRY81Q1ZitthiG04Z/mVDzaBV/3zVBv6UOcW/vEX84Nq6R9z6yplT/mpcLnQ/Bpsp+Gv6
zetG1i2y2soPyxwp19wKTaBkwXlRkg6GnMhEFXVnLK5ir+YRGkzBYx+DvP43dO0iWImSopEeGfSE
P/UZJBxTOOHqLeQq6T6RWK2EuapBIgSmCrazeEinFeKz2irAb23eSEW0bBZ6YaAIGG2dzbQ2oJds
BoKPy4WHfqyzCIu1HnninhRYCeA77Ys7Jd5lKekoL+XATVgji2zVOo72SkChfQeDlKCdnbDq+BRE
2NY25oTixiKTSS0hZXN/ecm6kPnuRDNrWSPcouoZLf7UniowNB0g/ghkQYL8KZqY/WM3Sdk1nvrG
beVV6TeENeHzH14gEAMTguHQsfFYY2VTjYQEYkSDMzQdnEFtRRLR2jhq0TqvPO9gmMzTNUR+gngK
D2Xj3e+T0Zxs9QbMHPbkGM4vUtEcMAjtx5s53M2cNoc//qg31JZHwituDsRG5YRBSCpy6lshlTIc
2/FwwRHqEi8vDvV+C6l5Ma6DKNnlgnStgBJUKUbFVhDvaxUIB3pbvfE4vKzoJ85FqvRGZbskQ42H
oYfj1Z9LSYo0Fy7ByocIxMk6sNksS93bIn0z0Z6PzDD7r8lo5kRWEoosUdC+nL94IpQdfPVb+g3T
6JrxuxpFJi9uTC54XY+3VKC8z4FXZ+n5zHe52sXdFtDP56/jkciCtszvix6Bj9mmhPVkyXbWteRU
SVuEMJH91FykyLrKUleMqv3gqhTOhWeHpe3d/eZb57o2byiBOSDOqlnFtekvkaWsGweMPLRrzFSc
fN3gI2ELjBTGefvPUVdnf8EixOQ+xOWXUUY1ueUxiQ0GwaE4JrrsagUKyRrIblZhT02JzOmwkuKL
emFKGmLLBC5ObkpgJFeAFvHU7vcGv7D2UQgORY84l7ioLHUPc45UP18fbLKBSmtkpkAwtvO3bLCN
SgzE7ELxkvDfUWirc3XNAGNGlwmplqWj1zScjig6FeBDxeFnmLtlLSw3i6dlEdNCUtF5+1TWH7hp
mekXSnpVGuzsFChDPkcWqm3YwFvUeyRllFFAc7zgRMzTLhzQGIGzQjSU/gshGWrEKUmktM58N9bV
+y7dsEdBA0t7JND4UvPC65vQJYXy9B7HmvO7/PTrdhH5FDYJjtn7oOV0tc1wN+SCYs9eafNEegRs
ZDXmsbxpCXMJBP9gYbRDv00FoYI/zwEjsu171Og2eUKmg6qG64j8yh5xbCPlxWgLukUMeP+UIg8F
yvw/aXm8C5F0qbHvr8Lb9oZtu96LNsksg4DJdFnbldTXoD0X0srDZaiZ7tqKHSxGHIQe9RdKyknZ
bETE9uvU85LS5xE2WM+pDtxbqG85Vi/ZdDKgfE5WDNiuf+LOHFI/TbEp4BBQ/YVmfy2JQiG5dZw9
qS7QiEE/424fmCyrz+ZG5mNcQn+GNX7MhGdiwUJ39qTqSHB0fu5J0MPSKrWqfvCjaXU7arffWT2K
Zk27HgW5C629B29h6ijmhVY77CkZCraWGw/6vMcW5gbh6shNfZ01CvlImmBwhEn5QDb7Ry4QLQUU
0NTJfRmZZkUNO8ik26L/BWmOwVxBKKmt9XuG28iA9ainkZj8dl/YC05+A7z/yu7IjmLNWCdpNGQN
FdS0Q/ectTECrE3OSJWuOT3WFRK05/5PGZIfSsfoj+aVSoc/xFZI6rRkJZ5Ou8nCqADY6Vzqe6+5
w8CJTxALZXcX1R67860LTw1pe3EWyH77IndMWKD/6MTh4+lTTZrIYvW3JknA8EyWq1ubmhcXCe2y
UAReUqP39yPvsu7EqJZ77ad9ITbPln4bkNMh5PE2mL7K+1lePGI5drGNAm9jj4jdMF0S7vENArSb
SlTetTRBhnNQlZjQ41bk6+QD2wFezCJhRyh9951ySXzD0e6m70iq8VYyqcetEF6+0i1ypNPi+cwx
j596LX71yhGjsUQJEyfD6Djji2O25V1AHL88dXL8UUVX6hlgUoCt9L+HOlY6nIlCTxVzY03fHZnc
xOguiG+hJSKZNllcBZgOCSXc9Zqkuiwyna0n0/FkN1hHHsuCHeOBcfwFiDC87te16LxJQtCr/nhh
bd3enGGERtkYbzdn0Eu6jXQRFpKgxOP16Pm21z6/y+W5hRD8QtsmTIkkjFa0GUYB53TU05+DgDaT
SexFJkOAvFGxEhBlQLagzSDQM8qJQd79pzRHfPqXMJc7LWtdBJii/E1OBiLoRFZnbWjhv5Et4C4i
WQ9/xK6TmSboK/O+S/cBVfZ7ABT+u4lQUIOpFTzLBGSX7a4TOH+LYL8mNBZ3V/+yXTPLPhYbKbTb
wOh1Ga+l5Wyrkjo9gYO5JC78XYqQN46VYLBVmdrmo1/G02DZJ/Rt/5pbQpFuvETNMYpE63qB93ta
dtqdsQk/ymqaaMwwCka7z2JQGm2V8F5OMWLlPfmUyqJHPGSYg5vYWblUrIiVZ2BKoT4ABnp2u2RP
Sj91pq2l3Dkh0NhASugti2eO2LIPny+FmFyE8U1UAKJK8qmIIZJ0Rr/7lrdDU+sHOJxS36DYt98N
9LN6kZ9sAlAT3yMwOEzIJf8SIdFhly29ClEvmwzBP2eQo+81GRYKTTjSW+ffitySfcIWMlBVbmxZ
j62JGf9l/8RmNbulJw1tJJtvqLPA86c2TiibFUMD4knvMQnzKni3NgPPfEEuHMzNPmonkcoGNRww
C+kgrxO/ixBMPREQ2gKLRaHrfU1+gMnyUkm7ffFTJxgntYyHoWTkCAqfzVG7NY0hstGWTxuthACQ
HAvgzZCw0f1JsDAbo22BgsFVmZT0ZbvV5WBGV3kEPYxdPbtI/T99oPdvWO5LUcOhfvG7Vyr1eZJr
mKSkL+cuMtxJwUvyZNhg0rNX9x410iJ509cLR5MZaia9zOQ7gt7vBFe4R/yMjWLU6lQJaoWEjFyu
ijeEtH5SpQZ9jZUrGxXaiRFqGc9GzIlCf5vXorBCgh/TI3glKsIrwIGSnS2ocRyoXGWFAAojsddD
Mqu073HxvCb90CTknvspw+SpXLobdGCTI36rrYzf5E0SzQv1F/9aBVWBF5niIaRtTDGnflmNYVwH
K1FiS2QiCXb8KqUa1P8/a59IWsyqv9/qnwwexrOX355rYSVXZMQQhAK0Pq9jjNR4mKgRZSbf2TW5
66lr8MbkK7Ca6jiscP8majZBIXsP0b7BaGZjj6fIIGR77tOulip0XO1xkaYqrRgxKAFd0K5DuO4i
ZCM7DlEevcUYDTjiIzQ7xq+NAC4j06ajLmVljvpa4yARh/FofWBhx2u1oFoSydx2w++aaCb1Tf5Z
x5o4Qsvo+07IM/fg+7Og4ItRG25hs/ZAtTMobk2r15fZeuCU+6NR7R9Rp4u8+kPZBA5LP4u+QBHr
hXPvym13zoNdMrLkNyIT9R27r1dX9UTuPxJcbNNHpQBbkoHov1HvegRv0NdkHfiWaiz9UIX4gkAd
D8BGgOCw2XFSMde/LEGN7xeOfQtby+6B/sKrBFYYejJDB/58knaocTvK996tmUthyRNxC55YhK8O
UpzQyxAN/pa21a/GNfmR7f4rKuddGK6PefQTWb66JpOAb84mbQk6UpADLvbSky/Noi6zIb79TVmt
PpOdB/ZhES4Ktml0YcluFHCK4LQTZMnMp+5VKQQCGjervvl+3/ySKke8aLTasAtzUCvwiPu0bcdy
w9Lam7yAgZJrgkCrrZq9+1d/U6ZV1MGEookPHBVQmsd9qCN1tbwmOhGfMsLKoacreDMlrgbbmatX
68xE85i9boUCiPUNd/KGKZTYfR9io2SSGzVo8VlR4mPuTubSkEsaQr67Nq+6V8bDe/F/jR7QNqeq
pD0dIKb7pLwJ7If24ZTLqYQ60iAuSswZNGJj5aD32NJlkshvhyFKkDEBryY1x1aOgRd8lTsu1dYx
RypyDVHzkBUhJJP6s99I31ZS6E3VV31o5fHXAA+E2x1iIyrklwyOa5UGByoqH65nqWKeo9AFS2rr
nUP+RD6BxuyG/SoFMZWeJWPHcvCwy+y4uGKCvBpURXzwd5L7Aub6ZWebkhxHrTr4iHneKboSVjt2
bMEliAx07tYJ4HOrOQZEF7FgURj2VBkDvOfRq0IxVJloChQqLdajNBcXBS7dHMPm22PwKQ/1Ft8m
kdjmOmzYD9Ox5YyvtGuWGb6gTVCTfzGbmXszOMW4qtcWVc0xbW2KQUB9UZ607LYsymB3CiPiS756
1RAi22LF3iWOIcPE7UN3TjpPm6ObxvyySy7XVeoJmTolVq40qKnT5uPwSv/B1pMJexlK58PXxXHc
8w+mFWcz3BUv1Woaa7wo3DdpCyyHL8BCKjIihlD/tQ77IYaaHs6tFRalspjYqHsfJKyZNqIFCsiP
VcpIYprjUcthMlYzvOrygYHDuYYTdyagl5TSVBcjaw6+0GavYwnbVe/WKKgc2uXE8CIhAcB3UybV
DdmNvNJgo3pUMp1LT0Wwpl3fj5aYEjgmWILBrrWypkQibkLYoqiHDFOLP4NElkrO6VXB2H59w6tz
vwLlF+9coXmSEoGE1QH4vAUavABNiKMriNS5s0Q72GMKD/VxhSRph1w/6SoAw1sNGZmQRe+/ogKZ
x1QqzPJhwYZJN4gkY7qPr2/Vi3JQPo3jCiiBUYe9lEWwX6lH91weivnFS/mV0i8E+hDMGQShMwxb
m2Fuk5pQ8HQJ8R9i6I+YHooQuDUh6B+wbH/0WRnlspL9WdqeyfhxVVjlbtNwJxMhrWmJwBOuuU0m
CcEyXZ2VYQInup3Jj7eK1uUbl8edvW6ipyHhSOidEzoRMZfOqIgrwcoWnmPRLmLntaH+XYoCsmPi
qWDhr40mZQG7dwcyGozPCWAw+HuTV4l9L93UU8SKm2ss3BIkJn0w1840UirhFrkxHUr3bBxDVf/c
UOF4+gV0UQ22g6fHvQjfNT6NpFTBurIlQj3sRkpWdjRoDNpZY14EahJxQgFxnX2uib5qoFW8m4G1
dXqhMphF5A5Lv7jWh/eJOct2H8++zkSzwzfA3WtSChJ7TTCgPYOUdhIFofn7MKDinlcTOWB1l7x+
UybJCYcTxl6ND3LQVHG6fl7l9jGS0S9R4hm782ofSLLleCv5Er9XCsDO3eIPUrPc26nlgrqW4vVN
UuIXjXwZhY47sWbMAB4BqQFk4+4WUvx8d285EDJMKpDtQuvIGlW5OuT8etifUIKHeze0vtQPheN6
FjsQhRqfkWSXsBSt6cL6NQXrauHrNBHGDr89/ze0US0wHdpqWUXqDb0nv4vdujKxa6koK7usqVyt
8tpDSDhCEWvuC/HOqR66QhzkluRrQrF57viHv4aK6RD+PRGKtw5+n4DrC8qVGa0sEdwZbHGpodta
vuUyXzbrOqBsFIC5NZiIWXtcNNSJhGrGcLVy9vbeXToeVaZ5lSPoZGWfcI8IcrIBKc8Eu7elWI+v
wDctWwBdqSCKUdCcbuU8rjeM6MAW2LbYwa55sDvv5QWBBRofQ9DhnjbrQ7BzE3ObAex70lBOZ8E1
ISE34r922fcho3Ahb6qBr87dFcuLGMIfNLpCuLmjQUXAOzoCsZ8AEKCOVmUo2g5Z1+jF097zf1Wx
LKqZ9pvNcYdvlUE/y9/EYnFVpnaI7JKJyrydLhwVMUQQWqJ8iDcPfLt4Z08iAKcMZTHofnht3ehI
lYMV1aFJEISRSBRlqhjxff/jCGYiAurVG/QlsV02RLHLz2YO0SrGmi0bNPLbCovoMS4SPjbGpPrc
WjNzegr/D5dDYX0h77zVGN1b5HffpHPezN4xNPtzSXUHba/8sBeSQG8tApzD3vmEnyxuf5IRrHsm
CpSaWhZZEpRZKk5F6ZOmXmnsMZOMM8bhnQ8G/SLSLAxUkIEQX5WIUeSV3b9n4FiKyNM8uqFpjwIH
/5B+fUjDNSkPHz7nED5zlGIRs6Be1Jn67vPkcsHMcTzlUp7VYTvYmKk48vcIaW2Lxom9vXy4Qq6l
M+Wgru0Cw6X+uVnfSAitTOF+c1fSyLDwZ1/JCnJ7LA5Jo8K3lsK8gRz5y7SGXk5oAwbJak0suu4X
JtMA+wgH6HAnLOVYsz2Dy9pgyk0+XJWboD1XpOG43/y2fQXWRNJUqKoPzXGhVf5jpEcs9469zvXX
rJQtbw/XMBtcbgBcL7FzGy7BQ4z1Qh3YR2VcWeCa5Lv+uu765VNvl9px0bBCp+yQcmdWnwnesM9e
QV+BCMeCMBYEaZ/ouwE49LBA9h8qcbg+oL7Xse7okw7kPGunumK1003AwCScCY7Z9zqfm+TUD8h9
sAp/9Jq3K5sCfCG/4YAXKUSzVSqe/5vhGCAgZn4L/lxi5DhliOmXtd2E96v+CszPdLIYJiKXciDc
pg79rtNj3UIWyIgRlNIaqk9FTlGIUgLU8r4CZSeNVhNdRRz2Eo2KaH5ErK9Zad4Rt/r1j1jFEk/9
xIY1EV4NrEIKzkAXarVzg+IKZ/QuFRf965+8PTtjXJTCs86D30TxDTnj9i0vJWYX7hsNviSIoRjV
BKx8ohFAFmXTYe0HhkO3xLjgQvE9Txh3AaV2nRH9jaBlnuly+btMcR8t4nVKatd9sS5fN1nc3Lb/
YsLln4CiDYVsa6phrSR/EFhiqTV4MjQYvMjzSY97q9Q9bWeEqNCI7MUSXbWiN4Aux2/AjYt3xTi+
LvaXQWHt4Dd+YgDnOFCRJ+6SPuD72V602QDpbmRUGEl1DBMJ6KZkFR6iQ4qTgw9pVVj1fU5z3JIO
V4db9WB2QYJdRmhPzR3m+IRsZwrDfnvc/uNaL+//aEMNIv/pRCffrGoEAYKL2aGOpgUIYGoPUBSM
AjvEvWnkzQbCy3nkCce+vmTZ5q7kg98QY9rV3RJ7PpnTiIX5+oSNBJv8tXzxRTEtOkkX+pnf/TTr
vLubDudQ1Q7jy6qdk2tKXj572rxo6ps7xEMb1JtCQADLL9XiTXCWfrtSQtk98E5+BJqAtNoyEUV9
tlsaZc0wHg5Y7x9RzgbyXBP+o4+cs6jIDv41mWdyTtfrv7WUkjio54gFE7UELMLzf9WCuD0ZQdtr
Yn6h9o10GMvDeHNSPwJNhQTdIjQJM4jb2TrPXo24P4F79rzacoQnWAcbfnkxeuxo5fspDZTP7Ktg
OuMb1+JZE5Am19v9rjquTMxzyB8OV5P3UXZIcpydL8SOZIj6FKHIR4aHclLshLFAHxmaCV0M6gxO
3ezhZYwm56FdstF+LTMs/xv4cR/huMzOW+zQV/I8BthmLv/TQjJ9NrQBcsutMecRDU9RtyHl6Aul
g1wSNyCiRALdaH1RQ37umF9wfxxk3QlO9m6s5kiww+g6vP67CewWNL6ccsHPCYTgy7F7Zb4Ln762
oQdnBYGwmWHclD/pez6zwxJ9EzjOBOc3Vger9syoj+P+bYbQgqrA/LRN3BYFmCd2ENBhHtf++6h0
M5qsIqxUESytVJAK1MwbbiuagyPPAryMLvNE6x35ioImXrOhZcV53mKzWRxBG+IWxbLN+/s/eiCn
9bft90KdrYoM5OaZUzmMoK1Mez6piPvJnNpuxGwB8RqFaUWzT/lovZuKuJ/K7nplbbdQowNEU/Py
6C2i3QMDl/3B1COUiMNcz++zs5kQyeyQNoV9ZxRwVV80LrX51I1Yh8NLsCm4vRzs7l24E7l+ZQ2q
LXbeX+NRtlvtegw6z1pxR6E9/ZmtV7YuGx6qx/P9KMudO2B1NUqQhP9pwcjSSgPVAnqfDP0dQSoK
Xx7sqAFM0QZRhHJtA12eXXa7wSBImeKpbCqe+OCD/3Mzp7PYLzQv15Hvf+Y3BdGb6qmyV8e9jKWf
7iz5/4XK6guYy75jt0cGds9sA4R2ILyYmZWsax+g1MyrAIdDS/0YdniXZFloX5/uqbvMSJxRYbkY
2haSfht4vqasoQ/c7j1Y7zPwWX4DHnz37q5D9UFE4RUsExQABC7jxESSOxt4mOUZ+LheZuL/O35O
z6S4aWemv93ThqRsmhfjz9cTCgp1UhOvDr+QUbHp8TMu6/0xTaivnC+18e6Zn1kdLfSDsuXlCvwM
KNenKeRhCOpAyRGdP8c+tL9bafwjyoJUsYXcpHzlaQWbBdQeunRpOvMdR2fdMFoPn6tILhjPufIC
8b3k1sBe54y0g/3yLOCEIfRnbEPexNO4RuTHZTfEGa7c0uwOJriUiESxFKpxo0fZhbRGmuKsXmRb
Y48fFC10elaIb7F8MHEg0XNqJn977R7byMJpyTR3rbUSU7Qvmmn7UgAREK/LeVf3iNfJtqC3wn1x
RYVKFWBD2jTjcHjJHBc0VBbMjtsSNQd74Qq091N/K/oL3iaHqX6kWtfEGZuVOHdzFC/edfYfFS0t
ZzkEWtixAkZ/F7hujOzsyQoDWOnQGsQ91GLlOtKyUV93evmfQfn61i+GOlcQCD6nYpKxtjzz6YXq
u5kL69E1izR+9SDXcAs8ph1/83ft4ROZIsOYd+DcSVoWr+P6OljXOp1kTlJmfVadqx/xjeNs/G43
jMRd7p0JL4ypIztPC36QZE6GkLSAQSHYjQjCM/V+G+3aX6HbsrwzxV2GbpdrKuM5O1bOWBHA/e6Q
urNAK3DzulN8waq+XOFaosXYGT9kk6UHThoW3jZzVkvhqAuI4/DzyVR04NBkYSkurS5JLs6GMTwy
yNYNmJ2r8Lur73jPilPVbY7kCPFTV9LBxtE/FBnheo9IWHFxzMaf96sUr8AsIsLiKYDMEbDkX2oO
K2C+t8vyg2uQuvfdKI8FHUNvhu1P0xbmT+j700FlzYwL4MfilXGUsxBEymivjUV3eYTDjkYsHevg
tl/K1j/u0QzpgTZTKGZpfs6rlse5cboGngonpz9B8T4ks4pg13u94NT511BZxp6wAdIJkTgbFebb
AGXmIijXQ7w98skEMrDsZXN0ou1Cv79EaXMj3QqQiP1oQ6OA3lqqUIXymNxU8iLWT07YHxJlKuI/
ppzLWTNRz2Nf/Ej95jJ5I970xTFCz0nepllzZZDtU4oWhaQRdYC44t3TgFW+x8GrGi/b/n7dPYQs
mUPxQdt03zud+qZxs4GVaToSjVqJCEX/hPuHH4Mum3rYqR872OJERm18lcH5bMzkykG7yIE692By
EmOJhFLaYzkpxnLvdLDXP4ilA1OuodMZOOB1wIzWsCeJjJjjqJvua/PstZnHPSXH+2LpfraD+/cW
YowrTfl46JXg18BYHutfAz3RIeMxFOlzPPFLmc7QQbqf7qAnBCHEI0cfBmquyhIJLPo3AKmgCA4Z
vvPtjnQY0eItQkvrsqVrLMd4Z43OPytRJtvzneorMPkewoLpEpJ9Sr5RVtskMgA38MImnvkBbmfI
XgETOHWp2akZ0nQXFelYDdU4r/8mJZ9gjRR/RpPGV0B7LZHfZD5CBAAwDyAcfbLxlNnQvaL82hRJ
RZKUD6ZKXACgU8AdcbGBPlN5TaL6hSLv+mDH+dtbKSZWwYb3/vauu6LTuvRwyI0eY7n9GBasQVJS
qgB8OYtCXruGciNzBWNn8aCssRSoHW2m4c2MjqTew3xKUyN9PB56jMOLghO0H+wQVelr0KQ0xu6t
BZCmfz2CVPL2vVI2kJEduSIgV4jIsMWT4A5dP02p3GsHu4YEkYYb2QVNux2CuikTomxNR8O5Tt6T
EMQvvMWClTElHjgfeo1K4nTkPr/fCPqaJZV3oYg7BGitrtkhKsJIG+iALTlE6SOTVyZkirigqQG4
sInTYPK97jfyor4bmYd9zZXOn0GUOIRBn2P1qOXJqJDVCqYRJQZp5U8NpM3KWJ1gMcrvM7uu3IzU
oczWFMfXuwsCrFOwibPXfzlVUmMoHXFyLzS01XruSU3oH//C0yKRnEFuXCR9U+lCYLeqIViGE3dM
0PWKUgiDVRvSnYHBHA66t1oZE1EJxfnO8fC/V45ScXAXpxf6QHu1cgRYKubUyb9R+wsyjtMRfXKs
9JG+YxHqXPvHuzSt9r4dm4pTJ7diQJxwEHod44XyhkEbiHDMdKGHtLClkoFHR89EDGC7O1dVfX6C
DFLHL3Nf2v1nfYNJZ5Yyk5hr1uxiUSdUHL/PK3Unvhoa71jI5nwFugVb3qw9knk/cKgxX0V7wKRj
84dEDs4ckZaNwaOHcweDG7M/pFjsvM3xb5RnCRzs8+BL1u2Y0GDwT/LA0UNnnq5YxMLJN85HXqgC
RlZfjiDY0fYLr6KdCJ5IDcxTz+NLkIz2/jpckdGaY2rWgTIjsXEO49sBH9AFa6NxGENk+ju2H7hr
RJfERN1/jRVvBrpkUt/+xIK92uEKZ59DhzMm+1yi/akLy2PkC0BxrZYYbvEpjE784x1GQikTkUUF
TmoiSYM0szpjRsfWN1o4rSthDQ6quyob9JmFcMLZLuds9XtxYpT1m1RgnemJyu0ZUgRUzifSJ0Oa
L9lsX+Z4RUsyv6Rupuf1Z2zV4bq9572gzjW2cY4VnNUioc15EhkQo8TyCvEBVx88kww75/0FdkjT
mNH+/+SB8CuSYkIC1a8l6oHbzAAMoqHdJTNptmFeRPD9PoRdAajeIhmJhz/mZqgtxycxqeJPJ0ti
khECSK3hc3L0l4EhebQWF2Zp45yjW86XqnYUlZO5xxTF1CCnjMQ5BHEmsCxP09SexD9VQnWtVPnq
EKSmbXTpy/38+4lAc1q6XcT4exTxDVsjX54R8YkgJXLQ9pFBHH3KkTPLB2Nv6o42QYGkL1ZaGXL2
wmV5wCzmCLcqwIaw7HZJ9VbvZBgyR/6WSZwD3I4/45MpUhSCF5mB+VeEE3k3BByaNWjxRwh2r5E2
oDmpHSjUPZhhqaWq5qRG71Oou+Pg6eGnliW/0q17nKp7IbgbV0UPA/JhcjKNl0HEf4GCE4WFxLFQ
iLVyB0Jp5KXW4eZPklpVVOx3gvUG+9X+9HYVh3/X8TpNMzlLD5/lBY8p3XDmM8JjjAO9gRqdfuKV
3X+1OrYwyCTvc08B1bSmMLxNTajxzdcj4t+iHI/no8o/FGPLtb973Ga2et6i+XkD0QcPwCRVECdn
eQK2i9VGFYNhPAS3yGY9HHduc6hu8Q9FyCbp2ynk1gJc6wQ4At6tgBFt35hsykRXfLLaIbTVkgXu
xa2qxB3EIkUqPxoB3a5Qvsm4taudoTeujtuPBI2Wxuf5rMEP0QXDvvxOKQoqf1NmYcd0xOKc5aKl
5AC843/PB15zVjmKpbh6+Mgw5xxkvfuGTl2Od/0jvHxbV4cd8TR0tx7UZ4sggDmDirof/y5Frtp8
FbQOvgWVEgDa8d5lshSwoLJ3iCy1KaRGh68XXp8ynZeJmjzPO2sqO2m/Vg15xc4Ln26pkoVPtFWV
4UOJ4lOIVaNcSoFzOFjX35hwv+IR/suKCYbeVkhiOBUJ0fEUkmlYszBMjeHIEtuyiMEaWMOT10Dk
MHgeml0kfCX3LeTZO7bsY33vVvva+sivgfh7XOTCOzL4OXt60jod5A0a/sZ6+Sf8MSHnwIabg9Lt
m0HP6p7BNVizjmpqBZAj8rfLyvBEyr9Hm72dv/9iS2wmFboJLg2tQr0mpU5FFMD2CBfmO2rTcicn
kSNVzv23EHumhIBFuTTRMchTVtZJ5C7B/w+W7ffZR92voMaQjCs16Dm2hKHIEDHAOBHjI2/7biq8
uqCAQNmTwfO7ZEZQcxy2zniFbrjAV/rfLo/ox4Hp+QCOrBZz6Z1djs6n9I9hBySC4bcxWjFbZxJx
iXZ9iqutrKIBMBlvvZHNTFtR8Ho/3/9feRgHkn80uaRaW5kNgKwsz2JOCfOGLjwub+zr40iqoVeI
b/RWYsi7juJFr6igWL5t9huPaxQicAkEe4hvwaBl2yKXzXApr8jTG/dO7/+mOOKL1gVcalL0FbjX
CwG7rLW04Z9JF3PDWO7oTqs13sm2if5VGVD5SdBrSjO5awWRU/oU/NwJIpofXOwgkiohBLicP/mb
3CPHGmY151jSbzorHBiS5rzazaUh20jiMd68764xktK45fKWfw/gvHsdiGZq5tEhrCQk40gFibs3
8pweg7t43f55RW9JjlfNbP1ZeS36DAQSrDDJPAuyFUsl0pydkhMGpjyu1ZKgKw/vwOZ0IfTfGqY7
KOTdyhVi3aDS61YTjkXLie4CVzy/h1g4uJ5UKPEgFVO3xVD8+0fbk4EvuxPQ86gMgnfW9oT/lnw9
eq+N3W5NDHrFQoKtrZPNDLnrio3KIkTLrEBzEiZbXk7t2/KGb8lQ5XJdi+Kxf9xG6pts9CE0EKC2
AMEya7vXuMaBJJvsgZ6CLGI7fdo5YNwEYNtrFFbHsQheBlsNOwH1EazoUPcnNt94N7hOS7P9iFi2
PzkFJGlpoAuW1puWX/eNIdRbSS8owk286wXfdLSMe1SSoc8bXNg+gOBAhb/9ITXQLBxEXFZxxQ1c
+XB6LS9GnIhASdilkFbLAg6x7fO+qxzr32AOYnSPvnKgDOTKE9P33vwLga6AB9smYLm10t0gX5Cl
9RcXXhM3YWF+Fxsergibclp4jCBEI3gMKvBgRKG/ovvydzYTfyCNrTOmynlbSPfe5UTigtWWJnVE
5/7XsCEayHOpr+YpmGoAqUWKE4Vn/SGLFwMVR9l2S+q7lbbCtDDzPLFkW3fKAksAbEIcoX5ft8PG
uLAY34ylK4xzWtnmOMWYMT3vBEKI/v0D50R7LRaM5nS9YTwCwFve7RJvnzR3BxzaFtRVDAJDd5j1
z0U+XbqFyAjPeJz2TV9AdLAxZH0/p5bQJ2yqqZiCnNHqprUuJR/4t/XVnGnw1plbm8ZlCW0uwHIr
zPuNoyfd2wyBgBQ2sOejqQwMuCQP6QQuwkVAZkCiLitO8GTlLkIS/QJJ0FhsT3l21GA25bBcgMFf
QGRahoqW77y/HfCJSMrON5R7Vs2ZUkpHfzGelxKoC4SkEfWF85FkjcZqu7pAuPLbOVtDv9aH3nzE
xQywRtDtntV0wLi3RXEe+PB2F0oiKin5BLClXgciUbbtWtPpfhllfVdNTHdZMpmlOlKlnRa7Yy7t
aDP4L01OYT0VQCLLmb9s6Pf3WvULAz1kCMcI7mb+5P7fCn77MeLq4Zd8OQlqJlQStxaUlnk2uIoJ
cPkMUSkmzPBaU6Gpwga4yTy585a/GEW1Aalp1SH2ntyADAY67a8MglbYNP6zjslliFbwTtVmcSx3
IHm7hcojicsu5eYqAm5s1pSYuCghqJMtaOGArmULYKaNLj5Dn4eoMr9qe3dhYR08guaC8l3vYAZk
Z8nSIpV4p3sbFukYselghWBnuEmzjqKgXQ9RJ5Vt26PebVxJfZDENkoCMf6fmXfejWNZjo6g9ss/
G5+GDxIWmBthyl20tri3j5vD5VkPlv8rex6oXMNJCn81B/ZFlOZEv9cCxMSE1KSTpwJvMPOctSrC
iGqI1mtSHobkhX//6AoJdIzGGisFeYPm4n0r/3ZfEabb/yChzlh2nNgPSjMpMmkWwjBoeMcJvCQ3
PJCrsg8ldr6zmZSg0HLbCqEo1UXNaYCpjAc4EGReAER42f8rDSnqxIxVtg9esGCSD1Uq/GXQbsUB
cOb3e6pfox6PLMhwr6ZHkUfXSk6dVAUSZPVfnOvXqYtOVWE3bvADqcw4fQ5TOrZfKfikwM057zWR
6R98TZYS+9pXYlbzRwQA+6HNVL2H/ds1J27Fyl4ZA8hR2GP0EhqVx/qNP8GwYO/jzyqxlOVRyxi/
rIhIOwLt1rSwUri0cmFGeGXX00UH74UamcnfLq5nuFDt4VkN3P4WD+oEp3L8zwJI/CbUFgERXx+P
HEb+a2mmnq0Zg9jTqQ4NPIyPCQvh3tX7+J+9lEU12n84JDoq+qUNDSi8VUp2p9Z66ApBlm76tzCU
DcmkRgbdO5criE2SaBjmtScNYLM7VvISOwHqcSaUS3UiqBJG0Ytn42n0GNaGJboBgemYE7IfuuVi
Hh2v56gma9d2ttBrO5OWhHREyNi7bEEqrl1fnBvH39CuISn+7xICMWnJAv+2gpEV1dxye1feWeJi
4dFT9WhlJsabJZzxsKRfk32QhkQ7vMybItJEpqBnLaX66zIrvdKnJzrNJufhjs1OFBcgb0rpDDpY
ZD943qqiTbBDQ4Nm8zrGVYyIbVQtiRK7NzeCn2S2aJeCxhevXxarexrdKg6c2rKVqQk3jrab0Q7v
bQ8Kseik7qGFhQukNCzxS6W8Jr4cUQtV9AEDoqoiq6RdYIlPbOizKhTGIW9Wb6i9KDUcvTcoF6ik
eVJlosUqCLwVN7QcLVnYkbqWuvPhoMUMsgUbo3rsRUYGmS1Jm+D8ueFSTLqiIPSZBK5CMbyu/zoV
OcfiOf2aP+4zo8YXFEjapuOX3VbcQ3ImDT4aNnakPl/kMuw2RDq4AlFcAKZu7ui1YMwZowlvkRBf
//goGqcuUZK4YFYFHewZO8x5FhsVXS4AMsuFNyYQSZxeEs3U00NpJGMltTOxxVeBWxmpoZW5dWCw
8uSU9e+PtHKgdomoJff9FJlcRufnjEuvFiQ8WIdY+cJX+00lqUsR0Md0wa7pwGqtgeh1SYY1zpVa
zXJA2MSoYeG0/SIqSvEohWp6c2q6fZu5INNqg1+WEWcJ3Cy9X76ZNm4DL4tmU75A2LcS+mS2z2gj
rwo2w+Q/81D2ze38gudygY4b95nB8AJTOwFrc+7t0uG0iVPN7qVmoUWUQ92l9mhCGDq31X3C+V7X
pCdMeJkIxJ5Qld4dD6hZZ82vkbsxh/D15j0Pjx+jYhT7JYAtNQ/omLQoca5VwgL9o+ig87tAMmjg
a3BvVVmpD4zrwmQXr8MxdilVOeA32eFk9PJcf+OjqN29RhnkR8ay+UnHWozMA0+YGrXPIRdvo75B
UZPGy2JzsPhWmHBkklKe8eVlm61hoAWUc061HDBW8PxmXkDcKm8lD0PXJaIeO97jUsvRvJLqhI9P
pPYMnHUHpEkUcL2bjVsWYR5TmPvZcMlTb/a/shyyDBV09oVD+eypkLUpiIlc72rHL1MTTGEog2uR
Ct/e84j9dOZ7+FzhVmuHaRvbeSCr5CvE9+LAJSRS3BW84JNGZ3LIxWs+5hUbE9G7PbL/87VJrpp7
k0ccZXoqJGLs95+tgSldykSVpH/us9kEUNmihnxmr3pP+fcsyQfkrTAmY1HA7/ZKNZi+adZHXsYP
IubpwCDtJsVkygMp0zoCvhUlFMa8voHV8JBFzjq0z6tZcWQqdnfDmG3LOA+QsGhd8FrIdocT3pIw
PGZDRkfvozFmNT6axHt4xhhV4kL9jdrv/xoxatDqzzzojdUrX8DuRlIqsYERfANqWTXCBzKqdej/
ocQ+jckacZu+Tl7RQ1FJpwGHzrSXfldF/Ugloal9HQyx6MBL9z9XqwqfhgKIUNLnmvJYfG3GMB4L
qoT/5iQ3ZGBsyA39lOwe6T/juPfbHvHe9k2iYi2SbZpdNMx+VF9Y7U4HmEK+eA3OIlLCGP+ePJ3h
Qigbx0L/dQLsTtLCZSKJ0jpQyQ+CUAlA3/MIW/aLhFfc8OnTG77T991gZbN8VPwBFmPjYFWIog+X
oYJkVTdmbnmtqwuiRu9AC7PfPgrEJYqyt2Fhn6P0dTReOpab4A6NBDwOJVwKTfomr5I0atc1lnHN
5AI/SQMylUNEG+h4Sf6j+HRNd8378pzA9nzeqE97gqUIQtHN4Y/YuCQRa8CvovT2tAr2i3aTnqF4
MARVsdepaKUdEEHzQpZYot6xRZpdfo07RMTeONHM3vV42rSFM38t96icJOp4HU0XVf4rYDH1qHsG
L7JWbb6bC3Pc/3W+IHwuaqxmbHbmovAiyuFhfHKZ7M6v0+LiCxyyrxc6AYR4udMSEx16fnh3Ym1C
wEQaMWxe9hCPO8p9Rpm6yel3Z8OnbKCOODiW+U9Ibt6ctaIpYXnAAn+24M5HrYrW1zQXcIdC1Tu9
l/9JCTydsGQ75uskqOZd7pfPJUM/H1+d7csfiJUzGzok7XiC3nA6PBeiciYMsIcdnmnj4BE2Dn9d
/WpNh2fiKXmPYJiaN4GbwTrzIihSn3Y+xa8LeItdLNi7iXTV+slInryqrl80gHHmdaAI6WeaNwXr
UYxm//9KcQWIspl00vwJA1eBZRX+v/MwKMEgqSYkEkHfwRttnA0RbRgtl3m2yqPYRtWzAzBhca+F
Y5zi9d6ngTpSUhcizb6HihgnahhbU5VMcd72N1NwGBLkIHCqp80klkwoEzpDz5/hl7bQQ16KxaRL
Ebnu1Sr4N5071lQollPdrdxtll+3C22FB+ROMAR5BixIUKp4aBINztArLvY9T1rtsVgytyJBdDSO
I5NLt8mbVIsIx1MbVO73xYvO9UAS3Mz9xqPleqrq+ghz3KsGGzP8icFuptbSCmcsQT8Qv0xJHIXN
dpndAlHz9OCTZ+tapUx2zHbtUnvoo2AfJ+nacyORHr3el5l6atr4tS8y4Z6kYN7voDsmwLjLqhCD
hfpEqjqurqthICJrnMJzPyiAXwzNHW+uu87EPaZ/CUXoj6hHfvRCLQSXEeJAnZeSByIdTKAykfRy
CDoECDkhvXV1Js79kKQCDciSvCxTBpuI7PPoY5A4VhaEo+v8jJFOnQ3Diy3MjiOdlVU8yBxwK1z/
Xncr+leSrTcqsuAcXEN/hK6I3GcAIoKpZ9f+EKv8bVnSIIRTXF8IxG5Ic5dSwt+TZRsAhIi9nRnQ
SBKlQJnVdMed4M+wrJ8d7RzXTzNP0RZ95vALSrR5U+Al2+NbhGAwgozkK2AtSRa7Et0isIalS0U/
jHMvqMCtz08Y5xOT4meOx5LoiU9FTC7jXWBf4QNSpztxe9QXwKU/+OTWkhhKeuxem7/DnAjlfdv7
x+/WXxVXF7DX6b+6qhA7eGMDKAUqciG8QRJL2XPdgPgzSh33/tcm4Gj042z1Dp4Q1rV6/FpFijk6
yfGU4tecow2b2N3L0IAG7bVxq/121n8T0m5weKPfH6aFBU3KNOw/Yla/Bm4jfccV496Gd4uhCjQ7
M1Ajiua/GrXc6Kj1vCFWw3g1/3gkDOt3ahW0ZLt9p91HxYy3cqaSLxr2mu+Sq7EJc2FkOQg3nFJf
ghYMOfxjxO9E91TAH8g9hqxECkCrkA6SElm4j76aMboz96tuSpooygoE55lcq9QUsb3fGr9D2zly
r/OUCHFoGRJGMmH1fw2xTeZl+xFZ7oB1s7VOp29aU5Og/TH0X1Dn0+dRQamFMprsDrOEmAWQpb/9
s37NB9PrOMEG8VPuzrn6U+omTbLODL52eH/4VNZ6aCFKEpub914zrAJ82zRd3QrJSMXt0kJH9dgt
Kh3DCiAkXZihcbxMZR1RCB1AEV0Oj7sidsAgnn85FO/Lt82G6qxbZb0d1l/Sq4IBU0sCMeKqt0++
kq4jy/xwxmVoyg3sOjx7fvDClZYzTVs7b2sR4UG6uXeA+IF7u23eQWijdz/j/i6IsqyOaT3wn8Vh
lnnB9Df+vEYIg6trBPdCDdrGreXdO45pnOpAZco30n0SzbiYJ0RTAW1XMh0QTzvfdpsH2B5fyjpn
MZgOI5RartcwoqRuz8cJpe821nJmm/rPgStVixD1U93DxRXq+loOubAIPsR5Fxue5NCgmrdhPqgL
Z1l7nc1O6BZslnCbdRXmkQSuuTBj+19AnX0ispw6ayWxLue6v0682t3WN68QmPRnYbbKKlzVEW5y
SEh5epHdn4eB+rom6/E3pgUaFEx/8nUs0dbEi2kJilfAl+eTUAcFt0GWyKadGw010ZkuOaXhSCg9
ERb1SkNiBxWyJnWU4fNlgOK2R74gckLfo4zX6sYAN56SXAFBzqdboZbWS+Q+PNRF8ng7JGoVDHmL
Ja+9bEQBj2l+aWDo+MNOND3vP/0HzbYUITUIV5oH1gPWe8IynUVQ5WXZ6B+i62HibVawspN30IkZ
mMDsfkcGBcieUuaasLuigy+wtLKznbr3QL7s9+XyMGlz7jYTp2WafyHmhXbnF6Cx/3IXbkgTjqzm
WdTCLfPIAc+aHfQ0PREcXvZwMyM3SKQ2jX9tpjDiM1F6pyEX11Q2BjXZWmX7Genj1xDu7B/GBXQ1
6V9MniXSH25T0zdlPDemfmxbVGBF6J7FGiCL4D5Rm3U38JnqvuHyHZjRdDjGSnkyLYuPJ2fubMyA
1cSfOSRBXcIK+JI1VHTOFGEkA0CrOW1Tp2lkLKnItG+9+sO9Ee8kIHSSBGKQr4l7C8ZqtD77tHq6
NFMkDNGMm5gqLoau0GZgpPf6dL8k9eJFzaw3CpMwiUrxNbvCA5HReZ6tN0VB+hMjzFZDzH0k4Fb5
DcW3wlFtjJE5aCQF2de8SyZHrRlz72eRN8m+nmBuV5BQbPLwRM4pJh/PSD5F1muddFyaejhvU581
zYMPJBlxqLG+yzO2Ofy1PaBuLRb/rVi9N59Ba/xBbU+M18X23MXejygQ/znM9Yz4EKtGibe71RQQ
H1PayaQp305Ap1vYMaTrsr7ORkGf6qgcx4i/sAwW/+JjgC6v9SISZaKccugVaNG1jSlPC0AShZ9y
TLlNB/NADOM55eJjhPW2KxoSnP8neNCI0oBF/7joomiNuxVtjSP/okzBwb/uzO8CZQ3C5Qhl1bJT
9HH3pE1njw4MHKOI9noEdseJgmk3elciFW8NPTiRu3zsVBExJWWvVThtN8Pi/kp07frn7SGLL0ui
4XopCdUDBbBvmoXZJelXw7SRNptAlk4ggJLdAinkDFQXbsoi32SAA/c/Qeg97S7XKPhyyYuoWpUb
/qafyWRLDc6399YapyDSSzSbXMdqrgug3GNpoUpZ3t8aOAWbMJSvnS5J9Hgz22Np/Vz68d5N8XQ/
1nAfRBIx6hw0Y7GIFxCoyuh/O0didXBRK8Qy6hGw34nrwCj9aFXn2XsPcUhz/NSrufQU011feY4/
cNYRrOsSAdQs1Cs3GMcADgOcnoaqx3p9Fj/WEwuUztXzpewgsNsu4pj8+qa7d7voYpgJyii79QLk
/v2WPqz4iLOMGWkWk5UfNFgTZbwLL0d9iy/Xr6BQGq1bllZwCTXp4WxTlo98NO7JNEfJCyJX8oz3
iIhOkMGzf05MOsS8s+NaAy3E+oPBJWegWCIu5MwO9ZTZhQBTdrZB1gCdb8oTwYjQKfrTR2Fxk3h4
7+9LYuxOE2YGwCTbkn/xMWldpJWhBCAtvN/aHrSmcO3pbYcfMR5qGNq+mqVa0FCkCZSQwJeUq8sW
96btJvaQ0T8boyajNmIpLzJGroXjGzurS4hF09j3W+eWsr1VIY202fyMFIuxyVUTCx8xPiTD8C2l
oVmJ+q+j8XqM9QcmoQoLrZiFnkaxq05GuZJgEcDT1WCKi6PnCtwzGD4ogFX+0fNa/ZcUjz2wQszh
y7twAB3onz//HKcfGbLml7ytQ6QnneTGQAwodGOpIauv2v/baGfcijHDWnk7xPWbViQGBIHkPe6G
WcthG05BRYZsfEMRdVUwW8Xggw+jy5dnrB4K3zHoChdU1+gugMVfi76VD2j4u9Oho77SjbYVXiWI
ja8jO4H0IHhh82kEwsYB7sUUPdAmlWUVI4PzeJINGubriMdn830GoLAvn3AUCc23sn+YCeLKUDmc
lBgl6NtiMDmVRt5M8n9P2/wozlX8yfTqv/PJpFh7hPiHczOdd2J3aozHAiAC6pitS0nbbG2oT1rU
PXlNUH/rbzd+Bwd7s9SWqBEXU2xgasCNz67upm045UxBWTreSryrXu0ejBxM4oTiB/qY1PuoR7Ry
SK1tyErE+eYbe/Y2jFdpMQKHqw8CJJMh3talyuuvc7imIBz0JaVfLrDGpZf7gVWcupu+h5N1TBVk
R70aN0mIJEgm8u7hZn0ye3aMs0QZLCNf8u53ezJmfnKsYRGNKZU0qw3CpSaZS8clXbDj6sSx1ZhF
mY6OzIZ1d0mu/jgUdCXO+N8rc7APKTBZycGoeIZ3uDed/L6YB8LvPRCrsuS96tJAfxuJabmfb2QL
1Xj21PuXZkVot35yVxcTVCo6NmSvG0yUBvg3nDZZr4Le7yHZO8d+nS4f8wVli4+OhU8HUvN7Ki62
6EnCW9JZvSGcj2VldascBe7ooBRJ6vO4iUV8KRImKi3spqjlplz/OtaE+5DrefUgniLTj5x6YhPY
EbnpGoSb84Jo9vbnmzqBE/HZjXP4QjfMy3PIU0dcMvgv5izf4kTG69YmB7/2H0cJyLArzQ9atlKV
DX3lQ7Yyhuexh62Df8p45mxCOMpLr+7oCKhDh9mN5cBd9ORbSGjziuZIUOK3T/of6rwkGzKaEzS5
wIVNeZLaJdGf65rHZzvd/MbabRcJxT6w5Sj3HqInUVQ/b4bIScQIqiCxUYxEhyRXBl+GB5RcHC7K
dnD0XUix1KKNFIIeHl9CUSYIsq8lxtIe9ZbjV5XuMRhlJ3nGZViCqvM4GDIKhKBYajaMMVPHs900
zjmONqzYDMtYBmcxHVLz2AQxl/V/2mLBDbkZ96eBnJGAm4ERL3NwA3mkNBTvDnC4aMEPXAWYRpxL
ng4WrN08qgGALjK/5wF47ctJa3wFhbBsxAjPwYRMJ7cppbFO+3fYH/eVAmxYRwb1ER57iDQW4n4d
g+EunYR0R1muGVffpFOVI8dPR8vH+OB15AuWCwQqJ1PUT1fFsO38NBnHz9QxskeeMqbqRt9psvQg
HwRPvS1b+/t5Mtyns/WizFvYcyGqi2u7VhZjEr4NY4EIlvxZm6O/ILEaFQznTwWAao70n3DDIud3
xjzqtw0FsYhdJUwMIEj2yeEXTTXVQiHpHdAF/8VH/lewRZZscHyWpwfhP9XQUMJsuIGhXeRiyBxQ
lqFdlQ4BySFZQBY4PkfXfIn9epG6DMDuUaScxET3c0CQSdPmzr8VQLp00K+MmZksy4YRX6G1TdxT
2Ooa2Y5nWSqi3RXsTZtcMvRRr2KM0j5Rf8ff1JfdlS2We3Gd6wXwWf9hb7I6sJhy+s3ppg0mOu8u
qcf0oC9fDkexu8A8WnQTcFEaX5KgwgwLGv0LG9ggwKrtP3Hc3qpZ3IcidO72NpwXqMmo/kb4bAlV
Zq5HiXqbe7Tm09Pm5C6EUSK7Acs7UGUPFGuetlblXRR49FElv5/mlygVClVmn1lUWDb2EOlpasUR
9XgD8HL7jZN7evB43MUchunRh0PMAMVyP3KKsNI2agprVIfwgr9nuEWKR+Vhf7Vd/CqHvdb1LTCy
Yp++PQMNqgmbsw5dghB+0nWeVGDgzFS4XZJgpXT4YDOU0idkK6dqo5j2Zzf6kczm4tQnjb4xJcxw
s1sLi1SBTARae8/14Afhdslu5cpjGyuJtvASvexQohNatamMTj8PMQs2UvSYdt+UHLlcx/Ki42Mg
oHvKBZA0TNw5cXeDq1veb9/DHGGbvdtIyb+rQIp2rmmBjqcTUGh000Ioys7cEIljpzAlA9FACF7I
YLh2IDB6SxX21vAvhC3A4Kt2wePMvwVfBDOvuikErUXpVafw7W7hCxLx+Dj4lTsnJC4uaUs229wR
lwmBDsOuBfaOURnWWtXN5X1S3S1LT3Xx8uy1MYvXIgrie2JBxiO8gzHDtELIb4ONFAGF1Vo9vUJw
3gFygCRNkONxQ+9uYChmOd2TaMMAf0bN1R3em2TUu4kpsVJdhtisi9jTetu5lckncHzBYuorjSWh
ZCsuYUqRvn2yp45sNOp9RNf0X0NOqUt8Lvquek94eiy+O/12lJp/Y+DVdAyesBHw6MBGeaiqUE/U
bt8+v0X08QT6t2FId3xwM6TVW+ahTm2xdLTM5oQFGNenSTcVGKtYRnWHElWdqHkIKVGV5GtlFQaw
uw6uMzkPswRN0gLQCXtIl98hI5Q/tUZSWD3QZK8A6BwtYCVl4if4d7GUtdBQIFymJUVz7Z7dkypy
VZ6v1xKbOSL7z9wdnroZkzn76C3Y870Fq7yt7dBgQpB9tL07mM0gFrsZRSgterLv2amnbdqKS0gE
MFH4Nnajfy2EAxmuSvImCs8nNrx+jpBOpQoi2ACcJaejYnr/6nzmc0BlZPgpDk3R72yXZbrYgCox
E/CfkSCTICBHolmDpyXUMpfZO//i58+xSsRk/Kvc5VL3w8L+9cEtoHT0z3N5+b4F86ydkJWXyB/+
/tjX//69qykU1SY75Wz6o5bmP6pK5GBba02Me9RHkoeTs8u59zkNMzrC036Lv4UrlZEHrHZFs+7/
LzyHiUhToUWs09LeSm4Er6HFE6SaJ6+l8D4kCxxrNd9CQzjEn3SOZ7RufK0nueaIivk3/MIEtCws
25E7u+O15jUNcJ40K6k4/8rRmfgYm6aMHTRoLfz3hTwSdaHxnHUE66yl14Mz70mjlteF1g7+v1nv
7QA2WEqIV/Z+X+UA3x1Wfd+u5Ys4/JczQwF8VL6zDBiO7FW54UcBLo2CZRk42RQ4usjkSesJ/vZY
LIrpfh8v9pa3ERyEhIkw14vQcCQ9ayqviz67eo4lbRcEkUY+wZG5GJm9WWaaJsI97Ezb9syf/PYx
hg/jsdUqgmmsOTMgu6mnUKBWihU1N+VqXWd+F1Q79DX5GobrJHZz5+Gm8uDzaCqP3MAovhDub/Nt
0W65RSGPgtQTGiiLTBBLJzdJXFPWLVvZ+exq9v1jYt3D7/jhFC1+CPtVcE6fg+LKxKz2I3MUfRPV
fVgAkar/f2iAo5bwRmGk/8/UytSpEzLdf4gWR3p1UTJns0oeKv0Wp/IYEGNkz6PXfCgWrnbvy+b9
DzlNn3l2R/BKn0r311V6Ge1wb75iHm9iX4MA+jA9oyLS77JmPfsp1qZNT1H8uNJWyKIurvyoa6xy
L9y0LYyvVKSVAFfxEx9ywRaWa7wLiF+ko6hfoAhtcSqjYS8ppMg8SSuLi+xOyEiYbY3KhfmLzsup
9+rVCjPYxkQ/+vdMPZWn1V89pBmoHd5zmMSj4B1BRFGwudZv+Uu9Q1Oz/72i8ES1N3iU6ir0ISNv
ovNVu/qhRWPOObbEA3JqHT/B/lZHsAEWFWyOneujMlavXL+0z+EZAOjqhKVyBYy4LChKfe31JqH1
fYqAEAiNduppJRFEMAYP+t+QL+8U1t/QZEbh9pd5m8HFcLpdGEYR7pp/6nTDtWxb3cfgxlZjGAkj
vgNCaGJhzEW6JMX2OFzNNhiJq2HvKUjFj0zkeClwYX0Z9T64mKrYEGx46CAl1sWIUpcheYHBTeyV
2rMCkt6EF5n2pXDtx6l/wWdLm3cHpphIUpooKzVigVjp1Z8H/z2AHKJJMO0pTB9lRPXaTgOXz7ug
//W8QYa9Wjqv3JtyEaBgEyTdX/EDmjEx0UFT4Y6wRA7IPN+AsPcPx/X+oOJNsBUwqajvkqYfjLBP
pgh+OrfFiogJivkV3WR7O2f2bi4Y+J+ZdxoF7uDlsuy5GNwLBSfofk2DLFSQ1vANvmr9Il+GJZ2F
ocrIpuJuua37oEK5TGvryrQdbXlu/8XDitrPlS1Nu07OS8BfL6VBCvHjXLjGPZl1Of3YUPsAl4pc
RdL3VWp6rGlxO2LoAyiDNkpxk+jgcsdxhQ38zLkXJzd1pwlTfchXwMF9QigC+54/8e+5xIdSXUxs
wBtD99MF26fEkh/BfE5P9ez9ALHnNN24VtScNCXGfNLJNNCZHEkD3+/sRCS27LmpBZj4ONqVBGMd
lInYxdo1edc8WaTx3Zm5ynO5eQcbboLXezYO7LPoet0qbuMq6FbGrvWJZKwSe43gDEk56SOHZGnk
NDDD1dpnf2IDfKBPQ5o252CYnAqxA1H7GvwInCGqllYnhJpHOYTW07xKMWlXjtUF5ODU/xc+h2+p
BK6LqlIb3TZ3jQmTEscfWMd2kDp3q7ndrM6Fc6FAMrj8R6qklNvSHpwLHrMu2LoJfLXG9qPHjb8k
oK5Op084Q6curhseHLpvmZz78fjk21wzWlqgexppMnqy+On35noEvxyJIQdlhdXo8ZejC8GcJcVp
SOrSTEpO0mVkLIuVvD5+jHZK9gnySiqJfhscO5KRJZNr7Ci04yGvLSmRtqlpzUQFFhMvyF1/K3tP
OYfBZ6EDDO2ovl0FSCwgMoZdpEXm6j5J7AxYfcFFM8NWFfMdM6yRvdhzCEaoDq+COl32teBdvbhe
qUQRoR0pGKuFwbXWeONz7ZQSOdk9BrcIptQUItC/t0Ut1N4J1prANpXJ1YwVUZDmFS7ULdjpFfwW
XpJe63aRFW/HTmqer/IL0UoYZLqxU61A60SjUsOpAy/BnAGUcY8IhLA/el9eA5eW5KikiZP/z5zx
CV1LKwo5y7A10BG4CqZF0gN4kjGDFdkLq7K8KY06Yqg3IM+b0PgF4mW7hjVSGCPjbbRa1DodtA65
wpqkJ0HLZoG38cmyzr+dCXnB8cvBKj5XFYa+cyYBiGU60oDt4HXYLQ141eKfhPGyyFjY6j+G4e9H
+6kc9N5mpEeDgnQeEpkuDlsYOzWwiVngAJTc1n87EvB7iG6S8sG/JWCpvdCGrSQwPDdIbhKOvNoE
dR2Kp+bPQqBtU8waLs5+LJvPCB6DmqcBlrFgr7nBNUNuNAI14Sh4Z5xuu9zJO+Ypf8CfdWDhLoFe
TBNnRL6COX3S8+Iw18OfxRtUJozABIyTpYpOq0WF6MyynGFDsKU9x5f9ibzuwlCkbrUlsOG4GMDh
/9iWTMmcjfpg1UDEph3IQmPbd0GKQAbR3CMcAJ+16DCnkD3wz20ztv7I+Q7ou9W8A99IktGkl1tf
AvigfZPf8TyyG837igPEv3i4Cehke9R0ZP0r2L5y5fImuPmMPeGi6iWF5Cs82wN4F5PuFkSKN7hM
l2J9BiEumM4rtQWQWVRSPp0wKPDKQEp+4JGc220YqRUH3av6ZpgFSziAeedrhHBSrmYKF/3zamCo
z0ACgY+3CcWRTbvVnEgaPhy+WQMqDXvAo0bHNGxFjRGj+bwpdieZ1+Jejys8gNW9g1/Ke+N2YlI8
wKkkLxc0nq1k3BaOY27KoCZgsN72g/uVPCMGOyMLVn7BMnqSNUbUd9XUi6RrkZtMPn4tgMdXByc6
W5xeCPBxOelgkaykWdGRdlBL3j5fXd3bT2rLdI3lBlQKzWwJhJ11mI+tgZOUCddHbXNd1qrZ5wgR
j0jx4J1Iw1U5u5KyRbBMfaygrDlLbOv+vekUim+qLx1vXVk0ok4BD5p3s+WkgbKpdQP0QNCL565W
8Cy7Vh4+JOKVpl81wNoMna/n48uUn3GxDA/i8kQ/RN+nburu1YSMR210+3qmoDd6hesg6/l5RJLH
xM2qmoMP1gjDl9S9akW+BztNEG5xdgW0N7OJGcHs/4CzfE1TemSKd1W0eXKNya4M39NMZ4ck6Q1c
WBbejPr9E+4y0vdPIt5/egK4lUPX2/J3twS3nq+HbkIj/lhvZccwUYx9YGiOv1NrLHjuV/0aiX7x
C761VmYjTBuoZ7Ap4c0VCOTsDqCbDt1mw1pLIDiPRQBxpClc6W/zXTrr+mnjb0rS6dJfByzh4MPY
0uRmXXW4bCZy7QSsHAwfJEweM+nBXfPjM8zBGO+KXUac7j+7OJRVkP/HTt9BtTYb/pcEDiKcD7DS
XK8dq16vU7mH1oYR4T0294oSIzG344BFxdhDP0QPnKznqxIgdNpWxhHqnKHXW6ENXJR+hZjzA+xH
qUV95Mo+GodRfNwme/hs0P5RUuhmPKmmplD43GluU+0OEf+OCSogLPpWPEqGvmiSFxzh+JEKEPhE
Tc5hSZpaY7J6uPKMI8wAgXtISoArJSPBivvLey6LJ0q3AhuSc3Xdaxvv9eRZaox1ZcPciF1DMayq
8zi8lVCo/9BTfddS7WOoYTMxBMHfocc5EADxRVR8KYRhoZQguL/bGkxbvHz7pP1SuGRqbAW/cz3a
eWmjhDwAyJrDBnfKv0hY/hL8R5Ntv/fyH/axUHESIvTci8KXTrjRtYcNTHxQNgdbghUrwrSfsqQG
9KWr9elStz/+nfDf/9NcxgWPrqAdBSdgrq3pzj9uroZYMxVcn9sQfvdELK4k6RTsJuV7CdlaiJf3
cfwfpj1mN+NtMtniPzAGW86MtJjMvFcUoEgTiyEY6+3gUnqK0hfCXg+GnoWxTVZ4I/kLSRnGuy1e
+4LlZkcv0LwGce0AqFegN1Ap63T8Sqs5hnbvXkgOz5MX1Wc/YXFZtXSIMVpWUpAsbNoloZ5jeK0E
m7K0eSLmK4eEpsfqr0LgYDefTtKxs2fQo1AEvcBXgiC3jPgLxU36E0X3KeSmqpbVJw8Cya1rahyC
WAeRZoOkkZx5rCCemIGcya4dHj8g5K6+3fayIeV2569Yjjjv+5bC7ykTXlM+68QYtRZWqyyyAAGn
exopf3j9qiCxcLhF5PwTbOKI42+9V5Av7DH0zDzR1W06z3xnLIcFcQhPaJPFYgSq1m0B7cLbMDzz
ys4cuBRXX5NsuXtxfF+Htm7qTnSoH70l92JVsVUONYSInpx4/7IEN4DZ4Hmrx5Lbje1r5RZrbPKU
GPq2il1aFGs9ICeqeaqKQV2TXmTeFUtmSLBFNsoYp+8k9Y27no1PbX8Y5eSHxG5M4dnHRsTVaCZl
FM20Vt3RRDJmjmKPa3nTC6La7Uaq151dhXxTWnbLG5E+jFNFMQMh2AWQKRR/4Jjs2vBdtHVXPGF3
KcX/x0jXn9vcWgp8uIwYEGksUcDEvtkDw6B9ptym0BebzXhNaJIoI+v2p8+TWOiR7RYBd7rrcSsR
Gt63HD3SKgdpP1FktLyn41wfdiSPh2dwawHN+ZCmQ9FawWTosxAiA33Rcrj6Cg20NYpTIgAbMyiG
maEZ9ZcwDHsHory9MX6ku9Ur/ph3WfjPF0M+gX+IUPl8Pzu7PG/xEKtjUEf2gVeP13TGwtwufmfH
my/LAcJTmuNFy/rUuPZ/Rqxm+N1MabvxdYRdYQjbcwx+k0YfgtD9wXfHSLgZRWAX2NAK9AS528bb
0x3hbJHTcqz1zlrzEmr8pIH1t5MmHzwNg+0Z8s+LlqWPbBmlI6VdJ66w4ULZSjhHfvHYsfY6rOU2
8Ea0NYVLEP0WrIvRMy1Uc0wQ1KuKDP0j1s9SHjNvdu9FwUFFqlp6l7kVvUeAK9Vi/AUmCKuQ6oxO
+HwWyp0QBX/FzYdkCRAwgcve3ZQVH4bGgOkhvEkOwxBncbvpnUxQAald8s08hl5Px6geb+zOywhh
609dInDkOxUM0QcbTjBI78diBMWx//q93be9OZpGavFyZKo2AU8py5V6lLBvBsAZ/77geGQnQ/AS
5zPaY4NBBnQGcfEmeaCa9WyClXu0v+nsSYfR08cSM7lv7RPilE4ErSSzYpT+cqmIe8kUc3bWv/Ja
/UizyXT9RYfylR/MsYieg4EmD1lGn79kjctxnTuNuRkGm4SANEQ/4hNwNEb3nr6J2zCzhgkxoHiP
/ZnTIGvIoNSDtHzUbUiG8TQs5B42lk0/LcCghfwEV9ot4NVdeX/lMy1imbMoJfmvgkcXG06FPTOc
6vOG3UDTE5SSGFN0poKq8vESNW9rehUe7wdnD8l2hdf86aLo2hsXqpft/g0yl/UNNSu7Bq4fTlP4
hG3FwJhORgeLT0BMZ+H0c7lv0x8GEkxas/nBYQ5XOKkLQrTvn5bft2nmdi+2L/PdpwFZObVDtn14
IoZwz90lpIgw4idMcWaw/3S0BZDEolZoS9dpuelSeeEtp1YbJvdJBJkb2IX7WfxqdFYPH3ow6M89
F3Vd0BtpR6Y6H64sDOhcADukfA3N4VJNqRyJ1/tNMFf0Z700wANPae0ii1/ObptVXzmLyG4Tkkhf
5YyjHMG8t9/IsHliV5qNA2YkBWdmR7UlQfqSeLwEOGV6rryJS+dFVdlmD4IW7QONo0Jbn+j6QNPs
4eNGy7nuJhDU1kAZqtWkzlDullndSha/WTV+UZJuAci9HM/5A4Lql5DXyIonZarSK4FemkG1QhPx
2qrVPgUFwKYVGqWNqE+gUmTU5kgz8vvAlr3b4FadmiKvtIKwQJszU6ekX3973D2a2R63zyD3EX9u
yUfi+AJwP16f0drXKgBkspEfjO1Sp7GSJGpV3HbJm3X9rpr0+QCEEQdLcPK7bJWLq8S2KFoCc1Uu
KuYmB0IIJGhMHqgxxUd3XfcMLXskHEwynTAKBMFGmCkvIPNg+Ldf8fECBtz7gcDqni31d3YplVcM
naVME3rNaR2xnbvnBnUmmEEHI03qCXBCxvhWrmZ5UqWLILWbrNTzkgkikbw/OsoBGbYeJuDtgm7h
L0QUwSKhRRYh6bCihlBCe6/VWnzMEU+JT7tlZcLGXL6b5b/0nAyM+Y+yrSvXxkEEg5XX6ep2buD1
JcXKWUC60e59IYIDCMaOf0Ub0fe9o7f4cs+/Z/0FrPSMyvDG15qEH1WCxclBcA2k9R5wCt4LYLlE
L5NeHQwiTCOTWGRR6NqZ8LvsF8dQT4pHveIRwsMGwVdMMS6ssdXiyM7WTlVFu5N8Dz9ejksLPdTb
6EcXoaEkJhQcCEbKcIL8lRsMvbyTh2k+lsWwFGOcfmPR6/iIuUwlMjQOE0qc7GdWro3SRDN+ak4D
prUZJ8wgqiX8r4eki9Mhh1YwZa7MOdlA5D5s9/nFea57oEl4C1VwHSkRbgMNXt1Ki96YH4iUbbLo
+XGgfbkwZl1K8m1xH5tOQEZvTseO7w8Gk3WW+YF3ky0YEVIVHlF/qL0sTFEYVh1GkKFqXwz8/Jos
Fb/dQSecr+YYjdqaezjaiOVmcvXw1DVkD9tVG3wgLT5QfcIp6/wuA21dP2k1kCpThL+tFwa36OrT
004byBRtRV3JuMy47Y2gF0F1+V2/kq314yJ5gaZFz3+nlvUpeDPbjwfcT1U6TYSgwhmooxc7s0bT
tcTOPGE8IkUHiV5LCpFo0WABCTfvUTGsluqEuksmpE1tVov6k0Mv7xYLU/RcasbJUf0e0kf0ai7J
5oK2y3hwniPcK1NNjb/60vb4Wjil4rBVZmNhnGwJhpxk9kbmNeVrkK77awZpYkK9QI+O1UMNV0/l
ohAwlwVRawF41svWr8xFZEkkOVXK6f/QPPzPPLvfJwYUBB+bdqFZSJKtEDBRpoJG9pU/LvJNueUQ
JnmZOs30t72QMLDy0bIjzeA4ysvev5rGPDPtHAwUDUo2G5puXrPfXn1/BjO5/0l+sADfJQ2B+fRo
hFmFAVV/UmpIxgn9J1/ZIRzUMngUPrGyqlNwUcsRjgNLjbleCobN1ATAlf+9rd4usAtXxJ7YuM8e
Gv2yFgaR/9oPuCR/TokGxlOzCXrGr4tgqkWMUxtj2kqKETtUIEr0VR3HGeKAv1dR+1L4yoUVuTow
6QuWd8bKJ2PFbs/hxrMoHek9PKELZorh79HNjz1B7FfVGWMuutpj51SOQyOl7/q2rCyOa1+oiCmx
G6uRKWzPOH9+ULda7xaBlqlF7j79kh2t3wvYoNetzpkqpoiVkhDIOm0GDOwFwkudrEYebqHhO2jR
DYjJNQ65KgVM69RTqtmATG+AUtgfu73UCOolAbNodhtGaB1cKqffa0AhK44x5pCGBXCqbFfogR5d
WB5rqPTFM8HPozxcPg3no6s4mi6iv2pFymjyqctMGNiUqvbB6eGd7w0xGCPEXmpF5YBDMFIrbecr
9El8N2FWkXSmJc7qN/2ewEafASuc8g2vOcZtiFxd7XEqsBCBtLS1mIuQTnGqWua3QsQqJ2cT6ds3
FK4jdQt7WVCLyi3g9zleTpW1DSdSW+DGGOFUu3fcQROTHj1A7aIu7gVsUO4TwzCYRQy/wAxmEjrJ
gszLckShJYtE9AUq9fsjcJ9r3CVHl3/FpVEvMFWiDYCaKypoaojHMsWWr/jmk87foofh941WQXt2
OKRxstJl3fFFVXeECXjQ23X3M4ooSF3hNoIjwOldMsFPSWK2bVxjLK8ME1wN69mmtY7rkIKZWGSE
xJxGpqDniwuLwPsRUSDLVBU6wXRdWyzMAJRK85lis2rWSwWc5iHHWXYY8w/7uJ1rjXCJdQsKGEWr
Wb3yPCLcrRlIb6OdKbbpqZhETgxaqJdVyDAZgxxPl8/jkvp37xxu3qR1hRVDg6KIyIM8R0w8hUDs
Cw/LsaA99q4LxMqEwE+ykiUwcackLDNgkZGFI6HN4hiltjHQieLu42LKq4JeoNmmJltkn7ZuMK6O
E99aj4OR3U/OpOrKhXEvpcSq1+Q15r4HfPlX1T5ZDReBnHXyS7I1mU6tIM5yOCgNrCTAPQd3ZupG
XHYvoS93NLbUOtvmo7sy+fJpLrCLVTP64A8Z9IliTMZEmOkxHxai7Keo2Te4spMiBzhG3pMYIEZY
lQ82rGVunARZg58LpKrqpTEZTijyWAq0RC241/6AHSSDvswyVf8V/NAUX4rfSuww6/AEr2r4hcRG
BBzU8tu9vsobtgZJI6/l58+5gOHvoNl63pBLHMBEOef1/tVJlyFehhC4zWMazcwKmcoybfuMOEM+
mgnn8zktnSi+tYjc9neWVLrUttIxZZtC6L7RKb1S5vV5q/XJdOxlJARxvm+DwCCAfE4rK3JyXx1K
xGVBHVNwwLWEr9DFreRe0q+emS+FuIuzSxLy70mYE8EXVJonnopfQKWfWgHTUSe27w1fV2F6ofMr
Pj4pvb7fhYg8F3HMQhvElH8D05gDMUMu992W+EnX9B67uS8xkPWMYyxA726JBzPlnw5Cj6qhsNSr
ypno3BgsW1X6mjKVHFqVLzwBQCxcBkUwm4gCyfqw8HDyYHk9NlqZZa75LJMQDlJrLQhlyYG1tGJM
1aANf4BZr/6dsWon+O0hewoorXUzv8GgXZQuG/4pR6A9lMcshBiRR7qL94sCQU0vL51lZRdSKKlr
85o/NF6JfEly79U1EE5hRcXA/KaviMV9ADTCghtFyLrilTkDkneY2k5Uc0ukH9E4N+xdYblddR6f
oHtdtaEuFeZaqmcZ2nBNzPsizOxow1FVQ6LB/5SORLR8KPx6PuFG3P7ccrzMeuuj0nN+I8Oo9Mdb
tjanblrlCCX8bpk0HUs62uj7DDzFT2KpvQqSryJ6J8ZDNWztR1gU2ibuofUujb8mcGf2m3w2YKMA
ogXGPK+I/Km+8MhPIG9Xu/HiVxbRgBqIYSoi/vjfV1tZM3xoStrIxNYyPGBT7JRfFu/9fdei3ecF
qdwveQW37ahGTv02v6zSdbfXdRzF7hSzADMy7y5k8POto9/h8q5iA6CVtVXz6JZbC24t2liiZk3A
r2SSHGQALd/41orNkJcsSpwFD1ljFpLfJXgLHTU3MC+l4yqS4L16OprI7JiE+oUtHI9EeEBsELqM
ljRugND8gmCmn9pVe4dvb0N8nWlRhItpo1TZZp/ub5w/ID1zxFHO1iwnwIwBh3HJNSp/IIpGXPTv
8QYelkqOebmopmrHyQo4jZ97QfTQFciiFzYxURDgCxjt4TMD/AFoXWNyYvalBHkPdM12WLJhoibR
28W0kLlmnGbx3HgnfIiA6VZRxLK2g4rUu25v4WxV0nsLGuGxQJ/GSQYMddRV7/l1T8GgcTPO1Ldi
Dgn9txUPAf1d9JlMxoEF/XhLjWIYOK8PBMzTyObTcwVhVe0eMVrs36SRXJfvENb+nqYomijUaVj7
ETGXljYFRJ+rGrmi3NeSA677MTdnc8Ya5ZuQDXSjxOmApc6gq14v9yg0moN66fg53Itvmvaa5Gem
WrtduAEyJCBZ0RNRtotqSgA+EeUHFF1up+D3ttVNLn9LC3b+9740Us782rH9fts3iU2ehExn5cHe
ju9iTcBCAybG1vhUQZ1vDqCj9G1xqMduRKwHZdonS40AJapI+3LLhDHeZ1iDTfuN/LMiox12179E
SSv5QEsEpqq5zMwFLKo/fUcujcI+Y7b65XiZNppJ9zC61U+H2AAw8wXWG1BJXmjaPNDK4eVsqpjE
ON47MI7ZGBwkAbBfjrEEpLRC2Uo9s5CVWbIO/668wuXE7j8YL4K+RXGMOykj2iDLznGtPI+QRFWH
yml0Rlz59r+7yUeUeG6xnitrZPkTLSeCzfI5j4Gq9+S0M7hVKYtG0+t2F+2cg1C+2Ap116TB6SiX
fjo7HjNQg8YDbiyzU+nD3BYXOajq/ev4wEw4kY49vCQZO61ScMpU4IB34SWmi4o5yUn9tum9KEtw
kqknN6pnjWoxDn3wheAGNKRj/LhSTKU72wLsG8PGL4M7K7yQ4G4aY5927RkhjloE4ywUVqBNdISl
f6dI0w1ZM1+5mXZVfMm0XM23KOsFhxqiZgPGAgHdsATZ6KgmZZilB1qal5NaTB3qscsy2ViR/CQN
owmQfaeNZWOPn1FpZtk1/2T0V2klGrdnIIJE332appgopCyvp/IABk7/vIDLZvf62WDnyl7eLkLv
DFD89iLN4hF27mt1tDUGp3RbNJfHLorfmC7vq3kOIcr6VkXp0LmVLfzD/JCX5cUOam0qLswOUJ0r
j5rscG1LNCoJJ+w2Ip0O2vqQ2OWA/gfQBx4effCrXXo2brJA8FmCRYUY4djW6OwUqAv3QVYiHPXP
kjeFaRBzO328GLBfsEIo66XQlW/ljO9Q9ThfhvXsCWJUurLUs6mmOWQvMbYJ5Hv4R2ZFmv0IpR8d
bFVgy3PbJqZA0YyGjdq+lMDvSLGoG/nIdFT2XBgaATsI8izXXnbF4X3BadfN3EifBmy6ZOOAc+zO
C0+CQLujPC1Yz5dl/dlPEpfmg+0T9LoZ9R8aHY2JMxbBhrQjDM8El1cW3U0w9ojrEH/VWC/6apZt
mZexukDkRt27pPjgcgWa7qYHIozXILvrTd+FuLVrhRw+nKWypvhT8NEcf1EBjGHFvP61yCgdyODY
6MWfe63gGY2CYJBLryK4ZH8ZjWliiJ9Iuvk9vjgTRPSLKhaysE+rvlgSgmGbcJLllvSCFmo8oDHp
A7WZOIrhU6xKyoayJVDF+MNiNhfIMxrE3MJc2FL9QhPtQmzF5M0UfMaaDHBBchij/zo7JK+tGUNG
5veWf1xLvfCeJ27uNbD2IkeADBsVtW8TiO30Qf9dteMyFxJ6sw0q8EYznIOO/f7h9P7Y0QmhNHBl
QIOqSPkFQulXEnwfd0mV0vJ3SB2+uve2jphCJtyKYVskRuuVz4RIA1qasHFvgtQ3XbpYPNao07uj
dpBQjXHY/HBPys+frTzrdQ3CbQzNyWSLjWKr4j4PWYRp8dZWjWJJzLB2vwaY86SvYDG1D0dD05Lc
Oc3cwuvQqVKBuomHC1j/qopODJu7xK9aLOyCLDCPFb6ERzbpObjs2Bu+oaN1wNpl4wEkALEH+SXO
iqs+iuWA5UIhWL9OIsswivKCxTaovkp2wy2h9g0bwQihWerX+Oe4Zkk70hZ/+p404Af8J3W+wxkX
DKBivs7Mt/ncwPcIznnHYjCoP4OiMAIAkezO2aNgwUcpGIxU4gXnCKQ/gZY0UdOeEX2aA+cTgye7
XZhW3aXUWfEzUqRXOXC2sEUu0UVkLu5/hiqKpFrt+aor747jrbbrmZxy74RCMToIM0oKLq3X3BXf
Z61j8MqKmTcIUMbm/wPRueT0liU0TlxdLdJT6HOLFtFh1lIUv0LXzCE6Q6lq/hO9IC4Lj3BaoRx7
j+14+WeSM82eGRATvcgyzuQa9i2mFeb9GxUJttSa2SHLqxTgVm1izls6u6W88V47X7BUUb/DjUYV
5ChiczTSn2LAQt9ElDAPCBBdIdJO4amDYNIba5dg5PF+zuZTJChiqJlO1GIxOI9lVD9kbFwr+yS8
Mrrrf4tkoRNu8puHatWjC+nzjYfAGja5iV0kI+dtgMBLsB3vH6S11c7mtPiDNOBGSwwl+6lUbm0I
owB84E1QXGFuGvj68CkqCAOHYQslSUO4VkQUf6/GX3ZgBgJNjwG/Jm+Y14+IDmUfVk/8EHkRC3dR
W+0J6BLB7pKFiJ45Stl8p0i3caoopVt1FNL7cVoXGXztE7rd8ENfwvd1daYRpPnKmXzDetQRff5B
YrJPuMwCBNKFtJnqdz4b4/Xo8atrWyW74+n7FAxVFhAYVVyZfGGRCwc2uzlbIZWEQL9wbf/gJQVz
2+qpLZ4YCUnok6bYQTfgHGLXYyu9id+VWfjBh7qH60XY0BPCyr5h3NglRNHkSzMCguPPxMTplgSy
evtLx2U83EsmEFr004bAFFoPm9FV7W6Xkqlh/X5FbPlenbAw49OV5Svax3HxPI1xSC7m9O6Lelfd
9OHc2JUdIWPyKQjUI7UlzhD9kBOfRjmTANjK+Gh1SjHCBlvyXyIGkvQjFaB2EznAo6e1yMf3FNQr
Trnl4gk+zj0TUznYFD/BbFc9l8Q8DaRuGsNMDgyKk+Wyi/mZUdhQh/OZXe/NqgPMAQwcHD9B0XZL
0rSl6ZI9eNJUpuTUplBX+rRp2xcQ2QvtAbzRPrt1Etn2HCh6gpWqY0q4RDE0mCcxqqfw+UtSsNsY
CQP57GZIBA+ABCJjd8l+btTEzqnYFyRBjuYxUYWoxQBlCeCaXRX1AA7sfee1upleSsa6v1HcBpBH
HUmqpbftcMjtby9+zOINkFFVPa6NVhM/ibHmQxTob82BXzr/nA9hwX0l0td5M4dM6LaH6jWtWeR2
V3F5/NOcdhKq4f/RlJfcvVf4iDJafAsggejJmKCg/+mC80VeprgCsARuW0F/lVetgegMx2g0vCfc
GCi/i5487Gl73pnsJnVXUPfS7fm4cZWljrx/Cf0mfDYjyp9s0akrFmlm5U2SlNXCMfBlzkQEKkRG
WH6hrTDra4x4HMuJSge0v5SFIcDcwYs0Pow5RZLTB5OoLtI8x0EK1h/MzY1Og8BYHtsq03H9UN+1
hK/8nS7wDrikD7ZbzIj8fEf0v25DOIfaVQbRkf5nfKQJD1t3XlYw8KQMlXAwqtme6V5UHVG4wCoE
T+OIOqSJNERBg8LlH4bacErs+Eik9ppDBA4DTHrOqrM9szE6mblwB5Snb1a0Y6ie5a0JNHB0djSv
UkghhYiTbmGAZgvjQ0crmcmU6ulCbW1ui+vDQCBLZo98LI91qLdyzzjVmSP5z2VgoVvkxI20FtNG
ME7FnUemHtD9dRSpeg77/ZHNe4N4WYOUBKXooCKQgBK51kpU72tD/s3BMvowZqpdFtk4xJMP3VOW
4KgivKvVeM7RZ/z6hUxsUAfg+dWQ+o/7eakfjK6heWkKDQaXLcPURdMcn6YgvumN6cxxARCyNW7s
CU3EhOQiiCyP2em6mjN6QS/Bk+piqtb55cxUtP7sTBf2zOX2lZp47LBQ8geI5AmvytH/xE9havgU
OHKEd/Qp/AWVXoKw/3Bp8IPHWmSWP2CrfhFJZ7THIwl6wDrGpGCVrS17+FC3xm2vd+4a7YU6O2av
1531yQxOpQo9UyqXlPI4dWJ6iLMJsLNr4P2rWMCgaksa2wOdKkvQuh8cueWjaLnz9t8YZ8FzjH9e
absydXfm6EZWbZwjh9BunQjTieSurM6uTrcXd51uA9vhlswl398kbWunbxwHxkOhcGmuH63toAcm
7inFoB9WJqZZ1sf2kvmSYGsQFGoGJBuIIikxCHWr4caTthAa1ZcKakx1RAw0npyBC9ulICMKgR+1
RoBZ427jZ8mL2Trsdr7DhUiUzGd/brho7IckySt7Rz3W98Gz6eA41i+WMa+/bzBgwdgVBhKw1Nri
+4Gt87Bokp9OSLDCr5o0J5ppbSMyoYWddF0BKhpNBdLwti/1KsBJ/yBiFAY+0Jqm9paWd6CCPWDi
qoSS9R0YgvAuRNueZhgflTjA0bpn1VwuaSBpUUSyTlUkqgeRwL+gukWNRwnVGyZ97OPNjfzdd5Ho
YO3aLk7+6YpCy9RbgP9I6Zo85WcwRxVCI6xL5AC5JWfSIyXGlMFO1P+ulQ8Xf+WvAvTlYs5H9j/1
CoEMWQkO/jGYxVrmPUmo4C5oKIYFsY5adyZXNtzMGohFHGrvWn4mMmPe9bQ+xKv7C+fGDMvr/Bdl
ORHKNsLZHO44qLH5p9Wv+fWNfQ9KJSXSP/7yMwkpGTVezYCWr8oobg3iVL0I/VJTKtpkzA5O4wVp
wt3GloeOFfEbEhcTmr8EESS/thexPchWECbC+WCcVjd8mDgxu04EDd3xpaluOSHrbHOi8yeHqwzE
HVnwLneApcE7FcI+t63EZgR1Z0EzPewiMuh0WlZGBEObZdSF+Azwt2tWJU/as7l75j6dSUelz6sy
FMQvUJFxG+5LgSiKGKOm4OSSqFs+ermxzqqQ8m1wno8aumYbQumGZ9j2zTZ9Vra5EELvttV70mFJ
+1pazOVE+Edo0MtKPm+uOzs9++lMUsypJGs6U+n+CuVvwBuiC5oFNpfHK4iBq4U6UIVYrFYQzp/L
zly9h7tjBMdVtqanAzawIzJ2FTmKRxz7/UsbajjmYXpAakCzu+ZgLl781zcrgHPxVjmjh5UpY3ee
UZv0CqUjU2/dHm8eNK68shhh5ZB59jhoN9CZJQeZFEthSiL76YlMGlDHFGXdmRwIwkPj5vnTmd1r
x4vc1EVQC4OYSq0zfy66EwTWMc01omRpHheFg2MwCUBuxqOR63RZcS7B9Fv64PlW624JlWgnyI7e
EBd28iKAiNQct3w4tgcsx4K+h21vKvd158xG8GfsJAb2fCNx0vEaNJ7tDDjhPsrM7IQ/wduCFZz+
92UisV6kc1YTsGGTWjyCs8PCUX5g1aCWZxyNbnfd94aisrXNe09m3MLexokMSk8Ylzgxpk+2FU87
gnIDT+nVdY9vrLlK31qgqsS+4ZlHr30onM3h7Rl5x7OjW1LUQIDcOf5yQREu/R9gjEupJIX/A64j
sCuwvVJiwCwgymj+ZpDWZBzREUxAWlFVvGGEgudF0GXjaKMjdN7+vyTAC+yqKygp4igKeou8Rd5E
G9+wvyeZ7rrpbR6ExxTy77y/qdCOBstrRJITBUCQdnORyb/o5f5snOPG6ERroGKMYcpZTMyN8PwL
meY1Y42+LPVoVSnKCM94NLKoeaXi5VXD7k1qPNj1yT7GDEQIeWgJFK8bFKYfXR/PHqo98Agz7uEa
G3WKtQFVpOErzm6ifbuRbm69boSW4mXghds+V0O8iWs4sSeC4WJ/lebVFa7z5E520OgyKDlcIU+n
MtcoJ6b8w8FjPS/24bI0UKhZTSo/JLltnCD9aYrRBBMrOCfhCPHVHf2HyKvdKdxZrVgh+r37JUKx
ixaOyHeSkmyQbHpzyt0Cn6D3xkVOa+lvi52bQkU3DH7BQiVPRT5v15LeHANDo2d3k9DnomK+WSlz
FxYOefUMmpuIa5MbMX0Blt8XDtnyXeo276wmewN0e3g8E2/J41w5ZDaMIuChRCLZoVVYLKfYx698
QbhpACs4Lb2wsAzUy3uBvheJkrV0iGCpX5n7Gh72bJkVjd5gHIoijsgeismiXTne3irw8bE5ERuM
wMtkSYdYXG0vm/EHQjckJxowg9zQiwCbTOjjVGRDJG52F7G1yAMxk6T4stBaNcpRnLP4PgGFFdsf
OvXpgqE1ANuCozHgoAl1tBhD3qNdC8FqrqfhKSz3y+G7r6c3b7nrggpinsXce7uIQFokpDokzDSV
cKeHqbHMP+TTvtm/WTrzlk2xItOBaY1XjEq5Vqss+k9wXEToVORGgdPLRyCAHSehgbVFmtNkVLKs
LnMEavWgjnSCD+/ZeYN2WJk/SSsKH5wrQ8Vo5e2CZxW53vjBfL5dscKXk9CorrpDn5mpELw4R4cW
0BsgzC6fmUhdMthcg32pRuPOGBqQpFCcwFOYFCWfofm1uYaWShv1rKE7zHg+ymXqvZShvR9kbyRg
5YgTrSSnjxpQ9+flcrDpKKP53Kbj5c9ZykYOr3Yw8bG9yTYTMCfkrt+9XAWhCKtv2Of9QEPb5Mpx
xVHLpNgSw2ViZtK2yQjiZqxBFLZ1ydorlAJObfheoo1sb2OORRSHJiGyVSnXIVLCUzBdUfVcPmf0
DJzx4PgVGESVWP1gYTTW49CIujGUZORUaxjhtyKCidkjJlQ2hWRcUhlb2+N+50r4/apBoJvv6oyD
ZckWWMzEgD9vjTAJDBRlSiqouBV9fVjbSIh2rRGrv9XOKXP+g5dneRr3hjEQGuD2E+Wm6Lv1Ech2
PbQjqsHr6X1aXM0lfdmJWoiMfiKF7xg6HLNVRjR593twf0+pQsjsHmpYPD3aEK/wDDGNON/dt0dq
ui2AnH3rNwYXUtr5nH+ETV4+0QsdmBdRfk0AQDt3XYV3IzeN6CN2yoBS8R6xvOhMD+yrRZcl++8h
ONcNkCXzUIUhZWYI36a0ZRq4ZzChoKsE58cDm51POv+FI41F00+sn8w4quw9Xo1yaDV6Yx2l69LE
ImUy8/KT3zCNj2mtTkOyh+Jeq+7yEqeEHIozorhg+GE2fIUdhH3jzLDvUI0mqrmCkFpJJQuSgWyR
mpNAwNf9zC+YbTkrOeYCRXjoxBBK8ReiVfHR9u90A+0+npFyIDnuYngXdnJPl3ngkqGQhepRc4ov
BxqXTc3q2wQjEeHQcYrxpsrZkYIeHuJI3mViBNzl60BnR5SSN6ITGvAXeOcOIomilj3m0gxvuoyM
m63PVGvkUX6yi7RTX81tdyOxk++CyiySklXOxNUkvEHhZ4LrXgPyWRALTZnzUh6YSy408keF/lyR
CkHay5/uxtky7+1eh0nrpExtgGqfKAIwLWkHaBnz91x5cmYGmE3ANzAh0TnhgLLkeaSmMiZtX2a3
kJsNh78GYR+pSDgKbY/no48ESdIQ9bvKSPK/QsuzEIic9x1FXAqmA/XrJrxb1U9uN3fS0qNyE2Ho
wC+jofO4GhuGoG7FvNyNqjwEnPovqYe29AEIRgkNlvRLH9J6OCUSxdcXvDYGj3pN0NpTUn6UWXHJ
BdEZecfBRMERGKhh4+QzXfORzcEsXbzRTbgkxT/4WiV0Gticbj8kzFyxiHf6CqC/tCG0spSnE6VT
9I0uddjTTi9mZTxbGN/UT2Vh9f7vWk7Of1f6TN5q10EXvNo6d31Hk8kIBpbPhCk383XpchE6iwbx
ixNDCTVAJCrp9Wp3FQJFgrpjkyyV99OzHwHzB6xXYiS0jKu/GpvWGYfeI/z1RflrKgbATkCXz0gV
DYFPsqPo4HZWpPkxdFWtJrpGRfLSw94nHvQ3Zt3mxKTee0tawgir486N0jyNkeuLSD8bNAJHtaws
nv/0XD/IODwn7zE1DRM1vIn2BeWAMOdkcrj4fwmaDC+i1/ZltYc8M/Vr7J5fUVVclmOsrfNulOXF
7HzozEpiNPX8V5HoXZnCkNc61coYfVi7TGLcIcITuYgztfV2TEzYpFWlzA0i8CaNvbHRHi/vAg4a
vo/Qj5HOoilHYXwGrdW8tGX/X8WgeTgC8uEr+icADGgOXRjKRcdsS2EwMh5CMT2RQ1pe7pK7WKFa
TF/2HsKtS1MTrbv9uZ5ICT9dkJaihIIi1k0vkwm83SZCVcAYvNlqG3x0PZUCAUib9pcNImdndlw8
TbHPtHpVXnwJyCY4MXuq6KM7a8jzdGkB9wXdf4FqLalTJl9Hx+z+JmoBxutFqTbtzH64qcy0ETjj
ZyMUuuVbsCRr3cOfTX67ab8F6rjnJ6/tzRTQR60UtgFFSlnjg+725c9s321afJEazgpcCjyKYQGJ
Bd8rJrQLZRC4mcSy9/SliuUD6ZzfAF4p2CGqf+t+xSF82IA7gd3694khjFDsSJ199Opj0nOir45t
/x+giEq/eUFRrbLcnC4yuvD9OWHevfmxiLckA6m5JWUWxemmLX7fj08+t6GxTYUpNuCGB2z8TdrW
H7+yomNwZVg0eCNr6IChnIKd7QTTRNUxSUWRfGpLkD1zmpbZimwRj+PDcw0uuBe6J3eoCrC19FwL
xnL9CbbkrSlQQ8CaFA/0h19oRYcuWNMut38PwYavkIHefnVrKtYRhnzPxeRZdpzlYxVSiKnZgkrc
YH4dEt2wRyYj42hX5u5vyWd/P3Fyi9nG1uS22ZR9o4dDDoHN2k2+Q5XCG4dmDVZfppNHFDFPttkY
v9XnMERhdATwQFlqM3QdycXcB7kE5MaEDh16Z29aWvKRRqJ34nzpDYq9hd3PkMokJCoVlOXrvwhM
MP+ZPfagXk3dUzYdQq0rEPZLM4DmuQTYwL3GPa5EtTASsR4NV0gFSJ+2615zdgrvVFlQUAPErW36
J3ivIXRxU69Me9lT6g5Wbi0iVPKTTH3DHSkBINhlI+mVbP2vVT9QTLv5eGZDAlKpm99x+aQYDFcR
CSfNuWPkG5feWAPa21+yHzdH/dPuO0RnZYxZYfzzE16sa4GZAhUNLLH/Vt0ybtZ+ArJGEDRqWD3V
DY5Z2rW2w7TFEWKsjXETmwKieZvfp52JOW5WNh2KxpvIsO2JV1wzODJFA1FfHAeDavX5Ts8+O9wD
LXDod2YB81iyNLbR802x0yjz2/GDgbMJjNP8z1Y0jSsTjp6KZKWcSVPM5CjFGV87j/v9KjDWgsEy
FE9dTTFxmT+2mSP23LgNVg5b/pM2cQqySHhp3JjH5RjPhOuAvQNIR+Em8c86Kav7cRnEbhakogPH
e9zPqE+09SsOE/bstm4ADVwu8xrKIO/9BQIcJ6cFOTBn5UzxMA8rqhX6XQBRs9ZoNWrddprKISj5
m6WfmMZjwYlTe/Xv9GU6b3LuVf9EeYznaAT4xSCw4PhZWikyj+meFG54HBwOdVd1bYUpVE7lne7S
8n/wTBY9qWvmdmkmrzztJmDgoKD/roEY1i45QMooFAiov7ggI3OKVWsQ8lNNwMBg8423cydIXJyQ
Ab5uMD87DHQEqmLlFEhALjKPYtKbP4XDAoKA2rod+wBYhD+yG5fkMTudDNPboTxhsW12kOLDwnQ9
gsZjc+DfAEEjP1camB+QbAmw71dDSVKI9wnqKfwATcbw4IQSuY7ibNYJxiUffwZH69V6wPOBEYLC
m4fSWZm7MGG+2TPnRtKt8p+JPP1kuM2qP/kwiwK5nHjACF8+Vhd1w02w6VJvaXlRBBT9s7tq0zwX
Tx3BvA6fksO8Dd5BBseRh2e5/J9JuF9jj1F5+OsYQSwPkqLfUwmwqhFpQtflFky6PN95U9gybG+8
7NOopAhgFHU3w+6I4yk60p2XAGMSFA1gpo+1hUoFWXDiNWWi8x9AbFwzIsjbEqBrGEX2UU+adQRl
HImbb1/6r/niQ1yI6ztZpcmIn92Pe3MzCzAkRoSBE834bknoc0K5ViLYvKvAl8jwdApiCDs0i5Au
jFlGegnJfjLAXISIt/91trSuZtuDyrv6eeKl5QHkKZo2aRfFE4ZfsI/O7D+NW71R2xCxf7fZGWF/
+u68eqINSf2zHtj/RSvEjQRAhftcyXoLPlYjgZDzPtOfnL2fm86K7PHRXs2eJDzOMYbfAbPu8AIj
zDxFrTmcTB+b0S6M2Ygfm+BP/r/lHVxU+8fMurT7n3F5vXFrmxnHMx9tJ7pLkoHetkD6L7M4gTgL
se9/bQqfymeh0A+pgaMlNkoXB/YprC/jGsn36WZxJnBGJ6QuGCAuco4ak/6eB//IUG7txycIzJkb
Mjss6YUSVVF1Ehse7fu1ZRdc7s359mMfPU4UnH/r9yCRVG6eYy9yyd2GoCcTqQH8ykH88hcubRP6
6LW/1tVlqEPJ7kOXI2tLDBviqWTI3Do6B4crYMtw4vhj+kyQvJJkcD4w1HGTKebLCGoNmrsruXL9
1GES4jYFbi1Fvi2yWxat+6W0QonvUBd0ncyppzBvyBoWSVpsl/Kbhr9XKod579x1bho7R3dHbXi0
Cdsm0t4FUzIdIpO9NFaB1rfr5+NEGG8MBuZ3Z+lzINHaq5VsZbopzmozGRDUDmcW0pDrUaGaKgmh
Tf6G3wfHQa63aJDELEJObIgVaPAQ88mVqrq5PuadTPHsZq2Qzbh0JE28nSDY06S1x/rzJ9aILUZG
L+SdCR22gaK3xnhCXFXhIXv7QJFm9cPYQk54IG/8KIrIGe5fIJs1oahm+Cm1xZ82uoBcfKVAzC/u
fhCrTGZnvsb3r/VlMg9Vf3RS379SAv1xhOs8i8Nk/UMkmLBXV33/XKBdfwLwK1MFZci+WJ1TITMT
MqZzynjj9Q+ST2aLTYVNDwEua6uuOHmu/uFm4w5FHOz0DeQiROWExzr4edOigmDYWdZDdjyHByDv
4J65zsTi0UOIQOm9UhkWrvebKSI2jsq1PVq44WqACYrCox0FC8LA3AOdvVtfgs70CsEsSLYWAQaY
OVcc4UWM6krR7J3tHUfrQ9IpkWSKMGsPiVJ2tjcaPOHNQ15XNHSiWPza9az5TQK9yco6qJMIZdZA
JCH4E0eFdlqocYOVcyx1imq+ZQzKytsgGptKmXT7YjOa9zly3h+hV3m2XQ5qbboaTbYeCp0MxDcz
YPz3eykjdgp/qZX19UMVWfuGSBIEPYpCxOt4HeVyOESh0hWZOwenusInG8vOwDCZGo4e3uTOVsv8
fxOkIc5zybbbE06C/o6AC/T2R2QG4RZg2Y9nlBYpI2UArMgUAR0MwZE15Eu7yp9VYoW15QuMJYIb
L7ZI+tIOD3IykblzXcCrZ2FnTj+Hd07qXHan034vutjVRXFtimV58HBMf+jTajgPqxAqMMDCzMLH
uSD5h++AW6Fka6CiP0ku0TFY7rGBTVDDEK6xYjCbLSxF8gyF5fykirpe8oIdrMpgh1irefPHgZb4
hFCJM2Squ3bX+5W5MU+swDPGUsip/VGMYMXSwsn2mmXYCC8kYgYiXJKNFwJ6YoZSABe9o/0S8lcO
XPFZ2P4FiyJiKWSLtuifYyHDsowf0MNMe+kgCDnMpex5m/HkMYj+xh+kiOIbrMV2FVgNfLjX6wNV
Gk4xIphQju5ZBiXG49MGwLy0/KOWUfyhCCmupBcrzEfDGFQV2KBsAUY4QE7NYk/t6jREqPlpUHA7
zqW6mT8p7lerl2+MAE4NzEM61Y+VnTtH470dyReihmciDW8yKocsgmYkXu0iIswtSpYjyg0VXFqR
M2SEszV+p77UtrcCP0/yu//SB3GTRwun98+8dGniXHhVYcWeSrPD4ypQU5KpCq2AeuKhLXAyEYaw
kbfq57Negfg6Kcxbpz3/teGRVRpGqBEztV6cf/lx5sxgJU6raork05L6EpGMyN+LrZwgR5Q+yqUv
XFakJoIRujCRUjzOsLd59Lmkb12b94HP/LIlE8nvzyGbQthdRCMOObZNkfE4fbgrFj4WvR3G92I3
BVeSziUKh3sS9Ol58ggXtOLcKkgByKvUbilo2rSgUznsCHCqRpG3VvY6JdkuvSMY2JZgEla3wEZ5
bqo8oYDeya+yheyFmJfrElsbylf8VZmD3RoxKrxKlhZGvJ4i8UrXIAjHrp98zpANOvR5ReAvBwjT
54/mYjAkLwmWVxFoqLts8DLlwRIMk042elmHkjm7wg1KnRg4fxqFJAUfVVJLydf1E1nrhRhHYykN
v2mu3CPiDNp9bCbM5Ewoakh3SM/AVZINJDDx9uSKiy0QhUR2OWJnVt+T3eOmtZKzEgP1Ahb6gEq7
aRIreHywiT8t1w7f11M4DxQ07fo6CL6l8ZNbyo+IeoTe7Fs1SJ3wOnWAgT/fp2FJuBcRWxQjeNkL
zmeDEqtlrW5TUKebY1mDDunakjycmEg3Y04KFQskGuGUAD5r3Kh9GsE3/Q52pZruyudd7He1WdSa
Cr9zKRJwg5pGHJOp0hP89Vb6fpr0c9ChYAprQAQ5QR0xkzb3iVe9RVf+gT7wvdGXLWwPKShpiFhN
aUggMGmyIPIFYDrgc7onQZ6BTfruGvgSgz19GKyRShawiCpfEolPJ2bCn8jYl+QGT+uIsRDgG5F2
qCn2m2OrssTa9IiKPbHivEjt7/HSVFcRM8fGn1HEBQUSJgWm+dTjkUqcR874rH7EVdqwbTBUAaMf
eZLRVpzwl1byx8+uAnl+4667PstrAufhTa8s/Cd39U+ZpCSIv1fDlI6X96yZeAyyEkqHXfqps7dT
80XqMwD/YIeM0rgoAmpTK6lOn/eKexwDMHCOdRR5n0oyXsDX7h5sF6iyZIU0iX3shMfGlPlTjnCm
w2b5irRRbZ9m0OMcuRTFkN+ybk856nbXXmwGLgEGQ0gGVawn1GHugdKEx32MvghJTs4chbngOBDU
TSvrWfOMzPcanrm7+VvK9iCXPHqsBcuxUVtvswpxgrVFVAUXwA6O5RNpJ+BEqGLBn9zQ4BDIuNdT
nWVdzoB9nP72D05XpJcMS0qVM0bo8KpujOC7zqHBBucLZgu3fuoUJoDbAw75cgFkwKoHMhSxcb8c
YjMW6H+ds8UuaLupydDXpGOlrBqzwW5RyIrz7OJzzDtKo5ljseVIj8j4BOIv/wmHNb7U3Tv8b47p
Ujbm9aMi0YcTqKapfrLh2Hk84qCWa2uEnbU6wMspFl7uZ1vH2BEWcAgoBPQBBBOjET1HjPKjSTrF
HPgMP5IcFVq85EKE43ZqeXor46AlmgtEz6B07qpsU7Y1wDwBtR0fIFTrvkaEZDV7GknJ95XDlC91
AN5M/DcTynU+Lwh8fdpXFtk8DAK8WtNU+p3LUI/rCCEBwAHY1P1tVujGaStWczsrLZuNdVP7QuAz
V9elEo+3Kl3BsOuRFuyOH60j35Mt/Po1c3V9Stb1T+ggTE6Gc0BaGnon9NiTgvMk2C4L5/TRbG8O
ERVus0Mg4nFQwjTiy4bEHjnkqNTQ/FMtEQgoxHmAzDowxuQW/o9UATuLCin91fik9I6zjVQjDWuG
zvABjBd6bsW7MbY0Uq4EDaBlqgc3BVX6jey3xuPgb0h0LICuFrPdYCI2HSdN+gREW2bDo3DQsApK
Yzssa5IaIccRFaYsOvkg05uyOeLwWghb8Td7Cdk8CwVndjTzcLGFt6HQFuQUFXcQchEqAlplWUBG
6rmNcY+EjYG4mufEdcfNKEFNkzilC1bAujjorRZMKTt0i4E0F8WSD7sqnHAY5EVGVdOoLlpUnIMJ
+X/z0EAB6dccBUDWTq6BsgsqOi3ul+DQiY15GrBbrhM4IsN0LCRyWETDD7fpTvPuGgLmz0HRm+6Z
g3MVwGmHfOzBOb8x9UE6C+bu4PoBFH1wgHMdf8ybPCSpNa4++VqUF3USk7NWMKD6ICVHmedzxNi7
VvHCVXD6cL1kjTFejolXYY3nvR36AWNerJYF0EeJUxmog09W1Q9KtcpGsUku6kOK5quIFqSUkcDH
cPEYc+F6R8VEO7s6q+511Sep01ntQCXrd8O8l3faY+46K4nmEiYEcEcDEcAIYZAnCAiZAZqpQNVx
7XmRB2KNpUMKTMmOKpjziSdNmolzg6UdxA8M4gitI1/onIbur0xfa0TfB/eTwHyROnSZiOLkwHR6
t5githfVVQJpQwrsEKReLkOUrtEq3MsVIU3CHgdiMmF+8W6sCOSWQEj8uD92CossnrDfsKxOIBCU
/7LaM5D+GSL+BCF8+gb53NJBayhpL71fsj8/TjCgg2cq15jZuovfYrb8zR6j0blNCbN+4RZ3xko4
80d1M2e06k7P0UN4Y4Gs6T5osLkxX5z3uXSL6BJSeRie4y3KwGsjV4LEW4PMD+EzWa4YJ4+6zGJs
8JEOcFQcWuLSIJvT/zqQY7Cuc6UZuyY6i/vcCgR7zGtHxNLPYnB+koWNxZlPxRLoX10voumtphns
LQwXlwRE4BzLqwc4ygmQxz/1st1rqV32R8EhimrLrMleY2ksuMcD5YHLdKEKxOWMBiB/iyQeJFRJ
2W4WDbZ1Vcj92bETDNSWQ6XVnJ4FYhhJXb4sK4vU3ImM3HEwW0mZTY+XhTQllECjAbaTJs7aEY5f
ES6E6448YnQYCWOzbujrN4qt5udCZWrM4R1vdETCmiBEPP/9bwvm5WW95B6R2HE2SAdTxvxkgEhU
W28d+ja5AvndqsoH1CFtmK6luSwbRh5FcGUE9AZHUlgm1lkbWMjYm2Ol9PdNQxi9xCQ8f0v449BS
GZogMCpxpS93ZHqDfTTdJLVcwry0i+CBwOTDyJStIBYwn3jBCabiAsjdtDbD0ycwr5SFADGUCQz3
4zrCirMynnB5uGe+NYqG2OlNSSgm1GTIsmY48kXNVeTJ/MfsPGlccI1tS0EoleoZV1JSBl0FjDDf
BNQfEa4l7MsTF+iv7M74DBNPDJJIA6fY45+S7Lx7gDUseXU64izd33GbAv71ha91u4SROOf+n6Qr
0ScKrhwWZ/aIi0aMewME3L9p1t8Te7FWNQJmg7w75UTsCpt5YdmJDfXf8ypZ1g9M26BVCjGgKmRQ
D016/gT4BAt1V1eriGvSWF9T2NmsSC+wusifzY7YmlvZI8OskpSVDxuEwFqemeu/YeSfzfUfV8Kf
ZwpWI90exsIi9tM2NgZj2qTaRQDubnWrjMLgrpVhsPcxQnOcqG3ZZUuqwOXlglhpz84AUIttlQvq
gLp31GVpUovUPTsz3uQZsFjefYswVwHcDgMfC7ERNEHrumQCXyTQxUDqk3DUWFlSMRvXiTJk7FOd
fXy2tK+NtwKHYG34Tk01ejGGVIppx8Zklt05cJ8U1jZxS7hSzrKdUdLBalh/KMx+yrtVEvE4udla
EhQzjV04wNJNFnzvgV9/3hWdYwUe3n5avgdMz2wHmk2bWhCcpsdmoFbMoRNMOobAdaTPwXwP0Pwi
cRy0AxEv8NMRqFwlvGj68qtJ3R7cS9O7z85DboMOTV/38gFDJy/hMw8+VwV+oER17DsxA4Cd2EFJ
2ieRA5Cyd7aefdy3zQoznFA2X2boNe5gj3/vkix50WQnkn5OLeLZKWdQ6PhwXxthPAskIIWVppzW
njlo0cd/CCtwpObA6ZKmodaWGA8t1Mva45hPEH7+cPpFU7jGOhmLA9xKdgFgc+b7BuCAc0dTxgCn
1PqiJovZKL303nWn+0IPN26qR/aNFJSwlmGBYnEkP4TQAy7NLvH3I4qPOxhU6i04ng18Swyw2zi6
q+A83DNGrIB6ZJ76EQk9ULYH1bcn8krJ/kj8dt1Kn2zcaF3Ci55ZOoXMLzVSwirvYPz+yVmvTE0Z
rEjz4wrZWYfQybwJlVZIopa8oE7+wV4dWuoXQTQknVZBvMkyiDrQK0Z2iwaanWTz1rlE064iXnkc
j1ZrJSquwL1rpUdZiGN3suNQF1pFf8yC31mcWOeEk3BJ4K04xkkY4BisH+a+XqfKj8N8yoA+k+zu
v6HaXjOwv9m0kFvp83wGa5bZihGLKt1v0HfTkuKESlhyahkUdM22n0gJf/AGUD0foT01IXGHFCx2
K/UCjyrbDwJ6TSRIULY7XcORiMqnEoAE5KGGPFyH7+1RIuc2SHv7hkZxqYe0uE82vAmo4ozaoJ1p
wtPX2K/sN5gyKCzDI4fUzK1NOfORTdkOWXPaeGFdmElOM6eYCV7GJ+rov1Ie+oZLD+s9syIaI4HC
Z0KOKUnGAuvmZbmQneNgw55cNsRJYBIsZsloOxeG8pPeAX2PvbPmCy/yU1G6urdwxuPI6AFD6f4E
G0Hx96Arf6hIPlI+t49tcObcNjCQ+lY5SrkF+rioKwdpiLpNi3TzSM5bgnRjfd4cBoe0Shv0rfyb
j2BOgO/aYVKlWdd0jwCfCE3EjI31EJJPaflNq+FVJmEsc7EI4STSUO70F2h9DPMIT4MUqiLjbPoY
MCC6GxxyRXBfjaPFN74I1L/3CKD3QDmKucG6XgQ1NgzdwmMAR8Zr8HbckrIBsjypTDSIvJ1R1AgJ
DdorLS8DxNb67q/1uxzdUGWdS7+s3LmHfiQfvpYBQBA/duvpMes4fofq8LgruJeWGMpUHG3eW2V1
QbY1gvOXF0tujSucy+fdWDVdWCfEvoZN0CS9D7WvjmRtQfcuPY+Knr+pHxR28KiLyPrM/BRRFXXR
FSMhuUdP79nOfGrrWBoRBuS7NKazrrnTI9nvPx2pCbxKcIzhYttF/ZVEFkh5YuabVQxGNQArDCca
NG3R4KJzrSoefGCq0EmyZmPhtSve6uSeJ7om95KP5rJsFrcSYY42i7lLp/eFnIlvw3Akgi7B2W4I
MFYHhJS1+WaEVpb3VWtNHA963EZWaaCILpzkxOMeVgE0UPb8ecDX30U48g5bhorKvnlfN+tGCSLf
xkE1poNnMoxpDixS4fIWpGl7OMdWArtZdPOyE9um05JDabilfN6GX+Fok6k2upEXSTUli5ygzF9D
qXY8bgaJPv2HsM4gbxLUeLcXNBaRp5REO2uFIuETdv8YJWpHn8zQengSnkrhCMQtEi1esprR7CfU
hWk/1Rq+dhH/l4WVGbhxH0JNgHxLgkLiT96reJYK2N2RqFcOJfY6Amgu0BDykHoOqpmxWHiDVrZM
iyqKfzjSGEIZWA0GCNRtkvWmu1kvWoj8Wi2wPLO/gjdtHyQ3OStmSOME9Voc0A0ztAsXWKRPiWVc
nDf09tpCcOf5ck1Jq685DkiIzUMYgyEXZ2K2BHwlKcNG6xI97Fl1iLrhqNUeJRFJuSr1Ii5xInCI
TuVmaZ6p+jD7F/qOh/HcMOtuR0JkscuPCEdmOJEQlVyEQXK85ucewLRgiJttyZWjHzCZdo837gxq
csoGm1n7eJ20qYjf7o+Dzk5FU2UV5HllxVexIOlwSKXBu+AQ97NTMWfmDvmKZtk5Lra0pAIB5iQ8
fNcsxRbDaOEReJ2fPuA+C+h+0i+zP0UEThdWNQjXZciU+OSPxROIFybNSPLJyjmQMWRPQ3+zasWp
ZhXalrzojz2NoMQ3enhPGlLhY8AAl+Lx1kkn+tFdJmwyFvXwuYY724z40OeaKAiD9tLxu0/pKiHq
+nwNV1d4+CmuBAZB/waw9ozeXSgSMf9dpHt5sAFO0VqrZJYuVLf6yWYhdqRX2g5xVXSWyBtw6cfu
lXiepOl7cXbTIi6LrmbHjvDa3cex+hs+rlofuYF1LEFjr5+jW2bs4MJlq8lsJ+l40yPxhrLR1A0k
Aat0eb+1hnFT2rlyERQz28mhNVbxDmfZRzfmVXk8JH2fNSrdNc58jEHcxia8NhvGz4x76qBBKBp2
cj3rDRM0eqw/3hfAsPBTsNJXPU0Q45w37feWhKgOkWUu6EDt1nt+saE5r2pvuX6HAA/KpXDGf0Bj
lOPSYd6M2zmLTrgwbtPAGwOhqloyyYkIezH9kc/66vnLaqoAth3kY/g9u5bQ9PrjgPV0zrB9GMio
C3zPM7QgcwoEbTy1wOZmseW6A5dqGnTU/RR0moVPdTh9geYR8dgm7ew53qkrgBboL4nKBEe/RlEt
739JGgYRI2CNUBfVdHVdy6+90DMP1zH/98aF35zUYpzPNqN+I17u/nA3+6uCAxHOr6XWT57qdZOv
QncQk1LNL4W/1TMJ0mPLrOXpkDOCt2SoxPbl3XL0WFfKsQKAKGoiFnmxKvvqSH5K1ROvGtwY51PC
5c7ahuIBjdCOwci2G3BMNyYaZsJLR8lrjaDI7fqILSclpbSH+MX3ewlR9ubSv6nGOjdz2qO1FHdD
NdQ907tsjG9LDZjqr1PsGWXfKDo7tYnpyURzoCwcBgVBg/Q3kOi+LCQ9STZiWWkIiCWqH0vUa5FL
qUx3dB649ULQHLAZJ0mWBzxPN4yoZXJ1okqt99gs1NDilhy0LgssSaWjLpNTCv8OYVlS8uTXFhRD
dLxsFM6/1f3tMDy83d92UcBrAoIdWV77Rvxy2RQQa81BK6abzJZibAIob8qd7aF5zIA8apBf49ue
jBDmIg/say8TFvBOuW34RbmJZmSDsjPR4DH6gZzpUXa8Y/5Qi+ORpBdH0AryfodrYpUYyx8R1Y+/
HZTxmbrFvXhhKLazfBJLf3+3BY68XOYAdzdL97hM54K5l6yyqvTZWwMV9aL8svBLzFYxqD31Nghg
9rQOQg1Ooa3frfkhsA5iZCkKtpG8rDDnrgTaoahGpCcqcQkak/sASXX9WjXTInAT2bKuj2xTRvoc
94PvcpD2XQ47T9EoumRDwY8Xq4Nl2h3kuP+j4/dgBSkkKfiCWSfZH2tBA0+JN8Va3gajUliUr2I7
fWnLuohB7+9BrxTAhEEBvsY6JDiUUcO/Tz4Haj4XXHjYQx6lcjTJHHibdspQ5gZvxGCeGwLXegdT
8L2YRINmx6UMCKCU8jhqQt0r/W4XZyfHz37TgskLvI0EnKUP51GigxSEO8sAArm84pq8hN5U5Zml
mPw2/FapQ8jCy0fBj4lrt62HdZWqaDrI289hSmnNsCFHOb3k65y1saj9iE2CEtKFQ8EHjYUkCOIU
qcv2sW7jRNEAv7bE+wH4WTa1nH9oh2g1yr67wee8JVf006I4FDNMU2k5mhVDoEFSHPj698OYkEhz
8/ywR3eTGFMyKd7xofOkETeZxBLcxSYcm/6efPdlXyVjpaqRwl6ZHYbxjxuL2Aw9mcL19FjnUGC2
1+ZZ5YUEuhQAKIhRynTbjaZRvC3SLAtRXTW30iP0qnwhKJ5A54GO9gXKl/FHbFDqUKfflGnvQN1f
q2QawoQCdwDgaghOL/Xhzk55TNG7Duow0eZ8qpYbcNyDRE+Wewi85C0j30u6Cihe6I1w9+Plr6fs
Fzl9B/RJemQm9tqjyh6/2QzsbWzT5LJfKjGQHn23jgW88qRE6aqQBc5MBbuXzA86JL6W8zgkLyqY
lpdWtVhzpKsR/fblNMPvMT92zvgEq6uh5AcW7Yi6DZnRaMezQmOV814tu4pub6TO8vsmkroqNnA9
fOJjy8zkp3cV83BgJS9F8wDmffH4jV3tdYtOkqAGZ/hLKvQeL9C3eSVm+rQdOyyogaO7EXty/2oE
SpRgvuIjqNFrwrmKk+Y0DMy++2INTGMsXO5YnlucdnfgzHGzMU5MFjyrUPkSoGqi/KGPiegoCnf/
BfLRlJcZm88p1m1ePf4Wl3iaXPXeMWS5TWNCNnw/WmebiRIMgAetiCRfNS9YEGA5Ppyqk9iIQJ8J
ZYX/bNfCOa36Q7JbLQTNvRccv85+tl1ZAzwgwxqTSuFpJCOnynq/te9QZQH5NL6N5Qg5FqnUceP2
SIKo/nURzIlHLsAvE/T0wC7ZpF88b7sMyZRIoWu32GdvWM2tmZZiSttCeL52KSzNTTSDd209R24K
nqIuYgOOdg1at99tUm3CXrjIVu9sx4CHTYdRTHRON3b0RkOmH+nOOq+e7QRN0GJAnlE8SCbxP/3p
ZookMKFdVhSj66WX4oc5aYvnsQxbtf11Ijce+qH2/39tLcBBoQ8yuhIR0hdtuteB5wamX3axe/GA
yfCOisieHIYZHcnRvXdB5s4ld7fQgs27arvdK1eW/6aYRZaO7McY1x6oTcZLtLQts9qGenIfvoyV
aba3b5Gm4EQNFh3MdF0b35rVyFhDAo+fjq7MBVXeUEZBiU82VFhAB6gEci3luzmwk9B/nzbD7bK9
I0wC/sP5ckCSgutgDQ3cDogN2Bz3SdAxhWKAlM9o4Te9ykK1PAbdZQ9/iw3mgaLfJxhVyGx+EkWe
5saBRR4o2OTPL2WHZ+SDkKR5c+F7h1fzTZCBK5X9ex6WaqXis4/LoWH4oeZwGolwD9gsU1RBgNiS
b71rgg0spsB+jwUZ3AZtogI4G5VaSzfxKmIpgDuQA5bzDKP5arxuurKJ4gI1viFRwepat18GXRYL
knACifYsoc3pGRPfzaRLlRwKyhg9zCqtxnlBZRR13qTIFMPBT7SAnw4khvSvuYJ9GdKWbqkQ80N2
C7xGbCM6I5B5GpsyJhTsy74VqN2nj4zwmsUdDcmazAj98c/2ppVgnr4EDAFT5j+aabh4VdBHQOFW
BE7ReFuqehmMC7IMe2IiNQbv3BiK9L+n/HLoqCc6m6ewu/bwYQ54cYKc3zpeH59mW+Iav08ELjBv
KRtVGgxTRDUc2UOxEJNiU5XTVrRqc6lhcmF8IhA8JguEBbjyBdvgSNKMTelgm/e3ThFrJQsl5Gig
0CmtweRjsBF5JIY6rbGtNVz/5ZzePw5mERFJL8p+M3yYzuextL9XUYTl/coQ7Df6a22biNOrp2DQ
+SO5R9CbfZxyyljTq6eYfSN5kLuSWe/660VIe087uEFIsnyK53BvD51NSWPYcVoNow3rlDBg9+KW
g7n13ospjNg7GaJG/gVTTqETLLyJnpw2n8mbRbIrwqf5QkP4hvMRMnwZ+mMGPGGAjMslA+vCT942
DaQjG5jyJ3JHMWS06S3+3xj4yDoTrdP00RzRHQzkSqeRPoZpgZM97SkgHXoi1+ouQU6CeCk2w0up
LLNOf/Jiqu7FYXpsXHH8n3xTvmsAKW1wasAN4PhgLMpSrS5CbK57muzCNXcQIxGCGTl/sEICWGaN
h9D5PgYp74NyezODL48/LPuknSQ5AqhX4cmvlbHsvmIbNNyexdXSwJ5oeexF2UYRWJWlMOW16EZK
mkdZd/Yj0SuhfmJk6NBYmCNLvxUZa+R8DbnEanvAXFw1tEVwzh/PxRscgkma5B9i5b7mGINRk6JQ
A4Qi+TJUP10rn9Ncr0c8tEOb/6uZGmNkKIcWzcWLI0z5tuZnoojmdYfcLpwgjGwAyKiRyBT19z5w
aOTPzKIx+5NaGu4p3lkTjZT3kfky95YuvdtKtxcXMF5nhHl4aN2Mwa2aPG9ER8o/QqEfiv0B4z8U
O/FzKKGOnGc2p0gvpRy3TO4O6mxJvzfwkhKRxXFDLM5LleiWHyLEtvWuju1v5l5w9teUPz8/8kk5
Hexz1Q/+7HHxlJGjMIEcoHROMcmORRtNYP5p0thfAx+cXZTpZFHuRUdHFrjuLCMyhKzoBZCF6Lkv
eJMTsBTX0fGWiTxRX3/YFq8+j2omOtFynThTkjjD5f670cYwzbrc1bQ/ETYptkWjB8h968uxN5AN
Kt97At1vG/fFdSDsnlqunjmnsw4oEaHjKRHh6RXLfn0IhcWFCCkCDDT9KVFMVW6Qi62Hz5sy5k8x
yIMqhrvqQs5qiJr46UOcosrDM85T+WTEcnXKCKpWIJKtoSsDrxAHrQ67ooY/zegFo8yVj3Uj6Rhw
/ju5n1polAT2GjS54G6+cmIjmcZllsIck3f2Ex6mP5LoG1lzo1qF+BOALE2ZstyvOoFngzeFyMuB
myhIGttqfY0t2YlTe09zs5er4L94D40oJaceAenu5yrG4hL5oJdj5ibg5qCjOghZZUjI+w9GXyaO
xMlz7+bzJ9M9Dq5Hk0aRBnO1GP+8hlGUc7v36rXYFD8FCRVERch/zCtSPbwwgLGoTiWDlfyaDkgF
fR++K6aq+iyzcOJWCGkDSuTScPzNzcu8z2yal5HoBLgPcU6FujfEllDDonlzdGjYWL6gdHhe36zq
hVeO67uJQMQTAnZ1LxT/lI/AHCMvlCkXh+YhTqC4KBxiZhZQpdFAhuhNHEGSJqQ9q+RJdHT3CRb3
5jNelYhFdlT7B/o2JyA1QjCzG+osgHoXtL0usArgtHYozve+FCQMRLmI0hy4tFMU46g5STxBE7ew
AmobymBzwG/SlcYNjA5yCBPE3LVkLuJij4+UUH8LboEae4fvk4vlnLl/MFw1Sog3GbAcrolkdBZ3
ytk709krj5fi+0mxrvLsLgGYPTnhmIDvhY10cr9df+bJJEKsFqiP8QMrUl6074dBEPBq7CJ0pOSt
NGdBqYQ0NZLDHcX9/XI2GVxfPcnNc+pB+07CJCIKVFFjvYXz6QZNFNKe2u37PrrdeBjDlaytCc9/
2saRRJs+dacmuMfZVidYhu/7jzRKX3nTj1j7iLVa11adFNgLLmz16priXcHUUnnLQL2G8XWyAePi
uiL137S6K14yRsK2w9l0QULat6cnZk7JPBdor6xhlHMH9tcQHmL2fmslkRMs0g6KTU24M87jgYCz
Pp7OsqMTwRESeEnShVdr3btbR0Kv2ylkp3Gzr1bQgmIIOZ6TlQwx78MKtLP2Mnz4F2uYmBw802hp
yMQhhL57gWolPMT2bp8PELuxeWlvgd5q14icjBqT4kkFJZJvtpscOqujCe8AlkvIRTr/wNPlQi5Q
zTFtbJHDeYe9WY4YnH/8uUa6KlpYtPdXNcbcQcptMHMWXxR/H+2+aoqse4VCctosz+b2x2AnBq50
C/bYTTCQ97NWwQFy/nVMF8uLqytWyYB+GvYbPy/JIEnZcj+U3hW+U1G6i/zsVcV+giWxWasIHon6
wvgm1bXNRVbO/+c5sKrl8O1mylsuK+uv390M2GjEcDSgJh5h6QBcRcN2kGDhDwh3oj2mh5pYi7Z9
amjDHYouR5bmApXPVtmpf2wQJCNduXv6Lmq95coVl26D8AA+j/ZxR30hdCzuB82Wmz4/lmnON8By
4wPGMTikjxk/KR2bK2mUMgwQvoRdW+d4efLR8Tw/wO3UMDa0ICrWen2DuVtENxvUtADFgthtp1A4
UrjTS94mraIsOLZKOHP3LSBgFzmn5AWc5wfcnoTUzjmV3EEBBNe9Y9QHa5Oi/n45tWhvdv55gKXF
1mCbA/f1Jx2zXexoSM5Ng/2amJt+rGhHxn19ycIL7lZmaW1yNtE+tT85xnDcRTLXyRdoHFz51gb/
D29s7tdaFqJ/SDVk8ruPWABcoMV6MWn56lELUYwgvH+r8b8exRMiMJXmZ0kA11c0b8XF2zF966a5
X61UyBaCqGNRxNtncH7nqHrqmBYqrdApUOPgiQmtpLcazGw7w06juzBDoPbbI5Uosixq932KNNY4
IGXi9GIdEq96/oFe3F7xmHYP2/Pj3WF6sgCcm7SPHZVLP3wYQCYPHuVUsXIJesETViiI7gB7QATF
jPxRS/S6TS6DqoLuUAj7RvzorL7wTRzQjcaHpb/X3xSHZSrOfNjl7g6gbyJQ/S5AytT1JO7UW3Vh
rO8RP8GffHQCjY+F/KlU0+lXqlCLwoKYZAwmdtgM3jpKfY2x4/oERzB24SO399+w2bpVquLB76+4
bhL7d3HskCZfjM57bnFmc8qUdcbobP7+j6GNqZG/23i8GR5Tx9a4GJM9q7A39kEVLseGKb13n48i
b8ajXLukKrG1dihswUffosWXW2/JsbNT2RmNiVgXijuigUt/7xjwv+CV/2q07Q7bbDf98K+StFJp
1OWRwWub6Ex/zPipf3TG+1Qt5ilgvGiOe2wd7hOMUBnknNFryu9UBBgAE+Yw7Ry6CUS9p11rC0ZX
hZBjnXQA/8OPdkLyIZ/GzIbC7/MdCxJWKTU1jh4hULhdExmncvhXUEvkL3SYj93FDBAgPIDs70YS
3bprze5JzPgxMD+JbbBTSvXdvPubUftWPT611PeIrF+P/QAsOIK5iaPSv13Ajubmlghz0/JaYaRC
H+AmyD3YfY14rTrkJ4iDsrw8bXAyJLwV8o85I0F5ZQbF/PHviaUTaqKs7WBWJeCT2NUqo5QXX5Ub
hVqyLUr6yyr/Q4ON1eIUyB+4cLB2S8e/EdH5YtbBnID9eOy6vH/YNGbMDzEXPF0dyjYQto1CFYlq
Vc8X21uja1AQDonH4MxERP66+O4R+EoJXAs3HpPBxqglv2VvvHXY1tOkyv4LutGW/KF1g8B3d4Il
XERfZGNbkcDnhlcWmYoD89DI2sdtDQb7KfsnHAEUx1sKXiGxekCEv1U7iegEFwbkeK3CfETjbNx8
9GSqdN5e3qzqniUkIwtqETOvGbAujN5qLRylpmltDBZi0DztP6ZkWi4qG3YsMR3pqKPwkbXPRsbR
jckmoJQ6sRz8tcRmnSWfiHlDcUsypZRZFu9y6/AP7QqBaqmw1Ohgqpootlgwa1KqJcRsHfiXJdwy
BZPCMGs+vLbdsVKpOtPaM0EEEt5nc8OkrC7JtNTj1vAOiS6ySQchtNBiOoQC+VPnzGUFoPlIfJg5
h9K6NiGXidw2lTTApZe+BdINIC93PZK7zQ/kisf0NQdTlUUHAswHLvxcUP6AE16HQfhVE4G6KC4Y
15VOpZfuQwRYfRi6eDdBbMLthxNEB/GJECjvNL+A5T4yIhMKpuI4/nSMq+vEvE7cXUZHQvw0yRiM
62gsbv2fU67CwIErXSJsv4hqblzB6ZS243YqG4kqU0pYqzyqBMN+t5xq8GaddOg9iVYK0GoOzejL
s6YboES5jsofS/lbSj3zS+cTiSdFt6xuyYi3WcByCVzPn5OnHuC9RudvFwbgq8GoyCdamF21xgzc
/z9lnbGfJXyoupz5R2IKEcvvj1SVY2j57p+Bexcsy47ZxbVeoPpmN18iSK2lB84ZGxiD6cnBp1+h
bUJJS+vfhI2oKnb/jsgh7l6gH19+4RFUz3arUwFUqHm1MO2fRJ60tBxfM7QEBl9ruZyPCSS66rER
a1CX/5ufA/ZF8t38hDzK5Bc02PFZ5oiXVfnpRXS5cZW7qk+gD/fq6iGXoQZFCSyVkApPnXXlTBAh
bUawCf6rEE9+BIB40sH25wqWT7SUW9/B7Zz9VD07QMebHKe+fJW6cLtsEH6AB+R2qjdGPSvD81UU
cjpPhgK5X926IXHXoYb9rzYE5Qm5mDeOuIMqBrEXD/e5n6bMouH9GdiTLgaCUsqMCYuKmbbtadqK
ZA+/JA3LWjviYGgN3toaVuqMIt2oynQ1VRYMePwceEWyvfRdIYjmGwFKKks4Q0ECD1UmichfmIF7
BYGn48hwocaCZEQ51UgEltOcK5FiNIi7sduMJ6IYzkNMNF7uNBWjq+V9NtvxrKA8ww0JTjGYp/rh
oSHBvAe37EW+/0mzLiFHsHvTBtO1Zu7KvWNF5vJ8QI6R/Qmndh8fwCnMumdA94oOmez4bn6terIa
bG+CWfvB4thQtCz3cfqGZUxIypaIec8FSOWwGdJJX7RJi7PHKFgPIL/uEIi7HcwNBESmFuOaoDS/
/JDOHaH6MdvFAXqKZYE/6BNEzBjHjg8CUQjifyTgZATDboiVGNs9fpFbi4+1eFhK3MB5th2vD4dB
dJMTCVrL8IxQF5X+xpCqx/3qXYV2yRUuWWiQsdjoVWlU8DKWM3t61jK9eG2Wb4++yBG5Zha7c3/H
TVj5cV3/eJ5xQU50Dh401HD1tYpOR+wzg/8MZl3xSYr6u8FFxoz4dWjca1UiyfmgUGyZrP51SJko
/Ib2ZxK9itIocicET0bdM5BY5HJZCarKAfnKX4bTqRQlVSq/C4IeyweDx92qXLaL/PODkbb6Mp8W
6iIY3yQmR0hRO5F550OtkHh8QnuzOD+eqTarUqL3M/ao4jG/RjYpgyuadD+zL56LWSrGAv7Fgd5e
Ks7SWEKYhJNbpWFvidmJpculAYuc8xaoSPSTqnc7y/P52646HP7igq+NY1vRNUByEtdj9iSAOJQv
9EmpEPPPSw8WHt8RSHjoKkPUHIcBXh/aPBLO4vq/DpI+KNlzK0hOxKVvtGiAJzeHuthgGmcbfHam
SsICMcJw9ASk3kbQFvt3jgdDro+8dYOuoCYnc8XSlPZ81o1KD2eWU1duh60MVp+uygrc5/wWFasL
6N2U8Oh2+HraOaqObhAq6UoxCLRZAJYPhNB0CJ9YQOhOBiXTSN6DxdGUtcU/UVNWt2rqxU1GYaqg
oDeKNk1Yf6QJg0X3myz7TOfFXftD5hqi6MrJD4I2yosgij5tupaJ29NnIstq5BOJ5tndNzyzKaDG
ZqQs6MBuEyWAxEvDWFuvXuKlugKRWtdezhjKOvakgAyeRnKPnoWG2tkk24VPXyxzHquCuRWR+4VH
vw1xVssPtvvS3CXeoonhzVJRegvg0e4Ccm0CzEIyHCV52Ay7TY3UfryM5aevXd3v+hCz2Bh5YJGu
GhUwBTjUBNP+c9/C7KJenbUWVyRowLjieDxWSKE2ilP0QztIV1vVtE3S3A7z+8ScFste2UcMTnCI
/K1OUCshgaJI3+Q7FTo8e4njYEo1oVzkfOQiKtzynplW8Zyr5T83I4Gfv94breGBdUOFJxhZMqjo
TO5lspqbvPTCvWRra2L44BoATcDpqt3pSkTmwG1o1UhLiYjxTIvz29D8KaA3FSj+oEbHxLT5vb0G
gXMkuPi6jtY89G3JWIxATtd6FiqAvN0rxbJwoLM8ZktE58VuN3uWL39OIeZFsZKR0xf9gc/3a0xt
UWKIY4XlvXohMQAhzbSoVz5I+r+XYRkRogUfL/C6ULeJT1g3i2Zl9fztoRqzLtqaEvFgKnTyqe7U
bFMJNoWVJ16rwhV6GFiNEYVJwS6CzOb5aIBF2Kc2aj881kVV4ru6VHezt9e7qx7GDRNDUgwcMlkF
99LZSdNYoqtplyn0PUXIdJQK+Y2p2md0v9RLbC70YMwhiewL80KkxJUIsTeeLzQfNUtqNpK5K/0s
nhw+rLUQNolhaTvLrI2XwpH626GvS13PV8v1mWO3M3qdUrUpiSZVZzG6IE85hlPWa5e/4JosLgTs
NrNW6wEu+Ji/AzR87Wumd9z09ECPHG58OdbzP4DqvGe8l86hFDihsumVSSZtYqiIwfPMR+6XtLHG
YNYv8s3+CqN/xP7llcsU1U/u4OZZ4Ydpm47oWdt7hAqEqujtd2SN9CVu9+7jVYRU/KKfYeZcLR8X
JNEhVKPmFnAuConmQwfGz0W6HC3RbGWaJDmQOTje8rkY+NiJ4cUJ2+Sy/7XK+HqIMrmnvI9D5Maj
dFhKJSf7lgmUXKLcOAaS9YwUNzVtURy6Tgp3J200L+XvFIdO6FByGDQCh5+1IfmxU6JRzX32zgeO
n1eGMBetBWX3lsuQJARE4px6UmkHlT96yZAcLmZeKlSyTsX2vgED8QZ6Kwa0llhrPtpiZOBT3WmR
iW3GLgMZDQwbM9MaS/t+pMcwXg/L5+aWUnQzAdwqZ0gRFVUzTRuYufRpxBGy2ySkc4w9lURQslHP
7g0RA5jrD0qbnOW6Sdgvyp/0TMZ+U+VuZqWSzAi/FvW0KAc13CSm/a5z5DQCxPMXFxYkH+4JKYUu
Odq/cMatWkeGOb2HCGgfXq169QpjDoamUCM9eE/JmJP0e/Uam108WEMVKwlWqHO2SkXgVaMQoXo2
jmBUpWwUNfdDfJiOJjLg8dqrFEYTaGwnpIaEXMre/vG4tIQFJv9gGc5/fEIKQtzN2AsFGFl7u2vo
iHJxw/qxy7Uv3VzDkTj40qw6E7NpufO3Jl3DkW0IcL9300dkGLjYsisc2crCjwvgLjFMChmeOxcx
vr3pVCeggzM6xfKHTAMVgtKF2wfRn5oYH0ZiJRVLk6DcvxxNwVubUdAx9VFHc+zEuCA7TsRIyPRb
/Ud0qJjQ9rZ/X3W1xvWTon6UkxZ8fjFLVUAzImNjSAAwHBmoAuOUOPg8Oj9FhcpYTFZmX3q7dfzd
FYLP8U0CXcHP/XUvRYx9p+3DY2sg6JwJmJSaojT+ZdiC1j70gqZMuwHtozFlSNZFFinMxe5PzEL0
WCm1wWRonLCZLS+d2/ykvRuIqgI0Iq6jGdipuISoTZlUfA+GgMUT9nj/L2APW9o2movDZq3V6QVN
ZT5SGhEjFACEwLdaMqX1/cJOTAWwhDDrqHESRnZGAG2GgmX6GcYb3QuE+Va0AHDuLKg3KUBNFxZu
X0LTZwOnP2V8VBojdIso6kIM03bSZtYI4+w7F65HSl/z23nwZVIs3rJF97MlayH7v5nhDF7c8VKF
woqlUVB7li8lA2oX5xN+aTle3MlfypGvVREne4chjBn/LgWBxiI6SCcvOvkBmATO/TLUuQALHHKF
dcCLg5VvBOqA8mh8N99YxxEpJ51rf/edj/5uv63uqP7qhGACIFTerCx+svL9ekxjLuW8DBVy70PB
ZDNqaq2iZWAiEGQOQwthqmGfKZ7rUFEcnO0iNV4IWAXeKCGD0WzmzkRgezj/KQaC1fUCR8IFGqr9
BRuxwbleMdbRB1avqZWNuA4B3PxWg8PzrpgI4G2KF4PToLtl6GrDgemb+0JNe4L3two5VR62RbHY
1gIPsflHw1KckO0uQ9T+Zs0Yfg08EFs8ppl6O37K8vEwroCFch/8BIJLqmsrPmrRJoetzpf4F9ZA
D0IaNgDZpve8anJdioOqP1Fn73ewK3wShdDaD/OHFX95IyNzKwVvY+BoUIGDYBKT9AHs2cMEai3P
OcBOfY7rt+v56qIll68Lx1mvc0TQpuwudFyvFGOrUHw/w5l966bOELjRZOZPyD+g8Rb4k2UNicG5
ufdQgTpgiMUzOwbWCVDqQxKkJ7mVniaBO5H0cEJ6K/ciZpqAyNCg1Ohtk5bfzmzD/2wi7rINzptA
PW1HrAPxJbpOT7DblSxyEuWEfPAaYECZWJwAw+DcyawXlsFJU0+rEDfqjHDA76DhXfWmTMTkJ7R/
0mQK6HA+Io7XCcOpRRKgEVwjaxnqxy5AqNFXDOPeXwbgm0yEtO1+gI8azA51n4NbMmKRtd4Dooq6
JjqPKTC/AXqMjWxsvmYyr8pJ/xbgGq6pqS5qxVG++xc+MHao5kEf8DPeed2utAn3zLzkqSB1P6GK
hq/nzMoCT8027dOt1mi4vbuHrR5riCQlXjIZOUZJZrPR/lnBV8FaNcQX2/FKAyN3o4JKGq9fntNi
ziodAh86RiTLcHuQUNiizOeg9gJQVQkixsnMq2NOv5S1DFVMz5Q+VJU0Z/0T2HKnqpTQCBYxdZLz
rIs9P3pBvlGJKCM0CARhSKiGVUO8ibrq/fdYQGACBk/SrMu89t4T2EkmPzV3RGqCuYQxOiL7SqCE
ZFnNqSos7K528IWhU0aavzP9w8R5YqJUJHG+LKlPE7U47oEr4J8Xr4orhzJUydn/QC/ipsYlLxzq
FPPdsKtrYlfy4one/cHlaY7MZcd9eeBRf9NoYHF9uzNO4qSDGbljoefY9qKZLw6L5cDf4w3XNrOp
7FXOHLU5IrW1pvJQmjjGmH+L01JUPirYd4Hb3qMx0jkhH5wkPdIfWel2H/Zk709bs3Qac1lJDiji
GHkFWtn7bFR3X/8EgP0sQ26UwPiwSUeZA8VZhcVHkgIlcoKCoz4pMJjqkGcnDcsiuhazijlnvJ37
rjcGZziEjCexJs0q2bpJDGR0aG7JpZexzty1/0WoaWFv6CO4b2naNUwQKxEi+HmVRe0edDnmsqqi
4EBBZfGWp4a5HhY9LXFCijkXthjrni7LuZUgpfAzoXIR5/QvUCbkSKTYR/4GEQiucChlGidnAZgL
M3mtUzgjrVcnwYfcFAGmN265mZWhUpLffa51pzCt5POZFtfFfoDxfWw588oX+3maSoKQL9uPShdP
HV7hoSHbZaAXWNnqvH6qEWpxouswD14tQqKG13JqgYd+AUuKyY4a7anHYqqrIeDZ2/SzcIB5GP7r
JAEd1ll5jrRXIfWge7cMdYTdB8FG5vc/QTPMSaLVZ+uj6UHdsfJQIQzGSLP1fN+UxZ8EFHgOloOl
nlI8b9QONaFjYVfnSoRC761hJJlcZMfLqKb1yJFi+VmcMKzdJMLJHK/Tjmi6+9tptFThKUhfHNPy
IY0+JBvty4DZ7rrFv04bPjXbL+DjZR5HQtnSIWaWSj6X0CIWWtxwY7SHd+C6Nf3qJgLJMf2JzXKz
51oZvMDokx3uGRuysgtlZuqN4RgQLb115NScJ90DH/hoXgCsty4CvpsWkeL1jEz+iX+6nmGRNgxZ
EmVws5htg7Ze7K2THCJeQ58Kl2ZMh0AGJ+c2AFxDQjEikR3aV85ox/2c+lZbroY3gtARCc1qEf0o
1EyL107N0xiRVT/Tvynl8UPbt3GXN3LVkRdS/H/AGC+IGZUVGhDi+RPuoNxMju1df/OY4GBRtHy7
93xV9o4cDeONDgX0kH/ngddDpVSvJBMH4/4OgzvdjEaX2q4kHrXXl2VrRrKxEWfNBzslbRr3cPds
SDv9oMvgTJKQJZ2l+FDQIeXaI0axeo7bzyBULqeKjvEEiUhQX8Ez9yv9NiXw9bvZMDBTOMwxvWW8
uU+DKPg8ZqJBBfmlc5IozK13PpuqIjD1K5GjwdtRXPgVmkYVT6jUojWrevGY4R7Squu/DgwmNUvC
MYvkmHvWCC4Ckxoxs2gIDx01ph5IgyG/nP5hTTWySiUX/OiDIN8MGfIXGATrub6EsFaBXi/16otq
T9a2f8vg33FkJCm7EtOvN2Ud0Qojv9AXpuF6/XqjNYWvbtdpz0JwOfN1dSnRSVtDbQb5VPTKPD38
SMf1py1q+Cs9oTjrcHeUnNzt5qjJSU6dFwSHn3JCjg1nRNaiL6dxSEhu3OSviciwABo/1m4NwC/l
jnCHQ2+fr8EDrp4bLIIwVM3EdcTlJGU3wcafezd/qE/zacROgjCeCbRxLivGfCB848j3STykSlcC
EI66EdwpbQt+LrubjZuLOeAHDKmCK7vjW39f2hUTHKDiyo52P26LoBOxE39NkIU8ASgvS1ExKYZE
D7UWJQETY7KGvOV5/BKScc12x/i+y4Kfj1cPcet28A5K+Xdnnt1OaP0Vn5yq5PIyvHx02Tf1CtFW
EQBnKhknpYFImgV77aGKsep0Sui16jFjxJqdNwXolG4PYdnVgYc9VP7c+zGCND29m4EI+/g3SSao
GKONcXwMngSJ90WGrVeVPSbCl5bATLbQZqhdfRGvrBrHb7HjOllhbMY2XmI4XlkOEGnRdQv14ksB
IAcrp4fTvtFx2Aka4Whf3MRLi+u1vd6HDpdTJC3XixAAIUCF/db1NzlJlJM6lM0Ps7rcJM3WLOCR
/PQyHTDrXGYbDMthSLx2fuEiNjxDUGihSyi7QcghOS45xz9+BXQnfB/AJIpWhOU8gH0DtfffkJPH
TM+j6RgMndakx2rfdJKn7gd+i+gtpseqsQlSFWTD35QDknvB8oL0NOh4nxjQg/ShBZQD+Rf9kgCa
imQ0S+ZtZ4KI2aS7caVbXKderGr2L/0tNmyIBBNVkuyqlsMk/IVmPgKp1ii1a0XxgTVhE/ecBYbI
GxQiEyi1EZX/h8+G1M8MnYGlAJkmJqM9PrLuyyNXXvynMHs0KbN6bB0mu/Zrn0Ym1k6D6l5uHQfN
GR3e5YuV4pvxOH3ptRq0F4H+24xJA8GLdyghnSGBoqqQqX5de8+E8GV2YyF6+pbL6DC89vzHaaIA
ukgrF9UrhBZ/BpygByRUHdNmUai2spmncmjz+qgDfVu9wjsSJUYbU2xmGi+UGQPprOC2t97jixTt
CAeWaIYovE1msBPACYmnxCjX7k+xvT4ZZ0VsZoW99cRC3MLhVDTzokWesEZfm1AFtcti0qxGJYZ0
EKJNH6HagPY7A4MB+1gqj6d/Iu1m6oBMRNr3WpLne2wBhWmETz+zTivT6qSELz3qxURWwabMd1Ul
1x38Heh+OulzZZmia/e6fVtVobeug/Qy+JTvP0XuqB9Y7E14Npdb2DUQceRm87+40Jqvb8eoAuCM
DuyXI3q9GjjKBOJvr6AT2qoRxCjji6qFdb6Gz04ZWnvmIKdxWdDyAPulxzCle3TaVrXDVLwfVkVW
fmH53StsDGad6okgnL34NhmxXER8l4UIoMD/+vEl5Lk7oAuRmsUxuOXHwmA7vlcSejdfwUPE3PBH
qVXXhufd61qSNEBc4AVJFrp6XsHpTVnndlCsVe0/xnjA7efAeYtjpkjQ/+XnHJeuRAz9YW/OPdSw
b2iCQC7rYhrjfaQwu1sL1kM0TpG0/4W/wu2U5NaoCw3rf2GcomATZjeE+9kxGSQybCayPaPlPYLe
tDVU66+yVItiBN8DAdIzTYcTVFkdG6bWGZ38nOCzGQUPjA1a5JI7l3QjfqqKgC5PGT3T0lsAzEH1
9IioCpMPcxeoarxQXc2EALAOWssNLKXKQnQsGyTD3iWfzboIfwL7DhTJ3hhC0mxTMC0Dc1lUSZvY
wvFiG3jzh8OOnHj/4tVnNME9PDPh3bZYZQSrym5MwhD7WV5nDKZNMbfNU5Ai6Dj4wW3x8ALxzu4X
eMim1nsDB62dvf27D8fKIhHJAq+XN/iQpyWCMCPXiTEL1GlGB7pdRAIAMoo6CUuB5dYHSCRua6wi
OpD24yDulQcEQZQvqecx8cs9ABhBMyRZKk5qHWUibhgloLESUlTjnfGzhug06FFf/fT8jMmoJRjv
7sTRRMqNaCFte1FnUOfxxM9mh/H9Xcx2QnTYmGbk2SD+7aXVNIb0fkg+GuSzl/PWR9atcBf1yPdJ
LHW3vO+aFnIDkkM+85ngaDAjP6HCE0dKtyxXlkvZWc6qgaQBrgwdTGBTKfnE9gL8qr7Bzw1rzZDa
4J4uLi3hBQCNirKuyWYdgeVz5Owfs5MNr1XqaUeOibVAOt4AnjcEk/UDvLMrYoQtS6Giy/mammlt
HDTcfEkDVvjVr1XQ35NS56LbppPtcklxrFXJTfDjIC3wGi2J9vOeXevA5zBgtDbBi+8hoMebJWH5
331tbA2HIE0XONqAocKES1GCZodlG0eaHai1zLwNEM0zMEFuScCG6XdJiBJxxeLoNQ23ZTYDR6PV
pVhmEkBTIKKWF3S6KiJoKiOoNCbqvzLlHGsm0pphO3c+068XJxMw/QfCGk/o/KKlEPxAuCfvoSfq
vN/qVxcP1Zy+gY6sDbm8RmZzSEmFiikezj9c8mYKnzhiwta5vGgze0JUbpTxQ8kIqdesVJhFfLV8
ZHgS0mbWeR8v8QBXK6aBIQTS8XqiJQLlRBJehnL83EhYet7ZeTVDYEdY4OLT8cFQhN5XYsy54pQJ
k5vPel5OcqEk54JGNJRWe2WIbj4gQKgDa5xQD367xEM6kyqE5tGcXGQUR/0gqaVl0B5pCL3nPYw7
xQzRubfdd3s9lGLqViLaL/7vOPOfK+6A6UkT60jlumacAtSMH6P+3gm1yRfgwwERZME+uAC7LMWb
/vHIt9srGj0bazeJDoP9zlzwNN/WW2cLryIeUH0Cm6ncjuJIXp9Em+Odc/Iyb0eYsRZ61Zde0kzR
lwcl00o4A7zeFoUfp4VFvq0VP8ODTrc0W1YF5ZOAfiGgWL1LmSyzkERCP1zabOGmUIZRsq/D9kDN
zh1uTuoqzfc4GJRX2FXd5/jwaSC2UC42f8nn8mJcmFyDNZ0rVQVGeryoufEFlQ3PqCycqcXDZwmW
KpnVauNgr0v8wtkNKpcOKeyBdQ57A2gSbwAZhdr9YIJ9k0AC12Jb6zkvSde2t2oVPPLTpSUMCumO
P/pAoZ4OKHZrdHZes2QE9RlvOPoLun4DToFbX1tqNWagXQvrU1Ia0OEUz/NCifblQMniu/1YdhJ9
D/n3fDyTHqVNsAUmNlGhxb6pa0u8RGuBr7fTeOEbc9dYHJtBIaeX7t/57pfhx2zQMl8bFnd0cI4V
GdA46ImNVox/Xy1eV0ih9uLFvw6l9Af5nw/qxJ8GT/RXdVBN+tGDXi4fDh2npa09W8fmFmbjUk7X
IxbJYMXv3dFEQeLtStntwdKYX+geAQQmz1fF/kVp4LfhZRYf/e7en2h8nAnvJXtLweiC2DveobrO
115o4eJT3NZvtTo+1BUSz0nw3oxQ0Agm+L0OQ0iHWHuiFJRjJm+X5b4eeiN/9zYun6ovV+lnjY7i
CWV9+euQhVW2iD8P0bT0dp1mkNHcxvxmUj0nY7H8RF/ZF8thUIc40qwkQ/d5UOK5Nsw2Ic1WWnOh
7YDNLNJ+3zjhaAG0fPdFZz5cJ6hbVIjBRSj78RT8DBFIy7Ckdh81v8JDcT4UtWvqvS4cqjqcpZkt
J1Y9BJCs8ISQJ0nmQrP5ZmuMbz8D+uE3wTFtFdGU+qpNF2HuopLK26AdVLamYYt3S4OvGLgk48qR
XnwH8FWxnPbEFnUfGgj1ZW18oPnQMQNU/n4Y8n5D0nXeFq4PDeYgUQC4UFonMmbUp65lmBinF+7r
6FTdfraHoUe4UCr5p3C16JsGlORSEx5qIXdCiLBhhnALUv1zNpJZOifWaYAiaRGpDhun2pMNyuaB
V+BU8fFX0BrYTaR1ftGPlztEt6qO1DSJWi7QTDsS4Qc58c2lfdyq4xyaxduMlMPyGDI239PM/AlO
jRKLZjIkiiqXANoG8Z4cpV7F6UcRNGpUa2hp7zm6GNw3M/d3NG9qsdQXh/RKgXXUcJkhi65y+nHS
RyVrC1qrPI7dt0eMlFEA6+/EcXZjjeQFrcgnc0AQGYb0MwWStXtzNgkAb507ED3fyDyUC2HU32NR
c3Qhz9D0Uz6aqlaUbkYKBgDKLb1kxIG+GUdiUAkc+OBhz+SHYbqvk5YUTcrrfK1LJqWNRJIu7Rlu
mlzvfraoLPJv8wZhBEs8dFZI/443tbxZm9cVdK/ud4rmrraJOPsK/hwc/Guwobt+vBln8R91rL7n
ZZ22RtIeBmlcKX+UF5aF+1HIM/ZkgsEUJAJzK5qhuYM6iDZMhrzZvhNeRS01Bo8E+i9448KRxGrR
WzsHZS8edVQwCzLmooVq763L3dW4UBXjAOuwGrTOmD8gDFz0fULKXSU9nmC1tVmBxiWBlWYZDENN
PavgjtyWkh9EimIloMyOWhqhEvh5CMeW6R4C7GOm+D9s8eKV8C629qIngWATFVEd58KfGuXi03LP
sL8c+GaChXDKdcWGsicmu/QCG2Jn1MtjnGA15Onoi9pxSHRGB8TLRGMT0pWN2b/Fr6rV97eDRDRQ
/ktcHtIDgilXJHu2+YogGBIo2pHxop79xWtxoAXYRgmiuAb7Y/cI8s/L5PfBUowkp/oxoHLHeE7o
UkQ/Sx154T5mI6A/A7h06Y9c3g29CJ1FMexB2iE/ZSfPXtK9SXBk1MLcWg9TmggLIJCYnFSfRQ2X
JMbg0dL/j2o0W83RpnHLMV83k/xM2j8ZvZHqJZLNMDwnXjq1yCQr7idrJAGlMqjeHuMg+upP6v8f
Ka/SQMztlUVhk9jIpMeWlbidt5jN82O/hXLvGbPVM9bnNXiMR1vWGImO32djeRLkYKXJERVlpMQE
NOpnDkjIui4bGkj1DQG2c1XDZ27ESavAjTFcAf8nJrcCynoFfgbz7BQr2Z2ueeJ1M99i3H0xtWQ3
89Pk99MNsICLfMxOo7Cl3wNtzsHAMGL7TmJdnO8R7KXDIEJx9GJ2gpAs1GXvI0Fw2iOMKXf7rWwj
UM+Jaq1yOBZ96EgrD05vMOYo4QQBebtdKuECnftTzZ8RjlEIW12hqRMLs/etEbRkAgyKv/nmEGhp
1Gs+SJgR5qBepUtn/qmwJ+99wzr15ZN6w/J19iIX54ZJuQkDirRDnuKztgLy3HcTFFZoujMJhQcK
0vv3ZICqC3V0nt8e2c28YhexpgaDjcqlW4J7W3afEsOzgh12CNIffnyFqiZIxjmblt9G7FqMIQ/6
sXNpou3pghZs76/QZcDCg7k6uYJBH+zhe/G4Hkufu/568KRHn7eqGqE3iWQ+C3CL6fQD9f8bPqfA
YfOuZrGAdOb4M+9aI5gEisYZzgIzqHg6+YuUkI3w1lQ5k0DFTLD/pyd/q8ttaL/Lv49IHYm/Rpsn
symKEJH9VMmgVHQmwkuwCDlbzTrELgxaMfnIZOdyhVBMXLayAfpL6/UPmBqTTsN2JhpqPLU3amq5
jJHGXynnUQgvCykHYke1eQc4OtMe46aUoyZXpsKNr90yWN3ISPRiVPo0uRxr/DzKC3+7hHKYWbcK
GPYKQI3U6eGmUm3ALF9FXeAFbczDyGL7fPPojU76W6F/dzufy2E0Bv1cRfTj4jNq8+3iLx6WRmXC
InawuLi31zqh9fpV1c3xlynyAE/S0zRUG5jnKn/Gyhm9pwza4aiKMo7gRXpDz5UdjJrf+na6MH7x
VU/RkigvIear5dgPHqNVc12juZjm89rPZptd9EcuxrSZnYTXfjXxd9YqU74qh564TkZG6btGVDDs
KAJxUZdlzOqM59MSnoHj3fqrXK7EL66XQyi0EyGx6Q+5Sue06P4Wjxp3wwRaPPFJvV3Dv4cCi/hL
SMccqpteoox3R3Qh2HQXqEbXTXUN0k2TzuaYIeATqPEHEamnPxCePCrbMfGXay9VuaPPj8cZPpOz
raVgT72owN7IVpOy/4FYOTdXECkYgo2qnn4ZyqUC0Bip8/Wk6dtEtbzNEoBtxUrCSi1CcnGYMe9I
uUqAZOt3i5IZqcCx5qO1Ppe+dk1cFDDEQcfsci2pqIXsVQ07IRHt7JFUUqkr+By5bfhoA6PA7k7M
sz6daEyskHLO6c04OJuZRFJT3IHlRSfWbbgNqZeIdAOzXdIaK7AISM9/gbHl18reZ8afIKbHnOq4
KJwIO1oJmeuyegqChBVmMYTDebqYFzR5460GTg53vSi3IpBn7g9uv9iuQfZX51t42n71SxfcmKjQ
Ch+MDp8QhbxSBvjWygEBFnI14flLPbXT9vDfevtkN7mELiHi0lAGpvpCQYdwEKgex0HhEQ/agTA0
x6t8Ekf3ImLe+LLmnpml4dziGVvpkYIXJqzB39xy5IKuETLwVuVRa9P3Y7g84VL7fogghhjBIU9R
m3D0SIeDUbftePv0sIlftHQvHfob0NA7Ek5que/K3Ohb3kzPicwEhwPPZHZ5e2pfAMptpNamhqpK
9iE8EwG5dxY9eGhZbu9naAedNLFR82zJfGDMFT/uFaLhpH8zUfO530FZzhU1bgj/hs1YPydQ5tvS
Q+QHVxWq34aGb54eANpsRK/Wi5M9h/EXAr4DxaAoW6HQBIf68bvwdT7kDb4WJSi5MXHAUStX/YxP
xIVtFYDg790Gqux66P7WYeUVN8rRMpCC3wAY5Bhh5qPdVwwAz1HtOOUZgZFMHdSpGlTiFYKb5aBC
n9uI18RW9y84sY13gvPNeIoVBArqM5qB+9seAGxKo6Lih55Up9pR+CpHorvZEBSIXwwzUWz00B2k
PcVdkCxPB79Y5NZjzQA/+E0lS3OJZbp3wfbxogg3uIv6boc6d9ln7ZtFBo2DFESv3Wx3EVCAy1wi
MX6bzTT6udI7JQS9VfdwMYEsmjOs+/yDxERHZ1IeczQmiRTyIHmHgROOGOakt2OYXEKp8ZoMR1uc
tiRG0Dtg2+gkq3neozn9+6qaog9ZwTsPu0Dn/tvy7/6P8IrUk2PP7WJZI8ov4TXT6g21oKrVNdCH
+hfACkgCnzwLC0roHPs64EseGfggxwMPzeO58LqIgc8OGxsJW3qZWphZI8fMi2zHZN0dhgDAztQ1
EJvn7GfayLDhIXyAE4V2T+LFOwa1A1QPFhoFaMNBnPwyj9sKMk8GzwXy0s/gfPf11gyiFI+0J8Qt
v6PTChAqqFbbJdWab0nz2bI8V2MZ6/rlb+zude6GrLEqJ8ie3canY8uREC8bnUuodnpfrCqeiLVm
X8K+JluJVBGili88mcf4czenNcxA6LLP4LI0Muh1n8TbvuLSIftxHXO01jnKzkDOCGMnYNg3fcNA
3Gf0BoYxoPxIE4qguaWEebt2anyNr5pP5lF4HyUu3tGVl1k6EnA3/0IrTNaNXrpR3t5DGcwYSSDt
jcrTzXDpxH8GoUNA+JPGrSx5vMGWoo7kndt+96W/SnH+qXhZLiRbVf0Q6/mO5GdwRHsEmPf2H6xE
bJkNndg7xuITq1wxFa0IVU54wSF4VwBb86jQSDN83KavkEsT4yAtj/Z6qfNbtEWhBLz6nCcqc6qq
qlGZlaWTuc1sS9gA2ObCfzpRG0rpMSiHSukBY1k7Ys69tm3ukQw/7bGBXKOEKgJNF4N6sTvqTyvM
cuxoyGVroCsydhedNHYOVkd9Ri/lO9xmKreOGz/8amijbd7Tvs2c21PsdaA7spZ7Gf7AN8GUL2i7
Sa29FYJIQ6jUnOBGaxLWBsVrLZLzuCeO2DSa9Rkm3F5UnSNRN8ZaQMeVSLWsZagxXjP4owuk4Fbq
7GWUFGVQqPq7EWBIguz5tFhyw2qdDLkAjyK1SUitxvqc+XaqwH6m0JdLQdc6PbdfjmJSw74nUY9A
s+rifeEzMTkTTzY/AaELEAEqP4MBWJyqU3VaMjNFSvJoiXSimIvSdm77+DzZWI0maVXfLGACDYLs
Gva7ltin8vw+3eHwfpEUkxtTJxoqceGOaCilfn6yyio9GF9n2vOxnPlBqrHaHoCIIEEUi7nBYxi0
xwFjchcgd+FBIKasze1z3a7Drc8W8wvmWqWrYFPLvSvzN8XNJ7h6pTyq+ADAMUD1Ro3NnXxd8/ey
hsanfytf649s8gIHcxEw9K2cAbevNTvEOlIU5xjSbObWzRGetp8O1QXrHlB5OLul9Ui29C5YWRPl
86ba5Dng2EEjuo2EXbQV7XWcEs/8zifSRETpB320tuA/Wi3JU10eygn92KCpQg7w86KvrQceBWMV
tqVH/GpK5SOzro+z+I0iZUvVzgvL2EnaXflIJh/5l1zYpD9UaFvrjg3iQDGHBOscE3h9dd/leGZv
bmVnCZGhZSiFKGv+bilOF1LthpRHQB7Qdau8AqKrdCpoPgTdC8LcqunpTQVjeDM0HGxFmGRlPiaV
buorG0MgvMcoZ8uPpkmLsqbCNlkdEoIoYyXt5r6CkKotrE9XYUaolC3dOd0UdNW2iJL3HpsfhebP
F1S4ZmaIt+QzFvEBmzpG2BlfKR7bIn9Az9vdwgBf1Zyo/GQLaRY52Xkz2UjIEbSNgJ7TxpJxiETR
tQeb1it58wsVdX90rk/DYwLm5YNLhTfyapvvY4lAg/29fMqpM32HBs1NK1nEqdDEVYDzBo0wfOA8
Tpzmro76Qep2JL+VJ70bZQA29t1D1CgnkGV4FRA0t2MR5gQPLbvoYBlSKOQveX8FeQjZeHoHWrGQ
9M9kbQ/U0NFH1VixV63Mq72lAvls/Ep+6yc1tYEFKdCKMg6Io1YGPzivUlqW8ki+pNEAv2iFG+OR
9Aroa2rtvmhTQhSvqQgTVsiiNtUcddf4XcH2XnRywmLQE+WLZ3lGyfLwiyJvO/EJC3AHaA0gvXQx
wqK8d0zeOmzYJtEx5x9JD5WR2ZeXodnS6CA65SsFx/GVh6E7FkKFR1uhno71Oyjnh8PXqm5gRn2+
oi9HRZNDIOfphrUhy8EoPTnc26X4rOrd0LDCJi8dC3tmhOF4SIpeLl/LCKJ+ZFPftjtIu8V0d7od
ynEkEU3bAAfT74h1jtajj3RPXtEyWQvSm3ZFx6tUk17PqDKh9QzgbihT98Psva1ibsnXc+jjLOZW
hLbDywFLEwBeHOkMB8mWyzx8VRX20eGG1jpGQRwI+TIytob2ZVlAZUKrD1qW9RoM7QMHuWUI/5LX
Lsi/U7u0o3jQjsJ6YShdvxKsyVppgdZ28H7qKxxgAAfHA6JXOcMU4WaQIid+VChjbScalg7uXw8c
8ehWgxYW0+IKZrXQOr5vshPQBNQnNCuQ8NR8OlJasAP8p5HVKR1l2/iFTQYEyvmcZcY7R+GmAvpT
J5Pm/RruAg5umURj5R+h0Opp638Tq0hMhYrsaQMP0I//P8N3A3yXk4/PArTdj8VkAHdAuKjIWZbL
vYZywCj/xsQChZrKdTbNCL6ZNOeV5233/2Tr0eaJ4CEOmlNudNxiAx92SQPxQmvO4FBE6U+Dt/pc
FjRIG54CI84aTVusLJnL3p8rT+BWn4tXpBQZ5W9Ej2JJxbjYHQQtkwO+ZdE++NjFW84uV69vkCD/
eDy5Ay3dVpYOHHqLb5BbF/QUX75XiAbYkYbYnLoMVsnSy9plw3VFs+bcm0apOTHyhxWk+KEysEPc
qQ80NfCgsS7aOGKA0wQn7Oejr+c9QRY+OCpND0gR1jUvJliu6CVr/xWD/NU9Wz92OSNnkUU/tpvz
7XGQj4WdtcaDyQaqgGYBwg713dsBsMszKbaZluQR1+ieswQQqgwmqKrZtcO2IVg6Ye5ZsKi5SJNN
GAJdp5ZnCLsw4SY1cvtiN7tKCFsK1BNWIeTscXUSRF4cm7XwGZk9cygBuQFK9k2ZIpS2+3xQ2hc0
c+Dm2xJJYjoJ0R7pykWRAFQQM9stuuF+8MPZ4iSmjGaehZurq7BS9v8++zgKF55ra3zfHThKQYak
3TSSXrNSe93HFP7Iyx5n6fu6LjuVuiDxZhQfiZnyVwhAT0X6u+8dHN0oWLh8hf4gwMr71RqLpW8S
vvWT2Qg52cxyRd7OGjny9IAX+aNBSeF45zZMwIH7UvrEY6VXTAGySlxki0xnxfpnzbx5IjF2AQ1N
6LrU0EHY3YM316vEjvd7dSwsb3DBCwxzVOL+NPeSvtZfFntBgiZvXzIsIkuWwXP8v9B3fwvhGZmk
zHKZNjbw22j9OVyxs/GN6SAqci5xyTDUfJjM1FjGsTSn5Er5HLTlin1i2om5MSINaTGCfnJTNAug
cQ7YhLaLZk7bEkziJ9OCAh+nWRDsf2iUFCY+tETslErTw0tKsFBNUeDsgYkatyydHcdNZ7ZBzUGw
LiGFMcArO3ImKhtRqz9b7iJLYNBv7jlZ+Z9CWqy9h+JgzCO+7kQpa+M1oIgIAI40wJaGOmJ1hQka
EEHAaV3wh39ky8YaiAs8bbooOpp0OxvqtYYPqKwne8nfa3HPKKnX6DfceVyNAKP8j7sI0hwK4ccU
op5TZwQTcl5cLHbh5zv5ssD2XUtpYCo+F0et7vdb+PztClPR9bCBvm9ecwyzUiZY40R//zrf5kll
k6UkOBadUmtAlGnrxJIxxSdlzmZq+Pd7B2y4WbG23GC+E78/2OmlgKwpVdMJejEsTnJycBjEaFIm
nuYTuIShiT3vTxM9lVGexSTJlxay977/WCbbWY4+x0LWCA61tlvD9Nfi2hN+ROh0XHvVSt5jwSrL
N6jJIj39ipcYWvWTE6FCnVH6DY3ax+YcAdl2kw+sNwbm2I0ipUVOXyN2+bf8wbrgcfhdZB7i7Qch
sF6XpAALneT1Mm+wEK3eli8t0FUnsEyHRY+lPYCecFxon5eT3EaXaUkM0SEqWcdmg7EzRvirTB0u
8MuBoQwCbUff+3UaR/tnjRa6PnoEKQIaBOLWjIVVmU36eFRsTXdSB0sveN8KYyDXZGWvqSoTFc3d
hayBIbWTZzcS9pBwZTNy/3LKEdDVVDEwVt0fSPNl5AB2RfiPh0ZEkdwM+F0lH7SIIniIJgTg65Lg
Da1mP9P8MhqX3MkMmO+YUzqpBR6umTL0FbvutuQSU5SBMD4D7WLJi+rmu8mswzAE5GuBQ9ucwS1U
j/+du+WRAvd/Fc0oyOpCp7Fe3Cuts1jnEvs99rQwrytymyeVMeaINKBnByt6ZPSwnn7Kei9Ewqcg
5NvvhyVr0kKNGaC/Gy9ew4ECSfrxOMIijOwusHtanKsQmxTToteRwYRAB5UlzMfoOsU3lW2pVarL
0btLyYtk6aqyGuVZcEgnGyo8ENgP7Czf0QmJuJDTHiBxQ5Pj0aBaB0fPmoKIB0fb0BJ4ozesw6Iv
LtZAP5+t1cBIvZwSrWLwQ6L27hyc8VOIaEvcPnqYMet65IZ4Qa3NUPetXkJKFEsvuykVKqhrbhZJ
f3ewQVwBGTw1wD0gve7W2BwH1t1/DvZQ1Pe5y4LXUM/SbWru5pZXmgzHVkAPw4SXBXfZUIUlN9GR
2a8k8sbwsNw25hVEk5tcIhocKyD4WrTSMHoLb24YWXbEqD+T84HPSOwm3V9JOP5nhU6+PyLWCQfa
RwGgZweBPp3hIAdTELBDyZmgoMw3kc1tuGlJY4nxtrl41ntvmb0C5/W0EQb9yB5BtW1VaD4N0v0i
d8gYOiTjiHuvHBGE3T136GJ9/LFEqmCj3xR4vBubsxiWR2Usb5ZjwbgIpaWvKnA6S/ghhbdCHidi
DdkWiK2+2ouTgb5oGYOtzxQ3uGLKknHmUlwpkdy6SjDpQv4nclvPllJUbg7SmS1rTo1fmVPfDqm/
ctrse0XPTWWgbPX5+xakJrEajrBC2SbN1vEnZ697eZNW/pPMewH8LEGkL3rpfloXGcGmRExWb5qA
u+orJQ/+2aeaD/o87DFPdZzHsjtIDCfJX19mB5Ybux2fJ85NaoHCBjwFh4MiEdmzcCyWt/3yhFEw
hk3XWnRpXeA2BsT0NNQaUUC8EnCUZ5XWUyPLh2ooV1IQNqhsr5psWcL4u0VeV0FLGldwFYiSO6Cf
Aw5UDGG3P6ZdYn0wJ/R6n+kT9jnu/ylKUPR1Y7rFJYnN7+jGlPrLrwQ3JTpcsJKVLN00si4c/+WW
daFjQO4MEJT5k7bvjY4wa0BkmTdXqCGKk0Zn9xJkhXxKIh7M33PA5i5vu1MPuacoBvC1jI8V8hut
UQDXM5oW7uSMt3lZhoA9Z2F5yRg76BttPxWmRF4FKDR47kRDjS4qW3wXNHGwgNfMY3nqIwYXhAtZ
QQKx7B1FpuXvaBA2MessZauyCQjaPsQ8Hf/6aYxiTY2XEuIAvvt/scOGxB4Ja4D+X8Ky5qGzwgpS
Tu0IFq8lTX/AXMxJr0mEvZ8vpnjr7sr9MUwIkviQlVZjUThH/SHoGNoWRAHHFf+bjyeLj6eBDvqN
65mpJ5bTKfLcRNrJwGhqLuMl3pWPwcrqr/dWOHxZP+gIgo2O3xez5IkqvLMcRx381wP2tZG5/lKs
RaQlAiFJXgRnrWpjV7Y1B1AWlfE39oxddcCHyW7aZxYw7tESqUXLqUPAXJXLDemdDvccwI3MHVZy
jkEH7H4Ic6npx4Ce5m88UsxffNFHIn4RvIhJ/GOs2Xm6xubTThWquAkL/x9tMssYJkag+pMrU79c
1CQbXULk/J34oygVGBw52PbxGbQzupmF4s60oI+Vff0YesYhgScATPEK6Do/+KIeLBnqD5HsGFgE
v87hhOUJJ8zY2UUDHRZheklgp6nSKA4PLG5I7X+UtlMCurourc28JLxKsVlZmaZf1PEDWoGl5IiI
f+j3Gudy2xBy1swokoMDvUjei1Z6TyZbTRDNObpNPVxl9PdIz2aI8iQu/IaK0nX0f3hzndFnkSFV
vvKDDDuFcrly91Bj9ifPQkW18LkZyCzmtbJSo1GeQjLXjGEFGNZJ/x+otH2Da4UtsSS4H+5U0zlQ
kLAEw2pMfaRfdxptEEElBBZ3im8iWcjGusHskAdFihl7WOUS63CP6768+OmNS1b+PIBt+9FMlIA1
UuM1VUZon4CWO6mt1xv3AB2jhfR26ADQ5EFTmGrPqqfNWTAzCoBkD4kmELPgoEPyjhr9QWyKKWBU
Lm/uIUwMSyA+UWJf1zw1/Iyo6vv0024ujybHJ0k0x+IVFbWy3BHhyHVo6c/4SAQBNmsDkeg2rx2w
Gp5GnJ+7CzP71iABqmcXU3wxldTu3UDl2S2giG27MaCycFb4LhKYfwcgbePp86x3HHSnSFfMNy57
HXniMvY3m2vzNi1rnERBXbt2MgZ8L/3/qzwhUfCPACzKVjuh0LQJFq9HEaYhgqm1M0kMzZk6XWKL
DAG34Ym7eyz0PjRgbmW0KkRotpSETB3X/gC3I0Ls6DHctv8k46XTJjEo03nRI8wRVljWB+quhl4O
aBginWD9CMvCAIE6Wm0+fWT/FuKESyI0uPqypE2u5RnsCHObCNthB9nqugAwOOT9sSkQ3+FQ3m1S
4gl6azqOokWwOPKeUD09V8+KXKEoLuCkYKwR4CQalicnxoaksRoD8HlwzKdoKJfsv4ORiQ7z/m3Z
LVWEGgQ08gr/B19WwQmd3iNra4z++f1qPiv77r0TG0Y33W5x57FXSXsYpfhJAluoea1ZcgdjM1ia
o6wJtVR7alGrYTF7EuKC/pV2RUXJdiYl3JX4tWo3OJ1Sx/UE6giTwnlio9pCDUkYaxGRXLtsrBl1
7w6S/3WL44Ca3F8cCLW4rL/jlhMVS5uY0nKM+6umarQdsror2Ld2yOt1XPYZ6ZfCqnFwaJP7GOtO
G7EHOnSGps5qV1+p75l3gOb8DmJ4sVia1trn8ZlXWHUfp6RBHfQqvD0sYcXiF18sdNC7ZmxPEGKK
OKkMzmx+4M03rOJFAvvWFSLI16mmIJyomm+QUlJ4oQE3eau91Syj5HbrCNXd0Sjw1crYsY/0i/E7
8R8Lg47YgzVK2jeazkTZiQSpaO8+moFojHJf3rAKr3fmCE1I1aHw5MoIWbzQC1DLFw4ODfmrPEjk
j7wrgQ7Y/Rx84ARf1YjBL6OJ4Rgs9Axl98UXHX97+f7O4gYqGudq84KOnOHprAqRbmszEEB3eShI
mdUBUhBhk6VBZcHaSkfkG83jo353pxiIs8ur2kh6ose3Yc1XNbD8TljCFZW9dcs6CIHurHtFI4Q5
zIj0SIpdBnKMEbU2iNwep5VYCixcnQNb+DLcXsvp8zaUuGGdMAbqnjWfbAPImKv9FMZD+OiTMyEX
LsOxuLB0FErL2/gzE2sB4Z+B42VEUNviiRtRKiQFLWek6zsvVS2lGdRyr5uw0TALrtiJ2TLCXJ8U
B9KcdsHPDzDBDWL4hbpv7+QA/I6YavGfuqBZOruQs3HNRtupfaKFgH5Hy2dQklPWbUJ4Fd5waxgM
lhbR5DTQ9aID7Hy7T91gER1caR4XoQTvfSMd5JrvzVkbFSKz0r/y869k0uBr6uLrsNBn9hyR02nF
gqB6hm//dLmy+VMHyAHbM1NluHVNNBO5F1tBxcF3VH/F4RaUYn8jdHo1doZe1SJq3aUMuQPQbLqD
x2dfY6nq03nTGu1uj+aurOwqWMcW+HNsBqs/vSAvxz+aF8pFu0gQRLIC4r+myxNmHtbO9GNk0dwk
KWja0slPqk8kj2pWByyGmvKJbKE+rXWDl2Hd+aiOPOXEJymbFe8XAyNuBZSH0IWZ1vIelD4WEjYs
0VWzm7MAjcofPufzVC7TqX5x+CAVr6WwOmKhvkwADnuv7YcFa3y48kFLA274lxvJGODt3tI7oge6
p/6V0/QaBuyC45G/K0N4PWxopT8Ms4Q/9w/y9BfCZ8iSXnu85pT4XSJR5svQGOQ1spD3W5+bHs34
65Hf0h5Q/4G+7OJPV/sG+Kd+HdXQwcuiGp9htYX83UL6O+PG+wTQna98ttVUIRt/v6LE1b1yqSmQ
XH+xen2KV0tChV/sdAgLzx5IcGrDyP6nIEqvGU/jEZuy+mDUJZvj83Y0RMx/WXWbr2xXTq0cLRmp
Q7z91wlXA9ml2ZUf4LElMHuxXboiN3ahoMgMIvKAAf14ww6e90cRfN++XQk0SW7sos/sQ4wJyoY6
8BiWDi1NvEWdpPsZ2MAyaX3T/xBiMe4hiZUL1JEkQfZqzIcWBbe4u1UMLyUmY3Q9fUpXOieW5JL2
HkzJi4LHvcxbX6c9G/O+T9FCamuaQv2NAR2njYIS5EFGfoi+cmERe5ncQFM8mUAdTF5CcbcM66pu
IoIvsgH6wNosIgDScjEbqnVHBlbm+hHPKngX7xz+J2Er+NEvBxqxog1opvNe0fyqzLhWqRZTCbnm
eu3mZaYMV8LCV2Qhj0JBqQkcU6m0m08pnf0kKl12CUg1UOjX2GKWfjoZDTOyHCT9RKyAl9Ll6ZLW
zYjYXn1J390+sw8IuZ2GaPYu5VpwFoQfuCL5A6CX6FXthEsTw/Jmca7uHLq+UEZ1az8H4gvEPxfT
YRSqzT/apoK0gw1AAHjc7QVQU7xXp7/VGYLGD0+bqYHwjZlCFH4zGB+0grSruRoR0v1mKz1qzZq6
tGJh0rpFh3Go/AiFuBtqSfpR68tuecJVPUPdvAPDgVpcUDbMZ8uPocfhwoSu7qhhtq56c1P589J+
8cvJMtDH6WdT9mQwUCp0we7K80pmfN9Ekx/tlRUwZNMqCtNTpMowXotg5qZ4xB4yWxeW5trKG1gk
Zx+PSmWvwZyVFmwNatYj8yky62vs87KA+7IP8tZjUBsqKb5+Vl3mECZPyF/P3VpyyoIgxNsCdQee
ZVPkcWhSWYhW95QAQIaGnX6h1h5lBW1rgwKHxsR3kYmgrnDUM6nCzpCg0wC7oW8AXpen+Z0BmNVS
DMx2yH9C9dCUf1wuvRT7KB3zTHRb7cBPHcK+GRj1RWD0HMxcABlO0du/HlPu3/42GewURH9Hq7YP
ouml8InsBqPSjOtxbd9RNEGxj9pZ8ron1jgHDhQMnhyltkpJ6WeJt6DmBxGRLVogjb5b+krCNmvF
ILG6JP8eEQV5Qu45apNhKXX7WsSPQB0qJiujHdSAkmJx5PpbwrZzPAMJmWVp8hSUCY/SmZu3mw0G
sQCPJ3mtM9YrBlU6K2ZuZDNsJ5++C4R4s9ZQ0NoV64J4hcZJxxhuCExCeqZyHjGXjQvQESzbJf/a
pzoMnDl94iKGPrjU2vNOG+4twMKZ50s9ajeSnkF8x9jmoktzv/MBft+sRZRDZemW89YpVR3f+Rzu
W9vjbFMkTsfC+8M5heKrtTl+B7zJhLPsas1VLYan3/DDPnZEC65oFWpfu5KUNMrSfFs6VRTmBKpn
RR3UtFC4Ar5c6BkdIVGWUKBAVlx4Gmc5gPeIKfDvw52v/RFU83Mq8LUmYlAHNWDhd393x3A9cfcK
90cX/cEs2zKyhak5qv12WSQPjvDrQTKrSTAemIRwGnABJ9VMwyXkVfSgTA3GbPeztSxINliwc2Db
KQtiQp8HDvy2gLJUGFY8+AR+rx7wmPQiKYyiZxtoEmJJRCjJXrJbeNY0W3/5VrRDqA73ZAHmbyRI
mnXEZinQ1YQl1JNp1QYgSRg7xjtXOd8qQnLLqKXLn2w+w5h4AjYjUgoIvQUs5yX3AUDH9gTkfdZD
tVY/rNeZ/qsiwANZ9EKhu2PaxJyuj67c092zdWOx8uJyrJ4u/aYQgSOLbQfhdRppDuW8r3mfLoC6
9+SQQhFIJVqyPxhWatz+1KSgbgcSkucMN7wlYag10ByKjxbIZWp1vUN3b4GR6jAuvVax8zsL2/13
PAgbqciHl3+EhIFrucfeWaekcP4n+37WNwMEwEZYGzAJOmNPS43fDzTQxdZGEr6KOq7USXpcety2
7ydwVDncKtP1M9B3aKzymLrFBN7XWPwmat6KB6i4O/d25PKZ5zM2XGfPb1tZ93hlqRkjcTKzkWa8
pD1Om1tvH2N2/IPztJVQ9IPDsU4GQCgpUOGLkfgt1utIt6ZOHFZLksXUyn/VtXZFC6I3Clx0cM3G
9jZMPyw46/KTqpadagrjeyEPPdkooWq4QimkTjxcVJWXn5t3VRQp46ei6cQKU17PmYJ6RAEwwr0m
fju87CO+Z1TOdjCXp0y0Hd3gAPrUbmgYyfdZ1Dz8d3wTNZ3j347/TsV/FtyeBCT477emDwK/bd5E
Vq/aMIgjYQ53rhsiEsbP0oNXYvWCP3t+Mz1j9GsXPeglJW1tRhHqX1rrwZTwD2y42YF/GmBYxOFE
vdYNYQ852ipf9H3adth8S1HQFVMq/TXj+AfFiI+4tyHAfMFDmzLnHwjzrfGmsX7LywMJIiL8PN9q
Y8GFJFTXhKiZIE/CXw9ThLUAwgM0PK658CZLsLGTOsvZOKd8JlWTqwuc7i5in55hf6MXyqNrHxdL
QGxwtDIlcC1t+4Hea3GRBSKGj8o6UYvAtvj7NbU0GjEO+vp4glRj+FQzrHMnZRjFk9K3MCitQ9tQ
G/YnJ3rIEZXSoQMoUclZco02Nw1D1Z0o3N5hndsAEFcERVMBD46NyPA48eiEjfrKaL7JTdDp8isL
RkQeH+T7Jck4HWKWN6aR/0KIx8JAI1on0TdZvm9n9DG6yDu3GBWHseIl5X0QVjY5DBTnkCDBhQhM
6dMjfpwkKZERFTsjO2iVg+eg+HoxSYz3os30sVDa3hyeHVTISyMRpCWu3BnF9cIkBRxGwq2uL9MN
Nu50fZX/YO1a98y1YCBNoTsXZT1t/6hBgni5HvymOEbMXnPF3QrYG1mtYNVrTEZ6fVrg18h/MJGM
RtnaBvxaw2vKU2PzoAs3XLrocD1xKEoIcgjXBIsFzPIEziEC6q35gbSLAkz5oJrajTGiZBKy2bli
Poxqwqi8uGslPPHyYQZ++hc0GEvaTheMQ92S0LDmM7lTHx+akxCdZcNcX1l+qLqW0p6ucNL8gJWw
62fqO33YU36/eU8oCCBFy3pgaDpRfSSd83q8MJEQ9dxeq7P/HNrMIYt8z3MjRG+Lit/KKZZ9SOx3
/WAfYjgY6+7fyoA2V9Z0XW+Qs/XiM1+H7Fu3QATq5BRnE32dDU8HUi/Q3MJyHMHBl7YEyCL7KMDE
IokDITuqBmx6wYkpLkHIyNYWlGXeT7M3pxnK4Fc7XcxRZkefL1dnLLwUZg3uV1Dz6S6NBywuGH+l
CfjLhLWWnPJDTPRJau3ii28hjuQ6M0+xoWFgiwnR/xIluANtcXXxMHSIO0Qgy554Jc/PRPsoGhi1
u/BJhUVEUfkfgzREENMhlJrqIptNRuM2TT6bv+F3MTIUEgBMlGvGXbbMPrp74RHbAaf/ueWWHP7n
yITdRibnSbAbOVPjwOZOVlTqJq5Aa7vjeWiDrbQoadH7FUshgxOkGNszWhZuh4/VlWeMHDDDX3ax
cxZS0xvgkC0e1K0NrCM91V8Pvh+63yA95tXvBJXp5ywWCGUrLS7O9zJ8TyMfE6zMaTrLE2jHoQyq
qe+cDXQrSOhErVCrvuqgmd1BopFzqnw3BuiOY+jsE3e9O9V5uHBSphOz0scNIqMLoRp1cFq2ILoR
tnbR73FK2MqRavC8WXwFdsfCrQxPaF7En3FZL4HlnzwsSZ14imCT4fPz/XIR5Rllw1dW2oQhgvoF
utM508Vu+btEaF/VGQaf0Ylsy9u9sjml1i+tRy7VyKf1HUAEFkq6uy6+o0R4OuTTCigo2DAvAGmC
gM9t4h5j18LVvxKvNq7G3wrbli6PMk4z7D0uTBbQRbFFRa55BLqhzUWC/R3mFWKRAXCqJH8DPehs
5JuEDqX2bomMkz19hj2/67TvH1YXOFGOJsDTVY0h0xY8IEG4bl/RSST7NKtLPaiOtSlapQBvo0qN
8V20uwPq2gdX6UKpiP4ErBtqiJpTXzH1KuabPyEFT794d5te4NxoaqosxUGX51Z2gIXvfK8uRtYO
LaKeR47kGLdl26+h8fVeE4kJPMw7/OrADk5u/kXCIZNw1AZkiO75C9tfR8KnrbV+pnt0s690V6E9
M/3J/Ot4HoIodsXBnB/jHwzZbyGSfTN0A121q5cEApHkjBiPI1thpEGs1w36cY3yM+l1nMr0ZqIY
+bi09W8C6WzGdDu/tItp0x74cxVJp0O1d2ASIfPFFvuw47McngOI49Z3hE8DuWYlmQL7cst39K/I
nFdo+FKRg3xipL+tBMn9ufM2W4kuqWKUZGvp/tN5N2p+G8rzCIIymg/KAt+vBRGh9K2PwUWdttyz
M57rojSZpQPrR3EQvhKIh4+zgsNPR0cVQwDqMfH75RFlU53Tuum8ioybof1k2MpDCLh2GrE0hS+j
m+KDB/+lMsRqWkf8IU+ls85/Z0BhWiYchReFCI+D59Ca5xkurPiC6C7Gg5eRwkqVUR2no1dTu6AK
GSY1Lts2GnqXgVX/oMf+yZRtEUiUac15CkulQxsITmRqWp8wgpDY1AgIHyOhgK/PQAmxoKJ6zQCj
V47k4AkJlPlxxeYh8Dr9bMfTc5Raed55zlhllDI7DM+qeHBoERQ8Z2UG35H8LV8mlg/GxHd6Rj1r
HAMx7KurE9ycO3aO1C1Upnh43+m+2PXLrvn7jP2NZ/loIni/sP8mQFFK0YEMqV2PLk4VzSjV+fMV
CIj2qpuNdplevkYWcV2GAHPqIyfJhJ8j6Fj6eM4rOicOSQwGels3Q0YwCacrM1Dpq20c5YVDqj+H
KbWxQXsMXiSwfX3rJIY0s8my5IlD6Xmwx+0Eajj6qwLg1CfDl9T1jUeuNh9hElAE4wTxwl7zc0LN
hRYGxCnQwvaAJXpX0mqxZe4jXPWTdydX+FaJrwvl/t3p6DT5eyWVFbGLr/oMgeRl3JVEjmm1HOM8
MWXbyigex6YCUVfZlgcyGq9iD9LP324Fus8y/NLdiH8Voe6GQcwBtGH9wRKVsuQx3+3gqKlppwZh
QmyMxVLExrR/qa1ETExBZB5WXWAfzU7mUivPyBEd8Sm3NjqYfUJbyEGcsV3/RAFSNiWrAg99Rb4a
lw2yuKjISR+zay5iYEL7M6iK9qYBBP1F28LzGSRuQT9FAxAbIh/ntKNky/4P1r8ayF5bKz+2TwQ1
mQQjZNzfObFXXfmpM8woFeB9rf6UsnTxWUCFzkRorm5EhuqRwouf+5mnwORK3hbJJBUUEBsf0Vu2
hUKupgTDyViY/CD/89n0pTMzVUd+qpN9YDhzB+Z0bcQE4ywWeT65sxuyM6sDJaMJIZFPwOwg/FKk
mGUictckdX5YvCRLbwcOxJoUcIDyIIoj+7UMZ5iQyrGIpyXyCfWM/U9chwby5eicfmeJseQO6GcM
lTjFeDZ6fPUq9e/Ay1PedT719LkgnsKomF1xjLqrdosr9iZHg1kPHoGKGn7o+F75H5jQdG2/uTh5
HGzZ+X44A+UnmAI8+1+Fdmb8gLuf2AT/aNt8Sag6kWqh4GUYqcQTT7nQ9wkGk2XvOPPqnzBxqLAP
1Qr00l4fcuxYvxhsVE6WJurGywIS7a8x9SF9fQrbcKwTZTtliVE74x5oYXloJMQDVQUV7oE0+n68
TXwOpk14bOCh/5AoH7ntJIqQfnAdFrICqqN81cm39Z8owNMz9wMvaNFY+ONhASdqR6/FlXhGcg/1
8qeeMean5XiMOX9ur0K2+BMMqauGpCzAlR05/znsndu+W1gsW2RdQEaE1b/8AYqKgiob5OOlXkZF
hMCao7Aid1jVflIfyNTq9HL1jWqRwO4LHcSRIBV9bLrv1CSmQLoV+Ey15od64Sk996XUYEUi9Ttw
NI7sMABJC6Tnb5h77P9PUqqezBr8b7M3nX7ldNeQ4szFwZiea1bfiF7glen3qYbYz4lJ68L2DBVt
PmyLIaN9goHuWQiC41Y0xL/iFaE4S7wmGSEKWAS0rLZ8llBedb+Qzk03mp/vngAa2mJNgVNmmmPG
JtBXdnacnidQcuZRY71mxcrta0UMU8vyYkGbqSate9TibkaYYQVa0ksBYruQqNjsoMnPqtrPuWSl
CS3DOn1l58CKLncZcyZsL8D798E2UFS+WKI/EEcu9k5PJM7v6acLDkv/2kKincPJ6ByIRqN+Z/O7
wEuBz9VMuBstS+HuEm/dO5lSSceemDRbSe0QNERunyyAdTKWEpoS8YLgqtjVDp705SEH/SN1pGiY
r6WjTC8k+xNJZ3/wzTETf57mRA50OzeMJcyPW9iFnWzJ8lg0agWWBPT530i+A+w41zWYi6uv0v3U
q5eSAYLkwmTTr8puHxMN5eW4om3QZR3Uf2szSqLSUW4SNXFpqRw2d3hvHLffZ0BNP+XqDrs5vqvy
vR1CBs995njSDR2YmbR7gx1B0CxAEDWZCMEXBrl6ovBCNXP2wXZsAboI5VLFdhUhSyTRpOXHn0jf
BbaGkIiOa3qxjiroGRwHxQZU1yUpE2Ad/XcU6As2vXxdaZG/Ip4w/Fc+VflZP3d0kn/zJ8hFsY5a
O5sv8UI3cWMxeTA5WVJJTsWpMxmyE5yjtH+tIQfm88Yu+pTgrMApxDMOP6UqNMU+UzgnkSDjvxrf
Q3DlL0tp7Y/sqOBTn6yLTllWPO3lP8PmKoOutXd7O9QMgYa2TGMA3EH5vjwdUIx5faf4uI8V8tJr
or2aK8RU1uyDnH/b7A8Eqj5PuvwkZUWGTqs2J7ob8G9pe54DUXKE68VADjTghaLdUU0XQV2L0bot
3DwuBWZ26c+nEUt1B70zQ+ClnuMvzUguppRhuh1AvME/OhA/B9DDIr7ORCVq2mv73DCa50SDmDAK
vn9spVC9nX2AEWx/iOXIN0tubqThR4HE27gko0LhScEjpiULW5pijqNbo/wLmJL/JBC8lR9HFxgU
VuDFquUAQc6hJBipunnU7oaqanTRpZPQNFh4t5sGlMbYQAMWg2IGz4MWCTIFAlS0by5MQJPHrpac
nzs9EzLzz8aqeDKVjfQsm5AecrnVALFhaJagQ4lAU+XbTsQTYZdjeSnc1g56vUQl1Y0lveyqIJR5
U6ZLLuwMTKKag4pr1PTGSIFmQPEeIYYAyGiYqODGxlx8KnW/kLuDxqAekAZpZgW+fu0/vpPu6Iiw
02DllXPRaKw/3eWtERmovjEEA08Qws0Z7xSVA4xBiKadwInw6jwjFDpYc5ko6Z4dWeQSwpcuAK8h
1akxt/ICMHAUPUcUtnwbgYoIoT7KGFDxcTSA3YSHK4ftSTmB98BhU51S7oRPkq5DcA7UkcbwnScD
4M5pEdAh0nsoZGMga979b4hX0x6j2+EZTvJrzERFMv9vdbYZ59Z4Kah7jz8f7wbpar3yydlotntj
7/E/BWV0WaXMvevzfY4iYanXG372H5cEdCKTRahx9U7B6Mhy3guCMEQTrkrXKdHX+uEOujtKzmlQ
8GZVWrGyB1lQ1yCFOaiyGGsLM0eMPeoE7uA0Wh/NXAY64zUCSWasGVIi+mktZ6rHlzBjGMu7hBAc
g9h4oSsmUR692r9F2Jx6Ni1ktPeIgsY1bto0fURbk6jY4PNYzkvQqFdlnxURKQtICOZt55SwiZWQ
r4jiOo7op926Erx8nNEJfJxQ6zTn2INhehlYcu1yJ01lBE/BxhydoZZeYSIjJi9SFX2p2uAZN6Je
oBHQmyg5+hw+rsAIIe0hSxXs4+HzCDUCZsRa/QoAphVmHVZdo8cQ1wIyJ5vV7o/nQyJqI4BXVlNa
9PlZm+j/sU1n4n0kIPtvX4/0F43Egw3RniWX91UmPcuQ7HFjkddkpJautixLfitmt3lmaMqbungr
5h+PHJEVtmWWy/Wrk+6fbevKfWEpfgNobacivz1i3pc2PurjoWdUKpUfovGMxG15scFpPyovLumV
HXS/yvkuf/1BD8TvbfCxM/h34oEYB4MdaIvkaU9ET08bPaeWQZ3E9X18sIOb/55Kui+TJHsBUYTe
7iyS6Fq2aVwbn/nXJhGk2a1V1cEEkGOL6sKmEiy94GWJtco2UG6OLsCK6WmDDeR1ag7tB1SpMgtg
f7Abje09M4r5lOYpDSiJEP31UFMVpka+v7OYcgE0G0PCb7tzf9gZRuaOvHMT6yj7LH6VFMaHrmvD
uVGLAkWZ3kjsD5hwUboZydwxdc4MD6pww+VfZOAr3ic6Iro538ki4MwgiglOKLJwyTyel7ZOKWhM
s1dAdTOxO4CHgFhE1mGQrcDgn7F/yjwDSUzARiwtmQ1Zj2htdobp29KHTlfAPc1kNOuTTF172jyI
g5Rha+RQutvBEjKNpa2PhsPjWDpYiGhVxMe2MzOjybrlbNmHglhN7KcdhCc278/UB6+Rpky1/bwT
TJyZvkLBR8EB5qmjukPmbcsoim2vzyIBHLhF6asKYXwUdV6cKGU9GINMWL/JxY6eUMeic/V4dYEt
tseLd+KxO6O0oJSG0nb7vNEvDNFjeBzXebdpqeGHQJbY/kEdFYuKaC65seoPpXPx+nTaxUO/UDvf
e4OhqDTO5byOM7WUYrtdrRj0cALt+mFyCHVLOkXzGdaKoNweM2MqYX07YMejiZ151xWVE1jict37
hCDjcJgvokB/SWwimOOddKdyj4iPizKt8tpLiTkILaZWSZ7vPHbieXX+Ty5xf8OEO5cBxOP5YI04
r0EFNGc5DBbeUwUa1wG8ouorhjFj32g7sin9WVeqUchKczzNeQtdUUikK/w9TjYGpZCZ26e9kKbr
BLk9XNklAW2Xrks2MsHgVRxx0aBX4283NfEd1Q/NJJ5IjputO3xv5O5KkcHNGADJTe156RlSD8E3
2yaW8AREPcsPDtESPwPvRaR7tO0Goyb6O8bqLhUjFE2gIzlq/9DBDZYs75LwomTwyVIKCXwbAiwk
BOWMHckDBi5s7Ss9Ll3NS/RP5jbkmr+Hws9OqLBUkCcJLrkffEkY9QKDXxacySU85HKXuhocOH9s
ot+MpEPCZ1kLBtUlM/xm1OFHyUYu3y38R6mZycSQY/2M0ikbFDZQm85i5xa73KXDUX6Dw0a0m0b2
4zBKPnNjcP1HREgqNnhP2Wo2UWs2IYkjGXFGYL6mN+Tn9CKlkUM4B/x3PNvER4pynkZCm/wCv/4C
wTuagQ1G7fuf/omdICbGRhBFhajFAWM6pTkl5J89QescamZwTBQRUkmZdofGzrT+islSqsc0emYf
hzgoYj/t4Cyrx9FdMsG+WWP+w2/IJHFnnix+lpsmxtX/m9BNCiZTJkCjxSpRJltWZ7oeXHXR32nB
v6FMmfPEP/pMTSEx4icJFgjhYdsF+O2igmby+gwg1lpg8yErEjx+s2j2c2O4UEtgEZFQjKUOIBEs
19dQZ+SYInUSRyzVyxUEIt5uhisbB1kMlxNW4+efcy2xA1iSszFivpH5YzPp9SZqHMHyxINfcHp0
zHbqRMW8+PZ8coPbUNO4ugiThGVQdi7xHOvHKdb9GL0TjIpTD47xa7SW/qdrMgzYl9YiW5HyuKNL
moGST0To7zA9HOxBnOxMCVBPxiVwL0csfSrD+FECnlbBetZARDrx6yDhyutp8GTVbvheafCKbrC9
ofGeC7hOZyud86XOV3hsf2fM6clNcagIsrJBpPJL9gyzd2+5wpLEaHz8/ih2YuAEeYd1KDTK2xWD
QbhA5d3s4HahtCpS+sNhiT4Mu5M75KkwitgplSWVFNWCHVFtUJxjXfML0lluUo8UGnJeZxhbJ/FZ
Hhkzl7QCWS3qpJscFrCfCl/o7/rsfwRh46Za1/o2NLcjwSIk4VDbOMK5MBLkWSBFKTiqqdg5Rlj9
7YOlUcy01xkppUamm7SZlrqMacUAY7hMwh9WN/53lY3iEgbPuS3hASKy5D1eAvzT9kXTppWEUBXg
y75zY3BcUAqRqGq5CpqWJi9qO9BuUdbDqjwF4RNm6huYOrJZFAp1D4XWE92n86Ux/GUd3TBiuafE
7vFXy7NCgoJ1R766dSKzkZbVkYLSh5WHsXa5gQCPn/un2Sdan6ATvMTmrEZnVjYNbRItVXAXKg32
hF4ez8S3UdWz/Ncue6BBX80OOu1K88XEwh7hKW37IFMcsxv+dj9ubO5PjG614OY1LY+9fskHmGW9
Zat9l4AMQs4zLQy3Oa5tNABtNLdGvZ2do7jFIDg9cytfmrNhu8X7Bklq7c8ppHXDN8XkvQlys546
obyja20SnBNI3v6rxp4PC4Zqiquyrhn75IDKgKkm3VsTqC47uG7cweszcY4EJwXRkFoJx8+T02uK
NouJoKui91wRTHPGb/FeKxijkyTV42dhIVnKI2iG4fdDguP8Odkyhxb+b5T9lfjTvlAehc/rf00M
yzQL2TRdKGsslSNTgdYIvgU42O2QSgh55pQ2dAEAAMtLttl2rO5/np+sM8yxMYZB8mKIDo7oyc1F
iXv8do8B19xXGgQpqdb5d+/oT62TDzvvCjhIQkAteeTx9XwSP3Af4NNewkivAa/eb7BNShbLecHX
QRsWIJ0A6JLPS5gNrN5O176jcufquUu2/18cjuj/6lOtt8NWfac0xcTUCsVYIxWdO6lXaUIM1IaB
+h3gpqJ0xgPVX/wf2UV5a4u09Ocz3vz/z+2TUyw1St/EpBXsHXwojwEw+k22sgYAlSQptykiU+0s
755IQdjP1ki635dZKDi+VPfVNomD3RrAAIz8/QWJStCvWyjH5xvTNUjsANCmMIMj5Lx1rCmlJ9b6
hKNv7qcl6FZsP64rh2rSCzUQsc5MUc7RLbiXZagfXrv1W7BlvOqUIYguuuMqmm8E51Z+5sHc+WeW
hpY4pvKMwmScYqT0Er56yb3s0AVV2X9/lqqkqG7I17ZA9/WSi/Z4ZXxPupSnxELuapiG1NZXlkaV
OmJ0nhfUlafTjMmp7xbw9h30HilZWmyYGA5rAs8wRYU6bxrWfnzQ3TEdCpnR2xzUdb/2QHDx0erI
FDqGLQ1dOyZvGJixDLipwC+6IAvHd6NmLH6tdI1/ayvH9EPHdl4pCeZx9q3TtbYuDDUOsYcFJ0TW
/5SS5SGjMa1VNbPG7ZWRyM9jEbCM0z6/bmlhQc4p+zIE91/Qqp1nIxFkQ9On8jqgKdsSBbFjD93W
bKVjDUVKdsmct3AFAqMLDsPgqb3CT2khwWT2UJcjEQqDJY2MKwI7szNrncR4P4vh+61NT8tjERqg
rTAw13XD1gqZCVywQVRZIGtZ08eWQ4fKq++iKRYkI3+t6utnt7xj/YgLHu1ODB8amoL/Yw3b/7HZ
1315W7xwgsMNhjvKX+wLs2tBZDwrvYX3Pjlehokn5nRdLfCq0rix+RM1VZN2ticjl7R9XeiJfleA
Rzoig7NZ5T0DjTIwJ2f3NAyXn0hpN1Ur9d1whxzZzr0j463vGMmZVkkfgJGBU5fwBO7XhER9bN/h
Ltt/8cY1KXQwQMRgayX/UvB/gG4T0TmFxgaaYUZIe0yBQPs4/iS42KuPJQUlKoNm+CJwQgai94Eg
dV79lhLVeNO78CB6EjR0WFRzjJnmqEMGTI0m0szCQSgBNTMw3YoywfHgz+XlYdwZtDXaJjchcISE
AeGQbkpySh8N8ikdbULtCmMBNWHtm5Q+lwuG4rhrRkoAovgfZABGEskMPqyimLV7kDVivK+1U0Pj
7lFPtr9/VI6NKEErqu/ByrBZKCwvaqe8AfN9TMK+k3EWpkvoJqWcY4HR7SxqzP7oRZdkzSEMCEdj
Stkv7K8pNTG/KFTup8QY5VFEa+rZV6vjIOcIDpGXSGFVVyjOTKVaPzZgO+72ZU0evltpU+gmeCxe
1Micdi+6j6dL7Yy6G+UG1cDvqpCrwO0dEfAu2Tqr84bdh74QuIPYGIpApugYG4gELfMy5FGi7/go
8Zu34LvwVQnR/hyILK2CPah5Zqulct96IVSF7WsmMOwlqkncUHa0Ax3qKOk6Nb2Sp3R7Psh2rIwP
pm8VG4fOqvAi89RXAumuP6uKO84sWzhjBo4YNe6/SW+rmOAgWc0F8kwqtlVltESmOnKrugiPfNug
atjXhdxSbK9NWxosnOCqBQrYDN5r37faimfeT4PXgS/P03Ud+DnWv8lHXPNZdx4yRiciqh1WKMpk
7L2PNsOv/drCP4HbTLjaMHMWAyJGOaiw0OJdMlBcePhowk6eF4KD8bR9RHvnShqgqGCOZcY6uJW4
iYe2N3ryUcaWnO8HlYke1/BhRqS7Q8+emVtjFUJBSw4oz6EdG3YEAbfmLlb7tKkR51iu8YKvUv41
WVcw1MTv5vFMlD3HrrNUco0ZjScp1EvbdxJOtyjZ4krCR4YuPJv9p7pkrVJa0tpcdp0cwtg5YSSY
XeMe0R3a1ZVOHBD+/wKhzMgqTV73P50WQ1MNApomoO9z81vk9I0LLzLB7HVSvFQa5ZSExnnwsynB
RWyOwp1qta03KAXMmj3MBe+wdHY706U/da6+0wDxtpnDWypwwjto6nKC8LnQ+l2LKDau+gdcQ6Ys
TujRn2cFRyuLGldyE3iv1EYTg3E7i/dBqXCdn6rfpkBvms1kSVxPXNyIkiF3MhcvavCNLChPQ10i
YnCawPuGfBLMHFQt4TZGiiheqfHGvN8UIe9+1MC9YoGRuS9tz5O/WqbGWMoluMgi1NBZ7VsZwNqt
ZT+PJCfPGVRs1F7zNotKLm94WF5m3jxV15PchKizEoLU8uzYC+nSJYEZ21an+pJPVMaqweGE77tT
+MCMhrZFywzsniDgXyVcI933abTfleLoLpoqeil2DqBPlCpRDAyHBubZfDuhXIfyK1MxaH5SKBts
F14C64ovIuGwY0gQ51AMlLNz5230yN+aUhaLvJ9WKJ5HUniK5wC4gcCIDZcy4Wqv9S/mZS5WJ3FQ
r1DZaFiJ1MTPk2mamSVFD/Bp+kLLFFo4Do9nswa6jbxzJpAnUu11JeNme+2uQ+BX70iOAIMDvfVM
Bz78Q+/uCP1o+cZ2u2tfqcrBE3MgDAfnBn/G8+ojjozWRI5OclJCaNImCGgfldR0X276FJ21Cuua
mjOEyDlTvkds07P4+pcDoCaO8aPi+YBJibAZjDrPWeILEWXn5r1WWaISDunrNCugw8jbu8J6W60u
yP2kgHcms/BJZ3OTJxNusUYkg3vLq9OwfJ8wtGC4wo15bqq5W4VW7eDF/q9A+ZDygDEafABjxHUL
R0fGqrWNAKb+2LWGvo6DNxxkTnUOVWYCd8q+/QfnGmkYRy/lR1JRce0UU3RaYPwNR1qigzMs2Zh+
H1zl1bUqDcI0ExRpUEuOWVEBnpcMaJh/OiXIQiiFKrSaEOqVIrzrG6SbcHkdsG9M5SXUm/+ddhiS
pnh7MRw/6H9vstqwITva7tDoqv/pG6EMGQm1VgNDzrXD8xPpGLdTWrankXDNu3jBmNQCSdlkKbd/
Imm4N8ESIqkhpnCYepECfa9gfUs+akvMIlqZEw97iLS3FWLS4MRt58OA7QLenE2BQZNIX5ukvWs2
6xOLFyWxsCCQ8zQ71o9WWZC6LY8GUmKGLD82K7Ex8N4pERE7x0Ghlw+ikQZ19Zg9rcjTu3Pgrz5s
iUh18tDfqqllHnkjlfbEbzHGR9oW5tLLF0zzQc1DIOfXs0Ggggdv0r7wWV3q/EBCYG/yWJ3JHcv5
QkTUaXPIkb1Rp5cBnkdJaZcw1K3yZbDD4nnMlEMP58mae3vORTI7yeghWfS2S6QlSSJoieNc3ik5
ijr5rj1HAPYHfcYfZDbMVRDIPVxN+eFoAFt14PJ65mQjGI0dbWSpAWyaqgwMAaSz8UyqwZkEvRjK
FzeVqts4g+ZZGsUfKAH4ATmYiqLCUw956OqY+IW2P55QnXxjl1WQJp3zsxWJ4x92+zd57nOQnoGm
Q8ZJA+vdSbwQcDSc1ipAzE4yLi+Lm1e3bfGrd1bg71fcHFExqsDC4V9GpeHd/aODThHzr6bR4pvp
ZjUhcTU/pbDYkkN46g3dBDJrAVoRr72YTVHs0D1Ckob5N7GzwHBwCBLaki/fbX5cKVdsldGYxJVL
DZLh8hyBxYDHxdBpt1eI3+sT1Bx0hykEgH9YD/6DEmAITMhZtpCCvW8XJ/HQ5pdMCZ2B9sVjHHoc
6CQxrcTNG8P7UWvCBRqyeH0Tjku9cHT/IZzZgQIBQV+f7ztI2nZani3rQEECi8DyhaO2CafKGVmq
VUAqJhfD+FejarrG0DTJJzC3Puq4ntOs/kJj/Z/ArHRdLGb6yyUpIPGnGz4f4ztaUcB5DIdavw1e
XwtUKWqrHYibeh5wvQplNKjubVobog/HUjUAkPs7qGAbSsEESH7nAds1iKZlObXtUWFhXtG6E0HR
rnh76HO+eGIGbyKsXB2DPW4KFuxt8u8CLhWLxIB1fLSePtarybbJDtuoLSbhnngzcYKuDvKb2hca
xn67Xqgx3WLyWnU+SFM7R/RsMLmy8nE6/X8K5jqBJgiEnDCf2frJR+pSftX9wauO5LsWpnWG9wuY
MOHZjDaCtlN6fnNF3uD9uE05l8djo1xgbdsrYfczMzpHfSb9KcvrkGIKDX9XxqPyi6nBKNjIsiKA
3MHXONDeg5Dl0IF27rWYXlMknaY6RJ9TgYfHk7vd2P6um0/zsMEgUxody0lB2M3c0pO6FVkQNxjL
7gJgBp1JSyt+ZCJW41l4wWJfWxLGBkkHfvx6EJEqRStZz7wSluBlV+3Mn7hSDsThS4Ze9CkXR/zw
dJoC6yibuBLByoFiDnbYX9u/U8Suz4OFKEce+bQW0ve8LNM3OCpUDXj1i667YVBTW/nzralZItX/
km9/0aiBV6TxeqI8cdyxf11velBqTsHs8aicgmpqY/TzLjZcSncogisdFJ1Z0q7kb+tXEXV6M7/P
23anfK9XS6MxvN8MOEFfLaKpHWY4ND4VuuXgUKCpUYRaXDdUgWAh/09CJ8qPJ7l9e0jugpuBCJPz
TnzcGCzL2uti6eSZGPvBh5rtYUPWTuZt/v2vsykB2pK6AALs4t1d0JT2xKePNDKMJ3Y2vyJ9DQUk
kVl4F1KaovrEZCe/3tV4ehu6VcHBLbRm5iR+/8cRznHMTLrnWx0NgsJKwKiWuaPfDNcTXUcLszZe
FDV6C9UtEEGoELMc4WGh4SrxEQtSmVn/hq7M86faPVWZfoj/11wCiU7tdMwLenFefuq6mqHr07BH
Xwe4wLPQk9R6UxqYhQzqac/xFFC3+OPiEiintrXaN/DQYtuiroqpK6XnkmV/nmr7O27XdB/mKBse
IKKnExaoAsQuoOn0IivTWN+udE11oP+gKgFzWk1VN+j1UOZugRoE/NMT/IfPYHclpSsHW/upjnrJ
cbT/j/T3tjtVvrrYhuD2tNDBcoFWUojdheMSpil8Ci1AjE6ncRi7qqv2WL51fG0upiRy/sHxtsls
H9R+0WVsD+kaf0O6QZtudDAEPrZJ5UcQ6MwXU46Z83fkH6TGU47ncQxAYHHdsYnGyDLwnGQTP+KG
NpvZdvQU5r0cA2HhpQbQY1O8hjTkLEu+JvlWrF2JXLBkNjjtQWexM5Sc8U60PhSF0QxD4TsAA/d3
wfTaKQpEfX55u/bmFOFbKfnjXhNBnKrmxbYU5+PUxXe9Rbov1YjnuYGxZ6YYB9pOYYDScke4xKp2
0GzScsPRTCLfUqumoWtQ4+vM++pUsTPT8x/qEA+Q/PlKsrt1FTX+9kNGyv8yx1tk3DCuM3wEUu7H
7QjzMZWSvmNX7g7Foh03mnk+FLi94q79cz1tFzUQWpBuXU0W2RS3m0kQVvQXfvx1NIeqfcGzHB//
9Xzv3u70/u+Wv6W01Qg7lUc0lI2wqBm8dljujyBOpCnntPQF4727TUQV1J6JIqBde/azqCax8PJO
xe8usHIUW20l8eWA+DWaQbLvrfHVvay3Og5xjOfZV3qqTNh/dOXZ0p1mvrRYzTMj0fR3Og9bSzKa
IGs5HyIgTZuMw0rqj8gtgpAwj0YFTVjvisT5v1GIZepcDzboyxZbrUIUgnDrO2zjMRRe6HppQQE6
7/W1OOWd+PifR/oRUcQ2QLELq3BYTTLgqzOaPIPVI1VvewRE2cLy63GWOFwpoZnN0u+XqHaXi0e5
54ypjGiXg+kiMEhaPAKIFQsP31ukCbucZVKyYRqNu/KXk357mdhpECKoecb04N0eAk+xb+zB0ri8
M/DDxXSU6cD1l4tYUL6GGScImm2nUp1yXsDZmU7h9X4yBkkCh2HzWQ+DFx7FIb1v3/FHyLkobtis
ZTFwF8/seeM2QjtEcTHqkTmfu2ZBgT0XoT3EOSGcdm4RDjXAx0hmO1QFScawYx3xKlWIlcy8NWFc
juN0ZDgsG+5hgMoZk9fkgzsIrvQnAqY2UoypLfcszXlwh8doFwg7GaS5BVz+iB1H2J0Cbo/X5cKB
NZJf6ihNC4BMMTNwLPXcyHpAHc+wHJc5hoTRHbQAWTs3/FT6wQBEIOiRpvtHydr995abi7IGFHL2
MzaUp3Bl33NgbfEtsSuZ1/M+Xc/mtb2j0RvFxgchsQGWmNxaWBKm0YrmjcKx4EWHE3A+7Q+UNCav
db97wU2r/+6yTnyR1T6zGZN/EL21PVTJaoNlLgRtyaxELJ9EFQTdQ4Q1hyD6J3vm/X8KWKaDUNCu
O5FFxW7RkD5iGWSrY6wP4we4scCqR64wxT926F3JakHDbSDYyuv0zxa+H0TkavQEFQsugwNQ7lg+
kZrppNnpMbdZHmEQn904Ca6Fvam6jrr3WNdjts113nC/TzTQBoH3t/Bhmf4C+S31fyZouVUGfACa
0yEOT6cauK3bcPl6Cdm1dVTevvhKRiZxUq+e6cZ8no0sKQ7b9/49wa6FcQ666VFMMjsq3nKnTFZF
cGPbL7dDp0x3WD2BhzDzgzJ3J2/RFYQqdeSmka1NLQIbsEjclAUSKue6gMbs0s/0zfiOyQU6tn1p
fxg1iuCKfD/j6hpvrpAs1vDdS9PDQvGcD+v8eSUf5RHJyluzrOS24+LhR3Jg4yVPL+Ixym/u7zqZ
M1ZTK9g0Xa8+1PWC3AOO3UDQzSwfLqGeo7q9J7gkhJmFNYibS4NA7uRxtey03ThepEJHD03bSCPn
cNXKL/dNqo+gqx/2krh/lpbfZ5H4LeuybVMVE8eDQEgbu48k/brlsa/whuEIvjb0bZ3PFQG36wtf
dN34cgjXYq54wCIHEhggEqMng8X1KhsnP7PW86HGNuMYjCbPWgBoUX5kXiVieY123ZftP/FIH6z5
yG+l/HkDal6uYRlJZ5nx2oQtc/h0aZPTjwhE87wguT+TMJBLQGKkR0lqXmaPcYDCS5CIv/OBxsBh
wmL5k5FWw3UqEDWOK2Kj8vK6XRIzMKqZSIuu/eN24e9xMwH5ijWUfTz6SYDELYCbRyFOxbK6BADC
CT/3XxOQtHEJ9aiSpohr63BZTX6iL6hK/++JQKMPznb4GCmQg04rU800cPeP5OS3W5hdyw3uX9zW
wTjL1ciClfBjGyvoKtoRzJGARhFbG+AaqmGUpvgu3PjT/Kh788fmqIfUxDMS9VKCPgbVaXDHXPMk
m/K8dptRp+DK5QHhvjQJb5xvngJxNYXUoaEY/CiMovRtaXUsFedA9j/3HQgioNiztls96cWsVgNY
nvk1i/0jDXZ20zV9s56cC++x7rF0ceZeIz7ir6Z6HlyV0rQj7mj1h3TiURxxcfXKsmSd0XDbmwaK
NX6u8U0HSZeVNEJv7LkBsXsxRoC7aIO0tvSLzh/iT6dFOKGqOeMFDMxHyqQNnjQhxzD2/LXuY/MJ
oJg9DRS0lA6P3vnqypaK2C2SirwJN5weCnmC+y3MWWq9zbv4zh1C+UZS06CPj+wIc2bvm6pVr+Gv
KNAdEs/JN65zjGRybCWHvY3bD/CpJ6GRoPdZQwk6pTBgK50s1kXxLvAqDc/aljTue6VgNpJe/SUN
untW1V/0TD69DrIo3CTOlfFR8q35ncWiquV4FTKnH9pw+6e3ggDS/dcGNBtN4+3ujahfzmrd1bGU
OB5j2NseCEYiuPOUefVomMXpAG6MMTUec/heiLTQoFwtZflsfCTcH2ag9VX7RwI0SZaKidXDdxTC
JdbPEIgHDiqsFkugQg/Wlw1HmoLlanMXzfmqQOE7I4yQK4lCeZikVwEqp6Vcfo9VmWRENwUAi46O
0umBHFyzcsiU3g3+cD7bPZ0YktCr3OeoSWQraLKnSYAZEd0zs0C9X0UsIWvNO5NQGPC19fwsf+Ux
vH9zgHD8dbwepCaarI5cd6zQKDnRKK0vrJ8WKi5OzebyraSBwtGReZbhy6rur0I5rbWNQAoeJyzd
zKn76sOO1DcbJUN3MFYuwNmWER1hUxRXN9lmNgkXges7F2UMjb6adHXfhKAbH/MQB1ZbSMVVegsK
v2zTOycEbNM7bjk6U4JTYiUegNgMRxL0sjEIgj62JFq407ofbEM2eggt6Lr7by5C1Wv/5Vb+hjiA
j/4mRnWA3K1P/UT0D8vNTMFTK7OvgKn06XffNsB0E7GixCLUms0cn3+XqeaOEqpKkiVyf+t8HU9P
igqunuxxS/HAGSdgXPZZ/8U8HM8xnmzmd8RxPXDHowSEjrDCIycRzMV+Z463QnEM9GjXqh/9k0Rl
/P536bXrwRHEGxrvx/PuhtBe5QJQXfVNlA/pLCSAmKdNh0b9KrdYFnESGAKGl+14WFLBuQKLGw2+
rEsKImQ7hV3e7MU2sgjpoAbeXM4yz2EIMht7zvvP9xnymu+ZRjIGDjOfASIjS5E94F4BT/JdEz5c
MrX7hKRveAfFAz2rytwI7koHWDrJGu2dOWm13SWJMyAVunxDLdUw7KXmPzr2QVnatNQQhn+DG6TN
m7six8xKtrr0GFbwTkXcDWqKeuqhy/2lOHg7xEpdU4i911SBoUQHX8sXqrlP6YtQUrYkoxg7jZIh
f2Xw97O1fSl7cep2c3GXykQsFS1GNZGlWNErb/7d7WpnTiUJ0yur0obS2CwfK99lT/X+yEdg8D1h
CRacnozXt/OBtlU2XEub7jKyD7hPMAGDdPpym1XDFUxZMUP55k8emWMdRfPe8gDpKOEkI3DNymwX
HAPCHh4WUhw2L2SUyzMEyA6UGncdpWy2NRxR4Td2AdTHGdIZYEh7jm37ouj/1wO+lyCFWrmcLWYF
oDY0Zca/PGd9XDkDVdO1
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
