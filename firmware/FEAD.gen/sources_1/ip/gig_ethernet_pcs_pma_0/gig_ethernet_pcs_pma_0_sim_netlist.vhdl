-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- Copyright 2022-2024 Advanced Micro Devices, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2024.1.1 (lin64) Build 5094488 Fri Jun 14 08:57:50 MDT 2024
-- Date        : Tue Jul 30 12:07:50 2024
-- Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 38 (Thirty Eight)
-- Command     : write_vhdl -force -mode funcsim
--               /data/cms/ecal/fe/fead_v2/firmware.2023.2/FEAD.gen/sources_1/ip/gig_ethernet_pcs_pma_0/gig_ethernet_pcs_pma_0_sim_netlist.vhdl
-- Design      : gig_ethernet_pcs_pma_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7k70tfbg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_GTWIZARD_GT is
  port (
    gtxe2_i_0 : out STD_LOGIC;
    gt0_cpllrefclklost_i : out STD_LOGIC;
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    gtxe2_i_1 : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    gtxe2_i_2 : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_4 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_5 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_7 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    cpll_pd0_i : in STD_LOGIC;
    cpllreset_in : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_gttxreset_in0_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gt0_rxuserrdy_t : in STD_LOGIC;
    gtxe2_i_8 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_txuserrdy_t : in STD_LOGIC;
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_9 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_10 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_11 : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end gig_ethernet_pcs_pma_0_GTWIZARD_GT;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_GTWIZARD_GT is
  signal gtxe2_i_n_0 : STD_LOGIC;
  signal gtxe2_i_n_10 : STD_LOGIC;
  signal gtxe2_i_n_16 : STD_LOGIC;
  signal gtxe2_i_n_170 : STD_LOGIC;
  signal gtxe2_i_n_171 : STD_LOGIC;
  signal gtxe2_i_n_172 : STD_LOGIC;
  signal gtxe2_i_n_173 : STD_LOGIC;
  signal gtxe2_i_n_174 : STD_LOGIC;
  signal gtxe2_i_n_175 : STD_LOGIC;
  signal gtxe2_i_n_176 : STD_LOGIC;
  signal gtxe2_i_n_177 : STD_LOGIC;
  signal gtxe2_i_n_178 : STD_LOGIC;
  signal gtxe2_i_n_179 : STD_LOGIC;
  signal gtxe2_i_n_180 : STD_LOGIC;
  signal gtxe2_i_n_181 : STD_LOGIC;
  signal gtxe2_i_n_182 : STD_LOGIC;
  signal gtxe2_i_n_183 : STD_LOGIC;
  signal gtxe2_i_n_184 : STD_LOGIC;
  signal gtxe2_i_n_27 : STD_LOGIC;
  signal gtxe2_i_n_3 : STD_LOGIC;
  signal gtxe2_i_n_38 : STD_LOGIC;
  signal gtxe2_i_n_39 : STD_LOGIC;
  signal gtxe2_i_n_4 : STD_LOGIC;
  signal gtxe2_i_n_46 : STD_LOGIC;
  signal gtxe2_i_n_47 : STD_LOGIC;
  signal gtxe2_i_n_48 : STD_LOGIC;
  signal gtxe2_i_n_49 : STD_LOGIC;
  signal gtxe2_i_n_50 : STD_LOGIC;
  signal gtxe2_i_n_51 : STD_LOGIC;
  signal gtxe2_i_n_52 : STD_LOGIC;
  signal gtxe2_i_n_53 : STD_LOGIC;
  signal gtxe2_i_n_54 : STD_LOGIC;
  signal gtxe2_i_n_55 : STD_LOGIC;
  signal gtxe2_i_n_56 : STD_LOGIC;
  signal gtxe2_i_n_57 : STD_LOGIC;
  signal gtxe2_i_n_58 : STD_LOGIC;
  signal gtxe2_i_n_59 : STD_LOGIC;
  signal gtxe2_i_n_60 : STD_LOGIC;
  signal gtxe2_i_n_61 : STD_LOGIC;
  signal gtxe2_i_n_81 : STD_LOGIC;
  signal gtxe2_i_n_83 : STD_LOGIC;
  signal gtxe2_i_n_84 : STD_LOGIC;
  signal gtxe2_i_n_9 : STD_LOGIC;
  signal NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_PHYSTATUS_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXDATAVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXELECIDLE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXQPISENN_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXQPISENP_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXRATEDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXQPISENN_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXQPISENP_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXRATEDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXCHARISK_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXCHBONDO_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gtxe2_i_RXDATA_UNCONNECTED : STD_LOGIC_VECTOR ( 63 downto 16 );
  signal NLW_gtxe2_i_RXDISPERR_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXHEADER_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gtxe2_i_RXSTATUS_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_gtxe2_i_TSTOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  attribute box_type : string;
  attribute box_type of gtxe2_i : label is "PRIMITIVE";
begin
gtxe2_i: unisim.vcomponents.GTXE2_CHANNEL
    generic map(
      ALIGN_COMMA_DOUBLE => "FALSE",
      ALIGN_COMMA_ENABLE => B"0001111111",
      ALIGN_COMMA_WORD => 2,
      ALIGN_MCOMMA_DET => "TRUE",
      ALIGN_MCOMMA_VALUE => B"1010000011",
      ALIGN_PCOMMA_DET => "TRUE",
      ALIGN_PCOMMA_VALUE => B"0101111100",
      CBCC_DATA_SOURCE_SEL => "DECODED",
      CHAN_BOND_KEEP_ALIGN => "FALSE",
      CHAN_BOND_MAX_SKEW => 1,
      CHAN_BOND_SEQ_1_1 => B"0000000000",
      CHAN_BOND_SEQ_1_2 => B"0000000000",
      CHAN_BOND_SEQ_1_3 => B"0000000000",
      CHAN_BOND_SEQ_1_4 => B"0000000000",
      CHAN_BOND_SEQ_1_ENABLE => B"1111",
      CHAN_BOND_SEQ_2_1 => B"0000000000",
      CHAN_BOND_SEQ_2_2 => B"0000000000",
      CHAN_BOND_SEQ_2_3 => B"0000000000",
      CHAN_BOND_SEQ_2_4 => B"0000000000",
      CHAN_BOND_SEQ_2_ENABLE => B"1111",
      CHAN_BOND_SEQ_2_USE => "FALSE",
      CHAN_BOND_SEQ_LEN => 1,
      CLK_CORRECT_USE => "TRUE",
      CLK_COR_KEEP_IDLE => "FALSE",
      CLK_COR_MAX_LAT => 36,
      CLK_COR_MIN_LAT => 33,
      CLK_COR_PRECEDENCE => "TRUE",
      CLK_COR_REPEAT_WAIT => 0,
      CLK_COR_SEQ_1_1 => B"0110111100",
      CLK_COR_SEQ_1_2 => B"0001010000",
      CLK_COR_SEQ_1_3 => B"0000000000",
      CLK_COR_SEQ_1_4 => B"0000000000",
      CLK_COR_SEQ_1_ENABLE => B"1111",
      CLK_COR_SEQ_2_1 => B"0110111100",
      CLK_COR_SEQ_2_2 => B"0010110101",
      CLK_COR_SEQ_2_3 => B"0000000000",
      CLK_COR_SEQ_2_4 => B"0000000000",
      CLK_COR_SEQ_2_ENABLE => B"1111",
      CLK_COR_SEQ_2_USE => "TRUE",
      CLK_COR_SEQ_LEN => 2,
      CPLL_CFG => X"BC07DC",
      CPLL_FBDIV => 4,
      CPLL_FBDIV_45 => 5,
      CPLL_INIT_CFG => X"00001E",
      CPLL_LOCK_CFG => X"01E8",
      CPLL_REFCLK_DIV => 1,
      DEC_MCOMMA_DETECT => "TRUE",
      DEC_PCOMMA_DETECT => "TRUE",
      DEC_VALID_COMMA_ONLY => "FALSE",
      DMONITOR_CFG => X"000A00",
      ES_CONTROL => B"000000",
      ES_ERRDET_EN => "FALSE",
      ES_EYE_SCAN_EN => "TRUE",
      ES_HORZ_OFFSET => X"000",
      ES_PMA_CFG => B"0000000000",
      ES_PRESCALE => B"00000",
      ES_QUALIFIER => X"00000000000000000000",
      ES_QUAL_MASK => X"00000000000000000000",
      ES_SDATA_MASK => X"00000000000000000000",
      ES_VERT_OFFSET => B"000000000",
      FTS_DESKEW_SEQ_ENABLE => B"1111",
      FTS_LANE_DESKEW_CFG => B"1111",
      FTS_LANE_DESKEW_EN => "FALSE",
      GEARBOX_MODE => B"000",
      IS_CPLLLOCKDETCLK_INVERTED => '0',
      IS_DRPCLK_INVERTED => '0',
      IS_GTGREFCLK_INVERTED => '0',
      IS_RXUSRCLK2_INVERTED => '0',
      IS_RXUSRCLK_INVERTED => '0',
      IS_TXPHDLYTSTCLK_INVERTED => '0',
      IS_TXUSRCLK2_INVERTED => '0',
      IS_TXUSRCLK_INVERTED => '0',
      OUTREFCLK_SEL_INV => B"11",
      PCS_PCIE_EN => "FALSE",
      PCS_RSVD_ATTR => X"000000000000",
      PD_TRANS_TIME_FROM_P2 => X"03C",
      PD_TRANS_TIME_NONE_P2 => X"19",
      PD_TRANS_TIME_TO_P2 => X"64",
      PMA_RSV => X"00018480",
      PMA_RSV2 => X"2050",
      PMA_RSV3 => B"00",
      PMA_RSV4 => X"00000000",
      RXBUFRESET_TIME => B"00001",
      RXBUF_ADDR_MODE => "FULL",
      RXBUF_EIDLE_HI_CNT => B"1000",
      RXBUF_EIDLE_LO_CNT => B"0000",
      RXBUF_EN => "TRUE",
      RXBUF_RESET_ON_CB_CHANGE => "TRUE",
      RXBUF_RESET_ON_COMMAALIGN => "FALSE",
      RXBUF_RESET_ON_EIDLE => "FALSE",
      RXBUF_RESET_ON_RATE_CHANGE => "TRUE",
      RXBUF_THRESH_OVFLW => 61,
      RXBUF_THRESH_OVRD => "FALSE",
      RXBUF_THRESH_UNDFLW => 8,
      RXCDRFREQRESET_TIME => B"00001",
      RXCDRPHRESET_TIME => B"00001",
      RXCDR_CFG => X"03000023FF10100020",
      RXCDR_FR_RESET_ON_EIDLE => '0',
      RXCDR_HOLD_DURING_EIDLE => '0',
      RXCDR_LOCK_CFG => B"010101",
      RXCDR_PH_RESET_ON_EIDLE => '0',
      RXDFELPMRESET_TIME => B"0001111",
      RXDLY_CFG => X"001F",
      RXDLY_LCFG => X"030",
      RXDLY_TAP_CFG => X"0000",
      RXGEARBOX_EN => "FALSE",
      RXISCANRESET_TIME => B"00001",
      RXLPM_HF_CFG => B"00000011110000",
      RXLPM_LF_CFG => B"00000011110000",
      RXOOB_CFG => B"0000110",
      RXOUT_DIV => 4,
      RXPCSRESET_TIME => B"00001",
      RXPHDLY_CFG => X"084020",
      RXPH_CFG => X"000000",
      RXPH_MONITOR_SEL => B"00000",
      RXPMARESET_TIME => B"00011",
      RXPRBS_ERR_LOOPBACK => '0',
      RXSLIDE_AUTO_WAIT => 7,
      RXSLIDE_MODE => "OFF",
      RX_BIAS_CFG => B"000000000100",
      RX_BUFFER_CFG => B"000000",
      RX_CLK25_DIV => 5,
      RX_CLKMUX_PD => '1',
      RX_CM_SEL => B"11",
      RX_CM_TRIM => B"010",
      RX_DATA_WIDTH => 20,
      RX_DDI_SEL => B"000000",
      RX_DEBUG_CFG => B"000000000000",
      RX_DEFER_RESET_BUF_EN => "TRUE",
      RX_DFE_GAIN_CFG => X"020FEA",
      RX_DFE_H2_CFG => B"000000000000",
      RX_DFE_H3_CFG => B"000001000000",
      RX_DFE_H4_CFG => B"00011110000",
      RX_DFE_H5_CFG => B"00011100000",
      RX_DFE_KL_CFG => B"0000011111110",
      RX_DFE_KL_CFG2 => X"301148AC",
      RX_DFE_LPM_CFG => X"0904",
      RX_DFE_LPM_HOLD_DURING_EIDLE => '0',
      RX_DFE_UT_CFG => B"10001111000000000",
      RX_DFE_VP_CFG => B"00011111100000011",
      RX_DFE_XYD_CFG => B"0000000000000",
      RX_DISPERR_SEQ_MATCH => "TRUE",
      RX_INT_DATAWIDTH => 0,
      RX_OS_CFG => B"0000010000000",
      RX_SIG_VALID_DLY => 10,
      RX_XCLK_SEL => "RXREC",
      SAS_MAX_COM => 64,
      SAS_MIN_COM => 36,
      SATA_BURST_SEQ_LEN => B"0101",
      SATA_BURST_VAL => B"100",
      SATA_CPLL_CFG => "VCO_3000MHZ",
      SATA_EIDLE_VAL => B"100",
      SATA_MAX_BURST => 8,
      SATA_MAX_INIT => 21,
      SATA_MAX_WAKE => 7,
      SATA_MIN_BURST => 4,
      SATA_MIN_INIT => 12,
      SATA_MIN_WAKE => 4,
      SHOW_REALIGN_COMMA => "TRUE",
      SIM_CPLLREFCLK_SEL => B"001",
      SIM_RECEIVER_DETECT_PASS => "TRUE",
      SIM_RESET_SPEEDUP => "TRUE",
      SIM_TX_EIDLE_DRIVE_LEVEL => "X",
      SIM_VERSION => "4.0",
      TERM_RCAL_CFG => B"10000",
      TERM_RCAL_OVRD => '0',
      TRANS_TIME_RATE => X"0E",
      TST_RSV => X"00000000",
      TXBUF_EN => "TRUE",
      TXBUF_RESET_ON_RATE_CHANGE => "TRUE",
      TXDLY_CFG => X"001F",
      TXDLY_LCFG => X"030",
      TXDLY_TAP_CFG => X"0000",
      TXGEARBOX_EN => "FALSE",
      TXOUT_DIV => 4,
      TXPCSRESET_TIME => B"00001",
      TXPHDLY_CFG => X"084020",
      TXPH_CFG => X"0780",
      TXPH_MONITOR_SEL => B"00000",
      TXPMARESET_TIME => B"00001",
      TX_CLK25_DIV => 5,
      TX_CLKMUX_PD => '1',
      TX_DATA_WIDTH => 20,
      TX_DEEMPH0 => B"00000",
      TX_DEEMPH1 => B"00000",
      TX_DRIVE_MODE => "DIRECT",
      TX_EIDLE_ASSERT_DELAY => B"110",
      TX_EIDLE_DEASSERT_DELAY => B"100",
      TX_INT_DATAWIDTH => 0,
      TX_LOOPBACK_DRIVE_HIZ => "FALSE",
      TX_MAINCURSOR_SEL => '0',
      TX_MARGIN_FULL_0 => B"1001110",
      TX_MARGIN_FULL_1 => B"1001001",
      TX_MARGIN_FULL_2 => B"1000101",
      TX_MARGIN_FULL_3 => B"1000010",
      TX_MARGIN_FULL_4 => B"1000000",
      TX_MARGIN_LOW_0 => B"1000110",
      TX_MARGIN_LOW_1 => B"1000100",
      TX_MARGIN_LOW_2 => B"1000010",
      TX_MARGIN_LOW_3 => B"1000000",
      TX_MARGIN_LOW_4 => B"1000000",
      TX_PREDRIVER_MODE => '0',
      TX_QPI_STATUS_EN => '0',
      TX_RXDETECT_CFG => X"1832",
      TX_RXDETECT_REF => B"100",
      TX_XCLK_SEL => "TXOUT",
      UCODEER_CLR => '0'
    )
        port map (
      CFGRESET => '0',
      CLKRSVD(3 downto 0) => B"0000",
      CPLLFBCLKLOST => gtxe2_i_n_0,
      CPLLLOCK => gtxe2_i_0,
      CPLLLOCKDETCLK => independent_clock_bufg,
      CPLLLOCKEN => '1',
      CPLLPD => cpll_pd0_i,
      CPLLREFCLKLOST => gt0_cpllrefclklost_i,
      CPLLREFCLKSEL(2 downto 0) => B"001",
      CPLLRESET => cpllreset_in,
      DMONITOROUT(7) => gtxe2_i_n_177,
      DMONITOROUT(6) => gtxe2_i_n_178,
      DMONITOROUT(5) => gtxe2_i_n_179,
      DMONITOROUT(4) => gtxe2_i_n_180,
      DMONITOROUT(3) => gtxe2_i_n_181,
      DMONITOROUT(2) => gtxe2_i_n_182,
      DMONITOROUT(1) => gtxe2_i_n_183,
      DMONITOROUT(0) => gtxe2_i_n_184,
      DRPADDR(8 downto 0) => B"000000000",
      DRPCLK => gtrefclk_bufg,
      DRPDI(15 downto 0) => B"0000000000000000",
      DRPDO(15) => gtxe2_i_n_46,
      DRPDO(14) => gtxe2_i_n_47,
      DRPDO(13) => gtxe2_i_n_48,
      DRPDO(12) => gtxe2_i_n_49,
      DRPDO(11) => gtxe2_i_n_50,
      DRPDO(10) => gtxe2_i_n_51,
      DRPDO(9) => gtxe2_i_n_52,
      DRPDO(8) => gtxe2_i_n_53,
      DRPDO(7) => gtxe2_i_n_54,
      DRPDO(6) => gtxe2_i_n_55,
      DRPDO(5) => gtxe2_i_n_56,
      DRPDO(4) => gtxe2_i_n_57,
      DRPDO(3) => gtxe2_i_n_58,
      DRPDO(2) => gtxe2_i_n_59,
      DRPDO(1) => gtxe2_i_n_60,
      DRPDO(0) => gtxe2_i_n_61,
      DRPEN => '0',
      DRPRDY => gtxe2_i_n_3,
      DRPWE => '0',
      EYESCANDATAERROR => gtxe2_i_n_4,
      EYESCANMODE => '0',
      EYESCANRESET => '0',
      EYESCANTRIGGER => '0',
      GTGREFCLK => '0',
      GTNORTHREFCLK0 => '0',
      GTNORTHREFCLK1 => '0',
      GTREFCLK0 => gtrefclk_out,
      GTREFCLK1 => '0',
      GTREFCLKMONITOR => NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED,
      GTRESETSEL => '0',
      GTRSVD(15 downto 0) => B"0000000000000000",
      GTRXRESET => SR(0),
      GTSOUTHREFCLK0 => '0',
      GTSOUTHREFCLK1 => '0',
      GTTXRESET => gt0_gttxreset_in0_out,
      GTXRXN => rxn,
      GTXRXP => rxp,
      GTXTXN => txn,
      GTXTXP => txp,
      LOOPBACK(2 downto 0) => B"000",
      PCSRSVDIN(15 downto 0) => B"0000000000000000",
      PCSRSVDIN2(4 downto 0) => B"00000",
      PCSRSVDOUT(15 downto 0) => NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED(15 downto 0),
      PHYSTATUS => NLW_gtxe2_i_PHYSTATUS_UNCONNECTED,
      PMARSVDIN(4 downto 0) => B"00000",
      PMARSVDIN2(4 downto 0) => B"00000",
      QPLLCLK => gt0_qplloutclk_out,
      QPLLREFCLK => gt0_qplloutrefclk_out,
      RESETOVRD => '0',
      RX8B10BEN => '1',
      RXBUFRESET => '0',
      RXBUFSTATUS(2) => RXBUFSTATUS(0),
      RXBUFSTATUS(1) => gtxe2_i_n_83,
      RXBUFSTATUS(0) => gtxe2_i_n_84,
      RXBYTEISALIGNED => gtxe2_i_n_9,
      RXBYTEREALIGN => gtxe2_i_n_10,
      RXCDRFREQRESET => '0',
      RXCDRHOLD => '0',
      RXCDRLOCK => NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED,
      RXCDROVRDEN => '0',
      RXCDRRESET => '0',
      RXCDRRESETRSV => '0',
      RXCHANBONDSEQ => NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED,
      RXCHANISALIGNED => NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED,
      RXCHANREALIGN => NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED,
      RXCHARISCOMMA(7 downto 2) => NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED(7 downto 2),
      RXCHARISCOMMA(1 downto 0) => gtxe2_i_4(1 downto 0),
      RXCHARISK(7 downto 2) => NLW_gtxe2_i_RXCHARISK_UNCONNECTED(7 downto 2),
      RXCHARISK(1 downto 0) => gtxe2_i_5(1 downto 0),
      RXCHBONDEN => '0',
      RXCHBONDI(4 downto 0) => B"00000",
      RXCHBONDLEVEL(2 downto 0) => B"000",
      RXCHBONDMASTER => '0',
      RXCHBONDO(4 downto 0) => NLW_gtxe2_i_RXCHBONDO_UNCONNECTED(4 downto 0),
      RXCHBONDSLAVE => '0',
      RXCLKCORCNT(1 downto 0) => D(1 downto 0),
      RXCOMINITDET => NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED,
      RXCOMMADET => gtxe2_i_n_16,
      RXCOMMADETEN => '1',
      RXCOMSASDET => NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED,
      RXCOMWAKEDET => NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED,
      RXDATA(63 downto 16) => NLW_gtxe2_i_RXDATA_UNCONNECTED(63 downto 16),
      RXDATA(15 downto 0) => gtxe2_i_3(15 downto 0),
      RXDATAVALID => NLW_gtxe2_i_RXDATAVALID_UNCONNECTED,
      RXDDIEN => '0',
      RXDFEAGCHOLD => '0',
      RXDFEAGCOVRDEN => '0',
      RXDFECM1EN => '0',
      RXDFELFHOLD => '0',
      RXDFELFOVRDEN => '0',
      RXDFELPMRESET => '0',
      RXDFETAP2HOLD => '0',
      RXDFETAP2OVRDEN => '0',
      RXDFETAP3HOLD => '0',
      RXDFETAP3OVRDEN => '0',
      RXDFETAP4HOLD => '0',
      RXDFETAP4OVRDEN => '0',
      RXDFETAP5HOLD => '0',
      RXDFETAP5OVRDEN => '0',
      RXDFEUTHOLD => '0',
      RXDFEUTOVRDEN => '0',
      RXDFEVPHOLD => '0',
      RXDFEVPOVRDEN => '0',
      RXDFEVSEN => '0',
      RXDFEXYDEN => '1',
      RXDFEXYDHOLD => '0',
      RXDFEXYDOVRDEN => '0',
      RXDISPERR(7 downto 2) => NLW_gtxe2_i_RXDISPERR_UNCONNECTED(7 downto 2),
      RXDISPERR(1 downto 0) => gtxe2_i_6(1 downto 0),
      RXDLYBYPASS => '1',
      RXDLYEN => '0',
      RXDLYOVRDEN => '0',
      RXDLYSRESET => '0',
      RXDLYSRESETDONE => NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED,
      RXELECIDLE => NLW_gtxe2_i_RXELECIDLE_UNCONNECTED,
      RXELECIDLEMODE(1 downto 0) => B"11",
      RXGEARBOXSLIP => '0',
      RXHEADER(2 downto 0) => NLW_gtxe2_i_RXHEADER_UNCONNECTED(2 downto 0),
      RXHEADERVALID => NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED,
      RXLPMEN => '1',
      RXLPMHFHOLD => '0',
      RXLPMHFOVRDEN => '0',
      RXLPMLFHOLD => '0',
      RXLPMLFKLOVRDEN => '0',
      RXMCOMMAALIGNEN => reset_out,
      RXMONITOROUT(6) => gtxe2_i_n_170,
      RXMONITOROUT(5) => gtxe2_i_n_171,
      RXMONITOROUT(4) => gtxe2_i_n_172,
      RXMONITOROUT(3) => gtxe2_i_n_173,
      RXMONITOROUT(2) => gtxe2_i_n_174,
      RXMONITOROUT(1) => gtxe2_i_n_175,
      RXMONITOROUT(0) => gtxe2_i_n_176,
      RXMONITORSEL(1 downto 0) => B"00",
      RXNOTINTABLE(7 downto 2) => NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED(7 downto 2),
      RXNOTINTABLE(1 downto 0) => gtxe2_i_7(1 downto 0),
      RXOOBRESET => '0',
      RXOSHOLD => '0',
      RXOSOVRDEN => '0',
      RXOUTCLK => rxoutclk,
      RXOUTCLKFABRIC => NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED,
      RXOUTCLKPCS => NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED,
      RXOUTCLKSEL(2 downto 0) => B"010",
      RXPCOMMAALIGNEN => reset_out,
      RXPCSRESET => reset,
      RXPD(1) => RXPD(0),
      RXPD(0) => RXPD(0),
      RXPHALIGN => '0',
      RXPHALIGNDONE => NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED,
      RXPHALIGNEN => '0',
      RXPHDLYPD => '0',
      RXPHDLYRESET => '0',
      RXPHMONITOR(4 downto 0) => NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED(4 downto 0),
      RXPHOVRDEN => '0',
      RXPHSLIPMONITOR(4 downto 0) => NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED(4 downto 0),
      RXPMARESET => '0',
      RXPOLARITY => '0',
      RXPRBSCNTRESET => '0',
      RXPRBSERR => gtxe2_i_n_27,
      RXPRBSSEL(2 downto 0) => B"000",
      RXQPIEN => '0',
      RXQPISENN => NLW_gtxe2_i_RXQPISENN_UNCONNECTED,
      RXQPISENP => NLW_gtxe2_i_RXQPISENP_UNCONNECTED,
      RXRATE(2 downto 0) => B"000",
      RXRATEDONE => NLW_gtxe2_i_RXRATEDONE_UNCONNECTED,
      RXRESETDONE => gtxe2_i_1,
      RXSLIDE => '0',
      RXSTARTOFSEQ => NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED,
      RXSTATUS(2 downto 0) => NLW_gtxe2_i_RXSTATUS_UNCONNECTED(2 downto 0),
      RXSYSCLKSEL(1 downto 0) => B"00",
      RXUSERRDY => gt0_rxuserrdy_t,
      RXUSRCLK => gtxe2_i_8,
      RXUSRCLK2 => gtxe2_i_8,
      RXVALID => NLW_gtxe2_i_RXVALID_UNCONNECTED,
      SETERRSTATUS => '0',
      TSTIN(19 downto 0) => B"11111111111111111111",
      TSTOUT(9 downto 0) => NLW_gtxe2_i_TSTOUT_UNCONNECTED(9 downto 0),
      TX8B10BBYPASS(7 downto 0) => B"00000000",
      TX8B10BEN => '1',
      TXBUFDIFFCTRL(2 downto 0) => B"100",
      TXBUFSTATUS(1) => TXBUFSTATUS(0),
      TXBUFSTATUS(0) => gtxe2_i_n_81,
      TXCHARDISPMODE(7 downto 2) => B"000000",
      TXCHARDISPMODE(1 downto 0) => gtxe2_i_9(1 downto 0),
      TXCHARDISPVAL(7 downto 2) => B"000000",
      TXCHARDISPVAL(1 downto 0) => gtxe2_i_10(1 downto 0),
      TXCHARISK(7 downto 2) => B"000000",
      TXCHARISK(1 downto 0) => gtxe2_i_11(1 downto 0),
      TXCOMFINISH => NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED,
      TXCOMINIT => '0',
      TXCOMSAS => '0',
      TXCOMWAKE => '0',
      TXDATA(63 downto 16) => B"000000000000000000000000000000000000000000000000",
      TXDATA(15 downto 0) => Q(15 downto 0),
      TXDEEMPH => '0',
      TXDETECTRX => '0',
      TXDIFFCTRL(3 downto 0) => B"1000",
      TXDIFFPD => '0',
      TXDLYBYPASS => '1',
      TXDLYEN => '0',
      TXDLYHOLD => '0',
      TXDLYOVRDEN => '0',
      TXDLYSRESET => '0',
      TXDLYSRESETDONE => NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED,
      TXDLYUPDOWN => '0',
      TXELECIDLE => TXPD(0),
      TXGEARBOXREADY => NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED,
      TXHEADER(2 downto 0) => B"000",
      TXINHIBIT => '0',
      TXMAINCURSOR(6 downto 0) => B"0000000",
      TXMARGIN(2 downto 0) => B"000",
      TXOUTCLK => txoutclk,
      TXOUTCLKFABRIC => gtxe2_i_n_38,
      TXOUTCLKPCS => gtxe2_i_n_39,
      TXOUTCLKSEL(2 downto 0) => B"100",
      TXPCSRESET => '0',
      TXPD(1) => TXPD(0),
      TXPD(0) => TXPD(0),
      TXPDELECIDLEMODE => '0',
      TXPHALIGN => '0',
      TXPHALIGNDONE => NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED,
      TXPHALIGNEN => '0',
      TXPHDLYPD => '0',
      TXPHDLYRESET => '0',
      TXPHDLYTSTCLK => '0',
      TXPHINIT => '0',
      TXPHINITDONE => NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED,
      TXPHOVRDEN => '0',
      TXPISOPD => '0',
      TXPMARESET => '0',
      TXPOLARITY => '0',
      TXPOSTCURSOR(4 downto 0) => B"00000",
      TXPOSTCURSORINV => '0',
      TXPRBSFORCEERR => '0',
      TXPRBSSEL(2 downto 0) => B"000",
      TXPRECURSOR(4 downto 0) => B"00000",
      TXPRECURSORINV => '0',
      TXQPIBIASEN => '0',
      TXQPISENN => NLW_gtxe2_i_TXQPISENN_UNCONNECTED,
      TXQPISENP => NLW_gtxe2_i_TXQPISENP_UNCONNECTED,
      TXQPISTRONGPDOWN => '0',
      TXQPIWEAKPUP => '0',
      TXRATE(2 downto 0) => B"000",
      TXRATEDONE => NLW_gtxe2_i_TXRATEDONE_UNCONNECTED,
      TXRESETDONE => gtxe2_i_2,
      TXSEQUENCE(6 downto 0) => B"0000000",
      TXSTARTSEQ => '0',
      TXSWING => '0',
      TXSYSCLKSEL(1 downto 0) => B"00",
      TXUSERRDY => gt0_txuserrdy_t,
      TXUSRCLK => gtxe2_i_8,
      TXUSRCLK2 => gtxe2_i_8
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_clocking is
  port (
    gtrefclk_out : out STD_LOGIC;
    gtrefclk_bufg : out STD_LOGIC;
    mmcm_locked : out STD_LOGIC;
    userclk : out STD_LOGIC;
    userclk2 : out STD_LOGIC;
    rxuserclk2_out : out STD_LOGIC;
    gtrefclk_p : in STD_LOGIC;
    gtrefclk_n : in STD_LOGIC;
    txoutclk : in STD_LOGIC;
    mmcm_reset : in STD_LOGIC;
    rxoutclk : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_clocking;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_clocking is
  signal clkfbout : STD_LOGIC;
  signal clkout0 : STD_LOGIC;
  signal clkout1 : STD_LOGIC;
  signal \^gtrefclk_out\ : STD_LOGIC;
  signal txoutclk_bufg : STD_LOGIC;
  signal NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_PSDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute box_type : string;
  attribute box_type of bufg_gtrefclk : label is "PRIMITIVE";
  attribute box_type of bufg_txoutclk : label is "PRIMITIVE";
  attribute box_type of bufg_userclk : label is "PRIMITIVE";
  attribute box_type of bufg_userclk2 : label is "PRIMITIVE";
  attribute box_type of ibufds_gtrefclk : label is "PRIMITIVE";
  attribute box_type of mmcm_adv_inst : label is "PRIMITIVE";
  attribute box_type of rxrecclkbufg : label is "PRIMITIVE";
begin
  gtrefclk_out <= \^gtrefclk_out\;
bufg_gtrefclk: unisim.vcomponents.BUFG
     port map (
      I => \^gtrefclk_out\,
      O => gtrefclk_bufg
    );
bufg_txoutclk: unisim.vcomponents.BUFG
     port map (
      I => txoutclk,
      O => txoutclk_bufg
    );
bufg_userclk: unisim.vcomponents.BUFG
     port map (
      I => clkout1,
      O => userclk
    );
bufg_userclk2: unisim.vcomponents.BUFG
     port map (
      I => clkout0,
      O => userclk2
    );
ibufds_gtrefclk: unisim.vcomponents.IBUFDS_GTE2
    generic map(
      CLKCM_CFG => true,
      CLKRCV_TRST => true,
      CLKSWING_CFG => B"11"
    )
        port map (
      CEB => '0',
      I => gtrefclk_p,
      IB => gtrefclk_n,
      O => \^gtrefclk_out\,
      ODIV2 => NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED
    );
mmcm_adv_inst: unisim.vcomponents.MMCME2_ADV
    generic map(
      BANDWIDTH => "OPTIMIZED",
      CLKFBOUT_MULT_F => 16.000000,
      CLKFBOUT_PHASE => 0.000000,
      CLKFBOUT_USE_FINE_PS => false,
      CLKIN1_PERIOD => 16.000000,
      CLKIN2_PERIOD => 0.000000,
      CLKOUT0_DIVIDE_F => 8.000000,
      CLKOUT0_DUTY_CYCLE => 0.500000,
      CLKOUT0_PHASE => 0.000000,
      CLKOUT0_USE_FINE_PS => false,
      CLKOUT1_DIVIDE => 16,
      CLKOUT1_DUTY_CYCLE => 0.500000,
      CLKOUT1_PHASE => 0.000000,
      CLKOUT1_USE_FINE_PS => false,
      CLKOUT2_DIVIDE => 1,
      CLKOUT2_DUTY_CYCLE => 0.500000,
      CLKOUT2_PHASE => 0.000000,
      CLKOUT2_USE_FINE_PS => false,
      CLKOUT3_DIVIDE => 1,
      CLKOUT3_DUTY_CYCLE => 0.500000,
      CLKOUT3_PHASE => 0.000000,
      CLKOUT3_USE_FINE_PS => false,
      CLKOUT4_CASCADE => false,
      CLKOUT4_DIVIDE => 1,
      CLKOUT4_DUTY_CYCLE => 0.500000,
      CLKOUT4_PHASE => 0.000000,
      CLKOUT4_USE_FINE_PS => false,
      CLKOUT5_DIVIDE => 1,
      CLKOUT5_DUTY_CYCLE => 0.500000,
      CLKOUT5_PHASE => 0.000000,
      CLKOUT5_USE_FINE_PS => false,
      CLKOUT6_DIVIDE => 1,
      CLKOUT6_DUTY_CYCLE => 0.500000,
      CLKOUT6_PHASE => 0.000000,
      CLKOUT6_USE_FINE_PS => false,
      COMPENSATION => "INTERNAL",
      DIVCLK_DIVIDE => 1,
      IS_CLKINSEL_INVERTED => '0',
      IS_PSEN_INVERTED => '0',
      IS_PSINCDEC_INVERTED => '0',
      IS_PWRDWN_INVERTED => '0',
      IS_RST_INVERTED => '0',
      REF_JITTER1 => 0.010000,
      REF_JITTER2 => 0.000000,
      SS_EN => "FALSE",
      SS_MODE => "CENTER_HIGH",
      SS_MOD_PERIOD => 10000,
      STARTUP_WAIT => false
    )
        port map (
      CLKFBIN => clkfbout,
      CLKFBOUT => clkfbout,
      CLKFBOUTB => NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED,
      CLKFBSTOPPED => NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED,
      CLKIN1 => txoutclk_bufg,
      CLKIN2 => '0',
      CLKINSEL => '1',
      CLKINSTOPPED => NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED,
      CLKOUT0 => clkout0,
      CLKOUT0B => NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED,
      CLKOUT1 => clkout1,
      CLKOUT1B => NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED,
      CLKOUT2 => NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED,
      CLKOUT2B => NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED,
      CLKOUT3 => NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED,
      CLKOUT3B => NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED,
      CLKOUT4 => NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED,
      CLKOUT5 => NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED,
      CLKOUT6 => NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED,
      DADDR(6 downto 0) => B"0000000",
      DCLK => '0',
      DEN => '0',
      DI(15 downto 0) => B"0000000000000000",
      DO(15 downto 0) => NLW_mmcm_adv_inst_DO_UNCONNECTED(15 downto 0),
      DRDY => NLW_mmcm_adv_inst_DRDY_UNCONNECTED,
      DWE => '0',
      LOCKED => mmcm_locked,
      PSCLK => '0',
      PSDONE => NLW_mmcm_adv_inst_PSDONE_UNCONNECTED,
      PSEN => '0',
      PSINCDEC => '0',
      PWRDWN => '0',
      RST => mmcm_reset
    );
rxrecclkbufg: unisim.vcomponents.BUFG
     port map (
      I => rxoutclk,
      O => rxuserclk2_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_cpll_railing is
  port (
    cpll_pd0_i : out STD_LOGIC;
    cpllreset_in : out STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gt0_cpllreset_t : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_cpll_railing;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_cpll_railing is
  signal cpll_reset_out : STD_LOGIC;
  signal \cpllpd_wait_reg[31]_srl32_n_1\ : STD_LOGIC;
  signal \cpllpd_wait_reg[63]_srl32_n_1\ : STD_LOGIC;
  signal \cpllpd_wait_reg[94]_srl31_n_0\ : STD_LOGIC;
  signal \cpllreset_wait_reg[126]_srl31_n_0\ : STD_LOGIC;
  signal \cpllreset_wait_reg[31]_srl32_n_1\ : STD_LOGIC;
  signal \cpllreset_wait_reg[63]_srl32_n_1\ : STD_LOGIC;
  signal \cpllreset_wait_reg[95]_srl32_n_1\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  attribute srl_bus_name : string;
  attribute srl_bus_name of \cpllpd_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name : string;
  attribute srl_name of \cpllpd_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[31]_srl32 ";
  attribute srl_bus_name of \cpllpd_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name of \cpllpd_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[63]_srl32 ";
  attribute srl_bus_name of \cpllpd_wait_reg[94]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name of \cpllpd_wait_reg[94]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[94]_srl31 ";
  attribute equivalent_register_removal : string;
  attribute equivalent_register_removal of \cpllpd_wait_reg[95]\ : label is "no";
  attribute srl_bus_name of \cpllreset_wait_reg[126]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[126]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[126]_srl31 ";
  attribute equivalent_register_removal of \cpllreset_wait_reg[127]\ : label is "no";
  attribute srl_bus_name of \cpllreset_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[31]_srl32 ";
  attribute srl_bus_name of \cpllreset_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[63]_srl32 ";
  attribute srl_bus_name of \cpllreset_wait_reg[95]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[95]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[95]_srl32 ";
begin
\cpllpd_wait_reg[31]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"FFFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => '0',
      Q => \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllpd_wait_reg[31]_srl32_n_1\
    );
\cpllpd_wait_reg[63]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"FFFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllpd_wait_reg[31]_srl32_n_1\,
      Q => \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllpd_wait_reg[63]_srl32_n_1\
    );
\cpllpd_wait_reg[94]_srl31\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11110",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllpd_wait_reg[63]_srl32_n_1\,
      Q => \cpllpd_wait_reg[94]_srl31_n_0\,
      Q31 => \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED\
    );
\cpllpd_wait_reg[95]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => gtrefclk_bufg,
      CE => '1',
      D => \cpllpd_wait_reg[94]_srl31_n_0\,
      Q => cpll_pd0_i,
      R => '0'
    );
\cpllreset_wait_reg[126]_srl31\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11110",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllreset_wait_reg[95]_srl32_n_1\,
      Q => \cpllreset_wait_reg[126]_srl31_n_0\,
      Q31 => \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED\
    );
\cpllreset_wait_reg[127]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gtrefclk_bufg,
      CE => '1',
      D => \cpllreset_wait_reg[126]_srl31_n_0\,
      Q => cpll_reset_out,
      R => '0'
    );
\cpllreset_wait_reg[31]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"000000FF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => '0',
      Q => \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[31]_srl32_n_1\
    );
\cpllreset_wait_reg[63]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllreset_wait_reg[31]_srl32_n_1\,
      Q => \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[63]_srl32_n_1\
    );
\cpllreset_wait_reg[95]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllreset_wait_reg[63]_srl32_n_1\,
      Q => \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[95]_srl32_n_1\
    );
gtxe2_i_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => cpll_reset_out,
      I1 => gt0_cpllreset_t,
      O => cpllreset_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_gt_common is
  port (
    gt0_qplloutclk_out : out STD_LOGIC;
    gt0_qplloutrefclk_out : out STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end gig_ethernet_pcs_pma_0_gt_common;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_gt_common is
  signal gtxe2_common_i_n_2 : STD_LOGIC;
  signal gtxe2_common_i_n_5 : STD_LOGIC;
  signal NLW_gtxe2_common_i_DRPRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_common_i_DRPDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute box_type : string;
  attribute box_type of gtxe2_common_i : label is "PRIMITIVE";
begin
gtxe2_common_i: unisim.vcomponents.GTXE2_COMMON
    generic map(
      BIAS_CFG => X"0000040000001000",
      COMMON_CFG => X"00000000",
      IS_DRPCLK_INVERTED => '0',
      IS_GTGREFCLK_INVERTED => '0',
      IS_QPLLLOCKDETCLK_INVERTED => '0',
      QPLL_CFG => X"06801C1",
      QPLL_CLKOUT_CFG => B"0000",
      QPLL_COARSE_FREQ_OVRD => B"010000",
      QPLL_COARSE_FREQ_OVRD_EN => '0',
      QPLL_CP => B"0000011111",
      QPLL_CP_MONITOR_EN => '0',
      QPLL_DMONITOR_SEL => '0',
      QPLL_FBDIV => B"0000100000",
      QPLL_FBDIV_MONITOR_EN => '0',
      QPLL_FBDIV_RATIO => '1',
      QPLL_INIT_CFG => X"000006",
      QPLL_LOCK_CFG => X"21E8",
      QPLL_LPF => B"1111",
      QPLL_REFCLK_DIV => 1,
      SIM_QPLLREFCLK_SEL => B"001",
      SIM_RESET_SPEEDUP => "FALSE",
      SIM_VERSION => "4.0"
    )
        port map (
      BGBYPASSB => '1',
      BGMONITORENB => '1',
      BGPDB => '1',
      BGRCALOVRD(4 downto 0) => B"11111",
      DRPADDR(7 downto 0) => B"00000000",
      DRPCLK => '0',
      DRPDI(15 downto 0) => B"0000000000000000",
      DRPDO(15 downto 0) => NLW_gtxe2_common_i_DRPDO_UNCONNECTED(15 downto 0),
      DRPEN => '0',
      DRPRDY => NLW_gtxe2_common_i_DRPRDY_UNCONNECTED,
      DRPWE => '0',
      GTGREFCLK => '0',
      GTNORTHREFCLK0 => '0',
      GTNORTHREFCLK1 => '0',
      GTREFCLK0 => gtrefclk_out,
      GTREFCLK1 => '0',
      GTSOUTHREFCLK0 => '0',
      GTSOUTHREFCLK1 => '0',
      PMARSVD(7 downto 0) => B"00000000",
      QPLLDMONITOR(7 downto 0) => NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED(7 downto 0),
      QPLLFBCLKLOST => NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED,
      QPLLLOCK => gtxe2_common_i_n_2,
      QPLLLOCKDETCLK => independent_clock_bufg,
      QPLLLOCKEN => '1',
      QPLLOUTCLK => gt0_qplloutclk_out,
      QPLLOUTREFCLK => gt0_qplloutrefclk_out,
      QPLLOUTRESET => '0',
      QPLLPD => '1',
      QPLLREFCLKLOST => gtxe2_common_i_n_5,
      QPLLREFCLKSEL(2 downto 0) => B"001",
      QPLLRESET => \out\(0),
      QPLLRSVD1(15 downto 0) => B"0000000000000000",
      QPLLRSVD2(4 downto 0) => B"11111",
      RCALENB => '1',
      REFCLKOUTMONITOR => NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_reset_sync is
  port (
    reset_out : out STD_LOGIC;
    CLK : in STD_LOGIC;
    enablealign : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_reset_sync;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_reset_sync is
  signal reset_sync_reg1 : STD_LOGIC;
  signal reset_sync_reg2 : STD_LOGIC;
  signal reset_sync_reg3 : STD_LOGIC;
  signal reset_sync_reg4 : STD_LOGIC;
  signal reset_sync_reg5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of reset_sync1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of reset_sync1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of reset_sync1 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of reset_sync1 : label is "VCC:CE";
  attribute box_type : string;
  attribute box_type of reset_sync1 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync2 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync2 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync2 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync2 : label is "VCC:CE";
  attribute box_type of reset_sync2 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync3 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync3 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync3 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync3 : label is "VCC:CE";
  attribute box_type of reset_sync3 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync4 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync4 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync4 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync4 : label is "VCC:CE";
  attribute box_type of reset_sync4 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync5 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync5 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync5 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync5 : label is "VCC:CE";
  attribute box_type of reset_sync5 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync6 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync6 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync6 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync6 : label is "VCC:CE";
  attribute box_type of reset_sync6 : label is "PRIMITIVE";
begin
reset_sync1: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => '0',
      PRE => enablealign,
      Q => reset_sync_reg1
    );
reset_sync2: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg1,
      PRE => enablealign,
      Q => reset_sync_reg2
    );
reset_sync3: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg2,
      PRE => enablealign,
      Q => reset_sync_reg3
    );
reset_sync4: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg3,
      PRE => enablealign,
      Q => reset_sync_reg4
    );
reset_sync5: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg4,
      PRE => enablealign,
      Q => reset_sync_reg5
    );
reset_sync6: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg5,
      PRE => '0',
      Q => reset_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_reset_sync_1 is
  port (
    reset_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_reset_sync_1 : entity is "gig_ethernet_pcs_pma_0_reset_sync";
end gig_ethernet_pcs_pma_0_reset_sync_1;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_reset_sync_1 is
  signal reset_sync_reg1 : STD_LOGIC;
  signal reset_sync_reg2 : STD_LOGIC;
  signal reset_sync_reg3 : STD_LOGIC;
  signal reset_sync_reg4 : STD_LOGIC;
  signal reset_sync_reg5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of reset_sync1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of reset_sync1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of reset_sync1 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of reset_sync1 : label is "VCC:CE";
  attribute box_type : string;
  attribute box_type of reset_sync1 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync2 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync2 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync2 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync2 : label is "VCC:CE";
  attribute box_type of reset_sync2 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync3 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync3 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync3 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync3 : label is "VCC:CE";
  attribute box_type of reset_sync3 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync4 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync4 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync4 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync4 : label is "VCC:CE";
  attribute box_type of reset_sync4 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync5 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync5 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync5 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync5 : label is "VCC:CE";
  attribute box_type of reset_sync5 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync6 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync6 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync6 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync6 : label is "VCC:CE";
  attribute box_type of reset_sync6 : label is "PRIMITIVE";
begin
reset_sync1: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => '0',
      PRE => SR(0),
      Q => reset_sync_reg1
    );
reset_sync2: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg1,
      PRE => SR(0),
      Q => reset_sync_reg2
    );
reset_sync3: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg2,
      PRE => SR(0),
      Q => reset_sync_reg3
    );
reset_sync4: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg3,
      PRE => SR(0),
      Q => reset_sync_reg4
    );
reset_sync5: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg4,
      PRE => SR(0),
      Q => reset_sync_reg5
    );
reset_sync6: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg5,
      PRE => '0',
      Q => reset_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_reset_sync_2 is
  port (
    reset_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    reset_sync5_0 : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_reset_sync_2 : entity is "gig_ethernet_pcs_pma_0_reset_sync";
end gig_ethernet_pcs_pma_0_reset_sync_2;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_reset_sync_2 is
  signal reset_sync_reg1 : STD_LOGIC;
  signal reset_sync_reg2 : STD_LOGIC;
  signal reset_sync_reg3 : STD_LOGIC;
  signal reset_sync_reg4 : STD_LOGIC;
  signal reset_sync_reg5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of reset_sync1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of reset_sync1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of reset_sync1 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of reset_sync1 : label is "VCC:CE";
  attribute box_type : string;
  attribute box_type of reset_sync1 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync2 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync2 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync2 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync2 : label is "VCC:CE";
  attribute box_type of reset_sync2 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync3 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync3 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync3 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync3 : label is "VCC:CE";
  attribute box_type of reset_sync3 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync4 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync4 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync4 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync4 : label is "VCC:CE";
  attribute box_type of reset_sync4 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync5 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync5 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync5 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync5 : label is "VCC:CE";
  attribute box_type of reset_sync5 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync6 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync6 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync6 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync6 : label is "VCC:CE";
  attribute box_type of reset_sync6 : label is "PRIMITIVE";
begin
reset_sync1: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => '0',
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg1
    );
reset_sync2: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg1,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg2
    );
reset_sync3: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg2,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg3
    );
reset_sync4: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg3,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg4
    );
reset_sync5: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg4,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg5
    );
reset_sync6: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg5,
      PRE => '0',
      Q => reset_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_reset_wtd_timer is
  port (
    reset : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    data_out : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_reset_wtd_timer;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_reset_wtd_timer is
  signal \counter_stg1[5]_i_1_n_0\ : STD_LOGIC;
  signal \counter_stg1[5]_i_3_n_0\ : STD_LOGIC;
  signal counter_stg1_reg : STD_LOGIC_VECTOR ( 5 to 5 );
  signal \counter_stg1_reg__0\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \counter_stg2[0]_i_3_n_0\ : STD_LOGIC;
  signal counter_stg2_reg : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal \counter_stg2_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal counter_stg30 : STD_LOGIC;
  signal \counter_stg3[0]_i_3_n_0\ : STD_LOGIC;
  signal \counter_stg3[0]_i_4_n_0\ : STD_LOGIC;
  signal \counter_stg3[0]_i_5_n_0\ : STD_LOGIC;
  signal counter_stg3_reg : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal \counter_stg3_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal eqOp : STD_LOGIC;
  signal plusOp : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal reset0 : STD_LOGIC;
  signal reset_i_2_n_0 : STD_LOGIC;
  signal reset_i_3_n_0 : STD_LOGIC;
  signal reset_i_4_n_0 : STD_LOGIC;
  signal reset_i_5_n_0 : STD_LOGIC;
  signal reset_i_6_n_0 : STD_LOGIC;
  signal \NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \counter_stg1[0]_i_1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \counter_stg1[1]_i_1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \counter_stg1[2]_i_1\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \counter_stg1[3]_i_1\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \counter_stg1[4]_i_1\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \counter_stg1[5]_i_3\ : label is "soft_lutpair71";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \counter_stg2_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg2_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg2_reg[8]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg3_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg3_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg3_reg[8]_i_1\ : label is 11;
begin
\counter_stg1[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \counter_stg1_reg__0\(0),
      O => plusOp(0)
    );
\counter_stg1[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \counter_stg1_reg__0\(0),
      I1 => \counter_stg1_reg__0\(1),
      O => plusOp(1)
    );
\counter_stg1[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \counter_stg1_reg__0\(1),
      I1 => \counter_stg1_reg__0\(0),
      I2 => \counter_stg1_reg__0\(2),
      O => plusOp(2)
    );
\counter_stg1[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \counter_stg1_reg__0\(2),
      I1 => \counter_stg1_reg__0\(0),
      I2 => \counter_stg1_reg__0\(1),
      I3 => \counter_stg1_reg__0\(3),
      O => plusOp(3)
    );
\counter_stg1[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(3),
      I1 => \counter_stg1_reg__0\(1),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(2),
      I4 => \counter_stg1_reg__0\(4),
      O => plusOp(4)
    );
\counter_stg1[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF2000"
    )
        port map (
      I0 => reset_i_2_n_0,
      I1 => counter_stg3_reg(0),
      I2 => reset_i_3_n_0,
      I3 => \counter_stg1[5]_i_3_n_0\,
      I4 => data_out,
      O => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(4),
      I1 => \counter_stg1_reg__0\(2),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(1),
      I4 => \counter_stg1_reg__0\(3),
      I5 => counter_stg1_reg(5),
      O => plusOp(5)
    );
\counter_stg1[5]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(3),
      I1 => \counter_stg1_reg__0\(1),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(2),
      I4 => \counter_stg1_reg__0\(4),
      O => \counter_stg1[5]_i_3_n_0\
    );
\counter_stg1_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(0),
      Q => \counter_stg1_reg__0\(0),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(1),
      Q => \counter_stg1_reg__0\(1),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(2),
      Q => \counter_stg1_reg__0\(2),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(3),
      Q => \counter_stg1_reg__0\(3),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(4),
      Q => \counter_stg1_reg__0\(4),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(5),
      Q => counter_stg1_reg(5),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(4),
      I1 => \counter_stg1_reg__0\(2),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(1),
      I4 => \counter_stg1_reg__0\(3),
      I5 => counter_stg1_reg(5),
      O => eqOp
    );
\counter_stg2[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => counter_stg2_reg(0),
      O => \counter_stg2[0]_i_3_n_0\
    );
\counter_stg2_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_7\,
      Q => counter_stg2_reg(0),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \counter_stg2_reg[0]_i_2_n_0\,
      CO(2) => \counter_stg2_reg[0]_i_2_n_1\,
      CO(1) => \counter_stg2_reg[0]_i_2_n_2\,
      CO(0) => \counter_stg2_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \counter_stg2_reg[0]_i_2_n_4\,
      O(2) => \counter_stg2_reg[0]_i_2_n_5\,
      O(1) => \counter_stg2_reg[0]_i_2_n_6\,
      O(0) => \counter_stg2_reg[0]_i_2_n_7\,
      S(3 downto 1) => counter_stg2_reg(3 downto 1),
      S(0) => \counter_stg2[0]_i_3_n_0\
    );
\counter_stg2_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_5\,
      Q => counter_stg2_reg(10),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_4\,
      Q => counter_stg2_reg(11),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_6\,
      Q => counter_stg2_reg(1),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_5\,
      Q => counter_stg2_reg(2),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_4\,
      Q => counter_stg2_reg(3),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_7\,
      Q => counter_stg2_reg(4),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg2_reg[0]_i_2_n_0\,
      CO(3) => \counter_stg2_reg[4]_i_1_n_0\,
      CO(2) => \counter_stg2_reg[4]_i_1_n_1\,
      CO(1) => \counter_stg2_reg[4]_i_1_n_2\,
      CO(0) => \counter_stg2_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg2_reg[4]_i_1_n_4\,
      O(2) => \counter_stg2_reg[4]_i_1_n_5\,
      O(1) => \counter_stg2_reg[4]_i_1_n_6\,
      O(0) => \counter_stg2_reg[4]_i_1_n_7\,
      S(3 downto 0) => counter_stg2_reg(7 downto 4)
    );
\counter_stg2_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_6\,
      Q => counter_stg2_reg(5),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_5\,
      Q => counter_stg2_reg(6),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_4\,
      Q => counter_stg2_reg(7),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_7\,
      Q => counter_stg2_reg(8),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg2_reg[4]_i_1_n_0\,
      CO(3) => \NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \counter_stg2_reg[8]_i_1_n_1\,
      CO(1) => \counter_stg2_reg[8]_i_1_n_2\,
      CO(0) => \counter_stg2_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg2_reg[8]_i_1_n_4\,
      O(2) => \counter_stg2_reg[8]_i_1_n_5\,
      O(1) => \counter_stg2_reg[8]_i_1_n_6\,
      O(0) => \counter_stg2_reg[8]_i_1_n_7\,
      S(3 downto 0) => counter_stg2_reg(11 downto 8)
    );
\counter_stg2_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_6\,
      Q => counter_stg2_reg(9),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \counter_stg3[0]_i_3_n_0\,
      I1 => \counter_stg3[0]_i_4_n_0\,
      I2 => counter_stg2_reg(0),
      I3 => \counter_stg1[5]_i_3_n_0\,
      O => counter_stg30
    );
\counter_stg3[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => counter_stg2_reg(3),
      I1 => counter_stg2_reg(4),
      I2 => counter_stg2_reg(1),
      I3 => counter_stg2_reg(2),
      I4 => counter_stg2_reg(6),
      I5 => counter_stg2_reg(5),
      O => \counter_stg3[0]_i_3_n_0\
    );
\counter_stg3[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => counter_stg2_reg(9),
      I1 => counter_stg2_reg(10),
      I2 => counter_stg2_reg(7),
      I3 => counter_stg2_reg(8),
      I4 => counter_stg1_reg(5),
      I5 => counter_stg2_reg(11),
      O => \counter_stg3[0]_i_4_n_0\
    );
\counter_stg3[0]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => counter_stg3_reg(0),
      O => \counter_stg3[0]_i_5_n_0\
    );
\counter_stg3_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_7\,
      Q => counter_stg3_reg(0),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \counter_stg3_reg[0]_i_2_n_0\,
      CO(2) => \counter_stg3_reg[0]_i_2_n_1\,
      CO(1) => \counter_stg3_reg[0]_i_2_n_2\,
      CO(0) => \counter_stg3_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \counter_stg3_reg[0]_i_2_n_4\,
      O(2) => \counter_stg3_reg[0]_i_2_n_5\,
      O(1) => \counter_stg3_reg[0]_i_2_n_6\,
      O(0) => \counter_stg3_reg[0]_i_2_n_7\,
      S(3 downto 1) => counter_stg3_reg(3 downto 1),
      S(0) => \counter_stg3[0]_i_5_n_0\
    );
\counter_stg3_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_5\,
      Q => counter_stg3_reg(10),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_4\,
      Q => counter_stg3_reg(11),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_6\,
      Q => counter_stg3_reg(1),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_5\,
      Q => counter_stg3_reg(2),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_4\,
      Q => counter_stg3_reg(3),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_7\,
      Q => counter_stg3_reg(4),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg3_reg[0]_i_2_n_0\,
      CO(3) => \counter_stg3_reg[4]_i_1_n_0\,
      CO(2) => \counter_stg3_reg[4]_i_1_n_1\,
      CO(1) => \counter_stg3_reg[4]_i_1_n_2\,
      CO(0) => \counter_stg3_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg3_reg[4]_i_1_n_4\,
      O(2) => \counter_stg3_reg[4]_i_1_n_5\,
      O(1) => \counter_stg3_reg[4]_i_1_n_6\,
      O(0) => \counter_stg3_reg[4]_i_1_n_7\,
      S(3 downto 0) => counter_stg3_reg(7 downto 4)
    );
\counter_stg3_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_6\,
      Q => counter_stg3_reg(5),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_5\,
      Q => counter_stg3_reg(6),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_4\,
      Q => counter_stg3_reg(7),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_7\,
      Q => counter_stg3_reg(8),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg3_reg[4]_i_1_n_0\,
      CO(3) => \NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \counter_stg3_reg[8]_i_1_n_1\,
      CO(1) => \counter_stg3_reg[8]_i_1_n_2\,
      CO(0) => \counter_stg3_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg3_reg[8]_i_1_n_4\,
      O(2) => \counter_stg3_reg[8]_i_1_n_5\,
      O(1) => \counter_stg3_reg[8]_i_1_n_6\,
      O(0) => \counter_stg3_reg[8]_i_1_n_7\,
      S(3 downto 0) => counter_stg3_reg(11 downto 8)
    );
\counter_stg3_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_6\,
      Q => counter_stg3_reg(9),
      R => \counter_stg1[5]_i_1_n_0\
    );
reset_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => reset_i_2_n_0,
      I1 => counter_stg3_reg(0),
      I2 => reset_i_3_n_0,
      O => reset0
    );
reset_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000001000000000"
    )
        port map (
      I0 => counter_stg3_reg(9),
      I1 => counter_stg3_reg(10),
      I2 => counter_stg3_reg(7),
      I3 => counter_stg3_reg(8),
      I4 => counter_stg2_reg(0),
      I5 => counter_stg3_reg(11),
      O => reset_i_2_n_0
    );
reset_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => reset_i_4_n_0,
      I1 => reset_i_5_n_0,
      I2 => reset_i_6_n_0,
      O => reset_i_3_n_0
    );
reset_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000008"
    )
        port map (
      I0 => counter_stg2_reg(3),
      I1 => counter_stg2_reg(4),
      I2 => counter_stg2_reg(1),
      I3 => counter_stg2_reg(2),
      I4 => counter_stg2_reg(6),
      I5 => counter_stg2_reg(5),
      O => reset_i_4_n_0
    );
reset_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0020000000000000"
    )
        port map (
      I0 => counter_stg2_reg(10),
      I1 => counter_stg2_reg(9),
      I2 => counter_stg2_reg(8),
      I3 => counter_stg2_reg(7),
      I4 => counter_stg1_reg(5),
      I5 => counter_stg2_reg(11),
      O => reset_i_5_n_0
    );
reset_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0002000000000000"
    )
        port map (
      I0 => counter_stg3_reg(4),
      I1 => counter_stg3_reg(3),
      I2 => counter_stg3_reg(1),
      I3 => counter_stg3_reg(2),
      I4 => counter_stg3_reg(6),
      I5 => counter_stg3_reg(5),
      O => reset_i_6_n_0
    );
reset_reg: unisim.vcomponents.FDRE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset0,
      Q => reset,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_resets is
  port (
    \out\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    independent_clock_bufg : in STD_LOGIC;
    reset : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_resets;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_resets is
  signal pma_reset_pipe : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg : string;
  attribute async_reg of pma_reset_pipe : signal is "true";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \pma_reset_pipe_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[1]\ : label is std.standard.true;
  attribute KEEP of \pma_reset_pipe_reg[1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[2]\ : label is std.standard.true;
  attribute KEEP of \pma_reset_pipe_reg[2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[3]\ : label is std.standard.true;
  attribute KEEP of \pma_reset_pipe_reg[3]\ : label is "yes";
begin
  \out\(0) <= pma_reset_pipe(3);
\pma_reset_pipe_reg[0]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => '0',
      PRE => reset,
      Q => pma_reset_pipe(0)
    );
\pma_reset_pipe_reg[1]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pma_reset_pipe(0),
      PRE => reset,
      Q => pma_reset_pipe(1)
    );
\pma_reset_pipe_reg[2]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pma_reset_pipe(1),
      PRE => reset,
      Q => pma_reset_pipe(2)
    );
\pma_reset_pipe_reg[3]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pma_reset_pipe(2),
      PRE => reset,
      Q => pma_reset_pipe(3)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    CLK : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_sync_block;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_0 is
  port (
    resetdone : out STD_LOGIC;
    resetdone_0 : in STD_LOGIC;
    data_in : in STD_LOGIC;
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_0 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_0;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_0 is
  signal data_out : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
resetdone_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => data_out,
      I1 => resetdone_0,
      O => resetdone
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_10 is
  port (
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_10 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_10;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_10 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_11 is
  port (
    \FSM_sequential_rx_state_reg[1]\ : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 2 downto 0 );
    rxresetdone_s3 : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_11 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_11;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_11 is
  signal cplllock_sync : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
\FSM_sequential_rx_state[3]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"008F0080"
    )
        port map (
      I0 => Q(0),
      I1 => rxresetdone_s3,
      I2 => Q(1),
      I3 => Q(2),
      I4 => cplllock_sync,
      O => \FSM_sequential_rx_state_reg[1]\
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => cplllock_sync,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_12 is
  port (
    \FSM_sequential_rx_state_reg[1]\ : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 2 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    reset_time_out_reg : in STD_LOGIC;
    reset_time_out_reg_0 : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    reset_time_out_reg_1 : in STD_LOGIC;
    reset_time_out_reg_2 : in STD_LOGIC;
    data_in : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[1]_0\ : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_0 : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_1 : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]\ : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_0\ : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_1\ : in STD_LOGIC;
    mmcm_lock_reclocked : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_2\ : in STD_LOGIC;
    time_out_wait_bypass_s3 : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[3]\ : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_3\ : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_2 : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_3 : in STD_LOGIC;
    data_out : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_12 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_12;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_12 is
  signal \FSM_sequential_rx_state[0]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[1]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_8_n_0\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  signal data_valid_sync : STD_LOGIC;
  signal reset_time_out_i_2_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_3_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_4_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[0]_i_3\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[3]_i_8\ : label is "soft_lutpair41";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
\FSM_sequential_rx_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEFEFEF"
    )
        port map (
      I0 => \FSM_sequential_rx_state_reg[0]_2\,
      I1 => \FSM_sequential_rx_state[0]_i_3_n_0\,
      I2 => Q(0),
      I3 => Q(1),
      I4 => Q(3),
      O => D(0)
    );
\FSM_sequential_rx_state[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => Q(3),
      I1 => reset_time_out_reg_2,
      I2 => data_valid_sync,
      I3 => rx_fsm_reset_done_int_reg_1,
      O => \FSM_sequential_rx_state[0]_i_3_n_0\
    );
\FSM_sequential_rx_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF24200400"
    )
        port map (
      I0 => Q(0),
      I1 => Q(1),
      I2 => Q(3),
      I3 => Q(2),
      I4 => \FSM_sequential_rx_state[1]_i_2_n_0\,
      I5 => \FSM_sequential_rx_state_reg[1]_0\,
      O => D(1)
    );
\FSM_sequential_rx_state[1]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => data_valid_sync,
      I1 => rx_fsm_reset_done_int_reg_1,
      O => \FSM_sequential_rx_state[1]_i_2_n_0\
    );
\FSM_sequential_rx_state[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFEEE"
    )
        port map (
      I0 => \FSM_sequential_rx_state_reg[0]\,
      I1 => \FSM_sequential_rx_state[3]_i_4_n_0\,
      I2 => Q(0),
      I3 => reset_time_out_reg,
      I4 => \FSM_sequential_rx_state[3]_i_6_n_0\,
      I5 => \FSM_sequential_rx_state_reg[0]_0\,
      O => E(0)
    );
\FSM_sequential_rx_state[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFCCC0C4C4"
    )
        port map (
      I0 => time_out_wait_bypass_s3,
      I1 => Q(3),
      I2 => Q(1),
      I3 => \FSM_sequential_rx_state[3]_i_8_n_0\,
      I4 => Q(0),
      I5 => \FSM_sequential_rx_state_reg[3]\,
      O => D(2)
    );
\FSM_sequential_rx_state[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAEFEA"
    )
        port map (
      I0 => \FSM_sequential_rx_state[0]_i_3_n_0\,
      I1 => \FSM_sequential_rx_state_reg[0]_1\,
      I2 => Q(2),
      I3 => \FSM_sequential_rx_state_reg[0]_3\,
      I4 => Q(0),
      I5 => Q(1),
      O => \FSM_sequential_rx_state[3]_i_4_n_0\
    );
\FSM_sequential_rx_state[3]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0CE20CCC"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => Q(3),
      I2 => data_valid_sync,
      I3 => Q(1),
      I4 => Q(0),
      O => \FSM_sequential_rx_state[3]_i_6_n_0\
    );
\FSM_sequential_rx_state[3]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => rx_fsm_reset_done_int_reg_1,
      I1 => data_valid_sync,
      I2 => reset_time_out_reg_2,
      O => \FSM_sequential_rx_state[3]_i_8_n_0\
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_out,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_valid_sync,
      R => '0'
    );
\reset_time_out_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEFFFFFEEEF0000"
    )
        port map (
      I0 => reset_time_out_i_2_n_0,
      I1 => reset_time_out_reg,
      I2 => reset_time_out_reg_0,
      I3 => Q(1),
      I4 => reset_time_out_reg_1,
      I5 => reset_time_out_reg_2,
      O => \FSM_sequential_rx_state_reg[1]\
    );
reset_time_out_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0FF30E0E0FF30202"
    )
        port map (
      I0 => \FSM_sequential_rx_state_reg[0]_1\,
      I1 => Q(0),
      I2 => Q(1),
      I3 => data_valid_sync,
      I4 => Q(3),
      I5 => mmcm_lock_reclocked,
      O => reset_time_out_i_2_n_0
    );
rx_fsm_reset_done_int_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"ABA8"
    )
        port map (
      I0 => rx_fsm_reset_done_int,
      I1 => rx_fsm_reset_done_int_i_3_n_0,
      I2 => rx_fsm_reset_done_int_i_4_n_0,
      I3 => data_in,
      O => rx_fsm_reset_done_int_reg
    );
rx_fsm_reset_done_int_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00040000"
    )
        port map (
      I0 => Q(0),
      I1 => data_valid_sync,
      I2 => Q(2),
      I3 => reset_time_out_reg_2,
      I4 => rx_fsm_reset_done_int_reg_2,
      O => rx_fsm_reset_done_int
    );
rx_fsm_reset_done_int_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0400040004040400"
    )
        port map (
      I0 => rx_fsm_reset_done_int_reg_0,
      I1 => Q(3),
      I2 => Q(2),
      I3 => data_valid_sync,
      I4 => rx_fsm_reset_done_int_reg_1,
      I5 => reset_time_out_reg_2,
      O => rx_fsm_reset_done_int_i_3_n_0
    );
rx_fsm_reset_done_int_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0008000808080008"
    )
        port map (
      I0 => rx_fsm_reset_done_int_reg_3,
      I1 => Q(1),
      I2 => Q(0),
      I3 => data_valid_sync,
      I4 => rx_fsm_reset_done_int_reg_2,
      I5 => reset_time_out_reg_2,
      O => rx_fsm_reset_done_int_i_4_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_13 is
  port (
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_13 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_13;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_13 is
  signal \^data_out\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
  data_out <= \^data_out\;
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => \^data_out\,
      R => '0'
    );
\mmcm_lock_count[7]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^data_out\,
      O => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_14 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_14 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_14;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_14 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_15 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_15 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_15;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_15 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_16 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg6_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_16 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_16;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_16 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_3 is
  port (
    data_out : out STD_LOGIC;
    status_vector : in STD_LOGIC_VECTOR ( 0 to 0 );
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_3 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_3;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_3 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => status_vector(0),
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_4 is
  port (
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_4 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_4;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_4 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_5 is
  port (
    reset_time_out_reg : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    reset_time_out_reg_0 : in STD_LOGIC;
    reset_time_out : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_0\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_1\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_2\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_3\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    reset_time_out_reg_1 : in STD_LOGIC;
    mmcm_lock_reclocked : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_4\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_5\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_6\ : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_5 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_5;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_5 is
  signal \FSM_sequential_tx_state[3]_i_5_n_0\ : STD_LOGIC;
  signal cplllock_sync : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  signal \reset_time_out_i_3__0_n_0\ : STD_LOGIC;
  signal \reset_time_out_i_4__0_n_0\ : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
\FSM_sequential_tx_state[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFEFFFE"
    )
        port map (
      I0 => \FSM_sequential_tx_state_reg[0]\,
      I1 => \FSM_sequential_tx_state_reg[0]_0\,
      I2 => \FSM_sequential_tx_state[3]_i_5_n_0\,
      I3 => \FSM_sequential_tx_state_reg[0]_1\,
      I4 => \FSM_sequential_tx_state_reg[0]_2\,
      I5 => \FSM_sequential_tx_state_reg[0]_3\,
      O => E(0)
    );
\FSM_sequential_tx_state[3]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000F00008"
    )
        port map (
      I0 => \FSM_sequential_tx_state_reg[0]_4\,
      I1 => \FSM_sequential_tx_state_reg[0]_5\,
      I2 => cplllock_sync,
      I3 => Q(2),
      I4 => Q(1),
      I5 => \FSM_sequential_tx_state_reg[0]_6\,
      O => \FSM_sequential_tx_state[3]_i_5_n_0\
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => cplllock_sync,
      R => '0'
    );
reset_time_out_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => reset_time_out_reg_0,
      I1 => \reset_time_out_i_3__0_n_0\,
      I2 => \reset_time_out_i_4__0_n_0\,
      I3 => reset_time_out,
      O => reset_time_out_reg
    );
\reset_time_out_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"020002000F000200"
    )
        port map (
      I0 => cplllock_sync,
      I1 => Q(2),
      I2 => Q(3),
      I3 => Q(0),
      I4 => mmcm_lock_reclocked,
      I5 => Q(1),
      O => \reset_time_out_i_3__0_n_0\
    );
\reset_time_out_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0505FF040505F504"
    )
        port map (
      I0 => Q(1),
      I1 => reset_time_out_reg_1,
      I2 => Q(2),
      I3 => Q(0),
      I4 => Q(3),
      I5 => cplllock_sync,
      O => \reset_time_out_i_4__0_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_6 is
  port (
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_6 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_6;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_6 is
  signal \^data_out\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
  data_out <= \^data_out\;
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => \^data_out\,
      R => '0'
    );
\mmcm_lock_count[7]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^data_out\,
      O => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_7 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg6_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_7 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_7;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_7 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_8 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_8 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_8;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_8 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_9 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_9 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_9;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_9 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2024.1.1"
`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
GNlNeNP2E6l/J7IS2LF/ItWIQpPIQxmqiENbpRcUcYDs2zMQssAZwDKWmGxc99etc2e+6UESUZFU
1OoN93QWwGY5lwtShrTc9cE66KsLO3U82lRN16EHmi+RZOdYR9rf5z7OgjqfdL/T03/rfwU37+Qw
8V3wDE2i5hqr8BCHEwM=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
afRfkAd8cICiTJYylAj89MfRuyfOa9d/tbr4mvbd5mWPw07cLRhiyi8gIe4+Ayw0CiOk7/EjPgWg
Tg/1hR99BWRw4VbbDTWu69lZfP8CCWoBxtdt8WRuUGz7PLk6qQMaoDf1ogw0xyXTK96u8N6bef3z
7mR3YInM0ECnk0+8vQDLRo6ETSTf4oQYcckMpPfbPQ+NnHRvcHroYdddFGNYasbeUqwn7tkHnYO+
ADUKING4g3DhZO3UAIPZJggQnH3ayID9nf49Oa7bj//jtWD0qELIGFMQn48spke+RcfDkdFLHWhY
0esV8oRsRwIWfwXwqK6i7aIis3paozzKSWbiNg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
ZI9ECw+Xpdoykt5zjVXLh0QGbCnqQDaDo8xSAWsYltuhqetS6+j38MNsVABP/vySCUkxp7yLlCMg
506S40Li7Nmy7qD4YvDLHMe+fIgwi3Hb193aoDluxnxdVk/aJ7VOJWXd88+scqfUSpbbFv7rk9La
8woMpCSiDNVKi6NB7Qs=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
R1h0p6UTerhva5Yb3duDj5nR3CyyGXjeE4dSD+H9PUwvK6SElm7FlL9AjlrnVaE2OmUXbsHu+WYi
T1XMnV1ekw5xTRICAudvAhAJrZYMHEjujHOfgXPmKtXHivF59pFlTxnEtD+dnchOL4x/jMm/lCgH
k3/wzvZbfOaxaKFceauCooIRZkqZa5FA3KG53cEucbspEldbnPaAcHsiO04PEwklvjo+2XAiGTHB
YB9XEkRUOprwSG+TeOWFNvz4Yu2FYHffCsobvgsF/8dSJQ1SVv3rM1uHf3coB/s7NIgZ0ye03NO/
wLFRL+3OuPxWNTqHO9YYXL0XTd1rPaaWB/mCcw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
4l+Avk44zzaOVxRJDzFVTXMyJmyE6qVo8wAiHogr6M/NkYt3AgnEITBHDJ9dF4SVZQLiV7ZY6shr
asWzrbISsiBCAaZgyPZLr1zlBErx08H5Enf1NkLuocQa0x2Dc1DwG+mO+flli1++tKpmHM6ADCS0
3/8fREk+ga01VkXbOXY6B7Wt4G2O3LBdFCkEvd0/MEx5uYrJNrtpaeSd3h2CpYLsfPsxxbAzNJN1
3pvNKoS8QwUnTPy7oe/k8bFD0TOzZmLvT/dy3x7SFlB6oSYr7FtWMOOuH5B08Upu75qdujq2/6ox
I9BucH2FnuLsghHnPuWfjh4WTDPekeCy51G/7w==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2023_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
r1TCY1FBxgJgPdlaGrkCY3ZPZ26hJnsdsOKnUu75PgvjzuIkty6RkwizEzV4coW2cxRBcWf87Rmb
xbDdy2cDu4f2qP6QUVBsGtY+ITHBIqBPqY7IqmHvnmT8DQ7mta5QD9HpSpd8E0HAu+1hmp2y9R2T
j6cEj7KNzaIGnuiJLket69e1dx31L14fHppyx7EJTOKaIYVHOTGcZeHi6ozbitA95N9bUNiSCS93
sJcjWWTWEB9Cx8arBeHBMIflTsCrUzgwd6f7Lb59R/pt8Zgvgr/BoV20ZGDDy6BCwEWgGSw6GDBL
o5ppIN+dICJOxCzX0Ij4ziKkTNPa1OnlJpcXcA==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
U0o+hd53NUTkWW/pm++iDbvu9EzyJbrE8cgDdW35QL1go6ppv1cnjM7W9LNAUoYpay5lYpZFkUZP
qgnoVBG2BG64sAHeAjjMVH8aUuTYzhhM3n6pvSqx5Ez8rezjhdOlMLX17yWFSCq3x15rfV2si91N
p+FFtjXOD3BmuNjRLXeeRjrvt6sxfN9IwN0yEZYhwp2ukPLeIyrYpRXZ+xMErI/b7BKLZNIljMSV
Cx5wPeURNjIC7AKZOJmo8YQe2JGJEHps80MGm98CsBtbIXDa7uAxib3WWstVs/UbQGgqjiyNIOSu
11VEYQSI8nO9bNfJPsLpVaeo9V+FUKdFcBgKbA==

`protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`protect key_block
JlAVtvWu4RylemkeXAwQaOcauzsF85mjSoJqBa5/i7p1cOK0PM2Rfou44syNOWyUG43Mq5DdY2P2
hSdJ0BrVnIF/JY67xayAWorKfMTlvfZN3zVnvvFrLORWpe5MwPzwHomEMtQgK/WjTmMnFJzWxYti
gedoe+hxxEoZILnHN3ge9A2vnRikwcJETpOKLK13mQa1Td8zo9tsQuagX91Bw+AWLzMrHGR2bmQr
grI95HpSioNMY5CjomezDLSd80Rlkr+6SiaUQt3pqExFVEAsKNJdtppxNwugqojHWEqm1lF0wKuj
aLs/KfYF2Uv5wNjccSP2gHLWWaF5DKs5G5u4c4FjEiS/slCZgrt5Ymwc6XKQHNOHbPRsQcbbkPMD
V/YuoNzw1R2386n0pOd+vMD9nTtpB104VrwBNBxlGPPa0qiFM4+xr58vMzHKNT91irPu/EirEIjt
wEtLynnUeSKMKz3aAvXZPRYeEurD0fgZ6iczWVNZOsltSIQZjOp5ErOC

`protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GeMop+4rJGQJTsAts4IyQAkQMeKt+Fa026naAEt7J5/8nhAvWlIS4YQDNOLHMMUe8p+sUA4yE6+s
hf5STrRcmXzM45O/bYrkBzWHNqGcsEuAHfdUd9ANbBZmpncQ5ywZbXLX0oI4xVx55B8sn2gAj+p2
NQEPjYWPZXJNML4IilPsrW6TlgTi1zhp0JFBFT3O6yHY35dAseHXtMWYgjr0+6WBtVCCvC/z4bA0
UY58Skgytan1bUM557AlikG2LqNB6YHVUQihnx4ZProlAL61WY/j4joeXcDH8Tpm0cmYcUM5yUax
haoFS6FezWAqD4KpiUS3xpkUVET2QpN0NnxTvw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
1x9gXtJFU2nGQ9CWRMPqjLG5B7nyL7ZKhgWVKPrQTraMot+PY1MLqNEQ8371fSgOtWrUw7wBswaL
CBsGdy0dDZt13psueOntqYT0wcW3vUbYzfkMCsSjkk2yeYjnCYclwJKMNk3fGxfxpTLQsScOFING
ZyUHBaE+KeZ7yfdqvxwcpP+rUXdNX/2BGvcr+a0ow/nFsiCiOMdYIlAgllnBFoUV7lDr001O00Hk
1MDqVCRdpiO5q5FnggoIp1sVIq1C+JbqiuyPkv5jmlk3iIfZiHhSSYoxP3DJf7VPvlJ1Z/Hwz6Dw
/39nFCTotl6l9hqiLHTmISLBvSYZnjDEcOPtPA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
mxE8AqszO8jEcufcEIxsyswviid2I+vp0wiBEslkbc/wVr2vlpOaIaviDrRuTwIVx162O0AViWa2
3/lH2PLY1lcjenGETw7njsuJd6MQv4KihzbHhd1HB0TKpoL1n4E9u5PgNIGghcgNu7s2hE5rpffM
FAxMXxyhZQ3j4WBKTgRtbqcexvVSl/whSo87Oa/V48wwF7Z68uoZdaD4cdNihYwHMLOm0LYqYesQ
cScpoz43IdSFiQkBEM4YXuqn7BFIsJ7UAr+EkHAFxWYqt0IJ3+/OCuOYW+y/3p2tXT2YSGnEsOzZ
SLIsZpsw7wAiEfu9tVZJlTQ0OUTZqfQXK68i9A==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 119328)
`protect data_block
1vFLS+NMWNyemPrhbTFP8+PW98y0nDIEylx/Qz3RULeeHomACXQRi1s9sjigdJok9/6AwtnE3VR4
6nG6vEp/pnkQBmF3859g2n4j1+eDu3MUBJRxcFJSqAvRzdnRXe9WsokzB5XXEGfp/EoDbWDB0KH6
sO1bAaSQkVm1zMyL3L21oh6h2915uJyCZhaKNPLzkOGlEzL3X1OP+K6Eec1TTUt6AfJYIvzH02xA
gh/uaS0xoS6AJBNjUSAkl0mTlHX7f99rAcY/I4zu8KHczr6J4M8agHkgrBe3AbpmWY4OfiYYW0hr
CZm5U55LEshvFQAr2QzBw7y8O6HYeB8dW/BaOG3eERRFbwAFpO4DK7pndSXfBBoRUTyqPoJbFbl1
nkekZplrAa2B6JsLBUIX2X/7CBCjsfxtouJF8SHAPVZSxTWh9MUw7MrKUJpLuxqrRhEq7KbSUtZD
RfUTPWcWYJDWR1AVtT0eN/pF3F92BRu7K9+U7v6igERqTjaYkk0uff9477Cn8HNIVtDg/tEYyKhK
g5QF0R2JOoJSXDRPvfNDU0sRVP0yBSYJVMMUIlVWE5w+7a76kkx6HBYTz7nD5wgYC7pWTURoeJrg
Do/JXi2h33Y3hnfjzLgzscSjiiIGmRUB8vqvxUoX2psFMS2UM+od7OGZebYfynC/CGUkAHmDphDU
mUz2fRFatV78jqRQsCyLGZfrtz+4KLe+DrO8tDdV+TXO5ia45d+lo4TeMT1Q9zEEk1u7236w1YWB
g5S6C49lx/wW4ElCc4MGfjgLEMC7KcOf3inVrxXshC/MKUHUnQTQaRN0TZePmFe9ESsGAgA0IlFw
CWceilOaGfinj0x4JzhPeHGpW8WoxK7kUu7/zzu6x3HpBuSsIMFhqBpV9qBY5EQ9EtbTI3fsscMY
Vd9BEiShtHKJ10753HDS7ubXEJRCM675mRTyWjL3ImSsm9mqomWup1j9wZ7JDkrXreBBOgnV4pTc
cxhbQsZfME/3w6OXOS24zS1y45q/Vx7sLZu2YCr3HPxXacpFn2AgE2zRbLEWQ0HtMEt32Co2jgST
zbWsRICr9TKvvk8GJfiDyTvSbrDx9XBWcVxRPbrtLYSGCGkoQDVtHB8p2T7TZ3VuEmX787hjrP8G
F3DS4Xe/W0zVIXfROfYZX9K/aa1CtFD0ZR6nSfQ/kOwKzkPtsLL8ticFJHeiTtorovX65caznfzg
w32tGgzFbTeygplQ5hKqcyhziEqRKGid1+iPnmoJL6CUg5/UxRpnPMC7l5IuYD6qOmgKscYs582W
vngKZbDx5t7+tjQuYgrM9onYtb6ZzwjfFv0f0XxUhjtEibFz+k/kcN39KkLxEOR64gIhBzMyN22i
LKjKeS9q9AkCsN1ZtqMiPLreKW8a7rX9dlaFmxUQhow9JGhctZAxdwxfNnGPavqXUjkoDkuo4E81
DsjRo5MRH8S96zpyPhFjCyK2jpASfuWvnpG0u4FZE8wX8o1QoxKbPartTK0AzGsGA7Achqh8YMIt
itmo4zxNakcylBNr+K+Su4/IhkDVrXzw+pjAK5U9CqlbDsxTr0HgbQYFt0tYW29HVegiyA/Ca00w
z32X7tkK5SMtLRT8MgM+OHwUWqbAFwgq/BOz4aTuxCSV/EAblJxtqPaAOSG5f9nt+e9qCs0GgUoO
Nls8NOetZEDmwAnsTdnmhU4jRLFf6CxkmTiE8+fW+cVveGUn4Fr1/anZ9PtRh4IJCA3WzmWOjEJG
rnOmaFi2kS2yvqUqA9RtgMRuu4llUgQsC+SiHCBiyCBlqtcY6SDGUGYQh6r6yDVO+j9/5vICUG5Z
9F3qVLvcFwVU1kF+UnCi54dPVxdlMlUVddRALTXPmXiedPJTlM7V4+PnPjCgBXqBbnRAiySBds+G
TyyA3kWO8QOCo6MVgyZ+/nenRkk2gJXDjWUKtn8Qaort8sXWPLVePCs4T+WD2jLcEm6l8hx4DNWX
nauH4rlbHeC2adEkXZHsM51wTRoS887hRDyGPl6DN/a3P73jXFB1Rv/t4ESQwgRxqZV9dnPcNAqu
yyr7M3A+iMLOzjpZ9jtRlvB6L3BAOzLcU7iVkYQn2YPpUYIEPg4MIrenFl0BZm8K6jJwy3UU9wop
IJrGpms3fZUyOlujLkl3ZWreWecLRR9d/CVOy2DPNbWERlAKszR+kpbQZ26TMvYpSfDCcCkati9s
zCk/Q5em2a7TQy6coNJPKHBPTOeVbk6ICB04PLI/OLB8tJkHRHft7UWXVQnwPmGFQVgvMpjkDS+l
7PvolQ/6PWcQL2jjA5RKiCvzJA1/bZAHOYZgvZWVjYPAeOjrC8OL+zok1M8Fu2RybgeXqFtmfppy
8PSmHjRrMYPtaEX6O0Kx/Io0owDUHRvGA5Z7St4NQjBDn2hdQ4ATL9YAUhV3fWFQ0TZzlBuasy6s
foSuwaOrMCan7V87kl6qOF4dMWitZ93Qch6SA9nsTRmKDJCYRUn477jP3+07+l990TFR4WIl0IUg
3+hvAyeXEWat4GlzAO60WgP0yqp3GddlH/smDsE0Kayi9CdlienLnhEHMh9NYWovXGxa5g3Si7SW
i23rcAbH11Wmb/ocQVI7ZFncki65OTy1fIGdEhcj7bSDR+Xb16nEhh860pbhl1q0xbUJ0Oye8Djc
aNdNzirgf1c/gwm+sAupkeAAj1VJY2BuOjhFx80fQoke3KXQPYl7qn9ngCLrcQPur5yAQkK7KBh7
GVq4wLaLmzU95oGM8m0YfvTLnyrDkTzVM36KoMOg8pRnLuAVZ4GSrHwcRpD68f0/3X9jRtcFYODY
pJ5PSVvZ1NiOvCv9/IC95JCkncYlch43YBmpC9iLaVXtg76Qr0r4ML/YOZRFZwzMLWoEb5G+2Qze
qcpUwdw+bUDyGUT+rzAkdETa2k1uMV1W1hV6qJqhbo7cSLKf+bItVeswyKRWe0Hdn3iNDYevMNkQ
o0H/eB+EitemCmi1Z5I4Ty9osuGW5ys2dce4oM1TpclId0Q0XrJwh8GCu2s0I4cU/OWKNH+w38Jy
xZOqj+3kRRd7ep3tdTDVqe0CR2qdZ/uzo1PYhDf+JBr7TfJk1AOVX/61m9NfQxDjAfx6Q7GRR1Dg
e2v74tDr5XPjjSTOXcPI1qK6w5bZoNEu4cDtzFhw6UNXKgaaiQBHqJupq3une7ru1V4qmOVANr4O
g9BThwSXBje6C8VF6DRgxTv8x9zfMiO4xiJjeXB9dHozUhNAjTT4NPs19Fatf2EiXbY8Wy0sSseP
NtsXdPxyyN9udXzRJkAVBK5kcBHgPL/kKWLnhEQDu+uz4Eopsef6CUBfdgLbCahklmUGvu1qqimK
3aO5PPDkFojLI48nRESh9JOsecm/jgUsisH0n/WvAX5Zmt7ReITbmhOLxZg1yUecsIdANaPuNUAP
VbbHwWw5bgce4vu6Rh4StCVhCik09hB9X3WU54iT/ZqSf5V26ZMDqm/90z7fgand5mhp6sWx4WMZ
1ms/7fCpHwYhXxXtoCqeA6L5+Lx+Yr+1v7fsJMVHp7VjVrt+EUEpmoHL1cK+x9nD1Dh3M6P2QBxj
FkK2fYJIRAL74vv0zgKF1PtC0pKbcPpJ66x2zFIV9mIbl3J0xegDHGYW/DF8jP6rrKTJpVLBezIY
MPKgn72kjdb2lCFYSlq4KZHh9/1nv4BukweXfrDWm1bsuUuCNBfJRNf1ki6ZQt3n2bqTOC4bjUKk
lr9hmoTLXG/LbgU9lGBkBjZun8aXAt/VL1ydtpJRNWT+SjURhBtly5mZoVfX9fV9JcF8dwP5wcOq
ymbUWL+uSrrxDX1ERd3gEB4w9SJFAfgt53xWAI78yfg90pRAlvq5rZJ7holf6tO9mGNB66U7zbI+
D3KkOcVhpk1Lo4AQW18I2C1gHmIzE3ciBLSon9pgxE1kDxrzKrJYdg2Vo/bRt36167iWUn9aDuiL
vwXQysDupRZ6AM4Xv9uW/dZb5BwrVNgxHvihufaflcIWEjThldppJ5OAdc8zDNiUEUdiNJY+h2Ju
OpSndcUgf9fFmp9qFDdCWr2aT4ffILjaHKYp1JIA9uYXfOLGkm1CRCCHztohWSmOhaa/Cr/5hDW1
Ezcr2bKivfpdSCuHntXiJxtdflfoO+cucAjKEu751YurNJWG7iy2Lcxl8a/vzLLaENjJEUGz8qpA
UoIPxXYABS1UKqLLC9aJIzPuVeVjZPaexYHpuWNcHVvMBnUvuvgXmIQZwEh98bnq+RQV/Puv0xCk
I9etKciHsaD6GAiLz5M7UKIM1Z1f4a5VAMO5N2bjWZqsCzWP6XsY5YvI1f5qfG8lCo+dX6WaAyx8
JxKkDS7aTiR74lRI2YixXRy7Pt5w3zrFqjlT8i8bvbcjnl2jMPzfyMJiLPPbgcBxPKchDpVDTMxM
8kexGOTCA5/XF4FtPdxdy1yoc1MAc0vxmiVwY5hoKbhGZFtR8+edPqH8IwlUKKrCEGV9sdp44Hhh
6wKxZ50SYGp18jm1YKAlbQYXOD0GLB8b7FlSpnb+eGxiUofxpuVX0ARVIBF9ZoGV+WWgoPDrassg
eLRfXAdt4j5w1HDgf6YbvwhROIdp04NVtXckWhoQQxg9XwCRn+1sN4NkOKeSGbUTX5Sm6dhhLyeX
wXyZ75KJdbqIOPlGsQaMU5yPBVGjRVyk8lt+XGIF0Csj6YXIFFQ7d/GpYhqdh4W9efUdLS0XydNB
4Z0xmAG0Wt30IPaE/KpXa/EMuyMfwK+84/Dh8CZXWy49gC30t2oCCKSCwoHx4lmR3qjteE+3WqjW
lpq872ClXbrKe3YuFW3YBhbv90os0BF3lBEqzgBkfIW3YnONliPatKq0wsLlBIzAr57gz5waPfDN
aU8bnXJPTRj26ELAqB88fMFf5rY28AM+KUVgxgNfE2rsEJaMmEVWhpmNekst+UC6Cdw6Q02vTPXi
G3yldga/huMASEuBICUEtAUmXFit/nI9jguVTP7T5tgHM7k8DamprFCRBz8jKggdBN5sQNXMlqvn
1fRt8Xi5PMtadFbrGKBYJX0EI9+ybc0Ll/PzBBthjPYxjz5KGBXV9mYA17CPq2ozuAr+/bYPlXpQ
f6koSXrc+M+3pUDK9wk4llvECx3ZAqTQvzrRHfEJPsdlp4wLOKLi9m6cveMR6MQkS4Pp9uMOMRgB
c+3oYDqIrumEHQFZPw8L9daCKqAbpY9UFdq6G57DuoF9+WwLL3omeMGn4TtXReiWPJEleS2pUGTn
1/yQgfwyBvuY0OHRyscfdDofLaVL0X0Grzc9J8U9uKazpGj10+Aru0Zz6YeEYWrArkaxl7RVdW5R
WMhkIjQOj37aOwRfn/uPDdq0KniLlULd2YK0jt6yNuMhucWOldReBiuL9lQn2AJT2YktStEf57UY
aTzISXlghrrM2XNNb2tByvaakskSgfQQEVVEjip4xXQWCMuJM8FKeQaZ08wxobhFanGEP7ZzqHMU
G/W9cMbhWE53zvVm+PXKpqLbVmKak6/mz+I+0OchjC7R2IfLHG9CQRgW/l47R+KCAroKaYHpT2sL
qopXVZh/fEhZ+q8/bzLfTvhkG634+WKqhf6WZjewOo49Td1R2gErRW6RNPFAk4py2ht5vaP3Dpvp
f7EGpcERvlMCQ3PxZFXTGxvzDbNsOKgwYD9CyP0MsPVWfjil8QvgOiVI8SB1guNW8BhWAeR9e9IO
ENhF/aIR/VNQdBYxPb2pAfBpFy0CuFfTLVOITK29IVnNd5x2EFP8iGRQLJV8aDOv+idLo2I2Hizp
Ma/zKT8MpQR2oB9EXRC/fnarmELcjBPOI/AgWPaN9Yr+9N1JfevSaomlqmf6bKEmJozlCCYh0cUr
CNhkACA1ontb6QCxauczOpY/Pw9NiQ3hsbf20dn9zWAGH4KkDfSf2/h8PhJHmt8q6Ka5/RlHYmnn
jrfA948FMxeSb9Ya6ZujM9zC2iv+dLCs/0SaaWI8HG+U0BoesYlErXBG5j+yMseNU0Ey7vGC/Es3
eVNJknUCaHkAOHsjiV2E4Pqwrb6BUFib7CZ0N042L3RwYgl1vx/wjTCLkV18PMxe9JDNc4V505PB
jqFIniU8tel7fpm08MFyNg2YJ1Fw6Q5VjAqylO181vLXei/UU06CQuZt2+O1fr37DjVCjs2M7AKi
QgspZ0y1ejr/PIvxbiqbNCicuzZ17HzoO+SE7VWdipIGwWW6JQdH/eacWDPx1sShdhAxpc7K8eYl
iMUHU+UWXJqAsz4C1cC2TDfSXJ9G1xkBIik8v6RDIJJ65A/s1gaAj4ZqYcxBsXyz3G65RpWyBCFv
O+M3qI83mJzW6y5KFm/Kw8Lh0e6DwIbtbNyHoTCHnTN34S8hXZuZmbwHJ/nJA2dGndbdbzzdaa/0
ea8tfwjsT8GsSnNTD7A/ab9jCOaRcyZT1IpH5nnbmFNeYuNUW6CMBoTRYaA+Zepco/SIeycNWqqg
SlVD+rX7zNa2sxAo9eJVsSoRNv2oBwdIKF46eXNlPvdtYcF5oE3ab0knIB7KQyzV+SIWlFbSS+fw
mk/Wko3k3Q9Ffd6iNzrfVypogseMA14Bys5eRLmsrvJU4qMoB1Z2ID/Vq8TSdlPq29NaPcso8jff
8LBbyFYaWE/pIL1zSlg0b3C53ELPy1i1WS6AJ7So20JBUJVgw71XVsVNWHs4bC0cAy4hb6W8xPQR
rsReqAkroYT45mSLLJZnXFOkaoYM1XB0VdM0Rim9hl+R8bGBDDHnJqnpOt9URaEHzzSCnhd7j4e8
Z+MxwSphSqb4XKZ9Wh74zJX0eHaDCFUnTizvgVKh43IvZMx1HpAJSyKXC3pb+FYwIBYpL5jkG/wy
zHDX4lPRjvypGc8eH6KyRdnbyfnc+Rmqt3/KuW/x+Yt5mlS94IEj8ZdDZlgTGxPJMlmts4z9hTm6
xknBCrVMdzW1wZNkfQEQKipzSiJ2bi1/u5PtZaTxMHbxZlQjM32GpOijtzOA+sGJp6/me3JZDXZQ
iv5Ap/Kaa6omYLivQpF1LJhXRfh70DV+Eq+o2Lp1XARCxJpQ+yfGC/9sJMs51RLFPeWM4VwreUCs
Y516oLyxgCibk96ZxyniQ/p/GL8ifO/eNmA+hE/sBT7h+8akWyhI0Yc94MNPG+zgcs68+rcKg+iR
JFvqgAV2bEU3AFXVLhYcKp7nyHxeHg4se56NPCk7yRI6tOBBS5T4BJUn4TtgyBz8J8EPQReOjRhI
3bjMxv5WZGgwxXeiNWY8sIsfKrzPDqHflYZoLILbXdKZFkHGUIV3xXKLFpdDfL0l6aPEAudT+5jc
aMI8fquaqjPAo3+7y59QywqIFqSGjVig6eBmNIfTiMFsAffBQ3voj1CNhHXGUYm+o5AJAH/1Z9lV
JDehSs9srH3c5Eqv86CUhoLSTj93TNMA2Lb0qfajOowuqUAITisH6LGtjpRfWLaQMwz/frq1mG5N
ffO+rBvtsZEOKHa3UwAbdr3OAzururqND4MBHvpnqkAnm4+voLBktYSNJ5h3WcXDi91/sbIl5yDJ
4EzuONnFR11nKFa3GhrXma2+wL9gtIIT4dfzXzCCpscm2q/bABolffVm12vH+wb/9o9UmDJb4Www
IiV0nsCNfOrvZSQ5lJdQGaf+Te1UYvPBUUrzEZN6CT1qjuXAsZGngoozykstpgfi00YI01IaL9s0
TNyNfRaiCRYUCohpS62ZNG7/V8xLiiJW3psdlDx5tk/XJ39jwzwiqrC+B9A04dH2whRwX1n9Q3DZ
bC6H3otigaUMrQKoRDINkItQOjFF4hX9oFZrHrJWE/MlIvzEjyoX/8rsC1ENjU8j6UUpnpBSXYIL
/bE0dPTaPnW8ZdZktenbEVtQlElDKihmYrdixJlSjm6wxoa7pjJ6DjshU+gtGgpgfLqKjP7Hd2Hd
WtY/1N3mXptuqGsK/7z7EOMhFFK5wzn804nujvbz6OGcFovrsLAnHCpL3WfnTqLEoLJ/A7PQepUx
ETRjlnmUxDzWP/a9ijoRtZE6oLyNb8xlv3Exjtgx3Ev3v0OeSkBnqZbqmjMr0ccXpV4HrzcnVY8Z
1qVFPYQ+dTung/FLdy9yhi4+23TTe4XkpN9FyDgNV1YdwPLM8Hgdmsxp+y8r65BRJYP0wNR+mOSy
a/SUdMG+xW9I6LAWux/571T3jFzZiLFJgProuUhi4mosgVb/eMSrYFjXsVPI7sL5uIKW2KXhRUhd
VN+03QkDQ2GmiaDWmn3h8UoECCjTZy9zfdS2s6IN/WO9KAHHlbCYYXz9WGectCZYLSdMY0IbXGqS
qur7h8Tdm0CRtL0j3cpxcq14fx/Lc1qU8Ta8+dKaZh6LKbwszkacKlHZlWMcbLhyd2UNwytBqMWV
z2PNcyDegreA1EEsfVDVrAl6CoEeWCZduEz7oOgFXfWbS5pY7f96PHzID5ichbA72mF2Jt5vs4Kx
GdkMNx/3VUY6KWyS7dzZC3Cx9VypnUUpwSn/soN5ShJWkf/OVdVPEc0DAfT6uHEhXz4sOuBRiCii
4cP0ylaK4sMWyAyOUhJkXE3d3g8RC6upHj+satdRACMSDzt/SARvfKFFsb7HYr2a0qylH38Ltwv3
NMvRFnW7hRY5Awfn8cprB5QWU2ZTqhPFaVu9HjM6PfvnEMo8114zPcHhlRwcYUa3wUGVcJMjTxtw
yeWNm+zz3PWrxhK6vEwSgUBIf92cvi5KdOaJzOZhq6PhQi3o+Ha+8Bj3ZgFmJhkiJOMJmJJh/nZy
In3n/X6/m3M5hmrmc4JTJBq9F5ZvVksL3joy69wlI5DfSUZv/6nOz5M92Q6Pe2KWk42Y28STkjUA
+VuWCZOS2FRkBUqLB2X5HuSeaOXZHaOim14Yyik0xDYxRiuqdnXG/ApQkXRv0Riy+6tA5griaFUJ
dNnYdeN4FL5vuyprxGxVexOzotm+roi5EuCNKH/zW7CJmo31foZXL5SnvmQ0OMo5C8p9pnSjxuKN
IWUMqY6nQnOnTLMNHVwNM4AGse8N/Z2aUWSq1jX+1LRLqG8Wo+KEoJbBN6cPdM2waXPniCNIw0IQ
CBjv2AKlixvwS2kAPIWJ3St2p1qprWlsec+ssmGvDC7cFNmO5hcXFKcgNSWGlwT2gG36Rabfj/Bs
TAiJhjch9VTiqs4ViFWrDFAU5iKsS0gNyR0P3WrER/vYQSjxnHY6hXhDrcoiwwf2B6WEA6xu5WNg
NjG99An1siT5G5yoGVp8/ZGxY9FT0jmCeJixpta5CPh8n9HvprxArZkz9aUCZQMc3+J3+vFlkA04
3SzparJVoroPY23VYhasAWbrfkDpaPnVYXrmptY1g7AykZCF9wbpu9JXRX0ZQoEmKDknzdMIdxya
8hTCDW8Hljg03i0p5UiONlm/qWSe7NkRfeWsyfucT7NIFnA8CZ6sWbBRZ2swBA/rslD/zgKvCVJ0
yf6lo8lNoldvLKj8YFnkYe8ytlc3JWF71YuVMD2seBb3gLGFRM1FDUd1YzdrgxARwYn60yXobN+E
MckSU9FyHQSSRjxipeSkF4Vgpxoylaetreyn9eF1eRdLeFRQS0kHckn3DhDSIZi4bgBv0TTzutku
BqhLwdyxxOiWlbHL4oKBGmJbLVZYY09/iSRE5ILOYQVkSlSk1cE84rgaEXTx1mc4QySv7CZ/CTTz
cmVy0CgJUBNPJNQliiia0J3DOwgByRF/PQLBpcAjriREv8N8lf//mRzyX0HIxN8JyfWflX0IT4fP
sKQJ3kGwmlAWGLkLnoi8XKjh0km79tRxhATmtXgqqVGKv3ELtiC0OPE9+VHngPLR4by7/dFkJqQz
tJ9LM0pZlMegdzcKIuWhegSWTOQa6F9Sf3L5/JXVSU8bxlAM0Rl7zDRNJzM64D32p01/1QSrVmWS
qBK2vHz3QEAQf9+4w9LqJ6g0yjWUUExz8qJjPGpnBDGsX0XXZv/17d0uLM+A9YC3hL9HbyroIjkq
lnQWWVL5PH+nmeEpzbybiWXU0DCLQ4QRihMc0ACeBZCmm656uzt7XpL/fO4DYMuhnI29Os2otsIC
ohq0YqMaJvGZR/o5N33gZ++vVe3szqU32mSX7D6pTPuIAdHEeSnS9mWVjV2coPRcx9JNQ8vnmxg/
2ybJs8YGWwcvWEwbOGh+p4Bks7EWtvMLvWVYnwDIV7pxGwX9O0fn+IfiE5ZSH+s66YoCbNJWuRnp
ytLLvFtonWjYgLgr8ajwHkCkFAtUFt+kNYMyF+RQSEQJSc1iE8lDlZ28Nth/v0P50O2hePmNchI9
gAbYKgvobA0YJfVlJ/Mo//DexUQkGS3LCXcTgFmH2rgo+hwsfvspyKRnqd8zwvGVfFW1GG6hMdOS
C9/MjhiXVGWDuyp1q02fLQ0t1r64oLefkL7xLk41phGxdlzsU8bcneGEUliGa2ab3pTHjNxsva3V
p+Z+MLJvXj9oomazsF7YKutx0r0ARw5vWkivceMmBaaSoy3NlPNa+i/PY3CIiDeYpWXzRFuxdeYa
dpwBugXrT3Xm77ugE+KrI9EVdc9y3iBQVWziqDjdrMR0uKby79KVLoryeeGP/YRC7wKptP0YWE9B
8Giu+Aee47iF6w+T1SFEfcr72GbSJ1jTvL4dy7ojV2GEj4JfIfgYPqekrOM/k/ZmMrAO2gkSrtB0
8gaKXJQZN5GKMCblaVXh41e6mJtKABTuYxe7DMPPsDrI+Ah3tHTTPAWFU25dE0JzAUYFgtHlMfJ7
w3V+Y90iOpAeMGQqmiKaf3PovCYX+Y7zKhcltdhyzUg3pZEmaelxqOCFwahrUsrnCtERbMFQiFP+
PoHBSl8Bol1DU03986hGTYzRp/6+oGGJpkPDGs80O8KvQvDDszDpnAWGLUzj8x6khwJnKtAsvvjj
yRSv1+qO3Ffp08tOPODCF6P3EwE0nM/+eYz4NHsy3x2oonEBov/98H2QlqNqnED9gdhgcL+tJtQW
meqoMDWX5ibNK/EMnBaab1PXeaH4sdiWhflbUQVBtjFHFkgnpyVwR6JgBRuGL4tXuzPvp4DUTqym
N4mKoWY157SHUmzYG9dxfiArAs56B23rvILfjUnmUTumNgoHB0AY4vXHzObp+uPV5xYdFBIWjlQo
cDOYNzDw6HHFYeStkeJ7I5RR68yjBao4poRw33E/esH0gX19BkEP9L+J0RUkBilC4vbbaCkiNecq
HwP4UwxA6TjDwVOpGHICSS4XZpbbzEDFIFFlpG96oLnbmaN+DDJZs/8rP0P76ie4dtcOHksb8cFe
LJIF6srMCIbvli9nHGwQ72vbJ0F89IOt/aSvrHCqOP+uAYbFFOMl2ehOlNPkomVKlXIuyr75k/C/
oiVpjZ+jsmZmT+AwxWJjYE5CEapPj9M+v6iIu0g+75d8+yyi7th0CWRsNCGW3ViNnKwrsxkzfVwX
YHfDX/Fi3H6ndaQFB4E8PTrII/QDoGkYTs5pzGxIHy7Te0L1gt17yeNxpsPXJJwWcMgodNnNC6ow
gU8ZMWpyJwt2QONrfS8A0EQbf174tq02G90ruNKmHQS9WpQu4lLdjD143Sp4SrFAe1U4eC7eX+Tx
N2LpCx71y05tcchkNnrqdTopFarsKHrX9/qprVE/uOlnvhv2Hgq91fY52Ck166xYxRJDSg3tjl7G
5OI99nQHNzR8NI1uNuCcvTuXJzRwToezPcuC+8W2F4ID8H3jLhjQ6paNqPz3w+t+uQRnpZd6Q5T4
hL9AFs8ZHDQr5c+dwDnZ3/Ez1D6ZPO+rA/U712hTN9IikYbO6eeTsvE0LOWWLrcCOXlYZ4+B07/L
IWkiGvA9KhIJn2lu6xkRGm/oyxrZpePdgvwv9zPgJrV6fcfYq7Ct1WKL0sCsHm64jyRBEh/yiWNA
/U4lA2CJw/S70txd08Zu8I3Hr720lm9qH4t4wZbyp+EyTtQ7m27uebpkTv4Tk/Kjp+mdizjpuPql
ivMImUVJ8MFqPhiTMEqJcgzFzuYSZ0NtaVVLVuAWqstW+SegyE4WqsLmu2B7z8tgEPef1u2GkrDN
HGaILTMiV8xOiRymXQGEq08E38K9rWkmAEW0rcWJxyztbd0cqkmAEU0bUcvgFpQTw79M4vo6W4Mk
kx/k/YJeCphKbcdgYkYDq265gOUf/WS2F36LJBKZjJZUrm0FurdfzE9qfDIbfvE1AFui5a8xiYXr
dB1/M0G6Zn1ehMWzW9XnVdTahZnBk7EykUPO8WjFdHZtBY1N8m/ZmcHn8xGl/zKsZ/dj4SgvFrQT
Sj9UZsELg6I5zHhDQYCkzrS/PcKMnZKtfvgBn0qdxLAJi6rBK1KfIzBfI/+hGsFYL+axZdi1+H+V
RiulPvJ9wle7jBDaHdz8Fu+Y34+fapMw6bpxLBDYgNPuE4L3bfcxdJw/9rUnSn/kUG9lG20jjTb1
+7CxBwYWRuVlwYGfQB6YhnluY8QhP2YzSPXV/OQejeLtAojGMJt6XC/peIvQid+wqlhLJUFFI7xL
2Na96tuLb4ou7+R1IWGNQnQLwR7r8rzs2MJZs2YV5XEM5F71ehnbPs22miGgR3ik0ZXcAiNiD94J
DU0EwWksFnqA4KMmfXSXIWQ7Sjwx1RWu/C7DsY6GTnpC29wu+zgXGqXw86/pc8ukD9UzDnm4XsOk
uXtQg+oJNTOhSeBnxEaRyGDKtGlyXmjZcT7/i01kYgF49B1wGM7gAaQY86LMg5sZCtY7/1MgHiCk
pfp3GpCKeDwtAzsvMsy9/GAm0737aEwnDuRD5UPCs2dzibZuTdy9MYxgDePoAvh9Gp+tYLBpa6DQ
EdkmkhZfFwFS4a9PuRW4rve0oQyvtMK2R/6J7JXxpKM6Rm/xdP7ewR9THjtZ9dNxvQadDstqbQz9
1bnyo/ZaaP6A6qvJ6oKWInlXGqcm5g3YnzJtyxikIbVAofj0I9HFeW79S5btNAvGYME0YVmbdtR5
9YGfFdWwc4+jHs9VGroHub80tZXZkU2RN8c+aDeWmBZtLJJOcFNHJwXc48iAY8idKarmMz/x/gt/
zVqlmj5sBRbWLh8e14sEILDLvfqBytRSnpQ/V5HcG91KbIMWtR9EfVpJlA3ncPfqAA+mbX0JPyT4
LICkAoqHs+nA2HkgIjnJ253vfQWEBapkkaw2dn8b1FSVB+A1V5ehg8ucJVmvPDicaz3/dr2ehdqG
0QDWJWYFUk6bPC0kblmX+j7y4cz+eQi0O6l3fwZZJHmdE2zgFT/xaA8BJcKalMUChyr0b/7lLV+/
tbtxNBfd9TYWk//YhnrkaPJWJHaPJvZsOEDEfOP9cd5reVh020CHJ3QXAYDXahdCGMfEjHM7oi0Q
iI6PaJxS7XYwML/dsvm9xakSISigH2ShirRyKwuFIYhz0bwGdFmShwExoOtWeKUsmfKBaKGe37nV
LtecHWu0Gb6zQnqFjNgyn6LsKLWdgJbtTcZysOzkeDIt3jgxJWm0fFDqrxJ2YOab6/zpkjlqECkg
ozrmlWInYMH6hdxntlZUHnuzca5Mrqei5zYR0v9Z6pAc0JbTIzdxGx2H4RzcH96KeC2racZpWawo
Lq8G5dUgQQwhq4VaFYOEGHig5mJilB3B61KwA1YOpxAkpAs4K5/VUZx7HHpthB7CH3oZ7AAzvC20
UPs1Plxhj3jYNJDxswwbVDlI5pHii+yflxFwYCZ7VtqI0l/qs7e+RN3YCJ3lkX+WL8kPIJOA5QNS
yk3JX4/JxAEloQ1zGOFFWMZa8S9h7s2XTXaAQ1c0f4/hH8mcFuf1x5blJxvAcVHnCkNJ716fjCBY
r2yaJ69mNxIkVm0xJJwBmGMpyrmGBnnuSeF2DVVz0ELFUzpSd+s+qMSvS5TGNlfpwODy3LQF9W+Z
sT6PPMVDWOLaT/dcNgswMSqf+Xv2ztHS3D8cGF1mkur+mANb4+PkcXkiaHFJ4Ig4nI6Q20vmuHjT
s24KFBvWBsw9MLhseWC03Wx/SaKAIuJsQVC+4tSN1yIDF5yOL28mTAq72td1EuWTBMOg3xrq93Kj
C20wLRksUbhgla7gHj2oLthWMgMB4dAHCDNzCLIurYZWPIedai3xW1NObU7QnFGelsNhT2OuvwgQ
97/+5KjaMBGTs8WUP69i2PCMij4bXyOOBKsVJyUU99MI1R4i2Sq1r3fhxGCm4DW3P9DrTbc4ZCTz
7avxxWDN7/0BJuIJ9ilkoz4rTvqp/Y3c4Dp85xDFA97ceyqKVD3Y/FWGNHDAlO9Wivo1N7SgWWq2
SP84l6vnJlIQMOLUy2fk9yhlE3vOdEbVRE3vCLr8TgP7JvuHmr4uqQUJUgAHjuwxWnGYSW5Gk9/l
ZyAJDFI1rzM44Vyve7PneIm3Zd0UVG8D75LmjuNdYdG58PHRDgV0U0y0PUrtdX21iGAbA6WvhhCW
VL4Q0uPLKhSPgn9R/zlyih5ilGlKNr2NHFDxmwt1+pTqwC0ufVRsO8dSSXciyoEHyioBz1Jv8Q6K
1TN09Lifgt4503xc5prDME+knCUUo2SPUatFbY2IlMenlj4r8fIoBuUS5Z6VFCy+6Dlaq1ew4TlR
5YtIQuNPZ55FP0clUzhvUGMzM+nwLm7fSiuE8hVZOKVwZ3Q/rSfRsahYh0xB1JpojAge1RXc1lxh
ooLYzOhJxxhV44GAhYQhaEp02YynEw49OJSxlgYlu6WnQrGtlqDA3+ahv2JtjKDWwru8vG3vG2vl
7R4lqLASeXfJ9qGdw7E8eJUYAdv0PCkFODn/hjtkDUsN5WroDzK2j/WMKHIqHvfXUwg2ferK8csb
Y5ZBvQ/MLAIm9+cDGfzB7n/AgjE6Hz1NwqspzjTnskyYzpnNihHA1pJleIE/lbH5eg27xdy2MSii
fBsg2r2ix2UVQ+Nc3HCm56ch3ooNsRe/4XIBQmqh1sxWCquROvw9EsdtCvlRjEQDW0gBYvlASdsR
NVO1stJwNMBpxFzu7bsK10FLeJboi1Sr0jFeoA7ubIZYR1FQVBwUm5r4GAdL4J+hWQZvwa51N8EY
YiyU0UAvwHL6mBhmT61wzz9P4ets1xR4BrojQqIjsSd7Y9ZD/3IjooxMUrXMj01EQjjhqki4DvtW
GF3UaEnBs9Oa0GsJPr5Ko5LNJqU3P/zVbazIUWKccisiV4jhodNPBY6Ee26S3nsGG3TP8FELIoiS
+88I+/wIPbGaP0tlHEaXUPt77fKDsz17UG+rj0VqoHjJc0UUMCdmBrtNv4i+S6taTovgc1Nv/fZu
mA4KosbeCJ3ovZ2J3wUkNWwmGLMliYmc362Bmbz/GsINQkcKXLdGRBDrGuDIZtwcFdTgswiQ38KV
k9TKFGGs46KnG3xa62yH9gaYaoGQXuQI+YN66HGB58GzKRJ7UNtBGBJeg85W/hAw+lzwHQnbKKig
FA7m6BeI/svdi1oA1A0EIlJ5OxXyxOAcrhzegZDEh6YiSgY3pEEDCtwgbNL9tSTNNRpzWc5N3RUc
9mPRvLEn0NZ4/cSjlY3SNxaBhIlWxpPhHmzsUWRWfMJaF5YXk+5wAczKt1gCLvO858gBuS/5LiWB
3szx/iMbbCylqvspbxqWSrwVLgpkimAIRZeYevxERgdnuYkEqSd1qjJFSzh3T7xumlIk7ifBe92b
k2hCYnNJpZ/iovP52+dlSg0TjpR/tpLbK+5vb5WMQ/8de5wpJZiRkcdu/B01Q3XwWF9N+jvic6Ce
uh3xfzYjJfW2y7jJLpHmryahnYT9hYL5xbqSrYbHnH4FY2sczQD3HO9yxTkzbD5+wGe1FzZNqf3f
AgYG2bQSEp2LEqo69I2Sk2pEgZKdRMRw5M4xr6Urwb9pOdam5lbYtjGXbD5Qc6+RJ/wDASdTUJG4
vSmQaelQPnGwI3gv5eaMB8h4FIdON9QegmFXpXk6Zoqhg2lBEJOvsVjd26x3EmHq8rZWZVsNuaPG
p6pD43GxtQIFFi7fo3v/eo+0Z19Nzv8KU4TXFAcQQSiX9STpDKPlKVvptX23vzaspn1rkMsS3P/f
UXixfGN3l1inVMvanPGqZu/+27pwKEpTaVasXsvNmO7mWtOCt5m1I3ngQFg3S6wTPCIOkz2PHtPJ
ggg9IuEVGVI+tzeTOb+gewhDUPT1twgW+bYL9CHYLqno2CD6aXp0Zd4cPtL8lrrmULuFppBTGr6B
GRypllePHwNXza6yW9oRJknlIMWGaCSUiSuDDzj/uPDJVgJKgsvnsJZGsC1hpD9DHwhtzvcp12NL
vaUdO5liNlVPeS7Pxq0XyPeary81Qkw8+jPCh2p59ZZa/+TuXxsuuz1dqPRBSZwBnP60pQdtw2oT
8YX/6LPO2ZKe3KOn27FmzjvZqah4aZKkLJ9HsHyhZaqBNfoNf/Su++fpjjDGFOtBYcg5byzPeE5G
eKi9oUUPrFsTyvGNHmige7SrqJuaBl7ITfm7LrL4a7zL48OAPRrJ3uaKkWhVDrLtjUDCM4d+MvrZ
KKPLyeqzZjquxHQTyzXSd9RGdq6wQIHGLOWR+CafgDzpJxLB9yXQJRTfF2cKX1xjQGo8RyDI/f9S
5XfzPybt9b2NkpeEF/vMLDnoH29bBqtm6UA2VgTCxosMmx4tHUKFu9J0ignnzrrhpEHe3rSOkTXH
9VG6eJjacJ0UtFvZpHN3OPVkj6TGVR4GyE9gDbzVu3VdNoaqafY7W9OeKneRLN7cd4lLQkkpl4v3
4r8/cn24OA60hl4H6igUtpAN19ppk9/OGPG9mgSg4zEMjZIwt7d00q+0j8hWcTyOG8KjdVJ8EMeG
CcqKdbqhvwt0ceD0TCaN+9u83J1/3WqUIWbnT8q4+6SZGDob0BUtT1ow/MSzDWhapbNMf+INAGwo
94RHS3XZ8Lc6BeZ58+lxpymCFgN2j+mNxvnu6g9xROgq4TV3QxPS4D5TI0YH4Q4eEIx10SmFjfFe
OVus0KVre+2gt+AzsU++oOQDFv9K/OFLm81V0/JwSBX8/LVQd3nXyQ1rqKEOiRP9Jr9mZOn5mG8V
bvEnztrByWAw7xeYwrU43cK4XWwkZibCDcS2DW2SHXK4fA7uWbEJhb4XpHXj2R6XW5LDVXCEW2//
5jjKWxFuRtuyyR2lOSZpAMfhL0pRK+0LfJZLtBLGbNQhw+/KOFVnp/T7f1YRZdeRukoOgtafNxs9
41mWijcCfValo/CpPQZA+QV8D85nud4frin2fPOyzsaYqeNugfC9PvBYEDvJbcV4e1GJhh2Hq/0R
y0fsWF2PMi459HYVyREbpYiSBuU95IAfz9l6bpblr5NjAJsCEhk1qc+7yq6RmrspAS5esy8olaje
ORLzHCNO71y5GCwCjYCl7jHl1yZNTg6FQPMGHQUihRGSBBnzY2rzOq5anlQMkkiuy/ZaTdVBCLjP
emd4Qt/tkCWekO1m1MZZ6p3TLnnzZO63UJzJxxapJt1qSRc19KAqsPv4vFqGXzwbWGuiBl6uTTsg
MJvuVodaP+wgrSJB4Eo06Z7dBQJLbSS+/Rsf1HAtTGK/iPA1eY/JtkO4fNDsLqFxxdJiDu7BA4WS
h+Si2A+DNkaO6FtstpiylobgIb377f+spvM8u/vLsga9KwA940YrenBcgrv+8Fnc+5TyJTfZSdbH
lewBWI0HBKxqxJ3hnh1jU/IG9k9uCHNaQayCuZnw8JfbqYYwBNW+BS6QGhJK/4nbD2D2UKfl3ATc
jaHTB2T37MUkDc3zjdQqDysu9g6aNNA3bbNobkYDbSZWLmiWy8A4S0pXVPA0OJ4ZMur55cpVuMm4
to6jLwdBZFhTrLOEk/AhXJta2FNmR1cvMO/5Evgi/vx+nf4aNDvd/H/S3hrStfu6nE4sMb1eILml
qV+M94VOdHlunwsAIvMUIZg7G5PFrJJnIH7yZxfm37fLGXQy3cmZ8zEp68F2VMW5PlWynHAjI4ym
0TZy0cpZcPgOPLeZut06OQ7qId2R+J9oE7AumetYmaIwuobF1+UgeQQ75E0kNPzwL1gOruGopAcc
stog9wxI+SeL0sPQ35AbalZeBnVcHCQXcIyzFlup3GzyV3CiRWzGSRqAYviqvjgWptCuVqn8uG+D
9/x7PpNcyqOxEPUCbJG5IW7n8wKNfjLqvhh4A6AJS9Kkajb/6X0iOnHcsAN7EYSeU2o8MwGLVxEY
jvXgjKSHxzt0fBhgwTCcWH3GytjvucICLbVhdWJ+TQ9Hbvf8dPX3oafFMSYhTb3uVQIYzz45mKgP
hqhqjHj4QmfK74/A6C2x9xnpjzpYKveIJJCBQVQ7/RUfOmxE2Bluv7D0i2cKxA0/DRsiZHdxmupE
V39anlRzoCRJ8shvlQR5Vwnot7T98Ub54vF152abZwhQk8tZeY2bGm1FJ7fsjFsrc/WZ5c3A5EeR
c8W3ybvUHiC8f1QKyxLsnqsnco0KAM+8OUrEWCPbEYaHlW5Ae+HkLe6ksQSU/raqpdxB9HXVNuU7
PWtLnilYyUyUnqstggoW9Xa0wRHAFeYWeBORXOhSXAldQ/ifhjlWQe4xC1I+Wtavhz3R66trYmXo
sJxmCVY8etG6a7ULXACeXroj3TmqTLjFfcBWc+7XmnKKI/7hKjvl64pTuARgzlHugSo2scZaT8hF
6zobyp6uhXR4MxGZUxcqI+DDyy0ML4PrgbQhwfV3nlaBYxHOtLHexxEl+VsJ2Pe14hFr+twr+1ox
GPQiDGjb5zpzWhqI/DaJ4oy2xqTHkSkTRE+2SBTXTGfg6y250ufn11AgJJpqNLflPqkx2EfCYcZI
ho9pvPNUXYJIbmoB75H9s3n+/BFVmxQ/qsmeBcy5yW+89HJQ9gCHvkUS+WJgU4FrkQq/SbkLx6K9
bHy7auelQv57J+Ki3g4LEQlbVV2lMoTUjq0t3FYawChMZgHkq3+h8oknHHFSw0VF1AFD12yx1L7o
/GAzJTUdKprSKS3+irFUMFZz/iSD5k53RAEWiJsUR5bUxuY16ZEZn+HPQYjxC2MlEGm9aJMJe+E4
BVVpDEkIlLaw/ethhVZlCCKgIxkOS06IcIu6GDeVpk+W59icTjDENQXPebbhN9qw29Da5rmDNqx4
YrS5/m97vO2Aq6G3swiDcE0ZB+7ZeQaS/XotouS3ZLC9K/oqlr8IhbC2UvAiNEy1wwdam8m+TjYo
p7NGlEzbEHVDSvgdN+o9YfIR++yGDHz5PqJ6b54NBJBJG4VxHeYvx0+rKJLxGNrCuCj02GQtGYun
nciO7Jd7OfgzeqqGCV9cmCGpkYLz52oiUHHRWt0wu+NihkbIUiQ2u6TBDdZzg+W3w8kZ1IlChBr2
2l/XCd3dvaDdLiYvUz+l8a2BcoJuZzSqpoiEYHXpDhQPrMTaawZNHWwEikKH0v3BMEVKPwN2EFmU
w/thLniFUrIEzceYhmgWRnMw3OUsxW++Q/GZdHKXefsZvUndKWQlEtBoZ5pZeC4Wavs/MI2XpWZc
dSpPkQPTlaTO4w7MVuklUzg81Vj/WgjqPU8WYxyiQvv4OiQdU/sB7m267MOrjvYEC2DkJLCKirzk
pBxztLpW1OG+k0Y/KQpQgLSl0l9Ozv1XOAg15PyTSlpI7IaU1soVjpRrHRJQcYmcvvxZU9QA5Xtl
FnwZ7hRpDtr0bCdDXb8JiCRXbmHFw7p3LKTuainA91lNUNQYYjYzlvtYd3983B2xUTCO2bm9le1A
NoWI6otXNGTxJ5u0DIIGdlXI5Aqd9dxGNu86dC8MY4QjuXcmTjMTJXx9VpsHA9fveFXX14kdmDdp
XBvH+2/NKCH4PFpVI5VyRZNt8g/Ils1NfX1pRcRAHNSbxpPb7KjyIVSPPZBnCRX+fontHOruksuZ
lsbZeTaZucJdOxGXgl1Qm5e3KcCY5Fode74pI7ww4JITRKNmmMr5+FWWjlEJjZXnHPkK4Rl087hq
Vbm6CXH2f9thWHZZf6gV2Reo25FcJqTOtmIZZ1O08MTMbzwK582njiqFPeYIzUpdDKdh1aB0mC63
LPfz+ivWu/J5UO8eCwjqq4dQduqdfgDWfJ8o0YoZMP/YZo33yuWWBsLyId5w4ZR0AUVyADJSdMYE
oz/iSQgKN9ahsvfPkXSYqU8YBiWYWd0ryey1ePgwW/0LX5ybDOCXy9kRRDNpF/Kfqhm81MjCD+bB
YNkHv0/wpgv+MZq4ugXUoIt8CUtxJjJm4mOVdkgAgjLz5YVle3kO+t9JBE1353vhZWFoI+BQ7DEh
IjQj26Ll5U+55IZZkllCLOqTXQyWFZlFgzk5JG3yCVa8KLcYOj9SaacETcG4cex6tgPnmt/QZOs9
tUTqZoEhZxYvg8pvfbpPE9anl1paamxgW47oeaG+13eUO0rwMiYuPgodCJNBwk2xNR4NBOCeP16O
W+V1q3zyTpieIcHfAgul9mbJHMjt4diLTt19K9noOhQJov6E5Q929rom2DisVI71OFB318ubD1dH
jTdmogMYegqDWJ/8MCRgswUTmYNMWi+fdSG8CM8YzwMAhijlxQr0xWvkLuqSJ9u065dwm+93aZyY
+mbUBjKFj3d6rLKdu4wF7lhd/IFMkLRLGuGpdmpkLLGySuHu+mf3BTNlAuydqox5ct1aFFKLpH8g
Q6oI6j4gCSk72dslh/brmwHnTfegom+fFYZ5QLU8bQ4bjwBZQ9jJJcJe9xsIlN1rt4CI9PhEZViL
+PH59BCgVdxuyI3/yniwXSrJx+XxviCyEr5pnRjJ/+v3ElzDG7rWL0cV1IcqHV6kfLX5HNbK4SZG
Go5S5DCShBbqaovzmVX9wl2gW8tGAy/5ylMjftyHbdYqPpGHCpM21JNAYHDGnqVGZibawEE17HEe
VLZIRfKnznYyE1m7DNWCvM5IffAn0LZtyC8AkjC/IZwOssrQnHRe8H/ez9RyHvgjLmbkngL+K5Wa
vkNuluAD1w61/8SE8uOzgUE9LZRpJXbXoxOgO4rDQfzsYaxAG+6ig+h4y2kvSSWiekxNgjFHnZhE
H7kUOhf3nGnOQOGYTU7/uVfrn57x0UUcbH00Zwn8S5VJaGjb7XwSadiNPY98Q+dod6NxSdg2ZdWM
/yJje/1nEjnTisEOjEAmaXazjkeulYaPAv/8NPHrM94oYjB0tT68fU/ttLvH/3mqtTL/Vsst1rhy
qRjItkxGYBD5jkRWUVOXNHTGhIfqzGrZF1ZJxXYYozYwlR+1rclQgDRm7W2zePLatsVg2Pgj9DCO
rGmAIPqc+Wf4XV2dPoMEXFbTtP0sQ8xsBca3p5FoO1Ibvt7EVK0cNANe44PzNUHMBIGO5OdzcOnA
+tGc5yiVNzmKPfOpDJkw96kSXEWTf9RP6emEfkYYT9vZWXAr4IKCXSs/Ylto3Ia/h21uPV4Xotn4
J7fwjLBHQCJfBd0KQnuV8dfrUjLeXQiHHcBQ9xWZNxnaLu4mJFKXZSU5dpM4DID72QbYLT65Eto0
QlvMeGe79SK5qvNEUFg33jpE5xuHByyH5PNX3I7DmddbAxwxhiWayaSBgE1fIDzIYuKXbBpiwbUe
bamAyioFomX0HLOj6y50Ekm2nsRCs4QRT38MhwxDPwDeNqdidgV7+UJ4dwtaECW+yZ3UVo7zBfIL
AgJrF/qW5q+DEWCnGUzFNntrnsxZoxbOYw8TaHb/tBAtytM1OrtEE62hpk1VMQawWRDrgsFGa05Y
XarJP8XffG1gIEvexBKU4kZvumlBzb9X9TZ1AVG4CfCIuh4X2g1N5FBkQ9ZAU3M7AKIMDwZzRR03
1l4pSzcO8ysNeltyX6uMObRiTkDevzH1ts0VqTa8aAmjWVJ5kYdym7y5pTeWhu9sPoYL2Y5b1pMu
du5UNudbViVwlb0bDnAZZ9p6zrY1HZE7WE6HurBMzV85VpxeupU6tZB8iB1Gfy3fS///E7GcjD9W
l0AJYv/ijaHBGFvKyn5e1XTTFlYfax7YFTLVsB8OIx1vUVZiEgPuq02Ji/GMdj1TbD6eLVA2zzf1
csP4hu9ic2N8ZhkATNKqCoLX0GIsrx644rgAgbaDzbFUpa5R+YvmKIGIxR7j8QJee7yQBK8mEy0I
1ZWZm0rK7tsHWu2a371v0jNV0ifap7NKDmn0Ms+TQIGKVyZXTVsORdsWu6EIm7Q9+TGyFsshiCG8
v6WO9jJIMUT2nvLIiJ5fvExzRFCWvWDFFQ/ea2gBqMztLAMaCBF6/lb+4/5wBnX3DSSi9so1uJBN
lErejc9Pdt+wXY+mZOiNjVg1oFDUryljnKLFJ8aYke0EYS9MqIOcIMCDpQgdv9Hk0Vmlwuxsr/hn
l/yqrTCe1Fc0t+mvGzDoGBI0GSCI2CRHoAnDyb8DI8GuRBusOK5AJl6C8bh2MJ47q7qZ/ea87J6q
tkchXL4/cdLqZYcoF1dGmAQ2XDZhW4MBnelvXefc1VvZr1AsEkro5rDIIlMoHbp7XbNNqTflL5yt
xCAxb8pTHTgesSCFT08u1S+VXnWiso6IZUfnoe1Zaq9mNxSszoE+aGzSsBY1KOyaqusYr7c7q1mK
YXea08gb6nQ1xj79vnrp3lkjHq412qLFXptRLpdEAld+pivEHEkFN+a3XmjC20FVL94+sOOHAIFP
3Cjhyw74S0ibAhcga78Kn+odUO4v0VphkvqhuwXOsolOVKCjkPglIFyRYdv/7c84gi2I6hrzRAR2
89a3G1eJtsPm3JUMFg223e/bAGJGghbHWVVttraTsjmSw5CNOXtgW9iqOpXkfoLVeLM+uA1cvnIm
9QvHq88bjzGAF2OMeOLX2lmvYmiN1ZQ+nvyLHgR3v9pICqbo1+wIqTm6ghY0ac5mMY7fKxZkI35Z
mbiu/2JhobYVpFZOiGxfawdl+mCaDT5cumCFLDvaA+RiB/SFa3DAmZKQngKW5gNA9c9A8RgZJIW/
QLWuB7+S5Hd+edYw/jJyVhsxWKK9AwSVbDaqdA6eu8M5FWFlBoiS633Npa9jDjY+ZYKvNN7ZxF9f
NZYEnUt5up/iSxfIz+eAbDQtFXebbp7MF36iShZx2YOwCj8f2WLegGrBvmrE/wr4JuGDI2CKGZNR
V/2XmRZKOPRuaEf6+V5xJvX5ODb0T3qHZdylfr7w9lkGm2WkAzDtrIwTzY2nuXwyZ9BeB0jsUuoT
VMvx0ZSer+/Kxt2Ne1KGXvn129geGeD3OL1q3mUH6yDHsm/ZDVKpTvo7jn1rF5LwKGXkoOB0gaYR
/RYN6f8aIlf9GXZYGB1DSRbeR7Xew//7Svsi2itqDTsPffxkTPH+jWJb5kXKHgnKzggo1uo9MPeu
cAdtsSgQfJEpKOKIM5a40KO7AfdNytlo6dTRFWBgjCmSSmfNjtvFZvGuSFb/ZLpNLn0fANpG+dhj
BYDDHT+NLPrJHfSYWr+fVs/k314ZKoQYDs9G7PYa4baqzO0vXci0vGX8OpRwj021bOLhBEBnDpFb
rAcr5rCCYJY87+QpAsEaOC3m7oP4VnNCh/+/hCokd2X2nbJGDqne2i8PX3HJZbxENmrPrSO2RAMg
R1Ha7hWO51O3J6D3CKeyxitlrM359Pur20ySz2hmtzpl4GQE0KXfQXn8255DZXqhZ0jCKk6Xp/F9
+Dk47iQQDdkUMNcdy9pfPTAxD66aRF3JVIpnlyf7Nn8AEDn9d/H8G4aPDmHbF97Lh5ylEnTh6CoE
/y+FLCv2XvcK3wBvz7xOTbTZJHiemAwPlczrGSsxBFsYnWzi7qbFY28kSFljbd3e+wEHLkNm9HTh
HTxeUU8b6c/gg03Ig6I5CmZhuwqYCnjkACNdyBbYkXbuwXWKeu3WwcAF/56m3Xkmy6jf1/NJS9Xe
xTWxOn0VXX7k3yEbddoLbia7c5SpYecBncHHSSeuuMr/wL1mCFeyh2ln0kdInyWNoq5fKJmD1UHQ
4b7MfdLKIVSMkqppU0bVIiRsQZmIaFMtvapccLlpOoTBe0xkuI3MizA2AoiwMCe1L05QSYDAog0G
dzFJK/CZPDY7LKzorXNLfoNaUnZaVpsw3RgvqwNIfeUJ8XFlFFnNF/P4gBrdOEAPPy742jvSUPru
ijECzRpIzEps+75YdaIgWMgYbLDvjNv1vkTVMbOe/Mg+kmLHpWJFCg/CK+ZhSErPlnHZgd9whRwI
cUnRwfHczL0Ja8yejR0s3Hrfrl/rvAgoehSdxgyucnW6kmBOKFbJ1jviEbf32G4kp1mgoPP9bUCm
vPAzAKuKAHI9euGOChKKCPFmi0rq8Fui7KoVUUh8c7L179bp5qoIVyKdg1Mh2F8ADJ9IVK3tLHJ2
WHD1XdKyr1Vv4pk4ptIpNrRK48uvgZZ4BkTFWfbPBjCFUBVGMO72WTHNshfSP+/6A7QwcOj5HIuJ
GcvLOYPp3N3W6hEkY/g9XSb+PsqHbnQDSGtYPNsBjIuHpv06FsxgYVac/dvQwsF4ztFNHV8PAkvs
oocvvjIDDvl//FEhggZj1vdWOEhanEFSIw+9MdYj+ainPzJwaYdQ4m64SWkX3deJIFrs2v9qt7cX
KO9NFDaxxuFmdGW+Be6eG57Fa5ZpnLS4r6iE2ll5ZC+CRt6gXeMNZqbMjkkCvnPC/Hq5ZziOGydY
WBeAPJYGk7re8rNL21To1/uM8RJC+nt/Ucbl2zvGEcwpqtRJl2VuUWhrEd3dRaj2WkbCfasZHSck
r3F3/kBMTvQ14y7v/aXGxBa7+B5syYpfCt39byvyneOvKAhrpDFt9KSQI/s2zBiJ/3YpN6ugHU31
fq7dv1Kt475VwWhsoZ8TiBNo3mA+m97LEHSy1tBQsLUfs5wAQufe6giF6usslv6Rv0oy6xsv/ewy
t0rxG+fD0JSpgH5I1ikXxZgvdWlCjxBSTx6s/tZx0+lpikkJWORataJZZbB/+P6YORsuq2jlOLJJ
NprAU6sDv3F4E9zvCikfpK0+IPGnScPGp1m1+varweDA5KZy5OSdoaTPsvFSupDbgkwh60qcRkUk
u12edehz5d/3TYoMzy/PWErQDWGEWCLCzUePAJscYEc8N8TUJurT/tak8ReCJT5pBqyE1cfDsp8K
yW9UgfqIF7KxpIBOyV0tXJEPRIUXpSM1OzvPRLptL6XhKwrUWf+Xfc+CTfCO/JLzcc9uo7GB6Qq5
tF2GsmOm0oCY/qfw3apPLGzy/oAlkQt50q4LwIYGMvudsV0IZvS5s9eiBPLPvZONaUtZMTU4oPBD
SEiKrPChvKWNTpn1DOq82ajdj5hfWVpBLwMeobSiyNmD9LWI6miqUEuDSvdS2AwPnsTir3Tse5n8
uxVfQBplJkLzDP1LWW1tgYZqnF3a6BNd5UHcaLxCMHp3zR4K008UAshcGyEtwP7y1jOzLpshySZv
9NxnmWXVbZFQ13SZcL3s9RY0kbpq424Npcv2J4lPzHHpbYJ75RquW3rD7o2+io2IBQmj4tMUOGMy
a1RlKC8SyShJiuWwP/q1mw59DF2TLUyggQvoniftyhSexcEIX4DY4KvXdfO6UsmQ5jQXumep7BH2
DZExyhFk3fRlIthkY0MJjkTvA08E2OquzfRBaN70ZRyPLk3gkeBABLNOx8lAesaCTLWfJAkIbyR9
6vYvgZmVhNx+/WwM7FHUQ+M22QqekymMpYFeW7bxzqUxbX74LWUXU4hjRLdy04Ud7kAyMVpEthg3
HOoCJK3Yf/cD3/miTCRqoaoVEZ4wl6ciGIpjW1kdhjFeMNcDFkMya3Y8mkwoBLPWNgLjvie1OmwR
NAIVNInJHbQfN9l1LNE28gXr9Vwu2+E683PjYnGwwzrsfttgn70U5pfo4wAB+jAQVISvqGB9LYYt
UlOtz6pNDoJ4sVdsmvL+EEKuQh6++mFfop0+Ra4JrwxfcGPdkw/0fSTMLjLa8SQQaaVVJ4gtkSRR
zKHJJLqHo/7FPddt0Sco8kqskVoX0nvcn0y+PoAtcFdxTJu3zgnUjhsRrkTZ2EoEUeLLSwplJfPk
f+h26ixsFTcot84uskWSaeMzx4+lZKawrCmzIMFTA60Bq5ZUJg+ANpXFL+4qt7GcdcwrXmRhECWK
+IFe9EOaIGYtH1GHeLrujuF5ncEcPXh74kOScSJxPZneWi6fX8wkRUNdmoY6/UJxDtPAget5QjsC
HFKGPPlpab0GWgMF4R2Wym19RlKe/n4kxg0xcLfhVzCzNBKB9KC63448AEzfgfSHkAXoQsgy2/mh
J+lBl1+hafbXLceXkVykoxtCfsT/wHBfyuss8i7d5y9fl4om0cN9qv2l++MwiSYMWYaCUvlZrffE
bjmUO+K7ZwF+Ubb7Xk+xxJq1af5jUjPHA4bs2A3md0WSvBl4UmPgI+TzCwmRA7zOq2eHpAQJCe7K
jEi2RvUoHKmWZ0UGqaSW9u5XiIfMfwDPTMlE1xu/9/zvKHzph1aGApU5I7J/Cjhc+WPJ73XTZIvP
U+Vt4KUpGeRL9JYM+OkGBCT69brf/RqVjEHRrHjXDDxUu+Dn1CV/vfrYQngFIlfk60H33NIXld/k
/HUwIwEoBPdzCW7r9V5lwOM6diJH2mnJAh554Q9dBTWj8JUNrr7knGdnRvVU14SIr8BHxTAv3BS8
a2ZTvNV27mY4uL3y8yZwA8+BRLtdxbAzJi63svt5e9j0K3zyK35veimbOdsB/a730K+etuPGm9LV
O/k8xlIBJWQ7DeUCSpiokhM4jxrUOlVSemGniANwggN9X6u9mtl3FpwxYJXOMktpUVpvVDIbzvAr
XJkw24v+oVKyXydJNZbCxKyizdDLMHqFP40oxmF/tWzvQO2O6InkPYLaiXM1FbgnVv1//pU+NbSk
4Za4UIfZRltMFreveqUTIkLFbXMPvUUpOruZ84MZ/MD/FiFqsVz00z5jGgsbmewZHc6pD2tJLKHa
9Q7BVWtz4SBNVA6KnrL5iC1gHO1wnTEvludN4C80VslPRn2oK78gQoAmutM+ptE+MffW3shUpBvW
NzoSIgCqp+SZAew8/kxy+M/4S950o3yILXwnaLJc2kbK0bqahTuNr6hRHXFGwBFZJN6GUC6ngddq
D4weGFjUZ9EHEuRP3dWemCe9fdYi+dmjmyrKm/1A1fGpw9yYV5fl1nPHR/CkI2Fj1OaYgbCaM7xm
Qyg1VkZS4mnCgvTuNga6bJlqPICvNhON1PWOEap+XKLliGZl/YTblDMWbrZ/qtT+Qs4BMdN5pI7Z
7WAjkQ5K1UsrWyM+rJOBFNHwoLpHqjocVVK7U+HjAqpcuUv+YMXCQKw2iisEYy8C1sZJ+lbXMBgU
SYPS+DNC+Z2n8ecanS5jpMKYnPX6jBdgWyGi9cdrfU4akqW1zudnEDdOuOd+9j85kt/2HuTPYBMS
k1Fu3xlf6X6g6N20yduvmWfBxEykmQ9HFgcotG+hUzUaLEXG4hY56Vt7eNIJVhQfIxSmaqcErlrG
nKjJnQw0+dcTNKCAbeXU9MkwF22EJG8fQqbkPMk9ctUjHCPeaXVN1mpuet5IxtX0sVdtrXgkcXBt
x5zMHaBFW77VouPpjnODgr/y4OE/vDtv9dg351hG/EifgeLST70LGNPOrgzVnwH5DO63emkRD3He
gkUAt/SDr7NJChUnTzKQUHNCzinel/F8hzJa3gUYkIL7Jf27GzerSxpiiC5h7j9UtUIw1N6WdMoT
mMfV8vJM2J1xrSohEmUotpuZ28T7g3Q/zKSrug0Y+c82YD5vfVzLmTk/G84R92h9I+fcx7TUXNfJ
6/dPkFpR+Ii0BvM0txI2+KdFx8oFcz4GUB/7trW81WYIDUNIcpYBQMWHXznIQ4tO+5RXBkbVINt1
/+wn2wneAApfCwBVa3RcdPwA0Ttp6Nl7BmCPjvWN5BHIrkaDMAfTFFC3YJgdUnHFfDS/Ot7EIDwN
qBHXfOlorNjADVtrqWMouI7Ks99rszjlzucBThi27xiykroKdl/muJmfqj+PuaBcFA617i1zdr/7
i/s3dfhokMtbsR+R11e15xf9PnMwsyxPRhw+BZnyEO35SkzJF+4sOwI7z77E1EAWqLlMrhT2xxa9
EtvP3FFaN7BZeOvmTRvWPY/qI72ogx2Bm4V2NNHmmWYqe7ypaHr0hnwiUJZG0zdANwK+NAOcEw1/
OjvFxwf+BB+dsfXxeYFuYD1lFEnzHwpzHHxcABFHvhqvDUDlANUZnkFy+ZvM/tDYEjVEAT4ziajG
u9/PgXCozKrONX+oPuShVLyb5MRrT4BTkFviWddiwYc7Xz5mlvnJ618JIhqi7lpemcqspTk/Pev1
8GvBE2j40uhQSmErJIAlAqaOFoSSRfpf3oKXJ0RU8FomgyJZZiMPd9JB9wJvCDsOZdOJGeND4qaE
lHSJl9M4eTtJfT0I2B7x3uqJBgi0z18+noszfIIF2Nbdn72j6ihu6Yw/3n5UKK3M64WaspmVU/u7
HOkjoqd3kO/eWZmhIdfdvC9hHhexqTrkoeW0xDNzkpJu+P2WJ5cTSK0k2AM2P/M+PLLkT2ZJNu3M
RYOJQ5sptDSyjfCjnGBTbC8kuUhStdJck/KequtSXWgmXmy22Waaf2YOE7p8iEQnyz+qn1OoLUq/
Sr/kp7vus6ZQ54DZX5O5rcnqKX+iWeCWDrfnAuVQyCbLW1LRtmCfE87CQaCQF3XZUGRxxs0ksK5u
Aulig43tbUoiLmPBnAwj2Jjov1ViRjIGBMeqUwwiVqyxNgmljTTi0+C+h7P2MXQ5k75zYIxZgW3Y
ugBc6S7tyxC922ZO+rjgmapCwr7Em8cjP4NlXYW4bGrujaxfZYW+XMXXFyQjmQSSzAG1WCVokm5h
91987xky7lZu+nM9VVaKksxZlhk7OVBBem2zIJ8GFuyDCdrP3jXywCPOMJnYx+uIhwjja9myQR5v
I9GUvE1Nqgs3D2nD2BsSciRCKfsWz8zFHxoq/aAnl0AW0dC9sQFNQjBhkINdI8nlfkszlBZYSPxp
e8kCRrrvpC94kgiDr9ZngKRySI7UZvz9xg6Nh02jdO9KjmofUOhH78nHZFtXwPuiQvkk9NoqbvJr
Sq9gIG+Sb1IOdzGG4eaSO6KOSuHzEHtnmwI9QpGUbLubdArOXbsk5f9JYKp8ql269b6D/thBjkaE
LdaLn3Xyv9L2NN2c0AMLoYXy3Ob4Nm43J6KakXWF/XWKn1wrwIHifps+zASxeAbqLnu621Jk95hF
+AlpWn9ZDnFxijpV3+vgHVngfdQr0WS+BoaLRrmPBc4eZ3V7Y4iN/yk9ck9wQezdBcNZW1hK7eDg
PP0eCAX47CrxHdz2k6dFl5QhvF8hNRSRtZHH7vAergXf0F4jn4nmma+WBHDGn7C7eukiHZ675/5y
GpVu6ed2a0npVFconU8s2v943o07pqAJDr8Ke96q3vG7mr4r+66OtSQOWgOOquwJ3dqk7qogBTVO
F67dwK9aNVufddFdZMFHiSXYtcjoIsRdSkUrSN7My7KpR72otKOiFE/3j6WyAQKfMVbW/fmw2peG
HWacjb1Q/3KMmIPS+ZZ1YC47IskcVuvTDa/4PwUjDbEMEqafr7WUiejhipya3mchZgbvtvz4+IEP
ohJVM5IewS42Xq9gl7Juq2Yyc49PIeGhwcw39D6JDYagJ+oEKoXaQYgTRymtQ4JltIGyAx4WD6rn
Dx7xCABVATBVkAfVXUXUxalT+0zszAgjp7UL4FGItDtqq24DNXGfzrnO6KcJ3ZMO9AIKUb4Zyam3
t1o8n4exNXMuqguYeAccIkh6SOfo8IK2TZF2n5KkkqR6vX7D77u03ygTjIXurmuqQVbMpGT6ZwK3
FZ+AlmH2Zx0S6VIUwJznybepTgHWjq9wAROnIMdBJ/dz8VKpr09JJazokNN+sEru+sHK72tRZJXh
8bHDUYkxsMvqM2dmAqrg0Yn7vCvA/bKEKA1PgPOoZVBR+Fv57i8pedNnO9arPQkRa2fnhcVP7xPE
r7slzZA5o94Sh+ya/PqnG3gi7p0FsoG7Zznsp6YMb6ecbo8fi+vArXgTBkebvC9IX60oj7TsDbeS
O2s9BseugbVkE5xdv9T1WYug4UDlObFMJt3lTlmE5/2MbsT03nF9rxnl86uW5Q+1oQvzqcFCFS3+
+15cjqybW72BqqJkhmvJTpbLHCPt/3xOkdfRRXzSl+dpYvHhk0r9+yHXt5z9TN9Sj90PJek8B8ID
V341z577Ke/gMQCxj0zVjgKJnRB3cnD4Gtcebl5QI0jbJBbm3R+O/FkMoepVUNU8hRIKDQGS+0Iz
woZ/977FNFD/SZR4LC3yT1R3lsBacltiw4+DbZKLvpZf+iVYY9C2Z6YLAL8MwnizG12Xgo9sGZwz
Bsb6kR72flyhdz2KcUzmlDJ/4ciqYYWVF0rAitzF/FSOazE+2h7VB1fzTHO+yHO1b+QtizqSIEzv
2/ggjroT0zgPGtOVSuV1debuyttHVmdM30CVscn+UB/WW47dRS03ALGkRab1ijTMpHunmEB9OFhV
/+a6I8HEEiUV1Dzz8hs5mTh61UxizMHNfaP4CAh6FFhvGpxH/itUqBK2JJTy8kMZ0AOX6XcN553F
2di2hQyoG7tAZQHc0NO5J0PFPse+efOUaxVmluPRxjta6mDv9OzaSZItoRki/13IMKFOPwv2zF1e
f/xO2HQ/kp42NJNX0evMM9iA8yJ68/4uJoUtBfQ0u2sckWhMCImt0LNbP/T2QZLnMvn3c2EOj54C
iCVxvFR2h8ylg24SubR3oeV/jE6RDMRd0f59pA25Ur90pgm9n7JL1eRNx8ZzLJB23RpYWJkRLXKA
4XKuDCDd9kn5LFHxzTELCP9YoZLfONUwvAVaJXEcOYy1xkVYBCoyD3u1GlO0b0rHfleKjpcIma3g
2QfZTjLckp159VjBd+rcN1aJjr+z7buzb7jl+ib0Y+WbEvMDBhxxl924EXsYvSxwBztDUqCGC4qC
du4bX/3JtiYOjJkKL1LLd230DgJNZKXm4YnBOqzoH64HkElrwZQ55cUTBShhuLIHW8EivI2dyBDR
532VGvwdEJfsZRoZ44BY37l7+ukkADjFtw6S19oRql9ZEzOBVL6yxKHd0MlwWx1JHKIBoVOsI2bX
C56oTDBMuUfR0gWfNkxmF9gBg2OvUQ1np5B4+qTWpSH671doHickTYAFgW3xTx66MJrsuw6wHzrq
9uKJlaXo7bE1SmOllxJMV2n5bxMh5iWD/Abfmmx8fNttGctDzjPIp7Vmtt85ldNwNYrAKiXAw3rY
BNY13zKA3FPURKyXIJYAsTKPaqHshB9CLbZ8c/beeyd0F0f2wN37wpa9UqZcaNCfJv7G3xa+KijA
68rJh6JK0prnhAjh9D+OpzgD44eG4PX+LQjo7erhz/eUJgq/Sq4t89tkOLqIHaK1us3pWVdyiGkj
j8lOLVUPcEyFTvs3ACNP+46IKwXIGmPaxheUnifmHjYRdmrF0NDq/eIn29jiRdUBECJl03BPXNJl
xFgCpUTjNs+9t4Vqmy+JDVry7O4B0wGvhE5ddI0aIqodcQY6drRL2BPoTnL704RynduJvsuWkgCf
9rKvqhm1QhFaaX6Ms7rjDWyMTg/pdBpRGlpT/PdAQSSll0kWaK1Q1OYN1+AQqCVUnHgGkXZ6NQCx
9Ovmfk2vGjZnC9060cwCyjA3FN9lgWXbbl94VruprlT5QBTz6dym0W9DdFYHiDDIXEenBpY1okQ6
KC+Bsy454UE3fXAtyILpLhx3vHnfPYOpuk1Vl/Mdn+hdbwp4qHLMfpYbkBxBht+xAMzyrSk7g3nD
csODlA7E2fRZH9VZnr7k65y81EmzKVo3Km0Mry/atzW4XF6n5BPE2ep/Uzc0loHYWiUuRrlKF0iJ
ngOAjyAIo+yhkncOq3ZN/3y3ohudUZS8ewiJxm3PFvzWjZWDGcsqFEuHN8MXmplkMFqVsUJ8+SLh
N+EaHjuaXFl/ZVwE+Z/CjRbjbeetz4cApc632f5XpAm0MZq91Pw3QKKYpkA2AGcBpxMA+iDV4XtY
QWrDKvDp55xjz3NKEmNvaVETn0mzN8JW5+NkVT/ZT5xvn5Q2BbagSQQOfDp3kxPhzd2MDi12DPF+
bAhpjDehYbyRUhmLnvPdFRxxdmtGgAiqrztxsF3fsogQF56v4lerlLvMB4CCcTJrcTboAcyigFX1
0Rj1dsCcejcK6ua4bUN1qZ9PKsjzddpfDLDdwCrctoTMjbY/PaOYZiEb8g3KXJ+l+wB4SjtgxfGL
HRd/EPILmWB4fPdFp1qUQyflqTD1a2dEOTDEmOuTCAOWaH+CL7QQAF9I2GpHU1wrv2N7uYAoikTV
P9i813e89H6q5eRYC9j5+OQHR5/NylHxXbAaX3JwCiMFIyXYEz3BQ/hwquapVnq1ev5bR9YpZpSD
BttT8+Opy5oLmEnUA40m0orRIzIYr8A5mVPwpmcz56l5SlZBpcg1JxiGRqkWvfPxoW0aaL7CO/ff
OLvRmdkdoAoZgpzhlyZi4LOlJNi3Dx5CuY0slf10y4LYCpYt8gafTXbMDZeuIetYBZhBaa3LKh/M
m1MAyAiUKQWjlXZZuIw0ZuLDVtM+byQhSG7hckMS8sMZpGhwRGnJuHhiVgV/bjUvs1nsJBXnQi5h
Qy67EIaCwZVIuTES+QpVWwb2E5VMfsKi/GWcoVx5r5fckmGjnuco/nr4qV9nwD59hSu4aZ5VmJiB
lbibgEy97eXGCp2V/272UtEd2PJF1C4eRXnLUid3Af3yoxWcQ3PXA0WOpXfMRT5ZJ9iTPUzaeSLx
75YMsP4r0+jYbMRK3MEmzZN+PMvPGFkDh1QK6R0ydBBsnU/4NKVAq+ZuiQjg9jYhUrhF6Z9thXjG
Yg+dPYyilYrR2ReYsN+uiCLr+bKwcc7WrwVCJIuTrWXcRnaLJPikGNZ58x+CRfSsH/QKgbLLnJ2y
1+9PbR9qSlAOHL/ZkyAGl9h3MaFUSL+gwJHdHxw+E9h7XuFwUQlC36eXV5cqpQPumY2e3ZyEXu+w
H71Yl/yhMTb2qWTxvrxWFPzmUbBZG+yzxzKIl/57HaDhag62pJoUlXYuSUiyPq7sQsHCUClQHbcW
8ucT3sFRbZVNwC8Qe0C7Q8y3Q7gssQ4AmNZNUF6Ggwu6qovWxfRfF67FdFHYuayJYe7npszAKu5B
m3q42AYIWe3edH41fUhyXUWUplYuT7jpas/9s7TCywQKNirZOPo4laXn+Ln5PDPARkwb23PgrVdh
Y6umeQsC6YjqJmE8PJOTbvr0aWyUhgwMSnn9iTXXgJ3cBLX7yxgAoBcFsx+LgAZsjBmVg/GM53lS
2qMiJh+t/WyM2qeH4le5enHqbqrUIMtDTDm8f3em+/gRiZ1bM3wXQolERJ4sUmHa7JylYhMEIFfD
3JcpIZnmgRk4tHgA7geaB3fD8E3Pfy4XRyQGM1yQrVHOFwB53b+GTGzsbMwfjsQ25j1nfEaac5zK
uFttgTFnYe4diVrH6T59E14iVBww5Aa0MO/pb4U0e+lx8yCCD93S613UE+6tmk3YkUgYeFWwYZtc
jRjX7MBp4jTLHh4fm4idBq7GRSUGxoHLlo8S7PgV4renj0jADpDWugACKyRdhZe+z18JkfW8Imj9
sOg1d9CfsHbCPUKcBmAmscovCy7Ru85BVSufuZazCvA+qdhqJUxttm8zvuIaQwyaJBahyyjxHJwy
LOfY5z2mdykYzsBCx0LMfXYZW7Rz8y8wVl7CxRdDwBk1idvorMI37TSe9Rr847EaPAtibCR6UwPd
nVmMCSmz0uybBAwCgsH8SDux8UCpryE8Sdx/RToly35u2k8oUC19Hhau9TqBeiS/zZgPT8074Fdo
PllB9CJIsJxlGxEgEuamc1Seo2UiaSC8NA9udJ3o7zhkJ2pVnEQk7GjCIwK71cMaIVIBUfFANRgk
83GLltH34G2aLxr1Hrr4bdRoc37KmKJYVjMH985VcfPiU8JuFfv99OOxHjOAxZU8eWzU0uLgRMKz
a9u7obOjsO42+ELGWbKPMTRSmNADrKT+utvMvqgX1WOcZ66Y53IqjNYaq7pm1tzsx2qu5mWZRDlC
G+nWziw6QIv96uAaHK+GMc4+Ni0ro1o9KpvbqIX4T/rQQJsa0GZWJc3CUWbBk4J5fOwvMFAfT7jB
jDs6e0pD2SPdl+DMs+p+ALu+HSAzoZfoBLNsnfj7XKTJjysEohFq/7lbfRpjITVA3M3/U91qkJK4
kzz2IM/pnCosWJODk7xhhPX8ab08xDfht9gEuGYkiHozLOgNPtjlffQgA4GMW0A4W1EBCns4g01Q
9XH5zEr5ObZhnd+UlX58m8ehG4xCX5aFIsVaoKpS3iX48YvNA+h99bfq35HR5tJMzAjlFWPx0fhe
aNmorI+gb8A6hmt+Dve9Lg8z2r6GuYBY5TEWtLFiNelIDc58IdOXbuDDQn586/AXv1jinKAneoYc
C5wvkA8K4Jh/YtYzarj4K63o6b/47Tzl4jAlDpSw/16OYVI1vGbJ2hMmSShGAJtqTY9GtfzUNdHC
4A6Ecx//WuSp3b8iQLdjWhIDAbalzGEwUOFYzKouZ/4BKVvAjksQfhv7H0A8IWcvahoHek55bWQc
oHiMHoWe25k6zz6uenfwuDPQ9Y4VLtEPf9v0/zxr6c7IPqkGdEg7lGEhvkdBrk1MuDMfpJFVKLYX
IpedHf2D73Bo8APWKeAfE7wgvgEH/J/XXjaKOEGxhQQA+FaeHNUW2yyUmDJXDFuU8eToyaUlGyoH
dlQVeB7AF75OsPLHPP24yefuowKiBd4wRgzytU0TySv1pIz4PYtZbS+INCvd0JN0fiyjo7rPMsEU
G86+zN+Y19ZnDTbTVBAylGRTxSRR+Mj97NJ6G7hVQprObuWXyjKrjLpeyJaytpNfD1wM0NWNUXHm
/uboe7gUNztJW8AWV3g8J7mjqELK2MgtVg+W23LApYaJmnyHRgrnLYDzXOJ5zu+2bCVmk/BBUR37
O8shY3U/vdPF5MZreLoEbp2uJ3X9YnAAWnfrohElGvs6myM/JigMz+1WK/svS9r4YhqIOspRCOa7
kG7vtjX21kFBSaDX3Zv1fLfg35rwuG8Z4rijZP+Dwur4vwQukhKfBSpo5AoDb/xPfSQBlgFa3WY9
1u5PAVUCwGsYA89FenotI8QSfFeuaS4sTbMJm2WYMy2i3EHqFDYFZnJKHMkOXCauw+r3lhUqbyVk
+X2FDjrR1M/aNotgaaX2mYxsagNFOeqYMOeRfLssGqPgIB9G0zmI8gtkEOm+raW9TS6O2v/tjm5N
T7B93nzLKq3BCFZtbc4VMDegIV34TItclzSuRW/xaKc1k1LHnfQuaoOtvCop8ooIjbYxc6/te5bQ
Qapf9FMTXXt4f9ATVP7b8L/rk5I+SwSMo+9tXmAoIR62upSOdlnl3Z2N7ObCY8ZpwpQXhF7fXhgl
lydHFGQuKu+wSBy5gkD1KyqWShMl8551EtmxbC0kzuaRXrrAklh4i+UKSad8ga9MtC8ABe3Q1VZX
AnL+lLr/x6TbB3/MK4ZYfu9q880/KX99wGkqaLPr5iXOEtKCXN8BGi/Gp0JcKE435mKSJ7cnrLrO
tny7aW4LY9ZJsmAthERYvrzsVrqi63URB7DB3mm8I1re0kWpjsGTr5GUTuYOtkmVW8Q26MdJURAQ
d1rqZBxPLvnW5/HZhSlXEpmL7zoFcYiGZ4ExgE42T1o5VHexOngAHaHEd0gFCpMbhaNhEqaPreqE
HZRog0glwjh5TOW5P0EGztBNEBzxaU444K76c2Yvnr3qYILYqLu0KSRB+hYVRA27k6lBwys+Z53x
vtk/uXdqNbXxVg9UaR95S+8XN+XIuPGmLmou6JWywwDIibM/jPKquQf4EjNSrKFS4mGEw+XGgbte
YYvlncb9fcxeOlu7KHfgfD3AHegtLH3tBJ4+lnIbzL5EEHIBe/nMm+8XgHeSOdhsKPGyZ2dgfiZq
u72zM0OWoxArGEsOqqHt9TkN6v4UzAMQIGw+B1/rcWnIyqifRxW78Ch7gDSE02lFa7PubmSXmStN
TsT5y3hUQguCew4vbrVAe8y+G4SmWNeWZ0ue7Bmgp9UjQ0W8gfWjXOyI6fEXE4QAQONG47AjiPoV
pQjEPCKl59zh/VaB/hGuSz9ZbsrVCLQNqNEhUAw04lWetw2/wc42bCzHrvS2w58J/QEzCqlXanpF
Uz1DACp6pUrBPcHT7NgBk0d1rBwV7zTwj+6ndMU4w/T1xgXNvJs0CDf7ikJY7GCPRJGTvqiBW7fP
HjeJfQFOxrXAwrwoNvnz+kx/IMLcCpULxmdYb2R0zuDe04rUP55cVdTDN4dXty1uynnynm1JW/+Q
yRRQPc91cB6CDq0WG1i703SFcnJ6zcOa2xE2wazVZDX/BvMElqoUMMhQND/UpNazyh9rxbtEHrRK
dhaqq4mqv++hUC6RsQpE7yWxx2v+wD1cxXajWThpbyZGl3UKNs8PbMVwf4EgeJc9SnTGLWRC10WG
SFDjpujshoEk7/+9lt8aBHrZSwhuoZuhbbbuFDWxZsSLV3GmRk+58b8NKDzF/ir/lW9ILN5ffc6l
M4hS3ERteKa8065h1w6ny8fPvvsA1cIzQ+ft0iXvHNg/KwkAhJd41N83sjh3nvo2YxM7quOGKL8a
HTJ2BRv1S03fT/0MYDd8KD6xwkyDj9InVz1+uch/eAqhMjqEF+QOzmkVPdCq5/VLe6Rtn/kMMaa+
xg1gzFBI0TSKvWSw25n11hRUUwKD3yN0T5Ybb58yESCCPSWqw6H/VW/OxDcponU5nk/SBGFYbDdE
282Ye6RvYltHeYMZmpLgh1H9i3NJ5ids/TI7QipnLCcL1tvRE06D5FwK5bhQ2v4VV73rofsJO6NG
YPawPBdZ5Q6xSKCOhxAGS3FF9FGFjCuWjBJH8db3PFktIEBuGjtGyQBY9ZXcYl/fYl1FSGgTy6rx
dwiyImHEZI6iY8Mzf6tNW253tQlwRMjsUchFzAaWEWXEsNMszh2E8qIlElWVqY8AhqzNWPK/Crb3
C7KdGTmI9unvyeQxbOzjCeBCNuoEKT0aLy2X+AIFEnmO/lozT61LrGdcLOODgr3HjlwZITufmABI
eB7EZJcyxujjviNO7VVikgVHk+N9aeO5F+ygopBUXkO1JxvPD9IyK8bFa9khHAwOFd34WGE+oxdT
Gwtrv6I4J0UYNMhWwXU1x3kDx8T3qllc8sl+HRVUju+k7K5C0ETxY1nwECoyVWrNkEgHAeeQCabj
NUnvsCBwa4xLfcGD5jjCFgKZkuizChFi4nrtFxyGkOPvzZcM8J6KSVbKIBdC5NbUzY7nCeHLmnNp
3c3Nx/IWRMj0NS9ABxf6fWmae7ameZBp/WvInDIo4hM7qOoOoy128v3LWv//HbPXP8lf40qk9Q2i
nh9s4/JLeLRegXi43dFbVDhVhpjCceV+NMytL4PCTeRrLdNEzQ4JXNvZKr61Buu8fZeHQk3b0PtA
oX3xQSYAMn+C0DLWxop0Zl+78S0rKtLjlYqBx06EkITVOP1iZgZeCWAdJOic5y2qA9Ho8I99Eoa0
z7Kqk6xuyeKUQ6LX6uiB6FdEZ+KIg/KWXX35SRMIIWdoFKSLEGhjVWcDYw/GSmtlTU6YG3BTPokQ
ZMVxle6hRv3vNY0dmXutjT7yEuz/ktqjshkyOBeknmdHTrb3+FMs+263w3Wzvp2rZp9qfb9+G3B1
IfzUVQC34T0UUPFrXJkGHNg95yXRSyOxnlf4g/ItNXPOpMxWbGewqi9+j9t7+Jee3y8pf+QKsS5q
PrCNYSmCN0r++M5eaH1KdHA4CN9Z7TpNq4Va78s35b4mkes5d1cweR1SMG+krdYKg0GrzGPALvUt
9aN0X3apKSyp6OJD6rrhkoTyOmmIPtOmZmeU8xDcZVGUQ6N0noCjgbBqtQAw9/PV7Tsrtvz68EAG
IfuHaFMWRKgAMuH2e9ThcR9Wei24TaCZGEBcQObxz9TUQfgXz2+9y5LVUH07wp4/b1Pk67pIcdzi
t8vRsgZ8IKAq/Ui7+fWNXtxJj4OWggLx3h8jPX5QI0QYb8Ml6GjBne/6f9CSyQMfziikYIN5TBl7
PNCyRsz6LKGRr+sYAIWX2jIZOv7e+RmoGeK2kqzmpCpKpw4+hQR83iY97iEuKNvIL7xfcCossF+j
39TwZRoe7FMrpKZdDIgoxYiwlNujSXDG6rz1VvCSzTfcY67qY7Had/HpUEXCZkZRAet6z5MFCRUG
w9R0QMuCPEeEmQKe/tPA3CsCBA70ogTK8d32WO2jficlDQULhkloxbKHzqpqOaz2FlCD5jNxNfuV
qmpzedyRlOrckzq4u8HUNJTnej+sppdKt27yS/k2U8iouZTTG3qItu5Ji5+MxxpUZOW97oqrIRX0
b7ceHAOLqSTksJS7wMVND1xrKQv3fkZ3E0h0iIQc2aO6amUrHjuuDlpbjKmWYZ2NEWjdNreZOnOD
/ODPdB3xhKuOHeKDCgccQSjngYq2oF1oivOaqkFeV4s8msdearwaNFIxf442Yy6BwtG+5dIovblL
TpPgNFqCs7NpzvehoDNfiZ3PiCaFatJpZHaLkHxS3l1iPi3ASxF94DYgls3LuvmY6GHOqjhqMN4m
QZ9BNdZmdS+9LunfZ5YmO1IKDq9LGShXLttLpjEBzk71+iuty8LjzmcvbS/KfxACttyrzb/NIrL4
8FQ8YhYfmdKVcxr9uXoHyS0JqQCh7AYN19023nQ/gdakJSmuargjUV0bE597GB/tay4NhWft+e4p
CSO/I1kPyD33mhwEoH4An7kvGk09ne5diHfSGYHaFKBNG0NnkOZqBOaNvGxFJA5ThQ0MmtOYpewB
visQUXyppuZyOC9nuFp7WJhQUp+sPtVI9zcCvls5XcGtG3bpmsA/85nEZmsR027wd/J0YhYmH7rL
4ioOpAVpmmdwLjKEL3rI0vPe9CpprNxADqseT7gpU0doNPcSbf3xvv8NG3MHjb199qkIeeJjS6/z
7HP7XvRI7Q5cMGmb5oJsQiAmTUn+e3q/YONuOP6fVp1zjUl5AMsIl68BicduV2xJet5HxABWcWZX
mik6P9uU+G2CrYH9A5Q+SB/ySxbmwN3n4MLGPN4AgTxilKDhwYGmK1Oi8SAn2r0bsxiLo+xFsfNH
CVP4cNEYI/brr3x9y2NiElGdKLYVox61dPp240NyNZNc+BvcXZPxlkH9Wt8ckR/mlPLBH1/UUnVJ
TnQRyWLdyYIpgdnPgBVj7CYB/5NDDD2rN4YHE1YdaPTF9hLWHQI1FCZQxKDxQrCOwRgABvtbwO+n
i8e2/5g+B/DbDHZ7NIa8jrd+Ij2PYcKnMskguY59VlJ5FA8No+e2Nmv4B/QXlnSGzZGe2lvp0iBf
FL4gc87xgTcDdA3Fk0CHHBb/iayQT7xE6FyNdAe9I6c+SK5zPk17mHho3egUO/x3KFz5om1YVosn
ABUecmmPZyRNzQTgY4/vqt8VOgfaUi+sHBEEkBt4ttpSyvg7fcvLEe27HF7t6F/V5k4NQKvVGfH4
hcUHYvt+CBl9fZyFtKE7NESkwm07dOd1GLPBRv5pimzPL5oHe+G6+PyGiAYHJU7xlwZ3YerstgZR
n0pqIvFwF+NRb1KiTtr55seMOEY+R+hDVQg+6yZ8mP9QNmDLf+7MDMEeJQawYNCz0s3U3cQAqogN
CZX2425FAbZY9dLZjjkFpEX/VCwz+vNORSGMxSNroUJt3qaWepMcSaOORxvaSP3IKa2LcXEvIXCP
n+jAm6Myk5lgM4+LNlN/Kv2GYVhSxD8T3WX9FaIGIkIHq0KOgnbslktBtTwV0qHaWFg2EwBqau7G
vH3sTVCsBOHl9q20e35MPdOEhMu4gT2oDmqn/xxrBpaQzSKkj+zhwGBO+uYtcN4BUnWcmdvDMQMF
faNgQT+8N9JsjW091pZlfiC0OkwxkffZFh1B+uOnwndVwBzrgjgTVqfI4vwJkbESt60pKvIqt1M8
qwCCNl3y1s12dlQtcDXhAtfGDYMOz8g/GUQ2dDmxvGIodAtwLQhH/kYdbZw1tgxnCvKqf0YtCePi
FymXn+eK0m9PXWHBAaKmLTWst+pOu9y7lYRVpc1KeRYarGOExDjzOu6XqHtSlgd2fec8pJparQ+5
vrHXf2oatMPpOUWtkIrfdp5F97rD+pE64xP3y4XwXwxz22FXLTQKb+f6QFOBLUjL9lHXKYHNzuMv
mnPBD48TDhiEG5mMdGOIUiH4ce3DvvQ32vOThv0BrR61iASayEt/9YzBtz0F+nVk2SypWlq9p1RC
TKp5gfuHhAOilCMw+YTWaJ4as3GY5W58wIvXzGe/PNgba8klHnyuIJlxwuSHOPzdyaXREx9OLAgH
g+OUKtY29Kg2UnuzfON9zWv4phY+y1hJV3XpEA80Wp/nLVtU53qQlq/3HJGdOy9VgJsBiWobZD4F
2skIjPqsSYq6U4yzqHLkSYRvu3XJ2VZ2LfsL2E2JT04L/s0/Y4BiQX/xo4frLlfsmR4+EslXHOx6
dzQJOlx24Y3B+Tap+gu/9LP+rm0+bBhvfaYtQh++hqVFdRShgptgFFkJD+I/ZLcA0qKVlHrzjYog
RGNXmLhSAtmLL16JlauWDjGI9rfQM+TQ7xT3OvVmHXUJQob+7KupMnNmCeu0X5ApZ/DSZF6uJJP0
As5lJfAjHs/08vZLIVdzdn09IOgYPOMkaZUjn06fceGzl1lygMCdJuZMA1ohBuyDRtiX62P7cWG0
L4B0cs8IfCA+COiKCewGAMg6XBL4MedmBskLbd8rp1bNArUKbPrsXN/ZJufgqCcMURVPHk/S+nch
gvIut4BtIB0hqZpJgdemKyZxcm66OZwq09oDWZ4KuX1ZS9IfIJb6vxrTL6myPMYnZjHcvVar5mxS
Anoh03xOS0uCwR6LZcFF6wFnPerSTP+mgPcJNsHZV3SJCV5aCOvKgsdnJMb7rssxoazthKEBtCjl
L14hyomiRJKryFTTs2Jx114752mU5EF0Zvqgq5m8gw5vXWHa0WquHEHbzfMJ2hXmpcc4z4dsnaWF
vt4y87Ju7CNdKLGb7Hm7anqC9hr/w2QWYb3ZRYXFff7H9iuj9wjOqxWoBeIRSK1/nkuAIENsTN24
HS/kx6UGLpRX2AKlVUl0gVQONwlA5TCrj91w8yQsJJMs76ApH3ucdMswbngTGvKC/9GQtGWiIZPE
kbp+WFKmU48EI3VAbggsY3Vsjy9NcCAtv+oxtSni/yXBgGu4JcweHZox3oy4EB2SiCZpEalU61+1
oa7OEO3k39eXTvTLXjaBLD/1pavOZ1du07Rm4gnin5k+dVXz9ZEhdyBGv4lHKk08+XRUFxfAjF4O
1m1pzGOIgeaZIwe1z3J+/t7FCPWIpvGTCMqQ1gFcTjReMrC4R/0t0BcvSKmYAzCTy/dWh7S/I/d1
5bqu7CGZ1cYbGXvkdbO0vMQdws2WDzXdNdQD+mq+8xzkZYNveQIn+DCb4E/F4f3V0KN4YJqrnQ4J
Ti4P5lepOz4KEpWEABLsq9Hn2ECGFY6mWmjY04cRWtNhBZJwLz4v3ZmMctyG54FAJMV5fa6jPHju
0f3trvk92/UdHwvtgtr2k288aGsvfpIMmu8DQdUsNh6dTP1N2ERTekLi9qVjPGonzX9ZOLC5wcG4
GLkZG8Mnvn6CRFQvIMemiUiWg8NL/7F7U+U/7mh3fYb3HHMQhplsn9SAQmC0uAjes18HYG8ZtL2c
OwCnqei7LQFARRld0N/sVIt+hjgLRQcWAvyf4uHEXmLHesel2QIyrv59+jtKKcuZfdclFDWH+Rey
VVJJy3VHRb5IOQuUMt2fLlAQM2AlNjU8bZX+I0s01+BZzx6G18rtDECMYuvwMvHYweu4flIpi3SO
lwBR75ONc3NWiXI6ZU8EFwHM7s2V7BK8kL1VSOu2sgtBvawm3sMJ/bWNN4JMxBIttX2f+gm8oqNW
KMQVEKJgOuhHdkAsBwlN/qdnRmWdb3T2NxC/xYGTT4qtJVmHFid+y+uHQgjHPZu+XMXAnf0K9Pam
d9jSE83XOMPBw5uC06AT8RsatIFrhxYQWfJUykjRYVQKWTIFI+3fSXiA3Ia0LFpANsAqSbZxd2Rb
wv+z8D25NQGd3xNpAaaBH5qYcssbIbXnTRFpMD33GHjmBSjJ0n74+GiF0vGXYKMpB3uI58cMPDKq
4Z62FsSH9dKUWhIAW1EiNNwtvkSfA/2rzLvdK/GuV1L04R8IORNtozoWl6ofxH465s6WyN4j+FLI
U0z//O14uf+lC52M97clQWUma8WZ1L+z8piDxBpIDkuH521sJkyLDdRQx6BkPWdOzOTauWykr3Ts
mDkIvCuX4s/mUkHQcc1RjYZV4YgAIFwCxUV+o7SaI8Y+TK8a6P5ZiubYF4ESMECr+RG8BcX+M4K2
Q4Noy61R4XQW9XUbvaOUwk6AuK3Q5cM8alZLZ87BpoCu4Sn0Wh/A7WaDKIgGD+bCEEs3/HsQZeX3
Ay6r+GwKSmR+aMlgdvJR/h1WV8+MUJ2ARQAwW/i0p34S4ijOhFw4LHN5Yi3Ui+CMNB5vMwWI2p51
pawBXPnwDfVZzSuKoVu8onuyQoPd5V8HIbbvYp+TplySgJ14UlJXuOenvlmAesjh0tzcBhOFiGug
WhsXUQLNYNlQwfCDMYQY1mC/TBQ88ZYIJlYbu06m+KCNqCF4PVT/gkgqtqkjuZ2Ao4lPEpw+B7rp
UJabUp2gpHAtaIKkBOApUJVkoqJyRBxX8kQCze6BmYh/ibuAF6p1NTaXVqbRk/oagK5ddCALOuXH
fcmqDf3zSVTXmRb9cPnIKIBXQ9m1PnluS7Bdg6fljONgNMI2CXlEGPDaYZ/A+Z4ufMPT24yO4fcb
eHkvnYMdLMi53HfCjaiVzPMYCMMIS92EyEeyV3OMpjnunZnao7rammb5yYJYU11gzqGgAIceSJMm
lmeQLtrdo+iCRZEJrsJejYPyQoO4W6aYgOoX4Qr2R8+GoO61MsCQJR1+B3A157chE+JelOJcPtIR
ZkaNn1YvCk+ls6L8xkcrLL+UJYST7goI3vTOD6EsaXd6nolwOelOgu6lVMRx6X7N2kkZ6mOPsBye
BuqLjzLq4OgRkIxE0BcctuWHR2nlm85UXjvLqcjtV6cpLlbnqCzvw52cT37fbTbnj1KaB2PffKqF
KPgVLuDvapL62Xili9jcOen0oBI5btikoSgj92Cq8t+LPbTCpnwTjOAxr2BVMc16Iidn7gglKjIf
OfVwZIJzIZrkTAiNarDRC8eLNAT4eH/sRsWd1XGME0IJE1F3xPGRykCpJEbeX+xp0lcjWOyUbIhm
g0L/3tm6o7zcmB+mzwE8AS6wbQ5U45xsY4hb1LrLyPutBGY6/jFd3xYjo+L+gFPi334mjPf9XW6g
1Utr//FGogFnHj+a2kmhNPIsBHBHeADb3xEqxdb08Asjr0WciMObovb4CxYNYeDPMtlyKWznhx8v
1td+hI8AAtctc9IKZLBl2FMM3+Yc3uCmwvhrEm6XD+mQq/zy4ouozJ/BWO+c7D+GbXHNuMXWVcXw
sHMWpEgeXIlqCgQ6naqkZsDnBG7l2eAIjCwQawyZTA7mY1BKIRuY6oTcW4e5MXEKWi/B0UGYUSR3
KSkh863SwRWGTn9DCuY7RycGQn39CLMvRiVM9i02v61B9bL+fNyFvqtm0/VivHGkNPYHXiORhKks
MMN4P1sDYBaiaREaZAbCutTm+IUsSKEnipDRvACw93rLnAmkjXY9QwB+n0qbZ8ZKqRQQ0a71HQPe
So4lEiy7yy/CgSw1WsEFk65hIsCI0WoyrDaIWcRcscjh7AepTlo6BZvbXkfSmnvqEprArqQ1h2xr
90OJfAWZYKB3/Xja6q61aCTuKnCY0EJalc9pV0PI1W5/6TuKfmICWcsMSH2s5Z+XD5L9Fn34SNdV
qDswC+weC5xgv9VldUvW/9kMiw0KwlASgYHREk0efw9bG7scxYckNKIRykw8VbJJXpyG2ZDQuj+Z
2RhSId9Hd6dkjde8ahxv/xU3cb2qQNpWEigYrdgYxliii7acmT2JZMSrU0B58fhRiKc82P6k9Dv4
Z0YrK3ByGUnciNTMGV8JKqGxC1IbpS8anv9o53PQsmQOx+SUVPUQgIt7+uCWNwv5IAZu3M7mTrTO
NUDEYbyufytgs1A9ALkp+CViXIQgmDC+7RJPvJ+pK+8oI5QK59VZL+fcBLbqXt2a45Ha9Ab5xDYg
/3gL/Z0BxeXBQrBL28mQSJU5bRNN5Zn95v0IA6FftX+W39ADsK0Th9QsHukbDTnIx8fH/a1BnP20
Jw4FlzbHFMTBch8yB8Q34WNsj3SLqhV25hY44hYE3TiJ0/y3+HWjEU3kYPPXBGlJZCynBRAJ111+
dF8NVWb1a90xvQrv77HBwY8US/0XZRrpveunmY2PlLsIPH4mFUvagT94pd/7Wr59v1p7PHzck7ZS
CXb8FcoRntGBEqFVd4v19cppUqAtsj0qeSZxrVPErvtzjNZezEO9wXN5kuWqsLVKZTuQnyzPVqgL
fxOFE8G0xrem8NuT5x9JsutWrIZxCxdc09D0QkGIpJCruObivgGb2OkSaKbeVZI/4z3i5lFr+A9T
CdOJgehn73FO5HEo3ABl5YDSicloYW6jPjzq4pssQfAGr+S6jduMFD5J4Nlj4zA77/upCrBV64k1
/nmY2iuXNZ0V6WmrZn3D9g/9lNIUyDIMUZcZMpaUgMvRsMqkl0c+XgyKVnS1/HX7pB3k5XKr5Sc3
kXg4TQUVv2ChmHzX5RyJSkhbadBcLcC27RPnZIS/Dv+0hyD/MHFzmjq2tO+cIFrfCdICciDWuGxh
Y+p20d2y1MjWE4kIBn7W3L277V44TAt0Tj/bOzaBQp9AdfJ1OYpUN6RB1kxSux341ZlpFiUyBV9w
ikpbN3ZT2cksCn0TEovlLQta/XFEyfjLAZ+5TmiwNR7m0TAIAhw7mG92d6WylKANYHJYFX3ZZR3k
OSUU9ZLmeDuhpj85/iQPIQp/cwQj0XMSbdD4Q8HN4dZMJ/WL09eG2tHYrK+7rL8IjA0ehzbvr0t5
zik+bDgbXYqM/R9D3Gu8DZRMTHzI0pShc8MkhBdQaALSvmVP36vukxEtXD3vRpuDR02s5bjLZbCJ
USkeKTHepZP9EAzJMxNAAKkHnC609KagCV+nLiHH69mhpTikIEXcs9ITIvLPJnbRFJAb4Zyre9di
Ye5cGN3cvF5wE9lalT1kdAV+p65spYeyAqdaPiYWoTLFDTDRbkrC1YDsbt3jSkPTg50Ir2KKJVut
aiElzrsBjpAnAUrGfYT09x/nk/szYqINPH5aULQ7ftBBtXGzR4csauVd7lIpmHNTiKovF4RfJzem
Z/YwvOjV9hEBbDGUCcqRKFXZ6TUYtWaLUeeNEg22BQraXlsX637qhQkxwpH43CZ/F4GQbKX/EXrX
qkLDRn3NnIgQnMia2Aeb8fbPlKe0Gi+W5m5lUlVO8+Rl6SDmnc2qefZxiahKtwqSuiczOWVvfDPP
TLWrwwcrHxVEYW+Trwb5jY952GZJQ8r0WpYlUVU4Xu+qogFo79SG5sBb1NKkT87NQ6J0TCAxmxxm
apP+1ZN0z3y3IjSlkPxj5yKXWBt+Ie2lnF8b5E93nS35DCcWmDw9XlZrdG7diT944GkA+0ai8dhO
lhshOUaoXkTpU0IYZq5ba/V8PD8jFqcHjWfFO+Wbfz8z7yX5CLQ57qQMcoU+WZUUvNPpfDvqa7Hn
/I8ocjqGbd44gkKGpSCbDbZ/5/AHvhR6oSPWM77+O8Kbf27O7CsNEFTPdYKpcP2rTMrkjZCjYd+L
RLOKCspXygCNdqid6rp1or4NlrUEWNcsz2pKnMGI0jnuP1gjxdt09NQ3RJ1+RjE5+zJhXv3bQQZy
rzJ5caJKrDlGkCU1u8UcGi2oFgOTIOzOSjm0butygUSym1/FyFarZxH5gaHH4RqW6FPfGR2vJF7U
vvGeOZVvvHwKhGBnejzpXFZG9tN1otyTnRXicpleCiOdmiLVUduIT80lUNEDOmEEuBgl7ioZiZ5Y
gXs1QuZdl3uRX09gBR7P7AKNRxmG2eZ4LW/b+swdpiO3sFTM6a8GXOYmlr+hAmTAvlxPjY2t58z9
ch78FIY+OLDOxRrm0gLXUadyfwAlK2IlPWbvWfukF7CLQn/rqv7+DCmIVDdMDqhKWVpfuIRVvZlr
mxYfpM5IDUFs2sHJFPvsoqNmConelFYBjXSxk/M8AIpXjOF6f9SkVDIOyHyVJkQTsAceyJBNnnUr
ZbXsYqf/NnZ8OoptD2QovCb9xMoTz0gAgYddEAl72tRvwLdgcAfF1pcQCu0oy32S/SDTg044QYEp
Rnbpr8cbfoXO33Dhk+GqXIJTHCwWkpyRRpUInDnFEXtzJGRVIL9JR48GPzd0GA2Swhkn4rnILq8i
E0ne79peAkLUs1Fl4bIMi2lm/3Krb3DPET41wx8FiKEl2p3n2KTTH0ALj2wmv5+te3gHMH6OBvAN
Ps8OIuGKj55F8gpIbsXRiSDfVyy9IAlblR6ozjTkJyPZteNniSdWpt603brfUAk7TzwvqM2JP/G7
J2lZ/DZBw+szkqW9LhjLoK+d/USW4EaoqqM4gHyAP7ZTo5wUba6YSwcIySascAPASE3sOur5hy22
VlM00c+fyAzBNBw0HMXy5ub/bLCk36/YNpKs7yq4kjVk1T3Iu+8m/VcEJGpAU4elpt6Qrpc1Rt6a
cbyc8e5KH0aW9stg/hUACEJdv2rg+OTML5e+PCUKSsa9XjeqT2TjM1ju6lTrTbfcBRTUDLAxt8FV
Gb0fIFun0CJtSv3LGFtULtn6Zi4DXoflw9eXEkUgFWSQW9YeAS+Mwrvj7rUh8CxyxJij0fvFaOSa
CXF7rjlukWg+rhAJIeXJG0us4vKQK9rzxBApvLafux2U7ybVTn2Qq8U2DCqYHqISEr+VSyPEFb/D
eOdyOwIvGsOEFyhuS/n0sBXmG7mtdAjnRBQAXQe7+otIs95ZEZUFvI/HdQ7MrUEtYYdeK04/npI7
U5W7mlEjjsqu2u6fr10GGF5uFwygJElM+AbqdohBgoSkGJpXEeIDnwUsStKOoVN8zp7hX/311eD2
BwSfs2k7yJF6WgCn20GzEU+5pZfAWScw46VGgJF6Q4Hg9sybVMjv2h0USmULA3uAfqJJA+Vx/asz
q2xv20VY9DdX2zecIc/C8dxAuqBhJXLKUMyRWtayCLsJmM73kq8Li83rJ69h+Zy5VK6x6N8rDveb
sZVkYapPq3lQ268Wk24KBqvnNycWeO0EIvLba+xCxhVAgOfZwAWBitLYf4Se3AD4Imek4iDCQLYB
Na2rH1FXhwkYS5NusLKCWrPeg54fuplwH6NigoIs/ezdnQcF1Ub05kAhu2ft6rDPTUt7nU4WouLs
3GF1koqWbk31Wz/bIJvBO1IP3/RPZ/N7EsHOjv2zuPW+JO5oOJOAvoue6/a8LDO1yUgZSrJzke8Q
tdNah8Oz+S40LBBQDb6pOpXoQKSGYyLcXIfs4BP/IxzzqGkSuXCL7ITMjTfg04hoP8dHtsLN70tf
5WUTlqSpILhvZoDRObQsWOv3uNwySqwATZH+EPX89Z9JN0QB4qDyjYGm1/3VETpdsXedjd3EllIx
RATY6bCPrVMJUvMqVkZrUxmInzUOpLvqz6VIFWQ1Wv/wyaGbIZ8RyxaFtiMPZPBfXwCTs+3AyzIw
aZ3Q0xTJSdN3oZ/nqAe/+xgtEmkln6JZSRsYt/FmDkSFrtKVy9Q12yemZO13/Q1BkORs1/FA8l0/
fI4yad70A5jVfq58/dipex400inGPf/FNU5CN6096+nCD58U0Bg71J7uvFYq0Lwao/0rDY4rC9cp
/YJZUT9G0EJVjgaqSS+ZX58oeupKIj8c/dX9BL+OdtV8CoT4r3sCEmYzcvFTJNj1530eLhQajuq+
J2rp1ef3txZGe9NDMDpWbXP4hyEBFmacW5Rdx7+vKkoMyqec76Kjut5Q+YiRwiTV8cOaj/zEWQ0Y
fw4BT+9Sx2kXf8JI/9f7ZbvIxWgZHHL2yxD84bJQEKk1Z9xngO47NulY3qqXqSfbTlCZAPIWZorQ
OqsQ+99dL+xDQNmsCnaXE+bmQokDoIGse2pwncrXT4cgT1Sv34QHCa00ssFD6yeteOKBenr0RIou
N7Mjq10KBYplhSBq5AbyZBI4Owgq1nJTVZ8ie1ndbfm2DeE8wpq9yptoY6AFRk5C/7VMN6eFBuP6
+1iSbV6DlTUd/bLn4Qxr0bxLkJf1gCctga02lz42SbhFjx1+Vl9M8YS60F/Teo3vFLlaJgGSj6hc
2el2kTvkEsGazfNWU3CYiMjlW2LGURI7aQeiQWDE6Nc+UGVFh0TEwI5MGLxUKUNeC7Tnb3v6kaId
fwDswUROZdxzafff7lOtlPyJN1ZHuXDf8m7X2jPYfByA64xzo+NC4lnZq3vh3a8nqDNebucz4H4b
xMOf2L0rm+WQzsSzcCLHbG/W9JGa0sA91morGlkPB41d66SeXeUFpFK1wQ68so/Vw53aJBYlUGZY
4Sc/yaDdV6UkiXDfgvPqauxpVK0xIsuKG81g/9qadL7ONW3cXNn6d7Angc/vkdY1AntoG4hKmGLF
DrHsoPjOCgpl3OybSZ3PTeqJyt+yDVjS5rtjIEfrbvRh03k36RZRPwWQrF/3ENt1xjzcBkPRCeZB
Jz910C4FWu0yTMGD9PFzD4DiSislZ2s4VTyFell/19BLeEO5zJyjr6F+PUgVHm4iq0NhNQRrRSzS
Fxt1Na0Pome9ewSjfqHnREPxIBTXwUE4AipG2qfvmGar0nq2WyY//DdilLnssyYsojF9wP5eLGYa
kFOGiH/rd+pDFKjZRQpGCya+gDTHPR/ddxFSJ8UA80GIV2T3x9V2MUIeGF+u4tX599yLcyk3Wk+H
RSubIhbZLKYG6stcHTgmXP2de48QgTDUWxgeYmeW0hIhzhIUHkaEhnzB8MZ+QQhaGP4+tH4PsInj
fhAtshEU6Mk7yxdBqUbCGPctdWUfAmMHjHBlBGS9XE3pmpEN2q+xRb9KnnQLUFCV4mXCQWX6adnY
ORIDcfNLXTqWCApvwFUgKfYqBiXAp0CY1D6p5N+vXZhMmfDSMSd4Sblmv8nWh+TvGnd4bqOB0VwX
Uvi4bIl6ugfSU6rvGrk8N4vim9KBRogzCq9hmag13OSn84yZMN1OVqR2GchW+JiDBhxyiH/yw1cL
6USuH6Utb4sSzBuXtE8I1Nsv3tCxxLQcerOWUu7MT5NlnfiKE/XtonVTKlKWeqaxQqxGuToKMhex
5lRlOIlNi5EQ/5/NmstnvuDuRIuVxEAzi3XQ0RR1bnIsRSn0CFGfk1nvHKd6GHPZ85hW5/+4O/wP
1FBFAm9pq6vkl0tbMoB20sjHXF7wIJztHXlj0CjUwpZTd7P9xxu41h/m76qNNP5KSslPEp9plf9U
xEZ6I9ZvIDeEpDjMl9F1nikAyMFliqhUy0VHgQd7SNezdiCtmDZqH4dIK6/k8zApezAYeFwErBb7
upuq/W028oPflyL+/POH1jMgh81IjoZtLJh7nBxaau7+w2lv4ZmIc7/ZfQh/X0r1vWODHrNV4QS6
o81wgTdr0U1BGpB859+XC3go+sfxvre7SQTtyOTcPSUazY/P+CzwtPOMXgRF77vDhKdnzicct/vD
9MuqWVp2uFWsnzDCGeWQCHvQZr6EeWHsuG8iDHkfW0V6vYBbGH1ImBfvUvJXI9jpNc3x5j/+XZR8
v0ZR5odZXPG9nGYJXMU2HSsw7EoNgUmiQ5Tim/GtETwtax6SDHbo13X4lX24XfGFMLK1G/+/Di1G
iEVF8PC5oqkY1g2Y2rVNAxrlLdwSXgusi+a73bG1o+G+4o4i5DGSpyE3KEhddYbRTcQ8XbSt3rsV
fMGaLsBy1VXYSXfLunIDrsxAFVeyaB8zO5Sr8Uz2+NVk0nG/HJfw0Rnx+OOTM0VXqsiUU0Q/h6cs
yM8dqzEK+tPTEcyFtzMgQ8ExZoiX3xlFtDuw7mhL1iMz2snc4gTX4cOxGilZuqFzOgH2PYmiFWGl
qfdd5tW+WJfD843Iz+4lGGsd0sb0ycWvhCDkeD+RokM8ybniqJmEt03Tz9bMlGCf85W7to8v24Ef
S6T5wpbej7K0f6sR7H8mwhN4bmIX1pLnXKRPB7SQKDXBq1RVFUL+8sbkrKY6T16TSnzJjhH/fZqB
C/kmZXJJTnLuMpSro3k3A35L+Ak1sVfkq2zqKRI0mWY74HnUF8mx/T/PqAT+TjFtBE6NPN2QFqWd
uePZ++l7l7+tdYrwbDQ04A8u/Q/7Y57li4lG9JxXXOmxctCDeFoFHIDlARQ6gKqzB+1inFKl+6Tc
2W4ZhObZ9wHhTYTEFTPjnK5cTrfWaAIPmphMb/b5sBsUNUNitnhmqa/9pkiklLbGl8LWx6qRNMcC
mfPQdAlFcz0w9Utl4rwwD5zBM1qvN6F5jwWe/uDvk1wEdrSJVWpStErhbjS77A/xRa3C7Vw9AcYH
8UvMOdI+HCmmBsLPdygH/Q1j3Rp0YvHpx8iYG3unPh43ZQtGE06NdUHCq+JNXDxU6lvKuLBOfUWh
w68WQDSpv3/y8NNkxupXORIQVsO4XPTgqrqY0l6rYTllkVqj947Do2g3DurdAKuOsxWlTEdFx8r4
J8mt7EqNZJqrg+UiLhKVRlb3IqpZOMuswH/Ax9oV2YirNIgCP8zDTeh0RBfS1iRTLEMYsAEyHDrA
ooLeBhijR+hsL03iCpNcVG+2kmxOt9iLDZ7Rh0p/oZ2r0lAXP6e4tO8jq1n5/GflVSOsQjZI3/4P
e9IGmEvNggTC5kMulPgdzkNz/RV33dELWHZtMANUxbpsw454LW+rZPTpvbJ1a7Aim+S8oCwFtDXL
LHcictBkLiuvRTSqI0b6xNUTiVnH2Rbie7ibQY9aKOQcLr+jHIzxDRNJuINGFcNmwcEGmj1F0nY4
lIBt1Gjpdj7HtM5cVNuz5KXnttKYerVMbGZNz478VU+99dEagBg8ZX7no6kMu2430oCci9Al9il9
giH5WwBNHLrNHNrqnKmezFFaOnqN9UwvqdC4Tcxle6digaMxMtExV6TvExdeHMt0ui8icxmT1yU0
XvkY7Q4ooSXz8/kmm4FF7cVcOCbz9VKfXwasv7bbgO175wzpz4bjIZd5KY/rbCN6YD4r9vTSLNIH
PSXwziKjGbQmG/dTYcR0sFKip4RTIcqZhpxV+/DJGS+NJgkVE5ac9WG3fArGWQDg7hA2Sbrl8x0O
hFlGv1Gw0IdYBi9UlbazF1JSOSrJCiYa+QSeDGICCj0exFtnvVtoacPU8kJCF2Rx/2hcZ5IK9tmm
h3Akg/+qx21qNP6csYiqA0Fkzz8E4JGOFryC1EjXMhiAktcFLHWZyBXHgos6JysU50W9IShc8DQe
iEh7l8o8cO5sXNnCbsZkIwy6/F3cTWmIpORN9UVarTJBYGiH8K1+5TpHprn8GDUcfkeQSB5ijLjY
oCm+buW7sI4tAYdfx1IpYh+9kZxKJEfsVh8zbwkoiQINRB1bKeKcpbZ8sIrQ4P0BG+wzhFVZUgiv
ssPJd7brkorvxsbNy8IBSbZMJqwfWXNalR9hG8UIU+YIXcCZ871XzUErH+7R6ZDWYtrjuhfHtKdw
RaHTgi/7nq/Agn8kQJBflpPK0tZtWE+H+UZyhCD/sxExP5BwlZGMcDNfp3WI0IBQ4702gTYz6jR4
OMak2Myt1JAmdnGnQXnFNHrI6U76NTsWcjmUSDnl8GFfGpdi/OfUoSXS4nknZPEz3xYZfvQ54fVM
2NoCPhNJwuo+uUtoqtsOykSgK8EVYmDuLPygRwm5+8q/0zRaLvYFXbur1caoZ4NpJydDPBfHgob7
P1p2GE7N4MLY2B1fBzUVOyt4000VRCIR1+4WGeWNXuA2jVfBPH2wAogv4FBjibc00na1zsM/o9k0
Vthk6mNVqn0FxxfqjHsjEnQTMxCMVpFNtswBb7VhF5X6s5FxIazuk78kECLvaoyld6bvLdNVS37p
biMtJif9Kt9hKSrlAFGslpYbTLBBaYA17H3lID+oG0enRdhk95Fr0rv7DLCDx5/4fR41g+zS3BBE
zoT+46gmEz87SUocyXD8xNsvrZSFpu8H8ykkJkHfMGR8cGMtNRUWhXF8ZMF6MJQPgd5qoNvai8zd
N57SLqt2IQn58DAYXjuJJbquqVsh7JsHxN1Rgi/jnGJzR55TlxiCoC+4+LCqxNa0HCcMFvkK/lQK
7q2pF70/wXqhA1IRdxu6Y4nSHUudUqpTYntqnVZT0dPTgbccoPiZ/FYmqZ4MUSr1IbEmpyo6QKGw
PjqmA8nMi0DeBSaCC/I3PML0/AA0odMLuH8oSOtInTiB0HJhJQYs5/RhgP+BxYWUJ8ZNLMbpI0lF
U5WYFFeA3hY13ccAwsFCD6Xo426fKSueqKV8/wpv94Ou+02UpwMX5H30d2FALJd5MT5IOO9AiVKW
/kP0TgAxDqfrhhvY47EZBkj0H3VPbWv26uOcO8s84Cc2MU7IwD5r1NHl5oJkgx3OTYkVqZYHTDkl
XqYZDBzArgwsKChQYlytTq20jVcHQIpX8WsgeC632yvNUzbioERVzANEtc6c+6hT1uHP4J3iS1nD
tM+MBj7Z/vvwRuJVGJfkSNUYVNMQbBrJSpPivGGcdYiHpIJTlftUqVfvoPlRJJi3EjAfeB5WjO9t
3VEf+uLGqO0dtCLqbKkShF6QVBI5GBZ2V67T99Pm4l5jI7qCCdl4dZsiwDb1g9YkU+sTnru9AVOw
Jr464moZaxqNY0rXsI5Jkn/oxzXpLAlIdkfmBrCjJgc/BbwClREL7SF9YVTq/L1lf2DvT0SqEVTe
aXUkwCsl1sRx3ecJGIDSwxz8ci+9bj14fW1uyJK+g0MfVRb02E3o7QcyMgdSk7M0ghgz4CU0/UC8
uoDYIyK0h109JseZNCTlTHUb8NIG+nPfOqVuMqFYEvplrNfLhCBCtkMmWlyw5hBvcITnVy3S729U
rH5PdZuI5VpwwibhC1hhInmg1ynGAuVef3kJl//s6rTBjgBEcs3oSTIPHPxKX7ldMevXEWq4H5Bb
C1D5VxfBao9KVP/JzqQkgFuV0fB2M8J0O6D5egKvfbFqbHHSoic77wg4+mxjN6AQNAYmY2GCfG0K
YkzgmP9GI6moatYR/mhNSIFXJvAXmEkWuI83GHwvH4OAfL+NXcbFuJXllBU+F9pLp73UgfnmvSgi
mNoYEw88c1xttSTl+fHeyKoc1yrukh4k+zK09Iasmhi7SlE0yYaaZzivft72X698K3eYBT6aQIf6
o/Wn9SUHO9WlSy5HJZUmZRJkUgQfw+FoDq0zDm1RhY5+/olmjMyS5xXJ2fX3jrYmZXKfYfUf5XDq
43ZgqffKbF0943qc+jWAmCaYRJJO5uwi1/tILZJP7/s+mVNyNePR/lhY8HjGqM8WXsQE37XNBc6W
Tf9XMM5urFpF+5KnVfVZk6vedPdeGcsyou/E3z/2wZXPr4cva0Yt45Dv41HY0pUyBwPAlPaRltES
j431mycXcXY9emz7wZFUFM6Mw+N06syW3R6/BQN8CpAUBefteTZ5swjqlh4cYUZqsV0frm2fl2Jm
OSgOZflc7KxL42i3xapr07AnHotPLSHrn8FcLrnxIgp5a+MmFxHe3q8VwXyNH/rMkPsU0PihAZCr
OPJkKPcvs61guI5IhNpoQtfwdzF7jQF3U3nEEslGs/2wMYUDqG728uHZWMJL9Mc60Jgy52FVV3CH
KyieDrFQp1ZsWvWQZdgsmj7qIsqwb9gVjCXqZKdG4jKitGavXpEZJfkjtwl3syngw59xZEzeRidx
aBz1co9XcviKbJMS7hQIM0meyO/sLr93zMWyLhcTkjwH9g2oH+1Djzx/ICGCGxt9GWDXfrPcSYar
m2Zp1T2bojCo0W+19ny3Ev68eY+SClf7QE6zQs9mpgf071GAE/IHoO5ykMf2eJwb/xAOBX2KBnJr
JnrJNeO0s8O1gvMbBtO+dxGuDMThvamcOGoJdcjNo4+4PSTTd4NQ8BOvUfT3E22whT4HSNe57CBU
87q5unhDpMsxZ6+WRt6P3OMm0i+hNQV1FnRhWPRcZIy9wRiphfXIlSmMFAdhumg5iTEOWWsbjwe6
tEby6e2nkddRvYIPvY+Mrx2dUv5MBiP7V6CS1FZtOhQXM4r3lA62WJF/cXRMIoQ2DsaT88XNFtWt
TzD7vORm29bGxPFGWbNAs7gM3w1YMfpO4ld/x35Y1XNSzTjR4e+mg48tXdoOBGSrEBKnusYtd4uF
qscgGI8qiaPK1lkHM0EHqoUqeI4UMIi2+KmrjalQSoE6Xd+wxJa+tyocIzoGtLCHKTIBGGsWm2J3
3RNTQQF6+dGOaGOytgMJQ/uanBgVBKsL1LQ65l2U6TTsVz0TpVdFHXIHrFZcWY8WEmUaXH8xnW1g
K3qqJqeazvDvYHlYavqHrx+0oSwKi+r9NC6/4B7u71CcLhX2A3NhF63t7cFnlnG8l+Hg6LkAl0sn
dRx/09AhtkxbShi+WkqZYzv79CsXYQRefL2zN03wk7RgID6ixkAAKEH4GdnfjWIu6UH0JkC/crOy
3RISpMBeyDOzKOcLqJ9/UL6JhpXsjpPqWfDG8aiPSTylhcS0DqOyxCoYOkY8yynbChy7GS0FwZyG
L+AQmXJWhwP9tQ5XoLGefYMQiuSaxZRAj2BnzNDwhWWqEMYqZERU2hfPsrOoJUMNAfIEVDm1SA4I
NW7uPNITLr8AzXiX4tElDUK6dI20DhoIXgV8/DxtjWr+q0ANq8PLSpnaSi1eIslAyQYIAnjbjXB0
DCZP0gX0K1t0mF36kli6PAMJwoivnTMpCSjO7cwWmq/Q4zdAj7p89zM7HMwxQD9hUZrutIhs3O3z
l8KBsxuzpcqlPXdLII1pUcanKjf7m02eg7T3MYnETlJ45KfBTqQ7lAvi3AnYb+LOS+qUIvCtiu7m
DqXv+LRzUjTAuOj4hIoJm/IWfqcf3BoMIFPdTST//e5hqiiaGCq82ZtYmMcn0sEouNmFnM2fH23Y
OKYjg+zEilfZ8gQPY48jP5HPMZINLZ05VVSDwQOq0+LYo0bZGKfd9vmSPe9iomz9p15jHBhU5wvW
idu3ae4Ja3OaziwKyXBZxI9T/LKDUiC3KllzqJRUgP/GwchR+56FbiAMpW2VkyWbKVo33tGCXooG
fY1pMTS80h/4JK3e3lbbhA59ivWTWNKAmqiN4BuOCzIi56eDli1hHAhLuYBNIibYYAWrhcHptUEw
dYSJp+falu9sbm1FIJN/dJHfWnOx8moWCDS6zD2NuxYftehpWy8ORhIl0TC6w6guiu/KJ2wTo/HD
M2DQlsc+fwoXt4mdkFQXsCaUUSKqM/nH6174LuiMUSXwRkOkOca9YzcBEJph/sdbtR7vsfoGUR1V
55CAhfE5DSPLm9srL8Y5hhl2eCmOSGVrr7v5WagzUF0ccZOTn4/erSf0CYSuIGzjowHo3dpVcMVc
w0SWkuLZcj5I8dEgIxQlfPcNR4UvO71SHcvzJSJZsbuSAyn3LHhfCR2iY03NWYRnOyyK8WuRIHza
eLgRRkxMTf0rTxQ77oiEjp0FX+JFdV6Cc3W/dcG1xAmUITL//Gr535oR/7pAJWYYGhO9vK6bYzfk
e9NcsLY9ueQO9w/prVbFkrDzN2qIbpDI0evI4WsXDCeqz3SGudMu/UPNPyfaUHAHuoXIaSXS3fFL
gLPJEVwTHInePeQ5FgQkcnV18fJkVKvHQpo+0lnNZpDOrrFEHLrhI4X94olkIa80v7YWiqiXuNT6
gudRK3b5dhX+cOFU9tgKwKUJqWbvfSHWfV1nN1JdoDK80aoFfOqTMZHWTeJ4llc9Un1kfFLLIm99
BO0IMlrmXyLTUAaNGZzaOnhgXOY8FYivY8De5MFCbAybY+eIw38NOsrERKGlHC5wN4/OfvZSB02d
opIlwXlO/x99RHJq2ia4GMK4W7YJNpywawRGUiWLkA1XW5+UdCZFr51S0kpUDEi1LSSumpbdqYg/
4RS3WOrjkseDmiHrdfdouKz0fFiSQm6UF9VRVQh8+8RNDS0vNVV/c/WFsscC4ll2eLAkfo00FCSz
bN4Fd6blIeS5kwGcgBVP77VOfIrquqAceNmoiKSxDHZJ3L9mYAPjeXHKoKneOK0mmaWhf5G4SQmZ
BVXUpc/mfRe47z4ZYpaSHGmzfdUQeIPb05pkmvZqVFeb55V4nlCnYMXnX3EJNQSyNKDDk2w5NEum
fHo22lbCBEYkMQ8+DoJTg9Pt3DqI6rLIDYbOTOpivoDXHxHi4/+0KyN6ypmI64L4Y0CHEeQB+Cl4
4sIJ6vS8QRPwNzTCziSMntDhBO4xxXub/xmXQxsWclDca+gdFMA8Bt/uKRKxnZ6FYxE045KEMQHU
3cp5tqHmrLe8drRiehhQsQoTVP3gYnIRjbT3zoONuNXV12XoFgFqjrxU/sVWYSbclzm5SyCv5p9E
LPIXY2CuQzyZqeTTAOEiYouN07JfSV5StTU48iiDt5U2etPTSGQVCNfIDu9qkP5LiejAYxjzcrQ+
lWWvfuWE91mK/+30ICG/P+8PNoTD7oafbdw4kh4s0ebS66xsH3zvelvcEgRptZUBhVn5i/2be2J5
OVYpHKGaxpw+PlTsdTM73mO2RMPeN+DMGgqdYfT4iLn4dETRzVmv8GA5z5PRpX+wzXtXfLmAbXYZ
RaInaqXGJPdKGGGzTYnX850DRjdDbBYLhhg7JLBoEbMNubKEyGtcthU3LUFFvaFAJJpgBfcKKOJz
1/jc9XPM8AiyGFqpvz0uqLZiDab6wkHAF/qR5fqV8i+z5ETVj8FYkoow+oQbMjiGp+YD7tnCvvI7
2miflGdkBZxItvzPHQATWZXTB7O0H7qjPHbP8a4ytZbDMyUl0BeK5O7LMxkYUV0++HK9SQoNmm25
s9FUDh9HwCvHxhg1kAdEcgXNcY1YNdqy5gGqUgRUES/0IvF/T9VluYUifU01gxEV+Ohci0FN7UwJ
AMSkoF44z1+hMS6dxlzvJBL3BYK792UW7GAH9iYoxawzgNQyaDnqVNLIeEgPpEbNcK9fSRp+Xmnz
26OmRjtgzxy8SEoI3vSy7IU9zMsIF0QAD1vhFWwKacGHNS+9CGtMj0ixzHdUJA6B1E1O4pTkS4Or
yW4B/Hk/kMO3uLrJJBatMMAP7sWbMwiTNGuKCMIDOpFYi4d0iNAX0KWR64JmyIqu8PRDosVKWL1H
1uTuqE/6gXpNMDR3GZ9A5EJEOQQZ+atsIkrp2+xpsoJYUAGGfg6maGkmppXnECyVXze/nuoxJ9iu
LCeM4/vYiyCBO+x3hxvMT6VCBUxwox0NnDeAlaS4ycRt7UfPzejtbfbM6q9p7xlhSyKBtxMkR+Mi
+MNspK4/lDTGGiLwIQ0ZC3ttf6FKuKFsWwsnBr/MvQeJX0Yk/zMwUHoFFtVS9DUR10O6J8fd7U11
Ni9IsQgFinDVjwbBRmo/QHYLsx3gWTSldZxas7Wq6+t9JMoTtCnxeuhkDJ22pg7P2RDSw1VW9wBu
jrS3KpguAzzYde3Nrpl8yEcpIEGNAY0Ur4ZyJHCYOVXaaHM/WOfafPaMm3OfZz1jikftr53Omh+U
iSVAnbvBJXScTGYlZEe0MLk7XD49L35njP/Uple0f3K3iR0+71tPiG03/LdbPT6koK5viiwQyJdq
RFb3hCZrFnHjHzgmBJbB3vuciVa0nizhorWEFBdl9bFn9uArOAU7qI1IRt7JcEQvRFkeQxHRc2BF
lptBo67NlSGuyVDE2IC4yeNWlMfqJqgCJ07y2pG7e/29e2s8lXfwne0tUOqsUGnq+qdPlqmwGeEv
/DD2zlYgUQ6CkpA22A+zadMMgKiBAloJgUCn1MYWP3QIS4QD09HZtCBl18P+ubHnQPVGLcAa0aAb
JxqhJ7uP7gwjq6yi1jmNEtMQhcJjmtEewHQnJLyCYR7ddCJbArtokU9tXZUiR3vSDj+6DifAqgTH
quM+XkM6sxP4QKDlwcTGSLP8dHRo60Nijt0FpAVzTRxdMdh2t+BUjqDbNHJh+oLaoz9y91yEIA+/
YYu3o3tOMeIC+C6cQ0+V/ItOLznPsx5bnmLCZu7zP6ffOPGI2U32B8m+ct3Eh95pKa3JAqm9YvpG
fD4ObCbiuAarpb6DdTvSNem/jDCFAt+ChJQTLO98iWpgl8RR++ZD/OEVL74LgccQH8Tj2K4WON70
bANKcXQrvuHq+WDArjACIVvKx+6Sbp0rOpByC52hK2ixQNZOq8HGT9FMaAiMq3xSSJVWO0ZRPiAk
zhobtgVYLqMz3eI/iqC0FJMz0f6Etk82e6Tf0VucK3JCBiZaYxrWrKJZ6PFB/YMcFVZsxobZ81al
FWXn8WvUVDeLeAqLkBmm6b6J4P82y2HBMQQeo6uzhMosh7Bpnj1pbLU7vKa+/1fUpzglhs0HKbpz
hYp+YYOKRj6n+loCgdNan77XlN055m6b+7NZCwp3Vi2p1U75H4LCVxlARk0UUhlj3QdPPN7fH0oV
1yU5Ri3SJPl+v8d/w4rCpx9p2I9z/fIuCneH461I/WeCDx4xRxsO+XJz1LuhjupPlNDR3kZSZEKQ
ssBOQ7bxLFgLesKKaHeNWBnV9cpUHnNc8BHNXg0rBVKgo7lKDwL9MSJb/sRimF7pcOnHFKXKOliZ
UP6SUz9xEu9EgES1I2T23ZDoNF73sbWLhBXRGjrrehpvJ0RA2Z86TXzNIHNgiWe2Q13jdzNeYQUy
9CghTmy3/FjkZG6hi3heLzphUVE1BQnJ7aQLhXrb/sLLR4218ah8Debpz9kmSupDeF2NvZrR6d6q
4oFIDgopo7RuXMW2AxKSmWyAUzFm2WbzyG4l0U1iosFPf9bhBw6lg+EtiClz4Vc+dnbCL45W+nQl
y1TPCxQLnsJcfp7B3It4sCLyc+gorc2k78NmYMrIdt9ei2zc+glcYeinG62vr5eIV4mnguMTRDuJ
a59u1x16RcSkf42GNh/rIMyQzTLqiWIVoisVzd/wW1NRWdEegGFlbA6o5mC1N5THwPN1SZhxpTPK
OU8L00UZVdeYI2hSkOeQbhgbqm0EAZk8z6DnVrhtfktxkgQ32Imf3zDMDfFcOxiO6/G0tbMsmsoi
NTnM3m+IpNjurdI0rtazSF698vvTJsy4R3vzrJteQQjq/H8ToxGsgSZwf/W+bqZioHkHrYLL/RsT
BNytTCtAxSkpVlb7V6Ab4tnCZFMKp4Ifhx/h4HxBOtNXU+MMn8rwmYqMWwrVD8afQnWkw3tQdlG1
JJjYyzVT5n/sUm8nSj8X7+CcmkVBk8/IrbOMokvps81jk8UjyPX3lLZCWolPg4tiRBKBv6uAl7Xj
IjaFdWZoKcAitTVEVfqnUXcLSb0RoFzwz1hPXXWQ9HMAglWpq9nzHY+GLqTgmL5F5QBdVIidKHYF
wbp8QBCalk4WT37415CsFtfMa2tyOa/x2eIQl3wmBL+zSoEPRYpK7NbmMSZnfSrEcTa2ZwsT9LBk
p2XTtJ/VMJUQr5FPJzhc95ib+zfp3ERJanA3h125CqvmNGZ1gIGnqcY1w6f1R3LaA5Gs8vdQfa43
Zlmq/0WOzKNgRtvUupV6ihytdujjcF/D7vGmp2F9ZZ/Kk/f9DH3ul3fo/wWHT5pCVASGrG3oiZLk
Zh/ZHFnGj3hy05QRgMcePxAOBk59/J+OaKQ33LYaDSLRX5OoJzV4BlivVmfIm924Zm+XKfHx7k1V
4cK8mmS73ViijFrdk4u/wNeXl23v0L6S3cPFdwzsNsrAXG9YaLoEYwFKJb6kdF6DAZ+cBUPt1sQv
QvaQDllinuwOCB4B7PoaG8lhRhfxRUtfwZxIO7KXInTMQ036Q1k937npQ1kNFGvDn7eYrkCoswoJ
9NdIjQf2ZJJ54XIG3ujvfZ85LTsKT2aQwxXzy8Vz0eFmxjFLSkUy/7jnK9vhfhb8j8jXbR1PMDCh
sgsU2QyGXWaunbCy/ZzH1lz+MRszZConqKG4GxAfe1oZcdqXgJqUUo0cR/qxLvgE426ZjCw/KH0Z
crHMM+ZAa4yPut6VgmAODwc5h8j4i3glY21gxWOGhllNELnztqAup6S6YoGQu5mosZ1BjeWFqqde
W2cDyzm6eHlo/kX9MDDtKnzZWrMusQjPS0VA0RpqOR1mR+pdq9v0xZSPeXmj4THcMtpaDHd69lah
ae9rzEEoe/pyJaCWeFOOITyNyLB/w+jCUuFBT9WLMeLc3BJUUNqayLLdjUePfyPmN0CuU+PPNfFb
9zJA+bn/0BqCHSAA535OY6/+P8njQZKa0jEnus1Wdwjg+8tAGac7Lbvgzqn9KQXwv5nt7Ua0c3f4
fwpYmzfRE194g3qRMdTYItS8dHihixqMJhxXMcevWx6Hfwn8rzVy4JDqYFT/JhG9DHnHNKZVbzID
CERLmJP55n7ePZ1T0LNL4gDqKW+/aKFBLj93KzSIA3z9YOSEPdTkj2k5FeVQjXVTGj9kN4Ijr2QT
i4gAM9uTC+AdYdMrD64GXsoFWn1sgu9O//ENR94SgvAZ6gAY/9LJ/LljecCvlUetrsxNBHON6Orp
Z08L7M+vuSL0p8rgDi11GG6+8KGcx0g/GbTJiUZuV5l3fJR31z6572Db5XFA9y3z0rPJc0oW1BX8
IEVSBHu4+/zM6VfDo6o0QAZuPQpJryP2gXGwX5aS+yU26qiBFY4LcSLc9lbJPjUIExpnV2o3qgq4
uovsJBoAD6+EKGLPFkeEGq/JbJOnW74LRF1/tDkqQtpu7bSE9/lkAYAY3qX3lJJm3vmOFjORzlNZ
rjr1l+adTqoYblC5XzP4LbxTH2VH8Z6hYuKTqttsVYFakyj3X+TBJsHjF72YInp3a9Z5bgqWaFLk
gWDDhp0GVahe7OMKbCUzQlipRh3E6Ke++fykgYkZtsvzmATYFwUHhWk/4N6tcQCd6DCSAi6wsrt2
auW4UsT6YVS/JEi0hz7mlMiioj2zjCG6ZTB03PuVS3ggn6PMm6aHyki6mfaqGLa+SHwCWTBtzJ2V
h/hEzpMIRGtsmS9g66riDl3ilOtCwI2jxAXz+iPzQWRPWV/AcRlrLac9R3MqJTqV/spqFlibPhBw
IgZhLSmZjBGVqd682te0+0vbVECstZ9VS5Wva/XZrHvMf1dxPO8+YJivGeXmVk+FLeiztgcKWzW3
oL0wiCl6pZ/LNTwnqmxa/ZitSgklKzBtYQaF2Gn1yzxOiCLQvrl6ZBdvBk0TJadNV5A7hEgpanPT
5uEh6HFZVzAyuEUZ0pcOcu7tBkO0uwtpV2t67bqSBcwnDt1LQq3m6z38fcx8ohB5NzHJ4mrG/zIp
bfUXCLobd7ok8lzO4GZd1Nn20fKon+IogktZyve85Z+ouszqeJLjfNmbrAcYvbcHS4PMB5ZNqknK
VQAv05lS6qmUrDuzVXyIwLHeJtrEDrkOnGUdl7XCjbOZ4s+FjliaKHJLwT60/vfdhFhOeLwWMp2+
Ms0WfiQ4S+jQbRnk6cSFVoljsgMbvVhizUDX0laWOazUnRJPAxPMIXysQptpr+MfgPzpYnYygOey
ru7n9oF4v6A5kNbpRN50ytfGecwMYSaoHjLjlX0vk7asAkrPiBXJ0JyVyl1c7at5KqCCDbk4sMAN
VElf6oi+5VVfYvAXW+5vDmbR8+JsYdSXIhr+44kVyO1lcogT8WOgu7grOU5jJVeFGBrS2ifTk72O
9+1qNn7aZiqzaVppc6Xy23/bvMRjUPLspeW+poMVOXzoX4UngbMaB+bN9fAnjLzmwkMmm2pb2ouj
s39hlZAAglHNL8doP1tbjuEz/zepjkJfzHR0zvAgcO4xhm9sKZbuyPtnVi9VCI8F8ZaqYu/E38FW
iW68o8GP1fYRcEUq51A7wHdxgFyg+26hwKvNWtEVIJCWRREkvrKPz3JGDwDnt2Eaug5+wzgoRzqe
A7zf0u85btxlu6Z1ZcdF0LC7qWropTxh/MMTvknBNtUz9R5Umz8+uueyADE8x/JgTKocx73G/cn0
Y2T8hdysllRCnpuvCaKNMHvCn6UbwgeWdG9H1tBp60wWmbSxZlJL7SQ9shc2d7JtDCDFPjI0mvxv
1bDQ5qmXAfVHYWDwVjETKqnV7ZR3WuJUf+Oa5YAddFKuEckgAxOkStU8dptf/0zcoxi65Tnp+3N5
iF8wIGi1hSUXX1g1cNTLCVUYR3gbOANqgsXcfeh4hvWt4RAC1W1ZolLYiSVxy/BPBbkfsWgscAGZ
+Pk5YyzBq+8tNVas8H+7iMd8rtf4AdY9NOefqnh/V6NPYY74wXkhCiwk5RR+If5+ZkEERQI27U76
gXyBkJMz0IeDIozTWWiTthtLmPNYMff9MkrlmAAZPtxoWV5zWd81MKICcCcLbxJOby0V6EuN+5bz
ZAOvdnBzCnSduaPkwSAIz6OEx6wWAVHRKq8lITsIIThVORCFXdMvLU/WMhN2FCeq7+p6Xv97cQPt
rmgMG0g6E929NAh8uS+c31JO6maJVza+T7oBE8x5QIgGUnNbqZuc7Lj2ZbcQaqL4v1uPwCKVD6fm
QG22Z8gU11YPByTnVS4Ge30mnE3LBJ8zdBRvjCp6q4DymRpikCd6BDyeHuSVbPSnIcaf8HKs4NPu
ec1v6AuCsM3qkN1Y2k78gR5YU12ot6+J7iCUlMoK/NRiu0NYfxzE37QAconBYuxIoAzzBaWU/ob9
UMFu2THzvYg7jwnDj01IRF1Ivzn6fJC9aR1xPmDAQBlYfpuBnJlUz0DGbbTLVzYLNZjZro8jzqGy
5oI+vYMqRkfjupvLEnnvjijJ3L85dVWiACr0KtnxtjR68YIIMR93JQi4eZy8xAl46bLPw4ZKaDHv
foWBx9vWMd+I5wwVVkjL800znWGdiDY3nrA1+2WKx9qplkCUMu/7Kx1sJHQewaijhRpmFLSuDB9p
e78dVSSs144IAy0tUn94pBolDyRSEsFniN6aw0u/f+I2VlYI5FI/52wHrp0r+9ms8Q7O6wUyWWMy
9XQPk8vcL6KCSuZOCA9ph3ty5GPhmeFdDldYuobDhHkhM7wkmGjb9HKd1+hNr/PSXI1Z6Ld8l2Rx
oVviq/GeI/4z9YGhZZHpUleSfk7uBYWOpDADLjMMwgBzkZdVu0nm66komPUgDr/QjlPj6zUqEHBa
gcekrcJJ7cknwlHZJgXzkGVI/J/0uvV53IDEzrx6+fDM4VXjxJnQ7uPJiXNzsFuPcm6jpH7+Ndrd
/0+ycArcHHGVKMZl6VGA/eqZbQVH1xI+463pBl/A46w+gf0xBDfqy32BMeeuIeW+OUXVRuvlYe4E
rInqhDTedziJwKOimKmDM3IohYZuy5qTeCAkMbsy5IxW4UU22n4YD6QjCANyzwS0t9cACXLnRL1t
gTSlAXkIdDjHUNeqUC6Q5OjZEiB++VIf6gP/xVuhiHb6F2hFira3zee5h4rPRY+W+eWpkTcFCStT
IicGsSR19xmfa5KbsmUgIcykWf+b8e0m5duRBxj57BcYsyVj5XnOrhBTfM1QR0pE6t4SH9oQfJkA
mb9AUrXvR6oc5VUaYMmmgxHLceyX8004LZPRhqWc0R0hlvPrqCXvr3rw0+tqLZo+yy40Q/yjJ0VV
94J42ziN7oJ4YYBP9+O5Aa5K2VTwRq7GBS/zjI+XDO3ODEr6wXM7Q26ex1OEGe8oflfDTzoi4EQz
Io2uOcFDjdfU4H1qqjeJGeaxdkSurljzMHhQ3/9pnbL+v1hTdnhSrIqICd75z+INyPITpTwI9bgG
3TedQYYHderuHo8JGgerHWiLEf1qE+sXNMYLKHSCqwYhT5liQpH+M61GmSexj2Aa/6CUMXjYXRdZ
Wc/WCwJ8d/6SDSqac4OSEP53jIn6v6ojjKbpTzZRY6QZmV3IfjPRTvzeutiLxzUtbhfXe1sqM46S
9HwqaS/lxyIMHmwEUx0pgAvPtd7tPKSS/IWrCKu/VVHjv1nl0VLnUqqVSlqtoBdf9SgPWzdATx70
QuHkr1EL8M+Rv8Fhrc8NW1MPeFeJx/46fQi61RY5vxP0aDCnjGhDD573r39elza7fULSfZm5yAw/
2AbGOm7SJjxD2bXkuzcGQ7z4ovIlpmaU1SX8t0a7hl2B9TrctDnsD058QumpmG50RRMKUmn/etLI
aTvgvmHAoj0Gv+zgbKfVjfQTf/Njpvaf/1xr1klh320D/asZeJymSvhJy25zqgxHZkDjEkLCrgCY
OlNEiMQdFuSzEq1rz5tIlmpgUrRr0JEgWvc908PcFtTKi+bslnm3pzRk7UZP9kMOd5KytdSuWxj1
6U0nZWbjlGJZfJBkkUDFq4hocNDdgOwmVoxOk/32cRnRyAdD22bcc8LGM9gRUcZ1OcJT2hNOFm3k
cTrNkgEatTcLIvHIahz3hiLoGcFT9+tqfzBoC7gWuJmYUtD8xQ25YjoVwdPo/eDaBulgIc2t/X1I
Ednn6G2C+KmQucBJOMln6XpKTDghhZN8y5Jd639UyF/++8pKTjPAnXhDiPFhibVvSQVs7UqIFiyb
WOWgG7KUfjK/mzSExmxTe3A9rL203r6QWSTpWnYfk5oferygrwTucEhr+++JfLkJ4NgBKm6hmOcL
YTcYSAM5wR2214EGRiPYmF0v5iClMIPnafmApXFKJDTta30wATEIlfqUDH4MgJeVvZRTThQwCPKS
BXWiaF2A9rsY+kGu3aQTB+k6iZt4WdJR3WSBphZIOsCOPTfsA0Jp1/ezt4ffi6f2h5Uv0owFYVXS
eCoyZoH7bFWZKYCKfPJ5ayiF9uewFPboaaiTO/Ym/f57H4EGUMlidVO9QGpW/pdTHtTZ9YqxERIX
K5gDnUOOnb7JmzkPOxkNgcBOXwwlGZlAOBA5bwgwmQ0QnuCd/U3KSvN+VM+1R2rXxTVT58zZz2jz
aO6sstZNirhBV8+RfjYRZuyA1K8QVDSdSKjFW/DacrV1sDSHFH5gzw/U439Ya4pMFk+JH6ac+qTp
OEBB7qJyib83UiVil53beaXW1fh/6kYBcIxbO70lMBP3No+xyLOb0R2YzgHeVbRrUOheXgrY10kb
PArZb8mb+VYiZrBmjySFg5XrNVI3hP7YoMi7VJntOjLvfhQ1pxaOhCT0uwS4kmJAQzXmfkzc4auZ
St9g97W0zvem7wVSJl4F41DWMjPIOFYnOp6O4Rh02VhsjzJmEANmIxpIBt90wvPSnzd4HgIETBz6
AXTV1/Ya/27xlz9ViHlme1Y4GfwYiuDSlqEuFSin3iDvBwB43LakTCGsDjX4WZ2KkYNikvFGh02/
Yvoun4efB36SBdH1Exz1KzHlhKHKMux79DJ3HpBcHjPnNaIk9JT4THC6Z9t+9qaGaAdXv00PIpH8
Aeq5GhkLXSp/il66PjhsN84E8PTj6hXSoR4gf4J0hrpYltlF8GIfdGSBBsY6fst+L+XA3e/+J5wP
8aBV1vD7f6rXOH5h0fPUXK36tPhjyOD/GiMi/3y9ORS08FJXJEM2VfrLAwuVeD/kqNPGVaWRVjwr
kEoMPMW0A4oH8+X2+FPbrO6Ug9AJaaYwiM7O4OEj15T58c9x260tuVAYS0PU6XZW/ytcVmrWKTA2
Hj2Gpbs/IlgQh1JYu5sLMckLjrKZPyah3MpNURrxp85Mb2MCuye6EzBAQwsBeYPASDyqcNo0vUQt
7SIyUi8num0ZG0XF4ntPDULdl5i1VSQCfjU4IHUQxe8q2hwMOlbReuGvRgZKcgWpr18clp+hcc08
fHztIV5/TZbYaC35fwkUE3Q6JNH/+lve3FmaW1t5KuqX5eT3UkVPorSdVR4Buyx74aMMzrY+8xUF
wJPokIJOjIdfqJ4Nl4ytKraAIvwpMiipH2zV846pbucUeZkwPkI1XhVbF9zji32ozJ+FGN6OCKdU
9RzhxCbYwQ/N6ha0ErqGgLhe0qurp/qodCO9X2vGc81CzYmXq5lr0dP3Pk1oJYCk6bIA2SpPYF6A
MziJaJzOMTrc8ZfsBmBNW+2vLY3gBgHb0uPGjFAJJXJOyF22RDT5aq2FGbNUigTNsAB+D6GcAgiK
yjOGgyduGDueLqVN9pY5nk43kJcMWPwHCdVtRQsbjMOesbVqwRTYpxwpsTlrtEH5B3uriDr7Mqfs
YzqjRY8Re6wZO7ctYO/N+OI95LC3fkpAqj9DzSuS20I8016nHE6gQF73bDJHvP9ukm+nhNxv4M72
F9qFPJNMN270dDsMRkaXkAePtxWY3khCKvZ3BUSUJ2sx+bPDx0V5moaq61WbXiJZM2c/d8z8mJXC
HlTZEStaR6bICDyvp8UPc2p2qSxjCn5ogin4S+mIW1ncJFXLCCIcxAFmXmJMFUJvdYcT0uLkPDFk
h9YRUuWkQa7m15wZoNIBQy7thW7OWmCEZ5sbwDNgqu2VJsrTlwVR/4E8kz6jJx6jUGA/hRH9LQhS
kDWrElVIaWE3tk1tj4xHuvdixpKnNS5XV+PvZ/dVUjEbOtrh6CQ2Uk0ZyxZoiS2MJxO9UViywS7V
VuaqsPgaB5+IOoIifXsRQjuOujkDpHryiXgxPVNvy9wwOP/TdGAv8JGLAZ+zHrmzijRoR7CnPZMD
np2JwU0qwBiFJaDYvGdSv2L0Ewij8/BXJVwP4lk++uVt1jKN/n12ZJgd8A7CkTyLlVKtLwy+HUjq
UqUAn0+9epO4LTZRFs8Pbfktzanmh/E3hsqZ5f24vSHO5T5thYHRtlK8HrqIucLiXiS1wRf14s37
vPBgjLYQERp8os1S37N1F7yJ2LXHhpGZ5Ob2fjXRgRG+wEujh3kfLnbxKni3w/3dZsnGA0MJSXwJ
9u0A+JkmZNUTAqgMw8u29sHGD977P9XCblOxYSTCecva3D16nhgjLR3eS4dr6rKPNKBcBbJbK7Q3
JQM1MLcl8MUAycQ+PbI1N+PIdEUM1Jf12W+Z/BKjPqZejIHw/+TPftvSszUuXl8SiHMqNHtU0R74
eg8AdNw6fh4wYk5UXz5+pRBJP2VP/+KpZ43H6dDAiLbc2+TVD1A32AzG3btQgLaZIIx5EJEjdGXu
6skMerund5CqGli0vbsKcn9L4F+StFFDdm2cqi2uf2XjdxEsCZDQF2oLlEA9emtZ4/4u99twCst7
QwSnBZtl0pZkhvmttsJZLQC6cnQqKcOwxZHPO2hx6AYazxAtdMTqYdLCMDRMHbmksYHICLIH2mme
laLv/mlhy4KKxFgzBRE7Pz2wN+zF33eK1C41HDGody9h9AvM9IC8SYan62U9BvoZfyVhmgja+MbY
pZtf0B3+bTUpnd5kA4dGqOPvC55kml9BumhLzIUnNvGEBtGNzLOTjFz84q/zkMV9aSi4GpAhNK7U
jtv08bTOPnyYKsoaOiZOP8OfW1tG8cmcTsSuuvS5zTA1xmL7V48uM3Dt0dM5bTrXwmmL4ozyN57F
aufO2dI6wLJlAu4t3ZZbAyu31Htb5RlPGOgp3d7nw1Twa+TDNL1P4oI1eNxlFsVh3w7FNCZu/nHy
4jq/PmFhobGxLwxMaDBeQrXnF86NAKkWIxj0dQftUkjDTo5iZ8nWT4e+nQxFp4+Ff3+I7CkK41xG
Rk2mf1dVHg1aAPbwxSICFp6urRuSJJWFFnz+MpBWl780iccSTewSHSyqPFjNs+hH4ezJkBmQMksx
pKaJx5nfVaQ5kbjE2FnrJr+VsgjMuhVU62tCy+hiJDDOf787xZOYJbVAC1/wwvfmGdt25BvpNgSv
I/EI8AzZVZ9ocv+TXGo2z8EVknVXCFy8zFQ29LFSYJR+2VzddxneCQ1tGZL7pKhlIbdhiPd7ymV8
5SnRdvOIS7F+QWUAyLYsDRMfDLK8/nHS/J2D4rLnmwt+Yy9lhu9l3n8gigVII+G7DMsKLSWdsB7n
qT02PRm5Q2FEdslIl4FVpvjmXxBrF+SKyaSr1+Jrdcjy5w/B87EWa/aViUtIDM06OYUf52j6BrkS
mhip0Lcb0pn98DT72ecMgtdG8i2KX7gWnO5lsMH6tLWpxXxWM6oqpvERuv/ArWJyjQ5y+Cxz35tl
Z8l1B9tSHaHku8w74bCYzdJOstHN52brElax6Zm1fRqU8oBLA+U47OYNWQx1zUe9GAK9Pn6KOULk
Msgk1D6Ecx0nlI8Sw7FaqX5fy3CIJJ+lrLwAbeJJiQdtW3az6oSXwMFP8wloh6GJqGrcHQgYQItt
Spik3pIqwKPtG7LqGvyPH8fuEkdibpYRd850crQY9+eRxoK6evoP215T1u4epeW5tUfkHOi/dp3T
e/jeYG9hbx39lkwPaeSR8/JBC23uNzcnl4tyQAV7wLrihYuHBxNGavjH1vAo1CYm0/6DGRHqP0KG
l8XoDsTkKvv4+Kzkc86jaAdxywTsbZpJBhXafJ5d76nS6MxJwnUPbKnPLkLWzWlNCuq4oa1RX421
jWKGqGFghp8C4/dVCXBs76cqYHAyBHbEFpspN3UhtnLovkyJkKvIh1aNGZhbE0RxHRAgOADVzJJS
UhX9xRVuBnHpwePgzaQBkGarl3ank9XVyQ4KQC8idKieLENWvxPz7IbNEbsLPcI4J2tr7X5Ga7yR
YyWM3zdR0lWCE47HyUm0QB7V91W63kR7fn4I3JM4yzMaurW8lHbwb62SyWsvUS31DEDVxkdrkJ5f
01yi5+UT6U/3183swMU2acDjYpvtrXY577fjq392862VhaGUn0bIsZsV+XEoax8mr4aVD38N+brV
1c12l4L7/qb5mjepzl9nJos1J1WnntT9F0Dx0FuxC0tWk2D6og7fuPoKfEpMbHsy++r1WAvLADRH
sNPqz9sVZaZtZ16+QNATbanTFt1EczJhVNC6tgG+BcgTCHdbRQPte+M3SK5otOGyB1/368gTBwCJ
XkAuvVeaHZFQP5hqbmduBOsIPN5zyKOIPmhFEOPrtYqLClEDMSuWAdZLEzNO0MnejYEJbgDvCsVF
P9EJqm06yKTTg9eSAmdI6dvvIG0WqZL0lrV8R24O8wkg5DPEPGHWWQCx/jj3fYhKd9dPB2RSlOcm
2O7A/ThuUhPqOIq+Q6FvdXmy4YYBKuQHHES/vjrTA+bkQJHGgIRNJwiD4T1er/Iz4SRXMhK1SNdu
RpPsHbDpGrVijqbBqTmJc50lEi5MINNUg94c/TXJnRNc0u18s7Ejzh2y1hMv9kWihKcJ9txRDNUO
n7+WHunIBu/yAuyJGL1ogambF3CFugnr0uN7WLwl20XWzGAT2pIC0Q4H6hgeNumU9xaP6SYlNe3L
fEpih0W3YUnY9CDK+VcQe5CIHVSoescJJ66Tu2+4hOCWRo1A43MuUy/CP7YU+QSSbem73PkP2duQ
AyIWvYoJbVxjjOmS1QIdWrNNhWu7xFIJ2k+zEQmMOCLW/oU+oLAPwQfnWQECOAAotunRHgIZAsLT
1qHQ2TCOY8TVNucor71RKyhXjOWvYFXaojmytnzfsOdkuDfyTrNkedWKjqthw5bJuXxe9gFDLxlK
yXA7xhPekYdXkw5E6R+1HfvLT+C5hmJlZIpKVhZwyHoAnyCs/7cCcsK/+cDH1+1eDUoDEWR0a67G
kMpBYQl7TW/ye+XD4uW9GQODmPEUekSFtb/Zq9tGuKMHzWoY0gs5eVHDfCLTUAtudFkL45p9gBDs
eCI63KGrZ4nTU48cT0N3+cl64rm5oJ1mONFh1TxzZivUcTbhVjAjKEuEwNCA+j3Ae9IAsYEIZNSO
QUJ4EWYGFtKeCBHrQZaZu82rhWRBd3Zz9asQlwicrEFU5Ueok8jkhv9cYDLh8ypGtrpXJ8i/IXrR
0Zke0cRONlRt+W+5FhbasCw+xA8KO60hUCms9t5R7BsNlizvG1iezrB0YETN0a7koTV/bN1O5GpG
Z/kkmtPWAa0l662WuqcMBGY67XKzWQnZf/D5zGoGpjlvLED1zm3TXt7tiaRaj9+uCp0VHwpFnrYh
imZEKluRz8bTYbHbFk82s9WFU7Gj02KBXx20TyC4lwYSuDNgZY+jegkg7vpDoJzZH0Y4xNCRGSDK
jnT009EVMZLOGRv0tzahaV3hFKuC5ooX4RUPH71ldq9fPMNWZJgmEWAsc20quDOsQrW0WAkK9usj
wuYC00Qg2Bbt1HMRoN6hWpUes1GzuRTRzcSWk348NEkjR2HZOx6BGjQb+qYyFL7oUoiQqa1HIiUt
7Snk1i6iFhTJdQcckfYDBphc2j8tumWesXmGE5YBQSHKc0apJdBAL3eruiq0npjGgsOh/C6Wq8ng
bgPaFOCUp5EB/b2KqpAPRSZLKLSdB3RQ8xOHcH+FxXyRgKJsEtpBezs7ens3uNfxwGoULsZ7ibGk
zzNMHOpYY8YhbY0ze8stk/rfe/tbQQlOSrwYnHG0lRuuLjy/YLfXQili4SjweraWOycXnZoRoCx6
+uyVIlAqc11fUERCAGtI1rS8Ly6jGs3qa4qDeksyoRCx1ACDqlMEHeAecp1wnC7Y7StpWPBzg23O
n6+VaKnI70LxfX6VCJ8K+xI7DdQ3scT4QShh3PwmCWOyXqcEBmJ4u9S6zZNKZwFz7PvJrNKELWfC
fn2uuNLum3J8uw/ucD4XEJaMu3p47xOz/Nq7YCVK7nOrUeUKSG+AJboeBdgAHEGn4aVwF9p9otGE
cIjtSjQ7jhdQz0kdfCRCPJqCBZ34xR4vBWyqDym3txiLpsiQOp8W4CuUNpdB8Lk7aZf5sj+ezYCC
KzEddvfLNhUqomUIFkU4Tg3HuKG57fhIPMZA9/TJDgKl+eIckDHhSAzN8WFLxH0VgrtX0zodJgMW
L6iiieJWQJgXUHKWV326rWZt94tokszpo05E5N/lCyN5PVbeZotJYTrDBmO2EqBGF62h+H0jWxv1
XEdd/MsVZScaIMFQIlnY+KWsOYBgOUGt7CKxovRIs32sIiAMJ5T/UGYihPqG6Xf5MSiQARjudNDa
GmhpukY3bEc943X2wJXUEs45hSVLt1WMY06LoH41CfaOGcwTqW9uK8LKjz4BHgYDx93uvPNllIa0
rzrczu+ri+qyWRqPNKlw7cl8399wPAtgmyTySV4917spFAxDN/XC4ZwIvL72R60v7pcj7zVvlFAQ
gIxVpi22rHu/ocNANjNaLQAjGOfIxHe1DycWipcu4vii1IZuQiiEmhOqfLZoVZ73XuXbnCUUK13y
Ekw0VuMMj55jzr+0n5Co4oXreolAKhDCJHjWMHzFTqUyvD/pdHiOwW9CFYXtog0NuPfANeBn4b6G
UXwubQ95l/PcoXDlFjrTTQWZ5QtAx4p6UPI+xy8VGhTFv8WGR0by7kOCqVCJNIFB6485s+qvI4/m
SJwkLEIyLW4uR1XjtPqpCHzkYejUZ7NMtaGT1EUoMIcQ0ZjB+nvObs+LTatBm3SYufSd01ODTzIs
0wQ3V3KRIjkHy4Kyjfmos0osctX+kv4YewEs67jLzd4qI3bBbOMD4P2b1M0Pu0dL0rpsTrNO8aZx
xFDOrw4d++wpMC6HT+SuoOtv9qmCFr0REamZg8Gp01Jc8BGpjedHjC2s1OdYBUoJVL9HhLM4pryS
B1hk6p4WI8M3+XcUooGXW08NqV5JoOj3mBjm9+zBYAD0cPY027JHLkBtTyL3Vo/k0Gn56ZPlYyHF
95QEOEjx+n03h+ZLfEgx6XdKvIf50gapOSOmSAq115efl5jqNO8tQ1/nUmGNYHh0me8fJuzrPhjq
E45LPbGGNUD44PVTPZgtKSYMvoUaLUEeNYSHl49YD+/anY/E3LXip2OW9/26xyuxO6BAZZcwRS4B
EY1/pOxU4AYZfjjlL9PwChtK0+mhFT9KqFLqMauE1J+22ypZL7vXzM/vwZHv5vbYE67FKJC6CfGD
FFmAwMWeWjzDrq/T/l3fXaXZAbhIT7EE/R5nFbph+jvk+Ycfghe0BmtD0SAKzXngdmWD1Ww+mawf
EIMOS+cxWf+3K59vwBcI4xPwAJjmn8Dqz2Y0517VQEsNTgmjU+DhGKxTzsWaFA3uqCPuTRV9Zcmt
QYHy9BuzMZY1t/Rq9L3cBDHHwMiz83IylAmfmMh8sA9c2S3yYsBaTa7f931RMieMw1i1/2FPA03h
HWdEkE5ZDnBhWdm/FZ+Z8B3pEuI9j1RWjm0fi7TqiCBW7De9/FxrxAivylEe6gGfRnPUpKSCWJdm
QrLTNG5PzYWhP8bRkcGEJJyBFXOqf1PUjjEeXZg5gofk1oLFdO3l5iDNyNvEBn4VWWuKkIvs8T4C
IsLPqbPceLwGn5a3Mljw/Y9GnfcRdUSnsx3pDyM+AKMLEUycNlFmjOwuk0iIXByTXGvxifKMJMOR
NwEFxDj06OvTeM67aep/ovSfa+3D/Xuux6iLNrs1EpheUOZjO7t6UEODC4OobS96LRErJIUoHzVS
eGnJF/bLhc6vcEc4xOI58RyRp6QyVXVf5QFOYLE0diakoFQ5h+9MTBEwPH6tTtv54/vfdwhcT+1v
dCqFH5gfcpsMYs9W1sb4tAUtLa7Q09WnLWFTcVUkXUaxFbiF9RREaC/IpX13sC7kUhAZRC5EFVDK
DOhaL4vTl0gFVhkg1A1yniFwdP0fomzS3e2evYSzov0j2J6YKc5ZCWFEejMZBNbR5239Ue+ckgnf
SDfgYmVy/OsYZXDtoxDVcz+yehn9w5rDdw5+8tEyrplmpuMmJKxfuqLCyZHBP74b5dK0TTt8JSsC
HXVIP7kajf2R7vOL6l/k/DAnbhNr5ZTIUSUGEhrwqQ82Zg+JJpKYy8USX701B6x55jFz2Y27+tN1
od+eih3dguZ6e29L6v8QylH9eEbqzAola2UQJAyik0MlWY0HE/FzosXj9cdAkWUiVC/NC+4VMnIe
ftZ80i18XbfHnAQ+PylPNyRrIYg3Ur3F4KhvC/hJ9WUacwTXsGTH7DENnZ1Gg6kgaaklqsC+tzAz
NcIwlow3y/2lzKNN4/HBh91rkx6c+naO//9udv3JJwc0mGqfQuDTalJnjJxT6EMLdrfPEBsmSODf
henQhmLHDRChc8x/Lum6W2PS0hyjWVn/gn/yx+kMvobhDF+E1Mv9Hz6e7H7xF7NaC/7PZQGbr0bk
EuBhfjPzb9IBUQdp3NFKr/wCMW5y6ITTYpC2SBR8SCypnRg24VrFnw6wJhhjFx/ZNlt/Wlknb8K0
+SyejKMpAx00DS5l9P44wUfS5pR5Qy3c2SyWJ5es9iU///FSlBber8HvKRFYn0DMzcG7eZvkHTLt
q2iEcP8GWCn6jPjxDcsCk/bZvS8PYjl3/uAbCEJyY0POmL5MG8k1DaehZ1GiwsjB1/4UdKtqH75y
LZPrVlBu1RdV1kZ9bcVFP9MQVJDQn+nb7IKTqHiIVyRNoI2tBRlK0PiSiZDSJLsd5eEfxGHzuu0f
Rp5yH/KjssWU+CAH4YSAIygcq7jAs838/5TAOP5tbx6JzLUUCfNs1xJOMlHCxmaZdRad7t0zUv+f
bT0mYQnfUFDn2zHOKzPDFqhW0sHTHE+4pz/dszaGLtG4WGNesFKqQJnrwLQtLAttCvyuww33C1Pe
9ygYebMAPEtO6O/NZTRIQ+qtimwGbuK1/biUvKjypwOXBoNtcf0rYA6vHuo8XqO6uBd8QzIhNYGx
hrngHBM/ZbnJiZJPvvaWbNOJ4URIBGTjUXLOCBYtyahqXd72bm7pEugv7NXrUZdCCEWKNienh2+B
J0vbgKuWPxrv9h/dJTlf3JpJNSBjftb2tzU0hqjwQUSlRVPVxPJ5asZ3p6fc2dkmRZikbpoJHwms
WJXrc0okde1oeH8Ig3jI4nxMxtRh4aHoMjCUbUALkeU/eCHGtqNeB3njAegWi9u06oFb1pevPPFu
HcyOc7iTE8+6dJaG7mqrqQrFkqoCUYFScfup/9tswXYlkZlZ/ftmfF+xrQDpjQMMl0+bmIqKOFZp
ZgQVW36fIe4r8obaFcc0teKKXySZWpQ9lWx5caXxL+PraMo8SM7/ehUI5/AzkefTPdPHb3NdWkVw
U3dNauKmbs7dukqIlC1GtFmtlpAvhW83ZIvgeTIOnydW5S6fPxQEhCbDQWF9ENwMNYh1Si35vp0s
Pzlw2d0mg+0c2Ye1qHR6UvkAX3Cego2WZ3S5Wf1pOxtjKPcYY+pBYLYuxN4+G0bpb+zkN3Ki+UVv
DWK81fJpeqMcdWWlu4GBdGdeNCCMVP8vQTF7irzYAvlC8IQjZ130CIofjA7fcg6O9gjQV2yaPNP8
b1gxX8PIdRclhrplW5x8Odkfhjl/hsHcdMYkMR3jTHdL3UavFYc9koIpIaEERRUh7MiykirZqGuX
+mpa+rVQNPG5tPKOvDPj2YjofkVF+qmf86a1uWrc5Kme6taGY/4v+YZJfGp9jzpKewBg5A3xTtXY
ot0TlJjiYLOXqK7juiCEh1WZgPb9Eoy3QQl6J0+SmbNC5Wtt7FBneWja9FQwMtcbIIDLydDkgD/O
2oQuatYOvxYIzAHe3jGXrhEf9kvnZGJsEk0mdI+NBPMa4PCLsf/ZhyDdZa7tjsZfWBl1inooUE0R
ThJ9LdWcGXem+WqOgBR1oKc84RykZoM6lL+Zbufj5gUSHjHROKPUDfgT+e9T5c3TOQYLP0hSrNAP
HWlNT1kmKSodkZ5P5ewjpNCcTInQnHXkRxbbAiSofkFRLa6+lqbyQa0Ri/g0pGwRq/IBgkW1kup7
svFryBBLhqoLM4r2wVRqrO4q55bfPDDF3jzWhHm04Iv4/l7f4CLKw4KHV62pKiYGrprgo6BRaXGv
B9pqm5/4jfJCZS1IYaP9/V2d+BTa1a0ynLLyttdDyIRLU7zmuouWysB/X2C+9VspGzuqsgTfCLHM
v49nZbwTGnQVX4vuqhQ20TmoxIuA2l7JAtyCnsoVHHkaOBrzktGgbnPPBjfTIbo7DUfRJiz1YsmD
nDPSGA9cb//xKx+DioAiVYM67eKYL5r3A4YUUylyezm1pLSz1BBR9VMfXjykHmhlP14X4J50k42b
82Z3HtRPtVFYwMqzjVdt4JVLTtvZnBqd9q9RHA2ZzZLTX+zS7n/IPbk1nxtNSxBELV/+Z7xVUrY9
tqSlaYvQsfErd3sTFCaIknf4PEj4wJ49M7LFGZ29MWD/Z4naWcnPkD9nW1BFAFpmOb0EQvQeLQpP
/k5leCSA1kWs7lfxGawRrKxNPdd7lphNpEWTkkE5B1qzSDEYfJ7oFSXnkjKREc2PVb0iouuyoev0
3TIi1Tb94D5Of2PMzHZcL1+otXno27ySNpS8dN7ZHGUqYhromRQby+OprJuA6gCnsTJ91I9mQlg0
+IEZudYRyb2ouqYjONcD8NADAA/hJRxePXi2mRbAekit6T0a4B+Hf9jq02nQ4oVZHzVMYNDOCMRq
aUNHouC8tNWVCr8P6Lr0MOWXUdbXblf5XNumykrRb/PORe2AsIBJ5UvW9PNuBEIiDbR0R0QEzThy
Ks6GVldsaSvp0shmIFb+vrfhiQQUO9+O30pZtut0lKeRDLIcHXP2+05+X8v9iB8GPv7YrU9qyb/D
yr9GUjcOVa63Ma7LlnJpKyCk2l9rpRQgZAcmqvhyJDsNS6ToaBkdWXh6ljqT0Pf5d6+sTWLDe07x
x30dVPuhBIb3I9QiR5jDq3dLkSPQQGqryjvBMaZm8KAkTqae3vwc5TQiMMUrLLROnGpnGorZhWmr
cUZgwk3gd5JDdYfOiD7uZv+NTmypDNyThzdz9cXxDBvolHaLlbf8LrPpLzlhUdk1Ryy8YzXX2aBQ
V96VWVlF2GxEv4R0cmZe/OTMQsJn2KPI3Pyg5HC2NtVpEhTjhhYCKJCqKxUAu35uhou7fXRJTFOE
v++pRIXb8u5aESdo63Y4vD9xIbLc0lxOhuL8jphuJvDgCSOBDi3cIE78gJ6F8cmTvQvwuWsD+7e4
Shhub23yZp/yuzkKDyV5ml9fZhiyhka2DEW3KW79uZTvyOJbvEh3QRznmF2Hr1ppTHtonXVT4xtF
7NlWRJE/yv8vJbisZem4pGxbH35Cb8chEXR2i0I1VNHbdkZX/nGVK8Fur/Cxr7a5QkMf9mN/NM/h
hMQ9AbhiBSYG+4ttvKjIeIRuyFfZV86ulzpw1myhSatCR5ks3/LjHY5KS0kLZJF74qvBz2+4ijar
Wkw9Kkbb1yxcKXy3qtpU71lmUEu0odCRjmDLetutxjtIDGcU8SA2RPyZzZVodNxfINxBmmFO9ZXP
QeXnpJTyOQcDgRmD7hLv6lphBlyyR1mRErKXAcfnddl3W/wsFUjVLChaajpC2p6CCnXbW/MeE1sm
CUTvlrxAsonKt1tk8CTe1qr+HC88nlj3Kv/sBxfeMoH+vcBMUhniqxvvgop3OrtQNcVOQfYa96aM
231fkd0uS7Kq0/R1tD/7vy/sNV51h4BaRhfJP77amXHGqfHhSfyxbUAwMUfoJmnojmhikwrdxy/h
k1fLBui6YfsqSD8DTupiqOMFiChTKjunB7CmF5pn8Q1gwfANHiRrJ3t/DCN+GKf+6ld8pIVbE427
vUsIS4dnt8qpf4piiAT0kzkbxS1UL6DDxpW7K/b5Cu0+ksnsYSXSAKsPPFKrCaXPLypceaMxiRtE
IFkccG9trCMjklf3eKs50zhuBG6ecaElMznIZX8eySzOGzInp5KRynkW5G1F+3IY3gOobldKztgr
krhU+5uS3I/SaXqr2Fw19Ksj9h3RdsJ0TRN3d6ScMZ6s1O0AnWQblVNIIVve7uaI4DeGHMP5staD
0CdjPYkUid235SDK069RlYMcmGs4jBvZbDttKzYsRgtPBUVzWzhMzT4smycTro5jolxrlJRVs77Z
DoobIp0CfjZD9xJNPJfpUnB5V+K9htZfVNFgYK5s0aCyqYo9RVHrM0pwXbcmttvEJIMliTw7Ohd0
ozk65kD0byi/sfJWV7fF983OogteC3C2VJKdMpfZnXLLLtDex6ntp7g9wLjL4j22EsYmxWaBMXT/
y0zpsafT8Ib3D93MDQGxdeS6KIRpQsSiwGoxFYfOGqJgfmbOr0tbEuLDdYBojoisWkWszIbc3DWO
ZGrYIoVsy2FzAS3QR0+SRzgDevk04xlYTWqq0Cno7WNPVFug7zBBprIXiR/70Z8bUrM6uaV8ilhz
ltUfasF6evyf34+PQUTni2h7oF4fZ6g1C9+AjvlB6ArjZdvZFM6vlyhA/K7RarvVPX640LdbVF6z
Epdt/pr46h3u541kje82/HA5cCK1wvBA+667P5/wtirM9xBy6f+bSmD4arnYKh3ghklT+blYyzTw
cmfQWzIM+Aar2GQm4pqKbQax2NYNhaKpZkusO91T1XAjGXvGfg7SNzCwYgtc3bP1Ma8CZeX7jLzc
jwgGqd0yO+xF6Be1Momn/QpOdXTay72bqf0eW8ACxGzIXmackmipGMgWeyV8kCWzXWAZj4YCnNd5
i36uIRGu6QTrSagSzEDN6/Ps8d5GKj8h4PFTAD/Se/HKPQaTrokXkGmQjPRwjVuUu1Dkov3dcXx9
VH2ha8pPFDICz498WtNkY7BbBmTtW/qFv1tW28sDrs/DrB9OOzih8ZMqE6Gj370lgbcTFsesHXwZ
0qSccbkQaefUUiYgTOCif3Oet5NwQpVe6utwrcLI27zK+YzHhpvemsJDBylHf9cGmT5/QTuAUbWy
WeA3tnnlG4mPc/UXEMOAAL2IyKKCKrwLrWFtFa+ieTYnszfp1kSMwNLn21EVCzN6scGUIsKbp7ex
xrLGhKScN61i9WhvD8WdlXe5fcM99rUToMzYSj47+ImYwgstocfGxxUt/WUY/+sgjPCubDAr+Cqa
lHQChCgj354JVp9U2NEckX7Tska1i5Rmf8f2GyLY7pei4aDllzLRXb9aIrCFijzpFgXt07aeO30S
uhQbhSwEvQVBBMawNmWVz6AJEdhjJnOf33R+7IhHfRVxm4Va2XdvVQ7k6UlZqTRMWl4aRJNFe+U9
VX6z6lHZULHr1OcgH2/2XVQbB17oDAVpP+TqKE31h3pBB4Txx82sHpUKIsi0FnklyCbEn7OZSbUR
UXskykX07K2tk/t4nLI2UzecOgyckFxsnLiYN4nNjVWq8dVw1UuG9rj9LOcu2FlsThl/kZJCYlUR
Hl864T/2CM0xBh3mnhbmK9sbr5souBHpN2EvNzhTZGvcET7FwPbZnDi/PgCkN+mZVvY5eK7BFAzA
uvK3h9QJfinhji4xUO8WWRJpEn417hRW9zH3eLFpNq2Uw9KX1uNEj70V52xx/p+SwdahxgEUU6GU
HCGARPxsoWW1OMla8AN9brcYSYC6qlkLpo1RXkY89J1RN3dNPDySDrEgoMqJDeVIp4Aqst2VLkH+
gBHVlblgNxtLYqyrjIviE0rP869NLeFVfhUQeWbRVBNp3O2X/QzAVkwtvaJ2lSd+wcbLvyOj4sOJ
FV0fsKrMUEUDL4744Keg6AZeqNl26YZSaCuEhNoTs8eLipUpo8980roKDa+p3g+z4gAl7WU1S/0T
4NAIoIPWgcIkZ0HNWPpTLxi2VwlIQeeWbI7ohW2uxi6UMFRRiIJF/vioy20+SVe9jxsjqpRbH1Dk
eb8QxMQB8WBZ77zt94e60RxAyJ3tTQvshi8lwMN44aqFFHpfBrCXoKp1PIYHJzRrv2LVWlEFyM2O
loMFnpTVSujUhl//NIude5AsRe53Pg4sAkBd3CKpQC+ebMBeySR3DTvdS26UhQ9mCJWqnCclT1Wy
9T+Fffqh8CVWAQRl0cWbMOfkBOfj1Sae73vKq6/iKCPSPXgvgKll76GNDIaWdk6Ja5XmilRwmo47
+FVyWizWnAl2JYEgh/ToeKxeDuSQwM7lyr7D3jCdkAJvELF6LEdoWJ6kQGkMBGgUaK30EvXOcbey
ntaqQ+w6k3MrmdVx5ASwZv0PG/lw2pGOULimrZqWZ2gwiyVUQtSMWGCS0LPSqGLm+B8Tmzqoha1b
L+onIFGRJlqsxlWnaFQGPanaNx5ojNUuOrfMk7EzWszkwzB7JBzoU6oYkBXAVvq9FjAL39VCpWT3
H50KSx93So0JnvmftVKTntw0CASdO16EEjBtOm5J1ZJQ8+lhjPSjeATE08Fz/U+cQ18WiVKugxgY
Tu8Lch5cmE9vExYlw7GKwwIAtYN2ZDw+ecbVHeiHWzZ3uubGTy6oUSiJDD+Elk+65cPnsELV7iDj
F5hSBjM/bsGKK0+ZZCVp4myDAoTl9zEkR6MKo1T5Jz3T07ZDLLY77xUEP+eHVwMHIK5jYSG4XgEb
VxfS7yrpywpClzX+RFZhWUGz/pFpVQomGQYZ2/8ArAcHT7jqhmxY5iOj/hN5XQG5Y93TWm3BZQzd
SgGIHxXUcZNvrHfurUx5vW52Ordaf8QR6SaBkCOMwT5WOYbKvAW2lRkFjY7kMWK4b67ssMFXKS7k
nzGhnJkBDjuw+zjj7ukgO4wxf7TtaRq5GpTVgDO+jDiFGPqqg+aP3pF+bft/VXL6PIHPWQ0SUnSo
hzHgPHyJjAHWfwDnaggaP09LV39U2Jdp8FWtJxBppbK3TJbBz++6KJpnfJdpH2Ju70teln36BFlZ
a6O28jdtFw0shj/pg/ypFSoAFJ6OWddRtoKJisPA2gPcS16WCv+zwnuZ1BSJLiFhQFfMC5GAWnL+
jE7cE/H0SNiLlQjjYStTeGEv60CJ1uM2CwKkDcZ0eVK+xUVNlMcHUWpzZgxGD6qgCMZdFzFoGZ+E
B01yAq5vQDAcYJ4wNr57ScUovx6l2uSYo4NMJguWbQMktaVJ+qViqsuaN/Gg1OE5j1Im9uhWvEX5
RE9bys8Y29ngcR/yAyREUuWfcl+nAllL5CYG3zZOfetLo+h+LsrmdfJgXjuQt6vlTyIj2Vz6AI+Z
NBelqvRY4goMB4jLKfvFm0YsOr6I6F6hN9FbNpgcIp0ejNFli2fE9XPGHJHANVh7pL7IN8jZQ8j/
GUzWMbSwndlWE6uz+Yks3PKljnHfzJt3bZ8L0aKinc1wky0C8DOeUt06yMUnxw8jAiMMupi2dYsp
c7QrfH1jUwWJl0PVr90pAad4yPaHnJx3Nml6bv4OVq1US5bJUfLLAryK8JYiO3rPc+z3hBnSg68L
+/Scr+qc33kY3nPz/Ua3PrI7X+z0jaRUUzEwfhWHbU/gusog9crSK4GsJ35ApDNmYepdmSiHgQyg
6mLZ/YASnnbWnX1g28N1fKHFZrx9aBHfqCwtIr8tYzqAsTgb1pBhZRcVCctitf3j2tUpa3MUBKXm
N0HZ0U6OjAP7WBMwjUo8sAB+6LhD1BuMCclTBnpZSfelTNFaW1Vhg180jsoL0vFSy4hmrORqCk39
ZPikjESOLx+0THPbSUSk14X35pSohDsi+ePGZqqU8lUQre4asnAc3hjfYtLKuBRRnvisMekPhH0R
NXjFJBuj4FFd+va3qGB56vSfVjkvC8uOD/xdwEUEmCuSHX8qQjmluKPhVLYBVnxBldGXzSscFYZj
MQZBsTYAYqZHEPsxDklOfMwbFOmEWkKtJVOleA222RbriVeiu8eV8G4l47OsU7leBUXwQDp3Nbwj
ZQTnl8MPbXEC0D0Fwv6Y60B+An5C2b0/oQxA6HhADGdkS3Mm1j/8jb8LFW9/9XtqPVReJNsC9GM1
BSpOrnEmzjWVltiZV7ZNF/Tbn+BnF/F9mrjY/aHRSEJAAB9tPgn7mEKr+l9q/n8CHKlK21LcTalp
2ty/o7Rl//s4RNVPbPZMCmWRjxGVlLZKbAerpk4h5BC6JGfOMSlsxpyRYTDO2Y0tTlDbJjBsbIdu
NV+y+3XQCt+IFDPfCvT+1h42gHEH87C/jZvJLAbQnKNu5pZNWDHEQWYbroHPegsPlGWBIs5jfXc0
NqnMg0Y9vMCB89OGpduaG/h8z9FPdXzmaQjQmtd/ywlOkxxMtJnnRHIqkWoaEXXj5IAj9CLSmKmS
fCnIK0jXZMWUekYXyQzzSqX/6byMBu+SgWSLBJZR01mYEaAG3O3e05z/8E3fShPTsbqXgW4RgQP3
3PqKgICQ4teK8m97rjgQ4CGHlfquJFNOsvv++jLUfv6gfZ6P5078n9qJWrrzb1X9E9WmdtmNdFWh
ePCWSDHIxTSeXndKL1+WwDdXzcsXrBq/vf2Va9Xk4xhTT02Dk1EXDe63JNirF9MxKmaclUItfzQi
xYYDNX9Zgi5fSGz/lXD3SLSIK/SyVtcVxU0inHrmbBrRouXmduQRKa6Y/zh9lxK0a1Zvbm8U9wJ/
7lcvKmbpHtY6Bam1azlP//Hbp93gHXyhsU4hnlV/iRr9SRNW9kjbQhimTgSUBfFJA1IbV1y6sgpR
w241MdBTAudEpfQBaWTuVL+D/NDWJxfUvg+cxS/w7dx+7LewAYW9wEvXZhwPwmfwY/kaTLQlUQoo
d2ODoDwC7qcHSXovd+2sPMQqLd+mj1+Lkl0dKYQn6opCCCOqGYQvWOUQvO4eh/lyzM8MK22K0Ixd
djXKxbWl6/rTib5hJXNq5c84FPUxbtaqkRJ31wAWoEyR1Ni2P+TDGufvS1TAIsB1/uY+1OfEJCkA
cpfndCCb+mPPP7wq82UpYvfJZeVusT6Ll12pGmheZ4C3cqJDAm66lCV1XMLDftNr10QLxo2wLrSN
3kaVrfPwM1E1Eai32pVKRY5maC/vbHAx/jFQ0XweyOqDaQMpBD58PWOQdk24yJLpF7GALFZokxKb
zUqBxWGJwMBi8g24mv17yQGv8ZHv27UsOrSZGc1aZuVnmZMoDFDIeKE0rSN8jFi2U1IKwfzliAN8
LhK/P0LD8jLNRGQ2PRby3bJUECzg6/bMocFcBMeWfCOYuYRoIhQYlnr9tjnYUQAiNY1nz4Pmr5SX
/R1Kp1ZQbVZBrfutgTavTBDZh/OfnVXeQh+anZWBXDqkkgfi+efe7afkuzw6iPq85rTW/Fbnbz6A
jPcmQP21od/2yFjtGsaQ1tXgOyKqKxaXJRBrgp+coGW47qKPUg2nN2GKif8Q5I3vPwgHqBuIThjN
SFVs+/reHCSAZ7+ocoO27GzTV/Q06icl2GIRIFVLnCJh8dmUlwSOfGTMGo+J0mURznD4qSLQV1AS
dMzCEDo8cTJe0vG8Y7jnSusXjtyDFulDbXLUxxcR/cNzbpzLgF/gZw/OpJRKleUGrrFrhRD36VeT
OIcSWalTKiICyjJjFOHRujJiTA6Bvk0UJm+pLEqsqwkq3/Y9jLYUk8dd6zwiSbxDW15l2OQEsyTf
ZVBxe9U4o0KmMlps63Pv/itnPgnHYDHCzpMjIzLT7LIgJx0HlkA6LtCP+nZ8SCuy6TF8u5yoaLhx
QG1hdHg8rBIoeF9Yup3DTNQvV9U0jiiCdeomFkr9/kQDt2eowrmHlVkyW8MB0m2uAMUcBWrrOTUU
9R9W5mTZTUROwP7Y+ylMn81UJnE1BWcU/z+xWYSOcM/k0quUpOvGtVWsXY7en+EFiUnrDRuG0jOp
jVxLykUZXTlg8U5ojf1Oo9aVonOfNg8FvdwP/Rsazl49xSSIDsgEKKAvZ2ciA6nXAYoXRcN7zMhb
p2IB6fZ5xNrTvdu6RTYLhUdqdnUZJFBJDUjezVQ2l93xbGcDGk2wp6oEI2SCxHdODYP60F3BPWdX
xjA0Ikc2RKokmwMv+KR5DazITu8Blel3TKiW2iEnh5mrozH0Yn0JYURhS6J6yO84n9mtjEZH/vY3
wa1ITfzfHwairIAQZ5ki/gwUNBx1/hmT6quQgfoQSu4kiEdEp8fwYuLBgPT2mCjWBqRYMN6zuSxj
cScHlyKlB5LKs0LCHGblokakCDqYWHENYFfhF+HfqvPYVpAXYETrEHZoZ8hNx4jWVfX7s4hu3n65
MmElSH6o/Mnzg2LJ34RWjS9gumzD93xoTrMIiEXJUYuV/+GbYpsR/feSCBwIvswIxgOTAAgesZAJ
vH8Rq7nGQ1A3p5RzB2wDIpa8jqwuUcsIWBgkWnfsXMByHKfuJgs24CpOozODi0MKE9S9mz1pVfjs
5peFTwtOIuu9/lLZE2WABJze3tVDXhDLBF/kCd4ydneru0TBOklC/8bOnf69tFKK7XphDDqVeJ3K
ehHgNz1JK9V+hZs8XPYuglWGrtuaF9+VVFQkKO/c+cgh+YAC6y3uRrzF75c+hB9vNpy722s9jNbi
ZvLcmKLVN26aTXiIpsy8O2NdQo5LyaYVm41YcSrWvQL/bjHfxgUb96Y+oF8k8EdBpPEl20XsQWok
GvFjed6ZWAflJKNdQLL2Owhfprgg6+UXb74c5ET+VA58X2aVI49zlubbvrAk8V8kpBFg7SBtTPzG
3+XzsJTkFohm5kAP9J/65tJFLyUARrs06XWDdu8bA71PerQem24PGImcK1ct3P44JOWVtOv4kVLC
/9aUATQI6B4q8JHj1ljpnaPgtPeq8zyPvutNl+Jt+e11mhkD1qzVbygDMs5OGZ0FAiAauBmlxh9B
HgjdRZuuM2qzElmcfd93LgMOTUQseN45KBPwCT7LA8X6iJjES5UwQNRd2tppfCpnNsCQ4yERyR6p
0qbmjFq4z2HnbXqPSa6QdNc1lWzIacR7tpev70XX2HyuuJoJte1dYIoCC72TBnlLm/FJuBbiFnGm
6fV5ebNbtAuZQoUN9EYSwsf1D56UZYPUo086BIUOaByLa9beE53BB2gmsHRE3/X0edlZAQvNQKvq
8//eDieCwYNg4rV4NEYzPHC/gdisr1ijsytWp5t1TNAKN5BmB2xqu8Mmv/hLZk2qSptWqPkMhDde
eOfofHZhH6S5zgol7FxCVbXTUIXOE9HXtZGb4oZVC3qt6j8ZS7Yv7EYJXEBnp+Mhke2pq8v+/rSs
40/3Rdwdu45KGFgb4eaOmzvjXiqqC9LFzJMxTWri1j93qLDxNS9wKzKTOWt19tgFTiGH2Klo1aAO
O54nxLH/6CPMUFpWbkB+a4JsCW31H7eUy13dzUcfVgHkSmg6vzHFVg9EHCK9RISEkCV01qvZgq8L
8IGCL8iNR1QjSWRuE+hZNCxGz62Yirf4A1ELZL4bZArFprdd6LxCoKv44gK5z0l4bnIXB2Sfqyz5
AH7/60Dl3x6bG/NHd8SMAZ6BohJw4yHUxp7Mc6QtRJoZW3tTrX60NnMglml8xnUhvg22/Z+BRs/I
ogVGAMEyU/2elWHetFKRxhjgKmDSvNs+aooMn88dLJcfmKjra7pcn9wi0Q62f1UodvDJpaaHv1yr
9nNDIO6w0nlxNt0OGr0WuEJ3SYJ5dFjIlDjDPDpVTd+eZYQKn9hfhz6OxPeQCIy9ITwTuglFEW+/
F7S/q9za1UksUWg2AoO416fke9M6K6JPU+Q6xXAzEC5RBsJOyilbQ+B+yZV4CrC8bgKej6W3VExW
uA6dKEHIKuY4VoyJIxVTmAHoayxaGNUTUk2A6Y2hmJnUmUpH2bquOKRohDPj2BYFvT3cyEteAMbd
gjRRRDnTJYpyOwh42MAlJhUAb0SPe0TaNv/VWkusejKxrrmmOqC9CbFjqpUI1E9UGCOBYATD/VCS
5t/Q4SUpuV6JEVxNgbx5ditQlzmpiv6NPVKBcbkLEDvJPlbZsxDSWQ2W65fZMRUnUpLDHE1CvN8J
wM3xXXYz/bjFozQl2iy3ffZcyt+KLuH6krH7VAqt6SunF35V13oxJN/gjqCMKUOJAnIQI3X1PPtM
tLTvFEgNeSCO+0fmfdpyE7CIiDx6A/d0QNiDFzjzLyXlgS+wEVlGSmgskWIt/vktdLFqEaCeGH73
8olRqk0bjGrCm9kMZtLg/94FIFzFNprFVNCDuxWHT36oZPNRn7JT0rugT8bTQO9rDI6Rx2P+c0I3
z3msuOlhek8JqdXWCGXMYeJySgKfxVIJZzy518tQxu1MoAc8t/m8Oa7XgtNd5kILejz0eA0wui9e
pNvI3Ctk1o4wrFBgqYw8heWHvwiI4GoOHWW4Qbwp4UfWcKF3ZS8FjPiGt8XAOXKGFgyrdg5RkYXa
MeW1BX1GU4zGzpLmawgzLP51l/KLzOfFCHQQ1tbnu8h1GlcUus0k7OXiwGWXysmj43fqxcb7o6VE
2MpxzbvsGAKFzVlknLcmn5CPssxl1+AJVHzT+vKi6lexc4prWfTr8Gv4dQxiFA9nGmegAzvbJ8Bg
Y8zDM80cKHWsw8dWklUJ3yqHTBrlLkmlUGludsritpdFzETUteScB32ZujjoVDZsEmiIj46UMoL4
aKwNlbaannpnfHdNi9r6Z5DNreEBWEVnJnDLqt5wKxnH5N1ZKPUYISdT5R3xJ/f5AbZ32lnzfQK1
fjUwWklD1r74Ja2HGPZyi/zG5JtBdRS6HwT8q9gSZp6k11aItYShRs8tmeNE+PDWlFv5qeUV0A3h
N5A/TOoAoiXX3L8u22PJmv+T9OUuareztHmgCxp4z3524f60T7RMzMYDIhtZgp/fUB11lutsF6Te
UTqgzNRSo75YBGft0OYjYq2Fveh8jnoQYafFPj5xjGRI/7q1AZ2ByOkzg02+ny05N0pN2XVISbpt
w+Uiqy0qortr8dv+t8DTvxdtWi3Os3MQ35YUWaoQ8U6oEvNiktcIrJ14HxSZuvHQqJ1nTMUvvbFX
dhBf4FL+bK7peTjecYNaiEINY93hFttjLS7Qkc48Bh671+rAjI73CZitEyVcRK1cI6d+1AfRm73o
6u99ulMHzLCuTzBCN1kDRGcYVQOC98sa+9xW/ANA01aWqw6PjU7p2k5whzh5uXh/hjXpYRkrrOaN
3l8ZJMzz8WcbQVKQLpNqFmKyo1JWBc9cgVWgoGviWGiVXaweq6Jk5AhXPt8Q6vFctTvblDnVdRe1
4c2nFioas7I7mlgY6PreUt3F7pSBY4sBX5CQ2SSsbpLr5oChDuMe2kgok8z2E+gjwLJFStU1nvW8
InJvcxG8T9Ccp+JdIrjTF5NGBqwyPgXtbX2ImEW6oo7JR5t6K/rvxhbWqNY3/dPW6kcmabOjQugS
bmMHP9P+8PrjpKK8i0Slgmb1saw8YzJ5omk3LhsjoZLaDh1i+MLs1GdcXkUtV7c3ZcswaolrbMOT
uhJJ6GjEa6Jk01p8JDhe6yPqUo0tjBRhSrRMAHiKntDuY29clysC/gYyZtuiL7pGiKH0/e5F7jfD
ZK5fdu/onnFkt7nhecSuZ/j1R8iOE/ZQpzbs1xgpk05fNItN1d3SuvIQYPCSBjemi9FlrKCU6kLR
JjnXIIkGYpMUitgUEB4cBi+Zw97eAUWShwTMkMAvLfj4OPteAIepXRs0D80MshP8mzXhHILIY0p/
YLmtCmEyvrXIkCm/58oVHYgGQoY6T2JUFWld2AMju0l7jYaQoGbmOI7yy0qHvyeKlLxxPbej7mZr
Oa4PpL4BLFmvnxmp38pzuNU4j3QCUm6nZ63+RMb1SjYo7gOApLDN374Rijqn82J5IRjyo9Ylr1XC
EGMV1uRm0PpALz4UmRGyTalP3T9ot+pIKr9Hd4PH8OJPt/R/bpgvMfbWoZVwgci5R/P6oQt6akCH
7P6VUMYnPh6DthH20ETKLJi0Vqb62O9NEWekivj6m2ozm+5KNpCJN+OUn4+sxx4NxCaXdpZCkjDP
PtMmydd2eFBvd0uarxVNnoQ4/EAoBmIahATB21PzIRb5vQ8gcPsU4VD8cLplQRvErSEnpmim/q24
SFcqBWvh95uepCUH78ONGEem0mpbPNmzQesUgM9WRSSOSl0shEBmE3DOyKgleLu+NhvDIhuw8u+u
TJeG7Zubbc6GQJp3HXEns098BrucR/DDL17V6M1GSudb+4fDtkxWlDpgOmWRInq75TlWVqAIJoG3
3nur+Zz4GqQM8UjS6WhWliRSLN7fC4Akfe8Lrydp9eQHuMsR9sDPpujkWgWFaefnZ9NwXOgSoh2c
W/e6v/Jo+2C/9hDXkUhLersVwh769Xy+oFA3PezcTK1Ywb0wWIftMgi/shjZ65MLdKSP5MNCj6+w
QjXqy/2cHq8wIakdtzxc2Vns5iEpj6xzHrJbmCNgrB/isvmCv9yxsjVw39u4tV06rzF5dRCsRAtO
lmRGj1nnz90pE3mher847vs8uZSijjKdvaX0BbJswt2e/Y9v+nCvJobgRzj4aRgQo+haGY5hj+75
RYB91uiXbdZGaTgFXn9TbhQC5aLLYORuSR+zees8uz+pu7NjsugcSxZ/C5Qd9bbZoIadByyP8GQO
qe+hOYDiXbAyKRedbL3tEHtFAs/5WMPz1szxXjcJuRfy321WPEmgPauODfJn+juNF/AeFOLuM37h
bxL8viXfY2bnoVT10wfnuIQ1M+5LbtwAu3fzQsqJefzc5c4uFIIlG1ACzlrYl5Q13JrY//s6rk8e
mf/97Wp2NrYVM2dbJ9N4DE/5HbQ6GFMKls/u/2+ydVrpCePUrWtDWZszvBAlHVsa7hnMYLpnfCt6
rrL8inVltMDvQIEArCYrB0HrDltejUTXBQJgwI/+Gt/AOm+ykebBO5XyDB1k0/tDnKDMpxjvrZQs
Ra26G87q2QVRJ/ffLV+W2cDi++tEdLhAR3UpZvM2KD6QpdC0vm2MkWPNmOp5i/AxLPgIbB4hjQ8N
spBskjXpGRr0NRiwqs5bNPtSuBPbDrd2Z54j73VbGD9bWOcB4gWBnrrO68zH1mAZcPL8ZZoIelWH
RJgCf+9NSyRHKWk6xROCzKBVbUa4+eMFBVQsPV0Xbk3Up8qfQfvUDEfs4d3nyEmpnNj2iIxU1jKH
UCTdKQvuzPMAmKcPgPO0UKas8iYh9mlFxvPLK5be38XmaxbLglzjuq5JYlac3/Vl7A5CSYKAt2P0
+sq/NMjWgCKTXshQJ7oNDdLAhaId1jugMwi6b8HqASXzdN3Ty9rKTOEyJgz4Ml3jWlVDvDtAWAIT
KHw5uShmv9/x8blh8lCjQVPgoAJ53fMDmOP35Y98TGy2L9Z05CU/nLKD/8tYvYtc+uwlCd9eY8XH
EM24tjoKh+TibOwDBxnl0gy0EJ9fi8R+mQ6A7D6zTDntNFjerlwncYPBRDMI/w+ojZBmiS7fDzIG
r7y8BeVA7+Lm5tGP5kVChghNM0h0oOux6TdnlqmXNaWQ2wy5jBV4nZVvtv0n2hjny6uMtZUIWzVS
9YUe8k5FbF/J2e1Of3m6d/ZxT9Vc2Y3QYYZeifaAGq5eXA1vdthAEq5BCCb8RG276wmZ9OvQ9TaG
7NTVLOeoIyNhAV2HBouNGNKehXxtK4NZu7Ij2qNDYI/gloU9pflV7W9sXva7HPNthLHdihPgfitO
tbXW9zRET5vmFhBNOehcHJK5eu6NcO8D2gaFv2AZzhnPnjbdODKbkr9+XhTJiXYlOfTxwEvmWGba
cfNK9bdn5MNIHiH20H/qWY8/GbVqyLijAsEPb9RFdq3FBcA7iBJuT6vFeXp5mFQZApxkxJtcuZ1Q
1/W5ZJqHIdC1dDGMac3W4OTgp8KuQSBoqpuG17OArNpGdoGe3D1fPIWgdwt9adD9dM/m3psRxrWs
qEXDvuanMeCq5TXI5BHL4SciHCF9anjbeGlzuLyi9PxNx3hxPvXC7XQQ+Y5ZfJt6fYsUIU5TFlel
soQSqt36OdbDknmzViXDiJaa3xilyTayUUGTQm/8dSj1VMPltoQkuYlzyXbWDtgs2Kvi+UXeyISW
crXJX8V16oQ73pJVB76VZu1HhDEKFKIrjhHex7Zk2V94W7k5MPZ4YroC/3gomINdgPCxOM/YhTG1
dzVUS1DO+ML6U6n844z+MvXxpo1FhpxxIH9Vh12AVQgjkXsuN720y5RefwOOKRiEkvNC7qpTorDp
JKVKnWMiAcH2NKJ9D3O10XcYhELOyX0/oK7baPEjMDAIgQ1C3rXTp1kB1Sv3XH6EbEv8j73KYH39
64ynrbULQnzkVdS4XmAT4bQzgJNdh2qiZWVZV0d+8kVK2Dx+scuLawD1TBYtkzf95MmBrxgNV15o
DcO7tbfEceOasHOFwsce2zgH7hQvgZzXugvel8RlIMzY+sKMNIlOQ/3K6Gk0ZG27BlHUfxO//V5C
iPLpaKp67BnZzPR/gK56JGxRKannBFSLZfWz1qZuspK0h7WCZWXXD9GA7VyWEBCeH0LJbAp0RjFf
n1FWXJzDtogXcaQLdw4BKSevNyqP5JyQxNKAgJRwIeoOGgWb41LGpwy3rMYgqp61QunjW1TWQXjR
+TETj6Tdq4R5ua5zFdNTIBRXPw8um3S5mCvJ3kRxj1Sw17tV7XPubAG2nAr3kJy4pIFdXLuRob9V
xyiKnlBbCCh+r5R6uDmyWT6THwHNrHj29oyPJzkjuVHP7xtsyR4KglQ0Dy/J84Y7zt5bxnBqnvqh
mdW34eETczNnyMJ2KvbsbEuulmh5MjS0ZBpKxwh2Guvgo3ki88RUnVuZ63ssWdvWjTjV5Vk6s6k/
K20T2M7iHWheNKFEbUa0B1lr83ffV1Hv4IArjc9s0pBEiy7UxbFH3SZvNfaJ7a4Qnc2D1PjMUOm8
vqisWNDJui5fOAEjOUafI6R77qjfUTiy1whkr5nPkwfoJ7QNqwb7OST50bs85tkfgXYuy5XYjBp+
R1sVIYT2lm+2urAESNfCeyZBc+p4BIxK9HbzUa4PVPgMDsv1YPr/SbAZVKpo8YsCqAi9JSS7D6Qr
vC32N4YvJs8iJ030taXDXG2VhzfFG+u8nqpWR08WqRqXoA0+pKSKA+FZI/JsI9OmBULdgEnd9QnI
fDv8YPqKdX5udY46f+tXAD5aohcPV81IOKsQk6WGUPUvnWyjKXLDGE8DE4cj+FtJPH+K2+m56JVU
D2ATi67NAKUGMDrN3peRcSdOP7XOFBo53dANvd1/KVLISAzhoWlDJsD/r3vQ08P5H/PlxSCgWz3h
qcGzAPszOogFc69P2189k1aExmGZLJQZ1vk3PgOXTnTrwLnMTD6LGu8C6Ee15RYJdR+WhX+H25Sx
aFn9FC9PUrLDG64PYDr1hAtu6Rhp7vr1t9ENCUB76lEzvEkxtXq28x+mc0CerJCugRWAZ0VJlgq6
ZuRuWffEhUc2m21Sb1BQ/3MobAWjZh1P/wmHcyoExuD+UJarpryC1Onj23ya+MxNaGm0JFvat0Gn
UtL9MVC7+1tFsNRcnVmJuKDX1qidYYnvcc3xg2OJoAtz501mtb9Wl8zrnQ1zK77MkFvQougDQgH0
P/oDQswWPknsSY3ECXT2lMUo896DzEtAfpijAlZqdrkSg5eG6SfFgxsZmnUxdiQsWyhv/IaYo4/6
bGWaNuGD2fFYfQfogiZg0xqx1tHneMid6yGTk7Cy8Xyn/NfDJD4KZ9s+XgzoP1g35IRH1zzQSL9q
bYdTCDt+BP4mhbZcn/0+noWXNpJXYt8wIuFRVYIP51dc0/erDrOXejQvzXwp/uOZq7HPFlAHdFuh
rpJbsxo5ETw8URiKhSJ5pAgdyoZCvzdtCvneoZAcZo5NkE/tiZPTE5kuj++LKE1Nn9vPmFqPl8jq
RRJLVDsa1BnAYauI3p8QJm9UnfuNe2AYtTOM0KMl+hE32pMeC655Uz68bpk82Hx94t5ipZhQ7tHe
EBxJEEv88eUd4ECslngCD174TbNJNPF25OW52tnv9gpizO2ggA8QDyVLj9OanVRiYa5E0mLGhc2+
tHfizpSogOHzIbsQH9Xp0RC1XXRBJczxGekp/jjQdrgXhzBe6U+GqjzoI807niM6TA8AFT9D/KhN
Tx8ZCyPhCq3O6U7FgULkYlH+wgeFe0aKLid4rcjfyVmj+xkULC6yQHuIf03kclpF3ZdBJh+veLjp
v/yWWFVee1f25KiEwc30o2l02O03wNJVcoUenBXV9kcTc9N1Qi6GqBhPmyxOnNd/Kv0vxnrkjDTe
Yi8FHG3b57Igr3QWZms6PB5IJApXdWYuDoiZoG5xIXSvyY9MqcKEvBVnqTx63cLMHYjQhlvboZUV
kRJD/UPA7aIM0T+1AzTta/wY5Dql+QYpgdxT8x8Zqz4lwdjxa4IvlibBTp6hgSfry63nmLHf3Xl3
bB7BNOal7OXAc0p+ghCipicpniA2t8J7xNdgFot9MjcJAx/DyNQailHkPhQ/tcR81KyoF7w5Ilnr
TotUXH/B7BA+q0ivbu+QCYnhLFVgiFpCVGxF4DsAs+xGiRIZ1dvpRqYQUZkvaUhWzR4a1FWw2ML/
mjuT9ni4Xd4VZnd84ab16mNcWPX9aL++JzZxJeh9ALYfid4ZJ+zlyXtYZ0PFsSsSaea7wtdlLbSg
FsImi50sWSQ0tLyazO7gJ9+kuMi8HCd1/wr2WkgiVZDbmI/4kNZ1CvcH7vu50YIdS3vvyMLxjdgA
GaZXW/zOHselEPMt5SfMZN5su23RtFiYOE4dK0TbLcllN8aKUTN1p3m1dz37JuM+mmbH0yWyH/Dz
6qPe/A4xxgbEf96J2uuTS0HRpWGsChXUCs/H6X5k61lHWPpCz4/YbW+4crU4fIAB80LPfFp2MgJQ
qKEBFOfIP8KuTnshouBSWY5xQBleWxx9iDCFrDiuHEB6zK+8qrGH0VSyRDnBDz51i7TGXMLUeYAx
bowKjZ9unf91zrB1c8TdTxIYNfU96nY7pYRdvIrfCQNiWqrc6DMCWzWXdqKTbG9ee18V7QJuNVLO
KdXL47P8LeZLICRgK7BW4iA2Nhxdr7ChFbsBQLZYlCTIwCGaqXqtblHP7q+qIx/cY8UgQyM/ZdEh
v/3YjBgSEDF1AlZr9Tb4Qb8A+kM52DTx5bVqDIrOQfU3haOPB69JVSLGYH1bre7K/iiLVTSCjfE0
nu09RAz5jbaqElXADH34rBjBNW3QBKoVvn+oU1ZvVRCQtT9J1/0XELatXP+4oreO3wm989OiXnYn
4WVDyKVtEYIh3odvbac6ytirFX6xkXK340x29qmhU0zdrsMTy70fVwcPvX2zOYLeuBYBKFIJ0e3q
pktP8FQP7O7/9qWpX9myRKxH/oFM86LGj3lQZHFdEKfnNah1gws/bUZB+2vNPVVpU99ZwNBhn7pN
ccVqsCiPb5cR438p6ljRf73lh3vNOIXsqjbo3ae0sFWJp2c3JEN3kRqxZ/8ljWHKP/OuEZ4IaOXJ
s/u1588FkCvWjmkL+xFHbvpju9db39kwHjU8RgpBO+/FpZPmgQ3iB4LlJUlCyYedZK25JR+zVmm7
Or/rnxrZPj/rNNl5NZJhLPkkJUYEDmYYCKcDAZ2S2qggnYyfoL22EqfhvRv6UKxp0AtZi6GDbA3T
cqyLp2wL1EFfOv2WDHa8DKsveP+iwVbKZD+ru37Coq3Nl/wm0bvPBEF5+hHsbE0VM7RYW0dmGNwc
hoQCO7rcCjvPR+q9tnSXaAXBY2NX7v50K8M6FLDleYCcS1uB3gtqXYzZkwfScg7PGxQzBtxofO6E
PjMjDEAHpftMV76l6v13SCxr3J41GtLRRf5lSRIrGvQLAdpxeJA0EPrRVbQckpbLwU3Tg99cVs7k
8pz/zkq89kR5dv4epGEiT8oLlgL0xzdXBDw5hp7+IDiFAm2HFPzTYmqUAi6GB85jQMHTTAmba5G7
b2A8Q7mtR86pAlHH/N/YkWk4OniNUcx6/KR5bCIsz650CJRrcML/QY7rlH1tjEU7yXbEYCyB85tQ
ZUxEdk6Dv+JhWO5gt6W6YBQCsWGuKNEnmgnCYvZ+oe8Wc21BxfEiNxy7cquB2glQYiKOSiR+flb2
c4t/sh7dUtePomGDrYF+mXS5ySLtQXER6RmJK74CQVaE+7hQfD2hvXwyvfTGHXi+o6gW1xybmMzN
L5bT7j9wTbd11EJD0cR41TOGmdliPk1dXSAP80bWag6oDUXsTH50PiEic2UGRIqhwSBiczrccRop
mAvCbjIHHuMFBjM692ubD3fYHuwW4tX6VVkRwAqhInRXXbdnLLXsCbPUYH7Wg3sQlE0DkItVuQFI
FJkp6LWCLnurZZuleyLvX9hb7zgnRtV3nuzqWpjFtIZnqNG3MmbEahfYsn24QVabAXZ97TmZmC69
QvLtBflWmlDjBlklX/vC6wVeKkszu0Ya844Q8uifOxD1MtJNpJRIIzBBWWwFC5m2qUZp32QGAkWL
CescFnaYKarhM34FAu5HanWuzBgApG6u/EDjQaSv+DxpHFThNezlWwSWTqD9oVhc0Y8dA+JtfhHi
SFkPrSSn7Bkhujw4IQsmHfDRaXNVlw959j2Xj/Lg/QwgSZgfs6p0y36ovAkbF4HalKzE07VFb2Bd
VmvY9Qu4CZST5trW8NVxAOYBxB8yzmM8QQbAhK9TpNjvA5rX9Nvpm741VnnxUHWxCOMJ0PNFB0Dn
7lozVAr6X6ZG5HTAAfFrrJgqKNIXnNId2ANtuaHsCxyFlSmNTl/6A5rQR8UwI6aFC+R65VuVGy6k
fWCyw9huxHBKB1lMoqz7m2D5hsYxZN7CTsevqad+k1z/gUcWj3C3WdT0dvc509N4w58ypZ+kP/nq
BXSyZTCiHbWRzJYMbfE8pFuqbSYHIDUMINHcwJ4I4aqKmpLDr8xZx2qKEQL4zB9Hgm+7vyqZSYEi
5CsAO633dmYZEA/yNc+7JiQO6t3CNFDBab6flUOhmxT77xbRRov/AGbAiTQVWSfhW2rMRkaYxRR6
8qXEeGNCE8hwimmDSpEe0HTF2SMYkHs/WkA4zwBm8ph4WrWxLbKk48prlSWkYemufq+dZVKeYkoJ
jeaR9f2191QzwrWdj2+TgB5z8IY4Nd9DNoQTwsBE1skQGBB09lETaA8mj1b6GeYR4QPwbJF8vEBk
b0XDovb8q5WU2gV7MbqRrGu5zdlOoJnHmTg28j8CPPJ44t7SjNrCsCg0sXNkUZYf6pIIQ5IECwsj
I9+8io81qvStlu5m/Er6Agz4ZLNk9Z3mDm1y6mvt1d4jVpdD9hE2Y3NlxOgzPfmpP34eVWb4M+Oj
KVH10WIxn8D1HaYD24LovqrMfhXrwZwaqR5lybZfP5gToeyHBrpAAPVpRC5jKJrQuO54k/VfxcDO
pEJXiEjZfkwcMNlFGpmX84OZJ2Jynp7ab1thMcer3+rChAfgOptHgxTY2j/e/qdgC0IvP82SJTfd
8P3qz7dqFYJuMbRJPkEEowboyO6X5BW8v2Ssq6bi9xJq1OJnloQiPC31nxVnnOx0GVl+G2yAf9I1
6Pzj6yxF3Iu1gNN15kc4CB/r6lnjnSuXO8vlbxHE2eR21haA117IPGQy9V+DhmwuBmcvwFF9osLz
irJFGRp1j3VfvIbmoZwuGEmpQAL10po6wj1ddyupeNNpy1pf1YQV+XH7T36AQFTR5AP64cARuwXs
MF+c3s8z0QmuEQ459ruOYSlAFnKxVjU/qopSHIswoXu9rK5Pqp00lcQnYRBMHpnkd78FdBGfBKaJ
pjICewP0wiFXCVnvkqlqq1raUP8SsiZ4uzvQ06xNMXV0AT34mozgBE3LQiyRpJ+p+zb8CJjWVUZk
Hc5HxCp32a22IVyq35Lk3Tl6xelIQ7ULIm9U1i1qnFl8F/QxKHpgQIZlXAeBWKRPaq7IoA9gHE6+
g2j1QLYhFo27nmEdUuRfUNCLm4SpEsez97tjB6ovOseyN0wAWMmwGJHfc6wXBs8ivs0uEctfmuUR
mfQ0ZYBRZLOVGkonCJP24868kRTuzYW/0Pc0AooDLkisuCibrmXVWBoF8Kq44UaUgri1ox9ZPCDl
UPLA3dkL8iDQqmu7Fs+mOCg+wwDfMH/0e7eGUTbaZFuvbnVp15nVlySmTjb9CpsDny8G6uto64l8
zqtrkXDx6cyjSOb2SiyGqGb/GuRE/TzR4zFK+98gq6xmZSAtNkF5T0RBZLyxQysAIWW/RzKvbVJ4
SLEo3fKHV4anZ56VdaFX7ntqE9wRTAEvzRAajuP8/dbrQIYi6zXh6mAJ4p1DHG4DuTaf+ayZNYa7
qeVzuJh8sgM8rVNUX8eL9wfxyjESOYveThDZEd2IOXto+eMu2fjx6j5ASpAc0a2lRXJcavMtItA9
7IUPIYd1YLjMp+jKgvomoPw2nUyqw89WPJ5YAhVEjQ7YLMMsMLFEjf6ZBGJHZpN1TNG3vdDNhflF
+mjAxyYM9fBi40k7mwwuQUozHIqZAFSlPGSs2MbBtcgc5HHbCzPRSate/D7fQuwnaHMNMZ0abd0M
rysa5yuDaakwHN5/7uD8CAQStOY68B5LYN9SRYRLFiQzK+4+sXMeqaymyyWDJm1JKGimTuum++h6
2cL91UNiDoPWaL75Esi/XESFrhI90NHXH+rHCANIJNIbUzXffYupuNm55JrTCmgqc60Q9MOhb70W
Uto6+0ePhC2fYZrkGVmMz9HIJxfwCWqgU1rmWOHVwYdi8tOJlPWSWbtW9QWXwiDGZnprgZgZ0LZ7
EHQAZs+OwijoMfZOR5Nqb0CCsRyYyAmI3o3jymAi3mYJab8lev25jReToz5Ul5xGr07nreZW+L5U
9Fz6nDNlrls1l81Hkm4Nlneep5EbL5BLUu1EVkHgiH88J93TTT9XwB125Hd3a3Xovrefujw+3lqk
NvfmzoG7hAy7cVPbmlImLph6dqshiTesUXkunue+xbhyq8S2pqQg4PxbW0l/N+Evw52HiPopESIT
ANdhqCfEfv4VNe9SU67B5RBZtCNiUBwntymQFTeUZyPhEVph7zd817dJwi2Cwu5jbaDJgxkgIo2t
WYAh95Sb9xrxP9NKjtAeT61BLdWi3KS3GibkXZ0McA0cuTmgPDbKhe8xrqYO2HKhnYv2sN1nadSO
2gjUUXN/37Ze863N8BjADy5b3aEOAhd+tD3knwyB/q/QhWaamc0i0FZpVuxjFake79K5QTHQYjjR
B09C0lu6b8WIuB12UCbPdvhGsITuyXV15lZpj1Y5OTg0NVlVq0Z28oyeCYXo4jW337qR4qxd250W
nBqUJCv6NWGNTTRG3uYnzWSsj2Lu1H4HIXPas9jHaKySyZNtXg0zs9YvNOYlI0IEkBklgDxKnEpm
G5z6mNUsaxRe7DPqqAXD+L0TVWInEBB2N8kizCvK3xoL48VQ0K1H0B60NcvqK2lVgD4IiEST8YTs
d/7DhPq7EvJ8JU3Iq5UhnzwND0nFSnyJK6EYOS10lEeFh6neO3LznqB/MEBjOEhBWs/RR0SyJIpL
i8zWT/7B4lB1dsygr/mPwscTdjNkvQTnfjue/Tgj5Oodea5lcMPwYzfRWARf32epoB5kvRvFZRXL
PTTI4CAMpWkmd71OU7afelV0O0kjg29dO4wyfTKLKoyqmfk6D8SYZbajSimGLGp2UMnF7F6oeHsf
JiYVV9hVKX+1G/povzEtTP6BuyEJOxt9JpTEujPjjaMw6OaV+3HYVXlrUE/ZyoZNtiMfLwAZC0VC
u8v3HbGKeI082Airgw066fyfT1VUot5Qj8JgVooS3ffxVbthIKpP5Ori87iOuKDH8ETB76X1yJt1
1h23YQ5kvOuRI0b1Le2MGcWe+B8iE0xipts+z/pe7ItmfAMO7HAKNULBuNPd7oeE7PEAtdH0TdPp
0+ZN7kw7YqisbxLEBapA2dCsTnYA2QrNKbVOBZPnORerQOH/93E931k3CoW6I1CW3tU/SleiTHEl
qMpa8K8EVzonJ3D0qDDwzBuWcluagNN8YfnribkqdnZnMRaO7KN3fH3Yte54NSCtFeJMqzw4RX8l
4EYQAkgo44KnmtJEsYhW9IyU+9KM8YN23veea3TNyufaRSkl5ni6LOJXYLMW6P+EfID9/Q5wLdOU
syWGhCfxOSJRua8SF3/HFfjYWyXgDjpyb6GSaBWpk7wYSu1UuQdOCncL38UpDyKe3gT0FkPMaHwv
VZcAgvJt016w4GDFM1ZsPQl0txYSy9V47rlK+ZoG1UKn97MZAzFSqDw3Rcj1xD/gG/gZfHNQCiMx
2FNMmrf1k/u/Gf2I8W4mnecHyhlnM3bxBlra6AGvBkGrsxadVNBB2vPzQGOPE3H8VyZA0pbHucVD
i7IcCFeJtC2J/zNOD0sNFKqo8fZWL7K72mcMH0d8P02exBh7MluRql1WIPj+ylZUk4BXSsP0K3kJ
niqBzJ2DGBW4rmNoAjNAPHqhs0utNhIhfIp38KSxjIv9L8sXibRlI5ZepsiVRtQ9St+9DyKE4+eA
GxThHFP1vQnsF8dhLK8uQ/CyK7M2Ci7f5CAxlsJCvZkY2AWj2fNdOzKfkSK3r6HBiSqJcpnm6SYW
u2EDZeTxa7WYl1yUpP3TInplhZuXHURaMdAfoe+dDS+VVAZkkcCGSspFNHyAXqMFP4sr6oXy0kJH
c7AfbynAVsQiIqZ0jhlJ3Ic7bi4mvbV9Bxjwug5vn8/W6DeFk5okkbaqKAYwerz9ui7NUllicvbr
KYe9U1VXBdirhAGxzhVlMNU3+4/f/7r0iBOQxLXHf6pxR0VWrUsO4juGRXg0fhwQgRDg8JpX56M2
zXkxZ3iJwgarVEu9tkBHME0GSyO4xH9ZDl0Ns2bg5NF9Eu4iy1rw12qItKuRfGxHhDSW/jLefOF+
9Fja0tzFQ1ilAUmFS0x6eB4Hs6jutxK+Dhco7duuYsrht6brmOLn6vaF8dyKAUwQUl4K8e8WBmCo
/IDofz8hm+ww12xd6jT/N73ZWKVJb5SqGPvmgeDnJqYkuUzDw8SFn858QujdokFQJq024Qv8a+aG
+KwDc0ZZQ5TSJ+8gVebjJ5cDH38Gg+HlpI+GnJqAZ8hWIttxPwtOyuThEfYhvk5oYignUtZQfUGr
vjdc7isSwZvWG+5xyYEP7HXtImqGPX9U3hpNS9ptDWxvNmojvX2fGTNRtBiJ1HSGhxwrIBR8XGgN
7KPL9kFQ1hJOHHr8r2SNV6fkE80jWTeRdr3zbAff1p6L+kjRg9TSEKaWVHf5OPdKxAXNug/yh4ET
CTlQgL+uZNarDEazitHqvlxesGgfYfCAKkljl+/TFZEAgWK2NLc9l9sb7G5u0GD9f4X6jZ7BjPPp
ZQDqXzPE6Ro4S5u5H2Ksu/w2YhK7jB5ocgYS1dnItKYnuJuflakVqjyLmyBawKdnSgMHfT6ozotC
3RJLPiksEFEV1y2K4oRBfOUtycryLjCGmR6DjkLBx/d3CzKT9koXlWKkBUql2xalY5/Y80rVCs8n
us10Tl2dmzsnzjmgB1TJFaEcpDt/6eV+Sya9rBtrhASA5UlZS5h8RgNz+vEj4FEb8+JIhQ46N0Sa
+4/YHkbnkZJctUFOwHkEe3XuNK4xaw+uqDJ+E5tZ2yq0/7S+VNFDZG1uGGlOsc7TyWuz7BR3KCQW
bASD4JyYr+wlcbd4dw67q/jCGYTnC1gANaxm4buVjoFr638+S175GtS72N+kJBS/FruV7nqz4Pz1
Llo9aFJ43caCcTOWX5UfM1a/5XQW4yIAtosXndqNkPn8+YIgGQxuFJk32HlaeN6Ttv7HkNgBRuGC
uwZR/tWKcuWKLSuh1pJiEBprFZgfmFgAmNmBx2D/LwSPQQ0LFacpL0kxK/8sotxDq2AqwfR5R1AY
4FVLOkvsnVtphBVYwMv55Ht4k0X8lS+OGF/s6KO3c4jPNAW0YqCmYW2wlx1PA08vjSzCFzz0xEpZ
P2AvOHrZhe+OWehGEbNEKsjSoBy1MDzne+5DwnIY7ENKVwKP9st593uuGd0/L5NIHWDoKqc2jmPp
5NwkitxYunshh0YJtSIbjRmNtlCNUvQXuj1vqgSQ/LzLs7aHgNVYZFwGO9yw9upRifvhkawpySxc
9NaKoz2qI8qTaa3fLxoGkNKr95F8wzy95F0Az6i0NncKlk2XkGKaWsqgrSQJUq0inhxGIFqWw8xA
h1XRHJJAn4STdYRx+Z0sjp3SNy/5pwCg2tD0oLbZaD7xpAeHhWvm4uUnoXy7NH0Mow163K9LbjlA
Rixrp6dSbFhY0pGuk1PUaCKoTXGKHQYzbpRS9RCDA6ICc2/38mQbggTEu5wPA94t7j+575/0M5uX
YP1ZbYE6lnj+l7vxcqt9LD0vJ+t8BVMP9yr9JnvhLbree6gEGVCzjS0HUdLujVUKlAGOEoxBctDV
DxVh8w041/XyPGw1jJtA8E/dh/TabFGtneKzTAT6X0EP/IAb+VdQqd5c7uDcrKfsD+H/QssYZdAk
2FpB97p/v0PiCXjg5kEjWdvc4o1Ug5xaWVoKBftM3jJKCcKb0nHRJkMXR8xIvMgp5wVWy5XaDwYt
8jXgaoClil5mhBAh2eUhLTtZsz9gk3GYHDej90hzGjFGBWpKgxPTr6l43EMSIe62Rs4qPhbAPg0U
Nw9Mdu2dOWdUxRBbQqaqqxS+trt20Q8srPZZA7OS/6FGI4MWoDR1hzjHUHq0y94K68Jfu5E0fKjJ
KA57k0sYUPIBs7lXAk/zgRmR0hQ1buuY2E2jlRfPwN1mQJvAORp8llgp76Zr6bPGELOGPbI4/jta
IAM2jxuseFJEUIP6aB+q9Ewgl8FqylWQsa9XCJJuJySxKH/EpTfe+lYBeFUjGl3+GeHwpWEo7+4r
Suq+j3Ezi6SYA0kIQfCcieKMmcF/7B5GOG0yxOqhVHB95g4+qwW8K8yeNrgt78sWj4kHqHW5v/Dk
2dvbdodWYR9cZFNRXeiloVuihhMtIL97J6T8Xg20oMc9/kaeVDlkZ32o9p+Hrw3O0074MkEyIbja
CqR9I1aTr1u1jWiWBOUdnE5BbQH6lgCq58HfAgwQTALTu7Y+bhRx4Pze+JW2uXUOB97KeCuPvqpP
ep5Spv4Iq7FrZ1UFHx2U05rT7E2W0zCO9GI7JBLfL5gxSSGl8ufpfYxmxtOy3q3jRzuZsg196B3A
6qRrxWdRa/7triHA+IAm5Z0GWCr5dAP9jZPCzJlC+LGwUpDXp3WMZ33eVpfavN876IeY7C0x7isX
18xixYQs2tSxjsqYyBoE0f8UVSlmJAtCkcLGSNTJF2kL4gBqJBFH6jBCwZplQHgyvIhpVIoIM/9k
Fx31cUtu373bZ6ZsFfyquVv8xpvLC/Wjqi10D3Fir8zxMkQjGpBIkowzvyAplV89dViJxpI9hkcP
otjAkkHibHfl5jYW2djgnf3hEGWbVJwtoiP52Zma7xVMoHlOyRHV/guxLnhZ8mOIMnrkTKp7xh9I
E/UQOYlKqrbLPm0HG+81ZOBX+DKQo2e6568NW1HJuGuJ2Ys3fsAAw5zK9FN+bfjpXUJlJJH4OAgL
Y0BuOa+l9xJpmh5PHiidMe3G1ha3yI701XhE5IjPQbnn5IwK/o+Ulm5JI1BrFGnUlafWBxgrenV9
WY5NEsUKwvA1wNyKAzRmseUGtRA/Xzhq++1KnNMRMWFgpcZFAJAwY1zb2hSg8ea6tzBPynNQz3o9
Sa3YS10kfSvIdB3seidyzHc6OqKzA13aw/WdSqV6S+vhiaUh4DAxhIBHJQBqqROZmmI/7WcmBxh1
onmuY1r4+TIzqNxxHmsb+fXENcFxUaDmvwy116R73Mo+ErIeeJwSUh/w6xwcNdSGuT3tqD7O/g3y
Gdl0IBCZZZkOV4xZ/7Zrk2pYcxFANHWsheqWH3Q8KwvPLiVYQBtokoNRNkDy2ARYHYxxg0bQAQl9
At98b36qXGKsO4em9oq9uaGKbcRUpx+eQByV4UvwyWptW4pAV6D2MYep7tpFMvdQN3qSU8QZJ6Ii
1K64kZwg10If8ykobDXgl0v44ncuLMarUgyPMhPF63R76yllj8ccWEi6UoN+a1O5+ezPQ6j0kjvu
iBC+WVNsFFGQuNUMVxvUf6hMCmEZ8w2Br2C1kCLyJFlRd87DoEtYTGtvjtnmbaosvcIAgQpL97kL
rp3xsEIFMOm1HxujX1bxdlyRrg1NnOISZdYw8XFHwjd3g8Ty+gdrkt/s3Bay7eKt2dNJqyOSBz4X
tOtbE9necHiAczGIPnF2jWf9ijS0vyJd+9lDWd6Hlkzm+vK8IJpHX33Tsn/+GvWMC7E35tM+XPAl
/s9U56kfZmyciVTRTJk5QNuf6QhcNu4BnWqaq2ndqUAhK0HixDhS1DnJcwq44ucJeDIVGQP1bww3
aXGuRd+gI2E/tEBg/WPlNjsl2KFvqKUV6CzKoxiVRqJY/bHlw97ls78ov8/jmU7KhDGmVMxxNy/3
A3wdEornKtIynq98D5hNmYKDRC8pxhodL5V/hL8duwLW4sSfG8vViha33WYaT+GxuoM5rBIN9+2y
5gWZHJmndy4ewGb8McqSFmM+yNZMSAP6AfOxQVTV2zFXq/cczLNWO4JdxDY7iWCYxPPYXBJU+6TW
Lbq0zpxd4qzwoSX/8khrwix2EIWDRTcpjgoMi5+lDJcns+0Cw+VzJ1JEWP5r0Jz6uVRi89GpQQM0
GbKwNRizAvw9sTOw1qoUWa8Tez9PNhYmYyLSNNB/YgHLrWrkqd/R1clbxqdFVZDkuBmQiETHnbS6
Q9TPgcsiTfO2D68GnKuHE8gm407S+si/qx5PFDIZUjBKWuGld7JSoiZYANlZReRA6m70af5ouR6r
kfzUV+RKaik+1vis4S99FpnHrcGcTU1joajWMi/8EIhZgPxQXCqeD2hKclJ2siermf+oTNDc6QBP
SS+FVfF069UR08qgSB5/DKvvjVZ3gAA5st62XP/PUP3KR2Ab5kXnrK0mQoZNKdnK2tOveHb6TQxs
WUggcdZ2PFhfJVWdZ2jDMYfW1z1+EJqle7MY0zhNH8y8kRi//5AN2EW99UXfSGu0fIdq+M1M3zoQ
pPvugSFMGjESmp4Sp3xLkXDh6qAn+53RhSOgsSmLSWT3STlciLeABPkGupiTJ2zYtJlZQcx78e6B
+hgZxs5I2BrIG3D5pT6vouOa7X+eTPXJEA3f+2NthBP0x8mOFvyLFqH8xYmidMeoUmWWWMhcfac+
2k8pE/RjjVt7TzLmrSXc8q8CexukI7Ln83dt8dK9XzVpgXTSdJ/3hpa5fIDpeG2OCHL+B9ekWDWQ
d6dhFCKuQgdOXqNH/ZL1yMmwCWM/KOuxLxALXZaMDOEwsq6xYyIpCPoSqHDAf5HMjupN0GWcYlBk
BzDn3kqK+2M+Zk/yJ1IHWja2Gvem/WOlx1MhAHOlg+UTuHGG75msLGRCguhO++lGo+lUWHVjb0W/
Zy4oSVuD3mGaSySs6uNK1mMOFUxLAsdnFBzgp9ZnlhIjyEKnd0qv2EaXVQJyomx/sY+WSHGA5iXI
DxBczwAGbmb13QNbQwCzaanRnYEdyODEWC2QPSP3P2aV7lzEEkq8CzmcvLGQjPtyNyvClRvtWJk8
E4nPGfXAcTZCbOLtrgQPckO3i8uvIG8jy9peK3zBpIM9qoGl40Kx/45rzng+d4pqLVYGzlKOyt3z
/AZ9LCCqdjqQdgkG953OdKGsYbpDPYN5N/o+olLYnxb0zush8SulDLCy/66Ogv+nexYGeLC4wAxS
fuJPJC49KgW/PaHIW5jzmXA8HWfTtXKYOgm1x2bjM0bIwXGiwBg/PelfE9j0l+vj6W4AaNYNCfQD
0D5Cw1hffhTkN3EjBAOgNTushgLvOXf3mnW0V7nxS64AMHrxsAiPbYSYRa0TMm60946mNQOcnne8
UptFi8BMw34VSYWN+xu7d1SPfcrkx0/+9lTbT4VXNRALdqDdMhz1TbWOXpkf62eestjXfPP88Hi3
N+y1PzJvjDFb+xCvYbBi7xH+2+TjcP9PM6KDOsB4L94JPMUqIGzcdFnXUMSsl0Y3xFzg8g4bNmV4
s7gZGnvmQVqAvodlI+6/cV+iJIsEJ1/VpXmkfBVTOiYSpJh2kc4efBc3TBPtbpVh8tvoAPwuqNzY
uNV1Cm0qaKt6+7J0/dNa2SPX0xm/R/4m1dhICZGwEQNt9rfdAVMVQM9Ti9jG9rKR4JyZKeiVr2z2
IAZMCgQy4cTTPcPzw/BspH991ASD+RI+rMpKa+c7c6rEL/2r2OicGHr58jJ8vqVl0aZlvVv6fav8
/oEAo+hnpBd8rKuqSNMilhN/tiD7s1zSuzFxowIbGBvHekla4Roz6PuGSLkayviANP/kj9Z2RhVg
KjeiypWvdgHFcPWKiRSKzPFLauodY2wTieR8+3Jm+d71XYdcLXehctd+UsveGlfWgWMg/CASYsJv
NLG1Uozt4FnIFB8wiQ4IfiinZQhQoFYiBlNDHvmoupD3HUWdv9nF2HBIvKOERE+dwJG4bXPrhFsH
SbH2z77S4gF88tvM+EbFykWa0yfqWIz5LLFhYy2mpyQs0tnYAc/JAtLlOGn6SMm5dgGYA2O7pfAE
JqXlO44hjbpIW7bgsV+TQCmjghbYaND6ajxDUzJAtFvBPyVsDYlRkBy3juFx6FqzZfOfdr9ppQfL
iAjHKu41/NkoMS35CAKVih2LAln+tBUhVskOJwHwIp+ceTNXxTYQ27puu8svbhK75nG1AYMBSclV
+YVeVUpJRcHcvbZIz7t7Qba4pJ1ZVVs9syXwtgCwrA29rhaE3snjDbcaYG+FftV+GJM+QlEC3JCf
W/Aga0Eu9UUKsV3i27IxXrRJOGqQcFR4frctBC0nM19CTGK67/yakAjdGI94Z2cTYftcDsLAYALP
DUPccQsuH+ZJ3X01REO5g1kXsaj2IQh1NMiyYITeezFkwuZht7Le5xz8vaSjAy12gr7QYqElQ0Er
LsVuzU5oPFDjancBMXC5d/hk+GzeVmk46sngg/UrkaUzsUqF6my2Rh9P9G2oONZv+sYNw688AQWx
q5CnNPKJFFHCIJaJ85nvfkEmW9F9fdap+daiKHFkUlZ/A32r4r3MqO4aY7Jkf5HPG6joT7pNhT/M
6eYqIRkxe36bpjEAGN1+CS1jkuil4yUFVVslIQrpokIzIhJ/2VxZWG9lgCclo9lX7/PBgrNGdAFf
Rpd7xsIohAg3fAJIYB2RqZ/Hu+A6+SHCF0J9jNkozuU/8GK6k+4pZyfi62xBQgJ5ATzOh/hJ8oEA
K7p7JQccbGcgbdrKgDT2G0ba0EQwBRoErDOS6Lf6aod1LE/vcFX7PM1I9TB2kcbSe1M2a5bA4n/x
Zjj1Y43vSHlaTGT5FV37ap15p57qIuPVRo9f+TCvnb95DUbpBqq0jatBBWFRbfhd2cVmmOTiVRk5
nEFzQ8WaojQyXJE+B07CNp04lyTJRuqGFZIsMhcRvSE3le6T2HlcJC2ghBHfiKxkvEictOs8myJb
+jJaCPi2CCdKTMJxa8nevxJXy8BempcMY/zKfUNZY52eFUN+Kd58bLDW4QJxvqFVNGfmaFGZnpbk
1GXNNP2E6cYFuuzrxVBEMVyIff4HAu+ax2jK5a1YfVvy5RpipcNsGFnD2SHVnQ0CD5CoA12Uei/T
MWHFXHUYCrsAFmqVfUv0hZSjfxOP4HPs1x+4C5rcfAYjMzCqJ0NrItk6xP2rY/ECYNyWZ1yNFjb/
VAPT13lbH1i3qM7BCdEZ1BsGuUTCgSUfzIHs4qxxPlDKLBzR/PkvJQC4JlwL2toI3YUusM8HrkV+
HuRYThNjGzdE212ZV/AovpwYEVUdG4X/bbxMVH2s/79eJCZjuKNyJE+S+HFrq8E2CHWBGyT7J3nL
hu5I/AzJSTt/888IEC7zqzYtVy+25Z3VQVPZD3lYfssMLzs08VCloDvbBsTuXG9X8lQQlZ+NdkXs
HxbT3WSp1rgnNJmzY8Jd1YeuMDLx7jkfN+KzsT9Ao/VYPk1paYb68bqePP7h1jfdqtLWqWhvUpKX
RxJrMhZT+hDGLyTJltri8bq57SQvl7sU4ZBGXJbno2UAdHxFITx4TxqRiu8kstFB2ZoXSzj/p1DK
FGrzPs+JiCU0aTxakVVcROqXfSGOtEgqQUjauwJaibIR4Ijerq+6jegrL/IbrSon0gGJe9H7w3st
19uDlVgCc0/6MIiNQ8ICzFatztAaiLXnU4H8v7y5gdYs7e5seuBXBcQntqn4QFrcl45fj45OkCSd
vq2yDIAC9lpL1uphAYeU8c7oBMDYXr24rf00n268U0t/Ifq/NbhLw+FhBxRyw6lC4XdDmOUGcaeT
MHIz+1CB4LZHXDOH/efsdwVu36EVjKbRH3b6t22eRpAUlboIPA7Parbmzm0626uN/rV1CmDe4csB
Ym/qPVpNYGCAiah4LJdA8fWfRnYxqWBDV3MSuL1iklPnVwh6mHu64vhHQt1Dl7tF2GAKosuisZxk
/kvWrfzCZXRz9vsLESwEMGfrUgLZsDWbA2SEqKEB5ud5v/dxyok5T3RXpUcLp2jqMT/21ZWvBPuk
2X5tDPLq2mj6fHSbNnpVQq1cQrb/G153GB5RdE8ooccLL8AZTqap+04i1DqMiNd5TG7ZOiu1yjCj
OKr1XdhRyRBSVmkA9041YD6Q+D282+LwjdK3LjP++oEwpo++eSI+PgJN4EHbkufTmejG1gFLmD5r
qWrTXPJ9URO8UJqvV4g1JpEOL0XGbdbAnQSTSyhEf8piIKrkY0qhhjaWV6IbxTCyC+5kjqGZ4RQL
OQ8f+kGdhAYMrK6HdDsjO8sY0rvNy7/H0m3SjMuxqnGvE0f8gUDiuUgWEU33yNOaPbWJiTCHBhEs
t4UB9J+AsRYqIpyvkm8hGf8p9P1yVJ3hQBf7FOjVnWC1lFBrPDTr4KX6H2Wl+XlIMwdo6ZIG0LyM
8/6D99npEuoAtfuFARyXFdkRvi/+oYqXltnoAETiDsDEvLUld7m+IzjnPBYIHAeeyjwSVxWEVzDI
CGSySymN+AAlSQTu6OxlHkDW/wopOkig9DSeN1rCHhW3Q+ZZCEYji5pL4o5XZ9pNGsLAUz3Dwe3g
e1QbrAE6m2VW0D0BsRSmANflbyX+Kv5hr/PvMnWJZhtT4odCBANoQwVMqaG7S7IetlwNcwGVYaHb
oCHy3zSj6whoNXmpSP0lAs22KvsqvLhsGMgthqwhv7ombNEvq/X0Ud510Q292E8u1k69rXfP3jNq
AUywXaoqGPO5tQRCvn1cahuy1Bm7x6HdqX+cGPGQXHGEyBkWXyfFaXxrZJjjxOJi4iSJf+zzfKvx
Yx86XLaPlAgW9pUQhlLhyiUSTPV/wl8zQXC7by7AMWYPQGJ16bfy4A1/gaXAsy8ilx7B9NKRmIkV
Nqe9iE7Fp5QYQ5Q0i6q+CtFZ5BcwXkHQHGtYGyNsRvm6XWiPk4Lw1DAcHNZnqHuW3pzItgwzgofV
o7G9h/8lt7lQDuh+m6XDJJeMOaB2iTnGHb3Xc26/dYQixxQmBUvYjAZXYtbi2Sr+z3FMKKNyY2ln
Luw+UHI01CDSMEdwRi7E+8csISwCp2rSchItouj+OvtZDX3lvBdZC1A19shNRF+sirx4xTpfl5ZA
ip5i3oCVOv5mXkW8DPWgFYhoTdd9YuXFqTkDPIS6+T09e+tUPgk5kWN15mfYL/pWhAprRyixOnYQ
JznSEjEc+51cc4pglaqEPpVbk86P+NwYqFNr4yzUjK2kP4uvGLkC0mLkGkGzwc34v/ezM6ukeM7p
FoQxV2JjnLqrVEcMQMGeq78PpDIIIgGNFJwMzk2L96XJxl01RK3osQjRl4pa4CVf6ZEgi77gsrGh
7KROf6a34hsyjiNxDNhMuEl0057BGVdMZ0T6n69I6d2KBCA0WPHIipIFWVY32rJqCiQ7R5hjNaOr
NtnEggmr5jxO+O3dBswM+Od810WCCh3UdS5InfV0pKuUQDCYIUg1oDljRSJDM8lej7em6xHtt/o5
1Sl+hsfLLg9qWTnXSKV35KyuWF7q76EqeYypxeSk8SECm7T7LJWjQBeH3kUL4Y1+4UITMstEknWH
KMvueCGbtcvXEdKOpVyYs0NHv6hnAwt+xhcWF6CNDDFt1EwDBZLPJRUZxKPw+kmY16fAj97W1ULD
ocmPuMSJtAgAQh0UX8f1FZecKITI1pY7mtLdG2xVCxSPOL48HH5yUwLcF6GZgvvnQN+aB72fT2vg
qzbPpkt3NJE2AKwuhNtM0as/NEXlCQJYh59D71tQJxNihd9wWs5EJ07rmPS4+gYEfuXvbK7Gk9MQ
XlArJgNbApzh092X+NTKBEn8VLd9nC9B/sP4fY7V0zQ6phBIud9kb5Rvk5arbA177VL0QSceTort
UG80vejZX1fS/yClIPkpqrSXLxBZukJMdbIgb6yAUVQgCdHOzHI0UkBa3fZB/nBgO7S8btdsKcPZ
7x1XItIkhvgnIL1K3bHy1m1kWhDiQwS6grJYWHxboxwgOWjgodHERFQYACHg2V4ek/BaZc3yeF+a
SLDb+f2X85yTH3nY+qGGiFRam5P/lGIrZKFOdoinhZqWsvi3ENrhMRk4V4yqpZ+zBoS3ycw13NpA
LRGxXAYFr3rIXPGiYfn/oSAspfTZ3I8uQzYsjJjIWUDUwmZxqPPpel/2EJIAYxU94ydg+eDp8vDK
mNoFpITZmcQPt4x2es90hMfCM1UA1TrBLwhTXIAZH66XvlE+FsVeDmbzGlm8FZh2y7iorvsW8yjW
4nIe06Eoz6QVIkgH74tTAztKULzrGZYdHOnKLqby9NVNsg4WFur/CpDrXx9kZ6gQHxIbPyGEugNc
RlcGGBvcoFsQFzlGQckz87R7wGZQ9TexOQ/K3HUT1blfyZbcMJH8S59Di/t7gppvDXeITpPOE7p/
7HgBat8KMp+kzri4YpONPIk75Vhv7QhaFppnkL58ugyLOoDpj2/x+0ijz80uQ7CBd9gNyl/KemzD
gjyO4IYbk3N1YrVCbt0ZiE8XdPtctr6jrMKZDk0SkilYi9C++qn7EY7JeqUo2bGRU3RcoCkRCwsj
mMBs3vNIxXLAu+sijva07a7X5qOY/fSFixrYAqm0DjquMKwSDOCbdlxxJdx2JY7bjmbwFKFUH7bp
qB3TG2n7lwUgqRwqcASFCa+jodfWLX/HVy+QS9JqoFtS2axxs4u2w0DoqmfOaUe8nFJkq1amqPn0
JhkuA4kzkKYiASRv1a45dhiDzJyglGwtys+oUbA9l2CYSV+bs+ciiXdbkFbnaO5zZNS0C/nQay4K
ZPJB7qG4KZrCX0y5b24qaeYd8C066J7mgfJn6Gtk2lOGxx7Xrth4RSq9PEU9IpKsfPHJZNiozspk
D0+fjFlJ9k5rWRJRT8BZ0f2HfTf8eeyycqr62pC/h4OJ8wVAjgFLdb+5YnQHWQXt9lR9umD1Nj0H
Lc5gf9DHTApJAPcnO6XWBPHIWN/AAzhIaTecB5Zz7YmijYW+ckCy/mrbT3rCB/kXQTgvjKbP3oYw
Y8aJ6E8g60ykKIdkSd5p2yE60KlOBuBBxRSSUeClUSzbvbqQwip7IOwTX3JRxZVJpowkENt8Caqh
03j9TECqcj2O7k2G942dU8KzMMExDmBNt82iWtneuuv+4EInWAvX8p2hU1bxH95pJP7ziDMVvDp6
t3xLX7fsjghjZ+mvJH1AYtW/bz/2WzLgUeV517Y+5x6ULCu64aeBpVbv9dkfsNC+Bu0c5Ax68ZHe
HhN4XKc3NeEvfgVGE8ogxc+uVmlLjCDu2M1IfGhG0rj67yQKcx3hB5layB2YqHq588IppvsWSrvi
f3Szv96L7F2DvZuMuPO4fi9JppKiGrSIi9Ugn6wYnnZJy5RskzcmlMxbY/OSsGZSxqiuwQV8uiV7
I5AFIw3OaXVikFCWVUVOoyu2H4ouu3kUprtuFOTy9LLThpBYdO4MxcSPUKCxK89TqoQCee4/iLvD
uU82cGMMm0bchCelj2j5OF99nwG4mkdzJ2ruHF9bXBWsuytlpAMF9Khj9rPopAdF+Yjxgmecqgdy
jSlGhNLV8z6frnoj/ffp5vy+P2xMVdWLRNxNfCvVdQV3NFEpB/milN8NZk7VWpDFHXowJOZU7yJI
TOGEzep1SRsk12vcFUWiy6MxhmIbfyA68RGafP9dil6YqriLw6dzYdzXupxlt9QOUB/bXDndW1rl
D3yEnHFxi0dkh19WiQ6YtgiKz9atmhIHFEO51SoXQqPvX+pkqwyaQA8k1QGPpsHDudy9bFzhDXc8
der82eFhgIGsXboN8kuXBUgXXOKY0vVWm/MOKYscIrTngqakDOlMOqiv9Wh7h1pj6Bp3I86LxI9Z
4AvfA5ltQwX73tJ6g5NLRETLbMLMLWV8epNKxZdN4Qwvt0XFLUdqqva58wNCJMwszRpk9xjhQN5I
cejiaq6JTg5TlSQPj6CdpB2d9snqjaCtr8Ib4uAeZVN8tqOvR2hKJ2EVbpZUjSypVT/hp++3LZyr
kSXCmiW23UX2WgMj6QsYC19R7LUpjxRS2PQiHa6nBeG6JzuIY2CRvo6DN6d5HaWVGVZ8phI4nEB9
DUbEKVJbve2UBydW07CcDBjjjpGKXGjbf9wd1MT6KtL6yG+0ghgQRulYiHM57TzRBpvudjGlS1u5
TuDIZ21lOcfedeTw5t3nQry1lJL6J9xIOoL3FSeRJN9G1ScS6lz87bIzFZVeHbXaYCzYMtLsHQa+
PnwoSnjoCiks6rY6QFWI5nRBChwFMO5JNfU/ZQBKOrRWx8Me30gYXorMdiWDp+Rft0vLEPIvdVdb
WdVag33xJMWY4GHfUnUlZdLFIPsPqEgT/+vHx7l66HeFUzaYWTnvH+Xka5nRhRrYvcmhDiPPdrm7
S1MC3RHI4kWmf0TOiWbH6IUGZY2k9DG2SyYJ1nYppruAA7BTpjiAhHDGsYyYmGi9sTO39pSQRoQe
Lo7/hwyAxC063ZWDtCtmZz1XRg/njPIy9xPAXGO1fAzIdZ7hXPlqHH8LAm5q1bByGnRDjdAFhPU/
DZdYIgwoQcKxxAOi/+vKRxe+2J9SZ2i9f+aeG99aNZZ4xsOehOiNOPlzmUayLq7GrjstmP/JAk5X
fqTRLvgF9xT3qeLIUKV/M/W5i25MXc2ID3VdIBzKEFWFZp8PwAsBY13KFVHCR+GNcNlDi7A3nTDR
T4y4zPj3OEAioLPNvRg9E83pmeL9U7U3CushEbKOP+g6Y56Xbi8sVNC/Ga71/chQK4eWl3OFQNam
6+g4zptHzrg0G2h14BhlVic2kCMgv9eqpz6Hsx6rZmc9lEyQjTcsmbQ1XlEDunwBy/grL8azevgh
kuBE/EbS5TPWhZ3y1vlIki7X43XuQMC5JfMQmWjWhQVWbgnz0B2qu2A6EF1Ai/ioSg9cdwsBXcNJ
BdP75VrEoxCxqlmSTN8vv7GkqpOWgsYDQXSyoFCBMWueGt4lm1ne6BGCl/47c7PQwoYsczasKW1D
eBY6noOeT4f55Dciek/lgjSUGwFOZmyScI//1lxoJW3z5uPgbiWhh6o3bJwZZZ4fwkD0NrE/s8yh
BSNmikR5N9DFmMiASw7pU/rSBV0aC8R5yVesvPY5xKUlfrzyCohAVHynGe9Er9QtMas46Bv6r3zN
n6aMHM4H8COJocfdu4eo08U0Hv+UczLXrDiNgg6o4Q4v2SNwdj8JiIMj/xK7zzM0oXjIjTirfQHp
3QkzUQus3k4Fc/2PdsxlfPMksS3MQYXfPISlgcbjmNqgqJmtRMGl5UJ7ynAPaBpQ05doTOwzj1/0
hjuV+WuKKfqV8EuJENit/uJs4hOVh83Ectxlc4IEzeuMnyl4C4g3CROU0Xvf8lnm84tZ9WKHlM0k
65Y0z17+Bqxm6rLvqunI0Dz7QGW4vBbjnFlC21TLTCwTy88WcRlLBdprFH0REgwbREE1mM1CvUko
55M99QYB+nUZ+WLikF1g2deYHh/sQb0baZ1wGcW567OTYyGS7ovNlMOVrb7VHd4Lq85jqlbDEQZ5
hdKZV9PjOLXoTPndFKfOfTeffxKtzH5KNRgJTPQssQIckQg4v96WHgJXqTGfVjxi7GEGSzSmnYzM
S8aIM/A8nuC2FsZsYGah0J8Qwdk980pfd1oFiIPinrb3Cz74jVE31VdAlcw/cxsDkZPvlOblfJg2
KTXX753GigDTh+mD4fS9QRQxeovCIISd5sfxhqdtzsZYNKTm23DFrleY9V0cbeTEjWfFIaEnZGa9
9aytbzecYYsA8E0U05OfASS5g2TKPUl/wK7N9dVzoVzxPEqwy2ApfHeY7tWeyKf9D70BFtWzdXyM
azg8SWxyMLytG02V2UJwDDws1aqpqXZG4pSIeS8pP/WYpEtXopvd1Ac9hz8Q5b60cOgab3bNUYj1
D2UrndyRdAMzAOXCfF1XLpfE3zaZpaOcYlPU8cFNotipxcTefhkcFHGnJ46m4ztczM5YnP47x6D7
PLNQt58HFFpfcVQc/x5wSp1pPDoNMkDNVIdCDyeqUeDEveScbibXhvyQo3WkHh78nFxaQLHAt21m
D7epI88pIT+pL0/p/bdWranwy9QPaRtLCFJ0w41Yw+NnFEJkQWE5UTVac2VUnsx2zk143f6RL89g
aBBQVlQ5/GqDLnGGWctffq85nQxkD4w652kJDeUV5+T/1y9lBSHW2ocC6qliXQmIPF3bJRLaPgCN
ElYGi2+200eV3Qkip/K0RnObTsuym08/eaLjoJkRTdmvK1QHRIP7FnOj36Srs/B2lcgIQuswuJR/
HnuyQPYydJy2pkp2kdYDh/pKahtKCMSgU3mvX8UDJNVIde4QJWXhH8rM8Wj7j1rwXCEZcmal5AgA
zUzrPd6/o0XX3IlsHMAqrrzk9viEyGYzOT+k5f78x0XGRJXbhIE/Rexym3sk4l7Mce6iIB2AIt0/
dJ+SM3+HSYux4G28HLjCwbznMJeiklPjzwgJxn27fJh5txoXhEbhwbnGRpEXQdVsxzkFKT3oUePB
DNsZueUaLkO6T0h2GyjY15c+szRgUC1qlkFJ5s20DKDLMHYoYyV7W6511/ogO8qj+l1+Vx2ZB+82
kZB/+zEFsUdL8L2OeP5yv3n3wuJIiyauzDfMTKyVBAkmt2w5t949yVyJeNoTHc2tqDctQRbJIh6k
Q91MKoDcDBK+b+6bKjUrs7k/vMb4FiCG2ClLK6l/7sMGwW6oJw9r+/HzSxujpzvJPN42L1zklm4Z
+sZXNQ3LpGrNXl72a7Ik7VtcpCilS4UcRFftKeUtRmX3pBlZ7itNBv8bDgNWfvqsJCvR+Y6MC1sH
NDsQ0/+0ZNkhR1EZBxDKRNuLdmGhcFFxhi3b2du7Sdfe16c5WwhpL7hsBMDLkwvIBIqnuk+EBk3J
FKuG3ISXLrv34gg7/MciZzJHZe095usjJWUNJY9mJrT+H7qUakUNjL/Ms1zZLcMOfuG8tR02RIdy
t2V9B6RDMqIYCTLRfEAWThBO7IVutnY7S7f2HWxDZ2zAVqA+KXVCbJacRBZgc2V2+K4Gz8tYm+iU
WojYX5ch9jJ3OeJaQjj/PLTU+gT7PYlg+Q4JIbIOHMhfUevGmEzbadTwT0M3seq+97yEcSKD96eH
qtQfStZBChjOhSDQpu/bODQVuV6XVrX4Ir8G230tTn14QWZg6kQTg4vL7OzeD9mNHgIzo5mPMvWH
eYztUcggiT5mZ9ecB+1kI17g6QoGz0LKn3ikbb5AiztuMwFicEVnzmKnxgZ1OphIte38uiJlm+Hs
FlddjLhiwMKxATTwj9L5weoJZKvjVgIiefrOZ+yaPUd5/q46J3VpvaUd9lmEaAFd2HfeRJX1s8pX
R/hw1yeUzR94bW4wNUQlQ2bBXqtCaMrs641jNSSlRxC9sDnTRnEZVgCvAKVevf32H9bAG8wXu1jn
Wh6Lei4TlstEMedgjlckvo+C5+06a7feCn239KkSqNuQANUNxauGbdEkuzvLUOmT+jBIc88rThj5
HS79oyii7C3QV29VU8Gj/Tzs6hy8FdMK8980m/IUM0igArtd3WTw/6wZz4TF81XLtZOhzEko+Z1N
eDW5oQYj23a1rqQF3W82VXOdGVVrgq+kn6mawWhQ+SL2dNcGf6XgzHb3WUB/Xseb19mJtaf+UYrf
nHFM8xmDTNon2WQO7aaJSN9xsvpV4t7ph4pAeXMkVyJBh6V3Up0pcmiPLr+GoaZBujWHmMBkTYd+
my04QOBz6pRhAXwZU6d5CV/73urS1fYrkkW6AS4Ph/+h5ZzX6+3KrCfW9vE2VZhlfCQ7Nl60AKYf
q+bPxXgEtj1wzZ3wdEhq8lG8pUeMvA1c17bN0maxgrqkQJUPQFYwGhbKG9TVWiLNvCh8/dL8RwUX
oYF/LJ9F1Uz3jwRsYeYTXPzPxyCkVT2tfgXp3yti9N9Y0eiGKl0qkgmINYlKqWPdkUXj4SF7CYw4
A6fqildPuZZ9+s7XfHwz5+O3esa5+aIKjE1lehvVvs9nW6FQ8ILzVfvWGPw2jlU5gdqxv/wp5jE/
8cxt53ZgSzFE4Dct1IzN2fvQxE/tYab/6ypi3aS6GkSGxn1C34YK+6YwaAxqYFpUZvxzjxdpCdZh
pZP+eccQZcxv9lg6+90ajIsVt7a1jhn2O52lmRJzpQJq6tcZgMnjjHz9A7D9BgyOUg3iXy+kEQ0x
1j3aJVe1ug1nvLtCxKD12bGhiBqekU01x8syMTvh4rCG0smBbrn/kvGKBS0Bm/xrlra2IlWtwNvX
dwvNMjHUDlWVZI04635owdZ/P8bTAn2Zz9KGL3zp9/zLlrH+fKIZJYV8cf24z1afUCLp/JukC4Mc
aG7wYpLiH3Zq5SlDenVRuDffIQ5obi6OrplZgO/RXK5adX15hbcSw8emDnmSuF0pKellefiTHT+a
8SFv4Jq/MS0UAswRBnAoLoi8t79FgULx7WUN1/OOBUlgN1SC5zXXsfLPT5Bv9cww3ZV+0BzbNDn8
gM186Pc3v+Sz744OBqaRAH/xkSiL0Gu3C130Uf2PSzq1kXGfXl8Zd7eRRegJFY0yJAT8niqHacCT
VqYAwt1MjF1xxsbG/NbPvgULw5oM7MNxN6lYNrfHVJyKfubPEp0V9Sz5Gld4hVYCRfuBkJrBFW6Q
RQvmnDeQx9WRbfa5L7nNuZgYQYKsZ3X760395C1euOXizm6A9U477uah/JTMMM9BEYsISbsPdsgH
cqWKkC23QNgCqEqswYYI0Ed+uFxvgsJefFEW38RIo/FJSvKJdM9TwY6LnuxqQz6piGCfttJ4Frlq
0lM+vqUnZg2FAwxZV6/P4bp4x6KaW+GnVJGfYNqjXQHVmxU02TfugOo4+MNQ0wuhtTg1Aryd74eK
TAyBSVT7MvTsPKWvBD6csBwMeSHfg7t3o5d4F/84r4cZj9svocWsTXPovMwApr/1ByU38c9fBCdP
DoCUjLgkCdtdVo4JnHi6vJsAUHvpDJXKzEoNgE0CkyVtsT+QSeqOHmCbTdcsxlsnP+/zFsGLv4Iw
LBDRlQVP29TyVdZiuNtgpPaffhqsbBjKhhfM6jDH+xRgbx2tuegf7m+Ma4PD5JaWWPkxJ84UvcnG
LYkrq2xclqbJKRoaKWb2kpxsLxbxUqiUZA2oFa7P4AEdk+FT00iGmtNE/zOS4bs3y9VxgN7dl8Ex
fN5CpcnJIEqLmC2NU55SSJNmDpYIXxEXIi9TO474zN+zfJIRfgTALLt2BINTUE01mfQxS5IuiZgX
oc8GZEaKSV1takLP1HCf0idSuULAyK9GVgngyWYpJbuTQk0BxuvBwl817ypYM9S8NnmryiWNFUAU
jh7+VAyRmYMX+TR8il9EAwWUfcgUhHQm2r3ptI5MiYCsplRHbaK8y75mtBz7ogo1lraJc9S4qDOq
4UobdADZmnekxtHymzeGxeu9UVFtP/Lhfx1EbTYh4U62A7wf0ov6tXJ8PR+hV1EB5CXCGMdtiiRU
vJmgsWoSMGF/v/7mBWhXEjObq4yt/3EBaJ1jzb7m2ukwMrcdqSBhAHYjoqR+x+s/LoiJcOXlPm0t
Ht3L0sbI3U2IBjVSzNBwc/KOWvyjDZxlf0wFo4WvYxHyNqjCQa1YM7ttvhmTzVmM5IDp7MEloyz8
gcCs5swaFTSElgvbsbxFii7rw8AEOQgZVsisWSwrdPE3Y5u0aKiJm+ZN4bt/XNcZuLyiBJH7lr4Y
2zrT9i5PRmlcR1Sp9JAOPwODQyrfpcAp4kEl9ax/tOpQSk8UH3N2ZejWHBEAJCyWqk+9mZ1d1R3j
PXgy4Pgj4TY5b/RYc5chjnGPCKD7RlPlJQ3Tmr9ITNoAH56W94TC6k9bi4WuyZtWqWkDoQU4WZvp
lD0jxVMfWP69Hs6C4yWJT60+B3EbqKjIFaEHgI1l5BUGi9GD851RaA2rzwuE1R4hxZTJeHuwZsj8
d5C4V69B01FonJLuIv5Dc7Addz5DRxmNGNLLOheC7lGVPNwOqqct7JXDQS2qlpjpIQ3yyD5Wvp8g
lZkLERS9GjZioxolEpyJG7ie53Elo+rniRHqrBNzs4XeAC904dUZjqYycP6Hcu1paFhyGcHtMXqt
/4aVIMEyd3KYV5UvaN7UDlTbpQI5k8fSUHR5A7wz2G3nyrUMOcKk6Wm+DxHEFQJxGx7CSJMBVeGT
myxs2bfBZI6At9NszJHbf9tweHiug5KsjiJPjRJ4EksIFAvdcu/msX1WpItELcck22J+EgWYAMrL
+0bRdOBIXBo/ZvFj+i8u1HR9TGCrhRwt79HEMn8b/2h/2Zo9m3EmSrIIEJSsYReYTb56nOpIps0j
UmSt/O43qrXPd4TgJF5GjoF5TuuZa9yUCjqHSbMRhL7Z2VmlhQsYC4Vuuryq1fp9vCdtD2Sea1ee
FrlVlglP84k17PSsFesaTwNhLWJPMbQC4HQvh344uHLXzO84sYQQx+DNhV69wSJBxc/J3yvNDdIQ
0TfCEcRrx8ngNrhezdPbr4wuL6jRmP/xs5f+db29EpMHQPH3AVCRv004kMuZ95B40yPyNxOQTjWv
V9gdng1Y+2xz33XFVWde53rEG1P/7MMFdh5a+/PN3KVaRFRIwOUzGmQAJRC/0sqjN2HTZcX6uNgI
XcG3Gh4ry4JC/oXzlDThWjEPH72cj7PLU2KzdSjNzptmR3h0hyMwAUv8L/xpsY4dJOSdywFrexVZ
K7R+mHMsRanlqlMHncrWxJgf5mnM7nAa0miBdqGl/Y8hytRMq6l57Ia9r5gyYgjPMCQ+cdXv4wF6
PEvefrEjFip/ZvRCRqAW9SGY8sLFHC7C2Sa8EsUctvWB9iuy+QKbt2hI+hPviaDi29HaRbdK+07V
+OJQzlJ1/PVnM9Ryq7obFSNU+/FXyoV3byTc9xZezi9ZY5Zoh/Yt8HXgA0je6HTlTkAlUGFN0qm1
/vwZwm4M7Q0TwZGzJmiID5Ce/I//0tai8WA+S+WZAJvjx6xZ/kEGynZSWrG2I0Tz9QUFnJl9KVfh
y5asXpvb1h6tCfQvYsyNNjwz2hLTYWsTmeqdxF1JnZnm1RNKINWAJOxtladHwl2+RaAD/N+pyM2Q
IWY5s5xQfA50Z/l93D1FCegpnGfPNt69sLW1fP52vvVTCZTaRIXFJkFo9QDcIvbRl+6nHIQrSsTI
SojqYym9r9fAXG4QOsL4L/5vh803q1HwHRgEB1cQct9fQArC/vBSXbqpP70+pf/ZwUa8l8m99o7j
qtJEYFihshxy2Kw15abEEvuZ3VGnvys2R3qEmdQhgN+/6l1+/6KxkenteuN0s9cLFQq/1qDhwuwf
rVnu1VKOV3BscqJSNfw62D11SpOncJWXLhwSW079ztipXOjlYYkkodtCPtXHEFGaTqxOaHw4KKiF
NJD7ELo5sGicm4n8cpp8HXdzSC9vILXz2iGGshuo0+QH2qUjhqXWnS3PCruB0SNmxGHHVFmY1EUN
9/vg+PGWkdBc0Ip2R4riQCKFWznfX3nwr6YVnfw9llla6XGWQ4MFKrenyEqmZ3oM0ryrWeg9w5hf
5VsyXBpsbFoser9Shp+GI1HJFrD2pIYvyMIp34cM1Ok88ycdLuO6FaXkPh3jsSqPaq3JE21KUGkj
6RRPYmMEdLpH5uHJukVxAP2jcLKM/WTxJBNfJtKRkUmHO205XQNLI5KPtTg+9Uj7gUXJaBOXRGP7
7irkCDNg/9Ld8KSB4uIGHYGL6WZUiOsJys0jMdGN+0BqpMFWkeC5SsLPZ9br1cm8NpwK31c0G694
O+ZYtE1HS0Gyy6XXgZHzK3tSuuCLsbKDpQ38Qybgjt9cJtxnBDszUVmAjp6o2NNU+YfaJ7kFp5y3
jOwXCgk9cpLt4bZHRYv7w0l/S32qi9gUL9Wp1JXbK5eyODHGWMUfsGMKelIFjttAqSYbgJELilDl
XWmxX4aZdUZvs+zLhk6MUcLBqum9JwuSyRmJm57EMOaZM3nolppCoTEqEBJyES6kewrANT2s5atT
C5NbcrcctpoZEOWElcAq2gqv4vYyNYus+TA6JVV9FchbRi78NrW6eiOCTUM1s7fKDlXLJ80bMScI
kJJjTgcxWK+6tiGOxO7uDukWn0HVT9KjeeGerp4h7ktQYRbz9aqLla8NRQURCcvHL/ES7pXTBZ8x
fI8ofAVJJmJbuS+x08vs+qO4sG5xgEKqpwx4dgQgqr7XUVzlkDA7YdkgAfQW0eJGm1LK5Fw9y8Bl
vBCLuI6I74rFy23UkSdK4sTZR/XamEDpXKqC+KMwkujCyf5b32HEdG8mAbAVGXRG425GjyehtQib
NiHxK+KlUSwZ6ppohD3pPlnaZTfl2PWpo0D49C6ainRLs7OlPeC1Z6uJbe55VTDQt/myBNmSRVQF
DZD35DRXo1t1JAHuX+x+TcooK6Nt3w9XWYcrMRJj+cV53iWcGh9UW+9yFH0xZydBaXCsNi9g9lcv
yG+Gtmd4j8Hx9f4wu+uvHGmVK8cbYp1/ECyfykPM+37WvPnnH5FlKUp3yTfaJr8oqM2JZAW9f98f
FmC3xbS4bu5uUS8wdbZLRTWylxiJX8OR3w7Vv3B3bs5urbnAZmPVKPyAhaq1Q1Rzfa43zi++sdBr
ImIQyx4Vld+6u19hB6XtCX8bHlJeDf4GuDyTylJ9DwKRPGy747O65Tbb7KZEHAPdnRjCV3I2BFoc
/imCibGYISjt3jkDHCMyqYphNyXQciWxNjPqjYNTDcoD8kALU/glDNmauGmLxzg1bUq+QULy9kaF
5pqy1e2adDFI34MIP9S8/zWGHgQSxpNie+ieRTmqBjLF21JiJaFAGpRApEhGmlIqAQKSatyF93DJ
Qrs7uDaYynng54KDe7ROKTbs2VFIaPZdKVVPkFpIo4K6ziKocf/r/m7f2dtz3bSIQ9mIEfRrq6Le
jRgt7h3p9fgBTvPKgeAYuYUScCWYSUTPb+qywb87d3gIjaOiJVnfCiVsZ1d3nHvixGF09SZjmXZc
hVApty6FNk8/0jrI/QJLeiq5YAcpWaJ+vUdOPh5DotVES943IUoZDXWWd0xfS3tVL3Et3l4M3BUJ
vtNgpN3BnDx3SN0tgDBeA9jzZSYOMprxrkmIbtG8bNgs4Sw+tnIJMaYrSDJXZVIcVzK2bMxsNSDG
YrR3i5gaDBoT+4NfLGIokyg3/waIjsKdPa99OMW3Bw0b0qgdiSn9JhNMErfWmue2dPObhbn4RScO
c6yqmKqcxIfzqrMjLVM4H9guSfexMvR93M0OIQr40TivccdTp5U4veDivIcS9N4BesXMu4fclyRK
DV/+ahvZ0i9fMYSJJauywD7YzOh/tImpn8w898kJwGS4F21uz7z/S/hYYY5opg3kbP6RLmLPUaT9
8cooTzESestLnKwKoAnx8MDQBRNVMVfFku7G6tiBRKs/1AhrH4JGO9UH8UiK/ZFN96PLcOAf61Pr
y1BMz1UYTnny5qeavjmhCDX+sTnfA2LQQOaLLtFSlrKs+4ojRx09GSDnTFf9f5a1pkQ+s+OUz67F
39Pvdb6lF2fJwxfAmBo6fBTDEFmAZaogTrcYlrpvwLAztivXbuHT3Zq2EC49Tdp5/BMYyUnuAOm6
nghnnexIQtE65L/EWGHot5Z+4jn7Y6yMKW93QK1QajB0PoM7NPjRl16Edw6xYOINAnfRmrhqGGXY
Ge9AaUfzBG9SzKNpu0RNdHnNpDdxmOJK/u8sSjXYPw3CqjemXH+gpy5sXmjItVBwXDmqgm5qJFF2
FfWJquPbS06UJIGcbZjRsaaXPoQLfziQwjXxapKtXDjKJWuxVxjIJWgSuV3+/6KbbPDRFsJq436N
OZEaocCUbjthGKRm7l5BzVWGuDxqeyiEuMib+Ng6/lJAnkCNzlt7TTogbW7f8SUJXB0B+mrzmVWt
VloT9bE5zMQt1wHqgIDGedgDm+CnncLBeP5MLO4MC5piH5XKPh46Dr1dw25vUa+ePzyex13KgVA1
PB+NoP3W73nMEVPsWxqPpPiDai8vAdYc5vtF4I8f0QAbkJDr7TgADQd1Cla+jY0n6aV2euYESltL
ufkD1TX22CLA63N2DcO7PVwltjcuD7uU7v86H2J9NHuP1bOpeVMwEFKmmV3KWbY2+eubsvMc3aNP
+Lz1Zxde9cPTVmucPS+LvuyXyiyKHol/L6iXeXNaI3VbDr8oGP9c+WweLlfJG6grNW/r5qEjAlEQ
am0Cehkw9wUffS/ANpywCTrIhsvBXku0lcThQfQdjA/fno61zkztTLqaE4KRcFlA/skZbdZjz6en
ro45An/qauIr4SWAXCW0ArUt97ncCSY2CWewhhxiHgEHeXEenYk6PEzZ9sH8KG3MPEbe7qgN8zCq
eoNatXDW1C/LNNr0sNcm+NRY9ZVlFdaQN9gX60cDll3juX5bSIKlU+2Geehi4mgN3UQbXOqbT7WI
yXCQ2MRNbq9lguJ4bD3kERKHIygE2Hn32RpCzreOlDCV1/NYU9uGQ3LAi4GxHJU24gDe98YpAV1J
cm54iiekB/RnmxjHQhWO1ImOXuWi8jRHrzxrEpcPZNzONXyXpMxGV7Fs6pGc2U0YXtmzgWI0sS12
cY7yhuHd0rXHMY/OuxMMvjxi0rDUzmhyGSvKDsZeKYUNPJ8D4x+jvxvAMcFs6jRw1vP42DhbPVFV
FxwlpmeyegzCDGdqYZm0k4/LfvNDoWLvbdmNiIbUnHfKWY3Z0nNjTrkn6OjZyS8O2d80Lk6QXBaN
xKnrbC01xbpbPJqnLn+m6KdDj8Y2tKUux3SpvpQriw+/LF32TQC2oiX9Auw1QFP7YZNYFVtISdTM
A3Qt1vSVeamFXJqL4FXkSIcPZnOCteFIkal+VBkx3h49yU1XFmeL4w4ocCQmRwB7u+ykCKMV/vuf
cDxCg/BQSVUC47EWud+toOiD8qBcJCjBbnLq4aQmX06PSR15LwmFHrT1NuRaAhF89xFKLy59K83r
PHjHrT2WR4OxUrmfmw5wsvWcEdBrKE6xIV8s+I1dmDaDkYT8ZKrb8tllrPlG2XlZvR4luSIDteMI
YzPHF5GO4YpSBdYF0QUdRsmIhOZidM8AnbHWpZ1xik/ePRPYncUT0aLeH4CBMaZgfuBNQRy8bjtK
ERkoSG/Bl0N7tVWvbbSrk3NZXecyu1gKTfKMIwNN9GQ0+/ti+XEcUEXJJygMKWGz5TezUXtZOagw
yTa4Z3Vax1p7dDnNby+NT0tWwIDgVQY9G0PnlmDgtfdgEKBsJosamO1usXUhQKutFNT2aQg9u4qb
ryephhVX867Z1//WayX7aUsDVXaEXgcqJg3F9uYKuIHMJkDgLGu5sFc3ErhQmCWhzI6je8bQ3A1G
n05SakWKgL8ODtrt5+gOwU/2R4icMHJD3hPVZFkza6TcMEwK7OrDp7GcWKwolFwtTa4ATastoQbH
ZVNXPYIrP6wcuH1CIbWTsP1phQY2WEhbn0UuLFTCwtmUXmxmwUpsxzusBWmtlm7iKJvzySWFevtb
87vF9h+R7WtPgv0g5nmHVnQQ1L4lckOSaOIUXTewumztQXZJRpixH6mefmg1MaNosxzDSFwo+f77
z9dYRcPFVRkSScpU9j32a27b1rHkmqwZINlH+Qs+e9O2e3pjg/38vnPcrdFrrvPmpjxndWssH0IH
/rOswuBwkiL+hkTR4bHXdJA9rqD85zKYt3LFhFx5Ynw2AT7h6anCbRzQAxIp79BFhM2F1hnGfOgb
9PNgfsuxfWbvYdmdW6gJNheP1nu20cWeURU9NbxDKG9MpmanZWffz9d/KhmyvgVFke+7B4QOiRvh
eARhR14NNIZZemmNIIt8xdgSGVRsUq5EHckBckQM3pPK0sv7VkgWLnHjjEIv8l6EZe1UW3spXTZn
IcwY0mYPBQCRq9ogG7G/O+lTpFin7GdHRAYerPiYDIbFf3drjiiVCL2mjyMdSeVR8q9qOJQ0aKyc
nBc5iB34004r8BrRuGHHngsAJ9JI/CcE9I9nkNIbwF1F3TPEZg5ktRFel//jVwLURZvxv566IoRP
QdGSo5ggljoftOGpFkYq6ZI36xG9Gmrx9L3rRhlko+Hw6TPhb0EYreXqpOOuYvaQDbd4Y2YuCBat
dIKWKzf2Ia3dMFjdMI0BCKw7SQO9AgZiZFSTD39p+Wzqiebz8foYWuN6hycZp0Xu2byLegt2fOFt
OVAJvKmulBeSGIhewnozVK5AsidgK9eDFZi9leRu95KmveHm3HPAcp2E0E4M3gHt3b6JRv7pT7dY
4abFoA8w64oYZ4DS3Lm/7CCLIYuFhh1cWDHeVVzqWIrm5zknE99gWAI4yclmJZ5rDCcK3ok+QZ7F
21u6Y1vk7EX2WvBBfjWOTqTjrc3AvPjOSLAUmYL4n/xeW6BdZ8iSovz/+uUkiKDxj4kaL9vWYF1+
/LSYGFsjO/avomXxfvKbfdBc97k/mwfiKgwRnpxlWOhm3rdNpqQeiWb1qarYlVJyhWmYexplVAQJ
XmqloivmEAfrBXuVrwSwLVs3wFi5Fzbe4FsttVJ5E4t/7381dBW9FSyGpNEO69nmQ+pavW6Ywp93
Hv50dXmmU+DzFzSiU+QJz09DfxGOWfYHdtXuA3KPDO0WYG/Ov+/UzYM6Wwxisga5REsgmKDUaES9
t/CPcENHQoJMJw+jOcDZR73xBR7QSFobl2a1HkO3G16zOAkKk250GX4utQBs/Axmm1u0UzD62BQn
e3ys0kQc8AFlA0MKToDtWtzqlFhp9ke687sNz34Y2NdsUfC6gid/KoKu5J6g7dJu8dcqMFyXlWVf
93enD2YzQgrlpjNKgLNJJiWoJm/2z7CMHdRi3tFnDJPNzwA4p8Vl8tP0nreglskVm57sb79ZsU5l
rfccg4+3PB5MXOpcGQ+imD2nk0C7n2vh30ODDbbpeoCdPWaUsryOO6McLI9VwYnpp+dWGdrDqPp+
HxPZWbz+zd0S15fgjtuEih/CqgSa28tXRlrId6jYCUVQVXYdehwWS6+ZoCkRr1/mzIikowDNKs/Z
2R9DLUihWe7ufVDEonEyAYjPcmhl8LyDlAMe7O9ONZe/xvlI0CpR1kfdx+0m+MvZJ1np5o20Q8io
QKiSGxdv5N1fscxlfEcwdlDZCxxRy0roA2Tr6UXHD6qb5qVFchsffQUmDqyUsxOyXrM2VPNFfjWM
baqsB2aPgHdMa3t47IF5rt/JpZjpz6AaAcnH553H0mOWf90WD+/LqmMiLblEbhaPhRLZuNvUrk+d
XOWqLSzujhHpMZu0KElokn+IWfQ/SgEkGnhnuVTH+l6l0ansSOsyQpZl3DFObW0axBBFwNutCbQk
WepGSnHXhD2fqDMbodFGda1e5Ag7Iy740gS6kBFEpcuyWQt7/j9snNpSG794W82giCK4HPeE3W+n
Lvib13byKB+zJB0D257g3HT7bPOxyxbGI5No8viDum4eDcDzkISVlwcaBzItb+KHUKpSNSOGyfvi
O5lHACPbp3kmuRsLI1Waxez6hIRFDaka+re8ZyJ40CWv35YpjIANF2M1n1IQ0ai2dGsoxFARGlyu
KYrRDHFbzOwyu18sArCCX1dnG5sXBQZpy49avTZJnGZA1thL306P+EmeUr2Tzm91jVPGz20Vg3yf
o1vzg2YDk/sOfWARNCuLNE/j9Ziy8BI/9bZOK9q4Y69rdsZbfC/8P9BrCPcSukc5p8vXvUfSsB03
UZikDgY2tpqncdQIebIYeCaq0zNCUOityuNArbOSvi6dQIstheLsGCq9FRfTMSQQntqzG5eKjOJd
KL6Xl6AbYDqe50L4PVY8+gt1gd1ynwvPR0VtQJOhfcewUc2NnauEbumwJVEbLJApAqK/32dTJuTI
zbXaUS9gr/jFACKZhiyYAdA2/XU+3kdhBWPCdX4wEUycDXLHP1JDWfxjC88HpKyKgnfE1F97MRcF
vR/Ns9kMzsBZivPKX2c1efMFV2YZgwhVLFMc9lU4pUb2wpCtuQ4op4aPZjuoV/icsXhUU+7sPFNL
tR7fiQdN0uGB1TU7Vm3Gx7ueD66NDrslQiH6NH88fG589bf9nOK0/d6EaWz64JYdZ7Lq2HPqIAnX
1gADVivd3xg225gSmm8W+dd6tYY/TvNmfMoPyNkz1IykP1X57oNDwG3ZvhvkvZFzk28ZIUk8Ath9
coX9nQkriMiP95y1KHFfeqzZHoYHetSItsnO8MoJ68F9LwrQjsVq9+EMsQPks65jmqPBEygEL5FJ
e6Lk0qznjO1b7AwWvtaHSQfOGQL5F364vmFEzXegdR7KrqaI9OguHe/VZB29Appoihhq3/e6LTBZ
8nSmmzaTBfPlay4aN9MsZ9C92LMQ3I8vBkjFzXI/8O8u8h6YPYJdxBJdrDFLUA5LRmyeCANjRLwk
87WLnaIKk0IrejPc1+RG9Q1wouQRMHRfdwVwfHWF9PRi36wVEt5F6VQ8lnjHmNV128esl4yYTklb
W17eRgcrwtWLIKpkAShWafS4WbmWt5coW/hVRfg/vF3FpeAZ+GF36GlcbpzsMEvdGVpDV+Z1jjlV
pvmL/5HAnFdAkpFgYIFC/ELGa0Nk7MbR4s47NSShhhhmgIhqpst4dPsluc+IeC5txQh1VjJTB219
aWVoGB13Y5a53JU/JO+wzqnPNmgoj5IvN0rkrD49JtjbbifBFQ3Wqb5Bp1c8yDr8qUk3nw1rc3VB
x/sLX6Ja8BTFkWaCdRGXjWGIBRXaxrn6ckkEoYLTfMSjW+7Nz5APq3XeMSJFcWDN8mPln20uUm3X
pyBri6JA9wHUKFhKCmizSWNR+kiAwJFSX8mWOI7/GBl+dEAsEB8gK+R5s3FLg6Y4obt+EmquiCOG
9d0MF9BwfX/Rb+rkBBkmRbrbIDUCksWk35KwK93znI8xfW9nNmfvDSR3rMTUUf/h6RS5EUVrsoad
HJrWruF0/4bPVw/7c80lkx2I3j3pYZZ42L3ystZNTj8IoH+lfWhIw2gF4UObSHN1Pe2I76r+qnpK
fvUwRW878Najl9oZVCSX/PBiyLVIZnXQ+77qAHJgyTx0fzmlzarHQvJ5VZxeSIdwnzgwruxBB99R
uvDmChxqJiMEwJJ8FFXGY8M0+YsxgVYAOUIt6TFFFZN/oDuMPIYwBL5Hpl2fiFiMmPJnGUKe59df
oMi9XFWtYrnz8JuH5OXWvysnYK2P6TCBX4gXBtxg1MGs2iyubnmMrcrS01OWo0fsIAoK76WZ9BPp
M5f3QFSBbQUkHvurZm/mQIgmBvXk/lMOwpirSUAhmgCHeDcTCIMPNt2EQ1H6FhP8BnupAbp4GNSm
tgdQ6FePLR/70JWnkRU0erbeeFOtkVM98EolmbJFKi1V9qgKRLa1ztBtCyUCrYIGRQ19NQVtwAYQ
7w8CvjvkXztdeVeoluShAz1fWVWejJizYw6m0HB9a+VNXWnaQb96GOwNnJ6tdENGp9dcDcqmmG/G
Wv6dJHUdL5xlsmKv6Ywmsb4ac4n7tJj34mcg9m5zdiBeKlEb8AGFO6lk/q3I0bGR+FC4klXGDgBN
Pz5OTTg5xXktV5Em6MHld1UbT0B1//yWYb5o67p+216cZEePH0I+ueegT4x471xQr0CndykM+ZZM
B8v43d5AJhGuKTWqumyeV5sBXJVIksW9LJFvSLLExOup8WZ0krUu1+c8Acl6G9VgKw6fzRSLzq3S
S2o5XnjSQUb8dF/gISxtXjSVds2TcHll1vDDbvcLSaIMrPH4U9Gn31ukvJXpUPq9YXuXyfM7B6r3
i8iNZaxsOv7wu5RE1JNECpgnlc9nVtKsYl/S+KVJPx8k3r9nowno9zPF/FfsI6Cj2w1+QiwZ9Slz
MQqPWyEqnKo7FN2XBPjMwhQeRlH9lkCnmRflJR87OIP/f/JwRGjezX3nzVeVTdGdQdxxJgVYy8HN
4jtr9MaMmv/4xYpCAKaP1eWryMlp4dksuhBrJiJ/9HtlIW2BL0Zu+37KSr+MdL6nPLEukQkrtpmt
QD69Fwdyvfcw+KHoy+QFXfZpWfV5ux3cFnX0wuz+awyOltF/kcPVEFDQ5N8JEgh62K0/Wdm0izRj
Xa4ZrOPpfDPtv5bQW498r90tCgtKqPRuZZ0p4IWAwfyJHQMhSO68idaON/KJc8YQgM8/G2s7dy1G
t88cDVm+Tt+/6jUFzU9iF3q07VEyVFaRrpsF0k1y+dv+mI1TO1sUQUeAWtEpONPtY9tUMrtlIOjU
7CJ6n/Btky1XBkD/2wq3D56xAgJAPsPOrr13N5LabUulrgdYHk7aGQunkp22eRZ4fj85xxWv2u5V
fh37MvjcEbM3akDb1XBbzt16Rv9mWY+wPbmH2FuoriYThawSG3ph/2nOxK6ph5+lyuD0wTJx8FLT
yCJ1pS7OT1QaQeBMGmY80POlm7Tv+CKh1nHYFJ1m6Qnf2cZH775zYWrTQ20ve7/HFw6hKS9D6ES6
v1P8XLsRsCo/XJzOMdG7lYElVYIAfiS8jgmtR+7L7qCZBJ7t6fx21fXLTc/9SiccjRkKcIOQpkcS
biVTiP7v/q5S1VDk29QhvL1dMR3ActZ0TZpIfFnUZBycYELw67ZR9oEazJo+odALSsXoJiflUU7y
G9+EQZ9IWlKg92PEcBRBIBvpVC9SmshWe4SVqfCFxxGoStk0ifz2JPfu8PcWXNaz8jfAdt59dp+w
eyklZuAtHmxVYrJD951LTwi2jLD8pKYpAql/AwdHA0A39YE0Bz5s889JpVv1uwDFj/4FJBhKqypK
t2k7KeTNfKFaU9OoeO882yZnbfZVMyK+c7Qm8CZPCvYaSiqUoT4HrRBPM+k/baygwCzwXcYgOakd
XuXnlMk0VrB4t/GjS9id4fQsbU3wvao+CSBMWoGQw5ZW15Eu3s17RNNCMdJUT4wH1sJAgKYRgYvJ
0IJBy2s1WtVY6nMS7H/BxLCLmC3E4aCCdOaakKNvWyX4pb7/+rsETw6nSGpW9fGYF+PV5FBIet1d
ITEiaVi1OplzPgxa2vFR7gDxJ2js9+umnrLc0q1WhTK5rAiht5FzkpgPqsIETdR+mTrBX8jyL3r1
Ghc6uTuHQCDRsUUm3sHiucfbNxI+292Q8X9W/VxE2T847FoBazV7ZI6wiDarLL+qsMEF9PIN7Fs+
M+xCnkjGmU0keMTZk8n7aPY9cfgLf8WZx/BN3npQYRil1cJFcah3psDQTqK/kaLORAVJMDEymBd3
S3D1yGsGeQ1Lvutfpnyc9/dAq2OQvwQy0XyfxfOfoV8qa12lmhp11OTJ+v/ipiliea9SEsP2ZttL
5rP0+k/IBaOr19oPn66kiRac/dPpGSsqbsWOvjbDS3K17RApT0bdvBk3VyEswov1FMmrtWm9xK49
0gPL0HYOQunIRYCenxsQeXiiPBN2kjbWvdUBwWxmBvcWYpgkXIuE7zxu/Ab9hg/dfzzxTFOEYStm
UaEWGTmG4O0vET17b1MZugLasrJ8JFyZxRbRaHNG2mDbJ856o0Jq/dzRZto+uW9LpB9Hj1CHkcsq
pZ0d6CMLX35sFBbsxpQo7wmNd86MfPN7IEM9a9Wl/BL34Yc9EEPWCTWZdb4cA2muHZR/vEH9r2L/
tMglUR6O/uCsZNzBaE8rh5R+yP30pvx7+d+l0GuiDmBDa/bpgg9E8AgtQeEvrJrlYyKPGs261NTt
fUowZS0L72oyx5Yt5Of1NLbSp/SyZzpmkT0kn8DxvN1V7KfUOVkNUFiA4CUje0WymSqwpHnRMuKW
v+IsoOAi6NlBCZnHJSqUrD1joFhOdbpMy1m9MTB+MyXrkRcdITRoBEZatakkIkAhDgmOXW+bB45s
p3WUK34zFmId02iLNLM744TVtd3xRe/s6LEkCH+5Yh1DiBFPl6qB273QDBJdFB+bj5QTqHfBCZPn
MC14ItvWUbZfN56nE2p9pSeIBuXOAAKs2stYgUWAYN6Rb/s8wVX8cH42K6wqO+EcfbmXItCkzb7V
cZ72mUk9gdzvhls07d3JrD4H0Xi1cPeQFdxYNH9eBshKk+1kKWg0tWorFD5jGTuO1dDp3xpdnVSh
ZnTEozEtHdnTIZ+a2y4h5LRPA3sWRsLw6ED8whVx+xfvePiq/aH0RixBWUe1nPY3lL6tNiluu6oF
T5mr6glkZKqFF+6ylwHZYbcGL2xdjgIwzEO9G/F3RnqWa8Svn1HGz0fvGNQVj36K1/GWUoE1mA5U
X20YoU4NSDF28dgRiPcmzDR+tRmiNpZ2gMGHSOf1AAoZEeiPfOUN9sje4XFIsbAC1EYOhIAiHdKZ
9Uhq9rmEiaTBRKzZHtIYesODMjney+3J/7c71cklIWJiHkDcoF3mG9uEynCMwiPXsly0mdgMnts5
NAjUz+qC+oix41ko98cg2ndfKmmZ8J8PYot94jeixAuMnq8o1XeqKLQA4mkA0PrCzl2bFV9VlSQf
wRD2a0k3cq0GOqt2U8ReC+2xkAZAhnhoMKfkABQJQ7kZ04ax94xI78YRBSd8bjer9jjLoLNECc9w
imbJCojkDanreUE4knKsIigFGHkvZoEAtiOhqs0XUusv3Gsclofi9iwzjAZ/H1QeZFoOjgY96C7Z
UJbYjRu95qXDRdnkWpMy0WaWvxin+/CkG1P1+FZL1Nn7lMG5nflP++qqHZOYr9ftuxHZC04W+/QL
sMYbaOlH4IVaPuSrRJMQDreCiQQCUBi6vHEbyzHMCfjdD13XQmBwUaDDyfz9TZ9/AiHAjjP2cc3U
+b260rjKIkLW3KVjo3aCc1gcB4RuITCw/57uN2fjZE8inawgk0MW4+I1wSjIlKCKkFvUwdcUS9GS
Er6CVgufAJZ7Ekv0PahtAC/2nHRX7A9TpWn8aofc2FHq2EmOXD5lMbL9/xV/fdU3mFLHVclNI8sM
ir73ZW8J4bsCtCA8T6oQBOL1WVJnJMlLgjgM5x6zzsn6wDUl4r/f0kGiwKirwZgiuc80MZAWROsA
DXFs+s7aot75lhTiRViHUW9TqYSc9Y/yrDQqZRIEAfg/G8PZDeErzDvhVmiFST+tVssH4Jk02iD4
5BCUJTmrCo5Bzh9PwjcGtVVlxtT0u9VumTN0PZMydknLwCR3/Va4nrQ0UQo7OMYmteaKvQW7NsSD
RgwqbShUW5MWnLDTZOCdwsTOLtzpjBG7btm0UvhmtKqL9LyuvFGaVra9jNwEbXhX11/DgWS6p42d
gg+htE7Qy5dP6PQEQHldsdOpBTUUZfQWIUW6IbBdfUyZ6Lo+3jypxibanO85hguN6w8nptshbotw
a+VJj3F2Aa5UO/8D4Qna5BW1QMMd6OphExYPXvRFw1jKOFHl5Y8W75F8VSPsZMIKHfK9y3pl+B+r
30J1+hrKVsf1KZ2q5cj1NFEltat/HI4SvfPRoXbkBaJhvQrevdCPOp6kXOL2hqiu5byDq08ru19I
HmoTTvv4SqEP3/qt/2GS+c0lPgQFLMQCcq7dW1ne5zFQivu7rBX5gmhqyDdIZq7b1U/r+moxUF2y
QoiwZ2B6KLW+Jrf9MQTVf9W7aso/9Hv0eyWE850eNf7Vre0BbY3lXZAwaRqI1kcRtvOiH7ZgN02V
KaG/SzT/qJpCtUEl2wFNZ9OXrAibXt/7sYgNV1JArZug3DNKlvvmqAY2ZvaRqNSiYxUkfupKd3d1
i3H5p2MihgjwYAkKzIaDOQRPQx2v5eCzQBwsRnVqPB1L5qcdtb04WCnEGmTHKaZiEmC+qw7uv94m
DrO2BIPKs3FmPtJ0E0nFft9SYJqzdrtQzeLIQT8lOwkjhD9vhahui9+RUG6zdhqsV+Vhnkd/ZaVs
IMVhGMD/wK8HpDxS6VTVBs2CO2X0/TFYQ8N+Bqg8xRTKKuDHZbHg6Z7+LdVUItNlNXJMEyfiZ2mm
NUBHwb2XoaQvt+/t5X5QFr8/Xw57yRkdUcSiSbj5sVel48HIco1F81wBNoMPk6fLrEMSUUwaccc3
QEEksEmFPT8p5UnU7LtgYalYAMhnvBs8oIiaQI8e3Qvmn0iEpVaYv1UxbRDvCLODS8NAyw29QHgE
tTtNKXRgPE2WLNPwuWwNCgwdPJaRA+AlxjplBRJaI5QZ+CaJbW06O0hneq5CJk9jPufbJtjKgUKa
LHV0dOkt6HFCiXqbIjZ4UeM85eb0kFq1hXbzWawZSr92odSPOtNgIEZAjBMutnv+nlG5DvT97VqB
lx/0+VdFfececFLo+CL35yOwAzUMFGNerz0aAJZ5JC0bsgp3ZB1caMja4SEFSpMhkJSd+NpmdJGS
KKDJ5+ockNBtwfyz4OGZUL04yAWceCltRK+qo/gL9ZNWcfS9FkAD+ShBjBhDbNs/z5wyzCIynpsr
eBZHaViVrvKb6sYeMHexanwL8+Mm+EeWMvjNZVJmnUntolGY8FuafX7hlSF+v+wliQkNtUhLfmGc
oq4Y84P0+uoQd/pcscKDfh0MW0K+BT89OECSWLrvIkryTEnHR9AzgdshiULxbQzmP6CdeByXV+dr
TopAkUslIlX73TnGWehjdoC3UJkGllx3QF7w0lr5bIMYivsvPGrtlCNaGy1cOQ9YS+FsfUT7qx1Q
+GjWxiEmcY+jqeqrL1Th4mH5aJvDfL69CfNHC8b8eBef3be5sL1CoIXaZva8hZ7bT5+Ykfhmf7zL
/5yLLb8U4ShT8Sz+dY7E9QGdjjirkBAMfwpiEBjX95WqcoPodfwhNpu5a8TNXwJ+Ca+a7hB7K24D
Mc3NLN2RVB879PSPvm89z3fFvzHItCBMu7VHSRTL6L4L79WpfOXljCdLhWsS4OYLUKelAP/WME8x
7r8pXBiNW+zwzFxiVz94qm8bqaZH4gk/fO05N9TOhgOP+POJ5VoHn2REws1T4pVOlN4/5QrZ5BMO
vmeKIOPpojrt50sATgAYV/rLE0HN0fu9UbI5PPkKd2pQBeNwzpVjpgg3nT2ZAXiy+E0/J9t3ryXM
dKizvfLKZZ2YHiUyx6Lsl1qsDbKHxjuSYxTuye8shCwKKYlaw71yQSJvET8o63Xf3niir9xPLCfC
bThdIxx7/SgjjbwyX1IONu+IAzs4FectiCFfsD56wHnBnZCND5KoukYM0VkeHJ/P+Y9cqzvqSOdh
Almlnf39wZCUfjunDmIWaOS8dw+7MWoqu0Co/6UGgaoTdwO9LiiLp/KzZaQnjVsGcp3wenPbEagv
oCMEI5iIpG4CiOjyLp8AL/K5g0LDmxZMHEGNRNk4ANAHp8J+13YCnbkk3dUwLhyXfFi8cai1ZTgL
P/2r4umypyxCUbfpexjEHQJnXuIrbvomdcwE49jU+VbDerBJyPTKidIFScBG3RqxNUl8uuo1jH42
/7YeyWTIoL7lD6L4bVYXIg2mCP+Llt0PlfJvrcQPpOrtIz/wwXOegu7oWCKglHIo0/jr0TDIA77N
UPCg+GjIQ99noC7+F9IJwBh4uN/BhDYMCIZsg5PUbM3byLEY0GgBdp2GbRC7taYeFTx1UN5PeCnC
bImLUdlE4GuY1GM2D5LCTzlz1NPQurVqvd6RdNxB8oq/ZsAmFlfN/3OjrlpMxE+u6R6hw3/VBzIF
X9ejzMVacvffwcCWzTH29O9WYlAG/Chw9KzPvJZDr9U+EYDcg/m5wTC9elRVB9ydXaekXe4umxz1
HEfTdd+8LVXAouRPPreC8WL69OGOLX5cn20QNXYta4C5uQvPq768DVg3Sm+rlBYEcVyOP5HPMBB/
ZI3MmyUiKXwukVUUod+hbjiPM94NeRXVxhmDVt7UrYCgQMOTGIy38q5hHyWqmBceuOQ+QDY348Rr
lhJ5gP7MLTWKH5zxI4ImVJAYe6dmBEZkLQ0TZUxOiSjTc2lw6pHx2i1K0WaVqHIVMz5p6jiIZnbI
1L4fyXToY9RmWh5nVrfPZh6ZIXIYm7rJKeZTmDm3WWJ1iFxC5oyvruN0C6sfd6P/Tb9wEBh2DwMt
8f6KJbrnWNXIWp8YB8X1uuarMEAtqJ38cfS7hhNThMuutw7wELOU/6fl+g3S1r+0JQP96WYCOWVK
dmraGf2nzJ8ojkgAVUWHhsYL3DxOQAaS/TZJr8ftP5Hr6WsKxB6gHk/nbIxxkOZm5gONx2uqRkNK
dyp9t5dLqh2dqBpwhKNEsktaWApC3qMtWl0HwQZiVMhjrD2y2oASd85HJTgK7bVXfD0zCHSp+5ES
GUoIK2RacOXAzfh7nHq1R81M1CiYuzr1J9xTbfnt7Pc0RgI//bYVk+ZQDaglXOpa39Ju3mhyfTV/
yGrhX+T+b2RtY59S0S0XIiM2AZ6uSIB3ndLIyW4KaxNfNRUFyx+JppSpulUbhtjbr86BA4VAsUKz
/xwulkManJVXeCJfWq1ZH51YAqLVtCYi2vAUl3+8KmpbXFgqFDh4RpmFdpOT2pjmvaJabKyiCb6x
U241wn6sXiOlH2kdsR7LVbuRySfQ6orN8SQz26SIkYoxMlTlFbxgdnCsHeCSYIWmcWAvOXe6f9yX
NZZJxJuCMFkHIFOdoZbykoPNEpya1Gf1kp0dbURKMI4uD7Zju+/nshJkDQ2EDASSf2MgJpoF8ob/
fKnPHQjf9GWH9NzPM+xfamFUboPLmsCwdhI9Od1GH60xi0tZkHjwLEMabw3nzoJA9H1ClpLFL+pt
ERsqgE0oK9xYnv2/qUqabDhjvLU1LqPxSZzQB2M8ZdzrJ3eWih1Hmw6CeXB2uWQ0lCzwPobLyB7k
GXhqtx8WeHolZs8ms6fnylSn2a6SjzcarKKq5fR3l5fmnIZCKSqpSOEJm2vx1dQB/62Zb/eqV6/o
lwig7ZLRYIyp/CbPs+tMmjWc9KLJH0eKxkYI91W6LlueTPHCrwpx2ixNiEuPT3iA1utbMgP2ygbF
Slm8tg+C4EhZm1IdTQ8IK+GLo15bvwb4xbtZ2KfsJJFA/GFNszIZfcP26eutyVXwTVExpEft+jOG
tiKzb7ihG7G/FaC+Jwj4GFxVyRAisl4DbaOzJOp1k1F+qVzWgBqfcjpjvaIKdoLavYbELzWGnC3G
8V5A7A98EdIOAimzwa+Y+gq7rFfKXgwTyqlzBAiHA6/tfoR9+PdzFsBuIRt4l3zRokQ3r6kxJuAW
3dQsfUY5OUFvfQhcYDgMlnkUZfc4nkg/H7TJ1NAs7C0S7+I+rYMMecUHI2N0nd6hDYuDsM9G3nrn
RLdfJP0npsbWVCM4OnxjHmnvyd4KjNvHEoM4OzKfEYQYCWH0zIDnHVITaeqa12GV0CK/kuXZMjpq
rssrlxdVZ4GHEndhc4EFI0EGlVT/MGqEl9nGaXd8GhwAwiK49jizOhKdLdXNOkReLnHoOKtLx110
y7DUVpgesUeOQ58XoI3Kl27jxpUrZ6sYE/YHr4nEEI19ChBCgowO/EY//PpUMZ+HbZPe9/ExDXD5
Wxi0xOCbC06gpKLuGhwSGE2pzs9m33481ubG+98aEHnzpR6SrGnU4SSvQunbdzU01QvP6OA/lWHS
Pctj/4l2nylwGVawRc/DmSixuaJ2Xs8uzmqdrevHxJHdiFxOrJ+baItv2wddkxEbJSPqa8oH5Puz
MifQ7tW0PDzQX3xAs052zmoahCW7HZ5rmryAxKunHpvJ2TM1iNjoxv3NnwKtnBsc7KVgRKRqFG0p
XZXuy0hwixo2pQ4gsIsWhMU+DrOj6n5gXmEKaijpiTqdAQ+OLJH5USbLBpAtkekSGFcr9TwzP+Jt
5FjMDAGQbOPM1ITosiMG+8M773HOcQRSjibnieCKvSLMNAGwPht1HOwL7Addnb4xsx7vbbCqUWNU
qZe1cVRuOLvuRpP/IFznhU7sPOaJoL49QCSjFJ2BaE7QIvYdh20Dc5I5RnKk4siklz1s3YLljKng
Xa6l/lZFzHUzutM0/EZQXasAZx76UzRijeyHpDXf0WSIB0Xco7irQiF+DUykEl4KN1Z6LXUK8JyM
yaZYfMbhifxHpwxr7kVXVqLBvjCrBKses34oKq3AkhCH9HQ327+RcXJ+uJxeRaTccLNdeSl49DeK
jF3Upx1qeOZgMP6P4DZb8QyF6JbnHlNEa8xRbM0NmVpkSJznBEkk6X+PmakqS7g6GbDmjJlGk46H
GfdHlddv9MW/1/0UQlr2Krld8guPynfXy8IUupNwm6epETD44nFck7dwoSqngaVsAAq8HCi1Ch1y
L2hU0f0GlCRyhkDJyb7g0Uf8kqmMPZ2qbjh9hWQi22FVjbpjhXQwnQ9TRzX+dEZhqK2h9qXUnVaC
+u2Ykp0WzyROatpEbWEa/RVYrlQl3E1RRWsgb9HHxfqsdooTxsYE9VtCs1VWfeyxiNBiVvT+XwQH
LIQK5kqasBq3zndKpn5NULXhGZ35CBEPIKGc37Cmc+Cqrz55E8G73uwGSrUhLxpjMMB/XnKKmP+x
C+JEXNsTcbhVhUNG94cLZfSqHRJa/SUwGGIvCYU71+AaVh6ka8ppW4ZIZbcPMNfIIxcCUUKpYtPm
SfsCAx22vHEUERf1Av1iddSzBTGtGte0+D/Oc15X2Hnbc5Kun6o33HLwK8TAmZXEfxAzsNsoPRtG
Xyg/7cL4mTlzfSXKDxjZCpUD4p2PH71ltun1P8i+lojWFMOntp7s0DBJzyw+Ssiolnn+fH/wV0m4
3XiG4qdbTjIcLuEOsCt2SbSVz5UmoGY7TeC/u+l3AIpp6waa6dSkHeDf8BWMMf1GpIdeLP3qUA4t
WlO345cTQd3ZMadjcX8h8hJRtbHy/H758coocxdu7YgfLCQwhzK9h0hI7omL41flRljX5cc4+/+8
X+vTuKKZzHTghNA2Mdpg0FmIW4n+/FqpfWyqi01A4wS+hi1Dv8/heC+L9KbrgXW3jBuuEmbTKIAe
rOLhQnrF3iRWElIO1rHM4+nF+xQjDxUa6KOuCDCj9+r3jtVocoXXvygq/tYeY+TIqLMmQwK9RKUP
l2PS99g34pyOAvV5hlzfk/QrSf6SM5sBXIt/C7FLYBHy1pisnOiOqrt2qG486coehun2Glq79ePj
2QZWHkhX9AgR/znXg9WiBhBN+1wIqDn4CmtyJrE9NpBkA1Uuu98/ydT1qQC6cxXIwuz+x1Oh0gJe
IdZJrtlCPTTtfhn0bZEtLPMcvmCqn0dMedWeSFofvC/SBw4Mv8lbIaw9k926RLdlhNJ7NzMShlC/
a+CD5LCfT86xVSFnyatXGwsHb0UN8A8Z1zLMYo+Uq8RStXy1LQOFoiuyimsFE1iqv0hE4JiUpRyw
CzqvfCwo0jGtioYrya1jtsrwrDr5Djrgd1BKuuvKXOrI1173RY+pDUvsxpoG7pl9TzBLjCPFg55Q
VUd9HeDJYb9dcuFD/BTNM5LVeJH2z7bSB0sFUsdGm0FY//9VRkA/yOqlnj28DOGY7ZCfUX8Ppjss
xwNg9xQS5ZXi9pMFCMXD80VQ1SMBTbbc+da8NE15qjdu9hfKAc7bzOEslsKRoUK5+CPwW2DVU59s
GmKOtl/Q1klKkIUtMAZq3hNQkvNGLR9fDfBiAEZGg8Jro3lTswazFpTFZjocTGy3qHp5+75+isuX
6j2hUuhCAmfppZiwhe0dSKTGzCIeR5cw1/VGGNrI1droH/4Em5ll+DO0EK43XETi6WJZPzJbFEkK
wQbTSXjscP4hoK5eG1LDxQ0aAelISQc9OVBit8vPG/XZAmTJpCUQ+9926o4qt+ORsUZv2PySM+xJ
1QV9lY+9boHRe+/ydLy3HuMwbCVA3QkfdF2IJdRHRuVh/6z7q4GIy1rj2D42sHQiE6+qIcvaglx+
ttPoSwTwaFs7zVBJkPXWDIURtk9HI2RZk+GEFkHxByWfqqUx1VjT4KHXAHtuSBa+qiN+3jrq04xk
BqdUccfyJGaGFkOPDSOVxcEkInJct2RjK32PuoW51xAfGeIvdCj2lquGJvx6RsRtntvEYAhkd2FN
sKYFsxM16OhRSWNDn+94/0IJkLjPHkn1PPLOiEaHpD+VlXcjwJNFObVrODOPwMjtZPo3YVHs9Nj0
NAsrTti0gLto2C2GZXN0M2AYOlDLPQOBIVc+yDW3848Cj0v4pqOzHc+KVx0suQLT9FwtxCmu6uLF
mWe5F7wfJRcf4pm58Nnmah+QedaEtZakEFdEzboZj1vAaFGd+4THQGgVk6nxKm/7DYHLZeryCE/4
X3X45960gmk4Ci2Zkny6GNPCNcyXGMKIefCeRWfjWjB0w5R10OyZKQY6gB942g2ZdchMg1g4ZTnD
AerVU7XEbhhOUCi+cz5CujoSV2AdBxr8lQgtDFZzW9Geiv49EF+jK2fECOJr7Q84eODqRXWOWPf+
CImnZcpiS3xs/CVZlfqOxUASyXzNBfQWQYWWPNE+jLvr/1IAspELAPIxWqif5exu6SCEDqB6lEVb
x3R6oMdGywNcA6VxkgRFIrbUFDk3SVOPEaK+2upwxOMScR/ioec56cwSCWwYOrANsj+xdehhSloo
RtO3MtR41hLp54+EMfG+Py0PWvwlDhzAxK1lanUffDLXGz0xw2tGeDk+xK+CqCT7gI7OdJlgFCWJ
QKbuLqnAxu4cXcFk96e1RgXmaZ1qROIXHkDB3d0nSd0+5/MqrRS2ZruqnYOspvaDx4HbrMNZ+jak
M69LScU4kzTjkqEU87dGAqR6lyJlhG4h87zDM/nDy2XWs42Cb1sU3e23wB6Hbsz9tbaNIkoj9v7B
IQvWE/w1BWQOHiVMMNrk9S+IB2TDHrpgsT8Y4+ckLnXIUTwI6MqoPvo+V3aJKGTwsQBCTNY6DQzO
q4HP5tBH0Xtv6eF/doYkZ2O3Kczs7NXCccb2pElS0EpKFbPd/4aY6dcPpxFSeo6Rn9nkCWQNfrWi
BMyeTBrSJryy2gXsvnb2f5T/zqBszaLqoF1NbTbRSSdzAJfS/W2Z0K6is10ZoY3YXfPhgT55igzc
DdkEeku/S+z8OLQ8VEzUJD5H8a5y/g1rkMb5Q5lhE6g6g8maDzWgdC8Tb2ccvdLmg605cghIqxKn
mx5D42RycRiXZ8ZJEyV1JFFJvuXcqEVO55sgyhEh90vlyW6Z0esVMiQTlsmDyDwCa/B8exvd4/o+
UD+AScKtycp/KYWe/gKXX8dgfbKdCWbsh7/7bj5+qd8l95Kp8bUI6e541EiPN/sVfvUW5KIKzrJw
G17xMD4bhe+dxThVmAfEklm7uUvQs0OjcFOHMPnYmy+RDKIlF1iHtK9RU9TF2k9UDV2WjqVF8J5w
S435G8J6y2n04WW2zcZ/R6yIv5e933JFqHg7PiynEW2sWd71FJqA80XBiXyMlx3PWYR3P28u5JgU
vO9l4eUQdmWGG0eNXBdQhD3CB6qyPW8DT3ZvykMREU3FH7ERo9qw9cfMmutVNfB3obscqDrdPRNb
w94nOE/juIA/7cPtlGNUlKZGcuE6FPy5MhvT1fMgOjKesBiQ7NRaPFsaNvQDJGz09sgd3wBHsFKk
vHUvLIP/gzC1JshmdnoDuowfamX/wDuOFjtkIFR1RCw62X4g7Am8059AYQfI/iKMU12Mw6ZTlo0L
NDCwN/qOxwLOotjMHB56H1enYNV2p0xfRf0oNUnArrnzVzZGA0r3IgmzRLkRYf/GqDnrwUvIGy91
G66wtwuvOr7+H9DJFbNeOLD6QVerLIDbGR7yQeo9KqAECqtL537zTCY0bq3IgNl5Vz2phsYfZSHW
dqYCymsJfgK6wR+IECTquSHAePiLq0BlOT4iZhYcezQfRlqgWm31BaxOUNt4e2RHSNA49ItaheSB
raAIAMYJdMg3iyx7mxiQl/1uimOj4oMpxyKkPLtoTNPFtpRxa0MGjIUO2q/FmbYtLv6UggjLmpw4
W5CYa8sOPUF4suTY+UzfD6Yiv9auC/p2iEnBwI+QMx7zdxinjhkYMrp87hl/ysJK98SiatMegNhf
80BMiVi73oV1Gz1hcngRRHYX4iV2u8WDvbKr2ul4lpuyvkpsXnIoD2PPcxtC1xQVu0ywpMQY35lK
GJS1HUmghyNJtz3wE41Fo/tEeq7bZuRszq3KNr58e2O6qDEYizkEX81EUXysdHUyE/ul1ZhJNwHg
rWXAIwP9m6ZVg6qrhqwB41iv2oqBNWqtpQoq9pbC9vlnNAwYal7zg3WN87BPHH2x1g4eW6owG1uj
ugeV954Nsalyk9IHDfwiPQeLh6Q6RYKWMzFtRuSQgfW9QFrcOD6gfS5YGWKIsbuGDTz1Dc6sbWD3
zwExL2z6vVTmBAbbVQP6k7jeKAq83JMOkBd9QkdFpd/7+B3VjkiUDUc9watr5Jr94UiJq1TE74La
8OTTGxtvP0t/kuh6aF67J48IFfd4Lw1cEqt6ttyLoBC7eRzPbIMGnzmbu5nM33lbmoGt19pkhih2
W/urtYfgqjJx6xkUDjZ+W4+7TGEnwbFTmkhDUe5OLYywj6iewT5IFZFUudBti7S/6huNEpNUxcrL
Zg1KIGaD+AvXkmGhdzbdvcwir9BD4aF8r/PBYE8xgSEL+VexjYg9vpAWRdkGgOK+6PdaWDBFes/u
gfqxHLW8nVvaZx9O4A2vFDufcqrWGd3dDG+NjYHtT8uvZqfYff1td/FyH9lL9Yzk2j/uYq8fnMi/
3GaCBh1bpLFu/OjgwwNF817TupMVhYormqXgTJxAdxQBZ1wYMFBhn1Yd3IIltv4JH48ItKn8GUMS
VyJ6iah6z+2lXU3Z/0guoe88Kx6AC75eRBRlioUACaMwkBTJYCcso3Pk5W4xWz8uzY53WubtRizW
t2DP9udjAbDBgMQ7+uBGEDsfYLAvvG20VMcFBFIZ/kwTR2oVwUMGL16KUFHj9VJrq7XKUs/4FBTY
r27Tyr46Q28gb/5P27HJJFmlk1gSdUDUdpSXXKbBuh9UcIP38cs64q69meNcwtfKrhd7NZXDJEXH
amySjsCCsK4dtXleT4RaNASIPnespPC2kHn/9atEfKcET+kE90auKckotDCS7lSNritEl4v7J2K0
Hwng6fkOzGOobft2KQkWCKkJV+xpHWSJcwhxebiTwSJxQzp5hWo4DVyYN6MEKaPZohac+LaVFdgD
fNsAN7P2pRP9ziolwOTxrFSpm417etSMdmv9AylsOIFs52UdCi3JJCump9HUJsD2X3fsIvSDEfiM
5lOARkK8PVxmmY0ca/s6qoVVUsZ26FZ7qx2JOT976kzknKF6h+ssvY89k0aoADHtohncnbm9PPKA
h/4RZeUtMLfc41I1bGfByVnKh3uoi8yOp5iZz06NIiAwnVFu9uW3wBLJA3gBqAJ7Hj/dGv5acxqN
UqtzyzxwP4PY1yuuY8u04x5tVgdO8vXHoLWqB9FQnkIEQSn8o6xc0syAZbGo/sR5CRIgpKwIfoQ8
u2ryGakEVopDOUJwFnt5aVZSzIv1h7tkPmpAFi3AFXEN+32WNA3XwM8+78YvNdwn+A8afILGMpdO
WfaASVLViDbyrjJACQ/SEEI1l7NmsyAbMDXzM9/3y8XNfz5HZQ0v8ELd9WG6V5pwD+SQDBc6b0hC
JZ0VwXwt89oJpnkC+/ROdXLr6APhcBybRiyG8vyg/iTyJsuWZZX105TVm6YtTAR7LlsklBpyM12x
+TJLgW8Z4YEYgtc9ASozoQ1O+FIRNrtTXBcQ+t8FznVgc411/ii6ujUCEJVMoqKa3QLS0xx86frK
KTvB8JclYC9nGT36ygJMsy97mc0sKetnffEexBilkRmOV6BbO6VsWbNnnTDO8a6LEQMv5rHOmWpm
Q6sKdC6HGUup3DcS3VfzC1GATkn59pBGug0T9ZE5ltymJ2a5k4ABjd70s+/qXQyEDPBipOvVJ5Ix
nG7OdjRThZ27zH0xksmtRpZ/8cBlq2+wZK61RJmxNO4SdM5hetiVWknJrOfsSSdN+Fzp3/hkBvBi
RO2uGMP917is2WfFLyiRLiJsDIl9p4vzfVO1YTbgBNHWF5rCY2tUynHIqcC22IlOPWrpukg6xj6d
rBSnKKuY6Aiy6/Vvcihf4brvd02iwXjy0ACKkVmJvrugF3jn5Z1shrZcVAhwYV86Q45ssd+18VbT
wqcs26wLwrV5DYGM+Dwd4jteL6TYk0BcU+DF6n6wqad6+1vZ5xMwej5wjnLI4CAbn8NfkjH3Rhk/
h0pDRDNqd/9T19/DgLKdIpVpHH0Y5buRAryjdpralb32MdLmrXILkABkuOgbx12f5memi48LVOyI
6nPGRlQ5bBUmffY/r66c3gb3GLBLOVUmEebeVjLcGfUcrXB0Gf5F6eK68QvOz6nt60HSP9312KkJ
RV8cua5ehIVT9fmNsBZJywFballBMihkZ15w3T/ON9n/Z0kyGM9nyNLCtxvT5o3yIX8Z3JY8TV/D
8BRpZMVV799s+JH7FwcQFY73uYtPp/VDeUEAx1f2WAweRf+8sZA3akD93fe9YrXpM+6G+FJQuY23
NXB99cVsreLesZWgaZ6afzlnLXzKPK7R22jVoqe/jr/PB2Lyxxs2qkzwO1kvbF85lygRL8dLADPQ
bHTVycsT79gU9VzTWqTEG6Yx1WxVvygOBnMMleYNYyxtQ6OJoMReh4LmX3hP4WPmbFjc5BPs9LhL
6pOJbsOq2FCJutO9zS1goW0UBfqPWa7KLEDnY/SUnc7I0mRvKS4xA8EaEqZW7H1dqwHgII5RNo9k
V5BlJ1A5Bx6UOILvReuap/1fBC6J8xMvnsdGK8bPu28l7lPxvfNup/mzeevjYqmbF2Z91Ld57KNF
1tMSh7YzIR4uknAroOMaPbCqtwye6vWE8Re1ZUJ78R1a6WtHEmPIQCYeqUQW9GjB5zMdhZL0WPEc
YMXxkoFuMhqCzrQkYd7aBpTaajlq89b1MMNZwDkUWR2pcN7aCtFWrZLOIK1PV3iiTvFwnIQ5Ko1J
rAsLGICSSUhiC2uWzAiZRqxzEOSwnidLBQw3LYezoNvpOEROCStSv/iMzD9RfckL3DTSy9XtMtGG
Sr/Cm9RIUE4OUSSGchrZdYWjM5ahu2/QOLxMrWFK6L4UUOCAbZM3hnvF4bxhqvFgr5zUHn9ShVQK
NOk1Ulqv4tdrcuM7wokASfKwp+1Syj8VSzAA13xt5pxbDPCLv8AI2evckvNL/eGzcJpsMxIKU+yk
dBMid4aZei4qNanWqgGtX0Go8FO8xK1WZgnfmKc3G8KvuDCdgsHelCNIiDigf+biq0/Ns7xtodve
Lnq8tWIrxMUKKaZe+9AqBDIO89F9liVpCmZA9x/pGUbmeMeEkAfeJzgVTmgvczPkfmfZzjKgKRDz
bQITDGjk8bNPm8N+KQVih1RNkRwhH4UQvf3ShvRR74uZbJKSx1A0OO2Rwe57s38I/ycmSW67jVKa
occVhcgRPnel8tNDfgcRIrPUfCQ7y4HWxLAz0S+cw7c4AnBzKjz+XQ3lnAu78llkjdN+2R2fQRCL
wyS27vVZJo4HxdaIHUnmD0QJ39qXd8mkBH+ypLj71Wa5WichVCs+/Y7SJ5cvRxO5HHDSlCBW8VqS
wP8vB2UCpPe1ObwccwTrZKCzbSyTHK9ot9XptkRu3pYo3OgYEqXuN5lkBFFygwjISXMrskJ8hb9Y
pe3aqRpWuK3NMumEFckWx71x1ECLnP7JqdS6xXxL3Uzne2A5YiRHTa8g8VATIdsZKeKqweWmXI3z
wfWQ3Z3rPBkBQF4YZnW74hmXsOuo/OvRylDzHH3mZLpyWIwxozv904aSf3fVREHXMpExj6zddzdC
0YjB2xhy7cga96YIWLJV93s3McFlWyuX93E2EwWxBif3Qi0Topwsz6saRx4evjGkNWtkyCJ3wXAE
btTf1A5V4OKgk4ibumoshA+R8x5ZTjO/2LCtQ7BczuVFSMjMjBrftUwz4LmVnJDNJ/dUVByXr6Fr
5zvtTXPZY4OHgw6oYYxnXepfjfUxs0LDOOrdApo90DsGClfCC1kYg49xNeKyOsaeXcXaJ+j6jGHu
+V/RHyGaDq3mH4nD7OhBxz4jw1uPLedbIs5yr4bvurRBvztf04EaJbe64bRXPSAglIRhH/5cBddA
qmr2pDTTYEnXms/DKANQrtl0KXCZoViZ6Lr1GKL2QezYKWTuM98ypbTC6X3xOrfLMf4ZQhPKahr7
7Y9D5yINd30+lkRh/RAT80TMpDBl/2DrMfyUJUxqCr75nCWhVsQrUtQ+IINn5tyBokjkKbJEVMS3
NeXNsn9DWNLGfRkG0vQbgOl4/FS1wwzmGxAoET8HFPtlnHAt3Efe2MNvz6ly8zii9JKKbRS6ZKhe
toxiI9vWKssn/l2BZPGcAdvIR+mqYJhuwtpmCAqLdgxYVMet0UV8nL0GWpISdsvW8wUFZ2p7tVFq
SO66NGvyKjNzfUJ6YoOB2qdGkMPZoJHZC5Qw1uiQp+oeAnr6iWJE4CSRA1Or6tWJxpmU2M98srBH
mNCFtD9XNeo/6AYGYSwZ+8pIGQ/8xlaR5UO3Bw2CvFiOg0Ga7Ga3QCHnrnrb/ZgHb59hou8fn7JL
30Klb9c7UIFnqbBAbT5Os8v3TnjDZb8umwATJWfqh9oF5bur9RobSBW2UbH+Fhmfa8AWtXBlRVc4
/73rikTf0ZzZa0cZl7Wuy+tSi3CyeCW3Uyt40Ym3aqA+HhLPey8rWnJj0y7g/DiCo3qbYe7YdU7U
ghA4i1dre6EjWxBjYf4NnxskzApKb8l0PnZ7YIwBIquarkFNnj9JYb0I/xRldhAEjOhu8Sn6Gm7F
m7+lUuHvHC17phCwra7YcmYqqQ3JXZzAm0T/kq+d6RgCEGHBIPOX1kXtsfN/Rc7XmB5y8M5WdbP6
zJgkAfsNG4jDoqYT5CUuu3xrPYACQMQiS1SfbDDPQjaMKUQXn4cMm3n5TVd6ZxWzGcWXXJyrwc7s
4cZdgTg3o52S+VvcBZomKCSeQJS7mUiELmexsuOUrumBUMya7KRZvIVcMkNuRw3rmwkWELd3yz9Z
+CVgU4RoC2+8HCu4B313+4bHH7Ww7fJ7XpqtWAPH47mWTjrTUqEnK/HCRI2KsbVE4bmIsP/J8jmd
sfZkoMzYBZ/pX7Xt6qt+Spl7yu3+cLJCaNZlf7kMhfngbaz78P5EbPyPqjC3l67hWbaTRPbBrb+H
JgrYJnHtUHmWKRIKsjns082057WmYn8mI96cM9P/BFRwaIALCac7Z+8xl01UGskmyRvpaR5cOxTn
TiGlolb/YNtwfKehlAvPVg92rcPVBktmVS2M3rlc81GQztuEEgUQFULJOAHSOc4QJ3R5645ubGTN
5BL3OXbIq4XjwN6D+Cpp21cDFfUO3PeLYky+Ojne/AlQI2pbvyv/zdZn3Q+/W+lICcvG8AadU1mm
HmBkpjIrk+XohBfuoWo3otbZbe9P9de2nFapqJT4DeDEiQIqBYIGWDMlM7dL9C2duPgmbd3Ph5PH
SE/8zliqsFshTkFetFTdbs7wB3XplQUIEzkYRro0MlzTeYgyU8UWW3NtNantNw3DeAgMKm+/Bzqt
+ks3kUryGma4TW4KNlz1pvLADCcxba5AplpakBT0CAdhA5zDWkmTWrFVJ7Y2xMuJAW7yy7rJTEeI
KhTNgvHcRfTbkZaiiJaRT3YKYELVxaoE0jLNPqUUpxv/BrfyDzqRH7NYZ8wlAlzvZroW9FEt4GOK
gGykBNVYJqgeAo6LM2YdgUsILqukbO2hv+VpZcMBXYHYwgg7dBabPgA1Qkp96/u9r4JX+o+sX57H
Wg0iI9Y8v9XPOGNnh3P81+jmKlaC6AGvO2Yzn19IqDBTuMSghX0d7IrHYIetywi8WKOvxDyNeBv1
8uxxVO+S31RN52yOeyFRssBSYU1MVAaCYczvqydZq1t5cRDNQ5hEAwjV0x110wtrPJkeM4M3cuTN
BKQWitsCMs+FTeSdE6i2jHMtRaAj0Xffop425Mfqp2Lr7WHK5mElV2p+IEnac4RbCW1BnrZbN9N+
JwzZaykeLxY10Esy38a3WEV0e1Fykq3KpUUScVAWRcOBBg57W1B/QDlBKPjdy+G1LtgAQ1BIgqUe
afgKE8VCU38kQ6+wZ9/Z057TkZ8ECz7DRTCygDuNHy00ow+cwFQH/RrocGO5QQxJV2/gF1eVe0rb
1fdmneEg4NCA/a9MPhHWmyxa6/H+6rRzFcSAj8da2dZ9DHXf0NgRSI/tt5souALbaS9xkvwcUDp5
pdEjzSFA09gZhFi83Ni5qOYAbyvWZOU7iTQYonNexTYlr36Pou3Vdeep4CLnuPmVn++NShXuYOYD
ekXWGY8b/XwWwQDaL0A+pxdQ+hO4vQ62nnipG72CEoKgKXnPuW2TKxd8t22sXpViUCUiMk3fP89t
JwOipgFdABBicsvFHz3Fq/IjQ26tutF8gKH9b6z/CBjOj0EQPRuzsQVFgrConwReNZF18OHJa5X0
57Mks4Zp5QcfOzcH7cXyiHSYchviC5ijezAFFk6UU4FYalWxDUVxmqBlrzcmT9kQj8PYXV84Nk1G
02oe29Mdvko85BOcLLkmRZw8jyMmkf6RceH2Onn5ZfLCtG66Ptrkwv0GGvvhWVJA2mnzeUmG6VFy
QDHlCFgJT3JAzAafvlem8IoRlEvf5gQq6Knb6bTi+lpis11wO3bLMAv+nVpAGw7x045xYLNjXEmV
JGzXFSM+79MF0EdNbM5jofaJOq2gyHuJ8Rkpl3jj6dhKEi28xRuh3+aNySh4ohCdLbzBh+WStra4
h8E5BStHMdczlZ6JEF1+o0MrYspkXT3k9ALVLwajGDIow9jlPZbUU8nmuGQgyftSyCdjNTikj0r/
g7a3t2clUOyFUb5if54qMyx77l8yK99qYy0PfvDulRGqEZi72v/dOmE8851ktgsxL8pAmF3TnsFx
t7TWd/1LigCIo6ViZeu5toh7BHhO2bVdFDaUjChiXoO5TqiyFyI6Kvbiq3LcJZK21QTIXkp+3WYe
StDF3bKOPP2uc/D4AquCU6FGkOS0dArpAfcApFF17J7ttPdTuCWCiNjptnUmYDB/MLRwHDXYkU0+
tnmBln9ahW13fPquAvuq9RXoT0XO3z7yoyE54Ahi1UEpD+Pc6gvwRR+7lF3O4c5LwlQWrzOAjZkg
08wWUu59uhFC5jY18r6YSSk9Q9Ue/d1p4pIvHUqy7BeSnDi3zLvM1WJB++qsCP995Gvu+tCvs+9d
0kxScepcxiOZi8wuX0xTyxygTXxgQuKbreZyO5F8ZMiqBXYXVI32SDxKXLnQA0V0G+HGjP5FyCW9
6yGSxFnJGf9xj5Y8FiMfliF/PO4DGHS+k/K2OfK+V3A0fOj25yJYrHYgHlOZ8VkmKr3EzgTYdzSW
x5s6NWZWv8/9k/q4SeOLkxtQwydHb6VQi3LhpQQP4Cx9fpYYGb8FmX5fAFe9W1Juv1N3QvUYFHyp
v8K2BvUnZxONh4KBujy0JnUJLNiE3Grj2HEDcJFO9mqFuCY57t2mmREW9tnwVLWlzlDx0h+SQ9iH
yskxMq40BVlVheNxqS5Db6Hfffw7FlpnPvtcm+TSqC2Y+YgZiVu+6aHTubpWVm9BRpSOWp7Uv58w
Mq1LgbuVKc3tConRyof5SDFr4ZWsBmzjsju7/VVcvL8c158wZ2Pd2Ht3veu+MXwjPFpGfPNtdBAo
tSk5r0jbDQFO2ukKKYoqOGwGzags0sQwjenla+R9UxJb+EW9nrzLX7ANGVwJ0Ou36gQDz173/ak5
Aasc2sdjEU3MdvAI1kZb8fD271uF8zJMC7jpMpnj1l5JO6ZodJo8NFJc9Jt1Z6eLWH6RbcmxPSJE
78ebAHEOn1JEDVcxN2j92NFqHnx0gaW/yQtX+RIKD98ElA1i+ldC/pfpT23cS23MGYMVgCr+jVgx
s2y36Quo++FJNT9AHl+K8xCD9tQiyAvO5Kjw4Mrn9I8BbT+8pmnwLvxTAF+bywaMLN470PLwYnaz
TjRSDKEWXqb6ZxsHKz+W2aFEesnLzFOZSFWfJOjSHGPbJWIwYqF1fz+H4OW7iwzZ5DnOy2oYGBJy
suL5LB0qazazlLnC+4rs+wgmQdP72sH9br0JpCydcpSuTssVnw4owyVgUILYpzydSsT3nRfHw+dJ
viFUOIZ0yPCLxXOTIEKFze2LvQg+IqQOJ/JfFIXkzzY6O8COgA8fnB/jOO5565Cnr7ifB6/0MMdY
36sno9KGPF76SX6xZNH+fjU3LQVb8PXgK+kviaaBSk8ZoPhAa4k7xvdKvuJkjcwM/iTnWTeC7KoD
BA9JUiF8Ye/2zPp+5j2RfzR0xI3Hnh7HjuEvITgBs3sJ8SycAkLz/ya8LrH5p1z91OUBPYEMQg1S
iIWgnWRaJyHS/JdIvq7k3mxQLQ5vGnRu1Zhp8zrFVgoYOsAnHKrIAmbIBVDtwODaYy8gLj9GCaa5
nHwyBHMQtd4DzR4xQDq77pdD1eKK+ee/hmw/JVoucit1fpVmgAd7RBjYzoVFWxyCUTNyJG44ypVq
HDl3MVdgsSCq5E4oyoc1rCm8tI+WVDL52Zj7MEKMq6LhFJOyRGYafbJouwm/JHjT8BhHR4YXsJjO
tUYHu4b98zlsj61q6MIQyW3vTbj3mjiOL1ulrIFbTrx3Vp10S4W1NSBBrOmD3yMlQnTaK5qIFbO/
3kjCtqA7goTGVaFvR9Yf2mgkT/V26uS8acwvd6GfzosZyvYxztxWbkXBW/B0DAcNjCNa88CFt8Z7
Xph3djOj1MtUr8q/fxuacQR7G9hjHVdgX3gTpYWkm0VRU6W6oW8RE2oG/tJBNyg6gRxBqgeB+Udw
xg4wrKh7Cql5czmDwhYMMsH6KJVmZo9IGp9f0uJAnl62fgmLnijSGB3/QjNteM8ADbsl8e+sFro8
iS9oalcfCeSyYXc/zirx9G/CGGeKd5Pd+6at3wV8clfTOLjXR43GEww/2qi/FSZFtgoXyeU+JOaN
g3s0SDNbSBhBc639ZSOwOZ6Uz2qshYlpis3X7z3X4EnDPL7HfpKBS/lx30ZiwA9n+IuAwwG41ZmV
W4dfFHH7bRRYBnxmfL4aO8OZAV7lLETe0BlarK/KedXGVBavAJBYfq8uVumKhFCTiccdDLwdjmkI
gQg8Jn/fUfEryPFW/mOxVcjKZ0RAaf5YeXv1K015gB4ZkTXJl43300FXHWQ0/lHkiSxvIQzLlchJ
2MeU12MvKmjh1xprAwNMk7euMQK6NFF0j4nSgppWqGuORv7vfFZq+sD0ie7nuup8zu8guQq3cfYm
MVXXCNenraxBPipDiDilo+JHSs0dWk02UV1QE01hY/FBWJflINgqaMcXIUQZk0sYSPD0fFphcf90
NQC8hYhfezVejug3MVZ8aLQ4L7lkhdcqk8lcUlWrlAUrFDJNXsMDRh/LiQtkkcZWfPezw11axrLK
wNO4gtNHts5v4TpI6NtlrSCR7e0WSNh5yuyTMTOepjOdt5U5TbtGVe1Tw4pc/EgrjR6Uz1Ldjn+n
xmJm9om1WzJ2OLhpBegND5C7CJek5yOeu33hxj6y2ymyR2sPtbQy60qU4JGJQTWa9upJ5unVMBsZ
Q0mtZxlle///vYZCVpuX54E191gvPe7dxxTYLFSK44avFzma8Yan8PWgAB7U+0QirWbaNjJMzJwz
zq3Fol6QBN6qVyx8Zfrazu1LDMC6Twr+tFXd/mqVwsG+2U3lFMjAE9T8Inz0acNoksEVYmvFkYvO
vMm9YgeJTBn92LrvC135xil5my6w9Op7GttgwiAUITkogH7sH7iTZQRYjLxZETZ8JyX7zA+7zXLw
Ml800JOvrrR3AiJDnLUz1/O6Xgv9Wq3QFOOga0PnYWdELARK2qxNkvkl9Eu0WRS2II0Oeuv+WtXU
2n1s3f3jjMA0BV8bAPc69I6nTNOaJ/Hq6VRJumdy+rnBRIhoFC9PyOJmOr3po/95NIJA4hLP6gcM
E0bTUfBcbnNPAuUl5H1yleM4G2F1CP9/aYdOTV1v+/FTADAQ9p67zSNqkU26Wckpw5hu3Eb/U2ht
8ASlR3NYdxk6Btg/+TU/jAxo7jldW/tBsEg5jemfKNLSJryBSuh7EdYMlJ0IESSQrXeMHJomIlHM
Tsp/z+YSShJNj+WFNAk76zCijG8AVtLtTbHrbMlnJmnuLOG/SXbwPr2RFI1y/cJlq+E4me539FvO
FZ63PMx0WOsKFQwfwwJygYilqm1paekhpJ2JW6Gt2m8xy/Z6EDgSbp6T4HAt4xCp8m9ah4qWBeJh
ZAvDqBTMdQauHr+56VCmyY+A65x+VHu8kLQ6PAIzenTI+++NTqcQ108arWQc6b3go3HxuHDJlQIl
OFCsApHbE8mnhrElEpHa9hQ71QlqQA3PMQUpRubdVPogBZQT5Q90120Z3Hhjd8nyIIyCzc3Ur93F
9IrSGTXtLGg1ZA3j/0W0lzQxjGF/65hLsf1X7Cbv1sM5LPmEfFN+nTJSanNg5+5eZjv2oxNKbzCN
4aOR54TOApsxRfpi6+33xaRIpWl7adxxDyqH3mHS8uBUqlFWd7djr6EkkKtcEvOFYrLYMDL5eXPl
W5dFO8IAAs7RSa3vTJX7Vm9503ndWkMPKfeR39e4DlAeauYXeMRI7RRFNMx1iKayg28r/qEwAz/i
C3/PuxsBPOeFXMlJwCJj+MuSS7TlTzTfCY5lmqC2/1nxh68trkKRGmZhGrIfZoVN2H8KlBQFeVO2
fbEiIcH4DrfmVsK/SYyFwEx/tuz+WGhrJNbAr5ifIHaByTciEFtIUXuJd7f2jKVYdB2cd4iiXTGw
/BxViWWQNflkgZj8rgqyiM7LP8ry60WJllXrWPcUu7Ge3a9oFvENUn0+qxakLukbRwTvjUvCPwXM
S5fohjU5XWZCBczdKIOoJEKWhD5t9Bnr8aELikHZq4d0WWifgU+MqL8PqPSVzCkJARsh/sG03HLu
2hWxhfTIOex1usCn9LbYaoYnWeHxlsQMSbrfPYIZJ0yrzi7QkcvzLZaabNyFGxCZ2oaDd303CDjq
OlHQSlq5V6pN28oLoZASAjpUTpsVzZm8Qy4WAqzoLXaqCFBlk8gagwZU00JycSBJGnv65OmYeupY
4X+6qNLTqkuoptnfYHX3VKXN0IJWu18oEKqaS95zyDtiSw+5BWsMrlZCNssJwKVi8Ce7ogR3aLId
b5ntushJl/6cDSL/HgADjab0Whk3IrYZX8Apvo1q96RjFKFkBmBtJYruvQpFns+NRKpzOoPbK5ja
TTPCV27WXtjSQ8zLTgqLwB6uJlTfQ0UQZltAETHluHqRYJ70Uc66d0H113xz+46a+n7Qul3/slDJ
660Xwthl+H6BJNnhFK4NuSHsajKLF9ORw7aLJkuRGYAEx2DWC1nJPFkjCsp8VQ37DMpqC+B2nhs3
ZE92i4BI9NwgJy2qLWgqnRDF5R53fnXXR/zK05ToNyciAJ6ROEln1w9oAHRDFAbvWbFWv4YFya3x
8hRf7OcSOBufdfEvtCGtS4GUHGbGTK1aS+SnjCTKi0ex8EwzhRjVNbtx3F7hAIT5ecQQXbGMo4Ns
Og+LOee9DyeKJOoo5I4AFIeQgQTcIWdQVvtFjrSSNfYMo7mG+lNQY2k6ulgjOYTL92BSc9zZ1zTY
DCAZJWWP2fu2qHySWFf/gGlnOpAlO8xBsyFfZVd8jLBahbairMYZR97vBUdxqutyAd4dDYOA57og
/Bjk6aA+sMg2zvLTSCChArwU9deZZphkt58mO/c5cC52rA3cX2Yg9K11H65OtMsN8I8DzMqj6ACd
l8QYx4/4qKMYHwGdl70lkMOVDBnrkvdK9gie1H/FbVv69fT2gKKWaV3Gs1Mi4h90pYGMKRfWW6It
VHzc9mNcaShzE7RrWhCi+Niwl1uIDulysthqyDM8BwxsZP4QtV28PPujM+0NqcZHHm/OtcOnZORQ
muDd3WTLt7dSz0Gx0Fa4QZ7t0yml9zD0Kc+8EL7FyUaUtEh1I3QxF9Jqwu1FSp4xxZA4cQw1nQI0
vfCh630nO7M3qnBGd5tYV5XABi2wRFpu4hCj7f9LZDmc3SD2s6Uv7OgvICdPaeZ9rJXNhxQUHPER
ZjhpMbv3m+o53+pH2a7avKeULcH1Nlz+rF6GKg1jKyBwrI7McAc9hqX5Wp1sVriUf9E8vw6qc5Jh
T9jz4PphxWzQHSMWMHkma/nxhGZkf3HE2pGnTdkpd/yeEbPxPBPU3brujMFY/ZssXnH0c99wKXx3
lr8gjkUkr92mMUCCoPvnLw+m3AITU5sH9PoRojezpxEbxlL6dYQtTppDUridJ7F4f5dAaXZM56Ui
W+TM1dBWNARAo9LEbA3CyS5KFgJuBJ2GMiksmbXtfhCeZ5CkLm4Ru/HDocqUglHfTH/BsMgE2u9r
/4ObFNHX2OsvLpfiKqKlnvfaAJfVFoI1ceeliuaAWajzr4YjSQx4/fnUfdk/+ANxjSjWxeiG4cAf
IoelB62IxGdGHcHfRX0Pe294V8YYqVZlNWJN4yCkhY81yjTsGihCM+U8n2XvOJbwANDxIEyWdtad
kdjynSiSVvWFq8ngDgJspIPEwCUMZg5nkdx2c81I5k6DWf0NntIcS3cuZc1bd37dkzHniHEBiLJU
Jbca8rwM0y/Wbjzn0uCFFKkQdjS3dqEHEWv2lFoiobHxg9polDNij4+pV6UVNArwzTkRFqSVWzVN
+gxKcDaaBLXDN59v0tZS/csH2msZ925aR2VfngJmX2zMMJP+FWcxxxCc4n+WOpjXSvGHDr/g2x3K
me30Y56/Tz5F6CXHKim1jA30T0zsWRUdVp8MXKvqdJP4S+hZzovSKFbx+uJwp64ZTOTlRbbFj2l6
JPnMWLCLCRvkrL+12z9LMxG6nS/SZu2VaSCPShQzSE4FhTeBtvumB/YdOsG2KywCwPKKtVsqwJYl
a2f7y7JlwfzSFbpMd62AvzMy2CVPc5mlNYcAxXinn+8a3WD176jlbidjwz43Opt7aPpIXAV4YOyS
OdDy8WlHhDdWyURIpnmSWCY3zKVaUfFtkRxyCVRN54u0tSBSlnyzlo6ReQC6PzjMUDK6h4kFSiw3
3wJBpU7m9nfbvBEzDF3g3Ic4jIc+8/fXS7w8Y2Qy53rXquV1YfkDolpG+3QWL9GQ7B58Ld5qz3Ec
SsjytMTwsZ6xatyClIkgvDPx419Sg3GvZzMmeZ3RMVZlSuMKEMZgWnuZrihqAfTYG17s9uUkVwsP
XmtTzJ4Y6DXh/m66io3mI7r7nUWWa5BQQknVYWc74C1YKoD6hJItV0s3ng/FB4qwzsuq4g6lRJMM
a1Y094KMYodBtmtbRna+3lRtXXphXGX4a7/y2NoUT7DUSi4jRzMExojGRwZkF2N9a0qj0U8Z/s2K
9Uxb3oc0ojTYNUViCZOZF9eMzQjOPMROGGaW6tFfD/1ad66umN0/HkiVZ2ee37jsNc/1ZNbG/QnW
iUO6k3cADe2B2o0L6wSbHGnqk9YEJtdsifWDXJx7iHSR5BBCO6jRKI0TYZYeuuTQRa6CnBsfevM7
lZ4Vf7ZuHRW5Aju6L+WogvEIw5mVGR03dFL8vj6FIZReglPNc4JsbQjR9ppqRczuUaqoD0LRo3oN
T5EjE8b4S2WDWeR3J1EDadV1YH/6xZO3WSnegJKQUKuXbctNTA2oZF7drLWxGZ+xeY11/GX6+eQh
bwBzvqRZHA0vaMOCdWMKvVwxSvWwMX3zDJQmEU0haxQtNM8fuMnYpWHy7QpQhQt5x8+sou62kWvW
FjPEcZLpRoKXEPJ6QX/4jF5V060iQ/Vzgdk1kPbqdk1++eHna0T46KusxPNLkaRLJ/cMYcEO0dYv
ClIdnJFc/g5XNOcxRlr3ZCKPuXuc3iVo+HO/mDIobpaJeupNRe+CpnRm4VWgMgFMRWLtMHXwcykP
oBPRvCJo1qrbRVWc+KDJ6HNgCKS2MIT/YlOFTILrqu/SA5njfmlYZ0egxYVP8v8i4RExxDVUQr9A
TnIzpor6u776He+zle4ulbDTmESrLRUr1NbxLCSODsE5cKIBGeYpKaiLqDo4I57zUdAwvICtfXP6
0oBG7BqwBqo5vq+6Q45IwqJgQIu7ncNaWvRq1WspEpDGCH6UOQHOQ9Jy2uVURKIbDkAzmIHI3py6
39fCUf16E5VQk0gr+YMZ7ou5ef2pYlS6tYzAhgP+h3Ouh9J6+IRprhjCyS1Yn9r/+fvM/Dazc0Oe
6elwHzVdH0Ldk+bxqDh9Sw+KMcgqgIRI8NNq29KLxHThrIY+DMHQk7xSrIRSpEUuHYf0ED78C2Hh
HrK5YIXHyCPrmDTncbRwhcR5trvJnxX2MhogQb3ThWhg6uztBTgYgr0WjQykdqu+nXyI9BV39/Sy
RMY5H+cPHBrC3uXYUWMLuoQG3/g6bmgJRbiRgWmP4Tuw9ofwiCPfQY32yHAyF65foQnvssX7qeBC
mN1niyUPF8trvFcGWJLslzIpaF2ZPZXzoP/SNrJRnUdmb8G9KkerZo7wp4U3yatFQkJE+op1B7om
qsQzn8EerT4VajsjDEkHtvd47r5FLI375bC0GZFzJO0Cv4lENpf+2dHAK1gfNFMmUsNI6buAAuRh
H3/T7B1zOJNY6aOR6fJ2uuT/rDDW3dE+UlxTgewOiQjAlH0ScjxGbz+yVHN5vGNt8Ela5tgEV4Hw
XnJA+ppktJ+OM5EKMOz2DP6WvDZH1+rMn+q43C95ZiZfmsDsMDJmZCDRwOzviSRDDpe0KSMTSPQO
ZeNdZ8zAoIUYCOm20rFyWX0hgHByFq3HCkW2gHrsO6AxR+PxMP40XlzJUTstSxg9yUsk+s/NzE1H
X5AnlFlxfcGMwBq+hOZIuRGdEZderbcl5lgmZmM9roEc8kynhjgXjJQMsmmDRneZu4IbciFcRuSI
u5NH8cRKJpmC7+P5O0q1vupsZjsLpVodxkyz54OOaE0iHiGt6GisAozebSg41J3QLLI2PPhQrqrp
TFUBFthJyx+qcQXfL8NcTGTlIuY530pv0o/9GG8FEUhZJhEzAZN3vp5hxSnRhPApP6dKoJEQeZ2q
llYhrn0X7HpLNcc+5zsm6xamrLrDPV26+Y30km2ANAVz9xdrQ+HQknxSZXEcHRrluAFtYvBWlZZK
ZCifeWoKly2gpXiY8EbBqA4AWiudOy1pDll79Bow5vY21gZH4W5pi3bjlSHAAgF22njzSgneP/wn
kHWy92m1tS82iHyM3sgI30qQvBx10Qgz3ZRLCXncUwBCfenLIyOu2zwv3BrhEBHVRnRCpqMLJ49C
/WJxG/A7cV2w6zI6Z5vk+ycS7wlcroyYJbhlFMiDyWiJUle8373nVTD9a0kjExLMeFURH0liJVHx
n43nYhKOAlnPy2LRL0/3jVI4t0SJPsXJl85jfSO/ie1Xeuk3OonR4YR3FgTwKH3pc7znStzv6W0q
TfzbbXD+US+6LEBxlSv1HhCOSNCuvtXBi1dn7pUyuwZtIIIHHjD1wmKhGejnJjz6SbYTgXBDykX8
yBmvwcq9ps5bOIevThG37jvvygM0G9+DFRGAQb0kIU+90k9ZgiT/kU8DBexNKBO8v7ywlmbDcnxe
U7jiR+AweWCoJdfZf0QYvTn/wKeKH3BBG6hdB/OHS8OCooXmTnPrdDu4MjnarPqvLdtyVM4WAZ4n
pqTaijcbXTYTxXM92Q3vxC4Tyi5wCHyDUvmME4YnXCifXu0rr4gMQdWvqbDno6w8/qNHtNRoY7JZ
bJ2leRE24d5G7thlQ42FImHByu+6BEIumHtTEZ2E29UnKyRuvCK7eazaefTaWTcbnAkdz9JJfOgF
fjiBRfDgQ5ccnYnnCH7xfxv0z8fcKmlDTxyQrCKF+AVf6O1x4Gh3PARF6bRuvTflVWSQDeZ8e+4P
FJ7nbGcaDkzz8uEQhLcJnjn4aO/LTgHvQKJRulTnXPrG0OiOCmfPwI0oZyd08Ua89SsIn0NQ6T9v
fwUEeck6bkYgvVaNuUBmzah+eTdCSvBkFjFcF4568F9jGwkxnPuX2g+2OjBRH52u+rRAOmqk1hBt
4iRZVVwjvSF245NxQ/F/s1dIekL11Iu1RNopqOiiXG6Cj0OzV9bPf1PtPAToCXBpkYikXG1IswMI
JjEwwbFga3Z2I5QxnG+83oz/DneKHJhcvO7PFwFYauXsZhrmBwca0GwAMMMvoA0gVj710c0oko32
yG877oyEltTVod/hF/bhdovQIc2jLBuL1KishNsNcNU+ijAuCrK/cLpNYFHrWDLKcKKVYRg4NXDC
9HAVFwa708SXcyJuVSWx8eAPQLxsScQZJ2BURJbTTK6uUwd1rn6oPU/1OP4TZoZhAgLLuMFVlQ+h
2iagUmprg0hlp4ztVUkxMULRp/fQVaJm8zBtkoKXg13+Zh1vOgr1k9FvtMlZa/e4NJ63hkt33eSu
P7HsgJQlwMbEnDTPCSO2ui+IA3HAH0osRygVKm9ooNyA/W1/Q5fE2tfR45UJz703w89ICrJW72Di
b9aDByICEeDQRW3oINfX7CKa1DRaG99kEnUBoLMUCObo5xRX2JWDn37shICevaBOQyh3qyg1rK0z
nTh3yorUlvuJsSSNJwWHa8y5VmPJha9UoUxpvxVLYxBBEJMkPWC7SzMsqYtaLDVIQsLxzdTVsKzA
Z0uzad77Lw5UyBviVcLGUB/k1QqGo0008HKVNUf2pzkbW7EwRrjlbTvb2hNrh9CI5A1Vwityjx1W
0I4G5QlC4DDK4tX+u5i8CrCeGhe5Ojch5Lg1zwuFgMucET2qL3Ng397qqsOeIOvN/x75/QujYHYJ
U3MoRs2+oV1Z4ki17m8/8apG7VyqiLzZHHh7B4OYktlENqCernYq7i9WY6hA6ozQb8qMAE/KEJOI
eSrktQTdz2zcjTXlkltnSs4p0u3YtRLi7G9c6SrI8wa+qHrSimGNrok32zoXvjSNUC2jjtCYcqcI
P7kIeuW1mzh3IewdwUyBVeaZ7qKmDRWjL2/1M8Evra7hTTIW9JSh/bDcBaE1H3S634WncJXznEq+
N3MHvznnWVXv6S8ij7yXVhRQaLHLu/KIHDIHwCQSyK/bDCIB1fvW3OwZr5JghG/lbIaQjdSewln4
mHhOiSyxXLib43r6GHjg1zp7GVUSsU5oke/QXUzRk15YHD8uSvsr0GRpyUK2jx3Xm2kPvRRbygQt
yj1pnM/ev9om144DumTm3ejjr8csaRY97ZewyEkzEXmFrDpNzfVup7EdMZIQE/B1jz1sHVv6E9y4
IvzFNuO9+Zq2awBtff7QKub3Yy/Y16/QdMOUjbyGgUhl+50BYQrzRqBAyh+7AipHA1yS/CE7AQGB
x777LVgF19aXFi1Bbxgci8OOOQnRB6wEgAz7+GpRcHuqAyqsvsLx9KgVAOIN4fvHqYGf1Vc79VDz
CcdQygs2ZF+WcP/XuKVkFFvi8k9ktdONZ6xT5TbpTigUibJcLFc/lGDdr6/8XvGs/UT7ophY9Yu5
7gNeXrJtUUVQzZhfgpQQUWzVm/5PdbRB4Dy7GXwTyfHw1uuHtp5lrxTc0aObhj2S6UG7UEj7cWcy
zBluom9SMhONJClbNqKLIzr/3EuejrS4djy/v8MA6BTT6Mhz96CxnWN4ulpKdFdOH5GC+JmlG+qk
hejR8IcVI/mpHjyktGp/cV0rW/BX8/RImUd2els/o+GjqXuYpFWNUKlXxB0FBu0iUCQGhZQ3y/Rl
evHspxDgK9uT12NLJCafLroug7zz8Bkv8ziQhoVa0VH6p3HoGqm2zVAp5ZCcKWH/LWnwG5h9Y05M
CyfONnsUQgRM3bhPZ2E2y+ygwz0ooyVFnvryPPpm3NPtr8KJjSPnYGsRF1oKs+TUyvS9Vyc78Bk8
AP76ZuKhPkmryG01f/cELlBIWh5i/JEiMiXV7hufONUAv6gOXYJz+BTIf37qLUr6xCjzotC2L5dq
Af2lVfDTnu1q8vIhr7cSSKeBM6/ApEZKhm8X6qPOrjsV49JnSEBwxJN+rThAwss9hu4FEzhJjT+x
V5IJUG40gGd+0lAVgx5mASZM4ldkPD/TaErxR28t3IUj0ms6l10L3gerqBffLFYzGwETsVMgxo+2
zRB97cO52i83MgZOyF1ROmKsVeGgE08+Gbw2QjLt/vxosHxs0sFRPVs57qZcrq3b9cYuiRmYc9aC
NdV79twMctphrnu9fZUJKohNAMKdCroYPlO+dGU3Dt6zJ1/b8YPY90hnaecCXZvEvQHF5vYOGUe3
GVWUWmy0+t6iNViv9OB6QymnQQqxF1egCyErjQtqBGG5QoTM8iHukZ1YBfuN0xKh/9cMTB2hhQE2
92clSnpsxR0zVp3bZ48FoeFUVIZdoFC7toHDXFUITNvNhz4vi5rMnCcibDBWElwCEn5NeZLlqWYN
fj5xKC4pGb1V7kt9M7ahYMzQSdMIkQ54BJntE3qxa5IWBuL6apXnOGuvVe4KxhIHer9+y22zByEj
fokBmN2SUdjkmquYjeIoI9SRZhqcpj1Op2S2PuhO89qUR1hzrhhgFYLKdq/mZ9jSVd33gBKwoiqQ
WbWB2RvV/jga360wXW8+r1aRcq52aeDtCADV5f+/Vhlx/HEedRnDKzcONxR+q7jYvTWjTWqSdvPL
Ayp4NjIxnfUxd22BGRWBpMPl4qpFQ2xaHKxDohjBX3keYUk7qyVT+gLFYc52n4qvDf0eGxvphNuf
7l2CNMuXGsf7GLUT6+BwCD6udk/zw7IuQTRL2n/7efvVGtuOiyHpbgb6RgmqvdfbglcDLLFhJY+2
Im4bWwVuZwmtC8K8gh2mApJxaBGIDWtXcFs2XWhLPR/+I+P86cDeJjPiyHhsW3QNA0EYz/8cmzsk
LTRMNKq9u2KvY5xZno3BAWM9InasiLJHO1J8iKydaD+HyQhoKfgHYmFOizlzzMGCmGi8iCHpKBS0
5m0vLUum9r44GjURuohor0jM/BQ8K4ZEBnQYOQ6zMGzYwc2Jz2JVY22wAvNEne9luuN2/AQTFXFI
Pgy683ZIjDSt3x+n7Fs21a86x5RBWmvRJv06H7FrS/c/+HbjZIeMTJmsuIIXppnbinQMFOMnBX68
So0ZeX1L5l67Y26KrdVSsuA9Vx0Ag3dnHWbdoOXsZsEh4NJ44UwWePu5KojWsjSCrVhytgU6Vd25
tJDYAwUV0RoOEJDeEqx99Us8/TCNWIAUvfCZz51MnzPK1MCvYJCaW5Tv8M4/rulcL5O/mz9k2q/J
e/K0I2X4c8SRQsQnumoRUfNMp4OqSgff8hjdxitFQgYpIyWUgf9/ThhvHzVKdTSzwGdo7u65VrKT
6yva6YxVex+hK4aZoGvv39QYVSWx+TmeLAdcjCrkwX+b9TiqAqVKzW9xQPNIMY5NV7YWALyZ0ekW
3yPuLQzbBMhzKIpi4JtfYJ4HTv4gFJ1Ug625ak05PyIYm2q2iuF+KVQmEvtjZKEusSMuPBelHfNL
uy9F1zQU80goPGIzGZQCAw1nLtT9cK0vQCLOsyvi48JbPM6t8qM0FxVIxheBHMhGiJsY/+JRfIyc
F0+Rd6B4KQBrAD/xb2rIAQXmmf97+WVjO31m5N58zHvqXZ3gOnlKQeA4ZfFJpMKTzi/iQMzvSC2L
s7qqIqvXfnPYVdWk/wRDlY1MKoKp4zkYQyeQ5CVLWpkk1PWihEwMT1IJmyV5ux0iveYcYnE2fKL2
nLlHSDjIUDDBei2meP1l7WSWl3B6Aa6a7JILD+uv/2GEEzG/GrO1BmW10uMpnTc76zh0Dyc5YiZK
96aWfeBCYSwD64jWPGAjQ6RhnoWJJO9CYJSQEFtXgmUl6MA39yXYoy88nOplYlCoU07fTE9wt/mI
RJxGNviZHlw+71h4fMzPg4/2/JiBHsDDBeVY6FKoCAzW6U248FZ4r5Y3A2r/DI0WQ+DrYucgnC//
qO4e3g3QFcc4D9WS41n1yknFmmdp3VytpRL2jETeHystRh2UGLq3x5ZvQFRD5RdDdyw5Qm1wXJ2v
Ihr1qSZJbuN5FsBJyLTe/dZ41eCgZQJjcFbRiIwOhLFvB2+SVU/Kk+HTfcrMRnnlBKKtVWDiwEfX
UZKQJjw9PB1LM+3OUeUVpxv3kHeBts/lx8KO2nyxqRcvbEqimTuZPfS4fsNXZtlOqOHEIh9+xzTd
wOgEy4TtwMdMWrdfFjlW9Jva/6nlnaf/8FLBIO8dTl6AGHfN8rs2FiHEbVhWb9jZ0PuL65ZT+GLt
fjxZ9JRNi7HeWhbfWawgb7Nms43TMjk+Yz+j388/tDwtVYMbUt57FJezRNSfRjtmfVEaB/KdsJAy
DHQnCtIFdI1GRy5PahuHyGxACCBtqHd6ZZ9zwfPyHw89f9WGixE0h+3Cb6ic5qwAB8SJT5bPjzll
ITDp1s+kS/paFvCNDlm6MniBb+aJO1r1VqAgSKZKen1lsJ6NfyVO4S7b+ZXlnfaothaE/mFXPt8c
lkFETPwc4lDX+0b863A7E5FYuLC2bU3cWb/DkfiX/EkiNOlIZVv4yOtynjZzffbjNRawuWegkMlm
pbxsotI90csTh7goIiqgvgQQBuvC6+NGXUUYCrDSpqukmzjz0un7CBeYHPyS2347xMODpsqlD8Tc
R7Q7PzjKL/Z/asi9wAaizgYRDkcwBfjptP+Oajiv3OknuhQr/4UHFfv6oN/j/nnVu1VlMGvrJrQ5
sBqFb+o1YFf2BV8Aw4dYiQQkS9KNsyl/lWtERJ/QvYCGOZV/WC4qnkJLPKw3HHvm+kIWgG963dML
Jue5sxgMqrBj1X0BuJ3gwzYCNDqyad3BtmXsmMXhP4C1sYHf0E/w8Mo8MrI8fp6oi48/Ym0fa0+H
fA6CJoLEujsCXtc6+rRUdWZCSyGzwnZvIesNV8ACPqpvA0uJBQhTvwJco1T26JzOCHLnQFtGpyPZ
IPvXSIsJ7qOvASiVT6St/VB0aXMnVIfgtwbCDhhsQmueUBdcyxEVI39wnVNQR9bU/8rETosrJ92T
ZMNP+WIESl7l+5qYvSsp2P4KLYx6CK0s+CXqANOYF2zmJZ2pwALENV5+n+1LXtioPxufpCUIJjhr
ezPwfXrH665qUUeFBU0XtzIFxTEzcGh4127y2QOObhshRCCPMrnm++wysDHQSGOKaLE+vbySVbTF
iJKge1YjWlh4PEgxar1oqxts0ny94/+qXOhu3eAlPDGHW0kTMlmYVRHhFo4PuAmUW4qG6cgPO4sm
vDCZk55UnAL5NYVtfZmtsU7uba7o7Vgado0sOoZHUB7+58EX2rfbsYph6s9W09igc/1j5PPHP1Dt
EQ3En42uhCUVB43p/2iLCL/RmseYfNlp0BIZuf1j/FTy73FsPm8ngJ6jqpUZP9VxBkDzXzljTwdF
QsQUxnLxgjb8z1l4L6yMqDq+X/BctwrcsG7cylKfHSdUMrjUsJ6vfHsJhMhS5miISFBypIAVx1bm
Exf4E+Q3XqN7BButQ8WM5xrev0/I+RsV+wlhUAAYq6pRyHtpCIM9i+xh9mMXRTSr3HdOqg+8CLN8
D/kqyQaAo2l/eham1uf9yKyVsYIY9HELEtAKUmBtIFZbM1IW+Tj87gyD03GY1JpcOmNkP+CttFdF
ZvYxQROxA0AygP1wnoQEg+EIOK3erTbz2CTHwCNyfmNeAZwQ5EqWgMU4+NU5rFv/VdNsYFIvboyN
nLxCaKvjnJ6WSMkTNTM41Mfn0GT95+i4p4uG4eQXVoZDleRwhvU8ISH8OfaiD1ZPVVZN1sNl5qMt
q4NklKYLUF70h0536yB7z221JN6jxfRoQkXSpgOsLgebkI1YcM8RELnwZGvjX3j/DMbIvOo1cccT
Pk/xjrHWBKe948OU3d3ms15PByb4HlIcKxKpxPZw2iWHHSuBjwwKyNTTCb8vTDSMxEMA5Eq9dENK
8n/xnYa/qa360+kDejxMsmNyJaVeAb5BRfq8FWl17qWdoUicAfJ6FY//gf2PksYXF3z5LkcOIW38
ZonKrm1JJwFjGjXEULIB3aIS85QeB+UtrSbzKswzhSLhbAupEPt9qkc5pWYFECmAm8Jcz9qES/CA
9XOvYOzbhPhQsA0pdOA6XAVgVBdqyUC3Zx7owgE9kqeHTrUhuYGKGC4GRrTIvssnt3UJU7kX9jM+
mjCirbhBMuZ1a8C33wCgz8udT3QcBuZLHlcEzX0ckmMCWIZxbKAoAxo/4jRqJ4PwCmJA1CR23jmC
9VZFr2eixK+CvYMY04jQR9CUzBmblH8aVnnIPj8XdSfKqy501qZ5UbG/HClgvl9GB0OqKbpLesiW
sGylYf6cvbN7fDpiqDcSIdOlX4a/NXX44060ETGafP7qbhSV3FzzcdTwYaLUmtxE5t28M9Y0BIxl
A4MVwTFvIZoVtduvle5D6jWzFnjwdmyENvXP
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt is
  port (
    gtxe2_i : out STD_LOGIC;
    gt0_cpllrefclklost_i : out STD_LOGIC;
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    gtxe2_i_0 : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    gtxe2_i_1 : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_2 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_4 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_5 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_gttxreset_in0_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gt0_rxuserrdy_t : in STD_LOGIC;
    gtxe2_i_7 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_txuserrdy_t : in STD_LOGIC;
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_8 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_9 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_10 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_cpllreset_t : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt is
  signal cpll_pd0_i : STD_LOGIC;
  signal cpllreset_in : STD_LOGIC;
begin
cpll_railing0_i: entity work.gig_ethernet_pcs_pma_0_cpll_railing
     port map (
      cpll_pd0_i => cpll_pd0_i,
      cpllreset_in => cpllreset_in,
      gt0_cpllreset_t => gt0_cpllreset_t,
      gtrefclk_bufg => gtrefclk_bufg
    );
gt0_GTWIZARD_i: entity work.gig_ethernet_pcs_pma_0_GTWIZARD_GT
     port map (
      D(1 downto 0) => D(1 downto 0),
      Q(15 downto 0) => Q(15 downto 0),
      RXBUFSTATUS(0) => RXBUFSTATUS(0),
      RXPD(0) => RXPD(0),
      SR(0) => SR(0),
      TXBUFSTATUS(0) => TXBUFSTATUS(0),
      TXPD(0) => TXPD(0),
      cpll_pd0_i => cpll_pd0_i,
      cpllreset_in => cpllreset_in,
      gt0_cpllrefclklost_i => gt0_cpllrefclklost_i,
      gt0_gttxreset_in0_out => gt0_gttxreset_in0_out,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gt0_rxuserrdy_t => gt0_rxuserrdy_t,
      gt0_txuserrdy_t => gt0_txuserrdy_t,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i_0 => gtxe2_i,
      gtxe2_i_1 => gtxe2_i_0,
      gtxe2_i_10(1 downto 0) => gtxe2_i_9(1 downto 0),
      gtxe2_i_11(1 downto 0) => gtxe2_i_10(1 downto 0),
      gtxe2_i_2 => gtxe2_i_1,
      gtxe2_i_3(15 downto 0) => gtxe2_i_2(15 downto 0),
      gtxe2_i_4(1 downto 0) => gtxe2_i_3(1 downto 0),
      gtxe2_i_5(1 downto 0) => gtxe2_i_4(1 downto 0),
      gtxe2_i_6(1 downto 0) => gtxe2_i_5(1 downto 0),
      gtxe2_i_7(1 downto 0) => gtxe2_i_6(1 downto 0),
      gtxe2_i_8 => gtxe2_i_7,
      gtxe2_i_9(1 downto 0) => gtxe2_i_8(1 downto 0),
      independent_clock_bufg => independent_clock_bufg,
      reset => reset,
      reset_out => reset_out,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_RX_STARTUP_FSM is
  port (
    data_in : out STD_LOGIC;
    gt0_rxuserrdy_t : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    independent_clock_bufg : in STD_LOGIC;
    data_sync_reg6 : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_0\ : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    data_out : in STD_LOGIC;
    data_sync_reg1_1 : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_RX_STARTUP_FSM;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_RX_STARTUP_FSM is
  signal \FSM_sequential_rx_state[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[1]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_10_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_9_n_0\ : STD_LOGIC;
  signal GTRXRESET : STD_LOGIC;
  signal RXUSERRDY_i_1_n_0 : STD_LOGIC;
  signal check_tlock_max_i_1_n_0 : STD_LOGIC;
  signal check_tlock_max_reg_n_0 : STD_LOGIC;
  signal \^data_in\ : STD_LOGIC;
  signal data_out_0 : STD_LOGIC;
  signal \^gt0_rxuserrdy_t\ : STD_LOGIC;
  signal gtrxreset_i_i_1_n_0 : STD_LOGIC;
  signal \init_wait_count[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_1__0_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_3__0_n_0\ : STD_LOGIC;
  signal init_wait_count_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \init_wait_done_i_1__0_n_0\ : STD_LOGIC;
  signal init_wait_done_reg_n_0 : STD_LOGIC;
  signal \mmcm_lock_count[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[6]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_2__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_3__0_n_0\ : STD_LOGIC;
  signal mmcm_lock_count_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal mmcm_lock_i : STD_LOGIC;
  signal mmcm_lock_reclocked : STD_LOGIC;
  signal mmcm_lock_reclocked_i_1_n_0 : STD_LOGIC;
  signal \mmcm_lock_reclocked_i_2__0_n_0\ : STD_LOGIC;
  signal \p_0_in__2\ : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \p_0_in__3\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal reset_time_out_i_3_n_0 : STD_LOGIC;
  signal reset_time_out_i_4_n_0 : STD_LOGIC;
  signal reset_time_out_reg_n_0 : STD_LOGIC;
  signal \run_phase_alignment_int_i_1__0_n_0\ : STD_LOGIC;
  signal run_phase_alignment_int_reg_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_s3_reg_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_5_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_6_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_s2 : STD_LOGIC;
  signal rx_fsm_reset_done_int_s3 : STD_LOGIC;
  signal rx_state : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \rx_state__0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rxresetdone_s2 : STD_LOGIC;
  signal rxresetdone_s3 : STD_LOGIC;
  signal sync_cplllock_n_0 : STD_LOGIC;
  signal sync_data_valid_n_0 : STD_LOGIC;
  signal sync_data_valid_n_1 : STD_LOGIC;
  signal sync_data_valid_n_5 : STD_LOGIC;
  signal sync_mmcm_lock_reclocked_n_0 : STD_LOGIC;
  signal time_out_100us_i_1_n_0 : STD_LOGIC;
  signal time_out_100us_i_2_n_0 : STD_LOGIC;
  signal time_out_100us_i_3_n_0 : STD_LOGIC;
  signal time_out_100us_reg_n_0 : STD_LOGIC;
  signal time_out_1us_i_1_n_0 : STD_LOGIC;
  signal time_out_1us_i_2_n_0 : STD_LOGIC;
  signal time_out_1us_i_3_n_0 : STD_LOGIC;
  signal time_out_1us_reg_n_0 : STD_LOGIC;
  signal time_out_2ms_i_1_n_0 : STD_LOGIC;
  signal time_out_2ms_i_2_n_0 : STD_LOGIC;
  signal \time_out_2ms_i_3__0_n_0\ : STD_LOGIC;
  signal time_out_2ms_i_4_n_0 : STD_LOGIC;
  signal time_out_2ms_reg_n_0 : STD_LOGIC;
  signal time_out_counter : STD_LOGIC;
  signal \time_out_counter[0]_i_3_n_0\ : STD_LOGIC;
  signal time_out_counter_reg : STD_LOGIC_VECTOR ( 18 downto 0 );
  signal \time_out_counter_reg[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_7\ : STD_LOGIC;
  signal time_out_wait_bypass_i_1_n_0 : STD_LOGIC;
  signal \time_out_wait_bypass_i_2__0_n_0\ : STD_LOGIC;
  signal \time_out_wait_bypass_i_3__0_n_0\ : STD_LOGIC;
  signal \time_out_wait_bypass_i_4__0_n_0\ : STD_LOGIC;
  signal time_out_wait_bypass_reg_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_s2 : STD_LOGIC;
  signal time_out_wait_bypass_s3 : STD_LOGIC;
  signal time_tlock_max : STD_LOGIC;
  signal time_tlock_max1 : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_1\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_2\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_3\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_n_3\ : STD_LOGIC;
  signal time_tlock_max1_carry_i_1_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_2_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_3_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_4_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_5_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_6_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_7_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_8_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_n_1 : STD_LOGIC;
  signal time_tlock_max1_carry_n_2 : STD_LOGIC;
  signal time_tlock_max1_carry_n_3 : STD_LOGIC;
  signal time_tlock_max_i_1_n_0 : STD_LOGIC;
  signal \wait_bypass_count[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_4_n_0\ : STD_LOGIC;
  signal wait_bypass_count_reg : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \wait_bypass_count_reg[0]_i_3__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_7\ : STD_LOGIC;
  signal \wait_time_cnt0__0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \wait_time_cnt[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_2__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_3__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_4__0_n_0\ : STD_LOGIC;
  signal wait_time_cnt_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_time_tlock_max1_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_time_tlock_max1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_time_tlock_max1_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_time_tlock_max1_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[2]_i_2\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[3]_i_10\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[3]_i_9\ : label is "soft_lutpair43";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[0]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[1]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[2]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[3]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute SOFT_HLUTNM of check_tlock_max_i_1 : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \init_wait_count[1]_i_1__0\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \init_wait_count[2]_i_1__0\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \init_wait_count[3]_i_1__0\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \init_wait_count[4]_i_1__0\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_2__0\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_3__0\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \mmcm_lock_count[1]_i_1__0\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \mmcm_lock_count[2]_i_1__0\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \mmcm_lock_count[3]_i_1__0\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \mmcm_lock_count[4]_i_1__0\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \mmcm_lock_count[6]_i_1__0\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \mmcm_lock_count[7]_i_3__0\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of reset_time_out_i_3 : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of reset_time_out_i_4 : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of rx_fsm_reset_done_int_i_5 : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of rx_fsm_reset_done_int_i_6 : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of time_out_1us_i_2 : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of time_out_2ms_i_1 : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \time_out_2ms_i_3__0\ : label is "soft_lutpair51";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[0]_i_2__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[12]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[16]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[4]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[8]_i_1__0\ : label is 11;
  attribute COMPARATOR_THRESHOLD : integer;
  attribute COMPARATOR_THRESHOLD of time_tlock_max1_carry : label is 11;
  attribute COMPARATOR_THRESHOLD of \time_tlock_max1_carry__0\ : label is 11;
  attribute COMPARATOR_THRESHOLD of \time_tlock_max1_carry__1\ : label is 11;
  attribute SOFT_HLUTNM of time_tlock_max_i_1 : label is "soft_lutpair52";
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[0]_i_3__0\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[12]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[4]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[8]_i_1__0\ : label is 11;
  attribute SOFT_HLUTNM of \wait_time_cnt[0]_i_1__0\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \wait_time_cnt[1]_i_1__0\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \wait_time_cnt[3]_i_1__0\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \wait_time_cnt[4]_i_1__0\ : label is "soft_lutpair45";
begin
  data_in <= \^data_in\;
  gt0_rxuserrdy_t <= \^gt0_rxuserrdy_t\;
\FSM_sequential_rx_state[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222AAAA00000C00"
    )
        port map (
      I0 => time_out_2ms_reg_n_0,
      I1 => rx_state(2),
      I2 => rx_state(3),
      I3 => time_tlock_max,
      I4 => reset_time_out_reg_n_0,
      I5 => rx_state(1),
      O => \FSM_sequential_rx_state[0]_i_2_n_0\
    );
\FSM_sequential_rx_state[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000AABF000F0000"
    )
        port map (
      I0 => reset_time_out_reg_n_0,
      I1 => time_tlock_max,
      I2 => rx_state(2),
      I3 => rx_state(3),
      I4 => rx_state(1),
      I5 => rx_state(0),
      O => \FSM_sequential_rx_state[1]_i_3_n_0\
    );
\FSM_sequential_rx_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000050FF2200"
    )
        port map (
      I0 => rx_state(1),
      I1 => time_out_2ms_reg_n_0,
      I2 => \FSM_sequential_rx_state[2]_i_2_n_0\,
      I3 => rx_state(0),
      I4 => rx_state(2),
      I5 => rx_state(3),
      O => \rx_state__0\(2)
    );
\FSM_sequential_rx_state[2]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => reset_time_out_reg_n_0,
      I1 => time_tlock_max,
      O => \FSM_sequential_rx_state[2]_i_2_n_0\
    );
\FSM_sequential_rx_state[3]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => reset_time_out_reg_n_0,
      I1 => time_out_2ms_reg_n_0,
      O => \FSM_sequential_rx_state[3]_i_10_n_0\
    );
\FSM_sequential_rx_state[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000050005300"
    )
        port map (
      I0 => \FSM_sequential_rx_state[3]_i_10_n_0\,
      I1 => \wait_time_cnt[6]_i_4__0_n_0\,
      I2 => rx_state(0),
      I3 => rx_state(1),
      I4 => wait_time_cnt_reg(6),
      I5 => rx_state(3),
      O => \FSM_sequential_rx_state[3]_i_3_n_0\
    );
\FSM_sequential_rx_state[3]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000023002F00"
    )
        port map (
      I0 => time_out_2ms_reg_n_0,
      I1 => rx_state(2),
      I2 => rx_state(1),
      I3 => rx_state(0),
      I4 => \FSM_sequential_rx_state[2]_i_2_n_0\,
      I5 => rx_state(3),
      O => \FSM_sequential_rx_state[3]_i_7_n_0\
    );
\FSM_sequential_rx_state[3]_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80800080"
    )
        port map (
      I0 => rx_state(0),
      I1 => rx_state(1),
      I2 => rx_state(2),
      I3 => time_out_2ms_reg_n_0,
      I4 => reset_time_out_reg_n_0,
      O => \FSM_sequential_rx_state[3]_i_9_n_0\
    );
\FSM_sequential_rx_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(0),
      Q => rx_state(0),
      R => \out\(0)
    );
\FSM_sequential_rx_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(1),
      Q => rx_state(1),
      R => \out\(0)
    );
\FSM_sequential_rx_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(2),
      Q => rx_state(2),
      R => \out\(0)
    );
\FSM_sequential_rx_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(3),
      Q => rx_state(3),
      R => \out\(0)
    );
RXUSERRDY_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB4000"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(0),
      I2 => rx_state(2),
      I3 => rx_state(1),
      I4 => \^gt0_rxuserrdy_t\,
      O => RXUSERRDY_i_1_n_0
    );
RXUSERRDY_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => RXUSERRDY_i_1_n_0,
      Q => \^gt0_rxuserrdy_t\,
      R => \out\(0)
    );
check_tlock_max_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB0008"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(0),
      I2 => rx_state(1),
      I3 => rx_state(3),
      I4 => check_tlock_max_reg_n_0,
      O => check_tlock_max_i_1_n_0
    );
check_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => check_tlock_max_i_1_n_0,
      Q => check_tlock_max_reg_n_0,
      R => \out\(0)
    );
gtrxreset_i_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0100"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(1),
      I2 => rx_state(2),
      I3 => rx_state(0),
      I4 => GTRXRESET,
      O => gtrxreset_i_i_1_n_0
    );
gtrxreset_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gtrxreset_i_i_1_n_0,
      Q => GTRXRESET,
      R => \out\(0)
    );
gtxe2_i_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => GTRXRESET,
      I1 => \^data_in\,
      I2 => gtxe2_i,
      O => SR(0)
    );
\init_wait_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => init_wait_count_reg(0),
      O => \init_wait_count[0]_i_1__0_n_0\
    );
\init_wait_count[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      O => \p_0_in__2\(1)
    );
\init_wait_count[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      O => \p_0_in__2\(2)
    );
\init_wait_count[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => init_wait_count_reg(1),
      I1 => init_wait_count_reg(2),
      I2 => init_wait_count_reg(0),
      I3 => init_wait_count_reg(3),
      O => \p_0_in__2\(3)
    );
\init_wait_count[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      O => \p_0_in__2\(4)
    );
\init_wait_count[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      I5 => init_wait_count_reg(5),
      O => \p_0_in__2\(5)
    );
\init_wait_count[6]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => \init_wait_count[6]_i_3__0_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \init_wait_count[6]_i_1__0_n_0\
    );
\init_wait_count[6]_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF40"
    )
        port map (
      I0 => \init_wait_count[6]_i_3__0_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \p_0_in__2\(6)
    );
\init_wait_count[6]_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => init_wait_count_reg(3),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      I3 => init_wait_count_reg(5),
      O => \init_wait_count[6]_i_3__0_n_0\
    );
\init_wait_count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \init_wait_count[0]_i_1__0_n_0\,
      Q => init_wait_count_reg(0)
    );
\init_wait_count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(1),
      Q => init_wait_count_reg(1)
    );
\init_wait_count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(2),
      Q => init_wait_count_reg(2)
    );
\init_wait_count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(3),
      Q => init_wait_count_reg(3)
    );
\init_wait_count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(4),
      Q => init_wait_count_reg(4)
    );
\init_wait_count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(5),
      Q => init_wait_count_reg(5)
    );
\init_wait_count_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(6),
      Q => init_wait_count_reg(6)
    );
\init_wait_done_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0010"
    )
        port map (
      I0 => \init_wait_count[6]_i_3__0_n_0\,
      I1 => init_wait_count_reg(4),
      I2 => init_wait_count_reg(6),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_done_reg_n_0,
      O => \init_wait_done_i_1__0_n_0\
    );
init_wait_done_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      CLR => \out\(0),
      D => \init_wait_done_i_1__0_n_0\,
      Q => init_wait_done_reg_n_0
    );
\mmcm_lock_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      O => \p_0_in__3\(0)
    );
\mmcm_lock_count[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      I1 => mmcm_lock_count_reg(1),
      O => \p_0_in__3\(1)
    );
\mmcm_lock_count[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => mmcm_lock_count_reg(1),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(2),
      O => \mmcm_lock_count[2]_i_1__0_n_0\
    );
\mmcm_lock_count[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => mmcm_lock_count_reg(2),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(1),
      I3 => mmcm_lock_count_reg(3),
      O => \mmcm_lock_count[3]_i_1__0_n_0\
    );
\mmcm_lock_count[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => mmcm_lock_count_reg(3),
      I1 => mmcm_lock_count_reg(1),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(2),
      I4 => mmcm_lock_count_reg(4),
      O => \mmcm_lock_count[4]_i_1__0_n_0\
    );
\mmcm_lock_count[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => \mmcm_lock_count[5]_i_1__0_n_0\
    );
\mmcm_lock_count[6]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I1 => mmcm_lock_count_reg(6),
      O => \mmcm_lock_count[6]_i_1__0_n_0\
    );
\mmcm_lock_count[7]_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I1 => mmcm_lock_count_reg(6),
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_2__0_n_0\
    );
\mmcm_lock_count[7]_i_3__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => mmcm_lock_count_reg(6),
      I1 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_3__0_n_0\
    );
\mmcm_lock_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \p_0_in__3\(0),
      Q => mmcm_lock_count_reg(0),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \p_0_in__3\(1),
      Q => mmcm_lock_count_reg(1),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[2]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(2),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[3]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(3),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[4]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(4),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[5]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(5),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[6]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(6),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[7]_i_3__0_n_0\,
      Q => mmcm_lock_count_reg(7),
      R => sync_mmcm_lock_reclocked_n_0
    );
mmcm_lock_reclocked_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEA0000"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => mmcm_lock_count_reg(7),
      I2 => mmcm_lock_count_reg(6),
      I3 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I4 => mmcm_lock_i,
      O => mmcm_lock_reclocked_i_1_n_0
    );
\mmcm_lock_reclocked_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => \mmcm_lock_reclocked_i_2__0_n_0\
    );
mmcm_lock_reclocked_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => mmcm_lock_reclocked_i_1_n_0,
      Q => mmcm_lock_reclocked,
      R => '0'
    );
reset_time_out_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(3),
      O => reset_time_out_i_3_n_0
    );
reset_time_out_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"34347674"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(3),
      I2 => rx_state(0),
      I3 => \FSM_sequential_rx_state_reg[0]_0\,
      I4 => rx_state(1),
      O => reset_time_out_i_4_n_0
    );
reset_time_out_reg: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => sync_data_valid_n_0,
      Q => reset_time_out_reg_n_0,
      S => \out\(0)
    );
\run_phase_alignment_int_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFF0010"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(1),
      I2 => rx_state(3),
      I3 => rx_state(0),
      I4 => run_phase_alignment_int_reg_n_0,
      O => \run_phase_alignment_int_i_1__0_n_0\
    );
run_phase_alignment_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => \run_phase_alignment_int_i_1__0_n_0\,
      Q => run_phase_alignment_int_reg_n_0,
      R => \out\(0)
    );
run_phase_alignment_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => '1',
      D => data_out_0,
      Q => run_phase_alignment_int_s3_reg_n_0,
      R => '0'
    );
rx_fsm_reset_done_int_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => rx_state(1),
      I1 => rx_state(0),
      O => rx_fsm_reset_done_int_i_5_n_0
    );
rx_fsm_reset_done_int_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(2),
      O => rx_fsm_reset_done_int_i_6_n_0
    );
rx_fsm_reset_done_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => sync_data_valid_n_1,
      Q => \^data_in\,
      R => \out\(0)
    );
rx_fsm_reset_done_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => '1',
      D => rx_fsm_reset_done_int_s2,
      Q => rx_fsm_reset_done_int_s3,
      R => '0'
    );
rxresetdone_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => rxresetdone_s2,
      Q => rxresetdone_s3,
      R => '0'
    );
sync_RXRESETDONE: entity work.gig_ethernet_pcs_pma_0_sync_block_10
     port map (
      data_out => rxresetdone_s2,
      data_sync_reg1_0 => data_sync_reg1,
      independent_clock_bufg => independent_clock_bufg
    );
sync_cplllock: entity work.gig_ethernet_pcs_pma_0_sync_block_11
     port map (
      \FSM_sequential_rx_state_reg[1]\ => sync_cplllock_n_0,
      Q(2 downto 0) => rx_state(3 downto 1),
      data_sync_reg1_0 => data_sync_reg1_1,
      independent_clock_bufg => independent_clock_bufg,
      rxresetdone_s3 => rxresetdone_s3
    );
sync_data_valid: entity work.gig_ethernet_pcs_pma_0_sync_block_12
     port map (
      D(2) => \rx_state__0\(3),
      D(1 downto 0) => \rx_state__0\(1 downto 0),
      E(0) => sync_data_valid_n_5,
      \FSM_sequential_rx_state_reg[0]\ => \FSM_sequential_rx_state[3]_i_3_n_0\,
      \FSM_sequential_rx_state_reg[0]_0\ => \FSM_sequential_rx_state[3]_i_7_n_0\,
      \FSM_sequential_rx_state_reg[0]_1\ => \FSM_sequential_rx_state_reg[0]_0\,
      \FSM_sequential_rx_state_reg[0]_2\ => \FSM_sequential_rx_state[0]_i_2_n_0\,
      \FSM_sequential_rx_state_reg[0]_3\ => init_wait_done_reg_n_0,
      \FSM_sequential_rx_state_reg[1]\ => sync_data_valid_n_0,
      \FSM_sequential_rx_state_reg[1]_0\ => \FSM_sequential_rx_state[1]_i_3_n_0\,
      \FSM_sequential_rx_state_reg[3]\ => \FSM_sequential_rx_state[3]_i_9_n_0\,
      Q(3 downto 0) => rx_state(3 downto 0),
      data_in => \^data_in\,
      data_out => data_out,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_lock_reclocked => mmcm_lock_reclocked,
      reset_time_out_reg => sync_cplllock_n_0,
      reset_time_out_reg_0 => reset_time_out_i_3_n_0,
      reset_time_out_reg_1 => reset_time_out_i_4_n_0,
      reset_time_out_reg_2 => reset_time_out_reg_n_0,
      rx_fsm_reset_done_int_reg => sync_data_valid_n_1,
      rx_fsm_reset_done_int_reg_0 => rx_fsm_reset_done_int_i_5_n_0,
      rx_fsm_reset_done_int_reg_1 => time_out_100us_reg_n_0,
      rx_fsm_reset_done_int_reg_2 => time_out_1us_reg_n_0,
      rx_fsm_reset_done_int_reg_3 => rx_fsm_reset_done_int_i_6_n_0,
      time_out_wait_bypass_s3 => time_out_wait_bypass_s3
    );
sync_mmcm_lock_reclocked: entity work.gig_ethernet_pcs_pma_0_sync_block_13
     port map (
      SR(0) => sync_mmcm_lock_reclocked_n_0,
      data_out => mmcm_lock_i,
      data_sync_reg1_0 => data_sync_reg1_0,
      independent_clock_bufg => independent_clock_bufg
    );
sync_run_phase_alignment_int: entity work.gig_ethernet_pcs_pma_0_sync_block_14
     port map (
      data_in => run_phase_alignment_int_reg_n_0,
      data_out => data_out_0,
      data_sync_reg1_0 => data_sync_reg6
    );
sync_time_out_wait_bypass: entity work.gig_ethernet_pcs_pma_0_sync_block_15
     port map (
      data_in => time_out_wait_bypass_reg_n_0,
      data_out => time_out_wait_bypass_s2,
      independent_clock_bufg => independent_clock_bufg
    );
sync_tx_fsm_reset_done_int: entity work.gig_ethernet_pcs_pma_0_sync_block_16
     port map (
      data_in => \^data_in\,
      data_out => rx_fsm_reset_done_int_s2,
      data_sync_reg6_0 => data_sync_reg6
    );
time_out_100us_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000100"
    )
        port map (
      I0 => time_out_2ms_i_4_n_0,
      I1 => time_out_counter_reg(17),
      I2 => time_out_counter_reg(16),
      I3 => time_out_100us_i_2_n_0,
      I4 => time_out_100us_i_3_n_0,
      I5 => time_out_100us_reg_n_0,
      O => time_out_100us_i_1_n_0
    );
time_out_100us_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000002000000000"
    )
        port map (
      I0 => time_out_counter_reg(10),
      I1 => time_out_counter_reg(12),
      I2 => time_out_counter_reg(5),
      I3 => time_out_counter_reg(7),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(14),
      O => time_out_100us_i_2_n_0
    );
time_out_100us_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => time_out_counter_reg(4),
      I1 => time_out_counter_reg(0),
      I2 => time_out_counter_reg(1),
      I3 => time_out_counter_reg(15),
      I4 => time_out_counter_reg(13),
      O => time_out_100us_i_3_n_0
    );
time_out_100us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_100us_i_1_n_0,
      Q => time_out_100us_reg_n_0,
      R => reset_time_out_reg_n_0
    );
time_out_1us_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00100000"
    )
        port map (
      I0 => time_out_2ms_i_2_n_0,
      I1 => time_out_1us_i_2_n_0,
      I2 => time_out_counter_reg(3),
      I3 => time_out_counter_reg(2),
      I4 => time_out_1us_i_3_n_0,
      I5 => time_out_1us_reg_n_0,
      O => time_out_1us_i_1_n_0
    );
time_out_1us_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(17),
      O => time_out_1us_i_2_n_0
    );
time_out_1us_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000010"
    )
        port map (
      I0 => time_out_counter_reg(9),
      I1 => time_out_counter_reg(11),
      I2 => time_out_counter_reg(6),
      I3 => time_out_counter_reg(8),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(12),
      O => time_out_1us_i_3_n_0
    );
time_out_1us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_1us_i_1_n_0,
      Q => time_out_1us_reg_n_0,
      R => reset_time_out_reg_n_0
    );
time_out_2ms_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF01"
    )
        port map (
      I0 => time_out_2ms_i_2_n_0,
      I1 => \time_out_2ms_i_3__0_n_0\,
      I2 => time_out_2ms_i_4_n_0,
      I3 => time_out_2ms_reg_n_0,
      O => time_out_2ms_i_1_n_0
    );
time_out_2ms_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFEFF"
    )
        port map (
      I0 => time_out_counter_reg(10),
      I1 => time_out_counter_reg(14),
      I2 => time_out_counter_reg(5),
      I3 => time_out_counter_reg(7),
      I4 => time_out_100us_i_3_n_0,
      O => time_out_2ms_i_2_n_0
    );
\time_out_2ms_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => time_out_counter_reg(12),
      I1 => time_out_counter_reg(16),
      I2 => time_out_counter_reg(18),
      I3 => time_out_counter_reg(17),
      O => \time_out_2ms_i_3__0_n_0\
    );
time_out_2ms_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFEFFFFFF"
    )
        port map (
      I0 => time_out_counter_reg(2),
      I1 => time_out_counter_reg(3),
      I2 => time_out_counter_reg(8),
      I3 => time_out_counter_reg(9),
      I4 => time_out_counter_reg(11),
      I5 => time_out_counter_reg(6),
      O => time_out_2ms_i_4_n_0
    );
time_out_2ms_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_2ms_i_1_n_0,
      Q => time_out_2ms_reg_n_0,
      R => reset_time_out_reg_n_0
    );
\time_out_counter[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFF7FF"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(18),
      I2 => time_out_counter_reg(16),
      I3 => time_out_counter_reg(12),
      I4 => time_out_2ms_i_2_n_0,
      I5 => time_out_2ms_i_4_n_0,
      O => time_out_counter
    );
\time_out_counter[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(0),
      O => \time_out_counter[0]_i_3_n_0\
    );
\time_out_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_7\,
      Q => time_out_counter_reg(0),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[0]_i_2__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \time_out_counter_reg[0]_i_2__0_n_0\,
      CO(2) => \time_out_counter_reg[0]_i_2__0_n_1\,
      CO(1) => \time_out_counter_reg[0]_i_2__0_n_2\,
      CO(0) => \time_out_counter_reg[0]_i_2__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \time_out_counter_reg[0]_i_2__0_n_4\,
      O(2) => \time_out_counter_reg[0]_i_2__0_n_5\,
      O(1) => \time_out_counter_reg[0]_i_2__0_n_6\,
      O(0) => \time_out_counter_reg[0]_i_2__0_n_7\,
      S(3 downto 1) => time_out_counter_reg(3 downto 1),
      S(0) => \time_out_counter[0]_i_3_n_0\
    );
\time_out_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_5\,
      Q => time_out_counter_reg(10),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_4\,
      Q => time_out_counter_reg(11),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_7\,
      Q => time_out_counter_reg(12),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[12]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[8]_i_1__0_n_0\,
      CO(3) => \time_out_counter_reg[12]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[12]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[12]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[12]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[12]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[12]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[12]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[12]_i_1__0_n_7\,
      S(3 downto 0) => time_out_counter_reg(15 downto 12)
    );
\time_out_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_6\,
      Q => time_out_counter_reg(13),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_5\,
      Q => time_out_counter_reg(14),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_4\,
      Q => time_out_counter_reg(15),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1__0_n_7\,
      Q => time_out_counter_reg(16),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[16]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[12]_i_1__0_n_0\,
      CO(3 downto 2) => \NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \time_out_counter_reg[16]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[16]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED\(3),
      O(2) => \time_out_counter_reg[16]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[16]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[16]_i_1__0_n_7\,
      S(3) => '0',
      S(2 downto 0) => time_out_counter_reg(18 downto 16)
    );
\time_out_counter_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1__0_n_6\,
      Q => time_out_counter_reg(17),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1__0_n_5\,
      Q => time_out_counter_reg(18),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_6\,
      Q => time_out_counter_reg(1),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_5\,
      Q => time_out_counter_reg(2),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_4\,
      Q => time_out_counter_reg(3),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_7\,
      Q => time_out_counter_reg(4),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[4]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[0]_i_2__0_n_0\,
      CO(3) => \time_out_counter_reg[4]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[4]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[4]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[4]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[4]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[4]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[4]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[4]_i_1__0_n_7\,
      S(3 downto 0) => time_out_counter_reg(7 downto 4)
    );
\time_out_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_6\,
      Q => time_out_counter_reg(5),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_5\,
      Q => time_out_counter_reg(6),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_4\,
      Q => time_out_counter_reg(7),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_7\,
      Q => time_out_counter_reg(8),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[8]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[4]_i_1__0_n_0\,
      CO(3) => \time_out_counter_reg[8]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[8]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[8]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[8]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[8]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[8]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[8]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[8]_i_1__0_n_7\,
      S(3 downto 0) => time_out_counter_reg(11 downto 8)
    );
\time_out_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_6\,
      Q => time_out_counter_reg(9),
      R => reset_time_out_reg_n_0
    );
time_out_wait_bypass_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AB00"
    )
        port map (
      I0 => time_out_wait_bypass_reg_n_0,
      I1 => rx_fsm_reset_done_int_s3,
      I2 => \time_out_wait_bypass_i_2__0_n_0\,
      I3 => run_phase_alignment_int_s3_reg_n_0,
      O => time_out_wait_bypass_i_1_n_0
    );
\time_out_wait_bypass_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FBFFFFFF"
    )
        port map (
      I0 => \time_out_wait_bypass_i_3__0_n_0\,
      I1 => wait_bypass_count_reg(1),
      I2 => wait_bypass_count_reg(11),
      I3 => wait_bypass_count_reg(0),
      I4 => \time_out_wait_bypass_i_4__0_n_0\,
      O => \time_out_wait_bypass_i_2__0_n_0\
    );
\time_out_wait_bypass_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => wait_bypass_count_reg(9),
      I1 => wait_bypass_count_reg(4),
      I2 => wait_bypass_count_reg(12),
      I3 => wait_bypass_count_reg(2),
      O => \time_out_wait_bypass_i_3__0_n_0\
    );
\time_out_wait_bypass_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000400000000"
    )
        port map (
      I0 => wait_bypass_count_reg(5),
      I1 => wait_bypass_count_reg(7),
      I2 => wait_bypass_count_reg(3),
      I3 => wait_bypass_count_reg(6),
      I4 => wait_bypass_count_reg(10),
      I5 => wait_bypass_count_reg(8),
      O => \time_out_wait_bypass_i_4__0_n_0\
    );
time_out_wait_bypass_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => '1',
      D => time_out_wait_bypass_i_1_n_0,
      Q => time_out_wait_bypass_reg_n_0,
      R => '0'
    );
time_out_wait_bypass_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_wait_bypass_s2,
      Q => time_out_wait_bypass_s3,
      R => '0'
    );
time_tlock_max1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => time_tlock_max1_carry_n_0,
      CO(2) => time_tlock_max1_carry_n_1,
      CO(1) => time_tlock_max1_carry_n_2,
      CO(0) => time_tlock_max1_carry_n_3,
      CYINIT => '0',
      DI(3) => time_tlock_max1_carry_i_1_n_0,
      DI(2) => time_tlock_max1_carry_i_2_n_0,
      DI(1) => time_tlock_max1_carry_i_3_n_0,
      DI(0) => time_tlock_max1_carry_i_4_n_0,
      O(3 downto 0) => NLW_time_tlock_max1_carry_O_UNCONNECTED(3 downto 0),
      S(3) => time_tlock_max1_carry_i_5_n_0,
      S(2) => time_tlock_max1_carry_i_6_n_0,
      S(1) => time_tlock_max1_carry_i_7_n_0,
      S(0) => time_tlock_max1_carry_i_8_n_0
    );
\time_tlock_max1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => time_tlock_max1_carry_n_0,
      CO(3) => \time_tlock_max1_carry__0_n_0\,
      CO(2) => \time_tlock_max1_carry__0_n_1\,
      CO(1) => \time_tlock_max1_carry__0_n_2\,
      CO(0) => \time_tlock_max1_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => time_out_counter_reg(15),
      DI(2) => \time_tlock_max1_carry__0_i_1_n_0\,
      DI(1) => '0',
      DI(0) => \time_tlock_max1_carry__0_i_2_n_0\,
      O(3 downto 0) => \NLW_time_tlock_max1_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \time_tlock_max1_carry__0_i_3_n_0\,
      S(2) => \time_tlock_max1_carry__0_i_4_n_0\,
      S(1) => \time_tlock_max1_carry__0_i_5_n_0\,
      S(0) => \time_tlock_max1_carry__0_i_6_n_0\
    );
\time_tlock_max1_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(12),
      I1 => time_out_counter_reg(13),
      O => \time_tlock_max1_carry__0_i_1_n_0\
    );
\time_tlock_max1_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(8),
      I1 => time_out_counter_reg(9),
      O => \time_tlock_max1_carry__0_i_2_n_0\
    );
\time_tlock_max1_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(14),
      I1 => time_out_counter_reg(15),
      O => \time_tlock_max1_carry__0_i_3_n_0\
    );
\time_tlock_max1_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(13),
      I1 => time_out_counter_reg(12),
      O => \time_tlock_max1_carry__0_i_4_n_0\
    );
\time_tlock_max1_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(10),
      I1 => time_out_counter_reg(11),
      O => \time_tlock_max1_carry__0_i_5_n_0\
    );
\time_tlock_max1_carry__0_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(9),
      I1 => time_out_counter_reg(8),
      O => \time_tlock_max1_carry__0_i_6_n_0\
    );
\time_tlock_max1_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_tlock_max1_carry__0_n_0\,
      CO(3 downto 2) => \NLW_time_tlock_max1_carry__1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => time_tlock_max1,
      CO(0) => \time_tlock_max1_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => time_out_counter_reg(18),
      DI(0) => \time_tlock_max1_carry__1_i_1_n_0\,
      O(3 downto 0) => \NLW_time_tlock_max1_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3 downto 2) => B"00",
      S(1) => \time_tlock_max1_carry__1_i_2_n_0\,
      S(0) => \time_tlock_max1_carry__1_i_3_n_0\
    );
\time_tlock_max1_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(17),
      O => \time_tlock_max1_carry__1_i_1_n_0\
    );
\time_tlock_max1_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(18),
      O => \time_tlock_max1_carry__1_i_2_n_0\
    );
\time_tlock_max1_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(16),
      O => \time_tlock_max1_carry__1_i_3_n_0\
    );
time_tlock_max1_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(6),
      I1 => time_out_counter_reg(7),
      O => time_tlock_max1_carry_i_1_n_0
    );
time_tlock_max1_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(4),
      I1 => time_out_counter_reg(5),
      O => time_tlock_max1_carry_i_2_n_0
    );
time_tlock_max1_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(2),
      I1 => time_out_counter_reg(3),
      O => time_tlock_max1_carry_i_3_n_0
    );
time_tlock_max1_carry_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(0),
      I1 => time_out_counter_reg(1),
      O => time_tlock_max1_carry_i_4_n_0
    );
time_tlock_max1_carry_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(7),
      I1 => time_out_counter_reg(6),
      O => time_tlock_max1_carry_i_5_n_0
    );
time_tlock_max1_carry_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(5),
      I1 => time_out_counter_reg(4),
      O => time_tlock_max1_carry_i_6_n_0
    );
time_tlock_max1_carry_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(3),
      I1 => time_out_counter_reg(2),
      O => time_tlock_max1_carry_i_7_n_0
    );
time_tlock_max1_carry_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(1),
      I1 => time_out_counter_reg(0),
      O => time_tlock_max1_carry_i_8_n_0
    );
time_tlock_max_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => check_tlock_max_reg_n_0,
      I1 => time_tlock_max1,
      I2 => time_tlock_max,
      O => time_tlock_max_i_1_n_0
    );
time_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_tlock_max_i_1_n_0,
      Q => time_tlock_max,
      R => reset_time_out_reg_n_0
    );
\wait_bypass_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => run_phase_alignment_int_s3_reg_n_0,
      O => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count[0]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \time_out_wait_bypass_i_2__0_n_0\,
      I1 => rx_fsm_reset_done_int_s3,
      O => \wait_bypass_count[0]_i_2__0_n_0\
    );
\wait_bypass_count[0]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_bypass_count_reg(0),
      O => \wait_bypass_count[0]_i_4_n_0\
    );
\wait_bypass_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_7\,
      Q => wait_bypass_count_reg(0),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[0]_i_3__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \wait_bypass_count_reg[0]_i_3__0_n_0\,
      CO(2) => \wait_bypass_count_reg[0]_i_3__0_n_1\,
      CO(1) => \wait_bypass_count_reg[0]_i_3__0_n_2\,
      CO(0) => \wait_bypass_count_reg[0]_i_3__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \wait_bypass_count_reg[0]_i_3__0_n_4\,
      O(2) => \wait_bypass_count_reg[0]_i_3__0_n_5\,
      O(1) => \wait_bypass_count_reg[0]_i_3__0_n_6\,
      O(0) => \wait_bypass_count_reg[0]_i_3__0_n_7\,
      S(3 downto 1) => wait_bypass_count_reg(3 downto 1),
      S(0) => \wait_bypass_count[0]_i_4_n_0\
    );
\wait_bypass_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_5\,
      Q => wait_bypass_count_reg(10),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_4\,
      Q => wait_bypass_count_reg(11),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[12]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(12),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[12]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[8]_i_1__0_n_0\,
      CO(3 downto 0) => \NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED\(3 downto 1),
      O(0) => \wait_bypass_count_reg[12]_i_1__0_n_7\,
      S(3 downto 1) => B"000",
      S(0) => wait_bypass_count_reg(12)
    );
\wait_bypass_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_6\,
      Q => wait_bypass_count_reg(1),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_5\,
      Q => wait_bypass_count_reg(2),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_4\,
      Q => wait_bypass_count_reg(3),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(4),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[4]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[0]_i_3__0_n_0\,
      CO(3) => \wait_bypass_count_reg[4]_i_1__0_n_0\,
      CO(2) => \wait_bypass_count_reg[4]_i_1__0_n_1\,
      CO(1) => \wait_bypass_count_reg[4]_i_1__0_n_2\,
      CO(0) => \wait_bypass_count_reg[4]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[4]_i_1__0_n_4\,
      O(2) => \wait_bypass_count_reg[4]_i_1__0_n_5\,
      O(1) => \wait_bypass_count_reg[4]_i_1__0_n_6\,
      O(0) => \wait_bypass_count_reg[4]_i_1__0_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(7 downto 4)
    );
\wait_bypass_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_6\,
      Q => wait_bypass_count_reg(5),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_5\,
      Q => wait_bypass_count_reg(6),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_4\,
      Q => wait_bypass_count_reg(7),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(8),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[8]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[4]_i_1__0_n_0\,
      CO(3) => \wait_bypass_count_reg[8]_i_1__0_n_0\,
      CO(2) => \wait_bypass_count_reg[8]_i_1__0_n_1\,
      CO(1) => \wait_bypass_count_reg[8]_i_1__0_n_2\,
      CO(0) => \wait_bypass_count_reg[8]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[8]_i_1__0_n_4\,
      O(2) => \wait_bypass_count_reg[8]_i_1__0_n_5\,
      O(1) => \wait_bypass_count_reg[8]_i_1__0_n_6\,
      O(0) => \wait_bypass_count_reg[8]_i_1__0_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(11 downto 8)
    );
\wait_bypass_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_6\,
      Q => wait_bypass_count_reg(9),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_time_cnt[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      O => \wait_time_cnt0__0\(0)
    );
\wait_time_cnt[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      I1 => wait_time_cnt_reg(1),
      O => \wait_time_cnt[1]_i_1__0_n_0\
    );
\wait_time_cnt[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => wait_time_cnt_reg(1),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(2),
      O => \wait_time_cnt[2]_i_1__0_n_0\
    );
\wait_time_cnt[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE01"
    )
        port map (
      I0 => wait_time_cnt_reg(2),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(1),
      I3 => wait_time_cnt_reg(3),
      O => \wait_time_cnt[3]_i_1__0_n_0\
    );
\wait_time_cnt[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0001"
    )
        port map (
      I0 => wait_time_cnt_reg(3),
      I1 => wait_time_cnt_reg(1),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(2),
      I4 => wait_time_cnt_reg(4),
      O => \wait_time_cnt[4]_i_1__0_n_0\
    );
\wait_time_cnt[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000001"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[5]_i_1__0_n_0\
    );
\wait_time_cnt[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => rx_state(0),
      I1 => rx_state(1),
      I2 => rx_state(3),
      O => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt[6]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4__0_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => \wait_time_cnt[6]_i_2__0_n_0\
    );
\wait_time_cnt[6]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4__0_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => \wait_time_cnt[6]_i_3__0_n_0\
    );
\wait_time_cnt[6]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[6]_i_4__0_n_0\
    );
\wait_time_cnt_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt0__0\(0),
      Q => wait_time_cnt_reg(0),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[1]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(1),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[2]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(2),
      S => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[3]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(3),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[4]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(4),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[5]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(5),
      S => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[6]_i_3__0_n_0\,
      Q => wait_time_cnt_reg(6),
      S => \wait_time_cnt[6]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_TX_STARTUP_FSM is
  port (
    mmcm_reset : out STD_LOGIC;
    gt0_cpllreset_t : out STD_LOGIC;
    data_in : out STD_LOGIC;
    gt0_txuserrdy_t : out STD_LOGIC;
    gt0_gttxreset_in0_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : in STD_LOGIC;
    gt0_cpllrefclklost_i : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    data_sync_reg1_1 : in STD_LOGIC;
    data_sync_reg1_2 : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_TX_STARTUP_FSM;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_TX_STARTUP_FSM is
  signal CPLL_RESET_i_1_n_0 : STD_LOGIC;
  signal CPLL_RESET_i_2_n_0 : STD_LOGIC;
  signal \FSM_sequential_tx_state[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[0]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_8_n_0\ : STD_LOGIC;
  signal GTTXRESET : STD_LOGIC;
  signal MMCM_RESET_i_1_n_0 : STD_LOGIC;
  signal TXUSERRDY_i_1_n_0 : STD_LOGIC;
  signal clear : STD_LOGIC;
  signal \^data_in\ : STD_LOGIC;
  signal data_out : STD_LOGIC;
  signal \^gt0_cpllreset_t\ : STD_LOGIC;
  signal \^gt0_txuserrdy_t\ : STD_LOGIC;
  signal gttxreset_i_i_1_n_0 : STD_LOGIC;
  signal \init_wait_count[0]_i_1_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_1_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_3_n_0\ : STD_LOGIC;
  signal init_wait_count_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal init_wait_done_i_1_n_0 : STD_LOGIC;
  signal init_wait_done_reg_n_0 : STD_LOGIC;
  signal \mmcm_lock_count[2]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[3]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[4]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[5]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[6]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_2_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_3_n_0\ : STD_LOGIC;
  signal mmcm_lock_count_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal mmcm_lock_i : STD_LOGIC;
  signal mmcm_lock_reclocked : STD_LOGIC;
  signal mmcm_lock_reclocked_i_1_n_0 : STD_LOGIC;
  signal mmcm_lock_reclocked_i_2_n_0 : STD_LOGIC;
  signal \^mmcm_reset\ : STD_LOGIC;
  signal \p_0_in__0\ : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \p_0_in__1\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pll_reset_asserted_i_1_n_0 : STD_LOGIC;
  signal pll_reset_asserted_i_2_n_0 : STD_LOGIC;
  signal pll_reset_asserted_reg_n_0 : STD_LOGIC;
  signal refclk_stable_count : STD_LOGIC;
  signal \refclk_stable_count[0]_i_3_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_4_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_5_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_6_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_7_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_8_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_9_n_0\ : STD_LOGIC;
  signal refclk_stable_count_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \refclk_stable_count_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal refclk_stable_i_1_n_0 : STD_LOGIC;
  signal refclk_stable_i_2_n_0 : STD_LOGIC;
  signal refclk_stable_i_3_n_0 : STD_LOGIC;
  signal refclk_stable_i_4_n_0 : STD_LOGIC;
  signal refclk_stable_i_5_n_0 : STD_LOGIC;
  signal refclk_stable_i_6_n_0 : STD_LOGIC;
  signal refclk_stable_reg_n_0 : STD_LOGIC;
  signal reset_time_out : STD_LOGIC;
  signal \reset_time_out_i_2__0_n_0\ : STD_LOGIC;
  signal run_phase_alignment_int_i_1_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_reg_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_s3 : STD_LOGIC;
  signal sel : STD_LOGIC;
  signal sync_cplllock_n_0 : STD_LOGIC;
  signal sync_cplllock_n_1 : STD_LOGIC;
  signal sync_mmcm_lock_reclocked_n_0 : STD_LOGIC;
  signal time_out_2ms_i_1_n_0 : STD_LOGIC;
  signal \time_out_2ms_i_2__0_n_0\ : STD_LOGIC;
  signal time_out_2ms_i_3_n_0 : STD_LOGIC;
  signal \time_out_2ms_i_4__0_n_0\ : STD_LOGIC;
  signal time_out_2ms_i_5_n_0 : STD_LOGIC;
  signal time_out_2ms_reg_n_0 : STD_LOGIC;
  signal time_out_500us_i_1_n_0 : STD_LOGIC;
  signal time_out_500us_i_2_n_0 : STD_LOGIC;
  signal time_out_500us_reg_n_0 : STD_LOGIC;
  signal time_out_counter : STD_LOGIC;
  signal \time_out_counter[0]_i_3__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[0]_i_4_n_0\ : STD_LOGIC;
  signal time_out_counter_reg : STD_LOGIC_VECTOR ( 18 downto 0 );
  signal \time_out_counter_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal time_out_wait_bypass_i_1_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_2_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_3_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_4_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_5_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_reg_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_s2 : STD_LOGIC;
  signal time_out_wait_bypass_s3 : STD_LOGIC;
  signal time_tlock_max_i_1_n_0 : STD_LOGIC;
  signal time_tlock_max_i_2_n_0 : STD_LOGIC;
  signal time_tlock_max_i_3_n_0 : STD_LOGIC;
  signal time_tlock_max_i_4_n_0 : STD_LOGIC;
  signal time_tlock_max_reg_n_0 : STD_LOGIC;
  signal tx_fsm_reset_done_int_i_1_n_0 : STD_LOGIC;
  signal tx_fsm_reset_done_int_s2 : STD_LOGIC;
  signal tx_fsm_reset_done_int_s3 : STD_LOGIC;
  signal tx_state : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \tx_state__0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal txresetdone_s2 : STD_LOGIC;
  signal txresetdone_s3 : STD_LOGIC;
  signal \wait_bypass_count[0]_i_2_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_4__0_n_0\ : STD_LOGIC;
  signal wait_bypass_count_reg : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal \wait_bypass_count_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal wait_time_cnt0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal wait_time_cnt0_0 : STD_LOGIC;
  signal \wait_time_cnt[1]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[2]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[3]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[4]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[5]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_3_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_4_n_0\ : STD_LOGIC;
  signal wait_time_cnt_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of CPLL_RESET_i_2 : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[0]_i_2\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[0]_i_3\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[1]_i_1\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[3]_i_2\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[3]_i_7\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[3]_i_8\ : label is "soft_lutpair59";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[0]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[1]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[2]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[3]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute SOFT_HLUTNM of MMCM_RESET_i_1 : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of TXUSERRDY_i_1 : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of gttxreset_i_i_1 : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \init_wait_count[1]_i_1\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \init_wait_count[2]_i_1\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \init_wait_count[3]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \init_wait_count[4]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_2\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_3\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \mmcm_lock_count[1]_i_1\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \mmcm_lock_count[2]_i_1\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \mmcm_lock_count[3]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \mmcm_lock_count[4]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \mmcm_lock_count[6]_i_1\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \mmcm_lock_count[7]_i_3\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of pll_reset_asserted_i_2 : label is "soft_lutpair60";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[12]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[16]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[20]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[24]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[28]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[8]_i_1\ : label is 11;
  attribute SOFT_HLUTNM of run_phase_alignment_int_i_1 : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \time_out_2ms_i_4__0\ : label is "soft_lutpair56";
  attribute ADDER_THRESHOLD of \time_out_counter_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[12]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[16]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[8]_i_1\ : label is 11;
  attribute SOFT_HLUTNM of time_tlock_max_i_4 : label is "soft_lutpair56";
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[0]_i_3\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[12]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[16]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[8]_i_1\ : label is 11;
  attribute SOFT_HLUTNM of \wait_time_cnt[0]_i_1\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \wait_time_cnt[1]_i_1\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \wait_time_cnt[3]_i_1\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \wait_time_cnt[4]_i_1\ : label is "soft_lutpair63";
begin
  data_in <= \^data_in\;
  gt0_cpllreset_t <= \^gt0_cpllreset_t\;
  gt0_txuserrdy_t <= \^gt0_txuserrdy_t\;
  mmcm_reset <= \^mmcm_reset\;
CPLL_RESET_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF1F0000001F"
    )
        port map (
      I0 => pll_reset_asserted_reg_n_0,
      I1 => gt0_cpllrefclklost_i,
      I2 => refclk_stable_reg_n_0,
      I3 => CPLL_RESET_i_2_n_0,
      I4 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I5 => \^gt0_cpllreset_t\,
      O => CPLL_RESET_i_1_n_0
    );
CPLL_RESET_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      O => CPLL_RESET_i_2_n_0
    );
CPLL_RESET_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => CPLL_RESET_i_1_n_0,
      Q => \^gt0_cpllreset_t\,
      R => \out\(0)
    );
\FSM_sequential_tx_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F3FFF3F0F5F0F5F0"
    )
        port map (
      I0 => \FSM_sequential_tx_state[2]_i_2_n_0\,
      I1 => \FSM_sequential_tx_state[0]_i_2_n_0\,
      I2 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I3 => tx_state(2),
      I4 => time_out_2ms_reg_n_0,
      I5 => tx_state(1),
      O => \tx_state__0\(0)
    );
\FSM_sequential_tx_state[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => reset_time_out,
      I1 => time_out_500us_reg_n_0,
      O => \FSM_sequential_tx_state[0]_i_2_n_0\
    );
\FSM_sequential_tx_state[0]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      O => \FSM_sequential_tx_state[0]_i_3_n_0\
    );
\FSM_sequential_tx_state[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"005A001A"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(0),
      I3 => tx_state(3),
      I4 => \FSM_sequential_tx_state[2]_i_2_n_0\,
      O => \tx_state__0\(1)
    );
\FSM_sequential_tx_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"04000C0C06020C0C"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(3),
      I3 => \FSM_sequential_tx_state[2]_i_2_n_0\,
      I4 => tx_state(0),
      I5 => time_out_2ms_reg_n_0,
      O => \tx_state__0\(2)
    );
\FSM_sequential_tx_state[2]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => time_tlock_max_reg_n_0,
      I1 => reset_time_out,
      I2 => mmcm_lock_reclocked,
      O => \FSM_sequential_tx_state[2]_i_2_n_0\
    );
\FSM_sequential_tx_state[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F4FF4444"
    )
        port map (
      I0 => time_out_wait_bypass_s3,
      I1 => tx_state(3),
      I2 => reset_time_out,
      I3 => time_out_500us_reg_n_0,
      I4 => \FSM_sequential_tx_state[3]_i_8_n_0\,
      O => \tx_state__0\(3)
    );
\FSM_sequential_tx_state[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00BA000000000000"
    )
        port map (
      I0 => txresetdone_s3,
      I1 => reset_time_out,
      I2 => time_out_500us_reg_n_0,
      I3 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I4 => tx_state(2),
      I5 => tx_state(1),
      O => \FSM_sequential_tx_state[3]_i_3_n_0\
    );
\FSM_sequential_tx_state[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000300FF00AA"
    )
        port map (
      I0 => init_wait_done_reg_n_0,
      I1 => \wait_time_cnt[6]_i_4_n_0\,
      I2 => wait_time_cnt_reg(6),
      I3 => tx_state(0),
      I4 => tx_state(3),
      I5 => CPLL_RESET_i_2_n_0,
      O => \FSM_sequential_tx_state[3]_i_4_n_0\
    );
\FSM_sequential_tx_state[3]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0404040400040000"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I3 => reset_time_out,
      I4 => time_tlock_max_reg_n_0,
      I5 => mmcm_lock_reclocked,
      O => \FSM_sequential_tx_state[3]_i_6_n_0\
    );
\FSM_sequential_tx_state[3]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1000"
    )
        port map (
      I0 => tx_state(2),
      I1 => tx_state(3),
      I2 => tx_state(0),
      I3 => tx_state(1),
      O => \FSM_sequential_tx_state[3]_i_7_n_0\
    );
\FSM_sequential_tx_state[3]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      I2 => tx_state(2),
      I3 => tx_state(1),
      O => \FSM_sequential_tx_state[3]_i_8_n_0\
    );
\FSM_sequential_tx_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(0),
      Q => tx_state(0),
      R => \out\(0)
    );
\FSM_sequential_tx_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(1),
      Q => tx_state(1),
      R => \out\(0)
    );
\FSM_sequential_tx_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(2),
      Q => tx_state(2),
      R => \out\(0)
    );
\FSM_sequential_tx_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(3),
      Q => tx_state(3),
      R => \out\(0)
    );
MMCM_RESET_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70004"
    )
        port map (
      I0 => tx_state(2),
      I1 => tx_state(0),
      I2 => tx_state(3),
      I3 => tx_state(1),
      I4 => \^mmcm_reset\,
      O => MMCM_RESET_i_1_n_0
    );
MMCM_RESET_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => MMCM_RESET_i_1_n_0,
      Q => \^mmcm_reset\,
      R => \out\(0)
    );
TXUSERRDY_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFD2000"
    )
        port map (
      I0 => tx_state(0),
      I1 => tx_state(3),
      I2 => tx_state(2),
      I3 => tx_state(1),
      I4 => \^gt0_txuserrdy_t\,
      O => TXUSERRDY_i_1_n_0
    );
TXUSERRDY_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => TXUSERRDY_i_1_n_0,
      Q => \^gt0_txuserrdy_t\,
      R => \out\(0)
    );
gttxreset_i_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0100"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(1),
      I2 => tx_state(2),
      I3 => tx_state(0),
      I4 => GTTXRESET,
      O => gttxreset_i_i_1_n_0
    );
gttxreset_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gttxreset_i_i_1_n_0,
      Q => GTTXRESET,
      R => \out\(0)
    );
gtxe2_i_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => GTTXRESET,
      I1 => \^data_in\,
      I2 => gtxe2_i,
      O => gt0_gttxreset_in0_out
    );
\init_wait_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => init_wait_count_reg(0),
      O => \init_wait_count[0]_i_1_n_0\
    );
\init_wait_count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      O => \p_0_in__0\(1)
    );
\init_wait_count[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      O => \p_0_in__0\(2)
    );
\init_wait_count[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => init_wait_count_reg(1),
      I1 => init_wait_count_reg(2),
      I2 => init_wait_count_reg(0),
      I3 => init_wait_count_reg(3),
      O => \p_0_in__0\(3)
    );
\init_wait_count[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      O => \p_0_in__0\(4)
    );
\init_wait_count[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      I5 => init_wait_count_reg(5),
      O => \p_0_in__0\(5)
    );
\init_wait_count[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => \init_wait_count[6]_i_3_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \init_wait_count[6]_i_1_n_0\
    );
\init_wait_count[6]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF40"
    )
        port map (
      I0 => \init_wait_count[6]_i_3_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \p_0_in__0\(6)
    );
\init_wait_count[6]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => init_wait_count_reg(3),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      I3 => init_wait_count_reg(5),
      O => \init_wait_count[6]_i_3_n_0\
    );
\init_wait_count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \init_wait_count[0]_i_1_n_0\,
      Q => init_wait_count_reg(0)
    );
\init_wait_count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(1),
      Q => init_wait_count_reg(1)
    );
\init_wait_count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(2),
      Q => init_wait_count_reg(2)
    );
\init_wait_count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(3),
      Q => init_wait_count_reg(3)
    );
\init_wait_count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(4),
      Q => init_wait_count_reg(4)
    );
\init_wait_count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(5),
      Q => init_wait_count_reg(5)
    );
\init_wait_count_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(6),
      Q => init_wait_count_reg(6)
    );
init_wait_done_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0010"
    )
        port map (
      I0 => \init_wait_count[6]_i_3_n_0\,
      I1 => init_wait_count_reg(4),
      I2 => init_wait_count_reg(6),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_done_reg_n_0,
      O => init_wait_done_i_1_n_0
    );
init_wait_done_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      CLR => \out\(0),
      D => init_wait_done_i_1_n_0,
      Q => init_wait_done_reg_n_0
    );
\mmcm_lock_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      O => \p_0_in__1\(0)
    );
\mmcm_lock_count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      I1 => mmcm_lock_count_reg(1),
      O => \p_0_in__1\(1)
    );
\mmcm_lock_count[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => mmcm_lock_count_reg(1),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(2),
      O => \mmcm_lock_count[2]_i_1_n_0\
    );
\mmcm_lock_count[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => mmcm_lock_count_reg(2),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(1),
      I3 => mmcm_lock_count_reg(3),
      O => \mmcm_lock_count[3]_i_1_n_0\
    );
\mmcm_lock_count[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => mmcm_lock_count_reg(3),
      I1 => mmcm_lock_count_reg(1),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(2),
      I4 => mmcm_lock_count_reg(4),
      O => \mmcm_lock_count[4]_i_1_n_0\
    );
\mmcm_lock_count[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => \mmcm_lock_count[5]_i_1_n_0\
    );
\mmcm_lock_count[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => mmcm_lock_reclocked_i_2_n_0,
      I1 => mmcm_lock_count_reg(6),
      O => \mmcm_lock_count[6]_i_1_n_0\
    );
\mmcm_lock_count[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => mmcm_lock_reclocked_i_2_n_0,
      I1 => mmcm_lock_count_reg(6),
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_2_n_0\
    );
\mmcm_lock_count[7]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => mmcm_lock_count_reg(6),
      I1 => mmcm_lock_reclocked_i_2_n_0,
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_3_n_0\
    );
\mmcm_lock_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \p_0_in__1\(0),
      Q => mmcm_lock_count_reg(0),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \p_0_in__1\(1),
      Q => mmcm_lock_count_reg(1),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[2]_i_1_n_0\,
      Q => mmcm_lock_count_reg(2),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[3]_i_1_n_0\,
      Q => mmcm_lock_count_reg(3),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[4]_i_1_n_0\,
      Q => mmcm_lock_count_reg(4),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[5]_i_1_n_0\,
      Q => mmcm_lock_count_reg(5),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[6]_i_1_n_0\,
      Q => mmcm_lock_count_reg(6),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[7]_i_3_n_0\,
      Q => mmcm_lock_count_reg(7),
      R => sync_mmcm_lock_reclocked_n_0
    );
mmcm_lock_reclocked_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEA0000"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => mmcm_lock_count_reg(7),
      I2 => mmcm_lock_count_reg(6),
      I3 => mmcm_lock_reclocked_i_2_n_0,
      I4 => mmcm_lock_i,
      O => mmcm_lock_reclocked_i_1_n_0
    );
mmcm_lock_reclocked_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => mmcm_lock_reclocked_i_2_n_0
    );
mmcm_lock_reclocked_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => mmcm_lock_reclocked_i_1_n_0,
      Q => mmcm_lock_reclocked,
      R => '0'
    );
pll_reset_asserted_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000CD55CCCCCCCC"
    )
        port map (
      I0 => tx_state(3),
      I1 => pll_reset_asserted_reg_n_0,
      I2 => gt0_cpllrefclklost_i,
      I3 => refclk_stable_reg_n_0,
      I4 => tx_state(1),
      I5 => pll_reset_asserted_i_2_n_0,
      O => pll_reset_asserted_i_1_n_0
    );
pll_reset_asserted_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => tx_state(0),
      I1 => tx_state(3),
      I2 => tx_state(2),
      O => pll_reset_asserted_i_2_n_0
    );
pll_reset_asserted_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pll_reset_asserted_i_1_n_0,
      Q => pll_reset_asserted_reg_n_0,
      R => \out\(0)
    );
\refclk_stable_count[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFFFF"
    )
        port map (
      I0 => \refclk_stable_count[0]_i_3_n_0\,
      I1 => \refclk_stable_count[0]_i_4_n_0\,
      I2 => \refclk_stable_count[0]_i_5_n_0\,
      I3 => \refclk_stable_count[0]_i_6_n_0\,
      I4 => \refclk_stable_count[0]_i_7_n_0\,
      I5 => \refclk_stable_count[0]_i_8_n_0\,
      O => refclk_stable_count
    );
\refclk_stable_count[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFDFFFFFFFFFFFFF"
    )
        port map (
      I0 => refclk_stable_count_reg(13),
      I1 => refclk_stable_count_reg(12),
      I2 => refclk_stable_count_reg(10),
      I3 => refclk_stable_count_reg(11),
      I4 => refclk_stable_count_reg(9),
      I5 => refclk_stable_count_reg(8),
      O => \refclk_stable_count[0]_i_3_n_0\
    );
\refclk_stable_count[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFDF"
    )
        port map (
      I0 => refclk_stable_count_reg(19),
      I1 => refclk_stable_count_reg(18),
      I2 => refclk_stable_count_reg(16),
      I3 => refclk_stable_count_reg(17),
      I4 => refclk_stable_count_reg(15),
      I5 => refclk_stable_count_reg(14),
      O => \refclk_stable_count[0]_i_4_n_0\
    );
\refclk_stable_count[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => refclk_stable_count_reg(30),
      I1 => refclk_stable_count_reg(31),
      I2 => refclk_stable_count_reg(28),
      I3 => refclk_stable_count_reg(29),
      I4 => refclk_stable_count_reg(27),
      I5 => refclk_stable_count_reg(26),
      O => \refclk_stable_count[0]_i_5_n_0\
    );
\refclk_stable_count[0]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => refclk_stable_count_reg(24),
      I1 => refclk_stable_count_reg(25),
      I2 => refclk_stable_count_reg(22),
      I3 => refclk_stable_count_reg(23),
      I4 => refclk_stable_count_reg(21),
      I5 => refclk_stable_count_reg(20),
      O => \refclk_stable_count[0]_i_6_n_0\
    );
\refclk_stable_count[0]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => refclk_stable_count_reg(0),
      I1 => refclk_stable_count_reg(1),
      O => \refclk_stable_count[0]_i_7_n_0\
    );
\refclk_stable_count[0]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFF7"
    )
        port map (
      I0 => refclk_stable_count_reg(6),
      I1 => refclk_stable_count_reg(7),
      I2 => refclk_stable_count_reg(4),
      I3 => refclk_stable_count_reg(5),
      I4 => refclk_stable_count_reg(3),
      I5 => refclk_stable_count_reg(2),
      O => \refclk_stable_count[0]_i_8_n_0\
    );
\refclk_stable_count[0]_i_9\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => refclk_stable_count_reg(0),
      O => \refclk_stable_count[0]_i_9_n_0\
    );
\refclk_stable_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_7\,
      Q => refclk_stable_count_reg(0),
      R => '0'
    );
\refclk_stable_count_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \refclk_stable_count_reg[0]_i_2_n_0\,
      CO(2) => \refclk_stable_count_reg[0]_i_2_n_1\,
      CO(1) => \refclk_stable_count_reg[0]_i_2_n_2\,
      CO(0) => \refclk_stable_count_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \refclk_stable_count_reg[0]_i_2_n_4\,
      O(2) => \refclk_stable_count_reg[0]_i_2_n_5\,
      O(1) => \refclk_stable_count_reg[0]_i_2_n_6\,
      O(0) => \refclk_stable_count_reg[0]_i_2_n_7\,
      S(3 downto 1) => refclk_stable_count_reg(3 downto 1),
      S(0) => \refclk_stable_count[0]_i_9_n_0\
    );
\refclk_stable_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_5\,
      Q => refclk_stable_count_reg(10),
      R => '0'
    );
\refclk_stable_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_4\,
      Q => refclk_stable_count_reg(11),
      R => '0'
    );
\refclk_stable_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_7\,
      Q => refclk_stable_count_reg(12),
      R => '0'
    );
\refclk_stable_count_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[8]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[12]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[12]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[12]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[12]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[12]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[12]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[12]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(15 downto 12)
    );
\refclk_stable_count_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_6\,
      Q => refclk_stable_count_reg(13),
      R => '0'
    );
\refclk_stable_count_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_5\,
      Q => refclk_stable_count_reg(14),
      R => '0'
    );
\refclk_stable_count_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_4\,
      Q => refclk_stable_count_reg(15),
      R => '0'
    );
\refclk_stable_count_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_7\,
      Q => refclk_stable_count_reg(16),
      R => '0'
    );
\refclk_stable_count_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[12]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[16]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[16]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[16]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[16]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[16]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[16]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[16]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(19 downto 16)
    );
\refclk_stable_count_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_6\,
      Q => refclk_stable_count_reg(17),
      R => '0'
    );
\refclk_stable_count_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_5\,
      Q => refclk_stable_count_reg(18),
      R => '0'
    );
\refclk_stable_count_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_4\,
      Q => refclk_stable_count_reg(19),
      R => '0'
    );
\refclk_stable_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_6\,
      Q => refclk_stable_count_reg(1),
      R => '0'
    );
\refclk_stable_count_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_7\,
      Q => refclk_stable_count_reg(20),
      R => '0'
    );
\refclk_stable_count_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[16]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[20]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[20]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[20]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[20]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[20]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[20]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[20]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(23 downto 20)
    );
\refclk_stable_count_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_6\,
      Q => refclk_stable_count_reg(21),
      R => '0'
    );
\refclk_stable_count_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_5\,
      Q => refclk_stable_count_reg(22),
      R => '0'
    );
\refclk_stable_count_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_4\,
      Q => refclk_stable_count_reg(23),
      R => '0'
    );
\refclk_stable_count_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_7\,
      Q => refclk_stable_count_reg(24),
      R => '0'
    );
\refclk_stable_count_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[20]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[24]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[24]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[24]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[24]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[24]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[24]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[24]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(27 downto 24)
    );
\refclk_stable_count_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_6\,
      Q => refclk_stable_count_reg(25),
      R => '0'
    );
\refclk_stable_count_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_5\,
      Q => refclk_stable_count_reg(26),
      R => '0'
    );
\refclk_stable_count_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_4\,
      Q => refclk_stable_count_reg(27),
      R => '0'
    );
\refclk_stable_count_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_7\,
      Q => refclk_stable_count_reg(28),
      R => '0'
    );
\refclk_stable_count_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[24]_i_1_n_0\,
      CO(3) => \NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \refclk_stable_count_reg[28]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[28]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[28]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[28]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[28]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[28]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(31 downto 28)
    );
\refclk_stable_count_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_6\,
      Q => refclk_stable_count_reg(29),
      R => '0'
    );
\refclk_stable_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_5\,
      Q => refclk_stable_count_reg(2),
      R => '0'
    );
\refclk_stable_count_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_5\,
      Q => refclk_stable_count_reg(30),
      R => '0'
    );
\refclk_stable_count_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_4\,
      Q => refclk_stable_count_reg(31),
      R => '0'
    );
\refclk_stable_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_4\,
      Q => refclk_stable_count_reg(3),
      R => '0'
    );
\refclk_stable_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_7\,
      Q => refclk_stable_count_reg(4),
      R => '0'
    );
\refclk_stable_count_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[0]_i_2_n_0\,
      CO(3) => \refclk_stable_count_reg[4]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[4]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[4]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[4]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[4]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[4]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[4]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(7 downto 4)
    );
\refclk_stable_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_6\,
      Q => refclk_stable_count_reg(5),
      R => '0'
    );
\refclk_stable_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_5\,
      Q => refclk_stable_count_reg(6),
      R => '0'
    );
\refclk_stable_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_4\,
      Q => refclk_stable_count_reg(7),
      R => '0'
    );
\refclk_stable_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_7\,
      Q => refclk_stable_count_reg(8),
      R => '0'
    );
\refclk_stable_count_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[4]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[8]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[8]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[8]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[8]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[8]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[8]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[8]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(11 downto 8)
    );
\refclk_stable_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_6\,
      Q => refclk_stable_count_reg(9),
      R => '0'
    );
refclk_stable_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \refclk_stable_count[0]_i_7_n_0\,
      I1 => refclk_stable_i_2_n_0,
      I2 => refclk_stable_i_3_n_0,
      I3 => refclk_stable_i_4_n_0,
      I4 => refclk_stable_i_5_n_0,
      I5 => refclk_stable_i_6_n_0,
      O => refclk_stable_i_1_n_0
    );
refclk_stable_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001000000000000"
    )
        port map (
      I0 => refclk_stable_count_reg(4),
      I1 => refclk_stable_count_reg(5),
      I2 => refclk_stable_count_reg(2),
      I3 => refclk_stable_count_reg(3),
      I4 => refclk_stable_count_reg(7),
      I5 => refclk_stable_count_reg(6),
      O => refclk_stable_i_2_n_0
    );
refclk_stable_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000200000000000"
    )
        port map (
      I0 => refclk_stable_count_reg(10),
      I1 => refclk_stable_count_reg(11),
      I2 => refclk_stable_count_reg(8),
      I3 => refclk_stable_count_reg(9),
      I4 => refclk_stable_count_reg(12),
      I5 => refclk_stable_count_reg(13),
      O => refclk_stable_i_3_n_0
    );
refclk_stable_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => refclk_stable_count_reg(16),
      I1 => refclk_stable_count_reg(17),
      I2 => refclk_stable_count_reg(14),
      I3 => refclk_stable_count_reg(15),
      I4 => refclk_stable_count_reg(18),
      I5 => refclk_stable_count_reg(19),
      O => refclk_stable_i_4_n_0
    );
refclk_stable_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => refclk_stable_count_reg(22),
      I1 => refclk_stable_count_reg(23),
      I2 => refclk_stable_count_reg(20),
      I3 => refclk_stable_count_reg(21),
      I4 => refclk_stable_count_reg(25),
      I5 => refclk_stable_count_reg(24),
      O => refclk_stable_i_5_n_0
    );
refclk_stable_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => refclk_stable_count_reg(28),
      I1 => refclk_stable_count_reg(29),
      I2 => refclk_stable_count_reg(26),
      I3 => refclk_stable_count_reg(27),
      I4 => refclk_stable_count_reg(31),
      I5 => refclk_stable_count_reg(30),
      O => refclk_stable_i_6_n_0
    );
refclk_stable_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => refclk_stable_i_1_n_0,
      Q => refclk_stable_reg_n_0,
      R => '0'
    );
\reset_time_out_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"440000FF50505050"
    )
        port map (
      I0 => tx_state(3),
      I1 => txresetdone_s3,
      I2 => init_wait_done_reg_n_0,
      I3 => tx_state(1),
      I4 => tx_state(2),
      I5 => tx_state(0),
      O => \reset_time_out_i_2__0_n_0\
    );
reset_time_out_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => sync_cplllock_n_0,
      Q => reset_time_out,
      R => \out\(0)
    );
run_phase_alignment_int_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB0002"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      I2 => tx_state(2),
      I3 => tx_state(1),
      I4 => run_phase_alignment_int_reg_n_0,
      O => run_phase_alignment_int_i_1_n_0
    );
run_phase_alignment_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => run_phase_alignment_int_i_1_n_0,
      Q => run_phase_alignment_int_reg_n_0,
      R => \out\(0)
    );
run_phase_alignment_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => '1',
      D => data_out,
      Q => run_phase_alignment_int_s3,
      R => '0'
    );
sync_TXRESETDONE: entity work.gig_ethernet_pcs_pma_0_sync_block_4
     port map (
      data_out => txresetdone_s2,
      data_sync_reg1_0 => data_sync_reg1_0,
      independent_clock_bufg => independent_clock_bufg
    );
sync_cplllock: entity work.gig_ethernet_pcs_pma_0_sync_block_5
     port map (
      E(0) => sync_cplllock_n_1,
      \FSM_sequential_tx_state_reg[0]\ => \FSM_sequential_tx_state[3]_i_3_n_0\,
      \FSM_sequential_tx_state_reg[0]_0\ => \FSM_sequential_tx_state[3]_i_4_n_0\,
      \FSM_sequential_tx_state_reg[0]_1\ => \FSM_sequential_tx_state[3]_i_6_n_0\,
      \FSM_sequential_tx_state_reg[0]_2\ => time_out_2ms_reg_n_0,
      \FSM_sequential_tx_state_reg[0]_3\ => \FSM_sequential_tx_state[3]_i_7_n_0\,
      \FSM_sequential_tx_state_reg[0]_4\ => pll_reset_asserted_reg_n_0,
      \FSM_sequential_tx_state_reg[0]_5\ => refclk_stable_reg_n_0,
      \FSM_sequential_tx_state_reg[0]_6\ => \FSM_sequential_tx_state[0]_i_3_n_0\,
      Q(3 downto 0) => tx_state(3 downto 0),
      data_sync_reg1_0 => data_sync_reg1_2,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_lock_reclocked => mmcm_lock_reclocked,
      reset_time_out => reset_time_out,
      reset_time_out_reg => sync_cplllock_n_0,
      reset_time_out_reg_0 => \reset_time_out_i_2__0_n_0\,
      reset_time_out_reg_1 => init_wait_done_reg_n_0
    );
sync_mmcm_lock_reclocked: entity work.gig_ethernet_pcs_pma_0_sync_block_6
     port map (
      SR(0) => sync_mmcm_lock_reclocked_n_0,
      data_out => mmcm_lock_i,
      data_sync_reg1_0 => data_sync_reg1_1,
      independent_clock_bufg => independent_clock_bufg
    );
sync_run_phase_alignment_int: entity work.gig_ethernet_pcs_pma_0_sync_block_7
     port map (
      data_in => run_phase_alignment_int_reg_n_0,
      data_out => data_out,
      data_sync_reg6_0 => data_sync_reg1
    );
sync_time_out_wait_bypass: entity work.gig_ethernet_pcs_pma_0_sync_block_8
     port map (
      data_in => time_out_wait_bypass_reg_n_0,
      data_out => time_out_wait_bypass_s2,
      independent_clock_bufg => independent_clock_bufg
    );
sync_tx_fsm_reset_done_int: entity work.gig_ethernet_pcs_pma_0_sync_block_9
     port map (
      data_in => \^data_in\,
      data_out => tx_fsm_reset_done_int_s2,
      data_sync_reg1_0 => data_sync_reg1
    );
time_out_2ms_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AE"
    )
        port map (
      I0 => time_out_2ms_reg_n_0,
      I1 => \time_out_2ms_i_2__0_n_0\,
      I2 => time_out_2ms_i_3_n_0,
      I3 => reset_time_out,
      O => time_out_2ms_i_1_n_0
    );
\time_out_2ms_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000800"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(18),
      I2 => time_out_counter_reg(10),
      I3 => time_out_counter_reg(12),
      I4 => time_out_counter_reg(5),
      I5 => time_tlock_max_i_3_n_0,
      O => \time_out_2ms_i_2__0_n_0\
    );
time_out_2ms_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => time_out_counter_reg(7),
      I1 => time_out_counter_reg(14),
      I2 => \time_out_2ms_i_4__0_n_0\,
      I3 => time_out_2ms_i_5_n_0,
      O => time_out_2ms_i_3_n_0
    );
\time_out_2ms_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => time_out_counter_reg(4),
      I1 => time_out_counter_reg(3),
      I2 => time_out_counter_reg(8),
      I3 => time_out_counter_reg(6),
      O => \time_out_2ms_i_4__0_n_0\
    );
time_out_2ms_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEF"
    )
        port map (
      I0 => time_out_counter_reg(0),
      I1 => time_out_counter_reg(13),
      I2 => time_out_counter_reg(9),
      I3 => time_out_counter_reg(2),
      I4 => time_out_counter_reg(1),
      O => time_out_2ms_i_5_n_0
    );
time_out_2ms_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_2ms_i_1_n_0,
      Q => time_out_2ms_reg_n_0,
      R => '0'
    );
time_out_500us_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AAAAEAAA"
    )
        port map (
      I0 => time_out_500us_reg_n_0,
      I1 => time_out_500us_i_2_n_0,
      I2 => time_out_counter_reg(5),
      I3 => time_out_counter_reg(10),
      I4 => time_out_2ms_i_3_n_0,
      I5 => reset_time_out,
      O => time_out_500us_i_1_n_0
    );
time_out_500us_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000008"
    )
        port map (
      I0 => time_out_counter_reg(15),
      I1 => time_out_counter_reg(16),
      I2 => time_out_counter_reg(11),
      I3 => time_out_counter_reg(12),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(17),
      O => time_out_500us_i_2_n_0
    );
time_out_500us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_500us_i_1_n_0,
      Q => time_out_500us_reg_n_0,
      R => '0'
    );
\time_out_counter[0]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFBFFFF"
    )
        port map (
      I0 => time_tlock_max_i_3_n_0,
      I1 => \time_out_counter[0]_i_3__0_n_0\,
      I2 => time_out_2ms_i_3_n_0,
      I3 => time_out_counter_reg(10),
      I4 => time_out_counter_reg(12),
      I5 => time_out_counter_reg(5),
      O => time_out_counter
    );
\time_out_counter[0]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(18),
      O => \time_out_counter[0]_i_3__0_n_0\
    );
\time_out_counter[0]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(0),
      O => \time_out_counter[0]_i_4_n_0\
    );
\time_out_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_7\,
      Q => time_out_counter_reg(0),
      R => reset_time_out
    );
\time_out_counter_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \time_out_counter_reg[0]_i_2_n_0\,
      CO(2) => \time_out_counter_reg[0]_i_2_n_1\,
      CO(1) => \time_out_counter_reg[0]_i_2_n_2\,
      CO(0) => \time_out_counter_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \time_out_counter_reg[0]_i_2_n_4\,
      O(2) => \time_out_counter_reg[0]_i_2_n_5\,
      O(1) => \time_out_counter_reg[0]_i_2_n_6\,
      O(0) => \time_out_counter_reg[0]_i_2_n_7\,
      S(3 downto 1) => time_out_counter_reg(3 downto 1),
      S(0) => \time_out_counter[0]_i_4_n_0\
    );
\time_out_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_5\,
      Q => time_out_counter_reg(10),
      R => reset_time_out
    );
\time_out_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_4\,
      Q => time_out_counter_reg(11),
      R => reset_time_out
    );
\time_out_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_7\,
      Q => time_out_counter_reg(12),
      R => reset_time_out
    );
\time_out_counter_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[8]_i_1_n_0\,
      CO(3) => \time_out_counter_reg[12]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[12]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[12]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[12]_i_1_n_4\,
      O(2) => \time_out_counter_reg[12]_i_1_n_5\,
      O(1) => \time_out_counter_reg[12]_i_1_n_6\,
      O(0) => \time_out_counter_reg[12]_i_1_n_7\,
      S(3 downto 0) => time_out_counter_reg(15 downto 12)
    );
\time_out_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_6\,
      Q => time_out_counter_reg(13),
      R => reset_time_out
    );
\time_out_counter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_5\,
      Q => time_out_counter_reg(14),
      R => reset_time_out
    );
\time_out_counter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_4\,
      Q => time_out_counter_reg(15),
      R => reset_time_out
    );
\time_out_counter_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1_n_7\,
      Q => time_out_counter_reg(16),
      R => reset_time_out
    );
\time_out_counter_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[12]_i_1_n_0\,
      CO(3 downto 2) => \NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \time_out_counter_reg[16]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED\(3),
      O(2) => \time_out_counter_reg[16]_i_1_n_5\,
      O(1) => \time_out_counter_reg[16]_i_1_n_6\,
      O(0) => \time_out_counter_reg[16]_i_1_n_7\,
      S(3) => '0',
      S(2 downto 0) => time_out_counter_reg(18 downto 16)
    );
\time_out_counter_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1_n_6\,
      Q => time_out_counter_reg(17),
      R => reset_time_out
    );
\time_out_counter_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1_n_5\,
      Q => time_out_counter_reg(18),
      R => reset_time_out
    );
\time_out_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_6\,
      Q => time_out_counter_reg(1),
      R => reset_time_out
    );
\time_out_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_5\,
      Q => time_out_counter_reg(2),
      R => reset_time_out
    );
\time_out_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_4\,
      Q => time_out_counter_reg(3),
      R => reset_time_out
    );
\time_out_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_7\,
      Q => time_out_counter_reg(4),
      R => reset_time_out
    );
\time_out_counter_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[0]_i_2_n_0\,
      CO(3) => \time_out_counter_reg[4]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[4]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[4]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[4]_i_1_n_4\,
      O(2) => \time_out_counter_reg[4]_i_1_n_5\,
      O(1) => \time_out_counter_reg[4]_i_1_n_6\,
      O(0) => \time_out_counter_reg[4]_i_1_n_7\,
      S(3 downto 0) => time_out_counter_reg(7 downto 4)
    );
\time_out_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_6\,
      Q => time_out_counter_reg(5),
      R => reset_time_out
    );
\time_out_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_5\,
      Q => time_out_counter_reg(6),
      R => reset_time_out
    );
\time_out_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_4\,
      Q => time_out_counter_reg(7),
      R => reset_time_out
    );
\time_out_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_7\,
      Q => time_out_counter_reg(8),
      R => reset_time_out
    );
\time_out_counter_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[4]_i_1_n_0\,
      CO(3) => \time_out_counter_reg[8]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[8]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[8]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[8]_i_1_n_4\,
      O(2) => \time_out_counter_reg[8]_i_1_n_5\,
      O(1) => \time_out_counter_reg[8]_i_1_n_6\,
      O(0) => \time_out_counter_reg[8]_i_1_n_7\,
      S(3 downto 0) => time_out_counter_reg(11 downto 8)
    );
\time_out_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_6\,
      Q => time_out_counter_reg(9),
      R => reset_time_out
    );
time_out_wait_bypass_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AB00"
    )
        port map (
      I0 => time_out_wait_bypass_reg_n_0,
      I1 => tx_fsm_reset_done_int_s3,
      I2 => time_out_wait_bypass_i_2_n_0,
      I3 => run_phase_alignment_int_s3,
      O => time_out_wait_bypass_i_1_n_0
    );
time_out_wait_bypass_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEFFFFFFFFF"
    )
        port map (
      I0 => time_out_wait_bypass_i_3_n_0,
      I1 => time_out_wait_bypass_i_4_n_0,
      I2 => wait_bypass_count_reg(5),
      I3 => wait_bypass_count_reg(13),
      I4 => wait_bypass_count_reg(11),
      I5 => time_out_wait_bypass_i_5_n_0,
      O => time_out_wait_bypass_i_2_n_0
    );
time_out_wait_bypass_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => wait_bypass_count_reg(16),
      I1 => wait_bypass_count_reg(9),
      I2 => wait_bypass_count_reg(12),
      I3 => wait_bypass_count_reg(10),
      O => time_out_wait_bypass_i_3_n_0
    );
time_out_wait_bypass_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => wait_bypass_count_reg(4),
      I1 => wait_bypass_count_reg(15),
      I2 => wait_bypass_count_reg(6),
      I3 => wait_bypass_count_reg(0),
      O => time_out_wait_bypass_i_4_n_0
    );
time_out_wait_bypass_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4000000000000000"
    )
        port map (
      I0 => wait_bypass_count_reg(8),
      I1 => wait_bypass_count_reg(1),
      I2 => wait_bypass_count_reg(7),
      I3 => wait_bypass_count_reg(14),
      I4 => wait_bypass_count_reg(2),
      I5 => wait_bypass_count_reg(3),
      O => time_out_wait_bypass_i_5_n_0
    );
time_out_wait_bypass_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => '1',
      D => time_out_wait_bypass_i_1_n_0,
      Q => time_out_wait_bypass_reg_n_0,
      R => '0'
    );
time_out_wait_bypass_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_wait_bypass_s2,
      Q => time_out_wait_bypass_s3,
      R => '0'
    );
time_tlock_max_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AAAAAAEA"
    )
        port map (
      I0 => time_tlock_max_reg_n_0,
      I1 => time_tlock_max_i_2_n_0,
      I2 => time_out_counter_reg(5),
      I3 => time_tlock_max_i_3_n_0,
      I4 => time_tlock_max_i_4_n_0,
      I5 => reset_time_out,
      O => time_tlock_max_i_1_n_0
    );
time_tlock_max_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => time_out_counter_reg(14),
      I1 => time_out_counter_reg(12),
      I2 => time_out_counter_reg(10),
      I3 => time_out_counter_reg(7),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(17),
      O => time_tlock_max_i_2_n_0
    );
time_tlock_max_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(15),
      I2 => time_out_counter_reg(11),
      O => time_tlock_max_i_3_n_0
    );
time_tlock_max_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => time_out_2ms_i_5_n_0,
      I1 => time_out_counter_reg(6),
      I2 => time_out_counter_reg(8),
      I3 => time_out_counter_reg(3),
      I4 => time_out_counter_reg(4),
      O => time_tlock_max_i_4_n_0
    );
time_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_tlock_max_i_1_n_0,
      Q => time_tlock_max_reg_n_0,
      R => '0'
    );
tx_fsm_reset_done_int_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF1000"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(0),
      I3 => tx_state(3),
      I4 => \^data_in\,
      O => tx_fsm_reset_done_int_i_1_n_0
    );
tx_fsm_reset_done_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => tx_fsm_reset_done_int_i_1_n_0,
      Q => \^data_in\,
      R => \out\(0)
    );
tx_fsm_reset_done_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => '1',
      D => tx_fsm_reset_done_int_s2,
      Q => tx_fsm_reset_done_int_s3,
      R => '0'
    );
txresetdone_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => txresetdone_s2,
      Q => txresetdone_s3,
      R => '0'
    );
\wait_bypass_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => run_phase_alignment_int_s3,
      O => clear
    );
\wait_bypass_count[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_wait_bypass_i_2_n_0,
      I1 => tx_fsm_reset_done_int_s3,
      O => \wait_bypass_count[0]_i_2_n_0\
    );
\wait_bypass_count[0]_i_4__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_bypass_count_reg(0),
      O => \wait_bypass_count[0]_i_4__0_n_0\
    );
\wait_bypass_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_7\,
      Q => wait_bypass_count_reg(0),
      R => clear
    );
\wait_bypass_count_reg[0]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \wait_bypass_count_reg[0]_i_3_n_0\,
      CO(2) => \wait_bypass_count_reg[0]_i_3_n_1\,
      CO(1) => \wait_bypass_count_reg[0]_i_3_n_2\,
      CO(0) => \wait_bypass_count_reg[0]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \wait_bypass_count_reg[0]_i_3_n_4\,
      O(2) => \wait_bypass_count_reg[0]_i_3_n_5\,
      O(1) => \wait_bypass_count_reg[0]_i_3_n_6\,
      O(0) => \wait_bypass_count_reg[0]_i_3_n_7\,
      S(3 downto 1) => wait_bypass_count_reg(3 downto 1),
      S(0) => \wait_bypass_count[0]_i_4__0_n_0\
    );
\wait_bypass_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_5\,
      Q => wait_bypass_count_reg(10),
      R => clear
    );
\wait_bypass_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_4\,
      Q => wait_bypass_count_reg(11),
      R => clear
    );
\wait_bypass_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_7\,
      Q => wait_bypass_count_reg(12),
      R => clear
    );
\wait_bypass_count_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[8]_i_1_n_0\,
      CO(3) => \wait_bypass_count_reg[12]_i_1_n_0\,
      CO(2) => \wait_bypass_count_reg[12]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[12]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[12]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[12]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[12]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[12]_i_1_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(15 downto 12)
    );
\wait_bypass_count_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_6\,
      Q => wait_bypass_count_reg(13),
      R => clear
    );
\wait_bypass_count_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_5\,
      Q => wait_bypass_count_reg(14),
      R => clear
    );
\wait_bypass_count_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_4\,
      Q => wait_bypass_count_reg(15),
      R => clear
    );
\wait_bypass_count_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[16]_i_1_n_7\,
      Q => wait_bypass_count_reg(16),
      R => clear
    );
\wait_bypass_count_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[12]_i_1_n_0\,
      CO(3 downto 0) => \NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED\(3 downto 1),
      O(0) => \wait_bypass_count_reg[16]_i_1_n_7\,
      S(3 downto 1) => B"000",
      S(0) => wait_bypass_count_reg(16)
    );
\wait_bypass_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_6\,
      Q => wait_bypass_count_reg(1),
      R => clear
    );
\wait_bypass_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_5\,
      Q => wait_bypass_count_reg(2),
      R => clear
    );
\wait_bypass_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_4\,
      Q => wait_bypass_count_reg(3),
      R => clear
    );
\wait_bypass_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_7\,
      Q => wait_bypass_count_reg(4),
      R => clear
    );
\wait_bypass_count_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[0]_i_3_n_0\,
      CO(3) => \wait_bypass_count_reg[4]_i_1_n_0\,
      CO(2) => \wait_bypass_count_reg[4]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[4]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[4]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[4]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[4]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[4]_i_1_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(7 downto 4)
    );
\wait_bypass_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_6\,
      Q => wait_bypass_count_reg(5),
      R => clear
    );
\wait_bypass_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_5\,
      Q => wait_bypass_count_reg(6),
      R => clear
    );
\wait_bypass_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_4\,
      Q => wait_bypass_count_reg(7),
      R => clear
    );
\wait_bypass_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_7\,
      Q => wait_bypass_count_reg(8),
      R => clear
    );
\wait_bypass_count_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[4]_i_1_n_0\,
      CO(3) => \wait_bypass_count_reg[8]_i_1_n_0\,
      CO(2) => \wait_bypass_count_reg[8]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[8]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[8]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[8]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[8]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[8]_i_1_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(11 downto 8)
    );
\wait_bypass_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_6\,
      Q => wait_bypass_count_reg(9),
      R => clear
    );
\wait_time_cnt[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      O => wait_time_cnt0(0)
    );
\wait_time_cnt[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      I1 => wait_time_cnt_reg(1),
      O => \wait_time_cnt[1]_i_1_n_0\
    );
\wait_time_cnt[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => wait_time_cnt_reg(1),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(2),
      O => \wait_time_cnt[2]_i_1_n_0\
    );
\wait_time_cnt[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE01"
    )
        port map (
      I0 => wait_time_cnt_reg(2),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(1),
      I3 => wait_time_cnt_reg(3),
      O => \wait_time_cnt[3]_i_1_n_0\
    );
\wait_time_cnt[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0001"
    )
        port map (
      I0 => wait_time_cnt_reg(3),
      I1 => wait_time_cnt_reg(1),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(2),
      I4 => wait_time_cnt_reg(4),
      O => \wait_time_cnt[4]_i_1_n_0\
    );
\wait_time_cnt[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000001"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[5]_i_1_n_0\
    );
\wait_time_cnt[6]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0700"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(3),
      I3 => tx_state(0),
      O => wait_time_cnt0_0
    );
\wait_time_cnt[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => sel
    );
\wait_time_cnt[6]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => \wait_time_cnt[6]_i_3_n_0\
    );
\wait_time_cnt[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[6]_i_4_n_0\
    );
\wait_time_cnt_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => wait_time_cnt0(0),
      Q => wait_time_cnt_reg(0),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[1]_i_1_n_0\,
      Q => wait_time_cnt_reg(1),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[2]_i_1_n_0\,
      Q => wait_time_cnt_reg(2),
      S => wait_time_cnt0_0
    );
\wait_time_cnt_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[3]_i_1_n_0\,
      Q => wait_time_cnt_reg(3),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[4]_i_1_n_0\,
      Q => wait_time_cnt_reg(4),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[5]_i_1_n_0\,
      Q => wait_time_cnt_reg(5),
      S => wait_time_cnt0_0
    );
\wait_time_cnt_reg[6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[6]_i_3_n_0\,
      Q => wait_time_cnt_reg(6),
      S => wait_time_cnt0_0
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2024.1.1"
`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
GNlNeNP2E6l/J7IS2LF/ItWIQpPIQxmqiENbpRcUcYDs2zMQssAZwDKWmGxc99etc2e+6UESUZFU
1OoN93QWwGY5lwtShrTc9cE66KsLO3U82lRN16EHmi+RZOdYR9rf5z7OgjqfdL/T03/rfwU37+Qw
8V3wDE2i5hqr8BCHEwM=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
afRfkAd8cICiTJYylAj89MfRuyfOa9d/tbr4mvbd5mWPw07cLRhiyi8gIe4+Ayw0CiOk7/EjPgWg
Tg/1hR99BWRw4VbbDTWu69lZfP8CCWoBxtdt8WRuUGz7PLk6qQMaoDf1ogw0xyXTK96u8N6bef3z
7mR3YInM0ECnk0+8vQDLRo6ETSTf4oQYcckMpPfbPQ+NnHRvcHroYdddFGNYasbeUqwn7tkHnYO+
ADUKING4g3DhZO3UAIPZJggQnH3ayID9nf49Oa7bj//jtWD0qELIGFMQn48spke+RcfDkdFLHWhY
0esV8oRsRwIWfwXwqK6i7aIis3paozzKSWbiNg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
ZI9ECw+Xpdoykt5zjVXLh0QGbCnqQDaDo8xSAWsYltuhqetS6+j38MNsVABP/vySCUkxp7yLlCMg
506S40Li7Nmy7qD4YvDLHMe+fIgwi3Hb193aoDluxnxdVk/aJ7VOJWXd88+scqfUSpbbFv7rk9La
8woMpCSiDNVKi6NB7Qs=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
R1h0p6UTerhva5Yb3duDj5nR3CyyGXjeE4dSD+H9PUwvK6SElm7FlL9AjlrnVaE2OmUXbsHu+WYi
T1XMnV1ekw5xTRICAudvAhAJrZYMHEjujHOfgXPmKtXHivF59pFlTxnEtD+dnchOL4x/jMm/lCgH
k3/wzvZbfOaxaKFceauCooIRZkqZa5FA3KG53cEucbspEldbnPaAcHsiO04PEwklvjo+2XAiGTHB
YB9XEkRUOprwSG+TeOWFNvz4Yu2FYHffCsobvgsF/8dSJQ1SVv3rM1uHf3coB/s7NIgZ0ye03NO/
wLFRL+3OuPxWNTqHO9YYXL0XTd1rPaaWB/mCcw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
4l+Avk44zzaOVxRJDzFVTXMyJmyE6qVo8wAiHogr6M/NkYt3AgnEITBHDJ9dF4SVZQLiV7ZY6shr
asWzrbISsiBCAaZgyPZLr1zlBErx08H5Enf1NkLuocQa0x2Dc1DwG+mO+flli1++tKpmHM6ADCS0
3/8fREk+ga01VkXbOXY6B7Wt4G2O3LBdFCkEvd0/MEx5uYrJNrtpaeSd3h2CpYLsfPsxxbAzNJN1
3pvNKoS8QwUnTPy7oe/k8bFD0TOzZmLvT/dy3x7SFlB6oSYr7FtWMOOuH5B08Upu75qdujq2/6ox
I9BucH2FnuLsghHnPuWfjh4WTDPekeCy51G/7w==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2023_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
r1TCY1FBxgJgPdlaGrkCY3ZPZ26hJnsdsOKnUu75PgvjzuIkty6RkwizEzV4coW2cxRBcWf87Rmb
xbDdy2cDu4f2qP6QUVBsGtY+ITHBIqBPqY7IqmHvnmT8DQ7mta5QD9HpSpd8E0HAu+1hmp2y9R2T
j6cEj7KNzaIGnuiJLket69e1dx31L14fHppyx7EJTOKaIYVHOTGcZeHi6ozbitA95N9bUNiSCS93
sJcjWWTWEB9Cx8arBeHBMIflTsCrUzgwd6f7Lb59R/pt8Zgvgr/BoV20ZGDDy6BCwEWgGSw6GDBL
o5ppIN+dICJOxCzX0Ij4ziKkTNPa1OnlJpcXcA==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
U0o+hd53NUTkWW/pm++iDbvu9EzyJbrE8cgDdW35QL1go6ppv1cnjM7W9LNAUoYpay5lYpZFkUZP
qgnoVBG2BG64sAHeAjjMVH8aUuTYzhhM3n6pvSqx5Ez8rezjhdOlMLX17yWFSCq3x15rfV2si91N
p+FFtjXOD3BmuNjRLXeeRjrvt6sxfN9IwN0yEZYhwp2ukPLeIyrYpRXZ+xMErI/b7BKLZNIljMSV
Cx5wPeURNjIC7AKZOJmo8YQe2JGJEHps80MGm98CsBtbIXDa7uAxib3WWstVs/UbQGgqjiyNIOSu
11VEYQSI8nO9bNfJPsLpVaeo9V+FUKdFcBgKbA==

`protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`protect key_block
JlAVtvWu4RylemkeXAwQaOcauzsF85mjSoJqBa5/i7p1cOK0PM2Rfou44syNOWyUG43Mq5DdY2P2
hSdJ0BrVnIF/JY67xayAWorKfMTlvfZN3zVnvvFrLORWpe5MwPzwHomEMtQgK/WjTmMnFJzWxYti
gedoe+hxxEoZILnHN3ge9A2vnRikwcJETpOKLK13mQa1Td8zo9tsQuagX91Bw+AWLzMrHGR2bmQr
grI95HpSioNMY5CjomezDLSd80Rlkr+6SiaUQt3pqExFVEAsKNJdtppxNwugqojHWEqm1lF0wKuj
aLs/KfYF2Uv5wNjccSP2gHLWWaF5DKs5G5u4c4FjEiS/slCZgrt5Ymwc6XKQHNOHbPRsQcbbkPMD
V/YuoNzw1R2386n0pOd+vMD9nTtpB104VrwBNBxlGPPa0qiFM4+xr58vMzHKNT91irPu/EirEIjt
wEtLynnUeSKMKz3aAvXZPRYeEurD0fgZ6iczWVNZOsltSIQZjOp5ErOC

`protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GeMop+4rJGQJTsAts4IyQAkQMeKt+Fa026naAEt7J5/8nhAvWlIS4YQDNOLHMMUe8p+sUA4yE6+s
hf5STrRcmXzM45O/bYrkBzWHNqGcsEuAHfdUd9ANbBZmpncQ5ywZbXLX0oI4xVx55B8sn2gAj+p2
NQEPjYWPZXJNML4IilPsrW6TlgTi1zhp0JFBFT3O6yHY35dAseHXtMWYgjr0+6WBtVCCvC/z4bA0
UY58Skgytan1bUM557AlikG2LqNB6YHVUQihnx4ZProlAL61WY/j4joeXcDH8Tpm0cmYcUM5yUax
haoFS6FezWAqD4KpiUS3xpkUVET2QpN0NnxTvw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
1x9gXtJFU2nGQ9CWRMPqjLG5B7nyL7ZKhgWVKPrQTraMot+PY1MLqNEQ8371fSgOtWrUw7wBswaL
CBsGdy0dDZt13psueOntqYT0wcW3vUbYzfkMCsSjkk2yeYjnCYclwJKMNk3fGxfxpTLQsScOFING
ZyUHBaE+KeZ7yfdqvxwcpP+rUXdNX/2BGvcr+a0ow/nFsiCiOMdYIlAgllnBFoUV7lDr001O00Hk
1MDqVCRdpiO5q5FnggoIp1sVIq1C+JbqiuyPkv5jmlk3iIfZiHhSSYoxP3DJf7VPvlJ1Z/Hwz6Dw
/39nFCTotl6l9hqiLHTmISLBvSYZnjDEcOPtPA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
mxE8AqszO8jEcufcEIxsyswviid2I+vp0wiBEslkbc/wVr2vlpOaIaviDrRuTwIVx162O0AViWa2
3/lH2PLY1lcjenGETw7njsuJd6MQv4KihzbHhd1HB0TKpoL1n4E9u5PgNIGghcgNu7s2hE5rpffM
FAxMXxyhZQ3j4WBKTgRtbqcexvVSl/whSo87Oa/V48wwF7Z68uoZdaD4cdNihYwHMLOm0LYqYesQ
cScpoz43IdSFiQkBEM4YXuqn7BFIsJ7UAr+EkHAFxWYqt0IJ3+/OCuOYW+y/3p2tXT2YSGnEsOzZ
SLIsZpsw7wAiEfu9tVZJlTQ0OUTZqfQXK68i9A==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 59328)
`protect data_block
1vFLS+NMWNyemPrhbTFP8+PW98y0nDIEylx/Qz3RULeeHomACXQRi1s9sjigdJok9/6AwtnE3VR4
6nG6vEp/pnkQBmF3859g2n4j1+eDu3MUBJRxcFJSqAvRzdnRXe9WsokzB5XXEGfp/EoDbWDB0KH6
sO1bAaSQkVm1zMyL3L3UnczsfLNTqtfNsMpWVIa85cMcKdxk+R2K/gYhI3iaITdyrGSni9VybVJt
P3iIrXXrTfgvhxCjppwQuhcylVXRZ3+UqxHQP2uQueDA6M3UhsikajofCVk/Q3XE2NTq4MCoTC1z
3q902Fhos50fbnU8QK0YtINjo+WnORv0aY+XsExWqtrDDcaVuk6fOrIlquBcdGdUdBOyT392sMKZ
cauucBIKp8FFzrS+Hwypx7L2h5hKhUTSuZkjMDml1zt2zI/k8N7Z5xjWOOVDQE4jmdEuxgx8ydDs
bE5Hzu5/2PhHwEbkdr8Zr6JhPhLIIdsTu8ZUNEjOQGlllRx1pnM9YagNh4noTQ252JoJuKKrQLfl
MBWVgh2kzIGQwQLCWbTh3voIqxNTWt4UqkfG+Ft5I/8AITJvQOHom45zF78yfm1I0RhkkSWczKEP
EhSNcyfU2sfZs5lP53CjMO17o1bnCoh6EZnqh0zlcdkdB4qnGcBWnn+xf6pPvzFWJWg2wuvbfQJx
JK5DtdTUpkywshe66hXpM+EpXWb5/yZggKKoG0BZhMpffCQvq5y4SJQg4L8u70M3Sxh8t32IxFBE
wfc+adMSDBWHcezHVOFS/skvtcZ+FPL8GdVrE1vINmnqpXv904UkvZyD7W8x9/JQbuqUFzLKCg1J
bvpdgx9ptjzY8GcOnc9OW9UHjFqBpxVeElJHoEHLPtTv10C26+AZx7Vg0AoUmwIlOnml2iReCea7
eMunin29+3SbkcAXJZ1N6FU/T1fe7Wc2Z4mRQBgq9Pza8IT3ElXwSwt71RmEgxQ1EWrVmpvZf+GS
YHTZinAJe7+9RsqX5DMNgU0RJxc++6KApoUi2Zt4oWizFKuOxtpP+LTcMy9EeG0/MF4LZ8WcwpsT
lvqpsuWqgxdDo4Vw+zjXnZ7WhsXtykmSNhidgoxNZGFoRM+59M+XM65tSZ0oPz2EWmrunBRaK8sc
HsEObfZUyowuxaI11KXrmQmUHmKHXqIWlGcEfasKio3Z5kCBsqmofzs4pbuvmWpmP22hXxOJ+VvY
0/sk3WJrfPDW8WPzB/5YDDkrxtIc67s3DeeqGBht89Qiyl3ofiHKsYafruoYOneKMMvTTnhkdFBi
Lo/sxT1c2RCH2Bf2Y/BgVuQD2kZAUa2v2cUR9lWx0b0ddaNnlAN1dn7KPpdN4I+F58IdKPQRQC1v
nRIKZVFvVhm8q0omG3cfub2YXPQccuuocumxixJQ6LKm5eKYrcuN3NLvHnj8jXrP2hmBQAD8Iyhh
mR7NzSQUAaQpUCxzQEOwapcuDDBYEmjqYUUt7pGJ/u4TS214jGxuOnTvPzTyHqEmORRePeIcjpTs
XmnOEYf67R27YFaOz8KEsDDXXGsQyA9bSJDy5sz//XuX9hkem0vw951krU/VzbH5dKLP5FLEiB2K
pSKaG7fK+bHdR9MvvAa7oQAPMPGsAS87ayhRWtUpYV69vj0E7EaNaw2ZOH66hWV+OJ9eYfWZ5YRu
prYIIcuXTA093Tk8zOczvpDj+nojs2lLHSqc4ruXFFOOmBw7tN2RFaRJGIxp/vKnijHRhj2fwA8l
yJLgjG8m4PZptNG3kUflEpQJ2nk/OE96wx8oh3MpuSl7jiBD38tQ7FZw5TBpRNMH461lzZ8SG82W
l6Zy80PWfEVZxTasWUuwskZgBvKBBUUwt4eRcI3o4IM9gZ9VJNlK1MCezA2drXU4TiZRinMVVu4V
xw36D/6fFdwN5LB3OFSUBvG0Qi25jJC1fd1FcQm/v8v4BnYxVAfU5fLQuaRf7uCN8zH3TWwkLIq1
d4W5b02HmX6nZQDip9OFtiWBQwIdykretct+PIWPGjnB/7hKmbAUgsd6ZGS0r3hHh8PF8JXZBAAr
dMXMV8ymtsxs/HEDAlzdNcc7p2it3bmYEG3J2asHABQJv5JFCovxfnQs9mgiU0AN03ZDX5ZG2RLX
vAQQmzRtaTMLf2zADhxw7ISTzib8BUm5GX4jm1JP9c+C5aIQqy45SojTmnFuwyzASELiZlS/0nLY
d3q75nPcO7ytj7DXffDYrU9CntszunNc481MHTVKYJYToL+4JXetgEc0uVcrxIvyZEDeRKVwxrxN
JBljcRszf9vhq+28QwAt9aRdaH7q8ozsKcqyVzp+bB2klwEh0ACr231Wjd69Hhdcgc3PR3NgGNn0
X+RvbBq2wSxtmJzgqQvUKQgkEsStu44E0I56j2IRglQVXm55mybP50Lp/8uomFIY/2FxAc/IqDsL
ShRJVaP9zul6FU5WI5xg/TKVSy3LTxzFyeJ7Dw0ScV52x3X51HTbOSEsJVuJ9CY2Bf8uoTexBHEU
uACwnOTDLmxdJZ8ma04wmN7U4uyPhHmbfCikJ3VYYVtPccMf5TeoRqWIFWfYSazNWKTk5LLuQR7D
7EbxswkOy6f1FMRuz5k+RzBCsdaI4D7l7RIhIfLuJ7zTmPa89ubgis3BsqqZFdlqeSDEeebH6XSa
OOKEFthRhLnQuzXRx86zlTeYu/dEpNAwjA0KaVAQWoikiSvURpYP+BEuL+r9al6U4By7oQAPdBEm
tRLRDdW8h7TtQr7fx1SzRYWGt/OE0W/uAp9wXz+tEzmc9LUlUzfvQXANbe9Jz5aDuc/dtyKDkYeC
G6YVU3wXuySVkoqBc8ss05CHpxoa5EwYZDOEAE4///taIflpGvkUPdZOmdavrRp2y9O01D5RbATU
d+b2leNxuBxSk9dh7gbbIz8/GayI1Hgr8uicHgKA2mTYrmooclqb18KOxXaIcTig+hIi1/d0Yicv
ardPi3n+AjQJd3FJWZ2j7tjvLnRXEquZPn3KAN1vZAvhpBfGN8wZOqetMJe/NKTx0qi1bw+ZdG46
Dj5S6/Sg+2ZC1mSuosmMDLibo7iSgyJsKes4qrQw2ArNriTr/GDkiKl84TjEaP+hBWODWpE6SEqg
AtzkMXhh880tleImbCvzPkqnR7bVVMSykilS6IdHkNoGgJVsn0PXY/EV/+K2DuXPC7a/d+O+zFv7
CXS+NqOPAb7Sxaa/Bz9Ek9eWJ6E5d6jXNiunkNeC/mIJv2OhwQ9S0+QAfNtIqGM5tcWKlBZU3tmG
WORnYFDJ9DCyKX+a4xszSbHVjGxG/C58wxXYujccy+P2GH553MV1EIiggrsRXnC7/llO0Ty7FTDt
45RtSeDmcJ+VXC8GgYhAu0U/zWhYE9EbjavEpdTRP98gL84eZUh7uPHsgpKXKxBh59cWimWakgIY
vF/e1duJOrRyUEjvD0ZM4V7Nio5DgjufVN4rfAvIpxk6VVqslH2ZHPL1c7fFzhBcemgAL2sWrh0E
heTSvY1Ch3HPtpGXJBifCTHnXVWlP6c/KClzFDE5OUFYQFL/V9rZuIPj5anVHVldqPmSx7QBooER
vjWt0IarexEcH8muFsRSslccZTqs9x41i1s+OaFEmf4bJL5QiQEDs2mCtonHrEp6G6Q7hn4FuJI6
2OY2qbewUCxHpV1TVSFVRrSMeOTxMYZMwvzMsd8afHFJKBx8DDR1KnnV0zQsNyhVnWKHK17ravpg
Waoj6VYPoyyzxiwraEyWEmZFPixLa9iB04Io3XtCVnu//P7OhlSH7xD4cTwQWd8ypNRgX1EAxMEo
rzmM3TBVMMu84PCwIum2AK4T5mztfZ9nz7zfC9VUxQlboM/b0aX7mqeSffUsmzj7b3Jff4fljLqx
y+GLQTp0iHqRfwWru1Bb4EaWvrgqF0MvY04nMkG8fMoojWYLdP7S++W2JnMLKLDVwEiiOiSEECJX
7g2xyUPs5RHAQIVYCFuAEt0R2ZvptNyAzMraUm/k2wlGOlvJID7WDLvCKAWIgLK8Z6FYFSKkK7sy
bSY+ubojoPdC+fiyI8h/Cosj6uAhuBv6rMwjGJmsXAxcE953cO2YCETJz6FelLXCv0180KKjJUys
ROcU12rL2xe947Se9D+9scr+R5MfDctXj2opF7A4fbXvzlZgXK/sdIXY36mCfE32GWfurrS/O3HM
s3NKxOur8T6ab7KLLaQLLEk8Kygqzy+1OrXwIGY0IDy7/rwuXc/tvI/Xbl8OnZ5EHfZfVrI23MTf
0vmK0IRSpF7oeDfGze2Kv0Qb+HrEcE130sTs8plT1KWwtP6FqF299QH+PiaWOivmEONXASoAz75N
4oPnAcCncANnd+sXkWkSpz3xJTF8akn60NQyymeV7DSUnQTDz4PnA6rjYvDhIkCriBTHplqtJ6sV
LZwOSocvh5JyRvqZJj85Hc28YqIcC1NGttuS6BMMK9bv2zkK3cqITAXsYUEUuP5RXDmjClvCTwLy
k9K2kcM+mp6vfd0ZCh3CU9JvHC+a1jmdPwba9Jt1OL8Ge9FhrthQbPLx9zvaLuaQT7haTEwE0gMu
wLmqk4uwOPTLP1uWocSE3zv+aeScg7kSARVT7I7jLsXlAyp7dV+dQXpowiXAV1B8QZQV89uNRN03
/TV2CW1gkA5dn9zadANXv1bnv27Ed/TCU95CL8NE6yqTx9s29JsdzYPAdxjfqyyp5pPwRVStGhDK
miBRwDvG/ZQFKxrJT29KPWH6jzaHzVd5s52modt2iIuNx2P9rAjwiYXGr2FQnis2GhuSF2mJY9UW
zbKyTe7zPg7rOlyDp8/ZC9U4Qy0u/fBD2UfRf1n4FqV7rffzQn0pwkFiOeV0mDll8u1xjOH500Gm
6ESHSmrnZbN/Qg+g704aD2nyqTGXY2HiS8k4y+97hL9ecoPASZ3dzk8cz1foWkX3MtUul+y6JTwE
EsH05ClVpvdITUgOyDZjh56REA4Xnxf6IHNF7X0iW8rrun0/JcNUuluP1myrQBeoRDJAA2lfnzQ1
r0nZRvLTkeZNek9gpje9UiV+anUXuF/pVjS3DMtz6YsYkvFUzempM8jBwwCyNLUb4odYNr8F8N6a
IWkiPVC6ABz08cZhMeT+nQ5zPlCZJ399f7RYkP/0WEnjTf7TqM3dsUnLolrstp0iTkUmtRgqaLND
s/1iCdpYOQbDhMQVRe4lnvpKdzEpxdsnIydbHhhnvc5c6ieVu8Fbf2aMUskEc8hWbZV6KyxsjUOF
bUNUU5vFrPkJkreonFG/EX9VgA9twqh1Cf+GX8ADcO/pNovWMtGWAFoPQYwAa697iaWzguUV+Ghl
ua39/np3IS6b3BRArZuCZ4zhlKY5HyMunK+3PzvtDbJjNfa8RrMxA0nWH+jftiZhA7DX6Ty9Crfo
rz+KvGYIYlcZojVtxNbPRB3OGWeT2bdWd6nU6JtlfdPsNXhkHt3MI3LQhyAZCocHVz9pb0E7bQNX
e6CNlgqlqzvQwiaMg39h9ghZBMvj1GWOih1Z+vSBRmIh5FuntXP8NV8KMjIeRcMt6JT6F9SDQkdJ
ZRTzbAlMMdby6BDRe4ymxQLyleCFu+FIbL02+DkGGHDTYL81m9PWiQcneu2EO1AXKyrtrpugPjlS
YbE+cWc+r5NwLqFT/BQkkb5h8mDsTut36dQ56sqpPgVsONiekr0UFcDielzQhIJ2S318zt8bKH/f
0HgGFn11ObwAjHLRUhAGFBNl55ZUDuiiGTUI91G98WewM0vGILW3zLX+1CJcApRCN+aGdAMw/+VY
uxlLU5MeT8MNyBSyByRyDBBwP44CbNRGtt0gq2cSn3TgQuUuatqFSyRjvpctNp+S5hQHWDLmVWf/
MgRTIiPXW+oeuq+GC+0jBHLhnpzoz1dSC/BKyGGzvzaHijNyVZ6KmjEccLYem6xv6b6LjJjhFpL3
QGS5ZzXk0k5MiqtPutDs6A8z7icy1XsiC0SF1bWFUha8WMwSIkro7Vftt3iE5NJuKYFOi1tRSBiC
TkGKWqA/MTR/S/npGawnyzkQhSgMtpT2jnUu5jBN7tIWeici/4goR5HxCcdKLbWALIAISvfdFprX
enADFFFMkOD0F3qTzEhCJ5gSauT6ZwRX7DO7/H5SI39EhjhpyjxeCDY6nXWH9S7k9vAXmcSZqxrS
T36p3N1ufU0DdtPJgpLkDu3nF/RTP86g6Dtt0W2+WvF6zl329Ev7ZTtpJ0VAgsSY5Z86p7b4L49D
PSUL8MLcyG57KT7RwNPAxPZXGFTHNRlskAlOG3mLqPlUWkcs+qwqk5TbKY5UG22FIOBqy08W/O9V
pH0XGFzX/munDc58WvUFURADNbAcOCt51j+VuKRmEcGA7ZaVZ8TYaMIs2eNTgkJVRyX9RVXM/eqz
UhorUKvuUXIX1skJJIgrb3m3mBxqtb8SbMt3w72whbZG1uTL9+1FwsX7wOnrTayocihKrfgZNjFY
/go9NYbJSp16RSmCqE3tdCDz4wYqswku3eEYORwFtVHdKQ2R1NjWCp4wiK9VgsstvZ+D7UDK3waf
Q8D6ltXInAk6WexH6Ee8f0HyScR5n2O/wiDYBqjfdQ36idc91SUrcjaPQVoaEYfptjo74DjusbHg
0eyIINLnbsJk3xDQJ9wdmgQ7PckEzPuLbc+TOE2iVYfO+VP29J7hK/X8nCBSORbmJfZSPOHpC/wo
bQcsKsyFPpFRZz2fZNVdi6Bs7CRGVeN/8gdSvm/wVBHzV+6MAmeVQbn+j9zb0KQXHh7C3x5LJgP2
ukM/O+RDltYGXlJ88MsI+GYpXb+07tiFVefU8lm99Bf2ipoD7vcna2saNyJP0/8Ct5AntOl69dZc
JlwMd8bhYC5bHlUufeMef4UJ8rfDo2lFhN9TNvuzfvwN8PoJHnV3a5tGhZiTtsh8n2lvMUpMsYnI
Fo61H1yNhcV/eip5WIGhHTKTUKwBq4GXSesXkefFur/K/jcJSjX8Psbil0QfkGBe7SqYcPpVP638
xU2p6fVhFsbjLq9qqnQ5ODrPZS8o9R4VMgTEaRXrkhNhlJ4123BD6u47gZ/0bohBSKEeUeYElrDC
bWx/ufO9QThgdlpKDaIS0cTNjcH8pfyguJ0GMh9mAN8FtdHLPIHkZytrtx0xb3HVkHxOnMxx/6fa
3DXB7cVgQaB71S0RjgqVeoe76JgUiA80WPtL2XtIo+ACOOrcPn5wdG8fHiFB5WTciXTmxJdwkPVn
RnYL5u/SzD0k89+2sYs2vRxyo5evsQs5nZiRE2c0Wcjn+hkkveN/xL9DQW35fK4LyI57ddqSE0vP
ThVFdBxZob4G2+jNMVhWW3qtkUgOno6Rh7t+FKaaf3lHN3ZmBw8AJHo3L/3Mu6BRFbM+Z1f03Yv5
KHUpb2Y2fWc/ZshA6EJ5g8YbCOZWttbTjtVXjirm6ebX6DdrMKAv3McH5gnqkwi5nagUZPckTeXr
EB0uhpPNYINIS1Hnt41Mp9gBRxiFnjurA3EtiRXxbBU8HcDMxspqIzDNOQXpdhtPlkGIEo61a0RX
U5GflFpwTkfGyZ3bIVudsQs/QqVw+rkmZgg70skcoLYelia7HgjG9u5YEr6w01pjOx6lvJSZ07xS
06/MYUvyrHw9io5ZaAsVKIYCvUYHEuUIj6nJHHqAJeRVxrJUfbvNMqOEbq2X1VcEUcRr2dSutcuH
swB4SCbw4RHRZiZsKrrvfAwCh2jR9eTTLGnkDr+eZ+lreq31Es/VulfI0CEIz4kqjzSmYoDteHQi
HNGbaxHlxPm28GIOUsaWefjetyNUuJfpVNN0Zfy1HaNwXygDwEpQciYR/4K+JeHtxdBh8wcrsX/a
tpmQ1CuLApzrMVGMHUIZOnVchZAwx1gp7VH+BKIf2ynzugHy2/I4ew+MW5INPmoKNUREK2Kob9GH
CaRdRF+yIviA0yIxXz2N3P238/JZyyA91YWJv/UWJsr5Jwa+NQMuISLsqHHbJBlt8w4Zons24qmd
p54F1PGD4EYkkqzUVi/7VLAl1eAA/XyrYcEGKAoQhK4eBHR0tKohEdcwrFqH4CUaKGTjWuQl4L96
FSlyrRbmW6Jcb5AJXa3EahbAY+Ur4gFnha+747cr/6bA9zKCLe7/ZDdkE8lpIlQVI+rpQ2AwNbl7
N0dWjlBxdi/Z2EcU5N4R//jOH3dh1EBwZydDWv09KhT/4ivKcfEcYxCBEGqA+uM5121OI63JzghO
fMhaKco+FK3QKp10mKOgW/1OTaO4hdrqNuPVAew7s9l5ePHVAByDHtMHl+xVgb7bzmhq3+GseMSi
4FlJC1SbVXHZ/QjEvwFY683G2gly9XuzHNiJKzpuUSI8SkpqFSbBuXdnrZcfpM6ntR+O8vLOReg8
0wutrTcIEc+k7MhjUf8TfE3mzXqYeCrYLOyzyTw0bzcY7B62Nnhd4ZBFPXwVoFYbX+2HhSMzrjNN
aXRoNN7pMpc1H4SwVGfPrvF7LDm4f03drMpKP3YV0TWfx6AZwHRm1S2wc+h/NCXEtK8Io3ttwKT7
kofohxUqmv8TsEetlPG5qnm0I/vJ2B1vAowZOvhMUXO+4mfcMoxnSesv/r2FhuIpcnUxvbLuZERI
01RHP1yX+KT/VFLKAE9HENbSfSL/Hu3JeST1h4ArRURAfMSdd97Q9hu7RnTMnAN0luZGsn3y9J5i
w+wr6qAUEojs2zCtgxJiPYiCC8Iy+lvGGem/suwdFXvqjNI4Z+I8EZCn+VOa4ap1e/cvibONLpz9
BU8ckaG4+1KrT8rzDHk9DSv6VtPXHHruyuifXKg9PU2azuif2jTfw7u4JRO4HJ6zQ7JTZnegrSOJ
0xSGpJi/ZT5Wd1GDyczqQ/JdQR7R9V4fML3baZO/H3YQBxVUtaFA7kwl0R5kGfzyDST5yrzVRDOA
N7/QMg1l5RyyRC8zPoR7CNPLe6RpbpUajVJg6RidOBCxPSVasViPD243VXmB3vUiCDGHoWEWy+sy
QeavAMJq9WKQYXoL45dFPyJ7Y62TYQTUKE8pTpbXsE8ZawGz7VCKiS7BnUqRBDVHhbdo8o+zFQnW
ZpnFK9A3p2aKyTSPSDD3zT+4Ey9qlFZO+FMy5BMwWGSUm9+SgFk6gqyDqz1gV4gdFysSfKpHFUq3
pUh/Pyh+632syTCML3hFjtRUD6lJoATx1bP5mZoer7PRM85OT6E1it5cyHZcoire/R2LogxNMq8G
ezn/IpdiTM8ZV5qD5sYtewBlIBVY/rW6jcVuIE8MOU7hU0jxC1g7sXQFTlpHmK5VrDqinJ1m37DE
LX5BdvPfxxg3EFn3nFjgh3Rr9ZisMvuVmCMSuKJFEY+TB8yXqjQiQer2YhiiMXyzCgUR9Jo1xGnI
Fi9mvLWavz7zsHjCiI+JAL1cSKG9+yn4waBCPf46u9hS83+1Fe4X7sPdmJBOAx84ATEJvUectY29
XkUnemSZ3EumksJy1SgLoiGZSppAlsNge6AOeo0mn1BcK3BctAvGg+UjAJx0IithJKq7FK5iIGh1
ENL7tfqtkcVaSj30m1RtGO6oURVeUA8sESWZ4mPaJe/2IjOlnKtgLSlEZ/rvvqfNczeUN3YyoZcQ
x2eGXjMUJQ4TTXvymo0ORjarTL2NOR64ntTl+sE8UNPElhbHUYXs/aPNqjKW7k20qBj2hqm2X7cy
Gb213IAqd9T1jxY8rX0iA3xG+uOMAapyeGUBHArW00D+rnZche3aHmgRwVoFH2YXkQPqg8cJUMCy
twizsf/sqpMdSEaxWCdEprmjKsatjMLT0eQ73PMHwBeFxT7VN+JP4ZtrKN36UVICn09cmrzGs7NB
WTl5sYDd7ZAxFS7Ivkd5NxmPhZ4/S2bbqgFWRL8u8P269BzIC5d69A0fd4Mj+SWvGVYayAYiW32+
JXzggXIB8rpM6fs62aMPe7rA2+WD0NXi64SajQ3ClP8CNQYs/0g6csgfceb0u7RyW0b1OG/RazXy
sliyadu0EY0twkI3XIdLPauwX3R+Nb0zpKj+aG0o6itCrPluLXrPm2BmupZQ1nk1oUO1e8P1hOrs
3anLrSycng8Bo/HJvfMCJQwEs5Z0dpbabYSHtM+ZrbEEQ5Eu942GI+O9+lVF57I37kiV13vGcGgR
qiP5Oactopk5/nH3gg3+c5UVfytfWuCaoJvnjhChSF+l5RWtuyojk2YMumGVRuq+gltvcxjbcrXo
0BQbLQLQOlaviEInso0ugCjol6dLiOF0NQIevJfBdqjeXMuBmiczQj+079gmvJ1/zdgG3fDfBpC3
ROkD4MO8Aq+DR8ulYYnvC7KeKwm5/VXOuDv0TPhmFRb9vCw8GNQZpyHN9msP6SagPoSfkKeiRC55
VFSzEf38zCz37jqRih0cZ3a5j78Uo3M6TuNDWv/pmhiEvcR8vbBlWxCCBx4QnechSLhLUeLZiONE
O7Yzw76bQRbBXDKPAojDB+yBGAT9qY0NLf+sn0icfuJDWT7fY9ToTa9d0YG4yCQbJEps58ewoqil
i/Bt02fYrqHwiemMOSkk1q0mVq1+dVnABGVA4FOFlol+BQY3Xf+NIoFgwptROLsftcLI/jKlJFEX
sQTRazh860trXGlZkwQcxNznspXndZyzxSohj0iT7gJf3rTHjD6PZIHcewrRiCCcuChRr+I6YRJk
tHD+I9bihlRYVup9nTrFrcnYnI8gcsKdZKhxG7yTS8C0tOW7dspo7pWZvl4G9RupaEQUSlBvK2QP
z0aJU39UcMPRPNhQ5cA2RiOCujBixCaiWVbJ72CsrNzl/i7+S2hDERdaykFSl+lWXSK+z4xr4tlu
eBJn2uPQQh06vFmXVVNJhl1a45OTMI2WkUNkygoLv55czQ4ERnCO4edGswOhuMLH/1feQdGLwj8k
1gBn4Md1xm120UfhKSjG+LuuzsZlLeIlMk+NPRbjefOt3eTVeLeWzrdZWjEfkhCWCP4p8eVhWAFy
GHxdNE0vYi3pQdG9RcUjE4RmoL9xwJSspX3QD230kBlX3owP4OslD3sfNoWFaVsKeefx7OpB32GS
5hBWahxym4NAPB6Lyc0H3BCdbkv6PyATGPeLfAy6bcSRx50TULNhKjeuCbkruOPAVCNHSAz6Kw7a
tcYm+qm2scFUA+F9JyOt+Ra89lucGbhk+BTGzTqg+VMakkM4n561LAM4eG1y3LncYhLGN090pppP
YQsXpZUoIwKP3iOhWcmGN35CQsGlQCSA+BCh0ghTFxBJD7+HbKRO4h3L8hkvZPLmbSTIxclfZQav
h0eoDPSJC3f0zKRkGzE8DZk/ltv//DFifc0tRbs9+fDFcG3BgTILIIIVL67HyxPiwN0Ah8KrAOIg
AgncI6xiidt0qwtCREUhLw5pTBJA9bnVS4wId45c4DQwz2jGakiRiHWEsQJYzmfLvk/3l5jay0Vb
wPAvozla+hFX+3jCdmiZA6SL6AWaywhpYOQNZE843iT9tZw8YI6wUcgWuP3/Veo2GNtJLL18XUQz
7AnW/Ujh6lhHiWYKxPF8pV85CRteEwloSuv4lepuVPHfXDwiaRt399f/VVHQI5lAnSseS+V+J0Ab
bz5X/VVxJxLkiiPcvYYyyDTacykD9SNtTsrS0EsN9ICHjfAZQJ8+Iw9eY2G6Rfu9hRwmzhfvkiIo
/6MAR8IYUV7Kq/xsHXRfSxUBqhNBvCSv2aeoG/VMXji/JKZ+ZkB9wqJIBwBrtTimKZnqqvpjBX3d
xPwe8GJ7+8m5w8VjDKONailUc9kYnlPymylr3aIDYHAYut7gwAcI8sogqNwJk7M28tkRx6z2SINV
oD+8WMwZ0i+FpSc6fpRAxnxuY6ncnKWHYdpB+9sa2+rVP4TV79U6Mmr6fx/kupCgJy9XXd8X59o5
xaSpIlELh11Ww2PfZ1DAMeL8EP3Q8FXXmcDn5AY9fT6ideYtuZAswnl5lSCmxqz8Tgie6XwewVv2
KLfCFz4W50DG36vucqCvpCBKn8F7gT9z3DDrSPivHZTWUgeHEezXU9JQSEHyaKdNMIMQHuXC3sLh
oWjPt++WxCB/Cwd4HDJULXlWtyAAVMVWL6441Wsf3YFftw61HTO5SyvH52apnAatZFFU/6Pvyek9
nx+LRWKlOUmZOzkW1TcwJzAHj0jCiUZpmf30qt4Kv3f648Iil/FZOGgrmuHDlwwGP7n617iSEkFY
Jse0YmGr1GTnjfwn/s8LP3bwcg2PH/Um3j8cGrTTzFZVR/Fz+Qd8QR5IYplFxxtfSx+OJ465jVpO
/MkS8RXd4Sk/Aa6FzHe32JGMfnbmFNcFNVoCsMJap5W3krML7KmQ8RtYAuAb1/zB1kGr1HCj7LMD
QI9UixVO/e/ZA6npC0YX8UJq4oGPxJLbeIMkzcStV1xMAjnbt9Ku5HAilMtLB3Jmj6WTiLZX3D3j
g2ncx6eaM1EwXXqyHB88N6N5NSex0nJb80m+hbpPXVFiwUciO92InS2HYUZKt+MWxkUgYG/xX9XT
OFz0iofDLot0+TpLy34cEXkbtggwHMengkuyQ1TScNZ/v8rMLL+Zy9DOfa3eROsvl9DPsVBuT9TB
31RNNTloQLA1YLD6zEpxnerT/Vh8pVM8zlB3LJFJvE8nw+AuTs+5X74lDeE+9Lv8Y3YmsPBxTPN5
oCKC5kTZ3sXR9F00Y5XHs0ND6TPeYJ4dRlDLjn3GV9btYLDchk+oZm7HDhcCpqgZo4maGUPS2PnC
pUGJphsYfox6Fxw6Cvh8ZiXyTu3JtiMG08AI6U3VX4YiHUr1h40VXVb8zI1ZqwjDhRCInAtRyqUv
qqyXcP/qA0I58rM2U+kn2XrFi1YOWSkZrj2zc2UcKDu45eKeZKIFbwqLFLeH9Mw5RTM/n9l45wre
f3BOiQnONVVBeiN4zdLfR9kjWSiUAHphpVOR+K7uuJKDxkbyWbqJ7cGwMpLQgIRQu0mA0Brsa+P6
q/5STwE3x4prx3i6Hduo0XOwpMR1eLTTmNWJMlj+8bmoxPatQYn/pZbaTXRfAEqDk+TqfiTOLkZ0
odsUGVrQW4M/5fsHt7Gg+ZX8u0JovlVFBja7tjOEhcewJG9LegE4o0D89i3o8GYGv/hDShwQFCAi
aIoel0RhfT16jtbgkPj3jSumy6brneezpGd/omqVATFUwCutDGx1rxlLty5gFsziqXWDLLunzLY2
DHeiXlVfSsl+qwVIhu0YrrqbVt6P8FByL5QfMb02Xx270V2YOve//3cXYp//bNSqzHcVaXFQxlVu
Sj3i4LmU7GoYN5y4y4t/wFz6CkJqDpBh/lYEirZW97qoWWNy3bLJeJ47I8T4Ed6w6xhtogVb9O1T
cc4wWf23NDh0nFW5s0bXUJVoV1EO2VPiGOG/TYwzOy2bZXS7VIb1/RM+O2L0cQmyjyFYxCp0y9pG
bL9XYKlHeQuEVByzbNSw/f50cxu+gOBFbeyFj3Hri2mPbLCQiQnVvvGvptU1F5VQgYQIq2OA2P/O
Q7Jj8/LSTULwPZpAS5WmeL1hJ29pv6XEx4V39x1c7qNYXUhsq7/TRCvH4BnCKboEcGIBvghRI+ha
kkzGZd7iAN1dYb9oTex9WHTdvUuUa6JAarEfgowRoeGW9E7IqDeobvqnVbcPhjs5ssTjC1qZgdot
yj1ljwJUJVz29TUs6861EJIaIm0WxbnJpaV+IHmB3UCAdd5g6GAeSgDYPAc0W/kiAWEjgnt+aPWm
NjJNwjq+YkGRQJLGw7FAvEU1E8+GH8Lr88kB26t7wg1qMCmkPLVcEk5xnXEAy23apZtlnxxSIvlZ
UNTW0/ignjGqmhY8RNp05L6s8djnUIQ2gWs6QKUA+Vf0x6zp98wi2TfqzkMWIpC1NmOd3JLaWUjV
sGtEA4Sj9NaCOdG8u4dYVCD86pzhFclpPcYGoo71DIy3+T8hln+Fq0h6F3/s8IuakbAQHkIk2Q9N
F6SbjQM+B74nPuNUGmzreY90GgEfx1Via2sVFRdWv/dGyfgew6fZHYcs1pC5OYj9WHCMBIUF1yyq
lYoWSwE/HMoqsWbqcg4qPNAN0oxOilx9P3x/WMEwV1Cx+g4kIXdET0wdTMfx6+B3RY+CTBEDz7MP
97yVlc0hMuhSBhPleqSWeQiGZGGKCwzXdXeDoOul4uYi4T9LUr4HWfeZqbjreYyWaxDQu9exnZ++
QFBeil6uh6dC8qA0aTAziq/Vsj8+cQgMfmwBdbi1Z6rNv17o18sLIey+8reekAnI4j/Y7uu5WjPB
hZLOs0wkowYTZtfQuDSNWE/s5g6XK8wWS2mDta6on89W9CmAsBhjCjOM/LjyEsgJsLiXGclCgBye
E5cdfnWBZMM6V/9OLByFhdPhNGkucCIqPfeFvvH/8fYOyMKc4ygvIHJPuGhuPyaW1icU533zLbBW
E8JcMz/kRTdAjznS1ZeW9xiQMNCzIIfrQhNzlQzi2pwAccKtdrYPdzj6soii1M4QTVt+YsFdlR9Z
dC9qNNO2GBquLUz9RAqoupehDjuRoVMLISxkRyHsqIvSqVBFCVDzx/mzyzAKDtvlHpSzavWGMvvP
dzjUb1mWvQp+TM8bRsRBNXWyyj5BI5B5bcbCQ1POBPdUUpc56eGQF5H6/8kVs9S4EvQI3ZUsFGjs
MC3pKWyLZMpEh2TQTNL5CyqgJt32MJKbvQtWpf6iCLYtDUeX3z8xsrvVsydCi2Ry/fgucGPWyntz
EUp0Spdld55y1+A9ZN5MzWdFr83QqsCApJT1/wZ/t5N8vOJ6oVlK8f8r0BDFdCYnB8ExotMD4lMo
hsCw8uz4Hk0MbweAMM7ucgKmkV2X1P2W2VOW3VHb7r0pLMdTko0WOKIIAvwzY1hO5FDjYUDn+8YA
I7FZXlrJZ6wWoLpNkNUMvNwCyxjQg5Jh7g9t4oKEaQsSbFvkXWm3AFLIuNU9yew2yDMMpr6/gbkb
9d6pIj1uROPX4lIaDCjmdqch3J7AZYGp1iihFe9NJ+7VsbTv9Kchrct3FzHi2Yjseta9A6DvTP7m
PjoQ7sAd1RLY+DPL89DYBx37ZI+l/8EKcbMBcgjyYKGNBy40obLhHNLCJZsd/b8tvN7tz/4/M+3m
aPW5UX8Z+qNt+E5zQSQhuHX9GaM4B7YMDfcv93GRMdqfYCrPC4eWDBQZsOg7BrgoysbysL1J5rCs
DF2svhC8HEqAbcBpghkDpvXUZDN8/zyzpP6HUBSK7zYchWujIvy91FJZ4xvWG1IUp6SL//vXA84F
h488cE3y/SmjyZneWdxFVlfCQx4u8//PTf8bgOYoeg5RDrXhwEiSzecCajukYptmLgNezdCCeEbV
8Fwg6yg9cH5WLJq5BN3/gJT9RyjatDD7RxoBAL5zOUULSgqY1wIUAdFGWYRxaWcXO+G/LvPdlMZ8
QRJtNGc1oCcPUjkWVZ9yrgYqo6AM5yP4i1BuN0DkRSpMOYBfr4CTwiCzvD9dMf4KC3lRSpChmPBJ
LLEZ3sNY3V0m2gYhMPCK48WuUF3hPEI5iSEZXdA7kVJs9LsSgwBq8rW5hTM3B8Kk2WSG9eJJXlKp
du4C46KVD+gIJeVH9G63snaw994xhgQRJ/KgyKArTQjr79w8dvk6MpOt5noCzRFp2WK/zTafz1P2
nQP6jma6J3MGVmqmPzUb9/4EdTvDy3VI7CstL9gT960RrQ4KAotczTv3+3w4tP/JzU0+IdvzPzwY
w+Td6PSTe8aKWYZbFz5m6BFPWTv7gmAq6piyBsKsD5qnm7oUL7jJtIEBM+J0eHmYE+1Hs6tXOrbV
KaeedkEb9EBqwxQVcSpXA06drK6yHQF1rFN6/n/yzyKpEAISLgq7JIGnR3OYiMt2LAjADfWSa5vA
C7UBKh33GwSyzFTgBBKBDDGxuJR6B6Ux96iVqx1rIpDeSmBsluTrX+aBBs8VIg3P/1IReUC2XIjv
u/768lg89wruSGPpBKOnlO55FjHZYWu1ouYJo+Tg+x0TEbWmVW056yOPbDz8xMof5AhScYmQCuCA
Ino4nXFyqeEfTL7QxQzlCC3bZQxPNX5YrWMe1c+tKCqCrXl1/85s87tKqQCanE9f3ARFtpYAOkYR
YNLT/ep2ecutFL+kkaNb1bpV6/x05YJEH/MGUXkiVufZCowBuTumvVAJsi8qDR5JmPATHqn3Vu5a
pdqEdbDQdQAdxY5a89ocfti5WugKf9RkDiJGiqcsMj9MNOnJ2RwfjR7DW5B/ubWT6ottpdYgsrzz
exJ525ZKDNp2u8f+PAR2l+zNWorrO9MXW23/rfXw3SujMv4XrXr/YB6Hfjg32pM29rDPBKruiQAu
0ilyQqh6JkM3d89GcEmNof7ZWzuzLBLwqk4Rufl8oN+pfZI7V0ybCYn81hRPA7hvWYZfL7m68pqC
C6aM7Z5uotWl5GXYeuq3xthHBKpbBXZm/unJ8sZIidug7GqW8ppVfpXTU6iOTiHRm7s/GY88gfCe
Qi0QfmeLTjlSxrRjHjFLqKfBo0TKlGDHxvD3DHPTThuggfNg3G+hbG0EwhIvwF6mhANxqcIVZS0C
ZKsQekTPeAAMGkK7jvHXyKVUYW54bMeC5XoDSHqbvVY6bFVPZ3QJXZIeyZa45t6VLhA+TKZha1CP
swSnbRUk4JQlab09M8J4jYBnA8IS0nzjOHD4fuM+T4xntOEgrY9q5pCtyX/AFQqGuAMSC2NgLGUy
4AiGLklfEF+wKPRrGqrLgaK/VkkWPtsRCEoELlYaKQnoe0KETeCKfLQhML6rvrJKlH9vSH21KPLc
9v8mcw6RsCUAv33B7lO0BeAt1sQu0e7iaNOm8SXcAxOGgYnL0QSAuhHcXEjOWbKubZJ4map2q7cv
Bcslodn+5SGcAS5CADgHSR37O9tdUMwlnClv9rDy0MIEaZxgLLK4/ZzinU080FtyPHQq96OZMUYT
OBk/4ztfoj4TQbCipb1EXvv+5f+Js+qSQiJ63wA1F3dCbfAsB0C0zyAb5tPYMGBYlK7yun1khgUs
GXQqL+sFfgNGVTMaZiKAwHO496YoNieZRtEjoKlp5JOQHBUDESmLua9H4yFF01JKsopJ3nGffTK9
B7kherWekkD/8x1izACSb/XY7xQckF+vqiFYs/Daj8bosTu2Arw9IIQUyv6Mjh25ACbB0jJ9Dt4N
NUloa17DJZQ1vzze+rphmL1dG71s65wrfrRvBBgTRl1wBjNWUSixnqKQaL1ubGIkiwcAS0xbJvFW
hexyqo5LyqGNdxz8+SwvAzmB+Zc00i0L4Qt1+l4M8ny62NmkzEfM9AScJ3o1DTAgqcgGDmwegkRq
AtdO2Vbd0iuXp/CV9ylH+nVjRppbZd16Yp8Pb8IFjNZwUGJ1SJXOdB2jUO4S5VaYOMZ4944bLawv
lqWziP3R+WIaKG2jnXG2CC1ScLW2eXYCqRrw2pbn3dRxz4rUADlNfPUMJolTinnLGkRluBTP7yz7
vy2uy+O3SLAXKhHHO83IQpXbQTPJc0bfWthQs1C5OohGzhWFnci79o9+4XvcupN5dUMCORo3eEUU
lXU1NJknOgx7CAb9qpcwI9lHfK3mGpWwVJ1Yn6zwyyyR7Ka6JaLajQqeGIKVk/DdxT1TDn9ZjKMc
gLNrcmMwQ0ZKgFBZl88rsiXkp/3Pj24CJypDrfuSFf4nO+9h+jNRic+g7z02SKcpx1hoDDDemKG6
S1c9KEbID2cG1k5+82AjCaalkFTq7k7guYlW7msKpnVpgyw+b1HmFWwgzxnvzcQEVVkAdOXBieTl
+fFoHAVyhKF8bKG6133ymbuWfycLwTcNR5/digqR02EcR1e+AKpmYhA7rnUzv8c459ERhnTIYWAB
OuOs8tg4XVbPHjiNZNlMTXj5eepND3FvqHjNqK0RToSQNaZmqbmR2TJyNu3OR6R/WkGnz8Rt11JZ
xc/oz14tqc/qnKQT1BUENqS0R8F/5Q91yZfXCPs3c5fzbV6XTC3YNGKJKNsoIv+OG4zji+c60nyz
YQCo/RIlU554lk4el772TDZNhHdCohCA9/oZLr/CdT5cu3YG0jbeAW6R3rzGKDr69BahIat7dZni
sYKt3COb6c+EPFSy8LUpUZa8UfHDog8ZbH9J1PE/ebohHW8eYt2wkDxRN3wGpqkq9pzQco6TWXua
wnwuDstSGb5EbDvaDMhulhSVC9NP6DuL6sIiL9QfJ3fEG8eOaGtP15BGB1V6uuJH4gvvAQ6Mjlf7
Sxw4goOTI0CwhJMJhThWcQBBRDx7Fr1I6ctdmSzVJeMRCzh/7ZubEas37DDrLAkW7KGAU2wW90kG
nBk9oXzrp9t9inzjrWBXSXl8U0/7a0mWcH4Bf7sJa6frpn4jmiUOjQX/MKjZvUlM4OqG4Rd1t+tx
ZMRkS4BPbOcR+v79Jb4j0HARFQzJrGM+YgRSZu3nHg8RYbvFXg+Fasnla5ZvBW6I69nJMO3goOR2
IiPtQBhnpY9goOCvIegs/40x9ZS7UWyvj2ObZw8DJl8ZOcfGYujRdcyFCafO8qA4p6AEK3lJHeQU
jFPft/M0JYl8iS8/LFGm2c5VMN6iAPgMwRfb6GJC1K3Qexum5ruu0kJDdBtEjCFMHf6c1HmUzsoa
U+cx57jahvu78uZ6+EhmrR3NEpe8LfL3Q4CvQrqIMdeqoXOoas1NX1zq/v6yyJYrK8RIL1SmvWC3
QtG48iUB/UeHUZKfBea6cUkx9ZontM9USQzP1N2lc4z1PabXzoM9a+MsfZicSUp7SIrxOtOfb/li
7EJl4XwhZZ5nwcD/KtuZOhKB5u3pkM3NRvv2INjdHZ/miBmGC9eAZJzis3yxymwQ0jTIoKzedev5
bl6mDkXC0tUUO8kNyDCmtpGbTNeb9WC0Vo68MYFNRAA6M23IRRLvji3m9rU2QZene70U88Ya+WMP
+qDtxqePr12KD5SDgi0h7F344D2g5rhPfiPrblXOagCL1p0mIOrc2XAbuKGoxk+WByf0sn205j9K
Io1LclA5v9eEZza7CjcRUlc/5TtGQ6gHeR9vtFM9Zl/c9s/GD4KhD1fgB4t5+QnOhKUSNkZnNd15
zYO3c9vlruNhFEwZeOSfJdt9M2LZpP16vjMxGyiSxgHHkS4M7hBIThtOXBKTngI15kr+FFHbrTXa
DmuMd3xLEDp/dcds7gNikYGlaFqPwNiAcUJNq2BK5pY1WbcG8a5MOeE6KZ0SRZxdx8dHSRSRbsp1
xKtP2+FV6axEWrt7greIGIznU+yvDJBXKE/GRITXkcNOlIECCaB+xvIdq+t6fY62VR1jxiaSY9ce
3kjqfdaAC+GYvH5cN6XILJavF73JSp/AYckFhlP4CupSCMUtaL+ZWxhg81MhxLMXBlWc36zPQfmE
NqE/KHDfSMzy90bBWipO7mFRdkT5ornwmsqn1t5K3S+kiHAIiBEEkzoBYyXaEx/HvpplE4DKqgyJ
8sHcK6q1IE6pfDGicwc4TtdP9I5J+LmghPlFccfgtsPBTW3mSzEICioiHPKYFwyZcW+MbTnArxZz
Om5m6S62TckeI3h/C4mF4KT3x07BPGzofCnj8DFx8hf8bu4cShdzT4uG3/o30K8AUZpYmR58Quon
4cqeLxBkGcnmcgNu0znwi8wyjNWZSy2VhmuODujb4xZCUv4UnwIXVLmX+sISR9488YJbmrJv1597
TeZm6fQiy1EB09aWNj+S70sgX24+XerL70AI/t6hZFWesxjTnYUS/FnmNSEvp4t4T5u4C6GDZsyJ
7t5ejCJzHefrA/a8QftEYu5C6SOqew624cJl8Quvv8zvhTm4EhbRB9tbAi/uYBvR/ZxIGM6OTo5V
q/WOTk8IF9eA676uS4fpXtt1BqZJuyeciIfLOGPy3SOMIfywFMfU0Aw86jytpsrrO+0aCJ61thzF
XMJSQ60Gy7BatQuMaxcnVY52toYoBqyejsN7snn3icTJKWBCR8ThoThDs0sIyAdK89vftAsiuog/
VvI9yUcMAd0r3gPEySJSKIN2K5YM/1bHUfu8vNRJlhL3WxgR12NyUeNHTI6mm3okd6JEvhQSrLNY
mB83PZBImCOV9lcJXLppzuoMnBsJwZkGxxUcLLVTKqeDzFHGaQxWbh4+3OZFMB2dVXxYcLS/rUXF
gkcNLdpC2vyIW2zKSvvQZJh5DK79vsBenqOQ2gZhhHqHQyM6N8wx1zqVIubFj1job5mFTDCfh+sD
GoRV6dTgJXD2t8msIJA2xcOIIuXC8a0ubQIeoXAebyTgzC7Md8Qv1KIEwnSt8VyLncQOnEDBmWZW
voZ35XgQkjJU35+j2zp2kU8mjCEZTLETyi0DEGF15h01V0ZAFMYSpneN8sAO8Gyiw1rDn6pX1ijW
7KjtDEwmiyjR5IlCXg82dXRokCGvLUwbTehT2Ug/b8XYte0Pok3qZxT6HytuX8HHFMCxcSye/LU7
XK2s/bnQfdzcuxNP9DuR6EpbZ34GxXYR7IEEemBHlmhmJcB3uns9SkhAncx6ddk1DikSNzROGyiv
Tc9BfvhUxy70Xnr25qYJjL9q3nx1tlWMHYRSondGxk+gULgOnU23vCXbxjsdjLb0aKPIN5Rpfo7c
2iAJgzq6ufSJa6i7EGxeS0sM6xxgQcTyYVW4e+Vu+mdkmFGLVnnkzI2KyJb2TtRNIa5CUrc1nmE/
ujQ5b7N6+lCufI64Hc5S+NazK6f87l4UEP2+HPveEbqlyrD7nD5obKP8BYBy3H9A1xXs1EZ2vrjS
3i7onttX7+xgBYkO4yB34HdPBj52VGJJn+HkU5AnBQkhq2YxZQSQfLxasDxNZIFYje725d/Lzvur
tx12TE30wzNzLolhcU7PxBf1IVPtowm45AAzyCFg5juEe7vNZG+u/CPQZLzKlKA10cbtHAk1wa4v
mL3QnTNbMVrhdNgGq9dfU6ZjpjTdUDn7K8OeEL/M3/sonhFnIfgzDC3gl8DPMJ+/bXK+qiyRBCRi
zfkEHHiJiFBgBivCayKxbvFiPoDlZOgk4/oEYvnUP2kWAFQZcH2C7gGFHNkY431XBpkJYIpbKrEr
OAi+MawkavgcfZTLvPLsXb0S6ABLvu5i7ZitiuCZmvpbJ8SY+h21oIeFCeTD8iPeki3az9hxjDhu
MwZu25+VZog3AnaZpVIIhrf/mqS5eH6wn3C6LD4K3xPF9h1lJ/hVUCVQsIFoLktqXd0CNufvzlqH
VtKj/S7I986Mb09tE73Al8OgGgzdlhStpsnRDyzOeK03ZubvXkXRVkyHrVeM7mGfbN3/Ar2pjm4q
HlY6Red7f0I0Gq3o4slbCSxdShUF5K3PxLdmWSHM1aj2cspsuoqOTDSb+xv3eqvDzvcxTq5wkpHY
QXDKDkDhn3RIQdKf6j/B0LQASgq0f7d4EFMFzLsTTwpcJTjYmpAlGjrPga8ZgBmzJwck/syGJ0tD
s/CR7hI8yxp/wjgGiDN0lkDcDPRC6TfO1gbTb9nrp/K2FUg9mHlO6CN4OAUYk6PGlLPDPjUyE5za
sOQOi3dzLunh8q2FubjmfXoGJIT7q+qYUhx7VGQOuo+n1zWTxP5s11NOBY5GDLXnI710v0bMVs3q
hCskOmIhovrgWM7Bn1mYbuKGLDLmZ9EhkMTyLfYhynDqqqzts+7rUvNM1fQmGq+WrT2txLuFcQAs
QkzkVQwI7guq+8MhLsSspwo8+AHlzvvyX1AWzwq+yUMf/aBr5PIbvUYaCOBjpRXDgUersdgqi4WF
d4mQKSgvv4I9pKOQpGrM8M/dYqn4B+y5rLhz4EhzSOeeca2Bsg4el3dthwg9vVbidL0GHIzMZnjt
HS+2rORPG7tQ/hr/uVfc6GpzzvkEYQzLM/gE43Ao/Lqw3aGIYdIwi0Kj0p8YFTaTyBqdPQ9Mt9MT
pmUGjJsgaIhgNhMK92Xqs2ubEd7V7Xpe2W5D8Prioy8WCsJ2nY2MUP+PwnvXo9jMu4X77aaOUs+O
qDYfvEi2OR7hRmrPaGwzD3EQiRLWRikfXULl6q31qHoLXxDgItdN+4csNSAkhePGjWODe94BeSw3
M71QBO5XEQmY5V3HAK/WCFU/w0XPUZPxkL1xXY9nxBWJ2uOoVBARuuU23mjLUJsCz9sH+gWZsyye
TXemLEUIL6RgwZFQvfvH2kvoBbl+DFWCvBK/d4gu+O6HNqHFAP6DGezRKDXHMN0+soFPN+b3wWfx
oKN/Xk4mPI0Mna5mS+XyyRwX9OH9hYplTCM2VmYf82tcvN7iq9voAiQdt4g48k5OKVycsXHAijXe
us5zQEa0/7hqzTQjTQ8IU9rCcYzy4BQ7b5GMQfS+/EmsiT0h/OcZtJpykcoDhvKSVhPoQVKu7K3S
ch+G2S0MqVOa6mTzwDqjRU5OZK+/V24kyyd9wfvj0P16HElKC+7zgVHw346Xll5tpD+h6zc1osYL
84iSg5x2Bpa8WxcRUqWVBbkZ8gLUgzF91WO9hqPOvcQFLYSKZyBX6+y1cVRoAeMc8GLRZVAVfIg6
nCkFtEe3E6gRFuGF0XtRvbxzNgL6cszczhoHwG+pvusB3kr+j2UMhlV+TZXzmBKRV19yjywCUtsC
3+L+lUhW3aW8g0l/1zBc10TXEbABbLh8F/NnHFwM3ZZk/oZEosFU6UMAU+xZmtLwyumPTvzoPouo
1Nkf0nxeczxw4b/QUqLOlUkYBLTql3C6pdijCGZ2u++7aTIstJED+e0CbFesGvKDvgp+vIHHV/sU
ocvsuuVuupeqcCjBh9qG1CCJ39nxaiDrGoPCF2IABRYKjqm3fMWScNgIUKprcNgKpzIeccVDKTrb
lN57W3I/NR/ptxouZy7Uu71VGK0qdtBxH3QP2OYWehPFElOE0ds+LAIBlAnCEmQBYVVZGBqOijDk
/mOfh/MbzFzBDgyKHKKD9DUf9Os8naZerfD0iXtH1XVcuhiLupFzQwoEJlkWACMrz6W/e6pTyN18
Z5/rYCyGuGhVi3ofbT6pYnJDZpab90kkuOePu0e4mqzbZxLRGG6M87VQ2twTdVq8MasRjPpNB6ii
MwlzfkyC6JxuqfrMBgGcO3omh1e/G35kjzxwEVgoUos/Av/8xOZAnnZKtLA+I/rUjNefPvKpF9qU
jDW5fOXZ9n7VnjlNCLzDOmpp3jvuzkbxw2pZ6/POHF1MgSlwhtvzZ2VL2RUH2qEyILhw2dOcSsd9
b3nIiVF3BE9Jk00/t2giY2BPP/2IAtP5ajDMHAsdeOwo0qrcOL63yCO3LsRvuL/i8o7hbKbvOrc1
Phsz2K2xTmng7cAAhKEjaCBdL/pYw6dQuam7rFGq7Tz8nwkmQBcg53n5A524jG4ReiTH6aNNJDYV
rKEAPtWVORF5tJhX5CKYeW1JteK8gUt0JBXHNrLakplOlSzGPcntENcfdW4TzVmNySq/gHL1/UTG
6Tsl7yDbMaRlRpRRTkoXMxVCkZ+wyb4Z6w98xeB5RUDmyPRECZet1Nco74iuvaMPCIhRjve/sig6
v/Crcwyk/MqmISeRwnCrxPnPZhZVd475dS+W6x1ClrYotpc4/xl+mWl6fdNV7uWG9h50eq2YHEex
+aIVO5tCYr1X6ypt7ZHhcAwzaew5nxxb5eQoInhK9jxggLhA+iW1DzidJ0C6t7kFDa2x/7VdXwQH
qAKZ34I+V7FrYCnEsxMoJW6p90kJvLaRQBREGQCgetP415KFHOlNgDz58cnzgxMukqUHqxCQ2WAo
1yU20ltR+JNIXVpLoktb3IzexBVnbrwkx7W21ZI2TUWf5SkE0wLCektv/lpRcryDt+EpstX6fTrp
g6DnT89QYAghxeEgWw0CQR5gV2EDCvcd/c8mJIRa4sGgrgnfGt20JDKFZ8DJOXbE+yFy6TwP8eej
dcczK6A8xrt8caF9z1VC5OuyfPgEgeTd4httQiuAfmKDPW3azQfxG2f8vEooHHOGbHrIpu7DVONp
EFyCDLLoQjPKvITnlnOyCC3/S5iOd+73gYS2ZFmLczSvZ+gqw1GZo2JD6ao8SCdmala/0ilVJAt2
mSb4vImbhVQlFlhy1Qd6qLbT+uEK0KqjIe3t+7d9nBo86fV/Ydkji1UJEw9Ad/QiMBd7HP1hiRoR
mIc0Gb+2Rw/AYnU2KYvhP+6L6NHmXu05DoxAanNcZGNBbxCPs/sEDo7SoVIo0eVLnaDHd+JSwX+u
6toEiq1Hj42A0W5lXSzMoxyEx0VvanUXDdzY8xNxuf4cw7gwvs8yXk1RkEuL4WCO67smFtQWI+fv
xF1eFsN5cwcDSTmNcSRD+b3BAKe8TKcfxQzbrKY6+qOyXOv5ckWBcFnov88JImx8qrIhDGlzpbz8
pF1SF2OaBjJho/xFAQ2q8Ar0mDB3nlv/bCQaKIA+4eB2tSwhs8RRGd2xl0zs81zjX0jaGq+i0f2b
xAQCzqg4J1Z4uzca4byD3btNF/Z+4gm4nFwXyDOX+uPgSopOQ/f7Zh2VQo1R0Z/4KWYyoXzuwQyG
OJEP4R6HZqscZKOzO5Sr8uxu/h/7ph6ys15zy+nyyjQeStUeRF8WykoTOVFBa+Qn5ME7KkldDxix
2w+uj6yIilPDfTU8s6M1IkoRrJYdutX7gxc+PpwoEs1zJjOJ70I4EaU7vBcfCPf0rUq54zf4ZPRZ
vmFL/b0ztMzb7QASGNEbFhh8hwrz0ejDY1+46fkUBO5pJwhNb8tTepOhoKwKKyKd+RaKQOd3F/oX
GXYgkq/FNvCszcv6bYYojGCFEej+/XcuLluHm9MqEJPxOEFJoYWQZFhTzqbj2OKWfr53HkmOlyvq
ILioKT6WzCQOsJxZFV0K8fRUGFRT7B+04AlLhDljmwXy4tGJz8mgsjKD4vUHFyfC6ad8itBanJln
Q4RaQDibfLiCLR124OD227JWCcu2s1bWmWdknYe80qW8xXiKjZYXPHYvJvrpdcBBObT5DP4iev3P
IjxMWWuQLlha5ThQTE5r2V0Q2cO/KyKts2eI1HDiwhZrOaG1ino35DZs4hLpGiD50UzpLj/QLE0I
VXV2zZA3Sb60srnOQ+UmIRKzJp1IFh1XRTQBwQZ8GPSe2w8SRWo/Br7ddVPa27zQu44l2yJ6L/mo
aJaWOzo4M22lVYC6UHE3U9GfdxaL9QZL5SqMGy9AQLMCDxjpDZJjG5Ss4g4OJCdcOHPgVcvFTZqR
wbMj+/gvFeEaVO8Xc4bxHhOiAGDGQLbj8WQ8zcVRmEvhyRyFhUcwnejFaCX6wu+nHev+/CXftsy4
ssRUjpXRfHCSome1vri57/D/ZFv+PXhl6gvT+tjcytEbzyU5gH5+ku4TRi6i4P53OHx1dO5AYte9
PsJpU7nHcpi983A+O0XCYXX6x7yhq/F4iehdJUAFf6dpRucxqaxyQaD2zvmK0GVh1cDj2p7QAmBh
TSlMSwZZboiHTAOjGdy15kjc7ulkLOuXNln1LD9eIqLql3ZWhHXx78x7NepSYja52ERd9SJHkirs
JgYmiAfxzsNQxKNyyta7UOzJ6UKLXlqZc2ddmZHva/jD+KPl2GkcUv0RqOX5UGuxEHxRT13nPUBg
s1iv+8oeaZypNV2k1Y/Iu/3JywXtRv9D4Gp6b1+MHwzEYTboiSM0t65qrpJTx3SrXpkx4rrHAEwc
fRcsSer6vW6ATwYbgKPijHQja85ePbvrTWPvUBy90pBa/DwdKMrPVcOkoB6hNfX+1Ej6g7byoXrY
xqt08vGEBVyVu+1GqO1GHArBVfaQmvwqBJRPkM1EZZ39cD13l6Sj+vmiAHrLNGx9v7d7e22dLJdj
zRO6laZvFRQ2dbV59XbQov27GG3S7v3tvvfUB6TVzWVUBLO/vC74QOEHLTUjRdJagfLXzHKZOYAn
Zh6nwy//Vc1kmtExsyiWfq1Tzdsd+eyBu+q3VrQVPLJJh0Ckb79Xxp55/f+UW2OUGaegJO+2sNCR
Qx9xeUYzia7bFyq9na9mSGsVEu41W1ZXzMqrukGXzRCqgl4grE+wqUhyjM5hEyXO52D7ge/5xMkF
9NHNhNp4WCeXGTrZkmloWkBoHAV9b1UwnpbCvvI66ow8+YJFs9Ipt4l8/4ABJm1aXS/9DhgMRhuP
FGpkVPxxRukvUZQEW+osXDxVfzHOURC3yQKRySsUO+AaqEAH58EUSzghXZZH5Q7EZiHsUf76cK4d
gn5WorQzgxDfSI7HZse5QozlHt4QiHlekMSA45YbKXlCVLoC9Ixv2CCrlbd38jQwiF/BdBGLt7TU
PpatP8pEnNC8WJGvBnmNnZvtvJeoQITf/VeZaKt0tP7gqXUpMfx8tvFN2bC2hv8qlOfE6gAjX+sZ
T/0T7ObXM2w0rU0Mq4UFy1V3hu9E9Vc4gQiGR8kUrsqIO33sUokK7KkFkGrKMy7cEUZUbcCbjO6j
jmBupGhkSvWpsUVhcTLpShyYfURJl9uaRFyxRYTQr4OyTJrhMXFI19hxYI42gy8Q8EWLy7EsRw+o
Be4qZDaoSOffwKddoKm+3aFfYAqXhfoURl7Af3zddIMXgsDjPapglqoFVULrVGEV2y32KtWzEfch
UOMi+Rreo+Xz3RBh/j3bWuwcmiGZlwk1IEITKt79TfSKjwPlKwPAfq8FNuFfMMBCMTaG+fPxdyEx
XjpqWQHQqOgA23CmqBLI+Nfa0S0hLaE1BHpZtKWAwoXtNvHrTaO/TL1DiaAAX5KKdPu1l9AyzXqv
JWvotWI+cpQI5PvbQ8z/VKmgoZRnHeFJxmFFNmbv1EG2KPga4cxLQHF7ZHDNSPoV4wSo629+qnEO
qwQF+DeDcPrYNwhcIWd5XasLzYZ+8Jh23RwB8AAPyuF6RjbGleSiVVBk2hxUdX1YFyQIdWq5q+i8
5rVIPmzPCuDkLiPXDMvUe60pmmk5e+i8GHq+u60MJ0dUV66sMOcNfLaBCQVMruX1l23FvTleCsBm
qUh3L8kfQwNzZAICArYettib4xVlQGRVCugMHphq3EVIF6f/n1Nu8lH46xO4oWXQ5Wu7rbicVmdP
Fn8o/DcV0Sb8zL6cxauP5OCIjNnjxeMrxNrYg0LGIiYtnnOvzU0pB+Nn5b6jek/03KlBk/cXxtH9
k2TfyhtHbfCsGLi52NZiUaQJuuZzzx/H8C9tz76cB0k1npMf9bWiehzsqlawDmeYR9WvBZSgqQ68
NJ/TExj6lpaUGYUsJuaxlqEblsABKSq8nCsSbXlEq/YDdFzwdU/5sKxbCluZxZtO6bqGwrzZD6++
MXrPgVUR1CKHfijgm0nfzvlj+2oDq3i72o7TrcT1z+rQXhPaM+7gZtp7n31GxBy5fJAWAkQvWx/I
zxQsHNSQ371FKIOWQZNL0lxcDKQv4xcAmEXCWooxtfH9CWWiVmXeaMSzfIv2u27vsYvVaSkaxqXH
GOWENgTrwQLOvuDfQFp42j68RhakSPa3TNLMiRYEJE02ktdnLFiD5O+muDflguQW0JLqizFBqt5/
G7FH5oTVWEvQ02YLMe0/+h8uVXoqsvk0J/iTQuGvE1T6f3XR/jCjmaOBJeMQsvyhv2fO0iWxUazY
6XhtW8u0NHaSrL1WJ6Tyo1bgMQZQZiUjp5+S9GfcpYMskCiw9zmtN6iII7RCSG5scDaMNKXa3eiG
RJ1n8nihwhLQmjgXe4ES/IA8Kg73ITbGhf59aH0ftnsQMDeS2uPSXPyQ9/9MDuBbCSVHTVUcE9Ad
qvULv2+zle0fD79X2oj0pokQNFyKjghD2z4qfYZgnqrqtIGJp4ZPfaQ9711NWEDUvVkiO7cfKaoR
QB3BJn+hC9CM/A53OuRQ8cJjFhhNd60AX2jKoM9kAKtQoYnC47FtxOyvgrR6HOlSFTHP3JPjyi3K
KpILLMZ8xqyxthF0rLwAC8xoDyiJC1Sa59HXzrOQn4SzulSd4hhsQfmq/rbFeqGvPfUOKfN2AZXl
QrzwueHfeC+w8cV5iPREVH4Pl/zS6EgDwIEKJ5HQYdr0DkC84qa5GT3yO4kj5FYxqKRLWknL8lUk
Nj/hZ+bCrSuUiXLa+jvsiXMfd+nju4xhZQIDoDelAO6FvKWmkWQlTqAevSKawSGVSQA7Du/ODXuz
U0DaM0/8nJZOJLwm/LWIVpbP7EbDMBA7G1ql3t47Rp9FIjFlpXczNIot0Y387qrj4Jx2K6Ymc4HE
R4ZU7YqWysiqGUN8+tnnaLRXSc0GWBNmG3euJCdX8+PWzs4l1XOraqS1pagu+LoVWmg7OQ/wPrtV
gkHW/hwGAbv93Mts+PqwB7kIlkV+mQRU2Bt/oiw66u+y7slE0k2Gveq4zI3ZVsWCoHrjePmhspAP
6GikzIEKWF0V6GiVFkCrvNlHAooxbiHgxE+nwkiTiWHgvnrR705nP5zbcLFIGXUcz1GN96l9ssHv
5LI3QCNpSgjfCfAHJ8MNs8ESBY9mBIF7zDPT0d+MLS0YgDeyheF+wTeAEH4BKPmbSY8bLHdawtcN
emnUBBZBtqhLBozm5XHs2y/KvTG4IproZ+eibHRj6nrRp3akdMhzxW7q7GYO9VELEAs4TvyVg++V
2GQUpCV8Smsbzmz8f4LG8tHD+ElNfEgKwOx/fMvRjF1DceYJdjfhKB2bcdWdBQrq1sVVXQD0lI47
q25NON2XAhyCtqh4LfrqgeHE4WdWjO7P1fDO6XsrO1k5ywuzKV8YizYH4eMS9tSQFzoBtj9dirgK
I+zWfcW3KukS542mgRMXie5x2MRSsNM66XpL1FftnW5eI5xokkXGXgPMEF/D7gchqWh3oZIy31eo
LNWz8c+yEv6JMNg1+9mi5NNUgdxccZdiBU1pi51UhjzH3VHrdD3PF9TlJtBDY3dvEfvcHfR6JvLz
/ewWihCXWuzBAfQqk4qK4rROdslXTxSWlrGyMhYFPwPmzboczqoTxf36FOHw1W3HwaqKA+CsuP6i
g7lO0jaMWw90NWkFb3Zejx6/orQyPbHES/FuOVqwz4EeIkNKfauhS3h/6Ssx35fkiivxJ+N7v3PJ
Zd8kBWUs4SKbiOfTqlxcEqBHX0yKKZkzohJM4FN7HO7aMmY+KCGSEq1oXAuyfum6iaCfHUVIVZxh
7s9PDrsRf19bvS8Whxm+NDprX3r1PiHNlDT+2zjxnAMDWyG3YDlYyvJOubuMcjFOMEMIaf9MzBYb
9V6H9DNGHUA9qVXUzvXIrowKNA8ANfrqFBTI0lwovtMBflyWjDTBU3anBoFCkSpYQzMjDqwiu7jj
kUYIcO365es603S4onXamXGmseTawfSKN2K0E3EsYa8BQl9QE0IL6rOOtkbtNde3kK8a35nVD2m/
ElSHxj/gli1qfNX/86675x2zQ+6qrZDENnkxt5ffYqdtEGdgfYCPdNyRBlOfGuQH4SSjSsSWKF+/
h+dlmnCP5WFAweebSBB9v+/FnQelctb/8TZAjb4jqTbxIVj06DOAFt/K8K9vvcwOeV1RCWW5wy8c
pE1tJKqJvLZRfeRm0+TytmVAWeiSqxIQSI0Cbukxwj053lQrxcUbY+q8/XaQHmIHD+e+rXEnRoRQ
o6qOhKxIJt00UF462ShUhphZxGRetL9OUV4ZMSVsN0nrv876icCg1wsFEuV51/TUhvI0wSfAMb7i
Qq+q+NIk0yawIRSVnM0irlJ7+T05vnDN7iGqDy+CFTodc7E/ITgswCl5p7CwE4QL3aDTD1+VAmxA
1zM/v3DadDun/HpB/mQDEITFkf46ZuzvfP0WCiB/+Xr5Dc5zdH44FZe+WbzVz2I6mYqlEIA2Z6OV
cLiZJDVX1vhrGEVhih0xcUKv74clROjCr0xi/7n9a7mPgyEYfd01v+QDH0kKT1uStCeU5VXF38YG
PVCFlXtQ1cKxRZy9brB6FufbR+uFB1VBgC6gjeAqe0XlSYahRUSC0KJRzPAmV+AoyiLWaI9R4//m
dxrfjhsKBPex6QH5+qyQsON6zD876CpIUdLLU7uZAqCUAKxSEy/No8FAxPdntiWxdGGR78yw0NPG
rMrh0u3uu5hLJ0RtqpvDzWLUtv5RvgzXn9f8T3rqginP0uaOmWZE8k9abed+dH0nu6KkB8BqfwoG
FrPeIVLqkBFsJbTKLR+7bOxvp+Ep7LJUccq+KYjKogx4gTb0Jve49hC5YE1ZnzLVqKTEJdjwmVH3
9EqoYysTvvir4ew+rUg4WOIeFHhAxOY4e8kuAOaDXJ+iUx/scbb9U+CpaT+Y8pXgpauGfte1RnaW
9MySYd2sBz61GeGIHAfwkYsuGsYIOf2fU6dl7EV+ZiosiQwjtUpTnUKwJ81f99YaZpNgyj0kiuxU
BTpcRTuIAFMIvdQv9KyUSExBHrTCKzgNke1el16kZfhKrxw8oLuGRYMBGyj/xU+1mdE7l9xYzKab
BD1PWmAxBzHFa/+l27KympiuIyLtO6LnwN2ZZ3Kg1EnpdJ/GlibSzb1MeQ6CYW8vx4D6TqKUn3KY
gKctFS/mw1k0z0U05k1ZmC4+hMYGkd7rmx54LJ6T/QKeOqdW7I2B+vLx/Y2jGrVbjLHjiOruMlUK
uzGEnI8diPLd5JUe1hdwFjhaOraITs1GzDGcJsvAxRNggAZd60aoz1k2ue+mmp7m33iy5ozq0Yw5
YmEgBjnB4iTOZJzcJ8sXaHrANAcjbi5ZJg+ARQcoLkSaS/nwDTr0N0OMmzay8OAxtNv+vG4HjF4m
98tvgQlWqGtWFCzqbqbcz4fXlyT5eMUc3aqrGhjTGYUKku95odc/kGqzbYypQg0J9zBdzP5YrQFq
dTE6R6RuXyUKMSg4zIcySSIFplzFJlk/EilRQJaQe2ZcZzR1HTVLHgjg3FoCZ8tS5nNz7j69L8yk
MtecL2FdN8pEFSO4+mZE/RXWEWGBJpsTWaRO4AhIsChRjvZUfIXCGboyCw/Hy9QMEiajH0A/X4TT
IJCxmUKJZIH7nHVA2oHeInLQ3n2fqUMLCIhZlUdg1Fn5Z+9HPr2AMZXPwGVoechZMcwENPqZ7PND
J84F/fXtAwCWHplgT7VUEFXCEW07zs2cOhwxyzibToZVg9XGlI2wdG4sne9aHzfczNtw9b6NaRDx
sCydw9XztycPQCzszGDcfqyhIM0KTcuXsiV0MeSAJ/n5xp+Kd+PdAZrYzY+keMBjciffNqfZlMhi
F9CbB0IbvfQ7D3MrvlTMmeyqtpzIU3d8lGYyzifsV8lAlbIBJ48TQWUG/BIApDzaTjfHmy5XlBDT
J/yrU2eTOZVQgI44noAtBYrWtXCYfWgMQvANeguE9ssor1sSRZSZOrQG0roq9tguZP7Fy+m4C8mw
3+EYqUmJr4V0e1BH017khd3HdNDSemzHjc+uII5u/kT3PRlgSdzTus+bjM571d1ELF58OZIorALA
YgNGuwIP4ZMEz3DKhIRMC+SIiq8tVDAibJxHFN1D0T/PDC9uhf3H3+f0VTRrxRY8Kgf6owVASg1R
9tRaWI3ooZjI23ihvSd53zin2ChRnhzmS6clPCvLNrUrUawRrtaU2eOAND++sdaRmNuEq6De7PFb
pBNLwlAvyg+KSOYsyHdo9iV/pl1wPY/Lw0hLwsRJgF6pXeA9NQhHNPjyue5/o1tIiE56gDNXVd6k
QNHGgz7pzwd0zl/qfu9sM6WzIV2gYEj9qPyQbNsRFOuZfljk5e5cEhAiDdDF2ydSMQMR4SSVeuNI
XpbHleEbJHVEGlf1rdgQRheZmfHicPnsZuL1gte2bcFqL5QQbYFuJe6PBoREmCqcXL4+B1Mun0h7
Xhtt0KpzIHVEsDkRdBCqmF74GDyzeDnjKjPhfTQwwfIuGIAx08jtGqk61SAd2VUEvLjVj9eu2ENo
243CDvC0ZULo2/VuwinPcNHK+DIGWIuJUt3bzkCXiSIQuPAsqzpLbBgkxu6iYfhgB+ZQRU+aJEZF
VpuNQnS4DkWMvuSccu/+Ot/7cQnLR7UlPRYBHHI7L7O4uSB0nV90YMP/13nkfxHM8AuOBYWnOGEZ
5sgjiY64u+qrb42MzExkt/OB4i46eqkoq1LSUHIZy5Mpu4vLDq8Mj0Xj1Ulkg94hOLe6h9n9aAck
+2rrcx7aaEWrmpGhzlUG7iHr68i9nCSWlZgYXBCDa1gpv5u4Cszta/SnVIjPW3LRJK8ku5cO/3K7
Zsze2MuiR1vsQ6FXU79abSKTSuXn2KtIceJZmK7ynLjj8fSs0E4wjtH0Q+rC088T6DEpVKRnjA++
Gfg/eowWb8k7al80rVwjW3o7Bm1uJe+WajOdokY0bfVJk3xhkqiowEX0jWpffN7BTJxWirRudpW7
n/O0/D/fZ1CpJ5ZD38qqcPBHFYNvD4P5FnY0pktNiNQkol11X87g5eqIXO6O82HsWCvhiiv7lLxC
3qKE4IMWAxHOGcYEqVsz9SOcdJvF4awRAbxkA4raUpgGe6xFAJpy01mE4HmeOlvFhybWv4rj9mNo
yR66cNaILFcYZ0IqajM0dc2ol7jZOmG1lDavTlMGT8QgC/+KnJ0u2QLiqo3MC63vZKCMsF/36J28
qAB4G9nLZOFUQqLbo1VwKkBc71cVQA6D8lphlZR/cXxb9ZMeBsdiUl3/sX1aecno5L9nDjRw5SsY
DSw2VTumGxmmtHbFIpTgwTkPiSCWme5sxiTDQqM2f2eU7MCQqqDznXXg8c5h6BeUMfHUHL+Bjfsc
PCM+UgDik2bE0VwI8u0aWqhNYdPYAM0oStH55QU/SdX7xK005KYqkTyvzurNLLD85pd4Iu51JkY4
aHMNdlOc/2gQRKbeRCH7N2rqABr/+m7Bz2TlNkR8yq/eqhePPR++OXXj0l9KsRNZfZ64nplCzMPo
NNrVvi9yW17PgXollFqE6LQy/i0wB0IItj2nfodcz+PPPElJfIQp2J1GjBcQSnM+ORlqRadD9Dk+
+DrEcK+3W8vfdza6gfTAOa45UjY4B9nX6eXFrrXWIH11nZ1rMjBR/Sp6RWX3uouY6DrlLqe44hsY
dmZDi2n0SD0+/L4dDORnVqvhUC7SDmO6qVxcvP/DsLYjqq4g9mUYItQmrJ+S227pmOyQ8GIiUGky
lhoNLlSwQeRzCYtvxh/XHfANF9mloGJs3Scxuo/dcbNAgye93nCeCNPQKLBu1lhI9dokQ4i/turq
jBDQTUk43W32BJnmUKEIOEPUnKQ8jQ82+m61lYjth0REu4C4mzcRV4xRgGyaTytR9zDpt32+me2q
P96LnEbcHIfMW28awk4AbW0cDb8E2p6foURmcHFnskOMXMlzY3pD2cgTlpcVxZj85Pd3yjfHwjh9
lzFufQL+WQdpsewykqUJgpeGq8WF9t5qqq6QpeeVnBOw39y3AApk8RmoU2GGjcN6nqr8jgj3iDnl
OiE21O04ZOk+rwehhZCPnTznr4sUsNXQbixpLqqokQg0Uuc7N5w1wAkYxFKvhfqkEU+4vS8kTX0Q
7VZcYvgnoWfkhVUV8rp+GdeVwl94W42eNFkwnJjv0vKmeB0PH/NYGUz+83ceZuRF6P3e9DsGs2N6
8OKo95NefUmB34tO50RKfQAthwQakbFVf4RMlehnKLvR49rlRasXBs7UTVAfRDukVVS9/TM+dmW/
UC5Da4XX2c+Pxf2HP3b97KVPgvcTGbb9pbnA8Alzrnfr/J9eO1KsWo6nIcylIoSvpRIRjAXIjR1k
JbgSUcAL4L/Fx5J/LkDEC0J52TQcUWAvVU1Zl1tYr6d1qbwR1OvCxzelxTBPEjQgI9CD+jhGXmGA
AfN88IZ7l2gyfPNf53tGAO0S1ARasKdxO7PcvvLEqmR+wE8YHyCuUb33T3gmExavlSpZuzgz1Nhf
oh/DF3WYKypJQ9AdMGzpCdpEFqe01RD71DLU4umT8GUVaXTlQH7de7ParM6jGW0VmVLsxqdKd0sR
Hhc36TOzuBz/MW+XciH8knQEjaWrVQqA9widdjJFLkwcyCKe/9cMtHZZzCjBwob6+QR+MIK1pyeo
FPKbFdeX3w5u9tpWzlJi0NGZDc400fdyLJy1rdcMTdD33Pdw73smJMr4+eKGFZJxuD+71KiYcOgK
EhxN5xdWNStYc7OlSW3vQjyl72vKT6BT6KpsnQn+8NwQJDYxuL4K/JOGHg+yStHqKrm/m0SQGwVc
sMwyXDfKUVWXxeCHidK0lkdrB/k2A+pR3m4PZD6/lTtL5m+8bxBoGbR1Xn//GYrorRKWFb7Ve1BU
7LnpbImCnV9WXC/TuNWFx8/GiJNFPbDhEvSwNHm/7hv9zjcvyNLf5vnZyTkm4A2g35GiV3iHP9Nz
Cj0ysZJb+7NmuMghxrvcPbu9x+tuMXwfbTYTE5RxiWjCNCMNWwl5CSJu2hL/CyN+rOGUN6YDd+wV
yIcdDiyOxO6STOJNvlVrDHr7aGq1/8VdYDpjERkCv3Ir2UbXT2ItyNKbfcpzV87B6IQZH/JGIunM
46wHV9cBACFaQlVSgBuCIki2qVDPG0JPLeekouEcvg97NJoUOJbqzqb/ETA2HuTcGpi0M7hCOJ74
MSMme1KsY+nVs85dov3MB0iyTT9XOHuXg5WIXPJ75mDe9tz/wsSFBRTiDIpfKiWQQpGbij6Nj3Rw
J4A12HaU5JCnc2oVLSHAvxqmZ9iwbopDKb8MQDgskEdjB39djJcRByMmyGxPbG+BBXj8v+k2Megm
FYtjsuhCU/X4oEh4KWd1GQolTceGG2GwzN30/+sgIR6hUY1mr1sCR01tIm/jRK3JTET92sscGtQB
xBCDW5U9PrzKLyiB0/dd36ksvLV5ny7LorHdm2SJ/9aa7qWprXSFyc6MfKKwE6iAwMA8kyqB5bj0
YM5D1lm5pwKt0pRDb886SCvoTRKiqDhpyqojdigtjLwKdO1tULtN89rtQlbvh68Sg+iIZYBtFeHg
DMhfMJfJEJXaVHzKkUel2MrvcAdXxPsu0SHcGb1kHyVkiLWbAIY2nq0Dj0PNXJFPZwvr23jPPZ4H
uFEQ7Qtsxh8nNzH3SUxGgmo7pIuOJDQLclvEsFT+16EdA24BVK3pHG5oJXUsTMAcjZZVM8KGJwUd
FODAPqCA3umbpJXPx8KtJ+cbb8UcAKLtCBLD9yUOqBWEMsdl6ObpXcWGrYaPHIP+rSLBjQofl9he
sjdW9XwBOOdsdyFktzsPczVOcjKG+T1QR98He9PHIZlfkxdroOnWPM+4Vrqamg6Hk+HxKlnYTDfW
y4+Bvna9jemOOWizHYDCd56IDkHM0JXfZbXN0KmM3itob4BWiWkTsz9q0vhDiFyk7qC4voMa2yNs
r0XrnzRT03BQf58ke/NVVsDAB4+MFmYZzEFj2b+vfP2JLli4v9jYQXpkxb8U/VPTPHlyUCWdDlRj
BCdIyuUZQpvNKq4MdlheUPHkLQf4LwQ0yveYQblFnAlNF/meexTHeFBSKJqckxEXhVGqLm2Kv/C8
/7v3RZ6Ls2MMI4DS/QpcEj80dmoGxYTbXdOIoqmepTNf1z5F5rWHtYgUrYSAYU5HQub9yZ40nAqR
k00iAzhNoU6Pb9WLX+oK4Fsk/cQPmWlw/GzsYpfw6tMtJ6D1XnKtOW3VOPaBzbpRME2/FCaE3TAv
BTeMTsEaEPFGikGUPgR842URg9cO/8ssboihapxyBwk64VQwqDc9i1XhGLhSQKH5HIdFiKcvlG8c
k6zOcvabbyJ9tnF4mCXFx7cGl0NM824g5rD7sms+3UxA9MhZ8ETaj1ELiF7ijElr5wJ72F92x06H
VJgsYuqghLDJeA/adogJTWEJiYCgMkTd+nmLoN1yxElD7HYIQJL4JFSwfmUPJ8Gn5zeSwsS9BKZe
NRVrc3hPHA2OBpySQ5C5yXzHFA0GbFgt0Ien1a6MCZuGASgMLeqkyD55KwzM15nl4ARXRLKqNN7q
BAxyOqhSCajzXLcXitbbKFvQIaNQbuKw24Ky8CY1+Vpb5EjGPoSImIlbYirGYBWFLDQkPY9N2o9/
YVcBt7Kor2bVv1e7io2CyJAodApRdeQDWYUTb5Md2RvQ+jsdwF5gTXuSPEet8Jut3x48Oo999WOt
aXQoHF2ugy6Tqb3Jz17XOPQV30JnBECvbg20kiO7EA8iW5cui18BYWalcw049Ixp/AcxW7sryqTh
+iP6xY56URIWCFrij3rjguOGIa9JjJeKmSpDz6AZoMVOiMdEf/Hq/BlnkAmaKWbNAZlPSIMeqCRg
qRc7MmGajoRtLmDqOmr68ezzLZ4PIDM713h6cFeofPsQBtns63J0kFHu9pTQLyDX4mHIyqpTc0mW
Z8BH49rAvfeQyVVDkGhG4EVfmEUj0rLiSBAFX+j9aGOqES99N95kJ5tFmuHdXzXQS5dxzAlGUwS2
m/MmgKFhFDnHi8gqlqE/nqrFkQ20oKZdoUZPs/dbrntBggKrmM0RIZ25cWetoBw8R+GrC2yTWVhK
CKI8v4rTq4fdeOmMB6lZQe6veOmOJ6UwfUIZoy6rHtt92onZNiYFeQzA7rS3H6N0JXZcINGieO1J
s+0fFQKrRrdj5evSDaVv74eCEH4agmIUiPR9ot4fF38SGwkgdmGJiBNkY/WoKmwGBf5ggHks65B7
Ro6Pvf9lK75LxtJir6SV4Cf7Ox5ZHiFFaIvXjfYTB3scezQRwBVSXEUUwZ9rprufJ7HK0/VnoBGZ
wUOTSthO6RAhy0DgNa6I0bnGAG58mF/KetTUmMEpL4XvsSiupvMWFmzWnHKW3oLfZXdC1Nr9WiyT
o1tveQ5WGC7Py/n9vEb88j+vWoj/i7sGuo7J3Tjaor2lqdJSmdo1vMsd2gZB/BCqA3mGDAXbScc+
2UtvoIhh2ZIXPEbt5uO8KDyOSrb8EWJDvmlqM0jj/k7HPjEa4JWqnDvJKbDVDcSgHWhf3jMvAlWP
q9cbHXjOvw6tMuvT/XBaNSHA65qtsUenK6Ga1d6pbLCkAxjUt88lC8jk4XwkwFlLgA2C77fGhmJZ
9aRX0OZgJSvC1ihX+he4w5ORuuUBbgqSQkFc2Os83CpBM6V72bT+aahj8mhdZiqWswWV1LXiq064
9Zxvtzsu+yjmhu0RcwG4df+obyjYmPE3I/7ZS9NJ0jvF6U45bcu8oYQNeZj6sMes5gJCSb6GTdne
ys96ZNF/ISxjWOgae7QdgDFD5Y71/eRS0AU/4rVJALvRWmWyzQIxdI0//j+qUbjBfWvcVRM8qeCp
2GLl50yhuCXHscas0uvdjbBLzJCIJJTHaNZW91uUoUmYlR3juVM2gWaJIu2rmTxqoimyYZ1oN2W5
g/+JI2kMDtoohoS1ZALTWddaLaQ2GdmFGIqIh7OzpPxXldb5THB+4Ycw3/GtuonsO1Ptnl617pUr
g8Q0Ax0FXs1DDHyy9lYp9775i4v+6pSkOlE1z27jGSFTIQ5lSjPG6fIJkls6acbJKmXvuN9bGblH
3ElnnbdIzjjjpletD7wz1A66eTBViT2eBh32Xa3nJGEXXjpLw+xcH7O07lNfJxZ4iBo2hORXKmE7
yiRLxxTdVi98z7FQggV49ZTWTVNt6bH65qLbQaz9H7/KQu6/E6qRGkFUU0EOIiup3O8clTbhnKFX
/TRl8Dy2740qpegRWVULdrSlpMr1cPLdnUWFgV17BUjtirTfXCby+Z3x2Y1h+e7bh+5jfLRVyqHY
nMF6D595Z+k1Vb5PMnd36ShWxT0J5NYqVUlbKAmwqHuZ4GIjuhekJtOoYzo9NVC2WGLOY+cQhPvi
CoDlQ+VIOd1OXTlLj3w5tgeDLdZ10hgrm6TjL9mywN6eQclSDYYw88oPw7+XoQIFWaeLnX7G0cNN
cncAyTtYw7ZtkdlJWrMyVGk33opGwbToLAmqQ7QxuZnE8d6AFLWT1LwqF4ObvkrQTluV7IJCuq/d
192rDb1jon/g8tt5JSZW9sT5w0kbFSmKy8BL1mN6iKbucb6iZggAJnPbKVR7h203PxW7JDLdmGh6
i9uI7xJL+jakKO38O8oqNGJRBh91Jb+ocTXiVfPMc8XrFLGh2g0UpGGn6mXtoYWQ3/PS7cmDXC2x
ZaqDWbiMF3jGtjTGV+S/as6kQj5xA1rFUv4OCdEB93CQFMyUBrKOucfEk0pQoHHynHS/SodP7Va4
TGvtfN58kwyi8S/WPOOsVb2y7o3KWAU06xkW+9qwELZnW1jX1wH2E+/mw6f8iIF2x4aud1w4NxP/
lGWZ89Zfjr4QNkzs87Tlv4Dy+t63dj8eepthOC9GOmgwhOk+7Dy/waKT8VQ5lmtJxhKuozY5CHRx
KeFbmYmoVRPSF2whX95H6DAnI9Cg5chKvh2vGrzhjxJs7T0yxgdFVGsifiHJ4vPl+K/Bq9mPJOJW
Pu1rexEEiH07M12n0gaX2N33FUKmzU/k7/VTITKCobwuedHBlOVnbLlXRkLWSR9ufUPtC7sDZKC2
4CI0F99miU8+9a7ygNx7UHBpjciV8VA2BH+GVceJubwlVFoYO+im8JXz/XzIsymv2TZqBOhQbep1
aDcREmRKKPRKkmGvbXZvFXA4zbZDPy0K0qW5WzRqMgZ4xpXGv/YuHzKg/rsCEqyDoZJbjFmgdrZb
zRtvxiCyE023PwA2imtK9DJVDqd+ENT5mYiqSwLaX3raaC5ncPWcMYkRrGlJ3XZXvh1VmvR8tl2t
aJv/Rz/etax+KXasPXcy5n4G7Uvm6TvihBEHnhRfblQjY3hnGcP00baGZSSY0ov8phnG0uXmszxF
t0NNYIQnxWRgfEeFet0MpKo46oppyFi64UQrwEWKRYD2w9haf/ik9Zv2Osq2qPEKRb+k47xQdcU4
XT3FuL2RrnIKwXrtctgRog72EN97YxOkOfIFZctzB9SkLU/twerjuY0VuuUiouP0x0do90bEFQ29
wn8i7xg3uIXaBv4QbRVF5W6Z4DZ85f7NAenUvGtsBX9spo/h+z4diFIP633flquBm+jjxGlI8bMI
9nySRNs1XNHz6lrhuMHXxVJZiDtw5BxWNXPxQewryIo5GJQFy2F75iwHlOvqDqs2+r6sTEmHmH3N
SwlVyXr3iaf4sKlnCso+ken1gDhuX1N0m9Kmp05522ddsyL2PId/AIBQpwALfsCY17ch+lIRzh+3
ZDR0rku5Uq9Lk2UG79FER1FQRwAKvS6FGHPznKtH+nXG4eqOIxbhVxwLDGiCF0D4tov+pRwAFC3L
e4KkDoXR9QJlapeLWTRBpSZoo6UOu7DT5uqGOZff932kXIlE3Wj5rapgmSyZbjhPo/6zKWgVkHOT
wDKOGvuMt6AEn70EuzOi3AEnypnI10FqtybKk9uxvw9DRgC41AvmlNip47m3exAaHfqZl19PRCXn
AtrSphhtD6psePuY01YFU/0qtESzGfPwcNBlwjBMGu4bEfdJ13gNyM17btT4nrsNiDMbBSfTndS2
yyCcUMgp4IcHTjYILmM5gVkjM3MfxWlbv01ic9a2d07puLNcBpQ3url9kr6/1r8Sa4xLRvMLbKrT
zzLz8+bSMBozyoMql2gOR3kc/rRbQB/rg7LDHXpG/mLkefjjXRHp4IrQf46wZZeH1WPjt8h7AYXD
sG9Sej5+kF4MkOWd8mhruAtlehCYAPp1j+RxL4VAI+8fXzKWscAIFRSCCdyn4/2qpz2s8Jp2fDgV
rKuK9NskHx+1FpLvnZa4fI4BOqJ3Z/jlJgYsOMPsfwARx/UkDfaRPqPzBfgQwRQmeu/BxKGYJKLm
NbgYVksowQ9KaRyFWOX2z4nCqgKE2i2wQTClEo6UO5O+6WCo6o9IMMcbuOT3khhSlLv+41NuJTzN
ya0YXJI9/jWubd4k/LS+nNKLyNoeSdwkjGfGzYX7+6w8LYonCfD0eo2O0ayYRedOspKZOZRSIRJ5
BElTF5RgfudBXUooLhn9peRnExTI3CQJPXrEs89NsYvQfIxmFuVvMsJUoIlT7vQc0Icq0S5uMrpw
yic2MlYyBOTc0yg4EVALHXOIGE5aBVxfnvmnh2VxdpJnO0HGzXK3oDVnm3GImp7YthNNUO6SS4ic
aItH+3b7mEovt+x11SYGPoI0bfzWx3RV2uYm5nMaYTu9/8HPPgZ4Ch3WiF+kfTPnYPob8bHUi3zH
J3D6V6YI1WeiS50yhg/fr4MbdySHcANBBMr5AATBiy7MdS19ZpkG/aFNlH+vTZCPLHbW/KFfO3Kl
vLJVZaMXzFjg5gLuJzWvcbRyCJRIgse1DhZH18hSoq3qXWi5dQjokHSxHLK/ym9gYCJhoxVZiLWg
oqdP0/3sjVuWa8HFoGifmTq2KAyZWfYTpSlSs+COTgau2p0RCXnUJuiHK2NLiW45uoCZyvaRAWgO
SND2DAfeMJarQnFUxaPwyxnCuceqPKsqA/50fSnmyFrAA1nWhDprK5QiPeAkqfJuMQ9HRYM8X49n
nrELGKZmBYENQqN69PmSYLEGSni79kgzUcexCqB3RBUQYVP/pbhbLXn+weG2MOAO1oKSZJ+fHoK1
yw9lIwMRuQIba6yqge+uH10l6J3IC2ik/J3D/4OmuIYPG6PpP/UpK3xKBCg974Akp4krYHdfyWjl
V4Hbz6H4UbSrMJI195WuCM6liJ1Gb7Ma5azIIa7NDV3IsuarLfDuCMK3b3maQDj6++plESJPbKan
K0JiCAePQWNWMpHZCYXHhYytUxWzk2Utaj3tyTbStvu4fBCeNfnOr0mYCzu6S+GzPCV9DhfhNi6R
l5NhRENSXXy+Bi5SL/4B2AeOrAU+jbyQtYSFZEEZdH4cPBD0oogj6rR4bJQ/52Fuw9rTU/FudiWD
vc4sfx2T6eE3fjO3Y5Bp8QxcYxJ8zZ2C/Lg/DGmFBibOQ1dgXcuJX+qwY0Cmhi93ryaWzEg//OA6
/Jr39FuSQMkP1XlDvU5k7OkYMCDyOSzzILVCqsKnAwBTH4LVC8ykbi2PhkrAC60TMHCSBrAupsFC
Xu2bxVAT3z/8Ydjv3UKDN+gWsmniAHNCKKV7tJk/M8utt+kkNvyh9kZ7pVI32NJeWp2BZhZPFE7H
e6cyR420lYv8McHzRMQbbyTFGpuRW4G6EQGfa5uYRmhBVAJPe+Bmwxrgka8EZjPtyJTVy2L1wx9V
FN/gtk7azCNaxzvgLAl8OcVYmTKUs1qoK4ugvuQg5oV9FRnKN/MH3NMMpXHr0DGo1LK64CQdzln4
NCl+JZdS3MRrW7OLDJoZ10q7OLpGhvlmncRwOFIRWcBvj/XCIEyi/2ADVBw2zMlzRJqVs8RnuTUi
C1ajvPaMyPlFUCcY8w8CKBhPV0haG4H9vgtQp6CePKr6OHwRY5KnOKg7zfhmV41nvo3imXECO1dL
9jIu//cXWpmlE4Yr4lycKN7+MBddFNnQNOcLLkkms3IJu7rsJLg/EGa3YaaQ9noLZFtckmAabKjL
fR5XdxsAHo4u9JpgcdFkhanOD5AN1qogVktQdLH7GVqrCnzae8vo4MTqxj83SePk4yw9gf8/V6ZL
qzIWDCj7WSQySENGea9XpkQiDAHC1ndo2JOyfiN+VPIYPpq95Q0JiIRnj2xCxnMjGKg1yGqDEg4S
k5+liQFNaWy0Q2030ePrdMxPGcraH9rzHliuGPntP880Xpeps8rE/mHdieXz486N6PjBLBWbT0Az
epjKYXy5lNYFfOrQnRBDlZz/8Isnn6Tmc2Z2votBM0oeEU9ywWmqUXBrfoK7WPbzO8moKhLqVwmu
lh/pe0MfzfsQ8+Z4ReSyn1axMkOJ62NtdiZQs8dp3J7EGmEEWPPRjpAzz0Gv4u7mbSqOrDSjVYpW
BlgmglByFpTwS351tnsbdkEqV7OBsddW5RyQKEsXVhG6IFEt2ZOH66GTJ+cYeRtvIHao0HyTDQQw
9U+kK6e5DZW08yGnvEHMbGBQhZSj0srBX5WPk22lzwWCZX04FYdcepfKyat+KOeYdA7LfbBSFmKO
sJXZK+Iddb0Sm1crJhURZA9VooYPQMaZdDu0Smxa1bCXf1jTrEkv7iOsV9FBkl29vKLG+wjN0kou
gyxFeaWAqrc6HKP7I9zaa7bt1b6FTMcPOsN+JNZHEvKxwRJhFQyi55bL4OVPuhSq7KQBL7+TJkRu
oKgu/RGHfFQg6DCgyRCdib8miY3TBtTLZVOTBPk0VxiHiH4U2VvACU/WzilyPpNKjAQbwJf9vD5D
xu+Sf6+u9D0yZXIQnWIpto/E4Ui9/UztAH69MGc7o9UFqkxgyBFnoMAm1ZX7K6NPh19oHiYhI2UZ
ZRY8eV1ouBR4erB9uezk7zu/qWHISlpuLzovZYNr6OupJDKFG2yqEmyI6AAdPHkD/IF4D/k6yZQ/
/CJ2HRujckaIddLdclWo+Lbz0rtbcHzaDDliuuAJM9Wo3y/WlSRp3Lhs3zB8ExXqmx1updznL7l4
cCrvG5UMzPxeW8aC9OKjFqU0010TLPOkUZ7RnssAzMLoJScUOFWv3gV7SbTV8IOI/57w/2ld+Xjn
Ute7qjSJFffVgCbphaN4Waen8/eH75pbEmexYc9oEQFfjauvTE6FoO0lvSCyYWyIi7lumfMV2YeK
fs1AdWUbHPWGnUFGghOXFdizO2MSzmaRZ0JU8OD+UgIXfgAFRlBpafBxXc0g32J9qqenpSgBsKB0
hzwXck5RttKrz7g0dfqsfGccW7dkHuGzWzS0yZVx2dDwCCLn9xVgHjaDMKRSWmqCMD1csgo0foD0
9VPDSfjyzbdH9DFBFncqaYlJLhhCK3emwsn6UTIXPN12pL9EtflU1aY5thb2hJ5d1vPBrovtWkvg
zNOiT2HMz5JFKPp+aFHMBmvZ6FH7pclM6F9agpzknroNngun+dVroHSym+Nu8QhHmPjvOO2j5Fdd
Jon+W9HLIhbT2SCrQwmIAocOyIFBqQ40SHU7Oht6z2mOmhmskIYfa4vHtMwzFu/mU2Yr3eWjlTwA
UK2IKS35PyOTh/qP99iKCR+coj9ucTeNBBk9Y0mD6DVoKZ2LPh79Sg2wylSlX171bl5c2HBY0Gbe
BIb2gp1KMj+wzB2KvFMHW2F15s8kB3r1Ah+dlOezfsmEVzPQPkwz9ZzMD0CkwL2sEPiHjtHuwlJl
64hBf5vMR3/xV5tCRRNKaMJhv1zH9vxZ3hRn82xp/poxSVz3WWw5KYmUatcCHSeV8UC1Nim1G3ij
I1nahp2W4eFC4JSQauF1H0ZDZOclgHTsEVdUjPFb/wCQ4HDz0T+U6rLoFOqkVzFESqOFfmhOBbdQ
wgT22gHE/ykF9QFcYd4Kp9ejgLmohh2Lf49vTXCmtw+aWObsCN4T/ApyYcMJU0zWhRk0p02I37Sp
ahR5kmHGM1+AzF0/TXRabO/tU5B1F400Y34rT3/2A4jSBnWv7NnVXZu3aVOA73IOPtUgs5KeTzXY
vaYbQyT9YZfZl1jRHwhac+i4OgZdnLLGW/nJbfegnQXJzBmmYel1MeCxXPUVd2ht3FiPY/B+BoFQ
I3jUaiGLLowREI6SfpZ0EVcCvKvvNVSdydMQvYZypVacFmLvk/kpU14a2AM1seIi1+4z8vW4vk/F
Awl9qd6qV3Dx0Xd8ssF91JaHKYECUG0245i3Cq6/rKyvDq7sz9MPZmCZxc6oa8GGVrYRPBKSevvv
yHssQp3Vh9hCQuUvGHSLQQJ5HQ8QpkMGS+sQSq1s3wtlDUclzjYHa+SFLrMusk5oIt8fYD2t+SPU
gp39aniD1rl2BH+EOCDMTjCsjzmeF87mF758CfBX3Hf21D7GDpVBOfi9X4Z0vVGrTruLgErX2nfZ
/XhWaMym/DYzeNjAFMDJfEPvL1NyXlWryAQMjrdGCsMtzNev+cI2tcJmtlanrBnMUmFjGZuEW7Sj
CQD2TjuqxZ1uIXaB0JeeETN9gK0wmCTp5zxWwZ5troIQ8Iv8JMTailmIKYpQ01vMWXlpI2/s5eVG
+jjMTxUa5z4g+2Wi4xQj0ibwlVEohXQHXF+mHWVQk2BfcUo55p9W4fxg6Mvn9M5Iibdss12MQzop
en2+hZWTjQ12LkFy5faPLNO+OaZGATrutfqfl898DfUs1ZEhnT6izeOehqe6RaOxDl+C5+l72G65
gXVX4SMCb1jYH7Vx7uyFYNfEYs9uwrX/yL+dQQWWn5W/JuogjX2UhIzxz3l8qu7c7JbSxK/h5GcT
a8TV57gbjgT13NTxIMf8/1dBhfhzIr65O+Kn1OMgfrP2Bn+rQXmepvBePpaBiu7SlPAqwXaaXWKu
WXZsxCWIdobX/0lut+iTZOVPs1xfgP/vK7RBZUoQcHX1M+zAXppOCrrxMrkMc0k6ADth9tN3VLVn
Rs+irNpV7vDqmGClcDkv7zH4bbgfYt3/BR6hYZTrtjig7Rx76GX0r/nV0xO2k4O9H1QI6KMrN7Yy
YgjZTNGQCF1p2lzhMJH44v47poY70k6Gxn/GWSvcr9zw7isBGv8uzSESS4Rutv9VRryiLQIj2CPw
MdzwiI5MpmcFahYn8C4FNixAtLJ+8Z3ERx8hlgFZFS/4PgGVc4N63wsaYReTyUqaPlH9hsq6FKyN
19RWOIowNq7h4lpsRI4BN4RalPrwxOtT81bG7BpkzrRXemQ3VOVfs+3sIwH/s4CiRF+AFraK57Ii
iUuHbQx9k4kPG0okHQu6aCyGy0BOkRqt3G9iFU5kNntMIS26GH7dV+ikQnQJ4bWf4ib1TwDS018R
xYNIJBrLNe2Dm7vDakrYOPTlzxPnrMMkyKc2OMfhOCzRrDBC7o0WJ2Kbv6PdG13uoopSYOSJOXxV
BB4rRAhcm/+6diEivxrvaPT3gLcLQ4p7bKELxIBrQqO6Wm74vn1aw+3XQFI7ihQXZf2uadntspUj
qSS0ku+DGmKWZMaseLzQPCyNAtjGfN1KriVIvVg5ggemIMoszq3+x+9zHLz/7Q6QItT7qi1Xn4PX
Mn9FxvW8fThDKqds0azR0WymmrstefEcmI3PdhlLSy/tblh3vDTtbfguXSfeIHX8mMIVSF5DlHVf
JAWDrje0U0O8HEfhpVwrwlbB6L/HdKwIS8WNskASiHhfupVdqEEuL+7joBuUGn17dGb2VbWzLSKi
I/D9PDScOwFqe6toHmUWSwqPlRBBFcUn4jSOX6/Zrrljfa2ODb2Cz85m2vDDCSLmR+VnuYO4ngIo
H8NgksQp8MREYjj7Ph4q/cE6yUB90vdCChgNMBlyQN/aDjzYy6bf5ZLyAVw3cEP4UCGi5Rvv1Rih
CXvO6jToVZ3EHmCa6dQuYzDt25RKxNtwjV/noggekwpev3QjAmN0fvKfvkCHUYVZeT0Wk27xn7qM
OT+VZ306pr+HpT9Coi/zNDZq/QvNfVDCIP2YrFGdZ1A1dgHiFNSJFMiiIBMCx9nLZuzbxTQDrV3C
ZSMHGwXUW8M2AFhvpGiWP1hU8Dw0h7volHmqo4Elm45l87I/O5B1I8OZPO7ag4VzWgNCtF2cufHM
P08rlKszStFpuIB+qTJpNDjB6B6mJp+58ZYx4iPuoRAlX+C9YbISWf5YJ23MSAqLzDGSdmD4MEnA
0ZU6S3MilLNXIuCCxZ1MXhGaJnydE2dtTddpW2A9yiboWXW2yP28FI33WXQWn28bnXcMkTSMaGuH
evQyNq23x9m4C1QB37lLta24yHSm2z0096cl45VG4nF94/U0VWuTkGX60BCj7Wcq5oO+qCNRywL5
B4pVPHYBL8GyWu6KiqdQUJMsQakhLcHWCgJHJhtFqyJLrWU37Ssg+TYR6PnXIvnjctIakdKFINAk
HkBgFcXYsUxQW0tFNAdSlCVFUrlmcWCk295Y2uUHr5ivKgszNHqugTWNaB/d8EElR1Alne5mAeeH
H01mxVpqqNQzfmKkHF3w8t3r027ovGRg4tPdNb10YMgvlYL/RHSTf+7O5a3YvPe3o9Dy6yD+WJHT
Ma0oFU34scw22e3AWZ6C6OT9iHebdoFepvbYUGpW+uYLcACUkueRkgmLEO/zZC2JPYNH8K0isTtH
7RhjZ8t0GvX938m4cFudlaZi4eO5lofFK34omqeL1Up++l0MgtsdAXtWdvb2lNXKkyBMAsO1t6sz
wleA6bdDu7kEf2KQfbs5vaOcEyH/qZ8MNfvxLSwqnK4beqljwBTo5PHsQn2lQ2retsIhpdeMwQ23
1faIjGIfVlhKPFwD4KiGGBJb6oLKhVuBYCgFmRQe9FHvtNDrPrKlWkhtDpSJJOzsGti/FEEvf+vx
MqirCcGFLfcvUgDXS3/lB5GH8TW6ZsuzV1pdvtjPMiLExLabBbYSIbQygg4bg2KTbCm9RZ+mA6ox
dizQg44fweydI3ffgYnnYJBQqc+gjMtdXUiGLWqqDGt9TC9WBW1MAiEbvuth0eK/jCTXu4payrN2
6hZnvudmq5kQJjuZytC13GPeN2AeS1qMVDmEMK7pKcW6VFpuodVaddYL5Ig4i6sT6nSi0rmYWRcQ
nVteMRTVw8A2dTGSTloEufVBEpof6OspRvls69RAXIydUSWxeLqKBkSUHIDtA9gmUXycOwyqvILb
16GWvLP5vnYR0Z2uOln6YOKkdiOeb5EDzhHW6DQLoJKyVIf82NLZeeMkJvKZ35M4KhMaMbqtuzDp
/hcOUwnmu0VfjrFpfivM/E+FVJvxKrB0oSr9OOjb9JLFo+KvSxQ2KXLy/aeRcTo132x2U0AdTYR9
HTxPxY0tP3fUREOmKOdvAdnOB+cL1//ZAeOpUBeqYy+/oMhuvZ9ZlHRL1Pg8U02BHIL6x44hu+RL
yOj7NsIF5U2GfKp3X+1P7kHpNfUctusw2ufzO1I/FO3AlwfDK4Pb+db9MO3AuGYVXwuukoHaBybK
14+0aHV66TMIr3pm/bV6YicmAoyBlTPvKPrqV2l7uByAcoiYY6Wp7BLKm8AcER8S4TTnwJ4KVn23
WMlh6usxe9hQxokhL8ULBQkxMz20y2qmw2nIp3R3zEqVkGpBFpf/A4GpgurZ+6ZVxGkn/6a4TKII
YbtlLFsL5eme71mWB/8drpNWU1AZw5fL1s3XZ5UuNgQmvnPgZihLnyLDgrJLYyiH5CDwHvNHEVGl
89Vt0EVi56CvgtbRMjVTtZ333aVvBLyB2ud6NZwC9nO+ZNTqb7y96fk0OUpxAKOGSVtk6mYBK0PZ
xqsMkYNlwEk5aMbUyXto99e16SE4HxrVqNq3O2c913QJ3LsOHJAA+KLvnuuiXI/k18kH9KiuZIS2
bRltSL5Y2q2xVBbqXUpCHbec0054yaqEgNbnK958TmTaKLNFzHfMLatLwznmUxKEP6INEf9sUR1X
11eb3+TskCu82Nxn2Yryvu9HLOp4ekGYm0n0KlL0ieRl3ZCaEZ6gqO9vJ5p+PUh0QfEiGIpRV6un
woM31cJ6DSYVC68l2gDXN/zMM2SEqe8/kSNmna1qbGc08+R9IGs4GKjT95vhTYTPN3zjZbpiJuin
2PSTQBCkHzBACQyRNKyghXf0cExKcRxqvybefdoGQN3XOAAQ30YvpcoCqfVOK44Na9zU+BihfHmz
dwNYNcHnHrSI74Xy3nW/IFSisKM+wKBtflwqED1miy0fPjwgV1v/AIVPvEBXqwTETvZypVJLnjb3
1TuDFlPOsEK68sK0VLj+epgM8SoOg1msjRJSq+olHgJyx9zShiY/qU/GT9p/8DEIJPz3Gcom0W2I
CKEpCSUbiDr0MSsfU68DQfqnduEErEf1SEh0CEiJoWz9m+32WsQQySTDCN0ehm3BGo+hst2i2Z+3
4yUx3O1SLvPQRtD81gMhF6E5FGOCjHhWuuhNRdd2dyvE82YRmxJ+KZFLxUrJG7086LAa7sSyBWDu
azTILEs/WGniOXUssuEGGJapirLBBzcMwFs8U3/lHuOuIk0J0463u1Ej80ngjgTvGbBavZ20bBYw
lH9VqZopDZtXAWWnGv62VTDgBOFKQOlsI7EkC8HxM3jpGbbkAONoGMU+/OFE01iUVOAP0RFuMrjf
IiHVkXYA7E1elmOEjvlsI7OTKhKKtL66jmi0Iu7H027vAxHyQuRl0wHQLS5vbGIzqA7NxfVNctOe
fLdpM6zSh6MZHnPowXGy/Sukx+PWvf+QR7yJJu/WBqNBs+/eEDJibfNzWwwQUKwJbIn6xm3PMjxk
pR7jC43oxBDYdUfkn99upcML5xZ0ZUhvhDXSA443vFJGTdUSBAm7+3/5rG57BdLl4XwlY1ld9RkE
rYhnhO997rmDGX7upYhPP+sEXm9kInCYF4yKR2dUBwF5jYMf9mgZaLpL0bDI6t86LhSgYr8+GPK5
MocSsSrGiRsIcSRDGyYmEcgI1VqRAxAA609CTThHlDL90HG5Xeiyn7m1My41VsrcnGQQEi4xA6MX
w+zl0SiBgiFVy1KNP+KlRwIiCku41lyJiHYtEVfuCR4b89iyS4gFJWBUKpAFYx0pKkkpdNbFX/T5
unwK+LURbWXKBv/XZ7HQVR/oey488+9tM2RVmBJaT4Kwnlhm0Lh/ABokFWcCH6CNvtESDCk08wrz
sVkVAdUDew3OBFrjjtZ1F8V3MHmIg4N4p1JW7Vdz1b92IxoVk3uYfli9O4VJH6is16iZyLuRvolH
EHFQAexdNVwooxQkCt2LBUHT5uAfc8mM7jd+WuKWgKccazklBpPZRFxjg0UNHCrTl8xR/3V7Mnsu
z3VrHxYxX1kHPuQ3EALeBghTErF6Tz6h7iWWqICinZiOGSWPBtbB6h2UWcvrdjzJLskiUnNrqO0Q
eeb9q4zEtubvtOvJ/56BXEUUCCMzArIVSJvyONMJX2bhYaKQViPWNPajNVQlejImnyjQx9MTyOrD
mmb/T6sVytAmqaoSl/ZLb3nxA6m0zS+mb0GLZWx/G+lY4th7PAqDtOKEzmBUERZamiW/EomjOa7o
1cEcx742ZCL5RYFYlgERmsNCCx+NBFvEGGc/hDB1tw49FhL+mbOB5yMPwwjDHjnCsIey/TmWN/1A
4AbshZy3vHh/IvWJmCbU1Y3PuzJLbjbdsWtlxtOqYgPQTa90DHmLvWqTUmcfAxp9C1iT5EGB07Vy
/AkCT616ZVBLFNPOuYNTD+4fUqDGNuVwQj3jvduKo2Lh0uEReHpKOV7TCTID6h8SS6WdIt7WT0vr
8gILl/Tna4o0TIRPtuTmwE3vK7ViqdCCWTyhTQvreaEPvp2rstxwIeRnxqcUW59LWtPGEEbQVv58
eGLVLVWMjceFYjyiHpJbv1Ds94IpYxfBjEGskRxxRaK4OQVjxhfKqPhy3/ncLkyvi0qWjZVs0arb
X2S/F2l1PZDqfXzhspcsURNglFI0pdFVxmwbu7XBcc4sJ+zQFNofQ/LgZskOOeVElIzRsbOryso5
7q1CBdSX6yHZbrAlhEj+8T0H8h0Ym9tvZSq6uH6LEa3EonhyOK3FDquZOftMHzieDiyYwR4XkVsQ
Zo2aP8fD/S91HlUT594HcpVNjEInsMvOn7+FPPJ0mpPYAJKpKOlfRAw3a711U9PZktu1PalvT2Xx
JQ09LS1WBTmfLcbLht3knH11Z0CPl/kPaHZfPgYbWX8quFi/tSj++gVwHXO94Z59r3LzlQEmuRcu
dyZzldzOqAW9GzG2lAFNVw4uXesAnvbCw77VkIVHxVxbo0eZPdvj/13dPObHXCXRFPom+8D8uZtZ
rVCkku4Tvo0pXXJen2u6iT6rjKeOWMXlhyHtTOE+6WkKZ/GtCa0NukD+6HgInH5ObVDnz7p5Ygel
exWysHIz4jX3NX6kqlf2x/Xd7yW8zFGAfsguq8v/I9tnLP9z8Zkh3SnaJZMH3LgyA/FBBYzKYCej
GGB+TZy1t1prFUu+S18+BeDYTR7e9xRH4HFziosTm977j6efiPQL+pSyKi5frkUH13zypVkxxp50
W1qawaRc9Y40l5YUSMpEY3BPSM4Feu+o7OzJ7IcSV3at0S/j9moU+w/+LecMOZSbv74qmC9GA66b
XO2JNQyg47hifud+ZFUALsMw2qtK+GQi3T1SOsoWT4ncvg3PcmK9FosVeIY+7/88La8u0fUICrZP
gQCFlu3qfFW3xymyOMHCZeEIuLz/kcCjN7uObKu8VRX2S3kFHyHj3jCKyuqtDb+5ZXoOsQqZAWzd
DbB7y4vyZCcDcisaEk8w/uVlgBKAU/2W7F0DwuWAegGbKlZHrzn2WfcPFtO39k5+muZD+O3v+kk1
PXt5QYUL0HyzCvNhWJkhArKS6Fl5bGZHLIGxYCJ/rlVEsiL7pU6JC3I3gWbFmVnJWPMNcg2KhTPk
Q+Jo2mHZ1h9QFNLaIay0d3Uay5+R5liiQsBP3MlZDl89Mmm6hpcz6OghQ3aKkQlNS7p4jiWq/p0Y
7P7nJQklU/BmVHJKZG18+PFzAOgIfsjfDUBMdGWyCqoPad2hwSYfqDTQRysyddQx1QK/WMLVOUSz
UhG52JRk0zZbPFtXBeyCbZVAfrSaH7iCW8d5eK28YDRVoHKeZ6SYIwYbJru/m7OqiLPAbQKkstaq
mYKA03qXGFHf3Xz7Ewj4mlBkSmqHy/+oXH4jk9axTa2baTmgymGtPB6arXClHV0ZjWgTs7hQ/3AX
KERCku7sB5aKUGGcE6Na6M7z6YX4wJRpLyoX+nGXFRb53UW6qCPnXO1/dDpeJPD+enJ4EjL6LdkH
mgED5AWpGHv2hUngat/9i/QLMzeckcyAfFrCPAcOZJOdhG4oBu9rg15OnXdgsk4BJI4TsNLNs8E8
TxBaex/HB0pDsyhRl2sauE9j2U8XvGfcRnZQdA8WKWUpL2Eib++70yuyPUw9DzwsF/dHcwlswTuG
H9XFXJVZyaeHxwvgSFDXwy2O/0Uxlz4HRipFV5OgtEw+jfIyDugmtHhUNjC7KnMmcKUkFIMlG/5J
MPWxz0e6ta7908Kqs0zBPiow01JpgeazZ+DRq9Fbj1MECk4DfJgO3tZSF2wA3ngyVysn7vaJi1WF
L0TcSH2btPS96HqTVy7zP7HxXSI4np3/OBTZv6kCl5Z90Q5St03lz8qfPvha8R5Q4md3YvthsOZf
Yt4dY+M5Uh9ET2c017fylT0jHi0h9ELMsRGs3zmvQYHDghSbp1xdUSP0fzvX4Wy1DfkfzelIVW1R
jMqwiGQ3E3S12aHth1ASOpsVnPdE9xO9K3JlzouW5KmNU5sxLQNgPsJZnFFRmTihlB/a+XF+9YKa
nd6wM6z+cGHvnP+JXJK2e+H5I4wMNQpxldoledHxCIyiZFsI1MfPF343ZU7YZlzYx71WH21L5Cbn
LbNrPrC3DTcYh2O8aGC0Fc0NjOxb3P5g+7Yn/5xmQLFPmyLY3nMCIzc81kkev6sB0CFKOXNrEehv
u6WjhD5QDZG56tIMHrMD+1ps0BWAwqeu2AXevFR5j1q2sAXGe8AuPWaC4x9CJCNXkqplDKFcT8o/
NW+b0j5qvqsZXnEbrumkQLxwcMbuoBPT0QFCp7jPIgr3w39R8L5TwxDxZ0c73j8131sfa/QfCvib
EeX5+yDXc2qBAw0rmopAvhPMt6Y0tngxpSXJQr2LBIhoMJBpEmsMpSWOCoK2WlDG8W/dqup9B5qY
cmEPCij+5P+bUpX4YOfgtoDZ8QoaSYEd10fHCcxMMu9YLWpbjw5/tjCs67sSXkwwJUiNYdFsvban
1RihBLLPI6ff13jtCVgvm+VrW0/QySWDHkQVqKkl0vN2o04UvBXPGiOIYJIWjEGZuTCOygWuawkU
TDv9wL36nyZcvQ7+wcKpmDRS9/d44FXqPqupOPfnnRTO+41t0jX2oS/qZ7Sa7khWPKZHjXhlZS0G
m+MoSsaMuTVyM/3N5iOWAQNubr7Zrm3y0CvTBEy9HSNYgylIv3CD+MGPqTykHPEnnZoi5DKDtrn6
DJ0CJWRzysoYDILiH9ibDlwCyr3jKYPqleBciYOoGZucbHROttdwXSDFK4D4QRI/vYgvwS1MmBxz
lbgmbVcAzzNg+xkrFtVR40sOJwAVsABl8B3R+kbppACTWbcoFsfYu4yywiMbnVmQVz6FTgl3bdQa
GyZ8IGWcJ6vlGr2xl4hAHW0sCSmqM0GZTD6es00SRECxnDD4Dk2VScJb29CJ3M6EeqxGoQ4v+4IN
Bp5BO6z1/juT4MtlT/SdjlDKGdj40R4ogAad0Av43nTj+S4331r+X9+kGZI4tkwLpBQ6tN1ogNFw
spPpuWISObTgtrUcberRlFn0Ola9ON6WUQR2AR7+SqryCMfSmjLycCX7HJicRV5rsMSXR3ZFbaCC
Gk3d8FoenVGH9U2Emt1a0EESqa5roX19vS3uoCCvt0f7D3KkQzoUsGR112TAxILnQcHKA5xEp/Tz
NXFzqjHQkNO2Hj/Bbltua4Bq2zS/gQO2LqLJOXKJcU7Ny2A6k8sHJ0APosXb0CaGcagxcvKTA/bb
FKsrDEjaPZw2xTRc/KHNNvJzbrUYv0g1kw6PQv7Lw3XXsc2Q3pGv1Tppu9iolXq4tYldgs3NzLJ5
wQ2nXbVHANZx0IGeLlU+9bW3gVDrJdorbsMDdqSHnA+siWFIkYDVI1+AdY7IF/1FRVdUhVMEjuUR
NnQLTyKb+RmxjhGvJV/oUOY7vwtx7nten9sz7OJJKXWe/z6fEc6vpXbogZdp9ZHw7w684NxRbeDw
VSEdukdqwMdwzY1PWDGjvG2hn6P3FgD3tmqrjCO3AI5PDucfgTWHPPn3+p9MfepnCr/NyrnXdIjX
4YlQKgsF9bzRNhBPIvd9rQqKVW4E1eS9mgQKf1kgRgbcJY/8obQ55/psdF44JxRCxTzKkhCljAJq
rD8/+1rYRlMwh7vFc5qbldTtGxOhpkKe0M9DMCTmtQFXvm6H/s1GPBNZpxBqSvdVwgyLoANxMDP+
uYVGZO5iGihOoTRKNkWAcqa/iWxJxRlodDW180IAvezNMmE7TWhQulbt/jDW7B/wYYSZ/NYeWORy
JfHBncT8SGNByRMgNMyBKq1pohbzwDFVMfUfseovVdZnX45AsNAbt7AmxrS14XGJ4GeJHX3kvoVp
uzvqJSWYu4NuBGEPthafARpSagIt0b636By3p/ebbeMhHXFrC8zfGD0UWq5mUp1to7nncM0V52JE
Ud3UhpnDSr3z9CdOUOB0wOs4yg9LIl7omvNyeoPYDNd1osMItKxSQ2uTMHDBqwhBLr3sVK+kn9ZW
iKuL89oxUeOHQZZuw3YZ45F6j7PpQGWnhPR+g5ym8hdkS1MwaHkvPQVAwzMj6/GC8tt8IAMYCnSb
jiRG6WmY0PUUEcIZ1I0mou8snOB0iC+8kzk12OnCtKaMKyi58A4cMdLsOrqp/lVvlY20r/eJPmHz
4V7VoE6EHJ8EVCgSGgcGBF9tFleraZ64SCGwTXKdu1fXwb/nA/4ColMmzU9BrwORAl4/MKJnUXkz
FpxUygYgCwUSL5cSkRM6mSzJg7haW3MiIQ1d9i6/SHAYc9Gqqa9JxFzaIptMf9tjznojTuAy5m8B
R3valNndummhEV0lX61KozF9n6lMbvaybxKwLeAlA+01Sl3yRauYhYemNHNKkcEEk2YS3IyJSnok
AtTpxg/39yTMPVBiJs2xrqEeKuauceO2srUiznNSlPslPtM42x2iHjrAPku2hHn8Lf3mJYMG9m1S
29pKP1urHGPKzadT2pyVXWib14TxxGDswjZkv4QA4DzmAw1U6cS8C2pGYuck1GrPMSLdYrPQ5sus
UV3wErUrLyeou9RrP/yaueoaerHC+RAUJnWt48VNJS6cW6G+UfmpsldmWdZG0GS0aQWUSfiz1m+H
EZ1Fpsgaj30F4tl1hgxUJJ/hWNTd1UqQzA7z9TFaKTwPyL7bOw1Ed8ctd75sLDn3bQKqPUe+23vf
dCBHV7e/xwwaebtwsMt9Ofc+U24X9HCtn8VPHrJoJxMxfXqbm4Do3WkbtLxrxxKNgFmmZzyINotE
K7kHHPerHX9SvOBMVBktJrA3wnpad/KmFaEY8PJIBQ4T3EqaZT9koo8n59jfiW8V5pleGnNuAG61
v5My+n2mjB/yFeZFk+EZliMrQyJHF2gSHbhvK50ahXDVYHjo9yW3aiOo46lKnkFX7DHZMbiFyqfn
a0pkO7ci6X40vrav/2MX9jwcz+AoJLYk1Psf3YkhYW9KBvbCBUVhwRE6j7UK3Q4HPnqvjVdw0f10
rw9HW3YEo5+zBHS+8obuVQEEh2AhEqspMT0JQS1DrRlDy0KZ4L0/SQT335tGj5g5UcCvPj296nvM
ZZUZnmYSQ+WCUEYTco9qISrYwCom2kbGytYghLb1c4zs8t1HggZXuTtoU3UYEHKFTNhAP78hwXGr
TdGJZZbTiTJuUjxOfzL3pHa38wLaXsm73qKJfGEpCmm38Y7xpuGhGHvTHf6W907UU7o/nQpr00ry
Dyc/vEbgSAegpnePTuM34aiW/tdGnabxjLroMCsZSf2meWrtg18V363O4JN0nJuq/VC16lunszrr
87f8eqxKnnU4nmhtyM13SLatGQ5XRy6+SsAOhNYaFVANhJcpZBdngiDJBVbEv2FBDi8Os+fCI7Ip
b+875OBd/FGY3+WVIOe+hKz0zGV2Me0lGj3ROjD65yOcF2gblxSPKl8h2MtwkghzncbyKkPfhKQh
XGXS+uyBpZ4P6Bjf/JTrjSuh6B3Yn90wHWAcvpOd+WRbJl2EySnMZgjQO8ErnD8r/F5txtTHufcu
TwRTLEri0DDbStpTENL5ldnP1OpiyHRBNtGrPbYlZVNvCSafYl/RZP85DelUGt07aDLEmfpOsc1H
9e/bI9+p+gDkHQtfc8s8/q0wUxZtAuM5sqp5nctg1wKV+oDsSjqGe6ntVf7yItWnGvA51kyrwhhq
fjonr+w43Sk3tzf5BUedu2T8+Qh6V/6zF/vtPgFvj9V12Ef5+Qvn+s7YCvv7VMs53yrN6IcYocaa
llFb1QDGbpBhByIVE1FKP6jKyBdmMJEC+z8ef+YlDEq7afBX9n9HbNng15yzh676g6UyOZ8V0Xby
cJsdq7qEMUnC8Y6SBHO0OLcI48haUHW/kX4PMwFgy+/9NkSRZjRbfdm84zDOdBYAmlCNeLzbdN5/
xLWSU9CuSGQWkCXnYB+pS0C5oP9Fn9mWqYzPCMG3Bd6XuLhzfdnSlJrcZJJXxMNzyzMMvNb8v9mq
r4gxTRytw6KGgfokTdh7DXyw2iVddyKRDCZofJ629G+L6Btz9+H+h2E6npLryIQ5uJ9pZhL8/QsH
Td14s879K+KuJXbElf9LCO+dkcmBA+HTGsBFeVM+5X03yTH9oxttJ1q/pstgNrfGu2fkT/eLVqJp
6i9r4b3DQx0dG7kutQaYuBq2XpYjJQMDmm1EkYQR4bz5YcRiw+1twvBNizIrO53/Effminaij2ej
XmZlHTo55AjiYUxTUXnM+8MA9BKee5V3+/dyLrL+GvBzRDTSmcWR4U3vzO7/KX/kMULMDu8Qbfgd
n+1bdSqg/1wzgQwuPJMdealiEzwNjte6HyiE5Paq1ZMyhL+SbcJgWd11LpVfHteRO1q3Kf8WQWhS
jlZgjymWgjaOJ2jTsgAhvabsW8Bv6YUDKuLEsY8sLqNLfzSkN6f2sMDq7KfcG74lENq4LIc64NUF
HfriQEgEkzS9ZDrGMNrLO7dIoNtqNgQQZpR+60jZpzJSGZYhQA+ox3Hrezn+KVzmNULYmmBJuXG2
XusIKYD9JMwi4pNMlll0TqOX0gSD4zTPT8a+Pugm1Oa0NQntjnV8j6wMOFWLjTpcOomtx5mSPwYq
QWP5W1LK15oz684RMIPMcJUtGYoSu+gQvkIgOdAN996pWYU5XIDvmo4bbXnpKbA4YL2WInYlzVCG
A6A20NE/U10RYL5nxM4U5MPdNfCGYXjLP9DY9oTJynw+ueLefJp2Qa6U4WHQpbLTjL/4K7aRl3pn
SBO/Ij1k8k9t25BG3nq2J+cJHqqI057Z5Ix176fOAaVYMdzUGnfpg/nwu3m6MlL7wdzJgJPlfMzZ
/nRXKShdFQZlCo4f1AvddSpksF4BkIeeughjq3SwYv3iJ7DhsCBL4cC8IU132KDSOxrD5RZbmNZm
1ZZpQdfVZNUcHiOLIpIyfvPOFshdUrOm16SJQsCUb753zcgn5u5zWNfN7xgKtVwt+9OK2Nb4r0V0
hljfu8LE7xOg72HQXXpbcs7usR861PU/PKWEuKxWpG5Irt2Ng/xBBI28jVV53OBWKjat56kqYMuT
FwX7/Moelid7CSmIEUgQgRQX8pNXqrReRWkJ0h7DKWGx+rW6ycD3RIqUhNFs89pZgaHt4dDRts2c
Uq/Jq4frQ3jLLGDCyOiIgDKNOLsT7XA5Em7wddQDwIgWNUDR84/hHT0GhekH8b8zSapKXYbxNhJ9
FWJNxW6AoDiliDDgmHm+qUWjHBpZEFtlqFtOtDHmcDa97AYZXuolKnwqU/HVHBljgAEI26118xPx
BwDqqPTkKODo4G1MsnZbIuUdVpQfhBaLOMFKYYDm4y4J+Mgick7XR96mCePbsMb5wrgJCgWrWiA3
dDrlGTvwyHFlbEXzSWxnKR9Mmj19xjUx+ctIjzCNxJz8Gd60I7cRF2KZWhobmvSUY0vmmvsS0EFH
MyfH1zxY/odm5T6iKMXtJ4C/37hSOZSX6uJIJL8OV82Rp2TTPE4o+14kEJeotobmhhgObKPO/L88
Q1QySR3wxauUdCeW11U3Ys6R7Ie2p0g58yKmS9e8GEzB0/61C+LNzuGMjtvD/nQfJZYn8YkccsLR
iHaw+95SRC+85Toe9d4Zs/rMtv65/3C64ZKISM9xm4xCxiNLogWHhAF4pONOM64MCtKVssKUMLq3
f7CVV7J7GwDvzEf3PK3q1rCRvGhoEOAHSQ2AMMJhUFNO2b1BLrCrGH1+yh8JJ/bpxO23oOMoUVfL
4+BIAjtNB+4EgK+L9oJOS0TUA2e1LFSf6k9mgrzjQGmWbdc4Jez9hNopHJEZLKc0BPbCpHwACML3
HBIjIMlUjKnF8b3zlPPBVe2div4xgcBWuevngtUZX3GYM81sdo1K2NMcQleB8qdtqEwm2lIUrO6G
tMCjNx/aVP+2mat0o4mrL6doSbDoE6aWsmRgHuVR+b507b1wEbc75TJ2vZbgdenAJ+g24kqvdl/j
jSarj9pFfpNbffqumMeP3VrYyXjXSbyorDy6tUP+S0bTdhsfvsqpLDHZkdx0OBjft43GDRjyU40E
TCB5m2flPQJ/2DL0KlMDa77giHI7cc4hV9UX1bP7bA4+eQhsYf0B2ygQrvsNigT8AqjAKJJNEeYE
58eNGK5JfgbyCzoekiC2ay2nryV0yYxgD/CUbXTQ4UTPWiJXZty0ifnlZmHp1k/1clmseZ6f84ks
ABP0Vvp+tNyU+KS2r7dGrVqATshCN7hF81/QTS+X6i2yZlXRzHz7XQh95bc4sFbdQr9nt07D0Kew
9By/MsaGpzmYUc648gQogNiOAxp+oMW5ZjCKpSony1Xo/JTotdTLJq/fogJ/mh83KZCsl2JrsX5R
J+lrY8DwbWWS9bjKH1+LirtLWIPTIF52sGBWz4tMkNluiub6E+c9APDccykphAi3XKB6xV30paM3
n4gl9Ie+CK3ZKWx2dpm8ezX2qIhE63qLl264MFLZawGFwNJscSL9BCTzvm7PhqJEjN9/20REXjPR
gfwcBT4GGqGtsFuv5AhOHsAD3S3E7D6MxkocLcQCPTU/YZ5Na2ezPbbw3lwdsPWo9uopJBzQzUnV
vC/zPIJKpt+JU20JbyZnf0nbsGqlmor0YhwLjFhoft1rX68FnvHmCrLehYiD1zfsMWtDg8XKQgs1
qNY/8/ATEe5+8AcP7VDUtgAY+3bnoSlNpQ1YPbkFH2ugRJVagjagulBE1OW6c3D3GB0Kp7Qitw44
ycTLTx0LYX9qUQbEg8X1lO7QcXw6Xrc/zXeg3ne1u+KQFVbX8Xk54jaP25TaXGwI5a49OqWApzaz
phkevPf88rxlFXh7oUHAtwKancZNpuGNVqInE2ece1sX5ynmNIB4Lrn9kE/+ucTTvbI/9axN5mEB
OF+D+b4XCU5x4/OMe2MZ1QZa5UjN91ovWCfr6E/SnjoI41i9F2JslW+Zq2xJG7GRbwVGpmcdxkdL
GEclnYsRINUs+dnJGozwIxdvqpkxONhC8+m/mMqzBpIopMMd7W0fakSGub/zhTDpM+D6S4PJ+RWd
JqTvzxVfVNhJ3Xw1p7kYkEe6ZN24UsLPtqfPEFFRAWJGAM50jPmmBICAODi8qhudf5bIOh5N0FTh
prNc1J8y5D3QxQZvCfnpNtccFc6PMwRx01y5BU7vKVEPdECdqfMvs60/aT3mwOwUtRTZUNNB83dm
AJiIZMaHOm2dj83kdsJHE4yPNTr//mcY8gQyUTTXQWTWiZdblfc9tA86i2YidqovnDPQoqvQS8iP
nKDIt3K7jKFREsmS6gYLd8K3EOpp840DL/VYkHSH+B8pCzyJ4nPKp0suiZOliyxGul/VCgZndIot
p/fGOhdzMZViEJ8SgSmbx1WpGRT2FiIJEUiIcOgJer+4i5ByI59wmyuesQPNpcrFSanodXsYg4Md
rxKpaLC/+rr2B7DTO+bkPdJ+daJ57SbTs3MwCgXOrd1eS8bepgaYVrW/DOvLOLIQ13yrrnfbcGQs
TtbTgrILT5NBpxxF1KRyCZ2EqOYfuVMFjR2YVkEBQRAScizZuGOOrnYAayp0Eg7Z8UYuelHrSCR4
9u++K2bvrgMKjuvsJrMYVuwGB20qQQpUwJje37ln7fiHPfNKXDfDw40kw+KfdNL90PRKhCYrczQU
KfUi283iNKlbWZgOT6AnobWM3WL0cf9FdNHUGrfMQ2ASq4UmTFNKkAxzq2unjNAanvRUXcCHFpRL
+t7DaF5oDr/O2CDRuA4n40/yxm5HKs66BiijSmxTSHuBK7/qgSr2/YaGjM8v9jkR8MdEMRjdSho1
ck33A3DbAD/5G5m8/CJayomdsoQF+Fbzxbn9MKi71eN2vDmyyuMlemp94zsq+/SLhtKFZpurnAvC
9nRKz+wVOegdSdEPgGyIzl1p3ly2AwCc0ppKxWETWq77GcxbYeTfP3HYY2BXaf8ebpYHI8C76G3j
A26St9QYK+GWkYIaynnNP7NtMMaGwCibm7B43oGWQmLGyYv0LJ0q5KAFlax5bgYmS3nWurarGkuU
gH0ZKtXTwOcwkHyE67QFItIVFZCxOZJrY0dAx7K43lILL9V+/T9NFu1J428JloBT9a/5Alq/KFJk
7tBs4wM4EtFaH/KKbv/Y+UFSrAfnDPb5yLbz4pA4J7M7UoQmVeWt+bgM4uCh+JXsy6kjVEYPlscK
7KD8pMi74BNuUtFAIFNQ8ChH7718xZJ4jnF88h+6BAGBdWsiAkexkFdHvARSljWCoh0BxBTnhOrQ
XjEAadUL0wjHD7b8Y108m582M4vfSfxpBXOqIdk6fV6AOJMtX8nUqJMMSySK7rBit7hJ15WTPtY8
zFG8xSPGmBtbhjIkdyZApBv6ecBfdk1jkZKSwn4a8D7sUN5Tva2vL6I3TQcOKMWByfLN0Q1FZQKJ
gmG8BFhJhJNYzVvGlRPGS10cVw8rcoUUl+sN4OR08Hdf49+IRj8Ehb2ae4KCpwz2vim1/mrFa129
rEfX+ps0RER7gqj7brypc0WtXtBDQ+lKcpsYUh1GZ7ATbQpADui7pMt73fnUIPRiPkEf2HqQrnMF
TTiMKPW2tCz8DN2hNLvwOb8Q2/S33A+GfvlHc30UPSNnj+z86EEn0zfZLwy5iTk1KboabrOKn5CN
c+Zf/YakN9ayu5U8E4GddHhGlHlffw2uHrWKo9OXQRiCqCW9yQ3jCU84KXpcJzyILanadg3kD0rm
rPj98eOpmoNkOaLGQHnVW/oDvWqwh65nQV/q1uHc1htbk27W0zhMmXmPmNd2bpt300vb/tYT7LlD
flhBeDILD8SPb91wQvrd3UIJmGb3KaUKZ2wHUwrGbvmorVyHd1ZzCKm0zYwydCYA7lKEQPOs0CEl
1Y56b3QV339nwR0GMO9LylWa/R0kUhI5CCiLnTfbe2ytfcxLpIXBiidKGcrf0ejcx4J6nvmvIyoq
S0eRiUX0a0rv2tWJqopXAzKAhtQyO49et+S+R7rfN/nX4pYsnHoSDZbuOr2Xherkc6kAI1uGmEk1
hSx++o5aWzQF49IVAapVMZ34h0Joja8NImXAOsNYivSZMV9XRIr4jCtfHi4PEEUc1lefoJtM38d8
4Q5vpbZVhW6yU/7cZqPezCe68dQaS4UJWX6QRWqO2FLqQUwbnbVUnlFww98rKO9FmXBQx/Hz7s0i
0vfsR2VsyxKLK181xcKiDYr7r8uNItH/DoFSpcYI2MorDJ3uOfYC98inJ5TqHI7I/ker4JDWJnK4
L1pMsbJMYqmpmSgvjLNmyUqlhjhlgeOxT+LAwma8tTioGDDqsojVme5ZVpBVOTjpPjxEDJzpM/zU
4ZLGEajfjNoFe0Kn2VuXTaAxaGjFkoDGi5piOKU513BlrviY0Xg1AkoDHi5mNrqDM7AAwoJUL2Zp
f02h0Kn8yQqzKQcWPyUXW5oGZiFpjjry9GTOqQ5Lse18ChGceycnfraakOrn/p1XskWcEk23pDSA
r10PQT2ELSYlELcK9tbhBzNcpoUKzlv9Xb9f/6rIGWZdcipG1XYNk/V7YONxasAZ8bQa/DbP3DBa
7wA8am8dPxgPviAP0wIVhOhRph34sFFor++gtyx/zwlz9ULwtmqUouXKDLF/2SYLndllihkVwLz3
kLnwvWHytNDeE0f4GJggWQASFM9DmCmgLLw0HGzsaA4DTQKe5uW2JGU80MoLK8IxKCK3dSgUobOm
CGH0/jVTmQWq39mPUjxOK4SMiP+DEMUcG+3/sgwL3Z09HLKafOcUTxmBMTSV3WfpWqTaBF4HKSBl
EChaufwgNJEI8iVwGoh36GQFPhan4AXT0EYeFAlzIKYAInFOdA7Xu9rIuZFU8EDdZuB5DRMzdWxt
jEnuNzad/KKINLPljR8WO9Df1vilt+TyKKuJDL8g6LIeUB9VWKUwhmRaWYdlL11broP38zhqtk7r
mVA3gY82KO4oocEwKztdAGo/9WQhuWMNNhWnIkJufq7cgUuu10ZsX/VeA9J9FRzT17MUnLWhxn49
5CTRaPTnzeSIvZkqBY7oEI995tTvY7Ha+6oMRiyLbDaHrcCiAJ+CXOFL26Eay7Sm7Yd+0nBzkiVw
kG1yMg5en4A4CEQAliuP/CEiSZ7PB0cgcEu8O1xKaM0mpgFLXZDeXHpn72+WgAPREnKU9SadO1cA
ewcRmOhY09ykQalUFK0NmHr73gn8zW+vzmgwlc//85Fscpep7rufcBaqwzdccwaeQYDOtKMccmDm
ntuUTtBQIz3vTuhacdi3kbD5QadivUnyN1DQ1MjT+NLgAfxi30K6vfyhPC8iHk+Su5sl4NXp2sxI
8287jJ8ODHW8sM/2NTnKAj3N4AZ2FGXi4obhZN1xQWm1DffS9MZxXOXjnd418MnrSSdiVe6C3i1l
iYDWAjXDSXeKvQuctTUe3HJU/kI+74e7gNBcwhaxX2U/0FrjOb6VOaJ5Ico7qJsiWvv3YjaSqdYT
8lB4ToMsqSL97MKfuO5Y21bQcqz+vHs/Sozn+kWz8G9vvFT3oLOmuIO5073c453NL4bAYybyu2+e
RA5P7vF2ooM9KwCAHk2JO94k0RQwtfCyi5Vi8G14Vu8jz3GgYbYOZ0NhHVYzztSNVN3QGBC02Ohr
qCcTieDz37Io0GNajGoY0n1AwFJ6Ki+v24PEIVEI9RSU7EZn8tJhCfAR4ndcV4cKXRJWuEzb6Tb7
dnncpT1bmbKKA1zJGbxMGUh9H4/aXYSmEIzoNTSPw51nhdmy43ZElKk9oSqExSuYYWxBvf1mjSUa
mHpmceWFuToChEMGamGHV/Sq6M9Mwi0Sn+vH4xsY4ZOHqpMJV573Nsr1zKrp4YK/RoZi4qGzTmRi
5nUWRoXUUH/vyRpKVHxD5PncugDrR6AFNaG1RDFoVqPxtnlOkQQ++Q9BBCYfAAg651k7DhCuqh8k
Mn7r24uKk67tDAfh9gwJhOXx1bNuq9edBZf0CML8n92wQGJEQZ7GpLfX26On7y76WD1VpF97SofV
ffLe/+lE73LtKooFZbY5IX4jz3VMRE7sTa0xY7LD/eMiod0JSBu0ny0geBfpfwesRBGSmh/k/r9v
LWTMRCWovtweMTYqoCAl/BjX3NBsiXbfTJPMGdWn1rHTOCvlI+uVev8dbn30C4kI/GdhmQm4TOK/
xSZcqApHgTLZLqv+ox5uxtwx2KYYctSpnF9HfiXBygaP/ZdikjLmKyg0BgzE9soJroQ1nCyfK5YX
YjicwAcDhUX/YtUw2N7K6E0gttJ38HooV1hhQkPlGmyMZ5iYTWopNrStJG7zegognjZ4narqWTfq
e2gBRjAeIUvZgzmz8/BIbiPmhi3Ejryup6ykBzRvdd4p/ke6AdBev76jLGG8hL+MzhbSkrerB94x
kmWdP7aI+0+sW4bvtglCOfNDhr9fFnRs09C3eAkh9ivNiFWCvI5oxLKlo7CO2NvQQ67BnrAFXYP5
nABGXgepLewd48w/ElMs1nYBAJcfgHlxebmHfdvwNfbcd6YCOMZ4MBDy+OS/VhTkkFXjCGB+S5KD
22wmh27KJMC9wSc79NE93ItjCrv7QgrprGnl+tHSpCNgWep7YzVk7H7lv9VEh6qPU5tUcUy4nlFU
txYye39ZklBnjhnYHqSEn8LovISOD+F8O8bE2/OjUSbegl1lwHBM3ttWM58eXRrNUPXedEvG3wyd
iP2I9SShIs2fEwtOqRAox6B6h+P5Q2Jmb+LWKN+U49TU/uCFH6y7cl0AM9FRLH63lJJPBA9OHXtX
L5ljvor4bugIuqwW7+DjH72XyPGlE1htuA6pnHjoPu9VgbRw7zc31kUWO+56dSAKvCmC96gr3KO5
rg58EYisEstfnT+CnbNUUTEOyXwKIfIAb1ZNswFEjUjcieBPaZUwglQcHutvg7lrs8BKE9qqfIqX
Gtsx1ne3gMiufCLGStpVWAbaZItKKCmFsNXVvnujpeKIwcMRUxsCfQ5aIRNhkMiLoHIboQOMkkSk
s6tCjVcdcxYMGfNCnb4tUj9PWCtRkjkJH2JSPRIVvSxc1elF1JHgC14XnxN7p+qIrWeIjBFtG8zo
TdUcRhFDXiunve5JQIYajmqoi+J+/sgzw553Av2nkEw3mydAs5ZQ1mg2SjcD97YxlnDzivb0fOKj
jQWVdNrmukYg5QfORjqa0yid2Eq+99hrL4NHquaRiseg0oKUjkxT9ejUdxfffrBJ0o3prIR9Jg49
5fwe7Us6zg+/zpEtyqZt5YHZSs+BQqf6B089QGar6dnaSTskA32VT6lJnO0jHW5QLW97qRawqTwA
r8qVDyuDWJLByo/aV3XnArvDiIHFPG1A3Yxpvi/0GtyQn74BYUjLULDeTuCNrMjCEdffPP13lWsw
FqL4rN8vK0tLBIEChBqn4oAU1f8DOUv48Yi5DMjoaWaj8DjeUBjUslot0TBayx6e/dtFXJVeKysv
0hdtd5LHzG0Im0tL2EqKhh6c0jxfNX6bPHWNVKwh4fzpZLgKWXyC8x2ud6+Rfq5l5hFMpEYvZUTX
N60DK/OxQRpQmAgwvbssp3t+u96tQq3tSgT962j7sxd70VZe5XQlIRsdob5NOXLW6OGImZXa6Fda
NBYOY8zs2/mg5k1AzLIwJXcLpdp0+UBFUI18xFPlvhqCccBkyefGwRvt6ORmuRAntBoiZ53Y7mOe
AOFk1hKlli8zrQHybJnQpD8krbOdZM/W/sJyzw+xnFNHwoXYXiulWM+GXS5So6uDvO9bArZeR9Im
YV82sJwHR9LS0fyc6NiUEHAXHHkKXUwcgklNwMaTgL4p1wfOQ5feArsJsU25F6idbtPcQ9pVOQnL
rqExS9FuayJyhkzJaCXRb8mhMfo3o1rm5UxboIombCeA5kfjKfo2dA7FaJhaUYQC0gIdBAjFA5QY
mz1Rz2CcaJUJieWP2od6VBG/2TfWsWCpdQzscVMP90RZKQk1goRjZV1gXyn13ZEdTCFutNwiZxJB
enKf4pWAEihLCuCjnA8ZGWneoGpJgOFcHIejoFUOrfH/f32l5fh3HWqYPipc44PvVYEdNYe+Ywp7
e7eidQfdxW2IS8DPKRI+Fpg0DiYMcUNkvLaNDGEVUGmxwT+uH7uPZqY5fSb56wFuUhUpKyKuvuZr
zMKKgKQnlMHTY09V+7wa4KK4WD6rpr4Zm1KFTVHnqSSw0BPoyarlVLDGIJz/Vq0/k4Tea+rBe1Md
Ms3KiSWu174uDyiMFmOwG3XtYzK5lD8/sZFpxlkKFMyMKD2CI20ZQgITpm0b0e+FJvhVfsqNRDU2
47Tpc1sQUBhJ7HYC3UjTPZXa6LDiNL62rd2n2yrns7QJ5TC1IGoTWZHKvI3zxD/dgagAxZkII87g
tH/GBksLGVHSfAnIjhc7q9obTYyR1gm7ysz9N/GalYvb+9kK4WzTeh/LwmnIZS3EpERI+FwOsGpg
/oUm3+xb12qsAZoUt6nPFCjJx1oR8DdoQZ1L3ClUimfsnNuZvReMVMDoNuoPLST+4oxP/QeiA1E4
XoRBjPnWb/+oziLDo86Hh9e1qvRhv/CMpMADUbvZJyXsm3EoBwD0RC1385oO21d0ah3m4hgJTiG0
xgmVDoRjJOAWoTEQuyOxbAI65jP75jF5Hv0WK8NJc6ENMKDtQ9daVXweOrZYivo+isupXUYYalgd
CYxKY9wpfMhYK7AzbqpNiPtHlMpG3qYIVVdrwhLtSZMkE91+1fZVBvbXB3pUHrTziM+esOwirSrE
8DN5I7E7UQ7ELlVxgU0K2ZJAk4ovNk927rt5pqWVSzwblM2DX22HSuf82DeaqrKeavwgE7ta5l5O
AaO5WL8gM6z5z3BRov+x7r64qyq+u/NoJmgZpPwjcPE3uIm0WdXzf07/5lSckae+XcBwvpDxrNzn
FosnsDpAl61jrci34y47vJn+3Sy0tqGLY7vRdbihAPD3BEVoyIlApifYLj1KQmX1Q3VFNNtx4Qex
taidOmhfbBQb2CDEKFyU2kULuXzmRayQPRIUPqYXaqn83zQLaQbHgzN0U95eZqtjihoTyq7L++7Q
rZKZDM5b37Di/lTKKgx8k/bMnxUxmMvrB4qAcMWRNi9ugCQosWFRhdXXQvxqeYCA/yeNAg0UlPM/
QJdYwZT1EaA0WmVdezz35PmPk10VLZVMcz/cAyjdfcdO+tVivVqch8IbrWrdhGUIXB0cKkDkhENK
V2CK2T3QPHMjxBz6WPtbP8HrKanhlcDGFq+TMYxZbCcaWCiDws74uxJUnEyy+NQHUE6IzylZSunV
KMdxU80w2z6Ez0DCjwG/+kigkW6oOmtWDX8Zw/xYWEhbIU3bPNKUNwM1d2T5/TCMs8+RDrff8UZH
Pv1Mqmbthl7GJ4avqcSAku/CVOg7yK8vyFUiew2TKAHwzv37jWZ2ai9yEYmEYg4OwppCEj7aJWUz
jPTJGi5bjWuUSOWgAf/dqEKqnegtsKHjkDsCk4lhkSgMpY/aspZGGOD7qJlw3l112H5mxdRfVKt2
wid5IoXQAGPOzvzE0BiH9Trc/rG/cJC3nRRWMwBBbW6gb9oLD+FB7yzzyiR7o9fjI8DYz6tcjQJt
60F4nioiufqS0yhqqCs4KmAJCQ2fZwUAdmpoAXwEnUUhStoznhWx4+WLFVkImKYC6elM51JQ+3Tk
0y4SEBQFc8ro2Sgf6WE9v+w15t1QcJsTjL0zpHDY3EyRP0pizCnbQiDz1gwKKrxWGMto9JzXdstS
IbTipwgCQuUaMlawPSjDXZkJUaVKZLJGt9Q/xyV/CooWqdiBw6eh1JBarAXjNcr22eNE73hBxZPI
9FlTQtsL7j1zoeoQprw51gHDI8eAFbx7zdywYamELIl5yyqc/PBvFKic/bwekNIQSsYtm92SQiAj
yUvwnrkx/RoukR7DfOVeDIXEUoclB87jSrn52oCSEPCUl14PWFcQh3s94bGAogV3D+AvHRaOSu/I
YeDHr9+esYP6st/Dvv0r/mt/OwQH6lLSg8YcV4zQQkCIfR0E5EK3HF+CgHdlK+I5t1OFq4UdAM4P
o3BfhQJukDhM9LOioymya47zIcgGcOPlPiHKiIQAoac/IIR/n7X/A4Xk4oQlMWIdoFPmOlxMUvi0
xf+W1meJ+uGhtqxh5TOAXyUwSTVmubsYZeBMimF2XkEtvFTu333Tvpxe84n1Jz74GjL7XRPAgVcu
NDhpID6fQgxnes+gJmGvLVHEVEXcudB07b1Kxf5IgPA12NUqc9/BWKcrIymyZLiG99jQsjBP7tJF
5pbB++QhzkZrFsDojuzv6fcN2WLh09mly8LTYYCFJi4XovflX2/wlXS4fm54NUUpeRi0V25JnMzu
3s7XAAIYUEsltaaQnoj0YY+r9uiILbJGYKpNzMbNsF8bAosbmBQFbqjhsHINued37yyAuQlXzhP/
vY93IJckyHTmCafpOjq8CB2YhqJ9IXv5W5CH9SZfPAm3TDb5BK4cBAEz1CYjFHI3L7cvfZJo9jv/
jaea3cYaSeiE3TIMOYPPY/JiYOIQMEr/h5hYH8sNAedzcDqux/etc4TbGaD5zwH8+mc0cFtuLhu1
rirTneQUv0rX9SlErWodyNdEHy9qUEt0uxAU/7Lmk4I99/hoVW0t9cvQ+j8raW/ErXSTe9K9iUKH
50eq28UhnPayEvVa6+4HEnmp3QcEOGa8VeJwFUGOhROBtCHj00TWr19Fy8RbcB9yfJ8nPFx8jl7h
s056ivc77WP6Wi2UrmQcd0XlcZ1iaThBf9FxiRiWXuK3UkgnQj9aZU0vMnAu5aMlKSguNI1nndBT
AoN62zrv0ABy+ZBrQt6NupEFSgAn9UiTGOXe5rElIg7/ElfFkEAmS6zt4GH6+IwCHBUY/PIxoFWV
p9ZTv/+V1c32DaY3Pqg8qtr98BWlf9RQaS9KiPASoxyczTB6WxYTNRLb/0sGgSrNPYzoGgC86KmW
3HM8LLXLLMmcsSePX1qrQmzC0sr81tlIFYStQvj544lIFFE4TSS0gHtoK8kzkS41K2K4Ljtqeq6f
Un5XSjAhpaMzQBY2cOVPgYWtQpqxXqsU6crpCiH1f6IS0/QBFGyazKXCQ7AmyZurgSFzXZkir+As
CbSzW+Z8ANx9rkszrSEia8xUW5AXC24xiOTqwZLf8RhYhIin0UIZi5WQFr4aXoBfjS8PW6jBICe8
6THzv8+AQhjVrv+5aZDtQ6bSrSbnxwlQdnmV4hgX3p+vuBz/Bc94de0OcCrHbVUzhaZpSbA7uMF8
+hOnHnG4qFDhTjvajCLR67AeeSNxtBhce3jo5z+cKDjN7Za7MquF9RV7ZQcd+6KMQHfKqJpPUVYV
R+xUaQe5+od8pcecvVbnxorKLNl1EFvuner8YNF29GbiHMamZbmtmTP5yhDBvStf/w9alp0+76rH
YIUszg8Qjc/pziXLvx432LF1vmfjG0mIBPvwxL7fR0BeoFoFyFFYG04PFd+yAC8m+6Tet/7HcpWp
R995dz5VNLwi4gDIapZiR/LqxoTSdFzQ19pjEQis0IYSPalhdWts0ZDrDBUC3sULqvC9ndsTRNpP
DNw3goXbhtv7TPvMwoSY0gWbfWM96GCRTLGA5wbrVX4xjjoPb52RONRd1scy4/Wyc7ljQ/uTD/3s
yT53fTrDqPLRsVJdJwp+lAaNCds95O76cvsjAY7RgPltyu5zIr3HCsWJXwxMEdCHHCwsd9Tcct5h
jocBFxctAquCvZG0sLqBnovgg6K5fx6B42sKhMEvDXVXchIMwWCcznQzJJWMlt562cLacfc1wGzz
5vGuQOsImAcXm5kJBPAZAK0l1hCc+AY6UxEYof7gK7rI5g/MKw2nIGiv7UhgXVRY/A3ntbWVdSwG
4JGbiigB1QyhMmBse43XzoIiWgn8tyWRDDhMx1Vjfle3G53nWMiLZx3wjjGqI60ChY+KXeEsCWZ+
PYt3aUIQxkX5up8V5YpYpiORWCobonsrF23zYhFx3IcTbB4Cs5dnJQzevebntb9QTmVrKIZPe4AZ
30uYsQwFuij/4dx9ThvF0l68tVje/2SxUSbNe4ej7gTOacnxo1ci+b6vorf8Csz9F1hsJzX5/I97
5Hg2CTnS86426IAdsloZakV8Z9QXDSaczPaiym5Fh6XT0ZgZ8eZ+w6UCwAHylx77blGud6nST33M
MC0xaGUefWxJ/fXSA/xklRmy87qRbO+etQYAB/c2Ia+bFhhhgz3Er8wv72sXsfC+TUuSpSU7QRvg
oYAyhwGW9PQvLdMezaq2+HTWJiiJBYRyMqa+FiJIvHHqXKnbku8FwlN2efRfmT6X8Fa/OcsiGCog
BnQMBXZH+BZgPIsqKzQor7TwUxjQt4rmogG4OxV35R6PpdFNikjz3EWqM/iI/0bvzxxPRVXnNVlj
etj0z3mY9rZ2QqOUIb2V63zVZzxb+9T01RcmJiftQ+sfBua/llOBFh37lfmkij/xQEWasGLCeT23
LnKJC+SpbOkSNUSelUReOMl4+DacY2bav49spA9yqv7jZzLc6/A+682DZcvsHyLbivkK0ri3BZJB
ZQ2DoELRIIIZ0btzNXHA1p0qyeOoHVWoO2keMA8RN0fiZobM7HlAuycUfUMojRuYWqBKuE15e7Ap
jV62zrAtzkjbr6HgNiF3KFlhmuskyT8bMvE8iUDL4fhN0VMtSOBs9RcwBalbEoz79Rpi+9yDcE12
TCGKUWj16gjWfmSpVNBBveSWn8c2M3kdnid22MwKjtqoUAy+QEp2dnuTx1d1y6rZZ/JyYz1GfCO8
qZLhMWrmmQXJOH6gUWpYmm93c+Udh1Tk1YMjyJ8b8EEilGYLDUHWlyZJXSAB6XGzkdd0bPiGdIo7
f6zo1HGc+Q/yBH/3RngFm7t5ipr/lmRGdmb7iRj89IGfeZ4TzCUxNQ3HWqvBwCkL1NH3QzlDe/Fd
3C+ZqeKzF++kIOO5bxrAAsa4xFe+lvOog8GLCilt8JigsSQJeIHGQL6Szs7liRdZtwokUvQ/bFrE
On4uIYe0HXcZP4fT1SAa/JnuZ1bq1SqScBbAMEGh8bJsG8MqNSMIUVeHEN5WYiQ+1FNlMt3aCtsx
yHKyMSq1iyn0aoANlDmj6ET/zBh2NFY3MIbTKMXbHETte4llqm52KLrjC56bX2vraUJTmv4A9iWi
lxtPh65OlYm3xitA7VNLltwAe+sECHNQ27/1iVSDCCzAgYaoBx+Yeqa00XiWYcCOJwBKJes3aUlZ
Ipz8G+8X+HP5kkw352kFMM0J7MsnvhvxDqWkQRRa0hh0LcR11Bpy38wi6HECxpLhjojFXv13qpvs
qI2OqvTjsXsO4yxbyT+Ax/pSjlslnJSn3JkJgsoY76P0QT5cA4LT+mEWthkSJNmHfymLq9dZbY73
HrXzxqhEd5vC5KkR90t+emEBRlrjIV3ljdgfA7QxZGIQDqUQJ2VfhxCWHB1o097/gbfZPWyQZevs
ig0XYdF6gLhR3fAOz5yq0NUymkK0hcPzMSfa3UqT/5BGCsSWH4j8q0+gMD/gVVq6BFhgHm1IxP3H
ndiBRuCIOoT22l4hCHANZpQVJNLRnvHPelF5Gl8V8ovWlLkLUW9n20yHN2VG6eEbvnINuetsWvLY
CNdlfRZFN2wUvr2DxyPC68B5M9vNoqz9wO0awzjjPWaqnoOT8JfCdMpTDZXJwynn3vK0x1XBX+L9
wfWuDYcSWCW+X891HJ5X+MuRj9Vaez5dKEe3SLPzin+OBI9zRj+WgTSqTaP9IDyTMpaVggxwH0qp
n2f3ciJ2PPV0MIaDnC1N4fBCehYehor5gXiZUUCyXTU25uc01mG3OSvDX5fwS4kUaWFxqi98qX4O
zqbMlq3IMUeGrP25rG0E0y1WWc3yRZBPOokJU/rq7HGSCn89Clk/ObCw6vmhFIbcZ1ijz3YdET4P
QtJaia42QTsr8VjCYMjt8f/VVu3ZrYZRpP0Ykpbe4dK0Vi46OCWoZbZAvfzO2Y/KdjNbXHvFePEj
48cKm9UZ0BB+g3D4ceGbBvdyTLHZUomKeebkmzRo0A3VJdHTAWtB24G4sBv+0pmDFv57HKJ+Xunp
Aj00rjGJQNPWNRrEhvis6a7fIM5GElRYxboV0kiEMSmZH46e7MlHVu9SaH3e33I/3BOBqlyocJgr
Oknr3WjUpwieOnScQYttPiFdKCx27d8i5/HQukjvwHNPvCGDOvXErnrNCr8h0mqGnPactzGuNuLA
EdxuWPMU8gS20zmQn3BeNwbtIhpT6zhM9SoceRj5b+K2vIvNG1rcnK1yd7GcIm+bKcIuNnGVdzTM
XBQQQjzPLHNELf5Z5/pOpul17wBzBJeAI/eU0oGldKuw4iUCf8jv2LRJt4zbvOmyHhVdIBuycQiT
EJM6fCyPUrgktjJpWREfppxxZVgSEM3c0mLqesjbxntMVR78XQrtf55a4D7vR7ZhhHo78qrUyOf2
a9J81QsFh7brmcsb9jLNxfICwB4YtCJ/w2LlVrgMGyKa09ot7NlrAWuXlf2UUWGG6QZ5rgOa6yVF
hMQvNdvTAg0MghrVkIXMMEBE+tanpxnR8uiY6zF6Ji4vVMzV3wzwZFyd1sXuFdWeKFobDz090kQf
aMKUxInnBOwgDRPNjvVx+Q1h97zP/B6Kp05jZuMo3wvdoLntC3SxYBVGlyeS+Dy6nmifycSGtas7
q7N7yKEIOuWwYY9mbof1rzcr4uJdazwqpcXvrGXCb76wk40f7eM9dcml64iFnhD947wXw5jdKTyz
fwxrhnecWLirqMzkAw1f2uqnFEVpue7jISo6woG6ZTJ+VsFJ+LRueWfAev9LXeFGlzNW/yUqq1DT
qlKZFPa5sO4VPVLyjvpQkTlC7cZKEUm1jMQI177/8YXXT9XHvMDBgUZyWBe6spemOkSwNEKzw1Fi
btwPkzaNLl34kCbq9d8LN/dzHzA7iRMMsGUeAZnoJGqJ4ITVwAfUM5tabdLVlHGtBqltfVOQI0F7
7sPUU9MWfv2MxciPJwCSTtyaFe1PsmYe6k6vf0FBQj3IfNfmDAUpo9nyf+tvAgE3OJZEWQKPy4aD
XhCW4z2UBMHTbBgDEzrIo9NY17uHITIL5grK8s9MdRwqgdLfhX3VANlnyPCZvijOU3rvVwAdUP8p
EZ+hbXGfMudxOkXBQ67P9Z7SYWX2u5TvhC5JDVVJdZv7+nUBT1WOPQ6tSvj+eKjToK5zKu0/RGwH
fwziuxOID+Su9zHer5Lw3d8XsQG94Sq/ZFO2E40ZdbxSKWUs4eds/XTe+lOAurUQ2UrbxIQJN4n/
rV4rWCWjpZGW1C7vQHxPEfmoITybgR//3PXlVVSJJIh+knO2TEAIwfYG2TKf0Kil+YNxR1LZ6xtK
voit47vKPyrRg8u5uWqpx6f6vyHeZOanDssvV3GQ0+OV64AQtYmmJALwlIgDkfGJcYVUDm/6HvEm
7/yr6cPhvtWzxVjA6HYTGw4dEoOgdPt2ZWhnBtUDUUex+6rq6Kg98+mPuYNQbblwMiJQUuKEYeF+
+3wqQrxz4ZesR3WsR9MVqg5SLKFU8RgMies8ePdc4d4UhFjbO9Ck2Ove6HL/ItxlX+rRty+dnQoY
ANAWKhG3Xr6Y3Wap9Ay+mpSHFDGJXd8N4EboBOlkerMzUQ/bMC9Kqd9EKqfOSs7pvfBRXShFkG25
oBZ9fuLH+c8+VvHKNrbDc2OJUoFrl24Am3xDa80WlJ3BHm0T/hlWpAUzVX1EdOxkW429yY67PPZD
VAl1cNz0NcUtkjwV+56uO4EWiWt+TtuQGgComtWlWHMxQmFN2OY2Mt5cg4i82WWgezGEFUH2q+V4
PWRA6a5Nz/3CZQfxZSTerOX7mVyuGQRXkluAC5Cg4HbXMZtV55b4DNAwPcIMYZlnqrby1iTI318c
iAmmM0ajjH8IfJpeSjKvisBKgYkKm+ReQun7XlDv/7aYx6OxOV5Nij+NYcCsPaHqPNhFBW1Pg5Ot
MC4DfRDi7m5TThYF4SdQw5nvRie30U7FMGzhOLKywfQ/fQ5cHkhi6LSgKfHuvJ30aXL+S4KnSzjH
AE4CU7rpBqy9VjzDd6MXxr26p8g8bivA4rRhDqQjoseO8PbRPGiU4uj4PcHcqlHN5UJUfd4Tfn4p
uWR936s9hwVZGKcYSGe8ZYIJqEIYQ95bezXMelurxP4i8yBDZzBB96hhxiuyIh903pKWMT1I3vdL
VXR23uiGtLGgBtKjAsXND+EiZ2txiTYZS1T5a9gSm7y5UcJussgMgYuSO0PWIwXHALVLD5pZyXqP
WPDnzJDVbq4Ex9WZugAqNgaL8H2EUfv0EvuvuXs4XKbArTV7BDexo9w8AiYaC+wGHLEGlCs5bhcp
LyD2mBGoeglN13LVBuzCOJRvP5re2dw0lX0LaEU7brphDG0x0uHmt7fXix7U/s159hmFYbXda1mY
LfGf4IurDDItyHVfzcMEIPNjCB+A9ohGeOJNcZNTmGbdVrmQLVQAOtCMU6g3qwJvNxwXMcwUW8rc
s1963dBDnEKik8L8qCMKp653aMEg2SY1HTH36U1WHKPpgnozFEhdhaig1pbW8YqH+Ag6mb3CJrX0
qbO0FD2TbpZswttJvZM+zgwyENfnXswBBp/ZW1A//wIcA2mhtUsBSOVC4i1rWEzVgRtEfNEvC3vm
3Sv+9E7iyB4z+C6BtqtY4PjLcaWMNvEs+1osh22DF29uSDCHiZG1pBGkH/Y31OY0TsqsqgltD95L
YHV7oz5NZpnB6B8QCFMB5kZDLYgbWRyM3jfTD95gUrq0X+BB7EyS/135iP+FjEKScQ8Iqcl42+o6
as0pssMv+g41KTDJNEwAUn+g5loVtc8aXKg3fT7vKoW6GgxJ9jPN/JK+gLCeQ9lgvrgCsEkZwq1X
BRHdUQGPkeCkpSOZMb30/Sje6o6OhK5Ag1rGrysXDNo+rR+qlQ+Hm93+UrwU9xewOa5qpLpuV+/n
wg1phL+8OZTP8ox2HNto5z4A9uxtyFwgvrWrphiSFjU3WRk90HabxU/VG5KzAHiF6eVP0kix9bgn
inMMqKjuyIDw3CJeao6ljyWAd3eApj3MpUGK/0sC3DqmSaOponhAApdr+TIBfXdUvhX7fB+NoHgU
xNFFUEYuvUv8z1Zvk2nFhXBKgpF+UoLp/STC/lw0pWgKrAHLAiELszaWcr7iUSCb49RuOt6SWbto
mZubYgYjwWbYwAQ0T+nVDXr4EMR97ltyELovymBUpVa5oY52cKM1FF5xR4a8vLw8rp9eumzjTLak
uCkr5fehjk0x5zw+jtIa+8IH4PwZKtsWsyD3jySMyocD/1hoVJN3jdVSXhYNA7HwejoDwYz9G3CR
0oXlhQnycdvA4FMheINBPN5Jl7ptXJvQMx66nSOnGrNd9cgTMDk+fqWwYcMjceZ3iAsppNgFm7D5
NNP/zPIibnmTOpzxpd61U8vVAKFjkmv2kcYC8eZCQgB9MmVHTjvk7XrEMyVB98kVojmzDOL+Wf2J
S/5E8iwp0PUpsTsseeK3dt372m+Zj2Ab8m2F4RQSf8hxyIvGCQXxsNnkP448kzVrbxWSXPr5xZs2
ppHl6KxVrBfuSNcK8FvcABlKmogoE187Df4bW9rjU86nXoKu9NSDaUpqCiXAQrEt8q8HhJtjCYD/
ywjrLsOty1z6bbt34HAfkJo5lKaOaFLA41xRKhT06AgDhkxcBsxUdWmEhlmU/OXVYNRcGSs8OkJo
xgZC1o9bRqDM/AnpAh2EmC0VR7wlnb3rfNPUWFmug7itnTFJbOlYh5I3yOzews6vQo6r9TZJLpmJ
EOO1bxUBRvIBiB9zgKILnD0IReykhVYVu4/Kc/OkWQSVKi19uEwb/SChx7ymf6r18shTUlLQniVO
TkrDwW9SR4/HFjTI5REuHa7aarzHLPjo144KryjIMwbJDDvIkg44JW2ThLmwCd8c5Wr6WfIBdSDV
QcabUJb1dyZAT3wX2e+Du1fA0RpDD2JdOuWyaeJzbTl5Q1QTXEJcSIwkfLXLPDKGlm3j1k4C4Aqy
I0fr01HqFeicQC0Hk7k7nluKQ1P+oWYPW1OBFcAW2c5svxTmiw0HPQVpASBgzTUl4b/bh8nMKn/c
0sa7Amt3cCTTei1czARmBNdO3QrOm1YhgJYLrhVrySNC7teZfquVvTvDghM0VoyuqXfDCtG0H/nw
D2C3Sbly8+xBpviCZO30JG/rJdP7ORijJh+xwW7SEHt62NJFMNA08DJshce8g1nSRlqFa69I7VXH
4AoRLRodAemNgMJ4J72IG/Y6CROse7Ky//DTO4agbDxYIqZOpdWuPZiDPbo9xrjEhahzPuYqDxH4
7IJRSi3JJa1aNIi1ANh4utL53BVW9FDIuZepNSAUyj6ms3sPiwhlRfcRTjHeUtoylrqq6k42f1zg
0t3LhknEzz8KSXQ0nri6y7e4iFkgWgb383J/XuIncOZX8RUqGWs/FEOO1IzA6qeYnV2Ek4LQEfW2
Xxidfq+HTqfA/yJFdd1YLy05lydsFUfCHHvDR1pV5D9tq0PZyxamylR5auLUavKgzqNV5yARzaZ0
hys8pJK3OPkqCoLlig/13p+40ev2wDu4UJICMYTQs9YxmNrGe4vlngJ1VEE9VazUTc6ijUl57nHJ
CHhrYUr+xAsxHPs1U8b4YiC/h52fC4LMl42tOgIkuGWS3pIR5KGaq4oxPOKPDQFLMy7srYPcueiY
lC4oxUtiPUfKI6VrZiHu9oFhjHcDp0A2vFnpMWXHWlempY575le7p+xfCV9iyPO00LeePmFBTC+4
RmTe1ooBuKzup5t/8SCFIYmTBpEAPNJFeolTPxUYbfsAGu4QuMo8xNYCNrU9ZkpsH6K/sT1kYvFV
I8IYyhg6EoYkFy+SN42CDva09a7jX1wYlh9CWItY9UwAgDh5cd5efFMM+S0fwQagOFp9zKeu+iS7
AIMMPRZGDMSGF4XawN/b1PC+MiyrOt6O6qVuMEtNukc/fXGZSL7M+mdFiehgsPkMyfm3ezVwVs4m
sxP6SCMmaXYsl0uOMeJogjyrLkmOCQA2JawJgGVYa4f7CuQKSk+rVW9fsvJI6H7Di73SAyrpuQcc
bbaBzCcBFOaj5wh7whRkITxbg8+R9tDfOU8EGmlEvENG5WwknUXmkhZ3pV8aX5lOwmB4QaUAUT3G
op+Il0J1Adyz+P2NgQ8K1dFdTVfXFMuGVjUq29eT/sw8malm+QGyKcgy8FuYTE2aoYHWNyoYcGB3
UM5HoPcsGIxtEGrl7h1fU1MAQXqt/KWVQn1kJNRRdhX2gcmPsaOmY48w2Kvms+mZjWJjmOgKHAgx
G0z2Jr0Fcj3fKL0oQuqLxthHUaus3IgDAGP8QNbY5M+vSUbBOZglgn1b/zXrdcK2d+0VUxiHEDJ4
znqKya6inucPZGcmDmej1NXe03pTrOhdE8Bll+zjl17EM6qlaazdN1BLUQZamsuO9glGsc5pEluN
doBN6wt7eZLxevt0eXblpmQg0fGn6Ghq1e0NKOOfbcl9HNksT+LVgVtuYgh7vOig38FqgFHNb0Q2
kvY/vvFU3oOoqaM1A0RR0N4xaAWwvwRuCLS0WOCGoUy2opKJIHEbxNZEwIxwTH84v+6tnfwII/8o
h1zz/kxzfQxKvEYJsz7cqkDq8poIsE28zlMRwY+cHqJ46aNzHymRrjBFTdvQtjC9d7TrxVGDdk3X
L3IpsF0ypuMKu3sa8VT/SCzgPyryxk9BL6zqmFAxqo7pUJO3mY6DR8vqFHp6gG4GKzO85kMlns4E
1aEuRXov20+ss2+lKz/Gnw8W5KjmfDRYZy33TzIhTMRUkODbjnKRneYmZpBHc4gPFf5ALAaGCm7+
9bYhYF+CqOcZGZ5KRf+rULrwqvSZML7V9qyf4/7CXOcA8iG9brmGylLOjsqi4P6cydw9XNdlXbga
MTgGfPsluZRNkT6FZEoi2Y5a5cJWiSBx/SwF/i2ylef4h9n94tbgdyV9fehdmdXQPcGF3VkUVHPE
/U2z4S8Ch18dfy9j8uy7KuZ2UaNJMII+NI1t6V9U5i4Ul4dtvX/x2vQXzwSCL5OQ7cxrNqSZ5eUv
y9Uy8VOSh3kC15XxGjgFbp39KQrNT7l1VqVRvO8ABIl9gXoniA+suoh3ZU3XBumDaHUtLa3+08Hg
Z/+wH+ufy9V/3uTSOwHj7JiLSuNLKHDzIEJppwU7nYYfPaTWwvisg0+YnqlJVyuQkeEhPYLp1NwJ
5Wzva6qOsEMTb2rox6Td2gDULN/WnhNaXam/PHMpSBQYb7KQjJDIYSw4L6geZ+QFV38lK9hsmOT1
D8ZnBdlm7MiqdFEb9b48C8qlvIAhrv4n/pDJ9TR8leyJ+UK3a82NwWwJpQwS70lB0xVFy9MV5MHs
rhzop0GKtkTzp56Ef+uKXKM9MjfLEpRXOR6lyY2nB/stERPSkrTa4YYl+cSmQUbW/4kdtX3jeUfi
exGtSt+ATDv/C/Kd6/jZc5SO4J5zKUKL4Ne+OJq+q6YF7aB1E3I/JLk341x2mnngAgTj051YRrhY
LHHIuZwMnUFDypNGclNb6cBGevL3lRBty9pCu1xiIXsM8OetpVkzTbr8Pmj62hv5TeSPS3XDLfIi
t7BL/eELADdNIA12FOwTLGZKzTDmLNHfXk83JPcfQk8Xy5kLmt47xMyHQemoHnuXaqH7yD/hR/t0
1apuD7nM6LR7la9IWqu2CnmOpE/njwyWbnt8HZUb4V6P2ctYmmRfoYqHmj8NUrmSCS8iKsxrZpe6
eWoq385NoxFQufaEDK7kSd7dQC7/2L4v+mG0Q3bi2CGRDoQBtjhCh8BCikqUXEAOjc4B2PJVAUFP
2YfeNtKm9ibKOnld4kWRi+NNSgXmYGaxFwy8qbdCg7rZc5MpHj3TfI4j3sJ6vV5+jQfhbGbxsk8O
4bvDNyUhklVi75VLMIVG92p2TEtVG8TTCpIEanqvD6sg0rQTLekLy3vkWD2j0uEr5Z2zuEXLbhDy
r0li54qyrHdScElUmACfc3kgcxA3Tn+78nIJjoqd4ObYr2V+qF/6rY3JAWV2hxxYep+Rzt8ML544
hxigGK05JMY2hdeLf/SDtutcQkaImmdFNISpPWhhE69n6N2OVbRlQ2gPqily6d6F/Isikljd71ph
34VFgV1LytoiFt53V7ZiMZyCPifDaPQoRMQOeBe8Crdi41QZiBqmjNnlUyPg1pSa5x821ACicayK
p3CCLYiAK8TO98d3EHqacVvlJcw9vfZxffNKdyhjc0lJ7q3hFJ0SJnZufK1cHGqrF80gXglqiEd6
bFWfd1cNwpFTB2snlFJ4GeaQ7JO/pIcSyaqL3THRj3pLO7R6rky9RYmNdMIVZBRNSNbnV03RfSCg
TiFT1B+8lL48yeZ5h0w2uadLikqL4lKKkXrrl8mbcWKeE6RwzKlpIXDyg3PzTf6ZK1NBiZzHIKM2
rZZPI65P+hNGuvI7R4wnTwSV+eomtnfz3sww2qyEUz7ogqvD1IJjZiKfkK4sTIWuHUBKukberJJT
L9e48K3mifuBRJapEaK/TJgGdghkuCAlzebT1c4rYO7CJg0AyTXM8FvLa5ubJrClPiFZ29ZcGiBz
e3BkLNbdfe1Y77K9H6ARgCYADAXbxDBfjdhhlSI3VsBKKupaVWstGg2ryqgkieNB/5F8wtm08aBo
ARfevY6d0BzSd1qi0NjwWjirPneKLUXjpbwywtuto65uNka1BDqijt32LRDQdEeQRP+Ybo2y1/yO
861Nd9kCQHG+jR1u+Warie0CUswrnLnCwRQH5+Rf/AYz8xNwnTq7+L4nDlfv7ujIJ2odlvGXemZa
4chHC/qJF/SA97cU9JZXrSfE4c3GYN7r7okWAhWiASdWftSfENr+9brkcj6fnKNFx5K64cN67AZj
7UhrZ4iFnHUUtEZ0J5PvNf1AKMmjCL/n5sTdctNdBhoIGoAo0fkB68S7WxWiOv2IsFosuyewDrQO
PxVA5T+xa8KnI4jjYr/U+JnTKIVqUMn3KREZqqbL/qE4heCLtR/04AVWag7uS7YJcu52YWvnwoLL
+kOcOs++RC7xcy59B68ME5pvu3VVmhH9518s+dwkymmLY138e5P/E8RE5rO2QZOoVL9MsJABg//L
eSqJVXcVK/ZcEY1lnj/3Bes67+Y7td91TMT3sdn1Z1JL1I7ePAB1dZegF1tftI5PDtiCSGbS5jEj
gCGOgXLeoZG8LYdhdZS7S49ii2vMPqVXNI9FTpgRHU/caVOVXvSHLwecywv03kBUC+s1CKrBYLbX
VagEJB7z8mFqf8xziHJm8mVOJ9DHh0JLowZebKfCyTnTU3ZxPocbKio5w/Gy1uz439ugT1eJFlev
bHUVDmJwuEn5oiLACdpVvxZ5gW4Tcu4Esokkj8iIr/7nuwqIVrSL8/M/+ZKZPowmxcdgO+q65oUu
yefnJOdSDlSchVbyZE8EESFLs8EBnH7JWEm5Vr+uGpKDLNJ0488YlbOR+DIEt8g/+m8wOkiSSKMx
yeGmJ0Mf0GUpZRNnsMjUenyJdzLlE+1DnawC/rBfJPDYWrD2Jt+AFtV+PTDSnJ/1bABxX/ANOQsp
Q9y1gOqbsfwCMOr0LhhpGyb3EUutBtcTPokJMK5rnLXT1J6LnGf9Rccny6e0c7h8nzbjVljywKil
H6ai2yv/fQJM/7QutvSPvxOLPf7nJn9o4O2GQLFhZQseiDk0BrnXF5+yn+D8GhH1xL0ifzfP6dvH
vEp6kE2gWKS11OwvJtzRMJIv8Q/cdZ1kgNGae006X3gY4g3PL7WVLHYNV55REZtqYdaiWW3Kbfzv
JP4a1Di7SvKjQ628+ZO4lt8GwGeokMiW7K1HU+Od/4rXJmopVRN50A5O2LsSUefuLhKfppsAIlq4
Ku0BhDUI24b/jgxJ2weJEWbRoye8BeGGo4J/m5QELee6XJChZNHaS3VJ43j9k7pgnQLtJJg45kkT
sUZ6qzTt1xsNqTxw+OS+301i3aQaK0CFzJN6rwfbpXFvBEXubOS1rwuD4T2dIVezmQkfgT5geB1B
7EtkMtb1VvCtRnrlbS8jj10ibF2ESeuRq0lADQruTa2at0bPQ1AGOKTgDRP6Ze6KwdsmRAR2h3U3
fnoKCbHjqoIWIg2qdRGN6TnfbPVW34AGIHj/+EugrsJzOdvJcSJ4FtIV1v/+tjRub9/Q0MsrYKX1
sgWq9hMEELyeZVkHwp7OvD6kv0MR1r5JbUUpawyF7Z4YbYj8eIEZs/ewEn1H3qD2
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_GTWIZARD_init is
  port (
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_1 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_2 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    mmcm_reset : out STD_LOGIC;
    data_in : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gtxe2_i_4 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_5 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_7 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_8 : in STD_LOGIC;
    gtxe2_i_9 : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    data_out : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_GTWIZARD_init;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_GTWIZARD_init is
  signal data0 : STD_LOGIC_VECTOR ( 13 downto 1 );
  signal gt0_cpllrefclklost_i : STD_LOGIC;
  signal gt0_cpllreset_t : STD_LOGIC;
  signal gt0_gtrxreset_in1_out : STD_LOGIC;
  signal gt0_gttxreset_in0_out : STD_LOGIC;
  signal gt0_rx_cdrlock_counter : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal \gt0_rx_cdrlock_counter0_carry__0_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__0_n_1\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__0_n_2\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__0_n_3\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_1\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_2\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_3\ : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_0 : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_1 : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_2 : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_3 : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[0]_i_2_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[13]_i_2_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[13]_i_3_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[13]_i_4_n_0\ : STD_LOGIC;
  signal gt0_rx_cdrlock_counter_0 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal gt0_rx_cdrlocked_i_1_n_0 : STD_LOGIC;
  signal gt0_rx_cdrlocked_reg_n_0 : STD_LOGIC;
  signal gt0_rxuserrdy_t : STD_LOGIC;
  signal gt0_txuserrdy_t : STD_LOGIC;
  signal gtwizard_i_n_0 : STD_LOGIC;
  signal gtwizard_i_n_5 : STD_LOGIC;
  signal gtwizard_i_n_7 : STD_LOGIC;
  signal \NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of gt0_rx_cdrlock_counter0_carry : label is 35;
  attribute ADDER_THRESHOLD of \gt0_rx_cdrlock_counter0_carry__0\ : label is 35;
  attribute ADDER_THRESHOLD of \gt0_rx_cdrlock_counter0_carry__1\ : label is 35;
  attribute ADDER_THRESHOLD of \gt0_rx_cdrlock_counter0_carry__2\ : label is 35;
begin
gt0_rx_cdrlock_counter0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => gt0_rx_cdrlock_counter0_carry_n_0,
      CO(2) => gt0_rx_cdrlock_counter0_carry_n_1,
      CO(1) => gt0_rx_cdrlock_counter0_carry_n_2,
      CO(0) => gt0_rx_cdrlock_counter0_carry_n_3,
      CYINIT => gt0_rx_cdrlock_counter(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(4 downto 1),
      S(3 downto 0) => gt0_rx_cdrlock_counter(4 downto 1)
    );
\gt0_rx_cdrlock_counter0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => gt0_rx_cdrlock_counter0_carry_n_0,
      CO(3) => \gt0_rx_cdrlock_counter0_carry__0_n_0\,
      CO(2) => \gt0_rx_cdrlock_counter0_carry__0_n_1\,
      CO(1) => \gt0_rx_cdrlock_counter0_carry__0_n_2\,
      CO(0) => \gt0_rx_cdrlock_counter0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(8 downto 5),
      S(3 downto 0) => gt0_rx_cdrlock_counter(8 downto 5)
    );
\gt0_rx_cdrlock_counter0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \gt0_rx_cdrlock_counter0_carry__0_n_0\,
      CO(3) => \gt0_rx_cdrlock_counter0_carry__1_n_0\,
      CO(2) => \gt0_rx_cdrlock_counter0_carry__1_n_1\,
      CO(1) => \gt0_rx_cdrlock_counter0_carry__1_n_2\,
      CO(0) => \gt0_rx_cdrlock_counter0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(12 downto 9),
      S(3 downto 0) => gt0_rx_cdrlock_counter(12 downto 9)
    );
\gt0_rx_cdrlock_counter0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \gt0_rx_cdrlock_counter0_carry__1_n_0\,
      CO(3 downto 0) => \NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED\(3 downto 1),
      O(0) => data0(13),
      S(3 downto 1) => B"000",
      S(0) => gt0_rx_cdrlock_counter(13)
    );
\gt0_rx_cdrlock_counter[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[0]_i_2_n_0\,
      I1 => gt0_rx_cdrlock_counter(0),
      O => gt0_rx_cdrlock_counter_0(0)
    );
\gt0_rx_cdrlock_counter[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFB"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I1 => gt0_rx_cdrlock_counter(4),
      I2 => gt0_rx_cdrlock_counter(5),
      I3 => gt0_rx_cdrlock_counter(7),
      I4 => gt0_rx_cdrlock_counter(6),
      I5 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      O => \gt0_rx_cdrlock_counter[0]_i_2_n_0\
    );
\gt0_rx_cdrlock_counter[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(10),
      O => gt0_rx_cdrlock_counter_0(10)
    );
\gt0_rx_cdrlock_counter[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(11),
      O => gt0_rx_cdrlock_counter_0(11)
    );
\gt0_rx_cdrlock_counter[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(12),
      O => gt0_rx_cdrlock_counter_0(12)
    );
\gt0_rx_cdrlock_counter[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(13),
      O => gt0_rx_cdrlock_counter_0(13)
    );
\gt0_rx_cdrlock_counter[13]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEF"
    )
        port map (
      I0 => gt0_rx_cdrlock_counter(1),
      I1 => gt0_rx_cdrlock_counter(12),
      I2 => gt0_rx_cdrlock_counter(13),
      I3 => gt0_rx_cdrlock_counter(3),
      I4 => gt0_rx_cdrlock_counter(2),
      O => \gt0_rx_cdrlock_counter[13]_i_2_n_0\
    );
\gt0_rx_cdrlock_counter[13]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => gt0_rx_cdrlock_counter(4),
      I1 => gt0_rx_cdrlock_counter(5),
      I2 => gt0_rx_cdrlock_counter(7),
      I3 => gt0_rx_cdrlock_counter(6),
      O => \gt0_rx_cdrlock_counter[13]_i_3_n_0\
    );
\gt0_rx_cdrlock_counter[13]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => gt0_rx_cdrlock_counter(9),
      I1 => gt0_rx_cdrlock_counter(8),
      I2 => gt0_rx_cdrlock_counter(10),
      I3 => gt0_rx_cdrlock_counter(11),
      O => \gt0_rx_cdrlock_counter[13]_i_4_n_0\
    );
\gt0_rx_cdrlock_counter[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(1),
      O => gt0_rx_cdrlock_counter_0(1)
    );
\gt0_rx_cdrlock_counter[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(2),
      O => gt0_rx_cdrlock_counter_0(2)
    );
\gt0_rx_cdrlock_counter[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(3),
      O => gt0_rx_cdrlock_counter_0(3)
    );
\gt0_rx_cdrlock_counter[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(4),
      O => gt0_rx_cdrlock_counter_0(4)
    );
\gt0_rx_cdrlock_counter[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(5),
      O => gt0_rx_cdrlock_counter_0(5)
    );
\gt0_rx_cdrlock_counter[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(6),
      O => gt0_rx_cdrlock_counter_0(6)
    );
\gt0_rx_cdrlock_counter[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(7),
      O => gt0_rx_cdrlock_counter_0(7)
    );
\gt0_rx_cdrlock_counter[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(8),
      O => gt0_rx_cdrlock_counter_0(8)
    );
\gt0_rx_cdrlock_counter[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(9),
      O => gt0_rx_cdrlock_counter_0(9)
    );
\gt0_rx_cdrlock_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(0),
      Q => gt0_rx_cdrlock_counter(0),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(10),
      Q => gt0_rx_cdrlock_counter(10),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(11),
      Q => gt0_rx_cdrlock_counter(11),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(12),
      Q => gt0_rx_cdrlock_counter(12),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(13),
      Q => gt0_rx_cdrlock_counter(13),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(1),
      Q => gt0_rx_cdrlock_counter(1),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(2),
      Q => gt0_rx_cdrlock_counter(2),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(3),
      Q => gt0_rx_cdrlock_counter(3),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(4),
      Q => gt0_rx_cdrlock_counter(4),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(5),
      Q => gt0_rx_cdrlock_counter(5),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(6),
      Q => gt0_rx_cdrlock_counter(6),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(7),
      Q => gt0_rx_cdrlock_counter(7),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(8),
      Q => gt0_rx_cdrlock_counter(8),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(9),
      Q => gt0_rx_cdrlock_counter(9),
      R => gt0_gtrxreset_in1_out
    );
gt0_rx_cdrlocked_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => gt0_rx_cdrlocked_reg_n_0,
      O => gt0_rx_cdrlocked_i_1_n_0
    );
gt0_rx_cdrlocked_reg: unisim.vcomponents.FDRE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlocked_i_1_n_0,
      Q => gt0_rx_cdrlocked_reg_n_0,
      R => gt0_gtrxreset_in1_out
    );
gt0_rxresetfsm_i: entity work.gig_ethernet_pcs_pma_0_RX_STARTUP_FSM
     port map (
      \FSM_sequential_rx_state_reg[0]_0\ => gt0_rx_cdrlocked_reg_n_0,
      SR(0) => gt0_gtrxreset_in1_out,
      data_in => rx_fsm_reset_done_int_reg,
      data_out => data_out,
      data_sync_reg1 => gtwizard_i_n_5,
      data_sync_reg1_0 => data_sync_reg1,
      data_sync_reg1_1 => gtwizard_i_n_0,
      data_sync_reg6 => gtxe2_i_4,
      gt0_rxuserrdy_t => gt0_rxuserrdy_t,
      gtxe2_i => gtxe2_i_8,
      independent_clock_bufg => independent_clock_bufg,
      \out\(0) => \out\(0)
    );
gt0_txresetfsm_i: entity work.gig_ethernet_pcs_pma_0_TX_STARTUP_FSM
     port map (
      data_in => data_in,
      data_sync_reg1 => gtxe2_i_4,
      data_sync_reg1_0 => gtwizard_i_n_7,
      data_sync_reg1_1 => data_sync_reg1,
      data_sync_reg1_2 => gtwizard_i_n_0,
      gt0_cpllrefclklost_i => gt0_cpllrefclklost_i,
      gt0_cpllreset_t => gt0_cpllreset_t,
      gt0_gttxreset_in0_out => gt0_gttxreset_in0_out,
      gt0_txuserrdy_t => gt0_txuserrdy_t,
      gtxe2_i => gtxe2_i_9,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0)
    );
gtwizard_i: entity work.gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt
     port map (
      D(1 downto 0) => D(1 downto 0),
      Q(15 downto 0) => Q(15 downto 0),
      RXBUFSTATUS(0) => RXBUFSTATUS(0),
      RXPD(0) => RXPD(0),
      SR(0) => gt0_gtrxreset_in1_out,
      TXBUFSTATUS(0) => TXBUFSTATUS(0),
      TXPD(0) => TXPD(0),
      gt0_cpllrefclklost_i => gt0_cpllrefclklost_i,
      gt0_cpllreset_t => gt0_cpllreset_t,
      gt0_gttxreset_in0_out => gt0_gttxreset_in0_out,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gt0_rxuserrdy_t => gt0_rxuserrdy_t,
      gt0_txuserrdy_t => gt0_txuserrdy_t,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i => gtwizard_i_n_0,
      gtxe2_i_0 => gtwizard_i_n_5,
      gtxe2_i_1 => gtwizard_i_n_7,
      gtxe2_i_10(1 downto 0) => gtxe2_i_7(1 downto 0),
      gtxe2_i_2(15 downto 0) => gtxe2_i(15 downto 0),
      gtxe2_i_3(1 downto 0) => gtxe2_i_0(1 downto 0),
      gtxe2_i_4(1 downto 0) => gtxe2_i_1(1 downto 0),
      gtxe2_i_5(1 downto 0) => gtxe2_i_2(1 downto 0),
      gtxe2_i_6(1 downto 0) => gtxe2_i_3(1 downto 0),
      gtxe2_i_7 => gtxe2_i_4,
      gtxe2_i_8(1 downto 0) => gtxe2_i_5(1 downto 0),
      gtxe2_i_9(1 downto 0) => gtxe2_i_6(1 downto 0),
      independent_clock_bufg => independent_clock_bufg,
      reset => reset,
      reset_out => reset_out,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2024.1.1"
`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
GNlNeNP2E6l/J7IS2LF/ItWIQpPIQxmqiENbpRcUcYDs2zMQssAZwDKWmGxc99etc2e+6UESUZFU
1OoN93QWwGY5lwtShrTc9cE66KsLO3U82lRN16EHmi+RZOdYR9rf5z7OgjqfdL/T03/rfwU37+Qw
8V3wDE2i5hqr8BCHEwM=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
afRfkAd8cICiTJYylAj89MfRuyfOa9d/tbr4mvbd5mWPw07cLRhiyi8gIe4+Ayw0CiOk7/EjPgWg
Tg/1hR99BWRw4VbbDTWu69lZfP8CCWoBxtdt8WRuUGz7PLk6qQMaoDf1ogw0xyXTK96u8N6bef3z
7mR3YInM0ECnk0+8vQDLRo6ETSTf4oQYcckMpPfbPQ+NnHRvcHroYdddFGNYasbeUqwn7tkHnYO+
ADUKING4g3DhZO3UAIPZJggQnH3ayID9nf49Oa7bj//jtWD0qELIGFMQn48spke+RcfDkdFLHWhY
0esV8oRsRwIWfwXwqK6i7aIis3paozzKSWbiNg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
ZI9ECw+Xpdoykt5zjVXLh0QGbCnqQDaDo8xSAWsYltuhqetS6+j38MNsVABP/vySCUkxp7yLlCMg
506S40Li7Nmy7qD4YvDLHMe+fIgwi3Hb193aoDluxnxdVk/aJ7VOJWXd88+scqfUSpbbFv7rk9La
8woMpCSiDNVKi6NB7Qs=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
R1h0p6UTerhva5Yb3duDj5nR3CyyGXjeE4dSD+H9PUwvK6SElm7FlL9AjlrnVaE2OmUXbsHu+WYi
T1XMnV1ekw5xTRICAudvAhAJrZYMHEjujHOfgXPmKtXHivF59pFlTxnEtD+dnchOL4x/jMm/lCgH
k3/wzvZbfOaxaKFceauCooIRZkqZa5FA3KG53cEucbspEldbnPaAcHsiO04PEwklvjo+2XAiGTHB
YB9XEkRUOprwSG+TeOWFNvz4Yu2FYHffCsobvgsF/8dSJQ1SVv3rM1uHf3coB/s7NIgZ0ye03NO/
wLFRL+3OuPxWNTqHO9YYXL0XTd1rPaaWB/mCcw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
4l+Avk44zzaOVxRJDzFVTXMyJmyE6qVo8wAiHogr6M/NkYt3AgnEITBHDJ9dF4SVZQLiV7ZY6shr
asWzrbISsiBCAaZgyPZLr1zlBErx08H5Enf1NkLuocQa0x2Dc1DwG+mO+flli1++tKpmHM6ADCS0
3/8fREk+ga01VkXbOXY6B7Wt4G2O3LBdFCkEvd0/MEx5uYrJNrtpaeSd3h2CpYLsfPsxxbAzNJN1
3pvNKoS8QwUnTPy7oe/k8bFD0TOzZmLvT/dy3x7SFlB6oSYr7FtWMOOuH5B08Upu75qdujq2/6ox
I9BucH2FnuLsghHnPuWfjh4WTDPekeCy51G/7w==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2023_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
r1TCY1FBxgJgPdlaGrkCY3ZPZ26hJnsdsOKnUu75PgvjzuIkty6RkwizEzV4coW2cxRBcWf87Rmb
xbDdy2cDu4f2qP6QUVBsGtY+ITHBIqBPqY7IqmHvnmT8DQ7mta5QD9HpSpd8E0HAu+1hmp2y9R2T
j6cEj7KNzaIGnuiJLket69e1dx31L14fHppyx7EJTOKaIYVHOTGcZeHi6ozbitA95N9bUNiSCS93
sJcjWWTWEB9Cx8arBeHBMIflTsCrUzgwd6f7Lb59R/pt8Zgvgr/BoV20ZGDDy6BCwEWgGSw6GDBL
o5ppIN+dICJOxCzX0Ij4ziKkTNPa1OnlJpcXcA==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
U0o+hd53NUTkWW/pm++iDbvu9EzyJbrE8cgDdW35QL1go6ppv1cnjM7W9LNAUoYpay5lYpZFkUZP
qgnoVBG2BG64sAHeAjjMVH8aUuTYzhhM3n6pvSqx5Ez8rezjhdOlMLX17yWFSCq3x15rfV2si91N
p+FFtjXOD3BmuNjRLXeeRjrvt6sxfN9IwN0yEZYhwp2ukPLeIyrYpRXZ+xMErI/b7BKLZNIljMSV
Cx5wPeURNjIC7AKZOJmo8YQe2JGJEHps80MGm98CsBtbIXDa7uAxib3WWstVs/UbQGgqjiyNIOSu
11VEYQSI8nO9bNfJPsLpVaeo9V+FUKdFcBgKbA==

`protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`protect key_block
JlAVtvWu4RylemkeXAwQaOcauzsF85mjSoJqBa5/i7p1cOK0PM2Rfou44syNOWyUG43Mq5DdY2P2
hSdJ0BrVnIF/JY67xayAWorKfMTlvfZN3zVnvvFrLORWpe5MwPzwHomEMtQgK/WjTmMnFJzWxYti
gedoe+hxxEoZILnHN3ge9A2vnRikwcJETpOKLK13mQa1Td8zo9tsQuagX91Bw+AWLzMrHGR2bmQr
grI95HpSioNMY5CjomezDLSd80Rlkr+6SiaUQt3pqExFVEAsKNJdtppxNwugqojHWEqm1lF0wKuj
aLs/KfYF2Uv5wNjccSP2gHLWWaF5DKs5G5u4c4FjEiS/slCZgrt5Ymwc6XKQHNOHbPRsQcbbkPMD
V/YuoNzw1R2386n0pOd+vMD9nTtpB104VrwBNBxlGPPa0qiFM4+xr58vMzHKNT91irPu/EirEIjt
wEtLynnUeSKMKz3aAvXZPRYeEurD0fgZ6iczWVNZOsltSIQZjOp5ErOC

`protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GeMop+4rJGQJTsAts4IyQAkQMeKt+Fa026naAEt7J5/8nhAvWlIS4YQDNOLHMMUe8p+sUA4yE6+s
hf5STrRcmXzM45O/bYrkBzWHNqGcsEuAHfdUd9ANbBZmpncQ5ywZbXLX0oI4xVx55B8sn2gAj+p2
NQEPjYWPZXJNML4IilPsrW6TlgTi1zhp0JFBFT3O6yHY35dAseHXtMWYgjr0+6WBtVCCvC/z4bA0
UY58Skgytan1bUM557AlikG2LqNB6YHVUQihnx4ZProlAL61WY/j4joeXcDH8Tpm0cmYcUM5yUax
haoFS6FezWAqD4KpiUS3xpkUVET2QpN0NnxTvw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
1x9gXtJFU2nGQ9CWRMPqjLG5B7nyL7ZKhgWVKPrQTraMot+PY1MLqNEQ8371fSgOtWrUw7wBswaL
CBsGdy0dDZt13psueOntqYT0wcW3vUbYzfkMCsSjkk2yeYjnCYclwJKMNk3fGxfxpTLQsScOFING
ZyUHBaE+KeZ7yfdqvxwcpP+rUXdNX/2BGvcr+a0ow/nFsiCiOMdYIlAgllnBFoUV7lDr001O00Hk
1MDqVCRdpiO5q5FnggoIp1sVIq1C+JbqiuyPkv5jmlk3iIfZiHhSSYoxP3DJf7VPvlJ1Z/Hwz6Dw
/39nFCTotl6l9hqiLHTmISLBvSYZnjDEcOPtPA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
mxE8AqszO8jEcufcEIxsyswviid2I+vp0wiBEslkbc/wVr2vlpOaIaviDrRuTwIVx162O0AViWa2
3/lH2PLY1lcjenGETw7njsuJd6MQv4KihzbHhd1HB0TKpoL1n4E9u5PgNIGghcgNu7s2hE5rpffM
FAxMXxyhZQ3j4WBKTgRtbqcexvVSl/whSo87Oa/V48wwF7Z68uoZdaD4cdNihYwHMLOm0LYqYesQ
cScpoz43IdSFiQkBEM4YXuqn7BFIsJ7UAr+EkHAFxWYqt0IJ3+/OCuOYW+y/3p2tXT2YSGnEsOzZ
SLIsZpsw7wAiEfu9tVZJlTQ0OUTZqfQXK68i9A==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 17776)
`protect data_block
1vFLS+NMWNyemPrhbTFP8+PW98y0nDIEylx/Qz3RULeeHomACXQRi1s9sjigdJok9/6AwtnE3VR4
6nG6vEp/pnkQBmF3859g2n4j1+eDu3MUBJRxcFJSqAvRzdnRXe9WsokzB5XXEGfp/EoDbWDB0KH6
sO1bAaSQkVm1zMyL3L2Z1mjk7F8sts/Pe111sZchEAinHZadOJ9Kk9HF21dnVEW3IX1H/awOPgfH
fXVXl59wQw/F5HxHmiNEs8VHlIra55a991dKNggtd+iS5qUc0wO6c4XWJSWfWvV+KwIvlOTWRFnq
W9bBkUG0OXPlcjUSFclQQ1xSN0UegNCpNJW6kvh8AzekWoPz4bJN4G3xZdSlc0oFh21W9CsssEqB
zNYQrsFM+8swW7DNayT1+ZZZCo3CW0CnmGuM8ECOQitC3d6eUZ3jUcV5HSt786hGW4djF8OmNIJE
2mp+rvFPnYj3/rkOLr6UjO4//JQxr96XFUp3qTzCiN84hG2MQ2Vqc5lY/Pac4vaTwLLj+Da2cDMp
B5b5iyO7wFxBMfEB/DOAqfoEgQUVkcfzuMyl7KDkNcyLr8AhV7jIgKBlihflp3OudQKdSkSUt8fV
WanzTzA9fgESU0ncKJ8u1SQ1kLnIunh6ywPEcel7SeyqLXMQdsWr8391aIi5g7/ZUeKxpT4P315k
/T88+V1UUQaiwx3ahUurP+5p8gwRBmzPR6r5lfSc057F2k72d6zj2PFNqpOYoVJo+tkDPnZp/Jzw
H1WPysH1qkYb3SGDJZmmvPsfbGAJvTo9jh/KiXUx/+jrNPGCu7bbfM9Jt7F/Vk6FZZmGcoQHgoYX
RgYYZUonBYkWo7k6jS9h0rNPGeRCsukcDbVh0q51yNIG/KS3D+HLb6Wg7rytaoy5SmdwwwaiDqgq
Ovow/5eZj5qJodaybtIcB5vBaMz+keT0pTUi4CMcsqa+GQAHV3hm6cvJiPo4tN8UaE99epntuBWu
OyADiB4sS8WPoJR5VrTQrfkeaj7NPixRAJ/c8ezPPMWzqeTLW9xOibHhGCm3PrZjBXxpmdEaJ34d
hvlHBLnbEOQGVfzXTTu7eHLwpl7Y5mhaLWODPhgBG29KA0i6nbQ6oaIBAvFIdxtGklYoEpavOjXs
oKmBHY93IP2z364H02Lv1WSAbtXvS8sDZOi+FbcKCxSFrnqr0ufGY8o1d0AEs4v8CUjKNfycFh4U
X/Wg5/8zWAXUTOJa28XKm60OppefljShU0cEgPbPjNgwWs7/PDt59Fz0g7e9WFJ7erl1fOpqdK/u
bxcUEZEpYJ8Gc7mj9AhwY/vIHBe2jjZIza88cxYf5tY0bVrmZE3F1KzxuINLdfSoLEJsGVdtgfbP
Q8W0uOmiH4Q15+YU/7TGQIvHtpH2/yPUb6+BVQw/8Oa/ZOXFBthpECsYjTAw6AnxkcbQx7eAojfq
UXLuRjYSKPXI+p2/vIMxd6dlPXiz/T5KnXkFskt4ZbqdS6birJ0zaMBEvqoD3LCiSTti65AhlgjE
hwZXK4ElYAosSWy6MuBuOWVVYLCOd5BKtr7Onhmuwj5Z50QdmRoTpsydjfZSRS8E46Tkwhg5hfx0
FOv/fOVFtaJ3z/T3r9/Z6m6pfVR9mqnokZH2cQsI4kbcg31vFt77PCOXRYo3NL8XazE3gyTLZE+e
u6/yjf3MyjSSKnZT1lB7HxngGPwyLsIRLCf+lVivFuNJqmOCc5TBOqLW6ScaYLJ5FxsmngNA4pMV
5IKE3Y6MVLlwG/FAdHOaDpnDlNAegyEYSaYPmqoa7465kdFh5FGsXd1BBe9DhKxjeqP2dhXPK//I
nXKIiVz66usV11twtHQ6sfDwjgHsvFXLylSeoW7ERUQJqvad5ZluidHhiC2sl3N1DyRazb3hB0G0
Lk2h3gmUpdpYNFcqeel+9qbmjBG6ijJeBTWq2a0f4AhZCX+rXXhWfT5gIrbR8SYE5u6yyTecgNfe
jaVIri213d1dR8/SPX1dqp3CVj1lTM5k7x7OnlG9FUf6bJYYTgfvld+prlE/ZTdY1raFOZZqLOl9
Ye6JqBheCJ35lHSXTV3iCZeibzYKAFK4Vw/Sl7fQS1RZjY08quQeyCRs2zoDfhaun2zrLyb9RdaE
jyWmzOudG+a2h56BzL1Zn50i9s4IhhKM08iQX9cOIx0YSjzJCACOp3XA+u3WdwVnBKxZy8Dfq5iS
diiID1+jGlj9D5YautsarGZqJnpzYcicey89/2Ujbmf8iyDq+xXtL2Tt55zgpT3kREc4olYgGbDX
CoGo5PtUFN85UgIzXcCStb/Jlc9noiG55xJu9ZmdanwJ5zIBJd4fx2Rrau5lCA2wYvK7+76p44Tt
4nxqqxxuZqCt4DExQTj1vA7VEUiQ5mCNw1yFggtt2e1m4XoL1a8+pOZrpz022pWpsOQmBJSei9eC
j+m+Yl3X62nceW/Ke8c/p+tBtm8UmVCiUU0Hn/aFNTFispOMLNdJ8Uyz8xBKBt7MGDEeHiqzMaSN
zo+t36ZAu6yRNtaUkw56NS/ANK76hN/TCwLpY5wKS4MnHfLBHjkDul4gOoRY2EAq7w0nPCDpgyWg
VIZj1GtVrL2pqViWRjM3+GCsPLe0TOuU1SPZcBqziLvVFj/r9kWa9XvzuyyEakSI8hzddMKHlPbz
WwcxacBvCBSntjRD8JhwNQtfBzBXnoA/5Dzff7I8nGJd8t599kej38Rfc9bLIWmX2soB9gKPWjZ4
QcJTuZWDYb0T7nFAhV0R/j/W0FUZAmW9iu13FW5mmI8Yw43p4K6K6R/gMw7NyQdD/VrI/zs16ZqH
JGPKF6fEgvsHJNrrtkkGgjzwRWsSFcWKQwnDs6G6ElL0d3l86RD31aZSI9E1r+Qnq1X51maf0iw+
bL07nlVjm6+5TtaXCq1t+va0xCB5JzYGFnCX33zotCm1QIUIWRs+Ep89NtzfmyICGluBUxkLmwTH
xHoLnSJlNk8g2EzGlkllggE4VrP/fIkhYwAjSMX1CeoTF2t/C8FCVE37DDA62NhbImlef659vccV
YcAqD8eDRkDTdtUNS0vrgxygOEAQWS07ep5ioyHZgrtrPpBgVwqITCpSHFrXVKreVpSuE5eBuWYa
nXGqyKErDp3z551dHwQA2ZnQ+JH++bjggfK9dsrztv+gYgYj59ycoxFki/qzLHRHNLqRy5kHfBm0
MKVbgagHSHZ4w4yua09vqrNlowJCLCISFIPOkOywlxgqS2nD6RCnsPQ/uxKZdrgPbTdQo5dC58cB
QCgS+BkHP2TkLmPArVscgVBhjTyQDIuZfR5DJjEKJJnVuhGJhIcghUjOWpiTVtAC60bfbN76acfJ
2QT/8IfIoow8VVNqqnPPWEyRxIRsDHGgxqyuLcmhdmZhxDyitLztAAAdGNu6UE6Jv+XX52kTW77v
M6UihIrZgzlkHVjZMPG405NgoqGNKW/MJW68pxp3CS7XZTe8/BOADp3fOFZlxppDKKX4kjzRowMx
rq1Hsq4mCQ8sigxUPgMiJxtkKMNo48LLl/TpZh0h3BDSTSUeiQf1u9HCZ/OgiFiC1ngl2iD8P9M0
pCTG7qNCkPFZBgUWPIVpOpBGNVjzhqi+Z5yginfOFg9w4YNrVSeLyBJ+hKa/OlDlTnyncDsOKzXH
WFkshuvwqnbUiploC6SEumIoUREdIAARxWf7cQRGQcpI3sUZVjT8foelNQ7+YcQr30AEVej3bDvu
iDPmaMT8kVJZxMqywwABPPMkXotzakwoScETbumgOi+oUg/smdryR3osf7XCICnJk1QwjSI+Mpo1
+mge9FUs/ts2zGSJnf2TzmSugxcpvbptu/wMwNfM9UGzTCjMv4gWmKZg2PBtzlDX0JjJWlklZ4HW
fshYu/jZLG7FAh/myxF+AP18yd1Y/qTIutqGWMOt9y3urwnXpdbfHsVUgzBv/lxAD0mYvbV6EcQb
DuxIxPb4+joc0sYXT4HVERqFJchphjDPcd1+E/Z6MKjJVHgVL7zTybU8PkPKsQWAtUYLDJtboB+9
zko4INGJ5kRlS6FUxmKcKjI12dwWCVnL6o7uzCS0T4Zgy99RLeWbunN+IY0WkYF15bXsmG1FlKPc
k5VHddiBpPa/8+WV+3DIdFAHY/nqccfaksUDzBvJXO60DNhxwBfTehS37kfSU7zdF+WJE2YPwOt3
cuO1gwR76KhW+SmZDQEFeavw2Qh2NaUXHNwW/iW56iJwg27eeNWK/gdN0cqgDGOrLp6PlrsEdlpL
lUDj/l4mhRddz7rhqiAOwzwQkUqvfjB26MGTTQSftOUCgm1V4oblmc0iblpP91wHp36Jj9tZfRQK
4W3CCt4c44R2b+vwrMxn0wMD+6SCEkg/1LzPBUKOn8DIN5zdDyF/KnzF4M12cUurzzgq/M0TPcaT
BMpfuabFG0P/EhAx6Cs5CGWKzvKXnMBhKILcdJE7AtlkvLDw6RYWdF0pZp6wr0zxZ3wPYRX/p8zR
GZeIL+sOFZZxXsrblF193MqCWV1jDqYKeKUigg41qUVqL5zJXh/HcUgkCS25QALXzIRxOIzxd2xY
ATcXQm6h3BBXbPXFg5wwVWOXg/LvFqDsMZGmr+HIPpccggmBXVe9eGS/LufqKsIIELszi+KdVlsl
1YTN6/NdaVY+xuz3+m+sZ0M1d+Ebnxtv9T0qdhUkuSCd4aypE/67lHoU2yiHph+a7JUGdHXZctZr
5aDyLnt3geYa4T9hT+x2uIWWDY7ahDYzi6pVRAOIYhNlTayxkQIoHPAN2SS1pHPOrdcttjnNyred
QSJ+cyA0MkUlooT8dJfLtSvoS+Bxfth18+QHiMv9Sd7ILKjfbsOQjRih+PN9havfQO4QgTIEk4vC
Gw7F91ofvwfF7PNH2fMF3ujvBL0zrwWxFU17lwKxkx+zRGTFpo9NiB0PWxsKiV4NTboCT7GG7b5A
EMOuLviSHXmw89ofCkdcGqUp/Lx/nOKrshEnB2n1z/H+S8YHCickDslzHBtP9Zq1s7ZpCfzX5pBy
U+Q73EnKbRrVZsWDfNlB/Y1qJwCz/H7PrpkuzVxTkETTtcE1jnsXfLre8h+B+IDBOOJaKvsQSRUr
yEn+jpFtD2iZwKfmI+joBAF9PjQdW9g578SE0NpZ8XWKWpPm02HFiEH28xdo8mXjDTc73CWC2RNa
jIfP/su5wOrTxWsnFvDBwmonXS2hPDuhy2fSfisLOAOc9F286wdSVlpa8t7uWk93JE4VHqpYoOju
d/pCs5DIxPSieerwTPUiaCCwGzn9H149d+XdBJEDbx8/6nl7XwR7gv3C42MHdkkAavncDNlF9kUd
VsJPloxw8d7gRMwkGNizaTN6AaUt5+AeYM+REe7s7NrOq/nhMsn8FqbUwZ41ctXiZyVGJHxwrkVw
D5nD670rTEt5OtBgstXCrg2JVSg3Kx60aO+OMKfxw2JfM+7B/M6jJKNgPMHSTENqxwyP7d77yyR5
snNV9VVkhv8+AOWrg8+2iwvjrL8z8Q1l6LP8qCh3J5hy0sGmJHIlRsjpm5lWAc//23H4FU1NlC42
tJ7Zsof7XhgTqKNCEmG0Dc6DR/cA7cO8cISMU4k4/mpm0gy12oy+ttJwOzSRBcZRMI3RKKDGEyU1
7ic/Wz/yib7VoT7kRo4KN8ghc4fTg4hTJgTyPpMcJNrAFqS7ekzvsnZ4xLtUUi1j72QN8OvYUfnc
uX0lTwjk6Lw9OeblxrrjtKMKeNR9g6GsSUbrJBvo6qS4YV1nI4SBRj1VY0ZzucItQba3Em25awZf
hfzQUynpc6znNFS39n/cIFAn7tElrS9tc/FdyIc5jhGjsvz3r5eGwqlR+hS3QpGpA2JZnogTcyWZ
DCPY9PnP/dx26u7VnjNfVr7WsU1fU7iWbkyTVw43ajf6hP3x5WPH6ctzSJCQV8zKG17TS+qmHXn4
k5qrvTUrM17GSZDOFPcZZOhSJGn+EuFR+DdLAabA5kiEIi0HkUCXiXsc+pB86+eVGiW7HnB2EfnB
f+Tq7+nFhHQvjB5KIalNZWLLoAGOTbP4grr+q0JkDjDSELagmTG5cEWgPz+U6L/C9BczSxBuwxtv
aRi+VqERRbxcajY1CtmiVAfa3bJjlw4Xq1ii2je7b3yLOSWmHuR/ROiwiX6VJwh6OZOWfPp0hMJ9
ADNFzRSSdW3z4DWZwOVDiirsCBoAeodJq6oyz39VV+YHCj2WzhxOeRp0z0lK1BAGchIBs+0pYHaG
VnsZ5qx2dnDXdfm5EXQsUhWYADoXl/DUCgvT+TwS8VmfF5I7MZuty624ibUoXaJaoh5H956LhMf3
BRkJETnIT0EGmu7HE4frwciXPEpxSMqAhEgfBMbSnMTw7OmeeNoaxSVsTTVPCy6m2mXS2fXWfJn5
nq9RnHFEcM0UMY3cDB8MUr2nRnd2y+3RcQRmeh4AR5q8fwUB8iSvvFOZLEfm0JyoHTFnUwJ3299r
8mcW+wJk98QztyHX7J03tUGJ18Hetj683rzr3JMdO6shpt8RcIPSssU4H7ilUXKam/YbCb2+3rqY
jTheoGfhPDyFk0jpQIt2Y2kGhmLpdxQRYAxQVvOW0ghELeteny4o1cpFFGbvcpvN/8746+QksTL2
FrlrY5kv2C9OdG7L7HdQ/AOIuaR2VLqisKlUt7m7aAt2qu19KXu+Z9YLSa44ix7nQMwQyujYmoEX
Bd29E+qinq7RxSaetnjUcbcvUOt/chOljPg/OyE4ZRhS7pjv0Kw7TwSzh/OSYkZXMuWxWo33N2l2
9NbCkqgN9tvX3Sxe7NEUKMdpKZIND50pky+wr5cNoPEBpFjE0CBzSCUcihgzYF3TMgQyWIirTkoU
XhpTxeYvrM/D9x/t5OFumE8F8sproHz8VezdsaRyGTrgJ7ivDN/Dj0NC9smbJ9Y4RYQSKOC5UKiL
Ms0iQm72atmM01Spcsx+opCp7O+M0OCRvUqkW95WtkhXYL+rYM5MVcdPOtqjPETNJECRdx4Q9Co0
mBSbCuulO6sqNEdjv0ayOBZC3rLul8zkqOgDpBrJBq05NVD5EpEeHpBrnID+zcb77hBKm1nBRzYN
papMU5Rk60lquPSbDKrIHOY+WC7nGxkKvQPR+lDwggt43iZ6haKdp/Ewvl7b269w5wtnrKakbQ6T
2BxQo3tX/hN/irZzC8Pm7LYpm/YU15IX2ADmxryetJ0yWNAEpOAua/04ISydBw/WFvwJoGp5zP6E
fAYVNdYQ1nEKdcD3tFD45VR8ntqaS124XJx06YGMw4RQzmgpfVGq29u2dr+aHR8V6RVUAEejiA27
RtIYc5wcG/kFrqcySZQoXEtgzBAtpowmIEt3OBoJKh5fNBe0SbnPQ0dW9+6bTJG9pqZim8Yfp4gc
CE0Tdu3dnAgHRKkJSpclhQ//UK901oAUIVVtz5d26lCpbxmIaOsf3H4HRjtUtjnKxDHWW0TDSKyJ
7JCWujh8rlHvWMU1/Ooi7akqZj1czNWm4KsB8r+Fz1Zq5zd/u2oHf9xu7DMq8up4wnOz72+HEliC
W0DR9iivrF3lmrhA9w+bhsi4nV1E0JQSttXdC3cXEbzRtHzLjptr8SN2wTdjsCmhClyvS8oI+eos
+1rHJQ1KDRlJg8ZkQzmVR/UihDLgGZhaVma/5p/IQ5km8L40kFzn03nzw0LbOngv4CZvDEolKyqd
5Z7gDLZq2MEBVR3exp4WldGq+pwp2BfVd1dBZ9erh1NN59Jiz/mNg0LKjBPER4se8UvSM/loS3Eg
MZC/KY1wTAJTyfk5wc7868QP06TvIaYch18TmlQ8ziGwPWURC4Qmwpc6H1hg7rPreaDxodgnmgoP
kTwnpqWI3bIrwMPwxKC814CJVNnJf6ekJ/g1jLcCoJARM0pOmVs0oTi8YrRZLbFOZvUzpR2D3Y5x
LOrBj7ARjePV5uJZIsqHXOYwqq3mBNacC/NlxQ3syZtW1KmCHr3CjWqD/JlLd43pIopalaqKErh1
YnX41jNzXn7Dxc7XI2dyrZSYZ9/0fuRnEr/upgA3qsE7Xp8lzRfkkcEZ8dx29l/gxKNmaNLRT18t
HaFo1elkg33d+uKVnLToj41eu8FCsI38jijnEyXt6K7QvNRlr2scRDzw80+7bt8sAzNcBrtHicTQ
9fJgQ+4Su7NIrI90gbzTXTlmsOg3uwxVRxYFY4OuoGOMM30PoqPPT6qQWnQ7QnogwK2rDtFZsjte
eLBiLUwScMzzy6ins587kUse4OvRky+K9r6SkArJ/2Uv98a4pIAas47a8/CN7dIbzzb/rZx7OM3+
me4jzjtwITSZaFMUtnuZSCuyCe3j+LlulPNU4RER4SieUmKuy/ALBeaRBagcHuKEx5W3dCiEysAx
6ceWuY/O6FUptlTVlXHkKer0hfgI6nFuvIFfFCtKC+M4DkWgxb3ahlr4/AYvXWvq8P4tn0cDg01V
p0sl9x1IpKbyt8dz8fpcr0DkWUsfNEjOIz6N+rMJaa2YomXR0Ac+P2zrhyYuOgyB7tc+EK0TCYrk
Vl3BefXwoYZJsEd6TVcr74sLb5PZl+fU/Z9o+sK7LyRg65aLC7hb2ehZMFL4ZRYU84CH+NzZ3O/n
f+BNhOws12Mw/ImbFbcoWBT1sN2iDmvbx4BJHcGIyWU0iQmcPBvS6cCC3xYwuASH8WgFVizw1Qiy
u7A/ck30Jq2dy2UjRMR3EGav2h43kCj+TIixHl0F0aBK61YXOyzVzRtMo8cdFC+ZuLOeQW34/JxH
2AbVgQYQ5M+2OMR3WCnBcViLhzNZJmT2qaaWt+60PKVeL29+3RJCdbl/cbgxBm6KC+sfcWeEYdmf
t2G0/+aYUfz50/3fRqR2tGmGb16Fg3xPoQQhkTwkX6NaV7IaEAufLVzra0SNZ1A4M+OlyR2JIZ6B
3xyu3XE62m0mW62rWUm2XwDV+xiGHXcGB/4B4CB0j9RYMCdcQgCWBVrSWjHbdQdjKQykKwn3JkyQ
afJzR4qqsnrrSZQuvtfHoStMWsyEUoU4sqbhpNqW3MIB5GZoKqCWNuJ/GxCDuRIkH9C+590ZGB2Y
8oP0MTFDH71MfLoxshGPgjVM2MBMyZ2DirzVJ5BeYyxxHODqHF8F+qjSQmS4YsNjRUarnW+m86cm
ta57yzM15Gq3Q5sPSExj6BvN4+6+vJ2rjXMIH22diL/Lc0XHFlfhB9wmwznphpyohgMyaXp2xqzi
j3aVqo5FldqEboA7cM1s6dKPKPYCVFFTFMDq7cT5sLuU+WzbHIKaLD/QL12jgOMXtG6KJvwLnnoG
RILh0e+EZgT1RIWoCEgmMA0AFEykPJyfhysv1usGgxGmdckbZ4zfh9tOJ92tRnKM+9BlLe7OITTt
dILWbE3z0QPL+pGflYlk5KkRv3Rq7wJLfklQml+lBcyeji4zGDxdjqvjXwsznQPlDF6HxgXzVNo8
f/fpp4XJqSGKjQ59LYQYbJXzYUfvJcaSQckg5OQyJ8eaFv1dncIgaeGzK7R3kDltzdzwIgRH0TcY
Xt2CvagoPg+mQpT+2krklk/YAprOP0Zm1uZsZLfSPYDMknPUSYDiF7GVZDGJykqgNPuhIAlR3D69
hGwm6o4/5VZ5tV8ImWTVUIZVgpneDAzfOReAGxiyZZnTfjlZg4xJDdr0xqpwpJ53iqqEugUW2xxp
HAAo8+Nbfe9oh2gUnNkq0pdk1iFIvsSBGeT4YD96myBwKyQJlTmz8fi7Y4VW6mUMXXOqj+B3vc8i
c01ReVkf6Lt3wOE6HU6kFcXYmrxa9yTgNbrMMwnEobrYsYe9naonb1NiMknNCd2mfPWnWTI2gBEg
4kjSFx3V+34wfGITPDbHgPa/HuMpextxW7Pp1VRLtHO5YkHuaVJkSfPytqH+TYJhBHvRMM5+ldxz
P8Nbf6eXvuHapUXMykNlzjcawAzTEEm20Sy6FtK/RefBB2VqGUiStJjacvODSR9uxHRxhd3Und0e
nhVKqHdV782NyRQkGSqA3PXTIzfAPkHfA6Ar4sL/88Dh6DZuagCGq4E2qtPnl8uqecV9tu7hiN8t
VvKlhR85b1DMii4HJPYBsRhsVubd0QjP8YZ2cf4prBPbxdryDXDgnaJWttJw2ow6FHLoSMn0spq4
PvszyPtrKwdyuQ4dxSNpuZeNdQNMiL1EHO+xEs0Mb4UXEYBRFRzJnICWVRlBfq1RZGCt8HTdm7rt
BR7LxpCI2FJC3dRJIpBI2+ZqrIm0fQnynYPHs4/agVo5XVIlRe24T5ruiLb+ni12W/pH+9WNhs9Y
OjkJYjwwScLAt7iiLcbOUzzCFkqlKhbTBtGhPMUvAA/Jw9D8visUQ9Sts5+K4GwJZtSuSG8wprrA
6J8YTvNU3MKLMlwGuYIVS10rRs//lwllyvgVFt7SP5dqZaISCqHJyxE+rvJX1/JX0M6t+7ivChNX
RMs1hNIOGQmcAmOWU7dWkOzNQLqLqF6lseHWaFo27eKXLOPms8zk+RI9ePj7xo324jw6KAMR2sX4
wItjx64nMCAQKH2c8OW/U9NEVcg/LHg0yhNBs6vXobrG30mGeq+271PFIhoGX9pE1aITgbCrIYmY
gQNrP9rDKMXhOJ7TRXCcG8O1mA+DfflPOD1IwJRULhmik8rV+NHpxBtxY32xIegIVnPvBB3/kHsX
24GU0keC4MtaJdOYqqSJGbhWjGRr1oHQhIRNwApTd+sntRM3O8g9zF+YEa2ogLd2fb7wYKQFPkd7
X2FKghenjkiAfXsn20ti3zDu2htp6hRsWil79t97NWvCfnHvK3m5ES0Rv0NVcpww8WqPHkxVoXHO
t27L/lL1MJza8iS3asBzYxGRgBT765u6fT7CRCsC7+MfvqGF5KO0nAgB42Za+BRnLs6gTOgWQ6wz
8ZhhAkZ7J5fK0hvBpyR7dxc/HeHuY7iWO3U+PIZqUsun6JdY4u5r/Epvc2cBZpawdVQaICdeVkTJ
gOLA7U1QDusx9L4DTgDUG9WZyWZ8WmyPk839SnRbB3M/Ia/6PC0YV7vjdQbMvuZbfx/QWjD75ScT
NP5bDxNewLXZ6DioyxYPHBRe8yNANIRp8jOv9BcWyqiLVGW4ZpzQzME29Rtd8blg4HwVVAT9fOM3
mTA/ZN9rTmFY7Eoz0xW+eJbrtlD6Yz08GxrJrt78NRCDgJeDCOXeU+dmXStgjuV1W5kZk2fT+ptM
ruizH7/WQ0LhWsDMWu8sYkVmOJWw/P9Xp0pnW1jUmfdHiwKLigNtwWAGMJYTiTgktCMbbRHKxNC8
4P2kQ2N6Fvs9v9SXznZp1wNNkMQzhxnamsqKd4ASdjIP8P7N7wRxfLI8FRPV4VIpUZXXmoWiEror
EhyMO4nosWChO7w/AaMPhrIaM+lhyjzQfFXtCM7oYhagMxz1EOFVA1qhwVrml0cKP2aBLRgGNixd
rtkE/XPoYjR8cQK3GvEKP/B1p1gxwj+ls/Guw2ZnnRfwtrzADPGfT3lKuKBTV5r2yCbNKD9utYQt
zaeR9qDWJ8W/AlQPbCH3NZ0MMGfeAR1k4lKtNeTeBGozc96Agpycp6y1M7GKxxqUTYgvCnTttLNT
rs48rnKNNfbvYnThrzNvJWeoSjP/AIOHRxQ8iLNQQD6ZJykSdP4slltRkpZosR85HLiVIFeJDDMl
kNcaZ7khAfTjczGDzS26DlH2ZnON8f77swRTHRLTwI/876dITHBp48DYwc6MId63826BpZmwIxiZ
JCs7t89gDtWsbxWmp/tkiI6SV+Oe4Z0OaqOyUO/nXnm/HXjGz/cIOzl7dH6qXhafC8vjtW/SqqGF
T3/xnax1+G6ZCD0eFgQnhXA1RT7C8DP+T0MGK9MXsuupKX+bpetJrfH2f+UQhLv4ZUhnQ+3cxxXT
3e5yaaFN6aNZI+zL9/ofaq1fXZO3XqjrcY/wXdZEwJpvy8uap7UE1PIBDk6YLfov9qSiYygbZLjt
HNCCT8ieKNHvBdpK7PKWIFrsubwDRRDIGpmqCEmj6dHyI3EuI1JGzvKw9HsfollKim0gFqNsHfRy
QC233TmY244ONbT9A/DM1p/+/Pl+opsPF7fzdaZ0BKrdei/2tdpXf2kWB/n0dRVuw+1U8G9dAB9C
H5ddaQ+U+eX5+gf3zRxWAiT2AOvx1vWFtQ6zgzU4va579IxH9UXpI1pP9n0HPIWf9Z2w3g1ziyss
K65b4ySY1rwR/goHDLuLgqWXMfwVtC1XE2Y9HcTBtdzDSTHjYiGRaeYPzyWW97P6T3J0pSvVJv+l
WXCYrOBG/1eQ0dGpadXF7J1so9fmS0yAkNn43Eg3+9pnt+PbMSxMRV5ErA9r9DWvY90Z6NkGe8Wq
Zq4J36lxxrSnFMDzseVXbl2YwkfJlvZt+nQOh+FnyD6RVyHZl9xMDloaUPc5ERXTUg9Rjsk4hAvP
AQDrw3emaOcbmJ5DQWbf5oYgvXW0EOBmrd7uuU2ZjaLvp6ACRX/762Z8vL47k1gfROMsxYrZJHj3
R5cJWAXHIfW3Zk1AD+A1q0St8RLnVNDppQ/lvJui7WKefmLea/zRODZxj+YFX23y8/24DhpObhnX
yFYp/QZ84RMYY3nOl896UkNf1Dus/YAVCDlu/MZPwGc7kciyGcFm/z+3SNOMto3igxfi7aILHMPa
weOjTnTlBEXdqOzPoMS9lpMngNvHuFPdO0ZadnAxlOcAP5JvtsLCqHtRlbR7kVlWA7J7X04+0v0C
ExXD0v/zDXdHapmpdRv1453wVJTTw8naEPzZmJWdnkK2UxKBBR4mO7PN4P1lACM6XZbyL0WnZoTh
+Lv0wDaEshkm813j3LRB8Kk9m9a/YOdPB/lVuFyfoYQgwHlIJvkbLma8HYM4qrmQWBOrmcEk65Ar
Vdh5LtXK4E00vcl6UtrgJqmsreBB0Fx4pd/TQiZ8e7TXNcz8YxdKBaOozCl5G9Xjzkr82CHDr95q
HV4HaWrfZqrJO7lBzFFAX05dM6QD0CytT4y1lKSL2bq7NYg5gTUkZLvEGtBB/NeQywKc8E3mpAiF
BCoU4MeZvrfTxH0tjeGEpB/I301RWCNiZIYg3v9356c7y93CdnXorBU+Y8VSHcRhRAJ2q+tVYXLA
mucqWQgUpBHwyKS7X0MhiY1SIVFjkZQ6PPeP2uhWm219ERV+062uVRGDq9LkhrMOtrdHzx3DMgne
Eom56fiLM85OxDqkyM6ffkm5Hb50OgfNhMAFoshmOjGb3k7NGoXFtaT1jTHxul76mBeWY2M3x9LT
f+oPtohRT2FnyEtoVaUNQaK+6fNFi289EeRTW0bFKkLK4YSTRHtNK6AdaWdnwW0rCnoYxVm0Mjkg
Q9CreDyBrGpvLcimkv3KEzeq8LSWS94uOi2UtD3hzsm2aJZehibPUr98QOk9T2CSENMDaWkzDN9r
ehIxLgjEBx/bKF8dVxFOy6CjemTSLgv+acwuhkI3exzB4+2idIg5PKVtfZjpF1YoXIYySfO4N4Ib
fPWDEWY7DZuUehBin2ezQyn5/p9OscHYNqEp5kUn17r+Qd4j7usOGLjv5UR/iB167nvKSf4x3sZ9
XaOJ+aF1he8ZhmXrKTWtUd2t+NNp45E0r6OzhGAntc3X6NfR3khejnWdP7ios6mGImd7gQYWfbue
NnuurNG8tdPSrJI0R2Mmmh1OYqqGNM1b5ispgnFaOnhv+6qdCAmQWkRJXj8j9PzKLIKz2wcAtxeg
MiQzz1811s4BqNzdz/OSbiZDWdkWVHRM8UEHh4mv3CJmJclGSo6AJsoCOknYeeEg9cvduTkUrNU6
Kxz2FfhXMJ+8ixdfuby0v+W4fdvQaS6sh6YRiK3r18bg1Too1gIDmfIx9SeHu4zjgWgvV7S1WDsu
+Qb+NYguM/QK44yAFYqMit1bCcfnh0sc3Hr9uwp3+gD+KJjz96fEjujA3ITcBlUJjhuQC/mPMFEA
jbvCXzwaHV4Ig/PBrpdXMl9BiJvAygCdpsqsvfJxcjJJFPt5vQ7kFgWnINujIwqJ2ayJIQtirZ1p
zc1pUD39cBoThXU9KNr7oIB8O5zKMKsRibXPM4BQDsT2uv1PcFwi7JZRdK1IZUy9FT7uUjJfbJaq
MVXUb5+gQMTiAjFjEzq2cUGraZxXFTTbV/YnIM55i5VtCNyVE+to70xBjeTi61CRRQh5i6S5BRac
9iK4hQDnsRUHdVDItTQMIGeV18oonfCMk9JyLE7PgrnipmYNEQClcqsU/48vCUTptUW6F08bUYGx
SLxz98wBoclwdUtF4+dxGbtyTrtUwLwpDQLtZIN/e9dXv+YHwrd41JDD0YTlF5dvOPThZy7CG5CW
/9pFWvi0ZJLUiZU6jNFmDPpxGejmYMkwME7IawMaXpgEas1Y6FUXFOfubi5+KYM1JPvgrfaNFdlV
3ZmPAxGBGWl+AP4+C4/hP7MBI0cZ2+lHu+HAFndUGaw+6M7/SvAS4+qy/hbQYqe2FvhjUuBZnkWX
1LldLQrh4/BsdsGysAmY6zYGQh6qcmC47+Q5p8ZEZoELvwynShXFB6lit/7znJxxZmgL9uL16MTT
/OgcIG3ERp4e7YpvSCXFBUFbygBeTV8jgNjGQwoEdZJlyiJdzPi5ksAG5WmE87HyOwcCBtLQ8Uz4
jJgvhlBMCarmyXgDt0Azb6EQr8JLHJwNpNicfLp8nboxFqy4c7L3ppS8OX3vE/nQs7BpwdjHPE11
G4tFd94g4nTfiFGoT5CtL74Jm6iapwRXK6ehfqYgrMflNYkzN2eAgk/o6Lk9Ks4EIr+hs2sqO68B
cpTdNlh7Svg2+YEo6r/HcZ7Ta+3BoSoaejcJRGWk4BypGJcnQvLfA8mLDlGYaTUeyawmJ1zwM5XY
XulsyJHfNsYrby1AR3CmOzpbpLasXmDU1FrVp9Ud5wx2bnqACJ/RiI0W4wKvmU6iVft5LB4cg+9d
8WmGxfkgrXyXy5Y/NKyHXNpoONVevFE6UAw47YC5U2KIWZlXB3AJHykCSA33JgeNg8AbYREdISE/
ZpdEuxJ2voDMfOg7eLWRkTUeR/zW8iblmzZTf/R5yMtPLpO9YuODSNIETL+3eim0jqaHXE2wyr+R
T5TXxpxUmTlVxJNnlvu5bcgTmVG0+5FTha7kzMHUP6rV633gpvtYGE7LMMlyCYZ/A166owIK9rKG
r5237mnoZ+k8eZBWp3Y1BkzZtjXHBeCP8vMjlsgowOKZGel9Dvh0cPLww6PHS907K9Ia2hYibPeu
ZXMsMbDJA42Tfto201NW0XTMh/VtB7pvTaKqylxdtOtSI6450ilqdo9ruEzcIZK4mWh0AjN6vSbf
705bn8lfOfd5IX3TGzFkL7qPn7T3okRQ3G658aGclt8EQ6qtWULd5De6weuZKYJ1DJqPqBo8Fggm
geEa95Px63cWZdHyvYQ/GzBIGPBMHS70uzsiHK5L43SgotyPJT/o/Leie1M7BTQXBJX5OQb6I8BI
DJkJZHqoGYKOiJrNdxwS0rVSmS904nfDnsa/ZpO5073Qw/FjDUaVhISStwBA2Smts8sZdmBSMqSB
gLEXRRSEIv0VMlF+Itu1PXMHBcmnY5uSN/b3ya75H//f9UJsRqyhYrKG2Rg6oVrhMtkfX/fnKp7Z
BjDRHkDiIQ2FP3HJ9v6Zpawj/hbAvVXs8vHSjkgQ7zD0/2GFqoLmsHqpuzRC+IWQSZ4AMbOEC+wx
ukv23pQHziOK/do7AEOuUfMFPSAOZuw6H5SeN+LSoxGrfDxN0omP50NFObhDf9rdbWxhGEp3J0Wc
GsWzu2vCmE171TF/BJrEfPJzUB/CprfWaqlqY+EIXR/Ucxu75JUNpCeL1P9gn0yfkxul/n+7nuQA
jgqaQDbVzC5980bDgLLiASczIkHBCgjJzzRQDlkizfktJI8BXktcALh9nx/gyl1IL6246aD0T0UY
1+7mcgWgYlknKeAsaHqvh16VsdYJdTffHojWx0g76OTuFWG51gNKDqk5n+R88j3yevJZS+B9MSuY
R0kT5mz62RFwDxwk6LFCFOqRi7gin7HEywGvWVfffm7iXLRWacDiSv2JzcGW8j1YrjcCtWccAoSi
LdHktsP86bNhEuevPafr30EM459zSHTVgOq3iuczdeqXRrEGLREDALP6qeBrbhwQ1qQphq1/R5w5
9ccwfUpfLUuQ9iFz+BFPrNMKfGYGEjE2B4O4fO3yfgZKmuVC9lxFkc/luKohQ4m9xd8XHVaXcBNC
1PAvoy8Z6+z/IKeZAm3HM5QLZ7US4p7+4dPT3a3QMkKtBM+BUUgxGTpGeVrHuLLaz4ljba8DcyUk
b2/gxe8i1dWxvY4tVmSnqi+MXjDxJSVo2EUiYXrT6jFFZuI3TEqyf5UxFX4ItHBFKIhs+Yib6l3J
Eldsr6fnSUryS+SVjc5G8vqhoohQyPEWuAlgft/ztZcGoN/uR4sbOyFNTZgjv5u0C2vETCPfLf5m
rKzXKgYG/RlJjrDcTKch+S6FPXQbGETzvxXii4duRZjp+yT2S1SjZl1CZmHkvhdo21obOaUIZR5i
Jp6ff3Ppk/6dMpsYSQT8wR2Yx5LXsk8uSWmVdt3/7CCkY8EkbxiH2qofwTvsO4ytJgB7J8V4Olzr
JCp/3cfyh2l/nUUIC9Q0RSArcwjf5Z3Vob1Kph1mB5jJJLvfK7FMsl7fnCkzou0V+AU2UGw1w0Gk
HfR6CayrlI+l3If4+BSKYAMWXpZZ5xyElTN94IUzwKohIE7Y89xeWULiZIZsJCnQN9ipSi4KOdt0
jpdi23ky2X3xRVAEyfeMzoFtsROPbK7KxXt/j3lNEHBSr+odPa7u1WCkdPVOPj9r+nK1lNWo0HM2
fhPQ5jChLu+OdxB7trPwscBpsD1n0ipQWcnCeD1hlZ5yaeceozfFxLAw4yu/I8nDMxiEnWFxWa50
+2O1ur2MEpmV9m1+Ha4BULnG06cRJDyp3TCWb7Kdjl3uwwQx2nFiOeCsGgnItADOJukC9BvC3J5m
PJklqefAR6RSGyLIzP0mOCYbO7Jc0EME+ad4al3/5J7IsHU0dvsZV2Hm74iKhiRjEoFuvfKKwUMk
/tmyOjphAM+9/acgEZqafpvQW3cF/DCCwDMZui2PFCtXStAMwN49kAycM+QnkA4dB/dG2OF3MQp8
f+obO1f7VgLCG36XUBszRgwu5i0RIwZiMYhyRU2TZiEWoUSMJz1SmQvfbU/Yv0oFtcDLXiKx6mKe
jKtd/1k4sQ1T+3yOQnsGSK2sQOXe+APIBbMmvTIv30EMr8qrY3H3ak7eTznAjHBTT60ZROHO10EY
FncPPBiVRHF+tVex8bHGVUZf7Lmc0zEf3mE4tEyOQQObeS1mei3jLikOpN72BY48JDK/yk43TUUJ
fhnhAl45zE5wIIAVJoCr8i+8bAIoeiGgsg6IRHV8jhR9OZahoIrtabcu5z9mRofPzhxzUDuEIyL5
icrrWNfK7yni4bgPDqLNer1/613Ym0WJTyAS1dqEe0b+otySbQMwv5Z4FuJA6w5fWwqomboMfM/b
9+psrzH8lJr8qi0nKCbdRPLa9Jva6io6OdsHh4QFPUgBynb2V+AW1au0kM+at3CP3EKZ+d3F18CE
BE2zo0o/PQuWmGmvP9XXydfBQWvWp8w6JT4UNQVrCymuAKQDVbBOL2l3PSaPnYTO+p4Ehx9UbC8a
gFnj+lFftGheuD/lHMu46Z0HJSxdGloVhiX/RScfvKtyUGm7jNUYiU5pWceoQBytOsxXu5npdNcG
NTGkVI8cIgWqvvUyL7f6tiUDIz5LvJMjxmFPUxpI+v+rhARVgVD1/O26n56Avj39tAge3jiOAtw0
9qyazuHJFRDwDucPpjUlcxAwlEcU/ZaRsQBXK3+QzCdXpPCbrV8Yykb0/vferpbedxp6UzXK1BSv
9YuRKe/A+Mu4KLJzZJ+k2iLeqNMWTwow9venSPMg+z72vGum6LX71BW0CXhvP47xA5Av8cfyngn/
qYj10BumE5ClfmOeVyOgEKpn/dn0rlhb6lmoh+9+D5bgiRx1LmksZEuyrvbXQ7mPhkYAJSwLZs9c
e6IKWeT8BEiscGoRgs9a1k7nO8hgd8WciEfWGrJ/+1B5qKwT0WRdUCdBfn+IMrJvIibmLb20r0JZ
f3tSGlPTpI57sCpw9R4juMsIT9V729lDb2QC0BWrvMWJSNCn26IO5IuKuCdMX0Luk+vKGm5AK6Vh
BB9PUc1s41+mLLpFQ/3Gxoo/uFqPIUjxZbmP17Is605r+LV4d0oqq91ewQq2URkU3VlcAzPmguuv
e6nQs7o6ssSGxc7ZFHCBgMzz8vaJQyHqstEiTcsiUO8yM/XrqzUk/1DNGHb6EiQkEDnbWRIk2ZRx
Krrk5xyoNYk1bF8RUgrFBmxKlcS0dRZzo94iQEJPXvVn4qXFKeqdPb6Y26NNmOQ9TUogUUZIy/14
U7zT21eatqVAGX0xYpfc3wLVZ/89Coq7pY9vdD9CxorhNJmUccIQQ2YcUNWRF94fSXGT7f3sKK3Q
IK/e4OfEJSF9aP6QTIfVnM0vpE+A3XFGNu4QlurIFh9bqPjY3uLPmoxDlTEdczAW20BuKWQ9uY3F
8OBHmLBFc1jV4K8O93xiJGsbbi1jGDRj7Nxetvql3ShO70M8X68ILrnfb56Obu8O6fQN6WCInGFw
3PczHACa9/iKXKpfZqBYhDidmNyIXx41TCaDZVc0H4bUcAJmYc6i4EFgHdorb5wTO6xCJMEy8KDv
xqIeKxjJlSY9eXmYlv//KSwGDT5rNxFcKs1gAKhlYltFgKWjMK01EVt2BUSdWP672geu3B8Q0K/6
LgavgKGAOd71dtJrhheg/fAyrCyHhHNanvN16xZ28KwA0qHB3v/lrgb0qjo9gl6V3QBro02StLwr
Bc2bxv7bWKPGJ4BxU5mS+HjxjV2BslWSLQmT1u5FE8UmZO7oPJ6a/qYEqHYNvwjpwxhtY3sHnyxD
VRDRE9w5xHdWk1CxgjxichKS1/cOtJjtYWaDly8SgbR7XTBMvB/M537g96cGv/ikqhXf+TdV45Hg
dw+vrdwns6cCWW5dBHQzTotUACUanZtSkD00bctza226QGIN0UeA+U2U9ZWRQMk0Nf/OZfcyQrs7
O/bJLy9M5kFLRwfzOnjyuO+II3mj+zVWMC8c7QijAGKZgim0MatRhp2WyUlah9VsMIEf+fjlYRlq
zNJDhWGny8k6HD1vbJ+F4XWWQsCkLu+kWfWvaPwLfCnb7SI1ou6eKi0QbgtdZOGhR3WeYwLohF7u
EVtCWZw+4tb+Qe+qN9crMjAJIeoIEbzm/82o0mwM1shJ1biMmV3IEaDBuXeLuZXWt8g6Yy/p6XPB
JGfdHUMecbllQZE/G6qV5LYgN/S3zZXcF6Y8tqh8D9v6hx2h0pSzw7PHiLlNo8NKFLzUqCrj38Tq
8QddxtThcmwEWfiKMdJSMmN5jganXu18EHpkE0L2NFxqyNECjGMHdn1MrIDniOGzLOsXFcSjLwU/
BnnXHcchvKQnZAqzknzqOQ+1RFukLfvobctE4tlvYuwgP6DfN5ypwPhDxk47x5J/Cxc4wxUzGeto
g+K1N61obanSZ170arwPDtF5WFPy+H59DaRfUGqwztqQg8o65BWm6XOBZ8F3CMofcCJFA3fCq4z2
Z6WeNu48w2DeH0yPFz5JjkQScUwRfQ5C0k/BFI9Ph2KOwZ6pPfl9LVjbK3AZDADQG2cDT3nhYKOZ
+Ud+5L/p9jnvl4uo7TckR5Yj88B2ZKgb2oETMrlaDB1Cnciz/bcDnWd4vdUqLY4nShF6CpbyKxr6
B6snL0XqTqQgN6IdkdC/aoDUnAycNbMaiqb5g519w+ivS9lx1MjibSbkkEaonXBfW87MinI5F5S6
grT+Ea5DFnHWNcgPW3IL4jxVWuPT/XYsVazP7NQw2RYFy3v7raD8aa9I1aEL9AKQ2UbVNP26t9uq
4+U2wH8DlBIWh9Q6ndEGF3mRKWsCNayLKtO9yOYvVLmGQG2TCEq3lfvj802TbUzMWXaRc2dJeM2d
IglqIOgWFb3k8txhrmDdccpOFazbiKkHjwRq7/AzkIcrbsXSkJm/MREy4hSGGN3AvUw99K5ry7eZ
7TgUInSDXih4en+AlpOkOQ+S++4w13z7li7yPuMB5xTjB6zXTK/NJtH+dwIPUvlq9zuDGFyMpqRQ
MXN7ciA+yoDtwjHbT9egByarmBdgkiSZIdbb1LZJQPlDbri88LmjwXN/NdIeHYcNMMkaOjjaAw+k
835m7URo3s1ji98Gy04lnKgasZUgkdw9AECNwO7Gqj9p15RQYAZiF9xFlgc/2PGaPUfLT67plp6B
GVonsu+7zSGaVuDgHQ+TtFYS2jJt4ZcdlP8xuCpp/wghRMxDamFVWNAR//v1ltDAjG2uhDuYgbEX
Ahdef5AmyM/9gzSfLW1j83vMYLoTOA8jLxRYUU1VGyIkxg2jGAY/Eu1djc7iWlNkSnIWQ9I7MjYM
rQlKcWh63lzwh8jocdM2Nom1e3h8fxZL3ROYlWvmCFHQKygl3y+doBPHIqWx5L5HvN/QbtVGr6UT
SlIVl2fl2wjF6p3ZCovkVuYp+i/p8pv5GNQ5IwQ4XAhgkaWDIbqbKGdBeTib2dFAwDkWGiy1fIAB
0Wr+vdw35LolTpiukfl5hBW0pZKKIcsVfxd8rLwm9fNzVIo2pVjhHa+Wa97bBrt3F5jPYZ1Fxcd8
DFPlo9jhqLYIa60eF3tnfq0WCNrLM7kUOqLBeV2iMopc5SarjoQ9iYI44E0E4b2RH8eta8UPvYB/
FwfLV+IlLf1/RP17cbTrfWf47lFRrGOOz4VYDvfo2UFqkx8A7r1eCIZnp3e9qWxjJ4X11NsgdqHY
WZ1zeMrepMXE7T1wwlJ5EnS6LBf1O8IpJN4h0f/dWKi9XokmdckCtfW76f9sTcu2BxpHCiNpyC7A
qY6hEWWszeRatYr2VZVTFKfB2Q37/KqexOogA04k2Pr5SaobS0WkPWD4rzSXtWa3NgMU6iSox1rd
cCbJKBpI354Svib/UdwgEufq62nkUn9jhc8iH9CV6MtzedPpH5EFOj4E59QsA/aIx9EufyvWXsJ/
I8skRrelcTr7RGzv/VN4UXCICHuW+ref1lfXxy3JGjZ2EMr176jjobgnQVLq5RA6KSleC4LEKQ2q
yQj+yistdUA0lArGJgJ3HKoQfq7H/0Mw+y5AhtrMMXXTVPXUhpB58/9Ef+N8ekdR5hOM2FMbs6Gt
fohoTxIsrAKL68L9OSdO5IXDkeUMZW/2G/1ZND33cZZmhrXErmY9cf+82EOigpGigv6GhzWigOKw
bog0rw04CcM0eHtc2JLUh7js56dD6UbfJplFtOXRtCWdkIxAuhl4qAFzkXASqsvNUIecLlTQ5W7i
MUA1oFCPqt3/5d/fKJzKpVGJ3K+uw8gF5Q/bjf+V40C2C7BLzY+AtEjJkj1ioqtHuxvsrkpofTAJ
YHBkZTXKsTUUuLstITKlV2p3Rla0whF/3nyH+a+I39wcBRLH6cIqptug+JIj+14uU1izeU6a/KPo
h8IM5PVbiIb5qs5k4a6D+i+zmamBVDaGvacEv6QkP0SCfeO46NRTchugAi8RUS2K5utAbEYO0coR
d1wfK5EXlOsLU9Weh02bTgDO3k7sTedI7sQEiF4ZKFgNW9fS9BF2vZitJkl1PLtXDv+U60xMRQfX
XQwEsWJcm7SqxZpeyBy8tGn3LkdqCrd7lviFiMbAayKJjByFJehXp6/rrfMfJtWwuQ3QkcYJ/xlg
C5KZ71Atqab7bmsab6yo1hx9BWDOafv0w8IBtNBXczgNbv5ixglJ+0EKq+m9NldPbcbT6uthHfXl
RbVABKMc6BjF/4KQovZaGs4E/dUOnyfehAv614irex5xFtfibUZdbFmYtvBg1osVeuQofqOqbU/m
Q+RFTp9ZRkKDwPIG2lEcCzRcLGQVYyOKdbgy6eFzFvX4GHhPR8tWatfK99Tpa8uPZP7OPwGXjT7m
bGVHotZ9t+hTNO4bO4pzO/CvGFzB6eCxRONQ3AvPb66jGzvUWx+SQbhSGjdUVrmc4/dRIEk8t6a2
ocVP6eyA6j2G6dBAVs2pavR9aUZfqFgRCtqh9b873r6nonaY0+rKvhbe2SmMSNLig98VKiW2W5t8
lDF9yCuitXoPq5h0DSV/8abmmNo0Jh2NCE1xmWfal6+rY0Mi2khlibRoHYpvnQjY5vLdq2VrNZLf
Ue5m48S2U/dBD1/syoNhVnE19mCSd0DQEAA9Afok85X6OAjIUdw0SJmQUhP9FIU0qxa67SKSHpNi
vfLwY6wfqR+WVCRUdw+W6u0TMO+ft/VXRvS9mKqUFnSGyJMNQQ5HL5xyCIB5uIwTKU7AjA8K6zZb
8ZI25nJXxaids1PzZU98fLYkwgiknryg6LmkqXaco6CUsNkEfPTMepzbPEdB+pMsknddCmku2x6Y
GQdvAAGUQrfqOGBCxZPma3y6rvgNa/oCHvCZAYEUHEOdZsM+sxcy8JfINFiZcj/C8APDwJ/Vpmhu
WPjWf7NBmwYzYavb0zFpOk8s5IxaE3P2CMxwRnsCy4+MkHS2irD9xUjQNkM9a1fwpv/eg7b7I3aC
WkkiULIVEr58PXvMYXRT5s9jB0yodktAELGWz1RUrkswDQFcEnM8l6W342OfXWpqcJJcTni/VA9y
uCEXJQatHm4NEkCGdS3DCZvgSwUwvGb2FeUWIBHg5OUYGL6OEt9MEDUViqviqS+kAWo9fwGs7CVg
R7Zs8qQIA0hhKG1yudLwpMGVh7HoonH6qCwYFBQu5BT9o2Dn2FYjUpFZXfiCed5g/VvD5ICNlZ4m
L0eFkpf9H6+48Kl7b3w9pWqZQ4Y5M8LyHGbCi0vbdfsSh4BZsYIlvc/74U8msEPGnZmnqXL4Ql2A
OxbCFZD3iuK5nvrTYca1qgETgWcA6scvsEcOmatwhcHxzwtOEH4lSYgXJZ3TSHatyPk6j4oabeha
usjLzV33lsW/5QscqmJqlFMDQUKZvuIFgYbfvfPbSjjdQRC14kd5edYu45lRt4QZReZC96gGEZmR
/C8ftAZgGLIz1TwZ+2CN2zHjSGPePUv1PAypv6vBx+JDbz5uozWO8Dl1hGYAfIXXX2VlU4dmpZoX
5CZXtGkPE9/8iUsyTxVJSLahpGy/megC9+8HXK+QIXv2ae07r4FchEJRQV5cqYNjlTmMCx507xWf
1TJgyr25OZ2qyg34+5BqKfeMZHlJqpFOm6cq6SS8Th2keiLJqzr72WPCy7y73NPpP8LSGKsqztBk
EWFyKXjl32ePP+GwJXkbp7RQt8ZyFeHvyUYWiiRC/pPgohKs/2LxTkuRi3hnPdc7+m+Byh/Tyh+X
eP/09EiQjlyw+j3uJK8tR8l0rhUz9sTv9wraD4tcOEpigHdpqruM5Yck18CQAgmw7xjJzO47r0s7
nS3t1q4vMIErwlSgTnW9Gb/8z/7fXF8MWD0j/GZF+s2gaxN0Y2KVmlXMQoG878WCuQZtQR0Y6LsI
QQxwKScLb6Zjc2vbkZrL8gB9XfpPiPAQMmzrxpgqcw/TxiftVUl0RkE2b7Ayxd84amXwsauff/SX
lvOY7RkPq/NeQoFL7Qh0vQ1hXAawcaaa7GTXmlrnhbYrvGYrsR5UWrV5x1JsN2GYKJDirH6DLIN0
1rBMFrhwrZZ/Ptcr9+nUK5M2gNZtS+wLNbrXoA0+6W0kNP9ipugxIdUwL6sEO5W0pQ==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_GTWIZARD is
  port (
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_1 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_2 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    mmcm_reset : out STD_LOGIC;
    data_in : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gtxe2_i_4 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_5 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_7 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_8 : in STD_LOGIC;
    gtxe2_i_9 : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    data_out : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_GTWIZARD;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_GTWIZARD is
begin
U0: entity work.gig_ethernet_pcs_pma_0_GTWIZARD_init
     port map (
      D(1 downto 0) => D(1 downto 0),
      Q(15 downto 0) => Q(15 downto 0),
      RXBUFSTATUS(0) => RXBUFSTATUS(0),
      RXPD(0) => RXPD(0),
      TXBUFSTATUS(0) => TXBUFSTATUS(0),
      TXPD(0) => TXPD(0),
      data_in => data_in,
      data_out => data_out,
      data_sync_reg1 => data_sync_reg1,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i(15 downto 0) => gtxe2_i(15 downto 0),
      gtxe2_i_0(1 downto 0) => gtxe2_i_0(1 downto 0),
      gtxe2_i_1(1 downto 0) => gtxe2_i_1(1 downto 0),
      gtxe2_i_2(1 downto 0) => gtxe2_i_2(1 downto 0),
      gtxe2_i_3(1 downto 0) => gtxe2_i_3(1 downto 0),
      gtxe2_i_4 => gtxe2_i_4,
      gtxe2_i_5(1 downto 0) => gtxe2_i_5(1 downto 0),
      gtxe2_i_6(1 downto 0) => gtxe2_i_6(1 downto 0),
      gtxe2_i_7(1 downto 0) => gtxe2_i_7(1 downto 0),
      gtxe2_i_8 => gtxe2_i_8,
      gtxe2_i_9 => gtxe2_i_9,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0),
      reset => reset,
      reset_out => reset_out,
      rx_fsm_reset_done_int_reg => rx_fsm_reset_done_int_reg,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_transceiver is
  port (
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    rxchariscomma : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxcharisk : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxdisperr : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxnotintable : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxbufstatus : out STD_LOGIC_VECTOR ( 0 to 0 );
    txbuferr : out STD_LOGIC;
    mmcm_reset : out STD_LOGIC;
    data_in : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \rxdata_reg[7]_0\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    gtxe2_i : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    powerdown : in STD_LOGIC;
    reset_sync5 : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    txchardispval_reg_reg_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    txcharisk_reg_reg_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    status_vector : in STD_LOGIC_VECTOR ( 0 to 0 );
    enablealign : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    \txdata_reg_reg[7]_0\ : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end gig_ethernet_pcs_pma_0_transceiver;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_transceiver is
  signal data_valid_reg2 : STD_LOGIC;
  signal encommaalign_int : STD_LOGIC;
  signal gtwizard_inst_n_6 : STD_LOGIC;
  signal gtwizard_inst_n_7 : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal reset : STD_LOGIC;
  signal rxchariscomma_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxchariscomma_i_1_n_0 : STD_LOGIC;
  signal rxchariscomma_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxchariscomma_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxcharisk_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxcharisk_i_1_n_0 : STD_LOGIC;
  signal rxcharisk_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxcharisk_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxclkcorcnt_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxclkcorcnt_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxclkcorcnt_reg : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxdata[0]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[1]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[2]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[3]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[4]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[5]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[6]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[7]_i_1_n_0\ : STD_LOGIC;
  signal rxdata_double : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rxdata_int : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rxdata_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rxdisperr_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxdisperr_i_1_n_0 : STD_LOGIC;
  signal rxdisperr_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxdisperr_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxnotintable_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxnotintable_i_1_n_0 : STD_LOGIC;
  signal rxnotintable_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxnotintable_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxpowerdown : STD_LOGIC;
  signal rxpowerdown_double : STD_LOGIC;
  signal \rxpowerdown_reg__0\ : STD_LOGIC;
  signal rxreset_int : STD_LOGIC;
  signal toggle : STD_LOGIC;
  signal toggle_i_1_n_0 : STD_LOGIC;
  signal toggle_rx : STD_LOGIC;
  signal toggle_rx_i_1_n_0 : STD_LOGIC;
  signal txbufstatus_reg : STD_LOGIC_VECTOR ( 1 to 1 );
  signal txchardispmode_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispmode_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispmode_reg : STD_LOGIC;
  signal txchardispval_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispval_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispval_reg : STD_LOGIC;
  signal txcharisk_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txcharisk_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txcharisk_reg : STD_LOGIC;
  signal txdata_double : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal txdata_int : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal txdata_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal txpowerdown : STD_LOGIC;
  signal txpowerdown_double : STD_LOGIC;
  signal \txpowerdown_reg__0\ : STD_LOGIC;
  signal txreset_int : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of rxchariscomma_i_1 : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of rxcharisk_i_1 : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \rxdata[0]_i_1\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \rxdata[1]_i_1\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \rxdata[2]_i_1\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \rxdata[3]_i_1\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \rxdata[4]_i_1\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \rxdata[5]_i_1\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \rxdata[6]_i_1\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \rxdata[7]_i_1\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of rxdisperr_i_1 : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of rxnotintable_i_1 : label is "soft_lutpair79";
begin
gtwizard_inst: entity work.gig_ethernet_pcs_pma_0_GTWIZARD
     port map (
      D(1 downto 0) => rxclkcorcnt_int(1 downto 0),
      Q(15 downto 0) => txdata_int(15 downto 0),
      RXBUFSTATUS(0) => gtwizard_inst_n_7,
      RXPD(0) => rxpowerdown,
      TXBUFSTATUS(0) => gtwizard_inst_n_6,
      TXPD(0) => txpowerdown,
      data_in => data_in,
      data_out => data_valid_reg2,
      data_sync_reg1 => data_sync_reg1,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i(15 downto 0) => rxdata_int(15 downto 0),
      gtxe2_i_0(1 downto 0) => rxchariscomma_int(1 downto 0),
      gtxe2_i_1(1 downto 0) => rxcharisk_int(1 downto 0),
      gtxe2_i_2(1 downto 0) => rxdisperr_int(1 downto 0),
      gtxe2_i_3(1 downto 0) => rxnotintable_int(1 downto 0),
      gtxe2_i_4 => gtxe2_i,
      gtxe2_i_5(1 downto 0) => txchardispmode_int(1 downto 0),
      gtxe2_i_6(1 downto 0) => txchardispval_int(1 downto 0),
      gtxe2_i_7(1 downto 0) => txcharisk_int(1 downto 0),
      gtxe2_i_8 => rxreset_int,
      gtxe2_i_9 => txreset_int,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0),
      reset => reset,
      reset_out => encommaalign_int,
      rx_fsm_reset_done_int_reg => rx_fsm_reset_done_int_reg,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
reclock_encommaalign: entity work.gig_ethernet_pcs_pma_0_reset_sync
     port map (
      CLK => CLK,
      enablealign => enablealign,
      reset_out => encommaalign_int
    );
reclock_rxreset: entity work.gig_ethernet_pcs_pma_0_reset_sync_1
     port map (
      SR(0) => SR(0),
      independent_clock_bufg => independent_clock_bufg,
      reset_out => rxreset_int
    );
reclock_txreset: entity work.gig_ethernet_pcs_pma_0_reset_sync_2
     port map (
      independent_clock_bufg => independent_clock_bufg,
      reset_out => txreset_int,
      reset_sync5_0(0) => reset_sync5(0)
    );
reset_wtd_timer: entity work.gig_ethernet_pcs_pma_0_reset_wtd_timer
     port map (
      data_out => data_valid_reg2,
      independent_clock_bufg => independent_clock_bufg,
      reset => reset
    );
rxbuferr_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => p_0_in,
      Q => rxbufstatus(0),
      R => '0'
    );
\rxbufstatus_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => gtwizard_inst_n_7,
      Q => p_0_in,
      R => '0'
    );
\rxchariscomma_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxchariscomma_reg__0\(0),
      Q => rxchariscomma_double(0),
      R => SR(0)
    );
\rxchariscomma_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxchariscomma_reg__0\(1),
      Q => rxchariscomma_double(1),
      R => SR(0)
    );
rxchariscomma_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxchariscomma_double(1),
      I1 => toggle_rx,
      I2 => rxchariscomma_double(0),
      O => rxchariscomma_i_1_n_0
    );
rxchariscomma_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxchariscomma_i_1_n_0,
      Q => rxchariscomma(0),
      R => SR(0)
    );
\rxchariscomma_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxchariscomma_int(0),
      Q => \rxchariscomma_reg__0\(0),
      R => '0'
    );
\rxchariscomma_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxchariscomma_int(1),
      Q => \rxchariscomma_reg__0\(1),
      R => '0'
    );
\rxcharisk_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxcharisk_reg__0\(0),
      Q => rxcharisk_double(0),
      R => SR(0)
    );
\rxcharisk_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxcharisk_reg__0\(1),
      Q => rxcharisk_double(1),
      R => SR(0)
    );
rxcharisk_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxcharisk_double(1),
      I1 => toggle_rx,
      I2 => rxcharisk_double(0),
      O => rxcharisk_i_1_n_0
    );
rxcharisk_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxcharisk_i_1_n_0,
      Q => rxcharisk(0),
      R => SR(0)
    );
\rxcharisk_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxcharisk_int(0),
      Q => \rxcharisk_reg__0\(0),
      R => '0'
    );
\rxcharisk_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxcharisk_int(1),
      Q => \rxcharisk_reg__0\(1),
      R => '0'
    );
\rxclkcorcnt_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxclkcorcnt_reg(0),
      Q => rxclkcorcnt_double(0),
      R => SR(0)
    );
\rxclkcorcnt_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxclkcorcnt_reg(1),
      Q => rxclkcorcnt_double(1),
      R => SR(0)
    );
\rxclkcorcnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxclkcorcnt_double(0),
      Q => Q(0),
      R => SR(0)
    );
\rxclkcorcnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxclkcorcnt_double(1),
      Q => Q(1),
      R => SR(0)
    );
\rxclkcorcnt_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxclkcorcnt_int(0),
      Q => rxclkcorcnt_reg(0),
      R => '0'
    );
\rxclkcorcnt_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxclkcorcnt_int(1),
      Q => rxclkcorcnt_reg(1),
      R => '0'
    );
\rxdata[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(8),
      I1 => toggle_rx,
      I2 => rxdata_double(0),
      O => \rxdata[0]_i_1_n_0\
    );
\rxdata[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(9),
      I1 => toggle_rx,
      I2 => rxdata_double(1),
      O => \rxdata[1]_i_1_n_0\
    );
\rxdata[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(10),
      I1 => toggle_rx,
      I2 => rxdata_double(2),
      O => \rxdata[2]_i_1_n_0\
    );
\rxdata[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(11),
      I1 => toggle_rx,
      I2 => rxdata_double(3),
      O => \rxdata[3]_i_1_n_0\
    );
\rxdata[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(12),
      I1 => toggle_rx,
      I2 => rxdata_double(4),
      O => \rxdata[4]_i_1_n_0\
    );
\rxdata[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(13),
      I1 => toggle_rx,
      I2 => rxdata_double(5),
      O => \rxdata[5]_i_1_n_0\
    );
\rxdata[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(14),
      I1 => toggle_rx,
      I2 => rxdata_double(6),
      O => \rxdata[6]_i_1_n_0\
    );
\rxdata[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(15),
      I1 => toggle_rx,
      I2 => rxdata_double(7),
      O => \rxdata[7]_i_1_n_0\
    );
\rxdata_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(0),
      Q => rxdata_double(0),
      R => SR(0)
    );
\rxdata_double_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(10),
      Q => rxdata_double(10),
      R => SR(0)
    );
\rxdata_double_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(11),
      Q => rxdata_double(11),
      R => SR(0)
    );
\rxdata_double_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(12),
      Q => rxdata_double(12),
      R => SR(0)
    );
\rxdata_double_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(13),
      Q => rxdata_double(13),
      R => SR(0)
    );
\rxdata_double_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(14),
      Q => rxdata_double(14),
      R => SR(0)
    );
\rxdata_double_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(15),
      Q => rxdata_double(15),
      R => SR(0)
    );
\rxdata_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(1),
      Q => rxdata_double(1),
      R => SR(0)
    );
\rxdata_double_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(2),
      Q => rxdata_double(2),
      R => SR(0)
    );
\rxdata_double_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(3),
      Q => rxdata_double(3),
      R => SR(0)
    );
\rxdata_double_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(4),
      Q => rxdata_double(4),
      R => SR(0)
    );
\rxdata_double_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(5),
      Q => rxdata_double(5),
      R => SR(0)
    );
\rxdata_double_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(6),
      Q => rxdata_double(6),
      R => SR(0)
    );
\rxdata_double_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(7),
      Q => rxdata_double(7),
      R => SR(0)
    );
\rxdata_double_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(8),
      Q => rxdata_double(8),
      R => SR(0)
    );
\rxdata_double_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(9),
      Q => rxdata_double(9),
      R => SR(0)
    );
\rxdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[0]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(0),
      R => SR(0)
    );
\rxdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[1]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(1),
      R => SR(0)
    );
\rxdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[2]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(2),
      R => SR(0)
    );
\rxdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[3]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(3),
      R => SR(0)
    );
\rxdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[4]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(4),
      R => SR(0)
    );
\rxdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[5]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(5),
      R => SR(0)
    );
\rxdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[6]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(6),
      R => SR(0)
    );
\rxdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[7]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(7),
      R => SR(0)
    );
\rxdata_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(0),
      Q => rxdata_reg(0),
      R => '0'
    );
\rxdata_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(10),
      Q => rxdata_reg(10),
      R => '0'
    );
\rxdata_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(11),
      Q => rxdata_reg(11),
      R => '0'
    );
\rxdata_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(12),
      Q => rxdata_reg(12),
      R => '0'
    );
\rxdata_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(13),
      Q => rxdata_reg(13),
      R => '0'
    );
\rxdata_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(14),
      Q => rxdata_reg(14),
      R => '0'
    );
\rxdata_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(15),
      Q => rxdata_reg(15),
      R => '0'
    );
\rxdata_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(1),
      Q => rxdata_reg(1),
      R => '0'
    );
\rxdata_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(2),
      Q => rxdata_reg(2),
      R => '0'
    );
\rxdata_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(3),
      Q => rxdata_reg(3),
      R => '0'
    );
\rxdata_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(4),
      Q => rxdata_reg(4),
      R => '0'
    );
\rxdata_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(5),
      Q => rxdata_reg(5),
      R => '0'
    );
\rxdata_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(6),
      Q => rxdata_reg(6),
      R => '0'
    );
\rxdata_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(7),
      Q => rxdata_reg(7),
      R => '0'
    );
\rxdata_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(8),
      Q => rxdata_reg(8),
      R => '0'
    );
\rxdata_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(9),
      Q => rxdata_reg(9),
      R => '0'
    );
\rxdisperr_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxdisperr_reg__0\(0),
      Q => rxdisperr_double(0),
      R => SR(0)
    );
\rxdisperr_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxdisperr_reg__0\(1),
      Q => rxdisperr_double(1),
      R => SR(0)
    );
rxdisperr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdisperr_double(1),
      I1 => toggle_rx,
      I2 => rxdisperr_double(0),
      O => rxdisperr_i_1_n_0
    );
rxdisperr_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxdisperr_i_1_n_0,
      Q => rxdisperr(0),
      R => SR(0)
    );
\rxdisperr_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdisperr_int(0),
      Q => \rxdisperr_reg__0\(0),
      R => '0'
    );
\rxdisperr_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdisperr_int(1),
      Q => \rxdisperr_reg__0\(1),
      R => '0'
    );
\rxnotintable_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxnotintable_reg__0\(0),
      Q => rxnotintable_double(0),
      R => SR(0)
    );
\rxnotintable_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxnotintable_reg__0\(1),
      Q => rxnotintable_double(1),
      R => SR(0)
    );
rxnotintable_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxnotintable_double(1),
      I1 => toggle_rx,
      I2 => rxnotintable_double(0),
      O => rxnotintable_i_1_n_0
    );
rxnotintable_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxnotintable_i_1_n_0,
      Q => rxnotintable(0),
      R => SR(0)
    );
\rxnotintable_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxnotintable_int(0),
      Q => \rxnotintable_reg__0\(0),
      R => '0'
    );
\rxnotintable_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxnotintable_int(1),
      Q => \rxnotintable_reg__0\(1),
      R => '0'
    );
rxpowerdown_double_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxpowerdown_reg__0\,
      Q => rxpowerdown_double,
      R => SR(0)
    );
rxpowerdown_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gtxe2_i,
      CE => '1',
      D => rxpowerdown_double,
      Q => rxpowerdown,
      R => '0'
    );
rxpowerdown_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => powerdown,
      Q => \rxpowerdown_reg__0\,
      R => SR(0)
    );
sync_block_data_valid: entity work.gig_ethernet_pcs_pma_0_sync_block_3
     port map (
      data_out => data_valid_reg2,
      independent_clock_bufg => independent_clock_bufg,
      status_vector(0) => status_vector(0)
    );
toggle_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => toggle,
      O => toggle_i_1_n_0
    );
toggle_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => toggle_i_1_n_0,
      Q => toggle,
      R => reset_sync5(0)
    );
toggle_rx_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => toggle_rx,
      O => toggle_rx_i_1_n_0
    );
toggle_rx_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => toggle_rx_i_1_n_0,
      Q => toggle_rx,
      R => SR(0)
    );
txbuferr_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => txbufstatus_reg(1),
      Q => txbuferr,
      R => '0'
    );
\txbufstatus_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => gtwizard_inst_n_6,
      Q => txbufstatus_reg(1),
      R => '0'
    );
\txchardispmode_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txchardispmode_reg,
      Q => txchardispmode_double(0),
      R => reset_sync5(0)
    );
\txchardispmode_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => D(0),
      Q => txchardispmode_double(1),
      R => reset_sync5(0)
    );
\txchardispmode_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispmode_double(0),
      Q => txchardispmode_int(0),
      R => '0'
    );
\txchardispmode_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispmode_double(1),
      Q => txchardispmode_int(1),
      R => '0'
    );
txchardispmode_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => txchardispmode_reg,
      R => reset_sync5(0)
    );
\txchardispval_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txchardispval_reg,
      Q => txchardispval_double(0),
      R => reset_sync5(0)
    );
\txchardispval_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txchardispval_reg_reg_0(0),
      Q => txchardispval_double(1),
      R => reset_sync5(0)
    );
\txchardispval_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispval_double(0),
      Q => txchardispval_int(0),
      R => '0'
    );
\txchardispval_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispval_double(1),
      Q => txchardispval_int(1),
      R => '0'
    );
txchardispval_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => txchardispval_reg_reg_0(0),
      Q => txchardispval_reg,
      R => reset_sync5(0)
    );
\txcharisk_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txcharisk_reg,
      Q => txcharisk_double(0),
      R => reset_sync5(0)
    );
\txcharisk_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txcharisk_reg_reg_0(0),
      Q => txcharisk_double(1),
      R => reset_sync5(0)
    );
\txcharisk_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txcharisk_double(0),
      Q => txcharisk_int(0),
      R => '0'
    );
\txcharisk_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txcharisk_double(1),
      Q => txcharisk_int(1),
      R => '0'
    );
txcharisk_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => txcharisk_reg_reg_0(0),
      Q => txcharisk_reg,
      R => reset_sync5(0)
    );
\txdata_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(0),
      Q => txdata_double(0),
      R => reset_sync5(0)
    );
\txdata_double_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(2),
      Q => txdata_double(10),
      R => reset_sync5(0)
    );
\txdata_double_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(3),
      Q => txdata_double(11),
      R => reset_sync5(0)
    );
\txdata_double_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(4),
      Q => txdata_double(12),
      R => reset_sync5(0)
    );
\txdata_double_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(5),
      Q => txdata_double(13),
      R => reset_sync5(0)
    );
\txdata_double_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(6),
      Q => txdata_double(14),
      R => reset_sync5(0)
    );
\txdata_double_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(7),
      Q => txdata_double(15),
      R => reset_sync5(0)
    );
\txdata_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(1),
      Q => txdata_double(1),
      R => reset_sync5(0)
    );
\txdata_double_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(2),
      Q => txdata_double(2),
      R => reset_sync5(0)
    );
\txdata_double_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(3),
      Q => txdata_double(3),
      R => reset_sync5(0)
    );
\txdata_double_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(4),
      Q => txdata_double(4),
      R => reset_sync5(0)
    );
\txdata_double_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(5),
      Q => txdata_double(5),
      R => reset_sync5(0)
    );
\txdata_double_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(6),
      Q => txdata_double(6),
      R => reset_sync5(0)
    );
\txdata_double_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(7),
      Q => txdata_double(7),
      R => reset_sync5(0)
    );
\txdata_double_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(0),
      Q => txdata_double(8),
      R => reset_sync5(0)
    );
\txdata_double_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(1),
      Q => txdata_double(9),
      R => reset_sync5(0)
    );
\txdata_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(0),
      Q => txdata_int(0),
      R => '0'
    );
\txdata_int_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(10),
      Q => txdata_int(10),
      R => '0'
    );
\txdata_int_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(11),
      Q => txdata_int(11),
      R => '0'
    );
\txdata_int_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(12),
      Q => txdata_int(12),
      R => '0'
    );
\txdata_int_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(13),
      Q => txdata_int(13),
      R => '0'
    );
\txdata_int_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(14),
      Q => txdata_int(14),
      R => '0'
    );
\txdata_int_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(15),
      Q => txdata_int(15),
      R => '0'
    );
\txdata_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(1),
      Q => txdata_int(1),
      R => '0'
    );
\txdata_int_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(2),
      Q => txdata_int(2),
      R => '0'
    );
\txdata_int_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(3),
      Q => txdata_int(3),
      R => '0'
    );
\txdata_int_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(4),
      Q => txdata_int(4),
      R => '0'
    );
\txdata_int_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(5),
      Q => txdata_int(5),
      R => '0'
    );
\txdata_int_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(6),
      Q => txdata_int(6),
      R => '0'
    );
\txdata_int_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(7),
      Q => txdata_int(7),
      R => '0'
    );
\txdata_int_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(8),
      Q => txdata_int(8),
      R => '0'
    );
\txdata_int_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(9),
      Q => txdata_int(9),
      R => '0'
    );
\txdata_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(0),
      Q => txdata_reg(0),
      R => reset_sync5(0)
    );
\txdata_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(1),
      Q => txdata_reg(1),
      R => reset_sync5(0)
    );
\txdata_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(2),
      Q => txdata_reg(2),
      R => reset_sync5(0)
    );
\txdata_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(3),
      Q => txdata_reg(3),
      R => reset_sync5(0)
    );
\txdata_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(4),
      Q => txdata_reg(4),
      R => reset_sync5(0)
    );
\txdata_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(5),
      Q => txdata_reg(5),
      R => reset_sync5(0)
    );
\txdata_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(6),
      Q => txdata_reg(6),
      R => reset_sync5(0)
    );
\txdata_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(7),
      Q => txdata_reg(7),
      R => reset_sync5(0)
    );
txpowerdown_double_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \txpowerdown_reg__0\,
      Q => txpowerdown_double,
      R => reset_sync5(0)
    );
txpowerdown_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gtxe2_i,
      CE => '1',
      D => txpowerdown_double,
      Q => txpowerdown,
      R => '0'
    );
txpowerdown_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => powerdown,
      Q => \txpowerdown_reg__0\,
      R => reset_sync5(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_block is
  port (
    gmii_rxd : out STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_rx_dv : out STD_LOGIC;
    gmii_rx_er : out STD_LOGIC;
    gmii_isolate : out STD_LOGIC;
    status_vector : out STD_LOGIC_VECTOR ( 6 downto 0 );
    resetdone : out STD_LOGIC;
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    mmcm_reset : out STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    signal_detect : in STD_LOGIC;
    CLK : in STD_LOGIC;
    data_in : in STD_LOGIC;
    gmii_txd : in STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_tx_en : in STD_LOGIC;
    gmii_tx_er : in STD_LOGIC;
    configuration_vector : in STD_LOGIC_VECTOR ( 2 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    gtxe2_i : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_block;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_block is
  signal enablealign : STD_LOGIC;
  signal mgt_rx_reset : STD_LOGIC;
  signal mgt_tx_reset : STD_LOGIC;
  signal powerdown : STD_LOGIC;
  signal \^resetdone\ : STD_LOGIC;
  signal rx_reset_done_i : STD_LOGIC;
  signal rxbuferr : STD_LOGIC;
  signal rxchariscomma : STD_LOGIC;
  signal rxcharisk : STD_LOGIC;
  signal rxclkcorcnt : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxdata : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal rxdisperr : STD_LOGIC;
  signal rxnotintable : STD_LOGIC;
  signal \^status_vector\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal transceiver_inst_n_11 : STD_LOGIC;
  signal transceiver_inst_n_12 : STD_LOGIC;
  signal txbuferr : STD_LOGIC;
  signal txchardispmode : STD_LOGIC;
  signal txchardispval : STD_LOGIC;
  signal txcharisk : STD_LOGIC;
  signal txdata : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 7 );
  signal NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  attribute B_SHIFTER_ADDR : string;
  attribute B_SHIFTER_ADDR of gig_ethernet_pcs_pma_0_core : label is "10'b0101001110";
  attribute C_1588 : integer;
  attribute C_1588 of gig_ethernet_pcs_pma_0_core : label is 0;
  attribute C_2_5G : string;
  attribute C_2_5G of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_COMPONENT_NAME : string;
  attribute C_COMPONENT_NAME of gig_ethernet_pcs_pma_0_core : label is "gig_ethernet_pcs_pma_0";
  attribute C_DYNAMIC_SWITCHING : string;
  attribute C_DYNAMIC_SWITCHING of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_ELABORATION_TRANSIENT_DIR : string;
  attribute C_ELABORATION_TRANSIENT_DIR of gig_ethernet_pcs_pma_0_core : label is "BlankString";
  attribute C_FAMILY : string;
  attribute C_FAMILY of gig_ethernet_pcs_pma_0_core : label is "kintex7";
  attribute C_HAS_AN : string;
  attribute C_HAS_AN of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_HAS_AXIL : string;
  attribute C_HAS_AXIL of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_HAS_MDIO : string;
  attribute C_HAS_MDIO of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_HAS_TEMAC : string;
  attribute C_HAS_TEMAC of gig_ethernet_pcs_pma_0_core : label is "TRUE";
  attribute C_IS_SGMII : string;
  attribute C_IS_SGMII of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_RX_GMII_CLK : string;
  attribute C_RX_GMII_CLK of gig_ethernet_pcs_pma_0_core : label is "TXOUTCLK";
  attribute C_SGMII_FABRIC_BUFFER : string;
  attribute C_SGMII_FABRIC_BUFFER of gig_ethernet_pcs_pma_0_core : label is "TRUE";
  attribute C_SGMII_PHY_MODE : string;
  attribute C_SGMII_PHY_MODE of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_USE_LVDS : string;
  attribute C_USE_LVDS of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_USE_TBI : string;
  attribute C_USE_TBI of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_USE_TRANSCEIVER : string;
  attribute C_USE_TRANSCEIVER of gig_ethernet_pcs_pma_0_core : label is "TRUE";
  attribute GT_RX_BYTE_WIDTH : integer;
  attribute GT_RX_BYTE_WIDTH of gig_ethernet_pcs_pma_0_core : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of gig_ethernet_pcs_pma_0_core : label is "soft";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of gig_ethernet_pcs_pma_0_core : label is "yes";
  attribute is_du_within_envelope : string;
  attribute is_du_within_envelope of gig_ethernet_pcs_pma_0_core : label is "true";
begin
  resetdone <= \^resetdone\;
  status_vector(6 downto 0) <= \^status_vector\(6 downto 0);
gig_ethernet_pcs_pma_0_core: entity work.gig_ethernet_pcs_pma_0_gig_ethernet_pcs_pma_v16_2_19
     port map (
      an_adv_config_val => '0',
      an_adv_config_vector(15 downto 0) => B"0000000000000000",
      an_enable => NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED,
      an_interrupt => NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED,
      an_restart_config => '0',
      basex_or_sgmii => '0',
      configuration_valid => '0',
      configuration_vector(4) => '0',
      configuration_vector(3 downto 1) => configuration_vector(2 downto 0),
      configuration_vector(0) => '0',
      correction_timer(63 downto 0) => B"0000000000000000000000000000000000000000000000000000000000000000",
      dcm_locked => data_in,
      drp_daddr(9 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED(9 downto 0),
      drp_dclk => '0',
      drp_den => NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED,
      drp_di(15 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED(15 downto 0),
      drp_do(15 downto 0) => B"0000000000000000",
      drp_drdy => '0',
      drp_dwe => NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED,
      drp_gnt => '0',
      drp_req => NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED,
      en_cdet => NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED,
      enablealign => enablealign,
      ewrap => NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED,
      gmii_isolate => gmii_isolate,
      gmii_rx_dv => gmii_rx_dv,
      gmii_rx_er => gmii_rx_er,
      gmii_rxd(7 downto 0) => gmii_rxd(7 downto 0),
      gmii_tx_en => gmii_tx_en,
      gmii_tx_er => gmii_tx_er,
      gmii_txd(7 downto 0) => gmii_txd(7 downto 0),
      gtx_clk => '0',
      link_timer_basex(9 downto 0) => B"0000000000",
      link_timer_sgmii(9 downto 0) => B"0000000000",
      link_timer_value(9 downto 0) => B"0000000000",
      loc_ref => NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED,
      mdc => '0',
      mdio_in => '0',
      mdio_out => NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED,
      mdio_tri => NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED,
      mgt_rx_reset => mgt_rx_reset,
      mgt_tx_reset => mgt_tx_reset,
      phyad(4 downto 0) => B"00000",
      pma_rx_clk0 => '0',
      pma_rx_clk1 => '0',
      powerdown => powerdown,
      reset => \out\(0),
      reset_done => \^resetdone\,
      rx_code_group0(9 downto 0) => B"0000000000",
      rx_code_group1(9 downto 0) => B"0000000000",
      rx_gt_nominal_latency(15 downto 0) => B"0000000011111000",
      rxbufstatus(1) => rxbuferr,
      rxbufstatus(0) => '0',
      rxchariscomma(0) => rxchariscomma,
      rxcharisk(0) => rxcharisk,
      rxclkcorcnt(2) => '0',
      rxclkcorcnt(1 downto 0) => rxclkcorcnt(1 downto 0),
      rxdata(7 downto 0) => rxdata(7 downto 0),
      rxdisperr(0) => rxdisperr,
      rxnotintable(0) => rxnotintable,
      rxphy_correction_timer(63 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED(63 downto 0),
      rxphy_ns_field(31 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED(31 downto 0),
      rxphy_s_field(47 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED(47 downto 0),
      rxrecclk => '0',
      rxrundisp(0) => '0',
      s_axi_aclk => '0',
      s_axi_araddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_arready => NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED,
      s_axi_arvalid => '0',
      s_axi_awaddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_awready => NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED,
      s_axi_awvalid => '0',
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_bvalid => NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED,
      s_axi_rdata(31 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED(31 downto 0),
      s_axi_resetn => '0',
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_rvalid => NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED,
      s_axi_wdata(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_wready => NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED,
      s_axi_wvalid => '0',
      signal_detect => signal_detect,
      speed_is_100 => '0',
      speed_is_10_100 => '0',
      speed_selection(1 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED(1 downto 0),
      status_vector(15 downto 7) => NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED(15 downto 7),
      status_vector(6 downto 0) => \^status_vector\(6 downto 0),
      systemtimer_ns_field(31 downto 0) => B"00000000000000000000000000000000",
      systemtimer_s_field(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      tx_code_group(9 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED(9 downto 0),
      txbuferr => txbuferr,
      txchardispmode => txchardispmode,
      txchardispval => txchardispval,
      txcharisk => txcharisk,
      txdata(7 downto 0) => txdata(7 downto 0),
      userclk => '0',
      userclk2 => CLK
    );
sync_block_rx_reset_done: entity work.gig_ethernet_pcs_pma_0_sync_block
     port map (
      CLK => CLK,
      data_in => transceiver_inst_n_12,
      data_out => rx_reset_done_i
    );
sync_block_tx_reset_done: entity work.gig_ethernet_pcs_pma_0_sync_block_0
     port map (
      CLK => CLK,
      data_in => transceiver_inst_n_11,
      resetdone => \^resetdone\,
      resetdone_0 => rx_reset_done_i
    );
transceiver_inst: entity work.gig_ethernet_pcs_pma_0_transceiver
     port map (
      CLK => CLK,
      D(0) => txchardispmode,
      Q(1 downto 0) => rxclkcorcnt(1 downto 0),
      SR(0) => mgt_rx_reset,
      data_in => transceiver_inst_n_11,
      data_sync_reg1 => data_in,
      enablealign => enablealign,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i => gtxe2_i,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0),
      powerdown => powerdown,
      reset_sync5(0) => mgt_tx_reset,
      rx_fsm_reset_done_int_reg => transceiver_inst_n_12,
      rxbufstatus(0) => rxbuferr,
      rxchariscomma(0) => rxchariscomma,
      rxcharisk(0) => rxcharisk,
      \rxdata_reg[7]_0\(7 downto 0) => rxdata(7 downto 0),
      rxdisperr(0) => rxdisperr,
      rxn => rxn,
      rxnotintable(0) => rxnotintable,
      rxoutclk => rxoutclk,
      rxp => rxp,
      status_vector(0) => \^status_vector\(1),
      txbuferr => txbuferr,
      txchardispval_reg_reg_0(0) => txchardispval,
      txcharisk_reg_reg_0(0) => txcharisk,
      \txdata_reg_reg[7]_0\(7 downto 0) => txdata(7 downto 0),
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_support is
  port (
    gtrefclk_p : in STD_LOGIC;
    gtrefclk_n : in STD_LOGIC;
    gtrefclk_out : out STD_LOGIC;
    gtrefclk_bufg_out : out STD_LOGIC;
    txp : out STD_LOGIC;
    txn : out STD_LOGIC;
    rxp : in STD_LOGIC;
    rxn : in STD_LOGIC;
    userclk_out : out STD_LOGIC;
    userclk2_out : out STD_LOGIC;
    rxuserclk_out : out STD_LOGIC;
    rxuserclk2_out : out STD_LOGIC;
    pma_reset_out : out STD_LOGIC;
    mmcm_locked_out : out STD_LOGIC;
    resetdone : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gmii_txd : in STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_tx_en : in STD_LOGIC;
    gmii_tx_er : in STD_LOGIC;
    gmii_rxd : out STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_rx_dv : out STD_LOGIC;
    gmii_rx_er : out STD_LOGIC;
    gmii_isolate : out STD_LOGIC;
    configuration_vector : in STD_LOGIC_VECTOR ( 4 downto 0 );
    status_vector : out STD_LOGIC_VECTOR ( 15 downto 0 );
    reset : in STD_LOGIC;
    signal_detect : in STD_LOGIC;
    gt0_qplloutclk_out : out STD_LOGIC;
    gt0_qplloutrefclk_out : out STD_LOGIC
  );
  attribute EXAMPLE_SIMULATION : integer;
  attribute EXAMPLE_SIMULATION of gig_ethernet_pcs_pma_0_support : entity is 0;
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of gig_ethernet_pcs_pma_0_support : entity is "yes";
end gig_ethernet_pcs_pma_0_support;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_support is
  signal \<const0>\ : STD_LOGIC;
  signal \^gt0_qplloutclk_out\ : STD_LOGIC;
  signal \^gt0_qplloutrefclk_out\ : STD_LOGIC;
  signal \^gtrefclk_bufg_out\ : STD_LOGIC;
  signal \^gtrefclk_out\ : STD_LOGIC;
  signal \^mmcm_locked_out\ : STD_LOGIC;
  signal mmcm_reset : STD_LOGIC;
  signal \^pma_reset_out\ : STD_LOGIC;
  signal rxoutclk : STD_LOGIC;
  signal \^rxuserclk2_out\ : STD_LOGIC;
  signal \^status_vector\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal txoutclk : STD_LOGIC;
  signal \^userclk2_out\ : STD_LOGIC;
  signal \^userclk_out\ : STD_LOGIC;
begin
  gt0_qplloutclk_out <= \^gt0_qplloutclk_out\;
  gt0_qplloutrefclk_out <= \^gt0_qplloutrefclk_out\;
  gtrefclk_bufg_out <= \^gtrefclk_bufg_out\;
  gtrefclk_out <= \^gtrefclk_out\;
  mmcm_locked_out <= \^mmcm_locked_out\;
  pma_reset_out <= \^pma_reset_out\;
  rxuserclk2_out <= \^rxuserclk2_out\;
  rxuserclk_out <= \^rxuserclk2_out\;
  status_vector(15) <= \<const0>\;
  status_vector(14) <= \<const0>\;
  status_vector(13) <= \<const0>\;
  status_vector(12) <= \<const0>\;
  status_vector(11) <= \<const0>\;
  status_vector(10) <= \<const0>\;
  status_vector(9) <= \<const0>\;
  status_vector(8) <= \<const0>\;
  status_vector(7) <= \<const0>\;
  status_vector(6 downto 0) <= \^status_vector\(6 downto 0);
  userclk2_out <= \^userclk2_out\;
  userclk_out <= \^userclk_out\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
core_clocking_i: entity work.gig_ethernet_pcs_pma_0_clocking
     port map (
      gtrefclk_bufg => \^gtrefclk_bufg_out\,
      gtrefclk_n => gtrefclk_n,
      gtrefclk_out => \^gtrefclk_out\,
      gtrefclk_p => gtrefclk_p,
      mmcm_locked => \^mmcm_locked_out\,
      mmcm_reset => mmcm_reset,
      rxoutclk => rxoutclk,
      rxuserclk2_out => \^rxuserclk2_out\,
      txoutclk => txoutclk,
      userclk => \^userclk_out\,
      userclk2 => \^userclk2_out\
    );
core_gt_common_i: entity work.gig_ethernet_pcs_pma_0_gt_common
     port map (
      gt0_qplloutclk_out => \^gt0_qplloutclk_out\,
      gt0_qplloutrefclk_out => \^gt0_qplloutrefclk_out\,
      gtrefclk_out => \^gtrefclk_out\,
      independent_clock_bufg => independent_clock_bufg,
      \out\(0) => \^pma_reset_out\
    );
core_resets_i: entity work.gig_ethernet_pcs_pma_0_resets
     port map (
      independent_clock_bufg => independent_clock_bufg,
      \out\(0) => \^pma_reset_out\,
      reset => reset
    );
pcs_pma_block_i: entity work.gig_ethernet_pcs_pma_0_block
     port map (
      CLK => \^userclk2_out\,
      configuration_vector(2 downto 0) => configuration_vector(3 downto 1),
      data_in => \^mmcm_locked_out\,
      gmii_isolate => gmii_isolate,
      gmii_rx_dv => gmii_rx_dv,
      gmii_rx_er => gmii_rx_er,
      gmii_rxd(7 downto 0) => gmii_rxd(7 downto 0),
      gmii_tx_en => gmii_tx_en,
      gmii_tx_er => gmii_tx_er,
      gmii_txd(7 downto 0) => gmii_txd(7 downto 0),
      gt0_qplloutclk_out => \^gt0_qplloutclk_out\,
      gt0_qplloutrefclk_out => \^gt0_qplloutrefclk_out\,
      gtrefclk_bufg => \^gtrefclk_bufg_out\,
      gtrefclk_out => \^gtrefclk_out\,
      gtxe2_i => \^userclk_out\,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \^pma_reset_out\,
      resetdone => resetdone,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      signal_detect => signal_detect,
      status_vector(6 downto 0) => \^status_vector\(6 downto 0),
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0 is
  port (
    gtrefclk_p : in STD_LOGIC;
    gtrefclk_n : in STD_LOGIC;
    gtrefclk_out : out STD_LOGIC;
    gtrefclk_bufg_out : out STD_LOGIC;
    txp : out STD_LOGIC;
    txn : out STD_LOGIC;
    rxp : in STD_LOGIC;
    rxn : in STD_LOGIC;
    resetdone : out STD_LOGIC;
    userclk_out : out STD_LOGIC;
    userclk2_out : out STD_LOGIC;
    rxuserclk_out : out STD_LOGIC;
    rxuserclk2_out : out STD_LOGIC;
    pma_reset_out : out STD_LOGIC;
    mmcm_locked_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gmii_txd : in STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_tx_en : in STD_LOGIC;
    gmii_tx_er : in STD_LOGIC;
    gmii_rxd : out STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_rx_dv : out STD_LOGIC;
    gmii_rx_er : out STD_LOGIC;
    gmii_isolate : out STD_LOGIC;
    configuration_vector : in STD_LOGIC_VECTOR ( 4 downto 0 );
    status_vector : out STD_LOGIC_VECTOR ( 15 downto 0 );
    reset : in STD_LOGIC;
    signal_detect : in STD_LOGIC;
    gt0_qplloutclk_out : out STD_LOGIC;
    gt0_qplloutrefclk_out : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of gig_ethernet_pcs_pma_0 : entity is true;
  attribute EXAMPLE_SIMULATION : integer;
  attribute EXAMPLE_SIMULATION of gig_ethernet_pcs_pma_0 : entity is 0;
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of gig_ethernet_pcs_pma_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of gig_ethernet_pcs_pma_0 : entity is "gig_ethernet_pcs_pma_v16_2_19,Vivado 2024.1.1";
end gig_ethernet_pcs_pma_0;

architecture STRUCTURE of gig_ethernet_pcs_pma_0 is
  signal \<const0>\ : STD_LOGIC;
  signal \^status_vector\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal NLW_U0_status_vector_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 7 );
  attribute EXAMPLE_SIMULATION of U0 : label is 0;
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
begin
  status_vector(15) <= \<const0>\;
  status_vector(14) <= \<const0>\;
  status_vector(13) <= \<const0>\;
  status_vector(12) <= \<const0>\;
  status_vector(11) <= \<const0>\;
  status_vector(10) <= \<const0>\;
  status_vector(9) <= \<const0>\;
  status_vector(8) <= \<const0>\;
  status_vector(7) <= \<const0>\;
  status_vector(6 downto 0) <= \^status_vector\(6 downto 0);
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.gig_ethernet_pcs_pma_0_support
     port map (
      configuration_vector(4) => '0',
      configuration_vector(3 downto 1) => configuration_vector(3 downto 1),
      configuration_vector(0) => '0',
      gmii_isolate => gmii_isolate,
      gmii_rx_dv => gmii_rx_dv,
      gmii_rx_er => gmii_rx_er,
      gmii_rxd(7 downto 0) => gmii_rxd(7 downto 0),
      gmii_tx_en => gmii_tx_en,
      gmii_tx_er => gmii_tx_er,
      gmii_txd(7 downto 0) => gmii_txd(7 downto 0),
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg_out => gtrefclk_bufg_out,
      gtrefclk_n => gtrefclk_n,
      gtrefclk_out => gtrefclk_out,
      gtrefclk_p => gtrefclk_p,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_locked_out => mmcm_locked_out,
      pma_reset_out => pma_reset_out,
      reset => reset,
      resetdone => resetdone,
      rxn => rxn,
      rxp => rxp,
      rxuserclk2_out => rxuserclk2_out,
      rxuserclk_out => rxuserclk_out,
      signal_detect => signal_detect,
      status_vector(15 downto 7) => NLW_U0_status_vector_UNCONNECTED(15 downto 7),
      status_vector(6 downto 0) => \^status_vector\(6 downto 0),
      txn => txn,
      txp => txp,
      userclk2_out => userclk2_out,
      userclk_out => userclk_out
    );
end STRUCTURE;
